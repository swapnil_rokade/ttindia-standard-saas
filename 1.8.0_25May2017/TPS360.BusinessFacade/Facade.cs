/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Facade.cs
    Description: This is the class library page.
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              Nov-10-2008           Jagadish            Defect ID: 9184; (Kaizen) Added a method called 'DeleteJobPostingSkillSetByJobPostingId'
                                                               to delete jobposting skills having same JobPostingId.
    0.2              Nov-27-2008           Gopala Swamy        Defect Id: 9336 2)Added extra parameter to  function "CreateJobPostingFromExistingJob" [2 lines]  
    0.3              Dec-02-2008           Shivanand           Defect #8745; In method GetAllCandidateOnSearch() new parameter "strState" is added and used to call
                                                                    method GetAllOnSearch().
                                                                    In method GetCandidateSearchQuery(),new parameter "strState" is added and used to call
                                                                    method GetSearchQuery().
                                                                    In method GetAllConsultantOnSearch(),new parameter "state" is added and used to call
                                                                    method GetAllOnSearch().
                                                                    In method GetConsultantSearchQuery(),new parameter "state" is added and used to call
                                                                    method GetSearchQuery().  
    0.4             Dec-10-2008             Yogeesh Bhat        Defect ID: 9398: Added new methods GetRequisitionCountByMemberId, UpdateMemberRequisitionCount
    0.5             Jan-09-2009             Jagadish            Defect ID: 9073; Added an overload for method 'GetAllCommonNoteByMemberId'.
    0.6             Jan-22-2009             Jagadish            Defect ID: 8822; Implemented sorting.
    0.7             Feb-02-2009             Gopala Swamy        Defect Id: 9061; Prototypes are changed for "GetAllCommonNoteByProjectId()"
    0.8             Feb-02-2008             Sandeesh            Defect ID: 9376; Implemented sorting.
    0.9             Feb-04-2009             Gopala Swamy        Defect Id: 9090; Added  overloaded function "GetAllMemberSkillByMemberId(int,string)";
    0.11            Feb-11-2009             Jagadish            Defect ID: 8871; Added an overload for method 'GetAllMemberTimeSheetDetailByMemberTimeSheetId'.
 *  0.12            Feb-20-2009             Rajendra            Defect ID: 9811; Added an parameter "string jobIndustry" for overload method 'GetPagedVolumeHireJobPosting'.
 *  0.13            Feb-26-2009             Sandeesh            Defect id :9671 Added  new method to implement sorting
 *  0.14            04-Feb-2009             Sandeesh            Defect Fix - ID: 10027; Added new method to get the H1B1 details of the employee.
 *  0.15            10-Mar-2009             Nagarathna          Defect Id:10068;    added as overload for method "GetAllJobPostingByProjectId".
 *  0.16            18-Mar-2009             Rajendra            Defect id:10165; Added an update overload method "UpdateMemberH1B1Document".
 *  0.17            4-April-2009            Nagarathna V.B      Defect Id:9545 Added  new method "GetMemberSSNCount"
 *  0.18            28-April-2009           Nagarathna V.B      DefectId:10258  Added new method "GetCompanyGroupContact"
 *  0.19            28-May-2009                Veda             DefectId:10493 Added new method "GetLicenseKeybyId"
 *  2.0             8-June-2009             Nagarathna V.B      Enhancement:10525; added "GetProductivityOverviewByDate"
    2.1             July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetCountryIdByCountryCode() to assist 
                                                                Resume parser team
 *  2.2             Sept-8-2009           Sukanta               Added GetDuplicateRecords method 
 *  2.3             Sept-18-2009          Gopala Swamy J        Defect Id:11375;Added new Method
 *  2.4             Sept 24-2009          Nagarathna V.B        Enhancement #11473; add GetMemberExtendedInformationByMemberId(),GetSiteSettingBySettingType
 *  2.5             Nov-18-2009           Sandeesh              Enhancement #10998 :-Added a new method 'GetMemberUserNameById' for getting the username of the user
 *  2.6             Nov-25-2009           Gopala Swamy J        Defect Id:11588 :Added one parameter
 *  2.7             Dec-17-2009           Rajendra              Defect Id:11983 :Added new method MemberSubmission();
 *  2.8             Feb-22-2010            Nagarathna             Enhancement:12129:Submission Report,GetPrimaryContact();
 *  0.26            22/May/2015            Prasanth Kumar G     Introduced GetPagedRequisitionAgingReport
 *  0.27            20/Jul/2015            Prasanth Kumar G     Introduced InterviewerFeedback
 *  0.28            16/Oct/2015            Prasanth Kumar G     Introduced InterviewQuestionBank,QuestionBank,InterivewResponse
 *  0.29            05/Dec/2015            Prasanth Kumar G     Introduced InterviewAssessment_GetBy_InterviewId_InterviewerEmail
 *  0.29            20/nov/2015            Pravin khot          Introduced by Interview Panel
 *  0.30            27/nov/2015            Pravin khot          Introduced by Interview Panel Master
 *  0.31            02/DEC/2015            pravin khot          Introduced by GetAllMemberNameWithEmailByRoleName_IP,GetAllCompanyContactByComapnyId_IP
 *  0.32            09/Dec/2015            pravin khot          Introduced by GetAllSuggestedInterviewerWithEmail
 *  0.33            10/Dec/2015            pravin khot          Introduced by  Int32 IFacade.SuggestedInterviewer_Id(string InterviewrEmail)
 *  0.34            11/Dec/2015            pravin khot          Introduced by AddSuggestedInterviewInterviewerMap
 *  0.35            15/Dec/2015            pravin khot          Introduced by MemberInterview_SuggestedInterviewerId(int interviewId, int InterviewerId)
 *  0.36            24/Dec/2015            Prasanth Kumar G     Introduced Member Interviewe feedback document
 *  0.36            27/Dec/2015            Prasanth Kumar G     Introduced Function InterviewFeedback_GetByInterviewId_InterviewerEmail

 *  0.37            24/Dec/2015            pravin khot           Introduced Function InterviewFeedback_GetByInterviewIdDetail,
 *                                                                InterviewFeedback_GetByInterviewIdAllEmail,GetPagedInterviewFeedbackReport,
 *                                                                GetPagedInterviewFeedbackReportPrint,GetPagedInterviewAssesReportPrint
 *  0.38            27/01/2016             suresh.s             Introduced by GetPagedMemberJobCartForEmployeeReferal
 *  0.39            8/Jan/2016             pravin khot            Introduced by MemberCareerOpportunities     
 *  0.40            12/Jan/2016            Pravin khot            Introduced by GetPagedCareerPortalSubmissionsList.
 *  0.41            18/Jan/2016            pravin khot            Introduced by CareerJobId_Add.
 *  0.42            25/Jan/2016            pravin khot            Introduced by MemberCareerPortalToRequisition_Add.
 *  0.43            29/Jan/2016            pravin khot            Introduced by GetAllByBUID.
 *  0.44             2/Feb/2016            Pravin khot            Introduced by GetPagedRequisitionSourceBreakupReport.
 *  0.45             23/Feb/2016           Pravin khot             Introduced by DeleteRoleRequisitionDenial,UpdateRoleRequisitionDenial,GetCustomRoleRequisition
 *  0.46                23/Feb/2016          pravin khot          Introduced by  #region UserRoleMapEditor
 *  0.47              26/Feb/2016          pravin khot            Introduced by GetCompanyContactByEmailAndCompanyId
    0.48              3/March/2016         pravin khot            Introduced by GetAllCustomUser
 *  0.49              4/March/2016         pravin khot            Introduced by GetPagedCandidateCDMDetailsReport
 *  0.50              7/Macrh/2016         pravin khot            Introduced by GetAllMemberNameWithEmailByRoleNameIfExisting
 *  0.51              14/March/2016        pravin khot            Introduced by GetPagedCandidateReject,GetPagedRejectCandidateForSelectedColumn
 *  0.52              27/April/2016        pravin khot            Introduced by UpdateMemberthroughvendor,CandidateAvailableInRequisition_Name,MemberJobCart_AddCandidateToMultipleRequisition
 *  0.53               5/May/2016          pravin khot            Introduced by CancelInterviewById , UpdateInterviewIcsCodeById,GetAllsuggestedInterviewersByInterviewId
    0.54               13/May/2016         pravin khot            Introduced by HiringMatrixLevel_Id
 *  0.55              16/May/2016          pravin khot            added by GetAllMemberNameByTeamMemberId
 *  0.56              24/May/2016          pravin khot            added- UpdateMemberManagerIsPrimary
 *  0.58              25/May/2016          pravin khot            added- GetRequisitionStatusBy_Id , GetRequisitionStatus_Id , TIMEZONE
 *  0.59              20/May/2016          Sumit Sonawane         GetAllTeamByTeamLeaderId
 *  0.60              26/May/2016          Sumit Sonawane         EmployeeReferral_GetCountGroupbyDate
 *  0.61              23/May/2016          Sumit Sonawane
 *  o.62              7/June/2016          pravin khot            added- GetEmployeeByEmailId
    0.63              10/Jun/2016           Prasanth Kumar G       Introduced function GetMemberByMemberUserName for validating UserName
    0.64              10/Jun/2016            Prasanth Kumar G       Introduced function GetMemberADUserNameById for validating UserNameGetMemberADUserNameById
    0.65               24/June/2016          pravin khot          added- RejectMemberJobCartById
 *  *0.66                8/Aug/2016           pravin khot          modify-GetAllCandidateOnSearchForPrecises
 *   0.67                1/Mar/2017           Sumit Sonawane       modify - restrict to rescheduled/cancel Interview feedback.
     0.68                21/Mar/2017          Sumit Sonawane       modify - Graphs
 *   0.69                23/March/2017        pravin khot          added- GetCompanyContactByJobPostingHiringTeam
 *   0.70              24Apr2017           Prasanth Kumar G        Introduced Errorlogdb issue id 1044
 * * * * ---------------------------------------------------------------------------------------------------------------------------------------------------      
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Security;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.DataAccess;
using System.Data;
using TPS360.Common.BusinessEntity;

namespace TPS360.BusinessFacade
{
    public sealed class Facade : IFacade
    {
        #region Instance Variables

        private bool _isDisposed;
        private Context _currentContext;

        private BaseDataAccessFactory _dataAccessFactory;

        #endregion

        #region Private Properties

        private Context CurrentContext
        {
            [DebuggerStepThrough()]
            get
            {
                if (_currentContext == null)
                {
                    _currentContext = new Context();
                }

                return _currentContext;
            }
        }

        private BaseDataAccessFactory DataAccessFactory
        {
            [DebuggerStepThrough()]
            get
            {
                if (_dataAccessFactory == null)
                {
                    _dataAccessFactory = BaseDataAccessFactory.Create(CurrentContext);
                }

                return _dataAccessFactory;
            }
        }

        #endregion

        #region Constructer & Destructor

        public Facade()
            : base()
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (_currentContext != null)
                    {
                        _currentContext.Dispose();
                    }
                }
            }

            _isDisposed = true;
        }

        ~Facade()
        {
            Dispose(false);
        }

        #endregion

        #region Implemented Methods

        #region Automation

        bool IFacade.CreateEmailAccount(string emailId, string smtp, string connectionString)
        {
            return TPS360.Automation.Facade.CreateEmailAccount(emailId, smtp, connectionString);
        }

        bool IFacade.DeleteEmailAccount(int Id, string connectionString)
        {
            return TPS360.Automation.Facade.DeleteEmailAccount(Id, connectionString);
        }

        DataTable IFacade.GetAllEmailAccount(string connectionString)
        {
            return TPS360.Automation.Facade.GetAllEmailAccount(connectionString);

        }

        bool IFacade.SetDefaultEmailAccount(int Id, string connectionString)
        {
            return TPS360.Automation.Facade.SetDefaultEmailAccount(Id, connectionString);
        }

        int IFacade.GetDefaultEmailAccountId(string connectionString)
        {
            return TPS360.Automation.Facade.GetDefaultEmailAccountId(connectionString);
        }


        #endregion

        #region ActionLog

        ActionLog IFacade.AddActionLog(ActionLog actionLog)
        {
            return DataAccessFactory.CreateActionLogDataAccess().Add(actionLog);
        }

        ActionLog IFacade.UpdateActionLog(ActionLog actionLog)
        {
            return DataAccessFactory.CreateActionLogDataAccess().Update(actionLog);
        }

        ActionLog IFacade.GetActionLogById(int id)
        {
            return DataAccessFactory.CreateActionLogDataAccess().GetById(id);
        }

        IList<ActionLog> IFacade.GetAllActionLog()
        {
            return DataAccessFactory.CreateActionLogDataAccess().GetAll();
        }

        bool IFacade.DeleteActionLogById(int id)
        {
            return DataAccessFactory.CreateActionLogDataAccess().DeleteById(id);
        }

        #endregion

        #region Activity

        Activity IFacade.AddActivity(Activity activity)
        {
            return DataAccessFactory.CreateActivityDataAccess().Add(activity);
        }

        Activity IFacade.UpdateActivity(Activity activity)
        {
            return DataAccessFactory.CreateActivityDataAccess().Update(activity);
        }

        Activity IFacade.AddActivityForInterview(Activity activity)
        {
            return DataAccessFactory.CreateActivityDataAccess().AddForInterview(activity);
        }

        Activity IFacade.UpdateActivityForInterview(Activity activity)
        {
            return DataAccessFactory.CreateActivityDataAccess().UpdateForInterview(activity);
        }

        Activity IFacade.GetActivityById(int id)
        {
            return DataAccessFactory.CreateActivityDataAccess().GetById(id);
        }

        //IList<Activity> IFacade.GetAllActivity()
        //{
        //    return DataAccessFactory.CreateActivityDataAccess().GetAll();
        //}Activity IActivityDataAccess.GetAllActivityByResourceId(int resourceID)

        bool IFacade.DeleteActivityById(int id)
        {
            return DataAccessFactory.CreateActivityDataAccess().DeleteById(id);
        }

        IList<Activity> IFacade.GetAllActivityByResourceId(int resourceID)
        {
            return DataAccessFactory.CreateActivityDataAccess().GetAllActivityByResourceId(resourceID);
        }
		
		IList<Activity> IFacade.GetAllActivityByResourceIdAndDate(int resourceID, string Date)
        {
            return DataAccessFactory.CreateActivityDataAccess().GetAllActivityByResourceIdAndDate(resourceID, Date);
        }

        #endregion

        #region ActivityResource

        ActivityResource IFacade.AddActivityResource(ActivityResource activityResource)
        {
            return DataAccessFactory.CreateActivityResourceDataAccess().Add(activityResource);
        }

        //ActivityResource IFacade.UpdateActivityResource(ActivityResource activityResource)
        //{
        //    return DataAccessFactory.CreateActivityResourceDataAccess().Update(activityResource);
        //}

        ActivityResource IFacade.GetActivityResourceByActivityID(int activityID)
        {
            return DataAccessFactory.CreateActivityResourceDataAccess().GetByActivityID(activityID);
        }

        IList<ActivityResource> IFacade.GetAllActivityResource()
        {
            return DataAccessFactory.CreateActivityResourceDataAccess().GetAll();
        }

        bool IFacade.DeleteActivityResourceByActivityID(int activityID)
        {
            return DataAccessFactory.CreateActivityResourceDataAccess().DeleteByActivityID(activityID);
        }

        #endregion

        #region Alert

        Alert IFacade.AddAlert(Alert alert)
        {
            return DataAccessFactory.CreateAlertDataAccess().Add(alert);
        }

        Alert IFacade.UpdateAlert(Alert alert)
        {
            return DataAccessFactory.CreateAlertDataAccess().Update(alert);
        }

        Alert IFacade.GetAlertById(int id)
        {
            return DataAccessFactory.CreateAlertDataAccess().GetById(id);
        }

        IList<Alert> IFacade.GetAllAlert()
        {
            return DataAccessFactory.CreateAlertDataAccess().GetAll();
        }

        PagedResponse<Alert> IFacade.GetPagedAlert(PagedRequest request)
        {
            return DataAccessFactory.CreateAlertDataAccess().GetPaged(request);
        }

        bool IFacade.DeleteAlertById(int id)
        {
            return DataAccessFactory.CreateAlertDataAccess().DeleteById(id);
        }

        Alert IFacade.GetAlertByWorkflowTitle(WorkFlowTitle workFlowTitle)
        {
            return DataAccessFactory.CreateAlertDataAccess().GetByWorkflowTitle(workFlowTitle);
        }

        #endregion

        #region ApplicationWorkflow

        ApplicationWorkflow IFacade.AddApplicationWorkflow(ApplicationWorkflow applicationWorkflow)
        {
            return DataAccessFactory.CreateApplicationWorkflowDataAccess().Add(applicationWorkflow);
        }

        ApplicationWorkflow IFacade.UpdateApplicationWorkflow(ApplicationWorkflow applicationWorkflow)
        {
            return DataAccessFactory.CreateApplicationWorkflowDataAccess().Update(applicationWorkflow);
        }

        ApplicationWorkflow IFacade.GetApplicationWorkflowById(int id)
        {
            return DataAccessFactory.CreateApplicationWorkflowDataAccess().GetById(id);
        }

        ApplicationWorkflow IFacade.GetApplicationWorkflowByTitle(string title)
        {
            return DataAccessFactory.CreateApplicationWorkflowDataAccess().GetByTitle(title);
        }

        //ApplicationWorkflow IFacade.GetApplicationWorkflowCurrentDescrition()
        //{
        //    return DataAccessFactory.CreateApplicationWorkflowDataAccess().GetCurrentDescription();
        //}

        ApplicationWorkflow IFacade.GetApplicationWorkflowByMemberId(int memberId)
        {
            return DataAccessFactory.CreateApplicationWorkflowDataAccess().GetByMemberId(memberId);
        }

        IList<ApplicationWorkflow> IFacade.GetAllApplicationWorkflow()
        {
            return DataAccessFactory.CreateApplicationWorkflowDataAccess().GetAll();
        }

        bool IFacade.DeleteApplicationWorkflowById(int id)
        {
            return DataAccessFactory.CreateApplicationWorkflowDataAccess().DeleteById(id);
        }

        #endregion

        #region ApplicationWorkflowMap

        ApplicationWorkflowMap IFacade.AddApplicationWorkflowMap(ApplicationWorkflowMap applicationWorkflowMap)
        {
            return DataAccessFactory.CreateApplicationWorkflowMapDataAccess().Add(applicationWorkflowMap);
        }

        ApplicationWorkflowMap IFacade.UpdateApplicationWorkflowMap(ApplicationWorkflowMap applicationWorkflowMap)
        {
            return DataAccessFactory.CreateApplicationWorkflowMapDataAccess().Update(applicationWorkflowMap);
        }

        ApplicationWorkflowMap IFacade.GetApplicationWorkflowMapById(int id)
        {
            return DataAccessFactory.CreateApplicationWorkflowMapDataAccess().GetById(id);
        }

        ApplicationWorkflowMap IFacade.GetApplicationWorkflowMapByApplicationWorkflowIdAndMemberId(int applicationWorkflowId, int memberId)
        {
            return DataAccessFactory.CreateApplicationWorkflowMapDataAccess().GetByApplicationWorkflowIdAndMemberId(applicationWorkflowId, memberId);
        }

        IList<ApplicationWorkflowMap> IFacade.GetAllApplicationWorkflowMapByApplicationWorkflowId(int applicationWorkflowId)
        {
            return DataAccessFactory.CreateApplicationWorkflowMapDataAccess().GetAllByApplicationWorkflowId(applicationWorkflowId);
        }

        IList<ApplicationWorkflowMap> IFacade.GetAllApplicationWorkflowMap()
        {
            return DataAccessFactory.CreateApplicationWorkflowMapDataAccess().GetAll();
        }

        bool IFacade.DeleteApplicationWorkflowMapById(int id)
        {
            return DataAccessFactory.CreateApplicationWorkflowMapDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteApplicationWorkflowMapByApplicationWorkflowId(int applicationWorkFlowId)
        {
            return DataAccessFactory.CreateApplicationWorkflowMapDataAccess().DeleteByApplicationWorkflowId(applicationWorkFlowId);
        }

        #endregion

        #region ASPNetMembership

        bool IFacade.LockUser(Guid userId)
        {
            return DataAccessFactory.CreateASPNetMembershipDataAccess().LockUser(userId);
        }

        #endregion

        #region AutoMailSending
        IList<AutoMailSending> IFacade.GetSearchDetails()
        {
            return DataAccessFactory.CreateAutoMailSendingDataAccess().GetSearchDetails();
        }
        #endregion

        #region CustomRole

        CustomRole IFacade.AddCustomRole(CustomRole customRole)
        {
            return DataAccessFactory.CreateCustomRoleDataAccess().Add(customRole);
        }

        CustomRole IFacade.UpdateCustomRole(CustomRole customRole)
        {
            return DataAccessFactory.CreateCustomRoleDataAccess().Update(customRole);
        }

        CustomRole IFacade.GetCustomRoleById(int id)
        {
            return DataAccessFactory.CreateCustomRoleDataAccess().GetById(id);
        }

        CustomRole IFacade.GetCustomRoleByName(string Name)
        {
            return DataAccessFactory.CreateCustomRoleDataAccess().GetByName(Name);
        }

        IList<CustomRole> IFacade.GetAllCustomRole()
        {
            return DataAccessFactory.CreateCustomRoleDataAccess().GetAll();
        }
        //************Added by pravin khot on 3/March/2016***********
        IList<CustomRole> IFacade.GetAllCustomUser()
        {
            return DataAccessFactory.CreateCustomRoleDataAccess().GetAllUser();
        }
        //******************************END**********************************
        string[] IFacade.GetAllCustomRoleNames()
        {
            IList<CustomRole> customRoleList = DataAccessFactory.CreateCustomRoleDataAccess().GetAll();

            if (customRoleList != null && customRoleList.Count > 0)
            {
                string[] customRoleNames = new string[customRoleList.Count];

                for (int i = 0; i <= customRoleList.Count - 1; i++)
                {
                    customRoleNames[i] = customRoleList[i].Name;
                }

                return customRoleNames;
            }

            return null;
        }

        PagedResponse<CustomRole> IFacade.GetPagedCustomRole(PagedRequest request)
        {
            return DataAccessFactory.CreateCustomRoleDataAccess().GetPaged(request);
        }

        bool IFacade.DeleteCustomRoleById(int id)
        {
            return DataAccessFactory.CreateCustomRoleDataAccess().DeleteById(id);
        }
        string  IFacade.GetCustomRoleNameByMemberId (int Memberid)
        {
            return DataAccessFactory.CreateCustomRoleDataAccess().GetCustomRoleNameByMemberId(Memberid);
        }
        #endregion

        #region CustomRolePrivilege

        CustomRolePrivilege IFacade.AddCustomRolePrivilege(CustomRolePrivilege customRole)
        {
            return DataAccessFactory.CreateCustomRolePrivilegeDataAccess().Add(customRole);
        }

        CustomRolePrivilege IFacade.UpdateCustomRolePrivilege(CustomRolePrivilege customRole)
        {
            return DataAccessFactory.CreateCustomRolePrivilegeDataAccess().Update(customRole);
        }

        CustomRolePrivilege IFacade.GetCustomRolePrivilegeById(int id)
        {
            return DataAccessFactory.CreateCustomRolePrivilegeDataAccess().GetById(id);
        }

        ArrayList IFacade.GetAllCustomRolePrivilegeIdsByRoleId(int roleId)
        {
            ArrayList idList = new ArrayList();
            IList<CustomRolePrivilege> customRolePrivilegeList = DataAccessFactory.CreateCustomRolePrivilegeDataAccess().GetAllByRoleId(roleId);

            if (customRolePrivilegeList != null && customRolePrivilegeList.Count > 0)
            {
                foreach (CustomRolePrivilege customRolePrivilege in customRolePrivilegeList)
                {
                    idList.Add(customRolePrivilege.CustomSiteMapId);
                }
            }

            return idList;
        }

        IList<CustomRolePrivilege> IFacade.GetAllCustomRolePrivilegeByRoleId(int roleId)
        {
            return DataAccessFactory.CreateCustomRolePrivilegeDataAccess().GetAllByRoleId(roleId);
        }

        IList<CustomRolePrivilege> IFacade.GetAllCustomRolePrivilege()
        {
            return DataAccessFactory.CreateCustomRolePrivilegeDataAccess().GetAll();
        }

        bool IFacade.DeleteCustomRolePrivilegeById(int id)
        {
            return DataAccessFactory.CreateCustomRolePrivilegeDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteCustomRolePrivilegeByRoleId(int roleId)
        {
            return DataAccessFactory.CreateCustomRolePrivilegeDataAccess().DeleteByRoleId(roleId);
        }

        void IFacade.DeleteAllCustomRolePrivilege()
        {
            DataAccessFactory.CreateCustomRolePrivilegeDataAccess().DeleteAll_CustomRolePrivilege();
        }

        #endregion

        #region Candidate

        Candidate IFacade.GetCandidateById(int id)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetById(id);
        }

        IList<Candidate> IFacade.GetAllCandidateByMemberId(int memberId)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetAllCandidateByMemberId(memberId);
        }

        Candidate IFacade.GetCandidateByIdForHR(int id)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetByIdForHR(id);
        }

        CandidateOverviewDetails IFacade.GetCandidateOverviewDetails(int CandidateId)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetCandidateOverviewDetails(CandidateId);
        }


        PagedResponse<Candidate> IFacade.GetPagedCandidate(PagedRequest request)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetPaged(request);
        }
        PagedResponse<Candidate> IFacade.GetPagedVendorCandidatePerformance(PagedRequest request)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetPagedVendorCandidatePerformance(request);
        }

        PagedResponse<Candidate> IFacade.GetPagedCandidateForSubmittedCandidate(PagedRequest request)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetPagedForSubmittedCandidate(request);
        }
        //***********************************Code added by pravin khot on 12/Jan/2016************
        PagedResponse<Candidate> IFacade.GetPagedCareerPortalSubmissionsList(PagedRequest request)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetPagedCarrerPortalList(request);
        }
        //******************************************End*****************************************

        PagedResponse<Candidate> IFacade.GetPagedCandidate(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit,int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetPagedForReport(request, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
            country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit,WorkSchedule ,
           gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager);
        }

       

        PagedResponse<DynamicDictionary> IFacade.GetPagedCandidateForSelectedColumn(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
      int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
      int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, IList<string> checkedList)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetPagedForReportBySelectedColumn(request, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
            country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
           gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, checkedList);
        }


        int IFacade.GetCandidateCountByMemberId(int MemberID)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetCandidateCountByMemberID(MemberID);
        }

        int IFacade.getCandidateIdbyPrimaryEmail(string PrimaryEmail)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetCandidateIdbyPrimaryEmail(PrimaryEmail);
        }

        void  IFacade.getMemberUpdateCreatorId(int Id,int creatorid)
        {
            DataAccessFactory.CreateCandidateDataAccess().GetMemberUpdateCreatorId(Id, creatorid);
        }
        bool IFacade.CheckCandidateDuplication(string firstName,string lastName,string mobileNo,DateTime dob,string email,int IdCardLookUpId,string IdCardDetail)
        {
            return DataAccessFactory.CreateCandidateDataAccess().CheckCandidateDuplication(firstName, lastName, mobileNo, dob, email, IdCardLookUpId, IdCardDetail);
        }
        string IFacade.GetCandidateEmailById(int Id) 
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetCandidateEmailById(Id);
        }

        string IFacade.GetCandidateNameByEmailId(string EmailId) 
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetCandidateNameByEmailId(EmailId); 
        }
        //****************Code added by pravin khot on 4/March/2016*******************
        PagedResponse<Candidate> IFacade.GetPagedCandidateCDMDetailsReport(PagedRequest request)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetPagedCandidateCDMDetailsInfoReport(request);
        }
       // ************************************END**************************************

        //****************Code added by Sumit Sonawane on 23/May/2016*******************


        PagedResponse<Candidate> IFacade.GetPagedCandidate(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, bool IsTeamReport, string TeamMemberIds, int TeamId)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetPagedForReport(request, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
            country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
           gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, IsTeamReport, TeamMemberIds, TeamId);
        }

        PagedResponse<DynamicDictionary> IFacade.GetPagedCandidateForSelectedColumn(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
      int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
      int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, IList<string> checkedList, bool IsTeamReport, string TeamMembersId, int TeamId)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetPagedForReportBySelectedColumn(request, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
            country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
           gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, checkedList, IsTeamReport, TeamMembersId, TeamId);
        }

        //PagedResponse<Candidate> IFacade.GetPagedCandidate(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
        // int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
        // int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager)
        //{
        //    return DataAccessFactory.CreateCandidateDataAccess().GetPagedForReport(request, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
        //    country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
        //   gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager);
        //}


     //   PagedResponse<DynamicDictionary> IFacade.GetPagedCandidateForSelectedColumn(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
     //int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
     //int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, IList<string> checkedList)
     //   {
     //       return DataAccessFactory.CreateCandidateDataAccess().GetPagedForReportBySelectedColumn(request, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
     //       country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
     //      gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, checkedList);
     //   }


        // ************************************END**************************************
        // ********Code added by Sumit Sonawane on 20/Jan/2017********
        //int IFacade.GetCandidateAgebodByMemberId(int MemberID)
        //{
        //    return DataAccessFactory.CreateCandidateDataAccess().GetCandidateAgeByMemberID(MemberID);
        //}
        string IFacade.GetCandidateAgebodByMemberId(int MemberID)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetCandidateAgeByMemberID(MemberID);
        }
        /////////////////////////////Sumit Sonawane 24/01/2017///////////////////////////////////////////////////////////////////////
        int IFacade.getAllCandiCountFronView(string jobCrtor, int jobid, int Step)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetAllCandidateCountFromView(jobCrtor, jobid, Step);
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #endregion


        #region CandidateSourcingReport

        PagedResponse<CandidateSourcingInfo> IFacade.GetPagedForCandidateSourcingReport(PagedRequest request)
        {
            return DataAccessFactory.CreateCandidateSourcingInfoDataAccess().GetPagedForSourcingInfoReport(request);
        }
        
        #endregion


        #region CandidateDashboard

        CandidateDashboard IFacade.GetCandidateDashboardByMemberId(int memberId)
        {
            return DataAccessFactory.CreateCandidateDashboardDataAccess().GetByMemberId(memberId);
        }

        #endregion

        #region CandidateReportDashboard

        CandidateReportDashboard IFacade.GetCandidateReportDashboardByParameter(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager)
        {
            return DataAccessFactory.CreateCandidateReportDashboardDataAccess().GetByParameter(addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
            country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit,
           gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager);
        }

        #endregion

        #region Candidate Search
        //custom
        PagedResponse<Candidate> IFacade.GetAllCandidateOnSearch(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType) //0.3  // 9372
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetAllOnSearch(request, allKeyWords, anyKeyWord, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, functionalCategory, hiringMatrixId, hotListId, memberId, rowPerPage, memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, strState, maxResults, applicantType); // 0.3  // 9372
        }

        PagedResponse<Candidate> IFacade.GetAllCandidateOnSearchForPrecise(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddWhereClause, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string CandidatID) //0.3  // 9372
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetAllOnSearchForPrecise (request, allKeyWords, anyKeyWord, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo,currencyType , functionalCategory, hiringMatrixId, hotListId, memberId, rowPerPage, memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, strState, maxResults, applicantType,Education ,WorkSchedule,Gender,Nationality,Industry,JobFunction,HiringStatus,CurrentCompany ,CompanyStatus ,CandidateEmail ,MobilePhone,ZipCode ,Radius ,AddWhereClause ,IsMile,AssignedManagerId ,SalarySelector ,internalRating, CandidatID  ); // 0.3  // 9372
        }
        //*************Added parameter bool ShowPreciseSearch by pravin khot on 8/Aug/2016
        PagedResponse<Candidate> IFacade.GetAllCandidateOnSearchForPrecises(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddWhereClause, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string SourceId, string CandidateID, string NoticePeriod, bool ShowPreciseSearch) //0.3  // 9372
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetAllOnSearchForPrecises(request, allKeyWords, anyKeyWord, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, rowPerPage, memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, strState, maxResults, applicantType, Education, WorkSchedule, Gender, Nationality, Industry, JobFunction, HiringStatus, CurrentCompany, CompanyStatus, CandidateEmail, MobilePhone, ZipCode, Radius, AddWhereClause, IsMile, AssignedManagerId, SalarySelector, internalRating, SourceId, CandidateID, NoticePeriod,ShowPreciseSearch); // 0.3  // 9372
        }
        //*****************************END***********************************       

        Candidate IFacade.GetOnSearchForPrecise_Sub(int CandidateId)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetOnSearchForPrecise_Sub(CandidateId);
        }

        IList<Candidate> IFacade.GetAllCandidateOnSearchForSearchAgent(string allKeyWords, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string hotListId, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType, string Education, string WorkSchedule,string HiringStatus, int JobPostingId) 
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetAllOnSearchForSearchAgent (allKeyWords,currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, hotListId,minExpYr, maxExpYr, strState, maxResults, applicantType, Education, WorkSchedule,HiringStatus,JobPostingId ); 
        }

        PagedResponse<Candidate> IFacade.GetAllCandidateOnSearchForSavedQuery(PagedRequest request, string whereClause, string memberId, string rowPerPage)
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetAllOnSearchForSavedQuery(request, whereClause, memberId, rowPerPage);
        }
//custom
        string IFacade.GetCandidateSearchQuery(string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string memberPositionId, string isMyList, string minExpYr, string maxExpYr, string strState, string applicantType, string internalRating)  // 0.3
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetSearchQuery(allKeyWords, anyKeyWord, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, functionalCategory, hiringMatrixId, hotListId, memberId, memberPositionId, isMyList, minExpYr, maxExpYr,strState, applicantType,internalRating );  //0.3
        }
        string IFacade.GetCandidateSearchQueryForPreciseSearch(string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string memberPositionId, string isMyList, string minExpYr, string maxExpYr, string strState, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddWhereClause, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string CandidateID)  // 0.3
        {
            return DataAccessFactory.CreateCandidateDataAccess().GetSearchQueryForPrecise(allKeyWords, anyKeyWord, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo,currencyType , functionalCategory, hiringMatrixId, hotListId, memberId, memberPositionId, isMyList, minExpYr, maxExpYr, strState, applicantType, Education,WorkSchedule ,Gender, Nationality, Industry, JobFunction,HiringStatus,CurrentCompany ,CompanyStatus ,CandidateEmail ,MobilePhone,ZipCode ,Radius ,AddWhereClause ,IsMile ,AssignedManagerId ,SalarySelector ,internalRating ,"0",CandidateID,string.Empty );  //0.3
        }
        #endregion

        #region Candidate Requisition Status

        void IFacade.AddCandidateRequisitionStatus(int JobPostingId, string MemberId, int CreatorId)
        {
            DataAccessFactory.CreateCandidateRequisitionStatusDataAccess().Add(JobPostingId ,MemberId ,CreatorId );
        }

        void IFacade.UpdateCandidateRequisitionStatus(int JobPostingId, string MemberId, int CreatorId,int StatusId)
        {
            DataAccessFactory.CreateCandidateRequisitionStatusDataAccess().Update(JobPostingId, MemberId, CreatorId,StatusId );
        }

        CandidateRequisitionStatus IFacade.GetByRequisitionIdandMemberId(int RequisitionId, int MemberId)
        {
            return DataAccessFactory.CreateCandidateRequisitionStatusDataAccess().GetByRequisitionIdandMemberId(RequisitionId, MemberId);
        }

        PagedResponse<CandidateRequisitionStatus> IFacade.GetPagedCandidateREquisitionStatus(PagedRequest request, int ManagerId)
        {
            return DataAccessFactory.CreateCandidateRequisitionStatusDataAccess().GetPaged(request, ManagerId);
        }
        #endregion

        #region Category

        Category IFacade.AddCategory(Category Category)
        {
            return DataAccessFactory.CreateCategoryDataAccess().Add(Category);
        }

        Category IFacade.UpdateCategory(Category Category)
        {
            return DataAccessFactory.CreateCategoryDataAccess().Update(Category);
        }

        Category IFacade.GetCategoryById(int id)
        {
            return DataAccessFactory.CreateCategoryDataAccess().GetById(id);
        }

        IList<Category> IFacade.GetAllCategory()
        {
            return DataAccessFactory.CreateCategoryDataAccess().GetAll();
        }

        bool IFacade.DeleteCategoryById(int id)
        {
            return DataAccessFactory.CreateCategoryDataAccess().DeleteById(id);
        }

        Int32 IFacade.GetCategoryIdByCategoryName(string CategoryName)
        {
            return DataAccessFactory.CreateCategoryDataAccess().GetCategoryIdByCategoryName(CategoryName);
        }



        #endregion

        #region CommonNote

        CommonNote IFacade.AddCommonNote(CommonNote commonNote)
        {
            return DataAccessFactory.CreateCommonNoteDataAccess().Add(commonNote);
        }

        CommonNote IFacade.UpdateCommonNote(CommonNote commonNote)
        {
            return DataAccessFactory.CreateCommonNoteDataAccess().Update(commonNote);
        }

        CommonNote IFacade.GetCommonNoteById(int id)
        {
            return DataAccessFactory.CreateCommonNoteDataAccess().GetById(id);
        }

        IList<CommonNote> IFacade.GetAllCommonNoteByMemberId(int memberId)
        {
            return DataAccessFactory.CreateCommonNoteDataAccess().GetAllByMemberId(memberId);
        }

        // 0.5 starts here
        IList<CommonNote> IFacade.GetAllCommonNoteByMemberId(int memberId, string SortExpression)
        {
            return DataAccessFactory.CreateCommonNoteDataAccess().GetAllByMemberId(memberId, SortExpression);
        }
        // 0.5 ends here


        IList<CommonNote> IFacade.GetAllCommonNoteByCompanyId(int companyId, string SortExpression)
        {
            return DataAccessFactory.CreateCommonNoteDataAccess().GetAllByCompanyId(companyId, SortExpression);
        }

        IList<CommonNote> IFacade.GetAllCommonNoteByCampaignId(int campaignId)
        {
            return DataAccessFactory.CreateCommonNoteDataAccess().GetAllByCampaignId(campaignId);
        }

        IList<CommonNote> IFacade.GetAllCommonNoteByProjectId(int projectId, string sortExpression)//0.7
        {
            return DataAccessFactory.CreateCommonNoteDataAccess().GetAllByProjectId(projectId, sortExpression);
            //0.7
        }

        IList<CommonNote> IFacade.GetAllCommonNote()
        {
            return DataAccessFactory.CreateCommonNoteDataAccess().GetAll();
        }

        bool IFacade.DeleteCommonNoteById(int id)
        {
            return DataAccessFactory.CreateCommonNoteDataAccess().DeleteById(id);
        }

        CommonNote IFacade.GetCommonNoteByCompanyIdAndNoteCategoryLookUpId(int companyId, int noteCategoryLookUpId)
        {
            return DataAccessFactory.CreateCommonNoteDataAccess().GetByCompanyIdAndNoteCategoryLookUpId(companyId, noteCategoryLookUpId);
        }

        #endregion

        #region Company

        Company IFacade.AddCompany(Company company, CompanyStatus requestedStatus, bool isSelfRegistration)
        {
            company = DataAccessFactory.CreateCompanyDataAccess().Add(company);

            if (company != null && isSelfRegistration)
            {
                ApplicationWorkflow workflow = null;

                if (requestedStatus == CompanyStatus.Vendor)
                {
                    workflow = DataAccessFactory.CreateApplicationWorkflowDataAccess().GetByTitle(Enum.GetName(typeof(WorkFlowTitle), WorkFlowTitle.VMSShouldSelectedEmployeesAlertedWhenVendorSelfRegistersExternalWebSite));
                }
                else if (requestedStatus == CompanyStatus.Partner)
                {
                    workflow = DataAccessFactory.CreateApplicationWorkflowDataAccess().GetByTitle(Enum.GetName(typeof(WorkFlowTitle), WorkFlowTitle.PRMShouldSelectedEmployeesAlertedWhenPartnerSelfRegistersExternalWebsite));
                }

                //if (workflow != null && workflow.RequireApproval)
                //{
                //    IList<CompanyTeam> teamList = DataAccessFactory.CreateCompanyTeamDataAccess().GetAllByCompanyId(company.Id);

                //    if (teamList != null && teamList.Count > 0)
                //    {
                //        if (workflow.IsDashboardAlert)
                //        {
                //            Alert alert = null;

                //            if (requestedStatus == CompanyStatus.Vendor)
                //            {
                //                alert = DataAccessFactory.CreateAlertDataAccess().GetByWorkflowTitle(WorkFlowTitle.VMSShouldSelectedEmployeesAlertedWhenVendorSelfRegistersExternalWebSite);
                //            }
                //            else if (requestedStatus == CompanyStatus.Partner)
                //            {
                //                alert = DataAccessFactory.CreateAlertDataAccess().GetByWorkflowTitle(WorkFlowTitle.PRMShouldSelectedEmployeesAlertedWhenPartnerSelfRegistersExternalWebsite);
                //            }

                //            if (alert != null)
                //            {
                //                foreach (CompanyTeam team in teamList)
                //                {
                //                    MemberAlert memberAlert = new MemberAlert(alert.Id, team.MemberId);
                //                    DataAccessFactory.CreateMemberAlertDataAccess().Add(memberAlert);
                //                }
                //            }
                //        }

                //        if (workflow.IsEmailAlert)
                //        {
                //            List<string> emailList = new List<string>();

                //            foreach (CompanyTeam team in teamList)
                //            {
                //                Member memberMapData = DataAccessFactory.CreateMemberDataAccess().GetById(team.MemberId);

                //                if (!string.IsNullOrEmpty(memberMapData.PrimaryEmail) && emailList.LastIndexOf(memberMapData.PrimaryEmail) == -1)
                //                {
                //                    emailList.Add(memberMapData.PrimaryEmail);
                //                }
                //            }

                //            Mailer.SendCompanyRegistrationMassage(company, requestedStatus, isSelfRegistration, emailList);
                //        }

                //        if (workflow.IsSmsAlert)
                //        {

                //        }
                //    }

                //}
            }

            return company;
        }

        Company IFacade.UpdateCompany(Company company)
        {
            return DataAccessFactory.CreateCompanyDataAccess().Update(company);
        }

        bool IFacade.UpdateCompanyStatus(int companyId, CompanyStatus newStatus)
        {
            return DataAccessFactory.CreateCompanyDataAccess().UpdateStatus(companyId, newStatus);
        }

        bool IFacade.ChangePendingStatus(int companyId, bool IsPending)
        {
            return DataAccessFactory.CreateCompanyDataAccess().ChangePendingStatus(companyId, IsPending);
        }

        Company IFacade.GetCompanyById(int id)
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetById(id);
        }

        CompanyOverviewDetails IFacade.GetCompanyOverviewDetails(int CompanyId)
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetCompanyOverviewDetails(CompanyId);
        }


        IList<Company> IFacade.GetAllCompany()
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetAll();
        }

        IList<Company> IFacade.GetAllPendingClient()
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetAllPendingClient();
        }

        PagedResponse<Company> IFacade.GetPagedCompany(PagedRequest request)
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetPaged(request);
        }

        bool IFacade.DeleteCompanyById(int id)
        {
            return DataAccessFactory.CreateCompanyDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteCompanyFromMemberById(int id, int memberId)
        {
            return DataAccessFactory.CreateCompanyDataAccess().DeleteFromMemberById(id, memberId);
        }

        Hashtable IFacade.GetAllCompanyByEndClientType(int endClientType)
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetAllByEndClientType(endClientType);
        }

        ArrayList IFacade.GetAllClients()
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetAllClients();
        }

        ArrayList IFacade.GetAllClientsByStatus(int status) 
        {

            return DataAccessFactory.CreateCompanyDataAccess().GetAllClientsByStatus(status);
        }
        ArrayList IFacade.GetAllClientsByStatusByCandidateId(int status,int candidateid)
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetAllClientsByStatusByCandidateId(status, candidateid);
        }
      
        //12129
        ArrayList IFacade.GetPrimaryContact()
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetPrimaryContact();
        }
        ArrayList IFacade.GetAllCompanyNameByCompanyStatus(CompanyStatus companyStatus)
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetAllNameByCompanyStatus(companyStatus);
        }

        Int32 IFacade.GetCompanyByJobPostingId(int JobpostingId)
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetCompayByJobPostingId(JobpostingId);
        }

        ArrayList IFacade.GetAllVendorPartnerNames()
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetAllVendorPartnerNames();
        }

        Int32 IFacade.GetCompanyIdByName(string companyName)
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetCompanyIdByName(companyName);
        }

        IList<Company> IFacade.Company_GetPaged(string preText, int count)
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetPaged(preText, count);
        }
        int IFacade.Company_GetCompanyCount()
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetCompanyCount();
        }
        string  IFacade.GetCompanyNameById (int CompanyId)
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetCompanyNameById(CompanyId);
        }

        string IFacade.GetCompanyNameByVendorMemberId(int MemberId)
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetCompanyNameByVendorMemberID(MemberId);
        }

        ArrayList IFacade.GetAllCompanyList()
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetAllCompanyList();
        }

        int IFacade.GetCompanyIdByNameForDataImport(string CompanyName) 
        {
            return DataAccessFactory.CreateCompanyDataAccess().GetCompanyIdByNameForDataImport(CompanyName);
        }
        #endregion

        #region CompanyAssignedManager

        CompanyAssignedManager IFacade.AddCompanyAssignedManager(CompanyAssignedManager companyAssignedManager)
        {
            return DataAccessFactory.CreateCompanyAssignedManagerDataAccess().Add(companyAssignedManager);
        }

        CompanyAssignedManager IFacade.UpdateCompanyAssignedManager(CompanyAssignedManager companyAssignedManager)
        {
            return DataAccessFactory.CreateCompanyAssignedManagerDataAccess().Update(companyAssignedManager);
        }

        CompanyAssignedManager IFacade.GetCompanyAssignedManagerById(int id)
        {
            return DataAccessFactory.CreateCompanyAssignedManagerDataAccess().GetById(id);
        }

        IList<CompanyAssignedManager> IFacade.GetAllCompanyAssignedManager()
        {
            return DataAccessFactory.CreateCompanyAssignedManagerDataAccess().GetAll();
        }

        IList<CompanyAssignedManager> IFacade.GetAllCompanyAssignedManagerByMemberId(int memberId)
        {
            return DataAccessFactory.CreateCompanyAssignedManagerDataAccess().GetAllByMemberId(memberId);
        }

        IList<CompanyAssignedManager> IFacade.GetAllCompanyAssignedManagerByCompanyId(int companyId)
        {
            return DataAccessFactory.CreateCompanyAssignedManagerDataAccess().GetAllByCompanyId(companyId);
        }
        // 0.6
        IList<CompanyAssignedManager> IFacade.GetAllCompanyAssignedManagerByCompanyId(int companyId, string sortExpression)
        {
            return DataAccessFactory.CreateCompanyAssignedManagerDataAccess().GetAllByCompanyId(companyId, sortExpression);
        }

        bool IFacade.DeleteCompanyAssignedManagerById(int id)
        {
            return DataAccessFactory.CreateCompanyAssignedManagerDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteCompanyAssignedManagerByCompanyIdAndMemberId(int companyId, int memberId)
        {
            return DataAccessFactory.CreateCompanyAssignedManagerDataAccess().DeleteByCompanyIdAndMemberId(companyId, memberId);
        }

        CompanyAssignedManager IFacade.GetCompanyAssignedManagerByCompanyIdAndMemberId (int companyId,int memberId)
        {
            return DataAccessFactory.CreateCompanyAssignedManagerDataAccess().GetCompanyAssignedManagerByCompanyIdAndMemberId(companyId, memberId);
        }

        string IFacade.GetPrimaryManagerNameByCompanyId(int companyId)
        {
            return DataAccessFactory.CreateCompanyAssignedManagerDataAccess().GetPrimaryManagerNameByCompanyId(companyId);
        }
        #endregion

        #region CompanyContact

        CompanyContact IFacade.AddCompanyContact(CompanyContact companyContact)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().Add(companyContact);
        }

        CompanyContact IFacade.UpdateCompanyContact(CompanyContact companyContact)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().Update(companyContact);
        }

        bool IFacade.UpdateCompanyContactMemberId(int contactId, int memberId)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().UpdateMemberId(contactId, memberId);
        }

        bool IFacade.UpdateCompanyContactResumeFile(int contactId, string fileName)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().UpdateResumeFile(contactId, fileName);
        }

        PagedResponse<CompanyContact> IFacade.GetPagedCompanyContact(PagedRequest request)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetPaged(request);
        }

        CompanyContact IFacade.GetCompanyContactById(int id)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetById(id);
        }

        CompanyContact IFacade.GetCompanyContactByEmail(string email)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetByEmail(email);
        }

        CompanyContact IFacade.GetCompanyContactByMemberId(int memberId)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetByMemberId(memberId);
        }

        CompanyContact IFacade.GetCompanyPrimaryContact(int companyId)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetPrimary(companyId);
        }

        IList<CompanyContact> IFacade.GetAllCompanyContact()
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetAll();
        }

        IList<CompanyContact> IFacade.GetAllCompanyContactByComapnyId(int companyId)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetAllByCompanyId(companyId);
        }

        IList<CompanyContact> IFacade.GetAllCompanyContactByCompanyGroupId(int companyGroupId)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetAllByCompanyGroupId(companyGroupId);
        }

        IList<CompanyContact> IFacade.GetAllCompanyContactByComapnyIdAndLogin(int companyId, bool IsLoginCreated)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetAllByComapnyIdAndLogin(companyId, IsLoginCreated);
        }

        bool IFacade.DeleteCompanyContactById(int id)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteCompanyContactByCompanyId(int companyId)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().DeleteByCompanyId(companyId);
        }

        IList<CompanyContact> IFacade.GetAllCompanyContactByVolumeHireGroup()
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetAllByVolumeHireGroup();
        }

        IList<CompanyContact> IFacade.GetAllContactByIdAndDate(int resourceID, string Date)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetAllContactByIdAndDate(resourceID, Date);
        }

        ArrayList  IFacade.GetAllCompanyContactsByCompanyId (int companyId)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetAllCompanyContactsByCompanyId(companyId);
        }

        PagedResponse <ListWithCount> IFacade.GetPagedVendorSubmissionsSummary(PagedRequest request, string type)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetPagedVendorSubmissionSummary(request, type);
        }

        IList<ListWithCount> IFacade.GetAllVendorSubmissionsGroupByDate(int JobPostingId, int VendorId, int ContactID, int StartDate, int EndDate)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetAllVendorSubmissionsGroupByDate(JobPostingId, VendorId, ContactID, StartDate, EndDate);
        }
        //-----------------CODE INTRODUCED BY PRAVIN KHOT 02/DEC/2015
        IList<CompanyContact> IFacade.GetAllCompanyContactByComapnyId_IP(int companyId)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetAllByCompanyId_IP(companyId);
        }
        //------------------------END--------------------------------

        PagedResponse<VendorSubmissions> IFacade.GetPagedVendorSubmissions(PagedRequest request)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetPagedVendorSubmissions(request);
        }
        //-----------------CODE INTRODUCED BY PRAVIN KHOT 26/Feb/2016******************
        CompanyContact IFacade.GetCompanyContactByEmailAndCompanyId(string email, int CompanyId)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetCompanyContactByEmailAndCompanyId(email, CompanyId);
        }
        //------------------------END--------------------------------
        //-----------------CODE INTRODUCED BY PRAVIN KHOT 23/March/2017******************
        string IFacade.GetCompanyContactByJobPostingHiringTeam(int CompanyContactId)
        {
            return DataAccessFactory.CreateCompanyContactDataAccess().GetCompanyContactByJobPostingHiringTeam(CompanyContactId);
        }
        //------------------------END--------------------------------
        #endregion

        #region CompanyDocument

        CompanyDocument IFacade.AddCompanyDocument(CompanyDocument companyDocument)
        {
            return DataAccessFactory.CreateCompanyDocumentDataAccess().Add(companyDocument);
        }

        CompanyDocument IFacade.UpdateCompanyDocument(CompanyDocument companyDocument)
        {
            return DataAccessFactory.CreateCompanyDocumentDataAccess().Update(companyDocument);
        }

        CompanyDocument IFacade.GetCompanyDocumentById(int id)
        {
            return DataAccessFactory.CreateCompanyDocumentDataAccess().GetById(id);
        }

        IList<CompanyDocument> IFacade.GetAllCompanyDocumentByCompanyId(int companyId)
        {
            return DataAccessFactory.CreateCompanyDocumentDataAccess().GetAllByCompanyId(companyId);
        }

        IList<CompanyDocument> IFacade.GetAllCompanyDocument()
        {
            return DataAccessFactory.CreateCompanyDocumentDataAccess().GetAll();
        }

        bool IFacade.DeleteCompanyDocumentById(int id)
        {
            return DataAccessFactory.CreateCompanyDocumentDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteCompanyDocumentByCompanyId(int companyId)
        {
            return DataAccessFactory.CreateCompanyDocumentDataAccess().DeleteByCompanyId(companyId);
        }

        CompanyDocument IFacade.GetCompanyDocumentByCompanyIdTypeAndFileName(int companyId, string Type, string fileName)
        {
            return DataAccessFactory.CreateCompanyDocumentDataAccess().GetAllByCompanyIDTypeAndFileName(companyId , Type, fileName);
        }
        PagedResponse<CompanyDocument> IFacade.CompanyDocumentGetPaged(int companyid, PagedRequest request)
        {
            return DataAccessFactory.CreateCompanyDocumentDataAccess().GetPagedByCompanyId(companyid, request);
        }
        #endregion

        #region CompanyNote

        CompanyNote IFacade.AddCompanyNote(CompanyNote companyNote)
        {
            return DataAccessFactory.CreateCompanyNoteDataAccess().Add(companyNote);
        }

        CompanyNote IFacade.GetCompanyNoteById(int id)
        {
            return DataAccessFactory.CreateCompanyNoteDataAccess().GetById(id);
        }

        IList<CompanyNote> IFacade.GetAllCompanyNote()
        {
            return DataAccessFactory.CreateCompanyNoteDataAccess().GetAll();
        }

        IList<CompanyNote> IFacade.GetAllCompanyNoteByCompanyId(int companyId)
        {
            return DataAccessFactory.CreateCompanyNoteDataAccess().GetAllByCompanyId(companyId);
        }

        bool IFacade.DeleteCompanyNoteById(int id)
        {
            return DataAccessFactory.CreateCompanyNoteDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteCompanyNoteByCompanyId(int companyId)
        {
            return DataAccessFactory.CreateCompanyNoteDataAccess().DeleteByCompanyId(companyId);
        }

        #endregion

        #region CompanySkill

        CompanySkill IFacade.AddCompanySkill(CompanySkill companySkill)
        {
            return DataAccessFactory.CreateCompanySkillDataAccess().Add(companySkill);
        }

        CompanySkill IFacade.GetCompanySkillById(int id)
        {
            return DataAccessFactory.CreateCompanySkillDataAccess().GetById(id);
        }

        IList<CompanySkill> IFacade.GetAllCompanySkill()
        {
            return DataAccessFactory.CreateCompanySkillDataAccess().GetAll();
        }

        IList<CompanySkill> IFacade.GetAllCompanySkillByCompanyId(int companyId)
        {
            return DataAccessFactory.CreateCompanySkillDataAccess().GetAllByCompanyId(companyId);
        }

        bool IFacade.DeleteCompanySkillById(int id)
        {
            return DataAccessFactory.CreateCompanySkillDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteCompanySkillByCompanyId(int companyId)
        {
            return DataAccessFactory.CreateCompanySkillDataAccess().DeleteByCompanyId(companyId);
        }

        #endregion

        #region CompanyStatusChangeRequest

        CompanyStatusChangeRequest IFacade.AddCompanyStatusChangeRequest(CompanyStatusChangeRequest companyStatusChangeRequest)
        {
            ApplicationWorkflow workflow = null;

            if (companyStatusChangeRequest.RequestedStatus == (int)CompanyStatus.Client)
            {
                workflow = DataAccessFactory.CreateApplicationWorkflowDataAccess().GetByTitle(Enum.GetName(typeof(WorkFlowTitle), WorkFlowTitle.SFAManagerAlertRequiredConvertCompanyToClient));
            }
            else if (companyStatusChangeRequest.RequestedStatus == (int)CompanyStatus.Vendor)
            {
                workflow = DataAccessFactory.CreateApplicationWorkflowDataAccess().GetByTitle(Enum.GetName(typeof(WorkFlowTitle), WorkFlowTitle.VMSIsManagerApprovalRequiredForNewVendorsWithCheckList));
            }
            else if (companyStatusChangeRequest.RequestedStatus == (int)CompanyStatus.Partner)
            {
                workflow = DataAccessFactory.CreateApplicationWorkflowDataAccess().GetByTitle(Enum.GetName(typeof(WorkFlowTitle), WorkFlowTitle.PRMIsManagerApprovalRequiredToAddPartnerOrConvertCompanyToPartnerWithCheckList));
            }

            //if (workflow != null && workflow.RequireApproval)
            //{
            //    //Require approval process
            //    companyStatusChangeRequest.ApproveStatus = ApproveStatus.Pending;
            //    companyStatusChangeRequest = DataAccessFactory.CreateCompanyStatusChangeRequestDataAccess().Add(companyStatusChangeRequest);

            //    if (companyStatusChangeRequest != null)
            //    {
            //        DataAccessFactory.CreateCompanyDataAccess().ChangePendingStatus(companyStatusChangeRequest.CompanyId, true);
            //    }

            //    IList<CompanyTeam> teamList = DataAccessFactory.CreateCompanyTeamDataAccess().GetAllByCompanyId(companyStatusChangeRequest.CompanyId);

            //    if (teamList != null && teamList.Count > 0)
            //    {
            //        if (workflow.IsDashboardAlert)
            //        {
            //            Alert alert = null;

            //            if (companyStatusChangeRequest.RequestedStatus == (int)CompanyStatus.Client)
            //            {
            //                alert = DataAccessFactory.CreateAlertDataAccess().GetByWorkflowTitle(WorkFlowTitle.SFAManagerAlertRequiredConvertCompanyToClient);
            //            }
            //            else if (companyStatusChangeRequest.RequestedStatus == (int)CompanyStatus.Vendor)
            //            {
            //                alert = DataAccessFactory.CreateAlertDataAccess().GetByWorkflowTitle(WorkFlowTitle.VMSIsManagerApprovalRequiredForNewVendorsWithCheckList);
            //            }
            //            else if (companyStatusChangeRequest.RequestedStatus == (int)CompanyStatus.Partner)
            //            {
            //                alert = DataAccessFactory.CreateAlertDataAccess().GetByWorkflowTitle(WorkFlowTitle.PRMIsManagerApprovalRequiredToAddPartnerOrConvertCompanyToPartnerWithCheckList);
            //            }

            //            if (alert != null)
            //            {
            //                foreach (CompanyTeam team in teamList)
            //                {
            //                    MemberAlert memberAlert = new MemberAlert(alert.Id, team.MemberId);
            //                    DataAccessFactory.CreateMemberAlertDataAccess().Add(memberAlert);
            //                }
            //            }
            //        }

            //        if (workflow.IsEmailAlert)
            //        {
            //            List<string> emailList = new List<string>();

            //            foreach (CompanyTeam team in teamList)
            //            {
            //                Member memberMapData = DataAccessFactory.CreateMemberDataAccess().GetById(team.MemberId);

            //                if (!string.IsNullOrEmpty(memberMapData.PrimaryEmail) && emailList.LastIndexOf(memberMapData.PrimaryEmail) == -1)
            //                {
            //                    emailList.Add(memberMapData.PrimaryEmail);
            //                }
            //            }

            //            Company company = DataAccessFactory.CreateCompanyDataAccess().GetById(companyStatusChangeRequest.CompanyId);

            //            Mailer.SendCompanyStatusChangedRequestMassage(company, (CompanyStatus)companyStatusChangeRequest.RequestedStatus, (CompanyStatus)companyStatusChangeRequest.LastStatus, emailList);
            //        }

            //        if (workflow.IsSmsAlert)
            //        {

            //        }
            //    }

            //}
            //else
            //{
            //    //Do not require approval process. Only update company status.
            //    DataAccessFactory.CreateCompanyDataAccess().UpdateStatus(companyStatusChangeRequest.CompanyId, (CompanyStatus)companyStatusChangeRequest.RequestedStatus);
            //}

            return companyStatusChangeRequest;
        }

        CompanyStatusChangeRequest IFacade.UpdateCompanyStatusChangeRequest(CompanyStatusChangeRequest companyStatusChangeRequest)
        {
            return DataAccessFactory.CreateCompanyStatusChangeRequestDataAccess().Update(companyStatusChangeRequest);
        }

        CompanyStatusChangeRequest IFacade.GetCompanyStatusChangeRequestById(int id)
        {
            return DataAccessFactory.CreateCompanyStatusChangeRequestDataAccess().GetById(id);
        }

        CompanyStatusChangeRequest IFacade.GetCompanyStatusChangeRequestByCompanyId(int companyId)
        {
            return DataAccessFactory.CreateCompanyStatusChangeRequestDataAccess().GetByCompanyId(companyId);
        }

        IList<CompanyStatusChangeRequest> IFacade.GetAllCompanyStatusChangeRequestByCompanyId(int companyId)
        {
            return DataAccessFactory.CreateCompanyStatusChangeRequestDataAccess().GetAllByCompanyId(companyId);
        }

        IList<CompanyStatusChangeRequest> IFacade.GetAllCompanyStatusChangeRequest()
        {
            return DataAccessFactory.CreateCompanyStatusChangeRequestDataAccess().GetAll();
        }

        bool IFacade.CompanyStatusChangeRequestApprove(int companyId, string comments, int changerId)
        {
            bool result = DataAccessFactory.CreateCompanyStatusChangeRequestDataAccess().Verify(companyId, ApproveStatus.Approved, comments, changerId);

            if (result)
            {
                CompanyStatusChangeRequest request = DataAccessFactory.CreateCompanyStatusChangeRequestDataAccess().GetByCompanyId(companyId);

                if (request != null)
                {
                    if (DataAccessFactory.CreateCompanyDataAccess().UpdateStatus(companyId, (CompanyStatus)request.RequestedStatus))
                    {
                        result = DataAccessFactory.CreateCompanyDataAccess().ChangePendingStatus(companyId, false);
                    }
                }
            }

            return result;
        }

        bool IFacade.CompanyStatusChangeRequestDecline(int companyId, string comments, int changerId)
        {
            return DataAccessFactory.CreateCompanyStatusChangeRequestDataAccess().Verify(companyId, ApproveStatus.Declined, comments, changerId);
        }

        bool IFacade.DeleteCompanyStatusChangeRequestById(int id)
        {
            return DataAccessFactory.CreateCompanyStatusChangeRequestDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteCompanyStatusChangeRequestByCompanyId(int companyId)
        {
            return DataAccessFactory.CreateCompanyStatusChangeRequestDataAccess().DeleteByCompanyId(companyId);
        }

        #endregion

        #region Country

        Country IFacade.AddCountry(Country country)
        {
            return DataAccessFactory.CreateCountryDataAccess().Add(country);
        }

        Country IFacade.UpdateCountry(Country country)
        {
            return DataAccessFactory.CreateCountryDataAccess().Update(country);
        }

        Country IFacade.GetCountryById(int id)
        {
            return DataAccessFactory.CreateCountryDataAccess().GetById(id);
        }

        IList<Country> IFacade.GetAllCountry()
        {
            return DataAccessFactory.CreateCountryDataAccess().GetAll();
        }

        bool IFacade.DeleteCountryById(int id)
        {
            return DataAccessFactory.CreateCountryDataAccess().DeleteById(id);
        }

        Int32 IFacade.GetCountryIdByCountryName(string countryName)
        {
            return DataAccessFactory.CreateCountryDataAccess().GetCountryIdByCountryName(countryName);
        }

        //2.1 starts here
        Int32 IFacade.GetStateIdByStateCode(string stateCode)
        {
            return DataAccessFactory.CreateCountryDataAccess().GetStateIdByStateCode(stateCode);
        }

        Int32 IFacade.GetStateIdByStateCodeAndCountryId(string stateCode,int CountryId)
        {
            return DataAccessFactory.CreateCountryDataAccess().GetStateIdByStateCodeAndCountryId(stateCode, CountryId);
        }

        Int32 IFacade.GetStateIdByStateNameAndCountryId(string Name, int CountryId)
        {
            return DataAccessFactory.CreateCountryDataAccess().GetStateIdByStateNameAndCountryId(Name , CountryId);
        }

        Int32 IFacade.GetCountryIdByCountryCode(string countryCode)
        {
            return DataAccessFactory.CreateCountryDataAccess().GetCountryIdByCountryCode(countryCode);
        }

        string  IFacade.GetCountryNameById(int CountryId)
        {
            return DataAccessFactory.CreateCountryDataAccess().GetCountryNameById(CountryId );
        }
        //2.1 end here


        #endregion

        #region CustomSiteMap

        CustomSiteMap IFacade.AddCustomSiteMap(CustomSiteMap customSiteMap)
        {
            CustomSiteMap root = DataAccessFactory.CreateCustomSiteMapDataAccess().GetRoot();

            if (root == null)
            {
                root = new CustomSiteMap();
                root.Title = "TPS360";
                root.Description = "Root of the site.";
                root = DataAccessFactory.CreateCustomSiteMapDataAccess().Add(root);
            }

            if (customSiteMap.ParentId == 0)
            {
                customSiteMap.ParentId = root.Id;
            }

            return DataAccessFactory.CreateCustomSiteMapDataAccess().Add(customSiteMap);
        }

        CustomSiteMap IFacade.UpdateCustomSiteMap(CustomSiteMap customSiteMap)
        {
            CustomSiteMap root = DataAccessFactory.CreateCustomSiteMapDataAccess().GetRoot();

            if (customSiteMap.ParentId == 0)
            {
                customSiteMap.ParentId = root.Id;
            }

            return DataAccessFactory.CreateCustomSiteMapDataAccess().Update(customSiteMap);
        }

        CustomSiteMap IFacade.GetCustomSiteMapById(int id)
        {
            return DataAccessFactory.CreateCustomSiteMapDataAccess().GetById(id);
        }

        CustomSiteMap IFacade.GetCustomSiteMapByName(string name)
        {
            return DataAccessFactory.CreateCustomSiteMapDataAccess().GetByName(name);
        }

        CustomSiteMap IFacade.GetCustomSiteMapRoot()
        {
            return DataAccessFactory.CreateCustomSiteMapDataAccess().GetRoot();
        }

        IList<CustomSiteMap> IFacade.GetAllCustomSiteMap()
        {
            return DataAccessFactory.CreateCustomSiteMapDataAccess().GetAll();
        }

        IList<CustomSiteMap> IFacade.GetAllParentCustomSiteMap()
        {
            return DataAccessFactory.CreateCustomSiteMapDataAccess().GetAllParent();
        }

        IList<CustomSiteMap> IFacade.GetAllCustomSiteMapByParent(int parentId)
        {
            return DataAccessFactory.CreateCustomSiteMapDataAccess().GetAllByParent(parentId);
        }

        PagedResponse<CustomSiteMap> IFacade.GetPagedCustomSiteMap(PagedRequest request)
        {
            return DataAccessFactory.CreateCustomSiteMapDataAccess().GetPaged(request);
        }

        bool IFacade.DeleteCustomSiteMapById(int id)
        {
            return DataAccessFactory.CreateCustomSiteMapDataAccess().DeleteById(id);
        }

        CustomSiteMap IFacade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(int ParentId, int MemberId)
        {
            return DataAccessFactory.CreateCustomSiteMapDataAccess().GetAllByParentIDAndMemberPrivilege(ParentId, MemberId);
        }

        IList<MenuListCount> IFacade.CustomSiteMap_GetMenuListCount(int ParentId, int Id)
        {
            return DataAccessFactory.CreateCustomSiteMapDataAccess().getMenuListCount(ParentId, Id);
        }
        #endregion

        #region Department

        Department IFacade.AddDepartment(Department Department)
        {
            return DataAccessFactory.CreateDepartmentDataAccess().Add(Department);
        }

        Department IFacade.UpdateDepartment(Department Department)
        {
            return DataAccessFactory.CreateDepartmentDataAccess().Update(Department);
        }

        Department IFacade.GetDepartmentById(int id)
        {
            return DataAccessFactory.CreateDepartmentDataAccess().GetById(id);
        }

        IList<Department> IFacade.GetAllDepartment()
        {
            return DataAccessFactory.CreateDepartmentDataAccess().GetAll();
        }

        bool IFacade.DeleteDepartmentById(int id)
        {
            return DataAccessFactory.CreateDepartmentDataAccess().DeleteById(id);
        }

        Int32 IFacade.GetDepartmentIdByDepartmentName(string DepartmentName)
        {
            return DataAccessFactory.CreateDepartmentDataAccess().GetDepartmentIdByDepartmentName(DepartmentName);
        }

        IList<Department> IFacade.GetAllDepartmentBySearch(string keyword, int count)
        {
            return DataAccessFactory.CreateDepartmentDataAccess().GetAllBySearch(keyword, count);
        }


        #endregion

        #region Duplicate Member

        IList<DuplicateMember> IFacade.GetDuplicateRecords(bool checkFirstName, string strFirstName,
           bool checkMiddleName, string strMiddleName,
           bool checkLastName, string strLastName,
           bool checkEmailId, string strEmailId,
           bool checkDOB, DateTime? DOB,
           bool checkSSN, string strSSN)
        {
            return DataAccessFactory.CreateDuplicateMemberDataAccess().GetDuplicateRecords(checkFirstName, strFirstName,
                                                                 checkMiddleName, strMiddleName,
                                                                 checkLastName, strLastName,
                                                                 checkEmailId, strEmailId,
                                                                 checkDOB, DOB,
                                                                 checkSSN, strSSN
                                                                 );
        }

        #endregion

        #region Employee

        Employee IFacade.GetEmployeeById(int id)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetById(id);
        }
        //***********added by pravin khot on 7/June/2016**********
        Employee IFacade.GetEmployeeByEmailId(string  Emailid)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetByEmailId(Emailid);
        }
        //************************END***************************


        Employee IFacade.GetEmployeeByIdForHR(int id)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetByIdForHR(id);
        }

        Employee IFacade.GetEmployeeActivitiesReportByIdAndCreateDate(int id, DateTime createDate)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetActivitiesReportByIdAndCreateDate(id, createDate);
        }

        PagedResponse<Employee> IFacade.GetPagedEmployee(PagedRequest request)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetPaged(request);
        }

        PagedResponse<Employee> IFacade.GetPagedEmployeeReport(PagedRequest request)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetPagedWorkReport(request);
        }

        EmployeeStatistics IFacade.GetEmployeeStatisticsByEmployeeId(int employeeId)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetStatisticsByEmployeeId(employeeId);
        }

        PagedResponse<Employee> IFacade.GetPagedEmployee(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, int industry, int functionalCategory, int workPermit,
            int gender, int maritalStatus, int educationId, int assignedManager)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetPaged(request, addedFrom, addedTo, addedBy, updatedFrom, updatedTo, updatedBy,
           country, state, city, industry, functionalCategory, workPermit,
           gender, maritalStatus, educationId, assignedManager);
        }

        BusinessOverview IFacade.GetBusinessOverviewByEmployeeId(int employeeId)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetBusinessOverviewByEmployeeId(employeeId);
        }

        HireDesk IFacade.GetEmployeeHireDeskByEmployeeId(int employeeId)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetHireDeskByEmployeeId(employeeId);
        }
        //starts 2.0
        DataTable IFacade.GetProductivityOverviewByDate(string RequireDate)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetProductivityOverviewByDate(RequireDate);
        }
        //ends 2.0

        DataTable IFacade.GetMyProductivityOverviewByDate(string RequireDate,int MemberId)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetMyProductivityOverviewByDate(RequireDate, MemberId);
        }


        PagedResponse<EmployeeProductivity> IFacade.GetPagedEmployeeProductivity(DateTime StartDate,DateTime EndDate, int MemberId, PagedRequest request)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetPagedEmployeeProductivity(StartDate ,EndDate , MemberId, request);
        }

        EmployeeOverviewDetails IFacade.GetEmployeeOverviewDetails(int memberid) 
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetEmployeeOverviewDetails(memberid);
        }

        void IFacade.Employee_EmployeeFirstLoginCreate(int MemberId)
        {
            DataAccessFactory.CreateEmployeeDataAccess().EmployeeFirstLogin_Create(MemberId);
        }

        bool IFacade.Employee_IsEmployeeFirstLogin(int MemberId)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().IsEmployeeFirstLogin(MemberId);
        }


        ArrayList IFacade.GetAllEmployeeByTeamId(string id)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetAllEmployeeByTeamId(id);
        }
        PagedResponse<EmployeeProductivity> IFacade.GetPagedByProductityReport(PagedRequest request) 
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetPagedByProductityReport(request);
        }


        IList<EmployeeProductivity > IFacade.GetEmployeeProductivityReportByDate(string UserIds, string TeamIds, int StartDate, int EndDate)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetAllListGroupByDate(UserIds, TeamIds, StartDate, EndDate);
        }

        PagedResponse<EmployeeProductivity> IFacade.GetPagedByTeamProductityReport(PagedRequest request) 
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetPagedByTeamProductityReport(request);
        
        }
        ArrayList IFacade.GetEmployeeNameAndContactNumber(int EmployeeId) 
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetEmployeeNameandContactNumber(EmployeeId);
        }
        //************Added by pravin khot on 25/May/2016**************TIMEZONE
        ArrayList IFacade.GetAllTimeZone()
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetAllTimezone();
        }        

        void IFacade.Employee_SaveTimezone(int MemberId, int TimezoneId)
        {
            DataAccessFactory.CreateEmployeeDataAccess().Employee_SaveTimezone(MemberId, TimezoneId);
        }

        Employee IFacade.GetTimeZoneByMemberId(int MemberId)
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetTimeZoneByMemberId(MemberId);
        }
        //****************************END*********************************
        //////////////////////code added by Sumit Sonawane on 21/Mar/2017////////////////////////////////////////////////////////////////////////////
        IList<EmployeeProductivity> IFacade.GetEmployeeProductivityDetails()
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetEmployeeProductivityDetails();
        }

        IList<EmployeeProductivity> IFacade.GetEmployeePresenttoInterviewRatio()
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetEmployeePresenttoInterviewRatio();
        }

        IList<EmployeeProductivity> IFacade.GetEmployeeTimetoHire()
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetEmployeeTimetoHire();
        }

        IList<EmployeeProductivity> IFacade.GetJoinedToRejectedByBusiness()
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetJoinedToRejectedByBusiness();
        }

        IList<EmployeeProductivity> IFacade.GetCandidateBySkillId()
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetCandidateBySkillId();
        }

        IList<EmployeeProductivity> IFacade.GetJobPostingByMonthYear()
        {
            return DataAccessFactory.CreateEmployeeDataAccess().GetJobPostingByMonthYear();
        }

        ////////////////////// End ////////////////////////////////////////////////////////////////////////////
        #endregion

        #region EmployeeReferral

        int IFacade.AddReferral(EmployeeReferral employeeref)
        {
            return  DataAccessFactory.CreateEmployeeReferralDataAccess ().Add(employeeref);         
        }
        PagedResponse<EmployeeReferral> IFacade.GetPagedEmployeeReferral(PagedRequest request)
        {
            return DataAccessFactory.CreateEmployeeReferralDataAccess ().GetPaged(request);
        }

        PagedResponse<ListWithCount> IFacade.EmployeeReferral_GetPagedSummary(PagedRequest request, string type)
        {
            return DataAccessFactory.CreateEmployeeReferralDataAccess().GetPagedReferralSummary(request, type);
        }
        ArrayList IFacade.GetAllEmployeeReferrerList()
        {
            return DataAccessFactory.CreateEmployeeReferralDataAccess().GetAllReferrerList();
        }
        IList<ListWithCount> IFacade.EmployeeReferral_GetCountGroupbyDate(int JobPostingId, int ReferrerID, int StartDate, int EndDate)
        {
            return DataAccessFactory.CreateEmployeeReferralDataAccess().GetAllListGroupByDate(JobPostingId,ReferrerID ,StartDate ,EndDate );
        }
        //////////////////////code added by Sumit on 26/May/2016////////////////////////////////////////////////////////////////////////////
        IList<ListWithCount> IFacade.EmployeeReferral_GetCountGroupbyDate(string JobPostingId, int ReferrerID, int StartDate, int EndDate)
        {
            return DataAccessFactory.CreateEmployeeReferralDataAccess().GetAllListGroupByDate(JobPostingId, ReferrerID, StartDate, EndDate);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        EmployeeReferral IFacade.EmployeeReferral_GetByMemberId(int MemberID)
        {
            return DataAccessFactory.CreateEmployeeReferralDataAccess().GetByMemberId(MemberID);
        }

        IList<EmployeeProductivity> IFacade.GetEmployeeProductivityReport(PagedRequest request)
        {

            return DataAccessFactory.CreateEmployeeDataAccess().GetEmployeeProductivityReport(request);
        }
        #endregion

        #region EmployeeTeamBuilder

        EmployeeTeamBuilder IFacade.AddEmployeeTeamBuilder(EmployeeTeamBuilder employeeTeamBuilder)
        {
            return DataAccessFactory.CreateEmployeeTeamBuilderDataAccess().Add(employeeTeamBuilder);
        }

        EmployeeTeamBuilder IFacade.UpdateEmployeeTeamBuilder(EmployeeTeamBuilder employeeTeamBuilder)
        {
            return DataAccessFactory.CreateEmployeeTeamBuilderDataAccess().Update(employeeTeamBuilder);
        }

        EmployeeTeamBuilder IFacade.EmployeeTeamBuilder_GetByTeamId(int id)
        {
            return DataAccessFactory.CreateEmployeeTeamBuilderDataAccess().GetByTeamId(id);
        }

        IList<EmployeeTeamBuilder> IFacade.EmployeeTeamBuilder_GetAllTeam()
        {
            return DataAccessFactory.CreateEmployeeTeamBuilderDataAccess().GetAllTeam();
        }

        bool IFacade.DeleteEmployeeTeamBuilderById(int id)
        {
            return DataAccessFactory.CreateEmployeeTeamBuilderDataAccess().DeleteById(id);
        }

        PagedResponse<EmployeeTeamBuilder> IFacade.GetPagedEmployeeTeamBuilder(PagedRequest request)
        {
            return DataAccessFactory.CreateEmployeeTeamBuilderDataAccess().GetPaged(request);
        }
        //***********code added by pravin khot on 16/May/2016*********
        ArrayList IFacade.GetAllTeamByTeamLeaderId(int TeamleaderId)
        {
            return DataAccessFactory.CreateEmployeeTeamBuilderDataAccess().GetAllTeamByTeamLeaderId(TeamleaderId);
        }
        //************************END**************************
        /////////////Code Added by Sumit Sonawane on 20/May/2016/////////////////////////
        IList<EmployeeTeamBuilder> IFacade.EmployeeTeamBuilder_GetTeamsByMemberId(int MemberId)
        {
            return DataAccessFactory.CreateEmployeeTeamBuilderDataAccess().GetTeamsByMemberId(MemberId);
        }
        /////////////////////////////////end///////////////////////////////////////////

#endregion

        #region EventLog
        void IFacade.AddEventLog (EventLogForRequisitionAndCandidate Log, string CandidateId)
        {
            DataAccessFactory.CreateEventLogDataAccess().Add(Log , CandidateId );
        }

        PagedResponse<EventLogForRequisitionAndCandidate> IFacade.GetPagedForEventLog (PagedRequest request)
        {
            return DataAccessFactory.CreateEventLogDataAccess().GetPaged(request);
        }

        IList<EventLogForRequisitionAndCandidate> IFacade.GetAllEventLog(int JobPostingId, int CandidateId, int CreatorId)
        {
            return DataAccessFactory.CreateEventLogDataAccess().GetAll(JobPostingId, CandidateId, CreatorId );
        }

        PagedResponse<EventLogForRequisitionAndCandidate> IFacade.GetPagedEventLogReport(PagedRequest request)
        {
            return DataAccessFactory.CreateEventLogDataAccess().GetPagedForReport(request);
        }
        ////////////////////////////Sumit Sonawane 1/Mar/2017///////////////////////////////////////////////////////////////////
        PagedResponse<EventLogForRequisitionAndCandidate> IFacade.GetPagedHiringMatrixEventLog(int MemberID, PagedRequest request)
        {
            return DataAccessFactory.CreateEventLogDataAccess().getPagedHiringMatrixEventLogDetails(MemberID, request);
        }
        #endregion

        #region GenericLookup

        GenericLookup IFacade.AddGenericLookup(GenericLookup genericLookup)
        {
            return DataAccessFactory.CreateGenericLookupDataAccess().Add(genericLookup);
        }

        GenericLookup IFacade.UpdateGenericLookup(GenericLookup genericLookup)
        {
            return DataAccessFactory.CreateGenericLookupDataAccess().Update(genericLookup);
        }

        GenericLookup IFacade.GetGenericLookupById(int id)
        {
            return DataAccessFactory.CreateGenericLookupDataAccess().GetById(id);
        }
        //***************Code added by pravin khot on 25/May/2016*******
        GenericLookup IFacade.GetGenericLookupByDescription(string location)
        {
            return DataAccessFactory.CreateGenericLookupDataAccess().GetByDescription(location);
        }
        //************************END*******************************
        IList<GenericLookup> IFacade.GetAllGenericLookup()
        {
            return DataAccessFactory.CreateGenericLookupDataAccess().GetAll();
        }

        IList<GenericLookup> IFacade.GetAllGenericLookupByLookupType(LookupType lookupType)
        {
            return DataAccessFactory.CreateGenericLookupDataAccess().GetAllByLookupType(lookupType);
        }
        // For Optimization by Vignesh
        IList<GenericLookup> IFacade.GetAllGenericLookupByLookupType(string lookuptypes)
        {
            return DataAccessFactory.CreateGenericLookupDataAccess().GetAllByLookupType(lookuptypes);
        }

        IList<GenericLookup> IFacade.GetAllGenericLookupByLookupTypeAndName(LookupType lookupType,string lookupName)
        {
            return DataAccessFactory.CreateGenericLookupDataAccess().GetAllByLookupTypeAndName(lookupType,lookupName);
        }

        PagedResponse<GenericLookup> IFacade.GetPagedGenericLookup(PagedRequest request)
        {
            return DataAccessFactory.CreateGenericLookupDataAccess().GetPaged(request);
        }

        bool IFacade.DeleteGenericLookupById(int id)
        {
            return DataAccessFactory.CreateGenericLookupDataAccess().DeleteById(id);
        }
        IList<GenericLookup> IFacade.GetGenericLookupbyIDs(string IDs)
        {
            return DataAccessFactory.CreateGenericLookupDataAccess().GetByIds(IDs);
        }

        string IFacade.GetLookUPNamesByIds(string Ids)
        {
            return DataAccessFactory.CreateGenericLookupDataAccess().GetLookUPNamesByIds(Ids);
        }

        //Code introduced by Prasanth on 05/Dec/2015 Start
        IList<GenericLookup> IFacade.InterviewAssessment_GetBy_InterviewId_InterviewerEmail(int InterviewId, string Email)
        {
            return DataAccessFactory.CreateGenericLookupDataAccess().InterviewAssessment_GetBy_InterviewId_InterviewerEmail(InterviewId, Email);
        }
        //******************END*********************
        #endregion

        #region HiringMatrix

        PagedResponse<HiringMatrix> IFacade.GetPagedHiringMatrix(PagedRequest request)
        {
            return DataAccessFactory.CreateHiringMatrixDataAccess().GetPaged(request);
        }


        PagedResponse<HiringMatrix> IFacade.GetMinPagedHiringMatrix(PagedRequest request)
        {
            return DataAccessFactory.CreateHiringMatrixDataAccess().MinGetPaged (request);
        }

        PagedResponse<HiringMatrix> IFacade.GetMinPagedHiringMatrixAll(PagedRequest request)
        {
            return DataAccessFactory.CreateHiringMatrixDataAccess().MinGetPagedAll(request);
        }

        HiringMatrix IFacade.getHiringMatrixByJobPostingIDAndCandidateId(int JobPostingID, int CandidateId)
        {
            return DataAccessFactory.CreateHiringMatrixDataAccess().GetByJobPostingIDandCandidateId(JobPostingID, CandidateId);
        }

        ////////////////////////////Sumit Sonawane 22/07/2016///////////////////////////////////////////////////////////////////
        PagedResponse<HiringMatrix> IFacade.GetPagedHiringMatrixBucketView(PagedRequest request)
        {
            return DataAccessFactory.CreateHiringMatrixDataAccess().GetPagedBucketView(request);
        }
        ////////////////////////////Sumit Sonawane 1/Mar/2017///////////////////////////////////////////////////////////////////
        HiringMatrix IFacade.getHiringMatrixLevelByJobPostingIDAndCandidateId(int JobPostingID, int CandidateId)
        {
            return DataAccessFactory.CreateHiringMatrixDataAccess().getMatrixLevelByJobPostingIDAndCandidateId(JobPostingID, CandidateId);
        }


        HiringMatrix IFacade.AddHiringMatrixLogDetails(HiringMatrix AddHiringMatrixLogDetails)
        {
            return DataAccessFactory.CreateHiringMatrixDataAccess().getHiringMatrixLogDetails(AddHiringMatrixLogDetails);
        }
        ////////////////////////////Sumit Sonawane 27/07/2016///////////////////////////////////////////////////////////////////
        PagedResponse<HiringMatrix> IFacade.HiringMatrixBucket(PagedRequest request)
        {
            return DataAccessFactory.CreateHiringMatrixDataAccess().BucketView(request);
        }
        PagedResponse<HiringMatrix> IFacade.HiringMatrixBucketLoad(PagedRequest request)
        {
            return DataAccessFactory.CreateHiringMatrixDataAccess().BucketViewLoadAll(request);
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #endregion

        #region HiringMatrixLevels

        HiringMatrixLevels IFacade.AddHiringMatrixLevels(HiringMatrixLevels hiringMatrixLevels)
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().Add(hiringMatrixLevels);
        }

        HiringMatrixLevels IFacade.UpdateHiringMatrixLevels(HiringMatrixLevels hiringMatrixLevels)
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().Update(hiringMatrixLevels);
        }

        HiringMatrixLevels IFacade.GetHiringMatrixLevelsById(int id)
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().GetById(id);
        }

        IList<HiringMatrixLevels> IFacade.GetAllHiringMatrixLevels()
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().GetAll();
        }

        bool IFacade.DeleteHiringMatrixLevelsById(int id)
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().DeleteById(id);
        }

        void IFacade.DeleteHiringMatrixLevels()
        {
            DataAccessFactory.CreateHiringMatrixLevelsDataAccess().DeleteAllHiringMatrix();
        }

        HiringMatrixLevels IFacade.HiringMatrixLevel_GetPreviousLevelById(int id)
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().GetPreviousLevelByID(id);
        }

        HiringMatrixLevels IFacade.HiringMatrixLevel_GetNextLevelById(int id)
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().GetNextLevelByID(id);
        }

        HiringMatrixLevels IFacade.HiringMatrixLevel_GetInitialLevel()
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().GetInitialLevel();
        }


        HiringMatrixLevels IFacade.HiringMatrixLevel_GetLastLevel()
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().GetFinalLevel();
        }

        int IFacade.GetHiringMatrixLevelsCount()
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().GetHiringMatrixLevelsCount();
        }

        IList<HiringMatrixLevels> IFacade.GetAllHiringMatrixLevelsWithCount(int JobPostingId)
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().GetAllWithCount(JobPostingId);
        }
        IList<HiringMatrixLevels> IFacade.GetAllHiringMatrixLevelsWithCountByMemberId(int MemberId)
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().GetAllWithCountByMemberId(MemberId);
        }

        HiringMatrixLevels IFacade.HiringMatrixLevel_GetLevelByName(string LevelName)
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().GetLevelByName(LevelName);
        }
        //**************Code added by pravin khot on 13/May/2016************* 
        Int32 IFacade.HiringMatrixLevel_Id(int LevelId)
        {
            return DataAccessFactory.CreateHiringMatrixLevelsDataAccess().HiringMatrixLevel_Id(LevelId);
        }
        //*****************************END*********************************
        #endregion

        #region Interview

        Interview IFacade.AddInterview(Interview interview)
        {
            return DataAccessFactory.CreateInterviewDataAccess().Add(interview);
        }

        Interview IFacade.UpdateInterview(Interview interview)
        {
            return DataAccessFactory.CreateInterviewDataAccess().Update(interview);
        }

        Interview IFacade.GetInterviewById(int id)
        {
            return DataAccessFactory.CreateInterviewDataAccess().GetById(id);
        }

        IList<Interview> IFacade.GetAllInterview()
        {
            return DataAccessFactory.CreateInterviewDataAccess().GetAll();
        }

        bool IFacade.DeleteInterviewById(int id)
        {
            return DataAccessFactory.CreateInterviewDataAccess().DeleteById(id);
        }
        //**********Added by pravin khot on 4/May/2016**
        bool IFacade.CancelInterviewById(int id) 
        {
            return DataAccessFactory.CreateInterviewDataAccess().CancelById(id);
        }

        bool IFacade.UpdateInterviewIcsCodeById(int id,string ics) 
        {
            return DataAccessFactory.CreateInterviewDataAccess().UpdateIcsCodeById(id, ics);
        }

        //***************************END********************

        //Code added by Sumit Sonawane on 1/Mar/2017 ***********
        bool IFacade.UpdateInterviewNoShow_ById(int id)
        {
            return DataAccessFactory.CreateInterviewDataAccess().UpdateIntNoShow_ById(id);
        }
        bool IFacade.UpdateInterviewCompleted_ById(int id)
        {
            return DataAccessFactory.CreateInterviewDataAccess().UpdateIntCompleted_ById(id);
        }
        Interview IFacade.AddInterviewStatusDetails(Interview InterviewStatusDetails)
        {
            return DataAccessFactory.CreateInterviewDataAccess().AddInterviewStatusDetails(InterviewStatusDetails);
        }
        //***************************END********************

        IList<Interview> IFacade.GetAllInterviewByJobPostingId(int jobPostingId)
        {
            return DataAccessFactory.CreateInterviewDataAccess().GetAllByJobPostingId(jobPostingId);
        }
        IList<Interview> IFacade.GetInterviewDateByMemberId(int MemberId)
        {
            return DataAccessFactory.CreateInterviewDataAccess().GetInterviewDateByMemberId(MemberId);
        }
        IList<Interview> IFacade.GetInterviewByMemberIdAndJobPostingID(int MemberId, int JobPostingId)
        {
            return DataAccessFactory.CreateInterviewDataAccess().GetInterviewByMemberIdAndJobPostingID(MemberId, JobPostingId);
        }
        Int32 IFacade.GetContactIdByJobPostingId (int JobpostingId)
        {
            return DataAccessFactory.CreateInterviewDataAccess().GetContactIdByJobPostingId(JobpostingId);
        }

        string IFacade.Interview_GetInterviewTemplate(int memberId, int InterviewID, string Template_Type,int CreatorId)
        {
            return DataAccessFactory.CreateInterviewDataAccess().GetInterviewTemplate(memberId, InterviewID, Template_Type, CreatorId);
        }
        //**************************code introduced by pravin khot on 10/Dec/2015********************
        string IFacade.SuggestedInterviewer_Id(int InterviewerId)
        {
            return DataAccessFactory.CreateInterviewDataAccess().SuggestedInterviewer_Id(InterviewerId);
        }
        //************************************************************************************

        #endregion

        #region InterviewInterviewerMap

        InterviewInterviewerMap IFacade.AddInterviewInterviewerMap(InterviewInterviewerMap interviewInterviewerMap)
        {
            return DataAccessFactory.CreateInterviewInterviewerMapDataAccess().Add(interviewInterviewerMap);
        }
        //**************************code introduced by pravin khot on 11/Dec/2015********************
        InterviewInterviewerMap IFacade.AddSuggestedInterviewInterviewerMap(InterviewInterviewerMap interviewInterviewerMap)
        {
            return DataAccessFactory.CreateInterviewInterviewerMapDataAccess().AddSuggestedInterviewer(interviewInterviewerMap);
        }
        //************************************************************************************


        InterviewInterviewerMap IFacade.UpdateInterviewInterviewerMap(InterviewInterviewerMap interviewInterviewerMap)
        {
            return DataAccessFactory.CreateInterviewInterviewerMapDataAccess().Update(interviewInterviewerMap);
        }

        InterviewInterviewerMap IFacade.GetInterviewInterviewerMapById(int id)
        {
            return DataAccessFactory.CreateInterviewInterviewerMapDataAccess().GetById(id);
        }

        IList<InterviewInterviewerMap> IFacade.GetAllInterviewInterviewerMap()
        {
            return DataAccessFactory.CreateInterviewInterviewerMapDataAccess().GetAll();
        }

        IList<InterviewInterviewerMap> IFacade.GetAllInterviewersByInterviewId (int InterviewId)
        {
            return DataAccessFactory.CreateInterviewInterviewerMapDataAccess().GetAllInterviewersByInterviewId(InterviewId);
        }

        //********************Code added by pravin khot on 5/May/2016*************
        IList<InterviewInterviewerMap> IFacade.GetAllsuggestedInterviewersByInterviewId(int InterviewId)
        {
            return DataAccessFactory.CreateInterviewInterviewerMapDataAccess().GetAllsuggestedInterviewId(InterviewId);
        }
        //**********************************END************************
        bool IFacade.DeleteInterviewInterviewerMapById(int id)
        {
            return DataAccessFactory.CreateInterviewInterviewerMapDataAccess().DeleteById(id);
        }

        string  IFacade.GetInterviersNameByInterviewId (int InterviewId)
        {
            return DataAccessFactory.CreateInterviewInterviewerMapDataAccess().GetInterviersNameByInterviewId(InterviewId);
        }

        bool IFacade.DeleteInterviewInterviewerMapByInterviewerId(int interviewerId)
        {
            return DataAccessFactory.CreateInterviewInterviewerMapDataAccess().DeleteByInterviewerId(interviewerId);
        }

        bool IFacade.DeleteInterviewInterviewerMapByInterviewId(int interviewId)
        {
            return DataAccessFactory.CreateInterviewInterviewerMapDataAccess().DeleteByInterviewId(interviewId);
        }
        #endregion

        #region IPAccessRules

        IPAccessRules IFacade.AddIPAccessRules(IPAccessRules rules)
        {
            return DataAccessFactory.CreateIPAccessRulesDataAccess().Add(rules);
        }

        IPAccessRules IFacade.UpdateIPAccessRules(IPAccessRules rules)
        {
            return DataAccessFactory.CreateIPAccessRulesDataAccess().Update(rules);
        }

        IPAccessRules IFacade.IPAccessRules_GetById(int Id)
        {
            return DataAccessFactory.CreateIPAccessRulesDataAccess().GetById(Id);
        }

        IList<IPAccessRules> IFacade.GetAllIPAccessRules()
        {
            return DataAccessFactory.CreateIPAccessRulesDataAccess().GetAll();
        }

        bool IFacade.IPAccessRules_DeleteById(int Id)
        {
            return DataAccessFactory.CreateIPAccessRulesDataAccess().DeleteById(Id);
        }

        PagedResponse<IPAccessRules> IFacade.IPAccessRules_GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateIPAccessRulesDataAccess().GetPaged(request);
        }


        #endregion
		#region InterviewFeedback
        void IFacade.InterviewFeedback_Add(InterviewFeedback interviewFeedback) 
        {
             DataAccessFactory.CreateInterviewFeedbackDataAccess().Add(interviewFeedback);
        }
        void IFacade.InterviewFeedback_Update(InterviewFeedback interviewFeedback)
        {
            DataAccessFactory.CreateInterviewFeedbackDataAccess().Update(interviewFeedback);
        }
        InterviewFeedback IFacade.InterviewFeedback_GetById(int id)
        {
            return DataAccessFactory.CreateInterviewFeedbackDataAccess().GetById(id);
        }
        IList<InterviewFeedback> IFacade.InterviewFeedback_GetAll()
        {
            return DataAccessFactory.CreateInterviewFeedbackDataAccess().GetAll();
        }
        PagedResponse<InterviewFeedback> IFacade.InterviewFeedback_GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateInterviewFeedbackDataAccess().GetPaged(request);
        }
        void IFacade.InterviewFeedback_DeleteById(int id)
        {
            DataAccessFactory.CreateInterviewFeedbackDataAccess().DeleteById(id);
        }
        IList<InterviewFeedback> IFacade.InterviewFeedback_GetByInterviewId(int InterviewId)
        {
            return DataAccessFactory.CreateInterviewFeedbackDataAccess().GetByInterviewId(InterviewId);
        }

        //function introduced by Prasanth on `
        InterviewFeedback IFacade.InterviewFeedback_GetByInterviewId_InterviewerEmail(int InterviewId, string InterviewerEmail)
        {
            return DataAccessFactory.CreateInterviewFeedbackDataAccess().GetByInterviewId_InterviewerEmail(InterviewId, InterviewerEmail);
        }
        #endregion

        //**********************Code introduced by Prasanth on 20/Jul/2015 Start**********************************

        #region InterviewerFeedback
        void IFacade.InterviewerFeedback_Add(InterviewFeedback interviewFeedback)
        {
            DataAccessFactory.CreateInterviewerFeedbackDataAccess().Add(interviewFeedback);
        }
        void IFacade.InterviewerFeedback_Update(InterviewFeedback interviewFeedback)
        {
            DataAccessFactory.CreateInterviewerFeedbackDataAccess().Update(interviewFeedback);
        }
        InterviewFeedback IFacade.InterviewerFeedback_GetById(int id)
        {
            return DataAccessFactory.CreateInterviewerFeedbackDataAccess().GetById(id);
        }
        IList<InterviewFeedback> IFacade.InterviewerFeedback_GetAll()
        {
            return DataAccessFactory.CreateInterviewerFeedbackDataAccess().GetAll();
        }
        PagedResponse<InterviewFeedback> IFacade.InterviewerFeedback_GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateInterviewerFeedbackDataAccess().GetPaged(request);
        }
        void IFacade.InterviewerFeedback_DeleteById(int id)
        {
            DataAccessFactory.CreateInterviewFeedbackDataAccess().DeleteById(id);
        }
        IList<InterviewFeedback> IFacade.InterviewerFeedback_GetByInterviewId(int InterviewId)
        {
            return DataAccessFactory.CreateInterviewerFeedbackDataAccess().GetByInterviewId(InterviewId);
        }

        //***********************code introduced by pravin  24/Dec/2015 ***********
        IList<InterviewFeedback> IFacade.InterviewFeedback_GetByInterviewIdDetail(int InterviewId)
        {
            return DataAccessFactory.CreateInterviewFeedbackDataAccess().GetByInterviewIdDetail(InterviewId);
        }

        IList<InterviewFeedback> IFacade.InterviewFeedback_GetByInterviewIdAllEmail(int InterviewId)
        {
            return DataAccessFactory.CreateInterviewFeedbackDataAccess().GetByInterviewIdAllEmail(InterviewId);
        }

        //*****************************End******************************************
        //***********************code introduced by pravin  24/Dec/2015 ***********

        PagedResponse<InterviewFeedback> IFacade.GetPagedInterviewFeedbackReport(PagedRequest request)
        {
            return DataAccessFactory.CreateInterviewFeedbackDataAccess().GetInterviewFeedbackReport(request);
        }
        PagedResponse<InterviewFeedback> IFacade.GetPagedInterviewFeedbackReportPrint(PagedRequest request)
        {
            return DataAccessFactory.CreateInterviewFeedbackDataAccess().GetInterviewFeedbackReportPrint(request);
        }

        PagedResponse<InterviewFeedback> IFacade.GetPagedInterviewAssesReportPrint(PagedRequest request)
        {
            return DataAccessFactory.CreateInterviewFeedbackDataAccess().GetInterviewAssesReportPrint(request);
        }
        //*************end******************************************


        #endregion
        //****************************************END********************************************
        //Code Interoduced By Prasanth on 16/Oct/2015 Start
        #region InterviewQuestionBank
        void IFacade.InterviewQuestionBank_Add(InterviewQuestionBank InterviewQuestionBank)
        {
            DataAccessFactory.CreateInterviewQuestionBankDataAccess().Add(InterviewQuestionBank);
        }
        void IFacade.InterviewQuestionBank_Update(InterviewQuestionBank InterviewQuestionBank)
        {
            DataAccessFactory.CreateInterviewQuestionBankDataAccess().Update(InterviewQuestionBank);
        }
        InterviewQuestionBank IFacade.InterviewQuestionBank_GetById(int id)
        {
            return DataAccessFactory.CreateInterviewQuestionBankDataAccess().GetById(id);
        }
        IList<InterviewQuestionBank> IFacade.InterviewQuestionBank_GetAll()
        {
            return DataAccessFactory.CreateInterviewQuestionBankDataAccess().GetAll();
        }
        PagedResponse<InterviewQuestionBank> IFacade.InterviewQuestionBank_GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateInterviewQuestionBankDataAccess().GetPaged(request);
        }
        void IFacade.InterviewQuestionBank_DeleteById(int id)
        {
            DataAccessFactory.CreateInterviewFeedbackDataAccess().DeleteById(id);
        }
        IList<InterviewQuestionBank> IFacade.InterviewQuestionBank_GetByInterviewId(int InterviewId)
        {
            return DataAccessFactory.CreateInterviewQuestionBankDataAccess().GetByInterviewId(InterviewId);
        }



        #endregion

        //***********************code introduced by pravin  27/nov/2015 ***********

        #region InterviewPanelMaster
        InterviewPanelMaster IFacade.InterviewPanelMaster_Add(InterviewPanelMaster InterviewPanelMaster)
        {
            return DataAccessFactory.CreateInterviewPanelMasterDataAccess().Add(InterviewPanelMaster);
        }
        InterviewPanelMaster IFacade.InterviewPanelMaster_Update(InterviewPanelMaster InterviewPanelMaster)
        {
            return DataAccessFactory.CreateInterviewPanelMasterDataAccess().Update(InterviewPanelMaster);
        }
        InterviewPanelMaster IFacade.InterviewPanelMaster_GetById(int id)
        {
            return DataAccessFactory.CreateInterviewPanelMasterDataAccess().GetById(id);
        }
        IList<InterviewPanelMaster> IFacade.InterviewPanelMaster_GetAll()
        {
            return DataAccessFactory.CreateInterviewPanelMasterDataAccess().GetAll();
        }
        IList<InterviewPanelMaster> IFacade.InterviewPanelMaster_GetAll(string SortExpression)
        {
            return DataAccessFactory.CreateInterviewPanelMasterDataAccess().GetAll(SortExpression);
        }
        PagedResponse<InterviewPanelMaster> IFacade.InterviewPanelMaster_GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateInterviewPanelMasterDataAccess().GetPaged(request);
        }
        bool IFacade.InterviewPanelMaster_DeleteById(int id)
        {
            return DataAccessFactory.CreateInterviewPanelMasterDataAccess().DeleteById(id);
        }
        string IFacade.GetSkills(int InterviewPanelMaster_ById)
        {
            return DataAccessFactory.CreateInterviewPanelMasterDataAccess().GetSkills(InterviewPanelMaster_ById);
        }
        Int32 IFacade.InterviewPanelMaster_Id(string InterviewPanel_Name)
        {
            return DataAccessFactory.CreateInterviewPanelMasterDataAccess().InterviewPanelMaster_Id(InterviewPanel_Name);
        }
        Int32 IFacade.InterviewPanelMaster_Id(string InterviewPanel_Name, int InterviewPanel_EditId)
        {
            return DataAccessFactory.CreateInterviewPanelMasterDataAccess().InterviewPanelMaster_Id(InterviewPanel_Name, InterviewPanel_EditId);
        }

        #endregion

        //*************end******************************************

        //***********************code introduced by pravin  20/nov/2015 ***********

        #region InterviewPanel
        InterviewPanel IFacade.InterviewPanel_Add(InterviewPanel InterviewPanel)
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().Add(InterviewPanel);
        }
        InterviewPanel IFacade.InterviewPanel_AddOtherInterviewer(InterviewPanel InterviewPanel)
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().AddOtherInterviewer(InterviewPanel);
        }
        InterviewPanel IFacade.InterviewPanel_Update(InterviewPanel InterviewPanel)
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().Update(InterviewPanel);
        }
        InterviewPanel IFacade.InterviewPanel_UpdateOtherInterviewer(InterviewPanel InterviewPanel)
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().UpdateOtherInterviewer(InterviewPanel);
        }
        InterviewPanel IFacade.InterviewPanel_GetById(int id)
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().GetById(id);
        }
        IList<InterviewPanel> IFacade.InterviewPanel_GetAll()
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().GetAll();
        }
        IList<InterviewPanel> IFacade.InterviewPanel_GetAll(string SortExpression)
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().GetAll(SortExpression);
        }
        PagedResponse<InterviewPanel> IFacade.InterviewPanel_GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().GetPaged(request);
        }
        bool IFacade.InterviewPanel_DeleteById(int id)
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().DeleteById(id);
        }
        bool IFacade.InterviewPanel_UpdateDeleteById(int InterviewPanelids)
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().UpdateDeleteById(InterviewPanelids);
        }
        bool IFacade.Interview_ChkEmailId(int InterviewerId, int panelid)
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().ChkEmailId(InterviewerId, panelid);
        }
        bool IFacade.Interview_ClientChkEmailId(int InterviewerId, int panelid)
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().ClientChkEmailId(InterviewerId, panelid);
        }
        IList<InterviewPanel> IFacade.InterviewPanel_OtherChkEmailId(int panelid)
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().OtherChkEmailId(panelid);
        }
        IList<InterviewPanel> IFacade.InterviewSchedule_OtherChkEmailId(int RequisitionId)
        {
            return DataAccessFactory.CreateInterviewPanelDataAccess().ScheduleOtherChkEmailId(RequisitionId);
        }

        #endregion

        //*************end******************************************



        #region InterviewResponse
        void IFacade.InterviewResponse_Add(InterviewResponse InterviewResponse)
        {
            DataAccessFactory.CreateInterviewResponseDataAccess().Add(InterviewResponse);
        }
        void IFacade.InterviewResponse_Update(InterviewResponse InterviewResponse)
        {
            DataAccessFactory.CreateInterviewResponseDataAccess().Update(InterviewResponse);
        }
        InterviewResponse IFacade.InterviewResponse_GetById(int id)
        {
            return DataAccessFactory.CreateInterviewResponseDataAccess().GetById(id);
        }
        IList<InterviewResponse> IFacade.InterviewResponse_GetAll()
        {
            return DataAccessFactory.CreateInterviewResponseDataAccess().GetAll();
        }
        PagedResponse<InterviewResponse> IFacade.InterviewResponse_GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateInterviewResponseDataAccess().GetPaged(request);
        }
        void IFacade.InterviewResponse_DeleteById(int id)
        {
            DataAccessFactory.CreateInterviewFeedbackDataAccess().DeleteById(id);
        }
        IList<InterviewResponse> IFacade.InterviewResponse_GetByInterviewId(int InterviewId)
        {
            return DataAccessFactory.CreateInterviewResponseDataAccess().GetByInterviewId(InterviewId);
        }

        IList<InterviewResponse> IFacade.InterviewResponse_GetByInterviewId_Email(int InterviewId, string Email)
        {
            return DataAccessFactory.CreateInterviewResponseDataAccess().GetByInterviewId_Email(InterviewId, Email);
        }

        //IList<GenericLookup> IFacade.InterviewAssessment_GetBy_InterviewId_InterviewerEmail(int InterviewId, string Email)
        //{
        //    return DataAccessFactory.CreateInterviewResponseDataAccess().InterviewAssessment_GetBy_InterviewId_InterviewerEmail(InterviewId, Email);
        //}



        #endregion


        #region QuestionBank
        QuestionBank IFacade.QuestionBank_Add(QuestionBank QuestionBank)
        {
            return DataAccessFactory.CreateQuestionBankDataAccess().Add(QuestionBank);
        }
        QuestionBank IFacade.QuestionBank_Update(QuestionBank QuestionBank)
        {
            return DataAccessFactory.CreateQuestionBankDataAccess().Update(QuestionBank);
        }
        QuestionBank IFacade.QuestionBank_GetById(int id)
        {
            return DataAccessFactory.CreateQuestionBankDataAccess().GetById(id);
        }
        IList<QuestionBank> IFacade.QuestionBank_GetAll()
        {
            return DataAccessFactory.CreateQuestionBankDataAccess().GetAll();
        }
        IList<QuestionBank> IFacade.QuestionBank_GetAll(string SortExpression)
        {
            return DataAccessFactory.CreateQuestionBankDataAccess().GetAll(SortExpression);
        }
        PagedResponse<QuestionBank> IFacade.QuestionBank_GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateQuestionBankDataAccess().GetPaged(request);
        }
        bool IFacade.QuestionBank_DeleteById(int id)
        {
            return DataAccessFactory.CreateQuestionBankDataAccess().DeleteById(id);
        }

        #endregion

        //*********************END*************************

        #region JobPosting

        ArrayList IFacade.JobPosting_GetAllEmployeeReferralEnabledByReferrerId(int ReferrerID)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetJobPostingIdByReferrerIdForERPortal(ReferrerID);
        }

        JobPosting IFacade.AddJobPosting(JobPosting jobPosting)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().Add(jobPosting);
        }

        JobPosting IFacade.UpdateJobPosting(JobPosting jobPosting)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().Update(jobPosting);
        }

        void IFacade.UpdateJobPostingStatusById(int statusid, int jobpostingid, int updatorid)
        {
             DataAccessFactory .CreateJobPostingDataAccess ().UpdateStatusById (statusid ,jobpostingid ,updatorid );
        }
        JobPosting IFacade.GetJobPostingById(int id)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetById(id);
        }
        //*********************Code added by pravin khot on 18/Jan/2016*******************
        JobPosting IFacade.CareerJobId_Add(JobPosting jobPosting)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().AddCareerJob(jobPosting);
        }
        //************************************End******************************************


        IList<JobPosting> IFacade.GetAllJobPosting()
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAll();
        }

        IList<JobPosting> IFacade.GetAllRequisitionByEmployeeId(int memberId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllRequisitionByEmployeeId(memberId);
        }

        IList<JobPosting> IFacade.GetAllJobPostingByMemberId(int memberId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllByMemberId(memberId);
        }
        //added by pravin khot on 8/June/2016***********
        ArrayList IFacade.GetAllJobPostingByCandidatesId(int ClientId, int CandidateId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllByCandidatesId(ClientId, CandidateId);
        }
        //***********************END***********************

        IList<JobPosting> IFacade.GetAllJobPostingByJobCartAlertId(int jobCartAlertId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllByJobCartAlertId(jobCartAlertId);
        }

        IList<JobPosting> IFacade.GetAllJobPostingByProjectId(int projectId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllByProjectId(projectId);
        }

        //starts  0.15
        IList<JobPosting> IFacade.GetAllJobPostingByProjectId(int projectId, string sortExpression)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllByProjectId(projectId, sortExpression);
        }
        //ends  0.15

        PagedResponse<JobPosting> IFacade.GetPagedJobPosting(PagedRequest request)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetPaged(request);
        }
        PagedResponse<JobPosting> IFacade.GetPagedJobPostingWithCandidateCount(PagedRequest request)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetPagedWithCandidateCount(request);
        }
        PagedResponse<JobPosting> IFacade.GetPagedJobPosting(string allKeys, string anyKey, string jobTitle, string jobType, string city, string lastUpdateDate, int jobStatus, string jobIndustry,PagedRequest request)  // 8971
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetPaged(allKeys, anyKey, jobTitle, jobType, city, lastUpdateDate, jobStatus,jobIndustry,request);
        }

        bool IFacade.DeleteJobPostingById(int id, int currentMemberId)
        {
            //6. Should deletion of requisitions be prevented except for approved uesrs?
            ApplicationWorkflow applicationWorkflow = DataAccessFactory.CreateApplicationWorkflowDataAccess().GetByTitle(WorkFlowTitle.RequisitionManagementDeletionRequisitionsBePreventedExceptForApprovedUesrs.ToString());
            if (applicationWorkflow != null && applicationWorkflow.RequireApproval)
            {
                IList<ApplicationWorkflowMap> applicationWorkflowMapList = DataAccessFactory.CreateApplicationWorkflowMapDataAccess().GetAllByApplicationWorkflowId(applicationWorkflow.Id);
                if (applicationWorkflowMapList != null)
                {
                    foreach (ApplicationWorkflowMap applicationWorkflowMap in applicationWorkflowMapList)
                    {
                        if (applicationWorkflowMap.MemberId == currentMemberId)
                        {
                            return DataAccessFactory.CreateJobPostingDataAccess().DeleteById(id);
                        }
                    }
                }
            }
            else
            {
                return DataAccessFactory.CreateJobPostingDataAccess().DeleteById(id);
            }
            return false;
        }

        JobPosting IFacade.CreateJobPostingFromExistingJob(int id, string jobPostingCode,int creatorId) //0.2
        {
            return DataAccessFactory.CreateJobPostingDataAccess().CreateFromExistingJob(id, jobPostingCode, creatorId); //0.2 & 2.6
        }

        JobPosting IFacade.CreateProjectJobFromExistingTemplateJob(int projectId, int jobId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().CreateProjectJobFromExistingTemplateJob(projectId, jobId);
        }

        ArrayList IFacade.GetAllJobPostingByStatus(int status)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllByStatus(status);
        }

        ArrayList IFacade.GetAllJobPostingListByStatusId(int statusId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllByStatusId (statusId);
        }

        ArrayList IFacade.GetAllJobPostingByClientId(int ClientId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllByCleintId (ClientId );
        }

        ArrayList IFacade.GetJobPostingByClientIdAndManagerId(int ClientId, int ManagerId, int count, string SearchKey)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetByCleintIdAndManagerId (ClientId,ManagerId ,count ,SearchKey );
        }
        ArrayList IFacade.GetAllJobPostingByClientIdAndManagerId(int ClientId, int ManagerId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllByCleintIdAndManagerId (ClientId, ManagerId);
        }

        ArrayList IFacade.GetAllJobPostingByInterview()
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllByInterview();
        }

        DataTable IFacade.GetAllJobPostingListByStatus(int status)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllJobPostingByStatus(status);
        }

        ArrayList IFacade.GetAllJobPostingByStatusAndManagerId(int status, int managerId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllByStatusAndManagerId(status, managerId);
        }

        ArrayList IFacade.GetAllJobPostingByCandidateId(int candidateId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllJobPostingByCandidateId(candidateId);
        }

        PagedResponse<JobPosting> IFacade.GetPagedJobPostingReport(PagedRequest request)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetReport(request);
        }

        //Code introduced by Prasanth On 22/May/2015 Start
        //PagedResponse<JobPosting> IFacade.GetPagedRequisitionAgingReport(string IsTemplate,string Account,string ReqStartDateFrom, string ReqEndDateTo, string Recruiter)
        PagedResponse<JobPosting> IFacade.GetPagedRequisitionAgingReport(PagedRequest request)
        {
            //return DataAccessFactory.CreateJobPostingDataAccess().GetRequisitionAgingReport(IsTemplate,Account, ReqStartDateFrom, ReqEndDateTo, Recruiter);
            return DataAccessFactory.CreateJobPostingDataAccess().GetRequisitionAgingReport(request);
        }
        //*********************END************************

        //2.8
        PagedResponse<Submission> IFacade.GetPagedSubmissionReport(PagedRequest request)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetSubmissionReport(request);
        }

        PagedResponse<Submission> IFacade.GetPagedDashBoardSubmission(PagedRequest request)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetDashBoardSubmission(request);
        }

        PagedResponse<JobPosting> IFacade.GetPagedVolumeHireJobPosting(string allKeys, string anyKey, string jobTitle, string jobType, string city, string lastUpdateDate, string volumeHire, int jobStatus, string jobIndustry, PagedRequest request)//9811
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetPagedVolumeHire(allKeys, anyKey, jobTitle, jobType, city, lastUpdateDate, volumeHire, jobStatus,jobIndustry,request);
        }

        IList<JobPosting> IFacade.GetAllJobPostingVolumeHireByStatus(int jobStatus)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllVolumeHireByStatus(jobStatus);
        }
        //***************************Code added by pravin khot on 29/Jan/2016**********************
        ArrayList IFacade.GetAllJobPostingByBUId(int BUId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllByBUID(BUId);
        }
        //***********************************End********************************************
        //Code introduced by Pravin khot On 2/Feb/2016 Start
        PagedResponse<JobPosting> IFacade.GetPagedRequisitionSourceBreakupReport(PagedRequest request)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetRequisitionSourceBreakupReport(request);
        }
        //*********************END************************
        int IFacade.GetLastRequisitionCodeByMemberId(int MemberId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetLastRequisitionCodeByMemberId(MemberId);
        }


        int IFacade.GetRequisitionCountByMemberId(int MemberId)     //0.4
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetRequisitionCountByMemberId(MemberId);
        }

        int IFacade.UpdateMemberRequisitionCount(int MemberId, int RequisitionCount)        //0.4
        {
            return DataAccessFactory.CreateJobPostingDataAccess().UpdateMemberRequisitionCount(MemberId, RequisitionCount);
        }




        int IFacade.GetCountOfJobPostingByStatusAndManagerId(int ManagerId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetCountOfJobPostingByStatusAndManagerId(ManagerId);
        }
        //[OperationContract]
        ArrayList  IFacade.GetPagedJobPostingByStatusAndManagerId(int status, int ManagerId, int count, string SearchKey)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetPagedJobPostingByStatusAndManagerId(status, ManagerId, count,SearchKey );
        }

        int IFacade.GetCountOfJobPostingByStatus()
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetCountOfJobPostingByStatus();
        }
        JobPosting IFacade.GetJobPostingByMemberIdAndJobPostingCode(int MemberId, string JobPostingCode)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetByMemberIdAndJobPostingCode(MemberId, JobPostingCode);
        }


        string IFacade.JobPosting_GetRequiredSkillNamesByJobPostingID(int JobPostingID)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetJobPosting_RequiredSkillNamesByJobPostingId(JobPostingID);
        }

        string  IFacade.GetJobPostingIdByJobPostingCode(string JobPostingCode)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetJobPostingIdByJobPostingCode(JobPostingCode);
        }
        PagedResponse<JobPosting> IFacade.GetPagedForCandidatePortal(PagedRequest request, int MemberId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetPagedForCandidatePortal(request, MemberId);
        }

        PagedResponse<JobPosting> IFacade.GetPagedForVendorPortal(PagedRequest request,int VendorId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetPagedForVendorPortal (request,VendorId);
        }
        PagedResponse<JobPosting> IFacade.GetPagedForVendorPortalForMyPerformance(PagedRequest request, int VendorId,int MemberId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetPagedForVendorPortalForMyPerformance(request, VendorId, MemberId);
        }
        //****************************Code added by pravin khot on 8/Jan/2016*************************************

        IList<JobPosting> IFacade.MemberCareerAllOpportunities()
        {
            return DataAccessFactory.CreateJobPostingDataAccess().MemberCareerOpportunities();
        }
        //************************************End******************************************


        PagedResponse<JobPosting> IFacade.JobPosting_GetPagedEmployeeReferal(PagedRequest request)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetPagedEmployeeReferal(request);
        }

        ArrayList IFacade.JobPosting_GetAllEmployeeReferralEnabled()
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllEmployeeReferralEnabled();
        }

        ArrayList IFacade.JobPosting_GetAllVendorPortalEnabled()
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllVendorPortalEnabled();
        }
        //*****************Code added by pravin khot on 25/May/2016
        Int32 IFacade.GetRequisitionStatusBy_Id(int CurrentJobPostingId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetRequisitionStatusBy_Id(CurrentJobPostingId);
        }
        string  IFacade.GetRequisitionStatus_Id(int CurrentJobPostingId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetRequisitionStatus_Id(CurrentJobPostingId);
        }
        Int32 IFacade.GetTimeZoneIdBy_MemberId(int MemberId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetTimeZoneId_MemberId(MemberId);
        }
        //************************END*********************************
        //*****************Code added by pravin khot on 8/Aug/2016
        ArrayList IFacade.GetAllBUContactIdByBUId(int BUId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllBUId(BUId);
        }
        //************************END*********************************

        ArrayList IFacade.GetAllJobPostingListBymemberId(int memberId)
        {
            return DataAccessFactory.CreateJobPostingDataAccess().GetAllBymemberId(memberId);
        }
        #endregion

        #region JobPostingSearchAgent

        JobPostingSearchAgent IFacade.AddJobPostingSearchAgent(JobPostingSearchAgent jobPosting)
        {
            return DataAccessFactory.CreateJobPostingSearchAgentDataAccess().Add(jobPosting);
        }

        JobPostingSearchAgent IFacade.UpdateJobPostingSearchAgent(JobPostingSearchAgent jobPosting)
        {
            return DataAccessFactory.CreateJobPostingSearchAgentDataAccess().Update(jobPosting);
        }

        JobPostingSearchAgent IFacade.GetJobPostingSearchAgentById(int id)
        {
            return DataAccessFactory.CreateJobPostingSearchAgentDataAccess().GetById(id);
        }

        bool IFacade.DeleteJobPostingSearchAgentByJobPostingId(int JobPostingId)
        {
            return DataAccessFactory.CreateJobPostingSearchAgentDataAccess().DeleteByJobPostingId(JobPostingId );
        }

        void IFacade.UpdateJobPostingSearchAgentByJobPostingIdAndIsRemoved(int JobPostingId, bool IsRemoved)
        {
            DataAccessFactory.CreateJobPostingSearchAgentDataAccess().UpdateByJobPostingIdAndIsRemoved(JobPostingId, IsRemoved);
        }

        JobPostingSearchAgent IFacade.GetJobPostingSearchAgentByJobPostingId(int JobPostingId)
        {
            return DataAccessFactory.CreateJobPostingSearchAgentDataAccess().GetByJobPostingId (JobPostingId );
        }

        #endregion

        #region JobPostingDocument

        JobPostingDocument IFacade.AddJobPostingDocument(JobPostingDocument jobPostingDocument)
        {
            return DataAccessFactory.CreateJobPostingDocumentDataAccess().Add(jobPostingDocument);
        }

        JobPostingDocument IFacade.UpdateJobPostingDocument(JobPostingDocument jobPostingDocument)
        {
            return DataAccessFactory.CreateJobPostingDocumentDataAccess().Update(jobPostingDocument);
        }

        JobPostingDocument IFacade.GetJobPostingDocumentById(int id)
        {
            return DataAccessFactory.CreateJobPostingDocumentDataAccess().GetById(id);
        }

        IList<JobPostingDocument> IFacade.GetAllJobPostingDocumentByJobPostingId(int jobPostingId)
        {
            return DataAccessFactory.CreateJobPostingDocumentDataAccess().GetAllByJobPostingId(jobPostingId);
        }

        IList<JobPostingDocument> IFacade.GetAllJobPostingDocument()
        {
            return DataAccessFactory.CreateJobPostingDocumentDataAccess().GetAll();
        }

        bool IFacade.DeleteJobPostingDocumentById(int id)
        {
            return DataAccessFactory.CreateJobPostingDocumentDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteJobPostingDocumentByJobPostingId(int jobPostingId)
        {
            return DataAccessFactory.CreateJobPostingDocumentDataAccess().DeleteByJobPostingId(jobPostingId);
        }

      
        #endregion

        #region JobPostingHiringTeam

        void IFacade.AddMultipleJobPostingHiringTeam(string MemberIds, int JobpostingId, int CreatorId, string employeeType)
        {
            DataAccessFactory.CreateJobPostingHiringTeamDataAccess().AddMultipleRecruiters(MemberIds, JobpostingId, CreatorId, employeeType);
        }
        //Added by pravin khot on 2/March/2017
        void IFacade.AddMultipleJobPostingHiringTeamWithOpening(int MemberId, int AssignOpenings, int JobpostingId, int CreatorId, string employeeType, bool IsPrimary)
        {
            DataAccessFactory.CreateJobPostingHiringTeamDataAccess().AddMultipleRecruitersWithOpening(MemberId, AssignOpenings, JobpostingId, CreatorId, employeeType, IsPrimary);
        }

        IList<JobPostingHiringTeam> IFacade.GetAllJobPostingRecruiterGroupByJobPostingId(int jobPostingId)
        {
            return DataAccessFactory.CreateJobPostingHiringTeamDataAccess().GetAllRecruiterGroupByJobPostingId(jobPostingId);
        }
        //******************END*************************
        JobPostingHiringTeam IFacade.AddJobPostingHiringTeam(JobPostingHiringTeam jobPostingHiringTeam)
        {
            return DataAccessFactory.CreateJobPostingHiringTeamDataAccess().Add(jobPostingHiringTeam);
        }

        JobPostingHiringTeam IFacade.UpdateJobPostingHiringTeam(JobPostingHiringTeam jobPostingHiringTeam)
        {
            return DataAccessFactory.CreateJobPostingHiringTeamDataAccess().Update(jobPostingHiringTeam);
        }

        JobPostingHiringTeam IFacade.GetJobPostingHiringTeamById(int id)
        {
            return DataAccessFactory.CreateJobPostingHiringTeamDataAccess().GetById(id);
        }

        JobPostingHiringTeam IFacade.GetJobPostingHiringTeamByMemberId(int memberId, int jobPostingId)
        {
            return DataAccessFactory.CreateJobPostingHiringTeamDataAccess().GetByMemberId(memberId, jobPostingId);
        }

        IList<JobPostingHiringTeam> IFacade.GetAllJobPostingHiringTeam()
        {
            return DataAccessFactory.CreateJobPostingHiringTeamDataAccess().GetAll();
        }

        IList<JobPostingHiringTeam> IFacade.GetAllJobPostingHiringTeamByJobPostingId(int jobPostingId)
        {
            return DataAccessFactory.CreateJobPostingHiringTeamDataAccess().GetAllByJobPostingId(jobPostingId);
        }

        bool IFacade.DeleteJobPostingHiringTeamById(int id)
        {
            return DataAccessFactory.CreateJobPostingHiringTeamDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteJobPostingHiringTeamByJobPostingId(int jobPostingId)
        {
            return DataAccessFactory.CreateJobPostingHiringTeamDataAccess().DeleteByJobPostingId(jobPostingId);
        }

        IList<JobPostingHiringTeam> IFacade.GetAllJobPostingHiringTeamByMemberId(int memberId)
        {
            return DataAccessFactory.CreateJobPostingHiringTeamDataAccess().GetAllByMemberId(memberId);
        }

        #endregion

        #region JobPostingNote

        JobPostingNote IFacade.AddJobPostingNote(JobPostingNote jobPostingNote)
        {
            return DataAccessFactory.CreateJobPostingNoteDataAccess().Add(jobPostingNote);
        }

        JobPostingNote IFacade.GetJobPostingNoteById(int id)
        {
            return DataAccessFactory.CreateJobPostingNoteDataAccess().GetById(id);
        }

        IList<JobPostingNote> IFacade.GetAllJobPostingNote()
        {
            return DataAccessFactory.CreateJobPostingNoteDataAccess().GetAll();
        }

        IList<JobPostingNote> IFacade.GetAllJobPostingNoteByJobPostingId(int jobPostingId)
        {
            return DataAccessFactory.CreateJobPostingNoteDataAccess().GetAllByJobPostingId(jobPostingId);
        }

        PagedResponse<RequisitionNotesEntry> IFacade.GetAllReqNotesByJobPostingId(PagedRequest page)
        {
            return DataAccessFactory.CreateReqNotesDataAccess().GetAllReqNotesById(page);
        }

        bool IFacade.DeleteJobPostingNoteById(int id)
        {
            return DataAccessFactory.CreateJobPostingNoteDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteJobPostingNoteByJobPostingId(int jobPostingId)
        {
            return DataAccessFactory.CreateJobPostingNoteDataAccess().DeleteByJobPostingId(jobPostingId);
        }

        #endregion

        #region JobPostingSkillSet

        JobPostingSkillSet IFacade.AddJobPostingSkillSet(JobPostingSkillSet jobPostingSkillSet)
        {
            return DataAccessFactory.CreateJobPostingSkillSetDataAccess().Add(jobPostingSkillSet);
        }

        JobPostingSkillSet IFacade.UpdateJobPostingSkillSet(JobPostingSkillSet jobPostingSkillSet)
        {
            return DataAccessFactory.CreateJobPostingSkillSetDataAccess().Update(jobPostingSkillSet);
        }

        JobPostingSkillSet IFacade.GetJobPostingSkillSetById(int id)
        {
            return DataAccessFactory.CreateJobPostingSkillSetDataAccess().GetById(id);
        }

        JobPostingSkillSet IFacade.GetJobPostingSkillSetBySkillId(int skillId)
        {
            return DataAccessFactory.CreateJobPostingSkillSetDataAccess().GetBySkillId(skillId);
        }

        JobPostingSkillSet IFacade.GetJobPostingSkillSetByJobPostingIdAndSkillId(int jobPostingId, int skillId)
        {
            return DataAccessFactory.CreateJobPostingSkillSetDataAccess().GetByJobPostingIdAndSkillId(jobPostingId, skillId);
        }

        IList<JobPostingSkillSet> IFacade.GetAllJobPostingSkillSet()
        {
            return DataAccessFactory.CreateJobPostingSkillSetDataAccess().GetAll();
        }

        bool IFacade.DeleteJobPostingSkillSetById(int id)
        {
            return DataAccessFactory.CreateJobPostingSkillSetDataAccess().DeleteById(id);
        }
        // 0.1 starts here
        bool IFacade.DeleteJobPostingSkillSetByJobPostingId(int id)
        {
            return DataAccessFactory.CreateJobPostingSkillSetDataAccess().DeleteByJobPostingID(id);
        }
        // 0.1 ends here

        IList<JobPostingSkillSet> IFacade.GetAllJobPostingSkillSetByJobPostingId(int jobPostingId)
        {
            return DataAccessFactory.CreateJobPostingSkillSetDataAccess().GetAllByJobPostingId(jobPostingId);
        }

        #endregion

        #region Job & Schedule

        bool IFacade.CreateJobSchedule(string JobType, string SqlCommandText, DateTime StartDateTime, DateTime EndDateTime, bool Active, string DbConnectionString)
        {
            return TPS360.Automation.Facade.CreateJobSchedule(JobType, SqlCommandText, StartDateTime, EndDateTime, Active, DbConnectionString);
        }

        DataTable IFacade.GetAllJobSchedule(string DbConnectionString)
        {
            return TPS360.Automation.Facade.GetAllJobSchedule(DbConnectionString);
        }

        bool IFacade.DeleteJobSchedule(string JobId, string DbConnectionString)
        {
            return TPS360.Automation.Facade.DeleteJobSchedule(JobId, DbConnectionString);
        }

        bool IFacade.UpdateJobSchedule(string JobId, string JobType, string SqlCommandText, DateTime StartDateTime, DateTime EndDateTime, bool Active, string DbConnectionString)
        {
            return TPS360.Automation.Facade.UpdateJobSchedule(JobId, JobType, SqlCommandText, StartDateTime, EndDateTime, Active, DbConnectionString);
        }

        bool IFacade.ChangeConfiguration(bool enable, string DbConnectionString)
        {
            return TPS360.Automation.Facade.ChangeConfiguration(enable, DbConnectionString);
        }


        #endregion

        #region MailQueue
        int IFacade.AddMailInMailQueue(MailQueue mailqueue) 
        {
            return DataAccessFactory.CreateMailQueueDataAccess().Add(mailqueue);
        }
        IList<MailQueue> IFacade.GetAllMailInQueue()
        {
            return DataAccessFactory.CreateMailQueueDataAccess().GetAllMailInQueue();
        }

        bool IFacade.DeleteMailQueueById(int id,out int MemberEmailId)
        {
            return DataAccessFactory.CreateMailQueueDataAccess().DeleteMailQueueById(id, out MemberEmailId);
        }
        void IFacade.UpdateFileNamesInMailQueue(string fileNames, int mailQueueId,int NoOfAttachments)
        {
            DataAccessFactory.CreateMailQueueDataAccess().UpdateFileNamesInMailQueue(fileNames, mailQueueId, NoOfAttachments);
        }
        void IFacade.UpdateMailbodyForInterviewTemplate(string EmailBody, int MemberEmailId) 
        {
            DataAccessFactory.CreateMailQueueDataAccess().UpdateMailbodyForInterviewTemplate(EmailBody, MemberEmailId);
        }
        #endregion

        #region Member


        Member IFacade.GetMemberByMemberEmail(string PrimaryEmail)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetMemberByMemberEmail(PrimaryEmail);
        }
        //Code introduced by Prasanth on 10/Jun/2016 Start
        Member IFacade.GetMemberByMemberUserName(string UserName)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetMemberByMemberUserName(UserName);
        }

        //***************END**************************

        Member IFacade.AddMember(Member member)
        {
            member = DataAccessFactory.CreateMemberDataAccess().Add(member);
            return member;

        }
        //**************Code added by pravin khot on 27/April/2016****************
        Member IFacade.UpdateMemberthroughvendor(Member member)
        {
            member = DataAccessFactory.CreateMemberDataAccess().UpdateMemberthroughvendor(member);
            return member;
        }
        string IFacade.CandidateAvailableInRequisition_Name(int MemberId)
        {
            return DataAccessFactory.CreateMemberDataAccess().CandidateAvailableInRequisition_Name(MemberId);
        }
        string IFacade.CandidateAvailableInRequisition_Name(int MemberId, int CurrentJobPostingId)
        {
            return DataAccessFactory.CreateMemberDataAccess().CandidateAvailableInRequisition_Name(MemberId, CurrentJobPostingId);
        }
        //************************END*************************************
        Member IFacade.AddFullMemberInfo(Member member, bool IsCreateMemberDetailandExtendedInfo, int Availability, bool IsCreateMemberManager)
        {
            member = DataAccessFactory.CreateMemberDataAccess().AddFullInfo(member,IsCreateMemberDetailandExtendedInfo,Availability,IsCreateMemberManager);
            return member;
        }

        ArrayList IFacade.GetLicenseKeybyId(string intLicenseKey, string varchrDomainName)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetLicenseKey(intLicenseKey, varchrDomainName);
        }//0.19

        Member IFacade.UpdateMember(Member member)
        {
            member = DataAccessFactory.CreateMemberDataAccess().Update(member);
            return member;
        }

        Member IFacade.UpdateMemberStatus(Member member)
        {
            return DataAccessFactory.CreateMemberDataAccess().Update(member);
        }

        Member IFacade.UpdateMemberResumeSharing(Member member)
        {
            return DataAccessFactory.CreateMemberDataAccess().Update(member);
        }

        void IFacade.UnSubscribeMe(int memberId)
        {
            DataAccessFactory.CreateMemberDataAccess().UnSubsciribeMemberFromBulkEmail(memberId);
        }

        Member IFacade.GetMemberById(int id)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetById(id);
        }

        //****************Code added by pravin khot on 23/Feb/2016****************
        void IFacade.DeleteRoleRequisitionDenial()
        {
            DataAccessFactory.CreateMemberDataAccess().DeleteRoleRequisition();
        }

        void IFacade.UpdateRoleRequisitionDenial(int RoleId, int MemberId, int sectionid)
        {
            DataAccessFactory.CreateMemberDataAccess().UpdateRoleRequisition(RoleId, MemberId, sectionid);
        }

        IList<CustomRole> IFacade.GetCustomRoleRequisition(int sectionid)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetCustomRoleRequisitiondetail(sectionid);
        }
        //********************************END****************************************
        /* 2.5 start*/
        string IFacade.GetMemberUserNameById(int id)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetMemberUserNameById(id);
        }
        //Function introduced by prasanth on 10/Jun/2016
        string IFacade.GetMemberADUserNameById(int id)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetMemberADUserNameById(id);
        }
        string IFacade.GetMemberNameById(int id)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetMemberNameById(id);
        }
        /* 2.5 End*/
        Member IFacade.GetMemberByUserId(Guid userId)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetByUserId(userId);
        }

        Member IFacade.GetMemberByUserName(string userName)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetByUserName(userName);
        }

        IList<Member> IFacade.GetAllMember()
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAll();
        }

        IList<Member> IFacade.GetAllMemberByJobPostingId(int jobPostingId)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAllByJobPostingId(jobPostingId);
        }

        IList<Member> IFacade.GetAllMemberDetailsByInterviewId(int InterviewId, int MemberId)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAllMemberDetailsByInterviewId(InterviewId, MemberId);
        }
        IList<Member> IFacade.GetAllMemberByCustomrRoleId(int customRoleId)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAllByCustomRoleId(customRoleId);
        }

        IList<Member> IFacade.GetAllMemberByCreatorIdAndRoleName(int creatorId, string roleName)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAllByCreatorIdAndRoleName(creatorId, roleName);
        }

        IList<Member> IFacade.GetAllMemberByMemberGroupId(int memberGroupId)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAllMemberByMemberGroupId(memberGroupId);
        }

        IList<Member> IFacade.GetAllMemberGroupManagerByMemberGroupId(int memberGroupId)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAllMemberGroupManagerByMemberGroupId(memberGroupId);
        }

        PagedResponse<Member> IFacade.GetPagedMember(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetPaged(request);
        }

        PagedResponse<Candidate> IFacade.GetPagedMemberByMemberGroupId(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetPagedByMemberGroupId(request);
        }

        PagedResponse<Member> IFacade.GetPagedMemberByCreatorIdAndRoleName(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetPagedByRoleNameAndCreatorId(request);
        }

        bool IFacade.DeleteMemberById(int id)
        {
            return DataAccessFactory.CreateMemberDataAccess().DeleteById(id);
        }
        void IFacade.UpdateAspNet_User(string UserName, Guid UserId)
        {
            DataAccessFactory.CreateMemberDataAccess().UpdateAspNet_User(UserName, UserId);
        }
        ArrayList IFacade.GetAllMemberNameByRoleName(string roleName)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAllNameByRoleName(roleName);
        }
        
        ArrayList IFacade.GetAllMemberNameWithEmailByRoleName(string roleName)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAllNameWithEmailByRoleName(roleName);
        }
        //****************Code added by pravin khot on 7/March/2016*************
        ArrayList IFacade.GetAllMemberNameWithEmailByRoleNameIfExisting(string roleName)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAllNameWithEmailByRoleNameIfExisting(roleName);
        }
        //********************************END*****************************************
        IList<Member> IFacade.GetAllMemberByIds(string Ids)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAllByIds(Ids);
        }

        string IFacade.GetNewMermberIdentityId()
        {
            return DataAccessFactory.CreateMemberDataAccess().GetNewMermberIdentityId();
        }

        Int32 IFacade.GetMemberIdByMemberEmail(string Email)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetMemberIdByMemberEmail(Email);
        }
        //------------------------CODE INTRODUCED BY PRAVIN KHOT 09/Dec/2015----------------
        ArrayList IFacade.GetAllSuggestedInterviewerWithEmail(int RequisitionId)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAllSuggestedInterviewer(RequisitionId);
        }
        //---------------------------------end-----------------------------------------

        //------------------------CODE INTRODUCED BY PRAVIN KHOT 02/Dec/2015----------------
        ArrayList IFacade.GetAllMemberNameWithEmailByRoleName_IP(string roleName, int paneltypeid)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAllNameWithEmailByRoleName_IP(roleName, paneltypeid);
        }
        //---------------------------------end-----------------------------------------

        Int32 IFacade.GetMemberIdByEmail(string Email)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetMemberIdByEmail(Email);
        }

        Int32 IFacade.GetCreatorIdForMember(int id)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetCreatorIdForMember(id);
        }

        ArrayList IFacade.GetAllEmployeeNameWithEmail(string role)
        {
            return DataAccessFactory .CreateMemberDataAccess ().GetAllEmployeeNameWithEmail (role );
        }
        int IFacade.GetMemberStatusByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetMemberStatusByMemberId(memberId);
        }
        int IFacade.GetMemberIdByCompanyContact(int CompanyContactId) 
        {
            return DataAccessFactory.CreateMemberDataAccess().GetMemberIdByCompanyContact(CompanyContactId);
        }
        //*****************Code added by pravin khot on 16/May/2016*************
        ArrayList IFacade.GetAllMemberNameByTeamMemberId(string roleName, int memberid)
        {
            return DataAccessFactory.CreateMemberDataAccess().GetAllNameByTeamMemberId(roleName, memberid);
        }
       
        //****************************END****************************
        #endregion

        #region MemberActivity

        MemberActivity IFacade.AddMemberActivity(MemberActivity memberActivity)
        {
            return DataAccessFactory.CreateMemberActivityDataAccess().Add(memberActivity);
        }

        MemberActivity IFacade.UpdateMemberActivity(MemberActivity memberActivity)
        {
            return DataAccessFactory.CreateMemberActivityDataAccess().Update(memberActivity);
        }

        MemberActivity IFacade.GetMemberActivityById(int id)
        {
            return DataAccessFactory.CreateMemberActivityDataAccess().GetById(id);
        }

        IList<MemberActivity> IFacade.GetAllMemberActivity()
        {
            return DataAccessFactory.CreateMemberActivityDataAccess().GetAll();
        }

        IList<MemberActivity> IFacade.GetAllMemberActivityByCreatorId(int creatorId)
        {
            return DataAccessFactory.CreateMemberActivityDataAccess().GetAllByCreatorId(creatorId);
        }

        PagedResponse<MemberActivity> IFacade.GetPagedMemberActivity(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberActivityDataAccess().GetPaged(request);
        }

        bool IFacade.DeleteMemberActivityById(int id)
        {
            return DataAccessFactory.CreateMemberActivityDataAccess().DeleteById(id);
        }

        #endregion

        #region MemberActivityType

        MemberActivityType IFacade.AddMemberActivityType(MemberActivityType memberActivityType)
        {
            return DataAccessFactory.CreateMemberActivityTypeDataAccess().Add(memberActivityType);
        }

        MemberActivityType IFacade.UpdateMemberActivityType(MemberActivityType memberActivityType)
        {
            return DataAccessFactory.CreateMemberActivityTypeDataAccess().Update(memberActivityType);
        }

        MemberActivityType IFacade.GetMemberActivityTypeById(int id)
        {
            return DataAccessFactory.CreateMemberActivityTypeDataAccess().GetById(id);
        }


        MemberActivityType IFacade.GetMemberActivityType(int ActivityType)
        {
            return DataAccessFactory.CreateMemberActivityTypeDataAccess().GetByActivityType(ActivityType);
        }

        IList<MemberActivityType> IFacade.GetAllMemberActivityType()
        {
            return DataAccessFactory.CreateMemberActivityTypeDataAccess().GetAll();
        }

        PagedResponse<MemberActivityType> IFacade.GetPagedMemberActivityType(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberActivityTypeDataAccess().GetPaged(request);
        }


        bool IFacade.DeleteMemberActivityTypeById(int id)
        {
            return DataAccessFactory.CreateMemberActivityTypeDataAccess().DeleteById(id);
        }

        #endregion

        #region MemberAlert

        MemberAlert IFacade.AddMemberAlert(MemberAlert memberAlert)
        {
            return DataAccessFactory.CreateMemberAlertDataAccess().Add(memberAlert);
        }

        MemberAlert IFacade.UpdateMemberAlert(MemberAlert memberAlert)
        {
            return DataAccessFactory.CreateMemberAlertDataAccess().Update(memberAlert);
        }

        MemberAlert IFacade.GetMemberAlertById(int id)
        {
            return DataAccessFactory.CreateMemberAlertDataAccess().GetById(id);
        }

        IList<MemberAlert> IFacade.GetAllMemberAlert()
        {
            return DataAccessFactory.CreateMemberAlertDataAccess().GetAll();
        }

        IList<MemberAlert> IFacade.GetAllMemberAlertByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberAlertDataAccess().GetAllByMemberId(memberId);
        }

        PagedResponse<MemberAlert> IFacade.GetPagedMemberAlert(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberAlertDataAccess().GetPaged(request);
        }

        bool IFacade.DeleteMemberAlertById(int id)
        {
            return DataAccessFactory.CreateMemberAlertDataAccess().DeleteById(id);
        }

        #endregion

        #region MemberAttendence

        MemberAttendence IFacade.AddMemberAttendence(MemberAttendence memberAttendence)
        {
            return DataAccessFactory.CreateMemberAttendenceDataAccess().Add(memberAttendence);
        }

        MemberAttendence IFacade.UpdateMemberAttendence(MemberAttendence memberAttendence)
        {
            return DataAccessFactory.CreateMemberAttendenceDataAccess().Update(memberAttendence);
        }

        MemberAttendence IFacade.GetMemberAttendenceById(int id)
        {
            return DataAccessFactory.CreateMemberAttendenceDataAccess().GetById(id);
        }

        MemberAttendence IFacade.GetMemberAttendenceByAttendanceDateAndMemberId(DateTime attendanceDate, int memberId)
        {
            return DataAccessFactory.CreateMemberAttendenceDataAccess().GetByAttendanceDateAndMemberId(attendanceDate, memberId);
        }

        IList<MemberAttendence> IFacade.GetAllMemberAttendence()
        {
            return DataAccessFactory.CreateMemberAttendenceDataAccess().GetAll();
        }

        IList<MemberAttendence> IFacade.GetAllMemberAttendenceByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberAttendenceDataAccess().GetAllByMemberId(memberId);
        }

        bool IFacade.DeleteMemberAttendenceById(int id)
        {
            return DataAccessFactory.CreateMemberAttendenceDataAccess().DeleteById(id);
        }

        IList<MemberAttendence> IFacade.GetMemberAttendenceByYearAndMemberId(Int32 year, Int32 memberId)
        {
            return DataAccessFactory.CreateMemberAttendenceDataAccess().GetByYearAndMemberId(year, memberId);
        }
        #endregion

        #region MemberAttribute

        MemberAttribute IFacade.AddMemberAttribute(MemberAttribute memberAttribute)
        {
            return DataAccessFactory.CreateMemberAttributeDataAccess().Add(memberAttribute);
        }

        MemberAttribute IFacade.UpdateMemberAttribute(MemberAttribute memberAttribute)
        {
            return DataAccessFactory.CreateMemberAttributeDataAccess().Update(memberAttribute);
        }

        MemberAttribute IFacade.GetMemberAttributeById(int id)
        {
            return DataAccessFactory.CreateMemberAttributeDataAccess().GetById(id);
        }

        IList<MemberAttribute> IFacade.GetAllMemberAttribute()
        {
            return DataAccessFactory.CreateMemberAttributeDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberAttributeById(int id)
        {
            return DataAccessFactory.CreateMemberAttributeDataAccess().DeleteById(id);
        }

        #endregion

        #region MemberAttendenceYearlyReport

        IList<MemberAttendenceYearlyReport> IFacade.GetMemberAttendenceByYearlyReport(Int32 year, Int32 memberId)
        {
            return DataAccessFactory.CreateMemberAttendenceYearlyReport().GetMemberAttendeceYearlyReport(year, memberId);
        }

        #endregion

        #region MemberCapabilityRating

        MemberCapabilityRating IFacade.AddMemberCapabilityRating(MemberCapabilityRating memberCapabilityRating)
        {
            return DataAccessFactory.CreateMemberCapabilityRatingDataAccess().Add(memberCapabilityRating);
        }

        MemberCapabilityRating IFacade.UpdateMemberCapabilityRating(MemberCapabilityRating memberCapabilityRating)
        {
            return DataAccessFactory.CreateMemberCapabilityRatingDataAccess().Update(memberCapabilityRating);
        }

        MemberCapabilityRating IFacade.GetMemberCapabilityRatingById(int id)
        {
            return DataAccessFactory.CreateMemberCapabilityRatingDataAccess().GetById(id);
        }

        IList<MemberCapabilityRating> IFacade.GetAllMemberCapabilityRating()
        {
            return DataAccessFactory.CreateMemberCapabilityRatingDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberCapabilityRatingById(int id)
        {
            return DataAccessFactory.CreateMemberCapabilityRatingDataAccess().DeleteById(id);
        }

        IList<MemberCapabilityRating> IFacade.GetAllMemberCapabilityRatingByMemberIdAndPositionId(int memberId, int positionId)
        {
            return DataAccessFactory.CreateMemberCapabilityRatingDataAccess().GetAllByMemberIdAndPositionId(memberId, positionId);
        }

        bool IFacade.DeleteMemberCapabilityRatingByMemberIdAndPositionId(int memberId, int positionId)
        {
            return DataAccessFactory.CreateMemberCapabilityRatingDataAccess().DeleteByMemberIdAndPositionId(memberId, positionId);
        }

        MemberCapabilityRating IFacade.GetMemberCapabilityRatingByMemberIdAndPositionIdAndCapabilityTypeAndPositionCapabilityMapId(int memberId, int positionId, int capabilityType, int positionCapabilityMapId)
        {
            return DataAccessFactory.CreateMemberCapabilityRatingDataAccess().GetByMemberIdAndPositionIdAndCapabilityTypeAndPositionCapabilityMapId(memberId, positionId, capabilityType, positionCapabilityMapId);
        }

        #endregion

        #region MemberCertificationMap

        MemberCertificationMap IFacade.AddMemberCertificationMap(MemberCertificationMap memberCertificationMap)
        {
            return DataAccessFactory.CreateMemberCertificationMapDataAccess().Add(memberCertificationMap);
        }

        MemberCertificationMap IFacade.UpdateMemberCertificationMap(MemberCertificationMap memberCertificationMap)
        {
            return DataAccessFactory.CreateMemberCertificationMapDataAccess().Update(memberCertificationMap);
        }

        MemberCertificationMap IFacade.GetMemberCertificationMapById(int id)
        {
            return DataAccessFactory.CreateMemberCertificationMapDataAccess().GetById(id);
        }

        IList<MemberCertificationMap> IFacade.GetAllMemberCertificationMap()
        {
            return DataAccessFactory.CreateMemberCertificationMapDataAccess().GetAll();
        }

        IList<MemberCertificationMap> IFacade.GetAllMemberCertificationMapByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberCertificationMapDataAccess().GetAllByMemberId(memberId);
        }


        bool IFacade.DeleteMemberCertificationMapById(int id)
        {
            return DataAccessFactory.CreateMemberCertificationMapDataAccess().DeleteById(id);
        }

        PagedResponse<MemberCertificationMap> IFacade.GetPagedMemberCertificationMap(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberCertificationMapDataAccess().GetPaged(request);
        }

        PagedResponse<MemberCertificationMap> IFacade.GetPagedMemberCertificationMapByMemberId(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberCertificationMapDataAccess().GetPagedByMemberId(request);
        }

        MemberCertificationMap IFacade.GetCertificationMapByCertificationName(string CertificationName)
        {
            return DataAccessFactory.CreateMemberCertificationMapDataAccess().GetByCertificationName(CertificationName);
        }

        bool IFacade.DeleteMemberCertificationMapByMemberId(int MemberId)
        {
            return DataAccessFactory.CreateMemberCertificationMapDataAccess().DeleteByMemberId(MemberId);
        }


        #endregion

        #region MemberChangeArchive

        MemberChangeArchive IFacade.AddMemberChangeArchive(MemberChangeArchive memberChangeArchive)
        {
            return DataAccessFactory.CreateMemberChangeArchiveDataAccess().Add(memberChangeArchive);
        }

        MemberChangeArchive IFacade.UpdateMemberChangeArchive(MemberChangeArchive memberChangeArchive)
        {
            return DataAccessFactory.CreateMemberChangeArchiveDataAccess().Update(memberChangeArchive);
        }

        MemberChangeArchive IFacade.GetMemberChangeArchiveById(int id)
        {
            return DataAccessFactory.CreateMemberChangeArchiveDataAccess().GetById(id);
        }

        IList<MemberChangeArchive> IFacade.GetAllMemberChangeArchiveByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberChangeArchiveDataAccess().GetAllByMemberId(memberId);
        }

        IList<MemberChangeArchive> IFacade.GetAllMemberChangeArchive()
        {
            return DataAccessFactory.CreateMemberChangeArchiveDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberChangeArchiveById(int id)
        {
            return DataAccessFactory.CreateMemberChangeArchiveDataAccess().DeleteById(id);
        }

        #endregion

        #region MemberCustomRoleMap

        MemberCustomRoleMap IFacade.AddMemberCustomRoleMap(MemberCustomRoleMap memberCustomRoleMap)
        {
            return DataAccessFactory.CreateMemberCustomRoleMapDataAccess().Add(memberCustomRoleMap);
        }

        MemberCustomRoleMap IFacade.UpdateMemberCustomRoleMap(MemberCustomRoleMap memberCustomRoleMap)
        {
            return DataAccessFactory.CreateMemberCustomRoleMapDataAccess().Update(memberCustomRoleMap);
        }

        MemberCustomRoleMap IFacade.GetMemberCustomRoleMapById(int id)
        {
            return DataAccessFactory.CreateMemberCustomRoleMapDataAccess().GetById(id);
        }

        MemberCustomRoleMap IFacade.GetMemberCustomRoleMapByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberCustomRoleMapDataAccess().GetByMemberId(memberId);
        }

        IList<MemberCustomRoleMap> IFacade.GetAllMemberCustomRoleMap()
        {
            return DataAccessFactory.CreateMemberCustomRoleMapDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberCustomRoleMapById(int id)
        {
            return DataAccessFactory.CreateMemberCustomRoleMapDataAccess().DeleteById(id);
        }

        #endregion

        #region MemberDailyReport

        MemberDailyReport IFacade.AddMemberDailyReport(MemberDailyReport memberDailyReport)
        {
            return DataAccessFactory.CreateMemberDailyReportDataAccess().Add(memberDailyReport);
        }

        MemberDailyReport IFacade.UpdateMemberDailyReport(MemberDailyReport memberDailyReport)
        {
            return DataAccessFactory.CreateMemberDailyReportDataAccess().Update(memberDailyReport);
        }

        MemberDailyReport IFacade.GetMemberDailyReportById(int id)
        {
            return DataAccessFactory.CreateMemberDailyReportDataAccess().GetById(id);
        }

        MemberDailyReport IFacade.GetMemberDailyReportByLoginTimeAndMemberId(DateTime loginTime, int memberId)
        {
            return DataAccessFactory.CreateMemberDailyReportDataAccess().GetByLoginTimeAndMemberId(loginTime, memberId);
        }

        IList<MemberDailyReport> IFacade.GetAllMemberDailyReport()
        {
            return DataAccessFactory.CreateMemberDailyReportDataAccess().GetAll();
        }

        PagedResponse<MemberDailyReport> IFacade.GetPagedMemberDailyReport(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberDailyReportDataAccess().GetPaged(request);
        }

        bool IFacade.DeleteMemberDailyReportById(int id)
        {
            return DataAccessFactory.CreateMemberDailyReportDataAccess().DeleteById(id);
        }

        bool IFacade.UpdateAllMemberDailyReportByLoginTimeAndMemberId(DateTime loginTime, int memberId)
        {
            return DataAccessFactory.CreateMemberDailyReportDataAccess().UpdateAllByLoginTimeAndMemberId(loginTime, memberId);
        }

        #endregion

        #region MemberDetail

        MemberDetail IFacade.AddMemberDetail(MemberDetail memberDetail)
        {
            return DataAccessFactory.CreateMemberDetailDataAccess().Add(memberDetail);
        }

        MemberDetail IFacade.UpdateMemberDetail(MemberDetail memberDetail)
        {
            return DataAccessFactory.CreateMemberDetailDataAccess().Update(memberDetail);
        }

        MemberDetail IFacade.GetMemberDetailById(int id)
        {
            return DataAccessFactory.CreateMemberDetailDataAccess().GetById(id);
        }

        //starts 0.17
        int IFacade.GetMemberSSNCount(string strSSN, int MemberId)
        {
            return DataAccessFactory.CreateMemberDetailDataAccess().GetSSNCount(strSSN, MemberId);
        }
        //ends 0.17

        MemberDetail IFacade.GetMemberDetailByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberDetailDataAccess().GetByMemberId(memberId);
        }

        IList<MemberDetail> IFacade.GetAllMemberDetail()
        {
            return DataAccessFactory.CreateMemberDetailDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberDetailById(int id)
        {
            return DataAccessFactory.CreateMemberDetailDataAccess().DeleteById(id);
        }


        bool IFacade.DeleteMemberDetailByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberDetailDataAccess().DeleteByMembertId(memberId);
        }

        bool IFacade.BuildMemberManager_ByMemberId(int memberId, int CreatorId)
        {
            return DataAccessFactory.CreateMemberDetailDataAccess().BuildMemberManager_ByMemberId(memberId, CreatorId);
        }
        #endregion

        #region MemberDocument

        MemberDocument IFacade.AddMemberDocument(MemberDocument memberDocument)
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().Add(memberDocument);
        }

        MemberDocument IFacade.UpdateMemberDocument(MemberDocument memberDocument)
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().Update(memberDocument);
        }

        MemberDocument IFacade.GetMemberDocumentById(int id)
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().GetById(id);
        }

        IList<MemberDocument> IFacade.GetAllMemberDocumentByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().GetAllByMemberId(memberId);
        }

        IList<MemberDocument> IFacade.GetAllMemberDocumentByTypeAndMemberId(string type, int memberId)
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().GetAllByTypeAndMemberId(type, memberId);
        }

        IList<MemberDocument> IFacade.GetAllMemberDocument()
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberDocumentById(int id)
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().DeleteById(id);
        }

        IList<MemberDocument> IFacade.GetAllMemberDocumentByTypeAndMembersId(string membersId, string documentType)
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().GetAllByTypeAndMembersId(membersId, documentType);
        }

        MemberDocument IFacade.GetMemberDocumentByMemberIdTypeAndFileName(int memberId, string Type, string fileName)
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().GetAllByMemberIDTypeAndFileName(memberId, Type, fileName);
        }


        MemberDocument IFacade.GetMemberDocumentByMemberIdAndFileName(int memberId, string fileName)
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().GetByMemberIdAndFileName(memberId, fileName);
        }

        bool IFacade.DeleteMemberDocumentByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().DeleteByMemberId(memberId);
        }

        PagedResponse<MemberDocument> IFacade.GetPagedMemberDocumentByMemberID(int MemberId, PagedRequest request)
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().GetPagedByMemberId(MemberId, request);
        }

        IList<MemberDocument> IFacade.GetLatestResumeByMemberID(int memberId)
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().GetLatestResumeByMemberID(memberId);
        }

        MemberDocument IFacade.GetRecentResumeByMemberID(int memberId)
        {
            return DataAccessFactory.CreateMemberDocumentDataAccess().GetRecentResumeDocument (memberId);
        }
        #endregion

        #region MemberEducation

        MemberEducation IFacade.AddMemberEducation(MemberEducation memberEducation)
        {
            return DataAccessFactory.CreateMemberEducationDataAccess().Add(memberEducation);
        }

        MemberEducation IFacade.UpdateMemberEducation(MemberEducation memberEducation)
        {
            return DataAccessFactory.CreateMemberEducationDataAccess().Update(memberEducation);
        }

        MemberEducation IFacade.GetMemberEducationById(int id)
        {
            return DataAccessFactory.CreateMemberEducationDataAccess().GetById(id);
        }

        //evan
        string IFacade.GetLevelofEducationByLookupId(int lookupId)
        {
            return DataAccessFactory.CreateMemberEducationDataAccess().GetLevelofEducationByLookupId(lookupId);
        }

        MemberEducation IFacade.GetMemberEducationByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberEducationDataAccess().GetByMemberId(memberId);
        }

        MemberEducation IFacade.GetHighestEducationByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberEducationDataAccess().GetHighestEducationByMemberId(memberId);
        }

        IList<MemberEducation> IFacade.GetAllMemberEducation()
        {
            return DataAccessFactory.CreateMemberEducationDataAccess().GetAll();
        }

        IList<MemberEducation> IFacade.GetAllMemberEducationByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberEducationDataAccess().GetAllMemberEducationByMemberId(memberId);
        }

        PagedResponse<MemberEducation> IFacade.GetPagedMemberEducation(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberEducationDataAccess().GetPaged(request);
        }

        PagedResponse<MemberEducation> IFacade.GetPagedMemberEducationByMemberId(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberEducationDataAccess().GetPagedByMemberId(request);
        }

        bool IFacade.DeleteMemberEducationById(int id)
        {
            return DataAccessFactory.CreateMemberEducationDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteMemberEducationByMemberId(int id)
        {
            return DataAccessFactory.CreateMemberEducationDataAccess().DeleteByMemberId(id);
        }


        #endregion

        #region MemberEmail

        MemberEmail IFacade.AddMemberEmail(MemberEmail memberEmail)
        {
            return DataAccessFactory.CreateMemberEmailDataAccess().Add(memberEmail);
        }

        MemberEmail IFacade.UpdateMemberEmail(MemberEmail memberEmail)
        {
            return DataAccessFactory.CreateMemberEmailDataAccess().Update(memberEmail);
        }

        MemberEmail IFacade.GetMemberEmailById(int id)
        {
            return DataAccessFactory.CreateMemberEmailDataAccess().GetById(id);
        }

        IList<MemberEmail> IFacade.GetByEmailAddressAndCreatedDate(string emailAddress, string createdDate)
        {
            return DataAccessFactory.CreateMemberEmailDataAccess().GetByEmailAddressAndCreatedDate(emailAddress, createdDate);
        }

        bool IFacade.ValidateSenderEmailID(string PrimaryEmail)
        {
            return DataAccessFactory.CreateMemberEmailDataAccess().ValidateSenderEmailID(PrimaryEmail);
        }
        MemberEmail IFacade.GetMemberEmailSenderByMemberIdSubjectDate(Int32 memberId, string subject, DateTime date)
        {
            return DataAccessFactory.CreateMemberEmailDataAccess().GetMemberEmailSenderByMemberIdSubjectDate(memberId, subject, date);
        }

        MemberEmail IFacade.GetMemberEmailReceiverByMemberIdSubjectDate(Int32 memberId, string subject, DateTime date)
        {
            return DataAccessFactory.CreateMemberEmailDataAccess().GetMemberEmailReceiverByMemberIdSubjectDate(memberId, subject, date);
        }

        IList<MemberEmail> IFacade.Synchronise(string fromdate, string todate, string subject, string toemail, string uid)
        {
            return DataAccessFactory.CreateMemberEmailDataAccess().Synchronise(fromdate, todate, subject, toemail, uid);
        }

        IList<MemberEmail> IFacade.GetAllMemberEmail()
        {
            return DataAccessFactory.CreateMemberEmailDataAccess().GetAll();
        }

        IList<MemberEmail> IFacade.GetByEmailAddress(string emailAddress)
        {
            return DataAccessFactory.CreateMemberEmailDataAccess().GetByEmailAddress(emailAddress);
        }

        PagedResponse<MemberEmail> IFacade.GetPagedMemberEmail(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberEmailDataAccess().GetPaged(request);
        }

        bool IFacade.DeleteMemberEmailById(int id)
        {
            return DataAccessFactory.CreateMemberEmailDataAccess().DeleteById(id);
        }

        string IFacade.GetMemberEmailTypeById(int id)
        {
            return DataAccessFactory.CreateMemberEmailDataAccess().GetMemberEmailTypeById(id);
        }

        #endregion

        #region MemberEmailAttachment

        MemberEmailAttachment IFacade.AddMemberEmailAttachment(MemberEmailAttachment memberEmailAttachment)
        {
            return DataAccessFactory.CreateMemberEmailAttachmentDataAccess().Add(memberEmailAttachment);
        }

        MemberEmailAttachment IFacade.UpdateMemberEmailAttachment(MemberEmailAttachment memberEmailAttachment)
        {
            return DataAccessFactory.CreateMemberEmailAttachmentDataAccess().Update(memberEmailAttachment);
        }

        MemberEmailAttachment IFacade.GetMemberEmailAttachmentById(int id)
        {
            return DataAccessFactory.CreateMemberEmailAttachmentDataAccess().GetById(id);
        }

        IList<MemberEmailAttachment> IFacade.GetAllMemberEmailAttachmentByMemberEmailId(int memberEmailId)
        {
            return DataAccessFactory.CreateMemberEmailAttachmentDataAccess().GetAllByMemberEmailId(memberEmailId);
        }

        IList<MemberEmailAttachment> IFacade.GetAllMemberEmailAttachment()
        {
            return DataAccessFactory.CreateMemberEmailAttachmentDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberEmailAttachmentById(int id)
        {
            return DataAccessFactory.CreateMemberEmailAttachmentDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteMemberEmailAttachmentByMemberEmailId(int memberEmailId)
        {
            return DataAccessFactory.CreateMemberEmailAttachmentDataAccess().DeleteByMemberEmailId(memberEmailId);
        }


        #endregion

        #region MemberEmailDetail

        MemberEmailDetail IFacade.AddMemberEmailDetail(MemberEmailDetail memberEmailDetail)
        {
            return DataAccessFactory.CreateMemberEmailDetailDataAccess().Add(memberEmailDetail);
        }

        MemberEmailDetail IFacade.UpdateMemberEmailDetail(MemberEmailDetail memberEmailDetail)
        {
            return DataAccessFactory.CreateMemberEmailDetailDataAccess().Update(memberEmailDetail);
        }

        MemberEmailDetail IFacade.GetMemberEmailDetailById(int id)
        {
            return DataAccessFactory.CreateMemberEmailDetailDataAccess().GetById(id);
        }

        IList<MemberEmailDetail> IFacade.GetAllMemberEmailDetailByMemberEmailId(int memberEmailId)
        {
            return DataAccessFactory.CreateMemberEmailDetailDataAccess().GetAllByMemberEmailId(memberEmailId);
        }

        IList<MemberEmailDetail> IFacade.GetAllMemberEmailDetail()
        {
            return DataAccessFactory.CreateMemberEmailDetailDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberEmailDetailById(int id)
        {
            return DataAccessFactory.CreateMemberEmailDetailDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteMemberEmailDetailByMemberEmailId(int memberEmailId)
        {
            return DataAccessFactory.CreateMemberEmailDetailDataAccess().DeleteByMemberEmailId(memberEmailId);
        }

        #endregion

        #region MemberExperience

        MemberExperience IFacade.AddMemberExperience(MemberExperience memberExperience)
        {
            return DataAccessFactory.CreateMemberExperienceDataAccess().Add(memberExperience);
        }

        MemberExperience IFacade.UpdateMemberExperience(MemberExperience memberExperience)
        {
            return DataAccessFactory.CreateMemberExperienceDataAccess().Update(memberExperience);
        }

        MemberExperience IFacade.GetMemberExperienceById(int id)
        {
            return DataAccessFactory.CreateMemberExperienceDataAccess().GetById(id);
        }

        IList<MemberExperience> IFacade.GetAllMemberExperience()
        {
            return DataAccessFactory.CreateMemberExperienceDataAccess().GetAll();
        }

        IList<MemberExperience> IFacade.GetAllMemberExperienceByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberExperienceDataAccess().GetAllByMemberId(memberId);
        }

        PagedResponse<MemberExperience> IFacade.GetPagedMemberExperience(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberExperienceDataAccess().GetPaged(request);
        }

        PagedResponse<MemberExperience> IFacade.GetPagedMemberExperienceByMemberId(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberExperienceDataAccess().GetPagedByMemberId(request);
        }

        bool IFacade.DeleteMemberExperienceById(int id)
        {
            return DataAccessFactory.CreateMemberExperienceDataAccess().DeleteById(id);
        }

        //2.3 starts here 
        bool IFacade.DeleteMemberExperienceByMemberId(int id)
        {
            return DataAccessFactory.CreateMemberExperienceDataAccess().DeleteByMemberId(id);
        }
        //end here

        #endregion

        #region MemberExperienceDetail

        MemberExperienceDetail IFacade.GetMemberExperienceDetailById(int id)
        {
            return DataAccessFactory.CreateMemberExperienceDetailDataAccess().GetById(id);
        }

        IList<MemberExperienceDetail> IFacade.GetAllMemberExperienceDetailByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberExperienceDetailDataAccess().GetAllByMemberId(memberId);
        }

        PagedResponse<MemberExperienceDetail> IFacade.GetPagedMemberExperienceDetail(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberExperienceDetailDataAccess().GetPaged(request);
        }

        #endregion

        #region MemberExtendedInformation

        MemberExtendedInformation IFacade.AddMemberExtendedInformation(MemberExtendedInformation memberExtendedInformation)
        {
            return DataAccessFactory.CreateMemberExtendedInformationDataAccess().Add(memberExtendedInformation);
        }

        MemberExtendedInformation IFacade.UpdateMemberExtendedInformation(MemberExtendedInformation memberExtendedInformation)
        {
            return DataAccessFactory.CreateMemberExtendedInformationDataAccess().Update(memberExtendedInformation);
        }

        MemberExtendedInformation IFacade.GetMemberExtendedInformationById(int id)
        {
            return DataAccessFactory.CreateMemberExtendedInformationDataAccess().GetById(id);
        }

        MemberExtendedInformation IFacade.GetMemberExtendedInformationByMemberId(int id)
        {
            return DataAccessFactory.CreateMemberExtendedInformationDataAccess().GetByMemberId(id);
        }
        //2.4 starts
        public MemberExtendedInformation GetMemberExtendedInformationByMemberId(int id)
        {
            return DataAccessFactory.CreateMemberExtendedInformationDataAccess().GetByMemberId(id);
        }
        //2.4 ends


        IList<MemberExtendedInformation> IFacade.GetAllMemberExtendedInformation()
        {
            return DataAccessFactory.CreateMemberExtendedInformationDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberExtendedInformationById(int id)
        {
            return DataAccessFactory.CreateMemberExtendedInformationDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteMemberExtendedInformationByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberExtendedInformationDataAccess().DeleteByMemberId(memberId);
        }

        void IFacade.UpdateUpdateDateByMemberId(int MemberId)
        {
            DataAccessFactory.CreateMemberExtendedInformationDataAccess().UpdateUpdateDateByMemberId(MemberId);
        }
        //***********Code added by pravin khot on 24/May/2016***********
        void IFacade.UpdateMemberManagerIsPrimary(int candidateid, int AssignManagerId)
        {
            DataAccessFactory.CreateMemberExtendedInformationDataAccess().UpdateMemberManagerIsPrimaryTrue(candidateid, AssignManagerId);
        }
        //******************************END**************************
        ////2.3
        //MemberExtendedInformation IFacade.GetMailSettingByMemberId(int memberId)
        //{
        //    return DataAccessFactory.CreateMemberExtendedInformationDataAccess().GetMailSettingByMemberId(memberId);
        //}

        #endregion

        #region MemberGroup

        MemberGroup IFacade.AddMemberGroup(MemberGroup memberGroup)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().Add(memberGroup);
        }

        MemberGroup IFacade.UpdateMemberGroup(MemberGroup memberGroup)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().Update(memberGroup);
        }

        MemberGroup IFacade.GetMemberGroupById(int id)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().GetById(id);
        }

        IList<MemberGroup> IFacade.GetAllMemberGroup()
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().GetAll();
        }

        IList<MemberGroup> IFacade.GetAllMemberGroup(int groupType)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().GetAll(groupType);
        }

        IList<MemberGroup> IFacade.GetAllMemberGroupByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().GetAllByMemberId(memberId);
        }

        PagedResponse<MemberGroup> IFacade.GetPagedMemberGroup(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().GetPaged(request);
        }

        PagedResponse<MemberGroup> IFacade.GetPagedMemberGroupByMemberId(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().GetPagedByMemberId(request);
        }

        PagedResponse<MemberGroup> IFacade.GetPagedMemberGroupByMemberIdandGroupType(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().GetPagedByMemberIdandGroupType(request);
        }

        bool IFacade.DeleteMemberGroupById(int id)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().DeleteById(id);
        }

        ArrayList IFacade.GetAllMemberGroupNameByManagerId(int managerId)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().GetAllNameByManagerId(managerId);
        }

        ArrayList IFacade.GetAllMemberGroupNameByManagerId(int managerId, int groupType)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().GetAllNameByManagerId(managerId, groupType);
        }
        int IFacade.GetMemberGroupCountByManagerIDAndGroupType(int ManagerID, int groupType)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().GetCountByManagerIDAndGroupType(ManagerID, groupType);

        }
        int IFacade.GetMemberGroupCountByGroupType(int groupType)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().GetCountByGroupType(groupType);
        }

        MemberGroup IFacade.GetMemberGroupByName(string name)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().GetByName(name);
        }

        bool IFacade.GetMemberGroupAccessByCreatorIdAndGroupId(int CreatorId, int GroupId)
        {
            return DataAccessFactory.CreateMemberGroupDataAccess().GetMemberGroupAccessByCreatorIdAndGroupId(CreatorId, GroupId);
        }
        #endregion

        #region MemberGroupManager

        MemberGroupManager IFacade.AddMemberGroupManager(MemberGroupManager memberGroupManager)
        {
            return DataAccessFactory.CreateMemberGroupManagerDataAccess().Add(memberGroupManager);
        }

        MemberGroupManager IFacade.GetMemberGroupManagerById(int id)
        {
            return DataAccessFactory.CreateMemberGroupManagerDataAccess().GetById(id);
        }

        IList<MemberGroupManager> IFacade.GetAllMemberGroupManagerByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberGroupManagerDataAccess().GetAllByMemberId(memberId);
        }

        IList<MemberGroupManager> IFacade.GetAllMemberGroupManager()
        {
            return DataAccessFactory.CreateMemberGroupManagerDataAccess().GetAll();
        }

        int IFacade.GetTotalMemberByMemberGroupId(int memberGroupId)
        {
            return DataAccessFactory.CreateMemberGroupManagerDataAccess().GetTotalMemberByMemberGroupId(memberGroupId);
        }

        bool IFacade.DeleteMemberGroupManagerById(int id)
        {
            return DataAccessFactory.CreateMemberGroupManagerDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteMemberGroupManagerByMemberGroupIdAndMemberId(int memberGroupId, int memberId)
        {
            return DataAccessFactory.CreateMemberGroupManagerDataAccess().DeleteByMemberGroupIdAndMemberId(memberGroupId, memberId);
        }

        #endregion

        #region MemberPendingJoiners
        PagedResponse<MemberPendingJoiners> IFacade.MemberPendingJoiners_GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberPendingJoinersDataAccess().GetPaged(request);
        }
        #endregion


        #region MemberPrivilege

        MemberPrivilege IFacade.AddMemberPrivilege(MemberPrivilege customRole)
        {
            return DataAccessFactory.CreateMemberPrivilegeDataAccess().Add(customRole);
        }

        MemberPrivilege IFacade.UpdateMemberPrivilege(MemberPrivilege customRole)
        {
            return DataAccessFactory.CreateMemberPrivilegeDataAccess().Update(customRole);
        }

        MemberPrivilege IFacade.GetMemberPrivilegeById(int id)
        {
            return DataAccessFactory.CreateMemberPrivilegeDataAccess().GetById(id);
        }

        ArrayList IFacade.GetAllMemberPrivilegeIdsByMemberId(int memberId)
        {
            ArrayList idList = new ArrayList();
            IList<MemberPrivilege> memberPrivilegeList = DataAccessFactory.CreateMemberPrivilegeDataAccess().GetAllByMemberId(memberId);

            if (memberPrivilegeList != null && memberPrivilegeList.Count > 0)
            {
                foreach (MemberPrivilege memberPrivilege in memberPrivilegeList)
                {
                    idList.Add(memberPrivilege.CustomSiteMapId);
                }
            }

            return idList;
        }

        IList<MemberPrivilege> IFacade.GetAllMemberPrivilegeByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberPrivilegeDataAccess().GetAllByMemberId(memberId);
        }

        IList<MemberPrivilege> IFacade.GetAllMemberPrivilege()
        {
            return DataAccessFactory.CreateMemberPrivilegeDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberPrivilegeById(int id)
        {
            return DataAccessFactory.CreateMemberPrivilegeDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteMemberPrivilegeByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberPrivilegeDataAccess().DeleteByMemberId(memberId);
        }
        bool IFacade.GetMemberPrivilegeForMemberIdAndRequiredURL(int memberId, string Url)
        {
            return DataAccessFactory.CreateMemberPrivilegeDataAccess().GetMemberPrivilegeForMemberIdAndRequiredURL(memberId, Url);
        }

        bool IFacade.DeleteByMemberIdWithOutAdminAccess(int memberId)
        {
            return DataAccessFactory.CreateMemberPrivilegeDataAccess().DeleteByMemberIdWithOutAdminAccess(memberId);
        }

        void IFacade.DeleteAllMemberPrivilige()
        {
            DataAccessFactory.CreateMemberPrivilegeDataAccess().DeleteAll_MemberPrivilege();
        }
        #endregion

        #region MemberGroupMap

        MemberGroupMap IFacade.AddMemberGroupMap(MemberGroupMap memberGroupMap)
        {
            memberGroupMap = DataAccessFactory.CreateMemberGroupMapDataAccess().Add(memberGroupMap);

            //if (memberGroupMap != null)
            //{
            //    Member member = DataAccessFactory.CreateMemberDataAccess().GetById(memberGroupMap.MemberId);
            //    MembershipUser user = Membership.GetUser(member.UserId);
            //    string role = (Roles.GetRolesForUser(user.UserName)).GetValue(0).ToString();

            //    //Memberrole
            //    WorkFlowTitle workFlowTitle = WorkFlowTitle.ATSShouldAssignedManagersForCandidateAlertedWhenCandidateAddedHotList;
            //    ApplicationWorkflow workflow = null;
            //    MemberType memberType = MemberType.Candidate;
            //    if (role == MemberType.Candidate.ToString())
            //    {
            //        workFlowTitle = WorkFlowTitle.ATSShouldAssignedManagersForCandidateAlertedWhenCandidateAddedHotList;
            //        workflow = DataAccessFactory.CreateApplicationWorkflowDataAccess().GetByTitle(workFlowTitle.ToString());
            //        memberType = MemberType.Candidate;
            //    }
            //    else if (role == MemberType.Consultant.ToString())
            //    {
            //        workFlowTitle = WorkFlowTitle.ConShouldAssignedManagersForConsultantAlertedWhenConsultantAddedToHotList;
            //        workflow = DataAccessFactory.CreateApplicationWorkflowDataAccess().GetByTitle(workFlowTitle.ToString());
            //        memberType = MemberType.Candidate;
            //    }

            //    if (workflow != null && workflow.RequireApproval)
            //    {
            //        string strMemberIds = String.Empty;

            //        if (workflow.IsDashboardAlert)
            //        {
            //            SaveDashboardAlertForMember(workFlowTitle, workflow.Id, member.Id);
            //        }

            //        if (workflow.IsEmailAlert)
            //        {
            //            MemberGroup memberGroup = DataAccessFactory.CreateMemberGroupDataAccess().GetById(memberGroupMap.MemberGroupId);
            //            if (memberGroup != null)
            //            {
            //                WorkFlowMailHelper mailHelper = new WorkFlowMailHelper(workFlowTitle);
            //                mailHelper.CurrentMemberType = memberType;
            //                mailHelper.ApplicantName = member.FirstName + " " + member.LastName;
            //                mailHelper.ApplicantEmail = member.PrimaryEmail;
            //                mailHelper.HotListName = memberGroup.Name;
            //                mailHelper.SendAlertByEmail(GetAlertEmailAddressesForMember(workflow.Id, member.Id));
            //            }
            //        }

            //        if (workflow.IsSmsAlert)
            //        {

            //        }
            //    }
            //}

            return memberGroupMap;

        }

        MemberGroupMap IFacade.UpdateMemberGroupMap(MemberGroupMap memberGroupMap)
        {
            return DataAccessFactory.CreateMemberGroupMapDataAccess().Update(memberGroupMap);
        }

        MemberGroupMap IFacade.GetMemberGroupMapById(int id)
        {
            return DataAccessFactory.CreateMemberGroupMapDataAccess().GetById(id);
        }

        MemberGroupMap IFacade.GetMemberGroupMapByMemberIdandMemberGroupId(int memberId, int memberGroupId)
        {
            return DataAccessFactory.CreateMemberGroupMapDataAccess().GetByMemberIdandMemberGroupId(memberId, memberGroupId);
        }

        IList<MemberGroupMap> IFacade.GetAllMemberGroupMapByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberGroupMapDataAccess().GetAllByMemberId(memberId);
        }

        IList<MemberGroupMap> IFacade.GetAllMemberGroupMap()
        {
            return DataAccessFactory.CreateMemberGroupMapDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberGroupMapById(int id)
        {
            return DataAccessFactory.CreateMemberGroupMapDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteMemberGroupMapByIdandMemberGroupId(int id, int memberGroupId)
        {
            return DataAccessFactory.CreateMemberGroupMapDataAccess().DeleteByIdandMemberGroupId(id, memberGroupId);
        }
        PagedResponse<MemberGroupMap> IFacade.MembetGroupMapGetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberGroupMapDataAccess().GetPaged(request);
        }
        #endregion

        #region MemberHiringDetails

        void IFacade.AddMemberHiringDetails(MemberHiringDetails memberHiringDetails,string MemberId)
        {
            DataAccessFactory.CreateMemberHiringDetailsDataAccess().Add(memberHiringDetails,MemberId );
        }

        MemberHiringDetails IFacade.UpdateMemberHiringDetails(MemberHiringDetails memberHiringDetails)
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().Update(memberHiringDetails);
        }

        MemberHiringDetails IFacade.GetMemberHiringDetailsById(int id)
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().GetById(id);
        }

        MemberHiringDetails IFacade.GetMemberHiringDetailsByMemberIdAndJobPostingID(int memeberID, int jobposingID)
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().GetByMemberIdAndJobPosstingID(memeberID, jobposingID);
        }

        bool IFacade.DeleteMemberHiringDetailsByID(int Id)
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().DeleteHiringDetialsById(Id);
        }

        PagedResponse<MemberHiringDetails> IFacade.GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().GetPaged(request);
        }
        PagedResponse<MemberHiringDetails> IFacade.MemberHiringDetails_GetPagedEmbeddedDetails(PagedRequest reqest)
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().GetPagedEmbeddedSalarydetails(reqest);
        }

        PagedResponse<MemberHiringDetails> IFacade.MemberHiringDetails_GetPagedMechDetails(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().GetPagedMechSalarydetails(request);
        }

        PagedResponse<MemberOfferJoinDetails> IFacade.MemberHiringDetails_GetPagedOfferJoinDetails(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().GetPagedOfferJoined(request );
        }
        PagedResponse<MemberOfferJoinDetails> IFacade.MemberHiringDetails_GetPagedOfferJoinDetailsForCommon(PagedRequest request) 
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().GetPagedOfferJoinedCommon(request);
        }
        MemberHiringDetails IFacade.GetSalaryComponentsByJobpostingIdAndMemberId(int jobpostingId, int MemberId)
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().GetSalaryComponentsByJobpostingIdAndMemberId(jobpostingId, MemberId);
        }
        MemberHiringDetails IFacade.GetOfferDetailsByJobpostingIdAndMemberId(int jobpostingId, int MemberId)
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().GetOfferDetailsByJobpostingIdAndMemberId(jobpostingId, MemberId);
        }
        IList<MemberHiringDetails> IFacade.GetAllManagementBand()
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().GetAllManagementBand();
        }
        IList<MemberHiringDetails> IFacade.GetAllLocation()
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().GetAllLocation();
        }
        IList<MemberHiringDetails> IFacade.GetAllManagementBandByManagementBandId(int ManagementBandId)
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().GetAllManagementBandByManagementBandId(ManagementBandId);
        }
        IList<MemberHiringDetails> IFacade.GetAllBand()
        {
            return DataAccessFactory.CreateMemberHiringDetailsDataAccess().GetAllBand();
        }
        #endregion

        #region MemberHiringProcess

        MemberHiringProcess IFacade.AddMemberHiringProcess(MemberHiringProcess memberHiringProcess)
        {
            return DataAccessFactory.CreateMemberHiringProcessDataAccess().Add(memberHiringProcess);
        }

        MemberHiringProcess IFacade.UpdateMemberHiringProcess(MemberHiringProcess memberHiringProcess)
        {
            return DataAccessFactory.CreateMemberHiringProcessDataAccess().Update(memberHiringProcess);
        }

        MemberHiringProcess IFacade.GetMemberHiringProcessById(int id)
        {
            return DataAccessFactory.CreateMemberHiringProcessDataAccess().GetById(id);
        }

        IList<MemberHiringProcess> IFacade.GetAllMemberHiringProcess()
        {
            return DataAccessFactory.CreateMemberHiringProcessDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberHiringProcessById(int id)
        {
            //23. Should employees be alerted if they are un-assigned from a requisition? 
            //ApplicationWorkflow applicationWorkflow = DataAccessFactory.CreateApplicationWorkflowDataAccess().GetByTitle(WorkFlowTitle.RequisitionManagementShouldEmployeesAlertedIfUnAssignedFromRequisition.ToString());
            //if (applicationWorkflow != null && applicationWorkflow.RequireApproval)
            //{
            //    if (applicationWorkflow.IsDashboardAlert)
            //    {
            //        SaveDashboardAlertForMember(WorkFlowTitle.RequisitionManagementShouldEmployeesAlertedIfUnAssignedFromRequisition, applicationWorkflow.Id, id);
            //    }
            //    if (applicationWorkflow.IsEmailAlert)
            //    {
            //        Member member = DataAccessFactory.CreateMemberDataAccess().GetById(id);
            //        if (member != null)
            //        {
            //            WorkFlowMailHelper mailHelper = new WorkFlowMailHelper(WorkFlowTitle.RequisitionManagementShouldEmployeesAlertedIfUnAssignedFromRequisition);
            //            mailHelper.CurrentMemberType = MemberType.Employee;
            //            mailHelper.ApplicantName = member.FirstName + " " + member.LastName;
            //            mailHelper.ApplicantEmail = member.PrimaryEmail;
            //            mailHelper.SendAlertByEmail(GetAlertEmailAddressesForMember(applicationWorkflow.Id, member.Id));
            //        }
            //    }
            //    if (applicationWorkflow.IsSmsAlert)
            //    {

            //    }
            //}
            return DataAccessFactory.CreateMemberHiringProcessDataAccess().DeleteById(id);
        }

        IList<MemberHiringProcess> IFacade.GetMemberHiringProcessByMemberIdJobPostingId(int memberId, int jobPostingId)
        {
            return DataAccessFactory.CreateMemberHiringProcessDataAccess().GetByMemberIdJobPostingId(memberId, jobPostingId);
        }

        MemberHiringProcess IFacade.GetMemberHiringProcessByMemberIdJobPostingIdAndInterviewLevelId(int memberId, int jobPostingId, int interviewlevelid)
        {
            return DataAccessFactory.CreateMemberHiringProcessDataAccess().GetByMemberIdJobPostingIdAndInterviewLevelId(memberId, jobPostingId, interviewlevelid);
        }

        #endregion


        #region MemberJobApplied
        PagedResponse<MemberJobApplied> IFacade.MemberJobapplied_GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberJobAppliedDataAccess().getPaged(request);
        }

        MemberJobApplied IFacade.AddMemberJobApplied(MemberJobApplied jobApplied)
        {
            return DataAccessFactory.CreateMemberJobAppliedDataAccess().Add(jobApplied);
        }

        MemberJobApplied IFacade.MemberJobApplied_GetByMemberIDANDJobPostingID(int memberid, int jobpostingid)
        {

            return DataAccessFactory.CreateMemberJobAppliedDataAccess().GetByMemberIdAndJobPostingId(memberid, jobpostingid);

        }
        #endregion
        #region MemberJobCart

        MemberJobCart IFacade.AddMemberJobCart(MemberJobCart memberJobCart)
        {
            //20. Should the assigned recruiters for a requisition be alerted when a recruiter adds candidates/consultants to the Hiring Matrix? 
            ApplicationWorkflow applicationWorkflow = DataAccessFactory.CreateApplicationWorkflowDataAccess().GetByTitle(WorkFlowTitle.RequisitionManagementShouldAssignedRecruitersForRequisitionAlertedWhenRecruiterAddsCandidatesHiringMatrix.ToString());
            if (applicationWorkflow != null && applicationWorkflow.RequireApproval)
            {
                if (applicationWorkflow.IsDashboardAlert)
                {
                    IList<JobPostingHiringTeam> jobPostingHiringTeamList = DataAccessFactory.CreateJobPostingHiringTeamDataAccess().GetAllByJobPostingId(memberJobCart.JobPostingId);
                    if (jobPostingHiringTeamList != null && jobPostingHiringTeamList.Count > 0)
                    {
                        foreach (JobPostingHiringTeam jobPostingHiringTeam in jobPostingHiringTeamList)
                        {
                            if (jobPostingHiringTeam != null)
                            {
                                if (jobPostingHiringTeam.MemberId > 0)
                                {
                                    // SaveDashboardAlertForMember(WorkFlowTitle.RequisitionManagementShouldAssignedRecruitersForRequisitionAlertedWhenRecruiterAddsCandidatesHiringMatrix, applicationWorkflow.Id, jobPostingHiringTeam.MemberId);
                                }
                            }
                        }
                    }
                }
                else if (applicationWorkflow.IsEmailAlert)
                {
                    List<string> listItem = new List<string>();
                    IList<JobPostingHiringTeam> jobPostingHiringTeamList = DataAccessFactory.CreateJobPostingHiringTeamDataAccess().GetAllByJobPostingId(memberJobCart.JobPostingId);
                    if (jobPostingHiringTeamList != null && jobPostingHiringTeamList.Count > 0)
                    {
                        foreach (JobPostingHiringTeam jobPostingHiringTeam in jobPostingHiringTeamList)
                        {
                            if (jobPostingHiringTeam != null)
                            {
                                if (jobPostingHiringTeam.MemberId > 0)
                                {
                                    Member member = DataAccessFactory.CreateMemberDataAccess().GetById(jobPostingHiringTeam.MemberId);
                                    if (member != null && !string.IsNullOrEmpty(member.PrimaryEmail))
                                    {
                                        listItem.Add(member.PrimaryEmail);
                                    }
                                }
                            }
                        }
                    }

                    WorkFlowMailHelper workFlowMailHelper = new WorkFlowMailHelper(WorkFlowTitle.RequisitionManagementShouldAssignedRecruitersForRequisitionAlertedWhenRecruiterAddsCandidatesHiringMatrix);
                    workFlowMailHelper.SendAlertByEmail(listItem);
                }
                else if (applicationWorkflow.IsSmsAlert)
                {
                }
            }
            return DataAccessFactory.CreateMemberJobCartDataAccess().Add(memberJobCart);
        }

        MemberJobCart IFacade.UpdateMemberJobCart(MemberJobCart memberJobCart)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().Update(memberJobCart);
        }

        MemberJobCart IFacade.GetMemberJobCartById(int id)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetById(id);
        }

        IList<MemberJobCart> IFacade.GetAllMemberJobCart()
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetAll();
        }

        IList<MemberJobCart> IFacade.GetAllMemberJobCartByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetAllByMemberId(memberId);
        }

        // Defect id 8841
        IList<MemberJobCart> IFacade.GetAllMemberJobCartByMemberId(int memberId, string SortExpression)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetAllByMemberId(memberId, SortExpression);
        }

        bool IFacade.DeleteMemberJobCartById(int id)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().DeleteById(id);
        }
    
        //**************Code added by pravin khot on 24/June/2016********
        bool IFacade.RejectToUnRejectCandidateStatusChange(string MemberIds, int JobPostingId)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().RejectToUnRejectCandidate(MemberIds, JobPostingId);
        }

        bool IFacade.RejectMemberJobCartById(int id)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().RejectById(id);
        }
        //*************************END***************************
        PagedResponse<MemberJobCart> IFacade.GetPagedMemberJobCart(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetPaged(request);
        }
		//Candidate Status in Employee Referral Portal.
        PagedResponse<MemberJobCart> IFacade.GetPagedMemberJobCartForEmployeeReferal(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetPagedForEmployeeReferal(request);
        }

        MemberJobCart IFacade.GetMemberJobCartByMemberIdAndJobPostringId(int memberId, int jobpostingId)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetByMemberIdAndJobPostingId(memberId, jobpostingId);
        }

        IList<MemberJobCart> IFacade.GetAllMemberJobCartByByJobPostingIdAndSelectionStep(int jobPostingId, int selectionStep)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetAllByJobPostingIdAndSelectionStep(jobPostingId, selectionStep);
        }

        IList<MemberJobCart> IFacade.GetAllMemberJobCartByMemberIdAndSelectionStep(int memberId, int selectionStep)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetAllByMemberIdAndSelectionStep(memberId, selectionStep);
        }
        //0.13 Start

        IList<MemberJobCart> IFacade.GetAllSubmittedCandidateInterviewScore(int memberId, int selectionStep, string sortExpression)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetAllSubmittedCandidateInterviewScore(memberId, selectionStep, sortExpression);
        }
        //0.13 End

        int[] IFacade.GetHiringMatrixMemberCount(int jobPostingId)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetHiringMatrixMemberCount(jobPostingId);
        }

        int[] IFacade.GetHiringMatrixMemberCount(int jobPostingId, string dateWhenCountTobeDone)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetHiringMatrixMemberCount(jobPostingId, dateWhenCountTobeDone);
        }


        void IFacade.MemberJobCart_MoveToNextLevel(int CurrentLevel, int UpdatorId, string MemberIds, int JobPostingID, string MovingDirection)
        {
            DataAccessFactory.CreateMemberJobCartDataAccess().MoveToNextLevel(CurrentLevel, UpdatorId, MemberIds, JobPostingID, MovingDirection);
        }

        //************code added by pravin khot on 25/Jan/2016*****************
        int IFacade.MemberCareerPortalToRequisition_Add(string CanIds)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().CareerPortalToRequisitionAdd(CanIds);
        }
        //*************************************End*****************************************
        int IFacade.MemberJobCart_AddCandidateToRequisition(int CreatorId, string MemberIds, int JobPostingId)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().AddCandidateToRequisition(CreatorId, MemberIds, JobPostingId);
        }

        //*********************Code added by pravin khot on 27/April/2016************
        int IFacade.MemberJobCart_AddCandidateToMultipleRequisition(int CreatorId, string MemberIds, int JobPostingId)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().AddCandidateToMultipleRequisition(CreatorId, MemberIds, JobPostingId);
        }
        //***************************END************************************
        void IFacade.UpdateByStatus(int RequisitionId, string MemberId, int StatusId, int UpdatorId)
        {
            DataAccessFactory.CreateMemberJobCartDataAccess().UpdateByStatus(RequisitionId, MemberId, StatusId, UpdatorId);
        }

        void IFacade.UpdateMemberJobCartByMemberIdAndJobPostingID(int MemberId, int JobPostingId, int StatusId)
        {
            DataAccessFactory.CreateMemberJobCartDataAccess().UpdateSourceByMemberIDandJobPostingID(MemberId, JobPostingId, StatusId);
        }

        IList<int> IFacade.CheckCandidatesInHiringMatrix(string CandidateIds, int JobpostingId) 
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().CheckCandidatesInHiringMatrix(CandidateIds, JobpostingId);
        
        }
        string IFacade.GetRecruiternameByMemberId(int memberId) 
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetRecruiternameByMemberId(memberId);
        }
        string IFacade.GetCandidateStatusByMemberIdAndJobPostingId(int MemberId, int JobPostingId) 
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetCandidateStatusByMemberIdAndJobPostingId(MemberId, JobPostingId);
        }
        int IFacade.GetActiveRequsiutionCountByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberJobCartDataAccess().GetActiveRequisitionCountByMemberId(memberId);
        }
        #endregion

        #region MemberJobCartAlert

        MemberJobCartAlert IFacade.AddMemberJobCartAlert(MemberJobCartAlert memberJobCartAlert)
        {
            return DataAccessFactory.CreateMemberJobCartAlertDataAccess().Add(memberJobCartAlert);
        }

        MemberJobCartAlert IFacade.UpdateMemberJobCartAlert(MemberJobCartAlert memberJobCartAlert)
        {
            return DataAccessFactory.CreateMemberJobCartAlertDataAccess().Update(memberJobCartAlert);
        }

        MemberJobCartAlert IFacade.GetMemberJobCartAlertById(int id)
        {
            return DataAccessFactory.CreateMemberJobCartAlertDataAccess().GetById(id);
        }

        IList<MemberJobCartAlert> IFacade.GetAllMemberJobCartAlert()
        {
            return DataAccessFactory.CreateMemberJobCartAlertDataAccess().GetAll();
        }

        IList<MemberJobCartAlert> IFacade.GetAllByMemberJobCartAlertSetupId(int memberJobCartAlertSetupId)
        {
            return DataAccessFactory.CreateMemberJobCartAlertDataAccess().GetAllByMemberJobCartAlertSetupId(memberJobCartAlertSetupId);
        }

        IList<MemberJobCartAlert> IFacade.GetAllByJobPostingId(int jobPostingId)
        {
            return DataAccessFactory.CreateMemberJobCartAlertDataAccess().GetAllByJobPostingId(jobPostingId);
        }

        bool IFacade.DeleteMemberJobCartAlertById(int id)
        {
            return DataAccessFactory.CreateMemberJobCartAlertDataAccess().DeleteById(id);
        }

        bool IFacade.RemoveAllByMemberJobCartAlertSetupId(int memberJobCartAlertSetupId)
        {
            return DataAccessFactory.CreateMemberJobCartAlertDataAccess().RemoveAllByMemberJobCartAlertSetupId(memberJobCartAlertSetupId);
        }

        bool IFacade.DeleteAllByMemberJobCartAlertSetupId(int memberJobCartAlertSetupId)
        {
            return DataAccessFactory.CreateMemberJobCartAlertDataAccess().DeleteByMemberJobCartAlertSetupId(memberJobCartAlertSetupId);
        }

        #endregion

        #region MemberJobCartDetail

        MemberJobCartDetail IFacade.AddMemberJobCartDetail(MemberJobCartDetail memberJobCartDetail)
        {
            return DataAccessFactory.CreateMemberJobCartDetailDataAccess().Add(memberJobCartDetail);
        }

        MemberJobCartDetail IFacade.UpdateMemberJobCartDetail(MemberJobCartDetail memberJobCartDetail)
        {
            return DataAccessFactory.CreateMemberJobCartDetailDataAccess().Update(memberJobCartDetail);
        }

        MemberJobCartDetail IFacade.GetMemberJobCartDetailById(int id)
        {
            return DataAccessFactory.CreateMemberJobCartDetailDataAccess().GetById(id);
        }

        IList<MemberJobCartDetail> IFacade.GetAllMemberJobCartDetail()
        {
            return DataAccessFactory.CreateMemberJobCartDetailDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberJobCartDetailById(int id)
        {
            return DataAccessFactory.CreateMemberJobCartDetailDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteAllMemberJobCartDetailByJobCartId(int memberJobCartId)
        {
            return DataAccessFactory.CreateMemberJobCartDetailDataAccess().DeleteAllByJobCartId(memberJobCartId);
        }

        bool IFacade.RemoveAllMemberJobCartDetailByJobCartId(int memberJobCartId)
        {
            return DataAccessFactory.CreateMemberJobCartDetailDataAccess().RemoveAllByJobCartId(memberJobCartId);
        }

        IList<MemberJobCartDetail> IFacade.GetAllMemberJobCartDetailByMemberJobCartId(int memberJobCartId)
        {
            return DataAccessFactory.CreateMemberJobCartDetailDataAccess().GetAllByMemberJobCartId(memberJobCartId);
        }

        MemberJobCartDetail IFacade.GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(int memberJobCartId, int selectionStepId)
        {
            return DataAccessFactory.CreateMemberJobCartDetailDataAccess().GetByMemberJobCartIdAndSelectionStepId(memberJobCartId, selectionStepId);
        }

        MemberJobCartDetail IFacade.GetMemberJobCartDetailCurrentByMemberJobCartId(int memberJobCartId)
        {
            return DataAccessFactory.CreateMemberJobCartDetailDataAccess().GetCurrentByMemberJobCartId(memberJobCartId);
        }

        #endregion

        #region MemberJobCartAlertSetup

        MemberJobCartAlertSetup IFacade.AddMemberJobCartAlertSetup(MemberJobCartAlertSetup memberJobCartAlertSetup)
        {
            return DataAccessFactory.CreateMemberJobCartAlertSetupDataAccess().Add(memberJobCartAlertSetup);
        }

        MemberJobCartAlertSetup IFacade.UpdateMemberJobCartAlertSetup(MemberJobCartAlertSetup memberJobCartAlertSetup)
        {
            return DataAccessFactory.CreateMemberJobCartAlertSetupDataAccess().Update(memberJobCartAlertSetup);
        }

        MemberJobCartAlertSetup IFacade.GetMemberJobCartAlertSetupById(int id)
        {
            return DataAccessFactory.CreateMemberJobCartAlertSetupDataAccess().GetById(id);
        }

        MemberJobCartAlertSetup IFacade.GetMemberJobCartAlertSetupByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberJobCartAlertSetupDataAccess().GetByMemberId(memberId);
        }

        IList<MemberJobCartAlertSetup> IFacade.GetAllMemberJobCartAlertSetupByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberJobCartAlertSetupDataAccess().GetAllByMemberId(memberId);
        }

        IList<MemberJobCartAlertSetup> IFacade.GetAllMemberJobCartAlertSetup()
        {
            return DataAccessFactory.CreateMemberJobCartAlertSetupDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberJobCartAlertSetupById(int id)
        {
            return DataAccessFactory.CreateMemberJobCartAlertSetupDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteMemberJobCartAlertSetupByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberJobCartAlertSetupDataAccess().DeleteByMemberId(memberId);
        }

        #endregion

        #region MemberJoiningDetails

        void IFacade.AddMemberJoiningDetails(MemberJoiningDetail MemberJoiningDetail, string MemberId)
        {
            DataAccessFactory.CreateMemberJoiningDetailsDataAccess().Add(MemberJoiningDetail, MemberId);
        }

        MemberJoiningDetail IFacade.GetMemberJoiningDetailsById(int id)
        {
            return DataAccessFactory.CreateMemberJoiningDetailsDataAccess().GetById(id);
        }

        MemberJoiningDetail IFacade.GetMemberJoiningDetailsByMemberIdAndJobPostingID(int memeberID, int jobposingID)
        {
            return DataAccessFactory.CreateMemberJoiningDetailsDataAccess().GetByMemberIdAndJobPosstingID(memeberID, jobposingID);
        }

        bool IFacade.DeleteMemberJoiningDetailsByID(int Id)
        {
            return DataAccessFactory.CreateMemberJoiningDetailsDataAccess().DeleteJoiningDetialsById(Id);
        }

        #endregion

        #region MemberManager

        MemberManager IFacade.AddMemberManager(MemberManager memberManager)
        {
            return DataAccessFactory.CreateMemberManagerDataAccess().Add(memberManager);
        }

        MemberManager IFacade.UpdateMemberManager(MemberManager memberManager)
        {
            return DataAccessFactory.CreateMemberManagerDataAccess().Update(memberManager);
        }

        MemberManager IFacade.GetMemberManagerById(int id)
        {
            return DataAccessFactory.CreateMemberManagerDataAccess().GetById(id);
        }

        MemberManager IFacade.GetMemberManagerByMemberIdAndManagerId(int memberId, int managerId)
        {
            return DataAccessFactory.CreateMemberManagerDataAccess().GetByMemberIdAndManagerId(memberId, managerId);
        }

        MemberManager IFacade.GetMemberManagerPrimaryByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberManagerDataAccess().GetPrimaryByMemberId(memberId);
        }

        IList<MemberManager> IFacade.GetAllMemberManagerByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberManagerDataAccess().GetAllByMemberId(memberId);
        }

        IList<MemberManager> IFacade.GetAllMemberManager()
        {
            return DataAccessFactory.CreateMemberManagerDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberManagerById(int id)
        {
            return DataAccessFactory.CreateMemberManagerDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteMemberManagerByMemberIdAndManagerId(int memberId, int managerId)
        {
            return DataAccessFactory.CreateMemberManagerDataAccess().DeleteByMemberIdAndManagerId(memberId, managerId);
        }

        bool IFacade.DeleteAllMemberManagerByMemberId(int memberId )
        {
            return DataAccessFactory.CreateMemberManagerDataAccess().DeleteAllMemberManagerByMemberId(memberId);
        }

        ArrayList IFacade.GetMemberManagerListByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberManagerDataAccess().GetListByMemberId(memberId);
        }


        PagedResponse<Member> IFacade.GetPagedMemberManagerByMemberID(int MemberId, PagedRequest request)
        {
            return DataAccessFactory.CreateMemberManagerDataAccess().GetPagedByMemberId(MemberId, request);
        }

        string IFacade.getMemberManager_PrimaryManagerName(int MemberId)
        {
            return DataAccessFactory.CreateMemberManagerDataAccess().GetPrimaryManagerNameByMemberId(MemberId);
        }
        #endregion

        #region MemberInterview

        MemberInterview IFacade.GetMemberInterviewById(int id)
        {
            return DataAccessFactory.CreateMemberInterviewDataAccess().GetById(id);
        }

        IList<MemberInterview> IFacade.GetAllMemberInterviewByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberInterviewDataAccess().GetAllByMemberId(memberId);
        }

        // Defect id : 10254
        IList<MemberInterview> IFacade.GetAllMemberInterviewByMemberId(int memberId, string sortExpression)
        {
            return DataAccessFactory.CreateMemberInterviewDataAccess().GetAllByMemberId(memberId, sortExpression);
        }

        PagedResponse<MemberInterview> IFacade.GetPagedMemberInterview(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberInterviewDataAccess().GetPaged(request);
        }

        PagedResponse<MemberInterview> IFacade.GetPagedMemberInterviewForDashboard(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberInterviewDataAccess().GetPagedForDashboard(request);
        }

        PagedResponse<MemberInterview> IFacade.GetPagedInterviewReport(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberInterviewDataAccess().GetInterviewReport(request);
        }
        //****************************code added by pravin khot on 15/Dec/2015***************
        bool IFacade.MemberInterview_SuggestedInterviewerId(int interviewId, int InterviewerId)
        {
            return DataAccessFactory.CreateMemberInterviewDataAccess().SuggestedInterviewerId(interviewId, InterviewerId);
        }
        //******************************End*************************************()
        #endregion

        #region MemberNote

        MemberNote IFacade.AddMemberNote(MemberNote memberNote)
        {
            return DataAccessFactory.CreateMemberNoteDataAccess().Add(memberNote);
        }

        MemberNote IFacade.GetMemberNoteById(int id)
        {
            return DataAccessFactory.CreateMemberNoteDataAccess().GetById(id);
        }

        IList<MemberNote> IFacade.GetAllMemberNote()
        {
            return DataAccessFactory.CreateMemberNoteDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberNoteById(int id)
        {
            return DataAccessFactory.CreateMemberNoteDataAccess().DeleteById(id);
        }

        IList<MemberNote> IFacade.GetAllMemberNoteByMemberId(int id)
        {
            return DataAccessFactory.CreateMemberNoteDataAccess().GetAllByMemberId(id);
        }

        #endregion

        #region MemberObjectiveAndSummary

        MemberObjectiveAndSummary IFacade.AddMemberObjectiveAndSummary(MemberObjectiveAndSummary memberObjectiveAndSummary)
        {
            return DataAccessFactory.CreateMemberObjectiveAndSummaryDataAccess().Add(memberObjectiveAndSummary);
        }

        MemberObjectiveAndSummary IFacade.UpdateMemberObjectiveAndSummary(MemberObjectiveAndSummary memberObjectiveAndSummary)
        {
            return DataAccessFactory.CreateMemberObjectiveAndSummaryDataAccess().Update(memberObjectiveAndSummary);
        }

        MemberObjectiveAndSummary IFacade.GetMemberObjectiveAndSummaryById(int id)
        {
            return DataAccessFactory.CreateMemberObjectiveAndSummaryDataAccess().GetById(id);
        }

        MemberObjectiveAndSummary IFacade.GetMemberObjectiveAndSummaryByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberObjectiveAndSummaryDataAccess().GetByMemberId(memberId);
        }

        IList<MemberObjectiveAndSummary> IFacade.GetAllMemberObjectiveAndSummary()
        {
            return DataAccessFactory.CreateMemberObjectiveAndSummaryDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberObjectiveAndSummaryById(int id)
        {
            return DataAccessFactory.CreateMemberObjectiveAndSummaryDataAccess().DeleteById(id);
        }

        #endregion


        #region MemberOfferRejection

        void IFacade.AddMemberOfferRejection(MemberOfferRejection offerRejection,string MemberID) 
        {
            DataAccessFactory.CreateMemberOfferRejectionDataAccess().Add(offerRejection, MemberID);
        }
        MemberOfferRejection IFacade.MemberOfferRejection_GetbyId(int id)
        {
            return DataAccessFactory.CreateMemberOfferRejectionDataAccess().GetById(id);
        }
        MemberOfferRejection IFacade.MemberOfferRejection_GetbyMemberIdAndJobPostingID(int memberid, int JobPostingId)
        {
            return DataAccessFactory.CreateMemberOfferRejectionDataAccess().GetByMemberIdAndJobPosstingID(memberid, JobPostingId);
        }
        #endregion
        #region MemberReference

        MemberReference IFacade.AddMemberReference(MemberReference memberReference)
        {
            return DataAccessFactory.CreateMemberReferenceDataAccess().Add(memberReference);
        }

        MemberReference IFacade.UpdateMemberReference(MemberReference memberReference)
        {
            return DataAccessFactory.CreateMemberReferenceDataAccess().Update(memberReference);
        }

        MemberReference IFacade.GetMemberReferenceById(int id)
        {
            return DataAccessFactory.CreateMemberReferenceDataAccess().GetById(id);
        }

        IList<MemberReference> IFacade.GetAllMemberReferenceByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberReferenceDataAccess().GetAllByMemberId(memberId);
        }
        /* 0.8 Start */
        IList<MemberReference> IFacade.GetAllMemberReferenceByMemberId(int memberId, string sortExpression)
        {
            return DataAccessFactory.CreateMemberReferenceDataAccess().GetAllByMemberId(memberId, sortExpression);
        }
        /* 0.8 End */
        string IFacade.GetMemberReferenceEmailById(int id)
        {
            return DataAccessFactory.CreateMemberReferenceDataAccess().GetEmailById(id);
        }


        IList<MemberReference> IFacade.GetAllMemberReference()
        {
            return DataAccessFactory.CreateMemberReferenceDataAccess().GetAll();
        }

        PagedResponse<MemberReference> IFacade.GetPagedMemberReference(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberReferenceDataAccess().GetPaged(request);
        }

        bool IFacade.DeleteMemberReferenceById(int id)
        {
            return DataAccessFactory.CreateMemberReferenceDataAccess().DeleteById(id);
        }

        #endregion

        #region MemberSignature

        MemberSignature IFacade.AddMemberSignature(MemberSignature memberSignature)
        {
            return DataAccessFactory.CreateMemberSignatureDataAccess().Add(memberSignature);
        }

        MemberSignature IFacade.UpdateMemberSignature(MemberSignature memberSignature)
        {
            return DataAccessFactory.CreateMemberSignatureDataAccess().Update(memberSignature);
        }

        MemberSignature IFacade.GetMemberSignatureById(int id)
        {
            return DataAccessFactory.CreateMemberSignatureDataAccess().GetById(id);
        }

        MemberSignature IFacade.GetActiveMemberSignatureByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberSignatureDataAccess().GetByMemberId(memberId);
        }

        IList<MemberSignature> IFacade.GetAllMemberSignatureByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberSignatureDataAccess().GetAllByMemberId(memberId);
        }

        IList<MemberSignature> IFacade.GetAllMemberSignature()
        {
            return DataAccessFactory.CreateMemberSignatureDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberSignatureById(int id)
        {
            return DataAccessFactory.CreateMemberSignatureDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteMemberSignatureByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberSignatureDataAccess().DeleteByMemberId(memberId);
        }

        #endregion

        #region MemberSkillMap

        MemberSkillMap IFacade.AddMemberSkillMap(MemberSkillMap memberSkillMap)
        {
            return DataAccessFactory.CreateMemberSkillMapDataAccess().Add(memberSkillMap);
        }

        MemberSkillMap IFacade.UpdateMemberSkillMap(MemberSkillMap memberSkillMap)
        {
            return DataAccessFactory.CreateMemberSkillMapDataAccess().Update(memberSkillMap);
        }

        MemberSkillMap IFacade.GetMemberSkillMapById(int id)
        {
            return DataAccessFactory.CreateMemberSkillMapDataAccess().GetById(id);
        }

        IList<MemberSkillMap> IFacade.GetAllMemberSkillMap()
        {
            return DataAccessFactory.CreateMemberSkillMapDataAccess().GetAll();
        }

        IList<MemberSkillMap> IFacade.GetAllMemberSkillMapByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberSkillMapDataAccess().GetAllByMemberId(memberId);
        }

        MemberSkillMap IFacade.GetMemberSkillMapByMemberIdAndSkillId(int memberId, int skillId)
        {
            return DataAccessFactory.CreateMemberSkillMapDataAccess().GetByMemberIdAndSkillId(memberId, skillId);
        }

        bool IFacade.DeleteMemberSkillMapById(int id)
        {
            return DataAccessFactory.CreateMemberSkillMapDataAccess().DeleteById(id);
        }

        bool IFacade.DeleteMemberSkillMapByIds(string  id)
        {
            return DataAccessFactory.CreateMemberSkillMapDataAccess().DeleteByIds (id);
        }

        PagedResponse<MemberSkillMap> IFacade.GetPagedMemberSkillMap(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberSkillMapDataAccess().GetPaged(request);
        }


        PagedResponse<MemberSkillMap> IFacade.GetPagedMemberSkillMapByMemberId(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberSkillMapDataAccess().GetPagedByMemberId(request);
        }

        bool IFacade.DeleteMemberSkillMapByMemberId(int memberID)
        {
            return DataAccessFactory.CreateMemberSkillMapDataAccess().DeleteByMemberId(memberID);
        }

        string IFacade.GetMemberSkillNamesByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberSkillMapDataAccess().GetAllMemberSkillNamesByMemberId(memberId);
        }

        #endregion

        #region MemberSkill

        IList<MemberSkill> IFacade.GetAllMemberSkillByMemberId(int memberId)
        {
            return DataAccessFactory.CreateMemberSkillDataAccess().GetAllByMemberId(memberId);
        }
        IList<MemberSkill> IFacade.GetAllMemberSkillByMemberId(int memberId, string sortExpression) //0.9
        {
            return DataAccessFactory.CreateMemberSkillDataAccess().GetAllByMemberId(memberId, sortExpression);
        }
        #endregion

        #region MemberSubmission

        void  IFacade.AddMemberSubmission(MemberSubmission memberSubmission,string MemberId)
        {
            DataAccessFactory.CreateMemberSubmissionDataAccess().Add(memberSubmission,MemberId );
        }

        MemberSubmission IFacade.UpdateMemberSubmission(MemberSubmission memberSubmission)
        {
            return DataAccessFactory.CreateMemberSubmissionDataAccess().Update(memberSubmission);
        }
        MemberSubmission IFacade.GetMemberSubmissionsByMemberIDAndJobPostingId(int memberid, int jobpostingid)
        {
            return DataAccessFactory.CreateMemberSubmissionDataAccess().GetByMemberIdAndJobPostingId(memberid, jobpostingid);
        }


        #endregion

        #region MemberSkillSet

        MemberSkillSet IFacade.AddMemberSkillSet(MemberSkillSet memberSkillSet)
        {
            return DataAccessFactory.CreateMemberSkillSetDataAccess().Add(memberSkillSet);
        }

        MemberSkillSet IFacade.UpdateMemberSkillSet(MemberSkillSet memberSkillSet)
        {
            return DataAccessFactory.CreateMemberSkillSetDataAccess().Update(memberSkillSet);
        }

        MemberSkillSet IFacade.GetMemberSkillSetById(int id)
        {
            return DataAccessFactory.CreateMemberSkillSetDataAccess().GetById(id);
        }

        IList<MemberSkillSet> IFacade.GetAllMemberSkillSet()
        {
            return DataAccessFactory.CreateMemberSkillSetDataAccess().GetAll();
        }

        bool IFacade.DeleteMemberSkillSetById(int id)
        {
            return DataAccessFactory.CreateMemberSkillSetDataAccess().DeleteById(id);
        }

        #endregion

        #region MemberSourceHistory

        MemberSourceHistory IFacade.AddMemberSourceHistory(MemberSourceHistory memberSkillSet)
        {
            return DataAccessFactory.CreateMemberSourceHistoryDataAccess().Add(memberSkillSet);
        }

        MemberSourceHistory IFacade.UpdateMemberSourceHistory(MemberSourceHistory memberSkillSet)
        {
            return DataAccessFactory.CreateMemberSourceHistoryDataAccess().Update(memberSkillSet);
        }

        MemberSourceHistory IFacade.GetMemberSourceHistoryById(int id)
        {
            return DataAccessFactory.CreateMemberSourceHistoryDataAccess().GetById(id);
        }

        IList<MemberSourceHistory> IFacade.GetAllMemberSourceHistory()
        {
            return DataAccessFactory.CreateMemberSourceHistoryDataAccess().GetAll();
        }

        bool IFacade.IsMemberCandidateSourceExpired(string primaryEmail)
        {
            return DataAccessFactory.CreateMemberSourceHistoryDataAccess().IsCandidateSourceExpired(primaryEmail);
        }

        bool IFacade.DeleteMemberSourceHistoryById(int id)
        {
            return DataAccessFactory.CreateMemberSourceHistoryDataAccess().DeleteById(id);
        }

        PagedResponse<MemberSourceHistory> IFacade.GetPagedMemberSourceHistory(PagedRequest request)
        {
            return DataAccessFactory.CreateMemberSourceHistoryDataAccess().GetPaged(request);
        }


        #endregion

        #region OccupationalGroup


        IList<OccupationalGroup> IFacade.GetAllOccupationalGroup()
        {
            return DataAccessFactory.CreateOccupationalGroupDataAccess().GetAll();
        }


        IList<OccupationalGroup> IFacade.GetAllOccupationalGroupBySearch(string keyword, int count)
        {
            return DataAccessFactory.CreateOccupationalGroupDataAccess().GetAllBySearch(keyword, count);
        }


        #endregion

        #region OccupationalSeries


        IList<OccupationalSeries> IFacade.GetAllOccupationalSeries()
        {
            return DataAccessFactory.CreateOccupationalSeriesDataAccess().GetAll();
        }

        IList<OccupationalSeries> IFacade.GetAllOccupationalSeriesBySearch(string keyword, int count)
        {
            return DataAccessFactory.CreateOccupationalSeriesDataAccess().GetAllBySearch(keyword, count);
        }
        OccupationalSeries IFacade.GetOccupationalSeriesById(int id)
        {
            return DataAccessFactory.CreateOccupationalSeriesDataAccess().GetbyId(id);
        }


        #endregion

        #region PasswordReset

        PasswordReset IFacade.AddPasswordReset(PasswordReset PasswordReset)
        {
            return DataAccessFactory.CreatePasswordResetDataAccess().Add(PasswordReset);
        }


        PasswordReset IFacade.GetPasswordResetById(int id)
        {
            return DataAccessFactory.CreatePasswordResetDataAccess().GetById(id);
        }

        PasswordReset IFacade.UpdatePasswordReset(PasswordReset PasswordReset)
        {
            return DataAccessFactory.CreatePasswordResetDataAccess().Update(PasswordReset);
        }


        #endregion

        #region Recurrence

        Recurrence IFacade.AddRecurrence(Recurrence recurrence)
        {
            return DataAccessFactory.CreateRecurrenceDataAccess().Add(recurrence);
        }

        Recurrence IFacade.UpdateRecurrence(Recurrence recurrence)
        {
            return DataAccessFactory.CreateRecurrenceDataAccess().Update(recurrence);
        }

        Recurrence IFacade.GetRecurrenceByRecurrenceID(int recurrenceID)
        {
            return DataAccessFactory.CreateRecurrenceDataAccess().GetByRecurrenceID(recurrenceID);
        }

        IList<Recurrence> IFacade.GetAllRecurrence()
        {
            return DataAccessFactory.CreateRecurrenceDataAccess().GetAll();
        }

        bool IFacade.DeleteRecurrenceByRecurrenceID(int recurrenceID)
        {
            return DataAccessFactory.CreateRecurrenceDataAccess().DeleteByRecurrenceID(recurrenceID);
        }

        #endregion

        #region RejectCandidate

        void IFacade.AddRejectCandidate(RejectCandidate RejectCandidate,string MemberId)
        {
             DataAccessFactory.CreateRejectCandidateDataAccess().Add(RejectCandidate,MemberId );
        }

        void IFacade.UpdateRejectCandidate(RejectCandidate RejectCandidate)
        {
             DataAccessFactory.CreateRejectCandidateDataAccess().Update(RejectCandidate);
        }

        RejectCandidate IFacade.GetRejectCandidateById(int id)
        {
            return DataAccessFactory.CreateRejectCandidateDataAccess().GetById(id);
        }

        IList<RejectCandidate> IFacade.GetRejectCandidateByMemberId(int memberId)
        {
            return DataAccessFactory.CreateRejectCandidateDataAccess().GetByMemberId(memberId);
        }


        bool IFacade.DeleteRejectCandidateByMemberId(int Memberid)
        {
            return DataAccessFactory.CreateRejectCandidateDataAccess().DeleteByMemberId(Memberid);
        }

        RejectCandidate IFacade.GetRejectCandidateByMemberIdAndJobPostingID(int memberId, int JobPostingID)
        {
            return DataAccessFactory.CreateRejectCandidateDataAccess().GetByMemberIdAndJobPostingId(memberId, JobPostingID);
        }


        bool IFacade.DeleteRejectCandidateByMemberIdAndJobPostingID(int MemberId, int JobPostingID)
        {
            return DataAccessFactory.CreateRejectCandidateDataAccess().DeleteByMemberIdAndJobPostingID(MemberId, JobPostingID);
        }

        PagedResponse<RejectCandidate> IFacade.GetPagedForRejectCandidate (PagedRequest request)
        {
            return DataAccessFactory.CreateRejectCandidateDataAccess().GetPaged(request);
        }

        int IFacade.GetRejectCandidateCount(int JobPostingId)
        {
            return DataAccessFactory.CreateRejectCandidateDataAccess().GetRejectCandidateCount(JobPostingId );
        }
        //***********************************Code added by pravin khot on 14/March/2016************
        PagedResponse<RejectCandidate> IFacade.GetPagedCandidateReject(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, 
           int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
           int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string JobPostingId)
        {
            return DataAccessFactory.CreateRejectCandidateDataAccess().GetPagedForReportReject(request, addedFrom, addedTo, addedBy, addedBySource, 
            country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
           gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, JobPostingId);
        }

        PagedResponse<DynamicDictionary> IFacade.GetPagedRejectCandidateForSelectedColumn(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, 
         int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
          int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string JobPostingId, IList<string> checkedList)
        {
            return DataAccessFactory.CreateRejectCandidateDataAccess().GetPagedForReportRejectBySelectedColumn(request, addedFrom, addedTo, addedBy, addedBySource,
            country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
           gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, JobPostingId, checkedList);
        }
        //******************************************End*****************************************
        #endregion

        #region Resource

        Resource IFacade.AddResource(Resource resource)
        {
            return DataAccessFactory.CreateResourceDataAccess().Add(resource);
        }

        Resource IFacade.UpdateResource(Resource resource)
        {
            return DataAccessFactory.CreateResourceDataAccess().Update(resource);
        }

        Resource IFacade.GetResourceByResourceID(int resourceID)
        {
            return DataAccessFactory.CreateResourceDataAccess().GetByResourceID(resourceID);
        }

        IList<Resource> IFacade.GetAllResource()
        {
            return DataAccessFactory.CreateResourceDataAccess().GetAll();
        }

        bool IFacade.DeleteResourceByResourceID(int resourceID)
        {
            return DataAccessFactory.CreateResourceDataAccess().DeleteByResourceID(resourceID);
        }

        Resource IFacade.GetResourceByResourceName(string resourceName)
        {
            return DataAccessFactory.CreateResourceDataAccess().GetByResourceName(resourceName);
        }

        #endregion

        #region SavedQuery

        SavedQuery IFacade.AddSavedQuery(SavedQuery savedQuery)
        {
            return DataAccessFactory.CreateSavedQueryDataAccess().Add(savedQuery);
        }

        SavedQuery IFacade.UpdateSavedQuery(SavedQuery savedQuery)
        {
            return DataAccessFactory.CreateSavedQueryDataAccess().Update(savedQuery);
        }

        SavedQuery IFacade.GetSavedQueryById(int id)
        {
            return DataAccessFactory.CreateSavedQueryDataAccess().GetById(id);
        }

        IList<SavedQuery> IFacade.GetAllSavedQuery()
        {
            return DataAccessFactory.CreateSavedQueryDataAccess().GetAll();
        }

        bool IFacade.DeleteSavedQueryById(int id)
        {
            return DataAccessFactory.CreateSavedQueryDataAccess().DeleteById(id);
        }

        PagedResponse<SavedQuery> IFacade.GetPagedSavedQuery(PagedRequest request)
        {
            return DataAccessFactory.CreateSavedQueryDataAccess().GetPaged(request);
        }

        #endregion

        #region SearchAgentEmailTemplate
        SearchAgentEmailTemplate IFacade.AddSearchAgentEmailTemplate(SearchAgentEmailTemplate template)
        {
            return DataAccessFactory.CreateSearchAgentEmailTemplateDataAccess().Add(template);
        }

        SearchAgentEmailTemplate IFacade.UpdateSearchAgentEmailTemplate(SearchAgentEmailTemplate template)
        {
            return DataAccessFactory.CreateSearchAgentEmailTemplateDataAccess().Update(template);
        }

        SearchAgentEmailTemplate IFacade.GetSearchAgentEmailTemplateById(int id)
        {
            return DataAccessFactory.CreateSearchAgentEmailTemplateDataAccess().GetById(id);
        }

        SearchAgentEmailTemplate IFacade.GetSearchAgentEmailTemplateByJobPostingId(int JobPostingId)
        {
            return DataAccessFactory.CreateSearchAgentEmailTemplateDataAccess().GetByJobPostingId(JobPostingId);
        }

        bool IFacade.DeleteSearchAgentEmailTemplateByJobPostingId(int JobPostingId)
        {
            return DataAccessFactory.CreateSearchAgentEmailTemplateDataAccess().DeleteSearchAgentEmailTemplateByJobPostingId(JobPostingId);
        }
        #endregion

        #region SearchAgentSchedule

        SearchAgentSchedule IFacade.AddSearchAgentSchedule(SearchAgentSchedule SearchAgent)
        {
            return DataAccessFactory.CreateSearchAgentScheduleDataAccess().Add(SearchAgent);
        }

        SearchAgentSchedule IFacade.UpdateSearchAgentSchedule(SearchAgentSchedule SearchAgent)
        {
            return DataAccessFactory.CreateSearchAgentScheduleDataAccess().Update(SearchAgent);
        }

        SearchAgentSchedule IFacade.GetSearchAgentScheduleById(int id)
        {
            return DataAccessFactory.CreateSearchAgentScheduleDataAccess().GetById(id);
        }

        bool IFacade.DeleteSearchAgentScheduleByJobPostingId(int JobPostingId)
        {
            return DataAccessFactory.CreateSearchAgentScheduleDataAccess().DeleteByJobPostingId(JobPostingId);
        }

        IList<SearchAgentSchedule> IFacade.GetAllSearchAgentSchedule()
        {
            return DataAccessFactory.CreateSearchAgentScheduleDataAccess().GetAll();
        }

        SearchAgentSchedule IFacade.GetSearchAgentScheduleByJobPostingId(int JobPostingId)
        {
            return DataAccessFactory.CreateSearchAgentScheduleDataAccess().GetByJobPostingId(JobPostingId);
        }

        void IFacade.AddCandidateForMail(SearchAgentMailToCandidate Candidate)
        {
            DataAccessFactory.CreateSearchAgentScheduleDataAccess().AddCandidateForMail(Candidate);
        }
        #endregion

        #region SiteSetting

        SiteSetting IFacade.AddSiteSetting(SiteSetting siteSetting)
        {
            return DataAccessFactory.CreateSiteSettingDataAccess().Add(siteSetting);
        }

        SiteSetting IFacade.UpdateSiteSetting(SiteSetting siteSetting)
        {
            return DataAccessFactory.CreateSiteSettingDataAccess().Update(siteSetting);
        }

        SiteSetting IFacade.GetSiteSettingById(int id)
        {
            return DataAccessFactory.CreateSiteSettingDataAccess().GetById(id);
        }

        IList<SiteSetting> IFacade.GetAllSiteSetting()
        {
            return DataAccessFactory.CreateSiteSettingDataAccess().GetAll();
        }

        bool IFacade.DeleteSiteSettingById(int id)
        {
            return DataAccessFactory.CreateSiteSettingDataAccess().DeleteById(id);
        }

        SiteSetting IFacade.GetSiteSettingBySettingType(int settingType)
        {
            return DataAccessFactory.CreateSiteSettingDataAccess().GetBySettingType(settingType);
        }

        //2.4 starts
        public SiteSetting GetSiteSettingBySettingType(int settingType)
        {
            return DataAccessFactory.CreateSiteSettingDataAccess().GetBySettingType(settingType);
        }
        //2.4 ends
        #endregion

        #region Skill

        Skill IFacade.AddSkill(Skill skill)
        {
            return DataAccessFactory.CreateSkillDataAccess().Add(skill);
        }

        Skill IFacade.UpdateSkill(Skill skill)
        {
            return DataAccessFactory.CreateSkillDataAccess().Update(skill);
        }

        Skill IFacade.GetSkillById(int id)
        {
            return DataAccessFactory.CreateSkillDataAccess().GetById(id);
        }

        IList<Skill> IFacade.GetAllSkill()
        {
            return DataAccessFactory.CreateSkillDataAccess().GetAll();
        }

        IList<Skill> IFacade.GetAllParentSkill()
        {
            return DataAccessFactory.CreateSkillDataAccess().GetAllParent();
        }

        PagedResponse<Skill> IFacade.GetPagedSkill(PagedRequest request)
        {
            return DataAccessFactory.CreateSkillDataAccess().GetPaged(request);
        }

        bool IFacade.DeleteSkillById(int id)
        {
            return DataAccessFactory.CreateSkillDataAccess().DeleteById(id);
        }

        IList<Skill> IFacade.GetAllSkillByParentId(int parentId, string searchSkill)
        {
            return DataAccessFactory.CreateSkillDataAccess().GetAllSkillByParentId(parentId, searchSkill);
        }

        Int32 IFacade.GetSkillIdBySkillName(string skill)
        {
            return DataAccessFactory.CreateSkillDataAccess().GetSkillIdBySkillName(skill);
        }
        IList<Skill> IFacade.GetAllSkillsBySearch(string keyword, int count)
        {
            return DataAccessFactory.CreateSkillDataAccess().GetAllBySearch(keyword, count);
        }

        string IFacade.Skill_GetandAddSkillIds(string skillxml, int CreatorId)
        {
            return DataAccessFactory.CreateSkillDataAccess().GetAndAddSkillIds(skillxml, CreatorId);
        }

        #endregion

        #region State

        State IFacade.AddState(State state)
        {
            return DataAccessFactory.CreateStateDataAccess().Add(state);
        }

        State IFacade.UpdateState(State state)
        {
            return DataAccessFactory.CreateStateDataAccess().Update(state);
        }

        State IFacade.GetStateById(int id)
        {
            return DataAccessFactory.CreateStateDataAccess().GetById(id);
        }

        IList<State> IFacade.GetAllState()
        {
            return DataAccessFactory.CreateStateDataAccess().GetAll();
        }

        IList<State> IFacade.GetAllStateByCountryId(int countryId)
        {
            return DataAccessFactory.CreateStateDataAccess().GetAllByCountryId(countryId);
        }

        bool IFacade.DeleteStateById(int id)
        {
            return DataAccessFactory.CreateStateDataAccess().DeleteById(id);
        }

        Int32 IFacade.GetStateIdByStateName(string stateName)
        {
            return DataAccessFactory.CreateStateDataAccess().GetStateIdByStateName(stateName);
        }

        string IFacade.GetStateNameById(int id)
        {
            return DataAccessFactory.CreateStateDataAccess().GetStateNameById(id);
        }
        #endregion

        #region Zipcode

        IList<string> IFacade.GetAllCity(string prefixText)
        {
            return DataAccessFactory.CreateZipCodeCityDataAccess().GetAllCity(prefixText);
        }
        #endregion

        #region WebParserDomain

        WebParserDomain IFacade.AddWebParserDomain(WebParserDomain webParserDomain)
        {
            return DataAccessFactory.CreateWebParserDomainDataAccess().Add(webParserDomain);
        }

        WebParserDomain IFacade.UpdateWebParserDomain(WebParserDomain webParserDomain)
        {
            return DataAccessFactory.CreateWebParserDomainDataAccess().Update(webParserDomain);
        }

        WebParserDomain IFacade.GetWebParserDomainById(int id)
        {
            return DataAccessFactory.CreateWebParserDomainDataAccess().GetById(id);
        }

        IList<WebParserDomain> IFacade.GetAllWebParserDomain()
        {
            return DataAccessFactory.CreateWebParserDomainDataAccess().GetAll();
        }

        bool IFacade.DeleteWebParserDomainById(int id)
        {
            return DataAccessFactory.CreateWebParserDomainDataAccess().DeleteById(id);
        }

        DataTable IFacade.GetAllWebParserDomainName()
        {
            return DataAccessFactory.CreateWebParserDomainDataAccess().GetAllName();
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uname"></param>
        /// <param name="sessionId">!=0 meand save dasboard alert to map list</param>
        /// <param name="AppName">!=0 means save dashboard alert to manager</param>
        /// <param name="Mode"></param>
        /// <param name="domainname"></param>
        /// <returns></returns>
        public int EntryofUserAccessApp(string uname, string sessionId, string AppName, string DomainName, string MacId, int allowedusers, string RequestedIp)
        {
            return DataAccessFactory.CreateUserAccessApp().EntryofUserAccessApp(uname, sessionId, AppName, DomainName, MacId, allowedusers, RequestedIp);
        }

        /// </summary>
        /// <param name="uname"></param>
        /// <param name="sessionId">!=0 meand save dasboard alert to map list</param>
        /// <param name="AppName">!=0 means save dashboard alert to manager</param>
        /// <param name="Mode"></param>
        /// <param name="domainname"></param>
        /// <returns></returns>
        public int DeleteUserAccessApp(string uname, string AppName, string DomainName)
        {
            return DataAccessFactory.CreateUserAccessApp().DeleteUserAccessApp(uname, AppName, DomainName);
        }

        /// </summary>
        /// <param name="uname"></param>
        /// <param name="sessionId">!=0 meand save dasboard alert to map list</param>
        /// <param name="AppName">!=0 means save dashboard alert to manager</param>
        /// <param name="Mode"></param>
        /// <param name="domainname"></param>
        /// <returns></returns>
        public void UpdateUserAccessApp(string uname, string AppName, string DomainName)
        {
            DataAccessFactory.CreateUserAccessApp().UpdateUserAccessApp(uname, AppName, DomainName);
        }

        /// </summary>
        /// <param name="uname"></param>
        /// <param name="sessionId">!=0 meand save dasboard alert to map list</param>
        /// <param name="AppName">!=0 means save dashboard alert to manager</param>
        /// <param name="Mode"></param>
        /// <param name="domainname"></param>
        /// <returns></returns>
        public void EntryUserAccessAppReset(string uname, string AppName, string DomainName)
        {
            DataAccessFactory.CreateUserAccessApp().EntryUserAccessAppReset(uname, AppName, DomainName);
        }

        public int IsAvailableSessionId(string _SessionId)
        {
            return DataAccessFactory.CreateUserAccessApp().IsAvailableSessionId(_SessionId);
        }

        public int InsertSessionTimeout(string uname, int time, string SessionId, string AppName, string DomainName)
        {
            return DataAccessFactory.CreateUserAccessApp().InsertSessionOutTime(uname, time, SessionId, AppName, DomainName);
        }

        public int GetSessionTimeOutValue(string uname, string AppName, string DomainName)
        {
            return DataAccessFactory.CreateUserAccessApp().GetSessionTimeOutValue(uname, AppName, DomainName);
        }

        public void UpdateAspStateTempSessions(string uname, string sessionId, string AppName, string DomainName)
        {
            DataAccessFactory.CreateUserAccessApp().UpdateAspStateTempSessions(uname, sessionId, AppName, DomainName);
        }

        PagedResponse<CurrentSessions> IFacade.CurrentSessions_GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateUserAccessApp().CurrentSessions_GetPaged(request);
        }
        #endregion


        //Code introduced by Prasanth on 24/Dec/2015 Start
        #region MemberInterviewFeedbackDocument

        MemberDocument IFacade.AddMemberInterviewFeedbackDocument(MemberDocument memberDocument)
        {
            return DataAccessFactory.CreateMemberInterviewFeedbackDocumentDataAccess().Add(memberDocument);
        }

        MemberDocument IFacade.UpdateMemberInterviewFeedbackDocument(MemberDocument memberDocument)
        {
            return DataAccessFactory.CreateMemberInterviewFeedbackDocumentDataAccess().Update(memberDocument);
        }

        //MemberDocument IFacade.GetMemberInterviewFeedbackDocumentById(int id)
        //{
        //    return DataAccessFactory.CreateMemberInterviewFeedbackDocumentDataAccess().GetById(id);
        //}

        //IList<MemberDocument> IFacade.GetAllMemberInterviewFeedbackDocumentByMemberId(int memberId)
        //{
        //    return DataAccessFactory.CreateMemberInterviewFeedbackDocumentDataAccess().GetAllByMemberId(memberId);
        //}

        //IList<MemberDocument> IFacade.GetAllMemberInterviewFeedbackDocumentByTypeAndMemberId(string type, int memberId)
        //{
        //    return DataAccessFactory.CreateMemberInterviewFeedbackDocumentDataAccess().GetAllByTypeAndMemberId(type, memberId);
        //}

        //IList<MemberDocument> IFacade.GetAllMemberInterviewFeedbackDocument()
        //{
        //    return DataAccessFactory.CreateMemberInterviewFeedbackDocumentDataAccess().GetAll();
        //}

        //bool IFacade.DeleteMemberInterviewFeedbackDocumentById(int id)
        //{
        //    return DataAccessFactory.CreateMemberInterviewFeedbackDocumentDataAccess().DeleteById(id);
        //}

        //IList<MemberDocument> IFacade.GetAllMemberInterviewFeedbackDocumentByTypeAndMembersId(string membersId, string documentType)
        //{
        //    return DataAccessFactory.CreateMemberInterviewFeedbackDocumentDataAccess().GetAllByTypeAndMembersId(membersId, documentType);
        //}

        //MemberDocument IFacade.GetMemberInterviewFeedbackDocumentByMemberIdTypeAndFileName(int memberId, string Type, string fileName)

        //{
        //    return DataAccessFactory.CreateMemberInterviewFeedbackDocumentDataAccess().GetAllByMemberIDTypeAndFileName(memberId, Type, fileName);
        //}

        MemberDocument IFacade.GetAllByMemberIDInterviewIDInterviewerEmailDocumentTypeAndFileName(int memberId, int InterviewID, string InterviewerEmail, string type, string FileName)
        {
            return DataAccessFactory.CreateMemberInterviewFeedbackDocumentDataAccess().GetAllByMemberIDInterviewIDInterviewerEmailDocumentTypeAndFileName(memberId,InterviewID,InterviewerEmail, type, FileName);
        }



        //MemberDocument IFacade.GetMemberInterviewFeedbackDocumentByMemberIdAndFileName(int memberId, string fileName)
        //{
        //    return DataAccessFactory.CreateMemberInterviewFeedbackDocumentDataAccess().GetByMemberIdAndFileName(memberId, fileName);
        //}

        //bool IFacade.DeleteMemberInterviewFeedbackDocumentByMemberId(int memberId)
        //{
        //    return DataAccessFactory.CreateMemberInterviewFeedbackDocumentDataAccess().DeleteByMemberId(memberId);
        //}

        //PagedResponse<MemberDocument> IFacade.GetPagedMemberInterviewFeedbackDocumentByMemberID(int MemberId, PagedRequest request)
        //{
        //    return DataAccessFactory.CreateMemberInterviewFeedbackDocumentDataAccess().GetPagedByMemberId(MemberId, request);
        //}

       
        #endregion

        //***************************END***********************************

        //**********Code added by pravin khot on 23/Feb/2016************
        #region UserRoleMapEditor
        UserRoleMapEditor IFacade.AddUserRoleMapping(UserRoleMapEditor UserRoleMapping)
        {
            return DataAccessFactory.CreateUserRoleMapEditorDataAccess().Add(UserRoleMapping);
        }
        bool IFacade.DeleteUserRoleMappingByid(int id)
        {
            return DataAccessFactory.CreateUserRoleMapEditorDataAccess().DeleteById(id);
        }
        UserRoleMapEditor IFacade.UpdateUserRoleMapping(UserRoleMapEditor UserRoleMapping)
        {
            return DataAccessFactory.CreateUserRoleMapEditorDataAccess().Update(UserRoleMapping);
        }
        PagedResponse<UserRoleMapEditor> IFacade.UserRoleMapEditor_GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateUserRoleMapEditorDataAccess().GetPaged(request);
        }
        UserRoleMapEditor IFacade.UserRoleMapEditor_GetAllByBUMappingID(int UserRoleMappingId)
        {
            return DataAccessFactory.CreateUserRoleMapEditorDataAccess().GetAllByUserRoleMappingId(UserRoleMappingId);
        }
        ArrayList IFacade.UserRoleMapEditor_GetUserListByBuid(int BUId, int jobpostingId)
        {
            return DataAccessFactory.CreateUserRoleMapEditorDataAccess().GetUserByUserRoleid(BUId, jobpostingId);
        }
        ArrayList IFacade.UserRoleMapEditor_GetUserNameListByBuid(int BUId, int jobpostingId)
        {
            return DataAccessFactory.CreateUserRoleMapEditorDataAccess().GetUserNameByUserRoleid(BUId, jobpostingId);
        }
        Int32 IFacade.UserRoleMap_Id(int RoleId)
        {
            return DataAccessFactory.CreateUserRoleMapEditorDataAccess().UserRoleMapping_Id(RoleId);
        }
        #endregion
        //**********************************END*******************************************************

        //***********************code introduced by pravin  30/Aug/2016 ***********

        #region OnLineParser
        OnLineParser IFacade.OnLineParser_Add(OnLineParser OnLineParser)
        {
            return DataAccessFactory.CreateOnLineParserDataAccess().Add(OnLineParser);
        }
        OnLineParser IFacade.OnLineParser_Update(OnLineParser OnLineParser)
        {
            return DataAccessFactory.CreateOnLineParserDataAccess().Update(OnLineParser);
        }
        OnLineParser IFacade.OnLineParser_GetById(int id)
        {
            return DataAccessFactory.CreateOnLineParserDataAccess().GetById(id);
        }
        IList<OnLineParser> IFacade.OnLineParser_GetAll()
        {
            return DataAccessFactory.CreateOnLineParserDataAccess().GetAll();
        }
        IList<OnLineParser> IFacade.OnLineParser_GetAll(string SortExpression)
        {
            return DataAccessFactory.CreateOnLineParserDataAccess().GetAll(SortExpression);
        }
        PagedResponse<OnLineParser> IFacade.OnLineParser_GetPaged(PagedRequest request)
        {
            return DataAccessFactory.CreateOnLineParserDataAccess().GetPaged(request);
        }
        bool IFacade.OnLineParser_DeleteById(int id)
        {
            return DataAccessFactory.CreateOnLineParserDataAccess().DeleteById(id);
        }
        OnLineParser IFacade.OnLineParser_GetByPath(string path, int CurrentMemberId)
        {
            return DataAccessFactory.CreateOnLineParserDataAccess().GetByPath(path, CurrentMemberId);
        }
        Int32 IFacade.OnLineParserCount_GetByUserId(int Userid)
        {
            return DataAccessFactory.CreateOnLineParserDataAccess().ParserCount_GetByUserId(Userid);
        }
        #endregion

        //*************end******************************************

        //*********Pravin khot � Candidate Hiring Status Update � 3/March/2017 � Start *****************

        IList<CandidatesHireStatus> IFacade.CandidateHiringStatusUpdate_GetAll(int ManagerId)
        {
            return DataAccessFactory.CreateGetCandidateHireStatusDataAccess().GetCandidateListbyManagerId(ManagerId);
        }

        IList<HiringMatrixLevels> IFacade.GetAllHiringMatrixLevelsForHiringManager(int MemberID)
        {
            return DataAccessFactory.CreateHiringMatrixLevelForHiringManagersDataAccess().GetAllHiringManager(MemberID);
        }

        void IFacade.CandidateHiringStatus_Update(int ID, int JobStatus)
        {
            DataAccessFactory.CreateHireStatusUpdateDataAccess().GetUpdateCandidateStatus(ID, JobStatus);
        }

        void IFacade.CandidHiringStatusRole_Save(int Roleid, int StatusID, int creatorId, bool IsRemovedFlag)
        {
            DataAccessFactory.CreateHiringStatusRoleSaveDataAccess().GetCandidHiringStatusRoleSave(Roleid, StatusID, creatorId, IsRemovedFlag);
        }

        void IFacade.SendCandidateStatusEmail(int ID, int CandidateId, int RequsitionStatus, int ManagerID, int JobPostingId, int AdminMemberid)
        {
            DataAccessFactory.CreateSendCandidateStatusEmailDataAccess().GetSendCandidtaeStatusEmail(ID, CandidateId, RequsitionStatus, ManagerID, JobPostingId, AdminMemberid);
        }

        IList<CandidatesHireStatus> IFacade.GetAllHiringStatusasperRoleId(int RoleId)
        {
            return DataAccessFactory.CreateAllHiringStatusasperRoleId().GetAllHiringStatusRoleId(RoleId);
        }

        IList<CustomRole> IFacade.GetAllCustomUserRole()
        {
            return DataAccessFactory.CreateCustomUserRoleDataAccess().GetCustomUserRoleAll();
        }
        //*********Pravin khot � Candidate Hiring Status Update � 3/March/2017 � End *****************

     
    }
}
