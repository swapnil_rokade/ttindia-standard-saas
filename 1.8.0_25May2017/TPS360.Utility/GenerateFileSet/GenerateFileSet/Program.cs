﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using System.IO.Compression;

namespace TPS360.GenerateFileSet
{
    class Program
    {
        private static Regex _scriptTags = new Regex(@"<script\s*src\s*=\s*""(?<url>.[^""]+)"".[^>]*>\s*</script>", RegexOptions.Compiled);


        private static string[] Headers = new string[] {"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; InfoPath.2; .NET CLR 3.5.30428)", 
                                                        "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1" 
                                                        };
        static void Main(string[] args)
        {
            string siteName = string.Empty;

            if (args != null && args.Length > 0)
            {
                siteName = args[0];
            }
            else
            {
                Console.WriteLine("Please specify the site url.");
                siteName = Console.ReadLine();
            }

            XDocument settingXML = XDocument.Load("Setting.xml");

            var urlList = (from xUrl in settingXML.Descendants("url")
                           select new
                           {
                               Url = xUrl.Value
                           }).ToList();

            var patternList = (from xUrl in settingXML.Descendants("pattern")
                               where xUrl.Attributes("isDefault").Any() == false
                               select new
                               {
                                   Match = xUrl.Element("match").Value,
                                   Set = xUrl.Element("set").Value,
                                   SetPrefix = xUrl.Element("setPrefix").Value
                               }).ToList();

            var defaultpattern = (from xUrl in settingXML.Descendants("pattern")
                                  where xUrl.Attributes("isDefault").Any() == true
                                  select new
                                  {
                                      Set = xUrl.Element("set").Value,
                                      SetPrefix = xUrl.Element("setPrefix").Value
                                  }).SingleOrDefault();

            List<UrlMapSet> urlMapSetList = new List<UrlMapSet>();


            foreach (var siteUrl in urlList)
            {
                string pageToHit = siteName + ConfigurationManager.AppSettings["applicationPath"] + siteUrl.Url;

                for (int i = 0; i < Headers.Length; i++)
                {
                    string output = GetUrl(pageToHit, null, true, Headers[i]);

                    MatchCollection matches = _scriptTags.Matches(output);

                    foreach (Match match in matches)
                    {
                        string resourceUrl = match.Groups["url"].Value;
                        string resourceUrlToHit = string.Empty;

                        if (resourceUrl.StartsWith("/"))
                        {
                            resourceUrlToHit = siteName.Remove(siteName.Length - 1, 1) + resourceUrl;
                        }
                        else if (resourceUrl.StartsWith("http://"))
                        {
                            //do nothing
                            resourceUrlToHit = resourceUrl;
                        }
                        else if (resourceUrl.StartsWith("../"))
                        {
                            resourceUrlToHit = resourceUrl.Replace("../", siteName + ConfigurationManager.AppSettings["applicationPath"]);
                        }
                        else
                        {
                            resourceUrlToHit = siteName + ConfigurationManager.AppSettings["applicationPath"] + resourceUrl;
                        }

                        string resourceOutput = GetUrl(resourceUrlToHit, null, true, Headers[i]);
                        
                        bool isMatched = false;

                        foreach (var pattern in patternList)
                        {
                            if (Regex.IsMatch(resourceOutput, pattern.Match.Trim(), RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.CultureInvariant | RegexOptions.Multiline))
                            {
                                CreateUrlMap(urlMapSetList, pattern.Set, pattern.SetPrefix, resourceUrl);
                                isMatched = true;
                                break;
                            }
                        }

                        if (!isMatched)
                        {
                            CreateUrlMap(urlMapSetList, defaultpattern.Set, defaultpattern.SetPrefix, resourceUrl);
                        }
                    }
                }
            }

            //Now Generate the XML
            XElement xml = new XElement("sets",
                    from urlMapSet in urlMapSetList
                    select new XElement("set",
                              new XAttribute("name", urlMapSet.Name),
                              new XAttribute("setPrefix", urlMapSet.setPrefix),
                              from url in urlMapSet.Urls
                              select new XElement("url",
                                        new XAttribute("name", urlMapSet.setPrefix + url.Name), url.Url)
                    ));

            string saveAs = ConfigurationManager.AppSettings["saveAs"].ToString();

            try
            {
                if (!string.IsNullOrEmpty(saveAs))
                {
                    xml.Save(saveAs);

                    Console.WriteLine("File successfully generated. Press Any Key to Continue...");
                    Console.ReadLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }


        /// <summary>
        /// Simple routine to retrieve HTTP Content as a string with
        /// optional POST data and GZip encoding.
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="PostData"></param>
        /// <param name="GZip"></param>
        /// <returns></returns>
        private static string GetUrl(string Url, string PostData, bool GZip, string userAgent)
        {
            HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(Url);

            Http.UserAgent = userAgent;

            if (GZip)
                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");

            if (!string.IsNullOrEmpty(PostData))
            {
                Http.Method = "POST";
                byte[] lbPostBuffer = Encoding.Default.GetBytes(PostData);

                Http.ContentLength = lbPostBuffer.Length;

                Stream PostStream = Http.GetRequestStream();
                PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                PostStream.Close();
            }

            HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse();

            Stream responseStream = responseStream = WebResponse.GetResponseStream();
            if (WebResponse.ContentEncoding.ToLower().Contains("gzip"))
                responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
            else if (WebResponse.ContentEncoding.ToLower().Contains("deflate"))
                responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);

            StreamReader Reader = new StreamReader(responseStream, Encoding.Default);

            string Html = Reader.ReadToEnd();

            WebResponse.Close();
            responseStream.Close();

            return Html;
        }



        private static void CreateUrlMap(List<UrlMapSet> urlMapSetList, string set, string setPrefix, string resourceUrl)
        {
            UrlMapSet existingSet = urlMapSetList.Where(u => u.Name == set).SingleOrDefault();

            if (existingSet == null)
            {
                urlMapSetList.Add(new UrlMapSet
                {
                    Name = set,
                    setPrefix = setPrefix,
                    Urls = new List<UrlMap>() { new UrlMap("1", resourceUrl) }
                });
            }
            else
            {
                if (existingSet.Urls.Where(u => u.Url.Equals(resourceUrl)).SingleOrDefault() == null)
                {
                    existingSet.Urls.Add(new UrlMap((existingSet.Urls.Count + 1).ToString(), resourceUrl));
                }
            }
        }
    }

    public class UrlMapSet
    {
        public string Name;
        public bool IsIncludeAll;
        public string setPrefix;
        public List<UrlMap> Urls = new List<UrlMap>();
    }

    public class UrlMap
    {
        public string Name;
        public string Url;

        public UrlMap(string name, string url)
        {
            this.Name = name;
            this.Url = url;
        }
    }
}
