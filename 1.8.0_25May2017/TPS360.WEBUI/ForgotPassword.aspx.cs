﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Login.aspx.cs
    Description: This is the page used for Login of the user.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               24-Sept-2008          Shivanand           Defect id: 8655; Implemented Remember me concept.
    0.2               17-Mar-2009           Jagadish            Defect id: 8655; Changed the code to remember Password.
 *  0.3               14-May-2009           Rajendra A.S        Defect id: 10465; Code modified in LoginUser_LoggedIn() event.
 *  0.4               04-Jun-2009           Veda                Defect Id:10554 ;User cannot goto login page by entering the the Login URL 
    0.5               15-Jun-2009           Veda                Defect Id:10628 ;License connection string is defined in a new section of web.config file 
 *  0.6               24-Jun-2009           Anand Dixit         Changes due to renaming of LicenseSectionHandler to TPS360Section handler are implemented
 *  0.7               08-Jul-2009           Veda                Defect Id:10871;Removed the links in Log in page.
    0.8              Jul-14-2009         Nagrathna .V.B         DefectId:9112 "Login" link has been removed.
 *  0.9               07-Oct-2009           Veda                Defect Id:11534 : Delete teh saved license details after teh expirydate
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Web.Security;

using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using TPS360.Common.Helper;
using System.Web;
using System.Configuration;
using TPS360.Common;
using System.Web.Configuration;
namespace TPS360.Web.UI
{
    public partial class ForgotPassword : BasePage
    {

    }
}
