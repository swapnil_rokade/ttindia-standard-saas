﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateReports.aspx.cs
    Description: This is the page used to display the candidate reports in excel,word,Pdf formats.
    Created By: 
    Created On:
    Modification Log: 
    --------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    --------------------------------------------------------------------------------------------------------------------------------------
    0.1                 14-May-2009         Jagadish            Defect id: 10472; Changes made in method 'GetCandidateReportTable()'.
    0.2                 Aug-10-2009         Nagarathna V.B      Defect Id:11173 interchanged currentzip code and permanetzip code.
    0.3                 Dec-09-2009         Rajendra Prasad     Defect Id:11912; Changes made in Page_Load(),BindList(),Clear();
    0.4                 Mar-24-2010         Sudarshan.R         Defect Id:12385; Changed Current City & State to Permanent City and State in lsvEmployeeList_ItemDataBound
    0.5                 March-30-2010       Prashant Tripathi   Defect Id:12418; Added new function  ClearSearchCriteria() to clear the search criteria
 *  0.6                 April-06-2010       Ganapati Bhat       Defect Id:12497; Added method ApplyDefaultsFromSiteSetting() in PrepareView()
 *  0.7                 April-07-2010       Sudarshan.R.        Defect Id:12385; Rework--- Interchanged all Permanent details with Current Details and vice versa. *  
 *  0.8                 May-06-2010         Ganapati Bhat       Defect Id:12764; Modified Code in PrepareView()
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Linq;

namespace TPS360.Web.UI.Reports
{

    public partial class CandidateReports : BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

    }
}