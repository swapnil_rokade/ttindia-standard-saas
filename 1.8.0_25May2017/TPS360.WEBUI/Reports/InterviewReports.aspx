﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="InterviewReports.aspx.cs"
    Inherits="TPS360.Web.UI.Reports.InterviewReports" Title="Interview Report" EnableEventValidation="false"
    EnableViewStateMac="false" %>
 

<%@ Register Src="~/Controls/InterviewReports.ascx" TagName="InterviewReport" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Interview Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
<ucl:InterviewReport ID="uncInterviewReport" runat="server" IsTeamReport="false" IsMyInterviewReport="false" />   
</asp:Content>
