﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using TPS360.Common.Helper;
using System.Configuration;
using System.Drawing;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.Security;

namespace TPS360.Web.UI.Reports
{
    public partial class OfferandJoined : BasePage
    {
        ////#region Member Variables
        ////bool _IsAccessToCompany = true;
        ////bool _IsAccessToCandidate = true;
        ////private static string UrlForCompany = string.Empty;
        ////private static int SitemapIdForCompany = 0;
        ////private static string UrlForCandidate = string.Empty;
        ////private static int SitemapIdForCandidate = 0;
        ////#endregion


        //#region Properties
        //string _cookiename;
        //string CookieName
        //{
        //    get { return _cookiename; }
        //    set { _cookiename = value; }
        //}
        //#endregion
        //private string AssociateRequisitionsWithEntitiesFromModule
        //{
        //get
        //{
        //    if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
        //    {
        //        return "BU";
        //    }
        //    else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
        //    {
        //        return "Account";
        //    }
        //    else
        //    {
        //        return "Vendor";
        //    }
        //}

        //}
        //#region Methods
        private bool _IsAccessToEmployee = true;
        private static string UrlForEmployee = string.Empty;
        private static int IdForSitemap = 0;

        private void GenerateOfferedReport(string format,string TypeofDetail)
        {
            //string offer="ByOffer";
            if (string.Equals(format, "word"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=Offer&JoinedReport" + TypeofDetail + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";
                if(TypeofDetail =="ByOffer")
                    Response.Output.Write(GetOfferedJoinedReportTable());
                else if (TypeofDetail == "ByEmbedded")
                    Response.Output.Write(GetOfferedEmbeddedReportTable());
                else if (TypeofDetail == "ByMech")
                    Response.Output.Write(GetOfferedMechReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "excel"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=Offer&JoinedReport" + TypeofDetail + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                if (TypeofDetail == "ByOffer")
                    Response.Output.Write(GetOfferedJoinedReportTable());
                else if (TypeofDetail == "ByEmbedded")
                    Response.Output.Write(GetOfferedEmbeddedReportTable());
                else if (TypeofDetail == "ByMech")
                    Response.Output.Write(GetOfferedMechReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PageWidth = 0;
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;

                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = null;
                if (TypeofDetail == "ByOffer")
                    downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetOfferedJoinedReportTable());
                else if(TypeofDetail=="ByEmbedded")

                 downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetOfferedEmbeddedReportTable());

                else
                     downloadBytes=pdfConverter.GetPdfBytesFromHtmlString(GetOfferedMechReportTable());
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=Offer&JoinedReport" + TypeofDetail + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }



        private string GetOfferedJoinedReportTable()
        {
            StringBuilder Offerjoin = new StringBuilder();
            MemberHiringDetailsDataSource joinedhiringdetails = new MemberHiringDetailsDataSource();
            IList<MemberOfferJoinDetails> offerjoinlist = null;
           //offerjoinlist =joinedhiringdetails.GetpagedOfferJoinedReport(OfferdateinOfferJoined.StartDate,OfferdateinOfferJoined.EndDate,OfferDeclineDateinOfferJoined.StartDate.ToString (),OfferDeclineDateinOfferJoined.EndDate.ToString (),ActualinOfferJoined.StartDate,ActualinOfferJoined.EndDate,(txtPSinOfferJoined.Text.Trim()==""?"":txtPSinOfferJoined.Text).ToString(),CreateDateinOfferJoined.StartDate.ToString(),CreateDateinOfferJoined.EndDate.ToString(),ddlCreatedByinOfferJoined.SelectedValue,ddlRequisitioninOfferJoined.SelectedValue,null,-1,-1); 
            offerjoinlist = joinedhiringdetails.GetpagedOfferJoinedReportForCommon(OfferdateinOfferJoined.StartDate, OfferdateinOfferJoined.EndDate, ActualinOfferJoined.StartDate, ActualinOfferJoined.EndDate, CreateDateinOfferJoined.StartDate, CreateDateinOfferJoined.EndDate, ddlCreatedByinOfferJoined.SelectedValue.ToString(), ddlRequisitioninOfferJoined.SelectedValue.ToString(), null, -1, -1);
            if (offerjoinlist != null)
            {
                string WebRoot = ConfigurationManager.AppSettings["WebRoot"].ToString();
                string LogoPath;
                if (string.IsNullOrEmpty(HttpRuntime.AppDomainAppVirtualPath))
                {
                    LogoPath = WebRoot;
                }
                else
                {
                    LogoPath = WebRoot + HttpRuntime.AppDomainAppVirtualPath;
                }



                Offerjoin.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

                Offerjoin.Append("    <tr style='text-align:center'>");
                //reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + LogoPath + "/Images/logo-left-75px.png' style= style='height:56px;width:56px'/></td>"); 
                Offerjoin.Append("        <td align=center  width='300' height='75' style=' text-align:center'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' height='40' width='150'/></td>");
                //Offerjoined.Append("<td style='font-weight:bold; font-size:22' colspan=" + "10" + ">Requisition Report</td>");
                Offerjoin.Append("    </tr>");

                Offerjoin.Append(" <tr>");
               // Offerjoin.Append("     <th>Create Date</th>");
                Offerjoin.Append("     <th>Req Code #</th>");
                Offerjoin.Append("     <th>Requisition</th>");
                Offerjoin.Append("     <th>Active Recruiter</th>");
                Offerjoin.Append("     <th>Offer Status</th>");
                Offerjoin.Append("     <th>Candidate ID #</th>");
                Offerjoin.Append("     <th>Candidate</th>");
                
                Offerjoin.Append("     <th>BU</th>");
                Offerjoin.Append("     <th>Date of Offer</th>");
                Offerjoin.Append("     <th>Current CTC</th>");
                Offerjoin.Append("     <th>Offered Salary</th>");
                Offerjoin.Append("     <th>Offered Position</th>");
                 Offerjoin.Append("     <th>Job Title</th>");
                 Offerjoin.Append("     <th>Supervisor</th>");
                 Offerjoin.Append("     <th>Commission/Referral (INR) (Lacs)</th>");
                Offerjoin.Append("     <th>Experience</th>");
                Offerjoin.Append("     <th>Source</th>");
                Offerjoin.Append("     <th>Source Description</th>");
                Offerjoin.Append("     <th>Actual DOJ</th>");
                Offerjoin.Append("     <th>Current Company</th>");
                Offerjoin.Append("     <th>Skills</th>");
                Offerjoin.Append("     <th>Education/Qualification</th>");
                Offerjoin.Append("     <th>Basic</th>");
                Offerjoin.Append("     <th>HRA</th>");
                Offerjoin.Append("     <th>Conveyence</th>");
                Offerjoin.Append("     <th>Special Allowance</th>");
                Offerjoin.Append("     <th>Medical</th>");
				
				Offerjoin.Append("     <th>Group Insurance</th>");
				Offerjoin.Append("     <th>Professional Tax</th>");
				Offerjoin.Append("     <th>Performance Linked Incentive</th>");
				Offerjoin.Append("     <th>E.A (Fixed)</th>");
				
                //Offerjoin.Append("     <th>Educational Allowance</th>");
                //Offerjoin.Append("     <th>LTA</th>");
                Offerjoin.Append("     <th>PF</th>");
				Offerjoin.Append("     <th>ESIC</th>");
				Offerjoin.Append("     <th>Gratuity</th>");
				Offerjoin.Append("     <th>Bonus</th>");
                //Offerjoin.Append("     <th>Maximun Annual Incentive</th>");
                //Offerjoin.Append("     <th>Sales Incentive</th>");
               // Offerjoin.Append("     <th>Car Allowance</th>");
                Offerjoin.Append("     <th>Grand Total</th>");
				Offerjoin.Append("     <th>Billable Salary (INR) (In Lacs)</th>");
                Offerjoin.Append("     <th>Billing Rate (%)</th>");
                Offerjoin.Append("     <th>Revenue (INR) (In Lacs)</th>");
                Offerjoin.Append("     <th>Job Location</th>");
               



                Offerjoin.Append(" </tr>");

                foreach (MemberOfferJoinDetails joined in offerjoinlist)
                {
                    if (joined != null)
                    {
                        Offerjoin.Append(" <tr>");
                      //  Offerjoin.Append("     <td>" + joined.CreateDate == DateTime.MinValue ?"": joined.CreateDate .ToShortDateString()+ "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.JobPostingCode .ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.JobTitle.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.ActiveRecruiterName.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.OfferStatus.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + "A" + joined.MemberId .ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.ApplicantName.ToString() + "&nbsp;</td>");
                        
                        Offerjoin.Append("     <td>" + joined.BUNAME + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + (joined.OfferedDate == DateTime.MinValue ? "" : joined.OfferedDate.ToShortDateString()) + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.CurrentCTC.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.OfferedSalary.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.OfferedPosition.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.ManagementBand.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.Band.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.CommissionPayRate.ToString() + "&nbsp;</td>");

                        Offerjoin.Append("     <td>" + joined.Experience.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.SOURCENAME.ToString() + "&nbsp;</td>");
                        if (joined.SOURCEDESCRIPTIONNAME.Contains('>'))
                        {
                            string[] SourceDesc = joined.SOURCEDESCRIPTIONNAME.ToString().Split('>');
                            Offerjoin.Append("     <td align=left>" + SourceDesc[1].ToString() + "&nbsp;</td>");
                        }
                        else
                        {
                            Offerjoin.Append("     <td align=left>" + joined.SOURCEDESCRIPTIONNAME.ToString() + "&nbsp;</td>");
                        }
                        Offerjoin.Append("     <td>" + (joined.JoiningDate == DateTime.MinValue ? "" : joined.JoiningDate.ToShortDateString()) + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.CurrentCompany.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.Skills.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.EducationalQualification.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.BasicEmbedded.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.HRAEmbedded.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.Conveyance.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.SpecialAllowance.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.Medical.ToString() + "&nbsp;</td>");
						Offerjoin.Append("     <td>" + joined.RoleAllowance.ToString() + "&nbsp;</td>");
                        //Offerjoin.Append("     <td>" + joined.EducationalAllowanceEmbedded.ToString() + "&nbsp;</td>");
						Offerjoin.Append("     <td>" + joined.SiteAllowance.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.PerformanceLinkedIncentive.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.RetentionBonus.ToString() + "&nbsp;</td>");
                        //Offerjoin.Append("     <td>" + joined.LTAEmbedded.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.PFEmbedded.ToString() + "&nbsp;</td>");
						Offerjoin.Append("     <td>" + joined.ESIC.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.GratuityEmbedded.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.Bonus.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.GrandTotal.ToString() + "&nbsp;</td>");
                        //Offerjoin.Append("     <td>" + joined.MaximumAnnualIncentive.ToString() + "&nbsp;</td>");
                        //Offerjoin.Append("     <td>" + joined.SalesIncentive.ToString() + "&nbsp;</td>");
                        //Offerjoin.Append("     <td>" + joined.CarAllowance.ToString() + "&nbsp;</td>");
                        
						Offerjoin.Append("     <td>" + joined.BillableSalary.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.BillingRate.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.Revenue.ToString() + "&nbsp;</td>");
                        Offerjoin.Append("     <td>" + joined.OfferLocation + "&nbsp;</td>");
                    }
                }

                Offerjoin.Append("    <tr>");
                //reqReport.Append("        <td align=left  width='300' height='75'><img src='" + LogoPath + "/Images/logo-left-75px.png' style= style='height:56px;width:56px;'/></td>");
                Offerjoin.Append("        <td align='center' style='width:300;height:75;' ><div style='width:150; height:40;'><img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' height=40 width = 150 /></div></td>");
                Offerjoin.Append("    </tr>");

                Offerjoin.Append(" </table>");
            }
            return Offerjoin.ToString();
        }

        private string GetOfferedEmbeddedReportTable()
        {
      
            StringBuilder Offerjoined = new StringBuilder();
            MemberHiringDetailsDataSource memberhiringdetails = new MemberHiringDetailsDataSource();
            IList<MemberHiringDetails> EmbeddedSalaryList=null;
            EmbeddedSalaryList = memberhiringdetails.GetpagedEmbeddedSalarydetailsReport(OfferdateinSalarydetails.StartDate.ToString(), OfferdateinSalarydetails.EndDate.ToString(), ActualinSalarydetails.StartDate.ToString(), ActualinSalarydetails.EndDate.ToString(), txtPSinSalarydetails.Text.Trim(), CreatedateinSalarydetails.StartDate.ToString(), CreatedateinSalarydetails.EndDate.ToString(), ddlCreatedbyinSalarydetails.SelectedValue, ddlRequisitioninSalarydetails.SelectedValue, null, -1, -1);

            if (EmbeddedSalaryList != null)
            {
                string WebRoot = ConfigurationManager.AppSettings["WebRoot"].ToString();
                string LogoPath;
                if (string.IsNullOrEmpty(HttpRuntime.AppDomainAppVirtualPath))
                {
                    LogoPath = WebRoot;
                }
                else
                {
                    LogoPath = WebRoot + HttpRuntime.AppDomainAppVirtualPath;
                }



                Offerjoined.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

                Offerjoined.Append("    <tr style='text-align:center'>");
                //reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + LogoPath + "/Images/logo-left-75px.png' style= style='height:56px;width:56px'/></td>"); 
                Offerjoined.Append("        <td align=center  width='300' height='75' style=' text-align:center'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' height='40' width='150'/></td>");
                //Offerjoined.Append("<td style='font-weight:bold; font-size:22' colspan=" + "10" + ">Requisition Report</td>");
                Offerjoined.Append("    </tr>");

                Offerjoined.Append(" <tr>");
                Offerjoined.Append("     <th>Create Date</th>");
                Offerjoined.Append("     <th>Req Code #</th>");
                Offerjoined.Append("     <th>Requisition</th>");
                Offerjoined.Append("     <th>Candidate ID</th>");
                Offerjoined.Append("     <th>Candidate</th>");
                Offerjoined.Append("     <th>Offer Status</th>");
                Offerjoined.Append("     <th>Active Recruiter</th>");
                Offerjoined.Append("     <th>Offer Category</th>");
                Offerjoined.Append("     <th>Offered Grade</th>");
                Offerjoined.Append("     <th>Basic</th>");
                Offerjoined.Append("     <th>HRA</th>");
                Offerjoined.Append("     <th>Conveyence</th>");
                Offerjoined.Append("     <th>Medical</th>");
                Offerjoined.Append("     <th>Educational Allowance</th>");
                Offerjoined.Append("     <th>LUFS</th>");
                Offerjoined.Append("     <th>Ad-hoc</th>");
                Offerjoined.Append("     <th>MPP</th>");
                Offerjoined.Append("     <th>AGVI</th>");
                Offerjoined.Append("     <th>LTA</th>");
                Offerjoined.Append("     <th>PF</th>");
                Offerjoined.Append("     <th>Gratuity</th>");
                Offerjoined.Append("     <th>Maximun Annual Incentive</th>");
                Offerjoined.Append("     <th>Embedded Allowance</th>");
                Offerjoined.Append("     <th>PLM Allowance</th>");
                Offerjoined.Append("     <th>Sales Incentive</th>");
                Offerjoined.Append("     <th>Car Allowance</th>");
                Offerjoined.Append("     <th>Grand Total</th>");
                Offerjoined.Append(" </tr>");

                foreach (MemberHiringDetails embedded in EmbeddedSalaryList)
                {
                    if (embedded != null)
                    {
                        Offerjoined.Append(" <tr>");
                        Offerjoined.Append("     <td>" + (embedded.CreateDate == DateTime.MinValue ? "" : embedded.CreateDate.ToShortDateString()) + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.JobPostingCode .ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.JobTitle.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + "A" + embedded.MemberId .ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.ApplicantName.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.OfferStatus .ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.ActiveRecruiterName  .ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.OfferCategory.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.Grade.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.BasicEmbedded.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.HRAEmbedded.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.Conveyance.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.Medical.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.EducationalAllowanceEmbedded.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.LUFS.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.Adhoc.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.MPP.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.AGVI.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.LTAEmbedded.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.PFEmbedded.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.GratuityEmbedded.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.MaximumAnnualIncentive.ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.EmbeddedAllowance .ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.PLMAllowance .ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.SalesIncentive .ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.CarAllowance .ToString() + "&nbsp;</td>");
                        Offerjoined.Append("     <td>" + embedded.GrandTotal.ToString() + "&nbsp;</td>");


                    }
                }

                Offerjoined.Append("    <tr>");
                //reqReport.Append("        <td align=left  width='300' height='75'><img src='" + LogoPath + "/Images/logo-left-75px.png' style= style='height:56px;width:56px;'/></td>");
                Offerjoined.Append("        <td align='center' style='width:300;height:75;' ><div style='width:150; height:40;'><img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' height=40 width = 150 /></div></td>");
                Offerjoined.Append("    </tr>");
               
                Offerjoined.Append(" </table>");
            }
            return Offerjoined.ToString();
        }

        private string GetOfferedMechReportTable()
        {
            StringBuilder mechdetail = new StringBuilder();
            MemberHiringDetailsDataSource mechhiringdetails = new MemberHiringDetailsDataSource();
            IList<MemberHiringDetails> MechSalaryList = null;
            MechSalaryList = mechhiringdetails.GetpagedMechSalarydetailsReport(OfferdateinSalarydetails.StartDate.ToString(), OfferdateinSalarydetails.EndDate.ToString(), ActualinSalarydetails.StartDate.ToString(), ActualinSalarydetails.EndDate.ToString(), txtPSinSalarydetails.Text.Trim(), CreatedateinSalarydetails.StartDate.ToString(), CreatedateinSalarydetails.EndDate.ToString(), ddlCreatedbyinSalarydetails.SelectedValue, ddlRequisitioninSalarydetails.SelectedValue, null, -1, -1);
            if (MechSalaryList != null)
            {
                string WebRoot = ConfigurationManager.AppSettings["WebRoot"].ToString();
                string LogoPath;
                if (string.IsNullOrEmpty(HttpRuntime.AppDomainAppVirtualPath))
                {
                    LogoPath = WebRoot;
                }
                else
                {
                    LogoPath = WebRoot + HttpRuntime.AppDomainAppVirtualPath;
                }
            }

            mechdetail.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

            mechdetail.Append("    <tr style='text-align:center'>");
            //reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + LogoPath + "/Images/logo-left-75px.png' style= style='height:56px;width:56px'/></td>"); 
            mechdetail.Append("        <td align=center  width='300' height='75' style=' text-align:center'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' height='40' width='150'/></td>");
            //Offerjoined.Append("<td style='font-weight:bold; font-size:22' colspan=" + "10" + ">Requisition Report</td>");
            mechdetail.Append("    </tr>");

            mechdetail.Append(" <tr>");
            mechdetail.Append("     <th>Create Date</th>");
            mechdetail.Append("     <th>Req code #</th>");
            mechdetail.Append("     <th>Requisition</th>");
            mechdetail.Append("     <th>Candidate ID</th>");
            mechdetail.Append("     <th>Candidate</th>");
            mechdetail.Append("     <th>Offer Status</th>");
            mechdetail.Append("     <th>Active Recruiter</th>");

            mechdetail.Append("     <th>Offer Category</th>");
            mechdetail.Append("     <th>Offered Grade</th>");
            mechdetail.Append("     <th>Basic</th>");
            mechdetail.Append("     <th>Flexi Pay 1</th>");
            mechdetail.Append("     <th>Flexi Pay 2</th>");
            mechdetail.Append("     <th>Additional Allowance</th>");
            mechdetail.Append("     <th>Ad Hoc Allowance</th>");
            mechdetail.Append("     <th>Location Allowance</th>");
            mechdetail.Append("     <th>SAF Allowance</th>");
            mechdetail.Append("     <th>HRA</th>");
            mechdetail.Append("     <th>H-LISA</th>");
            mechdetail.Append("     <th>Education Allowance</th>");
            mechdetail.Append("     <th>ACLRA</th>");
            mechdetail.Append("     <th>CLRA</th>");
            mechdetail.Append("     <th>Special Allowance</th>");
            mechdetail.Append("     <th>Special Performance Pay</th>");
            mechdetail.Append("     <th>ECAL</th>");
            mechdetail.Append("     <th>Car Mileage Reimbursement</th>");
            mechdetail.Append("     <th>Telephone Reimbursement</th>");
            mechdetail.Append("     <th>LTA</th>");
            mechdetail.Append("     <th>PF</th>");
            mechdetail.Append("     <th>Gratuity</th>");
            mechdetail.Append("     <th>Medical reimbursements</th>");
            mechdetail.Append("     <th>PC Scheme</th>");
            mechdetail.Append("     <th>Retention Pay</th>");
            mechdetail.Append("     <th>SalesIncentive</th>");
            mechdetail.Append("     <th>PLR</th>");
            mechdetail.Append("     <th>Total CTC</th>");

            mechdetail.Append(" </tr>");

            foreach (MemberHiringDetails mech in MechSalaryList)
            {
                if (MechSalaryList != null)
                {
                    mechdetail.Append(" <tr>");
                    mechdetail.Append("     <td>" + (mech.CreateDate == DateTime.MinValue ? "" : mech.CreateDate.ToShortDateString()) + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.JobPostingCode .ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.JobTitle.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + "A"+mech.MemberId .ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.ApplicantName.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.OfferStatus .ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.ActiveRecruiterName .ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.OfferCategory.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.Grade.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.BasicEmbedded.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.FlexiPay1.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.FlexiPay2.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.AdditionalAllowance.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.AdHocAllowance.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.LocationAllowance.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.SAFAllowance.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.HRAMech.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.HLISA.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.EducationAllowanceMech.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.ACLRA.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.CLRA.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.SpecialAllowance.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.SpecialPerformancePay.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.ECAL.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.CarMileageReimbursement.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.TelephoneReimbursement.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.LTAMech.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.PFMech.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.GratuityMech.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.MedicalreimbursementsDomiciliary.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.PCScheme.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.RetentionPay.ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.SalesIncentiveMech  .ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.PLRMech .ToString() + "&nbsp;</td>");
                    mechdetail.Append("     <td>" + mech.TotalCTC.ToString() + "&nbsp;</td>");


                }
            }

            mechdetail.Append("    <tr>");
            //reqReport.Append("        <td align=left  width='300' height='75'><img src='" + LogoPath + "/Images/logo-left-75px.png' style= style='height:56px;width:56px;'/></td>");
            mechdetail.Append("        <td align='center' style='width:300;height:75;' ><div style='width:150; height:40;'><img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' height=40 width = 150 /></div></td>");
            mechdetail.Append("    </tr>");
            mechdetail.Append("    <tr>");
            
            mechdetail.Append("    </tr>");
            mechdetail.Append(" </table>");

            return mechdetail.ToString();
        }

       
        void PopulateRequisition(int StatusId)
        {

            
            ddlRequisitioninOfferJoined.Items.Clear();
            ddlRequisitioninSalarydetails.Items.Clear();
            System.Collections.ArrayList arr = Facade.GetAllJobPostingListByStatusId(StatusId);
            JobPosting j = new JobPosting();
            j.Id = 0;
            j.JobTitle = "Any";
            arr.Insert(0, j);
            ddlRequisitioninOfferJoined.DataSource = arr;
            ddlRequisitioninOfferJoined.DataTextField = "JobTitle";
            ddlRequisitioninOfferJoined.DataValueField = "Id";
            ddlRequisitioninOfferJoined.DataBind();
            ddlRequisitioninSalarydetails.DataSource = arr;
            ddlRequisitioninSalarydetails.DataTextField = "JobTitle";
            ddlRequisitioninSalarydetails.DataValueField = "Id";
            ddlRequisitioninSalarydetails.DataBind();
        }

        private void PrepareView()
        {
            PopulateRequisition(0);
            MiscUtil.PopulateMemberListByRole(ddlCreatedByinOfferJoined, ContextConstants.ROLE_EMPLOYEE, Facade);
            MiscUtil.PopulateMemberListByRole(ddlCreatedbyinSalarydetails, ContextConstants.ROLE_EMPLOYEE, Facade);
        }


        private void offerclear()
        {
            OfferdateinOfferJoined.ClearRange();
            OfferDeclineDateinOfferJoined.ClearRange();
            ActualinOfferJoined.ClearRange();
            CreateDateinOfferJoined.ClearRange();
            ddlCreatedByinOfferJoined.SelectedIndex = 0;
            ddlRequisitioninOfferJoined.SelectedIndex = 0;
            txtPSinOfferJoined.Text = string.Empty;
            
        }
        private void salaryclear()
        {
            OfferdateinSalarydetails.ClearRange();
            ActualinSalarydetails.ClearRange();
            CreatedateinSalarydetails.ClearRange();
            ddlCreatedbyinSalarydetails.SelectedIndex = 0;
            ddlRequisitioninSalarydetails.SelectedIndex = 0;
            txtPSinSalarydetails.Text = string.Empty;
         
        }
       

      

      

        private void PlaceUpDownArrowforofferedandjoined()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvOfferJoined.FindControl(hdnSortColumninofferjoined.Text);
                lnk.EnableViewState = false;
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortOrderinofferjoined.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }

        private void PlaceUpDownArrowforEmbedded()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvEmbeddedSalarydetails.FindControl(hdnSortColumninembedded.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = lnk.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortOrderinembedded.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }

        private void PlaceUpDownArrowforMech()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvMechSalarydetails.FindControl(hdnSortColumninmech.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = lnk.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortOrderinmech.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }

        public void repor(string format, string TypeofDetail)
               {
                   GenerateOfferedReport(format,TypeofDetail);
               }

     
         //#endregion

        // #region Page Events
        //        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        //        {

        //            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
        //            {
        //                repor("pdf");
        //            }

        //        }
       


        protected void Page_Load(object sender, EventArgs e)
        {
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToEmployee = false;
            else
            {
                IdForSitemap = CustomMap.Id;
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }
                

            //try
            //{
            //    if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
            //    {
            //        FormsAuthentication.SignOut();
            //        FormsAuthentication.RedirectToLoginPage();
            //        return;
            //    }
            //}
            //catch { }
            if (!IsPostBack)
            {
                PrepareView();
               
            }
            

          

           
           
           
        }
       
        #region ListView

        protected void lsvOfferJoined_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            StringBuilder offercolumnoption = new StringBuilder();
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                
                MemberOfferJoinDetails offerjoined = ((ListViewDataItem)e.Item).DataItem as MemberOfferJoinDetails;

                Label lblJobPostingCode = (Label)e.Item.FindControl("lblJobPostingCode");
                lblJobPostingCode.Text = offerjoined.JobPostingCode;


                Label lblActiveRecruiter = (Label)e.Item.FindControl("lblActiveRecruiter");
                Label lblOfferStatus = (Label)e.Item.FindControl("lblOfferStatus");
                Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
                Label lblWorkLocation = (Label)e.Item.FindControl("lblWorkLocation");
                Label lblBasicofferJoined = (Label)e.Item.FindControl("lblBasicofferJoined");
                Label lblHRAofferJoined = (Label)e.Item.FindControl("lblHRAofferJoined");
                Label lblConveyenceOfferJoined = (Label)e.Item.FindControl("lblConveyenceOfferJoined");
                Label lblSpecialOfferJoined = (Label)e.Item.FindControl("lblSpecialOfferJoined");
                Label lblMedicalOfferJoined = (Label)e.Item.FindControl("lblMedicalOfferJoined");
                Label lblRoleAllowance = (Label)e.Item.FindControl("lblRoleAllowance");
                Label lblSiteOfferJoined = (Label)e.Item.FindControl("lblSiteOfferJoined");
                Label lblPerformanceOfferJoined = (Label)e.Item.FindControl("lblPerformanceOfferJoined");
                Label lblRetentionOfferJoined = (Label)e.Item.FindControl("lblRetentionOfferJoined");
                Label lblRelocationOfferJoined = (Label)e.Item.FindControl("lblRelocationOfferJoined");
                Label lblReimbursementOfferJoined = (Label)e.Item.FindControl("lblReimbursementOfferJoined");
                Label lblPFOfferJoined = (Label)e.Item.FindControl("lblPFOfferJoined");
                Label lblESICOfferJoined = (Label)e.Item.FindControl("lblESICOfferJoined");
                Label lblGratuityOfferJoined = (Label)e.Item.FindControl("lblGratuityOfferJoined");
                Label lblBonusOfferJoined = (Label)e.Item.FindControl("lblBonusOfferJoined");
                Label lblSalesIncentiveOfferJoined = (Label)e.Item.FindControl("lblSalesIncentiveOfferJoined");
                Label lblGrandTotalOfferJoined = (Label)e.Item.FindControl("lblGrandTotalOfferJoined");
                Label lblBillableSalary = (Label)e.Item.FindControl("lblBillableSalary");
                Label lblBillingRate = (Label)e.Item.FindControl("lblBillingRate");
                Label lblRevenue = (Label)e.Item.FindControl("lblRevenue");
                Label lblOfferLocation = (Label)e.Item.FindControl("lblOfferLocation");
                

               
                lblActiveRecruiter.Text = offerjoined.ActiveRecruiterName;
                lblOfferStatus.Text = offerjoined.OfferStatus;
                lblCandidateID.Text = "A" + offerjoined.MemberId .ToString();
                lblWorkLocation.Text = offerjoined.WorkLocation;

                //Label lblCreatedateinOfferJoined = (Label)e.Item.FindControl("lblCreatedateinOfferJoined");
                //lblCreatedateinOfferJoined.Text = offerjoined.CreateDate ==  DateTime.MinValue ? "" : offerjoined.CreateDate .ToShortDateString ();

                HyperLink lblRequisitioninOfferJoined = (HyperLink)e.Item.FindControl("lblRequisitioninOfferJoined");
                lblRequisitioninOfferJoined.Text = offerjoined.JobTitle.ToString();
                lblRequisitioninOfferJoined.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + offerjoined.JobPostingId.ToString() + "&FromPage=JobDetail" + "','700px','570px'); return false;");


                HyperLink lblCandidateinOfferJoined = (HyperLink)e.Item.FindControl("lblCandidateinOfferJoined");
                lblCandidateinOfferJoined.Text = offerjoined.ApplicantName.ToString();
                string strFullName = offerjoined.ApplicantName;
                if (strFullName.Trim() == "")
                {
                    strFullName = "No Candidate Name";
                }
                if (_IsAccessToEmployee)
                {
                    if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                        ControlHelper.SetHyperLink(lblCandidateinOfferJoined, UrlForEmployee, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(offerjoined.MemberId), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.ATS_OVERVIEW_SITEMAP_PARENTID);
                }
                else lblCandidateinOfferJoined.Text = strFullName;




                Label lblBUinOfferJoined = (Label)e.Item.FindControl("lblBUinOfferJoined");
                lblBUinOfferJoined.Text = offerjoined.BUNAME  ;

                Label lblDateofOfferinOfferJoined = (Label)e.Item.FindControl("lblDateofOfferinOfferJoined");
                lblDateofOfferinOfferJoined.Text = offerjoined.OfferedDate==DateTime .MinValue ? "": offerjoined.OfferedDate.ToShortDateString ();

                Label lblEDOJinOfferJoined = (Label)e.Item.FindControl("lblEDOJinOfferJoined");
                lblEDOJinOfferJoined.Text = offerjoined.JoiningDate ==DateTime .MinValue ?"": offerjoined.JoiningDate.ToShortDateString();

                Label lblCurrentCTCinOfferJoined = (Label)e.Item.FindControl("lblCurrentCTCinOfferJoined");
                lblCurrentCTCinOfferJoined.Text = offerjoined.CurrentCTC.ToString();

                Label lblOfferedCTCinOfferJoined = (Label)e.Item.FindControl("lblOfferedCTCinOfferJoined");
                lblOfferedCTCinOfferJoined.Text = offerjoined.OfferedSalary.ToString();

                Label lblOfferedPosition = (Label)e.Item.FindControl("lblOfferedPosition");
                lblOfferedPosition.Text = offerjoined.OfferedPosition.ToString();
                
                Label lblJobTitle = (Label)e.Item.FindControl("lblJobTitle");
                lblJobTitle.Text = offerjoined.ManagementBand.ToString();

                Label lblSupervisor = (Label)e.Item.FindControl("lblSupervisor");
                lblSupervisor.Text = offerjoined.Band.ToString();

                Label lblOfferedCommision = (Label)e.Item.FindControl("lblOfferedCommision");
                lblOfferedCommision.Text = offerjoined.CommissionPayRate.ToString();

                Label lblExperienceinOfferJoined = (Label)e.Item.FindControl("lblExperienceinOfferJoined");
                lblExperienceinOfferJoined.Text = offerjoined.Experience.ToString();

                Label lblSourceinOfferJoined = (Label)e.Item.FindControl("lblSourceinOfferJoined");
                lblSourceinOfferJoined.Text = offerjoined.SOURCENAME.ToString();

                Label lblSourceDescriptioninOfferJoined = (Label)e.Item.FindControl("lblSourceDescriptioninOfferJoined");
                if (offerjoined.SOURCEDESCRIPTIONNAME.Contains('>'))
                {
                    string[] SourceDesc = offerjoined.SOURCEDESCRIPTIONNAME.ToString().Split('>');
                    lblSourceDescriptioninOfferJoined.Text = SourceDesc[1].ToString();
                }
                else
                {
                    lblSourceDescriptioninOfferJoined.Text = offerjoined.SOURCEDESCRIPTIONNAME;
                }



                 Label lblActualDOJinOfferJoined = (Label)e.Item.FindControl("lblActualDOJinOfferJoined");
                 lblActualDOJinOfferJoined.Text = offerjoined.ActualDateOfJoining == DateTime.MinValue ? "" : offerjoined.ActualDateOfJoining.ToShortDateString ();

                 Label lblCurrentCompanyinOfferJoined = (Label)e.Item.FindControl("lblCurrentCompanyinOfferJoined");
                lblCurrentCompanyinOfferJoined.Text = offerjoined.CurrentCompany;

                // Label lblWorkLocationinOfferJoined = (Label)e.Item.FindControl("lblWorkLocationinOfferJoined");
                //lblWorkLocationinOfferJoined.Text = offerjoined.WorkLocation;

                 Label lblSkillsinOfferJoined = (Label)e.Item.FindControl("lblSkillsinOfferJoined");
                lblSkillsinOfferJoined.Text = offerjoined.Skills.ToString().Trim().TrimEnd(',');

                 Label lblEducationQualificationinOfferJoined = (Label)e.Item.FindControl("lblEducationQualificationinOfferJoined");
                lblEducationQualificationinOfferJoined.Text = offerjoined.EducationalQualification;

                lblBasicofferJoined.Text = offerjoined.BasicEmbedded.ToString();
                lblHRAofferJoined.Text = offerjoined.HRAEmbedded.ToString();
                lblConveyenceOfferJoined.Text = offerjoined.Conveyance.ToString();
                lblSpecialOfferJoined.Text = offerjoined.SpecialAllowance.ToString();
                lblMedicalOfferJoined.Text = offerjoined.Medical.ToString();
                lblRoleAllowance.Text = offerjoined.RoleAllowance.ToString();
                lblSiteOfferJoined.Text = offerjoined.SiteAllowance.ToString();
                lblPerformanceOfferJoined.Text = offerjoined.PerformanceLinkedIncentive.ToString();
                lblRetentionOfferJoined.Text = offerjoined.RetentionBonus.ToString();
                lblRelocationOfferJoined.Text = offerjoined.RelocationAllowanceForCom.ToString();
                lblReimbursementOfferJoined.Text = offerjoined.Reimbursement.ToString();
                lblPFOfferJoined.Text = offerjoined.PFEmbedded.ToString();
                lblESICOfferJoined.Text = offerjoined.ESIC.ToString();
                lblGratuityOfferJoined.Text = offerjoined.GratuityEmbedded.ToString();
                lblBonusOfferJoined.Text = offerjoined.Bonus.ToString();
                lblSalesIncentiveOfferJoined.Text = offerjoined.SalesIncentive.ToString();
                lblGrandTotalOfferJoined.Text = offerjoined.GrandTotal.ToString();
                lblBillableSalary.Text = offerjoined.BillableSalary.ToString();
                lblBillingRate.Text = offerjoined.BillingRate.ToString();
                lblRevenue.Text = offerjoined.Revenue.ToString();
                lblOfferLocation.Text = offerjoined.OfferLocation.ToString();
                



            }
        }
        protected void lsvOfferJoined_ItemCommand(object sender,ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumninofferjoined.Text == lnkbutton.ID)
                    {
                        if (hdnSortOrderinofferjoined.Text == "ASC") hdnSortOrderinofferjoined.Text = "DESC";
                        else hdnSortOrderinofferjoined.Text = "ASC";
                    }
                    else
                    {
                        hdnSortColumninofferjoined.Text = lnkbutton.ID;
                        hdnSortOrderinofferjoined.Text = "ASC";
                    }


                    //if (SortColumninofferjoined.Text == string.Empty || SortColumninofferjoined.Text != e.CommandArgument.ToString())
                    //    SortOrderinofferjoined.Text = "asc";
                    //else SortOrderinofferjoined.Text = SortOrderinofferjoined.Text.ToLower() == "asc" ? "desc" : "asc";
                    //odsinOfferJoined.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                    
                }

            }
            catch 
            {
            }
            
         }
        protected void lsvOfferJoined_PreRender(object sender,EventArgs e)
        {
        ExportButtonsinOfferJoined.Visible = lsvOfferJoined.Items.Count > 0;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvOfferJoined .FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "OfferJoinedReportReportRowPerPage";
            }

            if (IsPostBack)
            {
                if (PagerControl == null)
                   
                {
                    lsvOfferJoined.Items.Clear();
                    lsvOfferJoined.DataSource = null;
                    lsvOfferJoined.DataBind();
                    ExportButtonsinOfferJoined.Visible = lsvOfferJoined.Items.Count > 0;
                }
            }
            PlaceUpDownArrowforofferedandjoined();
            
        }



        protected void lsvEmbeddedSalarydetails_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            StringBuilder ColumnOption = new StringBuilder();
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberHiringDetails Embeddedsalary = ((ListViewDataItem)e.Item).DataItem as MemberHiringDetails;


                Label lblJobPostingCode = (Label)e.Item.FindControl("lblJobPostingCode");
                lblJobPostingCode.Text = Embeddedsalary.JobPostingCode;


                Label lblActiveRecruiter = (Label)e.Item.FindControl("lblActiveRecruiter");
                Label lblOfferStatus = (Label)e.Item.FindControl("lblOfferStatus");
                Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
               


                lblActiveRecruiter.Text = Embeddedsalary.ActiveRecruiterName;
                lblOfferStatus.Text = Embeddedsalary.OfferStatus;
                lblCandidateID.Text = "A"+ Embeddedsalary.MemberId .ToString();
               

                Label lblCreatedateinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblCreatedateinEmbeddedSalarydetails");
                lblCreatedateinEmbeddedSalarydetails.Text = Embeddedsalary.CreateDate.ToShortDateString();

                HyperLink lblRequisitioninEmbeddedSalarydetails = (HyperLink)e.Item.FindControl("lblRequisitioninEmbeddedSalarydetails");
                lblRequisitioninEmbeddedSalarydetails.Text = Embeddedsalary.JobTitle.ToString();
                lblRequisitioninEmbeddedSalarydetails.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + Embeddedsalary.JobPostingId.ToString() + "&FromPage=JobDetail" + "','700px','570px'); return false;");


                HyperLink lblCandidateinEmbeddedSalarydetails = (HyperLink)e.Item.FindControl("lblCandidateinEmbeddedSalarydetails");
                lblCandidateinEmbeddedSalarydetails.Text = Embeddedsalary.ApplicantName.ToString();
                string strFullName = Embeddedsalary.ApplicantName;
                if (strFullName.Trim() == "")
                {
                    strFullName = "No Candidate Name";
                }
                if (_IsAccessToEmployee)
                {
                    if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                        ControlHelper.SetHyperLink(lblCandidateinEmbeddedSalarydetails, UrlForEmployee, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(Embeddedsalary.MemberId), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.ATS_OVERVIEW_SITEMAP_PARENTID);
                }
                else lblCandidateinEmbeddedSalarydetails.Text = strFullName;


                Label lblOfferCategoryinEmbeddedSalryDetails = (Label)e.Item.FindControl("lblOfferCategoryinEmbeddedSalryDetails");
                lblOfferCategoryinEmbeddedSalryDetails.Text = "Embedded";

                Label lblOfferedgradeinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblOfferedgradeinEmbeddedSalarydetails");
                lblOfferedgradeinEmbeddedSalarydetails.Text = Embeddedsalary.Grade.ToString();

                Label lblBasicinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblBasicinEmbeddedSalarydetails");
                lblBasicinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.BasicEmbedded).ToString();

                Label lblHRAinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblHRAinEmbeddedSalarydetails");
                lblHRAinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.HRAEmbedded).ToString();

                Label lblConveyenceinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblConveyenceinEmbeddedSalarydetails");
                lblConveyenceinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.Conveyance).ToString();

                Label lblMedicalinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblMedicalinEmbeddedSalarydetails");
                lblMedicalinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.Medical).ToString();

                Label lblEducationalAllowanceinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblEducationalAllowanceinEmbeddedSalarydetails");
                lblEducationalAllowanceinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.EducationalAllowanceEmbedded).ToString();

                Label lblLUFSinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblLUFSinEmbeddedSalarydetails");
                lblLUFSinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.LUFS).ToString();

                Label lblAdhocinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblAdhocinEmbeddedSalarydetails");
                lblAdhocinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.Adhoc).ToString();

                Label lblMPPinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblMPPinEmbeddedSalarydetails");
                lblMPPinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.MPP).ToString();

                Label lblAGVIinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblAGVIinEmbeddedSalarydetails");
                lblAGVIinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.AGVI).ToString();

                Label lblLTAinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblLTAinEmbeddedSalarydetails");
                lblLTAinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.LTAEmbedded).ToString();

                Label lblPFinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblPFinEmbeddedSalarydetails");
                lblPFinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.PFEmbedded).ToString();

                Label lblGratuityinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblGratuityinEmbeddedSalarydetails");
                lblGratuityinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.GratuityEmbedded).ToString();

                Label lblMaximunAnnualIncentiveinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblMaximunAnnualIncentiveinEmbeddedSalarydetails");
                lblMaximunAnnualIncentiveinEmbeddedSalarydetails.Text =Convert.ToInt32(Embeddedsalary.MaximumAnnualIncentive).ToString();

                Label lblEmbeddedAllowanceinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblEmbeddedAllowanceinEmbeddedSalarydetails");
                lblEmbeddedAllowanceinEmbeddedSalarydetails.Text =Convert.ToInt32 (Embeddedsalary.EmbeddedAllowance).ToString();

                Label lblPLMAllowanceinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblPLMAllowanceinEmbeddedSalarydetails");
                lblPLMAllowanceinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.PLMAllowance).ToString();

                Label lblSalesIncentiveinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblSalesIncentiveinEmbeddedSalarydetails");
                lblSalesIncentiveinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.SalesIncentive).ToString();

                Label lblCarAllowanceinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblCarAllowanceinEmbeddedSalarydetails");
                lblCarAllowanceinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.CarAllowance).ToString();


                Label lblGrandTotalinEmbeddedSalarydetails = (Label)e.Item.FindControl("lblGrandTotalinEmbeddedSalarydetails");
                lblGrandTotalinEmbeddedSalarydetails.Text = Convert.ToInt32(Embeddedsalary.GrandTotal).ToString();


            }

        }
        protected void lsvEmbeddedSalarydetails_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumninembedded.Text == lnkbutton.ID)
                    {
                        if (hdnSortOrderinembedded.Text == "ASC") hdnSortOrderinembedded.Text = "DESC";
                        else hdnSortOrderinembedded.Text = "ASC";
                    }
                    else
                    {
                        hdnSortColumninembedded.Text = lnkbutton.ID;
                        hdnSortOrderinembedded.Text = "ASC";
                    }


                    //if (SortColumninofferjoined.Text == string.Empty || SortColumninofferjoined.Text != e.CommandArgument.ToString())
                    //    SortOrderinofferjoined.Text = "asc";
                    //else SortOrderinofferjoined.Text = SortOrderinofferjoined.Text.ToLower() == "asc" ? "desc" : "asc";
                    //odsinOfferJoined.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();

                }

            }
            catch
            {
            }
        }
        protected void lsvEmbeddedSalarydetails_PreRender(object sender, EventArgs e)
        {
            EmbeddedExportButton.Visible = lsvEmbeddedSalarydetails.Items.Count > 0;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvEmbeddedSalarydetails .FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "EmbeddedReportReportRowPerPage";
            }
           
            
            if (IsPostBack)
            {
                if (PagerControl == null)
                    //if (lsvEmbeddedSalarydetails.Items.Count == 0)
                {
                    lsvEmbeddedSalarydetails.Items.Clear();
                    lsvEmbeddedSalarydetails.DataSource = null;
                    lsvEmbeddedSalarydetails.DataBind();
                    EmbeddedExportButton.Visible = lsvEmbeddedSalarydetails.Items.Count > 0;
                }
            }
            PlaceUpDownArrowforEmbedded();
        }

        protected void lsvMechSalarydetails_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            StringBuilder ColumnOption = new StringBuilder();
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberHiringDetails Mechdetails = ((ListViewDataItem)e.Item).DataItem as MemberHiringDetails;
                Label lblJobPostingCode = (Label)e.Item.FindControl("lblJobPostingCode");
                lblJobPostingCode.Text = Mechdetails.JobPostingCode;



                Label lblActiveRecruiter = (Label)e.Item.FindControl("lblActiveRecruiter");
                Label lblOfferStatus = (Label)e.Item.FindControl("lblOfferStatus");
                Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
              

                lblActiveRecruiter.Text = Mechdetails.ActiveRecruiterName;
                lblOfferStatus.Text = Mechdetails.OfferStatus;
                lblCandidateID.Text = "A" + Mechdetails.MemberId .ToString();
               


                Label lblCreatedateinMechSalarydetails = (Label)e.Item.FindControl("lblCreatedateinMechSalarydetails");
                lblCreatedateinMechSalarydetails.Text = Mechdetails.CreateDate.ToShortDateString();

                HyperLink lblRequisitioninMechSalarydetails = (HyperLink)e.Item.FindControl("lblRequisitioninMechSalarydetails");
                lblRequisitioninMechSalarydetails.Text = Mechdetails.JobTitle.ToString();
                lblRequisitioninMechSalarydetails.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + Mechdetails.JobPostingId.ToString() + "&FromPage=JobDetail" + "','700px','570px'); return false;");

                HyperLink lblCandidateinMechSalarydetails = (HyperLink)e.Item.FindControl("lblCandidateinMechSalarydetails");
                lblCandidateinMechSalarydetails.Text = Mechdetails.ApplicantName.ToString();

                string strFullName = Mechdetails.ApplicantName;
                if (strFullName.Trim() == "")
                {
                    strFullName = "No Candidate Name";
                }
                if (_IsAccessToEmployee)
                {
                    if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                        ControlHelper.SetHyperLink(lblCandidateinMechSalarydetails, UrlForEmployee, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(Mechdetails.MemberId), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.ATS_OVERVIEW_SITEMAP_PARENTID);
                }
                else lblCandidateinMechSalarydetails.Text = strFullName;



                Label lblOfferCategoryinMechSalarydetails = (Label)e.Item.FindControl("lblOfferCategoryinMechSalarydetails");
                lblOfferCategoryinMechSalarydetails.Text = "Mech";

                Label lblOfferedGradeinMechSalarydetails = (Label)e.Item.FindControl("lblOfferedGradeinMechSalarydetails");
                lblOfferedGradeinMechSalarydetails.Text = Mechdetails.Grade.ToString();

                Label lblBasicinMechSalarydetails = (Label)e.Item.FindControl("lblBasicinMechSalarydetails");
                lblBasicinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.BasicMech).ToString();

                Label lblFlexiPay1inMechSalarydetails = (Label)e.Item.FindControl("lblFlexiPay1inMechSalarydetails");
                lblFlexiPay1inMechSalarydetails.Text = Convert.ToInt32(Mechdetails.FlexiPay1).ToString();

                Label lblFlexiPay2inMechSalarydetails = (Label)e.Item.FindControl("lblFlexiPay2inMechSalarydetails");
                lblFlexiPay2inMechSalarydetails.Text = Convert.ToInt32(Mechdetails.FlexiPay2).ToString();


                Label lblAdditionalAllowanceinMechSalarydetails = (Label)e.Item.FindControl("lblAdditionalAllowanceinMechSalarydetails");
                lblAdditionalAllowanceinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.AdditionalAllowance).ToString();

                Label lblAdHocAllowanceinMechSalarydetails = (Label)e.Item.FindControl("lblAdHocAllowanceinMechSalarydetails");
                lblAdHocAllowanceinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.AdHocAllowance).ToString();

                Label lblLocationAllowanceinMechSalarydetails = (Label)e.Item.FindControl("lblLocationAllowanceinMechSalarydetails");
                lblLocationAllowanceinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.LocationAllowance).ToString();

                Label lblSAFAllowanceinMechSalarydetails = (Label)e.Item.FindControl("lblSAFAllowanceinMechSalarydetails");
                lblSAFAllowanceinMechSalarydetails.Text =Convert.ToInt32 (Mechdetails.SAFAllowance).ToString();

                Label lblHRAinMechSalarydetails = (Label)e.Item.FindControl("lblHRAinMechSalarydetails");
                lblHRAinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.HRAMech).ToString();

                Label lblHLISAinMechSalarydetails = (Label)e.Item.FindControl("lblHLISAinMechSalarydetails");
                lblHLISAinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.HLISA).ToString();

                Label lblEducationAllowanceinMechSalarydetails = (Label)e.Item.FindControl("lblEducationAllowanceinMechSalarydetails");
                lblEducationAllowanceinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.EducationAllowanceMech).ToString();

                Label lblACLRAinMechSalarydetails = (Label)e.Item.FindControl("lblACLRAinMechSalarydetails");
                lblACLRAinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.ACLRA).ToString();

                Label lblCLRAinMechSalarydetails = (Label)e.Item.FindControl("lblCLRAinMechSalarydetails");
                lblCLRAinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.CLRA).ToString();

                Label lblSpecialAllowanceinMechSalarydetails = (Label)e.Item.FindControl("lblSpecialAllowanceinMechSalarydetails");
                lblSpecialAllowanceinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.SpecialAllowance).ToString();

                Label lblSpecialPerformancePayinMechSalarydetails = (Label)e.Item.FindControl("lblSpecialPerformancePayinMechSalarydetails");
                lblSpecialPerformancePayinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.SpecialPerformancePay).ToString();

                Label lblECALinMechSalarydetails = (Label)e.Item.FindControl("lblECALinMechSalarydetails");
                lblECALinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.ECAL).ToString();

                Label lblCarMileageReimbursementinMechSalarydetails = (Label)e.Item.FindControl("lblCarMileageReimbursementinMechSalarydetails");
                lblCarMileageReimbursementinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.CarMileageReimbursement).ToString();

                Label lblTelephoneReimbursementinMechSalarydetails = (Label)e.Item.FindControl("lblTelephoneReimbursementinMechSalarydetails");
                lblTelephoneReimbursementinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.TelephoneReimbursement).ToString();



                Label lblLTAinMechSalarydetails = (Label)e.Item.FindControl("lblLTAinMechSalarydetails");
                lblLTAinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.LTAMech).ToString();

                Label lblPFinMechSalarydetails = (Label)e.Item.FindControl("lblPFinMechSalarydetails");
                lblPFinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.PFMech).ToString();

                Label lblGratuityinMechSalarydetails = (Label)e.Item.FindControl("lblGratuityinMechSalarydetails");
                lblGratuityinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.GratuityMech).ToString();

                Label lblMedicalreimbursementsinMechSalarydetails = (Label)e.Item.FindControl("lblMedicalreimbursementsinMechSalarydetails");
                lblMedicalreimbursementsinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.MedicalreimbursementsDomiciliary).ToString();

                Label lblPCSchemeinMechSalarydetails = (Label)e.Item.FindControl("lblPCSchemeinMechSalarydetails");
                lblPCSchemeinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.PCScheme).ToString();

                Label lblRetentionPayinMechSalarydetails = (Label)e.Item.FindControl("lblRetentionPayinMechSalarydetails");
                lblRetentionPayinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.RetentionPay).ToString();

                Label lblPLRinMechSalarydetails = (Label)e.Item.FindControl("lblPLRinMechSalarydetails");
                lblPLRinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.PLRMech).ToString();

                Label lblSalesIncentiveinMechSalarydetails = (Label)e.Item.FindControl("lblSalesIncentiveinMechSalarydetails");
                lblSalesIncentiveinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.SalesIncentiveMech).ToString();

                Label lblTotalCTCinMechSalarydetails = (Label)e.Item.FindControl("lblTotalCTCinMechSalarydetails");
                lblTotalCTCinMechSalarydetails.Text = Convert.ToInt32(Mechdetails.TotalCTC).ToString();
                
            }
        }
        protected void lsvMechSalarydetails_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
             try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumninmech.Text == lnkbutton.ID)
                    {
                        if (hdnSortOrderinmech.Text == "ASC") hdnSortOrderinmech.Text = "DESC";
                        else hdnSortOrderinmech.Text = "ASC";
                    }
                    else
                    {
                        hdnSortColumninmech.Text = lnkbutton.ID;
                        hdnSortOrderinmech.Text = "ASC";
                    }


                    //if (SortColumninofferjoined.Text == string.Empty || SortColumninofferjoined.Text != e.CommandArgument.ToString())
                    //    SortOrderinofferjoined.Text = "asc";
                    //else SortOrderinofferjoined.Text = SortOrderinofferjoined.Text.ToLower() == "asc" ? "desc" : "asc";
                    //odsinOfferJoined.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                    
                }

            }
            catch 
            {
            }
        }
        protected void lsvMechSalarydetails_PreRender(object sender, EventArgs e)
        {
           MechExportButton.Visible = lsvOfferJoined.Items.Count > 0;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvMechSalarydetails .FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "MechReportReportRowPerPage";
            }
           
            if (IsPostBack)
            {
                if (PagerControl == null)
                {
                    lsvMechSalarydetails.Items.Clear();
                    lsvMechSalarydetails.DataSource = null;
                    lsvMechSalarydetails.DataBind();
                    MechExportButton.Visible = lsvMechSalarydetails.Items.Count > 0;
                }
            }
            PlaceUpDownArrowforMech();
        }



        protected void btnExportToExcelOfferJoined_Click(object sender, EventArgs e)
        {
            GenerateOfferedReport("excel", "ByOffer");
        }

        protected void btnExportToPDFOfferJoined_Click(object sender, EventArgs e)
        {
            GenerateOfferedReport("pdf", "ByOffer");
        }

        protected void btnExportToWordOfferJoined_Click(object sender, EventArgs e)
        {
            GenerateOfferedReport("word", "ByOffer");
        }




        
        protected void btnExportToExcelEmbeddedSalarydetails_Click(object sender, EventArgs e)
        {
           
            GenerateOfferedReport("excel","ByEmbedded");
        }

        protected void btnExportToPDFEmbeddedSalarydetails_Click(object sender, EventArgs e)
        {
            GenerateOfferedReport("pdf", "ByEmbedded");
        }

        protected void btnExportToWordEmbeddedSalarydetails_Click(object sender, EventArgs e)
        {
            GenerateOfferedReport("word", "ByEmbedded");
        }






        protected void btnExportToExcelMechSalarydetails_Click(object sender, EventArgs e)
        {
            GenerateOfferedReport("excel","ByMech");
        }

        protected void btnExportToPDFMechSalarydetails_Click(object sender, EventArgs e)
        {
            GenerateOfferedReport("pdf","ByMech");
        }


        protected void btnExportToWordMechSalarydetails_Click(object sender, EventArgs e)
        {
            GenerateOfferedReport("word","ByMech");
        }




        protected void btnSearchofferjoined_Click(object sender, EventArgs e)
        {
            try
            {
                lsvOfferJoined.DataBind();
            }
            catch { }
            
        }

        protected void btnClearofferjoined_Click(object sender, EventArgs e)
        {
            ExportButtonsinOfferJoined.Visible = false;
            offerclear();
            lsvOfferJoined.DataBind();
          
        }

        

              

        protected void btnSearchsalarydetail_Click(object sender, EventArgs e)
        {
            lsvEmbeddedSalarydetails.DataBind();
            lsvMechSalarydetails.DataBind();
        }

        protected void btnClearsalarydetail_Click(object sender, EventArgs e)
        {
            salaryclear();
            EmbeddedExportButton.Visible = false;
            MechExportButton.Visible = false;
            lsvEmbeddedSalarydetails.DataBind();
            lsvMechSalarydetails.DataBind();
        }

       
        #endregion
      
    }
}

