﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateCDMDetailsReport.aspx.cs
    Description: This is the page used to display the CandidateCDMDetailsReport in excel,word,Pdf formats.
    Created By: pravin khot
    Created On: 4/March/2016
    Modification Log: 
    --------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    --------------------------------------------------------------------------------------------------------------------------------------
   
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Web.Security;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using TPS360.Common;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Linq;

namespace TPS360.Web.UI.Reports
{

    public partial class CandidateCDMDetailsReport : BasePage
    {
        #region Members
        private bool _IsAccessToEmployee = true;
        private static string UrlForEmployee = string.Empty;
        private static int IdForSitemap = 0;
        private string companyStatus = null;

        string _cookiename;
        string CookieName
        {
            get { return _cookiename; }
            set { _cookiename = value; }
        }
         
        #endregion

        #region Methods
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvCandidateSourcingRpt.FindControl(txtSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }

        private void GenerateCandidateSourcingReport(string format)
        {
            if (string.Equals(format, "word"))
            {

                Response.AddHeader("content-disposition", "attachment;filename=CandidateSourcingReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";
                Response.Output.Write(GetCandidateSourcingReportTable());
                Response.Flush();
                Response.End();

            }
            else if (string.Equals(format, "excel"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=CandidateSourcingReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetCandidateSourcingReportTable());
                Response.Flush();
                Response.End();

            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PageWidth = 0;
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;


                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetCandidateSourcingReportTable());

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=CandidateSourcingReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }

        private string GetCandidateSourcingReportTable()
        {
            StringBuilder sourcingReport = new StringBuilder();

            CandidateDataSource candidateDataSource = new CandidateDataSource();

            IList<Candidate> candidateInfo = candidateDataSource.GetPagedCandidateCDMDetailsRpt(wdcSourcedDate.StartDate.ToString(), wdcSourcedDate.EndDate.ToString(),
                null, -1, -1);

            if (candidateInfo != null)
            {
                sourcingReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

                //sourcingReport.Append("    <tr>");
                ////sourcingReport.Append("        <td align=left>&nbsp;<img src='" + Server.MapPath(UIConstants.EMAIL_COMPANY_LOGO) + "' style='height:56px;width:56px;'/></td>");
                ////sourcingReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' style= style='height:56px;width:56px'/></td>"); //0.1
                //sourcingReport.Append("    </tr>");

                //sourcingReport.Append(" <tr>");
                sourcingReport.Append(" <tr>");
                sourcingReport.Append("     <th>Candidate ID # </th>");
                sourcingReport.Append("     <th>Title </th>");
                sourcingReport.Append("     <th>First Name </th>");
                sourcingReport.Append("     <th>Middle Name </th>");
                sourcingReport.Append("     <th>Last Name </th>");
                sourcingReport.Append("     <th>Primary E-Mail</th>");
                sourcingReport.Append("     <th>Father Name </th>");
                sourcingReport.Append("     <th>Date of Birth</th>");
                sourcingReport.Append("     <th>Gender</th>");
                sourcingReport.Append("     <th>Department Process</th>");
                sourcingReport.Append("     <th>Designation</th>");
                sourcingReport.Append("     <th>Permantent Address Line 1</th>");
                sourcingReport.Append("     <th>Permantent Address Line 2</th>");
                sourcingReport.Append("     <th>Permantent Location </th>");
                sourcingReport.Append("     <th>Permantent State Name </th>");
                sourcingReport.Append("     <th>Permantent PIN </th>");
                sourcingReport.Append("     <th>Mobile </th>");
                sourcingReport.Append("     <th>Current Address Line 1</th>");
                sourcingReport.Append("     <th>Current Address Line 2</th>");
                sourcingReport.Append("     <th>Current Location </th>");
                sourcingReport.Append("     <th>Current State </th>");
                sourcingReport.Append("     <th>Current PIN </th>");
                sourcingReport.Append("     <th>Band </th>");//23
                sourcingReport.Append("     <th>Structure </th>");
                sourcingReport.Append("     <th>Input Component </th>");
                sourcingReport.Append("     <th>Expected DOJ </th>");
                sourcingReport.Append("     <th>Bond Name</th>");
                sourcingReport.Append("     <th>Bond Amount </th>");
                sourcingReport.Append("     <th>Bond From Date </th>");
                sourcingReport.Append("     <th>Bond End Date </th>");
                sourcingReport.Append("     <th>Mark Letter To </th>");
                sourcingReport.Append("     <th>Hiring Source Name </th>");
                sourcingReport.Append("     <th>Hiring Cost </th>");
                sourcingReport.Append("     <th>Hiring Comment </th>");
                sourcingReport.Append("     <th>City </th>");//35
                sourcingReport.Append("     <th>Branch </th>");
               sourcingReport.Append("     <th>Maritial Status </th>");
                sourcingReport.Append("     <th>Requisition Date </th>");
                sourcingReport.Append("     <th>Wedding Anniversary </th>");
                sourcingReport.Append("     <th>Project </th>");
                sourcingReport.Append("     <th>Career Level </th>");
                sourcingReport.Append("     <th>Benefit Band </th>");
                sourcingReport.Append("     <th>Salary Grade </th>");
                sourcingReport.Append("     <th>Job Role </th>");
                sourcingReport.Append("     <th>Job Code </th>");
                sourcingReport.Append("     <th>Joining Date </th>");
                sourcingReport.Append(" </tr>");

                foreach (Candidate info in candidateInfo)
                {
                    if (info != null)
                    {
                        sourcingReport.Append(" <tr>");
                        sourcingReport.Append("     <td>");
                        sourcingReport.Append("A" + info.Id.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.Title.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.FirstName.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.MiddleName.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.LastName.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.PrimaryEmail.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.FatherName.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>");
                        if (info.DateOfBirth != DateTime.MinValue)
                        {
                            sourcingReport.Append(
                            (MiscUtil.IsValidDate(info.DateOfBirth) ? info.DateOfBirth.ToShortDateString() : string.Empty).ToString().Trim());
                        }
                        sourcingReport.Append("     <td>" + info.Gender.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.Department.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.CurrentPosition.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.PermanentAddressLine1.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.PermanentAddressLine2.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.PermanentCity.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.PermanentStateName + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.PermanentZip.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.PermanentMobile.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.CurrentAddressLine1.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.CurrentAddressLine2.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.CurrentCity.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.CurrentStateName.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.CurrentZip + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.Band.ToString() + "&nbsp;</td>"); //23
                        sourcingReport.Append("     <td>" + info.structure.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.InputComponent.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>");
                        if (info.ExpectedDOJ != DateTime.MinValue)
                        {
                            sourcingReport.Append(
                            (MiscUtil.IsValidDate(info.ExpectedDOJ) ? info.ExpectedDOJ.ToShortDateString() : string.Empty).ToString().Trim());
                        }
                        sourcingReport.Append("     <td>" + info.BondName.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.Bondamount.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>");
                        if (info.Bondfromdate != DateTime.MinValue)
                        {
                            sourcingReport.Append(
                            (MiscUtil.IsValidDate(info.Bondfromdate) ? info.Bondfromdate.ToShortDateString() : string.Empty).ToString().Trim());
                        }
                        sourcingReport.Append("     <td>");
                        if (info.Bondenddate != DateTime.MinValue)
                        {
                            sourcingReport.Append(
                            (MiscUtil.IsValidDate(info.Bondenddate) ? info.Bondenddate.ToShortDateString() : string.Empty).ToString().Trim());
                        }
                        sourcingReport.Append("     <td>" + info.MarkLetterTo.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.HiringSourceName.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.HiringCost.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.Hiringcomment.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.OfficeCity.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.Branch + "&nbsp;</td>");//36
                        sourcingReport.Append("     <td>" + info.MaritalStatus.ToString() + "&nbsp;</td>");

                        sourcingReport.Append("     <td>");
                        if (info.ReqDate != DateTime.MinValue)
                        {
                            sourcingReport.Append(
                            (MiscUtil.IsValidDate(info.ReqDate) ? info.ReqDate.ToShortDateString() : string.Empty).ToString().Trim());
                        }
                        sourcingReport.Append("     <td>");
                        if (info.WeddingDate != DateTime.MinValue)
                        {
                            sourcingReport.Append(
                            (MiscUtil.IsValidDate(info.WeddingDate) ? info.WeddingDate.ToShortDateString() : string.Empty).ToString().Trim());
                        }
                        sourcingReport.Append("     <td>" + info.project.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.CareerLevel.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.BenefitBand + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.SalaryType.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.JobRole.ToString() + "&nbsp;</td>");
                        sourcingReport.Append("     <td>" + info.JobCode.ToString() + "&nbsp;</td>");
                        
                        sourcingReport.Append("     <td>");
                        if (info.JoiningDate != DateTime.MinValue)
                        {
                            sourcingReport.Append(
                            (MiscUtil.IsValidDate(info.JoiningDate) ? info.JoiningDate.ToShortDateString() : string.Empty).ToString().Trim());
                        }
                                      
                        sourcingReport.Append(" </tr>");
                       
                    }
                }
            }

            sourcingReport.Append("    <tr>");
            //sourcingReport.Append("        <td align=left  width='300' height='75' >&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png'  style= style='height:56px;width:56px;'/></td>");//0.1
            sourcingReport.Append("    </tr>");
            sourcingReport.Append(" </table>");
            return sourcingReport.ToString();
        }

        private void BindList()
        {
            this.lsvCandidateSourcingRpt.DataSourceID = "odsCandidateList";
            this.lsvCandidateSourcingRpt.DataBind();
            divCandidateSourcingReport.Visible = true;
        }

        private void Clear()
        {
            divCandidateSourcingReport.Visible = false;
            ClearSearchCriteria();
        }

        private void ClearSearchCriteria()
        {
            lblMessage.Text = string.Empty;
            wdcSourcedDate.ClearRange();
        }    


        private void PrepareView()
        {
            divCandidateSourcingReport.Visible = true;                
        }     

        private void SetsStatusValue()
        {
            int value;
            // if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
            //{
            //    value = (int)CompanyStatus.Department;
            //    companyStatus = value.ToString();
            //    odsCandidateList.SelectParameters["CompanyStatus"].DefaultValue = companyStatus;
            //}
            // else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
            // {
            //     value = (int)CompanyStatus.Client;
            //     companyStatus = value.ToString();
            //     odsCandidateList.SelectParameters["CompanyStatus"].DefaultValue = companyStatus;

            // }
            // else 
            // {
            //     value = (int)CompanyStatus.Vendor;
            //     companyStatus = value.ToString();
            //     odsCandidateList.SelectParameters["CompanyStatus"].DefaultValue = companyStatus;
            // }
           
        }

        private void SetDataSourceParameters()
        {
           
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "[C].[FirstName]";
            }
            odsCandidateList.SelectParameters["SourcedDateFrom"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["SourcedDateTo"].DefaultValue = DateTime.MinValue.ToString();
          
            SetsStatusValue();  
        }

        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                repor("pdf");
            }

        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            CookieName = "CandidateReportUserColumnOption_" + base.CurrentMember.PrimaryEmail;
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToEmployee = false;
            else
            {
                IdForSitemap = CustomMap.Id;
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }
           
            if (!IsPostBack)
            {
                PrepareView();
            }
            if (!IsPostBack)
            {
                btnSearch_Click(sender, e);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string s = hdnRequisitionSelected.Value;
            hdnScrollPos.Value = "0";
            lsvCandidateSourcingRpt.Items.Clear();
            SetDataSourceParameters();
            ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvCandidateSourcingRpt.FindControl("pagerControl");
            if (PagerControls != null)
            {
                DataPager pager = (DataPager)PagerControls.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            divExportButton.Visible = false;
            BindList();

            string pagesize = "";
            pagesize = (Request.Cookies["CandidateCDMDetailsReportRowPerPage"] == null ? "" : Request.Cookies["CandidateCDMDetailsReportRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateSourcingRpt.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            PlaceUpDownArrow();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            divExportButton.Visible = false;
        }

        public void repor(string rep)
        {
            SetsStatusValue();
            GenerateCandidateSourcingReport(rep);

        }
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            repor("excel");
        }
        protected void lsvCandidateSourcingRpt_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateSourcingRpt.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "CandidateCDMDetailsReportRowPerPage";
            }

            PlaceUpDownArrow();
            if (IsPostBack)
            {
                if (lsvCandidateSourcingRpt.Items.Count == 0)
                {
                    lsvCandidateSourcingRpt.DataSource = null;
                    lsvCandidateSourcingRpt.DataBind();
                }
            }

            LinkButton lnkBtnAccount = (LinkButton)lsvCandidateSourcingRpt.FindControl("lnkBtnAccount");
            if (lnkBtnAccount != null)
            {
                //lnkBtnAccount.Text = lblAccountname.Text;
                //lnkBtnAccount.ToolTip = "Sort By " + lblAccountname.Text;

            }
        }

        protected void lsvCandidateSourcingRpt_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                }
            }
            catch
            {
            }
        }
        protected void lsvCandidateSourcingRpt_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            lsvCandidateSourcingRpt.Visible = true;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Candidate candidat= ((ListViewDataItem)e.Item).DataItem as Candidate;

                if (candidat != null)
                {
                    divExportButton.Visible = true;
                    Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
                      Label lbltitle = (Label)e.Item.FindControl("lbltitle");
                      Label lblCandidateFName = (Label)e.Item.FindControl("lblCandidateFName");
                      Label lblCandidateMName = (Label)e.Item.FindControl("lblCandidateMName");
                      Label lblCandidateLName = (Label)e.Item.FindControl("lblCandidateLName");
                    Label lblemail = (Label)e.Item.FindControl("lblemail");
                    Label lblfathername = (Label)e.Item.FindControl("lblfathername");
                    Label lbldateofbirth = (Label)e.Item.FindControl("lbldateofbirth");
                    Label lblgender = (Label)e.Item.FindControl("lblgender");
                    Label lbldeptprocess = (Label)e.Item.FindControl("lbldeptprocess");
                    Label lbldesignation = (Label)e.Item.FindControl("lbldesignation");
                    Label lblperaddr1 = (Label)e.Item.FindControl("lblperaddr1");
                    Label lblperaddr2 = (Label)e.Item.FindControl("lblperaddr2");
                    Label lblperlocation = (Label)e.Item.FindControl("lblperlocation");
                    Label lblstatename = (Label)e.Item.FindControl("lblstatename");
                    Label lblperpin = (Label)e.Item.FindControl("lblperpin");
                    Label lblmobile = (Label)e.Item.FindControl("lblmobile");
                    Label lblcuraddr1 = (Label)e.Item.FindControl("lblcuraddr1");
                    Label lblcuraddr2 = (Label)e.Item.FindControl("lblcuraddr2");
                       Label lblcurlocation = (Label)e.Item.FindControl("lblcurlocation");
                    Label lblcurstate = (Label)e.Item.FindControl("lblcurstate");
                    Label lblcurpin = (Label)e.Item.FindControl("lblcurpin");
                    Label lblband = (Label)e.Item.FindControl("lblband");
                    Label lblstructure = (Label)e.Item.FindControl("lblstructure");
                    Label lblInputComponent = (Label)e.Item.FindControl("lblInputComponent");
                    Label lblExpectedDOJ = (Label)e.Item.FindControl("lblExpectedDOJ");
                    Label lblBondName = (Label)e.Item.FindControl("lblBondName");
                    Label lblBondamount = (Label)e.Item.FindControl("lblBondamount");
                    Label lblBondfromdate = (Label)e.Item.FindControl("lblBondfromdate");
                    Label lblBondenddate = (Label)e.Item.FindControl("lblBondenddate");
                    Label lblMarkLetterTo = (Label)e.Item.FindControl("lblMarkLetterTo");
                    Label lblHiringSourceName = (Label)e.Item.FindControl("lblHiringSourceName");
                    Label lblHiringCost = (Label)e.Item.FindControl("lblHiringCost");
                    Label lblHiringcomment = (Label)e.Item.FindControl("lblHiringcomment");
                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    Label lblBranch = (Label)e.Item.FindControl("lblBranch");
                    Label lblMaritalStatus = (Label)e.Item.FindControl("lblMaritalStatus");
                    Label lblRequisitionDate = (Label)e.Item.FindControl("lblRequisitionDate");
                    Label lblWeddingAnniversary = (Label)e.Item.FindControl("lblWeddingAnniversary");
                    Label lblproject = (Label)e.Item.FindControl("lblproject");
                    Label lblCareerLevel = (Label)e.Item.FindControl("lblCareerLevel");
                    Label lblBenefitBand = (Label)e.Item.FindControl("lblBenefitBand");
                    Label lblSalaryGrade = (Label)e.Item.FindControl("lblSalaryGrade");
                    Label lblJobRole = (Label)e.Item.FindControl("lblJobRole");
                    Label lblJobCode = (Label)e.Item.FindControl("lblJobCode");
                    Label lblJoiningDate = (Label)e.Item.FindControl("lblJoiningDate");
                       
                    lblCandidateID.Text = "A" + candidat.Id.ToString();
                    lbltitle.Text = candidat.Title;
                    lblCandidateFName.Text = candidat.FirstName ;
                    lblCandidateMName.Text =  candidat.MiddleName ;
                    lblCandidateLName.Text =  candidat.LastName;
                    lblemail.Text = candidat.PrimaryEmail;
                    lblfathername.Text = candidat.FatherName;
                    if (candidat.DateOfBirth != DateTime.MinValue)
                    {
                        lbldateofbirth.Text = (MiscUtil.IsValidDate(candidat.DateOfBirth) ? candidat.DateOfBirth.ToShortDateString() : string.Empty).ToString().Trim();
                    }
                    lblgender.Text = candidat.Gender;
                    lbldeptprocess.Text = candidat.Department;
                    lbldesignation.Text = candidat.CurrentPosition;
                    lblperaddr1.Text = candidat.PermanentAddressLine1;
                    lblperaddr2.Text = candidat.PermanentAddressLine2;
                    lblperlocation.Text = candidat.PermanentCity;
                    lblstatename.Text = candidat.PermanentStateName;
                    lblperpin.Text = candidat.PermanentZip;
                    lblmobile.Text = candidat.PermanentMobile;
                    lblcuraddr1.Text = candidat.CurrentAddressLine1;
                    lblcuraddr2.Text = candidat.CurrentAddressLine2;
                    lblcurlocation.Text = candidat.CurrentCity;
                    lblcurstate.Text = candidat.CurrentStateName;
                    lblcurpin.Text = candidat.CurrentZip;
                    lblband.Text = candidat.Band;
                    lblstructure.Text = candidat.structure;
                    lblInputComponent.Text = candidat.InputComponent;
                    if (candidat.ExpectedDOJ != DateTime.MinValue)
                    {
                        lblExpectedDOJ.Text = (MiscUtil.IsValidDate(candidat.ExpectedDOJ) ? candidat.ExpectedDOJ.ToShortDateString() : string.Empty).ToString().Trim();
                    }
                    lblBondName.Text = candidat.BondName;
                    lblBondamount.Text = candidat.Bondamount;
                    if (candidat.Bondfromdate != DateTime.MinValue)
                    {
                        lblBondfromdate.Text = (MiscUtil.IsValidDate(candidat.Bondfromdate) ? candidat.Bondfromdate.ToShortDateString() : string.Empty).ToString().Trim();
                    }
                    if (candidat.Bondenddate != DateTime.MinValue)
                    {
                        lblBondenddate.Text = (MiscUtil.IsValidDate(candidat.Bondenddate) ? candidat.Bondenddate.ToShortDateString() : string.Empty).ToString().Trim();
                    }
                    lblMarkLetterTo.Text = candidat.MarkLetterTo;
                    lblHiringSourceName.Text = candidat.HiringSourceName;
                    lblHiringCost.Text = candidat.HiringCost;
                    lblHiringcomment.Text = candidat.Hiringcomment;
                    lblCity.Text = candidat.OfficeCity;
                    lblBranch.Text = candidat.Branch;
                    lblBenefitBand.Text = candidat.BenefitBand;
                    lblMaritalStatus.Text = candidat.MaritalStatus;
                    if (candidat.ReqDate != DateTime.MinValue)
                    {
                        lblRequisitionDate.Text = (MiscUtil.IsValidDate(candidat.ReqDate) ? candidat.ReqDate.ToShortDateString() : string.Empty).ToString().Trim();
                    }
                    if (candidat.WeddingDate != DateTime.MinValue)
                    {
                        lblWeddingAnniversary.Text = (MiscUtil.IsValidDate(candidat.WeddingDate) ? candidat.WeddingDate.ToShortDateString() : string.Empty).ToString().Trim();
                    }
                    lblproject.Text = candidat.project;
                    lblCareerLevel.Text = candidat.CareerLevel;
                    lblBenefitBand.Text = candidat.BenefitBand;
                    lblSalaryGrade.Text = candidat.SalaryType;
                    lblJobRole.Text = candidat.JobRole;

                    lblJobCode.Text = candidat.JobCode;
                    if (candidat.JoiningDate != DateTime.MinValue)
                    {                       
                        lblJoiningDate.Text =(MiscUtil.IsValidDate(candidat.JoiningDate) ? candidat.JoiningDate.ToShortDateString() : string.Empty).ToString().Trim();
                    }
                }
            }
        }


        #endregion

    }
}