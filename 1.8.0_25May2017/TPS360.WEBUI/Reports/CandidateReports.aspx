﻿<%---------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateReports.aspx
    Description: This is the page used to display the candidate reports in excel,word,Pdf formats.
    Created By: 
    Created On:
    Modification Log: 
    --------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    --------------------------------------------------------------------------------------------------------------------------------------
    0.1                 02-Jun-2009         Sandeesh            Defect id: 10547 ; Changes made to enable sorting in the 'List of Candidates' report
    0.2                 17-Jul-2009         Nagarathna V.B      Defect Id:10849: Added triggers.
    0.3                 Jan-19-2010         Gopala Swamy J      Defect Id:12901;Changed Style attribute and put events called "OnTextChanged"
    0.4                 Apr-06-2010         Ganapati Bhat       Defect Id:12497 Set "EnableViewstate=true" to fire ddlCountry_SelectedIndexChanged()    
    ------------------------------------------------------------------------------------------------------------------------------------------- 
--%>

<%@ Page EnableEventValidation="false"  Language="C#" MasterPageFile="~/Default.master"
    AutoEventWireup="true" CodeFile="CandidateReports.aspx.cs" Inherits="TPS360.Web.UI.Reports.CandidateReports"
    Title="Candidate Report"  EnableViewStateMac ="false" %>

<%@ Register Src="~/Controls/CandidateReport.ascx" TagName="CandidateReport" TagPrefix="ucl" %>

<asp:Content ID="cntCandidateReportHeader"  ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    <asp:Label ID="lblSubHeader" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
<ucl:CandidateReport ID="uncCandidateReport" IsTeamReport="false" IsMyCandidateReport="false" runat="server" />
</asp:Content>