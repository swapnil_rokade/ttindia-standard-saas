﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   RequisitionAgingReports.aspx.cs
    Description         :   This page is used to generate report on requisition Aging.
    Created By          :   Prasanth Kumar G
    Created On          :   20/May/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 11/Jun/2015         Prasanth            Introduced Turn Around Time and Time To Fill
 *  0.2                 18/Jun/2015         Prasanth            Search Options disabled for Hiring Matrix Levels
 *  0.3                 24/Jun/2015         Prasanth            " Days" String Added in Total Taken Time 
----------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;  
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using TPS360.Common.Helper;
using System.Configuration;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;
using System.Web.Security;
using System.Xml;
using System.Linq ;
namespace TPS360.Web.UI.Reports
{
    public partial class RequisitionAgingReports : BasePage
    {
        #region Members
        string clientRate;
        public int HeaderAdd = 0;
        string HiringHD = ""; //Code introduced by Prasanth on 22/May/2015
        #endregion

        private string AssociateRequisitionsWithEntitiesFromModule
        {
            get
            {
                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                {
                    return "Department";
                }
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                {
                    return "Account";
                }
                else
                {
                    return "Vendor";
                }
            }

        }


//*************************CODE COMMENTED BY PRASANTH ON 21/MAY/2015****************************START

        #region Methods
        private int NumofColumnSelected()
        {
            //return chkColumnList.Items.Cast<ListItem>().Where(x => x.Selected).Count();
            return 0; //G
        }

        private void GenerateRequisitionReport(string format)
        {
            if (string.Equals(format, "word"))
            {

                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=RequisitionAgingReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";

                Response.Output.Write(GetRequisitionReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "excel"))
            {
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=RequisitionAgingReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetRequisitionReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A3;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;
                int numofcolSelected = NumofColumnSelected();
                if (numofcolSelected > 0 && numofcolSelected > 7)
                {
                    pdfConverter.PageWidth += (110 * (numofcolSelected - 7));
                }

                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlStringWithTempFile(GetRequisitionReportTable(), "");
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=RequisitionAgingReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }
        private StringBuilder getReportHeader()
        {
            StringBuilder header = new StringBuilder();

            //foreach (ListItem item in chkColumnList.Items.Cast<ListItem>().Where(x => x.Selected == true))
            //    if (item.Selected) header.Append("<th>" + (item.Text.Contains("Turn Around Time") ? "Turn Around Time" : item.Text) + "</th>");


            header.Append("<th>Job Title</th>");
            header.Append("<th>Start Date</th>");
            header.Append("<th>Closing Date</th>");
           


            string[] hiringMatrixLevels = hdnMatrixLevels.Value.Split('#');

            if (hiringMatrixLevels != null)
            {
                if (hiringMatrixLevels.Count() > 0)
                {
                    foreach (string item in hiringMatrixLevels)
                    {
                        if (item != "")
                        {

                            header.Append("<th>" + item + "</th>");
                        }
                        //reqReport.Append(" </tr>");
                    }
                }
            }

            header.Append("<th>Total Time Taken</th>");
            header.Append("<th>Time To Fill</th>"); //Code introduced by Prasanth on 11/Jun/2015
            //foreach (ListItem item in chkHiringList.Items.Cast<ListItem>().Where(x => x.Selected == true))
            //    if (item.Selected) header.Append("<th>" + item.Text + "</th>");

            return header;
        }
        private string GetRequisitionReportTable()
        {
            bool InternalHiring = false;
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                InternalHiring = true;
            }
            StringBuilder reqReport = new StringBuilder();
            JobPostingDataSource jobPostingDataSource = new JobPostingDataSource();
            IList<JobPosting> jobPostingList = jobPostingDataSource.GetPaged(false, dtPicker.StartDate.ToString(),
                dtPicker.EndDate.ToString(), ddlRequisitionCreator.SelectedValue, ddlEndClient.SelectedValue, null, -1, -1);
            if (jobPostingList != null)
            {
                string WebRoot = ConfigurationManager.AppSettings["WebRoot"].ToString();
                string LogoPath = UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png";
                reqReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");
                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + LogoPath + "' style= style='height:56px;width:56px'/></td>"); //0.7
                reqReport.Append("    </tr>");

                reqReport.Append(" <tr>");
                reqReport.Append(getReportHeader().ToString());
                reqReport.Append(" </tr>");

                IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();


                foreach (JobPosting jobPosting in jobPostingList)
                {

                    if (jobPosting != null)
                    {
                        reqReport.Append(" <tr>");
                        reqReport.Append("     <td>" + jobPosting.JobTitle.ToString().Trim() + "</td>");
                        reqReport.Append("     <td>" + (MiscUtil.IsValidDate(jobPosting.OpenDate) ? jobPosting.OpenDate.ToShortDateString() : string.Empty).ToString().Trim() + "</td>");
                        reqReport.Append("     <td>" + (MiscUtil.IsValidDate(jobPosting.FinalHiredDate) ? jobPosting.FinalHiredDate.ToShortDateString() : string.Empty).ToString().Trim() + "</td>");
                        //reqReport.Append("     <td>" + jobPosting.TotalTimeTaken.ToString().Trim() + "</td>");

                        //string[] hiringMatrixLevels = hdnMatrixLevels.Value.Split('#');

                        //if (hiringMatrixLevels != null)
                        //{
                        //    if (hiringMatrixLevels.Count() > 0)
                        //    {
                        //        foreach (string item in hiringMatrixLevels)
                        //        {
                        //            if (item != "")
                        //            {
                        //                reqReport.Append("     <td>" + jobPosting.HiringMatrixLevels.Where(x => x.HiringMatrixLevelID == Convert.ToInt32(item)).ToList()[0].CandidateCount.ToString() + "</td>");
                        //            }    
                        //         }
                                    
                            
                        //    }
                        //}


                        if (hiringMatrixLevels != null)
                        {
                            if (hiringMatrixLevels.Count > 0)
                            {
                                foreach (HiringMatrixLevels levels in hiringMatrixLevels)
                                {
                                    reqReport.Append("     <td>" + jobPosting.HiringMatrixLevels.Where(x => x.HiringMatrixLevelID == Convert.ToInt32(levels.Id)).ToList()[0].CandidateCount.ToString() + "</td>");
                                }
                            }
                        }


                        reqReport.Append("     <td>" + jobPosting.TotalTimeTaken.ToString().Trim() + "</td>");
                        reqReport.Append("     <td>" + TimeToFill(jobPosting).ToString().Trim() + "</td>");
                        

                    }
                        
                    }
                        reqReport.Append(" </tr>");
                        reqReport.Append("    <tr>");
                        reqReport.Append("        <td align=left  width='300' height='75'><img src='" + LogoPath + "' style= style='height:56px;width:56px;'/></td>");
                        reqReport.Append("    </tr>");

                        reqReport.Append(" </table>");
                    
                   
                
               
    
    }
            return reqReport.ToString();
        }
        private string AssembleNotes(string notes)
        {
            if (notes != null && notes.Trim() != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(notes);
                XmlNodeList notesList = xmldoc.GetElementsByTagName("Note");
                StringBuilder sbNote = new StringBuilder();
                sbNote.Append("<ul style=\"list-style-type:none;\">");

                for (int i = 0; i < notesList.Count; i++)
                {
                    sbNote.Append("<li>");
                    sbNote.Append(notesList[i].ChildNodes[2].InnerText);
                    sbNote.Append(" by " + notesList[i].ChildNodes[0].InnerText);
                    sbNote.Append(" : " + notesList[i].ChildNodes[1].InnerText);
                    sbNote.Append("</li>");
                }
                sbNote.Append("</ul>");
                return sbNote.ToString();
            }
            return "";
        }

        private void BindList()
        {
            //bool checkedAtleastOne = false;
            //foreach (ListItem chkItem in chkColumnList.Items)
            //{
            //    if (chkItem.Selected == true)
            //    {
            //        checkedAtleastOne = true;
            //        break;
            //    }
            //}
            this.lsvJobPosting.DataSourceID = "odsJobPostingList";
            //if (checkedAtleastOne)
            //{
            this.divlsvJobPosting.Visible = true;
            this.lsvJobPosting.DataBind();
            //}
            //else
            //{
            //    this.divlsvJobPosting.Visible = false;
            //    MiscUtil.ShowMessage(lblMessage, "Please Select atleast One column", false);
            //}

        }

        private void ClearControls()
        {
            dtPicker.ClearRange();
            //ddlRequisition.Items.Clear();
            //int k = 0;
            //System.Collections.ArrayList arr = Facade.GetAllJobPostingListByStatusId(k);
            //JobPosting j = new JobPosting();
            //j.Id = 0;
            //j.JobTitle = "Any";
            //arr.Insert(0, j);
            //ddlRequisition.DataSource = arr;
            //ddlRequisition.DataTextField = "JobTitle";
            //ddlRequisition.DataValueField = "Id";
            //ddlRequisition.DataBind();
            //ddlJobStatus.SelectedIndex = ddlEmployee.SelectedIndex = ddlEndClient.SelectedIndex = ddlRequisitionCreator.SelectedIndex = ddlRequisition.SelectedIndex = 0; //0.9
            ddlEndClient.SelectedIndex = ddlRequisitionCreator.SelectedIndex = 0;
            //txtCity.Text = "";
            ResetPager();
            lsvJobPosting.Visible = false;
            //uclCountryState.SelectedCountryId = 0;

            //ddlEndClient.SelectedIndex = 0;
        }

        private void PopulateJobStatus()
        {
            //MiscUtil.PopulateRequistionStatus(ddlJobStatus, Facade);
            //ddlJobStatus.Items.Insert(0, new ListItem("Any", "0"));
        }

        //void PopulateRequisition(int StatusId)
        
        //{

            //ddlRequisition.Items.Clear();
            //System.Collections.ArrayList arr = Facade.GetAllJobPostingListByStatusId(StatusId);
            //JobPosting j = new JobPosting();
            //j.Id = 0;
            //j.JobTitle = "Any";
            //arr.Insert(0, j);
            //ddlRequisition.DataSource = arr;
            //ddlRequisition.DataTextField = "JobTitle";
            //ddlRequisition.DataValueField = "Id";
            //ddlRequisition.DataBind();
            //ControlHelper.SelectListByValue(ddlRequisition, hdnRequisitionSelected.Value);
        //}
        private void PrepareView()
        {


            //PopulateJobStatus();
            //PopulateRequisition(0);

            MiscUtil.PopulateMemberListByRole(ddlRequisitionCreator, ContextConstants.ROLE_EMPLOYEE, Facade);

            //MiscUtil.PopulateMemberListByRole(ddlEmployee, ContextConstants.ROLE_EMPLOYEE, Facade);

            string accountType = "";
            MiscUtil.PopulateClients(ddlEndClient, (int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting), Facade);
            ddlEndClient.Items[0].Text = "Select BU";
            lblByEndClientsHeader.Text = "By BU";
            ddlEndClient.Items[0].Value = "0";

            //if (chkColumnList.Items.FindByText("Account Name") != null) chkColumnList.Items.FindByText("Account Name").Text = AssociateRequisitionsWithEntitiesFromModule + " Name";
            //if (chkColumnList.Items.FindByText("Account Job ID") != null) chkColumnList.Items.FindByText("Account Job ID").Text = AssociateRequisitionsWithEntitiesFromModule + " Job ID";
            //if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            //{
            //    if (chkColumnList.Items.FindByValue("ClientHourlyRate") != null) chkColumnList.Items.Remove(chkColumnList.Items.FindByValue("ClientHourlyRate"));
            //}
            //IList<HiringMatrixLevels> levels = Facade.GetAllHiringMatrixLevels();
            //if (levels != null)
            //{
            //    foreach (HiringMatrixLevels lev in levels)
            //    {
            //        ListItem item = new ListItem(lev.Name, lev.Id.ToString());
            //        chkHiringList.Items.Add(item);

            //    }
            //}
        }
        private void RenameListHeader(string headerTitle, string LinkName)
        {
            //HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvJobPosting.FindControl(headerTitle);
            //if (thHeaderTitle != null)
            //{
            //    LinkButton link = (LinkButton)thHeaderTitle.FindControl(LinkName);
            //    link.Text = link.Text.Replace("Account", AssociateRequisitionsWithEntitiesFromModule);
            //    link.ToolTip = link.ToolTip.Replace("Account", AssociateRequisitionsWithEntitiesFromModule);


            //}

        }
        private void HideUnHideListViewHeaderItem(string headerTitle, string chkItemName)
        {
            HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvJobPosting.FindControl(headerTitle);

            if (thHeaderTitle != null)
            {
                //if (chkColumnList.Items.FindByValue(chkItemName) != null)
                //{
                //    if (!chkColumnList.Items.FindByValue(chkItemName).Selected)
                //    {
                //        thHeaderTitle.Visible = false;
                //    }
                //    else
                //    {
                        thHeaderTitle.Visible = true;
                //    }
                //}
                //else
                //{
                //    thHeaderTitle.Visible = false;
                //}
            }
        }
        private void HideUnHideListViewHeaderItemByHiringMatrix(string headerTitle, string chkItemName)
        {
            //HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvJobPosting.FindControl(headerTitle);

            //if (thHeaderTitle != null)
            //{
            //    if (!chkHiringList.Items.FindByValue(chkItemName).Selected)
            //    {
            //        thHeaderTitle.Visible = false;
            //    }
            //    else
            //    {
            //        thHeaderTitle.Visible = true;
            //    }
            //}
        }
        private void HideUnhideListViewHeader()
        {
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                RenameListHeader("thClientJobId", "btnClientJobId");
                RenameListHeader("thClientId", "btnClientId");
            }

            //thPostDate
            //    thJobTitle
            //    thStartDate
            //        thFinalHiredDate


            HideUnHideListViewHeaderItem("thJobLocation", "JobLocation");


            HideUnHideListViewHeaderItem("thSalesRegion", "SalesRegion");
            HideUnHideListViewHeaderItem("thSalesGroup", "SalesGroup");
            HideUnHideListViewHeaderItem("thPOAvailability", "POAvailability");
            HideUnHideListViewHeaderItem("thBUContact", "BUContact");
            HideUnHideListViewHeaderItem("thCustomerName", "CustomerName");

            HideUnHideListViewHeaderItem("thScheduledInterviews", "ScheduledInterviews");
            HideUnHideListViewHeaderItem("thSubmissions", "Submissions");
            HideUnHideListViewHeaderItem("thOffers", "Offers");
            HideUnHideListViewHeaderItem("thJoined", "Joined");

            HideUnHideListViewHeaderItem("thPostDate", "PostedDate");
            HideUnHideListViewHeaderItem("thJobTitle", "JobTitle");
            HideUnHideListViewHeaderItem("thJobPostingCode", "JobPostingCode");
            HideUnHideListViewHeaderItem("thTimeToFill", "TimeToFill");
            HideUnHideListViewHeaderItem("thClientJobId", "ClientJobId");
            HideUnHideListViewHeaderItem("thNoOfOpenings", "NoOfOpenings");

            HideUnHideListViewHeaderItem("thNoofUnfilledOpenings", "NoofUnfilledOpenings");

            HideUnHideListViewHeaderItem("thPayRate", "PayRate");

            HideUnHideListViewHeaderItem("thPayCycle", "PayCycle");
            HideUnHideListViewHeaderItem("thTravelRequired", "TravelRequired");
            HideUnHideListViewHeaderItem("thTravelRequiredPercent", "TravelRequiredPercent");
            HideUnHideListViewHeaderItem("thOtherBenefits", "OtherBenefits");
            HideUnHideListViewHeaderItem("thReqStatus", "JobStatus");
            HideUnHideListViewHeaderItemByHiringMatrix("thPreSelected", "PreSelected");
            HideUnHideListViewHeaderItemByHiringMatrix("thLevel1", "Level1");
            HideUnHideListViewHeaderItemByHiringMatrix("thLevel2", "Level2");
            HideUnHideListViewHeaderItemByHiringMatrix("thLevel3", "Level3");
            HideUnHideListViewHeaderItemByHiringMatrix("thFinalHired", "FinalHire");
            HideUnHideListViewHeaderItem("thJobDurationLookupId", "JobDurationLookupId");
            HideUnHideListViewHeaderItem("thJobDurationMonth", "JobDurationMonth");
            HideUnHideListViewHeaderItem("thJobAddress1", "JobAddress1");
            HideUnHideListViewHeaderItem("thJobAddress2", "JobAddress2");
            HideUnHideListViewHeaderItem("thCity", "City");
            HideUnHideListViewHeaderItem("thZipCode", "ZipCode");
            HideUnHideListViewHeaderItem("thCountryId", "CountryId");
            HideUnHideListViewHeaderItem("thStateId", "StateId");
            HideUnHideListViewHeaderItem("thReportingTo", "ReportingTo");
            HideUnHideListViewHeaderItem("thNoOfReportees", "NoOfReportees");
            HideUnHideListViewHeaderItem("thStartDate", "StartDate");
            HideUnHideListViewHeaderItem("thFinalHiredDate", "FinalHiredDate");
            HideUnHideListViewHeaderItem("thAuthorizationTypeLookupId", "AuthorizationTypeLookupId");

            HideUnHideListViewHeaderItem("thActivationDate", "ActivationDate");
            HideUnHideListViewHeaderItem("thRequiredDegreeLookupId", "RequiredDegreeLookupId");
            HideUnHideListViewHeaderItem("thExpRequired", "ExpRequired");
            HideUnHideListViewHeaderItem("thVendorGroupId", "VendorGroupId");
            HideUnHideListViewHeaderItem("thIsJobActive", "IsJobActive");
            HideUnHideListViewHeaderItem("thPublishedForInternal", "PublishedForInternal");
            HideUnHideListViewHeaderItem("thPublishedForPublic", "PublishedForPublic");
            HideUnHideListViewHeaderItem("thPublishedForPartner", "PublishedForPartner");
            HideUnHideListViewHeaderItem("thMinAgeRequired", "MinAgeRequired");
            HideUnHideListViewHeaderItem("thMaxAgeRequired", "MaxAgeRequired");
            HideUnHideListViewHeaderItem("thReqType", "JobType");
            HideUnHideListViewHeaderItem("thIsApprovalRequired", "IsApprovalRequired");
            HideUnHideListViewHeaderItem("thClientId", "ClientId");
            HideUnHideListViewHeaderItem("thClientProjectId", "ClientProjectId");
            HideUnHideListViewHeaderItem("thClientEndClientId", "ClientEndClientId");
            HideUnHideListViewHeaderItem("thClientHourlyRate", "ClientHourlyRate");
            HideUnHideListViewHeaderItem("thClientRatePayCycle", "ClientRatePayCycle");
            HideUnHideListViewHeaderItem("thClientDisplayName", "ClientDisplayName");
            HideUnHideListViewHeaderItem("thClientId2", "ClientId2");
            HideUnHideListViewHeaderItem("thClientId3", "ClientId3");
            HideUnHideListViewHeaderItem("thTaxTermLookupIds", "TaxTermLookupIds");
            HideUnHideListViewHeaderItem("thJobCategoryLookupId", "JobCategoryLookupId");
            HideUnHideListViewHeaderItem("thJobIndustryLookupId", "JobIndustryLookupId");
            HideUnHideListViewHeaderItem("thExpectedRevenue", "ExpectedRevenue");
            HideUnHideListViewHeaderItem("thSourcingChannel", "SourcingChannel");
            HideUnHideListViewHeaderItem("thSourcingExpenses", "SourcingExpenses");
            HideUnHideListViewHeaderItem("thPublishedForVendor", "PublishedForVendor");
            HideUnHideListViewHeaderItem("thTeleCommunication", "TeleCommunication");
            HideUnHideListViewHeaderItem("thCreatorId", "CreatorId");
            HideUnHideListViewHeaderItem("thUpdatorId", "UpdatorId");
            HideUnHideListViewHeaderItem("thCreateDate", "CreateDate");
            HideUnHideListViewHeaderItem("thUpdateDate", "UpdateDate");

            HideUnHideListViewHeaderItem("thJobCategory", "JobCategory");
            HideUnHideListViewHeaderItem("thSkills", "Skills");
            HideUnHideListViewHeaderItem("thMinimumQualifyingParameters", "MinimumQualifyingParameters");
            HideUnHideListViewHeaderItem("thWorkSchedule", "WorkSchedule");
            HideUnHideListViewHeaderItem("thSecurityClearance", "SecurityClearance");
            HideUnHideListViewHeaderItem("thExpenses", "Expenses");
            HideUnHideListViewHeaderItem("thAdditionalNotes", "AdditionalNotes");
            HideUnHideListViewHeaderItem("thAssignedRecruiters", "AssignedRecruiters");
            HideUnHideListViewHeaderItem("thTurnArroundTime", "TurnArroundTime");

            HideUnHideListViewHeaderItem("thInterviewCandidate", "JPICN");
            HideUnHideListViewHeaderItem("thSubmittedCandidate", "JPSCN");
            HideUnHideListViewHeaderItem("thOfferedCandidate", "JPOCN");
            HideUnHideListViewHeaderItem("thJoinedCandidate", "JPJCN");
            HideUnHideListViewHeaderItem("thJobDescription", "JobDesc");
            HideUnHideListViewHeaderItem("thNotes", "Jobpostnotes");
            HideUnHideListViewHeaderItem("thRequisitionType", "RequisitionType");
        }

        public void repor(string rep)
        {
            GenerateRequisitionReport(rep);


            //int j = 0;
            //int k = 0;
            //foreach (ListItem items in chkColumnList.Items)
            //{
            //    if (items.Selected)
            //    {
            //        j = 1;

            //    }
            //    else
            //        k = 1;
            //}
            //if ((j == 1 && k == 1) || (k == 0 && j == 1))
            //    GenerateRequisitionReport(rep);
            //else
            //{
            //    MiscUtil.ShowMessage(lbMessage, "Please select columns before generating the report.", true);
            //}
        }
        #endregion

        #region Properties
        string _cookiename;
        string CookieName
        {
            get { return _cookiename; }
            set { _cookiename = value; }
        }

        string HiringMatrixColumnsCookieeName
        {
            get;
            set;
        }

        #endregion

        #region Events

        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {

                repor("pdf");

            }
            else btnSearch_Click(sender, new EventArgs());
        }

        private void HideListItemsForCommonApp()
        {
            //ListItem workauth = chkColumnList.Items.FindByValue("JobLocation");
            //if (chkColumnList.Items.Contains(workauth))
            //{
            //    chkColumnList.Items.Remove(workauth);
            //}
            //workauth = chkColumnList.Items.FindByValue("SalesRegion");
            //if (chkColumnList.Items.Contains(workauth))
            //{
            //    chkColumnList.Items.Remove(workauth);
            //}
            //workauth = chkColumnList.Items.FindByValue("SalesGroup");
            //if (chkColumnList.Items.Contains(workauth))
            //{
            //    chkColumnList.Items.Remove(workauth);
            //}
            //workauth = chkColumnList.Items.FindByValue("POAvailability");
            //if (chkColumnList.Items.Contains(workauth))
            //{
            //    chkColumnList.Items.Remove(workauth);
            //}
            //workauth = chkColumnList.Items.FindByValue("CustomerName");
            //if (chkColumnList.Items.Contains(workauth))
            //{
            //    chkColumnList.Items.Remove(workauth);
            //}



        }
        protected void Page_Load(object sender, EventArgs e)
        {

            HiringHD = "";
            uclConfirm.MsgBoxAnswered += MessageAnswered;

            //ddlJobStatus.Attributes.Add("onChange", "return RequisitionStatusOnChange('" + ddlJobStatus.ClientID + "','" + ddlRequisition.ClientID + "')");
            //ddlRequisition.Attributes.Add("onChange", "return RequisitionOnChange('" + ddlRequisition.ClientID + "','" + hdnRequisitionSelected.ClientID + "')");
            //ddlEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlRequisitionCreator.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlEndClient.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            if (CookieName != null) CookieName = "UserColumnOption_" + base.CurrentMember.Id;
            if (base.CurrentMember != null) HiringMatrixColumnsCookieeName = "HiringMatrixColumns_" + base.CurrentMember.Id;
            if (!IsPostBack)
            {

            if (ApplicationSource == ApplicationSource.MainApplication) HideListItemsForCommonApp();

            PrepareView();
            ExportButtons.Visible = false;
            //    if (Request.Cookies[CookieName] != null)
            //    {
            //        string ColumnOptions = "PostedDate#JobTitle#JobPostingCode#TimeToFill#NoOfOpenings#NoofUnfilledOpenings#PayRate#JobStatus#City#StartDate#FinalHiredDate#ClientId#BUContact#CreatorId#Skills#AssignedRecruiters#TurnArroundTime#";
            //        string[] Columns = ColumnOptions.Split('#');
            //        foreach (ListItem item in chkColumnList.Items)
            //            item.Selected = false;
            //        foreach (string item in Columns)
            //            if (chkColumnList.Items.FindByValue(item) != null) chkColumnList.Items.FindByValue(item).Selected = true;
            //    }
            //    if (Request.Cookies[HiringMatrixColumnsCookieeName] != null)
            //    {
            //        string ColumnOptions = Request.Cookies[HiringMatrixColumnsCookieeName].Value;
            //        string[] Columns = ColumnOptions.Split('#');
            //        foreach (ListItem item in chkHiringList.Items)
            //            item.Selected = false;
            //        foreach (string item in Columns)
            //            if (chkHiringList.Items.FindByValue(item) != null) chkHiringList.Items.FindByValue(item).Selected = true;
            //    }

            }
            if (IsPostBack)
            {
             
            System.Web.UI.HtmlControls.HtmlTableRow thLevels = (System.Web.UI.HtmlControls.HtmlTableRow)this.lsvJobPosting.FindControl("trHeader");
            if (thLevels != null)
            {
                int ColLocation = 3;
                //Code Modified by Prasanth on 18/Jun/2015  Start
                IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
                //string[] hiringMatrixLevels = HiringHD.Split('#');
                
                
                //string[] hiringMatrixLevels = hdnMatrixLevels.Value.Split('#');
                
                //if (hiringMatrixLevels != null)
                //{
                //    if (hiringMatrixLevels.Count() > 0)
                //    {
                //        foreach (string item in hiringMatrixLevels)
                //        {
                if (hiringMatrixLevels != null)
                {
                    if (hiringMatrixLevels.Count() > 0)
                    {
                        //foreach (string item in hiringMatrixLevels)
                        foreach (HiringMatrixLevels levels in hiringMatrixLevels)
                        {   
                            //if (item != "")
                            //{

                                System.Web.UI.HtmlControls.HtmlTableCell col = new System.Web.UI.HtmlControls.HtmlTableCell("TH");
                                //LinkButton lnkLevel = new LinkButton();
                                Label lnkLevel = new Label();
                                lnkLevel.Text = levels.Name;
                                lnkLevel.ToolTip = levels.Name;
                                //lnkLevel.CommandName = "Sort";
                                lnkLevel.ID = "levl" + ColLocation;
                                lnkLevel.EnableViewState = true;
                                //lnkLevel.CommandArgument = levels.Id.ToString();
                                col.Controls.Add(lnkLevel);
                                LinkButton ll = (LinkButton)thLevels.FindControl(lnkLevel.ID);
                                if (ll == null) thLevels.Controls.AddAt(ColLocation, col);
                                ColLocation++;
                            //}
                        }
                    }
                }
            }
            //**************************END*******************************
            //    HideUnhideListViewHeader();
           }
            
            //if (System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern == "dd/MM/yyyy")
            //{
            //    ListItem workauth = chkColumnList.Items.FindByValue("AuthorizationTypeLookupId");
            //    if (chkColumnList.Items.Contains(workauth))
            //    {
            //        chkColumnList.Items.Remove(workauth);
            //    }
            //}
            if (!IsPostBack)
            {

                btnSearch_Click(sender, e);
            }
            
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            HiringHD = "";
            //string s = hdnRequisitionSelected.Value;
            lsvJobPosting.Visible = true;
            hdnScrollPos.Value = "0";
            int count = 0;
            //Adding Selected Columns to Cookie
            string ColumnOption = "";
            lsvJobPosting.Items.Clear();
            ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControls != null)
            {
                DataPager pager = (DataPager)PagerControls.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            BindList();
            if (lsvJobPosting != null && lsvJobPosting.Items.Count > 0)
            {
                ExportButtons.Visible = true;
                HideUnhideListViewHeader();
                lsvJobPosting.Visible = true;
            }
            else ExportButtons.Visible = false;

            System.Web.UI.HtmlControls.HtmlTableRow thLevels = (System.Web.UI.HtmlControls.HtmlTableRow)this.lsvJobPosting.FindControl("trHeader");

           
 
            //*************************************************************************





            if (thLevels != null)
            {
                int ColLocation = 3;
                IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
                System.Web.HttpCookie hiringaCookie = new System.Web.HttpCookie(HiringMatrixColumnsCookieeName);
                hiringaCookie.Value = HiringHD.ToString();

                System.Web.HttpCookie aCookie = new System.Web.HttpCookie(CookieName);
                aCookie.Value = HiringHD.ToString();
                Response.Cookies.Add(aCookie);

                //Code Modified by prasanth on 18/Jun/2015 Start
                //string[] hiringMatrixLevels = HiringHD.Split('#');
                if (hiringMatrixLevels != null)
                {
                    if (hiringMatrixLevels.Count() > 0)
                    {
                        //foreach (string item in hiringMatrixLevels)
                        foreach (HiringMatrixLevels levels in hiringMatrixLevels)
                        {   
                            //if (item != "")
                            //{
                            
                                System.Web.UI.HtmlControls.HtmlTableCell col = new System.Web.UI.HtmlControls.HtmlTableCell("TH");
                                Label lnkLevel = new Label();
                                lnkLevel.Text = levels.Name;
                                //lnkLevel.CommandName = "Sort";
                                lnkLevel.ID = "levl" + ColLocation;
                                lnkLevel.EnableViewState = true;
                                //lnkLevel.CommandArgument = levels.Id.ToString();
                                col.Controls.Add(lnkLevel);
                                Label ll = (Label)thLevels.FindControl(lnkLevel.ID);
                                if (ll == null) thLevels.Controls.AddAt(ColLocation, col);
                                ColLocation++;
                            
                            //}
                        }
                    }
                }
                //*****************************END*************************
                System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("tdpager");
                if (tdPager != null)
                {
                    tdPager.ColSpan = 3 + ColLocation;
                }
            }


            //**************************************************************************************************************




            //foreach (ListItem item in chkColumnList.Items)
            //    if (item.Selected == true) ColumnOption += item.Value + "#";
            //System.Web.HttpCookie aCookie = new System.Web.HttpCookie(CookieName);
            //aCookie.Value = ColumnOption.ToString();
            //Response.Cookies.Add(aCookie);
            ////Adding Hiring Matrix Selected Column to cookie
            //string HiringColumnColumnOption = "";
            //foreach (ListItem item in chkHiringList.Items)
            //    if (item.Selected == true) HiringColumnColumnOption += item.Value + "#";
            //System.Web.HttpCookie hiringaCookie = new System.Web.HttpCookie(HiringMatrixColumnsCookieeName);
            //hiringaCookie.Value = HiringColumnColumnOption.ToString();
            //Response.Cookies.Add(hiringaCookie);
            //lsvJobPosting.Items.Clear();
            //ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            //if (PagerControls != null)
            //{
            //    DataPager pager = (DataPager)PagerControls.FindControl("pager");
            //    if (pager != null)
            //    {
            //        if (pager.Page.IsPostBack)
            //            pager.SetPageProperties(0, pager.MaximumRows, true);
            //    }
            //}
            //BindList();
            //if (lsvJobPosting != null && lsvJobPosting.Items.Count > 0)
            //{
            //    ExportButtons.Visible = true;
            //    HideUnhideListViewHeader();
            //    lsvJobPosting.Visible = true;
            //}
            //else ExportButtons.Visible = false;
            //System.Web.UI.HtmlControls.HtmlTableRow thLevels = (System.Web.UI.HtmlControls.HtmlTableRow)this.lsvJobPosting.FindControl("trHeader");
            //if (thLevels != null)
            //{
            //    IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
            //    int ColLocation = 11;
            //    if (hiringMatrixLevels != null)
            //    {
            //        if (hiringMatrixLevels.Count > 0)
            //        {
            //            foreach (HiringMatrixLevels levels in hiringMatrixLevels)
            //            {
            //                if (checkCheckBoxHiringLevelSelected(levels.Id))
            //                {



            //                    System.Web.UI.HtmlControls.HtmlTableCell col = new System.Web.UI.HtmlControls.HtmlTableCell("TH");
            //                    LinkButton lnkLevel = new LinkButton();
            //                    lnkLevel.Text = levels.Name;
            //                    lnkLevel.CommandName = "Sort";
            //                    lnkLevel.ID = "levl" + ColLocation;
            //                    lnkLevel.EnableViewState = true;
            //                    lnkLevel.CommandArgument = "[" + levels.Id.ToString() + "]";
            //                    col.Controls.Add(lnkLevel);
            //                    LinkButton ll = (LinkButton)thLevels.FindControl(lnkLevel.ID);
            //                    if (ll == null) thLevels.Controls.AddAt(ColLocation, col);
            //                    ColLocation++;




            //                }
            //            }
            //        }

            //    }
            //    System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("tdpager");
            //    if (tdPager != null)
            //    {
            //        tdPager.ColSpan = chkColumnList.Items.Cast<ListItem>().Where(x => x.Selected).Count() + chkHiringList.Items.Cast<ListItem>().Where(x => x.Selected).Count();
            //    }
            //}
            //HideUnhideListViewHeader();
            if (hdnSortColumn.Value == "") hdnSortColumn.Value = "btnStartDate";
            if (hdnSortOrder.Value == "") hdnSortOrder.Value = "DESC";
            PlaceUpDownArrow();
            string pagesize = "";

            pagesize = (Request.Cookies["RequisitionReportRowPerPage"] == null ? "" : Request.Cookies["RequisitionReportRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            //uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;

            //uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
            //PopulateRequisition(Convert.ToInt32(ddlJobStatus.SelectedValue));
            //PopulateRequisition(0); //PGK0123456789 CODE INTRODUCED BY PRASANTH ON 22/05/2015
               
        }

        protected void ddlJobStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MiscUtil.LoadAllControlState(Request, this);
        }

     
        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
            ExportButtons.Visible = false;

        }

        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {
            uclConfirm.AddMessage("Note: Some columns may not appear in report if too many are selected.", ConfirmationWindow.enmMessageType.Attention, true, true, "");
            try
            {
                BindList();
            }
            catch { }
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            repor("excel");
        }

        protected void btnExportToWord_Click(object sender, EventArgs e)
        {
            ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(this.btnExportToWord);
            repor("word");
        }

        private bool checkCheckBoxHiringLevelSelected(int value)
        {
            //foreach (ListItem item in chkHiringList.Items)
            //{
            //    if (Convert.ToInt32(item.Value) == value && item.Selected == true) return true;

            //}
            return false;
        }
        protected void lsvJobPosting_PreRender(object sender, EventArgs e)
        {
            LinkButton btnPostedDate1 = (LinkButton)lsvJobPosting.FindControl("btnStartDate");
            if (btnPostedDate1 != null) btnPostedDate1.Text = "Start Date";
           
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "RequisitionReportRowPerPage";
            }
            PlaceUpDownArrow();
            HideUnhideListViewHeader();
            if (IsPostBack)
            {
                if (lsvJobPosting.Items.Count == 0)
                {
                    lsvJobPosting.DataSource = null;
                    lsvJobPosting.DataBind();
                }
            }
        }

        protected void lsvJobPosting_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                }
            }
            catch  
            {
                
            }
        }

        string TimeToFill(JobPosting jobPosting)
        {
            string TT;
            TT = "";
            if (((jobPosting.FinalHiredDate == DateTime.MinValue) || (jobPosting.FinalHiredDate == null)) || ((jobPosting.ActivationDate == DateTime.MinValue) || (jobPosting.ActivationDate == null)))
                //return "";
                return "";
            else
                //Code Modified by Prasanth on 01/Jul/2015 Start (Introduced Time To fill in Days)
                //return MiscUtil.FindDateDiffFrmEndtoStart(jobPosting.FinalHiredDate, jobPosting.OpenDate);
                TT =(jobPosting.FinalHiredDate - jobPosting.OpenDate).TotalDays.ToString();
            if (Int32.Parse(TT) == 1)
                return   TT + " Day";
                return TT  + " Days";
                //****************************END**********************************
        }

        string PayRate(JobPosting jobPosting)
        {

            //string payrate = "";
            //payrate = jobPosting.PayRate;
            //if (Convert.ToInt64(jobPosting.MaxPayRate) == jobPosting.MaxPayRate) jobPosting.MaxPayRate = Convert.ToInt64(jobPosting.MaxPayRate);
            //if (payrate != string.Empty && jobPosting.MaxPayRate > 0 && payrate.ToLower().Trim() != "open") payrate += "-";
            //if (jobPosting.MaxPayRate > 0 && payrate.ToLower().Trim() != "open") payrate += jobPosting.MaxPayRate;


            //string paymethod = "";
            //switch (jobPosting.PayCycle)
            //{
            //    case "1":
            //        paymethod = "Hourly";
            //        break;
            //    case "4":
            //        paymethod = "Yearly";
            //        break;
            //    case "3":
            //        paymethod = "Monthly";
            //        break;
            //    case "2":
            //        paymethod = "Daily";
            //        break;
            //}
            //if (paymethod.Length > 0 && (payrate != ""))
            //    return payrate + " / " + paymethod;
            //else
            //    if (payrate != "")
            //        return payrate;
            //    else
                    return "";
        }

        string BillRate(JobPosting jobPosting)
        {
            string paycycle = "";
            //switch (jobPosting.ClientRatePayCycle)
            //{
            //    case "1":
            //        paycycle = "hour";
            //        break;
            //    case "4":
            //        paycycle = "year";
            //        break;
            //    case "3":
            //        paycycle = "month";
            //        break;
            //    case "2":
            //        paycycle = "day";
            //        break;
            //}
            //if (paycycle.Length > 0 && (jobPosting.ClientHourlyRate != ""))
            //    return jobPosting.ClientHourlyRate + " / " + paycycle;
            //else
            //    if (jobPosting.ClientHourlyRate != "")
            //        return jobPosting.ClientHourlyRate;
            //    else
                    return "";
        }

        protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            HiringHD="";
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
                int ColLocation = 3;
                if (jobPosting != null)
                {
                    System.Web.UI.HtmlControls.HtmlTableRow tdLevels = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("row");
                    foreach (Hiring_Levels lev in jobPosting.HiringMatrixLevels)
                    {
                        //if (checkCheckBoxHiringLevelSelected(lev.HiringMatrixLevelID))
                        //{
                            System.Web.UI.HtmlControls.HtmlTableCell col1 = new System.Web.UI.HtmlControls.HtmlTableCell("TD");
                            Label lnkLevel = new Label();
                            lnkLevel.Text = lev.CandidateCount.ToString();

                            HiringMatrixLevels l = Facade.GetHiringMatrixLevelsById(lev.HiringMatrixLevelID);
                            HiringHD = HiringHD + "#" + l.Name;
                        
                            col1.EnableViewState = true;
                            //lnkLevel.CommandName = "Sort";
                            //lnkLevel.CommandArgument = lev.HiringMatrixLevelID;
                            //lnkLevel.ToolTip = l.Name;
                        
                            col1.Controls.Add(lnkLevel);
                            col1.EnableViewState = true;
                            tdLevels.Controls.AddAt(ColLocation, col1);
                            ColLocation++;
                        //}
                    }
                    hdnMatrixLevels.Value = HiringHD;
                    //JobTitle

                    //OpenDate

                    //CloseDate




                    //Post Date
                    Label lblPostdate = (Label)e.Item.FindControl("lblPostdate");
                    HtmlTableCell tdPostdate = (HtmlTableCell)e.Item.FindControl("tdPostdate");
                    //if (chkColumnList.Items.FindByValue("PostedDate").Selected)
                    //{
                    lblPostdate.Text = jobPosting.OpenDate.ToShortDateString();
                    lblPostdate.Visible = true;
                    tdPostdate.Visible = true;
                    //}
                    //else
                    //{
                    //    tdPostdate.Visible = false;
                    //    lblPostdate.Visible = false;
                    //}

                    //JobTitle
                    HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");//  0.4 
                    HtmlTableCell tdReqTitle = (HtmlTableCell)e.Item.FindControl("tdReqTitle");
                    //if (chkColumnList.Items.FindByValue("JobTitle").Selected)
                    //{
                    lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    lnkJobTitle.Text = jobPosting.JobTitle;
                    lnkJobTitle.Visible = true;
                    tdReqTitle.Visible = true;
                    //}
                    //else
                    //{
                    //    tdReqTitle.Visible = false;
                    //    lnkJobTitle.Visible = false;
                    //}



                    Label lblFinalHiredDate = (Label)e.Item.FindControl("lblFinalHiredDate");
                    HtmlTableCell tdFinalHiredDate = (HtmlTableCell)e.Item.FindControl("tdFinalHiredDate");
                    //if (chkColumnList.Items.FindByValue("FinalHiredDate").Selected)
                    //{
                    lblFinalHiredDate.Text = (MiscUtil.IsValidDate(jobPosting.FinalHiredDate) ? jobPosting.FinalHiredDate.ToShortDateString() : string.Empty);
                    tdFinalHiredDate.Visible = true;
                    lblFinalHiredDate.Visible = true;
                    //}
                    //else
                    //{
                    //    lblFinalHiredDate.Visible = false;
                    //    tdFinalHiredDate.Visible = false;
                    //}


                    Label lblTotalTimeTaken = (Label)e.Item.FindControl("lblTotalTimeTaken");
                    HtmlTableCell tdTotalTimeTaken = (HtmlTableCell)e.Item.FindControl("tdTotalTimeTaken");
                    tdTotalTimeTaken.Visible = true;
                    
                    //Code Introduced by Prasanth on 24/Jun/2015
                    if (jobPosting.TotalTimeTaken.ToString() == "1")
                    {
                        lblTotalTimeTaken.Text = jobPosting.TotalTimeTaken.ToString() + " Day"; 
                    }
                    else
                    {
                        lblTotalTimeTaken.Text = jobPosting.TotalTimeTaken.ToString() + " Days"; 
                    }
                    //*******************END***********************

                    //Code introduced by Prasanth on 11/Jun/2015 Start
                    Label lblTimeToFill = (Label)e.Item.FindControl("lblTimeToFill");
                    HtmlTableCell tdTimeToFill = (HtmlTableCell)e.Item.FindControl("tdTimeToFill");
                    
                    lblTimeToFill.Text = TimeToFill(jobPosting);
                  
                    //***********************END**********************


                }

            }

        }

        string JobDurationMonth(JobPosting jobPosting)
        {
            //if (jobPosting.JobDurationMonth.Trim() == string.Empty)
            //{
            //    return "";
            //}
            //else
            //{
            //    if (Convert.ToInt32(jobPosting.JobDurationMonth) > 0)
            //    {
            //        GenericLookup gklookup = Facade.GetGenericLookupById(Convert.ToInt32(jobPosting.JobDurationMonth));
            //        if (gklookup != null)
            //        {
            //            return gklookup.Name;
            //        }
            //        else
            //        {
            //            return "";
            //        }
            //    }
            //    else
                    return "";
            //}
        }

        string StartDate(JobPosting jobPosting)
        {
            if (System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern == "dd/MM/yyyy")
            {
                return jobPosting.StartDate.ToString() == "" || jobPosting.StartDate.ToString().Trim() == "ASAP" ? jobPosting.StartDate.ToString() : (jobPosting.StartDate.ToString().Split('/')[1] + "/" + jobPosting.StartDate.ToString().Split('/')[0] + "/" + jobPosting.StartDate.ToString().Split('/')[2]);
            }
            else
            {
                return jobPosting.StartDate.Trim() != string.Empty ? (jobPosting.StartDate.Trim() != "ASAP" ? Convert.ToDateTime(jobPosting.StartDate).ToShortDateString() : "ASAP") : " ";
            }
        }


        string Category(JobPosting jobPosting)
        {
            //if (jobPosting.JobCategoryLookupId > 0)
            //{
            //    Category ctg = Facade.GetCategoryById(jobPosting.JobCategoryLookupId);
            //    if (ctg != null)
            //        return ctg.Name;
            //    else
            //        return "";
            //}
            //else
                return "";
        }

        string WorkSchedule(JobPosting jobPosting)
        {
            //GenericLookup genlkp = Facade.GetGenericLookupById(jobPosting.WorkScheduleLookupId);
            //if (genlkp != null)
            //    return genlkp.Name;
            //else
                return "";
        }

        string AssignedRecruiters(JobPosting jobPosting)
        {
            //IList<JobPostingHiringTeam> teamMembers = Facade.GetAllJobPostingHiringTeamByJobPostingId(jobPosting.Id);
            //StringBuilder sb = new StringBuilder();
            //if (teamMembers != null)
            //{
            //    foreach (JobPostingHiringTeam item in teamMembers)
            //    {
            //        Member mem = Facade.GetMemberById(item.MemberId);
            //        if (mem != null)
            //            sb.Append(mem.FirstName + " " + mem.MiddleName + " " + mem.LastName).Append(", ");
            //    }
            //    if (sb.Length > 0)
            //    {
            //        int lastindex = sb.ToString().LastIndexOf(',');
            //        return sb.ToString().Substring(0, lastindex);
            //    }
            //    else
            //    {
            //        return "";
            //    }
            //}
            //else
                return "";
        }

        #endregion

        #region Pager Events

        private void ResetPager()
        {
            DataPager pager = this.lsvJobPosting.FindControl("pager") as DataPager;

            if (pager != null)
            {
                pager.SetPageProperties(0, pager.MaximumRows, true);
            }
        }
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvJobPosting.FindControl(hdnSortColumn.Value);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                int d = 0;
                Int32.TryParse(lnk.CommandArgument, out d);
                if (d > 0 || lnk.CommandArgument == "OpenDate")
                {
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Descending" : "Ascending"));

                }
                else
                {
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
                }

              }
            catch 
            {
               
            }
        }
        #endregion


//**********************************************END*********************************************
    }
}
