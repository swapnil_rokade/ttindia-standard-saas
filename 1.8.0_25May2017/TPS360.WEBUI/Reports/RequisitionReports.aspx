﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:RequisitionReports.ascx
    Description By:This is user control common for RequisitionReports.aspx
    Created By:Sumit SonawaneRequisitionReports
    Created On:25/May/2016
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
      0.1               30/June/2016       pravin khot         added ApplyDefaultSiteSetting="true
      0.1               06Mar2017          Prasanth Kumar G    issue id 1183
    ---------------------------------------------------------------------------------------------------------------------------------------     

--%>

<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="RequisitionReports.aspx.cs"
    EnableEventValidation="false" Inherits="TPS360.Web.UI.Reports.RequisitionReports"
    Title="Requisition Report" %>
    <%@ Register Src="~/Controls/DateRangePicker.ascx" TagName = "DateRangePicker" TagPrefix ="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src ="~/Controls/CountryStatePicker.ascx" TagName ="CountryState" TagPrefix ="ucl" %>
<asp:Content ID="title" ContentPlaceHolderID="cphHomeMasterTitle" runat="server">
    Requisition Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>

    <script src="../Scripts/ToolTipscript.js"></script>
    <script src="../js/JobPostingByStatus.js"></script>
    <link type="text/css" href="../Style/ToolTip.css" rel="Stylesheet" />
    <script language="javascript" type="text/javascript">
        
        //To Keep the scroll bar in the Exact Place
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
        function PageLoadedReq(sender, args) {
            try {
                var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
                var bigDiv = document.getElementById('ctl00_cphHomeMaster_divlsvJobPosting');
                bigDiv.scrollLeft = hdnScroll.value;
            }
            catch (e) {
            }
        }
        function SetScrollPosition() {
            var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
            var bigDiv = document.getElementById('ctl00_cphHomeMaster_divlsvJobPosting');
            hdnScroll.value = bigDiv.scrollLeft;
        }
    </script>

    <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

    <script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

    <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
    <asp:UpdatePanel ID="pnlCandidatereport" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="text-align: left">
                <asp:Label ID="lbMessage" runat="server" EnableViewState="false"></asp:Label>
            </div>
            <asp:HiddenField ID="hdnSortColumn" runat="server" />
            <asp:HiddenField ID="hdnRequisitionSelected" runat="server" Value="0" />
            <asp:HiddenField ID="hdnSortOrder" runat="server" />
            <asp:HiddenField ID="hdnScrollPos" runat="server" />
            <asp:ObjectDataSource ID="odsJobPostingList" runat="server" SelectMethod="GetPaged"
                TypeName="TPS360.Web.UI.JobPostingDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:Parameter Name="IsTemplate" DefaultValue="False" Type="Boolean" Direction="Input" />
                    <asp:ControlParameter ControlID="ddlJobStatus" Name="jobStatus" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlJobStatus" Name="subJobStatus" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="dtPicker" Name="jobStatusStartDate"
                        PropertyName="StartDate" Type="String" />
                    <asp:ControlParameter ControlID="dtPicker" Name="jobStatusEndDate" PropertyName="EndDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlReqCreator" Name="ReqCreator" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlEmployee" Name="employee" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="TxtCity" Name="City" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="uclCountryState" Name="StateID" PropertyName="SelectedStateId"
                        Type="String" />
                    <asp:ControlParameter ControlID="uclCountryState" Name="CountryID" PropertyName="SelectedCountryId"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlEndClient" Name="endClients" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="hdnRequisitionSelected" Name="JobPostingId" PropertyName="Value"
                        Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
              
            <asp:Panel ID="pnlSearchRegion" runat="server" DefaultButton="btnSearch">
                <div style="width: 100%;">
                    <div class="TableRow">
                        <div class="TabPanelHeader nomargin" style="border-style: none none none;">Filter Options</div>
                    </div>
                    <div class="TableRow" style="text-align: center">
                        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                    </div>
                    <div class="FormLeftColumn" style="width: 50%;">
                        <div class="TableRow" style="white-space: nowrap">
                            <div class="TableFormLeble" style="width: 50%;">
                                <asp:Label EnableViewState="false" ID="lblDateHeader" runat="server" Text="Post Date Range"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                              <ucl:DateRangePicker ID="dtPicker" runat ="server"  />
                            </div>
                        </div>
                       
                        <div class="TableRow" style="white-space: nowrap;">
                            <div class="TableFormLeble" style="width: 50%;">
                                <asp:Label EnableViewState="false" ID="lblByLocation" runat="server" Text="By City"></asp:Label>
                                :
                            </div>
                            <div class="TableFormContent">
                                <asp:TextBox EnableViewState="false" ID="txtCity" CssClass="SearchTextbox" runat="server"></asp:TextBox>
                            </div>
                        </div>
                       <%-- ************added pravin khot on 30/June/2016*****-ApplyDefaultSiteSetting="true"--%>
                      <ucl:CountryState ID="uclCountryState" runat="server" FirstOption="Any" ApplyDefaultSiteSetting="false" TableFormLabel_Width="50%" ShowAndHideState ="False" />
<%--                      ****************************END*******************************--%> 
                  </div>
                    <div class="FormRightColumn" style="width: 49%">
                        <div class="TableRow">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="false" ID="lblJobStatusHeader" runat="server" Text="By Requisition Status"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList EnableViewState="true" ID="ddlJobStatus" runat="server" CssClass="CommonDropDownList"
                                    AutoPostBack="false" OnSelectedIndexChanged="ddlJobStatus_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="false" ID="lblByRequisition" runat="server" Text="By Requisition"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList EnableViewState="true" ID="ddlRequisition" runat="server" CssClass="CommonDropDownList"
                                    AutoPostBack="false">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="false" ID="lblByReqCreator" runat="server" Text="By Req Creator"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList ID="ddlReqCreator" runat="server" CssClass="CommonDropDownList">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="false" ID="lblByEmployee" runat="server" Text="By User"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList ID="ddlEmployee" CssClass="CommonDropDownList" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble" style="width: 40%;">
                                <asp:Label EnableViewState="true" ID="lblByEndClientsHeader" runat="server"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList ID="ddlEndClient" CssClass="CommonDropDownList" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="TableRow well well-small nomargin">
                        <asp:CollapsiblePanelExtender ID="cpnlCandidateTopBar" runat="server" TargetControlID="pnlContent"
                            ExpandControlID="pnlHeader" CollapseControlID="pnlHeader" Collapsed="true" ImageControlID="imgShowHide"
                            CollapsedImage="~/Images/expand-plus.png" ExpandedImage="~/Images/collapse-minus.png"
                            SuppressPostBack="true">
                        </asp:CollapsiblePanelExtender>
                        <asp:Panel ID="pnlHeader" runat="server">
                            <div class="" style="clear: both; cursor :pointer ;">
                                <asp:Image ID="imgShowHide" ImageUrl="~/Images/expand-plus.png" runat="server" />
                                Included Columns
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlContent" runat="server" Style="overflow: hidden;" Height="0">
                            <div class="TableRow" style="text-align: left; padding-top: 5px">
                                <asp:CheckBox runat="server" Text="Select All" ID="chkColumns" CssClass ="ColumnSelection" onclick="javascript:SelectUnselectColumnOption(this);" />
                                <asp:CheckBoxList ID="chkColumnList" runat="server" RepeatColumns="5" CssClass ="ColumnSelection" onclick="javascript:SelectUnselectAllColumnOption('chkColumns')">
                                    <asp:ListItem Value="PostedDate" Selected="True">Post Date</asp:ListItem>
                                    <asp:ListItem Value="JobTitle" Selected="True">Job Title</asp:ListItem>
                                    <asp:ListItem Value="JobPostingCode" Selected="True">Req. Code</asp:ListItem>
                                    <asp:ListItem Value="TimeToFill" Selected="True">Time To Fill</asp:ListItem>
                                    <asp:ListItem Value="ClientJobId">Job ID</asp:ListItem>
                                    <asp:ListItem Value="NoOfOpenings" Selected="True">No of Openings</asp:ListItem>
                                    <asp:ListItem Value="NoofUnfilledOpenings" Selected="True">No. of Unfilled Openings</asp:ListItem>
                                    <asp:ListItem Value="PayRate" Selected="True">Pay Rate</asp:ListItem>
                                    <%--<asp:ListItem Value="JobLocation"  Selected="True">Job Location</asp:ListItem>--%>
                                    <%--<asp:ListItem Value="SalesRegion" Enabled="false" Selected="True">Sales Region</asp:ListItem>
                                    <asp:ListItem Value="SalesGroup" Selected="True">Sales Group</asp:ListItem>
                                    <asp:ListItem Value="POAvailability" Selected="True">PO Availability</asp:ListItem>--%>
                                   <%-- <asp:ListItem Value="CustomerName" Selected="True">Customer Name</asp:ListItem>--%>
                                    
                                    <%--<asp:ListItem Value="TravelRequired">Travel Required</asp:ListItem>
                                    <asp:ListItem Value="OtherBenefits">Benefits</asp:ListItem>--%>
                                    <asp:ListItem Value="JobStatus" Selected="True">Current Status</asp:ListItem>
                                    <%--<asp:ListItem Value="JobDurationLookupId" Selected="True">Employment Type</asp:ListItem>
                                    <asp:ListItem Value="JobDurationMonth" Selected="True">Duration</asp:ListItem>
                                    <asp:ListItem Value="JobAddress1">Address 1</asp:ListItem>
                                    <asp:ListItem Value="JobAddress2">Address 2</asp:ListItem>--%>
                                    <asp:ListItem Value="City" Selected="True">City</asp:ListItem>
                                    <%--<asp:ListItem Value="StateId" Selected="True">State</asp:ListItem>
                                    <asp:ListItem Value="ZipCode">Zip Code</asp:ListItem>--%>
                                    <asp:ListItem Value="CountryId">Country</asp:ListItem>
                                    <%--<asp:ListItem Value="ReportingTo">Reporting To</asp:ListItem>
                                    <asp:ListItem Value="NoOfReportees">No. of Reportees</asp:ListItem>--%>
                                    <asp:ListItem Value="StartDate" Selected="True">Start Date</asp:ListItem>
                                    <asp:ListItem Value="FinalHiredDate" Selected="True">Closing Date</asp:ListItem>
                                    <%--<asp:ListItem Value="AuthorizationTypeLookupId">Work Authorization</asp:ListItem>--%>
                                    
                                    <asp:ListItem Value="RequiredDegreeLookupId">Education</asp:ListItem>
                                    <asp:ListItem Value="ExpRequired">Exp Required</asp:ListItem>
                                    <asp:ListItem Value="ClientId" Selected="True">BU</asp:ListItem>
                                    <asp:ListItem Value="BUContact" Selected="True">BU Contact</asp:ListItem>
                                    <%--<asp:ListItem Value="TeleCommunication">Telecommuting</asp:ListItem>--%>
                                    <asp:ListItem Value="CreatorId" Selected="True">Published By</asp:ListItem>
                                    <asp:ListItem Value="UpdateDate">Date Updated</asp:ListItem>
                                    <asp:ListItem Value="JobCategory">Job Category</asp:ListItem>
                                    
                                    <asp:ListItem Value="Skills" Selected="True">Skills</asp:ListItem>
                                    <%--<asp:ListItem Value="MinimumQualifyingParameters">Minimum Qualifying Parameters</asp:ListItem>
                                    <asp:ListItem Value="WorkSchedule" Selected="True">Work Schedule</asp:ListItem>
                                    <asp:ListItem Value="SecurityClearance">Security Clearance</asp:ListItem>
                                    <asp:ListItem Value="Expenses">Expenses</asp:ListItem>--%>
                                    <asp:ListItem Value="AdditionalNotes">Additional Notes</asp:ListItem>
                                    <asp:ListItem Value="AssignedRecruiters" Selected="True">Assigned Recruiters</asp:ListItem>
                                    <asp:ListItem Value="TurnArroundTime" Selected="True">
                                                    
                                                        Turn Around Time <input class="span2" type="image" style ="  width :15px; height :15px" src ="../Images/tooltip_icon.png"  rel="tooltip" data-original-title="Time between the requisition publish date and the first candidate offer." data-placement="right" value="" onclick ="javascript:return false;"   >                                                             
                                    </asp:ListItem>
                                    <asp:ListItem Value="ScheduledInterviews">Scheduled Interviews </asp:ListItem>
                                    <asp:ListItem Value="Submissions">Submissions</asp:ListItem>
                                    <asp:ListItem Value="Offers">Offers</asp:ListItem>
                                    <asp:ListItem Value="Joined">Joined</asp:ListItem>
                                    <asp:ListItem Value="JPICN" Text="Interview Candidate Name"></asp:ListItem>
                                    <asp:ListItem Value="JPSCN" Text="Submitted Candidate Name"></asp:ListItem>
                                    <asp:ListItem Value="JPOCN" Text="Offered Candidate Name"></asp:ListItem>
                                    <asp:ListItem Value="JPJCN" Text="Joined Candidate Name"></asp:ListItem>
                                    <asp:ListItem Value="JobDesc" Text="Job Description"></asp:ListItem>
                                    <asp:ListItem Value="Jobpostnotes" Text="Notes"></asp:ListItem>
                                    <asp:ListItem Value="RequisitionType" Text="Requisition Type"></asp:ListItem>
                                    
                                </asp:CheckBoxList>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="PanelDevider">
                    </div>
                    <div class="TableRow well well-small nomargin">
                        <asp:CollapsiblePanelExtender ID="cpnlHiringMatrix" runat="server" TargetControlID="pnlHiringMatrix"
                            ExpandControlID="pnlHiringHeader" CollapseControlID="pnlHiringHeader" Collapsed="true"
                            ImageControlID="imgShowHideHirining" CollapsedImage="~/Images/expand-plus.png"
                            ExpandedImage="~/Images/collapse-minus.png" SuppressPostBack="true">
                        </asp:CollapsiblePanelExtender>
                        <asp:Panel ID="pnlHiringHeader" runat="server">
                            <div class="" style="clear: both; cursor :pointer ;">
                                <asp:Image ID="imgShowHideHirining" ImageUrl="~/Images/expand-plus.png" runat="server" />
                                Hiring Matrix Levels
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlHiringMatrix" runat="server" Style="overflow: hidden;" Height="0">
                            <div class="TableRow" style="text-align: left; padding-top: 5px">
                                <asp:CheckBox runat="server" Text="Select All Levels" ID="chkAll" CssClass ="ColumnSelection" onclick="javascript:SelectUnselectheader(this);" />
                                <div id="divHiringList">
                                    <asp:CheckBoxList ID="chkHiringList" runat="server" RepeatColumns="5" onclick="javascript:SelectUnselectAllMLevels('chkAll')" CssClass ="ColumnSelection">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px">
                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search" CssClass="btn btn-primary"
                            EnableViewState="false" OnClick="btnSearch_Click" ValidationGroup="Search" ><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                        <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                            EnableViewState="false" OnClick="btnClear_Click" />
                    </div>
                    <div class="TableRow" runat="server" id="ExportButtons" style="padding-bottom: 5px;">
                        <div class="TabPanelHeader nomargin" style="margin-bottom: 5px;border-style: none none none;">Report Results</div>
                         <asp:ImageButton ID="btnExportToExcel" CssClass ="btn"  SkinID ="sknExportToExcel" runat ="server" ToolTip ="Export To Excel" OnClick ="btnExportToExcel_Click" />
                            <asp:ImageButton ID="btnExportToPDF" CssClass ="btn"  SkinID ="sknExportToPDF" runat ="server" ToolTip ="Export To PDF" OnClick ="btnExportToPDF_Click" />
                            <asp:ImageButton ID="btnExportToWord" CssClass ="btn"  SkinID ="sknExportToWord" runat ="server" ToolTip ="Export To Word" OnClick ="btnExportToWord_Click" />
                            
                       <%--    <asp:Button ID="btnExportToPDF" runat="server" Text="Export To PDF" AlternateText="Export To PDF"
                            CssClass="btn" EnableViewState="false" OnClick="btnExportToPDF_Click" />
                        <asp:Button ID="btnExportToExcel" runat="server" Text="Export To Excel" AlternateText="Export To Excel"
                            CssClass="btn" EnableViewState="false" OnClick="btnExportToExcel_Click" />
                        <asp:Button ID="btnExportToWord" runat="server" Text="Export To Word" AlternateText="Export To Word"
                            CssClass="btn" EnableViewState="false" OnClick="btnExportToWord_Click" />
                            --%>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlGridRegion" runat="server">
                <div id="divlsvJobPosting" runat="server" class="GridContainer" style="overflow: auto;
                    overflow-y: hidden; width: 100%; text-align: left;" onscroll='SetScrollPosition()'>
                    <%--0.7--%>
                    <asp:ListView ID="lsvJobPosting" runat="server" DataKeyNames="Id" OnItemDataBound="lsvJobPosting_ItemDataBound"
                        OnItemCommand="lsvJobPosting_ItemCommand" OnPreRender="lsvJobPosting_PreRender"
                        EnableViewState="true">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid ReportGrid" cellspacing="0" border="0">
                                <tr id="trHeader" runat="server">
                                    <th runat="server" id="thPostDate" style="min-width: 100px; white-space: nowrap"  enableviewstate ="false" >
                                    <%--Code commented by Prasanth on 06Mar2017 Start--%>
                                    <%--    <asp:LinkButton runat="server" ID="btnPostdate" CommandName="Sort" CommandArgument="[J].[PostedDate]"
                                            Text="Post Date" ToolTip="Sort By Post Date" EnableViewState="false" />--%>
                                      <%--******************END***************--%>
                                            <%--Code introduced by Prasanth on 06Mar2017 Start--%>
                                                <asp:LinkButton runat="server" ID="btnPostdate" CommandName="Sort" CommandArgument="[J].[CreateDate]"
                                            Text="Post Date" ToolTip="Sort By Post Date" EnableViewState="false" />
                                            <%--******************END***************--%>
                                    </th>
                                    
                                    <th runat="server" id="thJobTitle" style="min-width: 100px; white-space: nowrap"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnJobTitle" CommandName="Sort" CommandArgument="[J].[JobTitle]"
                                            Text="Job Title" ToolTip="Sort By Job Title" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thJobPostingCode" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnJobPostingCode" CommandName="Sort" CommandArgument="[J].[JobPostingCode]"
                                            Text="Req. Code"  ToolTip="Sort By Requisition code" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thTimeToFill" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:Label runat="server" ID="btnTimeToFill" CommandName="Sort" CommandArgument="[J].[dateDifferce]"
                                            Text="Time To Fill" ToolTip="Sort By Time To Fill" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thClientJobId" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnClientJobId" CommandName="Sort" CommandArgument="[J].[ClientJobId]"
                                            Text="Job ID " ToolTip="Sort By Account Job ID" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thNoOfOpenings" style="min-width: 130px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnNoOfOpenings" CommandName="Sort" CommandArgument="[J].[NoOfOpenings]"
                                            Text="No of Openings" ToolTip="Sort By No. of Openings" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thNoofUnfilledOpenings" style="min-width: 170px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="lnkNoofUnfilledOpenings" CommandName="Sort" CommandArgument="NoOfUnfilledOpenings"
                                            Text="No of Unfilled Openings" ToolTip="Sort By No. of Unfilled Openings" EnableViewState="false" />
                                    </th>
                                    <%--<th runat="server" id="thPayRate" style="min-width: 120px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnPayRate" CommandName="Sort" CommandArgument="[J].[PayRate]"
                                            Text="Min Pay Rate" ToolTip="Sort By Pay Rate" EnableViewState="false" />
                                    </th>--%>
                                   
                                     <%--<th runat="server" id="thJobLocation" style="min-width: 120px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnJobLocation" CommandName="Sort" CommandArgument="[JobLocationLookUpId]"
                                            Text="Job Location" ToolTip="Sort Job Location" EnableViewState="false" />
                                    </th>--%>
                                 
                                    
                                    <%--<th runat="server" id="thSalesRegion" style="min-width: 120px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnSalesRegion" CommandName="Sort" CommandArgument="[SalesRegionLookup]"
                                            Text="Sales Region" ToolTip="Sort Sales Region" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thSalesGroup" style="min-width: 120px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnSalesGroup" CommandName="Sort" CommandArgument="[SalesGroupLookup]"
                                            Text="Sales Group" ToolTip="Sort Sales Group" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thPOAvailability" style="min-width: 120px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnPOAvailability" CommandName="Sort" CommandArgument="[J].[POAvailability]"
                                            Text="PO Availability" ToolTip="Sort PO Availability" EnableViewState="false" />
                                    </th>--%>
                                    <%--  <th runat="server" id="thCustomerName" style="min-width: 120px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnCustomerName" CommandName="Sort" CommandArgument="[J].[CustomerName]"
                                            Text="Customer Name" ToolTip="Sort Customer Name" EnableViewState="false" />
                                    </th>--%>
                                    
                                    
                                   <%-- <th runat="server" id="thTravelRequired" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnTravelRequired" CommandName="Sort" CommandArgument="[J].[TravelRequired]"
                                            Text="Travel Required" ToolTip="Sort By Travel Required" EnableViewState="false" />
                                    </th>--%>
                                    <%--<th runat="server" id="thOtherBenefits" style="min-width: 100px"  enableviewstate ="false" >
                                        Benefits
                                    </th>--%>
                                    <th runat="server" id="thReqStatus" style="min-width: 120px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnReqStatus" CommandName="Sort" CommandArgument="[J].[Name]"
                                            Text="Current Status" ToolTip="Sort By Current Status" EnableViewState="false" />
                                    </th>
                                    <%--<th runat="server" id="thJobDurationLookupId" style="min-width: 100px"  enableviewstate ="false" >
                                        Employment Type
                                    </th>--%>
                                    <%--<th runat="server" id="thJobDurationMonth" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnJobDurationMonth" CommandName="Sort" CommandArgument="[J].[JobDurationMonth]"
                                            Text="Duration" ToolTip="Sort By Duration" EnableViewState="false" />
                                    </th>--%>
                                    <%--<th runat="server" id="thJobAddress1" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnJobAddress1" CommandName="Sort" CommandArgument="[J].[JobAddress1]"
                                            Text="Address 1" ToolTip="Sort By Address 1" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thJobAddress2" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnJobAddress2" CommandName="Sort" CommandArgument="[J].[JobAddress2]"
                                            Text="Address 2" ToolTip="Sort By Address 2" EnableViewState="false" />
                                    </th>--%>
                                    <th runat="server" id="thCity" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnCity" CommandName="Sort" CommandArgument="[J].[City]"
                                            Text="City" ToolTip="Sort By City" Width="40%" EnableViewState="false" />
                                    </th>
                                   <%-- <th runat="server" id="thStateId" style="min-width: 100px "  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnStateId" CommandName="Sort" CommandArgument="[J].[StateId]"
                                            Text="State" ToolTip="Sort By State" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thZipCode" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnZipCode" CommandName="Sort" CommandArgument="[J].[ZipCode]"
                                            Text="Zip Code" ToolTip="Sort By Zip Code" />
                                    </th>--%>
                                    <th runat="server" id="thCountryId" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnCountryId" CommandName="Sort" CommandArgument="[J].[CountryId]"
                                            Text="Country" ToolTip="Sort By Country" EnableViewState="false" />
                                    </th>
                                    <%--<th runat="server" id="thReportingTo" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnReportingTo" CommandName="Sort" CommandArgument="[J].[ReportingTo]"
                                            Text="Reporting To" ToolTip="Sort By Reporting To" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thNoOfReportees" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnNoOfReportees" CommandName="Sort" CommandArgument="[J].[NoOfReportees]"
                                            Text="No Of Reportees" ToolTip="Sort By No Of Reportees" EnableViewState="false" />
                                    </th>--%>
                                    <th runat="server" id="thStartDate" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnStartDate" CommandName="Sort" CommandArgument="[J].[StartDate]"
                                            Text="Start Date" ToolTip="Sort By Start Date" />
                                    </th>
                                    <th runat="server" id="thFinalHiredDate" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnFinalHiredDate" CommandName="Sort" CommandArgument="[J].[FinalHiredDate]"
                                            Text="Closing Date" ToolTip=" Sort By Closing Date" EnableViewState="false" />
                                    </th>
                                    <%--<th runat="server" id="thAuthorizationTypeLookupId" style="min-width: 130px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnAuthorizationTypeLookupId" CommandName="Sort"
                                            CommandArgument="[J].[AuthorizationTypeLookupId]" Text="Work Authorization" ToolTip="Sort By Work Authorization" />
                                    </th>--%>
                                    
                                    <th runat="server" id="thRequiredDegreeLookupId" style="min-width: 100px"  enableviewstate ="false" >
                                        Education
                                    </th>
                                    <th runat="server" id="thExpRequired" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:Label runat="server" ID="btnExpRequired" CommandName="Sort" CommandArgument="ABS([J].[MinExpRequired])+''  ''+ABS([J].[MaxExpRequried])"
                                            Text="Exp Required" ToolTip="Sort By Exp Required" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thClientId" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnClientId" CommandName="Sort" CommandArgument="[J].[ClientId]"
                                            Text="BU" ToolTip="Sort By BU" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thBUContact" style="min-width: 100px" enableviewstate="false"> 
                                     <asp:LinkButton runat="server" ID="btnBUContact" CommandName="Sort" CommandArgument="[CC].[FirstName]"
                                            Text="BU Contact" ToolTip="Sort By BU Contact" EnableViewState="false" />
                                    </th>
                                    
                                    <%--<th runat="server" id="thTeleCommunication" style="min-width: 100px"  enableviewstate ="false" >
                                        Telecommuting
                                    </th>--%>
                                    <th runat="server" id="thCreatorId" style="min-width: 100px"  enableviewstate ="false" > 
                                        <asp:LinkButton runat="server" ID="btnCreatorId" CommandName="Sort" CommandArgument="[J].[CreatorId]"
                                            Text="Published By" ToolTip="Sort By Published By" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thUpdateDate" style="min-width: 120px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnUpdateDate" CommandName="Sort" CommandArgument="[J].[UpdateDate]"
                                            Text="Date Updated" ToolTip="Sort By Date Updated" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thJobCategory" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnJobCategory" CommandName="Sort" CommandArgument="[JobCategoryLookupId]"
                                            Text="Job Category" ToolTip="Sort By JobCategory" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thSkills" style="min-width: 100px"  enableviewstate ="false" >
                                        Skills
                                    </th>
                                    <%--<th runat="server" id="thMinimumQualifyingParameters" style="min-width: 100px"  enableviewstate ="false" >
                                        Minimum Qualifying Parameters
                                    </th>
                                    <th runat="server" id="thWorkSchedule" style="min-width: 120px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnWorkSchedule" CommandName="Sort" CommandArgument="[J].[WorkScheduleLookupId]"
                                            Text="Work Schedule" ToolTip="Sort By Work Schedule" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thSecurityClearance" style="min-width: 130px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnSecurityClearance" CommandName="Sort" CommandArgument="[J].[SecurityClearance]"
                                            Text="Security Clearance" ToolTip="Sort By Security Clearance" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thExpenses" style="min-width: 100px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnExpenses" CommandName="Sort" CommandArgument="[J].[IsExpensesPaid]"
                                            Text="Expenses" ToolTip="Sort By Expenses" EnableViewState="false" />
                                    </th>--%>
                                    <th runat="server" id="thAdditionalNotes" style="min-width: 120px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnAdditionalNotes" CommandName="Sort" CommandArgument="[J].[InternalNote]"
                                            Text="Additional Notes" ToolTip="Sort By Additional Notes" EnableViewState="false" />
                                    </th>
                                    <th runat="server" id="thAssignedRecruiters" style="min-width: 100px"  enableviewstate ="false" >
                                        Assigned Recruiters
                                    </th>
                                    <th runat="server" id="thTurnArroundTime" style="min-width: 130px;"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnTurnArroundTime" CommandName="Sort" CommandArgument="[JPTAT].[TurnArroundTime]"
                                            Text="Turn Around Time" ToolTip="Sort By Turn Around Time" EnableViewState="false"></asp:LinkButton>
                                    </th>
                                    <th runat="server" id="thScheduledInterviews" style="min-width: 170px;"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="lnkScheduledInterviews" CommandName="Sort" CommandArgument="[JIC].InterviewCount"
                                            Text="Scheduled Interviews" ToolTip="Sort By Scheduled Interviews" EnableViewState="false"></asp:LinkButton>
                                    </th>
                                    <th runat="server" id="thSubmissions" style="min-width: 120px;"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="lnkSubmissions" CommandName="Sort" CommandArgument="[JSDC].SubmissionCount"
                                            Text="Submissions" ToolTip="Sort By Submissions" EnableViewState="false"></asp:LinkButton>
                                    </th>
                                    <th runat="server" id="thOffers" style="min-width: 110px;"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="lnkOffers" CommandName="Sort" CommandArgument="[JODC].OfferCount"
                                            Text="Offers" ToolTip="Sort By Offers" EnableViewState="false"></asp:LinkButton>
                                    </th>
                                    <th runat="server" id="thJoined" style="min-width: 110px;"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="lnkJoined" CommandName="Sort" CommandArgument="[JJDC].JoiningCount"
                                            Text="Joined" ToolTip="Sort By Joined" EnableViewState="false"></asp:LinkButton>
                                    </th>
                                    
                                    <th runat="server" id="thInterviewCandidate" style="min-width: 110px;"  enableviewstate ="false" >
                                        <asp:Label runat="server" ID="lnkInterviewCandidate" CommandName="Sort" CommandArgument="[JJDC].JoiningCount"
                                            Text="Interview Candidate Name" ToolTip="Sort By Interview Candidate Name" EnableViewState="false"></asp:Label>
                                    </th>
                                    <th runat="server" id="thSubmittedCandidate" style="min-width: 110px;"  enableviewstate ="false" >
                                        <asp:Label runat="server" ID="lnkSubmittedCandidate" CommandName="Sort" CommandArgument="[JJDC].JoiningCount"
                                            Text="Submitted Candidate Name" ToolTip="Sort By Submitted Candidate Name" EnableViewState="false"></asp:Label>
                                    </th>
                                    <th runat="server" id="thOfferedCandidate" style="min-width: 110px;"  enableviewstate ="false" >
                                        <asp:Label runat="server" ID="lnkOfferedCandidate" CommandName="Sort" CommandArgument="[JJDC].JoiningCount"
                                            Text="Offered Candidate Name" ToolTip="Sort By Offered Candidate Name" EnableViewState="false"></asp:Label>
                                    </th>
                                    <th runat="server" id="thJoinedCandidate" style="min-width: 110px;"  enableviewstate ="false" >
                                        <asp:Label runat="server" ID="lnkJoinedCandidate" CommandName="Sort" CommandArgument="[JJDC].JoiningCount"
                                            Text="Joined Candidate Name" ToolTip="Sort By Joined Candidate Name" EnableViewState="false"></asp:Label>
                                    </th>
                                    
                                    <th runat="server" id="thJobDescription" style="min-width: 110px;"  enableviewstate ="false" >
                                        Job Description
                                    </th>
                                    <th runat="server" id="thNotes" style="min-width: 110px;"  enableviewstate ="false" >
                                        Notes
                                    </th>
                                    <th runat="server" id="thRequisitionType" style="min-width: 110px;"  enableviewstate ="false" >
                                        Requisition Type
                                    </th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td id="tdpager" runat="server" colspan="11">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td runat="server" id="tdPostdate">
                                    <asp:Label ID="lblPostdate" runat="server" Target="_blank" ></asp:Label>
                                </td>
                                <td runat="server" id="tdReqTitle">
                                    <asp:HyperLink ID="lnkJobTitle" runat="server" Target="_blank" ></asp:HyperLink>
                                </td>
                                <td runat="server" id="tdJobPostingCode">
                                    <asp:Label runat="server" ID="lblJobPostingCode" />
                                </td>
                                <td runat="server" id="tdTimeToFill">
                                    <asp:Label runat="server" ID="lblTimeToFill"  />
                                </td>
                                <td runat="server" id="tdClientJobId">
                                    <asp:Label runat="server" ID="lblClientJobId" />
                                </td>
                                <td runat="server" id="tdNoOfOpenings">
                                    <asp:Label runat="server" ID="lblNoOfOpenings"  />
                                </td>
                                <td runat="server" id="tdNoofUnfilledOpenings">
                                    <asp:Label runat="server" ID="lblNoofUnfilledOpenings"  />
                                </td>
                                <%--<td runat="server" id="tdPayRate">
                                    <asp:Label runat="server" ID="lblPayRate"  />
                                </td>--%>
                                 
                                <%--<td runat="server" id="tdJobLocation">
                                    <asp:Label runat="server" ID="lblJobLocation"  />
                                </td>--%>
                             
                              
                                <%--<td runat="server" id="tdSalesRegion">
                                    <asp:Label runat="server" ID="lblSalesRegion"  />
                                </td>
                                <td runat="server" id="tdSalesGroup">
                                    <asp:Label runat="server" ID="lblSalesGroup"  />
                                </td>
                                <td runat="server" id="tdPOAvailability">
                                    <asp:Label runat="server" ID="lblPOAvailability"  />
                                </td>--%>
                                <%--<td runat="server" id="tdCustomerName">
                                    <asp:Label runat="server" ID="lblCustomerName"  />
                                </td>--%>
                               
                                <%--<td runat="server" id="tdTravelRequired">
                                    <asp:Label runat="server" ID="lblTravelRequired"  />
                                </td>
                                <td runat="server" id="tdOtderBenefits">
                                    <asp:Label runat="server" ID="lblOtderBenefits"  />
                                </td>--%>
                                <td runat="server" id="tdReqStatus">
                                    <asp:Label runat="server" ID="lblReqStatus" />
                                </td>
                                <%--<td runat="server" id="tdJobDurationLookupId">
                                    <asp:Label runat="server" ID="lblJobDurationLookupId"  />
                                </td>
                                <td runat="server" id="tdJobDurationMontd">
                                    <asp:Label runat="server" ID="lblJobDurationMontd"  />
                                </td>--%>
                                <%--<td runat="server" id="tdJobAddress1">
                                    <asp:Label runat="server" ID="lblJobAddress1"  />
                                </td>
                                <td runat="server" id="tdJobAddress2">
                                    <asp:Label runat="server" ID="lblJobAddress2"  />
                                </td>--%>
                                <td runat="server" id="tdCity">
                                    <asp:Label runat="server" ID="lblCity"  />
                                </td>
                                <%--<td runat="server" id="tdStateId">
                                    <asp:Label runat="server" ID="lblStateId"  />
                                </td>
                                <td runat="server" id="tdZipCode">
                                    <asp:Label runat="server" ID="lblZipCode"  />
                                </td>--%>
                                <td runat="server" id="tdCountryId">
                                    <asp:Label runat="server" ID="lblCountryId"  />
                                </td>
                                <%--<td runat="server" id="tdReportingTo">
                                    <asp:Label runat="server" ID="lblReportingTo"  />
                                </td>
                                <td runat="server" id="tdNoOfReportees">
                                    <asp:Label runat="server" ID="lblNoOfReportees" />
                                </td>--%>
                                <td runat="server" id="tdStartDate">
                                    <asp:Label runat="server" ID="lblStartDate"  />
                                </td>
                                <td runat="server" id="tdFinalHiredDate">
                                    <asp:Label runat="server" ID="lblFinalHiredDate" />
                                </td>
                                <%--<td runat="server" id="tdAutdorizationTypeLookupId">
                                    <asp:Label runat="server" ID="lblAutdorizationTypeLookupId" />
                                </td>--%>
                                
                                <td runat="server" id="tdRequiredDegreeLookupId">
                                    <asp:Label runat="server" ID="lblRequiredDegreeLookupId"  />
                                </td>
                                <td runat="server" id="tdExpRequired">
                                    <asp:Label runat="server" ID="lblExpRequired"  />
                                </td>
                                <td runat="server" id="tdClientId">
                                    <asp:Label runat="server" ID="lblClientId"  />
                                </td>
                                <td runat="server" id="tdBUContact">
                                    <asp:Label runat="server" ID="lblBUContact" />
                                </td>
                                
                                <%--<td runat="server" id="tdTeleCommunication">
                                    <asp:Label runat="server" ID="lblTeleCommunication"  />
                                </td>--%>
                                <td runat="server" id="tdCreatorId">
                                    <asp:Label runat="server" ID="lblCreatorId"  />
                                </td>
                                <td runat="server" id="tdUpdateDate">
                                    <asp:Label runat="server" ID="lblUpdateDate"  />
                                </td>
                                <td runat="server" id="tdJobCategory">
                                    <asp:Label runat="server" ID="lblJobCategory"  />
                                </td>
                                <td runat="server" id="tdSkills">
                                    <asp:Label runat="server" ID="lblSkills"  />
                                </td>
                                <%--<td runat="server" id="tdMinimumQualifyingParameters">
                                    <asp:Label runat="server" ID="lblMinimumQualifyingParameters"  />
                                </td>
                                <td runat="server" id="tdWorkSchedule">
                                    <asp:Label runat="server" ID="lblWorkSchedule"  />
                                </td>
                                <td runat="server" id="tdSecurityClearance">
                                    <asp:Label runat="server" ID="lblSecurityClearance"  />
                                </td>
                                <td runat="server" id="tdExpenses">
                                    <asp:Label runat="server" ID="lblExpenses"  />
                                </td>--%>
                                <td runat="server" id="tdAdditionalNotes">
                                    <asp:Label runat="server" ID="lblAdditionalNotes"  />
                                </td>
                                <td runat="server" id="tdAssignedRecruiters">
                                    <asp:Label runat="server" ID="lblAssignedRecruiters"  />
                                </td>
                                <td runat="server" id="tdTurnArroundTime">
                                    <asp:Label runat="server" ID="lblTurnArroundTime" ></asp:Label>
                                </td>
                                <td runat="server" id="tdScheduledInterviews">
                                    <asp:Label runat="server" ID="lblScheduledInterviews" ></asp:Label>
                                </td>
                                <td runat="server" id="tdSubmissions">
                                    <asp:Label runat="server" ID="lblSubmissions" ></asp:Label>
                                </td>
                                <td runat="server" id="tdOffers">
                                    <asp:Label runat="server" ID="lblOffers" ></asp:Label>
                                </td>
                                <td runat="server" id="tdJoined">
                                    <asp:Label runat="server" ID="lblJoined" ></asp:Label>
                                </td>
                                
                                <td runat="server" id="tdInterviewCandidate">
                                    <asp:Label runat="server" ID="lblInterviewCandidate" ></asp:Label>
                                </td>
                                <td runat="server" id="tdSubmittedCandidate">
                                    <asp:Label runat="server" ID="lblSubmittedCandidate" ></asp:Label>
                                </td>
                                <td runat="server" id="tdOfferedCandidate">
                                    <asp:Label runat="server" ID="lblOfferedCandidate" ></asp:Label>
                                </td>
                                <td runat="server" id="tdJoinedCandidate">
                                    <asp:Label runat="server" ID="lblJoinedCandidate" ></asp:Label>
                                </td>
                                <td runat="server" id="tdDescription">
                                    <asp:Label runat="server" ID="lblDescription" ></asp:Label>
                                </td>
                                <td runat="server" id="tdNotes">
                                    <asp:Label runat="server" ID="lblNotes" ></asp:Label>
                                </td>
                                <td runat="server" id="tdRequisitionType">
                                    <asp:Label runat="server" ID="lblRequisitionType" ></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </asp:Panel>
          
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            <asp:PostBackTrigger ControlID="btnExportToWord" />
        </Triggers>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">


        function SelectUnselectheader(chkAllheader) {
            //debugger;
            var chkBoxAll = $get('<%= chkAll.ClientID%>');
            var chkBoxList = $get('<%= chkHiringList.ClientID%>');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            if (chkBoxAll.checked) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = true;
                }
            }
            if (!chkBoxAll.checked) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = false;
                }
            }
        }

        function SelectUnselectColumnOption(chkColumnOption) {
            var chkBoxAll = $get('<%= chkColumns.ClientID%>');
            var chkBoxList = $get('<%= chkColumnList.ClientID%>');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            if (chkBoxAll.checked) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = true;
                }
            }
            if (!chkBoxAll.checked) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = false;
                }
            }
        }

        function SelectUnselectAllMLevels(chkAllheader) {
            var checkedCount = 0;
            //debugger;
            var chkBoxAll = $get('<%= chkAll.ClientID%>');
            var chkBoxList = $get('<%= chkHiringList.ClientID%>');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            for (var i = 0; i < chkBoxCount.length; i++) {
                if (chkBoxCount[i].checked) {
                    checkedCount++;
                }
            }
            if (checkedCount == chkBoxCount.length) {
                chkBoxAll.checked = true;
            }
            else {
                chkBoxAll.checked = false;
            }
        }

        function SelectUnselectAllColumnOption(chkColumnOptionHeader) {
            var checkedCount = 0;
            //debugger;
            var chkBoxAll = $get('<%= chkColumns.ClientID%>');
            var chkBoxList = $get('<%= chkColumnList.ClientID%>');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            for (var i = 0; i < chkBoxCount.length; i++) {
                if (chkBoxCount[i].checked) {
                    checkedCount++;
                }
            }
            if (checkedCount == chkBoxCount.length) {
                chkBoxAll.checked = true;
            }
            else {
                chkBoxAll.checked = false;
            }
        }
        
    </script>

</asp:Content>
