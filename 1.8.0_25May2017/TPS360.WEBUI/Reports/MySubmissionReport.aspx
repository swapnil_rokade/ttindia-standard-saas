﻿

<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="SubmissionReport.aspx.cs"
    Inherits="TPS360.Web.UI.Reports.SubmissionReport" Title="My Submission Report" EnableEventValidation="false"
    EnableViewStateMac="false" %>
<%@ Register Src="~/Controls/SubmissionReport.ascx" TagName="SubmissionReport" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    My Submission Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">

 <ucl:SubmissionReport IsMySubmissionReport="true" ID="uncSubmissionReport"  runat="server" />   
</asp:Content>