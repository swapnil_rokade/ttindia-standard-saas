<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="SubmissionReport.aspx.cs"
    Inherits="TPS360.Web.UI.Reports.SubmissionReport" Title="Submission Report" EnableEventValidation="false"
    EnableViewStateMac="false" %>
<%@ Register Src="~/Controls/SubmissionReport.ascx" TagName="SubmissionReport" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Submission Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">

 <ucl:SubmissionReport ID="uncSubmissionReport" IsMySubmissionReport="false" IsTeamSubmissionReport="false" runat="server" />   
</asp:Content>
