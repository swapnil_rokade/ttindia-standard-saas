﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="HiringLogReports.aspx.cs"
    Inherits="TPS360.Web.UI.Reports.HiringLogReports" Title="Hiring Log Report" EnableEventValidation="false"
    EnableViewStateMac="false" %>
<%@ Register Src="~/Controls/HiringLogReports.ascx" TagName="HiringLogReport" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Hiring Log Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
  <ucl:HiringLogReport ID="ucnHiringLogReport" IsMyHiringLogReport="false" IsTeamHiringReport="false" runat="server" />  
</asp:Content>
