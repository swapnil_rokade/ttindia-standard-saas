﻿<%---------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateCDMDetailsReport.aspx
    Description: 
    Created By: pravin khot 
    Created On: 4/March/2016
    Modification Log: 
    --------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    --------------------------------------------------------------------------------------------------------------------------------------
  
    ------------------------------------------------------------------------------------------------------------------------------------------- 
--%>

<%@ Page Language="C#" MasterPageFile="~/Default.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeFile="CandidateCDMDetailsReport.aspx.cs" Inherits="TPS360.Web.UI.Reports.CandidateCDMDetailsReport"
    Title="Candidate Joining Details" EnableViewStateMac="false" %>

<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MultipleItemPicker.ascx" TagName="MultipleSelection"
    TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Candidate Joining Details
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <style>
        </style>

    <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

    <script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

    <script language="javascript" type="text/javascript">
        //To Keep the scroll bar in the Exact Place
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
        function PageLoadedReq(sender, args) {
            try {
                var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
                var bigDiv = document.getElementById('bigDiv');
                bigDiv.scrollLeft = hdnScroll.value;
            }
            catch (e) {
            }
        }
        function SetScrollPosition() {
            var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
            var bigDiv = document.getElementById('bigDiv');
            hdnScroll.value = bigDiv.scrollLeft;
        }

        

        
    </script>

    <style>
        .TableFormLeble
        {
            width: 25%;
        }
    </style>
    <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
    <asp:UpdatePanel ID="pnlCandidateSourcingReport" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnScrollPos" runat="server" />
            <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
            <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
            <asp:HiddenField ID="hdnRequisitionSelected" runat="server" Value="0" />
            <asp:ObjectDataSource ID="odsCandidateList" runat="server" SelectMethod="GetPagedCandidateCDMDetailsRpt"
                TypeName="TPS360.Web.UI.CandidateDataSource" SelectCountMethod="GetListCountCandidateCDMDetailsRpt"
                EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:ControlParameter ControlID="wdcSourcedDate" Name="SourcedDateFrom" PropertyName="StartDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="wdcSourcedDate" Name="SourcedDateTo" PropertyName="EndDate"
                        Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Panel ID="pnlSearchRegion" runat="server" DefaultButton="btnSearch">
                <div style="width: 100%;">
                    <div>
                        <div class="TableRow">
                            <div class="TabPanelHeader nomargin" style="border-style: none none none;">
                                Filter Options</div>
                        </div>
                        <div class="TableRow" style="text-align: center">
                            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <asp:Panel ID="pnlSearchContent" runat="server" Visible="true" >                          
                            <div class="FormLeftColumn" style="width: 49%">
                                <div class="TableRow" style="white-space: nowrap">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblSourcedDate" runat="server" Text="Joining Date"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <ucl:DateRangePicker ID="wdcSourcedDate" runat="server" />
                                    </div>
                                </div>                             
                              
                            </div>
                            <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px;">
                                <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search"
                                    TabIndex="12" CssClass="btn btn-primary" EnableViewState="false" ValidationGroup="Search"
                                    OnClick="btnSearch_Click"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                                <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                                    EnableViewState="false" CausesValidation="false" TabIndex="13" OnClick="btnClear_Click" />
                            </div>
                        </asp:Panel>
                        <div class="TableRow" style="padding-bottom: 5px;" id="divExportButton" runat="server"
                            visible="false">
                            <div class="TableRow">
                                <div class="TabPanelHeader nomargin" style="margin-bottom: 5px; border-style: none none none;">
                                    Report Results</div>
                            </div>
                            <asp:ImageButton ID="btnExportToExcel" CssClass="btn" TabIndex="14" SkinID="sknExportToExcel"
                                runat="server" ToolTip="Export To Excel" OnClick="btnExportToExcel_Click" />
                           <%-- <asp:ImageButton ID="btnExportToPDF" CssClass="btn" TabIndex="15" SkinID="sknExportToPDF"
                                runat="server" ToolTip="Export To PDF" OnClick="btnExportToPDF_Click" />
                            <asp:ImageButton ID="btnExportToWord" CssClass="btn" TabIndex="16" SkinID="sknExportToWord"
                                runat="server" ToolTip="Export To Word" OnClick="btnExportToWord_Click" />--%>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div id="divCandidateSourcingReport" runat="server">
                <asp:Panel ID="pnlCandidateSourcingBody" runat="server">
                    <div class="GridContainer" style="overflow: auto; overflow-y: hidden; width: 100%;
                        text-align: left;" id="bigDiv" onscroll='SetScrollPosition()'>
                        <asp:ListView ID="lsvCandidateSourcingRpt" runat="server" DataKeyNames="Id" OnItemDataBound="lsvCandidateSourcingRpt_ItemDataBound"
                            OnItemCommand="lsvCandidateSourcingRpt_ItemCommand" OnPreRender="lsvCandidateSourcingRpt_PreRender">
                            <LayoutTemplate>
                                <table id="tlbTemplate" class="Grid ReportGrid" cellspacing="0" border="0">
                                    <tr id="trHeader" runat="server">
                                    
                                        <th id="thId" runat="server">
                                            <asp:LinkButton ID="lnkCandidateID" Style="min-width: 120px" runat="server" TabIndex="17"
                                                ToolTip="Sort By Candidate ID #" CommandName="Sort" CommandArgument="[C].[ID]"
                                                Text="Candidate ID #" />
                                        </th>
                                          <th runat="server" id="thtitle" style="min-width: 100px">
                                            <asp:Label ID="btntitle" runat="server" Text="Title" /> 
                                        </th>
                                        <th runat="server" id="thFName" style="min-width: 100px">
                                            <asp:LinkButton ID="lnkBtnFName" runat="server" TabIndex="17" ToolTip="Sort By First Name"
                                                CommandName="Sort" CommandArgument="[C].[FirstName]" Text="First Name" />
                                        </th>
                                        <th runat="server" id="thMName" style="min-width: 100px">
                                            <asp:LinkButton ID="lnkBtnMName" runat="server" TabIndex="17" ToolTip="Sort By Middle Name"
                                                CommandName="Sort" CommandArgument="[C].[MiddleName]" Text="Middle Name" />
                                        </th>
                                        <th runat="server" id="thLName" style="min-width: 100px">
                                            <asp:LinkButton ID="lnkBtnLName" runat="server" TabIndex="17" ToolTip="Sort By Last Name"
                                                CommandName="Sort" CommandArgument="[C].[LastName]" Text="Last Name" />
                                        </th>
                                        <th runat="server" id="themail" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnemail" runat="server" TabIndex="18" ToolTip="Sort By email"
                                                CommandName="Sort" CommandArgument="[C].[PrimaryEmail]" Text="Primary Email" />
                                        </th>
                                        <th runat="server" id="thfathername" style="min-width: 100px" enableviewstate="false">
                                            <asp:LinkButton runat="server" ID="btnfathername" CommandName="Sort" CommandArgument="[C].[MiddleName]"
                                                Text="Father Name" ToolTip="Sort By father name" EnableViewState="false" />
                                        </th>
                                        <th runat="server" id="thdateofbirth" style="min-width: 100px">
                                            <asp:LinkButton ID="lnkBtndateofbirth" runat="server" ToolTip="Sort By Date of Birth"
                                                CommandName="Sort" CommandArgument="[C].[DateOfBirth]" Text="Date of Birth" />
                                        </th>
                                        <th runat="server" id="thgender" style="min-width: 100px">
                                            <asp:LinkButton ID="lnkBtngender" runat="server" TabIndex="20" ToolTip="Sort By Gender"
                                                CommandName="Sort" CommandArgument="[C].[Status]" Text="Gender" />
                                        </th>
                                        <th runat="server" id="thdeptprocess" style="min-width: 130px">
                                            <asp:LinkButton ID="lnkBtndeptprocess" runat="server" ToolTip="Sort By Department Process" CommandName="Sort"
                                                CommandArgument="[C].[Location]" Text="Department Process" />
                                        </th>
                                        <th runat="server" id="thdesignation" style="min-width: 100px">
                                            <asp:Label ID="lnkBtndesignation" runat="server" ToolTip="Sort By Designation"
                                                CommandName="Sort" CommandArgument="[ME].[CurrentPosition]" Text="Designation" />
                                        </th>
                                        <th runat="server" id="thperaddr1" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnperaddr1" runat="server" TabIndex="19" ToolTip="Sort By Permanent Address Line 1"
                                                CommandName="Sort" CommandArgument="[C].[PermanentAddressLine1]" Text="Permanent Address Line 1" />
                                        </th>
                                        <th runat="server" id="thperaddr2" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnperaddr2" runat="server" ToolTip="Sort By Permanent Address Line 2"
                                                CommandName="Sort" CommandArgument="[C].[PermanentAddressLine2]" Text="Permanent Address Line 2" />
                                        </th>
                                        <th runat="server" id="thperlocation" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnperlocation" runat="server" ToolTip="Sort By Permanent Location"
                                                CommandName="Sort" CommandArgument="[C].[PermanentCity]" Text="Permanent Location" />
                                        </th>
                                        <th runat="server" id="thstatename" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnstatename" runat="server" TabIndex="21" ToolTip="Sort By Permanent State Name"
                                                CommandName="Sort" CommandArgument="[S].[Name]" Text="Permanent State Name" />
                                        </th>
                                        <th runat="server" id="thperpin" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnperpin" runat="server" ToolTip="Sort By Permanent PIN"
                                                CommandName="Sort" CommandArgument="[C].[PermanentZip]" Text="Permanent PIN" />
                                        </th>
                                        <th runat="server" id="thmobile" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBtnmobile" runat="server" ToolTip="Sort By mobile" CommandName="Sort"
                                                CommandArgument="[C].[CellPhone]" Text="mobile" />
                                        </th>
                                        <th runat="server" id="thcuraddr1" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkcuraddr1" runat="server" ToolTip="Sort By Current Addres sLine 1" CommandName="Sort"
                                                CommandArgument="[C].[CurrentAddressLine1]" Text="Current Address Line 1" />
                                        </th>
                                        <th runat="server" id="thcuraddr2" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkcuraddr2" runat="server" ToolTip="Sort By Current Address Line 2" CommandName="Sort"
                                                CommandArgument="[C].[CurrentAddressLine2]" Text="Current Address Line 2" />
                                        </th>
                                        
                                          <th runat="server" id="thcurlocation" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkcurlocation" runat="server" ToolTip="Sort By Current Location"
                                                CommandName="Sort" CommandArgument="[C].[CurrentCity]" Text="Current Location" />
                                        </th>
                                        <th runat="server" id="thcurstate" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkcurstate" runat="server" ToolTip="Sort By Current State" CommandName="Sort"
                                                CommandArgument="[SCU].[Name]" Text="Current State" />
                                        </th>
                                        <th runat="server" id="thcurpin" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkcurpin" runat="server" ToolTip="Sort By Current PIN" CommandName="Sort"
                                                CommandArgument="[C].[CurrentZip]" Text="Current PIN" />
                                        </th>
                                        <th runat="server" id="thband" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkband" runat="server" ToolTip="Sort By Band" CommandName="Sort"
                                                CommandArgument="[C].[CurrentAddressLine2]" Text="Band" />
                                        </th>
                                        
                                         <th runat="server" id="thstructure" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkstructure" runat="server" ToolTip="Sort By Structure" CommandName="Sort"
                                                CommandArgument="[SCU].[Name]" Text="Structure" />
                                        </th>
                                        <th runat="server" id="thInputComponent" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkInputComponent" runat="server" ToolTip="Sort By Input Component" CommandName="Sort"
                                                CommandArgument="[C].[CurrentZip]" Text="Input Component" />
                                        </th>
                                        <th runat="server" id="thExpectedDOJ" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkExpectedDOJ" runat="server" ToolTip="Sort By Expected DOJ" CommandName="Sort"
                                                CommandArgument="[C].[CurrentAddressLine2]" Text="Expected DOJ" />
                                        </th>
                                         <th runat="server" id="thBondName" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBondName" runat="server" ToolTip="Sort By Bond Name" CommandName="Sort"
                                                CommandArgument="[SCU].[Name]" Text="Bond Name" />
                                        </th>
                                        <th runat="server" id="thBondamount" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBondamount" runat="server" ToolTip="Sort By Bond Amount" CommandName="Sort"
                                                CommandArgument="[C].[CurrentZip]" Text="Bond Amount" />
                                        </th>
                                        <th runat="server" id="thBondfromdate" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBondfromdate" runat="server" ToolTip="Sort By Bond From Date" CommandName="Sort"
                                                CommandArgument="[C].[CurrentAddressLine2]" Text="Bond From Date" />
                                        </th>
                                        
                                         <th runat="server" id="thBondenddate" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBondenddate" runat="server" ToolTip="Sort By Bond End Date" CommandName="Sort"
                                                CommandArgument="[C].[CurrentZip]" Text="Bond End Date" />
                                        </th>
                                        <th runat="server" id="thMarkLetterTo" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkMarkLetterTo" runat="server" ToolTip="Sort By Mark Letter To" CommandName="Sort"
                                                CommandArgument="[C].[CurrentAddressLine2]" Text="Mark Letter To" />
                                        </th>
                                         <th runat="server" id="thHiringSourceName" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkHiringSourceName" runat="server" ToolTip="Sort By Hiring Source Name" CommandName="Sort"
                                                CommandArgument="[SCU].[Name]" Text="Hiring Source Name" />
                                        </th>
                                        <th runat="server" id="thHiringCost" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkHiringCost" runat="server" ToolTip="Sort By Hiring Cost" CommandName="Sort"
                                                CommandArgument="[C].[CurrentZip]" Text="Hiring Cost" />
                                        </th>
                                        <th runat="server" id="thHiringcomment" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkHiringcomment" runat="server" ToolTip="Sort By Hiring Comment" CommandName="Sort"
                                                CommandArgument="[C].[CurrentAddressLine2]" Text="Hiring Comment" />
                                        </th>
                                        
                                          <th runat="server" id="thCity" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkCity" runat="server" ToolTip="Sort By City" CommandName="Sort"
                                                CommandArgument="[C].[CurrentCity]" Text="City" />
                                        </th>
                                         <th runat="server" id="thBranch" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBranch" runat="server" ToolTip="Sort By Branch" CommandName="Sort"
                                                CommandArgument="[SCU].[Name]" Text="Branch" />
                                        </th>
                                        <th runat="server" id="thMaritalStatus" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkMaritalStatus" runat="server" ToolTip="Sort By Marital Status" CommandName="Sort"
                                                CommandArgument="[GMS].[Name]" Text="Marital Status" />
                                        </th>
                                        <th runat="server" id="thRequisitionDate" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkRequisitionDate" runat="server" ToolTip="Sort By Requisition Date" CommandName="Sort"
                                                CommandArgument="[C].[CurrentAddressLine2]" Text="Requisition Date" />
                                        </th>
                                        
                                         <th runat="server" id="thWeddingAnniversary" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkWeddingAnniversary" runat="server" ToolTip="Sort By Wedding Anniversary" CommandName="Sort"
                                                CommandArgument="[C].[CurrentAddressLine2]" Text="Wedding Anniversary" />
                                        </th>
                                         <th runat="server" id="thproject" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkproject" runat="server" ToolTip="Sort By Project" CommandName="Sort"
                                                CommandArgument="[SCU].[Name]" Text="Project" />
                                        </th>
                                        <th runat="server" id="thCareerLevel" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkCareerLevel" runat="server" ToolTip="Sort By Career Level" CommandName="Sort"
                                                CommandArgument="[C].[CurrentZip]" Text="Career Level" />
                                        </th>
                                        <th runat="server" id="thBenefitBand" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkBenefitBand" runat="server" ToolTip="Sort By Benefit Band" CommandName="Sort"
                                                CommandArgument="[C].[CurrentAddressLine2]" Text="Benefit Band" />
                                        </th>
                                        
                                         <th runat="server" id="thSalaryGrade" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkSalaryGrade" runat="server" ToolTip="Sort By Salary Grade" CommandName="Sort"
                                                CommandArgument="[C].[CurrentAddressLine2]" Text="Salary Grade" />
                                        </th>
                                         <th runat="server" id="thJobRole" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkJobRole" runat="server" ToolTip="Sort By Job Role" CommandName="Sort"
                                                CommandArgument="[SCU].[Name]" Text="Job Role" />
                                        </th>
                                        <th runat="server" id="thJobCode" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkJobCode" runat="server" ToolTip="Sort By Job Code" CommandName="Sort"
                                                CommandArgument="[C].[CurrentZip]" Text="Job Code" />
                                        </th>
                                        <th runat="server" id="thJoiningDate" style="min-width: 80px">
                                            <asp:LinkButton ID="lnkJoiningDate" runat="server" ToolTip="Sort By Joining Date" CommandName="Sort"
                                                CommandArgument="[G8].[JoiningDate]" Text="Joining Date" />
                                        </th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                    <tr class="Pager">
                                        <td id="tdpager" runat="server" colspan="46">
                                            <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                        </td>
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <EmptyDataTemplate>
                                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                    <tr>
                                        <td>
                                            No data was returned.
                                        </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                    <td id="tdId" runat="server">
                                        <asp:Label ID="lblCandidateID" runat="server"></asp:Label>
                                    </td>
                                                                        
                                     <td runat="server" id="tdtitle" style="text-align: left;">
                                        <asp:Label ID="lbltitle" runat="server" />
                                    </td>
                                    
                                    <td runat="server" id="tdFName" style="text-align: left;">
                                        <asp:Label ID="lblCandidateFName" runat="server" />
                                    </td>
                                    
                                     <td runat="server" id="tdMName" style="text-align: left;">
                                        <asp:Label ID="lblCandidateMName" runat="server" />
                                    </td>
                                    
                                     <td runat="server" id="tdLName" style="text-align: left;">
                                        <asp:Label ID="lblCandidateLName" runat="server" />
                                    </td>
                                    
                                    <td runat="server" id="tdemail">
                                        <asp:Label ID="lblemail" runat="server" />
                                    </td>
                                    <td runat="server" id="tdfathername">
                                        <asp:Label runat="server" ID="lblfathername" />
                                    </td>
                                    <td runat="server" id="tddateofbirth">
                                        <asp:Label ID="lbldateofbirth" runat="server" />
                                    </td>
                                    <td runat="server" id="tdgender">
                                        <asp:Label ID="lblgender" runat="server" />
                                    </td>
                                    <td runat="server" id="tddeptprocess">
                                        <asp:Label ID="lbldeptprocess" runat="server" />
                                    </td>
                                    <td runat="server" id="tddesignation">
                                        <asp:Label ID="lbldesignation" runat="server" />
                                    </td>
                                    <td runat="server" id="tdperaddr1">
                                        <asp:Label ID="lblperaddr1" runat="server" />
                                    </td>
                                    <td runat="server" id="tdperaddr2">
                                        <asp:Label ID="lblperaddr2" runat="server" />
                                    </td>
                                    <td runat="server" id="tdperlocation">
                                        <asp:Label ID="lblperlocation" runat="server" />
                                    </td>
                                    <td runat="server" id="tdstatename">
                                        <asp:Label ID="lblstatename" runat="server" />
                                    </td>
                                    <td runat="server" id="tdperpin">
                                        <asp:Label ID="lblperpin" runat="server" />
                                    </td>
                                    <td runat="server" id="tdmobile">
                                        <asp:Label ID="lblmobile" runat="server" />
                                    </td>
                                    <td runat="server" id="tdcuraddr1">
                                        <asp:Label ID="lblcuraddr1" runat="server" />
                                    </td>
                                    <td runat="server" id="tdcuraddr2">
                                        <asp:Label ID="lblcuraddr2" runat="server" />
                                    </td>
                                    
                                     <td runat="server" id="tdcurlocation">
                                        <asp:Label ID="lblcurlocation" runat="server" />
                                    </td>
                                    <td runat="server" id="tdcurstate">
                                        <asp:Label ID="lblcurstate" runat="server" />
                                    </td>
                                    <td runat="server" id="tdcurpin">
                                        <asp:Label ID="lblcurpin" runat="server" />
                                    </td>
                                    <td runat="server" id="tdband">
                                        <asp:Label ID="lblband" runat="server" />
                                    </td>
                                    
                                    
                                    
                                     <td runat="server" id="tdstructure">
                                        <asp:Label ID="lblstructure" runat="server" />
                                    </td>
                                    <td runat="server" id="tdInputComponent">
                                        <asp:Label ID="lblInputComponent" runat="server" />
                                    </td>
                                    <td runat="server" id="tdExpectedDOJ">
                                        <asp:Label ID="lblExpectedDOJ" runat="server" />
                                    </td>
                                    <td runat="server" id="tdBondName">
                                        <asp:Label ID="lblBondName" runat="server" />
                                    </td>
                                    <td runat="server" id="tdBondamount">
                                        <asp:Label ID="lblBondamount" runat="server" />
                                    </td>                                    
                                     <td runat="server" id="tdBondfromdate">
                                        <asp:Label ID="lblBondfromdate" runat="server" />
                                    </td>
                                    <td runat="server" id="tdBondenddate">
                                        <asp:Label ID="lblBondenddate" runat="server" />
                                    </td>
                                    <td runat="server" id="tdMarkLetterTo">
                                        <asp:Label ID="lblMarkLetterTo" runat="server" />
                                    </td>
                                    <td runat="server" id="tdHiringSourceName">
                                        <asp:Label ID="lblHiringSourceName" runat="server" />
                                    </td>
                                    
                                      <td runat="server" id="tdHiringCost">
                                       <asp:Label ID="lblHiringCost" runat="server" />
                                    </td>
                                    <td runat="server" id="tdHiringcomment">
                                        <asp:Label ID="lblHiringcomment" runat="server" />
                                    </td>
                                    <td runat="server" id="tdCity">
                                        <asp:Label ID="lblCity" runat="server" />
                                    </td>
                                    <td runat="server" id="tdBranch">
                                        <asp:Label ID="lblBranch" runat="server" />
                                    </td>
                                    
                                       <td runat="server" id="tdMaritalStatus">
                                       <asp:Label ID="lblMaritalStatus" runat="server" />
                                    </td>
                                    <td runat="server" id="tdRequisitionDate">
                                        <asp:Label ID="lblRequisitionDate" runat="server" />
                                    </td>
                                    <td runat="server" id="tdWeddingAnniversary">
                                        <asp:Label ID="lblWeddingAnniversary" runat="server" />
                                    </td>
                                    <td runat="server" id="tdproject">
                                        <asp:Label ID="lblproject" runat="server" />
                                    </td>
                                    
                                     <td runat="server" id="tdCareerLevel">
                                       <asp:Label ID="lblCareerLevel" runat="server" />
                                    </td>
                                    <td runat="server" id="tdBenefitBand">
                                        <asp:Label ID="lblBenefitBand" runat="server" />
                                    </td>
                                    <td runat="server" id="tdSalaryGrade">
                                        <asp:Label ID="lblSalaryGrade" runat="server" />
                                    </td>
                                    <td runat="server" id="tdJobRole">
                                        <asp:Label ID="lblJobRole" runat="server" />
                                    </td>
                                      <td runat="server" id="tdJobCode">
                                        <asp:Label ID="lblJobCode" runat="server" />
                                    </td>
                                    <td runat="server" id="tdJoiningDate">
                                        <asp:Label ID="lblJoiningDate" runat="server" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </asp:Panel>
            </div>
            <asp:Panel ID="pnlmodal" runat="server" Style="display: none">
                <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                    <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="Modal" runat="server" TargetControlID="pnlmodal"
                PopupControlID="pnlmodal" BackgroundCssClass="divModalBackground">
            </ajaxToolkit:ModalPopupExtender>
        </ContentTemplate>
        <Triggers>
           
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
