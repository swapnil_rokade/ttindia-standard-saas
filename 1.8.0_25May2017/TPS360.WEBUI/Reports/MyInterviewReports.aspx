﻿

<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="MyInterviewReports.aspx.cs"
    Inherits="TPS360.Web.UI.Reports.MyCandidateReports" Title="My Interview Report" EnableEventValidation="false"
    EnableViewStateMac="false" %>
 
<%@ Register Src="~/Controls/InterviewReports.ascx" TagName="InterviewReport" TagPrefix="ucl" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    My Interview Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">

  <ucl:InterviewReport ID="uncInterviewReport" runat="server" IsTeamReport="false" IsMyInterviewReport="true" />  
</asp:Content>