﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:MyRequisitionReports.ascx.cs
    Description :This is user control common for MyRequisitionReports.aspx 
    Created By: Sumit Sonawane
    Created On: 25/May/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               11/Nov/2016      Sumit Sonawane          Modified My Requisition Report to correct data.
-------------------------------------------------------------------------------------------------------------------------------------------       

*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using TPS360.Common.Helper;
using System.Configuration;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;
using System.Web.Security;
using System.Xml;
using System.Linq;
namespace TPS360.Web.UI.Reports
{
    public partial class MyRequisitionReports : BasePage
    {
        #region Members
        string clientRate;
        public int HeaderAdd = 0;
        #endregion

        private string AssociateRequisitionsWithEntitiesFromModule
        {
            get
            {
                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                {
                    return "Department";
                }
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                {
                    return "Account";
                }
                else
                {
                    return "Vendor";
                }
            }

        }
        #region Methods
        private int NumofColumnSelected()
        {

            return 0; //gpk0123456789 Code Introduced by Prasanth on 22/May/2015
            //return chkColumnList.Items.Cast<ListItem>().Where(x => x.Selected).Count();
            //if (chkColumnList.Items.FindByValue("TurnArroundTime").Selected)
            //{
            //    numofcolSelected++;
            //}
            //if (chkColumnList.Items.FindByValue("JobTitle").Selected)
            //{
            //    numofcolSelected++;
            //}

            //if (chkColumnList.Items.FindByValue("JobPostingCode").Selected)
            //{
            //    numofcolSelected++;
            //}

            //if (chkColumnList.Items.FindByValue("TimeToFill").Selected)
            //{
            //    numofcolSelected++;
            //}

            //if (chkColumnList.Items.FindByValue("ClientJobId").Selected)
            //{
            //    numofcolSelected++;
            //}

            //if (chkColumnList.Items.FindByValue("NoOfOpenings").Selected)
            //{
            //    numofcolSelected++;

            //}

            //if (chkColumnList.Items.FindByValue("PayRate").Selected)
            //{
            //    numofcolSelected++;
            //}



            ////if (chkColumnList.Items.FindByValue("TravelRequired").Selected)
            ////{
            ////    numofcolSelected++;
            ////}


            ////if (chkColumnList.Items.FindByValue("OtherBenefits").Selected)
            ////{
            ////    numofcolSelected++;
            ////}

            //if (chkColumnList.Items.FindByValue("JobStatus").Selected)
            //{
            //    numofcolSelected++;
            //}

            ////Adding Hiring Matrix Levels
            //IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
            //if (hiringMatrixLevels != null)
            //{
            //    if (hiringMatrixLevels.Count > 0)
            //    {
            //        foreach (HiringMatrixLevels levels in hiringMatrixLevels)
            //            if (checkCheckBoxHiringLevelSelected(levels.Id))
            //                numofcolSelected++;
            //    }

            //}


            ////enhancement 0.8 ends
            ////if (chkColumnList.Items.FindByValue("JobDurationLookupId").Selected)
            ////{
            ////    numofcolSelected++;
            ////}

            ////if (chkColumnList.Items.FindByValue("JobDurationMonth").Selected)
            ////{
            ////    numofcolSelected++;
            ////}

            ////if (chkColumnList.Items.FindByValue("JobAddress1").Selected)
            ////{
            ////    numofcolSelected++;
            ////}

            ////if (chkColumnList.Items.FindByValue("JobAddress2").Selected)
            ////{
            ////    numofcolSelected++;
            ////}

            //if (chkColumnList.Items.FindByValue("City").Selected)
            //{
            //    numofcolSelected++;
            //}

            ////if (chkColumnList.Items.FindByValue("StateId").Selected)
            ////{
            ////    numofcolSelected++;
            ////}

            ////if (chkColumnList.Items.FindByValue("ZipCode").Selected)
            ////{
            ////    numofcolSelected++;
            ////}

            ////if (chkColumnList.Items.FindByValue("CountryId").Selected)
            ////{
            ////    numofcolSelected++;
            ////}

            //if (chkColumnList.Items.FindByValue("StartDate").Selected)
            //{
            //    numofcolSelected++;
            //}

            //if (chkColumnList.Items.FindByValue("FinalHiredDate").Selected)
            //{
            //    numofcolSelected++;
            //}

            //if (System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern == "dd/MM/yyyy")
            //{
            //    numofcolSelected++;
            //}

            //if (chkColumnList.Items.FindByValue("PostedDate").Selected)
            //{
            //    numofcolSelected++;
            //}


            //if (chkColumnList.Items.FindByValue("RequiredDegreeLookupId").Selected)
            //{
            //    numofcolSelected++;
            //}

            //if (chkColumnList.Items.FindByValue("ExpRequired").Selected)
            //{
            //    numofcolSelected++;
            //}


            //if (chkColumnList.Items.FindByValue("ClientId").Selected)
            //{
            //    numofcolSelected++;
            //}

            //if (chkColumnList.Items.FindByValue("ClientHourlyRate") != null)
            //{
            //    if ((chkColumnList.Items.FindByValue("ClientHourlyRate").Selected))
            //    {
            //        numofcolSelected++;
            //    }
            //}

            ////if (chkColumnList.Items.FindByValue("TeleCommunication").Selected)
            ////{
            ////    numofcolSelected++;
            ////}
            //if (chkColumnList.Items.FindByValue("CreatorId").Selected)
            //{
            //    numofcolSelected++;
            //}
            //if (chkColumnList.Items.FindByValue("UpdateDate").Selected)
            //{
            //    numofcolSelected++;
            //}

            //if (chkColumnList.Items.FindByValue("JobCategory").Selected)
            //{
            //    numofcolSelected++;
            //}
            //if (chkColumnList.Items.FindByValue("Skills").Selected)
            //{
            //    numofcolSelected++;
            //}
            ////if (chkColumnList.Items.FindByValue("WorkSchedule").Selected)
            ////{
            ////    numofcolSelected++;
            ////}
            ////if (chkColumnList.Items.FindByValue("SecurityClearance").Selected)
            ////{
            ////    numofcolSelected++;
            ////}
            ////if (chkColumnList.Items.FindByValue("Expenses").Selected)
            ////{
            ////    numofcolSelected++;
            ////}
            //if (chkColumnList.Items.FindByValue("AdditionalNotes").Selected)
            //{
            //    numofcolSelected++;
            //}
            //if (chkColumnList.Items.FindByValue("AssignedRecruiters").Selected)
            //{
            //    numofcolSelected++;
            //}
            //return numofcolSelected;
        }

        private void GenerateRequisitionReport(string format)
        {
            if (string.Equals(format, "word"))
            {

                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=RequisitionReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";

                Response.Output.Write(GetRequisitionReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "excel"))
            {
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=RequisitionReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetRequisitionReportTable());
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A3;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;
                int numofcolSelected = NumofColumnSelected();
                if (numofcolSelected > 0 && numofcolSelected > 7)
                {
                    pdfConverter.PageWidth += (110 * (numofcolSelected - 7));
                }

                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlStringWithTempFile(GetRequisitionReportTable(), "");
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=RequisitionReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }
        private StringBuilder getReportHeader()
        {
            StringBuilder header = new StringBuilder();

            foreach (ListItem item in chkColumnList.Items.Cast<ListItem>().Where(x => x.Selected == true))
                if (item.Selected) header.Append("<th>" + (item.Text.Contains("Turn Around Time") ? "Turn Around Time" : item.Text) + "</th>");

            foreach (ListItem item in chkHiringList.Items.Cast<ListItem>().Where(x => x.Selected == true))
                if (item.Selected) header.Append("<th>" + item.Text + "</th>");

            return header;
        }
        private string GetRequisitionReportTable()
        {
            bool InternalHiring = false;
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                InternalHiring = true;
            }
            StringBuilder reqReport = new StringBuilder();
            JobPostingDataSource jobPostingDataSource = new JobPostingDataSource();
            IList<JobPosting> jobPostingList = jobPostingDataSource.GetPaged(false, ddlJobStatus.SelectedValue, string.Empty, dtPicker.StartDate.ToString(),
                dtPicker.EndDate.ToString(), ddlReqCreator.SelectedValue, ddlEmployee.SelectedValue, MiscUtil.RemoveScript(txtCity.Text), 
                uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(),ddlEndClient.SelectedValue, 
                hdnRequisitionSelected.Value, null, -1, -1);

         
            if (jobPostingList != null)
            {
                string WebRoot = ConfigurationManager.AppSettings["WebRoot"].ToString();
                string LogoPath = UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png";
                reqReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");
                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + LogoPath + "' style= style='height:56px;width:56px'/></td>"); //0.7
                reqReport.Append("    </tr>");

                reqReport.Append(" <tr>");
                reqReport.Append(getReportHeader().ToString());
                reqReport.Append(" </tr>");

                foreach (JobPosting jobPosting in jobPostingList)
                {
                    if (jobPosting != null)
                    {
                        reqReport.Append(" <tr>");

                        foreach (ListItem item in chkColumnList.Items)
                        {
                            if (item.Selected)
                            {
                                switch (item.Value)
                                {
                                    //case "PostedDate":
                                    //    reqReport.Append("     <td>" + (MiscUtil.IsValidDate(jobPosting.PostedDate) ? jobPosting.PostedDate.ToShortDateString() : string.Empty).ToString().Trim() + "</td>");
                                    //    break;
                                    case "JobTitle":
                                        reqReport.Append("     <td>" + jobPosting.JobTitle.ToString().Trim() + "</td>");
                                        break;
                                    case "JobPostingCode":
                                        reqReport.Append("     <td>" + jobPosting.JobPostingCode.ToString().Trim() + "</td>");
                                        break;
                                    case "TimeToFill":
                                        reqReport.Append("     <td>" + TimeToFill(jobPosting).ToString().Trim() + "</td>");
                                        break;
                                    case "ClientJobId":
                                        reqReport.Append("     <td>" + jobPosting.ClientJobId.ToString().Trim() + "</td>");
                                        break;
                                    case "NoOfOpenings":
                                        reqReport.Append("     <td>" + jobPosting.NoOfOpenings.ToString().Trim() + "</td>");
                                        break;
                                    case "NoofUnfilledOpenings":
                                        reqReport.Append("     <td>" + jobPosting.NoOfUnfilledOpenings.ToString().Trim() + "</td>");
                                        break;
                                    case "PayRate":
                                        reqReport.Append("     <td>" + PayRate(jobPosting).ToString().Trim() + "</td>");
                                        break;
                                    case "JobStatus":
                                        GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(jobPosting.JobStatus);
                                        if (_RequisitionStatusLookup != null)
                                        {
                                            reqReport.Append("     <td>" + _RequisitionStatusLookup.Name.ToString().Trim() + "&nbsp;</td>");
                                        }
                                        break;
                                    case "City":
                                        reqReport.Append("     <td>" + jobPosting.City.ToString().Trim() + "</td>");
                                        break;
                                    case "CountryId":
                                        reqReport.Append("     <td>" + MiscUtil.GetCountryNameById(jobPosting.CountryId, Facade).ToString().Trim() + "</td>");
                                        break;
                                    case "StartDate":
                                        reqReport.Append("     <td>" + StartDate(jobPosting).ToString().Trim() + "</td>");
                                        break;
                                    case "FinalHiredDate":
                                        reqReport.Append("     <td>" + (MiscUtil.IsValidDate(jobPosting.FinalHiredDate) ? jobPosting.FinalHiredDate.ToShortDateString() : string.Empty).ToString().Trim() + "</td>");
                                        break;
                                    case "PostedDate":
                                        reqReport.Append("     <td>" + (MiscUtil.IsValidDate(jobPosting.PostedDate) ? jobPosting.PostedDate.ToShortDateString() : string.Empty).ToString().Trim() + "</td>");
                                        break;
                                    case "RequiredDegreeLookupId":
                                        reqReport.Append("     <td>" + MiscUtil.SplitValues(jobPosting.RequiredDegreeLookupId, ',', Facade) + "</td>");
                                        break;
                                    case "ExpRequired":
                                        if (jobPosting.MaxExpRequired.Length > 0 && jobPosting.MinExpRequired.Length > 0)
                                            reqReport.Append("     <td>" + jobPosting.MinExpRequired.ToString().Trim() + " - " + jobPosting.MaxExpRequired.ToString().Trim() + " years" + "</td>");
                                        else if (jobPosting.MinExpRequired.Length > 0)
                                            reqReport.Append("     <td>" + jobPosting.MinExpRequired.ToString().Trim() + " years" + "</td>");
                                        else if (jobPosting.MaxExpRequired.Length > 0)
                                            reqReport.Append("     <td>" + jobPosting.MaxExpRequired.ToString().Trim() + " years" + "</td>");
                                        else
                                            reqReport.Append("     <td></td>");
                                        break;
                                    case "ClientId":
                                        if (jobPosting.ClientId > 0)
                                        {
                                            Company company = Facade.GetCompanyById(jobPosting.ClientId);
                                            reqReport.Append("     <td width='30px'>" + (company != null ? company.CompanyName : string.Empty).ToString().Trim() + "</td>");
                                        }
                                        else
                                        {
                                            reqReport.Append("     <td></td>");
                                        }
                                        break;
                                    case "ClientHourlyRate":
                                        if ((chkColumnList.Items.FindByValue("ClientHourlyRate").Selected))
                                        {
                                            reqReport.Append("     <td>" + BillRate(jobPosting).ToString().Trim() + "</td>");
                                        }
                                        break;

                                    case "CreatorId":
                                        reqReport.Append("     <td>" + MiscUtil.GetMemberNameById(jobPosting.CreatorId, Facade).ToString().Trim() + "</td>");
                                        break;
                                    case "UpdateDate":
                                        reqReport.Append("     <td>" + (MiscUtil.IsValidDate(jobPosting.UpdateDate) ? jobPosting.UpdateDate.ToShortDateString() : string.Empty).ToString().Trim() + "</td>");
                                        break;
                                    case "JobCategory":
                                        reqReport.Append("     <td>" + jobPosting.JobCategoryText + "</td>");
                                        break;
                                    case "Skills":
                                        reqReport.Append("     <td>" + (MiscUtil.SplitSkillValues(jobPosting.JobSkillLookUpId, '!', Facade)).ToString().Trim() + "</td>");
                                        break;
                                    case "AdditionalNotes":
                                        reqReport.Append("     <td align=left  width='30px'>" + jobPosting.InternalNote.ToString().Trim() + "</td>");
                                        break;
                                    case "AssignedRecruiters":
                                        reqReport.Append("     <td>" + AssignedRecruiters(jobPosting).ToString().Trim() + "</td>");
                                        break;
                                    case "TurnArroundTime":
                                        reqReport.Append("   <td>" + (jobPosting.TurnArroundTime >= 0 ? jobPosting.TurnArroundTime.ToString() + " days" : "") + "</td>");
                                        break;
                                    case "ScheduledInterviews":
                                        reqReport.Append("   <td>" + jobPosting.InterViewCount.ToString() + "</td>");
                                        break;
                                    case "Submissions":
                                        reqReport.Append("   <td>" + jobPosting.SubmissionCount.ToString() + "</td>");
                                        break;
                                    case "Offers":
                                        reqReport.Append("   <td>" + jobPosting.OfferDetailsCount.ToString() + "</td>");
                                        break;
                                    case "Joined":
                                        reqReport.Append("   <td>" + jobPosting.JoiningDetailsCount.ToString() + "</td>");
                                        break;
                                    case "JPICN":
                                        reqReport.Append("   <td>" + jobPosting.InterviewCandidateName.ToString() + "</td>");
                                        break;
                                    case "JPSCN":
                                        reqReport.Append("   <td>" + jobPosting.SubmittedCandidateName.ToString() + "</td>");
                                        break;
                                    case "JPOCN":
                                        reqReport.Append("   <td>" + jobPosting.OfferedCandidateName.ToString() + "</td>");
                                        break;
                                    case "JPJCN":
                                        reqReport.Append("   <td>" + jobPosting.JoinedCandidateName.ToString() + "</td>");
                                        break;
                                    case "JobDesc":
                                        reqReport.Append("   <td>" + jobPosting.JobDescription.ToString() + "</td>");
                                        break;

                                    case "Jobpostnotes":
                                        reqReport.Append("   <td>" + jobPosting.JobPostNotes.ToString() + "</td>");
                                        break;

                                    case "JobLocation":
                                        reqReport.Append("   <td>" + jobPosting.JobLocationText + "</td>");
                                        break;
                                    case "SalesRegion":
                                        reqReport.Append("   <td>" + jobPosting.SalesRegionText + "</td>");
                                        break;
                                    case "SalesGroup":
                                        reqReport.Append("   <td>" + jobPosting.SalesGroupText + "</td>");
                                        break;
                                    case "POAvailability":
                                        string s = "";
                                        switch (jobPosting.POAvailability)
                                        {
                                            case 1:
                                                s = "Yes";
                                                break;
                                            case 2:
                                                s = "No";
                                                break;
                                            case 3:
                                                s = "None";
                                                break;
                                        }
                                        reqReport.Append("   <td>" + s + "</td>");
                                        break;
                                    case "CustomerName":
                                        reqReport.Append("   <td>" + jobPosting.CustomerName + "</td>");
                                        break;
                                    case "BUContact":
                                        reqReport.Append("   <td>" + jobPosting.BUContactText + "</td>");
                                        break;
                                    case "RequisitionType":
                                        reqReport.Append("   <td>" + jobPosting.RequisitionType + "</td>");
                                        break;
                                }
                            }

                        }

                        foreach (ListItem item in chkHiringList.Items.Cast<ListItem>().Where(x => x.Selected == true))
                        {
                            reqReport.Append("     <td>" + jobPosting.HiringMatrixLevels.Where(x => x.HiringMatrixLevelID == Convert.ToInt32(item.Value)).ToList()[0].CandidateCount.ToString() + "</td>");
                        }
                        reqReport.Append(" </tr>");
                    }
                }
                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left  width='300' height='75'><img src='" + LogoPath + "' style= style='height:56px;width:56px;'/></td>");
                reqReport.Append("    </tr>");

                reqReport.Append(" </table>");
            }
            return reqReport.ToString();
        }

        private string AssembleNotes(string notes)
        {
            if (notes != null && notes.Trim() != "")
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(notes);
                XmlNodeList notesList = xmldoc.GetElementsByTagName("Note");
                StringBuilder sbNote = new StringBuilder();
                sbNote.Append("<ul style=\"list-style-type:none;\">");
                //sbNote.Append("<ul>");
                for (int i = 0; i < notesList.Count; i++)
                {
                    sbNote.Append("<li>");// style=\"display:block\">");
                    sbNote.Append(notesList[i].ChildNodes[2].InnerText);
                    sbNote.Append(" by " + notesList[i].ChildNodes[0].InnerText);
                    sbNote.Append(" : " + notesList[i].ChildNodes[1].InnerText);
                    sbNote.Append("</li>");
                }
                sbNote.Append("</ul>");
                return sbNote.ToString();
            }
            return ""; //"JVM";
        }

        private void BindList()
        {
            bool checkedAtleastOne = false;
            foreach (ListItem chkItem in chkColumnList.Items)
            {
                if (chkItem.Selected == true)
                {
                    checkedAtleastOne = true;
                    break;
                }
            }
            this.lsvJobPosting.DataSourceID = "odsJobPostingList";
            if (checkedAtleastOne)
            {
                this.divlsvJobPosting.Visible = true;
                this.lsvJobPosting.DataBind();
            }
            else
            {
                this.divlsvJobPosting.Visible = false;
                MiscUtil.ShowMessage(lblMessage, "Please Select atleast One column", false);
            }

        }

        private void ClearControls()
        {
            hdnRequisitionSelected.Value = "0";
            dtPicker.ClearRange();
            ddlRequisition.Items.Clear();
            int k = 0;
            System.Collections.ArrayList arr = Facade.GetAllJobPostingListByStatusId(k);
            JobPosting j = new JobPosting();
            j.Id = 0;
            j.JobTitle = "Any";
            arr.Insert(0, j);
            ddlRequisition.DataSource = arr;
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataBind();
            ddlJobStatus.SelectedIndex = ddlEmployee.SelectedIndex = ddlEndClient.SelectedIndex = ddlReqCreator.SelectedIndex = ddlRequisition.SelectedIndex = 0; //0.9
            txtCity.Text = "";
            ResetPager();
            lsvJobPosting.Visible = false;
            uclCountryState.SelectedCountryId = 0;


            //ddlState.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));

            ddlEndClient.SelectedIndex = 0;


        }

        private void PopulateJobStatus()
        {
            MiscUtil.PopulateRequistionStatus(ddlJobStatus, Facade);
            ddlJobStatus.Items.Insert(0, new ListItem("Any", "0"));
        }

        void PopulateRequisition(int StatusId)
        {

            ddlRequisition.Items.Clear();
            System.Collections.ArrayList arr = Facade.GetAllJobPostingListByStatusId(StatusId);
            JobPosting j = new JobPosting();
            j.Id = 0;
            j.JobTitle = "Any";
            arr.Insert(0, j);
            ddlRequisition.DataSource = arr;
            ddlRequisition.DataTextField = "JobTitle";
            ddlRequisition.DataValueField = "Id";
            ddlRequisition.DataBind();
            ControlHelper.SelectListByValue(ddlRequisition, hdnRequisitionSelected.Value);
        }
        private void PrepareView()
        {


            PopulateJobStatus();

            //arrr <JobPosting > joblist=Facade .getjob
            PopulateRequisition(0);

            MiscUtil.PopulateMemberListByRole(ddlReqCreator, ContextConstants.ROLE_EMPLOYEE, Facade);

            MiscUtil.PopulateMemberListByRole(ddlEmployee, ContextConstants.ROLE_EMPLOYEE, Facade);

            string accountType = "";
            MiscUtil.PopulateClients(ddlEndClient, (int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting), Facade);
            ddlEndClient.Items[0].Text = "Select BU";
            lblByEndClientsHeader.Text = "By BU";
            ddlEndClient.Items[0].Value = "0";

            if (chkColumnList.Items.FindByText("Account Name") != null) chkColumnList.Items.FindByText("Account Name").Text = AssociateRequisitionsWithEntitiesFromModule + " Name";
            if (chkColumnList.Items.FindByText("Account Job ID") != null) chkColumnList.Items.FindByText("Account Job ID").Text = AssociateRequisitionsWithEntitiesFromModule + " Job ID";
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                if (chkColumnList.Items.FindByValue("ClientHourlyRate") != null) chkColumnList.Items.Remove(chkColumnList.Items.FindByValue("ClientHourlyRate"));
            }
            IList<HiringMatrixLevels> levels = Facade.GetAllHiringMatrixLevels();
            if (levels != null)
            {
                foreach (HiringMatrixLevels lev in levels)
                {
                    ListItem item = new ListItem(lev.Name, lev.Id.ToString());
                    chkHiringList.Items.Add(item);

                }
            }
        }
        private void RenameListHeader(string headerTitle, string LinkName)
        {
            HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvJobPosting.FindControl(headerTitle);
            if (thHeaderTitle != null)
            {
                LinkButton link = (LinkButton)thHeaderTitle.FindControl(LinkName);
                link.Text = link.Text.Replace("Account", AssociateRequisitionsWithEntitiesFromModule);
                link.ToolTip = link.ToolTip.Replace("Account", AssociateRequisitionsWithEntitiesFromModule);


            }

        }
        private void HideUnHideListViewHeaderItem(string headerTitle, string chkItemName)
        {
            HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvJobPosting.FindControl(headerTitle);

            if (thHeaderTitle != null)
            {
                if (chkColumnList.Items.FindByValue(chkItemName) != null)
                {
                    if (!chkColumnList.Items.FindByValue(chkItemName).Selected)
                    {
                        thHeaderTitle.Visible = false;
                    }
                    else
                    {
                        thHeaderTitle.Visible = true;
                    }
                }
                else
                {
                    thHeaderTitle.Visible = false;
                }
            }
        }
        private void HideUnHideListViewHeaderItemByHiringMatrix(string headerTitle, string chkItemName)
        {
            HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvJobPosting.FindControl(headerTitle);

            if (thHeaderTitle != null)
            {
                if (!chkHiringList.Items.FindByValue(chkItemName).Selected)
                {
                    thHeaderTitle.Visible = false;
                }
                else
                {
                    thHeaderTitle.Visible = true;
                }
            }
        }
        private void HideUnhideListViewHeader()
        {
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                RenameListHeader("thClientJobId", "btnClientJobId");
                RenameListHeader("thClientId", "btnClientId");
            }

            HideUnHideListViewHeaderItem("thJobLocation", "JobLocation");


            HideUnHideListViewHeaderItem("thSalesRegion", "SalesRegion");
            HideUnHideListViewHeaderItem("thSalesGroup", "SalesGroup");
            HideUnHideListViewHeaderItem("thPOAvailability", "POAvailability");
            HideUnHideListViewHeaderItem("thBUContact", "BUContact");
            HideUnHideListViewHeaderItem("thCustomerName", "CustomerName");

            HideUnHideListViewHeaderItem("thScheduledInterviews", "ScheduledInterviews");
            HideUnHideListViewHeaderItem("thSubmissions", "Submissions");
            HideUnHideListViewHeaderItem("thOffers", "Offers");
            HideUnHideListViewHeaderItem("thJoined", "Joined");

            HideUnHideListViewHeaderItem("thPostDate", "PostedDate");
            HideUnHideListViewHeaderItem("thJobTitle", "JobTitle");
            HideUnHideListViewHeaderItem("thJobPostingCode", "JobPostingCode");
            HideUnHideListViewHeaderItem("thTimeToFill", "TimeToFill");
            HideUnHideListViewHeaderItem("thClientJobId", "ClientJobId");
            HideUnHideListViewHeaderItem("thNoOfOpenings", "NoOfOpenings");

            HideUnHideListViewHeaderItem("thNoofUnfilledOpenings", "NoofUnfilledOpenings");

            HideUnHideListViewHeaderItem("thPayRate", "PayRate");

            HideUnHideListViewHeaderItem("thPayCycle", "PayCycle");
            HideUnHideListViewHeaderItem("thTravelRequired", "TravelRequired");
            HideUnHideListViewHeaderItem("thTravelRequiredPercent", "TravelRequiredPercent");
            HideUnHideListViewHeaderItem("thOtherBenefits", "OtherBenefits");
            HideUnHideListViewHeaderItem("thReqStatus", "JobStatus");
            HideUnHideListViewHeaderItemByHiringMatrix("thPreSelected", "PreSelected");
            HideUnHideListViewHeaderItemByHiringMatrix("thLevel1", "Level1");
            HideUnHideListViewHeaderItemByHiringMatrix("thLevel2", "Level2");
            HideUnHideListViewHeaderItemByHiringMatrix("thLevel3", "Level3");
            HideUnHideListViewHeaderItemByHiringMatrix("thFinalHired", "FinalHire");
            HideUnHideListViewHeaderItem("thJobDurationLookupId", "JobDurationLookupId");
            HideUnHideListViewHeaderItem("thJobDurationMonth", "JobDurationMonth");
            HideUnHideListViewHeaderItem("thJobAddress1", "JobAddress1");
            HideUnHideListViewHeaderItem("thJobAddress2", "JobAddress2");
            HideUnHideListViewHeaderItem("thCity", "City");
            HideUnHideListViewHeaderItem("thZipCode", "ZipCode");
            HideUnHideListViewHeaderItem("thCountryId", "CountryId");
            HideUnHideListViewHeaderItem("thStateId", "StateId");
            HideUnHideListViewHeaderItem("thReportingTo", "ReportingTo");
            HideUnHideListViewHeaderItem("thNoOfReportees", "NoOfReportees");
            HideUnHideListViewHeaderItem("thStartDate", "StartDate");
            HideUnHideListViewHeaderItem("thFinalHiredDate", "FinalHiredDate");
            HideUnHideListViewHeaderItem("thAuthorizationTypeLookupId", "AuthorizationTypeLookupId");
            //HideUnHideListViewHeaderItem("thPostedDate", "PostedDate");
            HideUnHideListViewHeaderItem("thActivationDate", "ActivationDate");
            HideUnHideListViewHeaderItem("thRequiredDegreeLookupId", "RequiredDegreeLookupId");
            HideUnHideListViewHeaderItem("thExpRequired", "ExpRequired");
            HideUnHideListViewHeaderItem("thVendorGroupId", "VendorGroupId");
            HideUnHideListViewHeaderItem("thIsJobActive", "IsJobActive");
            HideUnHideListViewHeaderItem("thPublishedForInternal", "PublishedForInternal");
            HideUnHideListViewHeaderItem("thPublishedForPublic", "PublishedForPublic");
            HideUnHideListViewHeaderItem("thPublishedForPartner", "PublishedForPartner");
            HideUnHideListViewHeaderItem("thMinAgeRequired", "MinAgeRequired");
            HideUnHideListViewHeaderItem("thMaxAgeRequired", "MaxAgeRequired");
            HideUnHideListViewHeaderItem("thReqType", "JobType");
            HideUnHideListViewHeaderItem("thIsApprovalRequired", "IsApprovalRequired");
            HideUnHideListViewHeaderItem("thClientId", "ClientId");
            HideUnHideListViewHeaderItem("thClientProjectId", "ClientProjectId");
            HideUnHideListViewHeaderItem("thClientEndClientId", "ClientEndClientId");
            HideUnHideListViewHeaderItem("thClientHourlyRate", "ClientHourlyRate");
            HideUnHideListViewHeaderItem("thClientRatePayCycle", "ClientRatePayCycle");
            HideUnHideListViewHeaderItem("thClientDisplayName", "ClientDisplayName");
            HideUnHideListViewHeaderItem("thClientId2", "ClientId2");
            HideUnHideListViewHeaderItem("thClientId3", "ClientId3");
            HideUnHideListViewHeaderItem("thTaxTermLookupIds", "TaxTermLookupIds");
            HideUnHideListViewHeaderItem("thJobCategoryLookupId", "JobCategoryLookupId");
            HideUnHideListViewHeaderItem("thJobIndustryLookupId", "JobIndustryLookupId");
            HideUnHideListViewHeaderItem("thExpectedRevenue", "ExpectedRevenue");
            HideUnHideListViewHeaderItem("thSourcingChannel", "SourcingChannel");
            HideUnHideListViewHeaderItem("thSourcingExpenses", "SourcingExpenses");
            HideUnHideListViewHeaderItem("thPublishedForVendor", "PublishedForVendor");
            HideUnHideListViewHeaderItem("thTeleCommunication", "TeleCommunication");
            HideUnHideListViewHeaderItem("thCreatorId", "CreatorId");
            HideUnHideListViewHeaderItem("thUpdatorId", "UpdatorId");
            HideUnHideListViewHeaderItem("thCreateDate", "CreateDate");
            HideUnHideListViewHeaderItem("thUpdateDate", "UpdateDate");

            HideUnHideListViewHeaderItem("thJobCategory", "JobCategory");
            HideUnHideListViewHeaderItem("thSkills", "Skills");
            HideUnHideListViewHeaderItem("thMinimumQualifyingParameters", "MinimumQualifyingParameters");
            HideUnHideListViewHeaderItem("thWorkSchedule", "WorkSchedule");
            HideUnHideListViewHeaderItem("thSecurityClearance", "SecurityClearance");
            HideUnHideListViewHeaderItem("thExpenses", "Expenses");
            HideUnHideListViewHeaderItem("thAdditionalNotes", "AdditionalNotes");
            HideUnHideListViewHeaderItem("thAssignedRecruiters", "AssignedRecruiters");
            HideUnHideListViewHeaderItem("thTurnArroundTime", "TurnArroundTime");

            HideUnHideListViewHeaderItem("thInterviewCandidate", "JPICN");
            HideUnHideListViewHeaderItem("thSubmittedCandidate", "JPSCN");
            HideUnHideListViewHeaderItem("thOfferedCandidate", "JPOCN");
            HideUnHideListViewHeaderItem("thJoinedCandidate", "JPJCN");
            HideUnHideListViewHeaderItem("thJobDescription", "JobDesc");
            HideUnHideListViewHeaderItem("thNotes", "Jobpostnotes");
            HideUnHideListViewHeaderItem("thRequisitionType", "RequisitionType");
        }

        public void repor(string rep)
        {
            int j = 0;
            int k = 0;
            foreach (ListItem items in chkColumnList.Items)
            {
                if (items.Selected)
                {
                    j = 1;

                }
                else
                    k = 1;
            }
            if ((j == 1 && k == 1) || (k == 0 && j == 1))
                GenerateRequisitionReport(rep);
            else
            {
                //Modal.Hide();
                MiscUtil.ShowMessage(lbMessage, "Please select columns before generating the report.", true);
            }
        }
        #endregion

        #region Properties
        string _cookiename;
        string CookieName
        {
            get { return _cookiename; }
            set { _cookiename = value; }
        }

        string HiringMatrixColumnsCookieeName
        {
            get;
            set;
        }

        #endregion

        #region Events

        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {

                //Modal.Show();
                repor("pdf");

            }
            else btnSearch_Click(sender, new EventArgs());
        }

        private void HideListItemsForCommonApp()
        {
            ListItem workauth = chkColumnList.Items.FindByValue("JobLocation");
            if (chkColumnList.Items.Contains(workauth))
            {
                chkColumnList.Items.Remove(workauth);
            }
            workauth = chkColumnList.Items.FindByValue("SalesRegion");
            if (chkColumnList.Items.Contains(workauth))
            {
                chkColumnList.Items.Remove(workauth);
            }
            workauth = chkColumnList.Items.FindByValue("SalesGroup");
            if (chkColumnList.Items.Contains(workauth))
            {
                chkColumnList.Items.Remove(workauth);
            }
            workauth = chkColumnList.Items.FindByValue("POAvailability");
            if (chkColumnList.Items.Contains(workauth))
            {
                chkColumnList.Items.Remove(workauth);
            }
            workauth = chkColumnList.Items.FindByValue("CustomerName");
            if (chkColumnList.Items.Contains(workauth))
            {
                chkColumnList.Items.Remove(workauth);
            }



        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
            //    {
            //        FormsAuthentication.SignOut();
            //        FormsAuthentication.RedirectToLoginPage();
            //        return;
            //    }
            //}
            //catch { }
            uclConfirm.MsgBoxAnswered += MessageAnswered;

            ddlJobStatus.Attributes.Add("onChange", "return RequisitionStatusOnChange('" + ddlJobStatus.ClientID + "','" + ddlRequisition.ClientID + "')");
            ddlRequisition.Attributes.Add("onChange", "return RequisitionOnChange('" + ddlRequisition.ClientID + "','" + hdnRequisitionSelected.ClientID + "')");
            ddlEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlReqCreator.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlEndClient.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            if (CookieName != null) CookieName = "UserColumnOption_" + base.CurrentMember.Id;
            if (base.CurrentMember != null) HiringMatrixColumnsCookieeName = "HiringMatrixColumns_" + base.CurrentMember.Id;
            if (!IsPostBack)
            {

                if (ApplicationSource == ApplicationSource.MainApplication) HideListItemsForCommonApp();

                PrepareView();
                ExportButtons.Visible = false;
                if (Request.Cookies[CookieName] != null)
                {
                    //string ColumnOptions = Request.Cookies[CookieName].Value;
                    string ColumnOptions = "PostedDate#JobTitle#JobPostingCode#TimeToFill#NoOfOpenings#NoofUnfilledOpenings#PayRate#JobStatus#City#StartDate#FinalHiredDate#ClientId#BUContact#CreatorId#Skills#AssignedRecruiters#TurnArroundTime#";
                    string[] Columns = ColumnOptions.Split('#');
                    foreach (ListItem item in chkColumnList.Items)
                        item.Selected = false;
                    foreach (string item in Columns)
                        if (chkColumnList.Items.FindByValue(item) != null) chkColumnList.Items.FindByValue(item).Selected = true;
                }
                if (Request.Cookies[HiringMatrixColumnsCookieeName] != null)
                {
                    string ColumnOptions = Request.Cookies[HiringMatrixColumnsCookieeName].Value;
                    string[] Columns = ColumnOptions.Split('#');
                    foreach (ListItem item in chkHiringList.Items)
                        item.Selected = false;
                    foreach (string item in Columns)
                        if (chkHiringList.Items.FindByValue(item) != null) chkHiringList.Items.FindByValue(item).Selected = true;
                }

            }
            if (IsPostBack)
            {
                System.Web.UI.HtmlControls.HtmlTableRow thLevels = (System.Web.UI.HtmlControls.HtmlTableRow)this.lsvJobPosting.FindControl("trHeader");
                if (thLevels != null)
                {
                    IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
                    int ColLocation = 11;
                    if (hiringMatrixLevels != null)
                    {
                        if (hiringMatrixLevels.Count > 0)
                        {
                            foreach (HiringMatrixLevels levels in hiringMatrixLevels)
                            {
                                if (checkCheckBoxHiringLevelSelected(levels.Id))
                                {
                                    System.Web.UI.HtmlControls.HtmlTableCell col = new System.Web.UI.HtmlControls.HtmlTableCell("TH");
                                    LinkButton lnkLevel = new LinkButton();
                                    lnkLevel.Text = levels.Name;
                                    lnkLevel.CommandName = "Sort";
                                    lnkLevel.ID = "levl" + ColLocation;
                                    lnkLevel.EnableViewState = true;
                                    lnkLevel.CommandArgument = levels.Id.ToString();

                                    col.Controls.Add(lnkLevel);
                                    LinkButton ll = (LinkButton)thLevels.FindControl(lnkLevel.ID);
                                    if (ll == null) thLevels.Controls.AddAt(ColLocation, col);
                                    ColLocation++;
                                }
                            }
                        }

                    }
                }
                HideUnhideListViewHeader();
            }
            //if (System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern == "dd/MM/yyyy")
            //{
            //    ListItem postalcode = chkColumnList.Items.FindByValue("ZipCode");
            //    postalcode.Text = "Pin Code";

            //}
            if (System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern == "dd/MM/yyyy")
            {
                ListItem workauth = chkColumnList.Items.FindByValue("AuthorizationTypeLookupId");
                if (chkColumnList.Items.Contains(workauth))
                {
                    chkColumnList.Items.Remove(workauth);
                }
            }
            if (!IsPostBack)
            {

                btnSearch_Click(sender, e);
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string s = hdnRequisitionSelected.Value;
            lsvJobPosting.Visible = true;
            hdnScrollPos.Value = "0";
            int count = 0;
            //Adding Selected Columns to Cookie
            string ColumnOption = "";
            foreach (ListItem item in chkColumnList.Items)
                if (item.Selected == true) ColumnOption += item.Value + "#";
            System.Web.HttpCookie aCookie = new System.Web.HttpCookie(CookieName);
            aCookie.Value = ColumnOption.ToString();
            Response.Cookies.Add(aCookie);
            //Adding Hiring Matrix Selected Column to cookie
            string HiringColumnColumnOption = "";
            foreach (ListItem item in chkHiringList.Items)
                if (item.Selected == true) HiringColumnColumnOption += item.Value + "#";
            System.Web.HttpCookie hiringaCookie = new System.Web.HttpCookie(HiringMatrixColumnsCookieeName);
            hiringaCookie.Value = HiringColumnColumnOption.ToString();
            Response.Cookies.Add(hiringaCookie);
            lsvJobPosting.Items.Clear();
            /////////added by Sumit Sonawane 11/Nov/2016/////////////////////////
            ddlEmployee.SelectedValue = CurrentMember.Id.ToString();
            ddlEmployee.Enabled = false;
            ddlEmployee.Style["cursor"] = "not-allowed";
            ddlEmployee.BackColor = System.Drawing.Color.LightBlue;
            /////////////////////////////////////////////////////////////////////
            ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControls != null)
            {
                DataPager pager = (DataPager)PagerControls.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            BindList();
            if (lsvJobPosting != null && lsvJobPosting.Items.Count > 0)
            {
                ExportButtons.Visible = true;
                HideUnhideListViewHeader();
                lsvJobPosting.Visible = true;
            }
            else ExportButtons.Visible = false;
            System.Web.UI.HtmlControls.HtmlTableRow thLevels = (System.Web.UI.HtmlControls.HtmlTableRow)this.lsvJobPosting.FindControl("trHeader");
            if (thLevels != null)
            {
                IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
                int ColLocation = 11;
                if (hiringMatrixLevels != null)
                {
                    if (hiringMatrixLevels.Count > 0)
                    {
                        foreach (HiringMatrixLevels levels in hiringMatrixLevels)
                        {
                            if (checkCheckBoxHiringLevelSelected(levels.Id))
                            {
                                System.Web.UI.HtmlControls.HtmlTableCell col = new System.Web.UI.HtmlControls.HtmlTableCell("TH");
                                LinkButton lnkLevel = new LinkButton();
                                lnkLevel.Text = levels.Name;
                                lnkLevel.CommandName = "Sort";
                                lnkLevel.ID = "levl" + ColLocation;
                                lnkLevel.EnableViewState = true;
                                lnkLevel.CommandArgument = "[" + levels.Id.ToString() + "]";
                                col.Controls.Add(lnkLevel);
                                LinkButton ll = (LinkButton)thLevels.FindControl(lnkLevel.ID);
                                if (ll == null) thLevels.Controls.AddAt(ColLocation, col);
                                ColLocation++;
                            }
                        }
                    }

                }
                System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("tdpager");
                if (tdPager != null)
                {
                    tdPager.ColSpan = chkColumnList.Items.Cast<ListItem>().Where(x => x.Selected).Count() + chkHiringList.Items.Cast<ListItem>().Where(x => x.Selected).Count();
                }
            }
            HideUnhideListViewHeader();
            if (hdnSortColumn.Value == "") hdnSortColumn.Value = "btnPostdate";
            if (hdnSortOrder.Value == "") hdnSortOrder.Value = "DESC";
            PlaceUpDownArrow();
            string pagesize = "";

            pagesize = (Request.Cookies["RequisitionReportRowPerPage"] == null ? "" : Request.Cookies["RequisitionReportRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;

            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;


            //MiscUtil.PopulateState(ddlState, Convert.ToInt32(ddlCountry.SelectedValue), Facade);
            //ControlHelper.SelectListByValue(ddlState, hdnSelectedStateId.Value);
            //ddlState.Items.RemoveAt(0);
            //ddlState.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));

            PopulateRequisition(Convert.ToInt32(ddlJobStatus.SelectedValue));


        }

        protected void ddlJobStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            MiscUtil.LoadAllControlState(Request, this);
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
            ExportButtons.Visible = false;

        }

        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {
            uclConfirm.AddMessage("Note: Some columns may not appear in report if too many are selected.", ConfirmationWindow.enmMessageType.Attention, true, true, "");
            BindList();
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            repor("excel");
        }

        protected void btnExportToWord_Click(object sender, EventArgs e)
        {
            //Modal.Show();
            ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(this.btnExportToWord);
            repor("word");
        }

        private bool checkCheckBoxHiringLevelSelected(int value)
        {
            foreach (ListItem item in chkHiringList.Items)
            {
                if (Convert.ToInt32(item.Value) == value && item.Selected == true) return true;

            }
            return false;
        }
        protected void lsvJobPosting_PreRender(object sender, EventArgs e)
        {
            LinkButton btnPostedDate1 = (LinkButton)lsvJobPosting.FindControl("btnPostedDate1");
            if (btnPostedDate1 != null) btnPostedDate1.Text = "Date Published";

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "RequisitionReportRowPerPage";
            }
            PlaceUpDownArrow();
            HideUnhideListViewHeader();
            if (IsPostBack)
            {
                if (lsvJobPosting.Items.Count == 0)
                {
                    lsvJobPosting.DataSource = null;
                    lsvJobPosting.DataBind();
                }
            }
        }

        protected void lsvJobPosting_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                }
            }
            catch
            {
            }
        }

        string TimeToFill(JobPosting jobPosting)
        {
            if (((jobPosting.FinalHiredDate == DateTime.MinValue) || (jobPosting.FinalHiredDate == null)) || ((jobPosting.ActivationDate == DateTime.MinValue) || (jobPosting.ActivationDate == null)))
                return "";
            else
                return MiscUtil.FindDateDiffFrmEndtoStart(jobPosting.FinalHiredDate, jobPosting.OpenDate);
        }

        string PayRate(JobPosting jobPosting)
        {

            string payrate = "";
            payrate = jobPosting.PayRate;
            if (Convert.ToInt64(jobPosting.MaxPayRate) == jobPosting.MaxPayRate) jobPosting.MaxPayRate = Convert.ToInt64(jobPosting.MaxPayRate);
            if (payrate != string.Empty && jobPosting.MaxPayRate > 0 && payrate.ToLower().Trim() != "open") payrate += "-";
            if (jobPosting.MaxPayRate > 0 && payrate.ToLower().Trim() != "open") payrate += jobPosting.MaxPayRate;


            string paymethod = "";
            switch (jobPosting.PayCycle)
            {
                case "1":
                    paymethod = "Hourly";
                    break;
                case "4":
                    paymethod = "Yearly";
                    break;
                case "3":
                    paymethod = "Monthly";
                    break;
                case "2":
                    paymethod = "Daily";
                    break;
            }
            if (paymethod.Length > 0 && (payrate != ""))
                return payrate + " / " + paymethod;
            else
                if (payrate != "")
                    return payrate;
                else
                    return "";
        }

        string BillRate(JobPosting jobPosting)
        {
            string paycycle = "";
            switch (jobPosting.ClientRatePayCycle)
            {
                case "1":
                    paycycle = "hour";
                    break;
                case "4":
                    paycycle = "year";
                    break;
                case "3":
                    paycycle = "month";
                    break;
                case "2":
                    paycycle = "day";
                    break;
            }
            if (paycycle.Length > 0 && (jobPosting.ClientHourlyRate != ""))
                return jobPosting.ClientHourlyRate + " / " + paycycle;
            else
                if (jobPosting.ClientHourlyRate != "")
                    return jobPosting.ClientHourlyRate;
                else
                    return "";
        }

        protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
                int ColLocation = 11;
                if (jobPosting != null)
                {
                    System.Web.UI.HtmlControls.HtmlTableRow tdLevels = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("row");
                    foreach (Hiring_Levels lev in jobPosting.HiringMatrixLevels)
                    {
                        if (checkCheckBoxHiringLevelSelected(lev.HiringMatrixLevelID))
                        {
                            System.Web.UI.HtmlControls.HtmlTableCell col1 = new System.Web.UI.HtmlControls.HtmlTableCell("TD");
                            Label lnkLevel = new Label();
                            lnkLevel.Text = lev.CandidateCount.ToString();

                            HiringMatrixLevels l = Facade.GetHiringMatrixLevelsById(lev.HiringMatrixLevelID);
                            //  if (l != null)
                            //     lnkLevel.Width = Unit.Pixel(l.Name.Length * 7 + 30);
                            col1.EnableViewState = true;

                            col1.Controls.Add(lnkLevel);
                            col1.EnableViewState = true;
                            tdLevels.Controls.AddAt(ColLocation, col1);
                            ColLocation++;
                        }
                    }
                    //Post Date
                    Label lblPostdate = (Label)e.Item.FindControl("lblPostdate");
                    HtmlTableCell tdPostdate = (HtmlTableCell)e.Item.FindControl("tdPostdate");
                    if (chkColumnList.Items.FindByValue("PostedDate").Selected)
                    {
                        lblPostdate.Text = jobPosting.PostedDate.ToShortDateString();
                        lblPostdate.Visible = true;
                        tdPostdate.Visible = true;
                    }
                    else
                    {
                        tdPostdate.Visible = false;
                        lblPostdate.Visible = false;
                    }
                    //TurnArroundTime
                    Label lblTurnArroundTime = (Label)e.Item.FindControl("lblTurnArroundTime");
                    HtmlTableCell tdTurnArroundtime = (HtmlTableCell)e.Item.FindControl("tdTurnArroundTime");
                    if (chkColumnList.Items.FindByValue("TurnArroundTime").Selected)
                    {

                        lblTurnArroundTime.Text = jobPosting.TurnArroundTime.ToString() + " days";
                        lblTurnArroundTime.Visible = jobPosting.TurnArroundTime >= 0;
                        tdTurnArroundtime.Visible = true;
                    }
                    else
                    {
                        lblTurnArroundTime.Visible = tdTurnArroundtime.Visible = false;
                    }

                    //JobTitle
                    HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");//  0.4 
                    HtmlTableCell tdReqTitle = (HtmlTableCell)e.Item.FindControl("tdReqTitle");
                    if (chkColumnList.Items.FindByValue("JobTitle").Selected)
                    {
                        lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                        lnkJobTitle.Text = jobPosting.JobTitle;
                        lnkJobTitle.Visible = true;
                        tdReqTitle.Visible = true;
                    }
                    else
                    {
                        tdReqTitle.Visible = false;
                        lnkJobTitle.Visible = false;
                    }


                    Label lblJobPostingCode = (Label)e.Item.FindControl("lblJobPostingCode");
                    HtmlTableCell tdJobPostingCode = (HtmlTableCell)e.Item.FindControl("tdJobPostingCode");
                    if (chkColumnList.Items.FindByValue("JobPostingCode").Selected)
                    {
                        lblJobPostingCode.Text = jobPosting.JobPostingCode;
                        tdJobPostingCode.Visible = true;
                        lblJobPostingCode.Visible = true;
                    }
                    else
                    {
                        tdJobPostingCode.Visible = false;
                        lblJobPostingCode.Visible = false;
                    }

                    Label lblTimeToFill = (Label)e.Item.FindControl("lblTimeToFill");
                    HtmlTableCell tdTimeToFill = (HtmlTableCell)e.Item.FindControl("tdTimeToFill");
                    if (chkColumnList.Items.FindByValue("TimeToFill").Selected)
                    {
                        lblTimeToFill.Text = TimeToFill(jobPosting);
                        tdTimeToFill.Visible = true;
                        lblTimeToFill.Visible = true;
                    }
                    else
                    {
                        tdTimeToFill.Visible = false;
                        lblTimeToFill.Visible = false;
                    }

                    Label lblClientJobId = (Label)e.Item.FindControl("lblClientJobId");
                    HtmlTableCell tdClientJobId = (HtmlTableCell)e.Item.FindControl("tdClientJobId");
                    if (chkColumnList.Items.FindByValue("ClientJobId").Selected)
                    {
                        lblClientJobId.Text = jobPosting.ClientJobId;
                        tdClientJobId.Visible = true;
                        lblClientJobId.Visible = true;
                    }
                    else
                    {
                        lblClientJobId.Visible = false;
                        tdClientJobId.Visible = false;
                    }

                    Label lblNoOfOpenings = (Label)e.Item.FindControl("lblNoOfOpenings");
                    HtmlTableCell tdNoOfOpenings = (HtmlTableCell)e.Item.FindControl("tdNoOfOpenings");
                    if (chkColumnList.Items.FindByValue("NoOfOpenings").Selected)
                    {
                        lblNoOfOpenings.Text = jobPosting.NoOfOpenings.ToString();
                        tdNoOfOpenings.Visible = true;
                        lblNoOfOpenings.Visible = true;
                    }
                    else
                    {
                        lblNoOfOpenings.Visible = false;
                        tdNoOfOpenings.Visible = false;
                    }

                    //No Of Unfilled Openings
                    Label lblNoofUnfilledOpenings = (Label)e.Item.FindControl("lblNoofUnfilledOpenings");
                    HtmlTableCell tdNoofUnfilledOpenings = (HtmlTableCell)e.Item.FindControl("tdNoofUnfilledOpenings");
                    if (chkColumnList.Items.FindByValue("NoofUnfilledOpenings").Selected)
                    {
                        lblNoofUnfilledOpenings.Text = jobPosting.NoOfUnfilledOpenings.ToString();
                        tdNoofUnfilledOpenings.Visible = true;
                        lblNoofUnfilledOpenings.Visible = true;
                    }
                    else
                    {
                        tdNoofUnfilledOpenings.Visible = false;
                        lblNoofUnfilledOpenings.Visible = false;
                    }

                    Label lblPayRate = (Label)e.Item.FindControl("lblPayRate");
                    HtmlTableCell tdPayRate = (HtmlTableCell)e.Item.FindControl("tdPayRate");
                    //if (chkColumnList.Items.FindByValue("PayRate").Selected)
                    //{

                    //    lblPayRate.Text = PayRate(jobPosting);
                    //    tdPayRate.Visible = true;
                    //    lblPayRate.Visible = true;
                    //}
                    //else
                    //{
                    //    lblPayRate.Visible = false;
                    //    tdPayRate.Visible = false;
                    //}
                    HtmlTableCell tdJobLocation = (HtmlTableCell)e.Item.FindControl("tdJobLocation");
                    HtmlTableCell tdSalesRegion = (HtmlTableCell)e.Item.FindControl("tdSalesRegion");
                    HtmlTableCell tdSalesGroup = (HtmlTableCell)e.Item.FindControl("tdSalesGroup");
                    HtmlTableCell tdPOAvailability = (HtmlTableCell)e.Item.FindControl("tdPOAvailability");
                    HtmlTableCell tdCustomerName = (HtmlTableCell)e.Item.FindControl("tdCustomerName");


                    //if (ApplicationSource != ApplicationSource.LandTApplication)
                    //{
                    //    Label lblJobLocation = (Label)e.Item.FindControl("lblJobLocation");

                    //    //if (chkColumnList.Items.FindByValue("JobLocation").Selected)
                    //    //{
                    //    //    lblJobLocation.Text = jobPosting.JobLocationText;
                    //    //    tdJobLocation.Visible = true;
                    //    //    lblJobLocation.Visible = true;
                    //    //}
                    //    //else
                    //    //{
                    //    //    lblJobLocation.Visible = false;
                    //    //    tdJobLocation.Visible = false;
                    //    //}


                    //    Label lblSalesRegion = (Label)e.Item.FindControl("lblSalesRegion");

                    //    //if (chkColumnList.Items.FindByValue("SalesRegion").Selected)
                    //    //{
                    //    //    lblSalesRegion.Text = jobPosting.SalesRegionText;
                    //    //    tdSalesRegion.Visible = true;
                    //    //    lblSalesRegion.Visible = true;

                    //    //}
                    //    //else
                    //    //{
                    //    //    lblSalesRegion.Visible = false;
                    //    //    tdSalesRegion.Visible = false;
                    //    //}


                    //    //Label lblSalesGroup = (Label)e.Item.FindControl("lblSalesGroup");

                    //    //if (chkColumnList.Items.FindByValue("SalesGroup").Selected)
                    //    //{
                    //    //    lblSalesGroup.Text = jobPosting.SalesGroupText;
                    //    //    tdSalesGroup.Visible = true;
                    //    //    lblSalesGroup.Visible = true;



                    //    //}
                    //    //else
                    //    //{
                    //    //    lblSalesGroup.Visible = false;
                    //    //    tdSalesGroup.Visible = false;

                    //    //}


                    //    Label lblPOAvailability = (Label)e.Item.FindControl("lblPOAvailability");

                    //    //if (chkColumnList.Items.FindByValue("POAvailability").Selected)
                    //    //{
                    //    //    if (jobPosting.POAvailability > 0)
                    //    //    {
                    //    //        switch (jobPosting.POAvailability)
                    //    //        {
                    //    //            case 1:
                    //    //                lblPOAvailability.Text = "Yes";
                    //    //                break;
                    //    //            case 2:
                    //    //                lblPOAvailability.Text = "No";
                    //    //                break;
                    //    //            case 3:
                    //    //                lblPOAvailability.Text = "None";
                    //    //                break;
                    //    //        }
                    //    //    }
                    //    //    tdPOAvailability.Visible = true;
                    //    //    lblPOAvailability.Visible = true;



                    //    //}
                    //    //else if (ApplicationSource == ApplicationSource.MainApplication)
                    //    //{
                    //    //    lblPOAvailability.Visible = false;
                    //    //    tdPOAvailability.Visible = false;
                    //    //}




                    //    //Customer Name
                    //    Label lblCustomerName = (Label)e.Item.FindControl("lblCustomerName");

                    //    if (chkColumnList.Items.FindByValue("CustomerName").Selected)
                    //    {
                    //        if (jobPosting.CustomerName != "")
                    //        {

                    //            lblCustomerName.Text = jobPosting.CustomerName;
                    //        }
                    //        tdCustomerName.Visible = true;
                    //        lblCustomerName.Visible = true;



                    //    }
                    //    else
                    //    {
                    //        lblCustomerName.Visible = false;
                    //        tdCustomerName.Visible = false;
                    //    }
                    //}
                    //else
                    //{
                    //    tdJobLocation.Visible = false;
                    //    tdSalesGroup.Visible = tdSalesRegion.Visible = tdPOAvailability.Visible = tdCustomerName.Visible = false;
                    //}

                    Label lblReqStatus = (Label)e.Item.FindControl("lblReqStatus");
                    HtmlTableCell tdReqStatus = (HtmlTableCell)e.Item.FindControl("tdReqStatus");
                    if (chkColumnList.Items.FindByValue("JobStatus").Selected)
                    {
                        GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(jobPosting.JobStatus);
                        if (_RequisitionStatusLookup != null)
                        {
                            lblReqStatus.Text = _RequisitionStatusLookup.Name;
                        }
                        tdReqStatus.Visible = true;
                        lblReqStatus.Visible = true;
                    }
                    else
                    {
                        lblReqStatus.Visible = false;
                        tdReqStatus.Visible = false;
                    }



                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    HtmlTableCell tdCity = (HtmlTableCell)e.Item.FindControl("tdCity");
                    if (chkColumnList.Items.FindByValue("City").Selected)
                    {
                        lblCity.Text = jobPosting.City;
                        tdCity.Visible = true;
                        lblCity.Visible = true;
                    }
                    else
                    {
                        lblCity.Visible = false;
                        tdCity.Visible = false;
                    }




                    Label lblCountryId = (Label)e.Item.FindControl("lblCountryId");
                    HtmlTableCell tdCountryId = (HtmlTableCell)e.Item.FindControl("tdCountryId");
                    if (chkColumnList.Items.FindByValue("CountryId").Selected)
                    {
                        if (jobPosting.CountryId > 0)
                        {
                            lblCountryId.Text = Facade.GetCountryById(jobPosting.CountryId).Name;
                        }
                        tdCountryId.Visible = true;
                        lblCountryId.Visible = true;
                    }
                    else
                    {
                        lblCountryId.Visible = false;
                        tdCountryId.Visible = false;
                    }



                    Label lblStartDate = (Label)e.Item.FindControl("lblStartDate");
                    HtmlTableCell tdStartDate = (HtmlTableCell)e.Item.FindControl("tdStartDate");
                    if (chkColumnList.Items.FindByValue("StartDate").Selected)
                    {
                        lblStartDate.Text = StartDate(jobPosting);
                        tdStartDate.Visible = true;
                        lblStartDate.Visible = true;
                    }
                    else
                    {
                        lblStartDate.Visible = false;
                        tdStartDate.Visible = false;
                    }

                    Label lblFinalHiredDate = (Label)e.Item.FindControl("lblFinalHiredDate");
                    HtmlTableCell tdFinalHiredDate = (HtmlTableCell)e.Item.FindControl("tdFinalHiredDate");
                    if (chkColumnList.Items.FindByValue("FinalHiredDate").Selected)
                    {
                        lblFinalHiredDate.Text = (MiscUtil.IsValidDate(jobPosting.FinalHiredDate) ? jobPosting.FinalHiredDate.ToShortDateString() : string.Empty);
                        tdFinalHiredDate.Visible = true;
                        lblFinalHiredDate.Visible = true;
                    }
                    else
                    {
                        lblFinalHiredDate.Visible = false;
                        tdFinalHiredDate.Visible = false;
                    }




                    Label lblRequiredDegreeLookupId = (Label)e.Item.FindControl("lblRequiredDegreeLookupId");
                    HtmlTableCell tdRequiredDegreeLookupId = (HtmlTableCell)e.Item.FindControl("tdRequiredDegreeLookupId");
                    if (chkColumnList.Items.FindByValue("RequiredDegreeLookupId").Selected)
                    {
                        lblRequiredDegreeLookupId.Text = MiscUtil.SplitValues(jobPosting.RequiredDegreeLookupId, ',', Facade);
                        tdRequiredDegreeLookupId.Visible = true;
                        lblRequiredDegreeLookupId.Visible = true;
                    }
                    else
                    {
                        lblRequiredDegreeLookupId.Visible = false;
                        tdRequiredDegreeLookupId.Visible = false;
                    }

                    Label lblMinExpRequired = (Label)e.Item.FindControl("lblExpRequired");
                    HtmlTableCell tdMinExpRequired = (HtmlTableCell)e.Item.FindControl("tdExpRequired");
                    if (chkColumnList.Items.FindByValue("ExpRequired").Selected)
                    {
                        if (jobPosting.MaxExpRequired.Length > 0 && jobPosting.MinExpRequired.Length > 0)
                            lblMinExpRequired.Text = jobPosting.MinExpRequired + " - " + jobPosting.MaxExpRequired + " years";
                        else if (jobPosting.MinExpRequired.Length > 0)
                            lblMinExpRequired.Text = jobPosting.MinExpRequired + " years";
                        else if (jobPosting.MaxExpRequired.Length > 0)
                            lblMinExpRequired.Text = jobPosting.MaxExpRequired + " years";
                        tdMinExpRequired.Visible = true;
                        lblMinExpRequired.Visible = true;
                    }
                    else
                    {
                        lblMinExpRequired.Visible = false;
                        tdMinExpRequired.Visible = false;
                    }


                    Label lblClientId = (Label)e.Item.FindControl("lblClientId");
                    HtmlTableCell tdClientId = (HtmlTableCell)e.Item.FindControl("tdClientId");
                    if (chkColumnList.Items.FindByValue("ClientId").Selected)
                    {
                        if (jobPosting.ClientId > 0)
                        {
                            Company company = Facade.GetCompanyById(jobPosting.ClientId);
                            lblClientId.Text = (company != null ? company.CompanyName : string.Empty);
                        }
                        tdClientId.Visible = true;
                        lblClientId.Visible = true;
                    }
                    else
                    {
                        lblClientId.Visible = false;
                        tdClientId.Visible = false;
                    }

                    Label lblBUContact = (Label)e.Item.FindControl("lblBUContact");
                    HtmlTableCell tdBUContact = (HtmlTableCell)e.Item.FindControl("tdBUContact");
                    if (chkColumnList.Items.FindByValue("BUContact").Selected)
                    {

                        lblBUContact.Text = jobPosting.BUContactText;
                        tdBUContact.Visible = true;
                        lblBUContact.Visible = true;


                    }
                    else
                    {
                        lblBUContact.Visible = false;
                        tdBUContact.Visible = false;


                    }





                    //Label lblClientdourlyRate = (Label)e.Item.FindControl("lblClientdourlyRate");
                    //HtmlTableCell tdClientdourlyRate = (HtmlTableCell)e.Item.FindControl("tdClientdourlyRate");
                    //if (chkColumnList.Items.FindByValue("ClientHourlyRate") != null && chkColumnList.Items.FindByValue("ClientHourlyRate").Selected)
                    //{
                    //    lblClientdourlyRate.Text = BillRate(jobPosting);
                    //    tdClientdourlyRate.Visible = true;
                    //    lblClientdourlyRate.Visible = true;
                    //}
                    //else
                    //{
                    //    lblClientdourlyRate.Visible = false;
                    //    tdClientdourlyRate.Visible = false;
                    //}


                    //Label lblTeleCommunication = (Label)e.Item.FindControl("lblTeleCommunication");
                    //HtmlTableCell tdTeleCommunication = (HtmlTableCell)e.Item.FindControl("tdTeleCommunication");
                    //if (chkColumnList.Items.FindByValue("TeleCommunication").Selected)
                    //{
                    //    lblTeleCommunication.Text = (jobPosting.TeleCommunication ? "Yes" : "No");
                    //    tdTeleCommunication.Visible = true;
                    //    lblTeleCommunication.Visible = true;
                    //}
                    //else
                    //{
                    //    lblTeleCommunication.Visible = false;
                    //    tdTeleCommunication.Visible = false;
                    //}

                    Label lblCreatorId = (Label)e.Item.FindControl("lblCreatorId");
                    HtmlTableCell tdCreatorId = (HtmlTableCell)e.Item.FindControl("tdCreatorId");
                    if (chkColumnList.Items.FindByValue("CreatorId").Selected)
                    {
                        if (jobPosting.CreatorId > 0)
                        {
                            lblCreatorId.Text = MiscUtil.GetMemberNameById(jobPosting.CreatorId, Facade);
                        }
                        tdCreatorId.Visible = true;
                        lblCreatorId.Visible = true;
                    }
                    else
                    {
                        lblCreatorId.Visible = false;
                        tdCreatorId.Visible = false;
                    }


                    Label lblUpdateDate = (Label)e.Item.FindControl("lblUpdateDate");
                    HtmlTableCell tdUpdateDate = (HtmlTableCell)e.Item.FindControl("tdUpdateDate");
                    if (chkColumnList.Items.FindByValue("UpdateDate").Selected)
                    {
                        lblUpdateDate.Text = (MiscUtil.IsValidDate(jobPosting.UpdateDate) ? jobPosting.UpdateDate.ToShortDateString() : string.Empty);
                        tdUpdateDate.Visible = true;
                        lblUpdateDate.Visible = true;
                    }
                    else
                    {
                        tdUpdateDate.Visible = false;
                        lblUpdateDate.Visible = false;
                    }

                    Label lblJobCategory = (Label)e.Item.FindControl("lblJobCategory");
                    HtmlTableCell tdJobCategory = (HtmlTableCell)e.Item.FindControl("tdJobCategory");
                    if (chkColumnList.Items.FindByValue("JobCategory").Selected)
                    {

                        lblJobCategory.Text = jobPosting.JobCategoryText;
                        tdJobCategory.Visible = true;
                        lblJobCategory.Visible = true;
                    }
                    else
                    {
                        tdJobCategory.Visible = false;
                        lblJobCategory.Visible = false;
                    }


                    Label lblSkills = (Label)e.Item.FindControl("lblSkills");
                    HtmlTableCell tdSkills = (HtmlTableCell)e.Item.FindControl("tdSkills");
                    if (chkColumnList.Items.FindByValue("Skills").Selected)
                    {
                        lblSkills.Text = (MiscUtil.SplitSkillValues(jobPosting.JobSkillLookUpId, '!', Facade));
                        tdSkills.Visible = true;
                        lblSkills.Visible = true;
                    }
                    else
                    {
                        tdSkills.Visible = false;
                        lblSkills.Visible = false;
                    }

                    //Label lblMinimumqualifyingParameters = (Label)e.Item.FindControl("lblMinimumqualifyingParameters");
                    //HtmlTableCell tdMinimumqualifyingParameters = (HtmlTableCell)e.Item.FindControl("tdMinimumqualifyingParameters");
                    //if (chkColumnList.Items.FindByValue("MinimumQualifyingParameters").Selected)
                    //{

                    //    char[] delim = { ',' };
                    //    string[] MQP = jobPosting.MinimumQualifyingParameters.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                    //    string strMQP = "<ui>";
                    //    foreach (string s in MQP)
                    //    {
                    //        strMQP += "<li>" + s + "</li>";
                    //    }
                    //    strMQP += "</ui>";


                    //    lblMinimumqualifyingParameters.Text = strMQP;//(MiscUtil.SplitSkillValues(jobPosting.JobSkillLookUpId, '!', Facade));
                    //    tdMinimumqualifyingParameters.Visible = true;
                    //    lblMinimumqualifyingParameters.Visible = true;
                    //}
                    //else
                    //{
                    //    tdMinimumqualifyingParameters.Visible = false;
                    //    lblMinimumqualifyingParameters.Visible = false;
                    //}



                    //Label lblWorkSchedule = (Label)e.Item.FindControl("lblWorkSchedule");
                    //HtmlTableCell tdWorkSchedule = (HtmlTableCell)e.Item.FindControl("tdWorkSchedule");
                    //if (chkColumnList.Items.FindByValue("WorkSchedule").Selected)
                    //{
                    //    lblWorkSchedule.Text = WorkSchedule(jobPosting);
                    //    tdWorkSchedule.Visible = true;
                    //    lblWorkSchedule.Visible = true;
                    //}
                    //else
                    //{
                    //    tdWorkSchedule.Visible = false;
                    //    lblWorkSchedule.Visible = false;
                    //}

                    //Label lblSecurityClearance = (Label)e.Item.FindControl("lblSecurityClearance");
                    //HtmlTableCell tdSecurityClearance = (HtmlTableCell)e.Item.FindControl("tdSecurityClearance");
                    //if (chkColumnList.Items.FindByValue("SecurityClearance").Selected)
                    //{
                    //    lblSecurityClearance.Text = jobPosting.SecurityClearance ? "Yes" : "No";
                    //    tdSecurityClearance.Visible = true;
                    //    lblSecurityClearance.Visible = true;
                    //}
                    //else
                    //{
                    //    tdSecurityClearance.Visible = false;
                    //    lblSecurityClearance.Visible = false;
                    //}

                    //Label lblExpenses = (Label)e.Item.FindControl("lblExpenses");
                    //HtmlTableCell tdExpenses = (HtmlTableCell)e.Item.FindControl("tdExpenses");
                    //if (chkColumnList.Items.FindByValue("Expenses").Selected)
                    //{
                    //    lblExpenses.Text = jobPosting.IsExpensesPaid ? "Yes" : "No";
                    //    tdExpenses.Visible = true;
                    //    lblExpenses.Visible = true;
                    //}
                    //else
                    //{
                    //    tdExpenses.Visible = false;
                    //    lblExpenses.Visible = false;
                    //}

                    Label lblAdditionalNotes = (Label)e.Item.FindControl("lblAdditionalNotes");
                    HtmlTableCell tdAdditionalNotes = (HtmlTableCell)e.Item.FindControl("tdAdditionalNotes");
                    if (chkColumnList.Items.FindByValue("AdditionalNotes").Selected)
                    {
                        lblAdditionalNotes.Text = jobPosting.InternalNote;
                        tdAdditionalNotes.Visible = true;
                        lblAdditionalNotes.Visible = true;
                    }
                    else
                    {
                        tdAdditionalNotes.Visible = false;
                        lblAdditionalNotes.Visible = false;
                    }

                    Label lblAssignedRecruiters = (Label)e.Item.FindControl("lblAssignedRecruiters");
                    HtmlTableCell tdAssignedRecruiters = (HtmlTableCell)e.Item.FindControl("tdAssignedRecruiters");
                    if (chkColumnList.Items.FindByValue("AssignedRecruiters").Selected)
                    {
                        lblAssignedRecruiters.Text = AssignedRecruiters(jobPosting);
                        tdAssignedRecruiters.Visible = true;
                        lblAssignedRecruiters.Visible = true;
                    }
                    else
                    {
                        tdAssignedRecruiters.Visible = false;
                        lblAssignedRecruiters.Visible = false;
                    }
                    //Interview Count
                    Label lblScheduledInterviews = (Label)e.Item.FindControl("lblScheduledInterviews");
                    HtmlTableCell tdScheduledInterviews = (HtmlTableCell)e.Item.FindControl("tdScheduledInterviews");
                    if (chkColumnList.Items.FindByValue("ScheduledInterviews").Selected)
                    {
                        lblScheduledInterviews.Text = jobPosting.InterViewCount.ToString();
                        tdScheduledInterviews.Visible = true;
                        lblScheduledInterviews.Visible = true;
                    }
                    else
                    {
                        tdScheduledInterviews.Visible = false;
                        lblScheduledInterviews.Visible = false;
                    }

                    //SubmissionsCount
                    Label lblSubmissions = (Label)e.Item.FindControl("lblSubmissions");
                    HtmlTableCell tdSubmissions = (HtmlTableCell)e.Item.FindControl("tdSubmissions");
                    if (chkColumnList.Items.FindByValue("Submissions").Selected)
                    {
                        lblSubmissions.Text = jobPosting.SubmissionCount.ToString();
                        tdSubmissions.Visible = true;
                        lblSubmissions.Visible = true;
                    }
                    else
                    {
                        tdSubmissions.Visible = false;
                        lblSubmissions.Visible = false;
                    }
                    //Offers Count
                    Label lblOffers = (Label)e.Item.FindControl("lblOffers");
                    HtmlTableCell tdOffers = (HtmlTableCell)e.Item.FindControl("tdOffers");
                    if (chkColumnList.Items.FindByValue("Offers").Selected)
                    {
                        lblOffers.Text = jobPosting.OfferDetailsCount.ToString();
                        tdOffers.Visible = true;
                        lblOffers.Visible = true;
                    }
                    else
                    {
                        tdOffers.Visible = false;
                        lblOffers.Visible = false;
                    }
                    //Joined Count
                    Label lblJoined = (Label)e.Item.FindControl("lblJoined");
                    HtmlTableCell tdJoined = (HtmlTableCell)e.Item.FindControl("tdJoined");
                    if (chkColumnList.Items.FindByValue("Joined").Selected)
                    {
                        lblJoined.Text = jobPosting.JoiningDetailsCount.ToString();
                        tdJoined.Visible = true;
                        lblJoined.Visible = true;
                    }
                    else
                    {
                        tdJoined.Visible = false;
                        lblJoined.Visible = false;
                    }
                    //Interview Candidate Name
                    Label lblInterviewCandidate = (Label)e.Item.FindControl("lblInterviewCandidate");
                    HtmlTableCell tdInterviewCandidate = (HtmlTableCell)e.Item.FindControl("tdInterviewCandidate");
                    if (chkColumnList.Items.FindByValue("JPICN").Selected)
                    {
                        lblInterviewCandidate.Text = jobPosting.InterviewCandidateName.TrimEnd(',', ' ');
                        tdInterviewCandidate.Visible = true;
                        lblInterviewCandidate.Visible = true;
                    }
                    else
                    {
                        tdInterviewCandidate.Visible = false;
                        lblInterviewCandidate.Visible = false;
                    }
                    //Submitted Candidate Name
                    Label lblSubmittedCandidate = (Label)e.Item.FindControl("lblSubmittedCandidate");
                    HtmlTableCell tdSubmittedCandidate = (HtmlTableCell)e.Item.FindControl("tdSubmittedCandidate");
                    if (chkColumnList.Items.FindByValue("JPSCN").Selected)
                    {
                        lblSubmittedCandidate.Text = jobPosting.SubmittedCandidateName.TrimEnd(',', ' ');
                        tdSubmittedCandidate.Visible = true;
                        lblSubmittedCandidate.Visible = true;
                    }
                    else
                    {
                        tdSubmittedCandidate.Visible = false;
                        lblSubmittedCandidate.Visible = false;
                    }
                    //Offered Candidate Name
                    Label lblOfferedCandidate = (Label)e.Item.FindControl("lblOfferedCandidate");
                    HtmlTableCell tdOfferedCandidate = (HtmlTableCell)e.Item.FindControl("tdOfferedCandidate");
                    if (chkColumnList.Items.FindByValue("JPOCN").Selected)
                    {
                        lblOfferedCandidate.Text = jobPosting.OfferedCandidateName.TrimEnd(',', ' ');
                        tdOfferedCandidate.Visible = true;
                        lblOfferedCandidate.Visible = true;
                    }
                    else
                    {
                        tdOfferedCandidate.Visible = false;
                        lblOfferedCandidate.Visible = false;
                    }
                    //Joined Candidate Name
                    Label lblJoinedCandidate = (Label)e.Item.FindControl("lblJoinedCandidate");
                    HtmlTableCell tdJoinedCandidate = (HtmlTableCell)e.Item.FindControl("tdJoinedCandidate");
                    if (chkColumnList.Items.FindByValue("JPJCN").Selected)
                    {
                        lblJoinedCandidate.Text = jobPosting.JoinedCandidateName.TrimEnd(',', ' ');
                        tdJoinedCandidate.Visible = true;
                        lblJoinedCandidate.Visible = true;
                    }
                    else
                    {
                        tdJoinedCandidate.Visible = false;
                        lblJoinedCandidate.Visible = false;
                    }
                    //Description
                    Label lblDescription = (Label)e.Item.FindControl("lblDescription");
                    HtmlTableCell tdDescription = (HtmlTableCell)e.Item.FindControl("tdDescription");
                    if (chkColumnList.Items.FindByValue("JobDesc").Selected)
                    {
                        lblDescription.Text = jobPosting.JobDescription;
                        tdDescription.Visible = true;
                        lblDescription.Visible = true;
                    }
                    else
                    {
                        tdDescription.Visible = false;
                        lblDescription.Visible = false;
                    }
                    //Notes
                    Label lblNotes = (Label)e.Item.FindControl("lblNotes");
                    HtmlTableCell tdNotes = (HtmlTableCell)e.Item.FindControl("tdNotes");
                    if (chkColumnList.Items.FindByValue("Jobpostnotes").Selected)
                    {
                        lblNotes.Text = jobPosting.JobPostNotes;
                        tdNotes.Visible = true;
                        lblNotes.Visible = true;
                    }
                    else
                    {
                        tdNotes.Visible = false;
                        lblNotes.Visible = false;
                    }

                    //Requisition Type
                    Label lblRequisitionType = (Label)e.Item.FindControl("lblRequisitionType");
                    HtmlTableCell tdRequisitionType = (HtmlTableCell)e.Item.FindControl("tdRequisitionType");
                    if (chkColumnList.Items.FindByValue("RequisitionType").Selected)
                    {
                        lblRequisitionType.Text = jobPosting.RequisitionType;
                        tdRequisitionType.Visible = true;
                        lblRequisitionType.Visible = true;
                    }
                    else
                    {
                        tdRequisitionType.Visible = false;
                        lblRequisitionType.Visible = false;
                    }

                }

            }

        }

        string JobDurationMonth(JobPosting jobPosting)
        {
            if (jobPosting.JobDurationMonth.Trim() == string.Empty)
            {
                return "";
            }
            else
            {
                if (Convert.ToInt32(jobPosting.JobDurationMonth) > 0)
                {
                    GenericLookup gklookup = Facade.GetGenericLookupById(Convert.ToInt32(jobPosting.JobDurationMonth));
                    if (gklookup != null)
                    {
                        return gklookup.Name;
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                    return "";
            }
        }

        string StartDate(JobPosting jobPosting)
        {
            if (System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern == "dd/MM/yyyy")
            {
                return jobPosting.StartDate.ToString() == "" || jobPosting.StartDate.ToString().Trim() == "ASAP" ? jobPosting.StartDate.ToString() : (jobPosting.StartDate.ToString().Split('/')[1] + "/" + jobPosting.StartDate.ToString().Split('/')[0] + "/" + jobPosting.StartDate.ToString().Split('/')[2]);
            }
            else
            {
                return jobPosting.StartDate.Trim() != string.Empty ? (jobPosting.StartDate.Trim() != "ASAP" ? Convert.ToDateTime(jobPosting.StartDate).ToShortDateString() : "ASAP") : " ";
            }
        }

        string Category(JobPosting jobPosting)
        {
            if (jobPosting.JobCategoryLookupId > 0)
            {
                Category ctg = Facade.GetCategoryById(jobPosting.JobCategoryLookupId);
                if (ctg != null)
                    return ctg.Name;
                else
                    return "";
            }
            else
                return "";
        }

        string WorkSchedule(JobPosting jobPosting)
        {
            GenericLookup genlkp = Facade.GetGenericLookupById(jobPosting.WorkScheduleLookupId);
            if (genlkp != null)
                return genlkp.Name;
            else
                return "";
        }

        string AssignedRecruiters(JobPosting jobPosting)
        {
            IList<JobPostingHiringTeam> teamMembers = Facade.GetAllJobPostingHiringTeamByJobPostingId(jobPosting.Id);
            StringBuilder sb = new StringBuilder();
            if (teamMembers != null)
            {
                foreach (JobPostingHiringTeam item in teamMembers)
                {
                    Member mem = Facade.GetMemberById(item.MemberId);
                    if (mem != null)
                        sb.Append(mem.FirstName + " " + mem.MiddleName + " " + mem.LastName).Append(", ");
                }
                if (sb.Length > 0)
                {
                    int lastindex = sb.ToString().LastIndexOf(',');
                    return sb.ToString().Substring(0, lastindex);
                }
                else
                {
                    return "";
                }
            }
            else
                return "";
        }

        #endregion

        #region Pager Events

        private void ResetPager()
        {
            DataPager pager = this.lsvJobPosting.FindControl("pager") as DataPager;

            if (pager != null)
            {
                pager.SetPageProperties(0, pager.MaximumRows, true);
            }
        }
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvJobPosting.FindControl(hdnSortColumn.Value);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                int d = 0;
                Int32.TryParse(lnk.CommandArgument, out d);
                if (d > 0 || lnk.CommandArgument == "[JPTAT].[TurnArroundTime]")
                {
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Descending" : "Ascending"));

                }
                else
                {
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
                }

            }
            catch
            {
            }
        }
        #endregion
    }
}