﻿
using System;
using System.Web.Security;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using TPS360.Common;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Configuration;
using System.Web;

namespace TPS360.Web.UI.Reports
{



    public partial class ProductivityReport:BasePage 
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            if (ReportType == ContextConstants.PRODUCTIVITY_REPORT)
            {
                uncProductivityReport.IsMyProductivityReport = false;
                uncProductivityReport.IsTeamProductivityReport = false;
                Page.Title = lblSubHeader.Text = "Productivity Report";
            }
            else if (ReportType == ContextConstants.MY_PRODUCTIVITY_REPORT)
            {
                uncProductivityReport.IsMyProductivityReport = true;
                uncProductivityReport.IsTeamProductivityReport = false;
                Page.Title = lblSubHeader.Text = "My Productivity Report";
            }
            else if (ReportType == ContextConstants.TEAM_PRODUCTIVITY_REPORT)
            {
                uncProductivityReport.IsMyProductivityReport = false;
                uncProductivityReport.IsTeamProductivityReport = true;
                Page.Title = lblSubHeader.Text = "Team Productivity Report";
            }

        }

    }
}