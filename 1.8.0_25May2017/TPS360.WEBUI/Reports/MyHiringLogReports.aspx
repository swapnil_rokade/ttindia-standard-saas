﻿
<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="MyHiringLogReports.aspx.cs"
    Inherits="TPS360.Web.UI.Reports.MyHiringLogReports" Title="My Hiring Log Report" EnableEventValidation="false"
    EnableViewStateMac="false" %>
<%@ Register Src="~/Controls/HiringLogReports.ascx" TagName="HiringLogReport" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    My Hiring Log Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
  <ucl:HiringLogReport IsMyHiringLogReport="true" IsTeamHiringReport="false" ID="ucnHiringLogReport"  runat="server" />  
</asp:Content>