﻿<%---------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateReports.aspx
    Description: This is the page used to display the candidate reports in excel,word,Pdf formats.
    Created By: 
    Created On:
    Modification Log: 
    --------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    --------------------------------------------------------------------------------------------------------------------------------------
    0.1                 02-Jun-2009         Sandeesh            Defect id: 10547 ; Changes made to enable sorting in the 'List of Candidates' report
    0.2                 17-Jul-2009         Nagarathna V.B      Defect Id:10849: Added triggers.
    0.3                 Jan-19-2010         Gopala Swamy J      Defect Id:12901;Changed Style attribute and put events called "OnTextChanged"
    0.4                 Apr-06-2010         Ganapati Bhat       Defect Id:12497 Set "EnableViewstate=true" to fire ddlCountry_SelectedIndexChanged()    
    ------------------------------------------------------------------------------------------------------------------------------------------- 
--%>

<%@ Page EnableEventValidation="false" Language="C#" MasterPageFile="~/Default.master"
    AutoEventWireup="true" CodeFile="MyProductivityReport.aspx.cs" Inherits="TPS360.Web.UI.Reports.MyProductivityReport"
    MaintainScrollPositionOnPostback="true" Title="My Productivity Report" EnableViewStateMac="false" %>

<%@ Register Src="~/Controls/ProductivityReport.ascx" TagName="MyProductivityReport" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
   My Productivity Report
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
<ucl:MyProductivityReport ID="uncMyProductivityReport" runat="server" IsMyProductivityReport="true" IsTeamProductivityReport="false" />
</asp:Content>
