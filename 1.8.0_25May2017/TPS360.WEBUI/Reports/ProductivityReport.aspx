﻿<%@ Page EnableEventValidation="false" Language="C#" MasterPageFile="~/Default.master"
    AutoEventWireup="true" CodeFile="ProductivityReport.aspx.cs" Inherits="TPS360.Web.UI.Reports.ProductivityReport"
    MaintainScrollPositionOnPostback="true" Title="Productivity Report" EnableViewStateMac="false" %>

<%@ Register Src="~/Controls/ProductivityReport.ascx" TagName="ProductivityReport" TagPrefix="ucl" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    <asp:Label ID="lblSubHeader" runat="server" ></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
<ucl:ProductivityReport ID="uncProductivityReport" IsMyProductivityReport="false" IsTeamProductivityReport="false" runat="server" />

</asp:Content>
