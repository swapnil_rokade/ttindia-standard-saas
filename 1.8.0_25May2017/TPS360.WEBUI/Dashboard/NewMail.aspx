﻿<%@ Page UICulture="auto" Culture="auto" Title ="New Email" language="c#" Inherits="TPS360.Web.UI.NewMail" CodeFile="NewMail.aspx.cs" MasterPageFile="~/Popup.master" AutoEventWireup ="true" ValidateRequest="false" %>
<%@ Register TagPrefix ="ucl" Src ="~/Controls/EmailEditor.ascx" TagName ="NewMail"  %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>
<%@ Register Assembly ="AjaxControlToolkit" Namespace ="AjaxControlToolkit" TagPrefix ="ajaxToolkit" %>
<asp:content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
		<link href= "../Style/AppointmentDialog.css" type="text/css" rel="stylesheet"/>		
		<link type="text/css" href="~/css.axd?fileSet=FileSet_CSS_Style2" rel="Stylesheet" />
		<link type="text/css" href="~/css.axd?fileSet=FileSet_CSS_Style1" rel="Stylesheet" />
<script src ="../Scripts/jsUpdateProgress.js" type ="text/javascript" >
			</script>
<script type ="text/javascript" language ="javascript" >
         
 var ModalProgress ='<%= Modal.ClientID %>';

</script> 
<asp:UpdatePanel ID ="uppanel" runat ="server" ><ContentTemplate >
		<div class ="FormBackground" style ="height:670px ;WIDTH: 770; overflow :auto  ">
		<%--<table style="Z-INDEX: 101; LEFT: 0px; WIDTH: 770; TOP: 0px; HEIGHT: 100%" cellSpacing="0" cellPadding="0">
				<tr>
					<td>--%>
					<%--<igtab:UltraWebTab id="UltraWebTab1" runat="server" DummyTargetUrl=" " Height="100%" width="770px">
							<BorderDetails ColorTop="White" WidthTop="1px" StyleTop="Solid"></BorderDetails>
							    <DefaultTabStyle Width="100px" Height="25px" BorderColor="White" CssClass="Fonts">
							    </DefaultTabStyle>
							<Tabs>
							    <igtab:Tab Key="NewEmail" Text="New Email"  >
								    <Style CssClass="Fonts BackgroundTab" ></Style>
								    <ContentPane UserControlUrl=  "~/Controls/EmailEditor.ascx" BorderStyle="None"    ></ContentPane>
								
								    <SelectedStyle Font-Bold="True"></SelectedStyle>
							    </igtab:Tab>
							</Tabs>
						</igtab:UltraWebTab>--%>
		<ig:WebTab ID="UltraWebTab1" runat="server" Height="100%" width="770px">
            <Tabs>
                <ig:ContentTabItem runat="server" Text="New Email">
                   <TabCssClasses CssClass="Fonts BackgroundTab" />
                   <Template>
                  
                   <%--<ig:ContentPane UserControlUrl="~/Controls/EmailEditor.ascx"></ig:ContentPane>--%>
                   
                   <ucl:NewMail ID="uclNewMail" runat="server" />
                   </Template>
                </ig:ContentTabItem>
            </Tabs>
        </ig:WebTab>
				    <A class="tbButton" id="delete" title="Delete" href="#" name="cbButton"></A>
		<%--		    </td>
				</tr>
    	</table>--%>

	    </div>
	    <asp:Panel ID="pnlmodal" runat="server" style="display: none;">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                        <img src ="../Images/AjaxLoading.gif"/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID ="Modal" runat ="server" TargetControlID ="pnlmodal" PopupControlID ="pnlmodal"
             BackgroundCssClass ="divModalBackground" ></ajaxToolkit:ModalPopupExtender>
             </ContentTemplate>
             </asp:UpdatePanel>
</asp:content> 