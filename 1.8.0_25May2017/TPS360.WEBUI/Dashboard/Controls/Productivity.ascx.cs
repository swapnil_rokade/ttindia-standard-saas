﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;



public partial class Productivity : BaseControl, IWidget
{
    #region Properties
    private bool _IsAccessToEmployee = true;
    private static string UrlForEmployee = string.Empty;
    private static int IdForSitemap = 0;
    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }
    public static DataTable ProductivityWidget =new DataTable();

    #endregion

    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {
    }
    void IWidget.Closed()
    {
    }

    #endregion

    #region methods

    #endregion
    
    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField hdnProductivity = (HiddenField)uclProductivity.FindControl("hdnProductivity");
        hdnProductivity.Value = "Master";
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }


    #endregion
}
   

