﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PendingJoiners.ascx.cs" Inherits="PendingJoiners" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pagers" TagPrefix="ucll" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false" />
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false" />
<asp:Panel ID="widgetBody" runat="server">
    <asp:UpdatePanel ID="upPendingJoiners" runat="server">
        <ContentTemplate>
            <asp:ObjectDataSource ID="odsPendingJoiners" runat="server" SelectMethod="GetPaged"
                TypeName="TPS360.Web.UI.MemberPendingJoinersDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression" EnableViewState="true">
                <SelectParameters>
                    <asp:Parameter Name="MemberId" DefaultValue="0" Type=Int32  />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ListView ID="lsvPendingJoiners" runat="server" DataKeyNames="Id" DataSourceID="odsPendingJoiners"
                OnItemDataBound="lsvPendingJoiners_ItemDataBound" EnableViewState="true" OnPreRender="lsvPendingJoiners_PreRender"
                OnItemCommand="lsvPendingJoiners_ItemCommand">
                <LayoutTemplate>
                    <p style="color: #e62e00"><b>Note:</b> Candidates highlighted in red are still pending after E-DOJ</p>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th>
                            
                                <asp:LinkButton ID="btnCandidateID" runat="server" ToolTip="Candidate ID" CommandName="Sort"
                                    CommandArgument="[C].[ID]" Text="Candidate ID #" />
                            </th>
                            <th>
                                <asp:LinkButton  ID="btnCandidateName" runat="server" ToolTip="Sort By Candidate Name"
                                    CommandName="Sort" CommandArgument="[C].[CandidateName]" Text="CandidateName" />
                            </th>
                            
                            <th>
                                <asp:LinkButton ID="btnReqCode" runat="server" ToolTip="Sort By Req Code"
                                    CommandName="Sort" CommandArgument="[J].[JobPostingCode]" Text="Req Code #" />
                            </th>
                            <th>
                                <asp:LinkButton ID="btnJobTitle" runat="server" ToolTip="Sort By Job Title" CommandName="Sort"
                                    CommandArgument="[J].[JobTitle]" Text="Job Title" />
                            </th>
                            <th>
                                <asp:LinkButton ID="btnEJOJ" runat="server" ToolTip="Sort By E-DOJ"
                                    CommandName="Sort" CommandArgument="MHD.JoiningDate" Text="E-DOJ" />
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkActiveRecruiter" runat="server" ToolTip="Sort By Active Recruiter"
                                    CommandName="Sort" CommandArgument="MA.FirstName" Text="Active Recruiter" />
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="6">
                                <ucll:Pagers ID="pagerControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                        style="width: 100%; margin: 0px 0px;">
                        <tr>
                            <td>
                                No Pending Joiners.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:Label ID="lblCandidateID" runat="server" />
                        </td>
                        <td>
                            <asp:HyperLink ID="lnkCandidateName" runat="server"  ></asp:HyperLink>
                        </td>
                        <td>
                        <asp:Label ID="lblJobPostingCode" runat="server"  />
                            
                        </td>
                        <td>
                        <asp:HyperLink  ID ="lnkRequisition" runat ="server" ></asp:HyperLink>
                            
                        </td>
                        <td>
                            <asp:Label ID="lblEDateOfJoining" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblActiveRecruiter" runat="server" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
