﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Productivity.aspx
    Description: DashBoard / MyProductivity Page
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Dec-14-2009          Basavaraj A        Defect id:11991 Cosmetic Changes. Blank space is displayed towards left side and bottom of the grid
 -------------------------------------------------------------------------------------------------------------------------------------------       

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyProductivity.ascx.cs"
    Inherits="Dashboard_Controls_MyProductivity" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Dashboard/Controls/cntrlProductivity.ascx" TagName="Productivity"
    TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<style type="text/css">
    .AspNet-GridView-Pagination
    {
        padding: 0;
        margin: 0;
        border-bottom: 0;
        clear: both;
        letter-spacing: normal;
        border-bottom-style: none;
    }
</style>
<asp:Panel ID="widgetBody" runat="server" EnableViewState="true">
    <ucl:Productivity ID="uclProductivity" runat="server" />
</asp:Panel>
