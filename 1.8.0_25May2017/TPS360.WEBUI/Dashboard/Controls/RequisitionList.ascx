<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionList.ascx.cs"
    Inherits="RequisitionList" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
<asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
<asp:TextBox ID="txtSortColumn1" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder1" runat="server" Visible="false"></asp:TextBox>
<asp:Panel ID="widgetBody" runat="server">
    <%--<asp:UpdatePanel ID="widgetBody" runat="server" EnableViewState ="true" >
<ContentTemplate >--%>
    <div class="CommonDashboardContent">
        <div style="overflow: auto;">
            <asp:ObjectDataSource ID="odsMyRequisitionList" runat="server" SelectMethod="GetPaged"
                TypeName="TPS360.Web.UI.JobPostingDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:Parameter Name="memberId" DefaultValue="0" />
                    <asp:Parameter Name="IsTemplate" DefaultValue="False" Type="Boolean" />
                    <asp:Parameter Name="IsDashBoard" DefaultValue="True" Type="Boolean" />
                    <asp:Parameter Name="SortOrder" DefaultValue="asc" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ListView ID="lsvRequisition" runat="server" DataSourceID="odsMyRequisitionList"
                DataKeyNames="Id" EnableViewState="true" OnItemDataBound="lsvRequisition_ItemDataBound"
                OnItemCommand="lsvRequisition_ItemCommand" OnPreRender="lsvRequisition_PreRender">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr id="trJobTitle" runat="server">
                            <th style="min-width: 80px; white-space: nowrap;" enableviewstate="false">
                                <asp:LinkButton ID="btnJobTitle" runat="server" ToolTip="Sort By Job Title" CommandName="Sort"
                                    CommandArgument="[J].[JobTitle]" Text="Job Title" />
                            </th>
                            <th runat="server" id="thCode" visible="true" style="min-width: 60px; white-space: nowrap;" enableviewstate="false">
                                <asp:LinkButton ID="btnCode" runat="server" ToolTip="Sort By Job Code" CommandName="Sort"
                                    CommandArgument="[J].[JobPostingCode]" Text="Code" />
                            </th>
                             <th id="thClientJobID" runat="server" visible="false" style="min-width: 60px; white-space: nowrap;" enableviewstate="false">
                                <asp:LinkButton ID="btnClientJobID" runat="server" ToolTip="Sort By Client Job ID" CommandName="Sort"
                                    CommandArgument="[J].[JobPostingCode]" Text="Client Job ID" />
                            </th>
                            
                            <th style="min-width: 75px; white-space: nowrap; min-width: 110px" enableviewstate="false">
                                <asp:LinkButton ID="btnCurrentStatus" runat="server" ToolTip="Sort By Current Status"
                                    CommandName="Sort" CommandArgument="[GL].[Name]" Text="Current Status" />
                            </th>
                            <th style="min-width: 120px; white-space: nowrap;" enableviewstate="false">
                                <asp:LinkButton ID="btnDatePublished" runat="server" ToolTip="Sort By Date Published"
                                    CommandName="Sort" CommandArgument="[J].[PostedDate]" Text="Date Published   " />
                            </th>
                            <th style="min-width: 100px; white-space: nowrap; min-width: 120px" enableviewstate="false">
                                <asp:LinkButton ID="btnTotalCandidates" runat="server" ToolTip="Sort By Total Candidates"
                                    CommandName="Sort" CommandArgument="[1]" Text="Total Candidates" />
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="5">
                                <ucl:Pager ID="pagerControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                        style="width: 100%; margin: 0px 0px;">
                        <tr>
                            <td>
                                No requisitions available.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:HyperLink ID="lblJobTitle" runat="server" ></asp:HyperLink>
                        </td>
                        <td runat="server" id="tdJobCode" visible="true">
                            <asp:Label ID="lblJobCode" runat="server" />
                        </td>
                        <td runat="server" id="tdClientJobID"  visible="false">
                            <asp:Label ID="lblClientJobID" runat="server" />
                        </td>
                        
                        <td>
                            <asp:Label ID="lblJobstatus" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblJobPostedDate" runat="server" />
                        </td>
                        <td>
                            <asp:HyperLink ID="hlnkNoOfCandidates" runat="server" ></asp:HyperLink>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
</asp:Panel>
<%--</ContentTemplate>
</asp:UpdatePanel> --%>