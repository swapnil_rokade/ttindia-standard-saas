﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing; 

public partial class CurrentSessions : TPS360.Web.UI.BaseControl, IWidget
{
    #region Member Variables
    private bool _IsAccessToEmployee = true;
    private  string UrlForEmployee = string.Empty;
    private  int IdForSitemap = 0;
    #endregion

    #region Properties

    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    #endregion
    #region Page Events
    private void setParameters()
    {
        odsCurrentSessions.SelectParameters.Clear();
        odsCurrentSessions.SelectParameters.Add("SortOrder", txtSortOrder.Text);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        setParameters();
      
      
        CustomSiteMap CustomMap = new CustomSiteMap();
        CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(360, CurrentMember.Id);
        if (CustomMap == null) _IsAccessToEmployee = false;
        else
        {
            IdForSitemap = CustomMap.Id;
            UrlForEmployee = "~/" + CustomMap.Url.ToString();
        }
        
        (this as IWidget).HideSettings();
      
        
        if (!IsPostBack)
        {
            GetWidgetData();
            txtSortColumn.Text = "btnEmployeeName";
            txtSortOrder.Text = "ASC";
            PlaceUpDownArrow();
            try
            {
                Bindchart();
            }
            catch { }
            try
            {
            DataTable dt = GetData();
            LoadChartData(dt);
            }
            catch { }
            try
            {
                DataTable dtInterview = GetDataInterview();
                LoadChartDataInterview(dtInterview);
            }
            catch { }
            try
            {
                DataTable dtTimetoHire = GetDataTimetoHire();
                LoadChartDataTimetoHire(dtTimetoHire);
            }
            catch { }
            try
            {
                DataTable dtJoinedToRejectedByBusiness = GetDataJoinedToRejectedByBusiness();
                LoadChartDataJoinedToRejectedByBusiness(dtJoinedToRejectedByBusiness);
            }
            catch { }
            try
            {
                DataTable dtCandidateForSkill = GetDataCandidateForSkill();
                LoadChartDataCandidateForSkill(dtCandidateForSkill);
            }
            catch { }
            //DataTable dtRequisitionsCount = GetDataRequisitionsCount();
            //LoadChartDataRequisitionsCount(dtRequisitionsCount);
        }
        string pagesize = "";
        pagesize = (Request.Cookies["DashboardCurrentSessionsRowPerPage"] == null ? "" : Request.Cookies["DashboardCurrentSessionsRowPerPage"].Value); ;
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvCurrentSessions.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize); 
        }
    }

    private void LoadChartData(DataTable initialDataSource)
    {
        for (int i = 1; i < initialDataSource.Columns.Count; i++)
        {
            Series series = new Series();
            foreach (DataRow dr in initialDataSource.Rows)
            {
                series.ChartType = SeriesChartType.RangeColumn;
                Chart1.ChartAreas["ChartArea1"].AxisX.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
                Chart1.ChartAreas["ChartArea1"].AxisY.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
                Chart1.ChartAreas["ChartArea1"].AxisX.Title = "Recuiter Name";
                Chart1.ChartAreas["ChartArea1"].AxisY.Title = "No Of Candidate";

                //Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;  
                try
                {
                    int y = (int)dr[i];
                    series.Points.AddXY(dr["Data"].ToString(), y);
                    series.IsValueShownAsLabel = true;
                }
                catch { }
            }
            Chart1.Series.Add(series);
        }
    }

    private void LoadChartDataInterview(DataTable initialDataSource)
    {
        for (int i = 1; i < initialDataSource.Columns.Count; i++)
        {
            Series series = new Series();
            foreach (DataRow dr in initialDataSource.Rows)
            {
                series.ChartType = SeriesChartType.StackedColumn100;
                Chart2.ChartAreas["ChartArea2"].AxisX.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
                Chart2.ChartAreas["ChartArea2"].AxisY.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
                Chart2.ChartAreas["ChartArea2"].AxisX.Title = "Recuiter Name";
                Chart2.ChartAreas["ChartArea2"].AxisY.Title = "Percent(%)";
                Chart2.ChartAreas["ChartArea2"].Area3DStyle.Enable3D = true;  
                try
                {
                    int y = (int)dr[i];
                    series.Points.AddXY(dr["Data"].ToString(), y);
                    series.IsValueShownAsLabel = true;
                }
                catch { }
            }
            Chart2.Series.Add(series);
        }
    }

    private void LoadChartDataTimetoHire(DataTable initialDataSource)
    {
        for (int i = 1; i < initialDataSource.Columns.Count; i++)
        {
            Series series = new Series();
            foreach (DataRow dr in initialDataSource.Rows)
            {
                series.ChartType = SeriesChartType.RangeBar;
                //Chart3.ChartAreas["ChartArea3"].Area3DStyle.Enable3D = true;  
                Chart3.ChartAreas["ChartArea3"].AxisX.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
                Chart3.ChartAreas["ChartArea3"].AxisY.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
                Chart3.ChartAreas["ChartArea3"].AxisY.Title = "No Of Days";
                Chart3.ChartAreas["ChartArea3"].AxisX.Title = "Job Title";
                try
                {
                    int y = (int)dr[i];
                    series.Points.AddXY(dr["Data"].ToString(), y);
                    series.IsValueShownAsLabel = true;
                }
                catch { }
            }
            Chart3.Series.Add(series);
        }
    }

    private void LoadChartDataJoinedToRejectedByBusiness(DataTable initialDataSource)
    {
        for (int i = 1; i < initialDataSource.Columns.Count; i++)
        {
            Series series = new Series();
            foreach (DataRow dr in initialDataSource.Rows)
            {
                series.ChartType = SeriesChartType.RangeColumn;
                //Chart4.ChartAreas["ChartArea4"].Area3DStyle.Enable3D = true;  
                Chart4.ChartAreas["ChartArea4"].AxisX.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
                Chart4.ChartAreas["ChartArea4"].AxisY.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
                Chart4.ChartAreas["ChartArea4"].AxisY.Title = "No Of Candidate";
                Chart4.ChartAreas["ChartArea4"].AxisX.Title = "BU Name";
                try
                {
                    int y = (int)dr[i];
                    series.Points.AddXY(dr["Data"].ToString(), y);
                    series.IsValueShownAsLabel = true;
                }
                catch { }
            }
            Chart4.Series.Add(series);
        }
    }

    private void LoadChartDataCandidateForSkill(DataTable initialDataSource)
    {
        for (int i = 1; i < initialDataSource.Columns.Count; i++)
        {
            Series series = new Series();
            foreach (DataRow dr in initialDataSource.Rows)
            {
                series.ChartType = SeriesChartType.Pie;
                Chart5.ChartAreas["ChartArea5"].Area3DStyle.Enable3D = true;  
                //Chart5.ChartAreas["ChartArea5"].AxisX.LabelStyle.TruncatedLabels = false;
               
                try
                {
                    int y = (int)dr[i];
                    series.Points.AddXY(dr["Data"].ToString(), y);                   
                    series.IsValueShownAsLabel = true;

                    //Chart5.Legends.Add("Legend1");                 
                    //Chart5.Legends[0].Enabled = true;

                   
                }
                catch { }
            }
            Chart5.Series.Add(series);

            //Chart5.Series[0].Label = "#PERCENT{P2}";
            Chart5.Legends.Add("Legend1");
            Chart5.Legends[0].Enabled = true;
            Chart5.Series[0].Label = "#PERCENT{P2}";
            Chart5.Series[0].LegendText = "#VALX";
            for (int i1 = 0; i1 < 10; i1++)
            {
                Chart5.Series[0].Points[i1]["Exploded"] = "true";
            }

        }
    }

    private void LoadChartDataRequisitionsCount(DataTable initialDataSource)
    {
        for (int i = 1; i < initialDataSource.Columns.Count; i++)
        {
            Series series = new Series();
            foreach (DataRow dr in initialDataSource.Rows)
            {
                series.ChartType = SeriesChartType.Line;
                Chart6.ChartAreas["ChartArea6"].AxisX.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
                Chart6.ChartAreas["ChartArea6"].AxisY.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
                Chart6.ChartAreas["ChartArea6"].AxisY.Title = "No Of Requisitions";
                Chart6.ChartAreas["ChartArea6"].AxisX.Title = "Month/Year";

                try
                {
                    int y = (int)dr[i];
                    series.Points.AddXY(dr["Data"].ToString(), y);
                    series.IsValueShownAsLabel = true;

                }
                catch { }
            }
            Chart6.Series.Add(series);

        }
    }


    private DataTable GetData()
    {

        //IList<CompanyDocument> ChartData = Facade.GetAllCompanyDocument();
        IList<EmployeeProductivity> ChartData = Facade.GetEmployeeProductivityDetails();
        DataTable dt = new DataTable();
        dt.Columns.Add("Data", Type.GetType("System.String"));
        dt.Columns.Add("Value1", Type.GetType("System.Int32"));
        dt.Columns.Add("Value2", Type.GetType("System.Int32"));
        dt.Columns.Add("Value3", Type.GetType("System.Int32"));
        dt.Columns.Add("Value4", Type.GetType("System.Int32"));
     //   dt.Columns.Add("Value3", Type.GetType("System.Int32"));

        for (int i = 0; i < ChartData.Count; i++)
        {
            if (i == 5)
            {
                return dt;
            }
            else
            {
                DataRow dr1 = dt.NewRow();
                dr1["Data"] = ChartData[i].EmployeeName;
                dr1["Value1"] = ChartData[i].CandidateSourcedCount;
                dr1["Value2"] = ChartData[i].OfferCount;
                dr1["Value3"] = ChartData[i].OfferRejectedCount;
                dr1["Value4"] = ChartData[i].JoinedCount;
                // dr1["Value3"] = ChartData[i].Description;
                dt.Rows.Add(dr1);
            }
        }
        //DataRow dr2 = dt.NewRow();
        //dr2["Data"] = "series2";
        //dr2["Value1"] = 62;
        //dr2["Value2"] = 10;
        //dr2["Value3"] = 89;
        //dt.Rows.Add(dr2);
        //DataRow dr3 = dt.NewRow();
        //dr3["Data"] = "series3";
        //dr3["Value1"] = 19;
        //dr3["Value2"] = 23;
        //dr3["Value3"] = 78;
        //dt.Rows.Add(dr3);
        return dt;
    }

    private DataTable GetDataInterview()
    {

        //IList<CompanyDocument> ChartData = Facade.GetAllCompanyDocument();
        IList<EmployeeProductivity> ChartData = Facade.GetEmployeePresenttoInterviewRatio();
        DataTable dtInterview = new DataTable();
        dtInterview.Columns.Add("Data", Type.GetType("System.String"));
        dtInterview.Columns.Add("Value1", Type.GetType("System.Int32"));
        dtInterview.Columns.Add("Value2", Type.GetType("System.Int32"));
        dtInterview.Columns.Add("Value3", Type.GetType("System.Int32"));
        //dtInterview.Columns.Add("Value4", Type.GetType("System.Int32"));
        //   dt.Columns.Add("Value3", Type.GetType("System.Int32"));

        for (int i = 0; i < ChartData.Count; i++)
        {
            if (i == 5)
            {
                return dtInterview;
            }
            else
            {
                DataRow dr1 = dtInterview.NewRow();
                dr1["Data"] = ChartData[i].EmployeeName;
                dr1["Value1"] = ChartData[i].InterviewCompleted;
                dr1["Value2"] = ChartData[i].InterviewNoShow;
                dr1["Value3"] = ChartData[i].InterviewReschedule;
                //dr1["Value4"] = ChartData[i].JoinedCount;
                // dr1["Value3"] = ChartData[i].Description;
                dtInterview.Rows.Add(dr1);
            }
        }
        return dtInterview;
    }

    private DataTable GetDataTimetoHire()
    {

        //IList<CompanyDocument> ChartData = Facade.GetAllCompanyDocument();
        IList<EmployeeProductivity> ChartData = Facade.GetEmployeeTimetoHire();
        DataTable dtTimetoHire = new DataTable();
        dtTimetoHire.Columns.Add("Data", Type.GetType("System.String"));
        dtTimetoHire.Columns.Add("Value1", Type.GetType("System.Int32"));
        dtTimetoHire.Columns.Add("Value2", Type.GetType("System.Int32"));
     
        for (int i = 0; i < ChartData.Count; i++)
        {
            if (i == 5)
            {
                return dtTimetoHire;
            }
            else
            {
                DataRow dr1 = dtTimetoHire.NewRow();
                dr1["Data"] = ChartData[i].EmployeeName;
                dr1["Value1"] = ChartData[i].ExpectedTimeToFill;
                try
                {
                    if (ChartData[i].NoOfOpeningsJoinedGraph <= ChartData[i].NoOfCandidateJoinedGraph)
                    {
                        double ActualTime = (ChartData[i].UpdateDateGraph - ChartData[i].OpenDateGraph).TotalDays;
                        dr1["Value2"] = ActualTime;
                    }
                    else
                    {
                        double ActualTimeNew = (System.DateTime.Now - ChartData[i].OpenDateGraph).TotalDays;
                        dr1["Value2"] = ActualTimeNew;
                        //dr1["Value2"] = ChartData[i].ActualTimeToFill;
                    }
                    //dr1["Value2"] = ChartData[i].ActualTimeToFill;
                }
                catch { }
                dtTimetoHire.Rows.Add(dr1);
            }
        }
        return dtTimetoHire;
    }
    private DataTable GetDataJoinedToRejectedByBusiness()
    {
        //IList<CompanyDocument> ChartData = Facade.GetAllCompanyDocument();
        IList<EmployeeProductivity> ChartData = Facade.GetJoinedToRejectedByBusiness();
        DataTable dtTimetoHire = new DataTable();
        dtTimetoHire.Columns.Add("Data", Type.GetType("System.String"));
        dtTimetoHire.Columns.Add("Value1", Type.GetType("System.Int32"));
        dtTimetoHire.Columns.Add("Value2", Type.GetType("System.Int32"));
        if (ChartData != null)
        {
            for (int i = 0; i < ChartData.Count; i++)
            {
                if (i == 5)
                {
                    return dtTimetoHire;
                }
                else
                {
                    DataRow dr1 = dtTimetoHire.NewRow();

                    dr1["Data"] = Facade.GetCompanyNameById(ChartData[i].ClientIdGraph);
                    dr1["Value1"] = ChartData[i].NoOfCandidateJoinedGraph;
                    dr1["Value2"] = ChartData[i].NoOfCandidateRejectedGraph;

                    dtTimetoHire.Rows.Add(dr1);
                }
            }
        }

        return dtTimetoHire;
    }

    private DataTable GetDataCandidateForSkill()
    {

        //IList<CompanyDocument> ChartData = Facade.GetAllCompanyDocument();
        IList<EmployeeProductivity> ChartData = Facade.GetCandidateBySkillId();
        DataTable dtTimetoHire = new DataTable();
        dtTimetoHire.Columns.Add("Data", Type.GetType("System.String"));
        dtTimetoHire.Columns.Add("Value1", Type.GetType("System.Int32"));
        //dtTimetoHire.Columns.Add("Value2", Type.GetType("System.Int32"));

        for (int i = 0; i < ChartData.Count; i++)
        {
            if (i == 10)
            {
                return dtTimetoHire;
            }
            else
            {
                DataRow dr1 = dtTimetoHire.NewRow();
                Skill skll = Facade.GetSkillById(ChartData[i].SkillIdGraph);
                dr1["Data"] = skll.Name;
                dr1["Value1"] = ChartData[i].NoOfCandidateForSkillGraph;
                //dr1["Value2"] = ChartData[i].NoOfCandidateRejectedGraph;

                dtTimetoHire.Rows.Add(dr1);
            }
        }
        return dtTimetoHire;
    }
    private DataTable GetDataRequisitionsCount()
    {
        //IList<CompanyDocument> ChartData = Facade.GetAllCompanyDocument();
        IList<EmployeeProductivity> ChartData = Facade.GetJobPostingByMonthYear();
        DataTable dtTimetoHire = new DataTable();
        dtTimetoHire.Columns.Add("Data", Type.GetType("System.String"));
        dtTimetoHire.Columns.Add("Value1", Type.GetType("System.Int32"));
        //dtTimetoHire.Columns.Add("Value2", Type.GetType("System.Int32"));

        for (int i = 0; i < ChartData.Count; i++)
        {
            if (i == 5)
            {
                return dtTimetoHire;
            }
            else
            {
                DataRow dr1 = dtTimetoHire.NewRow();
                dr1["Data"] = ChartData[i].MonthNameGraph.ToString() + " " + ChartData[i].YearGraph.ToString();
                dr1["Value1"] = ChartData[i].RequisitionsCountGraph;
                //dr1["Value2"] = ChartData[i].NoOfCandidateRejectedGraph;

                dtTimetoHire.Rows.Add(dr1);
            }
        }

        return dtTimetoHire;
    }



    private void Bindchart()
    {
        IList<EmployeeProductivity> ChartData = Facade.GetJobPostingByMonthYear();

        //storing total rows count to loop on each Record  
        string[] XPointMember = new string[ChartData.Count];
        int[] YPointMember = new int[ChartData.Count];

        for (int count = 0; count < ChartData.Count; count++)
        {
            //storing Values for X axis  
            //XPointMember[count] = ChartData[count].JobStatusGraph.ToString();
            //XPointMember[count] = ChartData.Rows[count]["Quarter"].ToString();  
            GenericLookup XPMemberName = Facade.GetGenericLookupById(ChartData[count].JobStatusGraph);
            XPointMember[count] = XPMemberName.Name;
            //storing values for Y Axis  
            YPointMember[count] = Convert.ToInt32(ChartData[count].CountByJobStatusGraph.ToString());
            //Chart1.ChartAreas.Add("ChartAreaNew" + count.ToString());
        }
         
        //binding chart control  
        Chart6.Series[0].Points.DataBindXY(XPointMember, YPointMember);
        Chart6.Series[0].IsValueShownAsLabel = true;
        //Setting width of line  
        Chart6.Series[0].BorderWidth = 10;
        //setting Chart type   
        Chart6.Series[0].ChartType = SeriesChartType.Radar;


        foreach (Series charts in Chart6.Series)
        {
            foreach (DataPoint point in charts.Points)
            {
                //switch (point.AxisLabel)
                //{
                //    //case "Q1": point.Color = Color.RoyalBlue; break;
                //    //case "Q2": point.Color = Color.SaddleBrown; break;
                //    //case "Q3": point.Color = Color.SpringGreen; break;
                //}
                //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

            }
        }

        //Enabled 3D  
        Chart6.ChartAreas["ChartArea6"].Area3DStyle.Enable3D = true;  
    }  

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }
    #endregion

    #region Listview Events
    protected void lsvCurrentSessions_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            TPS360 .Common .BusinessEntities . CurrentSessions _currentSessions = ((ListViewDataItem)e.Item).DataItem as TPS360 .Common .BusinessEntities .CurrentSessions;

            if (_currentSessions != null)
            {
                Label lblSessionType = (Label)e.Item.FindControl("lblSessionType");
                Label lblLoggedIn = (Label)e.Item.FindControl("lblLoggedIn");
                HyperLink lnkEmployeeName = (HyperLink)e.Item.FindControl("lnkEmployeeName");
                if (_currentSessions.ApplicationName == "TPS")
                    lblSessionType.Text = "Web";
                else if (_currentSessions.ApplicationName == "RP")
                    lblSessionType.Text = "Resume Parser";
                else lblSessionType.Text = "OL AddIn";
                lblLoggedIn.Text = _currentSessions.LoggedIn.ToShortDateString() + " " + _currentSessions.LoggedIn.ToString("HH:mm");

                if (_IsAccessToEmployee)
                {
                    if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                        ControlHelper.SetHyperLink(lnkEmployeeName, UrlForEmployee, string.Empty, _currentSessions.EmployeeName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(_currentSessions.Id ), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString());
                }
                else lnkEmployeeName.Text = _currentSessions.EmployeeName;


                //lnkEmployeeName.Text = _currentSessions.EmployeeName;
            }


        }
    }

    protected void lsvCurrentSessions_PreRender(object sender, EventArgs e)
    {
        lsvCurrentSessions.DataBind();
      
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvCurrentSessions.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                if (pager.Controls.Count >= 1)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardCurrentSessionsRowPerPage";
        }
        PlaceUpDownArrow();
        if (lsvCurrentSessions != null)
        {
            if (lsvCurrentSessions.Items.Count == 0)
            {
                lsvCurrentSessions.DataSource = null;
                lsvCurrentSessions.DataBind();
            }
        }
        
    }

    protected void lsvCurrentSessions_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
           
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (txtSortColumn.Text == lnkbutton.ID)
                {
                    if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                    else txtSortOrder.Text = "ASC";
                }
                else
                {
                    txtSortColumn.Text = lnkbutton.ID;
                    txtSortOrder.Text = "ASC";
                }

            }

            odsCurrentSessions.SelectParameters["SortOrder"].DefaultValue = txtSortOrder.Text; 
        }
        catch
        {
        }
    }
    #endregion
    #region Methods
    public void GetWidgetData()
    {
        try
        {
          odsCurrentSessions.SelectParameters["SortOrder"].DefaultValue = txtSortOrder.Text ;
            this.lsvCurrentSessions.DataBind();
        }
        catch
        {
        }
    }

    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton) lsvCurrentSessions.FindControl(txtSortColumn.Text);
            System.Web.UI.HtmlControls.HtmlTableCell th = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            th.Attributes.Add("class", ( txtSortOrder .Text   == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }
    #endregion
    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {

        if (lsvCurrentSessions != null)
        {
            if (lsvCurrentSessions.Items.Count == 0)
            {
                lsvCurrentSessions.DataSource = null;
                lsvCurrentSessions.DataBind();
            }
        }
    }
    void IWidget.Closed()
    {
    }

    #endregion
}
