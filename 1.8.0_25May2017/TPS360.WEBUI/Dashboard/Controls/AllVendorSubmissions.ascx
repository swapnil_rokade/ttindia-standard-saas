﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllVendorSubmissions.ascx.cs" Inherits="AllVendorSubmissions" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">    
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false" />
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false" />
 <asp:Panel ID="widgetBody" runat="server">
 <ucl:DateRangePicker ID="dtSubmissionDate" runat="server" />
 <asp:ImageButton ID="imgSearch" ImageUrl ="~/Images/search-icon.png" runat ="server" Width ="25px" Height ="25px" style="cursor :pointer ; margin :0px" ToolTip ="Apply Search Filter" />
            <asp:HiddenField ID="hdnStatus" Value="" runat="server" />
            <asp:ObjectDataSource ID="odsVendorSubmissions" runat="server" SelectMethod="GetPaged_ForVendorSubmissionDetail"
                TypeName="TPS360.Web.UI.CompanyConatctDataSource" SelectCountMethod="GetListCount_ForVendorSubmissionDetail"
                EnablePaging="True" SortParameterName="sortExpression" >
                <SelectParameters>
                    <asp:ControlParameter ControlID="dtSubmissionDate" Name="StartDate" PropertyName="StartDate"
                            Type="DateTime" />
                        <asp:ControlParameter ControlID="dtSubmissionDate" Name="EndDate" PropertyName="EndDate"
                            Type="DateTime" />
                            <asp:Parameter Name ="JobPostingId" DefaultValue ="0" />
                            <asp:Parameter Name ="VendorId" DefaultValue ="0" />
                            <asp:Parameter Name ="ContactId" DefaultValue ="0" />
                            <asp:Parameter Name="Type" DefaultValue ="" />
                            <asp:Parameter Name ="CandidateId" DefaultValue ="0" />
                </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ListView ID="lsvVendorSubmissions" runat="server" DataKeyNames="Id" DataSourceID="odsVendorSubmissions"
                OnItemDataBound="lsvVendorSubmissions_ItemDataBound" EnableViewState="true" OnPreRender="lsvVendorSubmissions_PreRender"
                OnItemCommand="lsvVendorSubmissions_ItemCommand">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th>
                                <asp:LinkButton ID="lnkDateSubmitted" runat="server" ToolTip="Sort By Date Submitted" CommandName="Sort"
                                    CommandArgument="[MJC].[CreateDate]" Text="Date Submitted" />
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkCandidate" runat="server" ToolTip="Sort By Candidate Name" CommandName="Sort"
                                    CommandArgument="[C].[CandidateName]" Text="Candidate Name" />
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkVendor" runat="server" ToolTip="Sort By Vendor Name"
                                    CommandName="Sort" CommandArgument="[Com].CompanyName" Text="Vendor" />
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkSubmittedBy" runat="server" ToolTip="Sort By Submitted By" CommandName="Sort"
                                    CommandArgument="[M].[FirstName]" Text="Submitted By" />
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkRequisition" runat="server" ToolTip="Sort By Job Title" CommandName="Sort"
                                    CommandArgument="[JP].JobTitle" Text="Job Title" />
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkStatus" runat="server" ToolTip="Sort By Satus" CommandName="Sort"
                                    CommandArgument="[DS].hiringstatus" Text="Status" />
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="6" id="tdPager" runat="server">
                                <ucl:Pager ID="pagerControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                        style="width: 100%; margin: 0px 0px;">
                        <tr>
                            <td>
                                No Vendor Submissions.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:Label ID="lblDateTime" runat="server"  />
                        </td>
                        <td>
                           <asp:HyperLink ID="hlnkCandidateName" runat="server"  ></asp:HyperLink>
                        </td>
                        <td>
                            <asp:HyperLink ID="hnlkVendor" runat="server"  ></asp:HyperLink>
                        </td>
                        <td>
                            <asp:LinkButton ID="hlnkContact" runat="server"  ></asp:LinkButton>
                        </td>
                        <td>
                           <asp:HyperLink ID="lnkJobTitle" runat="server"  ></asp:HyperLink>
                        </td>
                          <td>
                           <asp:Label ID="lblStatus" runat="server"  ></asp:Label>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
</asp:Panel>       