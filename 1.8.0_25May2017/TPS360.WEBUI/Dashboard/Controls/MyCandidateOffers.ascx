﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyCandidateOffers.ascx.cs"
    Inherits="MyCandidateOffers" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false" />
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false" />
<asp:Panel ID="widgetBody" runat="server">
    <asp:UpdatePanel ID="upMyCandidateOffer" runat="server">
        <ContentTemplate>
            <asp:ObjectDataSource ID="odsCandidateOffer" runat="server" SelectMethod="GetPaged"
                TypeName="TPS360.Web.UI.MemberHiringDetailsDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:Parameter Name="MemberId" DbType="Int32" />
                    <asp:Parameter Name="OfferLevelName" DefaultValue="" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ListView ID="lsvCandidateOffer" runat="server" DataSourceID="odsCandidateOffer"
                EnableViewState="true" DataKeyNames="Id" OnItemDataBound="lsvCandidateOffer_ItemDataBound"
                OnItemCommand="lsvCandidateOffer_ItemCommand" OnPreRender="lsvCandidateOffer_PreRender">
                <LayoutTemplate>
                    <table id="tlbTemplate" class="Grid" cellspacing="0" border="0" runat="server">
                        <tr>
                            <th>
                                <asp:LinkButton ID="btnOfferDate" runat="server" ToolTip="Sort By Offer Date" CommandName="Sort"
                                    CommandArgument="[MHD].[OfferedDate]" Text="Offer Date" />
                            </th>
                            <th>
                                <asp:LinkButton ID="btnRequisition" runat="server" ToolTip="Sort By Requisition"
                                    CommandName="Sort" CommandArgument="[J].[JobTitle]" Text="Requisition" />
                            </th>
                            <th>
                                <asp:LinkButton ID="btnCandidate" runat="server" ToolTip="Sort By Candidate" CommandName="Sort"
                                    CommandArgument="[M].[FirstName]+[M].[LastName]" Text="Candidate" />
                            </th>
                            <th style="width: 0px">
                                Accepted
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="4">
                                <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                        style="width: 100%; margin: 0px 0px;">
                        <tr>
                            <td>
                                No candidates available.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:Label ID="lblOfferedDate" runat="server"  />
                        </td>
                        <td>
                            <asp:HyperLink ID="lnkRequisition"  runat="server" ></asp:HyperLink>
                        </td>
                        <td>
                            <asp:HyperLink ID="lnkCandidateName"  runat="server"></asp:HyperLink>
                        </td>
                        <td align="center">
                            <div style="float: left; padding-left: 30px" align="center">
                                <asp:Image ID="imgAccept" runat="server" ImageUrl="~/Images/check_Image.gif" ImageAlign="Middle"
                                    Style="margin: 0px" />
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
