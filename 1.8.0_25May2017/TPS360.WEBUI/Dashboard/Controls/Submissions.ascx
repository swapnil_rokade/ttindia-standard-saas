﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Submissions.ascx.cs" Inherits="Submissions" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pagers" TagPrefix="ucll" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false" />
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false" />
<asp:Panel ID="widgetBody" runat="server">
    <asp:UpdatePanel ID="upSubmissions" runat="server">
        <ContentTemplate>
            <asp:ObjectDataSource ID="odsSubmissions" runat="server" SelectMethod="GetPagedForDashboard"
                TypeName="TPS360.Web.UI.JobPostingDataSource" SelectCountMethod="GetListCountForDashboard"
                EnablePaging="True" SortParameterName="sortExpression" EnableViewState="true">
                <SelectParameters>
                    <asp:Parameter Name="DashBoardSubmission" DefaultValue="true" Type="Boolean" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ListView ID="lsvSubmission" runat="server" DataKeyNames="Id" DataSourceID="odsSubmissions"
                OnItemDataBound="lsvSubmission_ItemDataBound" EnableViewState="true" OnPreRender="lsvSubmission_PreRender"
                OnItemCommand="lsvSubmission_ItemCommand">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th style="width: 20%;">
                                <asp:LinkButton ID="btnDateTime" runat="server" ToolTip="Sort By Date & Time" CommandName="Sort"
                                    CommandArgument="[MS].[CreateDate]" Text="Date Submitted" />
                            </th>
                            <th style="width: 20%;">
                                <asp:LinkButton ID="lnkJobPosting" runat="server" ToolTip="Sort By Requisition"
                                    CommandName="Sort" CommandArgument="[J].[JobTitle]" Text="Requisition" />
                            </th>
                            
                            <th style="width: 20%;">
                                <asp:LinkButton ID="btnCandidateName" runat="server" ToolTip="Sort By Candidate Name"
                                    CommandName="Sort" CommandArgument="[MN].[FirstName]+[MN].[LastName]" Text="Candidate" />
                            </th>
                            <th style="width: 20%;">
                                <asp:LinkButton ID="btnClient" runat="server" ToolTip="Sort By Account Name" CommandName="Sort"
                                    CommandArgument="[C].[CompanyName]" Text="Account" />
                            </th>
                            <th style="width: 25%;">
                                <asp:LinkButton ID="btnSubmittedby" runat="server" ToolTip="Sort By Submitted By"
                                    CommandName="Sort" CommandArgument="[M].[FirstName]+[M].[LastName]" Text="Submitted By" />
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="5">
                                <ucll:Pagers ID="pagerControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                        style="width: 100%; margin: 0px 0px;">
                        <tr>
                            <td>
                                No Submissions.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:Label ID="lblDateTime" runat="server" />
                        </td>
                        <td><asp:LinkButton ID ="lnkRequisition" runat ="server" ></asp:LinkButton>
                        </td>
                        <td>
                            <asp:HyperLink ID="lnkCandidateName" runat="server"  ></asp:HyperLink>
                        </td>
                        <td>
                            <asp:Label ID="lblClient" runat="server"  />
                        </td>
                        <td>
                            <asp:Label ID="lblSubmittedBy" runat="server" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
