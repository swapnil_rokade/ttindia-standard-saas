<%-- 
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName : ScheduleAppoinment.ascx
    Description: Schedules and Appoinments Widget 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Nov-20-2008           Anand Dixit         Defect ID: 8985; Removed Height attribute of the Web Week View
    0.2            Jun-25-2009           Veda                Defect ID: 10768; Wrong current date was dislaying.

-------------------------------------------------------------------------------------------------------------------------------------------        

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ScheduleAppoinment.ascx.cs"
    Inherits="ScheduleAppoinment" %>


<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>

<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>

<%@ Register assembly="Infragistics35.WebUI.WebScheduleDataProvider.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.WebUI.Data" tagprefix="ig_scheduledata" %>
<%@ Register assembly="Infragistics35.WebUI.WebSchedule.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.WebUI.WebSchedule" tagprefix="igsch" %>

    
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:Panel ID="widgetBody" runat="server">

    <%--<asp:UpdatePanel UpdateMode="Conditional" runat="server">
    <ContentTemplate>--%>
    <%--<div>
        <ig_scheduledata:WebScheduleSqlClientProvider ID="WebScheduleSqlClientProvider1"
            EnableAppStyling="True" runat="server" WebScheduleInfoID="WebScheduleInfo1">
        </ig_scheduledata:WebScheduleSqlClientProvider>
        <ig_sched:WebScheduleInfo ID="WebScheduleInfo1" runat="server" EnableSmartCallbacks="true"
            EnableRecurringActivities="True" EnableProgressIndicator="False" EnableUnassignedResource="True">
        </ig_sched:WebScheduleInfo>
        <igtab:UltraWebTab ID="UltraWebTab1" runat="server" Width="100%" Height="45%" Font-Names="Verdana"
            Font-Size="9pt" BorderWidth="1px" BorderStyle="Solid" BarHeight="0" BorderColor="#3050D0"
            SpaceOnLeft="1" OnTabClick="UltraWebTab1_TabClick">
            <BorderDetails WidthTop="0px"></BorderDetails>
            <DefaultTabStyle Height="20px" BackColor="#C5D5E7" ForeColor="Black">
                <Padding Left="6px" Right="4px"></Padding>
                <BorderDetails ColorTop="192, 192, 255" WidthTop="1px" ColorLeft="192, 192, 255">
                </BorderDetails>
            </DefaultTabStyle>
            <SelectedTabStyle BorderColor="#6464C0" BackColor="#D7EEFF">
                <BorderDetails ColorTop="White" ColorLeft="White"></BorderDetails>
            </SelectedTabStyle>
            <Tabs>
                <igtab:Tab Text="Calendar">
                    <ContentTemplate>
                        <ig_sched:WebCalendarView ID="WebCalendarView1" runat="server" Height="100%" Width="100%"
                            MonthDropDownVisible="False" YearDropDownVisible="False" NavigationAnimation="AccelDecel">
                            <CaptionHeaderStyle BackColor="#D7D7E5" ForeColor="#FFFFFF" Font-Bold="True" BackgroundImage="../Images/Dashboard/MonthHeaderCaption_bg.png">
                            </CaptionHeaderStyle>
                            <FooterTemplate>
                                <%--  Today: 4/28/2009 --%>
                      
         
                            <%--</FooterTemplate>
                            <NavigationButtonStyle BackColor="#D7D7E5" BackgroundImage="../Images/Dashboard/MonthHeaderCaption_bg.png">
                            </NavigationButtonStyle>
                            <DayHeaderStyle Height="18px"></DayHeaderStyle>
                        </ig_sched:WebCalendarView>
                    </ContentTemplate>
                </igtab:Tab>
                <igtab:TabSeparator>
                    <Style Width="1px">
                        </Style>
                </igtab:TabSeparator>
                <igtab:Tab Text="Day">
                    <ContentTemplate>
                        <ig_sched:WebDayView ID="WebDayView1" runat="server" Height="100%" Width="100%" CaptionHeaderText=""
                            NavigationAnimation="AccelDecel">
                        </ig_sched:WebDayView>
                    </ContentTemplate>
                </igtab:Tab>
                <igtab:TabSeparator>
                    <Style Width="1px">
                        </Style>
                </igtab:TabSeparator>
                <igtab:Tab Text="Week">
                    <ContentTemplate>
                        <%-- 0.1 --%>
                     <%--   <ig_sched:WebWeekView ID="WebWeekView1" runat="server" Height="400px" Width="100%"
                            NavigationAnimation="AccelDecel" StyleSetName="" StyleSetPath="" StyleSheetDirectory="">
                            <SelectedAppointmentStyle BackColor="#CC9900" BorderStyle="Solid">
                            </SelectedAppointmentStyle>
                        </ig_sched:WebWeekView>--%>
                        <%-- 0.1 --%>
                <%--    </ContentTemplate>--%>
                    <%--<ContentPane Scrollable ="Auto" EnableViewState ="false"  >
                    </ContentPane>--%>
                <%--</igtab:Tab>
                <igtab:TabSeparator>
                    <Style Width="1px">
                        </Style>
                </igtab:TabSeparator>
                <igtab:Tab Text="Month">
                    <ContentTemplate>
                        <ig_sched:WebMonthView ID="WebMonthView1" runat="server" Height="100%" Width="100%"
                            NavigationAnimation="AccelDecel">
                        </ig_sched:WebMonthView>
                    </ContentTemplate>
                </igtab:Tab>
            </Tabs>
        </igtab:UltraWebTab>--%>
<%--    </div>--%>
    <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>
    
    <div>
    <ig_scheduledata:WebScheduleSqlClientProvider ID="WebScheduleSqlClientProvider1"
            EnableAppStyling="True" runat="server" WebScheduleInfoID="WebScheduleInfo1">
    </ig_scheduledata:WebScheduleSqlClientProvider>
    <igsch:WebScheduleInfo  ID="WebScheduleInfo1" runat="server" EnableRecurringActivities="true" EnableSmartCallbacks="true" EnableUnassignedResource="true" EnableProgressIndicator="true">
    
    </igsch:WebScheduleInfo>
    <ig:WebTab runat="server" OnSelectedIndexChanged="UltraWebTab1_TabClick" Id="UltraWebTab1" Width="100%" Height="45%" Font-Names="Verdana" Font-Size="9pt" BorderWidth="1px" BarHeight="0" BorderColor="#3050D0" SpaceOnLeft="1" OnTabClick="WebTab1_TabClick">
    <PostBackOptions EnableReloadingUnselectedTab="true" EnableAjax="true" EnableLoadOnDemand="true" />
    <Tabs>
    <ig:ContentTabItem runat="server" Text="Calendar">
    <Template>
    <igsch:WebCalendarView ID="WebCalendarView1" DayNameFormat="Full" DayHeaderStyle-CssClass="calHeaderStyle" BorderStyle="Inset" EnableAppStyling="True"  WebScheduleInfoID="WebScheduleInfo1" runat="server" Height="100%" Width="100%" MonthDropDownVisible="False" YearDropDownVisible="False"  >
    
    <FooterTemplate>
    
    Today: <%=DateTime.Now %>
    </FooterTemplate>
    <NavigationButtonStyle BackColor="#D7D7E5">
    </NavigationButtonStyle>
    <DayHeaderStyle Height="18px" />
    </igsch:WebCalendarView>
    </Template>
    </ig:ContentTabItem>
    <ig:ContentTabItem runat="server" Text="Day">
    <Template>
    
    <igsch:WebDayView ID="WebDayView1" WebScheduleInfoID="WebScheduleInfo1" runat="server" Height="100%" Width="100%" NavigationAnimation="None" EnableAppStyling="True">
    <CaptionHeaderStyle BackColor="#A27F91" Font-Bold="True" BorderStyle="Groove">
      <Padding Bottom="2px" Left="2px" Right="2px" Top="2px" />
     
    </CaptionHeaderStyle>
    
    </igsch:WebDayView>
    </Template>
    </ig:ContentTabItem>
    <ig:ContentTabItem runat="server" Text="Month">
    <Template>
    <igsch:WebMonthView ID="WebMonthView1" runat="server" EnableAppStyling="True" WebScheduleInfoID="WebScheduleInfo1" Height="100%" Width="99.5%" >
    <NavigationButtonStyle Cursor="Hand"  BorderStyle="Double" />
    </igsch:WebMonthView>
    </Template>
    </ig:ContentTabItem>
    </Tabs>
    </ig:WebTab>
    </div>
</asp:Panel>
