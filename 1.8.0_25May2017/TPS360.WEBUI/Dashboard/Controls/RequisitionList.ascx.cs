using System;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Web.UI;
using TPS360.Common.Helper;
using System.Web.UI.HtmlControls;

public partial class RequisitionList : BaseControl, IWidget
{
    #region Properties

    private  bool _isMyList = true ;
    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    public bool IsMyList
    {
        get { return _isMyList; }
        set { _isMyList = value; }
    }


    
    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if(IsMyList )
        odsMyRequisitionList.SelectParameters["memberId"].DefaultValue = base.CurrentMember.Id.ToString();
        (this as IWidget).HideSettings();
       
        if (!IsPostBack)
        {
            GetWidgetData();
            txtSortColumn1.Text = "btnDatePublished";
            txtSortOrder1.Text = "DESC";
        }
        string pagesize = "";
        if (IsMyList)
            pagesize = (Request.Cookies["DashboardRequisitionListRowPerPage"] == null ? "" : Request.Cookies["DashboardRequisitionListRowPerPage"].Value);
        else pagesize = (Request.Cookies["DashboardAllRequisitionListRowPerPage"] == null ? "" : Request.Cookies["DashboardAllRequisitionListRowPerPage"].Value);
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvRequisition .FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)  pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
        }
   
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        (this as IWidget).HideSettings();
    }

    #endregion

    #region Methods

    #region Methods
    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvRequisition.FindControl(txtSortColumn1.Text);
            HtmlTableCell im = (HtmlTableCell)lnk.Parent;
            if (lnk.CommandArgument.Contains("[1]"))
                im.Attributes.Add("class", (txtSortOrder1.Text == "ASC" ? "Ascending" : "Descending"));
            else
                im.Attributes.Add("class", (txtSortOrder1.Text == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }
    public void GetWidgetData()
    {
        if(IsMyList )
         odsMyRequisitionList.SelectParameters["memberId"].DefaultValue = base.CurrentMember.Id.ToString();
        if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
        {
            SortOrder.Text = "desc";
            SortColumn.Text = "[J].[PostedDate]";
        }
        odsMyRequisitionList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
        this.lsvRequisition.DataBind();
    }

    #endregion
  
    #endregion

    #region ListView Events

    protected void lsvRequisition_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        bool isAssingned = false;
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;

            if (jobPosting != null)
            {
                JobPostingHiringTeam team = Facade.GetJobPostingHiringTeamByMemberId(base.CurrentMember.Id, jobPosting.Id);
                if (team != null) isAssingned = true;
                  
                HyperLink hlnkJobTitle = (HyperLink)e.Item.FindControl("lblJobTitle");
                Label lblJobCode = (Label)e.Item.FindControl("lblJobCode");
                Label lblJobstatus = (Label)e.Item.FindControl("lblJobstatus");
                Label lblJobPostedDate = (Label)e.Item.FindControl("lblJobPostedDate");
                Label lblClientJobID = (Label)e.Item.FindControl("lblClientJobID");
                HyperLink hlnkNoOfCandidates = (HyperLink)e.Item.FindControl("hlnkNoOfCandidates");
               // ControlHelper.SetHyperLink(hlnkJobTitle, UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE, string.Empty, MiscUtil.RemoveScript(jobPosting.JobTitle), UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id));
                System.Web.UI.HtmlControls.HtmlTableCell thClientJobID = (System.Web.UI.HtmlControls.HtmlTableCell)lsvRequisition.FindControl("thClientJobID");
                System.Web.UI.HtmlControls.HtmlTableCell tdClientJobID = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdClientJobID");
                //System.Web.UI.WebControls.TableCell tdClientJobID = (System.Web.UI.WebControls.TableCell)lsvRequisition.FindControl("tdClientJobID");
                System.Web.UI.HtmlControls.HtmlTableCell thCode = (System.Web.UI.HtmlControls.HtmlTableCell)lsvRequisition.FindControl("thCode");
                System.Web.UI.HtmlControls.HtmlTableCell tdJobCode = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdJobCode");
                hlnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                hlnkJobTitle.Text = jobPosting.JobTitle;
                lblClientJobID.Text = jobPosting.ClientJobId;
                lblJobCode.Text = jobPosting.JobPostingCode;
                lblJobPostedDate.Text = jobPosting.PostedDate.ToShortDateString();
                GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(jobPosting.JobStatus);
                if (_RequisitionStatusLookup != null)
                {
                    lblJobstatus.Text = _RequisitionStatusLookup.Name;
                }
                int count = 0;
                string FirstLevel = Facade.HiringMatrixLevel_GetInitialLevel().Id.ToString();
                foreach (Hiring_Levels lev in jobPosting.HiringMatrixLevels)
                    count = lev.CandidateCount;
                if (base .IsUserAdmin  || jobPosting.CreatorId == base.CurrentMember.Id || isAssingned) { ControlHelper.SetHyperLink(hlnkNoOfCandidates, UrlConstants.ATS.ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX, string.Empty, count.ToString(), UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id), UrlConstants.PARAM_TAB, FirstLevel.ToString()); } else hlnkNoOfCandidates.Enabled = false;// lnkLevel.Enabled = false;
                //0.1  End

                if (ApplicationSource == ApplicationSource.GenisysApplication) 
                {
                    tdClientJobID.Visible = true;
                    thClientJobID.Visible = true;
                    thCode.Visible = false;
                    tdJobCode.Visible = false;
                    

                }
            }

        }
    }
    protected void lsvRequisition_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (txtSortColumn1.Text == lnkbutton.ID)
                {
                    if (txtSortOrder1.Text == "ASC") txtSortOrder1.Text = "DESC";
                    else txtSortOrder1.Text = "ASC";
                }
                else
                {
                    txtSortColumn1.Text = lnkbutton.ID;
                    txtSortOrder1.Text = "ASC";
                }
                if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                {
                    if (e.CommandArgument.ToString() == "[1]")
                        SortOrder.Text = "desc";
                    else
                        SortOrder.Text = "asc";
                }
                else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                SortColumn.Text = e.CommandArgument.ToString();
                odsMyRequisitionList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            }
        }
        catch
        {
        }
    }

    protected void lsvRequisition_PreRender(object sender, EventArgs e)
    {
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvRequisition .FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                if (pager.Controls.Count >= 1)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = IsMyList ? "DashboardRequisitionListRowPerPage" : "DashboardAllRequisitionListRowPerPage";
        }
        PlaceUpDownArrow();

        if (lsvRequisition.Items.Count == 0)
        {
            lsvRequisition.DataSource = null;
            lsvRequisition.DataBind();
        }
    }
    #endregion
 
    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {
        if (lsvRequisition != null)
        {
            if (lsvRequisition.Items.Count == 0)
            {
                lsvRequisition.DataSource = null;
                lsvRequisition.DataBind();
            }
        }
    }
    void IWidget.Closed()
    {
    }

    #endregion
}
