﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;

public partial class MySubmissions : TPS360.Web.UI.BaseControl, IWidget
{
    #region Member Variables
    private bool _IsAccessToCandidate = true;
    private static string UrlForCandidate = string.Empty;
    private static int SitemapIdForCandidate = 0;
    #endregion

    #region Properties

    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    #endregion

    #region Page Events
    private void setParameters()
    {
        odsSubmissions.SelectParameters.Clear();
        odsSubmissions.SelectParameters.Add("DashBoardSubmission", "true");
        odsSubmissions.SelectParameters.Add("MemberId",  base.CurrentMember.Id.ToString());


    }
    protected void Page_Load(object sender, EventArgs e)
    {

        setParameters();
        (this as IWidget).HideSettings();
        CustomSiteMap CustomMap = new CustomSiteMap();
        CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
        if (CustomMap == null) _IsAccessToCandidate = false;
        else
        {
            SitemapIdForCandidate = CustomMap.Id;
            UrlForCandidate = "~/" + CustomMap.Url.ToString();
        }

        if (!IsPostBack)
        {
            GetWidgetData();
            txtSortColumn.Text = "btnDateTime";
            txtSortOrder.Text = "DESC";
            PlaceUpDownArrow();
        }
        string pagesize = "";
        pagesize = (Request.Cookies["DashboardMySubmissionRowPerPage"] == null ? "" : Request.Cookies["DashboardMySubmissionRowPerPage"].Value); ;
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvSubmission.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }
    #endregion

    #region Listview Events
    protected void lsvSubmission_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {

            Submission submission = ((ListViewDataItem)e.Item).DataItem as Submission;

            if (submission != null)
            {
                HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
                LinkButton lnkRequisition = (LinkButton)e.Item.FindControl("lnkRequisition");
                Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
                Label lblClient = (Label)e.Item.FindControl("lblClient");
                //Label lblSubmittedBy = (Label)e.Item.FindControl("lblSubmittedBy");
                // Label lblSubmittedTo = (Label)e.Item.FindControl("lblSubmittedTo");
                // LinkButton lnkSubmissionEmail = (LinkButton)e.Item.FindControl("lnkSubmissionEmail");
                //string  t = System.Globali
                lblDateTime.Text = submission.SubmissionDate.ToShortDateString();// +" " + submission.SubmissionDate.ToShortTimeString();

                string _strFullName = submission.ApplicantName;
                if (_strFullName.Trim() == "")
                    _strFullName = "No Candidate Name";
                if (_IsAccessToCandidate)
                {
                    if (SitemapIdForCandidate > 0 && UrlForCandidate != string.Empty)
                        ControlHelper.SetHyperLink(lnkCandidateName, UrlForCandidate, string.Empty, _strFullName, UrlConstants.PARAM_MEMBER_ID, submission.MemberId.ToString(), UrlConstants.PARAM_SITEMAP_ID, SitemapIdForCandidate.ToString());
                }
                else
                {
                    lnkCandidateName.Enabled = false;
                    lnkCandidateName.Text = _strFullName;// submission.ApplicantName;
                }

                lnkRequisition.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + submission.Id + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                lnkRequisition.Text = submission.JobTitle;

                lblClient.Text = submission.CompanyName;
                //JobPostingHiringTeam details = new JobPostingHiringTeam();
                //details = Facade.GetJobPostingHiringTeamByMemberId(CurrentMember.Id, submission.Id);
                //if (details != null)
                //    ControlHelper.SetHyperLink(lnkRequisition, UrlConstants.ATS.ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX, string.Empty, MiscUtil.RemoveScript(submission.JobTitle), UrlConstants.PARAM_JOB_ID, submission.Id.ToString(), UrlConstants.PARAM_TAB, Facade.HiringMatrixLevel_GetInitialLevel().Id.ToString());
                //else
                //{
                //    lnkRequisition.Enabled = false;
                //    lnkRequisition.Text = submission.JobTitle;
                //}
               
                //lblSubmittedBy.Text = submission.SubmittedBy;
                //lblSubmittedTo.Text = submission.SubmittedTo;
                //if (submission.MemberEmailId > 0)
                //{
                //    lnkSubmissionEmail.Visible = true;
                //    lnkSubmissionEmail.CommandArgument = submission.MemberEmailId + "$" + submission.ClientId + "$" + submission.Id + "$" + submission.MemberId;
                //}
                //else
                //    lnkSubmissionEmail.Visible = false;
            }
        }
    }

    protected void lsvSubmission_PreRender(object sender, EventArgs e)
    {
        lsvSubmission.DataBind();
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvSubmission.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                if (pager.Controls.Count >= 1)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardMySubmissionRowPerPage";
        }
        PlaceUpDownArrow();
        if (SiteSetting != null)
        {
            if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
            {
                LinkButton btnClient = (LinkButton)lsvSubmission.FindControl("btnClient");
                if (btnClient != null) btnClient.ToolTip = btnClient.Text = "BU";

            }
        }
        if (lsvSubmission != null)
        {
            if (lsvSubmission.Items.Count == 0)
            {
                lsvSubmission.DataSource = null;
                lsvSubmission.DataBind();
            }
        }
    }

    protected void lsvSubmission_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            if (string.Equals(e.CommandName, "Email"))
            {
                string[] ParamID = e.CommandArgument.ToString().Split('$');

                string sub_MemberEmailId = ParamID[0];
                string sub_ClientID = ParamID[1];
                string sub_SubmissionId = ParamID[2];
                string sub_MemberId = ParamID[3];
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.CommonPages.EMAIL_PREVIEW.Substring(3), string.Empty, UrlConstants.PARAM_MemberEmail_ID, sub_MemberEmailId, UrlConstants.PARAM_COMPANY_ID, sub_ClientID, UrlConstants.PARAM_JOB_ID, sub_SubmissionId, UrlConstants.PARAM_APPLICANT_ID, sub_MemberId);
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "EmailDashBoard", "<script>window.open('" + url + "');</script>", false);
            }
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (txtSortColumn.Text == lnkbutton.ID)
                {
                    if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                    else txtSortOrder.Text = "ASC";
                }
                else
                {
                    txtSortColumn.Text = lnkbutton.ID;
                    txtSortOrder.Text = "ASC";
                }

            }
        }
        catch
        {
        }
    }
    #endregion

    #region Methods
    public void GetWidgetData()
    {
        try
        {
            odsSubmissions.SelectParameters["MemberId"].DefaultValue = base.CurrentMember.Id.ToString();
            this.lsvSubmission.DataBind();
        }
        catch
        {
        }
    }

    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvSubmission.FindControl(txtSortColumn.Text);
            System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            im.Attributes.Add("class", (txtSortOrder .Text == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }
    #endregion

    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {
        if (lsvSubmission != null)
        {
            if (lsvSubmission.Items.Count == 0)
            {
                lsvSubmission.DataSource = null;
                lsvSubmission.DataBind();
            }
        }
    }
    void IWidget.Closed()
    {
    }

    #endregion
}
