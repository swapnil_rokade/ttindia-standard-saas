﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CurrentSessions.ascx.cs"
    Inherits="CurrentSessions" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pagers" TagPrefix="ucll" %>
<style type="text/css">
    .style1
    {
        width: 34px;
    }
</style>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false" />
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false" />
<asp:Panel ID="widgetBody" runat="server">
    <asp:UpdatePanel ID="upcurrentSession" runat="server">
        <ContentTemplate>
            <asp:ObjectDataSource ID="odsCurrentSessions" runat="server" SelectMethod="GetPaged"
                TypeName="TPS360.Web.UI.CurrentSessionsDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression" EnableViewState="true">
                <SelectParameters>
                    <asp:Parameter Name="SortOrder" DefaultValue="" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
                 <div >
       <%--  <div style="margin-right: 10%">--%>
           <%--  <asp:Chart ID="Chart1" runat="server" BackColor="0, 0, 64" BackGradientStyle="VerticalCenter"  
                        BorderlineWidth="0"  Palette="None" PaletteCustomColors="Maroon"  
                       BorderlineColor="64, 0, 64" Width="500px">  
                        <Titles>  
                            <asp:Title ShadowOffset="10" Name="Items" />  
                        </Titles>  
                        <Legends>  
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"  
                                LegendStyle="Row" />  
                        </Legends>  
                        <Series>  
                            <asp:Series Name="Default" />  
                          
                        </Series>  
                      
                        <ChartAreas>  
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />  
                        </ChartAreas>  
            </asp:Chart>--%>
           <%--  <div style="margin-left: 40%">
                  <asp:Label ID="lblGrapghOfferJoin" Text="Offered to Joining Rate" runat="server" Font-Bold="True" />
             </div>--%>
             
            <table >             
              <tr>              
                <td><asp:Chart ID="Chart1" runat="server" Width="600px" Palette="None" 
                         PaletteCustomColors="#33CCCC; #FF9900; Red; Green" Height="350px" BorderDashStyle="Solid" BackSecondaryColor="White"
      BackGradientStyle="None" BorderWidth="2px" backcolor="211, 223, 240"
      BorderColor="#1A3B69">
       <Titles>
                          <asp:Title Text="Offered to Joining Rate"  Font="Helvetica Neue, 10pt, style=Bold"/>
                    </Titles>
            <Series>             
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart></td>        
                <td><asp:Chart ID="Chart2" runat="server" Width="600px" Palette="None" 
                         PaletteCustomColors="Green; Red; #FF9900" Height="350px" BorderDashStyle="Solid" BackSecondaryColor="White"
      BackGradientStyle="None" BorderWidth="2px" backcolor="211, 223, 240"
      BorderColor="#1A3B69">
       <Titles>
                          <asp:Title Text="Present-to-Interview Ratio (%)" Font="Helvetica Neue, 10pt, style=Bold" />
                    </Titles>
            <Series>             
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea2">
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart></td> 
              </tr> 
              
                <tr>              
                 <td><table style="height: 50px ;margin-left: 20%;">
              <%--<tr>
                <th class="style1">Type</th>
                <th>Value</th>
              </tr>
              --%>
              <tr>
                <td><img id="Img2" runat="server" src="" height="10" width="20" 
                        style="background-color: #33CCCC"/></td>
                <td>Sourced</td>                
                 <td ><img id="Img3" runat="server" src="" height="10" width="20" 
                        style="background-color: #FF9900"/></td>
                <td>Offered</td>
                 <td ><img id="Img4New" runat="server" src="" height="10" width="20" 
                         style="background-color: #FF0000"/></td>
                <td>Offer Rejected</td>
                    <td ><img id="Img4New1" runat="server" src="" height="10" width="20" 
                          style="background-color: #008000"/></td>
                <td>Joined</td>
            <%--  </tr>
               <tr>
                <td ><img id="Img1" runat="server" src="" height="10" width="20" 
                        style="background-color: #0000FF"/></td>
                <td>Offer</td>
                    <td ><img id="Img5" runat="server" src="" height="10" width="20" 
                          style="background-color: #008000"/></td>
                <td>Joined</td>
              </tr>--%>
             <%--  <tr>
                 <td class="style1"><img id="Img2" runat="server" src="" height="10" width="20" 
                         style="background-color: #FF0000"/></td>
                <td>Candidate OfferRejected</td>
              </tr>--%>
             <%--  <tr>
                  <td class="style1"><img id="Img3" runat="server" src="" height="10" width="20" 
                          style="background-color: #008000"/></td>
                <td>Candidate Joined</td>
              </tr>--%>
            </table></td>
               
                <td><table style="height: 50px; margin-left: 20%;">
              <tr>
                <td><img id="Img1" runat="server" src="" height="10" width="20" 
                        style="background-color: #008000"/></td>
                <td>Completed</td>
                 <td ><img id="Img5" runat="server" src="" height="10" width="20" 
                         style="background-color: #FF0000"/></td>
                <td>No Show</td>
                 <td ><img id="Img6" runat="server" src="" height="10" width="20" 
                        style="background-color: #FF9900"/></td>
                <td>Pending</td>
                  
            </table></td>
               </tr>  
            <tr>
                <td>
                <asp:Chart ID="Chart3" runat="server" Width="600px" Palette="None"
                         PaletteCustomColors="Green; Red" Height="350px" BorderDashStyle="Solid" BackSecondaryColor="White"
      BackGradientStyle="None" BorderWidth="2px" backcolor="211, 223, 240"
      BorderColor="#1A3B69">
                     <Titles>
                          <asp:Title Text="Time to Hire (days)" Font="Helvetica Neue, 10pt, style=Bold"  />
                    </Titles>
            <Series>             
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea3">
                </asp:ChartArea>
            </ChartAreas>
            
        </asp:Chart></td>
                <td><asp:Chart ID="Chart4" runat="server" Width="600px" Palette="None"
                         PaletteCustomColors="#DDDD00; #B83D3D" Height="350px" BorderDashStyle="Solid" BackSecondaryColor="White"
      BackGradientStyle="None" BorderWidth="2px" backcolor="211, 223, 240"
      BorderColor="#1A3B69">
                     <Titles>
                          <asp:Title Text="Candidate Rejection Ratio - By Business" Font="Helvetica Neue, 10pt, style=Bold"  />
                    </Titles>
            <Series>             
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea4">
                </asp:ChartArea>
            </ChartAreas>
            
        </asp:Chart></td>
              </tr>
              <tr>
              <td>
                <table style="height: 50px; margin-left: 20%;">
              <tr>
                <td><img id="Img7" runat="server" src="" height="10" width="20" 
                        style="background-color: #008000"/></td>
                <td>Expected Time To Fill</td>
                 <td ><img id="Img8" runat="server" src="" height="10" width="20" 
                         style="background-color: #FF0000"/></td>
                <td>Actual Time To Fill</td>
               </tr>
                  
            </table></td>
            <td><table style="height: 50px; margin-left: 20%;">
              <tr>
                <td><img id="Img4" runat="server" src="" height="10" width="20" 
                        style="background-color: #DDDD00"/></td>
                <td>Sourced</td>
                 <td ><img id="Img9" runat="server" src="" height="10" width="20" 
                         style="background-color: #B83D3D"/></td>
                <td>Rejected</td>
               </tr>
                  
            </table></td>
              </tr>    
              
              <tr>
              <td><asp:Chart ID="Chart5" runat="server" Width="600px" Palette="Bright" Height="350px" BorderDashStyle="Solid" BackSecondaryColor="White"
      BackGradientStyle="None" BorderWidth="2px" backcolor="211, 223, 240"
      BorderColor="#1A3B69">
                     <Titles>
                          <asp:Title Text="Candidate's Skill (Top 10)" Font="Helvetica Neue, 10pt, style=Bold"  />
                    </Titles>
            <Series> 
              <%--<Legends>
                <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
            </Legends>--%>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea5">
                </asp:ChartArea>
            </ChartAreas>
            
        </asp:Chart></td>
              <td>
              <%--<asp:Chart ID="Chart6" runat="server" Width="600px" Palette="Berry" Height="350px" BorderDashStyle="Solid" BackSecondaryColor="White"
      BackGradientStyle="TopBottom" BorderWidth="2px" backcolor="211, 223, 240"
      BorderColor="#1A3B69">
                     <Titles>
                          <asp:Title Text="Requisitions (Per Month)" Font="Helvetica Neue, 10pt, style=Bold"  />
                    </Titles>
            <Series> 
            
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea6">
                </asp:ChartArea>
            </ChartAreas>
            
        </asp:Chart>--%>
        <asp:Chart ID="Chart6" runat="server" Width="600px" Palette="Pastel" Height="350px" BorderDashStyle="Solid" BackSecondaryColor="White"
      BackGradientStyle="None" BorderWidth="2px" backcolor="211, 223, 240"
      BorderColor="#1A3B69">
                <Titles>
                          <asp:Title Text="Requisitions (Current Month)" Font="Helvetica Neue, 10pt, style=Bold"  />
                    </Titles>
               <%--  <Legends>
                    <asp:Legend Docking="Top">
                    </asp:Legend>
                </Legends>--%>
                    <Series>  
                        <asp:Series Name="Series0">  
                        </asp:Series>  
                    </Series>  
                    <ChartAreas>  
                        <asp:ChartArea Name="ChartArea6">  
                        </asp:ChartArea>  
                    </ChartAreas>  
                </asp:Chart>
        </td>
              </tr> 
              <tr>
              <td>
               <%--<asp:Chart ID="Chart7" runat="server">  
                 <Legends>
                <asp:Legend Docking="Top">
                </asp:Legend>
            </Legends>
                    <Series>  
                        <asp:Series Name="Series0">  
                        </asp:Series>  
                    </Series>  
                    <ChartAreas>  
                        <asp:ChartArea Name="ChartArea7">  
                        </asp:ChartArea>  
                    </ChartAreas>  
                </asp:Chart>--%>  
              </td>
               <td></td>
              </tr>                       
           </table>

              
            </div>
            <br />
            <div>
        <%--    <div style="margin-left: 20%">--%>
            
            </div>
            
            <br />
            
           <div>
          <%--   <div style="margin-left: 40%">
                  <asp:Label ID="Label1" Text="Present-to-Interview Ratio (%)" runat="server" Font-Bold="True" />
             </div>--%>

              
            </div>
            <br />
            <div>
          <%--  <div style="margin-left: 20%">--%>
            
            </div>
            
             <br />
            
           <div>
      <%--       <div style="margin-left: 40%">
                  <asp:Label ID="Label2" Text="Time to Hire (days)" runat="server" Font-Bold="True" />
             </div>--%>

              
            </div>
            <br />
            <div>
         <%--   <div style="margin-left: 20%">--%>
            
            </div>
            
            <asp:ListView ID="lsvCurrentSessions" runat="server" DataKeyNames="Id" DataSourceID="odsCurrentSessions"
                OnItemDataBound="lsvCurrentSessions_ItemDataBound" EnableViewState="true" OnPreRender="lsvCurrentSessions_PreRender"
                OnItemCommand="lsvCurrentSessions_ItemCommand" Visible="False">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th style="width: 20%;" enableviewstate="false">
                                <asp:LinkButton ID="btnEmployeeName" runat="server" ToolTip="Sort By Employee Name"
                                    CommandName="Sort" CommandArgument="[M].[FirstName]" Text="Employee" />
                            </th>
                            <th style="width: 20%;" enableviewstate="false">
                                <asp:LinkButton ID="btnSessionType" runat="server" ToolTip="Sort By Session Type"
                                    CommandName="Sort" CommandArgument="[AAD].[AppName]" Text="Session Type" />
                            </th>
                            <th style="width: 20%;" enableviewstate="false">
                                <asp:LinkButton ID="btnLoggedIn" runat="server" ToolTip="Sort By Logged In" CommandName="Sort"
                                    CommandArgument="[AAD].[CreatedDate]" Text="Logged In" />
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="3">
                                <ucll:Pagers ID="pagerControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                        style="width: 100%; margin: 0px 0px;">
                        <tr>
                            <td>
                                No Sessions.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:HyperLink ID="lnkEmployeeName" runat="server" ></asp:HyperLink>
                        </td>
                        <td>
                            <asp:Label ID="lblSessionType" runat="server"  />
                        </td>
                        <td>
                            <asp:Label ID="lblLoggedIn" runat="server" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
