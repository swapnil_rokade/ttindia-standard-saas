﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CandidateOffers.ascx.cs"
    Inherits="CandidateOffers" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false" />
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false" />
<asp:Panel ID="widgetBody" runat="server">
    <asp:UpdatePanel ID="upOffer" runat="server">
        <ContentTemplate>
            <asp:ObjectDataSource ID="odsCandidateOffer" runat="server" SelectMethod="GetPaged"
                TypeName="TPS360.Web.UI.MemberHiringDetailsDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:Parameter Name="OfferLevelName" DefaultValue="" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ListView ID="lsvCandidateOffer" runat="server" DataSourceID="odsCandidateOffer"
                EnableViewState="true" DataKeyNames="Id" OnItemDataBound="lsvCandidateOffer_ItemDataBound"
                OnItemCommand="lsvCandidateOffer_ItemCommand" OnPreRender="lsvCandidateOffer_PreRender">
                <LayoutTemplate>
                    <table id="tlbTemplate" class="Grid" cellspacing="0" border="0" runat="server">
                        <tr>
                            <th style="width: 25%" enableviewstate="false">
                                <asp:LinkButton ID="btnOfferDate" runat="server" ToolTip="Sort By Offer Date" CommandName="Sort"
                                    CommandArgument="[MHD].[OfferedDate]" Text="Offer Date" />
                            </th>
                            <th style="width: 35%" enableviewstate="false">
                                <asp:LinkButton ID="btnRequisition" runat="server" ToolTip="Sort By Requisition"
                                    CommandName="Sort" CommandArgument="[J].[JobTitle]" Text="Requisition" />
                            </th>
                            <th style="width: 35%" enableviewstate="false">
                                <asp:LinkButton ID="btnCandidate" runat="server" ToolTip="Sort By Candidate" CommandName="Sort"
                                    CommandArgument="[M].[FirstName]+[M].[LastName]" Text="Candidate" />
                            </th>
                            <th id="thAccepted" runat="server" style="width: 0px" enableviewstate="false">
                                Accepted
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="4" id="tdPager" runat="server">
                                <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                        style="width: 100%; margin: 0px 0px;">
                        <tr>
                            <td>
                                No candidates available.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:Label ID="lblOfferedDate" runat="server" Width="80px" EnableViewState="true" />
                        </td>
                        <td>
                            <asp:HyperLink ID="lnkRequisition"  runat="server" Width="100px" EnableViewState="true"></asp:HyperLink>
                        </td>
                        <td>
                            <asp:HyperLink ID="lnkCandidateName"  runat="server"></asp:HyperLink>
                        </td>
                        <td id="tdAccepted" runat="server"     align="center">
                            <div style="text-align:center">
                                <asp:Image ID="imgAccept" runat="server" ImageUrl="~/Images/check_Image.gif" Style="margin: 0px 0px 0px 0px;float:none;"
                                    ImageAlign="Middle" />
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
