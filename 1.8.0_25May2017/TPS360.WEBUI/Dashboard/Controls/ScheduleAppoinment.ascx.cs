/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName : ScheduleAppoinment.ascx.cs
    Description: Schedules and Appoinments Widget 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            May-05-2009           N.Srilakshmi         Defect ID: 10168; Made changes in pageload and UltraWebTab1_TabClick
-------------------------------------------------------------------------------------------------------------------------------------------       
*/

using System;
using Infragistics.WebUI.WebSchedule;
using Infragistics.WebUI.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using TPS360.Web.UI;
using TPS360.Common.Helper;
public partial class ScheduleAppoinment : BaseControl, IWidget
{
    #region Properties

    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }
    private int MemberId
    {
        get
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                return Int32.Parse(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            else
            {
                return base.CurrentMember.Id;
            }
        }

    }
    
    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        (this as IWidget).HideSettings();
        try
        {
                WebScheduleInfo1.AppointmentFormPath = System.Configuration.ConfigurationManager.AppSettings["Appointment"].ToString();
                WebScheduleInfo1.ReminderFormPath = System.Configuration.ConfigurationManager.AppSettings["Reminder"].ToString();
                WebScheduleSqlClientProvider1.Connection = CreateConnection();
                string url = Request.Url.AbsoluteUri.ToString();
                if (url.Contains("Employee/TaskAndSchedule.aspx"))
                {
                    if (MemberId != CurrentMember.Id)
                    {
                        PrepareView();
                    }
                }
                else
                {
                    PrepareView();
                }
                string uid = UltraWebTab1.UniqueID;
                if (Session[uid] != null)
                {
                    UltraWebTab1.SelectedIndex = Convert.ToInt32(Session[uid]);
                }
                else
                {
                    UltraWebTab1.SelectedIndex = new WidgetInstanceDataAccess().GetStateByHostId(Host.ID);
                }
        }
        catch
        {
        }
        
     }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        (this as IWidget).HideSettings();
    }

    protected void UltraWebTab1_TabClick(object sender, Infragistics.Web.UI.LayoutControls.TabSelectedIndexChangedEventArgs e)
    {
        try
        {
            string uid = UltraWebTab1.UniqueID;
            Session[uid] = ((Infragistics.Web.UI.LayoutControls.WebTab)sender).SelectedIndex;
            new WidgetInstanceDataAccess().UpdateWidgetInstanceState(Session[uid].ToString(), Host.ID);
        }
        catch
        {
        }
    }

    #endregion

    #region Methods

   
    private void PrepareView()
     {




         this.WebScheduleSqlClientProvider1.WebScheduleInfo = this.WebScheduleInfo1;

         this.WebScheduleInfo1.ActiveResourceName = MemberId.ToString();

         WebCalendarView calViewOnTab5 =(Infragistics.WebUI.WebSchedule.WebCalendarView) UltraWebTab1.Tabs[0].FindControl("WebCalendarView1");
         if (calViewOnTab5 != null)
         {
             calViewOnTab5.WebScheduleInfo = this.WebScheduleInfo1;
         }

         WebDayView dayViewOnTab1 = (Infragistics.WebUI.WebSchedule.WebDayView)this.UltraWebTab1.Tabs[1].FindControl("WebDayView1");
         if (dayViewOnTab1 != null)
         {
             dayViewOnTab1.WebScheduleInfoID = this.WebScheduleInfo1.ID;
         }
         WebMonthView monthViewOnTab4 = (Infragistics.WebUI.WebSchedule.WebMonthView)this.UltraWebTab1.Tabs[2].FindControl("WebMonthView1");
         if (monthViewOnTab4 != null)
         {
             monthViewOnTab4.WebScheduleInfo = this.WebScheduleInfo1;
         }
    }

    private static IDbConnection CreateConnection()
    {
        return new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["TPS360CS"].ConnectionString);
    }

    #endregion

    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {
    }
    void IWidget.Closed()
    {
    }


    #endregion

   
}
