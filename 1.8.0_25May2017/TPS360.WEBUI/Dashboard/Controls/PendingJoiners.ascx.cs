﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;

public partial class PendingJoiners: TPS360.Web.UI.BaseControl, IWidget
{
    #region Member Variables
    private bool _IsAccessToCandidate = true ;
    private static string UrlForCandidate = string.Empty;
    private static int SitemapIdForCandidate = 0;
    #endregion

    #region Properties

    private IWidgetHost _Host;

    public IWidgetHost Host
    {
        get { return _Host; }
        set { _Host = value; }
    }

    private string _userId;
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    #endregion
    #region Page Events
    private void setParameters()
    {
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        setParameters();
        (this as IWidget).HideSettings();
        CustomSiteMap CustomMap = new CustomSiteMap();
        CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
        if (CustomMap == null) _IsAccessToCandidate = false;
        else
        {
            SitemapIdForCandidate = CustomMap.Id;
            UrlForCandidate = "~/" + CustomMap.Url.ToString();
        }
        
        if (!IsPostBack)
        {
            GetWidgetData();
            txtSortColumn.Text = "btnEJOJ";
            txtSortOrder.Text = "DESC";
            PlaceUpDownArrow();
        }
        string pagesize = "";
        pagesize = (Request.Cookies["DashboardPendingJoinersRowPerPage"] == null ? "" : Request.Cookies["DashboardPendingJoinersRowPerPage"].Value); ;
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvPendingJoiners.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize); 
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }
    #endregion

    #region Listview Events
    protected void lsvPendingJoiners_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {

            MemberPendingJoiners joiners = ((ListViewDataItem)e.Item).DataItem as MemberPendingJoiners;

            if (joiners != null)
            {
                HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
                HyperLink lnkRequisition = (HyperLink)e.Item.FindControl("lnkRequisition");
                Label lblEDateOfJoining = (Label)e.Item.FindControl("lblEDateOfJoining");
                Label lblJobPostingCode = (Label)e.Item.FindControl("lblJobPostingCode");
                Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
                Label lblActiveRecruiter = (Label)e.Item.FindControl("lblActiveRecruiter");

                string _strFullName = joiners.CandidateName ;
                if (_strFullName.Trim() == "")
                    _strFullName = "No Candidate Name";
                if (_IsAccessToCandidate)
                {
                    if (SitemapIdForCandidate > 0 && UrlForCandidate != string.Empty)
                        ControlHelper.SetHyperLink(lnkCandidateName, UrlForCandidate, string.Empty, _strFullName, UrlConstants.PARAM_MEMBER_ID, joiners .MemberID .ToString(), UrlConstants.PARAM_SITEMAP_ID, SitemapIdForCandidate.ToString() , UrlConstants .PARAM_SITEMAP_PARENT_ID ,"12" );
                }
                else
                {
                    lnkCandidateName.Enabled = false;
                    lnkCandidateName.Text = _strFullName;// submission.ApplicantName;
                }
                    lnkRequisition.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + joiners.JobPostingID + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    lnkRequisition.Text = joiners.JobTitle;

                lblCandidateID.Text = "A" + joiners.MemberID.ToString();
                lblJobPostingCode.Text = joiners.JobPostingCode;
                lblEDateOfJoining.Text = joiners.ExpectedDateOfJoining.ToShortDateString();
                lblActiveRecruiter.Text = joiners.ActiveRecruiterName;

                if(Convert .ToDateTime ( joiners .ExpectedDateOfJoining .ToShortDateString ())< Convert .ToDateTime (DateTime .Now .ToShortDateString ()))
                {
                    System .Web .UI.HtmlControls .HtmlTableRow row=(System .Web .UI .HtmlControls .HtmlTableRow ) lblCandidateID .Parent .Parent ;
                    if (row != null) row.Style.Add("background-color", "#ff8566");
                }


            }
        }
    }

    protected void lsvPendingJoiners_PreRender(object sender, EventArgs e)
    {
        lsvPendingJoiners.DataBind();
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvPendingJoiners.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                if (pager.Controls.Count >= 1)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardPendingJoinersRowPerPage";
        }
        PlaceUpDownArrow();
        if (lsvPendingJoiners != null)
        {
            if (lsvPendingJoiners.Items.Count == 0)
            {
                lsvPendingJoiners.DataSource = null;
                lsvPendingJoiners.DataBind();
            }
        }

        
    }

    protected void lsvPendingJoiners_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        try
        {
            if (string.Equals(e.CommandName, "Email"))
            {
                string[] ParamID = e.CommandArgument.ToString().Split('$');

                string sub_MemberEmailId = ParamID[0];
                string sub_ClientID = ParamID[1];
                string sub_SubmissionId = ParamID[2];
                string sub_MemberId = ParamID[3];
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.CommonPages.EMAIL_PREVIEW.Substring(3), string.Empty, UrlConstants.PARAM_MemberEmail_ID, sub_MemberEmailId, UrlConstants.PARAM_COMPANY_ID, sub_ClientID, UrlConstants.PARAM_JOB_ID, sub_SubmissionId, UrlConstants.PARAM_APPLICANT_ID, sub_MemberId);
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "EmailDashBoard", "<script>window.open('" + url + "');</script>", false);
            }
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (txtSortColumn.Text == lnkbutton.ID)
                {
                    if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                    else txtSortOrder.Text = "ASC";
                }
                else
                {
                    txtSortColumn.Text = lnkbutton.ID;
                    txtSortOrder.Text = "ASC";
                }

            }
        }
        catch
        {
        }
    }
    #endregion
    #region Methods
    public void GetWidgetData()
    {
        try
        {
            //odsSubmissions.SelectParameters["MemberId"].DefaultValue = base.CurrentMember.Id.ToString();
            this.lsvPendingJoiners.DataBind();
        }
        catch
        {
        }
    }

    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvPendingJoiners.FindControl(txtSortColumn.Text);
            System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }
    #endregion
    #region Implementation

    void IWidget.Init(IWidgetHost host)
    {
        this.Host = host;
    }

    void IWidget.ShowSettings()
    {
        pnlSettings.Visible = true;
    }
    void IWidget.HideSettings()
    {
        pnlSettings.Visible = false;
    }
    void IWidget.Minimized()
    {
    }
    void IWidget.Maximized()
    {
       
        if (lsvPendingJoiners != null)
        {
            if (lsvPendingJoiners.Items.Count == 0)
            {
                lsvPendingJoiners.DataSource = null;
                lsvPendingJoiners.DataBind();
            }
        }
    }
    void IWidget.Closed()
    {
    }

    #endregion
}
