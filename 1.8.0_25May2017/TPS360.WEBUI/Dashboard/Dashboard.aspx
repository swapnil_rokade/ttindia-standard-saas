<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Dashboard.master" AutoEventWireup="true" ValidateRequest="false"
    CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" Title="Employee Dashboard" %>
<%@ Register Src="~/Dashboard/Controls/WidgetContainer.ascx" TagName="WidgetContainer" TagPrefix="widget" %>
<%@ Register src="~/Dashboard/Controls/WidgetPanels.ascx" tagname="WidgetPanels" tagprefix="uc3" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHomeMaster" runat="Server">
  <style>
  .calHeaderStyle
  {
  text-align:center;	
  }
  </style>
  <link href =  "../assets/css/daterangepicker.css" type ="text/css" rel ="Stylesheet" />
<script >
    function DeleteDashboardPage(pageId)
    {
        if(confirm('Are you sure that you want to delete this widget page?'))
        {
            DeletePage(pageId );
        }
    }
 

Sys.Application.add_load(function() { 

 $('#btnAddWidget').unbind('click').click(function (){
 $('#menuPanels').toggle();
     if($('#menuPanels').is(':visible'))
            {$('#btnAddWidget').addClass('active');
            }
            else{ $('#btnAddWidget').removeClass('active');
            }
 });
 
 if($('.collapse').size()==0)
 {
 iNettuts.addWidgetControls();
 }
 iNettuts.makeSortable();

 });
 
 function saveWidgetLocation(args)
 {

 // WidgetService.DeleteWidgetInstance(InstanceId);
 //alert('d');
WidgetService.UpdateWidgetInstancePosition(args )
//alert (row );
 }
 {
var rowCount=0;
var movArr=''
 $(" ul#<%=PnlLeft.ClientID %>  > li").each(function(){ 
 var Id=$(this).attr("id");
 var CurrentId=Id.substr(Id.lastIndexOf("_")+1);
 movArr =movArr + ';'+ CurrentId + ',0,'+ rowCount ;
 rowCount +=1;
    });
     rowCount=0;
    $(" ul#<%=PnlRight.ClientID %>  > li").each(function(){ 
 var Id=$(this).attr("id");
 var CurrentId=Id.substr(Id.lastIndexOf("_")+1);
 movArr =movArr + ';'+ CurrentId + ',1,'+ rowCount ;
 rowCount +=1;
    });
    rowCount=0;
     $(" ul#<%=PnlFull.ClientID %>  > li").each(function(){ 
 var Id=$(this).attr("id");
 var CurrentId=Id.substr(Id.lastIndexOf("_")+1);
 movArr =movArr + ';'+ CurrentId + ',2,'+ rowCount ;
 rowCount +=1;
    });
 saveWidgetLocation(movArr );
 }
 function DeleteNewWidget(s)
 {

    var Id =$(s).find("li:first").attr('id');
 
    var CurrentId = Id.substr(Id.lastIndexOf("_")+1);
    WidgetService.DeleteWidgetInstance(CurrentId);
 
 }
 
</script>
    <asp:ScriptManagerProxy runat="server">
        <Services>
            <asp:ServiceReference InlineScript="true" Path="~/UserPageService.asmx" />
            <asp:ServiceReference InlineScript="true" Path="~/WidgetService.asmx" />
        </Services>
        <Scripts>
            <asp:ScriptReference Path="../Scripts/Dashboard.js" />
        </Scripts>
    </asp:ScriptManagerProxy>
    <div id="body">
        <asp:UpdatePanel ID="updPanelMessage" runat="server" UpdateMode="conditional">
            <ContentTemplate>
                <center>
                    <div align="center" style="width:50%; text-align:center">
                        <asp:panel id="ErrorPanel" class="CommonMessageError" runat="server" visible="false">
	                         <asp:label id="Message" runat="server"/>
                        </asp:panel>
                    </div>
                </center>
            </ContentTemplate>
        </asp:UpdatePanel>
<%--   <div style =""--%>
        <asp:UpdatePanel ID="UpdatePanelTabAndLayout" runat="server" UpdateMode="conditional">
            <ContentTemplate>
                <asp:UpdatePanel ID="TabUpdatePanel" runat="server" UpdateMode="conditional">
                    <ContentTemplate>                        
                        <div style="width: 100%;">
    	                    <div id="TabContainerBar">
        	                    <div id="TabContainer"><ul runat="server" id="tabList"></ul>
        	                    </div>
        	                </div>
                            <div id="TabBottomBar"></div>
                            <asp:Panel ID="pnlWidget" runat="Server">
                                <asp:UpdatePanel ID="AddContentUpdatePanel" runat="server" UpdateMode="conditional">
                                    <ContentTemplate>
                                        <div style="z-index: 10; text-align:right;  padding-bottom : 10px;">
                                            <button class="btn btn-small" type="button" id="btnAddWidget">Add Widgets <i class="icon icon-cog"></i></button>
                                        </div>
                                        <div id="menuPanels" style="border:1px solid #cccccc;padding:0px 0px 10px 0px; display : none " class="well">
                                            <asp:Panel ID="AddContentPanel" runat="Server" Height="100%" >
                                                <asp:DataList ID="WidgetDataList" CssClass="widgetDataList" runat="server" RepeatDirection="Horizontal"  RepeatColumns="6" RepeatLayout="Table" CellPadding="3" CellSpacing="3" EnableViewState="False" ShowFooter="False" ShowHeader="False" Width="100%" OnItemDataBound="WidgetDataList_ItemDataBound" OnItemCommand="WidgetDataList_ItemCommand">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgBtnAddWidget" ImageUrl='<%# Eval("Icon") %>' ImageAlign="AbsMiddle" ToolTip='<%# Eval("Description") %>' Width ="24px" Height ="24px" runat="server" />
                                                        &nbsp;<asp:LinkButton CommandName="AddWidget" ID="btnAddWidget" runat="server" CssClass="navLink"><%# Eval("Description")%></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </asp:Panel>            
                                        </div> 
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="UpdatePanelLayout" runat="server" UpdateMode="conditional">        
                    <ContentTemplate>
                  
                                        
     <link href="inettuts.css" rel="stylesheet" type="text/css" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <div id="Fullcolumns">
    <ul id="PnlFull" class ="column" runat ="server" style =" width:100%; " >
     </ul>
     </div>
    <div id="columns">
   
        <ul  ID ="PnlLeft"  class   ="column"  runat ="server"   >
       
        </ul>
        <ul id="PnlRight" class="column"  runat ="server"  style ="float :right "  >
           
       
        
    
            
        </ul>
    </div>


<script type="text/javascript" src="jQuery1.7.2.js"></script>
    <script type="text/javascript" src="jquery-ui.min1.8.7.js"></script>
    <script type="text/javascript" src="inettuts.js"></script>
                        <script type ="text/javascript" src ="../assets/js/daterangepicker.js"></script>
                    </ContentTemplate>
                </asp:UpdatePanel>
             </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
