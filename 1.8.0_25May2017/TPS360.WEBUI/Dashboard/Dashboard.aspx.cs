using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Linq;
using TPS360.Web.UI;
using TPS360.Common.BusinessEntities;

public partial class Dashboard : AdminBasePage
{
    private const string WIDGET_CONTAINER = "DashboardColumn1.ascx";
    private string[] updatePanelIDs = new string[] { "LeftUpdatePanel", "RightUpdatePanel", "CenterUpdatePanel", "ScheduleUpdatePanel" };
    
    private UserPageSetup _Setup
    {
        get { return Context.Items[typeof(UserPageSetup)] as UserPageSetup; }
        set { Context.Items[typeof(UserPageSetup)] = value; }
    }

    private int AddStuffPageIndex
    {
        get { object val = ViewState["AddStuffPageIndex"]; if (val == null) return 0; else return (int)val; }
        set { ViewState["AddStuffPageIndex"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            RefreshAllColumns();
        }
        this.Title = base.CurrentMember.FirstName + " " + base.CurrentMember.LastName + " - Dashboard"; 
    }

    protected override void CreateChildControls()
    {
        base.CreateChildControls();

        this.LoadUserPageSetup(false);
        this.LoadAddStuff();
        ResetMessagePanel();

        if (ScriptManager.GetCurrent(this).IsInAsyncPostBack)
        {
            string updatePanelName = Request[ScriptManager.GetCurrent(this).UniqueID];

            if (updatePanelName.Contains(UpdatePanelLayout.ID))
            {
               // this.WidgetPanelsLayout.SetLayout(_Setup.CurrentPage.LayoutType);
                this.SetupWidgets(wi => false);
            }
            else if (updatePanelName.Contains(TabUpdatePanel.ID))
            {
                this.SetupTabs();
            }
            else
            {
                this.SetupTabs();
              //  this.WidgetPanelsLayout.SetLayout(_Setup.CurrentPage.LayoutType);
                this.SetupWidgets(wi => false);
            }
        }
        else
        {
           // this.WidgetPanelsLayout.SetLayout(_Setup.CurrentPage.LayoutType);
            this.SetupWidgets(wi => true);
            this.SetupTabs();
        }
    }    

    private void LoadUserPageSetup(bool noCache)
    {
        UserPageDataAccess upda = new UserPageDataAccess();
        int pageCount = upda.GetUserPageCount(base.CurrentMember.Id);

        if (pageCount == 0)
        {
            //new user. Create page for him.

            UserPage page = new UserPage();
            page.Title = "Dashboard";
            page.UserId = base.CurrentMember.Id;
            upda.AddUserPage(page);

            //set the current page
            UserPageSetting userPageSetting = new UserPageSetting();
            userPageSetting.CurrentPageId = page.Id;
            userPageSetting.UserId = base.CurrentMember.Id;
            new UserPageSettingDataAccess().AddUserPageSetting(userPageSetting);

            
            List<Widget> defaultWidgetList = new WidgetDataAccess().GetAllWidget(true);
            
            if(defaultWidgetList != null && defaultWidgetList.Count > 0)
            {
                _Setup = new UserPageSetup(base.CurrentMember.Id);
                //add widget to instance in a page
                foreach (Widget widget in defaultWidgetList)
                {
                    WidgetInstance wi = new WidgetInstance();
                    wi.Title = widget.Name;
                    wi.PageId = _Setup.CurrentPage.Id;
                    wi.State = string.Empty;
                    wi.WidgetId = widget.Id;
                    wi.Expanded = true;
                    wi.State = widget.DefaultState;

                    WidgetInstanceDataAccess wida = new WidgetInstanceDataAccess();
                    wida.AddWidgetInstance(wi);
                    wida.UpdateWidgetInstancePosition(_Setup.CurrentPage.Id);
                }                
            }
        }

        _Setup = Cache[base.CurrentMember.Id.ToString()] as UserPageSetup;
        if (noCache || null == _Setup)
            _Setup = new UserPageSetup(base.CurrentMember.Id);
    }

    private void SetupTabs()
    {
        tabList.Controls.Clear();

        var setup = _Setup;
        var currentPage = setup.CurrentPage;

        foreach (UserPage page in setup.Pages)
        {
            var li = new HtmlGenericControl("li");
            li.ID = "Tab" + page.Id.ToString();
            var linkButton = new LinkButtonWithLayout();
            linkButton.ID = page.Id.ToString();
            linkButton.Text =MiscUtil .RemoveScript ( page.Title);
            linkButton.CommandName = "ChangePage";
            linkButton.CommandArgument = page.Id.ToString();

            if (page.Id == currentPage.Id)
            {
                linkButton.Click += new EventHandler(PageTitleEditMode_Click);
            }
            else
            {
                linkButton.OnClientClick = "ChangePage('" + page.Id + "'); return false;";                
            }

            linkButton.CssClass = page.Id == currentPage.Id ? "selected" : "";


            li.Controls.Add(linkButton);
            tabList.Controls.Add(li);
        }

        var addNewTabImageButton = new ImageButton();
        addNewTabImageButton.ID = "AddNewPage";
        addNewTabImageButton.ToolTip = "Add New Page";
        addNewTabImageButton.ImageUrl = "../Images/Dashboard/Add.png";
        addNewTabImageButton.ImageAlign = ImageAlign.AbsBottom;
        addNewTabImageButton.OnClientClick = "NewPage('0'); return false;";
        addNewTabImageButton.Attributes.Add("class", "AddNewPage");

        var li2 = new HtmlGenericControl("li");
        li2.Controls.Add(addNewTabImageButton);
        tabList.Controls.Add(li2);
    }

    void PageTitleEditMode_Click(object sender, EventArgs e)
    {
        var linkButton = sender as LinkButton;
        var editTextBox = new TextBox();
        editTextBox.ID = "PageNameEditTextBox";
        editTextBox.Text =MiscUtil .RemoveScript ( linkButton.Text,string .Empty );
        editTextBox.CssClass = "CommonTextBox";

        var okButton = new ImageButton();
        okButton.ImageUrl = "../Images/EditImage.png";
        okButton.ToolTip = "Save Page Label";
        okButton.ID = "SavePageLabel";
        okButton.OnClientClick = "RenamePage('" + this._Setup.CurrentPage.Id + "', '" + editTextBox.UniqueID + "'); ";


        var deleteButton = new ImageButton();
        deleteButton.ImageUrl = "../Images/DeleteImage.png";
        deleteButton.ToolTip = "Remove Page";
        deleteButton.ID = "DeletePage";
        deleteButton.OnClientClick = "DeleteDashboardPage('" + this._Setup.CurrentPage.Id + "'); return false; ";

        linkButton.Parent.Controls.Add(editTextBox);

        var labelspacer = new Label();
        labelspacer.Text = "  ";

        linkButton.Parent.Controls.Add(okButton);
        linkButton.Parent.Controls.Add(labelspacer);
        
        if (_Setup.Pages.Count > 1)
        {
            linkButton.Parent.Controls.Add(deleteButton);
        }

        linkButton.Parent.Controls.Remove(linkButton);
    }    

    void PageLinkButton_Click(object sender, EventArgs e)
    {
        var linkButton = sender as LinkButton;

        int pageId = int.Parse(linkButton.CommandArgument);

        if (_Setup.UserSetting.CurrentPageId != pageId)
        {
            _Setup.UserSetting.CurrentPageId = _Setup.CurrentPage.Id = pageId;
            new UserPageSettingDataAccess().UpdateUserPageSetting(_Setup.UserSetting);
            
            this.ReloadPage(wi => true);
            this.RefreshAllColumns();
        }
    }

    private void SetupWidgets(Func<WidgetInstance, bool> isWidgetFirstLoad)
     {

         var setup = _Setup;
         WidgetInstanceDataAccess _widgetAccess = new WidgetInstanceDataAccess();
         MemberCustomRoleMap rol = Facade.GetMemberCustomRoleMapByMemberId(base.CurrentMember.Id);
         bool IsPermit = false;
         //foreach (Panel panel in PnlFull )
         //{
         //    List<WidgetContainer> widgets = panel.Controls.OfType<WidgetContainer>().ToList();
         //    foreach (var widget in widgets) panel.Controls.Remove(widget);
         //}
         List<DashboardColumn1> widgets = PnlFull.Controls.OfType<DashboardColumn1>().ToList();
         foreach (var widget in widgets) PnlFull.Controls.Remove(widget);
         List<DashboardColumn1> widgets1 = PnlLeft.Controls.OfType<DashboardColumn1>().ToList();
         foreach (var widget in widgets1) PnlLeft.Controls.Remove(widget);
         List<DashboardColumn1> widgets2 = PnlRight.Controls.OfType<DashboardColumn1>().ToList();
         foreach (var widget in widgets2) PnlRight.Controls.Remove(widget);
         bool IsDisplay = false;
         bool IsSchedule = false;
         if (setup.WidgetInstances != null && setup.WidgetInstances.Count > 0)
         {
             foreach (WidgetInstance instance in setup.WidgetInstances)
             {
                 
                 var panel = new HtmlGenericControl();// Panel();
                 var pansc = PnlLeft;// rere;// columnPanels[3];
                 if (instance.Title.Trim() == "Schedule & Appointments")
                 {
                     panel = PnlFull;
                 }
                 else if (instance.Title.Trim() == "Graphs")
                 {
                     panel = PnlFull;
                 }
                 else if (instance.Title.Trim() == "My Recent Requisitions" || instance.Title.Trim() == "All Recent Requisitions" || instance.Title.Trim() == "Productivity" || instance.Title.Trim() == "My Productivity" || instance.Title.Trim() == "All Recent Status Changes")
                 {
                     panel = PnlFull;
                 }
                 else
                 {
                     if (instance.ColumnNo == 0)
                     {
                         panel = PnlLeft;
                     }
                     else panel = PnlRight;
                 }
                 var widget = LoadControl(WIDGET_CONTAINER) as DashboardColumn1;
                 widget.ID = "WidgetContainer" + instance.Id.ToString();
                 // widget.IsFirstLoad = isWidgetFirstLoad(instance);
                 widget.WidgetInstance = instance;
                 IsPermit = _widgetAccess.GetWidgetAccessByRoleId(rol != null ? rol.CustomRoleId : 0, instance.WidgetId);
                 if (IsPermit) AddControls(widget, panel, instance.Title, instance.Id);// panel.Controls.Add(widget);
             }
         }
    }
    private void AddControls(Control widget, HtmlGenericControl panel, string title, int id)
    {
        HtmlGenericControl li = new HtmlGenericControl("li");
        li.ID = id.ToString();
        li.Attributes.Add("class", "widget WidgetColor");
        HtmlGenericControl div1 = new HtmlGenericControl("div");
        div1.Attributes.Add("class", "widget-head");
        //if (title == "Current Sessions")
        //{
        //    title = "Graphs";
        //}
        div1.InnerHtml = "<H3>" + title + "</H3>";
        li.Controls.Add(div1);
        HtmlGenericControl div2 = new HtmlGenericControl("div");
        div2.Attributes.Add("class", "widget-content");
        UpdatePanel up = new UpdatePanel();

        //ASP.dashboard_controls_mysubmissions_ascx ob = new ASP.dashboard_controls_mysubmissions_ascx();
        up.ContentTemplateContainer.Controls.Add(widget);
        div2.Controls.Add(up);

        li.Controls.Add(div2);
        //if (title.Trim () == "Schedule & Appointments")
        //{
        //    panel.Controls.AddAt(1, li);
        //}
        if (title.Trim() == "Graphs")
        {
            panel.Controls.AddAt(0, li);
        }
        else panel.Controls.Add(li);

    }
    void widget_Deleted(WidgetInstance obj)
    {
        this.ReloadPage(wi => false);
        this.RefreshColumn(obj.ColumnNo);
        if (obj.Title.Trim() == "Recent Requisitions")
            this.RefreshColumn(2);
    }

    private void ReloadPage(Func<WidgetInstance, bool> isWidgetFirstLoad)
    {
        this.LoadUserPageSetup(true);
        this.SetupTabs();

        this.SetupWidgets(isWidgetFirstLoad);
    }

    private void RefreshAllColumns()
    {
        this.RefreshColumn(0);
        this.RefreshColumn(1);
        this.RefreshColumn(2);
        this.RefreshColumn(3);
    }

    private void RefreshColumn(int columnNo)
    {
       // var updatePanel = this.WidgetPanelsLayout.FindControl(this.updatePanelIDs[columnNo]) as UpdatePanel;
       // updatePanel.Update();
    }

    protected void ShowAddContentPanel_Click(object sender, EventArgs e)
    {
       // AddContentPanel.Visible = true;
        //HideAddContentPanel.Visible = true;
       // ShowAddContentPanel.Visible = false;
    }

    protected void HideAddContentPanel_Click(object sender, EventArgs e)
    {
       // AddContentPanel.Visible = false;
       // HideAddContentPanel.Visible = false;
       // ShowAddContentPanel.Visible = true;
    }

    private List<Widget> WidgetList
    {
        get
        {
            List<Widget> widgets = Cache["Widgets"] as List<Widget>;
            
            if (null == widgets)
            {
                widgets = new List<Widget>();

                MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(base.CurrentMember.Id);

                if (map != null)
                {
                    List<Widget> permittedWidgets = new WidgetDataAccess().GetAllWidgetByRole(map.CustomRoleId);

                    if (permittedWidgets != null && permittedWidgets.Count > 0)
                    {
                        foreach (Widget w in permittedWidgets)
                        {
                            widgets.Add(w);
                        }
                    }
                }

                pnlWidget.Visible = widgets.Count > 0;
            }

            return widgets;
        }
    }

    private void LoadAddStuff()
    {
        if (WidgetList == null && WidgetList.Count == 0)
        {
            Message.Text = "You do not have previlized to view widgets. Please contact with administration.";
            //ShowAddContentPanel.Enabled = false;
            ResetMessagePanel();
        }

        this.WidgetDataList.DataSource = WidgetList;
        this.WidgetDataList.DataBind();
    }

    protected void WidgetDataList_ItemCommand(object source, DataListCommandEventArgs e)
    {
        int widgetId = int.Parse(e.CommandArgument.ToString());

        WidgetInstanceDataAccess wida = new WidgetInstanceDataAccess();
        bool alreadyExists = wida.CheckIfAlreadyExists(_Setup.CurrentPage.Id, widgetId);

        if (!alreadyExists)
        {
            Widget widget = new WidgetDataAccess().GetWidget(widgetId);

            WidgetInstance wi = new WidgetInstance();
            wi.Title = widget.Name;
            wi.PageId = _Setup.CurrentPage.Id;
            wi.State = string.Empty;
            wi.WidgetId = widget.Id;
            wi.Expanded = true;
            wi.State = widget.DefaultState;
            wi.OrderNo = widget.OrderNo;
         
            wida.AddWidgetInstance(wi);

     
         //   _Setup = new UserPageSetup(base.CurrentMember.Id);
         ////   SetupWidgets (wii => false );
            WidgetInstance newWidget= wida.GetLastAddedWidgetInstanceByPageId(_Setup.CurrentPage.Id);
            var widgetnew = LoadControl(WIDGET_CONTAINER) as DashboardColumn1;
           widgetnew.ID = "WidgetContainer" + newWidget.Id.ToString();// instance.Id.ToString();
            widgetnew.WidgetInstance = newWidget;
            if (newWidget.Title.Trim() == "Schedule & Appointments" || newWidget.Title.Trim() == "Graphs" || newWidget.Title.Trim() == "My Recent Requisitions" || newWidget.Title.Trim() == "All Recent Requisitions" || newWidget.Title.Trim() == "Productivity" || newWidget.Title.Trim() == "My Productivity" || newWidget.Title.Trim() == "All Recent Status Changes")
              {
                             AddControls(widgetnew, PnlFull , newWidget .Title , newWidget .Id );
              }
              else           AddControls(widgetnew, PnlLeft, newWidget .Title , newWidget .Id );
            Message.Text = "Successfully added widget to Dashboard. Please <a href='' onclick='window.location.reload()'>reload the page.</a>";
            ErrorPanel.Visible = true ;
            updPanelMessage.Update();
            UpdatePanelLayout.Update();
        }
        else
        {
            Message.Text = "This widget has already been added to the page.";
            ErrorPanel.Visible = true;
            updPanelMessage.Update();
        }
    }

    protected void WidgetDataList_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Widget widget = e.Item.DataItem as Widget;

            ImageButton imdAddWidget = e.Item.FindControl("imgBtnAddWidget") as ImageButton;
            LinkButton btnAddWidget = e.Item.FindControl("btnAddWidget") as LinkButton;

            imdAddWidget.CommandName = btnAddWidget.CommandName = "AddWidget";
            imdAddWidget.CommandArgument = btnAddWidget.CommandArgument = widget.Id.ToString();
        }
    }

    void ResetMessagePanel()
    {
        ErrorPanel.Visible = false;
        updPanelMessage.Update();
    }
}
 