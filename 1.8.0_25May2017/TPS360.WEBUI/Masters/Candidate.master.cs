﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Collections;
using System.Collections.Generic;
using TPS360.Common.Shared;
using System.Web.Security;
namespace TPS360.Web.UI
{
    public partial class CandidateMaster : CandidateMasterPage
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
                {
                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage();
                    return;
                }
            }
            catch { }

            //helpModalPopupExt.Show();
            Response.Expires = -1;  //10573 
            
            if (!IsPostBack)
            {
                
                ctlOverviewMenu.CurrentSiteMapType = (int)SiteMapType.CandidateOverviewMenu;
                ctlOverviewMenu.EnablePermissionChecking = true;
                ctlOverviewMenu.OverviewType = OverviewType.CandidateOverview;
                if (!MiscUtil.ReturnDashboard(Request.RawUrl, CurrentMember.Id, Facade))
                {
                    Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
                }
            }
            //helpModalPopupExt.Hide();
        }

        
    }
}