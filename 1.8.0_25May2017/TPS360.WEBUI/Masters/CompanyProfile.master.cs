﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using TPS360.Providers;
using System.Text;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
namespace TPS360.Web.UI
{
    public partial class CompanyProfile : CompanyMasterPage
    {
        string _currentNode = string.Empty;
        string _currentParentNode = string.Empty;

        public override void RenderCurrentCompany()
        {
            Company currentCompany = base.CurrentCompany;

            if (currentCompany.IsNew)
            {

                RenderCompany(false);
            }
            else
            {

                RenderCompany(true);
            }
        }
        private void RenderCompany(bool isVisible)
        {
            Company currentCompany = base.CurrentCompany;

            if (isVisible)
            {
               // RenderMenu();
            }
            else
            {
                uclOverview.Visible = false;//0.1
            }
        }
        //private void RenderMenu()
        //{
        //    uclOverview.CurrentSiteMapType = (int)SiteMapType.CompanyOverviewMenu;
        //    uclOverview.EnablePermissionChecking = true;
        //    uclOverview.OverviewType = OverviewType.CompanyOverview;
        //    uclOverview.Visible = true;//0.1
        //}
       
        protected void Page_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
            //    {
            //        FormsAuthentication.SignOut();
            //        FormsAuthentication.RedirectToLoginPage();
            //        return;
            //    }
            //}
            //catch { }

            //helpModalPopupExt.Show();
            Response.Expires = -1;  //10573 

            if (!IsPostBack)
            {
                lblCandidateName.Text = CurrentCompany.CompanyName;
              
                uclOverview.EnablePermissionChecking = true;

                string currentSiteMapId = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
                if (currentSiteMapId == "11")
                {
                    uclOverview.CurrentSiteMapType = (int)SiteMapType.CompanyOverviewMenu;
                    uclOverview.OverviewType = OverviewType.CompanyOverview;
                }
                else if(currentSiteMapId =="638")
                {
                    uclOverview.CurrentSiteMapType = (int)SiteMapType.VendorProfileMenu ;
                    uclOverview.OverviewType = OverviewType.VendorProfileMenu ;
                }
                else
                {
                    uclOverview.CurrentSiteMapType = (int)SiteMapType.DepartmentOverviewMenu ;
                    uclOverview.OverviewType = OverviewType.DepartmentOverview;
                }
                uclOverview.Visible = true;//0.1
                if (!MiscUtil.ReturnDashboard(Request.RawUrl, CurrentMember.Id, Facade))
                {
                    Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
                }
            }
        }      

    }
}