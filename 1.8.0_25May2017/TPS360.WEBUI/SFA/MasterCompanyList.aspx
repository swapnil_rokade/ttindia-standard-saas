﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="MasterCompanyList.aspx.cs" EnableEventValidation="false" 
    Inherits="TPS360.Web.UI.Admin.MasterCompanyList" Title="Master Account List" %>
<%@ Register src="../Controls/SearchCompany.ascx" tagname="SearchCompany" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="cntCompanyListTitle" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
  <asp:Label ID="lblCompanyHeader" runat ="server" Text ="Master Account List"></asp:Label>
</asp:Content>
<asp:Content ID="cntCompanyList" ContentPlaceHolderID="cphHomeMaster" runat="Server">
<script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>

</script>
    <uc1:SearchCompany ID="cltSearchCompany" runat="server" />
    <asp:Panel ID="pnlmodal" runat="server" style="display: none">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID ="Modal" runat ="server" TargetControlID ="pnlmodal" PopupControlID ="pnlmodal"
             BackgroundCssClass ="divModalBackground" ></ajaxToolkit:ModalPopupExtender> 
</asp:Content>