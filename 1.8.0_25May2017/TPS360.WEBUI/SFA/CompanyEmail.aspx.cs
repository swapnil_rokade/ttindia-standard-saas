﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.Common.Helper;
using TPS360.Common.BusinessEntities;
using System.Collections.Generic;


public partial class SFA_CompanyEmail : CompanyBasePage
{
    #region Member Variables
    private int _companyId = 0;
    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
             
        string name=string .Empty ;
        if (!IsPostBack)
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID ]))
            {
                _companyId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]);
            }
        }
        if (_companyId > 0)
        {
            name = MiscUtil.GetCompanyNameById(_companyId, Facade); 
            this.Page.Title = name + " - " + "Email";
        }
        ucntrlEmailEditor.EmailFromType =ucntrlAllEmailList.EmailFromType=ucntrlSendEmailList.EmailFromType =ucntrlReceivedEmailLis.EmailFromType = EmailFromType.CompanyEmail;
        IList<CompanyContact> contact = new List<CompanyContact>();
        contact = Facade.GetAllCompanyContactByComapnyId(CurrentCompanyId );
        if (contact == null)
        {
            dvHide.Visible = false;
            tblEmptyData.Visible = true;
        }
        else
        {
            dvHide.Visible =true ;
            tblEmptyData.Visible = false ;
        }
    }
    #endregion
}
