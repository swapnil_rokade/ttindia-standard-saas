﻿<%--/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CompanyEmail.aspx
    Description: This is the page  for sending emails and it is inherited "EmailEditor.ascx" control pages
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Oct-15-2008          Jagadish N            Defect id: 8905;put "ucntrlEmailEditor" control out side of tha panel
--------------------------------------------------------------------------------------------------------------------------------       
 *--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/CompanyProfile.master" AutoEventWireup="true"
    CodeFile="CompanyEmail.aspx.cs" Inherits="SFA_CompanyEmail" EnableEventValidation="false"
    Title="CompanyEmail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/EmailEditor.ascx" TagName="EmailEditor" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AllEmailList.ascx" TagName="AllEmailList" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/SendEmailList.ascx" TagName="SendEmailList" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ReceivedEmailList.ascx" TagName="ReceivedEmailList"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCompanyMaster" runat="Server">

    <script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>

    <script src="../Scripts/fixWebkit.js" type="text/javascript"></script>

    <script type="text/javascript">
var ModalProgress ='<%= Modal.ClientID %>';
    </script>

    <div class="BoxContainer" id="tblEmptyData" runat="server" visible="false" style="height: 40px;
        min-height: 30px;">
        <table class="EmptyDataTable alert alert-warning">
            <tr>
                <td>
                    Please create a contact before sending email.
                </td>
            </tr>
        </table>
    </div>
    
    <div id="dvHide" runat="server">
    <div class="TabPanelHeader">
        Email
    </div>
        <div class="tabbable">
            <!-- Only required for left/right tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab">New Email</a></li>
                <li><a href="#tab2" data-toggle="tab">All Emails</a></li>
                <li><a href="#tab3" data-toggle="tab">Sent Emails</a></li>
               <%--<li><a href="#tab4"   data-toggle="tab">Received Emails</a></li>--%> <%-- code commented by pravin khot on 2/Jun/2016 --%>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                    <uc1:EmailEditor ID="ucntrlEmailEditor" runat="server" />
                </div>
                <div class="tab-pane" id="tab2">
                    <uc1:AllEmailList ID="ucntrlAllEmailList" runat="server" />
                </div>
                <div class="tab-pane" id="tab3">
                    <uc1:SendEmailList ID="ucntrlSendEmailList" runat="server" />
                </div>
                <div class="tab-pane" id="tab4" visible="false" >
                    <uc1:ReceivedEmailList ID="ucntrlReceivedEmailLis" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlmodal" runat="server" Style="display: none">
        <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
            <ContentTemplate>
                <img src="../Images/AjaxLoading.gif" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="Modal" runat="server" TargetControlID="pnlmodal"
        PopupControlID="pnlmodal" BackgroundCssClass="divModalBackground">
    </ajaxToolkit:ModalPopupExtender>
</asp:Content>
