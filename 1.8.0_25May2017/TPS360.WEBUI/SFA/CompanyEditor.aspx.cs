﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CompanyEditor.aspx.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                 Author                 Modification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              May-27-2009        Nagarathna V.B      Defect ID: 10500; code has been added for the replacement of the user control.
 *  0.2              Jun-05-2009        Veda                Defect ID:10562; Document cannot be uploaded if the company details are not available.(added if condition)
    ---------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using System.IO;
using TPS360.Common.BusinessEntities;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Web.UI.WebControls;

namespace TPS360.Web.UI.Admin
{
    public partial class CompanyEditor : CompanyBasePage
    {
      
        #region Member Variables

        #endregion

        #region Properties

        public StringDictionary UploadedFiles
        {
            get
            {
                if (Helper.Session.Get("companyFileUpload") == null)
                {
                    Helper.Session.Set("companyFileUpload", new StringDictionary());
                }

                return (StringDictionary)Helper.Session.Get("companyFileUpload");
            }
            set
            {
                Helper.Session.Set("companyFileUpload", value);
            }
        }

        #endregion

        #region Methods
        private void PrepareView()
        {
            UploadedFiles.Clear();

            string docPath = UrlConstants.GetCompanyDocumentDirectory(CurrentCompanyId.ToString());
            string filePath = null;

            IList<CompanyDocument> companyDocuments = Facade.GetAllCompanyDocumentByCompanyId(CurrentCompanyId);

            if ((companyDocuments != null) && (companyDocuments.Count > 0))
            {
                foreach (CompanyDocument companyDocument in companyDocuments)
                {
                    filePath = Path.Combine(docPath, companyDocument.FileName);
                    //UploadedFiles.Add(companyDocument.Title, filePath);
                }
            }
        }

        #endregion

        #region Events

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ctlCompany.OnSaved += new Action<int>(SetCurrentCompanyId);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PrepareView();
                if (base.CurrentCompanyId != 0)
                {
                    Page.Title = base.CurrentCompany.CompanyName + " - " + "Editor";
                }
                else
                {
                    Page.Title = "Company Editor";
                }
                hdnPageTitle.Value = Page.Title;
           
            }
            Page.Title = hdnPageTitle.Value;  
        }

        void SetCurrentCompanyId(int id)
        {
            CurrentCompanyId = id;
           
        }

        #endregion
}
}