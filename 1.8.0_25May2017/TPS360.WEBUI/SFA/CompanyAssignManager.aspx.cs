﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CompanyAssignManager.aspx.cs
    Description: This is the aspx.cs page used to assign managers to company.
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author                 Modification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              Jan-22-2009           Jagadish              Defect ID: 8822; Implemented sorting.
    0.2              Jan-22-2009           Kalyani Pallagani     Defect ID: 9056; Added code to delete the assign manager 
    0.3              Feb-03-2009           Kalyani Pallagani     Defect ID: 9688; Added code to check employee selectedindex in Save() method 
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using System.Collections.Generic;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;


namespace TPS360.Web.UI
{
    public partial class SFA_CompanyAssignManager : CompanyBasePage
    {
        #region Member Variables
        private static bool IsPrimaryManager=false;
        private bool Isdelete = false;
        #endregion

        #region Methods

        private void BindList()
        {
            odsCompanyAssignedManager.SelectParameters["companyId"].DefaultValue = base.CurrentCompanyId.ToString();
            lsvCompanyAssignedManager.DataBind();
            System.Web.UI.HtmlControls.HtmlTable tlbTemplate = (System.Web.UI.HtmlControls.HtmlTable)lsvCompanyAssignedManager.FindControl("tlbTemplate");
            if (tlbTemplate == null)
                btnUpdate.Visible = false;
        }

        private void Save()
        {
            if (lstEmployee.SelectedIndex > 0) 
            {
                    CompanyAssignedManager companyTeam = new CompanyAssignedManager();
                    companyTeam.CompanyId = base.CurrentCompanyId;
                    companyTeam.MemberId = Convert.ToInt32(lstEmployee.SelectedValue);
                    if (hfPrimaryManagerId.Value == "0")
                        companyTeam.IsPrimaryManager = true;
                    else
                        companyTeam.IsPrimaryManager = false;
                   
                    companyTeam.CreatorId = base.CurrentMember.Id;
                    try
                    {
                        Facade.AddCompanyAssignedManager(companyTeam);
                        MiscUtil.ShowMessage(lblMessage, "Successfully Assigned Manager.", false);
                        lstEmployee.SelectedIndex = 0;
                    }
                    catch (ArgumentException ex)
                    {

                        string message = "";
                        message = ex.Message;

                        if (CurrentCompanyStatus == TPS360.Common.Shared.CompanyStatus.Department)
                        {
                            message = message.Replace("Company", "BU");
                            message = message.Replace("company", "BU");
                        }
                        else if (CurrentCompanyStatus == TPS360.Common.Shared.CompanyStatus.Vendor)
                        {
                            message = message.Replace("Company", "Vendor");
                            message = message.Replace("company", "Vendor");
                        }
                        
                        MiscUtil.ShowMessage(lblMessage, message , true);
                    }
            }
            else 
                MiscUtil.ShowMessage(lblMessage, "Please Select user.", true ); //0.3

            BindList();
        }

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            lstEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            CompanyAssignedManager ca = Facade.GetCompanyAssignedManagerByCompanyIdAndMemberId(base.CurrentCompanyId, base.CurrentMember.Id);
            if (ca != null)
            {
                if (ca.IsPrimaryManager || IsUserAdmin)
                {
                    dvAddManager.Visible = true ;
                    btnUpdate.Visible = true ;
                    IsPrimaryManager = true;
                }
                else
                {
                    dvAddManager.Visible = false;
                    btnUpdate.Visible = false;
                    IsPrimaryManager = false;
                }
            }
            else
            {
                if (IsUserAdmin)
                    IsPrimaryManager = true;
                else
                    IsPrimaryManager = false;
            }
            if (ca == null && !IsUserAdmin)
                Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
                
            if (!Page.IsPostBack)
            {
                Page.Title = base.CurrentCompany.CompanyName + " - " + "Management Team";
                MiscUtil.PopulateMemberListByRole(lstEmployee, ContextConstants.ROLE_EMPLOYEE, Facade);
                hfPrimaryManagerId.Value = "0";
                IsPrimaryManager = false;
                BindList();
                hdnPageTitle.Value = Page.Title;
            }
            Page.Title = hdnPageTitle.Value;  
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {           
                Save();          
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int companyAssignedManagerid = Request.Form["rdoPrimaryManager"] != null ? Int32.Parse(Request.Form["rdoPrimaryManager"]) : 0;
            int previousPrimaryManagerId = Int32.Parse(hfPrimaryManagerId.Value);
            if (companyAssignedManagerid != 0)
            {
                CompanyAssignedManager companyManager = null;
                if (companyAssignedManagerid != previousPrimaryManagerId)
                {
                    if (previousPrimaryManagerId != 0)
                    {
                        companyManager = Facade.GetCompanyAssignedManagerById(previousPrimaryManagerId);
                        if (companyManager != null)
                        {
                            companyManager.IsPrimaryManager = false;
                            companyManager.UpdatorId = base.CurrentMember.Id;
                            Facade.UpdateCompanyAssignedManager(companyManager);
                        }
                    }
                    companyManager = Facade.GetCompanyAssignedManagerById(companyAssignedManagerid);
                    if (companyManager != null)
                    {
                        companyManager.IsPrimaryManager = true;
                        companyManager.UpdatorId = base.CurrentMember.Id;
                        Facade.UpdateCompanyAssignedManager(companyManager);
                        if (CurrentMember.Id != companyManager.MemberId && !IsUserAdmin)
                        {
                            dvAddManager.Visible = false;
                            btnUpdate.Visible = false;
                        }

                    }
                    MiscUtil.ShowMessage(lblMessage, "Successfully update primary manager", false);
                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "Primary manager is not changed to update", true);
                }
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select primary manager", true);
            }
            BindList();
        }

        #endregion

        #region ListView Events

        protected void lsvCompanyAssignedManager_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {

                if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteCompanyAssignedManagerById(id))
                        {
                            MiscUtil.ShowMessage(lblMessage, "Assign Manager has been successfully deleted.", false);
                            BindList(); 
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }
        protected void lsvCompanyAssignedManager_PreRender(object sender, EventArgs e)
        {
            foreach (ListViewDataItem list in lsvCompanyAssignedManager.Items)
            {
                ImageButton btnDelete = (ImageButton)list.FindControl("btnDelete");
                if (btnDelete != null)
                    if (btnDelete.Visible) Isdelete = btnDelete.Visible;
            }
                foreach (ListViewDataItem list in lsvCompanyAssignedManager.Items)
                {
                    System.Web.UI.HtmlControls.HtmlTableCell tdAction = (System.Web.UI.HtmlControls.HtmlTableCell)list.FindControl("tdUnAssign");
                    if (tdAction != null)
                        tdAction.Visible =Isdelete ;
                }

                System.Web.UI.HtmlControls.HtmlTableCell headerAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvCompanyAssignedManager.FindControl("thUnAssign"); //th in listview
                if (headerAction != null)
                    headerAction.Visible = Isdelete;
            
        }
        

        protected void lsvCompanyAssignedManager_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                CompanyAssignedManager companyAssignedManager = ((ListViewDataItem)e.Item).DataItem as CompanyAssignedManager;

                if (companyAssignedManager != null)
                {

                    Member member = Facade.GetMemberById(companyAssignedManager.MemberId);
                    Label lblManagerName = (Label)e.Item.FindControl("lblManagerName");
                    Label lblAssignedDate = (Label)e.Item.FindControl("lblAssignedDate");
                    Label lblPrimaryManager = (Label)e.Item.FindControl("lblPrimaryManager");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
                    string mambername = Facade.GetMemberNameById(companyAssignedManager.MemberId);
                    if (member != null)
                    {
                        lblManagerName.Text = mambername;
                        lblAssignedDate.Text = StringHelper.Convert(UIConstants.SHORT_DATE_FORMAT, companyAssignedManager.AssignDate);
                        if (companyAssignedManager.IsPrimaryManager)
                        {
                            lblPrimaryManager.Text = "<input id='rdoPrimaryManager' checked name='rdoPrimaryManager' type='radio' value='" + companyAssignedManager.Id + "' />" + mambername;
                            hfPrimaryManagerId.Value = companyAssignedManager.Id.ToString();
                        }
                        else
                        {
                            lblPrimaryManager.Text = "<input id='rdoPrimaryManager' name='rdoPrimaryManager' type='radio' value='" + companyAssignedManager.Id + "' />" + mambername;
                        }
                        
                    }

                    if (companyAssignedManager.IsPrimaryManager || !IsUserAdmin)
                       btnDelete.Visible = false;
                    else
                    {
                        btnDelete.Visible = true;
                        Isdelete = true;
                    }
                    
                    btnDelete.OnClientClick = "return ConfirmDelete('Manager')";
                    btnDelete.CommandArgument = StringHelper.Convert(companyAssignedManager.Id);
                }
            }
        }

        #endregion
    }
}
