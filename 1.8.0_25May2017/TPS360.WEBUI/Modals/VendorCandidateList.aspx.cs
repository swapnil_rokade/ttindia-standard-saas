﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalResumeBuilder.aspx.cs
    Description: This is the user control page used for resume builder.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.           Date               Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               Mar-04-2009        Shivanand          Defect #10053; usercontrol "ucntrlJobTitle" is removed.
    0.2               July-01-2009       Shivanand          Defect #10464; AsyncOptions are provided for Tabs.
 *  0.3               Apr-07-2010        Ganapati Bhat      Enhancement #12139; Removed tab "Skill Set"
 *  0.4               Feb-03-2016        Sakthi             Made changes in methods "AddToHiringMatrix" and "SendNotificationEmailToRecruiters".
 *  0.5               12/May/2016        pravin khot        Modify function-SendNotificationEmailToRecruiters
 *  0.6               15/Mar/2017        Sumit Sonawane     Modified function-SendNotificationEmailToRecruiters
------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;

using TPS360.Common.Helper;
using System.Drawing;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities ;
using System.Text;
using System.Collections;
using System.Web;
using TPS360.Web.UI.Helper;
using System.Collections.Generic;
using TPS360.Common.Shared;
namespace TPS360.Web.UI
{
    public partial class VendorCandidateList : RequisitionBasePage 
    {
        protected void Page_Load(object sender, EventArgs args)
        {
            Label pageTitle = (Label)this.Master.FindControl("lbModalTitle");
            if (pageTitle != null)
            {

                if(CurrentJobPosting !=null)
                 pageTitle.Text = CurrentJobPosting .JobTitle +  " - Submit Candidates";
            }
            if (!IsPostBack)
            {
                string JID = Request.QueryString["JID"].ToString();
                string FromPage ="";
              
            }
        }
              protected void btnApply_Click(object sender, EventArgs args)
        {
            

            AddToHiringMatrix();
           

        }
        public void AddToHiringMatrix()
        {
            string canids = uclCandidateList.SelectedCandidateIds;
            if (canids != "")
            {
                int JID = Convert.ToInt32(Request.QueryString["JID"].ToString());
                HiringMatrixLevels lev = null;// Facade.HiringMatrixLevel_GetInitialLevel();
                var webConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
                var vendorSubmission = webConfig.AppSettings.Settings["VendorLevel"];
                if (vendorSubmission != null && vendorSubmission.Value.ToString() != string.Empty)
                {
                    lev = Facade.HiringMatrixLevel_GetLevelByName(vendorSubmission.Value.ToString());
                }
                if (lev == null) lev = Facade.HiringMatrixLevel_GetInitialLevel();
                if (JID > 0)
                {
                    Facade.MemberJobCart_AddCandidateToRequisition(base.CurrentMember.Id, canids, JID);
                    Facade.MemberJobCart_MoveToNextLevel(0, base.CurrentMember.Id, canids, JID, lev.Id.ToString());
                    MiscUtil.EventLogForCandidateStatus(TPS360.Common.Shared.EventLogForCandidate.CandidateStatusChange, JID, canids, base.CurrentMember.Id, lev.Name, Facade);
                    Facade.UpdateCandidateRequisitionStatus(JID, canids, base.CurrentMember.Id, lev.Id);
                    Member Vendor = Facade.GetMemberById(base.CurrentMember.Id);
                    string VendorName = Vendor.FirstName + " " + Vendor.LastName;
                    string VendorCompanyName = Facade.GetCompanyNameByVendorMemberId(Vendor.Id);
					//Email Triggered to Assigned team when vendor submitted the candidate
                    if (canids.Contains(","))
                    {
                        string[] CanId = canids.Split(',');
                        foreach (string CandId in CanId)
                            SendNotificationEmailToRecruiters(JID, CandId, VendorName, VendorCompanyName);//Code modified by Sakthi on Feb-03-2016 - VendorName parameter added
                                                                                                          //Code modified by Sumit Sonawane  on 15/Mar/2017  - VendorCompanyName parameter added   
                         
                    }
                    else
                        SendNotificationEmailToRecruiters(JID, canids, VendorName, VendorCompanyName);//Code modified by Sakthi on Feb-03-2016 - VendorName parameter added
                                                                                                      //Code modified by Sumit Sonawane  on 15/Mar/2017  - VendorCompanyName parameter added   
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(lblNote, typeof(MiscUtil), "Show", "parent.ShowStatusMessage('Selected Candidates submitted to Requisition',false);", true);
                }
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);
            }
        }
		//Email Triggered to Assigned team when vendor submitted the candidate
        //Code modified by Sumit Sonawane  on 15/Mar/2017  - VendorCompanyName parameter added   
        private void SendNotificationEmailToRecruiters(int jobid, string CanId, string VendorName, string VendorCompanyName)//Code modified by Sakthi on Feb-03-2016 - VendorName argument added
        {
            string sentStatus = string.Empty;
            EmailHelper emailManager = new EmailHelper();
            JobPosting job = Facade.GetJobPostingById(jobid);
            Member Candidate = Facade.GetMemberById(Convert.ToInt32(CanId));
            string CName = Candidate.FirstName + " " + Candidate.LastName;
            MemberExtendedInformation memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(Convert.ToInt32(CanId));//line added by pravin khot on 12/May/2016
            string subject = "Notification : Vendor Submission " + job.JobTitle + " - " + job.JobPostingCode; //Code modified by Sakthi on Feb-03-2016 - Subject Changed
            //************ Code Modified By Sakthi on 04/05/2016 *************
            StringBuilder body = new StringBuilder();
            body.Append("<html>");
            body.Append("<head><title></title>");
            body.Append("<style>body, td, p{font-family: Arial;font-size: 12px;} </style></head>");
            body.Append("<body><P><STRONG>Dear [RecuriterName]</STRONG>,</P>");
            body.Append("<P>A candidate has been submitted for a requisition in the Vendor portal.</P>");
            body.Append("<table><tr>");
            body.Append("<td width=\"100\"><STRONG>Requisition</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [JobTitle] | [JobCode]</td></tr>");
            body.Append("<tr><td width=\"100\"><STRONG>Candidate</STRONG><td width=\"10\" align=\"center\">:</td> <td width=500>[Candidatename] | [ID: A[CandidateID]]</td></tr>");
            //************ Code ADDED By pravin on 12/05/2016 *************    
            body.Append("<tr><td width=\"100\"><STRONG>Candidate Email</STRONG><td width=\"10\" align=\"center\">:</td> <td width=500>[CandidateEmail]</td></tr>");
            body.Append("<tr><td width=\"100\"><STRONG>Total Experience</STRONG><td width=\"10\" align=\"center\">:</td> <td width=500>[Experience]</td></tr>");
            body.Append("<tr><td width=\"100\"><STRONG>Current CTC</STRONG><td width=\"10\" align=\"center\">:</td> <td width=500>[CurrentCTC]</td></tr>");
            body.Append("<tr><td width=\"100\"><STRONG>Expected CTC</STRONG><td width=\"10\" align=\"center\">:</td> <td width=500>[ExpectedCTC]</td></tr>");
            body.Append("<tr><td width=\"100\"><STRONG>Notice Period</STRONG><td width=\"10\" align=\"center\">:</td> <td width=500>[NoticePeriod]</td></tr>");
            //*********************END By pravin on 12/05/2016***********************
            //body.Append("<tr/><tr/>");
            //body.Append("<tr><td width=\"100\"><STRONG>Vendor Name</STRONG><td width=\"10\" align=\"center\">:</td> <td width=500> [VendorName]</td></tr>");
            body.Append("<tr><td width=\"100\"><STRONG>Consultant Name</STRONG><td width=\"10\" align=\"center\">:</td> <td width=500> [VendorName] from [VendorCompanyName]</td></tr>");
            body.Append("<tr/></table><P/>");
            body.Append("<P>Warm Regards,<br>[VendorContactName]</P>");
            body.Append("</body></html>");
            body.Replace("[JobTitle]", job.JobTitle);
            body.Replace("[JobCode]", job.JobPostingCode);
            body.Replace("[Candidatename]", CName);
            body.Replace("[CandidateID]", CanId);
            //************ Code END By Sakthi on 04/05/2016 *************    

            //************ Code ADDED By pravin on 12/05/2016 *************    
            body.Replace("[CandidateEmail]", Candidate.PrimaryEmail);

            if (memberExtendedInfo.NoticePeriod != string.Empty && memberExtendedInfo.NoticePeriod != "")
            {
                body.Replace("[NoticePeriod]", memberExtendedInfo.NoticePeriod);
            }
            else
            {
                body.Replace("[NoticePeriod]", "");
            }

            if (memberExtendedInfo.TotalExperienceYears != string.Empty && memberExtendedInfo.TotalExperienceYears != "")
            {
                body.Replace("[Experience]", memberExtendedInfo.TotalExperienceYears + " Years");
            }
            else
            {
                body.Replace("[Experience]", "");
            }

            int CurrentCurrencyLookupId = 0;
            int ExpectedCurrencyLookupId = 0;
            decimal CurrentYearlyRate = 0;
            decimal ExpectedYearlyRate = 0;
            string CurrentSalaryPayCycleType = "";
            string ExpectedSalaryPayCycleType = "";

            if (memberExtendedInfo.CurrentYearlyCurrencyLookupId > 0)
            {
                CurrentCurrencyLookupId = memberExtendedInfo.CurrentYearlyCurrencyLookupId;
                CurrentYearlyRate = memberExtendedInfo.CurrentYearlyRate;
            }
            else if (memberExtendedInfo.CurrentMonthlyCurrencyLookupId > 0)
            {
                CurrentCurrencyLookupId = memberExtendedInfo.CurrentMonthlyCurrencyLookupId;
                CurrentYearlyRate = memberExtendedInfo.CurrentMonthlyRate;
            }
            else if (memberExtendedInfo.CurrentHourlyCurrencyLookupId > 0)
            {
                CurrentCurrencyLookupId = memberExtendedInfo.CurrentHourlyCurrencyLookupId;
                CurrentYearlyRate = memberExtendedInfo.CurrentHourlyRate;
            }

            if (memberExtendedInfo.ExpectedYearlyCurrencyLookupId > 0)
            {
                ExpectedCurrencyLookupId = memberExtendedInfo.ExpectedYearlyCurrencyLookupId;
                ExpectedYearlyRate = memberExtendedInfo.ExpectedYearlyRate;
            }
            else if (memberExtendedInfo.ExpectedMonthlyCurrencyLookupId > 0)
            {
                ExpectedCurrencyLookupId = memberExtendedInfo.ExpectedMonthlyCurrencyLookupId;
                ExpectedYearlyRate = memberExtendedInfo.ExpectedMonthlyRate;
            }
            else if (memberExtendedInfo.ExpectedHourlyCurrencyLookupId > 0)
            {
                ExpectedCurrencyLookupId = memberExtendedInfo.ExpectedHourlyCurrencyLookupId;
                ExpectedYearlyRate = memberExtendedInfo.ExpectedHourlyRate;
            }
            string CurrentCurrency = Facade.GetLookUPNamesByIds(CurrentCurrencyLookupId.ToString());
            string ExpectedCurrency = Facade.GetLookUPNamesByIds(ExpectedCurrencyLookupId.ToString());

            int CurrentSalaryPayCycle = memberExtendedInfo.CurrentSalaryPayCycle;
            int ExpectedSalaryPayCycle = memberExtendedInfo.ExpectedSalaryPayCycle;

            if (CurrentSalaryPayCycle == 1)
            {
                CurrentSalaryPayCycleType = "Hourly";
            }
            else if (CurrentSalaryPayCycle == 2)
            {
                CurrentSalaryPayCycleType = "Daily";
            }
            else if (CurrentSalaryPayCycle == 3)
            {
                CurrentSalaryPayCycleType = "Monthly";
            }
            else
            {
                CurrentSalaryPayCycleType = "Yearly";
            }

            if (ExpectedSalaryPayCycle == 1)
            {
                ExpectedSalaryPayCycleType = "Hourly";
            }
            else if (ExpectedSalaryPayCycle == 2)
            {
                ExpectedSalaryPayCycleType = "Daily";
            }
            else if (ExpectedSalaryPayCycle == 3)
            {
                ExpectedSalaryPayCycleType = "Monthly";
            }
            else
            {
                ExpectedSalaryPayCycleType = "Yearly";
            }

            body.Replace("[CurrentCTC]", CurrentYearlyRate.ToString() + "  " + CurrentCurrency + "  " + CurrentSalaryPayCycleType);
            body.Replace("[ExpectedCTC]", ExpectedYearlyRate.ToString() + "  " + ExpectedCurrency + "  " + ExpectedSalaryPayCycleType);
            body.Replace("[VendorName]", VendorName);
            body.Replace("[VendorContactName]", VendorName);
            body.Replace("[VendorCompanyName]", VendorCompanyName);//Code modified by Sumit Sonawane  on 15/Mar/2017   
            //*********************END By pravin on 12/05/2016***********************
            emailManager.Subject = subject;
            emailManager.To.Clear();
            
            IList<JobPostingHiringTeam> team = Facade.GetAllJobPostingHiringTeamByJobPostingId(jobid);
            if (SiteSetting == null)
            {
                SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)TPS360.Common.Shared.SettingType.SiteSetting);
                Hashtable config = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                emailManager.From = config[TPS360.Common.Shared.DefaultSiteSetting.SMTPUser.ToString()].ToString();// CurrentMember.PrimaryEmail;
            }
            else emailManager.From = SiteSetting[TPS360.Common.Shared.DefaultSiteSetting.SMTPUser.ToString()].ToString();// CurrentMember.PrimaryEmail;
            string to = "";
            if (team != null)
            {
                // Code modified by Sakthi on Feb-03-2016 - foreach is changed to add Recruiter Name in each mail. 
                foreach (JobPostingHiringTeam t in team)
                {
                    emailManager.To.Add(t.PrimaryEmail);
                    Member recutier = Facade.GetMemberByMemberEmail(t.PrimaryEmail);
                    string RecutierName = recutier.FirstName + " " + recutier.LastName;
                    body = body.Replace("[RecuriterName]", RecutierName);
                    to += t.PrimaryEmail + ";";

                  
                }
            }
            //emailManager.Body = body.ToString();
            //***********Added by pravin khot on 8/March/2017***********
            //sentStatus = emailManager.Send();//comment by pravin khot on 17/June/2016
            //sentStatus = emailManager.SendMailFromSystemEmailId();//added by pravin khot on 17/June/2016
            int senderid = 0;
            string AdminEmailId = string.Empty;
            Hashtable siteSettingTable = null;
            SiteSetting siteSetting1 = Facade.GetSiteSettingBySettingType((int)TPS360.Common.Shared.SettingType.SiteSetting);
            siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting1.SettingConfiguration);
            if (siteSetting1 != null)
            {
                AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                senderid = Facade.GetMemberIdByEmail(AdminEmailId);
            }
            MailQueueData.AddMailToMailQueue(senderid, to, subject, body.ToString(), "", "", null, Facade);
            sentStatus = "1";
            //*****************END**********************
           
            if (sentStatus == "1")
            {
                SaveMemberEmail(subject, body.ToString(), emailManager.From, to);
                sentStatus = "Sent";
            }
        }

        private void SaveMemberEmail(string subject, string body, string From, string To)
        {
            if (true) // Isvalid
            {
                try
                {
                    MemberEmail memberEmail = BuildMemberEmail(subject, body, From, To);
                    Facade.AddMemberEmail(memberEmail);
                }
                catch (ArgumentException ex)
                {

                }

            }
        }

        private MemberEmail BuildMemberEmail(string subject, string body, string strFrom, string To)
        {
            MemberEmail memberEmail = new MemberEmail();
            memberEmail.SenderId = 0;
            memberEmail.SenderEmail = strFrom;
            memberEmail.ReceiverEmail = To;
            memberEmail.Subject = subject;
            memberEmail.EmailBody = body;
            memberEmail.Status = 0;
            memberEmail.IsRemoved = false;
            memberEmail.CreatorId = 0;
            memberEmail.SentDate = DateTime.Now.ToString();
            memberEmail.EmailTypeLookupId = (int)TPS360.Common.Shared.EmailType.Sent;
            return memberEmail;
        }
        
    }
}
