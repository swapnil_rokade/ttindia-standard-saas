﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true" 
ValidateRequest="true" CodeFile="CandidateNotes.aspx.cs" Inherits="TPS360.Web.UI.CandidateNotes" %>

<%@ Register Src="~/Controls/NotesAndActivitiesEditor.ascx" TagName="NotsActivities"
    TagPrefix="uc1" %>  
    <%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content> 
<asp:Content ID="Content2" ContentPlaceHolderID="cphModalMaster" Runat="Server">
  
   <div  id="schrollDiv" style="height: 500px; visibility: visible; overflow: auto; display: block; position: relative; width: 865px; background-color: #FFFFFF">
      
        <div style =" width : 865px; height : 500px; display:table-cell    ; vertical-align : middle  ">
     <uc1:NotsActivities ID="ucntrlNotsActivities" runat="server" />
    
    </div>
  </div>
</asp:Content>