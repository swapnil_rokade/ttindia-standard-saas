﻿<%-- -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CareerJobDetail.aspx
    Description: popup windows using career page
    Created By: pravin khot
    Created On:  18/Jan/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
  
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>

<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true"
    CodeFile="CareerJobDetail.aspx.cs" Inherits="TPS360.Web.UI.CareerJobDetail" EnableViewState="true" %>

<%@ Register Src="~/Controls/CareerJobDetail.ascx" TagName="CareerJobDetail" TagPrefix="ucl" %>
<asp:Content ID="cntCandidateResumeBuilder" ContentPlaceHolderID="cphModalMaster"
    runat="Server">
    <asp:UpdatePanel ID="upJobDetail" runat="server">
        <ContentTemplate>
            <div style="width: 675px; height: 500px;">
                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                <div class="TableRow" style="text-align: right;" id="divApply" runat="server">
                    <asp:Label ID="lblApplied" runat="server" Text="Applied" EnableViewState="true" CssClass=" btn btn-success">
                    </asp:Label>
                   <%-- <asp:Button ID="btnApply" runat="server" CssClass="btn btn-primary" Text="Apply for Job"
                        OnClick="btnApply_Click" />--%>
                </div>
                <div class="TableRow">
                    <div style="overflow: auto; max-height: 500px;" id="divContent" runat="server">
                        <ucl:CareerJobDetail id="uclJobDetail" runat="server" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
