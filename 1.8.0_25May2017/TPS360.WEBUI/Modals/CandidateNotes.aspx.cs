﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TPS360.Web.UI
{
    public partial class CandidateNotes : CandidateBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label lbModalTitle = (Label)this.Master.FindControl("lbModalTitle");
                lbModalTitle.Text = "Candidate Notes";
            }
        }
    }
}
