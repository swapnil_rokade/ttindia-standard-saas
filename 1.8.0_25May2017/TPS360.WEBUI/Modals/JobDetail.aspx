﻿<%-- -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: JobDetail.aspx
    Description: This is the page which is used to present tab controls
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Oct-16-2008        Gopala Swamy         Defect id: 8994; Renamed in tabcontrol from "Job tittle & Category" to "Job tittles"
    0.2             Mar-04-2009        Shivanand            Defect #10053; Removed tab "Job Titles".
    0.3             May-26-2009         Sandeesh            Defect id : 10464 Changes made to load the tabs instantaneously
    0.4             July-01-2009       Shivanand            Defect #10464; AsyncMode is enabled for Tabs.
    0.5             Jul-24-2009           Veda              Defect id: 11072 ; Contineous Spinning occurs in the browser tab.
    0.6             Jul-30-2009           Veda              Defect id: 11132; 
    0.7             Aug-31-2009         Ranjit Kumar.I      Defect id:#11421;Desabled Trace in Page Level Script
    0.8             Apr-07-2010        Ganapati Bhat        Enhancement #12139; Removed tab "Skill Set"
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>

<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true"
    CodeFile="JobDetail.aspx.cs" Inherits="TPS360.Web.UI.JobDetail" EnableViewState="true" %>

<%@ Register Src="~/Controls/JobDetail.ascx" TagName="JobDetail" TagPrefix="ucl" %>
<asp:Content ID="cntCandidateResumeBuilder" ContentPlaceHolderID="cphModalMaster"
    runat="Server">
    <asp:UpdatePanel ID="upJobDetail" runat="server">
        <ContentTemplate>
            <div style="width: 675px; height: 500px;">
                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                <div class="TableRow" style="text-align: right;" id="divApply" runat="server">
                    <asp:Label ID="lblApplied" runat="server" Text="Applied" EnableViewState="true" CssClass=" btn btn-success">
                    </asp:Label>
                    <asp:Button ID="btnApply" runat="server" CssClass="btn btn-primary" Text="Apply for Job"
                        OnClick="btnApply_Click" />
                </div>
                <div class="TableRow">
                    <div style="overflow: auto; max-height: 500px;" id="divContent" runat="server">
                        <ucl:jobdetail id="uclJobDetail" runat="server" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
