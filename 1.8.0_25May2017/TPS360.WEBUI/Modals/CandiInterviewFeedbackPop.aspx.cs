﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI;
using TPS360.Common.Shared;
using System.Collections.Generic;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;


namespace TPS360.Web.UI
{
    public partial class CandiInterviewFeedbackPop : CandidateBasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                int MemberId = int.Parse(Request.QueryString["mid"]);
                int JobPostingId = int.Parse(Request.QueryString["JID"]);
                Label lbModalTitle = (Label)this.Master.FindControl("lbModalTitle");
                lbModalTitle.Text = "Feedbacks";

                //IList<Interview> abc = Facade.GetInterviewByMemberIdAndJobPostingID(int MemberId, int JobPostingId);
                IList<Interview> abc = Facade.GetInterviewByMemberIdAndJobPostingID(MemberId, JobPostingId);
                IList<InterviewFeedback> f1 = null;

                DataTable dt = new DataTable();
                DataRow NewRow;
                if (dt.Columns.Count == 0)
                {
                    dt.Columns.Add("Feedback", typeof(string));
                    dt.Columns.Add("Name", typeof(string));
                    dt.Columns.Add("Email", typeof(string));
                    dt.Columns.Add("Date", typeof(string));
                }
                if (abc != null)
                {
                    for (int i = 0; i < abc.Count; i++)
                    {
                        int interviewId = abc[i].Id;
                        f1 = Facade.InterviewerFeedback_GetByInterviewId(interviewId);
                        try
                        {
                            NewRow = dt.NewRow();
                            NewRow[0] = f1[0].Feedback.ToString();
                            NewRow[1] = f1[0].SubmittedBy.ToString();
                            NewRow[2] = f1[0].Email.ToString();
                            NewRow[3] = f1[0].CreateDate.ToString();

                            dt.Rows.Add(NewRow);
                        }
                        catch { }

                    }
                    GridViewCandiInterviewFeedback.DataSource = dt;
                    GridViewCandiInterviewFeedback.DataBind();
                }
            }
        }
        protected void GridViewCandiInterviewFeedback_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].ToolTip = e.Row.Cells[i].Text;
                }
                // e.Row.ToolTip = (e.Row.DataItem as DataRowView)["Email"].ToString();
            }
        }
    }
}
