﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CareerJobDetail.aspx.cs
    Description: Using popup window for career page
    Created By: pravin khot
    Created On:  18/Jan/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.           Date               Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
 * 0.1               15/Jan/2016         pravin khot        Introduced by CODE COMMENTED "divApply.Visible = false;"
------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;

using TPS360.Common.Helper;
using System.Drawing;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities ;
namespace TPS360.Web.UI
{
    public partial class CareerJobDetail : RequisitionBasePage
    {
        protected void Page_Load(object sender, EventArgs args)
        {
            if (!IsPostBack)
            {
                string JID = Request.QueryString["JID"].ToString();
                //string JID ="5171";
                
                string FromPage = "";
                if (Request.QueryString["FromPage"] != null) FromPage = Request.QueryString["FromPage"].ToString();



                if (FromPage != "JobDetail" && FromPage != "Vendor")
                {

                    divContent.Style.Add(" max-height", "470px");

                    //*****CODE COMMENTED BY PRAVIN KHOT ON 15/Jan/2016********
                    //MemberJobApplied obj = Facade.MemberJobApplied_GetByMemberIDANDJobPostingID(CurrentMember.Id, Convert.ToInt32(JID));
                    //if (obj != null)
                    //{
                    //    //btnApply.Visible = false;
                    //    lblApplied.Visible = true;
                    //}
                    //else
                    //{
                    //    //btnApply.Visible = true;
                    //    lblApplied.Visible = false;
                    //}
                    divApply.Visible = false;
                    //********END***********************
                }
                else
                {
                    divApply.Visible = false;
                }
            }
        }
        //private void buildMemberJobApplied()
        //{
        //    MemberJobApplied newjob = new MemberJobApplied();
        //    string JID = Request.QueryString["JID"].ToString();
        //    newjob.MemberId = CurrentMember.Id;
        //    newjob.JobPostingId = Convert.ToInt32(JID);
        //    Facade.AddMemberJobApplied(newjob);

        //}
        protected void btnApply_Click(object sender, EventArgs args)
        {

            //MemberDocument memberDocu = Facade.GetRecentResumeByMemberID(CurrentMember.Id);
            //string JID = Request.QueryString["JID"].ToString();
            //if (memberDocu != null)
            //{
            //    buildMemberJobApplied();
            //    Facade.MemberJobCart_AddCandidateToRequisition(CurrentMember.Id, CurrentMember.Id.ToString(), Convert.ToInt32(JID));
            //    MiscUtil.ShowMessage(lblMessage, "success “Thank you for your application.”", false);
            //    btnApply.Visible = false;
            //    lblApplied.Visible = true;
            //}
            //else
            //{
            //    MiscUtil.ShowMessage(lblMessage, "Please upload a resume before applying to jobs.", true);
            //}
        }
    }
}
