﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalResumeBuilder.aspx.cs
    Description: This is the user control page used for resume builder.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.           Date               Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               Mar-04-2009        Shivanand          Defect #10053; usercontrol "ucntrlJobTitle" is removed.
    0.2               July-01-2009       Shivanand          Defect #10464; AsyncOptions are provided for Tabs.
 *  0.3               Apr-07-2010        Ganapati Bhat      Enhancement #12139; Removed tab "Skill Set"
------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;

using TPS360.Common.Helper;
using System.Drawing;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities ;
using TPS360.Web.UI.Helper;
namespace TPS360.Web.UI
{
    public partial class ReferralLink: RequisitionBasePage 
    {
        protected void Page_Load(object sender, EventArgs args)
        {
            if (!IsPostBack)
            {
                Label lbModalTitle = (Label)this.Master.FindControl("lbModalTitle");
                if(lbModalTitle !=null) lbModalTitle.Text = CurrentJobPosting.JobTitle + " - " + "Employee Referral Link";
                SecureUrl url = UrlHelper.BuildSecureUrl( UrlConstants .ApplicationBaseUrl + "Referral/EmployeeReferral.aspx",string.Empty ,UrlConstants .PARAM_JOB_ID , CurrentJobPostingId.ToString () );
                txtReferralLink .Text =url.ToString ();
                SecureUrl urlEmail = UrlHelper.BuildSecureUrl("../Dashboard/" + UrlConstants.Dashboard.NEWMAIL, "", UrlConstants.PARAM_JOB_ID, CurrentJobPostingId.ToString(), UrlConstants.PARAM_PAGE_FROM, "Referrer");
                btnSubmit .Attributes .Add ("onclick","window.open('" + urlEmail.ToString() + "','EmailView', 'height=626,width=770, toolbar=0, scrollbars=1, menubar=0, location=0'); parent.CloseModal();");

            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            
        }
        
        
    }
}
