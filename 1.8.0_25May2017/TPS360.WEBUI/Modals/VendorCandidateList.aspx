﻿<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true"
    CodeFile="VendorCandidateList.aspx.cs" Inherits="TPS360.Web.UI.VendorCandidateList"
    EnableViewState="true" %>

<%@ Register Src="~/Controls/VendorCandidateList.ascx" TagName="Candidate" TagPrefix="ucl" %>
<asp:Content ID="cntCandidateResumeBuilder" ContentPlaceHolderID="cphModalMaster"
    runat="Server">
    <asp:UpdatePanel ID="upJobDetail" runat="server">
        <ContentTemplate>
            <div style="width: 675px; height: 500px;">
                
                
                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
               
                   
                
                <div class="TableRow" style="text-align: right;padding-bottom:10px" id="divApply" runat="server">
                 <asp:Label ID ="lblNote" runat ="server" style="float:left;padding-top:10px" ><b>Note:</b> Disabled Candidates have already been submitted.</asp:Label>
          
                    <asp:Button ID="btnApply" runat="server" CssClass="btn btn-primary" Text="Submit"
                        OnClick="btnApply_Click" />
                </div>
                <div class="TableRow">
                    <div style="overflow: auto; max-height: 500px;" id="divContent" runat="server">
                        <ucl:Candidate ID="uclCandidateList" runat="server" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
