﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalResumeBuilder.aspx.cs
    Description: This is the user control page used for resume builder.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.           Date               Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               Mar-04-2009        Shivanand          Defect #10053; usercontrol "ucntrlJobTitle" is removed.
    0.2               July-01-2009       Shivanand          Defect #10464; AsyncOptions are provided for Tabs.
 *  0.3               Apr-07-2010        Ganapati Bhat      Enhancement #12139; Removed tab "Skill Set"
------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;

using TPS360.Common.Helper;
using System.Drawing;
using System.Web.UI.WebControls ;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Web.UI;
namespace TPS360.Web.UI
{
    public partial class OfferRejection : RequisitionBasePage
    {
        #region Member Variables
            
        #endregion 
        #region Properties
        public int JobPostingID
        {
            get 
            {
                return Convert.ToInt32(Request.QueryString["JID"].ToString());
            }
           

        }

 
        public string MemberID 
        {
            get 
            {
                return Request.QueryString["Canid"].ToString();
            }
          
              
            
                    
        }
        public int CreatorID 
        {
            get 
            {
                return base.CurrentMember.Id;
            }
        
        }
        public int _statusID = 0;
        public int StatusID 
        {
            get 
            {
                return Convert.ToInt32(Request.QueryString["StatusId"].ToString());
            }
           
        }
        
        #endregion
        protected void Page_Load(object sender, EventArgs args)
        {
          
            if (!IsPostBack)
            {
                wdcRejectedDate.Value = DateTime.Now;
                PrepareView();
             
                PrepareEditView();
                Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
                if (lbModalTitle != null)
                {
                    int memID = 0;
                    Int32.TryParse(MemberID, out memID);

                    if (memID > 0)
                    {
                        lbModalTitle.Text = Facade.GetMemberNameById(memID) + " - Offer Decline";
                    }
                    else lbModalTitle.Text = "Offer Decline";
                }
            }

            
        }

        private MemberOfferRejection BuildMemberOfferRejectionDetail() 
        {
            MemberOfferRejection detaill =new MemberOfferRejection();
            detaill.JobPostingId = JobPostingID;
            detaill.CreatorId = CreatorID;
            detaill.UpdatorId = CreatorID;
            if (ddlReasonForRejection.SelectedIndex != 0) 
            {
                detaill.ReasonForRejectionLookUpID = Convert.ToInt32(ddlReasonForRejection.SelectedValue);
            }
            if (!string.IsNullOrEmpty(wdcRejectedDate.Text) && wdcRejectedDate.Text != "Null") 
            {
                detaill.RejectedDate = DateTime.Parse(wdcRejectedDate.Text);
            }
            return detaill;
        }

        protected void btnSave_Click(object sender, EventArgs args)
        {
            MemberOfferRejection memberofferRejectionDetails = BuildMemberOfferRejectionDetail();
            //*******Code added by pravin khot on 28/July/2016**************
            RejectCandidate cand = Facade.GetRejectCandidateByMemberIdAndJobPostingID(Int32.Parse(MemberID), JobPostingID);
            if (cand != null)
            {
                Facade.RejectToUnRejectCandidateStatusChange(MemberID, JobPostingID);
            }
            //************************END****************************
            Facade.AddMemberOfferRejection(memberofferRejectionDetails, MemberID);

            if (StatusID != 0 && StatusID > 0)
            {
                Facade.MemberJobCart_MoveToNextLevel(0, base.CurrentMember.Id, MemberID, JobPostingID,Convert.ToString(StatusID));
                Facade.UpdateCandidateRequisitionStatus(JobPostingID,MemberID, base.CurrentMember.Id, StatusID);
                MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, JobPostingID, MemberID, CurrentMember.Id, "Offer Decline", Facade);
                ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Selected candidate(s) moved Successfully.');", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Offer decline details updated successfully.');", true);
            }
        }
        #region Methods
        private void PrepareView() 
        {
            MiscUtil.PopulateReasonForRejectionList(ddlReasonForRejection, Facade);

        }
        public  void PrepareEditView() 
        {
            ClearControls();

            MemberOfferRejection memberOfferRejectionDetail;
            if (!MemberID.Contains(","))
            {
                memberOfferRejectionDetail = Facade.MemberOfferRejection_GetbyMemberIdAndJobPostingID(Convert.ToInt32(MemberID), JobPostingID);
                if (memberOfferRejectionDetail != null)
                {
                    if (memberOfferRejectionDetail.ReasonForRejectionLookUpID != 0)
                        ControlHelper.SelectListByValue(ddlReasonForRejection, memberOfferRejectionDetail.ReasonForRejectionLookUpID.ToString());
                    if (!string.IsNullOrEmpty(Convert.ToString(memberOfferRejectionDetail.RejectedDate)))
                    {
                        wdcRejectedDate.Value = memberOfferRejectionDetail.RejectedDate;
                    }

                }
            }
           
         
        }
        private void ClearControls() 
        {
            ddlReasonForRejection.SelectedIndex = 0;
            wdcRejectedDate.Value = DateTime.Now;
        }
        #endregion
    }
}
