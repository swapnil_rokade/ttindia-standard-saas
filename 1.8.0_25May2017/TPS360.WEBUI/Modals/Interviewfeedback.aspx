﻿
<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true" CodeFile="Interviewfeedback.aspx.cs" Inherits="TPS360.Web.UI.Interviewfeedback"  EnableViewState ="true"  %>
<%@ Register Src="~/Controls/InterviewFeedback.ascx" TagName="InterviewFeedback"
    TagPrefix="uc1" %>
<asp:Content ID="cntCandidateResumeBuilder" ContentPlaceHolderID="cphModalMaster"
    runat="Server">
    <div style ="width : 670px; height : 495px;  max-height :500px; overflow:auto; ">
            <uc1:InterviewFeedback ID="ucntrlInterviewFeedback" runat="server" />
    </div>
</asp:Content>
        