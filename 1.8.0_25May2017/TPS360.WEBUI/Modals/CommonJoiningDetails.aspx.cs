﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalResumeBuilder.aspx.cs
    Description: This is the user control page used for resume builder.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.           Date               Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               Mar-04-2009        Shivanand          Defect #10053; usercontrol "ucntrlJobTitle" is removed.
    0.2               July-01-2009       Shivanand          Defect #10464; AsyncOptions are provided for Tabs.
 *  0.3               Apr-07-2010        Ganapati Bhat      Enhancement #12139; Removed tab "Skill Set"
------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;

using TPS360.Common.Helper;
using System.Drawing;
namespace TPS360.Web.UI
{
    public partial class CommonJoiningDetails : CandidateBasePage
    {

        protected void Page_Load(object sender, EventArgs args)
        {
            if (!IsPostBack)
            {
                string canid = Request.QueryString["Canid"].ToString();
                string statusid = Request.QueryString["StatusId"].ToString();
                string JID = Request.QueryString["JID"].ToString();
                uclJoine.JobPostingId = Convert.ToInt32(JID);
                uclJoine.MemberID = canid;
                uclJoine.StatusId = Convert.ToInt16(statusid); ;

            }
        }

    }
}
