
<%@ Page Language="C#" MasterPageFile="~/Public.master" AutoEventWireup="true" CodeFile="ErrorPage.aspx.cs" 
    Inherits="TPS360.Web.UI.ErrorPage" Title="Application Error" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
    
<asp:Content ID="cDefault" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <asp:UpdatePanel ID="pnlRequisition" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            
     
    <br />
    <br />
    <br />
    <div class="MidBodyBoxRow">
        <div class="MainBox" style="width: 55%;">
           
            <div class="MasterBoxContainer2">
                <div class="section-header" style="text-align: center; color: #FF0000; font-size :large; vertical-align: middle;">                            
                            Error Occurred                             
                        </div>
                        
            
                <div class="ChildBoxContainer2" style =" height : 200px; text-align : center ;  padding-top : 30px;">
                <img id="Img1" runat="server" src="~/Images/error_icon.png" alt="X" height="50" width="60" /><br /><br />
                        <asp:Label ID ="lblConfirmHeading" runat ="server" Width ="95%" Font-Bold ="true" Font-Size="Medium">The server returned an "Error".</asp:Label><br /><br />
 <asp:Label ID ="Label1" runat ="server" Width ="95%" Font-Bold ="true" >Something is broken.Please let us know what you were doing when this error occurred.We will fix it as soon as possible.</asp:Label><br />
 <asp:Label ID ="Label2" runat ="server" Width ="95%" Font-Bold ="true" >Sorry for inconvinience caused.</asp:Label><br /><br />
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="Images/button_home.png" PostBackUrl="Dashboard/Dashboard.aspx" Width="200" Height="30" />
                    <br />              
                </div>
             <asp:Panel ID="pnlSearchBoxBody" runat="server" CssClass="well well-small nomargin" >
        <asp:Panel ID="pnlSearchRegion" runat="server">
            <asp:CollapsiblePanelExtender ID="cpeExperience" runat="server" TargetControlID="pnlSearchBoxContent"
                ExpandControlID="pnlSearchBoxHeader" CollapseControlID="pnlSearchBoxHeader" CollapsedImage="~/Images/expand-plus.png"
                ExpandedImage="~/Images/collapse-minus.png" Collapsed="true" ImageControlID="imgSearchBoxToggle"
                SuppressPostBack="True">
            </asp:CollapsiblePanelExtender>
            <asp:Panel ID="pnlSearchBoxHeader" runat="server">
                <div class="SearchBoxContainer">
                    <div class="SearchTitleContainer">
                        <div class="ArrowContainer">
                            <asp:ImageButton ID="imgSearchBoxToggle" runat="server" ImageUrl="~/Images/expand-plus.png"
                                AlternateText="collapse" />
                        </div>
                        <div class="TitleContainer">
                            [View More]
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlSearchBoxContent" runat="server"  CssClass ="filter-section" Height="0">
                <div class="TableRow spacer">
                
                &nbsp;&nbsp;<asp:Label ID ="Label3" runat ="server"  Font-Bold ="true"   Font-Size="Medium">Error Message : </asp:Label>
                   <asp:Label ID ="lblCurrentErrorMessage" runat ="server" Width ="80%"  Font-Size="Medium" ForeColor="Red"></asp:Label><br /><br />
                &nbsp;&nbsp;<asp:Label ID ="Label4" runat ="server"  Font-Bold ="true"   Font-Size="Medium">Description &nbsp;&nbsp;&nbsp; : </asp:Label>
                   <asp:Label ID ="lblCurrentErrorInnerException" runat ="server" Width ="80%" ForeColor="Red" Font-Size="Medium"></asp:Label><br />                  
                </div>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
                
            </div>
        </div>
    </div>

   </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
