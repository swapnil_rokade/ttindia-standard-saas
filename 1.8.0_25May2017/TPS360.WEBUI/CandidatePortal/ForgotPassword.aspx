﻿
<%@ Page Language="C#" MasterPageFile="~/CandidatePortal/PortalPublic.master" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs"
    Inherits="TPS360.Web.UI.ForgotPassword" Title="Forgot Password" %>
	<%@ Register Src="~/Controls/Forgotpassword.ascx" TagName="regI" TagPrefix="uc1" %>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" Runat="Server" >
<div style =" padding : 18px;">
    <asp:UpdatePanel ID="pnlRegistrationInformation" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divRegInternal" runat="server">
                 <uc1:regI ID="rgInternal" runat="server" />                                      
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
</asp:Content>
