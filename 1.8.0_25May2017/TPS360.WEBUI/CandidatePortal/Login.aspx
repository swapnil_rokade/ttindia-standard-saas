
<%---------------------------------------------------------------------------------------------------------------------------------------------
    FileName:Login.aspx
    Description:This is the candidate login page
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date              Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
       0.1             15/Feb/2016        pravin khot         Sign Up button disable 
-------------------------------------------------------------------------------------------------------------------------------------------       
--%><%@ Page Language="C#" MasterPageFile="~/CandidatePortal/PortalPublic.master" AutoEventWireup="true" CodeFile="Login.aspx.cs"
    Inherits="TPS360.Web.UI.Login" Title="" %>

<asp:Content ID="cDefault" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script language="javascript" type="text/javascript">
        function HandleFailureText() {
            var HandleFailureText = document.getElementById('FailureAlert');
            if (HandleFailureText.innerText == "Your login attempt was not successful. Please try again. ")
                HandleFailureText.innerText = "";
        }
        function showAlert() {
            var failureAlert = document.getElementById('FailureAlert');
              if (failureAlert.innerHTML.trim() != "") {
                failureAlert.style.visibility = "visible";
            }
        }
        window.onload = showAlert;
    </script>

    <br />
    <br />
    <br />
    <div class="MidBodyBoxRow">
        <asp:Login ID="LoginUser" LoginButtonStyle-CssClass="CommonButton" runat="server"
            OnLoggedIn="LoginUser_LoggedIn" OnLoginError="LoginUser_Error" TitleText="" Width="100%"
            DisplayRememberMe="true" OnLoggingIn="LoginUser_LoggingIn">
            <LoginButtonStyle CssClass="CommonButton"></LoginButtonStyle>
            <LayoutTemplate>
                <div class="MainBox" style="width: 400px">
                    <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                    <div class="MasterBoxContainer2">
                        <div class="section-header" style="text-align:center">
                            Log In to Candidate Portal
                            
                           <%-- *********Code commented by pravin khot on 15/Feb/2016 Using for Sign Up button disable***********--%>
                              <%--<a class="btn btn-success"  style="position: absolute; right: 15px; width:57px; 	height: 25px;	margin-top:10px;padding: 0px 10px;text-align: center;line-height: 22px; border-radiua: 5px"
                                 href="candidateregistration.aspx">
                              Sign Up</a>--%>
                            <%--  **********************END******************************--%>
                        </div>
                        <div class="ChildBoxContainer2" style="padding: 0px 15px 40px 15px;">
                            <div id="LoginContainer" style="font-size: 14px; color: #333333">
                                <div>
                                    <div style="text-align: center;">
                                    </div>
                                </div>
                                <div style="margin-left: auto; margin-right: auto;">
                                    <table border="0" cellpadding="0" style="margin-left: auto; margin-right: auto;">
                                        <tr>
                                            &nbsp;
                                            <td id="FailureTextTd" align="center" style="color: Red;">
                                                <div id="FailureAlert" class="alert nomargin" style="visibility: hidden;white-space:normal;width:258px;padding:8px 10px;">
                                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="ctl00_cphHomeMaster_LoginUser_UserName">
                                                    Username</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-bottom: 8px;">
                                                <asp:TextBox ID="UserName" runat="server" CssClass="CommonTextBox" Width="325" TextMode="SingleLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="ctl00_cphHomeMaster_LoginUser_Password">
                                                    Password</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="Password" runat="server" CssClass="CommonTextBox" Width="325" TextMode="Password"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding-top: 10px; padding-bottom: 8px;">
                                                <asp:Button ID="LoginButton" runat="server" CommandName="Login" CssClass="login-button btn btn-primary"
                                                    Text="Log In" ValidationGroup="LoginUser" OnClientClick="HandleFailureText();"
                                                    Width="150" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="float: left">
                                                    <asp:CheckBox ID="RememberMe" CssClass="LoginText" runat="server" Text="Remember me" Visible="False" />
                                                </div>
                                                <div style="float: left">
                                                    <asp:LinkButton ID="lnkForgotPassword" runat="server" Text="Forgot Password?" PostBackUrl="ForgotPassword.aspx"></asp:LinkButton>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </LayoutTemplate>
        </asp:Login>
    </div>

    <script language="javascript" type="text/javascript">
        document.getElementById('ctl00_cphHomeMaster_LoginUser_UserName').focus();
    </script>

</asp:Content>
