﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:PasswordReset.ascx.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              19/May/2016          pravin khot         modify code btnReset_Click
 *  0.2              20/May/2016          pravin khot         modify code btnContinue_Click
-------------------------------------------------------------------------------------------------------------------------------------------       

>*/

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using System.Web.Configuration;
using System.Globalization ;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using WebChart;
using AjaxControlToolkit;
using System.Data.SqlClient;
using TPS360.BusinessFacade;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.IO;
using System.Net;
using System.Xml;
using iTextSharp.text.pdf;
using iTextSharp.text;
using PdfSharp.Pdf;
using PdfSharp.Drawing;

namespace TPS360.Web.UI
{
    public partial class PasswordReset : BasePage
    {

        private int _ResetRequestId = 0;
        public int CurrentRequestId
        {
            get
            {

                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PORTAL_PASSWORDRESET_ID]))
                {
                    _ResetRequestId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PORTAL_PASSWORDRESET_ID]);
                }


                return _ResetRequestId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            //*************Code added by pravin khot on 20/May/2016********
            if (Request.Url.ToString().ToLower().Contains("candidateportal"))
            {
                Helper.Url.Redirect(UrlConstants.CandidatePortal.LOGIN);
            }
            else
            {
                Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE); //old code 
            }
            //***************************END*******************************          
           
        }

        int noofallowedusers()
        {
            int allowedusers = 0;
            TPS360SectionHandler licenseSection = WebConfigurationManager.GetSection("tps360license") as TPS360SectionHandler; ;
            if (licenseSection != null)
            {
                try
                {
                    allowedusers = Convert.ToInt32(licenseSection.Details["NoofUsersAllowed"].Value);
                }
                catch { }
            }
            return allowedusers;
        }
        //**************Code added by pravin khot on 19/May/2016*********
        public static string GetFileContentFromUrl(string url)
        {
            string fileContent = string.Empty;

            try
            {
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                fileContent = StringHelper.Convert(reader.ReadToEnd());
                reader.Close();
                response.Close();
            }
            catch (Exception)
            {

            }

            return fileContent;
        }

        private void SendChangePasswordEmail(string mailFrom, string mailTo, string mailBody)
        {
            string sentStatus = string.Empty;
            EmailHelper emailManager = new EmailHelper();
            emailManager.To.Clear();
            emailManager.From = (mailFrom != "") ? mailFrom : "admin@tps360.com";
            emailManager.To.Add(mailTo);
            emailManager.Subject = "New Password For Talentrackr Access.";
            emailManager.Body = mailBody.ToString();
            //sentStatus = emailManager.Send(base.CurrentMember.Id);
            //sentStatus = emailManager.Send();//comment by pravin khot on 17/June/2016
            sentStatus = emailManager.SendMailFromSystemEmailId();//added by pravin khot on 17/June/2016


        }
        //*********************END*********************************


        protected void btnReset_Click(object sender, EventArgs e)
        {
          bool isvalid = false;
            TPS360.Common.BusinessEntities.PasswordReset reset = new TPS360.Common.BusinessEntities.PasswordReset();
            //reset = Facade.GetPasswordResetById(15);
            reset = Facade.GetPasswordResetById(CurrentRequestId);
            if (reset != null)
            {
                if (reset.IsValid == 1 && DateTime.Now <= reset.ExpiredDate)
                {
                    Member _member = Facade.GetMemberById(reset.MemberID);
                    if (_member != null)
                    {
                        //Response.Redirect(UrlConstants.CandidatePortal.HOME); //code commented by pravin khot on 19/May/2016

                        //*********Code added by pravin khot on 19/May/2016*************
                        MembershipUser user = Membership.GetUser(_member.UserId);
                        EmailHelper emailHelper = new EmailHelper();
                        if (user != null && user.UserName != "" && !user.IsLockedOut)
                        {
                            string memberName = user.UserName;
                            string strOldPassword = user.GetPassword();

                            if (txtNewPassword.Text.Trim() == txtConfirmPassword.Text.Trim())
                            {
                                user.ChangePassword(strOldPassword, txtNewPassword.Text.Trim());
                                Membership.UpdateUser(user);

                                string mailFrom = "";
                                SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)TPS360.Common.Shared.SettingType.SiteSetting);
                                if (SiteSetting == null)
                                {
                                    Hashtable config = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                                    mailFrom = config[TPS360.Common.Shared.DefaultSiteSetting.AdminEmail.ToString()].ToString();
                                }
                                else mailFrom = SiteSetting[TPS360.Common.Shared.DefaultSiteSetting.AdminEmail.ToString()].ToString();
 
                               
                                string mailTo = user.UserName;

                                string changePasswordTemplateUrl = AppSettings.ChangePasswordTemplateUrl;
                                //string mailBody = GetFileContentFromUrl(changePasswordTemplateUrl);
                                string mailBody = "";
                                mailBody = emailHelper.CandidateChangePassword(_member);

                               // memberName = _member.FirstName + " " + _member.MiddleName + " " + _member.LastName;

                                StringBuilder stringBuilder = new StringBuilder();

                                stringBuilder.Append(mailBody);
                                stringBuilder.Replace("[MemberName]", memberName);
                                stringBuilder.Replace("[Password]", user.GetPassword());

                                SendChangePasswordEmail(mailFrom, mailTo, stringBuilder.ToString());

                                MiscUtil.ShowMessage(lblMessage, "New password has been saved successfully.", false);

                                txtNewPassword.Text = String.Empty;
                                txtConfirmPassword.Text = String.Empty;

                                Helper.Url.Redirect(UrlConstants.LOGIN_PAGE); 
                            }
                        }
                        //***********************END************************


//                        try
//                        {
//                            isvalid = true;

//                            MembershipCreateStatus status;
//                            MembershipUser user = Membership.GetUser(_member.PrimaryEmail);
//                            user.UnlockUser();
//                            user.ChangePassword(user.GetPassword(), txtConfirmPassword.Text);
//                            reset.IsValid = 0;
//                            Facade.UpdatePasswordReset(reset);
//                            FormsAuthentication.SetAuthCookie(user.UserName, true);
//                            #region LoginDetailsCheck
//                            string varchrDomainName = GettingCommonValues.GetDomainName(); //Request.ServerVariables["HTTP_HOST"];



//                            int i = Facade.EntryofUserAccessApp(user.UserName, Session.SessionID, "TPS", varchrDomainName, "", noofallowedusers(),Request.UserHostName);
//                            SecureUrl urlLogin;
//                            if (i == -5)
//                            {
//                                urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
//                                Response.Cookies["LoginStatus"].Value = "User login not permitted from IP : " + Request.UserHostName;// "Maximum number of users reached";
//                                Helper.Url.Redirect(urlLogin.ToString());
//                                return;
//                            }
//                            else if (i == -7)
//                            {
//                                urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
//                                Response.Cookies["LoginStatus"].Value = "Only one session is allowed per browser.";// "Maximum number of users reached";
//                                Helper.Url.Redirect(urlLogin.ToString());
//                                return;
//                            }
//                             else if (i == -3)
//                            {
//                                urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
//                                Response.Cookies["LoginStatus"].Value = "Number of concurrent users allowed by license has been met. <br> Please try again after another user logs off.";// "Maximum number of users reached";
//                                Helper.Url.Redirect(urlLogin.ToString());
//                            }
//                            else if (i == -2)
//                            {

//                                urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_RESET, "", "UserID", user.UserName);
//                                Response.Cookies["LoginStatus"].Value = "User already logged in";
//                                Helper.Url.Redirect(urlLogin.ToString());
//                            }
//                            else if (i == 0)
//                            {
//                                FormsAuthentication.SetAuthCookie(user.UserName, true);
//                                if ((Request.Browser.Cookies))
//                                {
//                                    Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(30);
//                                    Response.Cookies["PBLOGIN"]["UNAME"] = base.CurrentUser.UserName;
//                                    Response.Cookies["PBLOGIN"]["UPASS"] = "";
//                                }


//                        // Delete the cookies
//                                else if (Request.Cookies["PBLOGIN"] != null)
//                                {
//                                    HttpCookie aCookie;
//                                    string cookieName;

//                                    cookieName = Request.Cookies["PBLOGIN"].Name;
//                                    aCookie = new HttpCookie(cookieName);
//                                    aCookie.Expires = DateTime.Now.AddDays(-1);
//                                    Response.Cookies.Add(aCookie);

//                                }
//                                Session["Loggedin"] = "Yes";
//                                //Response.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
//                                Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE, string.Empty, UrlConstants.PARAM_MSG, "Password reset successfully.");
//                            }
                           
//                            #endregion
////                            Helper.Url.Redirect(UrlConstants.Dashboard .DEFAULT_PAGE,string .Empty ,UrlConstants .PARAM_MSG ,"Password reset successfully." );
//                        }
//                        catch
//                        {
//                            isvalid = false;
//                        }
                    }
                }
            }
            if (!isvalid)
            {
                divReset.Visible = false;
                divResetFailed.Visible = true;
            }
        }
    }
}
