﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using System.Text;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
namespace TPS360.Web.UI
{
    public partial class SearchJobOpenings: BasePage 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) 
            {
                if(CurrentMember !=null)
                    hdnSearchJob.Value = MiscUtil.GetMemberNameById(CurrentMember.Id, Facade);
            }

            this.Page.Title = hdnSearchJob.Value + " - SearchJobOpenings";
            
            
            
            
            
            ddlState.Attributes.Add("onChange", "return State_OnChange('" + ddlState.ClientID + "','" + hdnSelectedStateId.ClientID + "')");
            ddlCountry.Attributes.Add("OnChange", "return Country_OnChange('" + ddlCountry.ClientID + "','" + ddlState.ClientID + "','" + hdnSelectedStateId.ClientID + "')");
            if (CurrentMember == null) Response.Redirect(UrlConstants.CandidatePortal.LOGIN);
            odsRequisitionList.SelectParameters["memberId"].DefaultValue = CurrentMember.Id.ToString ();
            if (!IsPostBack)
            {
                PrepareView();
                hdnSortColumn.Value = "btnPostedDate";
                hdnSortOrder.Value = "DESC";
            }
        }
        private void PrepareView()
        {
            MiscUtil.PopulateCountry(ddlCountry, Facade);
            MiscUtil.PopulateState(ddlState,Convert .ToInt32 (ddlCountry .SelectedItem .Value ), Facade);
            hdnSelectedStateId.Value = ddlState.SelectedItem.Value;
        }
        private void Clear()
        {
            txtJobTitle.Text = "";
            txtSkills.Text = "";
            txtCity.Text = "";
            ControlHelper.SelectListByValue(ddlCountry, "0");
            MiscUtil.PopulateState(ddlState, Convert.ToInt32(ddlCountry.SelectedItem.Value), Facade);
            hdnSelectedStateId.Value = "0";
            lsvJobPosting.DataBind();
        }

        #region Button Events
        protected void btnClear_Click(object sender, EventArgs args)
        {
            Clear();
        }
        protected void btnSearch_Click(object sender, EventArgs args)
        {

            MiscUtil.PopulateState(ddlState, Convert.ToInt32(ddlCountry.SelectedValue), Facade);
            ControlHelper.SelectListByValue(ddlState, hdnSelectedStateId.Value);
            lsvJobPosting.DataBind();
        }

        #endregion

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvJobPosting.FindControl(hdnSortColumn.Value);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                if (lnk.CommandArgument.Contains("[1]") || lnk.CommandArgument.Contains("[JMC].[ManagersCount]"))
                {
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Descending" : "Ascending"));
                }
                else
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        #region Listview Events

        protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
                if (jobPosting != null)
                {

                   
                    Label lblPostedDate = (Label)e.Item.FindControl("lblPostedDate");
                    HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    Label lblAppy = (Label)e.Item.FindControl("lblAppy");
                    ImageButton btnApply = (ImageButton)e.Item.FindControl("btnApply");
                    lblPostedDate.Text = jobPosting.PostedDate.ToShortDateString();
                    lnkJobTitle.Text = jobPosting.JobTitle;
                    lnkJobTitle.Attributes.Add("onclick", "javascript:parent.ShowModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "','700px','570px')");
                    lblCity.Text = jobPosting.City + ((!string.IsNullOrEmpty(jobPosting.City) && jobPosting.StateId > 0) ? ", " : string.Empty) + MiscUtil.GetStateNameById(jobPosting.StateId, Facade);
                    lblAppy.Visible = jobPosting.IsApplied;
                    btnApply.Visible = !jobPosting.IsApplied;
                    btnApply.CommandArgument = jobPosting.Id.ToString();
                }
            }
        }

        private void buildMemberJobApplied(string JID)
        {
            MemberJobApplied newjob = new MemberJobApplied();
            newjob.MemberId = CurrentMember.Id;
            newjob.JobPostingId = Convert.ToInt32(JID);
            Facade.AddMemberJobApplied(newjob);

        }
        protected void lsvJobPosting_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
           
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                }
                if (e.CommandName == "Apply")
                {
                    MemberDocument memberDocu = Facade.GetRecentResumeByMemberID(CurrentMember.Id);
                    if (memberDocu != null)
                    {
                        string JID = e.CommandArgument.ToString();
                        buildMemberJobApplied(JID);
                        Facade.MemberJobCart_AddCandidateToRequisition(CurrentMember.Id, CurrentMember.Id.ToString(), Convert.ToInt32(JID));
                        MiscUtil.ShowMessage(lblMessage, "success “Thank you for your application.”", false);
                        lsvJobPosting.DataBind();
                    }
                    else
                    {
                        MiscUtil.ShowMessage(lblMessage, "Please upload a resume before applying to jobs.", true);
                    }


                }
            }
            catch
            {
            }
           
        }

        protected void lsvJobPosting_PreRender(object sender, EventArgs e)
        {
            PlaceUpDownArrow();
        }
        protected void odsRequisitionList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
           
        }
        #endregion


    }
}