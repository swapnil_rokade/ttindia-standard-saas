﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidatePortalLeft.ascx.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
 * 0.1                 20/May/2016         pravin khot         For keva - comment My Jobs
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using TPS360.Providers;
using System.Text;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
namespace TPS360.Web.UI
{
    public partial class CandidatePortalLeft : BaseMasterPage
    {
        string _currentNode = string.Empty;
        string _currentParentNode = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {  try
            {
                if ( CurrentMember ==null)
                {
                    //FormsAuthentication.SignOut();
                    //FormsAuthentication.RedirectToLoginPage();
                    Response.Redirect(UrlConstants.CandidatePortal.LOGIN);
                }
            }
            catch { }

            
             _currentParentNode = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID ].ToString();
            _currentNode = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_ID].ToString();
            BuildMenu();
        }
        private void BuildMenu()
        {
            if (CurrentMember == null || CurrentUser == null)
            {
                return;
            }

            SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();

            SqlSiteMapProvider provider = SiteMap.Providers["SqlSiteMap"] as SqlSiteMapProvider;
            SiteMapNode root = null;
            bool setActive = false;
            if (_currentNode !="" && Convert.ToInt32(_currentNode) == (int)SiteMapType.CandidateProfileMenu)
            {
                setActive = true;
                root = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.CandidateProfileMenu));

            }
            else if (_currentNode != "" && Convert.ToInt32(_currentNode) == (int)SiteMapType.CandidateSearchJob)
            {
                setActive = true;
                root = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.CandidateSearchJob ));

            }
            else if (_currentParentNode != "") root = provider.FindSiteMapNodeFromKey(StringHelper.Convert(_currentParentNode));

            else
            {
                setActive = true;
                root = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.CandidateProfileMenu));
            }
           
            if (root != null)
            {
                SiteMapNodeCollection nodeList = root.ChildNodes;
                StringBuilder strMenu = new StringBuilder();

                if (nodeList != null && nodeList.Count > 0)
                {
                    foreach (SiteMapNode node in nodeList)
                    {

                        string strActiveClass = "";// node["Id"].Equals(_currentNode) ? " class=\"active\"" : "calss=\"active\"";
                        if (node["Id"].Equals(_currentNode) || setActive)
                        {
                            strActiveClass = " class=\"active\"";
                            setActive = false;
                        }
                        else strActiveClass = "";
                        if (node.Description != "Search Job Openings")//condition added by pravin khot on 10/Feb/2016
                        {
                            strMenu.Append(("<li " + strActiveClass + " ><a href=\"../" + BuildUrl(node.Url) + "\" title=\"" + node.Description + "\">" + node.Title + "</a></li>"));
                        }
                    }

                    //***********Code commented by pravin khot on 20/May/2016*** FOR KEVA ************
                    //strMenu.Append(("<li><a href='../CandidatePortal/MyJobs.aspx' title='My Jobs'>My Jobs</a></li>"));//code added by pravin khot on 10/Feb/2016 
                    //***********************************END*************************************
                    MenuList.InnerHtml = strMenu.ToString();   
                    
                }


            }
        }

        protected string BuildUrl(string url)
        {
            string pageUrl = url;
            pageUrl = (new SecureUrl(pageUrl)).ToString();

            return pageUrl;
        }

    }
}