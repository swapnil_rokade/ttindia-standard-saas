﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MyJobs.ascx.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
 * 0.1                 11/Feb/2016         pravin khot         Change jobtitle onclick url- (Modals/CareerJobDetail.aspx)
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using System.Text;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
namespace TPS360.Web.UI
{
    public partial class MyJobs:BasePage 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hdnMyJobs.Value = MiscUtil.GetMemberNameById(CurrentMember.Id, Facade);
                
            }
            this.Page.Title = hdnMyJobs.Value + " - My Jobs";

            if (CurrentMember == null) Response.Redirect(UrlConstants.CandidatePortal.LOGIN);

            odsRequisitionList.SelectParameters["MemberId"].DefaultValue = CurrentMember.Id.ToString();
            if (!IsPostBack)
            {
                hdnSortColumn.Value = "btnPostedDate";
                hdnSortOrder.Value = "DESC";
            }
        }
        #region Listview Events

        protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberJobApplied jobApplied = ((ListViewDataItem)e.Item).DataItem as MemberJobApplied;
                if (jobApplied != null)
                {


                    Label lblPostedDate = (Label)e.Item.FindControl("lblPostedDate");
                    HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    Label lblStatus = (Label)e.Item.FindControl("lblStatus");
                    lblPostedDate.Text = jobApplied.JobPostedDate .ToShortDateString();
                    lnkJobTitle.Text = jobApplied.JobTitle ;
                    //**********Code commented and change by pravin khot on 11/Feb/2016**********
                    //lnkJobTitle.Attributes.Add("onclick", "javascript:parent.ShowModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" +  jobApplied .JobPostingId + "','700px','570px')");
                    lnkJobTitle.Attributes.Add("onclick", "javascript:parent.ShowModal('" + UrlConstants.ApplicationBaseUrl + "Modals/CareerJobDetail.aspx?JID=" + jobApplied.JobPostingId + "','700px','570px')");
                    //*******************END**********************************************

                    //******Code added by pravin khot on 12/Feb/2016*************
                    if (jobApplied.CreatorId == 1)
                    {
                      lblStatus.Text = "Applied - " + jobApplied.CreateDate.ToShortDateString();
                      lblCity.Text = jobApplied.JobCity + ((!string.IsNullOrEmpty(jobApplied.JobCity) && jobApplied.JobState != string.Empty) ? ", " : string.Empty) + jobApplied.JobState;
                       
                    }
                    else  if (jobApplied.CreatorId == 2)
                    {
                        lblStatus.Text = "Rejected"; 
                        lblCity.Text = jobApplied.JobCity + ((!string.IsNullOrEmpty(jobApplied.JobCity) && jobApplied.JobState != string.Empty) ? ", " : string.Empty) + jobApplied.JobState;
                    }
                    else if (jobApplied.CreatorId == 3)
                    {
                      lblStatus.Text = jobApplied.JobState;
                      lblCity.Text = jobApplied.JobCity;
                    }                
                    

                    //*************************END*********************************
                    //CODE COMMENT BY PRAVIN KHOT ON 12/Feb/2016**********
                    //lblStatus.Text = "Applied - " + jobApplied.CreateDate.ToShortDateString();
                    //lblCity.Text = jobApplied.JobCity + ((!string.IsNullOrEmpty(jobApplied.JobCity) && jobApplied.JobState != string.Empty) ? ", " : string.Empty) + jobApplied.JobState;
                    //********************END**********************************
                }
            }
             
        }
        protected void lsvJobPosting_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                }
            }
            catch
            {
            }

        }

        protected void lsvJobPosting_PreRender(object sender, EventArgs e)
        {
            PlaceUpDownArrow();
        }
        protected void odsRequisitionList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {

        }
        #endregion
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvJobPosting.FindControl(hdnSortColumn.Value);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                if (lnk.CommandArgument.Contains("[1]") || lnk.CommandArgument.Contains("[JMC].[ManagersCount]"))
                {
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Descending" : "Ascending"));
                }
                else
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }

    }
}