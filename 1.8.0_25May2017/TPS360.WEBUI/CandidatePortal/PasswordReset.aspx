﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: PasswordReset.ascx
    Description: This is the user control page used to register the candidate
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             20/May/2016         pravin khot               added - lblMessage
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>
<%@ Page Language="C#" MasterPageFile="~/Public.master" AutoEventWireup="true" CodeFile="PasswordReset.aspx.cs"
    Inherits="TPS360.Web.UI.PasswordReset" Title="Password Reset" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <div class="MidBodyBoxRow">
        <div class="MainBox" style="width:600px; height: 10px">
       <%-- **************Added by pravin khot on 19/May/2016***********--%>
          <div class="TableRow" style="text-align: center;">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </div>
          <%--  *******************END******************************--%>
            <div class="MasterBoxContainer2">
                <div class="section-header">
                    Password Reset
                </div>
                <div class="ChildBoxContainer2">
                    <div class="BoxContainer">
                        <div runat="server" id="divReset">
                            <div class="TableHeading" style="height: 40px;">
                                <center>
                                    <div class="alert alert-info"> Please enter and confirm your new password.</div></center>
                            </div>
                            <div style="width: 98%;">
                                <div class="TableRow">
                                    <div class="TableFormLeble ">
                                        New Password:</div>
                                    <div class="TableFormContent">
                                        <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" CssClass="CommonTextBox"></asp:TextBox><font
                                            color="red"></font></div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtNewPassword"
                                            ErrorMessage="Please enter password." EnableViewState="False" Display="Dynamic"
                                            ValidationGroup="Profile"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtNewPassword"
                                            Display="Dynamic" ErrorMessage="Minimum password length is 6" ValidationExpression=".{6}.*"
                                            ValidationGroup="Profile" />
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble ">
                                        Confirm Password:</div>
                                    <div class="TableFormContent">
                                        <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" CssClass="CommonTextBox "></asp:TextBox><font
                                            color="red"></font></div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                                        <asp:RequiredFieldValidator ID="rfvReTypePassword" runat="server" ControlToValidate="txtConfirmPassword"
                                            ErrorMessage="Please confirm password." EnableViewState="False" Display="Dynamic"
                                            ValidationGroup="Profile"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cfvPassword" runat="server" ControlToCompare="txtNewPassword"
                                            ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="Passwords do not match."
                                            ValidationGroup="Profile"></asp:CompareValidator>
                                        <asp:RegularExpressionValidator ID="revRetypePassword" runat="server" ControlToValidate="txtConfirmPassword"
                                            Display="Dynamic" ErrorMessage="Minimum password length is 6" ValidationExpression=".{6}.*"
                                            ValidationGroup="Profile" />
                                    </div>
                                </div>
                                <br />
                                <div class="TableRow">
                                    <center>
                                        <asp:Button ID="btnReset" runat="server" Text="Reset Password" OnClick="btnReset_Click"
                                            ValidationGroup="Profile" CssClass="btn" /></center>
                                </div>
                            </div>
                        </div>
                        <div id="divResetFailed" runat="server" visible="false">
                            <div class="alert">Forgot password reset link expired.</div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                </div>
                                <div class="TableFormContent">
                                    <asp:Button ID="btnContinue" CssClass="btn" runat="server"
                                        Text="Continue" OnClick="btnContinue_Click" /></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
