﻿<%--
Name    :
created By :
create date :
puropose    :
----------------------------------------------------------------------------------------------------------------------------------
S.NO    Name                    date            Purpose    
1       Prasanth Kumar G        03Mar2017       Issue id 1206 (Introduced validateRequest=false in Page tag)
----------------------------------------------------------------------------------------------------------------------------------

--%>

<%@ Page Language="C#" MasterPageFile="~/CandidatePortal/CandidatePortalLeft.master" AutoEventWireup="true" CodeFile="CandidateProfile.aspx.cs" Inherits="TPS360.Web.UI.CandidateProfile" Title="Untitled Page" validateRequest=false %>
<%@ Register Src="~/Controls/BasicInfoEditor.ascx"  TagName ="BasicInfo" TagPrefix ="ucl"  %>
<%@ Register Src="~/Controls/AdditionalInfoEditor.ascx" TagName ="AdditionalInfoEditor" TagPrefix ="ucl"  %>
<%@ Register Src="~/Controls/ObjectiveSummaryEditor.ascx" TagName="ObjectiveSummaryEditor" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MemberSkillsEditor.ascx" TagName="Skills" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MemberExperienceEditor.ascx" TagName="Experience" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MemberEducationEditor.ascx" TagName="Education" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MemberCopyPasteResume.ascx" TagName="Resume" TagPrefix="ucl" %>
<%@ Register  Src ="~/Controls/MemberCertificationEditor.ascx" TagName ="Certification" TagPrefix ="ucl" %>
<%-- Add content controls here --%>

<asp:Content ID="Content3" ContentPlaceHolderID="cphCandidateMaster" Runat="Server">

<script >
$(document).ready(function (){
  
});

</script>


    <asp:HiddenField runat="server" ID="hdnCandidateProfile" />
    <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">Basic Info</a></li>
            <li><a href="#tab2" data-toggle="tab">Additional Info</a></li>
            <li><a href="#tab3" data-toggle="tab">Objective & Summary</a></li>
            <li><a href="#tab4" data-toggle="tab">Skills</a></li>
            <li><a href="#tab5" data-toggle="tab">Experience</a></li>
            <li><a href="#tab6" data-toggle="tab">Education</a></li>
            <li><a href="#tab7" data-toggle="tab">Resumes</a></li>
    </ul>
    <div class="tab-content">
         <div class="tab-pane active" id="tab1">
                <ucl:BasicInfo ID="uclBasicInfo" runat ="server" />
         </div>
         <div class="tab-pane" id="tab2" style =" width : 100%; overflow :auto ;">
            <ucl:AdditionalInfoEditor ID ="uclAdditionInfo" runat ="server" />
         </div>
         <div class="tab-pane" id="tab3">
            <ucl:ObjectiveSummaryEditor ID="uclObjuectiveandSummary" runat ="server" />
         </div>
         <div class="tab-pane" id="tab4">
            <ucl:Skills ID="uclSkills" runat ="server" />
         </div>
         <div class="tab-pane" id="tab5">
            <ucl:Experience ID="uclExperience" runat ="server" />
         </div>
         <div class="tab-pane" id="tab6">
            <ucl:Education ID="uclEducation" runat ="server" />
    <hr class="divider" style ="  margin-top : 10px; margin-bottom : 10px; "></hr>
            <ucl:Certification ID="uclCertification" runat ="server" />
         </div> 
         <div class="tab-pane" id="tab7">
            <ucl:Resume ID="uclResume" runat ="server" />
         </div>
    </div>
    </div>
    
  
    
    

           
</asp:Content>

