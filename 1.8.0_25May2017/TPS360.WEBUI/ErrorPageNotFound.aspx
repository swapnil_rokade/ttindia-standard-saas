
<%@ Page Language="C#" MasterPageFile="~/Public.master" AutoEventWireup="true" CodeFile="ErrorPageNotFound.aspx.cs" 
    Inherits="TPS360.Web.UI.ErrorPageNotFound" Title="Application Error" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
    
<asp:Content ID="cDefault" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <asp:UpdatePanel ID="pnlRequisition" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            
     
    <br />
    <br />
    <br />
    <div class="MidBodyBoxRow">
        <div class="MainBox" style="width: 55%;">
           
            <div class="MasterBoxContainer2">
                <div class="section-header" style="text-align: center; color: #FF0000; font-size :large; vertical-align: middle;">                            
                            Error Occurred                             
                        </div>
                        
            
                <div class="ChildBoxContainer2" style =" height : 170px; text-align : center ;  padding-top : 30px;">
                <img id="Img1" runat="server" src="~/Images/error_icon.png" alt="X" height="50" width="60" /><br /><br />
                        <asp:Label ID ="lblConfirmHeading" runat ="server" Width ="95%" Font-Bold ="true" Font-Size="Medium">The server returned a "404 Not Found".</asp:Label><br /><br />
 <asp:Label ID ="Label1" runat ="server" Width ="95%" Font-Bold ="true" >Something is broken.Please let us know what you were doing when this error occurred.We will fix it as soon as possible.</asp:Label><br />
 <asp:Label ID ="Label2" runat ="server" Width ="95%" Font-Bold ="true" >Sorry for inconvinience caused.</asp:Label><br /><br />
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="Images/button_home.png" PostBackUrl="Dashboard/Dashboard.aspx" Width="200" Height="30" />
                    <br />              
                </div>
             
                
            </div>
        </div>
    </div>

   </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
