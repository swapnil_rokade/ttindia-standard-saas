﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:Public.master.cs
    Description:This is the master page which inherited all page 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author               Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Feb-20-2009         Kalyani Pallagani     Defect Id:9031; Included Home link option  
 *  0.2              20/May/2016        pravin khot           added- lgsLogin.Visible = false;
-------------------------------------------------------------------------------------------------------------------------------------------       
*/
using System;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.Configuration;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace TPS360.Web.UI
{
    public partial class PublicMaster : BaseMasterPage
    {
        #region Variables

        ArrayList _permittedMenuIdList;

        #endregion

        #region Properties

        #endregion

        #region Methods

        private bool IsInPermitedMenuList(int menuId)
        {
            if (CurrentMember != null)
            {
                if (_permittedMenuIdList == null)
                {
                    _permittedMenuIdList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                }
            }

            if ((_permittedMenuIdList != null) && (_permittedMenuIdList.Count > 0))
            {
                if (_permittedMenuIdList.Contains(menuId))
                {
                    return true;
                }
            }

            return false;
        }

        private void AttachClientScript()
        {
            SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();

            SiteMapNode root = SiteMap.Providers["SqlSiteMap"].FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.ApplicationTopMenu));

            if (root != null)
            {
                SiteMapNodeCollection nodeList = root.ChildNodes;

                if (nodeList != null && nodeList.Count > 0)
                {
                    foreach (SiteMapNode node in nodeList)
                    {
                        if (IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
                        {
                            permittedNodeList.Add(node);
                        }
                    }
                }

                string script = "\n" +
                                "<script language=\"javascript\" type=\"text/javascript\">  " + "\n" +
                                "/* <![CDATA[ */                                            " + "\n" +
                                "                                                           " + "\n" +
                                "	GBL_NUMBER_OF_ITEMS = " + permittedNodeList.Count + "            " + "\n" +
                                "														    " + "\n" +
                                "/* ]]> */												    " + "\n" +
                                "</script>";

                if (!Page.ClientScript.IsClientScriptBlockRegistered("calc"))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "calc", script);
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            AttachClientScript();
            base.OnPreRender(e);
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Expires = -1;

            if (WebConfigurationManager.AppSettings["CopyRightsText"].ToString() != string.Empty)
            {
                lblCopyRights.Text = "Copyright © " + DateTime.Now.Year.ToString() + " " + WebConfigurationManager.AppSettings["CopyRightsText"].ToString() + "All rights reserved.";
            }
            if (WebConfigurationManager.AppSettings["VersionText"].ToString() != string.Empty)
            {
                lblVersionText.Text = WebConfigurationManager.AppSettings["VersionText"].ToString();
            }
            //*************Code added by pravin khot on 20/May/2016********
            if (Request.Url.ToString().ToLower().Contains("candidateportal"))
            {
                lgsLogin.Visible = false;
                imgTpsLogo.Visible = false;
            }
            //***************************END*******************************


            if (CurrentMember != null)
            {
                string welcomePageName = string.Empty;
                try
                {
                    if (base.CurrentUserRole == ContextConstants.ROLE_CANDIDATE)
                    {
                        welcomePageName = UrlConstants.Candidate.HOME_PAGE;
                    }
                    else if (base.CurrentUserRole == ContextConstants.ROLE_CLIENT || base.CurrentUserRole == ContextConstants.ROLE_PARTNER)
                    {
                        CompanyContact contact = Facade.GetCompanyContactByMemberId(base.CurrentMember.Id);
                    }
                    else if (base.CurrentUserRole == ContextConstants.ROLE_ADMIN || base.CurrentUserRole == ContextConstants.ROLE_EMPLOYEE || base.CurrentUserRole == ContextConstants.ROLE_CONSULTANT || base.CurrentUserRole == ContextConstants.ROLE_VENDOR|| base.CurrentUserRole == ContextConstants.ROLE_DEPARTMENT_CONTACT )
                    {
                        welcomePageName = UrlConstants.Dashboard.DEFAULT_PAGE;
                    }

                }
                catch
                {
                }
                string welcomeToolTip = "Dashboard: " + CurrentMember.FirstName + " " + CurrentMember.LastName;
                string welcomeNote = "<img src='Images/home.png' style='padding-right: 3px;' border='0' align='absmiddle' />Home";
                lnkMemberWelcomeNote.NavigateUrl = welcomePageName;
                lnkMemberWelcomeNote.Text = welcomeNote;
                settingPipe.Visible = true;
                lnkMemberWelcomeNote.ToolTip = welcomeToolTip;
                lnkMemberWelcomeNote.Visible = false;
                lgsLogin.Visible = false;
            }
        }

        protected void lgs_LogOut(object sender, EventArgs e)
        {
            string role = (Roles.GetRolesForUser(base.CurrentUserName)).GetValue(0).ToString();
            //Create Login activity history
            DateTime dtLogoutTime = DateTime.Now;
            MiscUtil.AddActivity(role, base.CurrentMember.Id, base.CurrentMember.Id, ActivityType.Logout, Facade);

            MembershipUser membershipUser = Membership.GetUser(base.CurrentUser.UserName, false);
            if (membershipUser != null)
            {
                MemberDailyReport memberDailyReport = Facade.GetMemberDailyReportByLoginTimeAndMemberId(membershipUser.LastLoginDate, base.CurrentMember.Id);
                if (memberDailyReport != null)
                {
                    memberDailyReport.LogoutTime = dtLogoutTime;
                    Facade.UpdateMemberDailyReport(memberDailyReport);
                }
            }
        }

        #endregion
    }
}