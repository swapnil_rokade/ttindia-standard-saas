﻿<%--
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: StartUp.aspx
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Nov-03-2008          Sandeesh        Defect id: 11827 ; Changes made for proper alignment of the controls.
    0.2             Mar-03-2010          Sudarshan       Defect id: 12149 ; Seperated lblMessage into another table. 
    0.3             Apr-13-2010          Ganapati Bhat   Defect id: 12439 ; Introduced Label instead of RequiredField Validator
    --%>

<%@ Page Language="C#" MasterPageFile="~/Public.master" AutoEventWireup="true" CodeFile="StartUp.aspx.cs"
    Inherits="TPS360.Web.UI.StartUp" Title="" %>

<asp:Content ID="cDefault" ContentPlaceHolderID="cphHomeMaster" runat="Server" >
  <div style =" margin-left :2%">
    <div class="MasterBoxContainer2">
        <div class="ChildBoxContainer2">
                    
                    <br /><br/><br/>
                    <div style="margin-left: auto; margin-right: auto; height:200px;">
                        <asp:Panel ID="content" runat="server" DefaultButton="SaveButton">
                        <div class="TableRow" >
                            <div class ="TableFormLeble">
                                <asp:Label ID="lbldomainName" CssClass="LicenseKey" runat="server" > 
                                                              Domain Name:</asp:Label>
                            </div>
                            <div class ="TableFormContent">
                                  <asp:TextBox ID="txtDomainName" runat="server" CssClass="CommonTextBox" Width="250"></asp:TextBox>
                                        <span class="RequiredField">*</span>
                            </div>
                        </div>
                          <div class="TableRow" >
                                <div class ="TableFormValidatorContent" style =" margin-left :42%">
                                    <asp:RequiredFieldValidator ID="rfvDomainName" ControlToValidate ="txtDomainName" runat ="server" Display ="Dynamic" ErrorMessage ="Domain name is required." ValidationGroup ="LoginUser" ></asp:RequiredFieldValidator>
                                </div> 
                          </div> 
                         <div class="TableRow" >
                            <div class ="TableFormLeble">
                                     <asp:Label ID="lblLicenseKey" CssClass="LicenseKey" runat="server" AssociatedControlID="LicenseKey"> 
                                                              License Key:</asp:Label>
                            </div>
                            <div class ="TableFormContent">
                                   <asp:TextBox ID="LicenseKey" runat="server" CssClass="CommonTextBox" Width="70"></asp:TextBox>
                                        <span class="RequiredField">*</span>
                            </div>
                        </div>
                          <div class="TableRow" >
                                <div class ="TableFormValidatorContent" style =" margin-left :42%">
                                
                                         <asp:RequiredFieldValidator ID="rfvLicenseKey" ControlToValidate ="LicenseKey" runat ="server" Display ="Dynamic" ErrorMessage ="License Key is required." ValidationGroup ="LoginUser" ></asp:RequiredFieldValidator>
                                         <asp:Label ID="LicenseKeyRequired" runat="server" 
                                            Text="License Key is required." Visible="False" ForeColor="Red" ToolTip="License Key is required."></asp:Label>
                                            
                                             <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
                                </div>
                          </div> 
                         <div class="TableRow" >
                            <div class ="TableFormLeble">
                            </div>
                            <div class ="TableFormContent">
                                 <asp:Button ID="SaveButton" runat="server" CommandName="Save" CssClass="CommonButton"
                                            Text="Save" ValidationGroup="LoginUser" OnClick="SaveButton_Click" />
                            </div>
                        </div>
                        
                        </asp:Panel>
            </div>

        </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
</asp:Content>
