﻿<%--
----------------------------------------------------------------------------------------------------------------------------------
    FileName: Controls/Campaign.ascx
    Description: This is the user control page used to create new requisition.
    Created By: 
    Created On:
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date            Author            Modification
    -------------------------------------------------------------------------------------------------------------------------------------------------
    0.1            Nov-03-2008     Shivanand         Defect ID: 8800; Required field validators are added for dropdownlist "ddlPriceCurrencyLookupId"
                                                        and "ddlExpectedRevenueCurrencyLookupId".
    0.2            Jan-29-2009     Nagarathna        Defect ID: 9209,9066; changed the div style of fileupload.       
    0.3            May-22-2009     Gopala Swamy      Defect Id: 10443;    Put gridview     
    0.4            June-18-2009    Gopala Swamy J    Defect Id: 10719; Commented the code which comes under "Application Site Setting" tab which had told to be deleted                                       
    0.5            June-18-2009    Gopala Swamy J    Defect Id: 10720; Made display "none" 
    0.6            June-18-2009    Gopala Swamy J    Defect Id: 10718; Renamed from "Application Site Settings" to "Site Settings"
    0.7            Sept-07-2009    Nagarathna V.B    Defect ID:11445; add Range validator for SMTP Port field.
    0.8            Oct-09-2009     Gopala Swamy J    Defect ID:11585; Added new Custom validator and javascript method "validateSMTP()"
    0.9            Nov-04-2009     Rajendra          Defect ID:11583; Modified error Message in javascript method "validateSMTP()"
    0.10           27/April/2016   pravin khot       added new field-chkRequisition , chkEnableupdatecandidatesinVendorPortal
    0.11           15/July/2016    pravin khot       added-ddlTimeZone
    0.12            8/Aug/2016     pravin khot       added-ChkShowPreciseSearchAllCandidates
----------------------------------------------------------------------------------------------------------------------------------------------------------
--%>

<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="ApplicationSiteSettings.aspx.cs"
    Inherits="TPS360.Web.UI.Admin.ApplicationSiteSettings" Title="Site Settings" %>


<asp:Content ID="cntApplicationWorkflowTitle" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    Application Settings
</asp:Content>
<asp:Content ID="cntApplicationWorkflow" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script src="../Scripts/fixWebkit.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        function validateSMTP() {
            var txtValidCurrency = $get('<%= txtSMTPort.ClientID %>').value.trim();
            var lblPortRange = $get('<%= lblPortRange.ClientID %>');
            var smtpPort = /^\d+$/;

            if (txtValidCurrency.search(smtpPort) == -1) {
                lblPortRange.innerHTML = "Please enter valid port number"; //0.9 
                lblPortRange.style.display = "block";
                return false;
            }

            if (txtValidCurrency > 65535) {
                lblPortRange.innerHTML = "Please enter valid port number";   //0.9 
                lblPortRange.style.display = "block";
                return false;
            }
            if (txtValidCurrency < 65535) {
                lblPortRange.innerHTML = "";
                lblPortRange.style.display = "none";
                return true;
            }


            return false;

        }

        function Remove() {
            document.getElementById('<%=txtSMTPwd.ClientID %>').value = "";
        }

    </script>

    <div style="width: 100%;">
        <asp:UpdatePanel ID="pnlDynamic" runat="server">
            <ContentTemplate>
                <div style="text-align: left">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblCountrySettingHeader" runat="server" Text="1. Default Country"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="CommonDropDownList" AutoPostBack="false"
                            Width="158px">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblCurrency" runat="server" Text="2. Default Currency"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="CommonDropDownList" AutoPostBack="false"
                            Width="158px">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblSmtp" runat="server" Text="3. Default SMTP"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:TextBox ID="txtSMTP" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblSMTPUser" runat="server" Text="4. SMTP User"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:TextBox ID="txtSMTPUser" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblSMTPwd" runat="server" Text="5. SMTP Password"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:TextBox ID="txtSMTPwd" runat="server" TextMode="Password" OnClick="javascript:Remove()"></asp:TextBox>
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblSMTPort" runat="server" Text="6. SMTP Port"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 30%; float: left;">
                        <asp:TextBox ID="txtSMTPort" runat="server"></asp:TextBox>
                        <%--0.7 starts--%>
                        <asp:Label ID="lblPortRange" runat="server" ForeColor="Red" Text=""></asp:Label>
                        &nbsp;<asp:CustomValidator ID="CustomValidatorCYCurrency" runat="server" ClientValidationFunction="validateSMTP"
                            ControlToValidate="txtSMTPort" Display="Dynamic" EnableViewState="false" SetFocusOnError="true" />
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblSMTPSslEnable" runat="server" Text="7. SMTP SSL Enable"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:CheckBox ID="chkSMTPSslEnable" runat="server" />
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblAdminEmail" runat="server" Text="8. Admin Email"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 30%; float: left;">
                        <asp:TextBox ID="txtAdminEmail" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revAdminEmail" runat="server" ControlToValidate="txtAdminEmail"
                            Display="Dynamic" ErrorMessage="Please enter valid email address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ValidationGroup="g1"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblPaymentType" runat="server" Text="9. Default Payment Type"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="CommonDropDownList"
                            AutoPostBack="false" Width="158px">
                        </asp:DropDownList>
                    </div>
                </div>

                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblApplicationEdition" runat="server" Text="10. Application Edition"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:RadioButtonList ID="rdlApplicationEdtion" runat="server" RepeatDirection="Vertical">
                            <asp:ListItem Text="Staffing firm" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Internal hiring" Value="1"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblCompanyCompany" runat="server" Text="11. Company Name"></asp:Label>
                    </div>
                    <asp:TextBox ID="txtCompanyName" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                </div>
                </div> </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="Label1" runat="server" Text="12. Enable Req Publishing to Candidate Portal"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:CheckBox ID="chkEnableCandidatePortalPublishing" runat="server" />
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblEmployeePortal" runat="server" Text="13. Enable Req Publishing to Employee Referral Portal"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:CheckBox ID="chkEnableEmployeePortal" runat="server" />
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblVendorPortal" runat="server" Text="14. Enable Req Publishing to Vendor Portal"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:CheckBox ID="chkEnableVendorPortal" runat="server" />
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblAutomaticEmail" runat="server" Text="15. Automatic Email Match' in Req Editor"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:CheckBox ID="chkAutomaticReqEditorEmail" runat="server" />
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblResumeParser" runat="server" Text="16. Resume Parser Download Link"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:TextBox ID="txtResumeParserLink" runat="server" CssClass="CommonTextBox" Style="width: 400px"></asp:TextBox>
                    </div>
                </div>
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="Label2" runat="server" Text="17. Associate requisitions with entities from module"></asp:Label>
                    </div>
                </div>
           
                <div style="text-align: left; width: 12%; float: left;">
                    <asp:DropDownList ID="ddlmodule" runat="server" CssClass="CommonDropDownList" AutoPostBack="false"
                        Width="158px">
                        <asp:ListItem Value="Department" Enabled="true" Text="Department" />
                        <asp:ListItem Value="Account" Enabled="true" Text="Account" />
                        <asp:ListItem Value="Vendor" Enabled="true" Text="Vendor" />
                    </asp:DropDownList>
                </div>
                
                
             <%--  *************Code added by pravin khot on 27/April/2016***************************--%>
             
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblEnableupdatecandidatesinVendorPortal" runat="server" Text="18. Allow Vendors to Update Candidate Profiles"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:CheckBox ID="chkEnableupdatecandidatesinVendorPortal" runat="server" />
                    </div>
                </div>
                
               <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblRequisition" runat="server" Text="19. Allow Candidate to be added on Multiple Requisition"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:CheckBox ID="chkRequisition" runat="server" />
                    </div>
                </div>
               <%-- *****************************END********************************************--%>
               
                  <%--  *************Code added by pravin khot on 15/July/2016***************************--%>
                 <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblTimeZone" runat="server" Text="20. Default TimeZone"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:DropDownList ID="ddlTimeZone" runat="server" CssClass="CommonDropDownList" AutoPostBack="false"
                            Width="158px">
                        </asp:DropDownList>
                    </div>
                </div>
                    <%-- *****************************END********************************************--%>
                    
                    
                         
             <%--  *************Code added by pravin khot on 8/Aug/2016***************************--%>
             
                <div style="clear: both; padding-top: 10px">
                    <div style="text-align: left; float: left; width: 40%; white-space: normal;">
                        <asp:Label ID="lblShowPreciseSearchAllCandidates" runat="server" Text="21. Hide Candidates under process in Precise Search"></asp:Label>
                    </div>
                    <div style="text-align: left; width: 12%; float: left;">
                        <asp:CheckBox ID="ChkShowPreciseSearchAllCandidates" runat="server" />
                    </div>
                </div>
                  <%-- *****************************END********************************************--%>
                  
                </div>
				 <%--App Settings mail Configuration--%>
                <div style="clear: both; padding-top: 10px; padding-bottom: 10px; text-align: center;">
                    <asp:Button ID="btnTest" CssClass="btn" runat="server" Text="Test" ValidationGroup="g1"
                        OnClick="btnTest_Click" OnClientClick="RemoveModal();" />
                    <asp:Button ID="btnSaveSetting" CssClass="btn btn-primary" runat="server" Text="Save"
                        OnClick="btnSaveSetting_Click" ValidationGroup="g1" OnClientClick="return validateSMTP()" />
                    <asp:UpdateProgress ID="UpdateProgress1" EnableViewState="false" runat="server" DisplayAfter="0"
                        DynamicLayout="true">
                        <ProgressTemplate>
                            <asp:Image runat="server" ID="imgLogo1" ImageUrl="~/Images/loading.gif" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div class="TableRow" style="text-align: center; width: 50%; margin-left: 25%; margin-top: 5px;">
                    <div id="divTestMessage" runat="server">
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
