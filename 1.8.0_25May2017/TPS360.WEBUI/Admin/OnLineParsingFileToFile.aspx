﻿<%--
----------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewPanels.aspx
    Description: Interview Panel
    Created By:  Pravin khot
    Created On:  19/nov/2015
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date            Author            Modification
    -------------------------------------------------------------------------------------------------------------------------------------------------
                                                            
   -------------------------------------------------------------------------------------------------------------------------------------------------------
--%>

<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="OnLineParsingFileToFile.aspx.cs"  Inherits="TPS360.Web.UI.Admin.OnLineParsingFileToFile" Title="Resume Parsing" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/OnLineParsingFileToFile.ascx" TagName="OnLineParsingFileToFile"
    TagPrefix="uc1" %>

<asp:Content ID="CntIP" ContentPlaceHolderID="cphHomeMasterTitle" Runat="Server">
    Resume Parsing
</asp:Content>
<asp:Content ID="CntIP1" ContentPlaceHolderID="cphHomeMaster" Runat="Server">


     <script src="../Scripts/fixWebkit.js" type="text/javascript"></script>
 <div style="width: 100%;">
        <asp:UpdatePanel ID="pnlDynamic" runat="server">
            <ContentTemplate>
         <uc1:OnLineParsingFileToFile ID="ucntrlCandidateList" runat="server" />
           </ContentTemplate>
        </asp:UpdatePanel>
</div>
</asp:Content>


 