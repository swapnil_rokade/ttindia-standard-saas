﻿<%---------------------------------------------------------------------------------------------------------------------------------------------
    FileName: HiringMatrixSetup.aspx
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------    --%>
<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="HiringMatrixSetup.aspx.cs"
    Inherits="TPS360.Web.UI.Admin.HiringMatrixSetup" Title="Hiring Matrix Setup"
    EnableEventValidation="false" %>

<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>
<asp:Content ID="cntLookupEditorHeader" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="cntLookupEditorTitle" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    Hiring Matrix Setup
</asp:Content>
<asp:Content ID="cntLookupEditor" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script language="javascript" type="text/javascript" src="../Scripts/fixWebkit.js"></script>

    <script type="text/javascript">
        function ddlSubmissionOnchange(e) {
            var val = document.getElementById('<%=hdnSub.ClientID %>');
            if (e.options[e.selectedIndex].value != '0') {
                val.value = e.options[e.selectedIndex].value;
            }
        }
        function ddlOfferOnchange(e) {
            var val = document.getElementById('<%=hdnOff.ClientID %>');
            if (e.options[e.selectedIndex].value != '0') {
                if (e.options[e.selectedIndex].value != '0') {
                    val.value = e.options[e.selectedIndex].value;
                }
            }
        }
        function ddlJoinOnchange(e) {
            var val = document.getElementById('<%=hdnJoi.ClientID %>');
            if (e.options[e.selectedIndex].value != '0') {
                if (e.options[e.selectedIndex].value != '0') {
                    val.value = e.options[e.selectedIndex].value;
                }
            }
        }

        function ddlInterviewOnchange(e) {
            var val = document.getElementById('<%=hdnInterview.ClientID %>');
            if (e.options[e.selectedIndex].value != '0') {
                if (e.options[e.selectedIndex].value != '0') {
                    val.value = e.options[e.selectedIndex].value;
                }
            }
        }    
        function SubmissionValidate(sender, args) {
            var currentVal = document.getElementById(sender.controltovalidate);
            var val = document.getElementById('<%=hdnOff.ClientID %>');
            var val1 = document.getElementById('<%=hdnJoi.ClientID %>');
            var hdnInterview = document.getElementById('<%=hdnInterview.ClientID %>');

            if (val.value == currentVal.value) {
                args.IsValid = false;
            }
            if (val1.value == currentVal.value) {
                args.IsValid = false;
            }
            if (hdnInterview.value == currentVal.value) { args.IsValid = false; }
        }
        function OfferValidate(sender, args) {
            var currentVal = document.getElementById(sender.controltovalidate);
            var val = document.getElementById('<%=hdnSub.ClientID %>');
            var val1 = document.getElementById('<%=hdnJoi.ClientID %>');
            var hdnInterview = document.getElementById('<%=hdnInterview.ClientID %>');
            if (val.value == currentVal.value) args.IsValid = false;
            if (val1.value == currentVal.value) args.IsValid = false;
            if (hdnInterview.value == currentVal.value) args.IsValid = false;
        }
        function JoinValidate(sender, args) {
            var currentVal = document.getElementById(sender.controltovalidate);
            var val = document.getElementById('<%=hdnOff.ClientID %>');
            var val1 = document.getElementById('<%=hdnSub.ClientID %>');
            var hdnInterview = document.getElementById('<%=hdnInterview.ClientID %>');
            if (val.value == currentVal.value) {
                args.IsValid = false;
            }
            if (val1.value == currentVal.value) {
                args.IsValid = false;
            }
            if (hdnInterview.value == currentVal.value) args.IsValid = false;
        }
        function InterviewValidate(sender, args) {
            var currentVal = document.getElementById(sender.controltovalidate);
            var val = document.getElementById('<%=hdnOff.ClientID %>');
            var val1 = document.getElementById('<%=hdnSub.ClientID %>');
            var hdnJoi = document.getElementById('<%=hdnJoi.ClientID %>');
            if (val.value == currentVal.value) {
                args.IsValid = false;
            }
            if (val1.value == currentVal.value) {
                args.IsValid = false;
            }
            if (hdnJoi.value == currentVal.value) args.IsValid = false;
        }
     
    </script>

    <asp:HiddenField ID="hdnSub" runat="server" />
    <asp:HiddenField ID="hdnOff" runat="server" />
    <asp:HiddenField ID="hdnJoi" runat="server" />
    <asp:HiddenField ID="hdnInterview" runat ="server" />
        <asp:HiddenField ID="hdnReferral" runat ="server" />
        <asp:HiddenField ID="hdnVendor" runat="server" /> 
        <asp:HiddenField ID="hdnOfferAccepted" runat="server" /> 
    <asp:HiddenField ID="hdnOfferDecline" runat="server" />             
    <div>
    <asp:UpdatePanel ID="pnlList" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
    <ig:WebTab ID="UltraWebTab1" runat="server" Width="100%">
    <Tabs>
  
     <ig:ContentTabItem runat="server" Text="Hiring Matrix" Key="editorTab">
    <Template>
    <div class="TabContentHolder">    
    <div style="width: 100%;text-align:center;">        
                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
                <div style="width: 90%;">             
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lbllevel" runat="server" Text="Add Level"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtLevel" runat="server" CssClass="CommonTextBox" TabIndex="1"></asp:TextBox>
                            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" TabIndex="1"
                                CssClass="CommonButton" />
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblOrder" runat="server" Text="Level Order"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <table style="display: inline">
                                <tr style="width: 100%">
                                    <td style="width: 75%">                                     
                                        <asp:ListBox ID="lstLevels" runat="server" SelectionMode="Single" Width="250px" TabIndex="2">
                                        </asp:ListBox>                              
                                        <asp:HiddenField ID="hdnDeletedItems" runat="server" />
                                        <asp:HiddenField ID="hdnUpdateItem" runat="server" />
                                    </td>
                                    <td style="width: 25%; text-align: center">
                                        <table>
                                            <tr>
                                                <td style="vertical-align: top">
                                                    <asp:ImageButton ID="imgUp" runat="server" ImageUrl="../Images/uparrow.gif" OnClick="imgUp_Click"
                                                        TabIndex="3" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: bottom">
                                                    <asp:ImageButton ID="imgDown" runat="server" ImageUrl="../Images/downarrow.gif" OnClick="imgDown_Click"
                                                        TabIndex="4" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">
                                                    <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/EditImage.png" OnClick="imgEdit_Click"
                                                        TabIndex="5" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: bottom">
                                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/DeleteImage.png"
                                                        OnClick="imgDelete_Click" TabIndex="6" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="TableRow">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="CommonButton" TabIndex="7"
                            OnClick="btnSave_Click" />
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblsubLevel" runat="server" Text="Submission Level"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlSubLevel" runat="server" CssClass="CommondropDownList" onchange="ddlSubmissionOnchange(this)"
                                ValidationGroup="HiringMatrix">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%;">
                            <asp:CustomValidator ID="cvsublevel" runat="server" ClientValidationFunction="SubmissionValidate"
                                ControlToValidate="ddlSubLevel" Display="Dynamic" EnableClientScript="true" ErrorMessage="Don't select same level to another"
                                ValidationGroup="HiringMatrix"></asp:CustomValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblofferLevel" runat="server" Text="Offered Level"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlOfferLevel" runat="server" CssClass="CommondropDownList"
                                onchange="ddlOfferOnchange(this)" ValidationGroup="HiringMatrix">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%;">
                            <asp:CustomValidator ID="cvofferlevel" runat="server" ClientValidationFunction="OfferValidate"
                                ControlToValidate="ddlOfferLevel" Display="Dynamic" EnableClientScript="true"
                                ErrorMessage="Don't select same level to another" ValidationGroup="HiringMatrix"></asp:CustomValidator>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lbljoinLevel" runat="server" Text="Joined Level"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlJoinLevel" runat="server" CssClass="CommondropDownList"
                                onchange="ddlJoinOnchange(this)" ValidationGroup="HiringMatrix">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%;">
                            <asp:CustomValidator ID="cvjoinlevel" runat="server" ClientValidationFunction="JoinValidate"
                                ControlToValidate="ddlJoinLevel" Display="Dynamic" EnableClientScript="true"
                                ErrorMessage="Don't select same level to another" ValidationGroup="HiringMatrix"></asp:CustomValidator>
                        </div>
                    </div>
                    
                    
                       <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblInterviewLevel" runat="server" Text="Interview Level"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlInterviewLevel" runat="server" CssClass="CommondropDownList"
                                onchange="ddlInterviewOnchange(this)" ValidationGroup="HiringMatrix">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%;">
                            <asp:CustomValidator ID="cvInterview" runat="server" ClientValidationFunction="InterviewValidate"
                                ControlToValidate="ddlInterviewLevel" Display="Dynamic" EnableClientScript="true"
                                ErrorMessage="Don't select same level to another" ValidationGroup="HiringMatrix"></asp:CustomValidator>
                        </div>
                    </div>
                    
                    
                       <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblReferralLevel" runat="server" Text="Referral Level"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlReferralLevel" runat="server" CssClass="CommondropDownList"
                                 ValidationGroup="HiringMatrix">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblVendorPortalSubmissionLevel" runat="server" Text="Vendor Portal Submission Level"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlVendorPortalSubmissionLevel" runat="server" CssClass="CommondropDownList"
                                 ValidationGroup="HiringMatrix">
                            </asp:DropDownList>
                        </div>
                    </div>                 
                    <div class="TableRow">
                        <asp:Button ID="btnSaveLevel" runat="server" Text="Save" CssClass="CommonButton"
                            TabIndex="7" ValidationGroup="HiringMatrix" OnClick="btnSaveLevel_Click" />
                    </div>
                </div>            
    </div>
     </div>
    </Template>
    </ig:ContentTabItem>
    
    <%-- ************** pravin khot - Candidate Hiring Status Update - 3/March/2017 - Start *****--%>
    <ig:ContentTabItem runat="server" Text="Candidate Hiring Status Configure" Key="editorTab">
             <Template>
    <div class="TabContentHolder" style="height:400px"> 
    <div style="width: 100%;text-align:center;">        
                <asp:Label ID="lblMessageNext" runat="server" EnableViewState="false"></asp:Label>
                <div class="alert alert-info" style="width:300px;margin: 10px auto 10px auto; font-size: 10px;
                text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);font-weight: bold;border-radius: 4px 4px 4px 4px;
                ">
                        Select a role to assign statuses.
                 </div>
    <div style="width: 90%;">
    <div class="TableRow">
         <div class="TableFormLeble">
                    <asp:Label ID="lblRoleText" runat="server" Text="Select Role"></asp:Label>:
         </div>   
         <div class="TableFormContent">
            <asp:DropDownList ID="ddlRole" runat="server" CssClass="CommondropDownList" AutoPostBack="true"
            DataValueField="RoleID" EnableViewState="true" Width="200" OnSelectedIndexChanged="ddlRole_OnSelectedIndexChanged">
            </asp:DropDownList>          
        </div>        
    </div>   
    <div  class="TableRow">
        <div class="TableFormContent" style="padding-left: 375px; font-size: small;">
             <asp:CheckBoxList ID="chkStatuses" runat="server" AutoPostBack="true" >
             </asp:CheckBoxList>
         </div>
    </div> 
    <div  class="TableRow">
        <div class="TableFormContent" style="padding-left: 430px; padding-top: 20px;">
            <asp:Button ID="btnSaveStatus" runat="server" Text="Save" OnClick="btnSaveStatus_OnClick"
            Visible="false"/>
         </div>
    </div>  
     <div  class="TableRow">
        <div class="TableFormContent" style="padding-left: 350px;">          
         </div>
    </div>        
     </div>
     </div>
    </div>   
    </Template>    
    </ig:ContentTabItem>    
    <%-- ************** pravin khot - Candidate Hiring Status Update - 3/March/2017 - End *****--%>
    
    
    </Tabs>
    </ig:WebTab>
    
    
    </ContentTemplate>
    </asp:UpdatePanel>
    </div>
</asp:Content>
