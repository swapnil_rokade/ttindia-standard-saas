﻿using System;
using System.Web.UI.WebControls;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections.Generic;
using System.Collections;

namespace TPS360.Web.UI.Admin
{
    public partial class DashboardAccess: AdminBasePage
    {
        #region Private variable

        private ArrayList _permitedWidgetIdList;

        #endregion

        #region Properties

        public ArrayList PermitedWidgetIdList
        {
            get
            {
                if (_permitedWidgetIdList == null)
                {
                    WidgetAccessDataAccess widgetDataAccess = new WidgetAccessDataAccess();
                    _permitedWidgetIdList = widgetDataAccess.GetWidgetAccessIdsByRole(ddlRole.SelectedItem.Value);
                }

                return _permitedWidgetIdList;
            }
            set
            {
                _permitedWidgetIdList = value;
            }
        }

        #endregion

        #region Methods

        private bool IsInPermitedWidgetList(int widgetId)
        {
            ArrayList permittedWidgetIdList = PermitedWidgetIdList;

            if ((permittedWidgetIdList != null) && (permittedWidgetIdList.Count > 0))
            {
                if (permittedWidgetIdList.Contains(widgetId))
                {
                    return true;
                }
            }

            return false;
        }

        private void SaveWidgetAccess()
        {
            if (IsValid)
            {
                string roleId = ddlRole.SelectedItem.Value;

                WidgetAccessDataAccess widgetAccessDataAccess = new WidgetAccessDataAccess();

                foreach (ListItem item in chkWidgetList.Items)
                {
                    bool isEnable = item.Selected;

                    if (isEnable && !IsInPermitedWidgetList(Convert.ToInt32(item.Value)))
                    {
                        widgetAccessDataAccess.AddWidgetAccess(Convert.ToInt32(roleId), Convert.ToInt32(item.Value));
                    }
                    else if ((!isEnable) && (IsInPermitedWidgetList(Convert.ToInt32(item.Value))))
                    {
                        widgetAccessDataAccess.DeleteWidgetAccessFromRole(Convert.ToInt32(roleId), Convert.ToInt32(item.Value));
                    }
                }

                MiscUtil.ShowMessage(lblMessage, "Successfully assigned access.", false);                
            }
        }

        private void PopulateWidgetList(string role)
        {
            chkWidgetList.Items.Clear();

            List<Widget> widgetList = new WidgetDataAccess().GetAllWidget();

            foreach (Widget w in widgetList)
            {
                ListItem item = new ListItem();
                item.Text = w.Name;
                item.Value = w.Id.ToString();
                item.Selected = IsInPermitedWidgetList(w.Id);

                chkWidgetList.Items.Add(item);
            }

            btnSave.Visible = true;
        }

        #endregion

        #region Events
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MiscUtil.PopulateCustomRole(ddlRole, Facade);
                btnSave.Visible = false;
            }
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateWidgetList(ddlRole.SelectedItem.Value);
            pnlDynamic.Update();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveWidgetAccess();
            PermitedWidgetIdList = null;
            PopulateWidgetList(ddlRole.SelectedItem.Value);
        }

        #endregion
    }
}