﻿/* -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: HiringMatrixSetup.aspx
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               29/Nov/2016         Prasanth Kumar G    Issue id 1041
-------------------------------------------------------------------------------------------------------------------------------------------       
*/
using System;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Configuration;
using System.Data;

namespace TPS360.Web.UI.Admin
{
    public partial class HiringMatrixSetup : AdminBasePage
    {
        #region Member Variables
        static int LevelID = 0;
        static bool _isNew = true;
        #endregion

        #region Properties

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            txtLevel.Focus();
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            if (!IsPostBack)
            {
                PrepareView();
                SetToolTip();

            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            uclConfirm.AddMessage("Warning: levels named Submitted, Offered, Joined and Hired are required to enter submission and hiring details.Changing the Hiring Matrix levels will affect existing data. Do you want to proceed?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
        }
        protected void btnSaveLevel_Click(object sender, EventArgs e)
        {
            var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
            var settings = webConfig.AppSettings.Settings;
            settings["Submission"].Value = ddlSubLevel.SelectedItem.Text;
            settings["Offer"].Value = ddlOfferLevel.SelectedItem.Text;
            settings["Join"].Value = ddlJoinLevel.SelectedItem.Text;
       
            settings["Interview"].Value = ddlInterviewLevel.SelectedItem.Text;
            if (settings["Referral"] != null)
                settings["Referral"].Value = ddlReferralLevel.SelectedItem.Text;
            else
                settings.Add(new System.Configuration.KeyValueConfigurationElement("Referral", ddlReferralLevel.SelectedItem.Text));
            if (settings["VendorLevel"] != null)
                settings["VendorLevel"].Value = ddlVendorPortalSubmissionLevel.SelectedItem.Text;
            else
                settings.Add(new System.Configuration.KeyValueConfigurationElement("VendorLevel", ddlVendorPortalSubmissionLevel.SelectedItem.Text));
            webConfig.Save();
            hdnSub.Value = string.Empty;
            hdnOff.Value = string.Empty;
            hdnJoi.Value = string.Empty;
            hdnInterview.Value = "";
            MiscUtil.ShowMessage(lblMessage, "Levels are assigned successfully", false);
        }
        protected void imgDown_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (lstLevels.SelectedIndex >= 0)
            {
                if (lstLevels.SelectedIndex != lstLevels.Items.Count - 1)
                {
                    int index = lstLevels.SelectedIndex;
                    string text = lstLevels.SelectedItem.Text;
                    string value = lstLevels.SelectedValue;
                    lstLevels.Items[lstLevels.SelectedIndex].Value = lstLevels.Items[lstLevels.SelectedIndex + 1].Value;
                    lstLevels.Items[lstLevels.SelectedIndex].Text = lstLevels.Items[lstLevels.SelectedIndex + 1].Text;
                    lstLevels.Items[lstLevels.SelectedIndex + 1].Value = value;
                    lstLevels.Items[lstLevels.SelectedIndex + 1].Text = text;
                    lstLevels.SelectedIndex = index + 1;
                }
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(MiscUtil), "Window", "<script>alert('Please select a level before moving.');</script>", false);
            }
        }

     

        protected void imgUp_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (lstLevels.SelectedIndex >= 0)
            {
                if (lstLevels.SelectedIndex != 0)
                {
                    int index = lstLevels.SelectedIndex;
                    string text = lstLevels.SelectedItem.Text;
                    string value = lstLevels.SelectedValue;
                    lstLevels.Items[lstLevels.SelectedIndex].Value = lstLevels.Items[lstLevels.SelectedIndex - 1].Value;
                    lstLevels.Items[lstLevels.SelectedIndex].Text = lstLevels.Items[lstLevels.SelectedIndex - 1].Text;
                    lstLevels.Items[lstLevels.SelectedIndex - 1].Value = value;
                    lstLevels.Items[lstLevels.SelectedIndex - 1].Text = text;
                    lstLevels.SelectedIndex = index - 1;
                }
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(MiscUtil), "Window", "<script>alert('Please select a level before moving.');</script>", false);
            }
        }

      

        protected void imgEdit_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (lstLevels.SelectedIndex >= 0)
            {
                hdnUpdateItem.Value = lstLevels.SelectedItem.Text.Trim();
                txtLevel.Text = MiscUtil.RemoveScript(lstLevels.SelectedItem.Text.Trim());             
                lbllevel.Text = "Update Level";
                btnAdd.Text = "Update";
                EnableDisableImages(false);
                _isNew = false;
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(MiscUtil), "Window", "<script>alert('Please select a level before editing.');</script>", false);
            }
        }

        protected void imgDelete_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (lstLevels.SelectedIndex >= 0)
            {
                int d = 0;
                int.TryParse(lstLevels.SelectedValue, out d);
                if (d > 0) hdnDeletedItems.Value += lstLevels.SelectedValue + ",";           
                lstLevels.Items.RemoveAt(lstLevels.SelectedIndex);
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(MiscUtil), "Window", "<script>alert('Please select a level before deleting.');</script>", false);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (_isNew)
            {
                if (MiscUtil.RemoveScript(txtLevel.Text.ToString().Trim()) != "")
                {
                    foreach (ListItem item in lstLevels.Items)
                    {
                        if (MiscUtil.RemoveScript(item.Text.Trim().ToLower()) == MiscUtil.RemoveScript(txtLevel.Text.Trim().ToLower()))
                        {
                            MiscUtil.ShowMessage(lblMessage, "Level name must be unique.", true);
                            return;
                        }
                    }
                    lstLevels.Items.Add(new ListItem(MiscUtil.RemoveScript(txtLevel.Text.Trim(), string.Empty), "#" + MiscUtil.RemoveScript(txtLevel.Text.Trim())));
                
                }
                else
                {
                    return;
                }
            }
            else
            {
                foreach (ListItem item in lstLevels.Items)
                {
                    if (hdnUpdateItem.Value == item.Text.Trim())
                    {
                        item.Text = MiscUtil.RemoveScript(txtLevel.Text.Trim());
                    }
                }
            }
            hdnUpdateItem.Value = "";
            _isNew = true;
            txtLevel.Text = "";
            lbllevel.Text = "Add Level";
            btnAdd.Text = "Add";
            EnableDisableImages(true);
        }     
       
      
        #endregion

        #region Methods

        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                Save();
                PrepareView();
                MiscUtil.ShowMessage(lblMessage, "Hiring Matrix Levels updated successfully", false);
                //LoadLevelsInList();
            }

        }

        private void Save()
        {
            int i = 0;
            foreach (ListItem item in lstLevels.Items)
            {
                int d = 0;
                Int32.TryParse(item.Value, out d);
                if (d > 0)
                {
                    HiringMatrixLevels hiringMatrixLevels = new HiringMatrixLevels();
                    hiringMatrixLevels.Id = Convert.ToInt32(item.Value);
                    hiringMatrixLevels.Name = MiscUtil.RemoveScript(item.Text);
                    hiringMatrixLevels.SortingOrder = i;
                    hiringMatrixLevels.UpdatorId = base.CurrentMember.Id;
                    Facade.UpdateHiringMatrixLevels(hiringMatrixLevels);
                }
                else
                {
                    HiringMatrixLevels hiringMatrixLevels = new HiringMatrixLevels();
                    hiringMatrixLevels.Name = MiscUtil.RemoveScript(item.Text);
                    hiringMatrixLevels.SortingOrder = i;
                    hiringMatrixLevels.CreatorId = base.CurrentMember.Id;
                    hiringMatrixLevels.UpdatorId = base.CurrentMember.Id;
                    Facade.AddHiringMatrixLevels(hiringMatrixLevels);
                }
                i++;
            }
          
            string[] delete = hdnDeletedItems.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in delete)
            {
                Facade.DeleteHiringMatrixLevelsById(Convert.ToInt32(s));
            }
        }
        void EnableDisableImages(bool value)
        {
            imgUp.Enabled = imgDown.Enabled = imgEdit.Enabled = imgDelete.Enabled = lstLevels.Enabled = value;
        }
        private void PrepareView()
        {
            IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
            lstLevels.DataSource = hiringMatrixLevels;
            lstLevels.DataTextField = "Name";
            lstLevels.DataValueField = "Id";
            lstLevels.DataBind();
            lstLevels = (ListBox)MiscUtil.RemoveScriptForDropDown(lstLevels);
            LoadLevelsInList();

            var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
            var submit = webConfig.AppSettings.Settings["Submission"];
            var offer = webConfig.AppSettings.Settings["Offer"];
            var join = webConfig.AppSettings.Settings["Join"];
            var interview = webConfig.AppSettings.Settings["Interview"];
            var Referral = webConfig.AppSettings.Settings["Referral"];
            var Vendor = webConfig.AppSettings.Settings["VendorLevel"];
            //Code introudced by Prasanth on 29Nov2016 issue id 1041
            var OfferAccepted = webConfig.AppSettings.Settings["OfferAccepted"];
            var OfferDecline = webConfig.AppSettings.Settings["OfferDecline"];
            //***************end**********************
            if (submit != null)
            {
                ControlHelper.SelectListByText(ddlSubLevel, submit.Value.ToString());
                hdnSub.Value = ddlSubLevel.SelectedValue;
            }
            if (offer != null)
            {
                ControlHelper.SelectListByText(ddlOfferLevel, offer.Value.ToString());
                hdnOff.Value = ddlOfferLevel.SelectedValue;
            }
            if (join != null)
            {
                ControlHelper.SelectListByText(ddlJoinLevel, join.Value.ToString());
                hdnJoi.Value = ddlJoinLevel.SelectedValue;
            }
            if (interview != null)
            {
                ControlHelper.SelectListByText(ddlInterviewLevel, interview.Value.ToString());
                hdnInterview.Value = ddlInterviewLevel.SelectedValue;
            }

            if (Referral != null)
            {
                ControlHelper.SelectListByText(ddlReferralLevel, Referral.Value.ToString());
                hdnReferral.Value = ddlReferralLevel.SelectedValue;
            }
            if (Vendor != null)
            {
                ControlHelper.SelectListByText(ddlVendorPortalSubmissionLevel, Vendor.Value.ToString());
                hdnVendor.Value = ddlVendorPortalSubmissionLevel.SelectedValue;
            }


            if (lstLevels.Items.Count > 0)
            {
                lstLevels.Items.FindByText("Rejected").Enabled = false;
                //lstLevels.Items[lstLevels.Items.Count - 1].Enabled = false;
            }
          
            //*********pravin khot – Candidate Hiring Status Update – 3/March/2017 – Start *****************            
            PrepareRole();
            //*********pravin khot – Candidate Hiring Status Update – 3/March/2017 – End *****************            
        }      
        //*********pravin khot – Candidate Hiring Status Update – 3/March/2017 – Start *****************            
        private void PrepareRole()
        {
            if (!IsPostBack)
            {
                MiscUtil.PopulateCustomUserRole(ddlRole, Facade);
            }
        }
        //*********pravin khot – Candidate Hiring Status Update – 3/March/2017 – End *****************            
        //*********pravin khot – Candidate Hiring Status Update – 3/March/2017 – Start *****************            
        protected void ddlRole_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            chkStatuses.Items.Clear();
            if (ddlRole.SelectedValue == "Please Select")
            {
                chkStatuses.Items.Clear();
                btnSaveStatus.Visible = false;
            }
            else if (ddlRole.SelectedValue != "" && ddlRole.SelectedValue != "Please Select")
            {
                PrepareViewForRole(Convert.ToInt16(ddlRole.SelectedValue));
                btnSaveStatus.Visible = true;
            }
        }
        //*********pravin khot – Candidate Hiring Status Update – 3/March/2017 – End ***************** 
        //*********pravin khot – Candidate Hiring Status Update – 3/March/2017 – Start *****************  
        private void PrepareViewForRole(int RoleId)
        {
            IList<CandidatesHireStatus> CandidStatus = Facade.GetAllHiringStatusasperRoleId(RoleId);
            if (CandidStatus != null)
            {
                for (int i = 0; i < CandidStatus.Count; i++)
                {
                    ListItem item = new ListItem();
                    item.Text = CandidStatus[i].Name;
                    item.Value = Convert.ToString(CandidStatus[i].ID);
                    chkStatuses.Items.Add(item);
                    if (CandidStatus[i].IsRemoved == true)
                        chkStatuses.Items[i].Selected = false;
                    else
                        chkStatuses.Items[i].Selected = true;
                }
            }
        }
        //*********pravin khot – Candidate Hiring Status Update – 3/March/2017 – End ***************** 
        //*********pravin khot – Candidate Hiring Status Update – 3/March/2017 – Start *****************  
        protected void btnSaveStatus_OnClick(object sender, EventArgs e)
        {
            int StatusID = 0;
            int Roleid = 0;
            int creatorId = CurrentMember.Id;
            bool IsRemovedFlag = true;
            Roleid = Convert.ToInt16(ddlRole.SelectedValue);
            for (int chkcount = 0; chkcount < chkStatuses.Items.Count; chkcount++)
            {
                if (chkStatuses.Items[chkcount].Selected)
                {
                    IsRemovedFlag = false;
                    StatusID = (Convert.ToInt16(chkStatuses.Items[chkcount].Value));
                    Facade.CandidHiringStatusRole_Save(Roleid, StatusID, creatorId, IsRemovedFlag);
                }
                else
                {
                    IsRemovedFlag = true;
                    StatusID = (Convert.ToInt16(chkStatuses.Items[chkcount].Value));
                    Facade.CandidHiringStatusRole_Save(Roleid, StatusID, creatorId, IsRemovedFlag);
                }
            }
            MiscUtil.ShowMessage(lblMessage, "successfully Added Statuses in Table.", false);
        }
        //*********pravin khot – Candidate Hiring Status Update – 3/March/2017 – End ***************** 
        private void LoadLevelsInList()
        {
            IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
            ddlSubLevel.DataSource = hiringMatrixLevels;
            ddlSubLevel.DataTextField = "Name";
            ddlSubLevel.DataValueField = "Id";
            ddlSubLevel.DataBind();
            ddlOfferLevel.DataSource = hiringMatrixLevels;
            ddlOfferLevel.DataTextField = "Name";
            ddlOfferLevel.DataValueField = "Id";
            ddlOfferLevel.DataBind();
            ddlJoinLevel.DataSource = hiringMatrixLevels;
            ddlJoinLevel.DataTextField = "Name";
            ddlJoinLevel.DataValueField = "Id";
            ddlJoinLevel.DataBind();

            ddlInterviewLevel.DataSource = hiringMatrixLevels;
            ddlInterviewLevel.DataTextField = "Name";
            ddlInterviewLevel.DataValueField = "Id";
            ddlInterviewLevel.DataBind();


            ddlReferralLevel.DataSource = hiringMatrixLevels;
            ddlReferralLevel.DataTextField = "Name";
            ddlReferralLevel.DataValueField = "Id";
            ddlReferralLevel.DataBind();

            ddlVendorPortalSubmissionLevel.DataSource = hiringMatrixLevels;
            ddlVendorPortalSubmissionLevel.DataTextField = "Name";
            ddlVendorPortalSubmissionLevel.DataValueField = "Id";
            ddlVendorPortalSubmissionLevel.DataBind();        

            ddlVendorPortalSubmissionLevel = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlVendorPortalSubmissionLevel);
            ddlReferralLevel = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlReferralLevel);
            ddlInterviewLevel = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlInterviewLevel);
            ddlSubLevel = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlSubLevel);
            ddlOfferLevel = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlOfferLevel);
            ddlJoinLevel = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlJoinLevel);

            ddlSubLevel.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
            ddlOfferLevel.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
            ddlJoinLevel.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
            ddlInterviewLevel.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
            ddlReferralLevel.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
            ddlVendorPortalSubmissionLevel.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        void SetToolTip()
        {
            imgEdit.ToolTip = "Edit";
            imgDelete.ToolTip = "Delete";
            imgDown.ToolTip = "Move Down";
            imgUp.ToolTip = "Move Up";
        }

        #endregion
    }
}