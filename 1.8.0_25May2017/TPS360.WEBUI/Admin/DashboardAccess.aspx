﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeFile="DashboardAccess.aspx.cs" Inherits="TPS360.Web.UI.Admin.DashboardAccess"
    Title="Dashboard Access" %>

<asp:Content ID="cntRoleAccessEditorHeader" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="cntRoleAccessEditorTitle" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    Dashboard Access Permissions
</asp:Content>
<asp:Content ID="cntRoleAccessEditor" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <div style="width: 100%;min-height:125px;">
        <div>
            <asp:UpdatePanel ID="pnlDynamic" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="TableRow" style="text-align: center">
                        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label EnableViewState="false" ID="lblRole" runat="server" Text="Role"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlRole" runat="server" CssClass="CommonDropDownList" AutoPostBack="true"
                                Width="200" DataValueField="RoleID" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" />
                        </div>
                    </div>
                    <%--<asp:CascadingDropDown ID="cddRole" runat="server" TargetControlID="ddlRole" Category="Role"
                        PromptText="Please select a role" LoadingText="Loading roles..." ServiceMethod="PopulateRoles"
                        ServicePath="~/DataService.asmx" />--%>
                    <asp:Panel ID="pnlSaveMenuAccess" runat="server">
                        <div class="TableRow" style="text-align: center">
                            <div class="TableFormLeble">
                                <asp:Label EnableViewState="false" ID="lblMenuAccess" runat="server" Text="Dashboard Access"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:CheckBoxList ID="chkWidgetList" runat="server">
                                </asp:CheckBoxList>
                            </div>
                        </div>
                        <br />
                        <div class="TableRow" style="text-align: center">
                            <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Save" OnClick="btnSave_Click" />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>
