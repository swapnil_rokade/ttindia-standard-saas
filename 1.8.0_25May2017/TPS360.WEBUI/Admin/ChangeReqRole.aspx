﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ChangeReqRole.ascx
    Description: 
    Created By: added by pravin khot from dispark
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
      0.1               4/March/2016     pravin khot         new added id=  uclRoleList1,uclRoleList2
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>


<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="ChangeReqRole.aspx.cs" Inherits="TPS360.Web.UI.ChangeReqRole"
    Title="Grant Access To Requisitions" %>
<%@ Register Src="~/Controls/MultipleItemPicker.ascx" TagName="RoleList" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MultipleSelect2ItemPicker.ascx" TagName="MultipleSelection" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MultipleSelect2ItemPicker.ascx" TagName="MultipleSelection1" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MultipleSelect2ItemPicker.ascx" TagName="MultipleSelection2" TagPrefix="ucl" %>


<asp:Content ID="cntChangeSPOC" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <div class="TabPanelHeader">
         Grant Access to Req.Workflow and Publish Sections
    </div>
    <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
     <div style="text-align: left; width: 100%; padding-top: 50px;">
     
        
          <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblRoles" runat="server" Text="Select Roles(Workflow Section) "></asp:Label>:
            </div>
            <div class="TableFormContent">
                <ucl:MultipleSelection ID="uclRoleList" runat="server" />
                    <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333; width: 20%; height: 100px; overflow: auto; float: left; display: none">
                        <asp:CheckBoxList ID="chkRole" runat="server" ValidationGroup="Valpublish" AutoPostBack="false" TabIndex="6">
                        </asp:CheckBoxList>
                    </div>
            </div>
        </div>       
   
   
<%--      *************************Code added by pravin khot on 4/March/2016*************--%>
       <br />
        
           <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="Label1" runat="server" Text="Select Roles(Publish Section)"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <ucl:MultipleSelection1 ID="uclRoleList1" runat="server" />
                    <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333; width: 20%; height: 100px; overflow: auto; float: left; display: none">
                        <asp:CheckBoxList ID="CheckBoxList1" runat="server" ValidationGroup="Valpublish" AutoPostBack="false" TabIndex="6">
                        </asp:CheckBoxList>
                    </div>
            </div>
        </div>
        
         <br />
        
           <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="Label2" runat="server" Text="Default Recruiter Assigned"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <ucl:MultipleSelection2 ID="uclRoleList2" runat="server" />
                    <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333; width: 20%; height: 100px; overflow: auto; float: left; display: none">
                        <asp:CheckBoxList ID="CheckBoxList2" runat="server" ValidationGroup="Valpublish" AutoPostBack="false" TabIndex="6">
                        </asp:CheckBoxList>
                    </div>
            </div>
        </div>
        
<%--        *************************END*******************************************--%>  
       <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <%--<asp:CompareValidator ID="cvRole" runat="server" ControlToValidate="chkRole"
                            ErrorMessage="Please select Role." EnableViewState="False" Display="Dynamic"
                            ValueToCompare="0" Operator="GreaterThan" ValidationGroup="TeamInfo"></asp:CompareValidator>--%>
                </div>
        </div>
        
        <br />
        
        <div class="TableRow">
            <div class="TableFormLeble">
            </div>
            <div class="TableFormContent">
                <asp:Button ID="btnSaveRole" Text="Save" runat="server" CssClass="CommonButton" OnClick="btnSaveRole_Click" ValidationGroup="TeamInfo" />
            </div>
        </div>
        
    </div>
</asp:Content>
