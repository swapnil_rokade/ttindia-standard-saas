﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: QuestionBank.aspx.cs
    Description: This is the InterviewerFeedback page.
    Created By: Prasanth Kumar G
    Created On: 16/Oct/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/
using System;
using TPS360.Common.Helper;
namespace TPS360.Web.UI.Admin
{
    public partial class QuestionBank : AdminBasePage
    {
        # region Member Variables

        int _memberId = 0;

        #endregion

        # region event

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            if (_memberId > 0)
            {
                string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                this.Page.Title = name + " - " + "Notes And Activities";
            }

        }
        # endregion
    }
}