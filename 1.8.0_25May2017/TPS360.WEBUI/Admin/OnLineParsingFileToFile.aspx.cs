﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: OnLineParsingFileToFile.aspx.cs
    Description: This is the OnLineParsingFileToFile page.
    Created By: pravin khot
    Created On: 19/nov/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/


using System;
using TPS360.Common.Helper;

namespace TPS360.Web.UI.Admin
{

    public partial class OnLineParsingFileToFile : AdminBasePage
    {

        # region Member Variables

        int _memberId = 0;

        #endregion

        # region event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            if (_memberId > 0)
            {
                string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                this.Page.Title = "OnLineParsingFileToFiles";
            }
        }
        # endregion
    }
}
