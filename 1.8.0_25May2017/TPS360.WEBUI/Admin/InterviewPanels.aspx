﻿<%--
----------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewPanels.aspx
    Description: Interview Panel
    Created By:  Pravin khot
    Created On:  19/nov/2015
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date            Author            Modification
    -------------------------------------------------------------------------------------------------------------------------------------------------
      0.1            06Mar2017      Prasanth Kumar G  Issue id 1206   ((Introduced validateRequest=false in Page tag)                                                            
   -------------------------------------------------------------------------------------------------------------------------------------------------------
--%>

<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="InterviewPanels.aspx.cs"  Inherits="TPS360.Web.UI.Admin.InterviewPanels" Title="Interview Panel" validateRequest=false%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/InterviewPanel.ascx" TagName="NotsActivities"
    TagPrefix="uc1" %>


<asp:Content ID="CntIP" ContentPlaceHolderID="cphHomeMasterTitle" Runat="Server">
    Interview Panel
</asp:Content>
<asp:Content ID="CntIP1" ContentPlaceHolderID="cphHomeMaster" Runat="Server">


     <script src="../Scripts/fixWebkit.js" type="text/javascript"></script>
 <div style="width: 100%;">
        <asp:UpdatePanel ID="pnlDynamic" runat="server">
            <ContentTemplate>
            <uc1:NotsActivities ID="ucntrlNotsActivities" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
</div>
</asp:Content>


 