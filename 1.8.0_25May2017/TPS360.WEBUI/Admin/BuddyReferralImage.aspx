﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="BuddyReferralImage.aspx.cs"
    Inherits="TPS360.Web.UI.Admin.BuddyReferralImage" Title="Buddy Rererral Change Image"
    EnableEventValidation="false" %>
    
<asp:Content ID="cntBuddyReferralImageTitle" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    Buddy Rererral Change Image 
</asp:Content>
<asp:Content ID="cntCandidateImport" ContentPlaceHolderID="cphHomeMaster" runat="Server">
<script type="text/javascript">
    function ValidateFileUpload(Source, args) {
        var fuData = document.getElementById('<%= fuBuddyImage.ClientID %>');
        var FileUploadPath = fuData.value;

        if (FileUploadPath == '') {
            // There is no file selected
            args.IsValid = false;
        }
        else {
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

            if (Extension == "png") {
                args.IsValid = true; // Valid file type
        }
        else {
                args.IsValid = false; // Not valid file type
        }
    }
 }
 
  function ValidateFileSize(Source, args) {
        var fuData = document.getElementById('<%= fuBuddyImage.ClientID %>');
        var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fuData.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
 
                //Set the Base64 string return from FileReader as source.
                image.src = args.target.result;
                       
                //Validate the File Height and Width.
                image.onload = function () {
                    var height = this.height;
                    var width = this.width;
                    if (height > 216 || width > 1305) {
                       // alert("Height and Width must not exceed 100px.");
                        return false;
                    }
                    //alert("Uploaded image has valid Height and Width.");
                    return true;
                };
 
            }
  }
 </script>
<asp:UpdatePanel ID="pnlBulkImport" runat="server">
<ContentTemplate>
<div class="TableRow" style="padding-top: 2em">
        <div class="TableFormLeble">
            Select Image:</div>
        <div class="TableFormContent">
            <asp:UpdatePanel ID="upDescriptionupload" runat="server">
                <ContentTemplate>
                <div style="text-align: left">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </div>
                    <asp:FileUpload ID="fuBuddyImage" runat="server" CssClass="CommonButton" Width="206px" TabIndex="36" />
                    <asp:Button ID="btnExport" Text="Upload" runat="server" CssClass="CommonButton" OnClick="btnExport_Click" ValidationGroup="UploadDocumentDescriotion" TabIndex="37" />
                    
                     <asp:CustomValidator ID="cvfileUpload" runat="server" ControlToValidate="fuBuddyImage" ClientValidationFunction="ValidateFileUpload"
                        ErrorMessage="Please select valid PNG image file."></asp:CustomValidator>
                        
                     <asp:CustomValidator ID="cvfileDimension" runat="server" ControlToValidate="fuBuddyImage" ClientValidationFunction="ValidateFileSize"
                        ErrorMessage="Please select PNG image file of size 1305 x 216."></asp:CustomValidator>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExport" />
                </Triggers> 
            </asp:UpdatePanel>
        </div>
    </div>
  
</ContentTemplate>

</asp:UpdatePanel>

</asp:Content>
