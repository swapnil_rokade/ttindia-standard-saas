﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:ChangeReqRole.aspx.cs
    Description: 
    Created By: pravin
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
 *     0.1             4/March/2016        pravin khot         new added- PopulateCustomUser(uclRoleList2.ListItem, Facade);
 *                                                                        Facade.GetCustomRoleRequisition(2);
 *                                                                        ListItem list2 in uclRoleList2.ListItem.Items)
-------------------------------------------------------------------------------------------------------------------------------------------       

>*/
using TPS360.Web.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Net.Sockets;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Net.Security;
using AjaxControlToolkit;



namespace TPS360.Web.UI
{
    public partial class ChangeReqRole : EmployeeBasePage 
    {
        const string AppName = "TPS";

        private UserPageSetup _Setup
        {
            get { return Context.Items[typeof(UserPageSetup)] as UserPageSetup; }
            set { Context.Items[typeof(UserPageSetup)] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                MiscUtil.PopulateCustomRole(uclRoleList.ListItem, Facade);
                uclRoleList.ListItem.Items.RemoveAt(0);

                MiscUtil.PopulateCustomRole(uclRoleList1.ListItem, Facade);
                uclRoleList1.ListItem.Items.RemoveAt(0);
                //***********Code added by pravin khot on 4/March/2016*********
                MiscUtil.PopulateCustomUser(uclRoleList2.ListItem, Facade);
                uclRoleList2.ListItem.Items.RemoveAt(0);
                //********************END*********************************
                PrepareView();
            }
           
        }
        #region Methods

        private void PrepareView()
        {

            IList<CustomRole> roleList = Facade.GetCustomRoleRequisition(0);
            if (roleList != null)
            {
                foreach (CustomRole role in roleList)
                {
                    foreach (ListItem list in uclRoleList.ListItem.Items)
                        if (Convert.ToInt32( list.Value) == role.Id)
                            list.Selected = true;
                }
            }

            IList<CustomRole> roleList1 = Facade.GetCustomRoleRequisition(1);
            if (roleList1 != null)
            {
                foreach (CustomRole role in roleList1)
                {
                    foreach (ListItem list in uclRoleList1.ListItem.Items)
                        if (Convert.ToInt32(list.Value) == role.Id)
                            list.Selected = true;
                }
            }

            //****************Code added by pravin khot on 4/March/2016***************
            IList<CustomRole> roleList2 = Facade.GetCustomRoleRequisition(2);
            if (roleList2 != null)
            {
                foreach (CustomRole role in roleList2)
                {
                    foreach (ListItem list in uclRoleList2.ListItem.Items)
                        if (Convert.ToInt32(list.Value) == role.Id)
                            list.Selected = true;
                }
            }
            //**************************END*********************************
        }

        protected void btnSaveRole_Click(object sender, EventArgs e)
        {
            StringBuilder vendorList = new StringBuilder();
            BusinessFacade.IFacade facade = new BusinessFacade.Facade();
            facade.DeleteRoleRequisitionDenial();
            int count=0;
            int sectionid = 0;

            foreach (ListItem list in uclRoleList.ListItem.Items)
            {
                if (list.Selected)
                {
                    count = 1;
                    try
                    {
                        IList<Member> memberList = facade.GetAllMemberByCustomrRoleId(Convert.ToInt32(list.Value));
                        facade.UpdateRoleRequisitionDenial(Convert.ToInt32(list.Value), CurrentMember.Id, sectionid);
                    }
                    catch (Exception ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }

            foreach (ListItem list1 in uclRoleList1.ListItem.Items)
            {
                if (list1.Selected)
                {
                    count = 1;
                    sectionid = 1;
                    try
                    {
                        IList<Member> memberList = facade.GetAllMemberByCustomrRoleId(Convert.ToInt32(list1.Value));
                        facade.UpdateRoleRequisitionDenial(Convert.ToInt32(list1.Value), CurrentMember.Id, sectionid);
                    }
                    catch (Exception ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
            //****************Code added by pravin khot on 4/March/2016******
            foreach (ListItem list2 in uclRoleList2.ListItem.Items)
            {
                if (list2.Selected)
                {
                    count = 1;
                    sectionid = 2;
                    try
                    {
                        IList<Member> memberList = facade.GetAllMemberByCustomrRoleId(Convert.ToInt32(list2.Value));
                        facade.UpdateRoleRequisitionDenial(Convert.ToInt32(list2.Value), CurrentMember.Id, sectionid);
                    }
                    catch (Exception ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
            //**********************END************************************


            if (count == 0)
            {
                MiscUtil.ShowMessage(lblMessage, "Select atleast one role.", true);
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Roles saved successfully.", false);
            }            
        }        
        #endregion
    }
}