﻿<%--
----------------------------------------------------------------------------------------------------------------------------------
    FileName: QuestionBank.aspx
    Description: Interview Assessment
    Created By:  Prasanth Kumar G
    Created On:  16/Oct/2015
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date            Author            Modification
    -------------------------------------------------------------------------------------------------------------------------------------------------
                                                            
   -------------------------------------------------------------------------------------------------------------------------------------------------------
--%>
<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true"  CodeFile="QuestionBank.aspx.cs" 
 Inherits="TPS360.Web.UI.Admin.QuestionBank"  Title="Interview Assessment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/QuestionBank.ascx" TagName="NotsActivities"
    TagPrefix="uc1" %>
 <asp:Content ID="cntQuestionBank" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    Interview Assessment [Question Bank]
</asp:Content>
<asp:Content ID="cntQuestionBankWorkflow" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script src="../Scripts/fixWebkit.js" type="text/javascript"></script>
 <div style="width: 100%;">
        <asp:UpdatePanel ID="pnlDynamic" runat="server">
            <ContentTemplate>
            <uc1:NotsActivities ID="ucntrlNotsActivities" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
</div>
</asp:Content>