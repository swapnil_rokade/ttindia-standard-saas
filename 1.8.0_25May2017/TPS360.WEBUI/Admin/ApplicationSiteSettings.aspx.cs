﻿/*<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ApplicationSiteSettings.aspx.cs
    Description: This is the  page Where admin can set default country and currency
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    0.1             Oct-16-2008          Gopala Swamy          Defect id: 8654; put else part in PopulateSiteSetting(),So the for the first time it sets default country and currency to USA and $ USD
    0.2             Feb-05-2009          N.Srilakshmi          Defect Id: 9838; Changes made in GetSiteSettingTable, PopulateSiteSetting, Prepareview
                                                                                Added lblPaymentType,ddlPaymentType,lblPaymentShow,lblPaymentTypeValue ApplicationSiteSettings.aspx
    0.3            Mar-09-2009          N.Srilakshmi           Defect Id: 10067; Changes made in GetSiteSettingTable, PopulateSiteSetting 
                                                                                Added new fields txtSMTPUser,txtSMTPwd,lblSMTPUserValue,lblSMTPwdValue
    0.4            Mar-23-2009          N.Srilakshmi           Defect Id: 10194; Changes made in GetSiteSettingTable, PopulateSiteSetting
    0.5            June-18-2009         Gopala Swamy J         Defect Id: 10719; Commented lines which comes under tab "Application Site Setting" 
    0.6            June-18-2009         Veda                   Defect Id: 10829; SMTP Password was not getting displyed after the save.
    0.7            13-Aug-2009          Shivanand              Defect Id: 10861 : Changes made in method btnSaveSetting_Click(), new method added "VerifyCredentials()"
    0.8            07-sept-2009         Nagarathna V.B         Defect Id: 11445 : Passing default value to SMTP port when it is blank.
    0.8            24-Nov-2009          Rajendra               Defect Id: 11597 : Changes made in method VerifyCredentials().
 * 0.10            31-Dec-2009          Srividya .S            Defect Id: 12056 : Changes made in method btnSaveSetting_Click(),  added "BuildMemberExInfo()"
 * 0.11            27/April/2016        pravin khot            New field added-AllowCandidatetobeaddedonMultipleRequisition ,chkEnableupdatecandidatesinVendorPortal
   0.12             21/June/2016        pravin khot           modify function - public string Send()  
 * 0.13             15/July/2016        pravin khot            added-ddlTimeZone
 * 0.14             8/Aug/2016          pravin khot            added-ChkShowPreciseSearchAllCandidates
 * --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------       

--%>*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Net.Sockets;
using System.Diagnostics;
using System.IO;
using Microsoft.Office.Interop.Outlook;



namespace TPS360.Web.UI.Admin
{
    public partial class ApplicationSiteSettings : AdminBasePage
    {
        #region Member Variables        
        private static int _memberId = 0;
        MemberExtendedInformation _memberExtendedInfo;

        #endregion

        #region properties
        private MemberExtendedInformation CurrentMemberExInfo
        {
            get
            {
                if (_memberExtendedInfo == null)
                {

                    if (_memberId > 0)
                    {
                        _memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(_memberId);
                    }
                    else
                    {
                        _memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(base.CurrentMember.Id);
                    }
                }

                return _memberExtendedInfo;
            }
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Prepareview();
            }
        }
       

        protected void btnSaveSetting_Click(object sender, EventArgs e)
        { 
            MemberExtendedInformation memberExInfo = BuildMemberExInfo();
             int IntSMTPPort=0;     
             if (txtSMTPort.Text.IsNotNullOrEmpty())
             {
                 IntSMTPPort = Convert.ToInt32(txtSMTPort.Text);
             }
             else 
             {
                 IntSMTPPort = 25;   
             }
            SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            if (siteSetting != null)
            {
                siteSetting.SettingType = (int)SettingType.SiteSetting;
                siteSetting.SettingConfiguration = GetSiteSettingTable();
                siteSetting.IsRemoved = false;
                siteSetting.CreatorId = base.CurrentMember.Id;                
                Facade.UpdateSiteSetting(siteSetting);
                    //MiscUtil.ShowMessage(lblMessage, "Site settings saved successfully.", false);
                divTestMessage.InnerHtml = "<div class=\"alert alert-success\">Site settings saved successfully.</div>";
            }
            else
            {
                siteSetting = new SiteSetting();
                siteSetting.SettingType = (int)SettingType.SiteSetting;
                siteSetting.SettingConfiguration = GetSiteSettingTable();
                siteSetting.IsRemoved = false;
                siteSetting.UpdatorId = base.CurrentMember.Id;                
                Facade.AddSiteSetting(siteSetting);
//MiscUtil.ShowMessage(lblMessage, "Site settings added successfully.", false);
                divTestMessage.InnerHtml = "<div class=\"alert alert-success\">Site settings added successfully.</div>";
            }
            PopulateSiteSetting();
        }

		 //App Settings mail Configuration
		protected void btnTest_Click(object sender, EventArgs args)
        {
            string pwd = txtSMTPwd.Text;

            if (Send() == "1")
            {
                divTestMessage.InnerHtml = "<div class=\"alert alert-success\">SMTP details verified</div>";
            }
            else
            {
                divTestMessage.InnerHtml = "<div class=\"alert alert-error\">Unable to connect with SMTP details</div>";
            }

            txtSMTPwd.Attributes.Add("value", pwd);
            txtSMTPwd.Text = pwd;
        }
        public string Send()
        {
            try
            {

                MailMessage mailMessage = new MailMessage();
                SmtpClient sendMail = new SmtpClient();
                MailAddress addressFrom = new MailAddress(txtSMTPUser.Text);
                mailMessage.From = addressFrom;

                //***************Code modify by pravin khot on 21/June/2016************
                //mailMessage.To.Add("talentrackr@gmail.com");
                //mailMessage.Subject = "Test Mail From" + txtSMTPUser.Text;
                mailMessage.To.Add(addressFrom);
                mailMessage.Subject = "Test Mail From Talentrackr for " + txtSMTPUser.Text;
                mailMessage.Body = "This is an e-mail message sent automatically by Talentrackr while testing the app settings.";
                //**************************END*****************************

                SmtpClient mailSender = new SmtpClient();
                string strSMTP = string.Empty;
                string strUser = string.Empty;
                string strPwd = string.Empty;
                string strssl = string.Empty;
                int intPort = 25;
                //strSMTP = txtSMTP.Text;
                strUser = txtSMTPUser.Text;
                TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
                SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                strSMTP = siteSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();

                if (txtSMTPwd.Text != "******")
                    strPwd = txtSMTPwd.Text;
                else
                {
                    MemberExtendedInformation MailSetting = Facade.GetMemberExtendedInformationByMemberId(base.CurrentMember.Id);

                    if (MailSetting.MailSetting != null)
                    {
                        //Hashtable MailSettingTables = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);
                        if (siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()] != null)
                        {
                            strPwd = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                        }
                    }
                }
                strssl = siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString();
                //strssl = chkSMTPSslEnable.Checked.ToString();

                //intPort = Convert.ToInt32(txtSMTPort .Text );
                intPort = Convert.ToInt32(siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString());
                mailSender = new SmtpClient(strSMTP, intPort);
                NetworkCredential SMTPUserInfo = new NetworkCredential(strUser, strPwd);

                mailSender.UseDefaultCredentials = false;
                mailSender.Credentials = SMTPUserInfo;
                mailSender.EnableSsl = strssl == "True" ? true : false;
                //***********Added by pravin khot on 8/March/2017***********
                int senderid = 0;
                string AdminEmailId = string.Empty;
                if (siteSetting != null)
                {
                    AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                    senderid = Facade.GetMemberIdByEmail(AdminEmailId);
                }
                MailQueueData.AddMailToMailQueue(senderid, strUser, mailMessage.Subject, mailMessage.Body, "", "", null, Facade);
                //mailSender.Send(mailMessage);
                //*****************END**********************
                return "1";
            }
            catch (System.Exception ex)
            {
                BasePage.writeLog(ex);
                return ex.Data.ToString();
            }

        }
        #endregion

        #region Methods

        private void Prepareview()
        {
            MiscUtil.PopulateCountry(ddlCountry, Facade);
            MiscUtil.PopulateCurrency(ddlCurrency, Facade);
            MiscUtil.PopulatePaymentType(ddlPaymentType);
            PopulateTimezoneList(); //line added by pravin khot on 15/July/2016*******
            PopulateSiteSetting();
        }

        //************Code added by pravin khot on 15/July/2016*******
        public void PopulateTimezoneList()
        {
            ddlTimeZone.Items.Clear();
            ddlTimeZone.DataSource = Facade.GetAllTimeZone();
            ddlTimeZone.DataTextField = "TimeZone";
            ddlTimeZone.DataValueField = "Timezoneid";
            ddlTimeZone.DataBind();
            ddlTimeZone = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlTimeZone);
            ddlTimeZone.Items.Insert(0, new ListItem("Select Timezone", "0"));

        }
        //**********************END*********************************

        private MemberExtendedInformation BuildMemberExInfo()
        {
            MemberExtendedInformation memberExInfo = CurrentMemberExInfo;

            memberExInfo.MailSetting = GetSiteSettingTable();

            if (_memberId > 0)
            {
                memberExInfo.MemberId = _memberId;
            }
            memberExInfo.CreatorId = base.CurrentMember.Id;
            memberExInfo.UpdatorId = base.CurrentMember.Id;

            return memberExInfo;
        }

        public byte[] GetSiteSettingTable()
        {
            Hashtable siteSettingTable = new Hashtable();
            siteSettingTable.Add(DefaultSiteSetting.Country.ToString(), ddlCountry.SelectedValue);
            siteSettingTable.Add(DefaultSiteSetting.TimeZone.ToString(), ddlTimeZone.SelectedValue);//code added by pravin khot on 15/July/2016
            siteSettingTable.Add(DefaultSiteSetting.Currency.ToString(), ddlCurrency.SelectedValue);
            siteSettingTable.Add(DefaultSiteSetting.SMTP.ToString(), txtSMTP.Text);
            siteSettingTable.Add(DefaultSiteSetting.AdminEmail.ToString(), txtAdminEmail.Text);
            siteSettingTable.Add(DefaultSiteSetting.PaymentType.ToString(), ddlPaymentType.SelectedValue);
            siteSettingTable.Add(DefaultSiteSetting.SMTPUser.ToString(), txtSMTPUser.Text);
            siteSettingTable.Add(DefaultSiteSetting.CompanyName.ToString(), txtCompanyName.Text);
            if (txtSMTPwd.Text != "******")
                siteSettingTable.Add(DefaultSiteSetting.SMTPassword.ToString(), txtSMTPwd.Text);
            else
            {
                SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);

                if (siteSetting != null)
                {
                    Hashtable siteSettingTables = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                    if (siteSettingTables[DefaultSiteSetting.SMTPassword.ToString()] != null)
                    {
                        siteSettingTable.Add(DefaultSiteSetting.SMTPassword.ToString(), siteSettingTables[DefaultSiteSetting.SMTPassword.ToString()].ToString());
                    }
                }
            }
            siteSettingTable.Add(DefaultSiteSetting.SMTPort.ToString(), txtSMTPort.Text);
            siteSettingTable.Add(DefaultSiteSetting.SMTPEnableSSL.ToString(), chkSMTPSslEnable.Checked);

            siteSettingTable.Add(DefaultSiteSetting.TweeterUserName.ToString(), "");
            siteSettingTable.Add(DefaultSiteSetting.TweeterPassword.ToString(), ""); //txtTwitterPassword.Attributes["value"]);
            
            siteSettingTable.Add(DefaultSiteSetting.FinalHireCompletionTimeframe.ToString(), "");
            siteSettingTable.Add(DefaultSiteSetting.ApplicationEdition.ToString(), rdlApplicationEdtion.SelectedValue == "0" ? UIConstants.STAFFING_FIRM : (rdlApplicationEdtion.SelectedValue == "1" ? UIConstants.INTERNAL_HIRING : UIConstants.GOVERNMENT_PORTAL));
            siteSettingTable.Add(DefaultSiteSetting.CandidatePortalJobPublishing.ToString(), chkEnableCandidatePortalPublishing.Checked .ToString());
            siteSettingTable.Add(DefaultSiteSetting.ResumeParserDownloadLink.ToString(), txtResumeParserLink.Text);
            siteSettingTable.Add(DefaultSiteSetting.AutomaticEmailMatchinReqEditor .ToString(),chkAutomaticReqEditorEmail .Checked .ToString ());
            siteSettingTable.Add(DefaultSiteSetting.AllowCandidatetobeaddedonMultipleRequisition.ToString(), chkRequisition.Checked.ToString());//code added by pravin khot on 27/April/2016
            siteSettingTable.Add(DefaultSiteSetting.AllowVendorstoUpdateCandidateProfiles.ToString(), chkEnableupdatecandidatesinVendorPortal.Checked.ToString());//code added by pravin khot on 27/April/2016
            siteSettingTable.Add(DefaultSiteSetting.AllowShowPreciseSearchAllCandidates.ToString(), ChkShowPreciseSearchAllCandidates.Checked.ToString());//code added by pravin khot on 8/Aug/2016
            siteSettingTable.Add(DefaultSiteSetting.EmployeeReferralPortaPublishing .ToString(), chkEnableEmployeePortal.Checked.ToString());
            siteSettingTable.Add(DefaultSiteSetting.VendorPortaPublishing.ToString(), chkEnableVendorPortal.Checked.ToString());
            siteSettingTable.Add(DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString(), ddlmodule.SelectedIndex == 0 ? UIConstants.AccountType_Department : (ddlmodule.SelectedIndex == 1 ? UIConstants.AccountType_Client : UIConstants.AccountType_Vendor));
            return ObjectEncrypter.Encrypt<Hashtable>(siteSettingTable);
        }

        private void PopulateSiteSetting()
        {
            SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            if (siteSetting != null)
            {
                Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                ddlCountry.SelectedValue = siteSettingTable[DefaultSiteSetting.Country.ToString()].ToString();
                //added by pravin khot on 15/July/2016
                if (siteSettingTable[DefaultSiteSetting.TimeZone.ToString()].ToString() == "0" || siteSettingTable[DefaultSiteSetting.TimeZone.ToString()] == null || siteSettingTable[DefaultSiteSetting.TimeZone.ToString()] == "")
                {
                    ddlTimeZone.SelectedValue = "2";
                }
                else
                {
                    ddlTimeZone.SelectedValue = siteSettingTable[DefaultSiteSetting.TimeZone.ToString()].ToString();
                }
                //***********END**************************
                ddlCurrency.SelectedValue = siteSettingTable[DefaultSiteSetting.Currency.ToString()].ToString();
                txtSMTP.Text = siteSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();
                txtAdminEmail.Text = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                if (siteSettingTable[DefaultSiteSetting.PaymentType.ToString()] != null)
                    ddlPaymentType.Text = siteSettingTable[DefaultSiteSetting.PaymentType.ToString()].ToString();

                if (siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()] != null)
                    txtSMTPUser.Text = siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                if (siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()] != null || siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()] != string.Empty)
                {
                    if (txtSMTPwd.Text != "******")
                    {
                        txtSMTPwd.Text = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                        if (txtSMTPwd.Text != string.Empty)
                            txtSMTPwd.Text = "******";
                    }
                }
                if (siteSettingTable[DefaultSiteSetting.SMTPort.ToString()] != null)
                    txtSMTPort.Text = siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString();
                txtSMTPwd.Attributes.Add("Value", txtSMTPwd.Text);
                if (siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()] != null)
                    chkSMTPSslEnable.Checked = Convert.ToBoolean(siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString());
                else
                    chkSMTPSslEnable.Checked = false;
                //if (siteSettingTable[DefaultSiteSetting.TweeterUserName.ToString()] != null)
                //{
                //    //txtTwitterUserName.Text = siteSettingTable[DefaultSiteSetting.TweeterUserName.ToString()].ToString();
                //}

                //if (siteSettingTable[DefaultSiteSetting.TweeterPassword.ToString()] != null)
                //{
                //    //txtTwitterPassword.Attributes["value"] = siteSettingTable[DefaultSiteSetting.TweeterPassword.ToString()].ToString();
                //}

                //if (siteSettingTable[DefaultSiteSetting.FinalHireCompletionTimeframe.ToString()] != null)
                //{
                //    rdlFinalHireCompletionTimeFrame.SelectedValue = siteSettingTable[DefaultSiteSetting.FinalHireCompletionTimeframe.ToString()].ToString();
                //}

                if (siteSettingTable[DefaultSiteSetting.ApplicationEdition.ToString()] != null)
                {
                    rdlApplicationEdtion.SelectedValue = siteSettingTable[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.STAFFING_FIRM ? "0" : (siteSettingTable[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING ? "1" : "2");
                }

                try
                {
                    if (siteSettingTable[DefaultSiteSetting.CompanyName.ToString()] != null)
                    {
                        txtCompanyName.Text = siteSettingTable[DefaultSiteSetting.CompanyName.ToString()].ToString();
                    }
                    if (siteSettingTable[DefaultSiteSetting.CandidatePortalJobPublishing.ToString()] != null)
                    {
                        chkEnableCandidatePortalPublishing.Checked = Convert.ToBoolean(siteSettingTable[DefaultSiteSetting.CandidatePortalJobPublishing.ToString()].ToString());
                    }

                    if (siteSettingTable[DefaultSiteSetting.ResumeParserDownloadLink .ToString()] != null)
                    {
                        txtResumeParserLink.Text = siteSettingTable[DefaultSiteSetting.ResumeParserDownloadLink.ToString()].ToString();
                    }

                    if (siteSettingTable[DefaultSiteSetting.AutomaticEmailMatchinReqEditor .ToString()] != null)
                    {
                       chkAutomaticReqEditorEmail.Checked = Convert.ToBoolean(siteSettingTable[DefaultSiteSetting.AutomaticEmailMatchinReqEditor .ToString()].ToString());
                    }

                    //**************Code added by pravin khot on 27/April/2016*******************
                    if (siteSettingTable[DefaultSiteSetting.AllowCandidatetobeaddedonMultipleRequisition.ToString()] != null)
                    {
                        chkRequisition.Checked = Convert.ToBoolean(siteSettingTable[DefaultSiteSetting.AllowCandidatetobeaddedonMultipleRequisition.ToString()].ToString());
                    }
                    if (siteSettingTable[DefaultSiteSetting.AllowVendorstoUpdateCandidateProfiles.ToString()] != null)
                    {
                        chkEnableupdatecandidatesinVendorPortal.Checked = Convert.ToBoolean(siteSettingTable[DefaultSiteSetting.AllowVendorstoUpdateCandidateProfiles.ToString()].ToString());
                    }
                    if (siteSettingTable[DefaultSiteSetting.AllowShowPreciseSearchAllCandidates.ToString()] != null)
                    {
                        ChkShowPreciseSearchAllCandidates.Checked = Convert.ToBoolean(siteSettingTable[DefaultSiteSetting.AllowShowPreciseSearchAllCandidates.ToString()].ToString());
                    }
                    //**********************************END*************************************
                    if (siteSettingTable[DefaultSiteSetting.EmployeeReferralPortaPublishing .ToString()] != null)
                    {
                        chkEnableEmployeePortal.Checked = Convert.ToBoolean(siteSettingTable[DefaultSiteSetting.EmployeeReferralPortaPublishing .ToString()].ToString());
                    }
                    if (siteSettingTable[DefaultSiteSetting.VendorPortaPublishing.ToString()] != null)
                    {
                        chkEnableVendorPortal.Checked = Convert.ToBoolean(siteSettingTable[DefaultSiteSetting.VendorPortaPublishing.ToString()].ToString());
                    }

                    if (siteSettingTable[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()] != null)
                    {
                        if (siteSettingTable[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                        {
                            ddlmodule.SelectedIndex = 1;
                        }
                        else if (siteSettingTable[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                        {
                            ddlmodule.SelectedIndex = 0;
                        }
                        else
                        {
                            ddlmodule.SelectedIndex = 2;
                        }
                    }

                }
                catch
                {
                }
            }
            else
            {
                ddlCountry.SelectedValue = ddlCountry.Items.FindByText("United States").Value;
                ddlCurrency.SelectedValue = ddlCurrency.Items.FindByText("$ USD").Value;
            }
            
        }
        

        #endregion       
    }
}
