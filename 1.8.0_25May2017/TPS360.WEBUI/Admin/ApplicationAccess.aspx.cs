﻿using System;
using System.Web.UI.WebControls;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.Web.UI.Admin
{
    public partial class ApplicationAccess : AdminBasePage
    {
        #region Member Variables

        #endregion

        #region Methods

        private void PrepareView()
        {
            int memberId = 0;
            if (ddlMember.Items.Count > 0)
            {
                int.TryParse(ddlMember.SelectedItem.Value, out memberId);
            }

            ctlMenuSelector.CurrentSiteMapTypes = new SiteMapType[]{ SiteMapType.ApplicationTopMenu,
                                                                    SiteMapType.EmployeeOverviewMenu,
                                                                    SiteMapType.CandidateOverviewMenu,
                                                                    SiteMapType.CompanyOverviewMenu,
                                                                    SiteMapType.EmployeePortalMenu, 
                                                                    SiteMapType.CandidateCareerPortalMenu, 
                                                                      SiteMapType .DepartmentOverviewMenu ,
                                                                    SiteMapType .VendorProfileMenu,
                                                                    SiteMapType .VendorPortalTopMenu ,
                                                                    SiteMapType .CandidateProfileMenuForVendor 
                                                                    };

            if (memberId > 0)
            {
                ctlMenuSelector.SelectList = Facade.GetAllMemberPrivilegeIdsByMemberId(memberId);
                btnSave.Visible = true;
            }
            else
            {
                ctlMenuSelector.SelectList = null;
                btnSave.Visible = false;
            }

            ctlMenuSelector.BindList();
        }

        private void SaveMenuAccess()
        {
            int memberId;
            int.TryParse(ddlMember.SelectedItem.Value, out memberId);
            
            TreeNodeCollection checkedNodes = ctlMenuSelector.SiteMapTree.CheckedNodes;

            Facade.DeleteMemberPrivilegeByMemberId(memberId);

            foreach (TreeNode node in checkedNodes)
            {
                MemberPrivilege memberPrivilege = new MemberPrivilege();

                memberPrivilege.CustomSiteMapId = Convert.ToInt32(node.Value);
                memberPrivilege.MemberId = memberId;
                memberPrivilege.UpdatorId = CurrentMember.Id;
                Facade.AddMemberPrivilege(memberPrivilege);
            }

            MiscUtil.ShowMessage(lblMessage, "Permission assigned successfully.", false);
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MiscUtil.PopulateCustomRole(ddlRole, Facade);
                PrepareView();
                btnSave.Visible = false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveMenuAccess();
        }

        protected void ddlMember_SelectedIndexChanged(object sender, EventArgs e)
        {
            PrepareView();
            pnlDynamic.Update();
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            MiscUtil.PopulateMemberByRole(ddlMember, Convert.ToInt32(ddlRole.SelectedValue), Facade);
            PrepareView();
            pnlDynamic.Update();
        }

        #endregion
    }
}