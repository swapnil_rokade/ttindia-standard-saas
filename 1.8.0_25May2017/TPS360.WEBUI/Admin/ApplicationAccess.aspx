﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeFile="ApplicationAccess.aspx.cs" Inherits="TPS360.Web.UI.Admin.ApplicationAccess"
    Title="Application Access" %>

<%@ Register Src="../Controls/MenuSelector.ascx" TagName="MenuSelector" TagPrefix="uc1" %>
<asp:Content ID="cntApplicationAccessEditorHeader" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="cntApplicationAccessEditorTitle" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    User Access Permissions
</asp:Content>
<asp:Content ID="cntApplicationAccessEditor" ContentPlaceHolderID="cphHomeMaster"
    runat="Server">

    <div style="width: 100%;">
        <div>
            <asp:UpdatePanel ID="pnlDynamic" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="TableRow" style="text-align: center">
                        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label EnableViewState="false" ID="lblRole" runat="server" Text="Role"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlRole" CssClass="CommonDropDownList" runat="server" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" Width="200" />
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblMember" runat="server" Text="Member"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlMember" CssClass="CommonDropDownList" runat="server" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlMember_SelectedIndexChanged" Width="200" />
                        </div>
                    </div>
                    <%--<asp:CascadingDropDown ID="cddRole" runat="server" TargetControlID="ddlRole" Category="Role"
                        PromptText="Please select a role" LoadingText="Loading roles..." ServiceMethod="PopulateRoles"
                        ServicePath="~/DataService.asmx" />
                    <asp:CascadingDropDown ID="cddMember" runat="server" TargetControlID="ddlMember"
                        Category="Member" PromptText="Please select a member" LoadingText="Loading members..."
                        ServiceMethod="PopulateMembers" ParentControlID="ddlRole" ServicePath="~/DataService.asmx" />--%>
                    <asp:Panel ID="pnlSaveMenuAccess" runat="server">
                        <div class="TableRow" style="text-align: center">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblMenuAccess" runat="server" Text="Application Access"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <uc1:MenuSelector ID="ctlMenuSelector" runat="server" />
                            </div>
                        </div>
                        <br />
                        <div class="TableRow" style="text-align: center">
                            <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Save" OnClick="btnSave_Click" UseSubmitBehavior ="true"  />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
