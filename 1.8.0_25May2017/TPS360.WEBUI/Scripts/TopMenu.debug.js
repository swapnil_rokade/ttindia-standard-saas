var gblNumOfItems = GBL_NUMBER_OF_ITEMS;
// strGlobalNavItemOn stores off ID item that should be highlighted/colored when it's menu is visible.
var strGlobalNavItemOn = '';	

//-----------------------

//menu constructor
function menu(thisitem, childitem, startvisibility)
{ 
	callname = thisitem;
	divname = childitem;  
	this.caller = document.getElementById(callname);
	this.thediv = document.getElementById(divname);
	this.thediv.style.visibility = startvisibility;
}// End menu constructor

//-----------------------

//menu methods
function MenuEventHandler(event,theobj)
{
	for (var i = 1; i <= gblNumOfItems; i++) 
	{ // turn off all divs
		var shutdiv = eval( "objMenuItem" + i + ".thediv");
		if (shutdiv != null) 
		{
    		shutdiv.style.visibility = "hidden";
	    	HighlightGlobalNav('off');	
	    }
	}
	
	if (theobj != null) 
	{
		// Then turn on the object specified in the parameter, storing the id.
		theobj.thediv.style.visibility = "visible";
		strGlobalNavItemOn = theobj.caller.id;	// globally store id of the main mouse over item
		HighlightGlobalNav('on');				// then toggle that item to visible 'on' state
	}
} // end MenuEventHandler()

//------------

function mouseX(evt) 
{
    if (evt.pageX)
    {
        return evt.pageX;
    }
    else if (evt.clientX)
    {
        return evt.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
    }
    else
    {
        return null;
    }
}

function mouseY(evt) 
{
    if (evt.pageY)
    {
        return evt.pageY;
    }
    else if (evt.clientY)
    {
       return evt.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
    }
    else
    {
        return 0;
    }
}

function CloseSubNav(event)
{
	if(gblNumOfItems > 0)
	{
	    // If mouse isn't in proper vertical range close all sub menu items.
	    if ((mouseY(event) < 120) || (mouseY(event) > 250))
        {
	        for (var i = 1; i <= gblNumOfItems; i++)
	        {
		        var shutdiv = eval('objMenuItem' + i + '.thediv');
		        if (shutdiv != null) 
		        {
		            shutdiv.style.visibility = 'hidden';
                }
	        }
	        // Then turn back on the subnav for this page
	        turnOnCurrentPageGlobalNav(); 
        }
    }
} // end CloseSubNav()

//------------

// HighlightGlobalNav(state) looks at the incoming state and changes the color of the 
// element that is on (name stored in the  global variable strGlobalNavItemOn).
function HighlightGlobalNav(state)
{
	if (strGlobalNavItemOn != '' && !(strGlobalNavItemOn < 0) )
	{
		theNavItem = document.getElementById(strGlobalNavItemOn);
		if (state == 'on') 
		{
//			theNavItem.style.color="#D3EAFF"; 
//			theNavItem.style.background-color="transparent"; 
//			theNavItem.style.background-image="none"; 
		} 
		else 
		{
//			theNavItem.style.color="#FFFFFF";
//			theNavItem.style.background-color="transparent"; 			
//			theNavItem.style.background-image="none";
		}
	}
} // end HighlightGlobalNav ()


function turnOnCurrentPageGlobalNav()
{
	var objMenuItemOn = eval(currentPageGlobalMenu);
	MenuEventHandler('', objMenuItemOn);	// Call handle to toggle on the selected menuObject
} // end turnOnCurrentPageGlobalNav()

//-----------------------