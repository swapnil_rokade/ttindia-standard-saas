/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Common.js
    Description: Common javascript functions
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Oct-16-2008           Anand Dixit         Defect ID: 9002; Added ConfirmAction Method
    0.2             5/May/2016           pravin khot         New function added - ConfirmDeleteSchedule
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
function ShowYearlyCurrency(id,lblid)
{
var e = document.getElementById(id);
if(e.options[e.selectedIndex].text=="INR")
{
document .getElementById (lblid ).style.visibility="visible";
document .getElementById (lblid ).innerHTML="(Lacs)";
}
else 
{
document .getElementById (lblid ).style.visibility="hidden";
}
}
function IsClientValidationEnabled()
{
if(typeof(Page_ClientValidate)=='function')
{
return true;}
else
{
return false;}}
function EnableValidator(validator,enable)
{
if(IsClientValidationEnabled())
{
ValidatorEnable(validator,enable);}}
function ValidateValidator(validator)
{
if(IsClientValidationEnabled())
{
ValidatorValidate(validator);}}
function UpdateValidatorDisplay(validator)
{
if(IsClientValidationEnabled())
{
ValidatorUpdateDisplay(validator);}}
function onUpdating(e)
{

var pnlPopup=$get('<%= this.p.ClientID %>');
var listView=$get('<%= this.l.ClientID %>');
pnlPopup.style.display='';
var listViewBounds=Sys.UI.DomElement.getBounds(listView);
var pnlPopupBounds=Sys.UI.DomElement.getBounds(pnlPopup);
var x=listViewBounds.x+Math.round(listViewBounds.width/2)-Math.round(pnlPopupBounds.width/2);
var y=listViewBounds.y+Math.round(listViewBounds.height/2)-Math.round(pnlPopupBounds.height/2);
Sys.UI.DomElement.setLocation(pnlPopup,x,y);}
function onUpdated(){
var pnlPopup=$get('<%= this.e.ClientID %>');
pnlPopup.style.display='none';}
function ChangeLanguage()
{
var request=new Sys.Net.WebRequest();
request.set_httpVerb("GET");
request.set_url('../ChangeLanguage.aspx');
request.add_completed(function(executor)
{
if(executor.get_responseAvailable())
{
var languageDiv=$get('LanguageDiv');
var languageLink=$get('LanguageLink');
var languageLinkBounds=Sys.UI.DomElement.getBounds(languageLink);
languageDiv.style.top=(languageLinkBounds.y+languageLinkBounds.height)+"px";
var content=executor.get_responseData();
languageDiv.innerHTML=content;
languageDiv.style.display="block";}});
var executor=new Sys.Net.XMLHttpExecutor();
request.set_executor(executor);
executor.executeRequest();}
function showScheduler()
{
var request=new Sys.Net.WebRequest();
request.set_httpVerb("GET");
request.set_url('../Scheduler.aspx');
request.add_completed(function(executor)
{
if(executor.get_responseAvailable())
{
var schedulerDiv=$get('SchedulerDiv');
var schedulerLink=$get('SchedulerLink');
var schedulerLinkBounds=Sys.UI.DomElement.getBounds(schedulerLink);
schedulerDiv.style.top=(schedulerLinkBounds.y+schedulerLinkBounds.height)+"px";
var content=executor.get_responseData();
schedulerDiv.innerHTML=content;
schedulerDiv.style.display="block";}});
var executor=new Sys.Net.XMLHttpExecutor();
request.set_executor(executor);
executor.executeRequest();}
function ConfirmDelete(moduleName)
{

   if (confirm("Are you sure that you want to delete this "+moduleName+"?\t")){ 
            return true; 
        } 
        else{ 
                    if (window.event) { //will be true with IE, false with other browsers 
                    window.event.returnValue=false; } //IE specific, seems to work 
                else { 
                    return false; }  
            }




        }
         //*****************Code added by pravin khot on 5/May/2016***********
        function ConfirmDeleteSchedule(moduleName) {

            if (confirm("Are you sure that you want to cancel this " + moduleName + "?\t")) {
                return true;
            }
            else {
                if (window.event) { //will be true with IE, false with other browsers 
                    window.event.returnValue = false;
                } //IE specific, seems to work 
                else {
                    return false;
                }
            }
        }
        //*********************END***********************************
function ConfirmAction(strMessage)
{
    if( confirm(strMessage))
        return true ;
        else{
        if (window.event) { //will be true with IE, false with other browsers 
                    window.event.returnValue=false; } //IE specific, seems to work 
                else { 
                    return false; }  
        }
}

function CheckUnCheckAllRowLevelCheckboxes(cntrlHeaderCheckbox,cntrlGridView,posCheckbox)
{
if(cntrlHeaderCheckbox!=null&&cntrlHeaderCheckbox.checked!=null)
{
var rowLength=cntrlGridView.rows.length;
for(var i=1;i<rowLength;i++)
{
var myrow=cntrlGridView.rows[i];
var mycel=myrow.getElementsByTagName("td")[posCheckbox];
var chkBox=mycel.childNodes[0];
if(chkBox!=null)
{
if(chkBox.checked==null)
{
chkBox=chkBox.childNodes[1];}
if(chkBox!=null)
{
if(chkBox.checked!=null)
{
chkBox.checked=cntrlHeaderCheckbox.checked;}}}}}}
function CheckUnCheckGridViewHeaderCheckbox(dgrName,chkboxName,posRowChkbox,p)
{

var chkBoxAll=document.getElementById(chkboxName);
var tbl=document.getElementById(dgrName).getElementsByTagName("tbody")[0];
var checked=0;
var rowNum=0;
var rowLength=tbl.rows.length;
var chkBox="";
var hndID="";
for(var i=1;i<rowLength;i++)
{
var myrow=tbl.rows[i];
var mycel=myrow.getElementsByTagName("td")[posRowChkbox];

if (mycel.childNodes[0].value=="on")
{
chkBox=mycel.childNodes[0];
}
else 
{
chkBox=mycel.childNodes[1];
}
try
{
if (mycel.childNodes[2].value==undefined)
{

hndID=mycel.childNodes[3];
}
else 
{
hndID=mycel.childNodes[2];
}
}
catch(e ) 
{
}

if(hndID!=null )
{
if(hndID.value==null)
{
try{
hndID=hndID.childNodes[3];

}
catch (e)
{
}
}
else
{
   // alert (hndID.value + "");
}
}

if(chkBox!=null)
{

if(chkBox.checked==null)
{

chkBox=chkBox.childNodes[1];}
if(chkBox!=null)
{

if(chkBox.checked!=null)
{
rowNum++;}

if(chkBox.checked!=null&&chkBox.checked==true)
{if(hndID != null)ADDID(hndID.value,'ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS');

checked++;}
else{if(hndID != null)RemoveID(hndID.value,'ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS');
}

}}}

if(checked==rowNum)
{
chkBoxAll.checked=true;}
else
{
chkBoxAll.checked=false;}}

function RemoveID(ID,hndCtrl)
{

var hdnSelectedIDS=document.getElementById (hndCtrl.trim()); //"ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS");

var arrID=hdnSelectedIDS.value.split(',');
var IDS="";
for(var i=0;i<arrID .length;i++)
{
    if(arrID [i]!=ID)
    {
       if(IDS !="")IDS +=",";
        IDS +=arrID [i];
    }
    hdnSelectedIDS .value=IDS;
    
}

}

function ADDID(ID,hndCtrl)
{

var idAvailable=false ;
var hdnSelectedIDS=document .getElementById (hndCtrl.trim());// "ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS");
var arrID=hdnSelectedIDS.value.split(',');
for(var i=0;i<arrID .length;i++)
{

    if(arrID [i]==ID)
    {
       idAvailable= true ;
        break ;
    }
    
}
if(!idAvailable )
{
       if(hdnSelectedIDS .value!="" && ID !="")hdnSelectedIDS .value +=",";
     hdnSelectedIDS .value= hdnSelectedIDS .value  + ID;
}

}
// Defect 8924 
function PrintHtml(cntrlName) 
{
var sWinHTML = document.getElementById(cntrlName).innerHTML; 
var winprint=window.open("","",'left=0,top=0,width=1,height=1,toolbar=0,scrollbars=0,status=0'); 
winprint.document.open(); 
winprint.document.write('<html><body onLoad="javascript:window.print();">'); 
winprint.document.write(sWinHTML); 
winprint.document.write('</body></html>'); 
winprint.document.close(); 
winprint.focus();
}
// Defect 8924  
function PrintData(cntrlName)
{
var divCandidateDetail=document.getElementById(cntrlName);
var WinPrint=window.open('','','letf=0,top=0,width=1,height=1,toolbar=0,scrollbars=0,status=0');
WinPrint.document.write(divCandidateDetail.innerHTML);
WinPrint.document.close();
WinPrint.focus();
WinPrint.print();
WinPrint.close();}
var $Ajax=
{
post:function(url,formFields,onSuccess,onFailure)
{
var request=new Sys.Net.WebRequest();
var requestBody=new Sys.StringBuilder();
if(formFields)
{
if(formFields.length>0)
{
for(var i=0;i<formFields.length;i++)
{
if(i>0)
{
requestBody.append('&');}
requestBody.append(String.format('{0}={1}',formFields[i][0],formFields[i][1]));}}}
request.set_url(url);
request.set_body(requestBody.toString());
request.set_httpVerb('post');
request.add_completed(onComplete);
request.invoke();
function onComplete(response,eventArgs)
{
if(response.get_responseAvailable())
{
var statusCode=response.get_statusCode();
var result=null;
try
{
var contentType=response.getResponseHeader('Content-Type');
if(contentType.startsWith('application/json'))
{
result=response.get_object();}
else if(contentType.startsWith('text/xml'))
{
result=response.get_xml();}
else
{
result=response.get_responseData();}}
catch(e)
{}
if((statusCode<200)||(statusCode>=300))
{
if(onFailure)
{
if(!result)
{
result=new Sys.Net.WebServiceError(false,'An unexpected error has occurred while processing your request.','','');}
result._statusCode=statusCode;
onFailure(result);}}
else if(onSuccess)
{
onSuccess(result,'','');}}
else
{
var msg;
if(response.get_timedOut())
{
msg='Request timed out';}
else
{
msg='An unexpected error has occurred while processing your request';}
if(onFailure)
{
onFailure(new Sys.Net.WebServiceError(response.get_timedOut(),msg,'',''));}}}}}
function renderDay(owner,day,evt)
{
if(customDays==null)return;
var d,back="",url="";
var i=customDays.length;
while(i-->0)
{
d=customDays[i];
if(d==null||d.length<5)continue;
if((d[0]==-1||d[0]==day.year)&&(d[1]<0||d[1]==day.month)&&d[2]==day.day)
{
if(!day.selected)back=d[3];
url=d[4];
if(url.length>0)url="url(\""+url+"\")";
break;}}
day.element.style.backgroundColor=back;
day.element.style.backgroundImage=url;
day.element.style.backgroundRepeat="no-repeat";}
function dayChange(owner,date,evt)
{
if(customDays==null||date==null)return;
var year=date.getFullYear(),month=date.getMonth()+1,day=date.getDate();
var back="",url="";
var bell=false;
var elem=ig_csom.getElementById("NoBell");
if(elem!=null)bell=elem.checked;
var i=customDays.length;
while(i-->0)
{
var d=customDays[i];
if(d==null||d.length<5)continue;
if((d[0]==-1||d[0]==year)&&(d[1]<0||d[1]==month)&&d[2]==day)
{
back=d[3];
url=d[4];
if(bell&&d[4]=="./images/bell.gif")
evt.cancel=true;}}
elem=ig_csom.getElementById("ShowSelected");
if(elem==null){alert("no ShowSelected");return;}
if(back.length>0||url.length>0)
back="Color:&nbsp;\""+back+"\"<br />Image:&nbsp;\""+url+"\"";
if(evt.cancel)back="<b>CANCEL SELECTION</b><br /><br />"+back;
elem.innerHTML=back;}
var $U=
{
display:function(e)
{
$U.fix(e).style.display='';},
displayBlock:function(e)
{
$U.fix(e).style.display='block';},
noDisplay:function(e)
{
$U.fix(e).style.display='none';},
show:function(e)
{
$U.fix(e).style.visibility='visible';},
hide:function(e)
{
$U.fix(e).style.visibility='hidden';},
setText:function(e,t)
{
e=$U.fix(e);
if(document.all)
{
e.innerText=t;}
else
{
e.textContent=t;}},
focus:function(e)
{
e=$U.fix(e);
try
{
e.select();}
catch(e)
{}
try
{
e.focus();}
catch(e)
{}},
setLocation:function(e,x,y)
{
e=$U.fix(e);
Sys.UI.DomElement.setLocation(e,x,y);},
resetInputs:function()
{
if(arguments.length>0)
{
var container;
var controls;
var control;
for(var i=0;i<arguments.length;i++)
{
container=$U.fix(arguments[i]);
controls=container.getElementsByTagName('input');
if(controls.length>0)
{
for(var j=0;j<controls.length;j++)
{
control=controls[j];
if((control.type=='text')||(control.type=='file')||(control.type=='password'))
{
control.value='';}
else if((control.type=='checkbox')||(control.type=='radio'))
{
control.checked=false;}}}}}},
displayChidren:function(e,t,c)
{
$U.displayOrNoDisplayChidren(true,e,t,c);},
noDisplayChidren:function(e,t,c)
{
$U.displayOrNoDisplayChidren(false,e,t,c);},
displayOrNoDisplayChidren:function(display,e,t,c)
{
e=$U.fix(e);
var elms=e.getElementsByTagName(t);
if(elms.length>0)
{
var e;
for(var i=0;i<elms.length;i++)
{
e=elms[i];
if(c)
{
if(Sys.UI.DomElement.containsCssClass(e,c))
{
if(display)
{
$U.display(e);}
else
{
$U.noDisplay(e);}}}
else
{
if(display)
{
$U.display(e);}
else
{
$U.noDisplay(e);}}}}},
fix:function(e)
{
if(typeof e!='object')
{
e=$get(e);}
return e;},
isEmptyString:function(s)
{
if(typeof s!='string')
{
return true;}
return((s==null)||(s.trim().length<1));},
showModalDialog:function(divBackground,divModal)
{
divModal=$U.fix(divModal);
divBackground=$U.fix(divBackground);
$U.display(divModal);
$U.display(divBackground);
var viewPortWidth=$U.getViewPortWidth();
var viewPortHeight=$U.getViewPortHeight();
var contentHeight=$U.getContentHeight();
divBackground.style.width=viewPortWidth+'px';
divBackground.style.height=Math.max(viewPortHeight,contentHeight)+'px';
$U.setLocation(divBackground,0,0);
$U.centerIt(divModal);},
centerIt:function(e)
{
e=$U.fix(e);
var x=(($U.getViewPortWidth()-e.offsetWidth)/2);
var y=(($U.getViewPortHeight()-e.offsetHeight)/2)+$U.getViewPortScrollY();
$U.setLocation(e,x,y);},
getViewPortWidth:function()
{
var width=0;
if((document.documentElement)&&(document.documentElement.clientWidth))
{
width=document.documentElement.clientWidth;}
else if((document.body)&&(document.body.clientWidth))
{
width=document.body.clientWidth;}
else if(window.innerWidth)
{
width=window.innerWidth;}
return width;},
getViewPortHeight:function()
{
var height=0;
if(window.innerHeight)
{
height=window.innerHeight-18;}
else if((document.documentElement)&&(document.documentElement.clientHeight))
{
height=document.documentElement.clientHeight;}
return height;},
getContentHeight:function()
{
if((document.body)&&(document.body.offsetHeight))
{
return document.body.offsetHeight;}
return 0;},
getViewPortScrollX:function()
{
var scrollX=0;
if((document.documentElement)&&(document.documentElement.scrollLeft))
{
scrollX=document.documentElement.scrollLeft;}
else if((document.body)&&(document.body.scrollLeft))
{
scrollX=document.body.scrollLeft;}
else if(window.pageXOffset)
{
scrollX=window.pageXOffset;}
else if(window.scrollX)
{
scrollX=window.scrollX;}
return scrollX;},
getViewPortScrollY:function()
{
var scrollY=0;
if((document.documentElement)&&(document.documentElement.scrollTop))
{
scrollY=document.documentElement.scrollTop;}
else if((document.body)&&(document.body.scrollTop))
{
scrollY=document.body.scrollTop;}
else if(window.pageYOffset)
{
scrollY=window.pageYOffset;}
else if(window.scrollY)
{
scrollY=window.scrollY;}
return scrollY;},
parseQueryString:function(url)
{
url=new String(url);
var queryStringValues=new Object();
var querystring=url.substring((url.indexOf('?')+1),url.length);
var querystringSplit=querystring.split('&');
for(var i=0;i<querystringSplit.length;i++)
{
var pair=querystringSplit[i].split('=');
var name=pair[0];
var value=pair[1];
queryStringValues[name]=value;}
return queryStringValues;}}
function hideMeClick(ctlId)
{
 document.getElementById(ctlId).style.display = 'none';
}






   function ManageSelectedID(dgrName)
    {
   
var tbl=document.getElementById(dgrName).getElementsByTagName("tbody")[0];
var checked=0;
var rowNum=0;
var chkBox="";
var hndID="";
var rowLength=tbl.rows.length;
for(var i=1;i<rowLength;i++)
{
var myrow=tbl.rows[i];
var mycel=myrow.getElementsByTagName("td")[0];
//var chkBox=mycel.childNodes[0];
if (mycel.childNodes[0].value=="on")
{
chkBox=mycel.childNodes[0];
}
else 
{
chkBox=mycel.childNodes[1];
}
//var hndID=mycel.childNodes[2];
try
{
if (mycel.childNodes[2].value==undefined)
{

hndID=mycel.childNodes[3];
}
else 
{
hndID=mycel.childNodes[2];
}
}
catch(e ) 
{
}
if(hndID!=null )
{
if(hndID.value==null)
{
hndID=hndID.childNodes[3]; //alert (hdnID.value);
}
else
{
   // alert (hndID.value + "");
}
}

if(chkBox!=null)
{
if(chkBox.checked==null)
{
chkBox=chkBox.childNodes[1];}
if(chkBox!=null)
{
if(chkBox.checked!=null)
{
rowNum++;}
if(chkBox.checked!=null&&chkBox.checked==true)
{if(hndID != null)ADDID(hndID.value,'ctl00_cphHomeMaster_ucntrlManageHotList_uwtCandidateInternalNotesActivities__ctl0_hdnSelectedIDS');

checked++;}
else{if(hndID != null)RemoveID(hndID.value,'ctl00_cphHomeMaster_ucntrlManageHotList_uwtCandidateInternalNotesActivities__ctl0_hdnSelectedIDS');
}

}}}
}



 function ScrollTop()
      {
      $('html, body').animate({
						scrollTop: 0
					}, 400);
      }
  function IsNumeric(input) {
        return (input - 0) == input && input.length > 0;
    }
    
    
    
function allownumeric(e,ctrl)
   {
    var key = window.event ? e.keyCode : e.which;
    var keychar = String.fromCharCode(key);
    
    
    var reg = new RegExp("[0-9.\b\n]")

    if (key == 8)
    {
     keychar = String.fromCharCode(key);
    }

    if (key == 13 || key ==8 ||  key==0 )
    {
     key=8;
     keychar = String.fromCharCode(key);     
    }
    return reg.test(keychar) && !($(ctrl).val().indexOf('.')>=0 && keychar =='.');
   } 
   
   
       
function allowInteger(e,ctrl)
   {
    var key = window.event ? e.keyCode : e.which;
    var keychar = String.fromCharCode(key);
    
    
    var reg = new RegExp("[0-9\b\n]")

    if (key == 8)
    {
     keychar = String.fromCharCode(key);
    }

    if (key == 13 || key ==8 ||  key==0 )
    {
     key=8;
     keychar = String.fromCharCode(key);     
    }
    return reg.test(keychar) && keychar !='.';
   } 
   
   
     Sys.Application.add_load(function() {
   
   $('input[rel=Numeric]').keypress(function (event){return allownumeric(event,$(this));});
      $('input[rel=Integer]').keypress(function (event){return allowInteger(event,$(this));});
      });