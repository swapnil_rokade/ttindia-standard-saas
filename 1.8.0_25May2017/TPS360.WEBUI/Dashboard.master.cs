﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:Dashboard.master.cs
    Description:This is the master page for displaying Links to "My Account","Logout","Home"
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Jan-13-2009          Gopala Swamy          Enhancement  id:8675; Made visibility for "My Account" and provided Navigation Path to Basic Info Page
    0.2             feb-26-2009           Nagarathna            Defect Id:9739 Variable strWelcomePageName is declared as static.
                                                                             In the method Page_Load(), the logic to assisgn properties to hyperlink "Home" is placed outside the Postback.
-------------------------------------------------------------------------------------------------------------------------------------------       
*/



using System;
using System.Collections;
using System.Web.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using Infragistics.WebUI.Shared.Util;

namespace TPS360.Web.UI
{
    public partial class DashboardMaster : BaseMasterPage
    {
        #region Variables

        ArrayList _permittedMenuIdList;
        static string strWelcomePageName;

        #endregion

        #region Properties

        #endregion

        #region Methods

        private bool IsInPermitedMenuList(int menuId)
        {
            if (CurrentMember != null)
            {
                if (_permittedMenuIdList == null)
                {
                    _permittedMenuIdList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                }
            }

            if ((_permittedMenuIdList != null) && (_permittedMenuIdList.Count > 0))
            {
                if (_permittedMenuIdList.Contains(menuId))
                {
                    return true;
                }
            }

            return false;
        }

        private void InitializeInfragistics()
        {
            try
            {
                wsSqlCPAddToMyCalender.Connection = CreateConnection();
                this.wsSqlCPAddToMyCalender.WebScheduleInfo = this.wsInfoAddToMyCalender;
                this.wsInfoAddToMyCalender.ActiveResourceName = base.CurrentMember.Id.ToString();
                wsInfoAddToMyCalender.EnableReminders = false;
                wsInfoAddToMyCalender.AppointmentFormPath = System.Configuration.ConfigurationManager.AppSettings["Appointment"].ToString();
                wsInfoAddToMyCalender.ReminderFormPath = System.Configuration.ConfigurationManager.AppSettings["Reminder"].ToString();
            }
            catch
            {
            }
        }

        private IDbConnection CreateConnection()
        {
            return new System.Data.SqlClient.SqlConnection(MiscUtil.ConnectionString);
        }

        private void AttachClientScript()
        {
            SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();

            SiteMapNode root = SiteMap.Providers["SqlSiteMap"].FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.ApplicationTopMenu));

            if (root != null)
            {
                SiteMapNodeCollection nodeList = root.ChildNodes;

                if (nodeList != null && nodeList.Count > 0)
                {
                    foreach (SiteMapNode node in nodeList)
                    {
                        if (IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
                        {
                            permittedNodeList.Add(node);
                        }
                    }
                }

                string script = "\n" +
                                "<script language=\"javascript\" type=\"text/javascript\">  " + "\n" +
                                "/* <![CDATA[ */                                            " + "\n" +
                                "                                                           " + "\n" +
                                "	GBL_NUMBER_OF_ITEMS = " + permittedNodeList.Count + "            " + "\n" +
                                "														    " + "\n" +
                                "/* ]]> */												    " + "\n" +
                                "</script>";

                if (!Page.ClientScript.IsClientScriptBlockRegistered("calc"))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "calc", script);
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            AttachClientScript();
            base.OnPreRender(e);
        }

        private void BuildCompanyLogo(int memberId)
        {
            if (memberId <= 0)
            {
                return;
            }

            CompanyContact contact = Facade.GetCompanyContactByMemberId(CurrentMember.Id);

            if (contact != null && contact.CompanyId > 0)
            {
                Company company = Facade.GetCompanyById(contact.CompanyId);

                if (!string.IsNullOrEmpty(company.CompanyLogo))
                {
                    string filePath = Path.Combine(UrlConstants.GetPhysicalCompanyUploadDirectory(), company.CompanyLogo);

                    if (File.Exists(filePath))
                    {
                        SecureUrl secureUrl = UrlHelper.BuildSecureUrl(UrlConstants.DRAW_MEDIA_PAGE, string.Empty, UrlConstants.PARAM_IMG_FILE, filePath, UIConstants.SHOW_THUMB, bool.TrueString);
                    }
                }
            }
        }

        private void LoggedinUserInfo()
        {
            if (CurrentMember != null)
            {
                if (CurrentMember.Id > 0)
                {
                    if (IsUserPartner || IsUserVendor || IsUserClient)
                    {
                        BuildCompanyLogo(CurrentMember.Id);
                    }
                }
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsUserVendor)
            //{
            //    try
            //    {
            //        if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
            //        {
            //            FormsAuthentication.SignOut();
            //            FormsAuthentication.RedirectToLoginPage();
            //            return;
            //        }
            //    }
            //    catch { }
            //}
            Response.Expires = -1;  
            SecureUrl todoUrl = UrlHelper.BuildSecureUrl(UrlConstants.Employee.EMPLOYEE_TASKS_SCHEDULE_WITH_MENU, "", UrlConstants.PARAM_MEMBER_ID, base.CurrentMember.Id.ToString());

            if (WebConfigurationManager.AppSettings["CopyRightsText"].ToString() != string.Empty)
            {
                lblCopyRights.Text = "Copyright © " + DateTime.Now.Year.ToString() + " " + WebConfigurationManager.AppSettings["CopyRightsText"].ToString() + "All rights reserved.";
            }
            if (WebConfigurationManager.AppSettings["VersionText"].ToString() != string.Empty)
            {
                lblVersionText.Text = WebConfigurationManager.AppSettings["VersionText"].ToString();
            }

           if (!IsPostBack)
            {

                if (!IsUserVendor && Facade.Employee_IsEmployeeFirstLogin(CurrentMember.Id))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "show", "<script>setFirstLoginPage();</script>");
                    iframe.Attributes.Add("src", "../Modals/WelcomePage.aspx");
                    mpeManagers.Show();
                }
                Facade.UpdateAspStateTempSessions(CurrentMember.PrimaryEmail, Session.SessionID, "TPS", GettingCommonValues.GetDomainName());
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.Dashboard.NEWMAIL, string.Empty,  UrlConstants.PARAM_PAGE_FROM, "Dashboard");
                //imgNewMail.Attributes.Add("Onclick", "btnMewMail_Click('" + url + "')");
                lnkNewEmail.Attributes.Add("Onclick", "btnMewMail_Click('" + url + "')");
                

                if (IsUserEmployee)
                {
                    strWelcomePageName = UrlConstants.Employee.HOME_PAGE;
                    //settingPipe.Visible = linkSettings.Visible = true;
                    linkSettings.HRef = UrlConstants.Employee.EMPLOYEE_CHANGEPASSWORD;

                    divHeaderBarRight.Visible = true;
                    //lnkTimesheet.HRef = UrlConstants.Employee.EMPLOYEE_TIMESHEETX;
                }
                else if (IsUserCandidate)
                {
                    strWelcomePageName = UrlConstants.Candidate.HOME_PAGE;
                    //settingPipe.Visible = linkSettings.Visible = true; 
                    linkSettings.HRef = UrlConstants.Employee.EMPLOYEE_BASICINFO; 
                }
                else if (IsUserAdmin)
                {
                    strWelcomePageName = UrlConstants.Employee.HOME_PAGE;
                    divHeaderBarRight.Visible = true;
                    //lnkTimesheet.HRef = UrlConstants.Employee.EMPLOYEE_TIMESHEETX;
                    linkSettings.HRef = UrlConstants.Employee.EMPLOYEE_CHANGEPASSWORD; 

                }
                else if (IsUserVendor)
                {
                    anchorNewAppointment.Visible = false;
                    A2.Visible = true;
                    lnkNewEmail.Visible = false;
                    lnkEmailHistory.Visible = false;
                    lnkDownloadParser.Visible = false;
                    linkSettings.Visible = false;

                    strWelcomePageName = UrlConstants.Vendor.HOME;
                    divHeaderBarRight.Visible = true;
                  lgsLogin.LogoutPageUrl = "~/Vendor/Login.aspx";
                  A1.Attributes.Add("href", UrlConstants.Vendor.HOME);
                }

                else if (IsUserDepartmentContact)
                {
                    strWelcomePageName = UrlConstants.Employee.HOME_PAGE;
                    divHeaderBarRight.Visible = true;
                    //lnkTimesheet.HRef = UrlConstants.Employee.EMPLOYEE_TIMESHEETX;
                    linkSettings.HRef = UrlConstants.Employee.EMPLOYEE_CHANGEPASSWORD;
                    lnkDownloadParser.Visible = lnkEmailHistory.Visible = lnkNewEmail.Visible = linkSettings.Visible = false;
                }
                //lnkTodo.HRef = todoUrl.ToString();
                LoggedinUserInfo();

                lnkDownloadParser.Visible = false;
                Hashtable siteSetting = Context.Items[ContextConstants.SITESETTING] as Hashtable;
                if (siteSetting != null)
                {
                    if (siteSetting[DefaultSiteSetting.ResumeParserDownloadLink.ToString()] != null && (siteSetting[DefaultSiteSetting.ResumeParserDownloadLink.ToString()].ToString().Trim() != string.Empty))
                    {
                        lnkDownloadParser.Attributes.Add("href", siteSetting[DefaultSiteSetting.ResumeParserDownloadLink.ToString()].ToString());
                        lnkDownloadParser.Visible = true;
                    }
                }
                if (IsUserVendor) lnkDownloadParser.Visible = false;
            }
            if (CurrentMember != null)
            {
                //string welcomeToolTip = "Dashboard: " + CurrentMember.FirstName + " " + CurrentMember.LastName;
                //string welcomeNote = "<img src='../Images/home.png' style='padding-right: 3px;' border='0' align='absmiddle' />Home";
                lblUserName.Text = CurrentMember.FirstName + " " + CurrentMember.LastName;  
                //lnkMemberWelcomeNote.NavigateUrl = strWelcomePageName;
                //lnkMemberWelcomeNote.Text = welcomeNote;
                //lnkMemberWelcomeNote.ToolTip = welcomeToolTip;
            }
            InitializeInfragistics();
        }

        protected void lgs_LogOut(object sender, EventArgs e)
        {
            string dmnName = GettingCommonValues.GetDomainName();
            Facade.DeleteUserAccessApp(base.CurrentUser.UserName,"TPS",dmnName);
            string role = (Roles.GetRolesForUser(base.CurrentUserName)).GetValue(0).ToString();
            //Create Login activity history
            DateTime dtLogoutTime = DateTime.Now;
            MiscUtil.AddActivity(role, base.CurrentMember.Id, base.CurrentMember.Id, ActivityType.Logout, Facade);

            MembershipUser membershipUser = Membership.GetUser(base.CurrentUser.UserName, false);
            if (membershipUser != null)
            {
                MemberDailyReport memberDailyReport = Facade.GetMemberDailyReportByLoginTimeAndMemberId(membershipUser.LastLoginDate, base.CurrentMember.Id);
                if (memberDailyReport != null)
                {
                    memberDailyReport.LogoutTime = dtLogoutTime;
                    Facade.UpdateMemberDailyReport(memberDailyReport);
                }
            }
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));

            Session.Abandon(); // Session Expire but cookie do exist
            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-30); //Delete the cookie
            Response.Expires = -1;
            Response.Clear();
            if (IsUserVendor) Response.Redirect("~/Vendor/Login.aspx");
            Response.Redirect("~/Login.aspx");
        }

        #endregion
    }
}