﻿<%@ Page Language="C#" MasterPageFile="~/CandidatePortal/Portal.master" AutoEventWireup="true"
    CodeFile="EmployeeReferral.aspx.cs" EnableEventValidation="false" Inherits="TPS360.Web.UI.EmployeeReferral"
    Title="Employee Referral Portal" %>

<%@ Register Src="~/Controls/RegistrationInternal.ascx" TagName="regI" TagPrefix="uc1" %>
<%--Candidate Status in Employee Referral Portal.--%>
<%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>  
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%--Candidate Status in Employee Referral Portal.--%>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
<%--Candidate Status in Employee Referral Portal.--%>
<asp:Label ID="lblTitle"  runat ="server" Text ="Employee Referral" ></asp:Label>
<%--Candidate Status in Employee Referral Portal.--%>
<%--<asp:Label ID="titleContainer"  runat ="server" Text ="Refer Candidate for job opening" ></asp:Label>
--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
<%--Candidate Status in Employee Referral Portal.--%>
<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
<script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
    <div class="tabbable"> <!-- Only required for left/right tabs -->
    <%--<div class="TabPanelHeader"></div>--%>
        <ul class="nav nav-tabs">
            <li><a href="#tab1" data-toggle="tab">Candidate Status</a></li>
            <li   class="active"><a href="#tab2" data-toggle="tab"><asp:Label ID="titleContainer"  runat ="server" Text ="Refer Candidate for job opening" ></asp:Label></a></li>
    </ul>
     <div class="tab-content" >
         <div class="tab-pane" id="tab1">
            
                        
                    

            <asp:UpdatePanel ID="pnlCandidateStatus" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
            <div style="padding: 18px;text-align: center;">
                <asp:Label ID="lblCandidateID"  runat ="server" Text ="Candidate ID" ></asp:Label>: 
                <asp:TextBox ID="txtCandidateID" runat="server" CssClass="ApplicantIDTextbox" rel="Integer" autocomplete="off" ></asp:TextBox>
                <span class="RequiredField" >*</span>
                <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search" OnClick="btnSearch_Click"
                        CssClass="btn btn-primary" EnableViewState="false" 
                        ValidationGroup="EmpReferal" ><i class="icon-search icon-white"></i> Search</asp:LinkButton></br>
                <asp:RequiredFieldValidator ID="rfvCandidateID" runat="server" ControlToValidate="txtCandidateID"
                     ErrorMessage="Please Enter Candidate ID." ValidationGroup="EmpReferal" EnableViewState="False"
                            Display="Dynamic"></asp:RequiredFieldValidator> 
                        
            </div>
                <asp:HiddenField ID ="Level" runat ="server" />
                <asp:HiddenField ID ="ReqId" runat ="server" />
 
                <asp:TextBox ID="txtSortColumn" runat ="server" Visible ="false"  ></asp:TextBox>
                <asp:TextBox ID="txtSortOrder" runat ="server" Visible ="false"  ></asp:TextBox>
            <asp:Panel ID="pnlJobCartContent" runat="server" >
            <div style="text-align: left; ">
            <asp:UpdateProgress ID="UpdateProgress1"  EnableViewState="false" runat="server" DisplayAfter="10">
                            <ProgressTemplate>
                                <center>
                                    <asp:Image EnableViewState="false" runat="server" ID="imgLogo" ImageUrl="~/Images/AjaxLoading.gif" />
                                </center>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:ObjectDataSource ID="odsJobCartList" runat="server" SelectMethod="GetPagedForEmployeeReferal"
                            TypeName="TPS360.Web.UI.MemberJobCartDataSource" EnablePaging="true" SelectCountMethod="GetListCount"
                            SortParameterName="sortExpression">
                            <SelectParameters>
                                <asp:Parameter Name="memberId" DefaultValue="0" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    
                <asp:ListView ID="lsvJobCart" runat="server" DataKeyNames="Id" DataSourceID ="odsJobCartList" EnableViewState="true" OnItemDataBound="lsvJobCart_ItemDataBound"  OnPreRender ="lsvJobCart_PreRender" OnItemCommand ="lsvJobCart_ItemCommand">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr>
                                <th style="width: 15%; white-space: nowrap;">
                                    <asp:Label ID ="thCandidateName" runat ="server" Text ="Candidate Name" ToolTip ="Candidate Name"></asp:Label> </th>
                                <th style="width: 15%; white-space: nowrap;">
                                    <asp:Label ID ="thJobTitle" runat ="server" Text ="Job Title" ToolTip ="Job Title"></asp:Label> </th>
                                <th style="width: 15%; white-space: nowrap;">
                                    <asp:Label ID ="thCode" runat ="server" Text ="Code" ToolTip ="JobPosting Code"></asp:Label> </th>
                                <th style="width: 15%; white-space: nowrap;">
                                    <asp:Label ID ="thApplyDate" runat ="server"  Text ="Date Applied" ToolTip ="Date Applied"></asp:Label></th>  
                                <th style="width: 15%; white-space :nowrap;">
                                    <asp:Label ID ="thStatus" runat ="server" Text ="Status" ToolTip ="Status"></asp:Label></th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                              <tr class="Pager">
                        <td colspan="5" runat ="server" id="tdPager">
                             <ucl:Pager Id="pagerControl" runat ="server" EnableViewState ="true"  />
                        </td>
                    </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>  No jobs in Job Cart. </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td> <asp:Label ID="lblCandidateName" runat="server"></asp:Label></td>
                            <td> <asp:Label ID="lblJobTitle" runat="server"></asp:Label></td>
                            <td style="white-space: nowrap;"> <asp:Label ID="lblJobCode" runat="server" /> </td>
                            
                            <td style="white-space: nowrap;"> <asp:Label  ID="lblAppliedDate" runat="server" /> </td>
                           
                            <td  style="white-space: nowrap;">
                            <asp:HiddenField ID ="hdnJobPostingId" runat ="server" />
                            <asp:HiddenField ID ="hdnStatusId" runat ="server" />
                            <asp:HiddenField ID ="hdnCurrentLevel" runat ="server" />
                            <asp:Label ID ="lblStatus" runat ="server" ></asp:Label></td>
                          </tr>
                    </ItemTemplate>
                </asp:ListView>



                </div>
            </asp:Panel>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
         <div class="tab-pane active" id="tab2">
<%--Candidate Status in Employee Referral Portal.--%>
    <div style="padding: 18px;">
        <asp:UpdatePanel ID="pnlRegistrationInformation" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divRegInternal" runat="server">
              
                    <uc1:regI ID="rgInternal" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
		<%--Candidate Status in Employee Referral Portal.--%>
       </div>
        </div>
    </div>
	<%--Candidate Status in Employee Referral Portal.--%>
    </div>
</asp:Content>
