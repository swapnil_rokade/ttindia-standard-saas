﻿using System;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Web;
//Candidate Status in Employee Referral Portal.
using System.Web.UI.WebControls;
using System.Collections.Generic;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Web.Security;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Configuration;
//Candidate Status in Employee Referral Portal.
namespace TPS360.Web.UI
{
    //public partial class EmployeeReferral: System .Web .UI .Page 
     public partial class EmployeeReferral:EmployeeBasePage 
    {
        #region Veriables

        #endregion

        private WebHelper _helper;
        protected WebHelper Helper
        {
            get
            {
                if (_helper == null)
                {
                    _helper = new WebHelper(this);
                }

                return _helper;
            }
        }
        #region Properties
        private int JobId
        {
            get
            {
                if (!TPS360 .Common .Helper . StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]);
                }
                else return 0;

            }
        }

        #endregion

        #region Methods



        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            
            rgInternal.Role = ContextConstants.ROLE_CANDIDATE;
            if (!IsPostBack)
            {
                if (JobId > 0)
                {

                    TPS360 .Common .BusinessEntities.JobPosting job=new TPS360.Common.BusinessEntities.JobPosting ();
                    TPS360.BusinessFacade .IFacade facade= new  TPS360 .BusinessFacade .Facade ();
                    job=facade .GetJobPostingById (JobId );
                    if (job != null)

                        titleContainer.Text = titleContainer.Text + " - " +  job.JobTitle;
                }
            }
            
        }
		//Candidate Status in Employee Referral Portal.
		protected void btnSearch_Click(object sender, EventArgs e)
        {
            //if (Convert.ToInt32(txtCandidateID.Text) > 0)
            {
                odsJobCartList.SelectParameters["memberId"].DefaultValue = txtCandidateID.Text;
                lsvJobCart.DataBind();
            }
            /*else
            {
                lsvJobCart.DataSource = null;
                lsvJobCart.DataBind();
            }*/
        }

        #endregion

        #region ListView Events

        protected void lsvJobCart_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }

        }

        protected void lsvJobCart_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Common.BusinessEntities.MemberJobCart memberJobCart = ((ListViewDataItem)e.Item).DataItem as Common.BusinessEntities.MemberJobCart;

                if (memberJobCart != null)
                {
                    Label lblCandidateName = (Label)e.Item.FindControl("lblCandidateName");
                    Label lblJobTitle = (Label)e.Item.FindControl("lblJobTitle");
                    Label lblJobCode = (Label)e.Item.FindControl("lblJobCode");
                    Label lblJobStatus = (Label)e.Item.FindControl("lblJobStatus");
                    Label lblCurrentLevel = (Label)e.Item.FindControl("lblCurrentLevel");
                    Label lblAppliedDate = (Label)e.Item.FindControl("lblAppliedDate");
                    // Label lblPublishedBy = (Label)e.Item.FindControl("lblPublishedBy");
                    Label lblStatus = (Label)e.Item.FindControl("lblStatus");
                    HiddenField hdnJobPostingId = (HiddenField)e.Item.FindControl("hdnJobPostingId");
                    HiddenField hdnStatusId = (HiddenField)e.Item.FindControl("hdnStatusId");
                    HiddenField hdnCurrentLevel = (HiddenField)e.Item.FindControl("hdnCurrentLevel");
                    hdnJobPostingId.Value = memberJobCart.JobPostingId.ToString();

                    lblCandidateName.Text = memberJobCart.MemberName;
                    lblJobTitle.Text = memberJobCart.JobTitle;
                    lblJobCode.Text = memberJobCart.JobPostingCode;
                    lblAppliedDate.Text = memberJobCart.ApplyDate.ToShortDateString();
                    // lblPublishedBy.Text = memberJobCart.PublisherName;
                    
                        lblStatus.Visible = true;
                        lblStatus.Text = memberJobCart.CurrentLevelName;
                }
            }
        }
        protected void lsvJobCart_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobCart.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "JocartInCandidateOverviewRowPerPage";
            }



            if (lsvJobCart.Controls.Count == 0)
            {
                lsvJobCart.DataSource = null;
                lsvJobCart.DataBind();
            }
        }
		//Candidate Status in Employee Referral Portal.

        #endregion
    }
}