﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewerFeedback.aspx.cs
    Description: This is the InterviewerFeedback page.
    Created By: Prasanth Kumar G
    Created On: 20/Jul/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  0.1             22/Dec/2015             Prasanth Kumar g    Introduced Jobtitile and Candidate name
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/
using System;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Web;
using TPS360.Common.BusinessEntities;
using TPS360.BusinessFacade;
using System.Collections.Specialized;

using TPS360.Web.UI;


namespace TPS360.Web.UI
{
   // public partial class InterviewerFeedback: System.Web.UI.Page 
    public partial class InterviewerFeedback : EmployeeBasePage
    {
        #region Veriables
       
        #endregion

        private WebHelper _helper;
        protected WebHelper Helper
        {
            get
            {
                if (_helper == null)
                {
                    _helper = new WebHelper(this);
                }

                return _helper;
            }
        }
        #region Properties
        public int InterviewId
        {
            get
            {
                int _interviewId = 0;

                string[] words = URL_Parameter.Split('~');

                for (int i = 0; i < words.Length; i++)
                {
                    if (i == 0)
                    {
                        _interviewId = int.Parse(words[i]);
                    }

                }


                return _interviewId;

            }
        }

        public string InterviewerEmail
        {
            get
            {
                string _InterviewerEmail = "";
                string[] words = URL_Parameter.Split('~');

                for (int i = 0; i < words.Length; i++)
                {
                    if (i == 1)
                    {
                        _InterviewerEmail = words[i];
                    }

                }


                return _InterviewerEmail;

            }
        }

       //Code introduced by Prasanth on 22/Dec/2015 Start
        public string CandidateName
        {
            get
            {
                string _CandidateName = "";
                string[] words = URL_Parameter.Split('~');

                for (int i = 0; i < words.Length; i++)
                {
                    if (i == 2)
                    {
                        _CandidateName = words[i];
                    }

                }


                return _CandidateName;

            }
        }
        public string JobTitle
        {
            get
            {
                string _JobTitle = "";
                string[] words = URL_Parameter.Split('~');

                for (int i = 0; i < words.Length; i++)
                {
                    if (i == 3)
                    {
                        _JobTitle = words[i];
                    }

                }


                return _JobTitle;

            }
        }

        

        public string InterviewDate
        {
            get
            {
                string _InterviewDate = "";
                string[] words = URL_Parameter.Split('~');

                for (int i = 0; i < words.Length; i++)
                {
                    if (i == 4)
                    {
                        _InterviewDate = words[i];
                    }

                }


                return _InterviewDate;

            }
        }
        //****************END****************************


        //Code introduced by Prasanth on 23/Dec/2015 Start
        public string CandidateId
        {
            get
            {
                string _CandidateId = "";
                string[] words = URL_Parameter.Split('~');

                for (int i = 0; i < words.Length; i++)
                {
                    if (i == 5)
                    {
                        _CandidateId = words[i];
                    }

                }


                return _CandidateId;

            }
        }
        //*******************END************************


        //Property introduced by Prasanth on 29/Dec/2015
        public string RecruiterEmail
        {
            get
            {
                string _RecruiterEmail = "";
                string[] words = URL_Parameter.Split('~');

                for (int i = 0; i < words.Length; i++)
                {
                    if (i == 6)
                    {
                        _RecruiterEmail = words[i];
                    }

                }


                return _RecruiterEmail;

            }
        }

  

      

        public string URL_Parameter
        {
            get
            {
                string _URLParameter = "";
                if (!TPS360.Common.Helper.StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWID]))
                {
                    return Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWID];
                }
                else
                    return _URLParameter;

            }
        }

      

        
      

        #endregion

        #region Methods

        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                Submitfeedback();
            }

        }


        protected void Submitfeedback()
        {
            try
            {
                
                    uclIA.UpdateResponse();
                    uclIF.SaveInterviewerfeedback();
                    btnSubmit.Enabled = false;
                    
                    MiscUtil.ShowMessage(lblMessage, "Interview Feedback has been submited Successfully", false);
                
            }
            catch (ArgumentException ex)
            {
                MiscUtil.ShowMessage(lblMessage, ex.Message, true);
            }
        }

       

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            uclIF.Checkexists(InterviewId,InterviewerEmail);
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            if (!IsPostBack)
            {
                uclIF.InterviewerEmail = InterviewerEmail;
                uclIF.InterviewId = InterviewId;
                uclIF.MemberId = int.Parse(CandidateId);
                uclIF.RecruiterEmail = RecruiterEmail;
                uclIF.CandidateId = int.Parse(CandidateId);
                uclIF.CandidateName = CandidateName;
                uclIF.InterviewDate = InterviewDate;

                uclIA.InterviewId = InterviewId;
                uclIA.InterviewerEmail = InterviewerEmail;
                


                    titleContainer.Text = titleContainer.Text + " - " + CandidateName + " - " + JobTitle + " - " + InterviewDate; //line introduced by Prasanth on 22/Dec/2015
               

            }
            
        }

        
        
        protected void btnSubmit_Click(object sender, EventArgs e)
        {


            try
            {
                 
                string msg = uclIA.ValidateListview();
                 
                if (msg != "")
                {
                    MiscUtil.ShowMessage(lblMessage, msg, false);
                }
                
                else
                {

                    uclConfirm.AddMessage("Warning: Feedback is one time submission And cannot be edited. Do you want to proceed?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
                }

            }
            catch (ArgumentException ex)
            {
                MiscUtil.ShowMessage(lblMessage, ex.Message, true);
            }


            

           
        }

        #endregion
    }
}