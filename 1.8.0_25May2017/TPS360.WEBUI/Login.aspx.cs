﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Login.aspx.cs
    Description: This is the page used for Login of the user.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               24-Sept-2008          Shivanand           Defect id: 8655; Implemented Remember me concept.
    0.2               17-Mar-2009           Jagadish            Defect id: 8655; Changed the code to remember Password.
 *  0.3               14-May-2009           Rajendra A.S        Defect id: 10465; Code modified in LoginUser_LoggedIn() event.
 *  0.4               04-Jun-2009           Veda                Defect Id:10554 ;User cannot goto login page by entering the the Login URL 
    0.5               15-Jun-2009           Veda                Defect Id:10628 ;License connection string is defined in a new section of web.config file 
 *  0.6               24-Jun-2009           Anand Dixit         Changes due to renaming of LicenseSectionHandler to TPS360Section handler are implemented
 *  0.7               08-Jul-2009           Veda                Defect Id:10871;Removed the links in Log in page.
    0.8              Jul-14-2009         Nagrathna .V.B         DefectId:9112 "Login" link has been removed.
 *  0.9               07-Oct-2009           Veda                Defect Id:11534 : Delete teh saved license details after teh expirydate
 *  1.0               7/June/2016         pravin khot           added - employee.Status
    1.1               10/Jun/2016           Prasanth            Introduced LDAP
 *  1.2               16/Mar/2017           Sumit Sonawane      code changed to access from IP add.
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Web.Security;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using TPS360.Common.Helper;
using System.Web;
using System.Configuration;
using TPS360.Common;
using System.Web.Configuration;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections;
using System.Net.NetworkInformation;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using Sys = System.Web.UI.WebControls;

namespace TPS360.Web.UI
{
    public partial class Login : BasePage
    {
        #region Variables
        string varchrDomainName = GettingCommonValues.GetDomainName();
        #endregion

        #region Properties

        #endregion

        #region Methods
        //Code introduced by Prasanth on 10/Jun/2016 Start
        protected void LoginUser_Authenticate(object sender, AuthenticateEventArgs e)
        {
            try
            {
                Sys.TextBox UserNames = (Sys.TextBox)LoginUser.FindControl("UserName");
                Sys.TextBox Password = (Sys.TextBox)LoginUser.FindControl("Password");
                if (ActiveDirectoryConnector.IsUserLoggedIn(UserNames.Text, Password.Text))
                {
                    e.Authenticated = true;
                }
                else
                {
                    e.Authenticated = false;
                }

            }
            catch (Exception ex)
            {
                e.Authenticated = false;
                LoginUser.FailureText = ex.Message;
            }
        }
        //******************END************************
        #endregion

        #region Events
         
        protected void Page_Load(object sender, EventArgs e)
        {
            //Code introduced by Prasanth on 10/Jun/2016 Start
            Sys.TextBox UserNames = (Sys.TextBox)LoginUser.FindControl("UserName");
            Sys.TextBox Password = (Sys.TextBox)LoginUser.FindControl("Password");
            if (UserNames.Text != "" && Password.Text != "")
            {
                Member Mem = new Member();
                bool isldap;
                Mem = Facade.GetMemberByUserName(UserNames.Text);
                if (Mem == null)
                {
                    isldap = false;
                }
                else
                {
                    isldap = Mem.IsLDAP;
                }

                if (isldap == true)
                {
                    LoginUser.Authenticate += new AuthenticateEventHandler(LoginUser_Authenticate);
                }
            }

            //********************END*******************************8
            if (!IsPostBack)
            {               

                if (Request["ReturnUrl"] != null)
                {
                    if (Request["ReturnUrl"].ToLower().Contains("/vendor/"))
                    {
                        Response.Redirect(UrlConstants.ApplicationBaseUrl + "Vendor/Login.aspx?SeOut=SessionOut");
                    }
                }



                if (Request.QueryString.Count > 0)
                {
                    string strTimeout = "";
                    try
                    {
                        strTimeout = Request.QueryString["SeOut"].ToString();
                    }
                    catch { }
                    if (strTimeout != "")
                    {
                        Literal lit = (Literal)LoginUser.FindControl("FailureText");
                        lit.Text = "Your Session has Expired.";
                    }
                }
            }

            LoginStatus save;
            save = (LoginStatus)Page.Master.FindControl("lgsLogin");
            if (save != null)
                save.Visible = false;
            try
            {

                HtmlGenericControl HeaderDiv = (HtmlGenericControl)this.Master.FindControl("SettingMenuHolder");
                HeaderDiv.Visible = false;
            }
            catch { }
            string ApplicationSource = WebConfigurationManager.AppSettings["ApplicationSource"].ToString();
            if (ApplicationSource.ToLower() == "genisys")
            { 
            TPS360SectionHandler licenseSection = WebConfigurationManager.GetSection("tps360license") as TPS360SectionHandler; ;
            if (licenseSection != null)
            {
                try
                {
                    System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                    System.Globalization.DateTimeFormatInfo info = new System.Globalization.DateTimeFormatInfo();
                    info.ShortDatePattern = "MM/dd/yyyy";
                    culture.DateTimeFormat = info;

                    System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                    System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
                    string LicenseKey = (licenseSection.Details["LicenseKey"].Value != null) ? licenseSection.Details["LicenseKey"].Value : String.Empty;
                    DateTime StartDate = Convert.ToDateTime(licenseSection.Details["StartDate"].Value);
                    DateTime ExpireDate = Convert.ToDateTime(licenseSection.Details["ExpireDate"].Value);
                    DateTime currentDate = DateTime.Now;
                    if (LicenseKey != null && ExpireDate != null && StartDate != null)
                    {
                        DateTime MaxDate = ExpireDate.AddDays(1);
                        if (StartDate > currentDate && MaxDate < currentDate)
                        {
                            DeleteLicenseDetails();
                            Response.Redirect("StartUp.aspx");
                        }
                    }
                }
                catch (FormatException ex)
                {
                    DeleteLicenseDetails();
                    Response.Redirect("StartUp.aspx");
                }
                catch (NullReferenceException ex)
                {
                    DeleteLicenseDetails();
                    Response.Redirect("StartUp.aspx");
                }
            }
            else
            {
                Response.Redirect("StartUp.aspx");
            }
            
            }
            if (!IsPostBack)
            {

                if (Request.Cookies["LoginStatus"] != null && Request.Cookies["LoginStatus"].Value != "")
                {
                    string LoginStatusPath = ConfigurationManager.AppSettings["LoginStatusPath"].ToString();
                    Literal lit = (Literal)LoginUser.FindControl("FailureText");
                    Response.Cookies["LoginStatus"].Path = LoginStatusPath;
                    Response.Cookies["LoginStatus"].HttpOnly = true;
                    Response.Cookies["LoginStatus"].Value = "";
                    string rawInput = Request.Cookies["LoginStatus"].Value;
                    string encodeString = Server.HtmlEncode(rawInput);
                    lit.Text = encodeString;
                }

                Context.Items[ContextConstants.MEMBER] = null;
                Context.Items[ContextConstants.SITESETTING] = null;
                //Check if the browser support cookies 
                if (Request.Browser.Cookies)
                {
                    //Check if the cookies with name PBLOGIN exist on user's machine & populate the textboxes.
                    if (Request.Cookies["PBLOGIN"] != null && Request.Cookies["PBLOGIN"]["UNAME"] != null)
                    {
                        TextBox txtUsername = (TextBox)LoginUser.FindControl("UserName");
                        txtUsername.Text = Request.Cookies["PBLOGIN"]["UNAME"].ToString();
                        LoginUser.UserName = Request.Cookies["PBLOGIN"]["UNAME"].ToString();
                        TextBox txtPassword = (TextBox)LoginUser.FindControl("Password");
                        txtPassword.Attributes.Add("value", Request.Cookies["PBLOGIN"]["UPASS"].ToString());

                        CheckBox chkRemember = (CheckBox)LoginUser.FindControl("RememberMe");
                        chkRemember.Checked = true;

                        LoginUser.RememberMeSet = true;
                    }
                }

            }

            if (HttpContext.Current.IsDebuggingEnabled) 
            { 
            
            }

            string fileContent = string.Empty;
            StringBuilder test = new StringBuilder();
            using (System.IO.StreamReader reader = new System.IO.StreamReader(Server.MapPath("~/Images/News.html")))
            {
                fileContent = reader.ReadToEnd();
            }
            Literal1.Text = fileContent;
          
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Helper.Url.Redirect(UrlConstants.CommonSite.HOME_PAGE);
        }
        protected void LoginUser_Error(object sender, EventArgs e)
        {              
           Facade.EntryUserAccessAppReset(LoginUser.UserName, "TPS", varchrDomainName);
            TextBox txtPassword = (TextBox)LoginUser.FindControl("Password");
            txtPassword.Focus();
        }

       
        protected void LoginUser_LoggedIn(object sender, EventArgs e)
        {
           
            if (Pagevalue_i == -2)
            {
                Pagevalue_i = 0;
                SecureUrl urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_RESET, "", "UserID", LoginUser.UserName);
                Response.Cookies["LoginStatus"].Value = "User already logged in.";
                try
                {
                    CheckBox chkRemember = (CheckBox)LoginUser.FindControl("RememberMe");
                    if (chkRemember.Checked)
                    {
                        CookieStore();
                    }
                }
                catch { }
                Helper.Url.Redirect(urlLogin.ToString());
                return;
            }
            AfterLogin();
        }
        protected void LoginUser_LoggingIn(object sender, LoginCancelEventArgs e)
        {
            TextBox UserNames = (TextBox)LoginUser.FindControl("UserName");
            try 
            {
                MembershipUser user = Membership.GetUser(UserNames.Text);

            }
            catch (Exception ex) 
            {
                if (UserNames.Text.Contains(",")) 
                {
                    SecureUrl urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
                    Response.Cookies["LoginStatus"].Value = "Your login attempt was not successful.Please try again. ";// +Request.UserHostName;// "Maximum number of users reached";
                    Helper.Url.Redirect(urlLogin.ToString());
                    return;
                }
            }
            
            string UserName = LoginUser.UserName;
            UserName = UserName.Replace(",", ".");
            if (Roles.IsUserInRole(LoginUser.UserName, ContextConstants.ROLE_ADMIN) || Roles.IsUserInRole(LoginUser.UserName, ContextConstants.ROLE_EMPLOYEE )||Roles.IsUserInRole(LoginUser.UserName,ContextConstants.ROLE_DEPARTMENT_CONTACT))
            {
                //Sumit Sonawane 16/Mar/2017 code changed to access from IP add.
                //if (ApplicationSource == ApplicationSource.MainApplication)
                //{
                //    BeforeLogin();
                //}
                //else if (ApplicationSource == ApplicationSource.GenisysApplication)
                //{
                //    BeforeLogin();  //Sumit Sonawane 16/Mar/2017 code added to access from IP add.
                //}
                if (ApplicationSource.ToString() != "" || ApplicationSource != null)
                {
                    BeforeLogin();
                }
                else if (ApplicationSource == ApplicationSource.LandTApplication)
                    return;               
            }
            else
            {
                SecureUrl urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
                Response.Cookies["LoginStatus"].Value = "Your login attempt was not successful. Please try again. ";// +Request.UserHostName;// "Maximum number of users reached";
                Helper.Url.Redirect(urlLogin.ToString());
                return;
            }
            
           
            //if (!Roles.IsUserInRole(UserName, ContextConstants.ROLE_CANDIDATE) && !Roles.IsUserInRole(UserName, ContextConstants.ROLE_VENDOR)) 
            //{
            //    BeforeLogin();
            //}
            //else
            //{
            //    SecureUrl urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
            //    Response.Cookies["LoginStatus"].Value = "Your login attempt was not successful. Please try again. ";// +Request.UserHostName;// "Maximum number of users reached";
            //    Helper.Url.Redirect(urlLogin.ToString());
            //    return;
            //}
        }
        int Pagevalue_i = 0;
        void BeforeLogin()
        {
            int allowedusers = 0;
            TPS360SectionHandler licenseSection = WebConfigurationManager.GetSection("tps360license") as TPS360SectionHandler; ;
            if (licenseSection != null)
            {
                try
                {
                    allowedusers = Convert.ToInt32(licenseSection.Details["NoofUsersAllowed"].Value);
                }
                catch { }
            }
            int i = Facade.EntryofUserAccessApp(LoginUser.UserName, Session.SessionID, "TPS", varchrDomainName, Request.Browser.Browser, allowedusers, Request.UserHostName);
            SecureUrl urlLogin;

            if (i == -5)
            {
                urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
                Response.Cookies["LoginStatus"].Value = "User login not permitted from IP : " + Request.UserHostName;// "Maximum number of users reached";
                Helper.Url.Redirect(urlLogin.ToString());
                return;
            }
            else if (i == -7)
            {
                urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
                Response.Cookies["LoginStatus"].Value = "Only one session is allowed per browser.";// "Maximum number of users reached";
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-30); //Delete the cookie
                Response.Redirect("~/Login.aspx?SeOut=SessionOut", true);
                Helper.Url.Redirect(urlLogin.ToString());
                return;
            }
            //else if (i == -3)
            //{
            //    urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
            //    Response.Cookies["LoginStatus"].Value = "Number of concurrent users allowed by license has been met. <br> Please try again after another user logs off.";// "Maximum number of users reached";
            //    Helper.Url.Redirect(urlLogin.ToString());
            //    return;
            //}
            
            Pagevalue_i = i;
        }

        void AfterLogin()
        {
            MembershipUser user = Membership.GetUser(LoginUser.UserName);
            CheckBox chkRemember = (CheckBox)LoginUser.FindControl("RememberMe");
            if (chkRemember.Checked)
            {
                FormsAuthentication.SetAuthCookie(user.UserName, true);
            }
            else
            {
                FormsAuthentication.SetAuthCookie(user.UserName, false);
            }

            if (chkRemember.Checked)
            {
                //Check if the browser support cookies 
                CookieStore();
            }

            // Delete the cookies
            else if (Request.Cookies["PBLOGIN"] != null)
            {
                HttpCookie aCookie;
                string cookieName;

                cookieName = Request.Cookies["PBLOGIN"].Name;
                aCookie = new HttpCookie(cookieName);
                aCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(aCookie);

            }

            Member member = Facade.GetMemberByUserId((Guid)user.ProviderUserKey);
            string returnUrl = FormsAuthentication.GetRedirectUrl(user.UserName, false);
            string RedirectUrl = string.Empty;
            bool IsRedirect = false;
            if (!string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = returnUrl.Replace("%2f", "/");
                string[] returnUrlPart = returnUrl.Split('/');

                if (returnUrlPart.Length > 3)
                {
                    if (returnUrlPart[3].ToString().Contains("?"))
                        RedirectUrl = returnUrlPart[2].ToString() + "/" + returnUrlPart[3].ToString().Substring(0, returnUrlPart[3].IndexOf('?'));
                    else
                        RedirectUrl = returnUrlPart[2].ToString() + "/" + returnUrlPart[3].ToString();
                    IsRedirect = Facade.GetMemberPrivilegeForMemberIdAndRequiredURL(member.Id, RedirectUrl.Trim());
                    if (IsRedirect)
                    {
                        FormsAuthentication.RedirectFromLoginPage(user.UserName, false);
                        return;
                    }
                }
            }

            SecureUrl url;


            MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(member.Id);

            if (member != null)
            {
                Session["Loggedin"] = "Yes";
                string role = (Roles.GetRolesForUser(LoginUser.UserName)).GetValue(0).ToString();
                //Create Login activity history
                MiscUtil.AddActivity(role, member.Id, member.Id, ActivityType.Login, Facade);

                if (Roles.IsUserInRole(LoginUser.UserName, ContextConstants.ROLE_CANDIDATE))
                {
                    FormsAuthentication.SignOut();
                }
                else if (Roles.IsUserInRole(LoginUser.UserName, ContextConstants.ROLE_CLIENT) || Roles.IsUserInRole(LoginUser.UserName, ContextConstants.ROLE_PARTNER) || Roles.IsUserInRole(LoginUser.UserName, ContextConstants.ROLE_COMPANY)) //0.3
                {
                    CompanyContact contact = Facade.GetCompanyContactByMemberId(member.Id);

                    Helper.Url.Redirect(UrlConstants.Company.HOME_PAGE, string.Empty, UrlConstants.PARAM_COMPANY_ID, contact.CompanyId.ToString());
                }

                    else if (Roles.IsUserInRole(LoginUser.UserName, ContextConstants.ROLE_ADMIN) || Roles.IsUserInRole(LoginUser.UserName, ContextConstants.ROLE_EMPLOYEE) || Roles.IsUserInRole(LoginUser.UserName, ContextConstants.ROLE_VENDOR) || Roles.IsUserInRole(LoginUser.UserName, ContextConstants.ROLE_CONSULTANT) || Roles.IsUserInRole(LoginUser.UserName, ContextConstants.ROLE_DEPARTMENT_CONTACT ) )
                    {
                        Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
                    }
            }
            else if (member == null && !Roles.IsUserInRole(LoginUser.UserName, ContextConstants.ROLE_ADMIN))
            {
                    url = UrlHelper.BuildSecureUrl(UrlConstants.CommonSite.COMPLETE_REGISTER_PAGE, "", UrlConstants.PARAM_USER_ID, user.ProviderUserKey.ToString());
                Helper.Url.Redirect(url.ToString());
            }
        }

        private void DeleteLicenseDetails()
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
            TPS360SectionHandler license = config.GetSection("tps360license") as TPS360SectionHandler;
            TPS360ConfigElement lkey = new TPS360ConfigElement("LicenseKey", "");
            TPS360ConfigElement sdate = new TPS360ConfigElement("StartDate", "");
            TPS360ConfigElement expdate = new TPS360ConfigElement("ExpireDate", "");
            TPS360ConfigElement noofusersallowed = new TPS360ConfigElement("NoofUsersAllowed", "");
            license.Details.Remove(lkey);
            license.Details.Remove(sdate);
            license.Details.Remove(expdate);
            license.Details.Remove(noofusersallowed);
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("tps360license"); 
        }

        void CookieStore()
        {
            if ((Request.Browser.Cookies))
            {
                //Create a cookie with expiry of 30 days 
                Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(30);

                //Write username to the cookie 
                TextBox txtUsername = (TextBox)LoginUser.FindControl("UserName");
                Response.Cookies["PBLOGIN"]["UNAME"] = txtUsername.Text;

                //Write password to the cookie 
                TextBox txtPassword = (TextBox)LoginUser.FindControl("Password");
                Response.Cookies["PBLOGIN"]["UPASS"] = txtPassword.Text;

            }
        }

        #endregion
      
}
}
