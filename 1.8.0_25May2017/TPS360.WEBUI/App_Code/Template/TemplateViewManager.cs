﻿using System.IO;
using System.Web;

namespace TPS360.Web.UI
{

    public class TemplateViewManager
    {
        public static ContentsResponse RenderView(string path)
        {
            return RenderView(path, null,null,null,null,null,null );
        }

        public static ContentsResponse RenderView(string path, object data, object datavalue,object NoteUpdatorName,object NoteUpdatetime,object jobPostingId,object StatusId)
        {
            

            TemplatePage pageHolder = new TemplatePage();
            TemplateUserControl viewControl = 
                (TemplateUserControl)pageHolder.LoadControl(path);

            if (viewControl == null)
                return ContentsResponse.Empty;

            if (data != null)
            {
                viewControl.Data = data;
                viewControl.DataValue = datavalue;
                viewControl.NoteUpdatorName = NoteUpdatorName;
                viewControl.NoteUpdateTime = NoteUpdatetime;
                viewControl.JobPostingID = jobPostingId;
                viewControl.StatusId = StatusId;
            }

            pageHolder.Controls.Add(viewControl);

            string result = "";
            using (StringWriter output = new StringWriter())
            {
                HttpContext.Current.Server.Execute(pageHolder, output, false);
                result = output.ToString();
            }

            return new ContentsResponse(
                    result, 
                    viewControl.StartupScript, 
                    viewControl.CustomStyleSheet
                    );
        }
    }
}