
using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberJobAppliedDataSource : ObjectDataSourceBase
    {
        PagedResponse<MemberJobApplied> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string memberId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberJobApplied> GetPaged(string MemberId,string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.Conditions.Add("MemberId",MemberId );
                pageResponse = Facade.MemberJobapplied_GetPaged (pageRequest);
                return pageResponse.Response as List<MemberJobApplied>;
            }
        }
       
    }
}