﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EventLogDataSource.cs
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 20/May/2016         Sumit Sonawane      Introduced GetListCount() GetPaged()
  
----------------------------------------------------------------------------------------------------------------------------------------------
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

/// <summary>
/// Summary description for EventLogDataSource
/// </summary>
namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class EventLogDataSource : ObjectDataSourceBase
    {
        PagedResponse<EventLogForRequisitionAndCandidate> pageResponse = null;
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int JobPostingId, int CandidateId, int CreatorId, int ClientId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int JobPostingId, int ClientId, int CreatorId, string StartDate, string EndDate, string ActionType)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<EventLogForRequisitionAndCandidate> GetPaged(int JobPostingId, int CandidateId, int CreatorId, int ClientId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (JobPostingId > 0)
                {
                    pageRequest.Conditions.Add("JobPostingId", JobPostingId.ToString());
                }
                if (CandidateId > 0)
                {
                    pageRequest.Conditions.Add("CandidateId", CandidateId.ToString());
                }
                if (CreatorId > 0)
                {
                    pageRequest.Conditions.Add("CreatorId", CreatorId.ToString());
                }
                if (ClientId > 0)
                {
                    pageRequest.Conditions.Add("ClientId", ClientId.ToString());
                }

                pageResponse = Facade.GetPagedForEventLog(pageRequest);
                return pageResponse.Response as List<EventLogForRequisitionAndCandidate>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<EventLogForRequisitionAndCandidate> GetPaged(int JobPostingId, int ClientId, int CreatorId, string StartDate, string EndDate, string ActionType, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (JobPostingId > 0)
                {
                    pageRequest.Conditions.Add("JobPostingId", JobPostingId.ToString());
                }
                if (ClientId > 0)
                {
                    pageRequest.Conditions.Add("ClientId", ClientId.ToString());
                }
                if (CreatorId > 0)
                {
                    pageRequest.Conditions.Add("CreatorId", CreatorId.ToString());
                }
                pageRequest.Conditions.Add("StartDate", StartDate != null ? Convert.ToDateTime(StartDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("EndDate", EndDate != null ? Convert.ToDateTime(EndDate.ToString()).ToString() : DateTime.MinValue.ToString());
                if (ActionType != "0")
                    pageRequest.Conditions.Add("ActionType", ActionType);
                pageResponse = Facade.GetPagedEventLogReport(pageRequest);
                return pageResponse.Response as List<EventLogForRequisitionAndCandidate>;
            }
        }

        /////////////Code Added by Sumit Sonawane on 20/May/2016/////////////////////////
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int JobPostingId, int ClientId, int CreatorId, string StartDate, string EndDate, string ActionType, bool IsTeamHiringReport, string TeamMemberId, string TeamId)
        {
            return pageResponse.TotalRow;
        }
        ////////////////////////////////////////////////////////////////////////////////////////

        /////////////Code Added by Sumit Sonawane on 20/May/2016/////////////////////////
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<EventLogForRequisitionAndCandidate> GetPaged(int JobPostingId, int ClientId, int CreatorId, string StartDate, string EndDate, string ActionType, bool IsTeamHiringReport, string TeamMemberId, string TeamId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (JobPostingId > 0)
                {
                    pageRequest.Conditions.Add("JobPostingId", JobPostingId.ToString());
                }
                if (ClientId > 0)
                {
                    pageRequest.Conditions.Add("ClientId", ClientId.ToString());
                }
                if (CreatorId > 0)
                {
                    pageRequest.Conditions.Add("CreatorId", CreatorId.ToString());
                }
                if (TeamMemberId != "" && TeamMemberId != null && TeamMemberId != "0")
                {
                    pageRequest.Conditions.Add("TeamHiringReport", TeamMemberId);

                    // pageRequest.Conditions.Add("TeamsId", TeamId);
                }

                if (TeamId != "" && TeamId != null && TeamId != "0")
                {
                    //pageRequest.Conditions.Add("TeamHiringReport", TeamMemberId);

                    pageRequest.Conditions.Add("TeamsId", TeamId);
                }
                pageRequest.Conditions.Add("StartDate", StartDate != null ? Convert.ToDateTime(StartDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("EndDate", EndDate != null ? Convert.ToDateTime(EndDate.ToString()).ToString() : DateTime.MinValue.ToString());
                if (ActionType != "0")
                    pageRequest.Conditions.Add("ActionType", ActionType);
                pageResponse = Facade.GetPagedEventLogReport(pageRequest);
                return pageResponse.Response as List<EventLogForRequisitionAndCandidate>;
            }
        }

        /////////////Code Added by Sumit Sonawane on 2/Mar/2017/////////////////////////
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int MemberID)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<EventLogForRequisitionAndCandidate> GetPagedHiringMatrixLogByMemberId(int memberId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedHiringMatrixEventLog(memberId, pageRequest);
                return pageResponse.Response as List<EventLogForRequisitionAndCandidate>;
            }
        }
    }
}