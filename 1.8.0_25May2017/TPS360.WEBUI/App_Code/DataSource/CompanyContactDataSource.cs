/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:CompanyContactDataSource.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author                Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Jun-03-2009            Gopala Swamy J   Defect id :10517 ; New function "UpdateCompanyContact()" added
 ------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class CompanyConatctDataSource : ObjectDataSourceBase
    {
        PagedResponse<CompanyContact> pageResponse = null;
        PagedResponse<TPS360.Common.BusinessEntities.ListWithCount> pageResponseListWithCount = null;
        PagedResponse <VendorSubmissions > pageResponseVendorSubmissions=null;
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string CompanyId, string SortOrder)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<CompanyContact> GetPaged(string CompanyId, string SortOrder, string sortExpression, int startRowIndex, int maximumRows) // 0.1
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                if (CompanyId != "")
                {
                    pageRequest.Conditions.Add("CompanyId", CompanyId);
                }
                pageResponse = Facade.GetPagedCompanyContact(pageRequest);
                return pageResponse.Response as List<CompanyContact>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<CompanyContact> GetAllByCompanyId(string companyId)
        {
            return Facade.GetAllCompanyContactByComapnyId(Convert.ToInt32(companyId)) as List<CompanyContact>;
        }

        public void Update(CompanyContact newValues)
        {
            //  simulate putting this record back into the database
            //CompanyContact oldValues = Facade.GetCompanyContactById(newValues.Id);

            //oldValues.FirstName = newValues.FirstName;
            //oldValues.LastName = newValues.LastName;
            //oldValues.Title = newValues.Title;
            //oldValues.Email = newValues.Email;
            //oldValues.DirectNumber = newValues.DirectNumber;
            //oldValues.MobilePhone = newValues.MobilePhone;
            //oldValues.City = newValues.City;

            //Facade.UpdateCompanyContact(oldValues);
        }
        public void UpdateCompanyContact(CompanyContact newValues) //0.1 starts here
        {
            //  simulate putting this record back into the database
            CompanyContact oldValues = Facade.GetCompanyContactById(newValues.Id);

            oldValues.FirstName = newValues.FirstName;
            oldValues.LastName = newValues.LastName;
            oldValues.Title = newValues.Title;
            oldValues.Email = newValues.Email;
            oldValues.DirectNumber = newValues.DirectNumber;
            oldValues.MobilePhone = newValues.MobilePhone;
            oldValues.City = newValues.City;

            Facade.UpdateCompanyContact(oldValues);
        } //0.1 end here 
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount_ForSummary(int JobPostingId, int VendorId, int ContactId, DateTime StartDate, DateTime EndDate, string Type)
        {
            return pageResponseListWithCount.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<ListWithCount> GetPaged_ForSummary(int JobPostingId, int VendorId, int ContactId, DateTime StartDate, DateTime EndDate, string Type, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (JobPostingId > 0)               pageRequest.Conditions.Add("JobPostingID", JobPostingId.ToString());
                if (VendorId > 0)                   pageRequest.Conditions.Add("VendorId", VendorId.ToString());
                if (ContactId > 0)                  pageRequest.Conditions.Add("ContactId", ContactId.ToString());
                if (StartDate != DateTime.MinValue) pageRequest.Conditions.Add("AddedFrom", StartDate != null ? Convert.ToDateTime(StartDate.ToString()).ToString("yyyyMMdd") : DateTime.MinValue.ToString());
                if (EndDate != DateTime.MinValue) pageRequest.Conditions.Add("AddedTo", EndDate != null ? Convert.ToDateTime(EndDate.ToString()).ToString("yyyyMMdd") : DateTime.MinValue.ToString());
                pageResponseListWithCount = Facade.GetPagedVendorSubmissionsSummary (pageRequest, Type);
                return pageResponseListWithCount.Response as List<ListWithCount>;
            }
        }


        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount_ForVendorSubmissionDetail(int JobPostingId, int VendorId, int ContactId, int CandidateId, DateTime StartDate, DateTime EndDate, string Type)
        {
            return pageResponseVendorSubmissions.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<VendorSubmissions> GetPaged_ForVendorSubmissionDetail(int JobPostingId, int VendorId, int ContactId, int CandidateId, DateTime StartDate, DateTime EndDate, string Type, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (JobPostingId > 0) pageRequest.Conditions.Add("JobPostingID", JobPostingId.ToString());
                if (VendorId > 0) pageRequest.Conditions.Add("VendorId", VendorId.ToString());
                if (ContactId > 0) pageRequest.Conditions.Add("ContactMemberId", ContactId.ToString());
                if (CandidateId > 0) pageRequest.Conditions.Add("CandidateId", CandidateId.ToString());
                if (StartDate != DateTime.MinValue) pageRequest.Conditions.Add("AddedFrom", StartDate != null ? Convert.ToDateTime(StartDate.ToString()).ToString("yyyyMMdd") : DateTime.MinValue.ToString("yyyyMMdd"));
                if (EndDate != DateTime.MinValue) pageRequest.Conditions.Add("AddedTo", EndDate != null ? Convert.ToDateTime(EndDate.ToString()).ToString("yyyyMMdd") : DateTime.MinValue.ToString("yyyyMMdd"));
                pageResponseVendorSubmissions = Facade.GetPagedVendorSubmissions(pageRequest);
                return pageResponseVendorSubmissions.Response as List<VendorSubmissions>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<VendorSubmissions> GetPaged_ForVendorSubmissionDetailForReport(int JobPostingId, int VendorId, int ContactId, int CandidateId, DateTime StartDate, DateTime EndDate, string Type, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (JobPostingId > 0) pageRequest.Conditions.Add("JobPostingID", JobPostingId.ToString());
                if (VendorId > 0) pageRequest.Conditions.Add("VendorId", VendorId.ToString());
                if (ContactId > 0) pageRequest.Conditions.Add("ContactId", ContactId.ToString());
                if (CandidateId > 0) pageRequest.Conditions.Add("CandidateId", CandidateId.ToString());
                if (StartDate != DateTime.MinValue) pageRequest.Conditions.Add("AddedFrom", StartDate != null ? Convert.ToDateTime(StartDate.ToString()).ToString("yyyyMMdd") : DateTime.MinValue.ToString("yyyyMMdd"));
                if (EndDate != DateTime.MinValue) pageRequest.Conditions.Add("AddedTo", EndDate != null ? Convert.ToDateTime(EndDate.ToString()).ToString("yyyyMMdd") : DateTime.MinValue.ToString("yyyyMMdd"));
                pageResponseVendorSubmissions = Facade.GetPagedVendorSubmissions(pageRequest);
                return pageResponseVendorSubmissions.Response as List<VendorSubmissions>;
            }
        }




    }
}