using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class CompanyDocumentsDataSource : ObjectDataSourceBase
    {
        PagedResponse<CompanyDocument> pageResponse = null;
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int companyId)
        {
            return pageResponse.TotalRow;
        }
        public List<CompanyDocument > GetPaged(int companyId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.CompanyDocumentGetPaged (companyId , pageRequest);
                return pageResponse.Response as List<CompanyDocument>;
            }
        }
        
    }
}