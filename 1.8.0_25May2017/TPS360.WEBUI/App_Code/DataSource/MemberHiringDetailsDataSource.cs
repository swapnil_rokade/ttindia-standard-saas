﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

/// <summary>
/// Summary description for MemberHiringDetails
/// </summary>
namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberHiringDetailsDataSource:ObjectDataSourceBase
    {
        PagedResponse<MemberHiringDetails> pageResponse = null;
        PagedResponse<MemberOfferJoinDetails> pageResponseOfferJoin = null;
        PagedResponse<MemberOfferJoinDetails> pageResponseofferJoinCommon = null;
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string OfferLevelName)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int MemberId,string OfferLevelName)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public IList <MemberOfferJoinDetails> GetpagedOfferJoinedReport(DateTime DateofOfferStartdate,DateTime DateofOfferEnddate, string  OfferDeclineDateStartdate,string  OfferDeclineDateEnddate, DateTime   ActualDOJStartdate,DateTime   ActualDOJEnddate,string PS, string   CreateDateStartdate,string  CreatedateEnddate, string CreatedBy, string Requisition, string sortExpression, int startRowIndex, int maximumRows)
        {
            PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
            if (DateofOfferStartdate != null && DateofOfferStartdate != DateTime.MinValue) pageRequest.Conditions.Add("OfferDate", "  between '" + DateofOfferStartdate.ToString("MMM dd yyyy") + "' and '" + DateofOfferEnddate.ToString("MMM dd yyyy") + "'");
            if (ActualDOJStartdate != null && (ActualDOJStartdate) != DateTime.MinValue) pageRequest.Conditions.Add("DOJDate", " between '" +  ( ActualDOJStartdate).ToString("MMM dd yyyy") + "' and '" + ( ActualDOJEnddate).ToString("MMM dd yyyy") + "'");
            if (OfferDeclineDateStartdate != null && Convert .ToDateTime ( OfferDeclineDateStartdate) != DateTime.MinValue) pageRequest.Conditions.Add("OfferDeclineDate", " between '" + Convert .ToDateTime ( OfferDeclineDateStartdate).ToString("MMM dd yyyy") + "' and '" +  Convert .ToDateTime ( OfferDeclineDateEnddate).ToString("MMM dd yyyy") + "'");
            if (CreateDateStartdate != null && Convert.ToDateTime(CreateDateStartdate) != DateTime.MinValue) pageRequest.Conditions.Add("CreateDate", " between '" + Convert.ToDateTime(CreateDateStartdate).ToString("MMM dd yyyy") + "' and '" + Convert.ToDateTime(CreatedateEnddate).AddDays(1).ToString("MMM dd yyyy") + "'");
            if (PS != null && PS!=string .Empty ) pageRequest.Conditions.Add("PS", PS);
            if (CreatedBy != null && CreatedBy != string.Empty && CreatedBy != "0") pageRequest.Conditions.Add("CreatedBy", CreatedBy);
            if (Requisition != null && Requisition != string.Empty && Requisition != "0") pageRequest.Conditions.Add("Requisition", Requisition);

            pageResponseOfferJoin = Facade.MemberHiringDetails_GetPagedOfferJoinDetails(pageRequest);
            return pageResponseOfferJoin.Response as List<MemberOfferJoinDetails >;

        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public IList<MemberOfferJoinDetails> GetpagedOfferJoinedReportForCommon(DateTime DateofOfferStartdate, DateTime DateofOfferEnddate, DateTime ActualDOJStartdate, DateTime ActualDOJEnddate, DateTime CreateDateStartdate, DateTime CreateDateEnddate, string CreatedBy, string Requisition, string sortExpression, int startRowIndex, int maximumRows) 
        {
            PagedRequest pagerequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
            if (DateofOfferStartdate != null && DateofOfferStartdate != DateTime.MinValue) pagerequest.Conditions.Add("OfferDate", "between'" + DateofOfferStartdate.ToString("MMM dd yyyy") + "'and '" + DateofOfferEnddate.ToString("MMM dd yyyy") + "'");
            if (ActualDOJStartdate != null && (ActualDOJStartdate) != DateTime.MinValue) pagerequest.Conditions.Add("DOJDate", " between '" + (ActualDOJStartdate).ToString("MMM dd yyyy") + "' and '" + (ActualDOJEnddate).ToString("MMM dd yyyy") + "'");
            if (CreateDateStartdate != null && Convert.ToDateTime(CreateDateStartdate) != DateTime.MinValue) pagerequest.Conditions.Add("CreateDate", " between '" + Convert.ToDateTime(CreateDateStartdate).ToString("MMM dd yyyy") + "' and '" + Convert.ToDateTime(CreateDateEnddate).AddDays(1).ToString("MMM dd yyyy") + "'");
            if (CreatedBy != null && CreatedBy != string.Empty && CreatedBy != "0") pagerequest.Conditions.Add("CreatedBy", CreatedBy);
            if (Requisition != null && Requisition != string.Empty && Requisition != "0") pagerequest.Conditions.Add("Requisition", Requisition);

            pageResponseofferJoinCommon = Facade.MemberHiringDetails_GetPagedOfferJoinDetailsForCommon(pagerequest);
            return pageResponseofferJoinCommon.Response as List<MemberOfferJoinDetails>;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountOfferJoinedReportForCommon(DateTime DateofOfferStartdate, DateTime DateofOfferEnddate, DateTime ActualDOJStartdate, DateTime ActualDOJEnddate, DateTime CreateDateStartdate, DateTime CreateDateEnddate, string CreatedBy, string Requisition)
        {
            return pageResponseofferJoinCommon.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountOfferJoinedReport(DateTime DateofOfferStartdate, DateTime DateofOfferEnddate, string  OfferDeclineDateStartdate, string  OfferDeclineDateEnddate, DateTime ActualDOJStartdate, DateTime ActualDOJEnddate, string PS, string CreateDateStartdate, string CreateDateEnddate, string CreatedBy, string Requisition)
        {
            return pageResponseOfferJoin.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public  IList <MemberHiringDetails > GetpagedEmbeddedSalarydetailsReport(string  DateofOfferStartdate,string  DateofOfferEnddate, string  ActualDOJStartdate,string ActualDOJEnddate, string PS, string  CreateDateStartdate,string  CreateDateEnddate, string CreatedBy, string Requisition, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (DateofOfferStartdate != null && Convert .ToDateTime ( DateofOfferStartdate) != DateTime.MinValue) pageRequest.Conditions.Add("OfferDate", "  between '" + Convert .ToDateTime ( DateofOfferStartdate).ToString("MMM dd yyyy") + "' and '" + Convert .ToDateTime ( DateofOfferEnddate).ToString("MMM dd yyyy") + "'");
                if (ActualDOJStartdate != null && Convert .ToDateTime ( ActualDOJStartdate) != DateTime.MinValue) pageRequest.Conditions.Add("EDOJDate", " between '" + Convert .ToDateTime ( ActualDOJStartdate).ToString("MMM dd yyyy") + "' and '" + Convert .ToDateTime ( ActualDOJEnddate).ToString("MMM dd yyyy") + "'");
                if (CreateDateStartdate != null && Convert .ToDateTime ( CreateDateStartdate) != DateTime .MinValue  ) pageRequest.Conditions.Add("CreateDate", " between '" + Convert .ToDateTime ( CreateDateStartdate).ToString("MMM dd yyyy") + "' and '" + Convert .ToDateTime ( CreateDateEnddate).AddDays (1).ToString("MMM dd yyyy") + "'");
                if (PS != null && PS != "") pageRequest.Conditions.Add("PS", PS);
                if (CreatedBy != null && CreatedBy !=string .Empty && CreatedBy !="0" ) pageRequest.Conditions.Add("CreatedBy", CreatedBy );
                if (Requisition != null && Requisition != string.Empty && Requisition !="0") pageRequest.Conditions.Add("Requisition", Requisition );


                pageResponse = Facade.MemberHiringDetails_GetPagedEmbeddedDetails(pageRequest );
                return pageResponse.Response as List<MemberHiringDetails>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountEmbeddedSalarydetailsReport(string  DateofOfferStartdate, string  DateofOfferEnddate, string  ActualDOJStartdate, string  ActualDOJEnddate, string  PS, string  CreateDateStartdate, string  CreateDateEnddate, string CreatedBy, string Requisition)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public IList<MemberHiringDetails> GetpagedMechSalarydetailsReport(string DateofOfferStartdate, string DateofOfferEnddate, string ActualDOJStartdate, string ActualDOJEnddate, string  PS, string CreateDateStartdate, string CreateDateEnddate, string CreatedBy, string Requisition, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {

                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (DateofOfferStartdate != null && Convert .ToDateTime ( DateofOfferStartdate) != DateTime.MinValue) pageRequest.Conditions.Add("OfferDate", "  between '" +Convert .ToDateTime (  DateofOfferStartdate).ToString("MMM dd yyyy") + "' and '" + Convert .ToDateTime ( DateofOfferEnddate).ToString("MMM dd yyyy") + "'");
                if (ActualDOJStartdate != null && Convert .ToDateTime ( ActualDOJStartdate) != DateTime.MinValue) pageRequest.Conditions.Add("EDOJDate", " between '" + Convert .ToDateTime ( ActualDOJStartdate).ToString("MMM dd yyyy") + "' and '" + Convert .ToDateTime ( ActualDOJEnddate).ToString("MMM dd yyyy") + "'");
                if (CreateDateStartdate != null && Convert.ToDateTime(CreateDateStartdate) != DateTime.MinValue) pageRequest.Conditions.Add("CreateDate", " between '" + Convert.ToDateTime(CreateDateStartdate).ToString("MMM dd yyyy") + "' and '" + Convert.ToDateTime(CreateDateEnddate).AddDays(1).ToString("MMM dd yyyy") + "'");
                if (PS != null && PS !="") pageRequest.Conditions.Add("PS", PS);
                if (CreatedBy != null && CreatedBy != string.Empty && CreatedBy != "0") pageRequest.Conditions.Add("CreatedBy", CreatedBy);
                if (Requisition != null && Requisition != string.Empty && Requisition != "0") pageRequest.Conditions.Add("Requisition", Requisition);


                pageResponse = Facade.MemberHiringDetails_GetPagedMechDetails(pageRequest);
                return pageResponse.Response as List<MemberHiringDetails>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountMechSalarydetailsReport(string  DateofOfferStartdate, string  DateofOfferEnddate, string  ActualDOJStartdate, string  ActualDOJEnddate, string  PS, string  CreateDateStartdate, string  CreateDateEnddate, string CreatedBy, string Requisition)
        {
            return pageResponse.TotalRow;
        }


        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberHiringDetails> GetPaged(int MemberId,string OfferLevelName,string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (MemberId > 0)
                {
                    pageRequest.Conditions.Add("MemberId", MemberId.ToString());
                    pageRequest.Conditions.Add("OfferLevelName", OfferLevelName );
                }
                pageResponse = Facade.GetPaged(pageRequest);
                return pageResponse.Response as List<MemberHiringDetails>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberHiringDetails> GetPaged(string OfferLevelName,string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.Conditions.Add("OfferLevelName", OfferLevelName);
                pageResponse = Facade.GetPaged(pageRequest);
                return pageResponse.Response as List<MemberHiringDetails>;
            }
        }

      

    }
}
