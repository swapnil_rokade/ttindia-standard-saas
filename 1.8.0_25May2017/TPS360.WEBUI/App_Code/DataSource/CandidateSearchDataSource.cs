/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateSearchDataSource.cs
    Description: This page provides datasource for CandidateSearch.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ------------------------------------------------------------------------------------------------------------------------------    
    0.1            Dec-02-2008        Shivanand        Defect #8745; New parameter "state" is added in the method "GetListCount()".
                                                                     New parameter "state" is added in the method "GetPaged()" and used in calling 
                                                                        methods "IsValidParameters()" and method "Facade.GetAllCandidateOnSearch()".
                                                                     New parameter "state" is added in the method "IsValidParameters()".
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel;
using TPS360.BusinessFacade;
using TPS360.Common.Shared;
using TPS360.Common.Utility;
using System.Collections;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web.UI;
using Microsoft.Office.Interop.Outlook;
using System.Timers;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class CandidateSearchDataSource : ObjectDataSourceBase
    {
        PagedResponse<Candidate> pageResponse = null;
        PagedResponse<Candidate> pageResponse1 = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string allKeys, string anyKey, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string state, int maxResults, string applicantType) // 0.1  // 9372
        {
            return pageResponse.TotalRow;
        }

        public int GetListCountForSavedQuery(string whereClause, string memberId, string rowPerPage)
        {
            return pageResponse.TotalRow;
        }

        public int GetListCount(string allKeys, string anyKey, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo,string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string state, int maxResults, string applicantType,string Education,string WorkSchedule,string HiringStatus) 
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        //
        public int GetListCount(string CandidateID, string allKeys, string anyKey, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string state, int maxResults, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddinWhereClaues, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string SourceId, string SortOrder, string SortColumn, string DisplayOption, string NoticePeriod)
        {
            return pageResponse.TotalRow;
        }


        [DataObjectMethod(DataObjectMethodType.Select, true)]

        public List<Candidate> GetPaged(string allKeys, string anyKey, string sortExpression, int startRowIndex, int maximumRows, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string state, int maxResults, string applicantType)  //0.1      // 9372 
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex + 1, rowPerPage == null ? 20 : int.Parse(rowPerPage));
                //custom
                pageResponse = Facade.GetAllCandidateOnSearch(pageRequest, allKeys, anyKey, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, functionalCategory, hiringMatrixId, hotListId, memberId, (rowPerPage==null?"20":rowPerPage ), memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, state, maxResults, applicantType);  // 0.1
                return pageResponse.Response as List<Candidate>;
               // if (IsValidParameters(allKeys, anyKey, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, functionalCategory, hiringMatrixId, hotListId, minExpYr, maxExpYr, state, applicantType)) //0.1
                //{
                //    pageResponse = Facade.GetAllCandidateOnSearch(pageRequest, allKeys, anyKey, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, functionalCategory, hiringMatrixId, hotListId, memberId, rowPerPage, memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, state, maxResults, applicantType);  // 0.1
                //    return pageResponse.Response as List<Candidate>;
                //}
                //else
                //{
                //    return null;
                //}
            }
        }

        public List<Candidate> GetPagedForSavedQuery(string sortExpression, int startRowIndex, int maximumRows, string whereClause, string memberId, string rowPerPage) //Defect #9223
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, rowPerPage == null ? 20 : int.Parse(rowPerPage));
                pageResponse = Facade.GetAllCandidateOnSearchForSavedQuery(pageRequest, whereClause, memberId, rowPerPage);
                return pageResponse.Response as List<Candidate>;
            }
        }
        //custom
        public bool IsValidParameters(string allKeys, string anyKey, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string functionalCategory, string hiringMatrixId, string hotListId, string minExpYr, string maxExpYr, string strState, string applicantType) //0.1
        {
            if (!string.IsNullOrEmpty(allKeys))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(anyKey))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(currentPosition))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(currentCity))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(currentCountryId))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(jobType))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(workStatus))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(securityClearence))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(availability))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(industryType))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(lastUpdateDate))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(salaryType))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(salaryFrom))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(salaryTo))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(functionalCategory))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(hiringMatrixId))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(hotListId))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(minExpYr))
            {
                return true;
            }
            else if (!string.IsNullOrEmpty(maxExpYr))
            {
                return true;
            }

         //0.1 starts

            else if (!string.IsNullOrEmpty(strState))
            {
                return true;
            }
                //custom
            else if (!string.IsNullOrEmpty(applicantType))
            {
                return true;
            }

        //0.1 ends

            else
            {
                return false ;
            }
        }
       
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPaged(string allKeys, string anyKey, string sortExpression, int startRowIndex, int maximumRows, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string state, int maxResults, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus) 
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex + 1, rowPerPage == null ? 20 : int.Parse(rowPerPage));
                pageResponse = Facade.GetAllCandidateOnSearchForPrecise(pageRequest, allKeys, anyKey, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, rowPerPage, memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, state, maxResults, applicantType, Education, WorkSchedule,Gender, Nationality, Industry, JobFunction, HiringStatus, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, true, false, "0", "0", "0", "0");  // 0.1
                return pageResponse.Response as List<Candidate>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPaged_ExpandSorting(string allKeys, string anyKey, string sortExpression, int startRowIndex, int maximumRows, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string state, int maxResults, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddinWhereClaues, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string SortOrder, string DisplayOption)//string allKeys, string anyKey, string sortExpression, int startRowIndex, int maximumRows, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo,string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string state, int maxResults, string applicantType, string Education, string WorkSchedule,string HiringStatus)
        {
            //using (new PerformanceBenchmark())
            //{
            //    PagedRequest pageRequest = base.PreparePagedRequest((allKeys == null || allKeys == "" ? "[C].[FirstName]" : "RANK Desc"), startRowIndex + 1, rowPerPage == null ? 20 : int.Parse(rowPerPage));
            //    pageResponse = Facade.GetAllCandidateOnSearchForPrecise(pageRequest, allKeys, anyKey, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, rowPerPage, memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, state, maxResults, applicantType, Education, WorkSchedule, HiringStatus, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, true, false, "0", "0");  // 0.1
            //    return pageResponse.Response as List<Candidate>;
            //}

            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest((allKeys == null || allKeys == "" ? "[C].[FirstName]" : "RANK Desc"), startRowIndex + 1, rowPerPage == null ? 20 : int.Parse(rowPerPage));
                pageRequest.SortOrder = SortOrder;
                pageResponse = Facade.GetAllCandidateOnSearchForPrecise(pageRequest, allKeys, anyKey, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, rowPerPage, memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, state, maxResults, applicantType, Education, WorkSchedule,Gender, Nationality, Industry, JobFunction, HiringStatus, CurrentCompany, CompanyStatus, CandidateEmail, MobilePhone, ZipCode, Radius, AddinWhereClaues, IsMile, AssignedManagerId, SalarySelector, internalRating,"0");  // 0.1
                return pageResponse.Response as List<Candidate>;
                //pageResponse1 = Facade.GetAllCandidateOnSearchForPrecises(pageRequest, allKeys, anyKey, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, rowPerPage, memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, state, maxResults, applicantType, Education, WorkSchedule, HiringStatus, CurrentCompany, CompanyStatus, CandidateEmail, MobilePhone, ZipCode, Radius, AddinWhereClaues, IsMile);  // 0.1
                //return pageResponse1.Response as List<Candidate>;
            }


        }
        ///  
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPaged_DefaultSorting(string CandidateID, string allKeys, string anyKey, string sortExpression, int startRowIndex, int maximumRows, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string state, int maxResults, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddinWhereClaues, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string SourceId, string SortOrder, string SortColumn, string DisplayOption, string NoticePeriod)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest((allKeys == null || allKeys == "" ? "[C].[FirstName]" : "RANK Desc"), startRowIndex + 1, rowPerPage == null ? 20 : int.Parse(rowPerPage));
                pageRequest.SortOrder = SortOrder;
                pageRequest.SortColumn = SortColumn;

                //added by pravin khot on 8/Aug/2016*************
                bool ShowPreciseSearch = false;
                SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                if (siteSetting != null)
                {                   
                    
                    Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                    string oShowPreciseSearch=siteSettingTable[DefaultSiteSetting.AllowShowPreciseSearchAllCandidates.ToString()].ToString();
                    if (oShowPreciseSearch == "False" || siteSettingTable[DefaultSiteSetting.AllowShowPreciseSearchAllCandidates.ToString()].ToString() == "0" || siteSettingTable[DefaultSiteSetting.AllowShowPreciseSearchAllCandidates.ToString()] == null || siteSettingTable[DefaultSiteSetting.AllowShowPreciseSearchAllCandidates.ToString()] == "")
                    {
                       ShowPreciseSearch = false ;
                    }
                    else
                    {
                        ShowPreciseSearch = true;
                    }
                   
                }
                //****************END*********************************

                //**********Code modify by pravin khot on 8/Aug/2016********Added ShowPreciseSearch************
                pageResponse = Facade.GetAllCandidateOnSearchForPrecises(pageRequest, allKeys, anyKey, currentPosition, currentCity, (currentCountryId.Trim() == "0" ? "" : currentCountryId), jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, rowPerPage, memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, (state.Trim() == "0" ? "" : state), maxResults, applicantType, Education, WorkSchedule, Gender, Nationality, Industry, JobFunction, HiringStatus, CurrentCompany, CompanyStatus, CandidateEmail, MobilePhone, ZipCode, Radius, AddinWhereClaues, IsMile, AssignedManagerId, SalarySelector, internalRating, SourceId, CandidateID, NoticePeriod, ShowPreciseSearch);  // 0.1
                //pageResponse = Facade.GetAllCandidateOnSearchForPrecises(pageRequest, allKeys, anyKey, currentPosition, currentCity, (currentCountryId.Trim() == "0" ? "" : currentCountryId), jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, rowPerPage, memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, (state.Trim() == "0" ? "" : state), maxResults, applicantType, Education, WorkSchedule, Gender, Nationality, Industry, JobFunction, HiringStatus, CurrentCompany, CompanyStatus, CandidateEmail, MobilePhone, ZipCode, Radius, AddinWhereClaues, IsMile, AssignedManagerId, SalarySelector, internalRating, SourceId, CandidateID, NoticePeriod);  // 0.1
                //**********************END**********************************

                return pageResponse.Response as List<Candidate>;
                //pageResponse1 = Facade.GetAllCandidateOnSearchForPrecises(pageRequest, allKeys, anyKey, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, rowPerPage, memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, state, maxResults, applicantType, Education, WorkSchedule, HiringStatus, CurrentCompany, CompanyStatus, CandidateEmail, MobilePhone, ZipCode, Radius, AddinWhereClaues, IsMile);  // 0.1
                //return pageResponse1.Response as List<Candidate>;
            }
        }


        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPaged(string CandidateID, string allKeys, string anyKey, string sortExpression, int startRowIndex, int maximumRows, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string state, int maxResults, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddinWhereClaues, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string SourceId, string SortOrder, string SortColumn, string DisplayOption, string NoticePeriod)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex + 1, maximumRows );
                pageRequest.SortOrder = SortOrder;
                pageRequest.SortColumn = SortColumn;


                //added by pravin khot on 8/Aug/2016*************
                bool ShowPreciseSearch = false;
                SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                if (siteSetting != null)
                {                  
                    Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                    string oShowPreciseSearch=siteSettingTable[DefaultSiteSetting.AllowShowPreciseSearchAllCandidates.ToString()].ToString();
                    if (oShowPreciseSearch == "False" || siteSettingTable[DefaultSiteSetting.AllowShowPreciseSearchAllCandidates.ToString()].ToString() == "false" || siteSettingTable[DefaultSiteSetting.AllowShowPreciseSearchAllCandidates.ToString()].ToString() == "0" || siteSettingTable[DefaultSiteSetting.AllowShowPreciseSearchAllCandidates.ToString()] == null || siteSettingTable[DefaultSiteSetting.AllowShowPreciseSearchAllCandidates.ToString()] == "")
                    {
                        ShowPreciseSearch = false;
                    }
                    else
                    {
                        ShowPreciseSearch = true ;
                    }
                }
                //****************END*********************************

                if ((DisplayOption!=null) && (DisplayOption.ToLower() == "Expand".ToLower()))
                {
                    pageResponse = Facade.GetAllCandidateOnSearchForPrecise(pageRequest, allKeys, anyKey, currentPosition, currentCity, (currentCountryId == "0" ? "" : currentCountryId), jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, maximumRows.ToString(), memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, (state == "0" ? "" : state), maxResults, applicantType, Education, WorkSchedule, Gender, Nationality, Industry, JobFunction, HiringStatus, CurrentCompany, CompanyStatus, CandidateEmail, MobilePhone, ZipCode, Radius, AddinWhereClaues, IsMile, AssignedManagerId, SalarySelector, internalRating, CandidateID);                
                }
                else
                {
                    //**********Code modify by pravin khot on 8/Aug/2016********Added ShowPreciseSearch************
                    pageResponse = Facade.GetAllCandidateOnSearchForPrecises(pageRequest, allKeys, anyKey, currentPosition, currentCity, (currentCountryId.Trim() == "0" ? "" : currentCountryId), jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, rowPerPage, memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, (state.Trim() == "0" ? "" : state), maxResults, applicantType, Education, WorkSchedule, Gender, Nationality, Industry, JobFunction, HiringStatus, CurrentCompany, CompanyStatus, CandidateEmail, MobilePhone, ZipCode, Radius, AddinWhereClaues, IsMile, AssignedManagerId, SalarySelector, internalRating, SourceId, CandidateID, NoticePeriod, ShowPreciseSearch);  // 0.1
                    //pageResponse = Facade.GetAllCandidateOnSearchForPrecises(pageRequest, allKeys, anyKey, currentPosition, currentCity, (currentCountryId == "0" ? "" : currentCountryId), jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, maximumRows.ToString(), memberPositionId, isVolumeHire, isMyList, minExpYr, maxExpYr, (state == "0" ? "" : state), maxResults, applicantType, Education, WorkSchedule, Gender, Nationality, Industry, JobFunction, HiringStatus, CurrentCompany, CompanyStatus, CandidateEmail, MobilePhone, ZipCode, Radius, AddinWhereClaues, IsMile, AssignedManagerId, SalarySelector, internalRating, SourceId, CandidateID, NoticePeriod);
                    //****************END*********************************
                }
                
                return pageResponse.Response as List<Candidate>;
            }
        }
    }
}