using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class CandidateRequisitionStatusDataSource : ObjectDataSourceBase
    {
        PagedResponse<CandidateRequisitionStatus > pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int ManagerId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<CandidateRequisitionStatus > GetPaged(int ManagerId,string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedCandidateREquisitionStatus(pageRequest, ManagerId);
                return pageResponse.Response as List<CandidateRequisitionStatus >;
            }
        }
    }
}