using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberDocumentsDataSource : ObjectDataSourceBase
    {
        PagedResponse<MemberDocument> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount()
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int MemberID, string type)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int MemberID)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int MemberID,string SortColumn,string SortOrder)
        {
            return pageResponse.TotalRow;
        }
      

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MemberDocument  > GetPagedByMemberId(int memberId,string type, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.Conditions.Add("type", type);

                pageResponse = Facade.GetPagedMemberDocumentByMemberID(memberId, pageRequest);
                return pageResponse.Response as List<MemberDocument>;
            }
        }
        public List<MemberDocument> GetPagedByMemberId(int memberId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedMemberDocumentByMemberID(memberId, pageRequest);
                return pageResponse.Response as List<MemberDocument>;
            }
        }
        public List<MemberDocument> GetPagedByMemberId(int memberId,string SortColumn,string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortColumn = SortColumn;
                pageRequest.SortOrder = SortOrder;
                pageResponse = Facade.GetPagedMemberDocumentByMemberID(memberId, pageRequest);
                return pageResponse.Response as List<MemberDocument>;
            }
        }
    }
}