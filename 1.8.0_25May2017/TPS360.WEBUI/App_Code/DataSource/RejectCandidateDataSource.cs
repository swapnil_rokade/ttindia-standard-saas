﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:       RejectCandidateDataSource.cs
    Description:    This is the objects source file
    Created By:     
    Created On:    
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
 *    0.1                14/March/2016     pravin khot          GetPagedReject,GetListCountReject,PagedResponse<Candidate>,PagedResponse<DynamicDictionary>

---------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class RejectCandidateDataSource : ObjectDataSourceBase
    {
        PagedResponse<RejectCandidate> pageResponse = null;
        PagedResponse<Candidate> pageResponse2 = null; //added by pravin khot on 14/March/2016
        PagedResponse<DynamicDictionary> pageResponse1 = null;//added by pravin khot on 14/March/2016
        [DataObjectMethod(DataObjectMethodType.Select, false)]

        public int GetListCount(int JobPostingId,string SortOrder)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<RejectCandidate> GetPaged(int JobPostingId,string SortOrder,string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                if (JobPostingId > 0)
                {
                    pageRequest.Conditions.Add("JobPostingId", JobPostingId.ToString());
                }

                pageResponse = Facade.GetPagedForRejectCandidate(pageRequest);
                return pageResponse.Response as List<RejectCandidate>;
            }
        }
        //**********************Code added by pravin khot on 14/March/2016************START*******

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountReject(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, 
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string JobPostingId, string SortOrder)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<RejectCandidate> GetPagedReject(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string JobPostingId, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                pageResponse = Facade.GetPagedCandidateReject(pageRequest, addedFrom, addedTo, addedBy, addedBySource, 
            country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
           gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, JobPostingId);
                return pageResponse.Response as List<RejectCandidate>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<DynamicDictionary> GetPagedReject(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, 
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string JobPostingId, string sortExpression, int startRowIndex, int maximumRows, IList<string> CheckedList)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse1 = Facade.GetPagedRejectCandidateForSelectedColumn(pageRequest, addedFrom, addedTo, addedBy, addedBySource,
                                country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
                                gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, JobPostingId, CheckedList);
                return pageResponse1.Response as List<DynamicDictionary>;
            }
        }
        //*************************************End***********************************************
    }
}
