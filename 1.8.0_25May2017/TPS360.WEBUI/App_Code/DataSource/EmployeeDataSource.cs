/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmployeeDataSource.cs
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
     0.1           23/Feb/2016         pravin khot         1.PagedResponse<UserRoleMapEditor> pagedUserRoleMapEditor ,GetCountUserRoleMapEditor,GetPagedUserRoleMapEditor
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class EmployeeDataSource : ObjectDataSourceBase
    {
        PagedResponse<Employee> pageResponse = null;
        PagedResponse <EmployeeProductivity > pageEmployee=null;
        PagedResponse<UserRoleMapEditor> pagedUserRoleMapEditor = null;//added by pravin khot on 23/Feb/2016
        PagedResponse<EmployeeTeamBuilder> pagedEmployeeTeamBuilder = null;
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string quickSearchKeyWord)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string quickSearchKeyWord,string CreatorId)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string quickSearchKeyWord, string CreatorId,string SortOrder)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int managerId, int organizationFunctionalityCategoryMapId, int tierLookupId, int branchOfficeId, int status)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int managerId, int organizationFunctionalityCategoryMapId, int tierLookupId, int branchOfficeId, int status,string SortOrder)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(DateTime startDate, DateTime endDate, int employeeId, int status)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(DateTime addedFrom, DateTime addedTo, int addedBy, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, int industry, int functionalCategory, int workPermit,
            int gender, int maritalStatus, int educationId, int assignedManager)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountEmployeeProductivity(DateTime  StartDate,DateTime EndDate, string  MemberId, string SortOrder)
        {
            return pageEmployee.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountEmployeeProductivityReportByEmployee(string StartDate, string EndDate, string TeamList, string User,string SortOrder)
        {
            return pageEmployee.TotalRow;
        }

        public int GetListCountEmployeeProductivityReportByTeam(string StartDate, string EndDate, string TeamList, string User,string SortOrder) 
        {
            return pageEmployee.TotalRow;
        }



        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Employee> GetPaged(int managerId, int organizationFunctionalityCategoryMapId, int tierLookupId, int branchOfficeId, int status, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (managerId > 0)
                {
                    pageRequest.Conditions.Add("memberId", managerId.ToString());
                }

                if (organizationFunctionalityCategoryMapId > 0)
                {
                    pageRequest.Conditions.Add("organizationFunctionalityCategoryMapId", organizationFunctionalityCategoryMapId.ToString());
                }

                if (tierLookupId > 0)
                {
                    pageRequest.Conditions.Add("tierLookupId", tierLookupId.ToString());
                }

                if (branchOfficeId > 0)
                {
                    pageRequest.Conditions.Add("branchOfficeId", branchOfficeId.ToString());
                }

                if (status > 0)
                {
                    pageRequest.Conditions.Add("status", status.ToString());
                }

                pageResponse = Facade.GetPagedEmployee(pageRequest);
                return pageResponse.Response as List<Employee>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Employee> GetPaged(int managerId, int organizationFunctionalityCategoryMapId, int tierLookupId, int branchOfficeId, int status, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                if (managerId > 0)
                {
                    pageRequest.Conditions.Add("memberId", managerId.ToString());
                }

                if (organizationFunctionalityCategoryMapId > 0)
                {
                    pageRequest.Conditions.Add("organizationFunctionalityCategoryMapId", organizationFunctionalityCategoryMapId.ToString());
                }

                if (tierLookupId > 0)
                {
                    pageRequest.Conditions.Add("tierLookupId", tierLookupId.ToString());
                }

                if (branchOfficeId > 0)
                {
                    pageRequest.Conditions.Add("branchOfficeId", branchOfficeId.ToString());
                }

                if (status > 0)
                {
                    pageRequest.Conditions.Add("status", status.ToString());
                }

                pageResponse = Facade.GetPagedEmployee(pageRequest);
                return pageResponse.Response as List<Employee>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Employee> GetPaged(DateTime startDate,DateTime endDate,int employeeId, int status, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (startDate != null && startDate!=DateTime.MinValue)
                {
                    pageRequest.Conditions.Add("startDate", startDate.ToString());
                }

                if (endDate != null && endDate != DateTime.MinValue)
                {
                    pageRequest.Conditions.Add("endDate", endDate.ToString());
                }

                if (status > 0)
                {
                    pageRequest.Conditions.Add("status", status.ToString());
                }

                if (employeeId > 0)
                {
                    pageRequest.Conditions.Add("employeeId", employeeId.ToString());
                }

                pageResponse = Facade.GetPagedEmployeeReport(pageRequest);
                return pageResponse.Response as List<Employee>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Employee> GetPaged(DateTime addedFrom, DateTime addedTo, int addedBy, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, int industry, int functionalCategory, int workPermit,
            int gender, int maritalStatus, int educationId, int assignedManager, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedEmployee(pageRequest, addedFrom, addedTo, addedBy, updatedFrom, updatedTo, updatedBy,
           country, state, city, industry, functionalCategory, workPermit,
           gender, maritalStatus, educationId, assignedManager);
                return pageResponse.Response as List<Employee>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Employee> GetPagedForQuickSearch(string quickSearchKeyWord, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (!string.IsNullOrEmpty(quickSearchKeyWord))
                {
                    pageRequest.Conditions.Add("quickSearchKeyWord", quickSearchKeyWord);
                }
                pageResponse = Facade.GetPagedEmployee(pageRequest);
                return pageResponse.Response as List<Employee>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Employee> GetPagedForQuickSearch(string quickSearchKeyWord, string CreatorId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (!string.IsNullOrEmpty(quickSearchKeyWord))
                {
                    pageRequest.Conditions.Add("quickSearchKeyWord", quickSearchKeyWord);
                }
                if (!string.IsNullOrEmpty(CreatorId))
                {
                    pageRequest.Conditions.Add("CreatorId", CreatorId);
                }
                pageResponse = Facade.GetPagedEmployee(pageRequest);
                return pageResponse.Response as List<Employee>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Employee> GetPagedForQuickSearch(string quickSearchKeyWord, string CreatorId,string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                if (!string.IsNullOrEmpty(quickSearchKeyWord))
                {
                    pageRequest.Conditions.Add("quickSearchKeyWord", quickSearchKeyWord);
                }
                if (!string.IsNullOrEmpty(CreatorId))
                {
                    pageRequest.Conditions.Add("CreatorId", CreatorId);
                }
                pageResponse = Facade.GetPagedEmployee(pageRequest);
                return pageResponse.Response as List<Employee>;
            }
        }


        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<EmployeeProductivity> GetPagedEmployeeProductivity(DateTime  StartDate,DateTime EndDate, string MemberId, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                if (StartDate == DateTime.MinValue) { StartDate = DateTime.Now.AddDays (-6); EndDate = DateTime.Now; }
                pageEmployee = Facade.GetPagedEmployeeProductivity(StartDate ,EndDate  , (MemberId==null || MemberId ==""?0 : Convert .ToInt32 (MemberId )), pageRequest);
                return pageEmployee.Response as List<EmployeeProductivity>;
            }
        }


        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public IList<EmployeeTeamBuilder> GetPagedEmployeeTeam(string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                pagedEmployeeTeamBuilder = Facade.GetPagedEmployeeTeamBuilder (pageRequest );//(StartDate, EndDate, (MemberId == null || MemberId == "" ? 0 : Convert.ToInt32(MemberId)), pageRequest);
                return pagedEmployeeTeamBuilder.Response as List<EmployeeTeamBuilder>;
                return null;// pageEmployee.Response as List<EmployeeProductivity>;
            }
        
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public int GetListCountEmployeeTeam(string SortOrder)
        {
           return  pagedEmployeeTeamBuilder.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<EmployeeProductivity> GetPagedEmployeeProductivityReportByEmployee(string StartDate, string EndDate, string TeamList, string User, string SortOrder,string sortExpression, int startRowIndex, int maximumRows) 
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                pageRequest.Conditions.Add("EndDate", EndDate);
                pageRequest.Conditions.Add("StartDate", StartDate);
              
                if (TeamList !=null && TeamList !="" && TeamList != "0") pageRequest.Conditions.Add("TeamID", TeamList);
                if (User !=null && User !="" && User != "0") pageRequest.Conditions.Add("MemberId",User );
                pageEmployee = Facade.GetPagedByProductityReport(pageRequest);
                return pageEmployee.Response as List<EmployeeProductivity>;
            
            }
        
        }


        public List<EmployeeProductivity> GetPagedEmployeeProductivityReportByTeam(string StartDate, string EndDate, string TeamList, string User,string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                pageRequest.Conditions.Add("EndDate", EndDate);
                pageRequest.Conditions.Add("StartDate", StartDate);
                if ( TeamList !=null && TeamList !="" && TeamList != "0") pageRequest.Conditions.Add("TeamID", TeamList);
                if (User != null && User != "0") pageRequest.Conditions.Add("MemberId", User);
                pageEmployee = Facade.GetPagedByTeamProductityReport(pageRequest);
                return pageEmployee.Response as List<EmployeeProductivity>;

            }

        }

        //*********Code added by pravin khot on 24/Feb/2016*****START******************
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public int GetCountUserRoleMapEditor(string sortOrder)
        {
            return pagedUserRoleMapEditor.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public IList<UserRoleMapEditor> GetPagedUserRoleMapEditor(string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                pagedUserRoleMapEditor = Facade.UserRoleMapEditor_GetPaged(pageRequest);
                return pagedUserRoleMapEditor.Response as IList<UserRoleMapEditor>;
                return null;
            }
        }
        //**********************END***********************************

        //*********Code added by Sumit Sonawane on 20/May/2016*****START******************
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<EmployeeProductivity> GetPagedEmployeeProductivityReportByEmployee(string StartDate, string EndDate, string TeamList, string User, bool IsTeamProductivityReport, int TeamLeaderid, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                pageRequest.Conditions.Add("EndDate", EndDate);
                pageRequest.Conditions.Add("StartDate", StartDate);

                if (TeamList != null && TeamList != "" && TeamList != "0") pageRequest.Conditions.Add("TeamID", TeamList);
                if (User != null && User != "" && User != "0") pageRequest.Conditions.Add("MemberId", User);
                if (IsTeamProductivityReport) pageRequest.Conditions.Add("TeamReport", TeamLeaderid.ToString());

                pageEmployee = Facade.GetPagedByProductityReport(pageRequest);
                return pageEmployee.Response as List<EmployeeProductivity>;

            }

        }


        public List<EmployeeProductivity> GetPagedEmployeeProductivityReportByTeam(string StartDate, string EndDate, string TeamList, bool IsMyProcuvtivityReport, bool IsTeamProductivityReport, string TeamLeaderid, string User, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                pageRequest.Conditions.Add("EndDate", EndDate);
                pageRequest.Conditions.Add("StartDate", StartDate);
                if (TeamList != null && TeamList != "" && TeamList != "0") pageRequest.Conditions.Add("TeamID", TeamList);
                if (User != null && User != "0") pageRequest.Conditions.Add("MemberId", User);
                if (IsMyProcuvtivityReport)
                    pageRequest.Conditions.Add("IsMyProductivityReport", User);
                if (IsTeamProductivityReport)
                    pageRequest.Conditions.Add("TeamReport", TeamLeaderid);
                pageEmployee = Facade.GetPagedByTeamProductityReport(pageRequest);
                return pageEmployee.Response as List<EmployeeProductivity>;

            }

        }
        ///////////////////////////////////////////////////////////////////////////////////////

        /////////////Code Added by Sumit Sonawane on 20/May/2016/////////////////////////
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountEmployeeProductivityReportByTeam(string StartDate, string EndDate, string TeamList, string User, string SortOrder, bool IsMyProcuvtivityReport, bool IsTeamProductivityReport, int TeamLeaderid)
        {
            return pageEmployee.TotalRow;
        }
        ///////////////////////////////////////////////////////////////////////////////////////
        /////////////Code Added by Sumit Sonawane on 20/May/2016/////////////////////////
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountEmployeeProductivityReportByEmployee(string StartDate, string EndDate, string TeamList, string User, string SortOrder, bool IsTeamProductivityReport, int TeamLeaderid)
        {
            return pageEmployee.TotalRow;
        }
        ////////////////////////////end/////////////////////////////////////////////////

    }
}