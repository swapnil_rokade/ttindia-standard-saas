/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:       QuestionBankDataSource.cs
    Description: This is the objects source file
    Created By:     Prasanth Kumar G
    Created On:     10/Oct/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
      
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class QuestionBankDataSource : ObjectDataSourceBase
    {
        public IList<QuestionBank> GetAllQuestionBank(string sortExpression)
        {
            IList<QuestionBank> QuestionBanks = Facade.QuestionBank_GetAll(sortExpression);

            return QuestionBanks;

        }

        //public IList<QuestionBank> GetAllByCompanyID(int companyId, string sortExpression)
        //{
        //    IList<QuestionBank> QuestionBanks = Facade.GetAllQuestionBankByCompanyId(companyId, sortExpression);

        //    return QuestionBanks;

        //}
        //public IList<QuestionBank> GetAllByProjectID(int projectId, string sortExpression)//0.1
        //{
            

        //    IList<QuestionBank> QuestionBanks = Facade.GetAllQuestionBankByProjectId(projectId, sortExpression);//0.1 

        //    return QuestionBanks;
        //}

        public string  AppendSortParameter(string sortExpression)
        {
            string strOutput = "";
            if (!string.IsNullOrEmpty(sortExpression))
            {
                string[] part = sortExpression.Split(' ');

                if (part != null && part.Length > 0)
                {
                    strOutput = part[0];

                    if (part.Length > 1 && part[1] != null)
                    {
                        strOutput = part[1];
                    }
                    else
                    {
                        strOutput = UIConstants.SORT_ORDER_ASC;
                    }
                }
            }
            return strOutput;
        }

    }
}