﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class MemberAttendeceYealyReportDataSource : ObjectDataSourceBase
    {
        public IList<MemberAttendenceYearlyReport> GetAttendenceReport(Int32 year, Int32 memberId)
        {
            using (new PerformanceBenchmark())
            {
                return Facade.GetMemberAttendenceByYearlyReport(year, memberId);
            }
        }

        public MemberAttendeceYealyReportDataSource()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}
