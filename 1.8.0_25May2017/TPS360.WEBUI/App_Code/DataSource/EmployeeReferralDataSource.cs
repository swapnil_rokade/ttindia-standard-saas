﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

/// <summary>
/// Summary description for EventLogDataSource
/// </summary>
namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class EmployeeReferralDataSource: ObjectDataSourceBase
    {
        PagedResponse<TPS360 .Common .BusinessEntities . EmployeeReferral> pageResponse = null;
        PagedResponse<TPS360.Common.BusinessEntities.ListWithCount > pageResponseListWithCount = null;


        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount_ForDetail(int JobPostingId, int ReferrerID, DateTime  StartDate, DateTime  EndDate)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount_ForSummary(int JobPostingId, int ReferrerID, DateTime StartDate, DateTime EndDate,string Type,string SortOrder)
        {
            return pageResponseListWithCount.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<ListWithCount > GetPaged_ForSummary(int JobPostingId, int ReferrerID, DateTime StartDate, DateTime EndDate, string Type, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                if (JobPostingId > 0)
                {
                    pageRequest.Conditions.Add("JobPostingID", JobPostingId.ToString());
                }
                if (ReferrerID > 0)
                {
                    pageRequest.Conditions.Add("RefererId", ReferrerID.ToString());
                }
                if (StartDate != DateTime.MinValue)
                    pageRequest.Conditions.Add("AddedFrom", StartDate != null ? Convert.ToDateTime(StartDate.ToString()).ToString("yyyyMMdd") : DateTime.MinValue.ToString());
                if (EndDate != DateTime.MinValue) pageRequest.Conditions.Add("AddedTo", EndDate != null ? Convert.ToDateTime(EndDate.ToString()).ToString("yyyyMMdd") : DateTime.MinValue.ToString());
                pageResponseListWithCount = Facade.EmployeeReferral_GetPagedSummary (pageRequest,Type );
                return pageResponseListWithCount.Response as List<ListWithCount >;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<EmployeeReferral> GetPaged_ForDetail(int JobPostingId, int ReferrerID, DateTime StartDate, DateTime  EndDate, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (JobPostingId > 0)
                {
                    pageRequest.Conditions.Add("JobPostingID", JobPostingId.ToString());
                }
                if (ReferrerID > 0)
                {
                    pageRequest.Conditions.Add("RefererId", ReferrerID.ToString());
                }
                if(StartDate !=DateTime .MinValue )
                    pageRequest.Conditions.Add("AddedFrom", StartDate != null ? Convert.ToDateTime(StartDate.ToString()).ToString("yyyyMMdd") : DateTime.MinValue.ToString());
                if (EndDate != DateTime.MinValue) pageRequest.Conditions.Add("AddedTo", EndDate != null ? Convert.ToDateTime(EndDate.ToString()).ToString("yyyyMMdd") : DateTime.MinValue.ToString());
                pageResponse = Facade.GetPagedEmployeeReferral(pageRequest);
                return pageResponse.Response as List<EmployeeReferral>;
            }
        }
    }

}
