/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:       CandidateDataSource.cs
    Description:    This is the objects source file
    Created By:     
    Created On:    
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
      0.1                12/Jan/2016       pravin khot          GetPagedCareerPortalSubmissionsList,GetListCountCareerPortalSubmissionsList
 *    0.2                4/March/2016      pravin khot          GetPagedCandidateCDMDetailsRpt,GetListCountCandidateCDMDetailsRpt
 *    0.3                20/May/2016       Sumit Sonawane       GetPaged
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/



using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class CandidateDataSource : ObjectDataSourceBase
    {
        PagedResponse<Candidate> pageResponse = null;
        PagedResponse<DynamicDictionary> pageResponse1 = null;
        PagedResponse<CandidateSourcingInfo> pageResponse2 = null;
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCounts(string quickSearchKeyWord)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCounts(string quickSearchKeyWord,string CreatorId)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCounts(string quickSearchKeyWord, string CreatorId,string SortOrder)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int memberId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]

         public int GetSubmittedCandidateListCount(int memberId, int selectionStep)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string creatorId, string companyId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int memberId, bool recentApplicant)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int memberId, DateTime DateFrom, DateTime DateTo, string SortOrder,bool IsVendor,bool IsVendorContact)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int memberId, int hotListManagerId, string SortOrder)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int memberId, int hotListManagerId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit,int  WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager)
        {
            return pageResponse.TotalRow;
        }


        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager,string SortOrder)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountForCandidateSourcingRpt(string SourcedDateFrom, string SourcedDateTo, string SubmissionFrom, string SubmissionTo, string OfferedFrom, string OfferedTo, string JoinedFrom, string JoinedTo,
            string ByRequisition, string ByAccount, string ByTopLevelStatus, string BySubLevelStatus, string ByRecruiter, string ByLead, string ByCandidateName, string ByLocation, string CompanyStatus)
        {
            return pageResponse2.TotalRow;
        }


        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<CandidateSourcingInfo> GetPagedForCandidateSourcingRpt(string SourcedDateFrom, string SourcedDateTo, string SubmissionFrom, string SubmissionTo, string OfferedFrom, string OfferedTo,
            string JoinedFrom, string JoinedTo,string ByRequisition, string ByAccount, string ByTopLevelStatus, string BySubLevelStatus, string ByRecruiter, string ByLead, string ByCandidateName,
            string ByLocation, string CompanyStatus, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.Conditions.Add("SourcedDateFrom", SourcedDateFrom != null ? Convert.ToDateTime(SourcedDateFrom.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("SourcedDateTo", SourcedDateTo != null ? Convert.ToDateTime(SourcedDateTo.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("SubmissionFrom", SubmissionFrom != null ? Convert.ToDateTime(SubmissionFrom.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("SubmissionTo", SubmissionTo != null ? Convert.ToDateTime(SubmissionTo.ToString()).ToString() : DateTime.MinValue.ToString());

                pageRequest.Conditions.Add("OfferedFrom", OfferedFrom != null ? Convert.ToDateTime(OfferedFrom.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("OfferedTo", OfferedTo != null ? Convert.ToDateTime(OfferedTo.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("JoinedFrom", JoinedFrom != null ? Convert.ToDateTime(JoinedFrom.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("JoinedTo", JoinedTo != null ? Convert.ToDateTime(JoinedTo.ToString()).ToString() : DateTime.MinValue.ToString());
               
                if (!string.IsNullOrEmpty(ByRequisition))
                {
                    pageRequest.Conditions.Add("ByRequisition", ByRequisition.ToString());
                }
                if (!string.IsNullOrEmpty(ByAccount))
                {
                    pageRequest.Conditions.Add("ByAccount", ByAccount);
                }
                if (!string.IsNullOrEmpty(ByTopLevelStatus))
                {
                    pageRequest.Conditions.Add("ByTopLevelStatus", ByTopLevelStatus);
                }
                if (!string.IsNullOrEmpty(BySubLevelStatus))
                {
                    pageRequest.Conditions.Add("BySubLevelStatus", BySubLevelStatus);
                }
                if (!string.IsNullOrEmpty(ByRecruiter))
                {
                    pageRequest.Conditions.Add("ByRecruiter", ByRecruiter);
                }
                if (!string.IsNullOrEmpty(ByLead))
                {
                    pageRequest.Conditions.Add("ByLead", ByLead);
                }

                if (!string.IsNullOrEmpty(ByCandidateName))
                {
                    pageRequest.Conditions.Add("ByCandidateName", ByCandidateName);
                }
                if (!string.IsNullOrEmpty(ByLocation))
                {
                    pageRequest.Conditions.Add("ByLocation", ByLocation);
                }
                if (!string.IsNullOrEmpty(CompanyStatus))
                {
                    pageRequest.Conditions.Add("CompanyStatus", CompanyStatus);
                }

                pageResponse2 = Facade.GetPagedForCandidateSourcingReport(pageRequest);
                return pageResponse2.Response as List<CandidateSourcingInfo>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPaged(int memberId, bool recentApplicant, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (memberId > 0)
                {
                    pageRequest.Conditions.Add("memberId", memberId.ToString());
                }
                if (recentApplicant)
                {
                    pageRequest.Conditions.Add("recentApplicant", recentApplicant.ToString());
                }
                pageResponse = Facade.GetPagedCandidate(pageRequest);
                return pageResponse.Response as List<Candidate>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPaged(int memberId, DateTime DateFrom, DateTime DateTo, string SortOrder, bool IsVendor, bool IsVendorContact, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder  = SortOrder;
                if (memberId > 0  )
                {
                    pageRequest.Conditions.Add((IsVendor ?"CompanyId":( IsVendorContact ? "CreatorId":  "memberId")), memberId.ToString());
                }

                if (DateFrom != DateTime.MinValue) pageRequest.Conditions.Add("DateFrom", DateFrom.ToString("yyyyMMdd"));
                if (DateTo != DateTime.MinValue) pageRequest.Conditions.Add("DateTo", DateTo.AddDays (1).ToString("yyyyMMdd"));
                pageResponse = Facade.GetPagedCandidate(pageRequest);
                return pageResponse.Response as List<Candidate>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPagedForQuickSearch(string quickSearchKeyWord, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (!string.IsNullOrEmpty(quickSearchKeyWord))
                {
                    pageRequest.Conditions.Add("quickSearchKeyWord", quickSearchKeyWord);
                }
                pageResponse = Facade.GetPagedCandidate(pageRequest);
                return pageResponse.Response as List<Candidate>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPagedForQuickSearch(string quickSearchKeyWord,string CreatorId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (!string.IsNullOrEmpty(quickSearchKeyWord))
                {
                    pageRequest.Conditions.Add("quickSearchKeyWord", quickSearchKeyWord);
                }
                if (!string.IsNullOrEmpty(CreatorId))
                {
                    pageRequest.Conditions.Add("CreatorId", CreatorId);
                }
                pageResponse = Facade.GetPagedCandidate(pageRequest);
                return pageResponse.Response as List<Candidate>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPagedForQuickSearch(string quickSearchKeyWord, string CreatorId,string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                if (!string.IsNullOrEmpty(quickSearchKeyWord))
                {
                    pageRequest.Conditions.Add("quickSearchKeyWord", quickSearchKeyWord);
                }
                if (!string.IsNullOrEmpty(CreatorId))
                {
                    pageRequest.Conditions.Add("CreatorId", CreatorId);
                }
                pageResponse = Facade.GetPagedCandidate(pageRequest);
                return pageResponse.Response as List<Candidate>;
            }
        }


        //submitted candidate
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPagedForSubmittedCandidate(int memberId, int selectionStep, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (memberId > 0)
                {
                    pageRequest.Conditions.Add("memberId", memberId.ToString());
                }
                if (selectionStep > 0)
                {
                    pageRequest.Conditions.Add("selectionStep", selectionStep.ToString());
                }
                pageResponse = Facade.GetPagedCandidateForSubmittedCandidate(pageRequest);
                return pageResponse.Response as List<Candidate>;
            }
        }
        
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPaged(string creatorId, string companyId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (creatorId != "0")
                {
                    pageRequest.Conditions.Add("CreatorId", creatorId.ToString());
                }

                if (companyId != "0")
                {
                    pageRequest.Conditions.Add("CompanyId", companyId.ToString());
                }

                pageResponse = Facade.GetPagedCandidate(pageRequest);
                return pageResponse.Response as List<Candidate>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPaged(int memberId, int hotListManagerId, string sortExpression, int startRowIndex, int maximumRows)
        {
           
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
              
                if (memberId > 0)
                {
                    pageRequest.Conditions.Add("memberId", memberId.ToString());
                }

                if (hotListManagerId > 0)
                {
                    pageRequest.Conditions.Add("hotListManagerId", hotListManagerId.ToString());
                }
                pageResponse = Facade.GetPagedCandidate(pageRequest);
                return pageResponse.Response as List<Candidate>;
            }
        }


        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPaged(int memberId, int hotListManagerId,string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                if (memberId > 0)
                {
                    pageRequest.Conditions.Add("memberId", memberId.ToString());
                }

                if (hotListManagerId > 0)
                {
                    pageRequest.Conditions.Add("hotListManagerId", hotListManagerId.ToString());
                }
                pageResponse = Facade.GetPagedCandidate(pageRequest);
                return pageResponse.Response as List<Candidate>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<DynamicDictionary> GetPaged(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string sortExpression, int startRowIndex, int maximumRows, IList<string> CheckedList)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse1 = Facade.GetPagedCandidateForSelectedColumn(pageRequest, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
                                country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
                                gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, CheckedList);
                return pageResponse1.Response as List<DynamicDictionary>;
            }
        }


        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPaged(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedCandidate(pageRequest, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
            country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
           gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager);
                return pageResponse.Response as List<Candidate>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPaged(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager,string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                pageResponse = Facade.GetPagedCandidate(pageRequest, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
            country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
           gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager);
                return pageResponse.Response as List<Candidate>;
            }
        }



        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPagedCandidateList(string  CandidateID,int memberId, string CandidateName,string CandidateEmail, string Position, string AssignedManager, string HiringLevel,string MobilePhone, string City, string StateId, string CountryId, string CandidateType,string SourceID,string SortColumn, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                pageRequest.SortColumn = SortColumn;
                if (memberId > 0)
                {
                    pageRequest.Conditions.Add("memberId", memberId.ToString());
                   
                }
                if(CandidateID !=null && CandidateID !="" && CandidateID !="0") pageRequest.Conditions.Add("CandidateID", CandidateID);
                pageRequest.Conditions.Add("CandidateName", CandidateName==null?"":MiscUtil .RemoveScript ( CandidateName.ToString()));
                pageRequest.Conditions.Add("CandidateEmail", CandidateEmail == null ? string.Empty : MiscUtil.RemoveScript(CandidateEmail.ToString()));
                pageRequest.Conditions.Add("Position", Position==null?"":MiscUtil .RemoveScript ( Position.ToString()));
                pageRequest.Conditions.Add("AssignedManager", AssignedManager == null || AssignedManager ==""? "0" : AssignedManager.ToString());
                pageRequest.Conditions.Add("HiringLevel",HiringLevel==null || HiringLevel=="" ?"0": HiringLevel.ToString());
                pageRequest .Conditions .Add ("MobilePhone",MobilePhone ==null ? string .Empty : MobilePhone .ToString ());
                pageRequest.Conditions.Add("City", City==null?"":MiscUtil .RemoveScript ( City));
                pageRequest.Conditions.Add("StateId", StateId == null || StateId == "" ? "0" : StateId);
                pageRequest.Conditions.Add("CountryId", CountryId == null || CountryId==""?"0":CountryId);
                pageRequest.Conditions.Add("CandidateType", CandidateType == null || CandidateType == "" ? "0" : CandidateType);
                if (SourceID != null && SourceID != "") pageRequest.Conditions.Add("SourceId", SourceID);
                pageResponse = Facade.GetPagedCandidate(pageRequest);
                return pageResponse.Response as List<Candidate>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountCandidateList(string CandidateID, int memberId, string CandidateName, string CandidateEmail, string Position, string AssignedManager, string HiringLevel, string MobilePhone, string City, string StateId, string CountryId, string CandidateType, string SourceID, string SortOrder, string SortColumn)
        {
            return pageResponse.TotalRow;
        }
        //*******************************ADDED BY PRAVIN KHOT ON 12/Jan/2016**************************
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPagedCareerPortalSubmissionsList(string CandidateID, int memberId, string CandidateName, string CandidateEmail, string Position, string City, string StateId, string CountryId, string CandidateType, string endClients, string JobTitle, string ReqCode, string JobPostingFromDate, string JobPostingToDate, string SortColumn, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                pageRequest.SortColumn = SortColumn;
                if (memberId > 0)
                {
                    pageRequest.Conditions.Add("memberId", memberId.ToString());

                }
                if (CandidateID != null && CandidateID != "" && CandidateID != "0") pageRequest.Conditions.Add("CandidateID", CandidateID);
                pageRequest.Conditions.Add("CandidateName", CandidateName == null ? "" : MiscUtil.RemoveScript(CandidateName.ToString()));
                pageRequest.Conditions.Add("CandidateEmail", CandidateEmail == null ? string.Empty : MiscUtil.RemoveScript(CandidateEmail.ToString()));
                pageRequest.Conditions.Add("Position", Position == null ? "" : MiscUtil.RemoveScript(Position.ToString()));

                pageRequest.Conditions.Add("City", City == null ? "" : MiscUtil.RemoveScript(City));
                pageRequest.Conditions.Add("StateId", StateId == null || StateId == "" ? "0" : StateId);
                pageRequest.Conditions.Add("CountryId", CountryId == null || CountryId == "" ? "0" : CountryId);
                pageRequest.Conditions.Add("CandidateType", CandidateType == null || CandidateType == "" ? "0" : CandidateType);
                pageRequest.Conditions.Add("Client", endClients);
                pageRequest.Conditions.Add("JobTitle", MiscUtil.RemoveScript(JobTitle));
                pageRequest.Conditions.Add("ReqCode", MiscUtil.RemoveScript(ReqCode));
                pageRequest.Conditions.Add("JobPostingFromDate", JobPostingFromDate == null || JobPostingFromDate == "" ? DateTime.MinValue.ToString() : JobPostingFromDate);
                pageRequest.Conditions.Add("JobPostingToDate", JobPostingToDate == null || JobPostingToDate == "" ? DateTime.MinValue.ToString() : JobPostingToDate);

                pageResponse = Facade.GetPagedCareerPortalSubmissionsList(pageRequest);
                return pageResponse.Response as List<Candidate>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountCareerPortalSubmissionsList(string CandidateID, int memberId, string CandidateName, string CandidateEmail, string Position, string City, string StateId, string CountryId, string CandidateType, string endClients, string JobTitle, string ReqCode, string JobPostingFromDate, string JobPostingToDate, string SortOrder, string SortColumn)
        {
            return pageResponse.TotalRow;
        }

        // *****************************************END*************************************

        //**********************Code added by pravin khot on 4/March/2016************START*******

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountCandidateCDMDetailsRpt(string SourcedDateFrom, string SourcedDateTo)
        {
            return pageResponse.TotalRow;
        }
        
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPagedCandidateCDMDetailsRpt(string SourcedDateFrom, string SourcedDateTo,string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.Conditions.Add("SourcedDateFrom", SourcedDateFrom != null ? Convert.ToDateTime(SourcedDateFrom.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("SourcedDateTo", SourcedDateTo != null ? Convert.ToDateTime(SourcedDateTo.ToString()).ToString() : DateTime.MinValue.ToString());

                pageResponse = Facade.GetPagedCandidateCDMDetailsReport(pageRequest);
                return pageResponse.Response as List<Candidate>;
            }
        }

        //*************************************End***********************************************

        //////////////Code Added by Sumit Sonawane on 20/May/2016/////////////////////////

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPaged(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom,
            DateTime updatedTo, int updatedBy, int country, int state, string city, DateTime interviewFrom, DateTime interviewTo,
            int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants,
            bool hasJobAgent, int assignedManager, bool IsTeamReport, string TeamMembersId, int TeamId, string SortOrder, string sortExpression,
            int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                pageResponse = Facade.GetPagedCandidate(pageRequest, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
                                country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
                                gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, IsTeamReport, TeamMembersId, TeamId);

                return pageResponse.Response as List<Candidate>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, bool IsTeamReport, string TeamMembersId, int TeamId, string SortOrder)
        {
            return pageResponse.TotalRow;
        }

        //****************Code added by Sumit Sonawane on 20/May/2016*******************

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<DynamicDictionary> GetPaged(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string sortExpression, int startRowIndex, int maximumRows, IList<string> CheckedList, bool IsTeamReport, string TeamMembersId, int TeamId)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse1 = Facade.GetPagedCandidateForSelectedColumn(pageRequest, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
            country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
           gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, CheckedList, IsTeamReport, TeamMembersId, TeamId);
                return pageResponse1.Response as List<DynamicDictionary>;
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Candidate> GetPagedCandidatePerformance(int memberId, DateTime DateFrom, DateTime DateTo, string SortOrder, bool IsVendor, bool IsVendorContact, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                if (memberId > 0)
                {
                    pageRequest.Conditions.Add((IsVendor ? "CompanyId" : (IsVendorContact ? "CreatorId" : "memberId")), memberId.ToString());
                }

                if (DateFrom != DateTime.MinValue) pageRequest.Conditions.Add("DateFrom", DateFrom.ToString("yyyyMMdd"));
                if (DateTo != DateTime.MinValue) pageRequest.Conditions.Add("DateTo", DateTo.AddDays(1).ToString("yyyyMMdd"));
                pageResponse = Facade.GetPagedVendorCandidatePerformance(pageRequest);
                return pageResponse.Response as List<Candidate>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountCandidatePerformance(int memberId, DateTime DateFrom, DateTime DateTo, string SortOrder, bool IsVendor, bool IsVendorContact)
        {
            return pageResponse.TotalRow;
        }
      
    }
}