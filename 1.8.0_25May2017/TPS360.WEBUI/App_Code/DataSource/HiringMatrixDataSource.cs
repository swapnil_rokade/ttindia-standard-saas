using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class HiringMatrixDataSource : ObjectDataSourceBase
    {
        PagedResponse<HiringMatrix> pageResponse = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount()
        {
            return pageResponse.TotalRow;
        }
        //-------added new filter option by Sumit Sonawane on 19/Sep/2017------
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string JobPostingId, string CandidateName, string CandidateEmail, string SourceID, string CandidateID,string PrimaryManager, string SelectionStepLookupId)
        {
            return pageResponse.TotalRow;
        }
       
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<HiringMatrix> GetPaged(string JobPostingId, string SelectionStepLookupId, string CandidateName, string CandidateEmail, string SourceID, string CandidateID, string PrimaryManager, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("JobPostingId", JobPostingId.ToString());
                pageRequest.Conditions.Add("CandidateName", CandidateName==null?"":MiscUtil .RemoveScript ( CandidateName.ToString()));
                pageRequest.Conditions.Add("CandidateEmail", CandidateEmail == null ? string.Empty : MiscUtil.RemoveScript(CandidateEmail.ToString()));
                if (SourceID != null && SourceID != "") pageRequest.Conditions.Add("SourceId", SourceID);                
                if (CandidateID != null && CandidateID != "" && CandidateID != "0") pageRequest.Conditions.Add("CandidateID", CandidateID);
                if (PrimaryManager != null && PrimaryManager != "") pageRequest.Conditions.Add("PrimaryManager", PrimaryManager);

                if(SelectionStepLookupId !="0")
                pageRequest.Conditions.Add("SelectionStepLookupId", SelectionStepLookupId.ToString());

                pageResponse = Facade.GetMinPagedHiringMatrix (pageRequest);
                return pageResponse.Response as List<HiringMatrix>;
            }
        }
        //---------------------------------End---------------------------------
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountAll(string JobPostingId, string SelectionStepLookupId)
        {
            return pageResponse.TotalRow;
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<HiringMatrix> GetPagedAll(string JobPostingId, string SelectionStepLookupId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {

                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("JobPostingId", JobPostingId.ToString());
                if (SelectionStepLookupId != "0")
                    pageRequest.Conditions.Add("SelectionStepLookupId", SelectionStepLookupId.ToString());

                pageResponse = Facade.GetMinPagedHiringMatrixAll(pageRequest);
                return pageResponse.Response as List<HiringMatrix>;
            }
        }
        ///////////////////////////////////Sumit Sonawane 20/07/2016////////////////////////////////////////////////////////////////////////////////////

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<HiringMatrix> GetPagedBucket(string JobPostingId, string SelectionStepLookupId, string JobPostingCreatorID, string MemberCreatorID, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {

                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);



                if (JobPostingId == null || JobPostingId == "0")
                {

                    if (JobPostingCreatorID != "0")
                        try
                        {
                            pageRequest.Conditions.Add("TeamMemberId", JobPostingCreatorID.ToString());// changed JobPostingCreatorID to TeamMemberId
                            //pageRequest.Conditions.Add("MemberCreatorID", MemberCreatorID.ToString());
                        }
                        catch
                        {
                        }
                    //pageResponse = Facade.HiringMatrixBucketLoad(pageRequest);
                    if (SelectionStepLookupId != "0")
                        pageRequest.Conditions.Add("SelectionStepLookupId", SelectionStepLookupId.ToString());
                    pageResponse = Facade.HiringMatrixBucketLoad(pageRequest);

                }
                else
                {
                    if (JobPostingId != null || JobPostingId != "0")
                        pageRequest.Conditions.Add("JobPostingId", JobPostingId.ToString());
                    if (SelectionStepLookupId != "0")
                        pageRequest.Conditions.Add("SelectionStepLookupId", SelectionStepLookupId.ToString());
                    //pageResponse = Facade.HiringMatrixBucket(pageRequest);
                    if (JobPostingCreatorID != "0")
                        try
                        {
                            pageRequest.Conditions.Add("TeamMemberId", JobPostingCreatorID.ToString());// changed JobPostingCreatorID to TeamMemberId
                            //pageRequest.Conditions.Add("MemberCreatorID", MemberCreatorID.ToString());
                        }
                        catch { }
                    pageResponse = Facade.HiringMatrixBucket(pageRequest);
                }
                //pageResponse = Facade.GetPagedHiringMatrixBucketView(pageRequest);
                //pageResponse = Facade.GetPagedHiringMatrix(pageRequest);
                return pageResponse.Response as List<HiringMatrix>;
            }
        }
        ///////////////////////////////////Sumit Sonawane 27/07/2016////////////////////////////////////////////////////////////////////////////////////
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<HiringMatrix> BucketViewCity(string JobPostingId, string SelectionStepLookupId, string RequisitionStatusID, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.Conditions.Add("JobPostingId", JobPostingId.ToString());
                if (SelectionStepLookupId != "0")
                    pageRequest.Conditions.Add("SelectionStepLookupId", SelectionStepLookupId.ToString());
                pageResponse = Facade.HiringMatrixBucket(pageRequest);
                return pageResponse.Response as List<HiringMatrix>;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}