/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:       OnLineParsingDataSource.cs
    Description:    This is the objects source file
    Created By:     Pravin khot
    Created On:     20/Nov/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
      
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;
using System.Web;
using System.Web.UI; 

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class OnLineParsingDataSource : ObjectDataSourceBase
     {
        PagedResponse<OnLineParser> pageResponse = null;
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public IList<OnLineParser> GetAllOnLineParser(int memberId, string SortColumn, string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            //IList<OnLineParser> OnLineParser = Facade.OnLineParser_GetAll(sortExpression);
            //if (memberId > 0)
            //{
            //    pageRequest.Conditions.Add("memberId", memberId.ToString());
            //}
            //return OnLineParser;            
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                pageRequest.SortColumn = SortColumn;
                if (memberId > 0)
                {
                    pageRequest.Conditions.Add("CreatorId", memberId.ToString());
                }

               // pageResponse = Facade.GetPagedCandidate(pageRequest);
                pageResponse = Facade.OnLineParser_GetPaged(pageRequest);
                return pageResponse.Response as List<OnLineParser>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetCountAllOnLineParser(int memberId, string SortColumn, string SortOrder)
        {
            return pageResponse.TotalRow;
        }
        public string  AppendSortParameter(string sortExpression)
        {
            string strOutput = "";
            if (!string.IsNullOrEmpty(sortExpression))
            {
                string[] part = sortExpression.Split(' ');

                if (part != null && part.Length > 0)
                {
                    strOutput = part[0];

                    if (part.Length > 1 && part[1] != null)
                    {
                        strOutput = part[1];
                    }
                    else
                    {
                        strOutput = UIConstants.SORT_ORDER_ASC;
                    }
                }
            }
            return strOutput;
        }

    }
}