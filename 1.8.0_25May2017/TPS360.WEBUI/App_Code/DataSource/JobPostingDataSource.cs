/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: JobPostingDatasSource.cs
    Description: This page is used for Job posting functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 Mar-10-2009         Nagarathna V.B      Defect ID:10068; Added new entity IsExpensesPaid
    0.2                 Apr-08-2009         Jagadish            Defect ID:10269; Changes made in methods 'GetListCount' and 'GetPaged'.
 *  0.3                 May-15-2009         Sandeesh            Defect id:10440 :Changes made to get the Requisition status from database instead of getting from the Enum
    0.4                 Feb-22-2010        Nagarathna V.B       Enhancement Id:12129;SubmissionReport;
 *  0.5                 22/May/2015         Prasanth Kumar G    Introduced GetPaged, GetListCount For Requisition Aging Report
 *  0.6                 2/Feb/2016         pravin khot          Introduced by GetListCountSourceBreakup,GetPagedSourceBreakup
 *  0.7                 23/May/2016        Sumit Sonawane       Introduced GetPaged() GetListCount() GetListCount() GetPaged()
----------------------------------------------------------------------------------------------------------------------------------------------
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Utility;

namespace TPS360.Web.UI
{
    [DataObject(true)]
    public class JobPostingDataSource : ObjectDataSourceBase
    {
        PagedResponse<JobPosting> pageResponse = null;
        PagedResponse<Submission> pageResponse1 = null;

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount()
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string jobTitle)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(bool IsTemplate)
        {
            return pageResponse.TotalRow;
        }

        //10531 starts

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(bool IsTemplate, int intDaysDifference)
        {
            return pageResponse.TotalRow;
        }

        // 10531 ends

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(bool IsTemplate, string memberId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(bool IsTemplate, string memberId,bool IsDashBoard,string SortOrder)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(bool IsTemplate, int JobStatus, bool IsJobActive)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(bool IsTemplate, int JobStatus, bool IsJobActive, int memberId)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string allKeys, string anyKey, string jobTitle, string jobType, string city, string lastUpdateDate, int jobStatus,string jobIndustry)  //8971
        {
            return pageResponse.TotalRow;
        }

        //0.2
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(bool IsTemplate, string jobStatus, int subJobStatus, string  jobStatusStartDate, string jobStatusEndDate, string ReqCreator,
            string employee,  string City,string StateID,string CountryID, string endClients,string JobPostingId)// string tier1Client, string tier2Client
        {
            return pageResponse.TotalRow;
        }
        
        //Code introduced by Prasanth on 22/May/2015 Start
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(bool IsTemplate, string ReqStartDateFrom, string ReqStartDateTo, string ReqCreator,
            string endClients)// string tier1Client, string tier2Client
        {
            return pageResponse.TotalRow;
        }

        //*******************END***********************
        //Code introduced by Pravin khot on 2/Feb/2016 Start
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountSourceBreakup(bool IsTemplate, string ReqStartDateFrom, string ReqStartDateTo)// string tier1Client, string tier2Client
        {
            return pageResponse.TotalRow;
        }

        //*******************END***********************
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(bool IsTemplate, bool IsValumeHire)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(int companyId, bool IsTemplate, int JobStatus, bool IsJobActive)
        {
            return pageResponse.TotalRow;
        }
        //12129
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string  JobPostStartDate,string  JobPostEndDate,string   JobSubmitStartDate,string  JobSubmitEndDate,
            string  ReqStartDate, string  ReqEndDate, string ApplicantTpye, string ReqOwner, string SubmittedBy, string Account, string Submittedto)
        {
            return pageResponse1.TotalRow;
            //return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountForDashboard(bool DashBoardSubmission)
        {
            return pageResponse1.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountForDashboard(bool DashBoardSubmission,int MemberId)
        {
            return pageResponse1.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Submission> GetPagedForDashboard(bool DashBoardSubmission, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (DashBoardSubmission )
                {
                    pageRequest.Conditions.Add("DashBoardSubmission", DashBoardSubmission.ToString ());
                }
                pageResponse1 = Facade.GetPagedDashBoardSubmission(pageRequest);
                return pageResponse1.Response as List<Submission>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Submission> GetPagedForDashboard(bool DashBoardSubmission,int MemberId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                if (DashBoardSubmission)
                {
                    pageRequest.Conditions.Add("DashBoardSubmission", DashBoardSubmission.ToString());
                }
                if (MemberId > 0)
                {
                    pageRequest.Conditions.Add("MemberId", MemberId .ToString ());
                }
                pageResponse1 = Facade.GetPagedDashBoardSubmission(pageRequest);
                return pageResponse1.Response as List<Submission>;
            }
        }
        //Requisition Search
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetRequisitionListCount(bool IsTemplate, string memberId,bool IsCompanyContact,int CompanyContactId, string JobTitle,string ReqCode, string jobStatus, string JobPostingFromDate, string JobPostingToDate, string ReqCreator, string employee, string City, string StateID, string CountryID, string endClients)
        {
            return pageResponse.TotalRow;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedJobPosting(pageRequest);
                return pageResponse.Response as List<JobPosting>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(string jobTitle,string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                if (!String.IsNullOrEmpty(jobTitle))
                {
                    pageRequest.Conditions.Add("JobTitle", jobTitle);                    
                }

                //0.3 Start

                string jobStatus=string.Empty;
                IList<GenericLookup> RequisitionStatusList = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.RequisitionStatus, JobStatus.Open.ToString());
                if (RequisitionStatusList != null)
                {
                    jobStatus = RequisitionStatusList[0].Id.ToString();
                }
               // pageRequest.Conditions.Add("JobStatus", ((int)JobStatus.Open).ToString());
                pageRequest.Conditions.Add("JobStatus", jobStatus);

                //0.3 End
                pageRequest.Conditions.Add("IsTemplate", "False");
                pageResponse = Facade.GetPagedJobPosting(pageRequest);
                return pageResponse.Response as List<JobPosting>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(bool IsTemplate, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("IsTemplate", IsTemplate.ToString());

                pageResponse = Facade.GetPagedJobPosting(pageRequest);
                return pageResponse.Response as List<JobPosting>;
            }
        }

        // 10531 starts


        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(bool IsTemplate, int intDaysDifference, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("IsTemplate", IsTemplate.ToString());
                pageRequest.Conditions.Add("intDaysDifference", intDaysDifference.ToString());

                pageResponse = Facade.GetPagedJobPosting(pageRequest);
                return pageResponse.Response as List<JobPosting>;
            }
        }

        // 10531 ends

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(bool IsTemplate, string memberId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("IsTemplate", IsTemplate.ToString());
                pageRequest.Conditions.Add("memberId", memberId.ToString());

                pageResponse = Facade.GetPagedJobPosting(pageRequest);
                return pageResponse.Response as List<JobPosting>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(bool IsTemplate, string memberId,bool IsDashBoard,string SortOrder, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.SortOrder = SortOrder;
                pageRequest.Conditions.Add("IsTemplate", IsTemplate.ToString());
              if(memberId !="" && memberId !="0")  pageRequest.Conditions.Add("memberId", memberId.ToString());

                pageResponse = Facade.GetPagedJobPostingWithCandidateCount(pageRequest);
                return pageResponse.Response as List<JobPosting>;
            }
        }


        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(bool IsTemplate, int JobStatus, bool IsJobActive, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("IsTemplate", IsTemplate.ToString());
                pageRequest.Conditions.Add("JobStatus", JobStatus.ToString());
                pageRequest.Conditions.Add("IsJobActive", IsJobActive.ToString());

                pageResponse = Facade.GetPagedJobPosting(pageRequest);
                return pageResponse.Response as List<JobPosting>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(bool IsTemplate, int JobStatus, bool IsJobActive, int memberId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("IsTemplate", IsTemplate.ToString());
                pageRequest.Conditions.Add("JobStatus", JobStatus.ToString());
                pageRequest.Conditions.Add("IsJobActive", IsJobActive.ToString());
                pageRequest.Conditions.Add("memberId", memberId.ToString());

                pageResponse = Facade.GetPagedJobPosting(pageRequest);
                return pageResponse.Response as List<JobPosting>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(string allKeys, string anyKey,string jobTitle,string jobType,string city,string lastUpdateDate,int jobStatus, string jobIndustry, string sortExpression, int startRowIndex, int maximumRows)  // 8971
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedJobPosting(allKeys, anyKey, jobTitle, jobType, city, lastUpdateDate, jobStatus, jobIndustry,pageRequest);  //8971
                return pageResponse.Response as List<JobPosting>;
            }
        }

        // 0.2
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(bool IsTemplate, string jobStatus, string subJobStatus, string  jobStatusStartDate, string jobStatusEndDate, string ReqCreator,
            string employee, string City, string StateID, string CountryId, string endClients, string JobPostingId, string sortExpression, int startRowIndex, int maximumRows) //string tier1Client, string tier2Client, 
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("IsTemplate", IsTemplate.ToString());
                pageRequest.Conditions.Add("jobStatus", jobStatus.ToString());
                pageRequest.Conditions.Add("subJobStatus", subJobStatus.ToString());
                pageRequest.Conditions.Add("jobStatusStartDate", jobStatusStartDate==null ?DateTime .MinValue .ToString () : jobStatusStartDate    );
                pageRequest.Conditions.Add("jobStatusEndDate", jobStatusEndDate!=null ? jobStatusEndDate : DateTime .MinValue .ToString ());

                pageRequest.Conditions.Add("ReqCreator", ReqCreator.ToString());
                pageRequest.Conditions.Add("employee", employee.ToString());
                pageRequest.Conditions.Add("City", City ==null ?"":MiscUtil .RemoveScript ( City .ToString ()));
                pageRequest.Conditions.Add("StateID", StateID .ToString ());
                pageRequest.Conditions.Add("CountryID", CountryId .ToString ());
                pageRequest.Conditions.Add("endClients", endClients.ToString());
  
                if(Convert .ToInt32 (JobPostingId )>0)
                pageRequest.Conditions.Add("JobPostingId", JobPostingId);
                //pageRequest.Conditions.Add("tier1Client", tier1Client.ToString());
                //pageRequest.Conditions.Add("tier2Client", tier2Client.ToString());

                pageResponse = Facade.GetPagedJobPostingReport(pageRequest);
                return pageResponse.Response as List<JobPosting>;
            }
        }



        //*********Code introduced by Prasanth on 22/May/2015 Start**********

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(bool IsTemplate, string ReqStartDateFrom, string ReqStartDateTo, string ReqCreator, string endClients,
            string sortExpression, int startRowIndex, int maximumRows) //string tier1Client, string tier2Client, 
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("IsTemplate", IsTemplate.ToString());
                pageRequest.Conditions.Add("endClients", endClients.ToString());
                pageRequest.Conditions.Add("ReqStartDateFrom", ReqStartDateFrom == null ? DateTime.MinValue.ToString() : ReqStartDateFrom);
                pageRequest.Conditions.Add("ReqStartDateTo", ReqStartDateTo != null ? ReqStartDateTo : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("ReqCreator", ReqCreator.ToString());
                
                //if (Convert.ToInt32(JobPostingId) > 0)
                //    pageRequest.Conditions.Add("JobPostingId", JobPostingId);
                //    pageResponse = Facade.GetPagedRequisitionAgingReport(pageRequest);
                
                //return pageResponse.Response as List<JobPosting>;


                pageResponse = Facade.GetPagedRequisitionAgingReport(pageRequest);
                
                return pageResponse.Response as List<JobPosting>;

            }
        }


        //*****************************END***********************************

        //*********Code introduced by Pravin khot on 2/Feb/2015 Start**********

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPagedSourceBreakup(bool IsTemplate, string ReqStartDateFrom, string ReqStartDateTo,
            string sortExpression, int startRowIndex, int maximumRows) //string tier1Client, string tier2Client, 
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("IsTemplate", IsTemplate.ToString());
                //pageRequest.Conditions.Add("endClients", endClients.ToString());
                pageRequest.Conditions.Add("ReqStartDateFrom", ReqStartDateFrom == null ? DateTime.MinValue.ToString() : ReqStartDateFrom);
                pageRequest.Conditions.Add("ReqStartDateTo", ReqStartDateTo != null ? ReqStartDateTo : DateTime.MinValue.ToString());
                //pageRequest.Conditions.Add("ReqCreator", ReqCreator.ToString());

                //if (Convert.ToInt32(JobPostingId) > 0)
                //    pageRequest.Conditions.Add("JobPostingId", JobPostingId);
                //    pageResponse = Facade.GetPagedRequisitionAgingReport(pageRequest);

                //return pageResponse.Response as List<JobPosting>;


                pageResponse = Facade.GetPagedRequisitionSourceBreakupReport(pageRequest);

                return pageResponse.Response as List<JobPosting>;

            }
        }


        //*****************************END***********************************


        //12129
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Submission> GetPaged(string  JobPostStartDate, string  JobPostEndDate, string  JobSubmitStartDate, string  JobSubmitEndDate, string  ReqStartDate, string  ReqEndDate,
            string ApplicantTpye, string ReqOwner, string SubmittedBy, string Account, string Submittedto, string sortExpression, int startRowIndex, int maximumRows)
        {
            DateTime t = DateTime.Now;
            //PagedResponse<Submission> pageResponse = null;
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("JobPostStartDate",JobPostStartDate!=null? Convert .ToDateTime ( JobPostStartDate.ToString()).ToString (): DateTime .MinValue.ToString () );
                pageRequest.Conditions.Add("JobPostEndDate", JobPostEndDate != null ? Convert.ToDateTime(JobPostEndDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("JobSubmitStartDate", JobSubmitStartDate != null ? Convert.ToDateTime(JobSubmitStartDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("JobSubmitEndDate", JobSubmitEndDate != null ? Convert.ToDateTime(JobSubmitEndDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("ReqStartDate", ReqStartDate!=null ? Convert.ToDateTime(ReqStartDate.ToString()).ToString() : DateTime.MinValue.ToString());

                pageRequest.Conditions.Add("ReqEndDate", ReqEndDate != null ? Convert.ToDateTime(ReqEndDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("ApplicantTpye", ApplicantTpye.ToString());
                pageRequest.Conditions.Add("ReqOwner", ReqOwner.ToString());

                pageRequest.Conditions.Add("SubmittedBy", SubmittedBy.ToString());
                pageRequest.Conditions.Add("Account", Account.ToString());
                pageRequest.Conditions.Add("Submittedto", (Submittedto == null ? "" : Submittedto.ToString()));
                //pageResponse = Facade.GetPagedSubmissionReport(pageRequest);
                
                pageResponse1 = Facade.GetPagedSubmissionReport(pageRequest);
                return pageResponse1.Response as List<Submission>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(bool IsTemplate, bool IsValumeHire, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("IsTemplate", IsTemplate.ToString());
                pageRequest.Conditions.Add("IsValumeHire", IsValumeHire.ToString());

                pageResponse = Facade.GetPagedJobPosting(pageRequest);
                return pageResponse.Response as List<JobPosting>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(string allKeys, string anyKey, string jobTitle, string jobType, string city, string lastUpdateDate, string volumeHire, int jobStatus, string jobIndustry, string sortExpression, int maximumRows, int startRowIndex)//9811
        {                           
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageResponse = Facade.GetPagedVolumeHireJobPosting(allKeys, anyKey, jobTitle, jobType, city, lastUpdateDate, volumeHire, jobStatus,jobIndustry, pageRequest);
                return pageResponse.Response as List<JobPosting>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string allKeys, string anyKey, string jobTitle, string jobType, string city, string lastUpdateDate, string volumeHire, int jobStatus, string jobIndustry)
        {
            return pageResponse.TotalRow;
        }

        //STARTS  0.1  

        public IList<JobPosting> GetAllByProjectID(int CurrentProjectId, string sortExpression,int maximumRows, int startRowIndex)
        {
            IList<JobPosting> jobPostingList = Facade.GetAllJobPostingByProjectId(CurrentProjectId, sortExpression);
            return jobPostingList;
        }
        //ENDS  0.1  

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(int companyId, bool IsTemplate, int JobStatus, bool IsJobActive, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("IsTemplate", IsTemplate.ToString());
                pageRequest.Conditions.Add("JobStatus", JobStatus.ToString());
                pageRequest.Conditions.Add("IsJobActive", IsJobActive.ToString());
                pageRequest.Conditions.Add("companyId", companyId.ToString());

                pageResponse = Facade.GetPagedJobPosting(pageRequest);
                return pageResponse.Response as List<JobPosting>;
            }
        }



        //Requisitgion List Search
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPagedRequisitionList(bool IsTemplate, string memberId,bool IsCompanyContact,int CompanyContactId, string JobTitle,string ReqCode, string jobStatus,string JobPostingFromDate,string JobPostingToDate,string ReqCreator,string employee,string City,string StateID,string CountryID,string endClients, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("IsTemplate", IsTemplate.ToString());
                pageRequest.Conditions.Add("memberId", memberId==null || memberId .ToString ()==""?"0": memberId.ToString());
                pageRequest.Conditions.Add("JobTitle",MiscUtil .RemoveScript ( JobTitle));
               if(jobStatus !=null && jobStatus!="") pageRequest.Conditions.Add("JobStatus", jobStatus );
                pageRequest.Conditions.Add("JobPostingFromDate", JobPostingFromDate==null || JobPostingFromDate ==""? DateTime .MinValue .ToString () : JobPostingFromDate  );
                pageRequest.Conditions.Add("JobPostingToDate", JobPostingToDate == null || JobPostingToDate == "" ? DateTime.MinValue.ToString() : JobPostingToDate);
                pageRequest.Conditions.Add("Creator", ReqCreator );
                pageRequest.Conditions.Add("Employee", employee );
                pageRequest.Conditions.Add("City", City==null?"":MiscUtil .RemoveScript ( City .Trim () ));
                pageRequest.Conditions.Add("StateId", StateID);
                pageRequest.Conditions.Add("CountryId", CountryID );
                pageRequest.Conditions.Add("Client", endClients );
                pageRequest.Conditions.Add("ReqCode", MiscUtil.RemoveScript(ReqCode));
                if (IsCompanyContact) 
                {
                    pageRequest.Conditions.Add("IsCompanyContact", CompanyContactId.ToString());
                }
                
                pageResponse = Facade.GetPagedJobPosting(pageRequest);
                return pageResponse.Response as List<JobPosting>;
            }
        }

        //Requisitgion List Search
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPagedRequisitionListForCandidatePortal(string memberId, string JobTitle, string ReqCode,   string City, string StateID, string CountryID,  string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.Conditions.Add("JobTitle", MiscUtil.RemoveScript(JobTitle));
                pageRequest.Conditions.Add("City", City == null ? "" : MiscUtil.RemoveScript(City.Trim()));
                 if(StateID !="" && StateID !="0")pageRequest.Conditions.Add("StateId", StateID);
                pageRequest.Conditions.Add("CountryId", CountryID);
                pageResponse = Facade.GetPagedForCandidatePortal (pageRequest,Convert .ToInt32 ( memberId) );
                return pageResponse.Response as List<JobPosting>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountForCandidatePortal(string memberId, string JobTitle, string ReqCode,   string City, string StateID, string CountryID)
        {
            return pageResponse.TotalRow;
        }




        /* Employee Referal Page */
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPagedRequisitionListForEmployeeReferal(string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
             
                pageResponse = Facade.JobPosting_GetPagedEmployeeReferal (pageRequest);//, Convert.ToInt32(memberId));
                return pageResponse.Response as List<JobPosting>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountForEmployeeReferal()
        {
            return pageResponse.TotalRow;
        }

        //Vendor Portal

        //Requisitgion List Search
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPagedRequisitionListForVendorPortal(string JobTitle, string ReqCode, string City, string StateID, string CountryID,string VendorId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.Conditions.Add("JobTitle", MiscUtil.RemoveScript(JobTitle));
                pageRequest.Conditions.Add("City", City == null ? "" : MiscUtil.RemoveScript(City.Trim()));
                if (StateID != "" && StateID != "0") pageRequest.Conditions.Add("StateId", StateID);
                pageRequest.Conditions.Add("CountryId", CountryID);
                pageResponse = Facade.GetPagedForVendorPortal (pageRequest,Convert.ToInt32(VendorId));
                return pageResponse.Response as List<JobPosting>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountForVendorPortal( string JobTitle, string ReqCode, string City, string StateID, string CountryID,string VendorId)
        {
            return pageResponse.TotalRow;
        }
        //Added by pravin khot on 15/March/2017**************
        //Requisitgion List MyPerformance Search
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPagedRequisitionListForMyPerformance(string JobTitle, string ReqCode, string City, string StateID, string CountryID, string VendorId, string MemberId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.Conditions.Add("JobTitle", MiscUtil.RemoveScript(JobTitle));
                pageRequest.Conditions.Add("City", City == null ? "" : MiscUtil.RemoveScript(City.Trim()));
                if (StateID != "" && StateID != "0") pageRequest.Conditions.Add("StateId", StateID);
                pageRequest.Conditions.Add("CountryId", CountryID);
                pageResponse = Facade.GetPagedForVendorPortalForMyPerformance(pageRequest, Convert.ToInt32(VendorId), Convert.ToInt32(MemberId));
                return pageResponse.Response as List<JobPosting>;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountForMyPerformance(string JobTitle, string ReqCode, string City, string StateID, string CountryID, string VendorId,string MemberId)
        {
            return pageResponse.TotalRow;
        }
        //Active Job Openings
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPagedRequisitionListForActiveJobOpenings(string JobTitle, string ReqCode, string City, string StateID, string CountryID, string VendorId, string sortExpression, int startRowIndex, int maximumRows)
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
                pageRequest.Conditions.Add("JobTitle", MiscUtil.RemoveScript(JobTitle));
                pageRequest.Conditions.Add("City", City == null ? "" : MiscUtil.RemoveScript(City.Trim()));
                if (StateID != "" && StateID != "0") pageRequest.Conditions.Add("StateId", StateID);
                pageRequest.Conditions.Add("CountryId", CountryID);
                pageResponse = Facade.GetPagedForVendorPortal(pageRequest, Convert.ToInt32(VendorId));
                return pageResponse.Response as List<JobPosting>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCountForActiveJobOpenings(string JobTitle, string ReqCode, string City, string StateID, string CountryID, string VendorId)
        {
            return pageResponse.TotalRow;
        }
        //Requisitgion List MyPerformance Search
        //[DataObjectMethod(DataObjectMethodType.Select, true)]
        //public List<JobPosting> GetPagedRequisitionListForMyPerformance(string JobTitle, string ReqCode, string City, string StateID, string CountryID, string VendorId, string sortExpression, int startRowIndex, int maximumRows)
        //{
        //    using (new PerformanceBenchmark())
        //    {
        //        PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);
        //        pageRequest.Conditions.Add("JobTitle", MiscUtil.RemoveScript(JobTitle));
        //        pageRequest.Conditions.Add("City", City == null ? "" : MiscUtil.RemoveScript(City.Trim()));
        //        if (StateID != "" && StateID != "0") pageRequest.Conditions.Add("StateId", StateID);
        //        pageRequest.Conditions.Add("CountryId", CountryID);
        //        pageResponse = Facade.GetPagedForVendorPortal(pageRequest, Convert.ToInt32(VendorId));
        //        return pageResponse.Response as List<JobPosting>;
        //    }
        //}

        //[DataObjectMethod(DataObjectMethodType.Select, false)]
        //public int GetListCountForMyPerformance(string JobTitle, string ReqCode, string City, string StateID, string CountryID, string VendorId)
        //{
        //    return pageResponse.TotalRow;
        //}
        //*******************END**********************************
        //////////////Code Added by Sumit Sonawane on 23/May/2016/////////////////////////
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<JobPosting> GetPaged(bool IsTemplate, string jobStatus, string subJobStatus, string jobStatusStartDate, string jobStatusEndDate, string ReqCreator,
            string employee, string City, string StateID, string CountryId, string endClients, string JobPostingId, bool IsTeamRequisitionReport, string TeamMembersId, string TeamId, string OpportunityID, string OpenStartDate, string OpenEndDate, string ExpectedFulfillmentDateStartDate, string ExpectedFulfillmentDateEndDate, string RevisedFulfillmentDateStartDate, string RevisedFulfillmentDateEndDate, string CustomerName, string StageName,
            string sortExpression, int startRowIndex, int maximumRows) //string tier1Client, string tier2Client, 
        {
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("IsTemplate", IsTemplate.ToString());
                pageRequest.Conditions.Add("jobStatus", jobStatus.ToString());
                pageRequest.Conditions.Add("subJobStatus", subJobStatus.ToString());
                pageRequest.Conditions.Add("jobStatusStartDate", jobStatusStartDate == null ? DateTime.MinValue.ToString() : jobStatusStartDate);
                pageRequest.Conditions.Add("jobStatusEndDate", jobStatusEndDate != null ? jobStatusEndDate : DateTime.MinValue.ToString());

                pageRequest.Conditions.Add("OpenStartDate", OpenStartDate == null ? DateTime.MinValue.ToString() : OpenStartDate);
                pageRequest.Conditions.Add("OpenEndDate", OpenEndDate != null ? OpenEndDate : DateTime.MinValue.ToString());

                pageRequest.Conditions.Add("ExpectedFulfillmentStartDate", ExpectedFulfillmentDateStartDate == null ? DateTime.MinValue.ToString() : ExpectedFulfillmentDateStartDate);
                pageRequest.Conditions.Add("ExpectedFulfillmentEndDate", ExpectedFulfillmentDateEndDate != null ? ExpectedFulfillmentDateEndDate : DateTime.MinValue.ToString());

                pageRequest.Conditions.Add("RevisedFulfillmentDateStartDate", RevisedFulfillmentDateStartDate == null ? DateTime.MinValue.ToString() : RevisedFulfillmentDateStartDate);
                pageRequest.Conditions.Add("RevisedFulfillmentDateEndDate", RevisedFulfillmentDateEndDate != null ? RevisedFulfillmentDateEndDate : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("StageName", StageName.ToString());
                pageRequest.Conditions.Add("ReqCreator", ReqCreator.ToString());
                pageRequest.Conditions.Add("employee", employee.ToString());
                pageRequest.Conditions.Add("City", City == null ? "" : MiscUtil.RemoveScript(City.ToString()));
                pageRequest.Conditions.Add("StateID", StateID.ToString());
                pageRequest.Conditions.Add("CountryID", CountryId.ToString());
                pageRequest.Conditions.Add("CustomerName", CustomerName.ToString());
                pageRequest.Conditions.Add("endClients", endClients.ToString());
                if (Convert.ToInt32(JobPostingId) > 0)
                    pageRequest.Conditions.Add("JobPostingId", JobPostingId);
                pageRequest.Conditions.Add("IsTeamRequisitionReport", IsTeamRequisitionReport.ToString());
                pageRequest.Conditions.Add("TeamMembersId", TeamMembersId.ToString());
                pageRequest.Conditions.Add("TeamId", TeamId.ToString());
                pageRequest.Conditions.Add("OpportunityID", OpportunityID == null ? "" : MiscUtil.RemoveScript(OpportunityID.ToString()));
                //pageRequest.Conditions.Add("tier1Client", tier1Client.ToString());
                //pageRequest.Conditions.Add("tier2Client", tier2Client.ToString());
                pageResponse = Facade.GetPagedJobPostingReport(pageRequest);

                return pageResponse.Response as List<JobPosting>;
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(bool IsTemplate, string jobStatus, int subJobStatus, string jobStatusStartDate, string jobStatusEndDate, string ReqCreator,
            string employee, string City, string StateID, string CountryID, string endClients, string JobPostingId, bool IsTeamRequisitionReport,
            string TeamMembersId, string TeamId, string OpportunityID, string OpenStartDate, string OpenEndDate, string ExpectedFulfillmentDateStartDate,
            string ExpectedFulfillmentDateEndDate, string RevisedFulfillmentDateStartDate, string RevisedFulfillmentDateEndDate, string CustomerName,
            string StageName)// string tier1Client, string tier2Client
        {
            return pageResponse.TotalRow;
        }
        /////////////////////////////////////////////////////////////////////////////////////////////

        /////////////Code Added by Sumit Sonawane on 23/May/2016///////////////////////////////////////
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetListCount(string JobPostStartDate, string JobPostEndDate, string JobSubmitStartDate, string JobSubmitEndDate,
            string ReqStartDate, string ReqEndDate, string ApplicantTpye, string ReqOwner, string SubmittedBy, string Account, string Submittedto, bool IsTeamReport, int TeamLeaderId, int TeamId)
        {
            return pageResponse1.TotalRow;
        }
        /////////////////////////////////////////////////////////////////////////////////////////////

        /////////////Code Added by Sumit Sonawane on 23/May/2016///////////////////////////////////////
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Submission> GetPaged(string JobPostStartDate, string JobPostEndDate, string JobSubmitStartDate, string JobSubmitEndDate, string ReqStartDate, string ReqEndDate,
            string ApplicantTpye, string ReqOwner, string SubmittedBy, string Account, string Submittedto, bool IsTeamReport, int TeamLeaderId, int TeamId, string sortExpression, int startRowIndex, int maximumRows)
        {
            DateTime t = DateTime.Now;
            //PagedResponse<Submission> pageResponse = null;
            using (new PerformanceBenchmark())
            {
                PagedRequest pageRequest = base.PreparePagedRequest(sortExpression, startRowIndex, maximumRows);

                pageRequest.Conditions.Add("JobPostStartDate", JobPostStartDate != null ? Convert.ToDateTime(JobPostStartDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("JobPostEndDate", JobPostEndDate != null ? Convert.ToDateTime(JobPostEndDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("JobSubmitStartDate", JobSubmitStartDate != null ? Convert.ToDateTime(JobSubmitStartDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("JobSubmitEndDate", JobSubmitEndDate != null ? Convert.ToDateTime(JobSubmitEndDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("ReqStartDate", ReqStartDate != null ? Convert.ToDateTime(ReqStartDate.ToString()).ToString() : DateTime.MinValue.ToString());

                pageRequest.Conditions.Add("ReqEndDate", ReqEndDate != null ? Convert.ToDateTime(ReqEndDate.ToString()).ToString() : DateTime.MinValue.ToString());
                pageRequest.Conditions.Add("ApplicantTpye", ApplicantTpye.ToString());
                pageRequest.Conditions.Add("ReqOwner", ReqOwner.ToString());

                pageRequest.Conditions.Add("SubmittedBy", SubmittedBy.ToString());
                pageRequest.Conditions.Add("Account", Account.ToString());
                pageRequest.Conditions.Add("Submittedto", (Submittedto == null ? "" : Submittedto.ToString()));
                //pageResponse = Facade.GetPagedSubmissionReport(pageRequest);
                if (IsTeamReport)
                {
                    if (TeamId > 0)
                    {
                        pageRequest.Conditions.Add("TeamReport", TeamLeaderId.ToString() + "," + TeamId.ToString());
                    }
                    else
                    {
                        pageRequest.Conditions.Add("TeamReport", TeamLeaderId.ToString());
                    }
                }

                pageResponse1 = Facade.GetPagedSubmissionReport(pageRequest);
                return pageResponse1.Response as List<Submission>;
            }
        }



        /////////////////////////////////////////////////////////////////////////////////////////////


    }
}