/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:           ActiveDirectoryConnector.cs
    Description:        This is the class library for Active Directory Connector 
    Created By:         Prasanth Kumar G
    Created On:         30/Mar/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author             Modification
    0.1                 10/Jun/2016        Prasanth Kumar G    LDAP Log file generation
    ------------------------------------------------------------------------------------------------------------------------------
    

 */

using System;
using System.Diagnostics;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;
using System.Web;
using System.Web.Security;
using System.Configuration;

namespace TPS360.Web.UI.Helper
{
    public static class ActiveDirectoryConnector
    {
        #region Member Variables
        
        private static ActiveDirectoryConfiguration _currentActiveDirectoryConfiguration = null;

        #endregion

        #region Properties

        private static ActiveDirectoryConfiguration activeDirectorySettings = null;
        public static ActiveDirectoryConfiguration ActiveDirectorySettings
        {
            get
            {
                try
                {
                    if (activeDirectorySettings == null)
                    {
                        activeDirectorySettings = (ActiveDirectoryConfiguration)ConfigurationManager.GetSection("ldapConfiguration");
                    }
                }
                catch
                {
                }
                return activeDirectorySettings;
            }
        }

        #endregion

        #region Methods

        public static bool IsUserLoggedIn(string userName, string password)
        {
            try
            {
                if (ActiveDirectorySettings.Enabled)
                {
                    int startIndex = userName.IndexOf("@");
                    if (startIndex >= 0)        
                    {
                        userName = userName.Substring(0, startIndex);
                    }
                    DirectoryEntry ldapConnection = new DirectoryEntry("LDAP://" + ActiveDirectorySettings.Server + "/" + ActiveDirectorySettings.DirectoryPath, userName, password);
                    DirectorySearcher searcher = new DirectorySearcher(ldapConnection);
                    searcher.Filter = ActiveDirectorySettings.Filter.Replace("and", "&");
                    searcher.Filter = searcher.Filter.Replace(ActiveDirectorySettings.FilterReplace, userName);
                    searcher.PropertiesToLoad.Add("memberOf");
                    searcher.PropertiesToLoad.Add("userAccountControl");

                    SearchResult directoryUser = searcher.FindOne();
                    if (directoryUser != null)
                    {
                        int flags = Convert.ToInt32(directoryUser.Properties["userAccountControl"][0].ToString());
                        if (!Convert.ToBoolean(flags & 0x0002))
                        {
                            string desiredGroupName = ActiveDirectorySettings.GroupName.ToLower();
                            if (desiredGroupName!=string.Empty)
                            {
                                desiredGroupName = "cn=" + desiredGroupName + ",";
                                int numberOfGroups = directoryUser.Properties["memberOf"].Count;
                                bool isWithinGroup = false;
                                for (int i = 0; i < numberOfGroups; i++)
                                {
                                    string groupName = directoryUser.Properties["memberOf"][i].ToString().ToLower();
                                    if (groupName.Contains(desiredGroupName))
                                    {
                                        isWithinGroup = true;
                                        break;
                                    }
                                }
                                if (!isWithinGroup)
                                {
                                    //Uncomment this section for validating group and mention specific group in config file
                                    //throw new Exception("User [" + userName + "] is not a member of the desired group."); 0123456789
                                }
                            }
                            return true;
                        }
                        else
                        {
                            throw new Exception("User [" + userName + "] is inactive.");
                        }
                    }
                    else
                    {
                        throw new Exception("User [" + userName + "] not found in the specified active directory path.");
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (LdapException ex)
            {
                if (ex.ErrorCode == 49)
                {
                    //throw new Exception("Invalid user authentication. Please input a valid user name & pasword and try again.",ex);
                    //message changed by Sumit on 17/Jan/2017
                    throw new Exception("Your login attempt was not successful.Please try again.", ex);
                }
                else
                {
                    BasePage.writeLog_LdapException(ex); //Line introduced by Prasanth on 10/Jun/2016
                    throw new Exception("Active directory server not found.", ex);
                }
            }
            catch (DirectoryOperationException ex)
            {
                throw new Exception("Invalid active directory path.", ex);
            }
            catch (DirectoryServicesCOMException ex)
            {
                if (ex.ExtendedError == 8333)
                {
                    throw new Exception("Invalid active directory path.", ex);
                }
                else
                {
                    //Line introduced by Prasanth on 10/Jun/2016
                    BasePage.writeLog_DirectoryServicesCOMException(ex, "LDAP://" + ActiveDirectorySettings.Server + "/" + ActiveDirectorySettings.DirectoryPath, userName);
                    //throw new Exception("Invalid user authentication. Please input a valid user name & pasword and try again.", ex);
                    //message changed by Sumit on 17/Jan/2017
                    throw new Exception("Your login attempt was not successful.Please try again.", ex);
                }
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                throw new Exception("Active directory server not found.", ex);
            }
            catch (ArgumentException ex)
            {
                if (ex.Source == "System.DirectoryServices")
                {
                    throw new Exception("Invalid search filter expression.", ex);
                }
                else
                {
                    throw new Exception("Unhandeled exception occured while authenticating user using active directory.", ex);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unhandeled exception occured while authenticating user using active directory.", ex);
            }
        }

        public static void UserAuthenticationCheck()
        {
            try
            {
                if (ActiveDirectorySettings.Enabled)
                {
                    if ((ActiveDirectorySettings.PageLevelSecurityCheck) && !HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("login.aspx"))
                    {
                        if (HttpContext.Current.User != null)
                        {
                            if (HttpContext.Current.User.Identity.IsAuthenticated)
                            {
                                if (HttpContext.Current.User.Identity is FormsIdentity)
                                {
                                    FormsIdentity formIdentity = (FormsIdentity)HttpContext.Current.User.Identity;
                                    FormsAuthenticationTicket userAuthTicket = formIdentity.Ticket;
                                    if (!IsUserLoggedIn(userAuthTicket.Name, userAuthTicket.UserData))
                                    {
                                        FormsAuthentication.SignOut();
                                        FormsAuthentication.RedirectToLoginPage();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        #endregion
    }
}
