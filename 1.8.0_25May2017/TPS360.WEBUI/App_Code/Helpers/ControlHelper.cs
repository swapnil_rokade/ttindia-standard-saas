/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ControlHelper.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               20-Jul-2009         Nagarathna           Defect id: 10990 ;uncommented sort array list in "PopulateEnumDescriptionIntoList"
 * 0.2                sep-09-2009        Ranjit Kumar.I        Enhancement iD :9763:Removed Code PopulateEnumDescriptionIntoList() In Order To Sort the List
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Reflection;
using TPS360.Common;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;

namespace TPS360.Web.UI.Helper
{
    public sealed class ControlHelper
    {
        private HttpRequest _request;

        public ControlHelper(HttpRequest request)
        {
            _request = request;
        }

        [DebuggerStepThrough()]
        public static Control GetControlOfType(ControlCollection controls,
        Type type)
        {
            if (controls.Count == 0)
            {
                return null;
            }

            if (controls[0].GetType() == type)
            {
                return controls[0];
            }

            foreach (Control c in controls)
            {
                if ((c.GetType() == type) || (c.GetType().BaseType == type))
                {
                    return c;
                }
            }

            return null;
        }

        [DebuggerStepThrough()]
        public string GetControlValue(string control)
        {
            return FormValue(control);
        }

        [DebuggerStepThrough()]
        public string GetControlValue(Control control)
        {
            return FormValue(control.UniqueID);
        }

        [DebuggerStepThrough()]
        public string GetControlValue(Control control,
        string defaultValue)
        {
            string returnValue = FormValue(control.UniqueID);

            if (returnValue == null)
            {
                return defaultValue;
            }

            return returnValue;
        }

        [DebuggerStepThrough()]
        private string FormValue(string control)
        {
            return _request.Form[control];
        }

        [DebuggerStepThrough()]
        public static void SetDefaultButton(WebControl[] controls, Control button)
        {
            if ((controls != null) && (controls.Length > 0))
            {
                for (int i = 0; i < controls.Length; i++)
                {
                    SetDefaultButton(controls[i], button);
                }
            }
        }

        [DebuggerStepThrough()]
        public static void SetDefaultButton(WebControl control, Control button)
        {
            if ((control != null) && (button != null))
            {
                control.Attributes.Add("onkeydown", "SetDefaultButton('" + button.ClientID + "')");
            }
        }

        //defect id: 10143
        [DebuggerStepThrough()]
        public static void SetHtmlAnchor(HtmlAnchor HtmlAnchor,
        string url)
        {
            HtmlAnchor.HRef = url;
        }

        [DebuggerStepThrough()]
        public static void SetHtmlAnchor(HtmlAnchor HtmlAnchor,
        string url,
        string returnUrl,
        params string[] parameters)
        {
            SecureUrl secureUrl = UrlHelper.BuildSecureUrl(url, returnUrl, parameters);
            SetHtmlAnchor(HtmlAnchor, secureUrl);
        }

        [DebuggerStepThrough()]
        public static void SetHtmlAnchor(HtmlAnchor HtmlAnchor,
        string url,
        string returnUrl)
        {
            SetHtmlAnchor(HtmlAnchor, url, returnUrl, null);
        }

        [DebuggerStepThrough()]
        public static void SetHtmlAnchor(HtmlAnchor HtmlAnchor,
        SecureUrl url)
        {
            SetHtmlAnchor(HtmlAnchor, url.ToString());
        }
        // defect id: 10143

        [DebuggerStepThrough()]
        public static void SetHyperLink(HyperLink link,
        string url,
        string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                link.Text = text;
            }

            link.NavigateUrl = url;
        }

        [DebuggerStepThrough()]
        public static void SetHyperLink(HyperLink link,
        string url,
        string returnUrl,
        string text,
        params string[] parameters)
        {
            SecureUrl secureUrl = UrlHelper.BuildSecureUrl(url, returnUrl, parameters);
            SetHyperLink(link, secureUrl, text);
        }

        [DebuggerStepThrough()]
        public static void SetHyperLink(HyperLink link,
        string url,
        string returnUrl,
        string text)
        {
            SetHyperLink(link, url, returnUrl, text, null);
        }

        [DebuggerStepThrough()]
        public static void SetHyperLink(HyperLink link,
        SecureUrl url,
        string text)
        {
            SetHyperLink(link, url.ToString(), text);
        }

        [DebuggerStepThrough()]
        public static bool IsItemDataRow(ListItemType itemType)
        {
            return (
                     (itemType == ListItemType.Item) ||
                     (itemType == ListItemType.AlternatingItem)
                    );
        }

        [DebuggerStepThrough()]
        public static bool IsListItemDataRow(ListViewItemType itemType)
        {
            return (
                     (itemType == ListViewItemType.DataItem)
                    );
        }

        [DebuggerStepThrough()]
        public static void SelectListByText(ListControl list,
        string text)
        {
            list.SelectedIndex = list.Items.IndexOf(list.Items.FindByText(text));
        }

        [DebuggerStepThrough()]
        public static void SelectListByValue(ListControl list,
        string value)
        {
            list.SelectedIndex = list.Items.IndexOf(list.Items.FindByValue(value));
        }

        [DebuggerStepThrough()]
        public static void ClearListSelection(ListControl list)
        {
            list.SelectedIndex = -1;

            foreach (ListItem item in list.Items)
            {
                item.Selected = false;
            }
        }

        [DebuggerStepThrough()]
        public static void RemoveSelectedFromList(ListControl list)
        {
            for (int i = list.Items.Count - 1; i > -1; i--)
            {
                if (list.Items[i].Selected)
                {
                    list.Items.RemoveAt(i);
                }
            }
        }

        [DebuggerStepThrough()]
        public static void PopulateEnumIntoList(ListControl list, Type enumType)
        {
            string[] names = Enum.GetNames(enumType);

            if ((names != null) && (names.Length > 0))
            {
                foreach (string name in names)
                {
                    int value = (int)Enum.Parse(enumType, name);

                    if (value > 0)
                    {
                        ListItem item = new ListItem(name, value.ToString(System.Globalization.CultureInfo.CurrentCulture));
                        list.Items.Add(item);
                    }
                }
            }
        }

        [DebuggerStepThrough()]
        public static void PopulateEnumDescriptionIntoList(ListControl list, Type enumType)
        {
            string[] names = Enum.GetNames(enumType);

            IList<Pair> arrayList = new List<Pair>();

            if ((names != null) && (names.Length > 0))
            {
                foreach (string name in names)
                {
                    int value = (int)Enum.Parse(enumType,name);
                    string strDescription = EnumHelper.GetDescription((Enum)Enum.Parse(enumType, name));

                    if (value > 0)
                    {
                        arrayList.Add(new Pair{ First = value.ToString(), Second = strDescription });
                        //arrayList.Add(new Pair(StringHelper.Convert(value), strDescription));
                    }
                }
            }

            //arrayList = arrayList.OrderBy(t => t.Second).ToList();  // 9763 //0.1//Enhancement iD :9763
            if ((arrayList != null) && (arrayList.Count > 0))
            {
                foreach (Pair pair in arrayList)
                {
                    ListItem item = new ListItem(pair.Second.ToString(), pair.First.ToString());
                    list.Items.Add(item);
                }
            }
        }
        public static void PopulateEnumDescriptionIntoList(ListControl list, Type enumType , bool isAsending)
        {
            string[] names = Enum.GetNames(enumType);

            IList<Pair> arrayList = new List<Pair>();

            if ((names != null) && (names.Length > 0))
            {
                foreach (string name in names)
                {
                    int value = (int)Enum.Parse(enumType, name);
                    string strDescription = EnumHelper.GetDescription((Enum)Enum.Parse(enumType, name));

                    if (value > 0)
                    {
                        arrayList.Add(new Pair { First = value.ToString(), Second = strDescription });
                     
                    }
                }
            }
            if (isAsending)
            {
                arrayList = arrayList.OrderBy(t => t.Second).ToList();
            }
            else
            {
                arrayList = arrayList.OrderByDescending(t => t.Second).ToList();
            }
            if ((arrayList != null) && (arrayList.Count > 0))
            {
                foreach (Pair pair in arrayList)
                {
                    ListItem item = new ListItem(pair.Second.ToString(), pair.First.ToString());
                    list.Items.Add(item);
                }
            }
        }

        public static void PopulateEnumDescriptionIntoListView(ListControl list, Type enumType)
        {
            string[] names = Enum.GetNames(enumType);

            IList<WorkOrderExpense> arrayList = new List<WorkOrderExpense>();

            if ((names != null) && (names.Length > 0))
            {
                foreach (string name in names)
                {
                    int value = (int)Enum.Parse(enumType, name);
                    string strDescription = EnumHelper.GetDescription((Enum)Enum.Parse(enumType, name));

                    if (value > 0)
                    {
                        arrayList.Add(new WorkOrderExpense { Cost =Convert.ToDecimal(strDescription), ExpenseType = value});
                        //arrayList.Add(new WorkOrderExpense(StringHelper.Convert(value), strDescription));
                    }
                }
            }

            arrayList = arrayList.OrderBy(t => t.Cost).ToList();

            if ((arrayList != null) && (arrayList.Count > 0))
            {
                foreach (WorkOrderExpense workOrderExpense in arrayList)
                {
                    ListItem item = new ListItem(workOrderExpense.Cost.ToString(), workOrderExpense.ExpenseType.ToString());
                    list.Items.Add(item);
                }
            }
        }       
    }
}