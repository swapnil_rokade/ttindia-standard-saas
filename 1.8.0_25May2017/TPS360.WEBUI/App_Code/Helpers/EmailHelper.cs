﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmailHelper.cs
    Description: This is the .net framework class used to build email helper.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               Oct-07-2008           Jagadish            Defect id:8790; Commented the code that adds Client Rate, Requisition Rate, Applicant Desired Rate, Submission Rate 
                                                                fields to email helper.
    0.2               Oct-16-2008           Jagadish            Defect id:8887;  When the user tries to click on the 'Save as Pdf' or 'Preview' buttons 'Runtime error' was occuring.
                                                                Modified the code to fix that. Added code to check the object for null.
    0.3               Nov-07-2008           Yogeesh Bhat        Defect ID: 8860; changes made in methods: GetHiringMatrixHTML(),GetApplicantHiringMatrix() 
    0.4               Nov-19-2008           Anand Dixit         Defect ID: 8990; changed line no's 1623,1651 and 1655.
    0.5               Jan-08-2009           Jagadish            Defect ID: 9524; Appended one line to string builder in 'GetApplicantResumeForSubmission' method.
    0.6               Feb-25-2009           Nagarathna V.B      Defect ID:9938 ; changed the font and alignment in 'GetApplicantSkillSetEmailBody','GetApplicantReferencesEmailBody'
                                                                'GetApplicantPersonalInformationEmailBody','GetApplicantObjectiveAndSummary'
    0.7               Apr-01-2009           Yogeesh Bhat        Defect Id: 10085; Added new overloaded methods GetApplicantPersonalInformationEmailBody() 
                                                                and GetSubmittedApplicantsResumesEmailBody()
    0.8               Apr-07-2009           Yogeesh Bhat        Defect Id:10238; changes made in GetSubmittedApplicantsResumesEmailBody()
    0.9               Apr-10-2009           Jagadish            Defect Id:10278; Changes made in method 'GetApplicantUploadedResumesEmailBody()'.
 *  1.0               Apr-13-2009           Sandeesh            Defect Id:10325;Changes made in method  'GetApplicantPersonalInformationEmailBody()'
 *  1.1               Apr-14-2009           Sandeesh            Defect Id:10331 ; Removed the 'Selected Resumes' section from the email. 
 *  1.2               Apr-21-2009           Gopala Swamy J      Defect Id:10311;Making use of ApplicationBaseUrl to diaplay image.
 *  1.3               Apr-24-2009           N.Srilakshmi        Defect Id:10371;Changes made in GetSubmittedApplicantsResumesEmailBody()   
 *  1.4               Apr-28-2009           Yogeesh Bhat        Defect Id:10375;Changes made in GetClientJobDescription()
    1.5               Aug-20-2009           Shivanand           Defect Id:10320;Changes made in GetSubmittedApplicantsResumesEmailBody().
    1.6               Nov-27-2009           Sandeesh            Defect Id:11634;Changes made to GetJobPostingBasicDetails
    1.7               Dec-23-2009           Rajendra            Defect Id:12037;Changes made in Send() Method; 
    1.8               Apr-02-2010           Ganapati Bhat       Defect Id:12440;Addedd " " in GetApplicantPersonalInformationEmailBody().
 *  1.9               17/May/2016           pravin khot         added new function - PrepareViewforTps360AccessCandidate
 *  2.0               20/May/2016           pravin khot         added new function-CandidateChangePassword ,  modify mailFrom = strAdmin;
 *  2.1               17/June/2016          pravin khot         added - SendMailFromSystemEmailId()
 *  2.2               20/June/2016          pravin khot         modify code function - Send(int CurrentUserID),Send(int CurrentUserID, int JobPostingID)
    2.3               21/Oct/2016           Prasanth Kumar G    Introduced Amazon email sending Functionality
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.IO;
using System.Collections;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using System.Web.UI;
using System.Configuration;
using System.Net.Mail;
using System.Net.Mime;
using TPS360.Common.Shared;
using System.Net;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;

namespace TPS360.Web.UI.Helper
{
    public class EmailHelper : IDisposable
    {
        public EmailHelper()
        {

        }
        //veda starts
        public EmailTo To = new EmailTo();
        public EmailCc Cc = new EmailCc();
        public EmailBcc Bcc = new EmailBcc();
        public EmailAttachments Attachments = new EmailAttachments();
        public EmailLinkedResourses LinkedResources = new EmailLinkedResourses();

        private string mailFrom = String.Empty;
        private string mailSubject = String.Empty;
        private string mailBody = String.Empty;
        private string errorMessage = String.Empty;

        public String From
        {
            set { mailFrom = value; }
            get { return mailFrom; }
        }

        public String Subject
        {
            set { mailSubject = value; }
        }

        public String Body
        {
            get { return mailBody; }
            set { mailBody = value; }
        }
        //veda ends
        #region Members

        string[] arrAllPersonalInfo;
        string[] arrPersonalInfoKeyValue;

        private string emailHeaderStyle = "style='background-color:#dcdde0;font-weight:bold'";
        private string emailFontStyle = "style='font-weight:normal;color:Black;font-family:Verdana; font-size:12px;'";

        private string _fromEmail;
        public string FromEmail
        {
            get { return _fromEmail; }
            set { _fromEmail = value; }
        }

        private string _toEmail;
        public string ToEmail
        {
            get { return _toEmail; }
            set { _toEmail = value; }
        }

        private bool _showAssessment = true;
        public bool ShowAssessment
        {
            get
            {
                return _showAssessment;
            }
            set
            {
                _showAssessment = value;
            }

        }

        private bool _showScreeningQuestion = true;
        public bool ShowScreeningQuestion
        {
            get
            {
                return _showScreeningQuestion;
            }
            set
            {
                _showScreeningQuestion = value;
            }
        }

        private bool _showJobDescription = true;
        public bool ShowJobDescription
        {
            set
            {
                _showJobDescription = value;
            }
            get
            {
                return _showJobDescription;
            }
        }

        private bool _showClientJobDescription = true;
        public bool ShowClientJobDescription
        {
            set
            {
                _showClientJobDescription = value;
            }
            get
            {
                return _showClientJobDescription;
            }
        }


        private string _additionalMailMessage = string.Empty;
        public string AdditionalMailMessage
        {
            get { return _additionalMailMessage; }
            set { _additionalMailMessage = value; }
        }

        private string _senderSignature = String.Empty;
        public string SenderSignature
        {
            get { return _senderSignature; }
            set { _senderSignature = value; }
        }

        private string _signatureFile = string.Empty;
        public string SignatureFile
        {
            get { return _signatureFile; }
            set { _signatureFile = value; }
        }

        private string _companyLogo = String.Empty;
        public string CompanyLogo
        {
            get { return _companyLogo; }
            set { _companyLogo = value; }
        }

        private bool _isPreview = false;
        public bool IsPreview
        {
            get { return _isPreview; }
            set { _isPreview = value; }
        }

        private string _hiringMatrixApplicant;
        public string HiringMatrixApplicant
        {
            set { _hiringMatrixApplicant = value; }
        }

        private string _recipientName;
        public string RecipientName
        {
            set { _recipientName = value; }
        }

        private string _senderName;
        public string SenderName
        {
            set { _senderName = value; }
        }

        private string _senderPosition;
        public string SenderPosition
        {
            set { _senderPosition = value; }
        }

        private bool _showFooter = true;
        public bool ShowFooter
        {
            set { _showFooter = value; }
        }

        private string _recipientAddress;
        public string RecipientAddress
        {
            set { _recipientAddress = value; }
        }


        #region Interview Notification

        private string _interviewDate;
        public string InterviewDate
        {
            get { return _interviewDate; }
            set { _interviewDate = value; }
        }

        private string _interviewTime;
        public string InterviewTime
        {
            get { return _interviewTime; }
            set { _interviewTime = value; }
        }

        private string _interviewLocation;
        public string InterviewLocation
        {
            get { return _interviewLocation; }
            set { _interviewLocation = value; }
        }

        private string _interviewType;
        public string InterviewType
        {
            get { return _interviewType; }
            set { _interviewType = value; }
        }

        private string _interviewRequisition;
        public string InterviewRequisition
        {
            get { return _interviewRequisition; }
            set { _interviewRequisition = value; }
        }

        private string _interviewNote;
        public string InterviewNote
        {
            get { return _interviewNote; }
            set { _interviewNote = value; }
        }

        private int _jobinfoid;
        public int JobInfoId
        {
            get { return _jobinfoid; }
            set { _jobinfoid = value; }
        }
        #endregion

        #endregion

        #region Requisition Email Body

        //public string GetJobPostingDetailHTML(int jobPostingId, IFacade facade)
        //{
        //    StringBuilder jobPostingHTMLText = new StringBuilder();
        //    try
        //    {
        //        jobPostingHTMLText.Append("<table width=780px align=center border=0 cellspacing=0 cellpadding=5>");

        //        //Introduction
        //        jobPostingHTMLText.Append("    <tr>");
        //        jobPostingHTMLText.Append("        <td><div align=justify>");
        //        jobPostingHTMLText.Append("            <br/><br/>Dear &nbsp;<RecipientName>, <br/><br/>");
        //        jobPostingHTMLText.Append("            I came across your resume while searching various internet job boards or in our internal database.  I believe the following job ");
        //        jobPostingHTMLText.Append("            is a good match with your profile.");
        //        jobPostingHTMLText.Append("        </td>");
        //        jobPostingHTMLText.Append("    </tr>");

        //        //Add Additional Message
        //        if (_additionalMailMessage != "")
        //        {
        //            jobPostingHTMLText.Append("<tr>");
        //            jobPostingHTMLText.Append("    <td>");
        //            jobPostingHTMLText.Append("        <br/>Additional Notes :<br/>");
        //            jobPostingHTMLText.Append(_additionalMailMessage.Replace("\r\n", "<br />"));
        //            jobPostingHTMLText.Append("    </td>");
        //            jobPostingHTMLText.Append("</tr>");
        //        }

        //        //Middle Part: Contains All Job Posting Information
        //        jobPostingHTMLText.Append(GetJobPostingInformation(jobPostingId, facade));

        //        //Last Part of Email: Contains employee signature and offer
        //        jobPostingHTMLText.Append("    <tr>");
        //        jobPostingHTMLText.Append("        <td><div align=justify>");

        //        jobPostingHTMLText.Append("<br/>If you are interested in pursuing the above job opportunity,"
        //                    + " please forward a word version"
        //                    + " of your resume by email with salary expectations and other details.");
        //        jobPostingHTMLText.Append("         </td>");
        //        jobPostingHTMLText.Append("    </tr>");

        //        //Email footer
        //        jobPostingHTMLText.Append(GetEmailFooter());

        //        jobPostingHTMLText.Append("</table>");
        //    }
        //    catch (Exception ex)
        //    {
        //        jobPostingHTMLText.Append(ex.Message.ToString());
        //    }
        //    return jobPostingHTMLText.ToString();
        //}

        //public string GetJobPostingInformation(int jobPostingId, IFacade facade)
        //{
        //    StringBuilder jobPostingHTML = new StringBuilder();
        //    JobPosting jobPosting = facade.GetJobPostingById(jobPostingId);

        //    if (jobPosting != null)
        //    {
        //        // Requisition Details
        //        jobPostingHTML.Append("    <tr>");
        //        jobPostingHTML.Append("        <td>");
        //        jobPostingHTML.Append("            " + GetJobPostingBasicDetails(jobPosting, facade) + "" );
        //        jobPostingHTML.Append("        </td>");
        //        jobPostingHTML.Append("    </tr>");

        //        if (_showJobDescription)
        //        {
        //            // Job Description
        //            jobPostingHTML.Append("    <tr>");
        //            jobPostingHTML.Append("        <td>");
        //            jobPostingHTML.Append("            " + GetJobDescription(jobPosting, facade) + "");
        //            jobPostingHTML.Append("        </td>");
        //            jobPostingHTML.Append("    </tr>");
        //        }

        //        if (_showClientJobDescription)
        //        {
        //            // Client Job Description
        //            jobPostingHTML.Append("    <tr>");
        //            jobPostingHTML.Append("        <td>");
        //            jobPostingHTML.Append("            " + GetClientJobDescription(jobPosting, facade) + "");
        //            jobPostingHTML.Append("        </td>");
        //            jobPostingHTML.Append("    </tr>");
        //        }

        //        // Skill Set
        //        jobPostingHTML.Append("    <tr>");
        //        jobPostingHTML.Append("        <td>");
        //        jobPostingHTML.Append("            " + GetJobPostingSkillSet(jobPosting.Id, facade) + "" );
        //        jobPostingHTML.Append("        </td>");
        //        jobPostingHTML.Append("    </tr>");

        //        if (false)
        //        {
        //            //Required Assessments
        //            jobPostingHTML.Append("    <tr>");
        //            jobPostingHTML.Append("        <td>");
        //            jobPostingHTML.Append("            " + GetJobPostingAssessment(jobPosting.Id, facade) + "" );
        //            jobPostingHTML.Append("        </td>");
        //            jobPostingHTML.Append("    </tr>");
        //        }

        //        if (_showScreeningQuestion)
        //        {
        //            // Required General Screening Questions
        //            jobPostingHTML.Append("    <tr>");
        //            jobPostingHTML.Append("        <td>");
        //            jobPostingHTML.Append("            " + GetJobPostingGeneralQuestion(jobPosting.Id, facade) + "");
        //            jobPostingHTML.Append("        </td>");
        //            jobPostingHTML.Append("    </tr>");
        //        }                
        //    }
        //    return jobPostingHTML.ToString();
        //}
        public string Send() //veda
        {
            int count = 0;
            SmtpClient mailSender = new SmtpClient();

            //bool IsError = false;
            string strSMTP = string.Empty;
            string strUser = string.Empty;
            string strPwd = string.Empty;
            string strAdmin = string.Empty;//added by pravin khot on 20/May/2016
            int intPort = 25;
            //bool boolsslEnable = false;
            Hashtable MailSettingTable = null;
            Hashtable siteSettingTable = null;
            TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
            SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            if (count == 0)
            {
                if (siteSetting != null)
                {
                    siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                    strAdmin = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                    strUser = siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                    strPwd = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                    strSMTP = siteSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();
                    string strssl = siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString();
                    if (siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != null && siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != "")
                    {
                        try
                        {
                            intPort = Convert.ToInt32(siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString());
                        }
                        catch (FormatException)
                        {
                            return "SMTP Port data is incorrect in site settings";
                        }
                    }

                    mailSender = new SmtpClient(strSMTP, intPort);
                    if (strUser == string.Empty)
                    {
                        TPS360.BusinessFacade.IFacade f2 = new TPS360.BusinessFacade.Facade();
                        MemberExtendedInformation MailSetting = null;
                        Member details = f2.GetMemberByMemberEmail(strAdmin);
                        if (details != null)
                            MailSetting = f2.GetMemberExtendedInformationByMemberId(details.Id);
                        if (MailSetting == null)
                        {
                            return "From Address is absent";
                        }
                        if (MailSetting.MailSetting != null)
                        {
                            siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);
                            if (siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()] != null)
                                strUser = siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                            strPwd = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                        }

                    }
                    NetworkCredential SMTPUserInfo = new NetworkCredential(strUser, strPwd);

                    mailSender.UseDefaultCredentials = false;
                    mailSender.Credentials = SMTPUserInfo;
                    mailSender.EnableSsl = strssl == "True" ? true : false;
                    count = count + 1;
                    //}
                }
            }

            mailFrom = strUser;//commente by pravin khot on 20/May/2016
            //mailFrom = strAdmin;//added by pravin khot on 20/May/2016

            if (mailFrom != String.Empty)
            {
                if (To != null)
                {
                    if (mailSubject != String.Empty)
                    {
                        if (mailBody != String.Empty)
                        {
                            MailMessage mailMessage = new MailMessage();
                            SmtpClient sendMail = new SmtpClient();

                            MailAddress addressFrom = new MailAddress(mailFrom);
                            mailMessage.From = addressFrom;
                            foreach (String strTo in To)
                            {
                                try
                                {
                                    mailMessage.To.Add(strTo);
                                }
                                catch
                                {
                                    errorMessage = errorMessage + "Invalid Email";
                                }
                            }
                            mailMessage.Subject = mailSubject;

                            //CC
                            if (Cc != null)
                            {
                                foreach (String strCc in Cc)
                                {
                                    try
                                    {
                                        mailMessage.CC.Add(strCc);
                                    }
                                    catch
                                    {
                                        continue;
                                    }
                                }
                            }
                            //Bcc
                            if (Bcc != null)
                            {
                                foreach (String strBcc in Bcc)
                                {
                                    try
                                    {
                                        mailMessage.Bcc.Add(strBcc);
                                    }
                                    catch
                                    {
                                        continue;
                                    }
                                }
                            }

                            //Add Mail Attachments
                            if (Attachments != null)
                            {
                                foreach (String attachmentPath in Attachments)
                                {
                                    try
                                    {
                                        Attachment newAttachment = new Attachment(attachmentPath);
                                        mailMessage.Attachments.Add(newAttachment);
                                    }
                                    catch
                                    {
                                        continue;
                                    }
                                }
                            }

                            //Add Email Banner Image
                            if (LinkedResources != null)
                            {
                                AlternateView avMailMessage = AlternateView.CreateAlternateViewFromString(mailBody, null, MediaTypeNames.Text.Html);
                                foreach (ListItem maillinkedResource in LinkedResources)
                                {
                                    try
                                    {
                                        string resourcePath = maillinkedResource.Text;
                                        string contentID = maillinkedResource.Value;
                                        if (resourcePath != "" && contentID != "")
                                        {
                                            LinkedResource newLinkedResource = new LinkedResource(resourcePath);
                                            newLinkedResource.ContentId = contentID;
                                            newLinkedResource.ContentType.Name = resourcePath;
                                            avMailMessage.LinkedResources.Add(newLinkedResource);
                                        }
                                    }
                                    catch
                                    {
                                        continue;
                                    }
                                }
                                mailMessage.AlternateViews.Add(avMailMessage);

                            }
                            else
                            {
                                mailMessage.Body = mailBody;
                            }

                            mailMessage.IsBodyHtml = true;

                            //SmtpClient mailSender = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]);

                            if (count != 1)
                            {
                                mailSender = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]);
                            }
                            try
                            {
                                if (mailMessage.To.Count > 0)
                                {

                                    mailSender.Send(mailMessage);
                                    return "1";
                                    mailMessage.Attachments.Dispose();
                                }
                            }
                            catch (Exception ex)
                            {
                                //BasePage.writeLog(ex);
                                errorMessage = errorMessage + "Error:" + ex.Message;
                            }
                            return errorMessage;
                        }
                        else
                        {
                            return "No Mail Body";
                        }
                    }
                    else
                    {
                        return "No Mail Subject";
                    }
                }
                else
                {
                    return "To Address is absent";
                }
            }
            else
            {
                return "From Address is absent";
            }
        }

        //************Code added by pravin khot on 17/June/2016*************
        public string SendMailFromSystemEmailId() //veda
        {
            int count = 0;
            SmtpClient mailSender = new SmtpClient();

            //bool IsError = false;
            string strSMTP = string.Empty;
            string strUser = string.Empty;
            string strPwd = string.Empty;
            string strAdmin = string.Empty;//added by pravin khot on 20/May/2016
            int intPort = 25;
            //bool boolsslEnable = false;
            Hashtable MailSettingTable = null;
            Hashtable siteSettingTable = null;
            TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
            SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            if (count == 0)
            {
                if (siteSetting != null)
                {

                    siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                    strAdmin = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                    strUser = siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                    strPwd = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                    strSMTP = siteSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();
                    string strssl = siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString();
                    if (siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != null && siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != "")
                    {
                        try
                        {
                            intPort = Convert.ToInt32(siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString());
                        }
                        catch (FormatException)
                        {
                            return "SMTP Port data is incorrect in site settings";
                        }
                    }

                    mailSender = new SmtpClient(strSMTP, intPort);
                    if (strUser == string.Empty)
                    {
                        TPS360.BusinessFacade.IFacade f2 = new TPS360.BusinessFacade.Facade();
                        MemberExtendedInformation MailSetting = null;
                        Member details = f2.GetMemberByMemberEmail(strAdmin);
                        if (details != null)
                            MailSetting = f2.GetMemberExtendedInformationByMemberId(details.Id);
                        if (MailSetting == null)
                        {
                            return "From Address is absent";
                        }
                        if (MailSetting.MailSetting != null)
                        {
                            siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);
                            if (siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()] != null)
                                strUser = siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                            strPwd = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                        }

                    }
                    NetworkCredential SMTPUserInfo = new NetworkCredential(strUser, strPwd);

                    mailSender.UseDefaultCredentials = false;
                    mailSender.Credentials = SMTPUserInfo;
                    mailSender.EnableSsl = strssl == "True" ? true : false;
                    count = count + 1;
                    //}
                }
            }


            //mailFrom = strUser;//commente by pravin khot on 20/May/2016
            mailFrom = strAdmin;//added by pravin khot on 20/May/2016

            if (mailFrom != String.Empty)
            {
                if (To != null)
                {
                    if (mailSubject != String.Empty)
                    {
                        if (mailBody != String.Empty)
                        {
                            MailMessage mailMessage = new MailMessage();
                            SmtpClient sendMail = new SmtpClient();

                            MailAddress addressFrom = new MailAddress(mailFrom);
                            mailMessage.From = addressFrom;
                            foreach (String strTo in To)
                            {
                                try
                                {
                                    mailMessage.To.Add(strTo);
                                }
                                catch
                                {
                                    errorMessage = errorMessage + "Invalid Email";
                                }
                            }
                            mailMessage.Subject = mailSubject;

                            //CC
                            if (Cc != null)
                            {
                                foreach (String strCc in Cc)
                                {
                                    try
                                    {
                                        mailMessage.CC.Add(strCc);
                                    }
                                    catch
                                    {
                                        continue;
                                    }
                                }
                            }
                            //Bcc
                            if (Bcc != null)
                            {
                                foreach (String strBcc in Bcc)
                                {
                                    try
                                    {
                                        mailMessage.Bcc.Add(strBcc);
                                    }
                                    catch
                                    {
                                        continue;
                                    }
                                }
                            }

                            //Add Mail Attachments
                            if (Attachments != null)
                            {
                                foreach (String attachmentPath in Attachments)
                                {
                                    try
                                    {
                                        Attachment newAttachment = new Attachment(attachmentPath);
                                        mailMessage.Attachments.Add(newAttachment);
                                    }
                                    catch
                                    {
                                        continue;
                                    }
                                }
                            }

                            //Add Email Banner Image
                            if (LinkedResources != null)
                            {
                                AlternateView avMailMessage = AlternateView.CreateAlternateViewFromString(mailBody, null, MediaTypeNames.Text.Html);
                                foreach (ListItem maillinkedResource in LinkedResources)
                                {
                                    try
                                    {
                                        string resourcePath = maillinkedResource.Text;
                                        string contentID = maillinkedResource.Value;
                                        if (resourcePath != "" && contentID != "")
                                        {
                                            LinkedResource newLinkedResource = new LinkedResource(resourcePath);
                                            newLinkedResource.ContentId = contentID;
                                            newLinkedResource.ContentType.Name = resourcePath;
                                            avMailMessage.LinkedResources.Add(newLinkedResource);
                                        }
                                    }
                                    catch
                                    {
                                        continue;
                                    }
                                }
                                mailMessage.AlternateViews.Add(avMailMessage);

                            }
                            else
                            {
                                mailMessage.Body = mailBody;
                            }

                            mailMessage.IsBodyHtml = true;

                            //SmtpClient mailSender = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]);

                            if (count != 1)
                            {
                                mailSender = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]);
                            }
                            try
                            {
                                if (mailMessage.To.Count > 0)
                                {

                                    mailSender.Send(mailMessage);
                                    return "1";
                                    mailMessage.Attachments.Dispose();
                                }
                            }
                            catch (Exception ex)
                            {
                                //BasePage.writeLog(ex);
                                errorMessage = errorMessage + "Error:" + ex.Message;
                            }
                            return errorMessage;
                        }
                        else
                        {
                            return "No Mail Body";
                        }
                    }
                    else
                    {
                        return "No Mail Subject";
                    }
                }
                else
                {
                    return "To Address is absent";
                }
            }
            else
            {
                return "From Address is absent";
            }
        }

        //***************************END*********************************************
        public string Send(int CurrentUserID) //veda
        {

            int count = 0;
            if (mailFrom != String.Empty)
            {
                if (To != null)
                {

                    if (mailSubject != string.Empty || mailBody != string.Empty || Attachments.Count > 0)
                    {

                        MailMessage mailMessage = new MailMessage();

                        SmtpClient sendMail = new SmtpClient();

                        MailAddress addressFrom = new MailAddress(mailFrom);
                        mailMessage.From = addressFrom;
                        foreach (String strTo in To)
                        {
                            try
                            {
                                string[] candidatesids;
                                if (strTo.Contains(";"))
                                    candidatesids = strTo.Split(';');
                                else
                                    candidatesids = new string[] { strTo };
                                foreach (string applicantId in candidatesids)
                                {
                                    mailMessage.To.Add(applicantId);
                                }
                            }
                            catch
                            {
                                errorMessage = errorMessage + "Invalid Email";
                            }
                        }
                        mailMessage.Subject = mailSubject;

                        //CC
                        if (Cc != null)
                        {
                            foreach (String strCc in Cc)
                            {
                                try
                                {
                                    string[] candidatesids;
                                    if (strCc.Contains(";"))
                                        candidatesids = strCc.Split(';');
                                    else
                                        candidatesids = new string[] { strCc };
                                    foreach (string applicantId in candidatesids)
                                    {
                                        mailMessage.CC.Add(applicantId);
                                    }
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }
                        //Bcc
                        if (Bcc != null)
                        {
                            foreach (String strBcc in Bcc)
                            {
                                try
                                {
                                    string[] candidatesids;
                                    if (strBcc.Contains(";"))
                                        candidatesids = strBcc.Split(';');
                                    else
                                        candidatesids = new string[] { strBcc };
                                    foreach (string applicantId in candidatesids)
                                    {
                                        mailMessage.Bcc.Add(applicantId);
                                    }
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }

                        //Add Mail Attachments
                        if (Attachments != null)
                        {
                            foreach (String attachmentPath in Attachments)
                            {
                                try
                                {
                                    Attachment newAttachment = new Attachment(attachmentPath);
                                    mailMessage.Attachments.Add(newAttachment);
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }

                        //Add Email Banner Image
                        System.Web.UI.Page page = new Page();
                        if (LinkedResources != null)
                        {
                            AlternateView avMailMessage = AlternateView.CreateAlternateViewFromString(mailBody, null, MediaTypeNames.Text.Html);
                            foreach (ListItem maillinkedResource in LinkedResources)
                            {
                                try
                                {
                                    string resourcePath = maillinkedResource.Text;
                                    string contentID = maillinkedResource.Value;
                                    if (resourcePath != "" && contentID != "")
                                    {
                                        LinkedResource newLinkedResource = new LinkedResource(resourcePath);
                                        newLinkedResource.ContentId = contentID;
                                        newLinkedResource.ContentType.Name = resourcePath;
                                        avMailMessage.LinkedResources.Add(newLinkedResource);
                                    }
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                            mailMessage.AlternateViews.Add(avMailMessage);
                        }
                        else
                        {
                            mailMessage.Body = mailBody;
                        }

                        mailMessage.IsBodyHtml = true;

                        //SmtpClient mailSender = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]);
                        SmtpClient mailSender = new SmtpClient();
                        //bool IsError = false;
                        string strSMTP = string.Empty;
                        string strUser = string.Empty;
                        string strPwd = string.Empty;
                        string strssl = string.Empty;
                        int intPort = 25;
                        //bool boolsslEnable = false;
                        Hashtable MailSettingTable = null;
                        Hashtable siteSettingTable = null;


                        TPS360.BusinessFacade.Facade f = new TPS360.BusinessFacade.Facade();
                        MemberExtendedInformation MailSetting = f.GetMemberExtendedInformationByMemberId(CurrentUserID);

                        TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
                        SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                        //*************code commneted and added by pravin khot on 20/June/2016***********
                        //siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                        siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);
                        //***************************END*********************************
                        if (MailSetting.MailSetting != null)
                        {
                            MailSettingTable = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);
                            try
                            {
                                strSMTP = siteSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();
                                //added for Appsettings mail configuration
                                strUser = siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                                strPwd = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();

                                strssl = siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString();
                            }
                            catch (Exception ex)
                            {

                            }

                            if (strSMTP != null && strSMTP != "")
                            {
                                if (MailSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != null && MailSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != "")
                                {
                                    try
                                    {
                                        intPort = Convert.ToInt32(siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString());
                                    }
                                    catch (FormatException)
                                    {
                                        return "SMTP Port data is incorrect in settings";
                                    }
                                }
                                mailSender = new SmtpClient(strSMTP, intPort);
                                NetworkCredential SMTPUserInfo = new NetworkCredential(strUser, strPwd);

                                mailSender.UseDefaultCredentials = false;
                                mailSender.Credentials = SMTPUserInfo;
                                mailSender.EnableSsl = strssl == "True" ? true : false;
                                count = count + 1;
                            }
                            //intPort = MailSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString();
                            //mailSender.Host = MailSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();
                        }
                        else
                        {
                            return "Mail Settings not available."; //added by pravin khot on 20/June/2016
                        }

                        //if (count == 0)
                        //{
                        //    if (siteSetting != null)
                        //    {
                        //        //mailSender = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]);

                        //        //SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);

                        //        //if (siteSetting != null)
                        //        //{
                        //        siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                        //        strUser = siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                        //        strPwd = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                        //        strSMTP = siteSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();
                        //        strssl = siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString();
                        //        if (siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != null && siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != "")
                        //        {
                        //            try
                        //            {
                        //                intPort = Convert.ToInt32(siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString());
                        //            }
                        //            catch (FormatException)
                        //            {
                        //                return "SMTP Port data is incorrect in site settings";
                        //            }
                        //        }

                        //        mailSender = new SmtpClient(strSMTP, intPort);
                        //        NetworkCredential SMTPUserInfo = new NetworkCredential(strUser, strPwd);

                        //        mailSender.UseDefaultCredentials = false;
                        //        mailSender.Credentials = SMTPUserInfo;
                        //        if (strssl == "True")
                        //            mailSender.EnableSsl = true;
                        //        else
                        //            mailSender.EnableSsl = false;
                        //        count = count + 2;
                        //        //}
                        //    }
                        //}
                        if (count != 1 && count != 2)
                        {
                            //mailSender = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]); //commented by pravin khot on 20/June/2016
                        }
                        try
                        {
                            IFacade facade = new Facade();
                            Member mem = facade.GetMemberById(CurrentUserID);
                            MailAddress add = new MailAddress(mailMessage.From.ToString(), mem.FirstName + " " + mem.LastName);

                            if (mailMessage.To.Count > 0 || mailMessage.Bcc.Count > 0 || mailMessage.CC.Count > 0)
                            {
                                mailMessage.From = add;
                                mailMessage.Sender = add;
                                mailSender.Send(mailMessage);
                                mailMessage.Attachments.Dispose();
                                return "1";

                            }
                        }
                        catch (Exception ex)
                        {
                            BasePage.writeLog(ex);
                            errorMessage = errorMessage + "Error:" + ex.Message;
                        }
                        return errorMessage;
                    }
                    else
                    {
                        return "No Mail subject or body or attachment";
                    }

                }
                else
                {
                    return "To Address is absent";
                }
            }
            else
            {
                return "From Address is absent";
            }
        }

        public string Send(int CurrentUserID, int JobPostingID) //veda
        {

            int count = 0;
            if (mailFrom != String.Empty)
            {
                if (To != null)
                {

                    if (mailSubject != string.Empty || mailBody != string.Empty || Attachments.Count > 0)
                    {

                        MailMessage mailMessage = new MailMessage();

                        SmtpClient sendMail = new SmtpClient();

                        MailAddress addressFrom = new MailAddress(mailFrom);
                        mailMessage.From = addressFrom;
                        foreach (String strTo in To)
                        {
                            try
                            {
                                mailMessage.To.Add(strTo);
                            }
                            catch
                            {
                                errorMessage = errorMessage + "Invalid Email";
                            }
                        }
                        mailMessage.Subject = mailSubject;

                        //CC
                        if (Cc != null)
                        {
                            foreach (String strCc in Cc)
                            {
                                try
                                {
                                    mailMessage.CC.Add(strCc);
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }
                        //Bcc
                        if (Bcc != null)
                        {
                            foreach (String strBcc in Bcc)
                            {
                                try
                                {
                                    mailMessage.Bcc.Add(strBcc);
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }

                        //Add Mail Attachments
                        if (Attachments != null)
                        {
                            foreach (String attachmentPath in Attachments)
                            {
                                try
                                {
                                    Attachment newAttachment = new Attachment(attachmentPath);
                                    mailMessage.Attachments.Add(newAttachment);
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }

                        //Add Email Banner Image
                        System.Web.UI.Page page = new Page();

                        mailMessage.Body = mailBody;

                        mailMessage.IsBodyHtml = true;

                        //SmtpClient mailSender = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]);
                        SmtpClient mailSender = new SmtpClient();
                        //bool IsError = false;
                        string strSMTP = string.Empty;
                        string strUser = string.Empty;
                        string strPwd = string.Empty;
                        string strssl = string.Empty;
                        int intPort = 25;
                        //bool boolsslEnable = false;
                        Hashtable MailSettingTable = null;
                        Hashtable siteSettingTable = null;

                        TPS360.BusinessFacade.Facade f = new TPS360.BusinessFacade.Facade();
                        MemberExtendedInformation MailSetting = f.GetMemberExtendedInformationByMemberId(CurrentUserID);

                        TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
                        SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                        //*************code commneted and added by pravin khot on 20/June/2016***********
                        //siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                        siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);
                        //***************************END*********************************                       
                        if (MailSetting.MailSetting != null)
                        {
                            MailSettingTable = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);
                            try
                            {
                                strSMTP = siteSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();
                                //added for Appsettings mail configuration
                                strUser = siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                                strPwd = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                                strssl = siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString();
                            }
                            catch (Exception ex)
                            {

                            }

                            if (strSMTP != null && strSMTP != "")
                            {
                                if (MailSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != null && MailSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != "")
                                {
                                    try
                                    {
                                        //intPort = Convert.ToInt32(siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString());
                                    }
                                    catch (FormatException)
                                    {
                                        return "SMTP Port data is incorrect in settings";
                                    }
                                }
                                mailSender = new SmtpClient(strSMTP, intPort);
                                NetworkCredential SMTPUserInfo = new NetworkCredential(strUser, strPwd);

                                mailSender.UseDefaultCredentials = false;
                                mailSender.Credentials = SMTPUserInfo;
                                mailSender.EnableSsl = strssl == "True" ? true : false;
                                count = count + 1;
                            }
                            //intPort = MailSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString();
                            //mailSender.Host = MailSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();
                        }
                        else
                        {
                            return "Mail Settings not available."; //added by pravin khot on 20/June/2016
                        }

                        if (count != 1 && count != 2)
                        {
                            //mailSender = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]);//commented by pravin khot on 20/June/2016
                        }
                        try
                        {
                            IFacade facade = new Facade();
                            Member mem = facade.GetMemberById(CurrentUserID);
                            MailAddress add = new MailAddress(mailMessage.From.ToString(), mem.FirstName + " " + mem.LastName);

                            if (mailMessage.To.Count > 0 || mailMessage.Bcc.Count > 0 || mailMessage.CC.Count > 0)
                            {
                                mailMessage.From = add;
                                mailMessage.Sender = add;
                                mailSender.Send(mailMessage);
                                mailMessage.Attachments.Dispose();
                                return "1";

                            }
                        }
                        catch (Exception ex)
                        {
                            BasePage.writeLog(ex);
                            errorMessage = errorMessage + "Error:" + ex.Message;
                        }
                        return errorMessage;
                    }
                    else
                    {
                        return "No Mail subject or body or attachment";
                    }

                }
                else
                {
                    return "To Address is absent";
                }
            }
            else
            {
                return "From Address is absent";
            }
        }

        private string GetJobPostingBasicDetails(JobPosting jobPosting, IFacade facade)
        {
            StringBuilder jobPostingDetailsHTML = new StringBuilder();
            string startDate = string.Empty;

            //Start Date
            if (string.IsNullOrEmpty(jobPosting.StartDate))
            {
                startDate = "ASAP";
            }
            else
            {
                startDate = jobPosting.StartDate.ToString();
            }

            //Education
            string education = string.Empty;
            education = MiscUtil.SplitValues(jobPosting.RequiredDegreeLookupId, ',', facade);  //MiscUtil.GetLookupNameById(Convert .ToInt32 ( jobPosting.RequiredDegreeLookupId), facade);

            //Salary

            //1.0 start

            string salary = string.Empty;

            if (jobPosting.PayRate == "Open")
            {
                salary = jobPosting.PayRate;
            }
            else
            {
                salary = jobPosting.PayRate + "  " + jobPosting.PayCycle;

                GenericLookup _PayRateCurrencyLookup = facade.GetGenericLookupById(jobPosting.PayRateCurrencyLookupId);
                if (_PayRateCurrencyLookup != null)
                {
                    salary = jobPosting.PayRate + " " + _PayRateCurrencyLookup.Name + " " + jobPosting.PayCycle; //1.0
                }
                //1.0 End
            }

            //Benefits
            string benefits = MiscUtil.GetBenifitsById(jobPosting.OtherBenefits, facade);

            //Employment Duration
            string duration = string.Empty;
            if (jobPosting.JobDurationLookupId != string.Empty)
            {

                if (Convert.ToInt32(jobPosting.JobDurationMonth) > 0)
                {
                    duration = MiscUtil.SplitValues(jobPosting.JobDurationLookupId, ',', facade) + " (" + MiscUtil.GetLookupNameById(Convert.ToInt32(jobPosting.JobDurationMonth), facade) + ")";
                    //MiscUtil.GetLookupNameById(Convert  .ToInt32 ( jobPosting.JobDurationLookupId), facade) + " (" + MiscUtil.GetLookupNameById(Convert.ToInt32(jobPosting.JobDurationMonth), facade) + ")";
                }
                else
                {
                    duration = MiscUtil.SplitValues(jobPosting.JobDurationLookupId, ',', facade);
                    //  duration = MiscUtil.GetLookupNameById(Convert.ToInt32(jobPosting.JobDurationLookupId), facade);
                }
            }
            else
            {
                duration = "No Preference";
            }

            //Travel Required

            string travel = "";

            /*1.0 start */

            /*  if (jobPosting.TravelRequiredPercent.ToString() == "0")
              {
                  travel = jobPosting.TravelRequired.ToString();
              }
              else if (jobPosting.TravelRequiredPercent.ToString() == "25")
              {
                  travel = jobPosting.TravelRequired.ToString() + jobPosting.TravelRequiredPercent.ToString() + "%";
              } */

            if (jobPosting.TravelRequired.ToString() == "True")
            {
                travel = "Yes";
            }
            else
            {
                travel = "No";
            }


            if (jobPosting.TravelRequiredPercent.ToString() != "0")
            {
                travel = "Yes " + "(" + jobPosting.TravelRequiredPercent.ToString() + "%" + ")";
            }
            /* 1.0  End */

            jobPostingDetailsHTML.Append("<table cellpadding=0 cellspacing=0 border=0 width=100%>");
            jobPostingDetailsHTML.Append("<tr>");
            jobPostingDetailsHTML.Append("    <td colspan=2 width=98%>");
            jobPostingDetailsHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td height=5 " + emailHeaderStyle + ">Requisition Details</td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("        </table>");
            jobPostingDetailsHTML.Append("    </td>");
            jobPostingDetailsHTML.Append("</tr>");

            jobPostingDetailsHTML.Append("<tr>");
            jobPostingDetailsHTML.Append("    <td colspan=2 valign=top align=left width=98%>");
            jobPostingDetailsHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td width=21%><b>Job Title<b></td>");//1.6
            jobPostingDetailsHTML.Append("                <td>" + jobPosting.JobTitle.ToString() + "</td>");
            jobPostingDetailsHTML.Append("                <td width=21%><b>No. of Openings<b></td>");
            jobPostingDetailsHTML.Append("                <td>" + jobPosting.NoOfOpenings.ToString() + "</td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td width=21%><b>City<b></td>");//1.6
            jobPostingDetailsHTML.Append("                <td>" + jobPosting.City.ToString() + "</td>");
            jobPostingDetailsHTML.Append("                <td width=21%><b>State<b></td>");
            jobPostingDetailsHTML.Append("                <td>" + MiscUtil.GetStateNameById(jobPosting.StateId, facade) + "</td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td width=21%><b>Start Date<b></td>");//1.6
            jobPostingDetailsHTML.Append("                <td nowrap=nowrap>" + startDate + "</td>");
            jobPostingDetailsHTML.Append("                <td width=21%><b>Experience Required (yrs)<b></td>");//1.6
            jobPostingDetailsHTML.Append("                <td nowrap=nowrap><b>Min</b>&nbsp;" + jobPosting.MinExpRequired.ToString() + "&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<b>Max</b>&nbsp;" + jobPosting.MaxExpRequired.ToString() + "</td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td width=21%><b>Educational Qualification<b></td>");//1.6
            jobPostingDetailsHTML.Append("                <td>" + education + "</td>");
            jobPostingDetailsHTML.Append("                <td width=21%><b>Salary<b></td>");//1.6
            jobPostingDetailsHTML.Append("                <td>" + salary + "</td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td width=21% valign=top align=left><b>Benefits<b></td>");
            jobPostingDetailsHTML.Append("                <td colspan=3><div align=justify>" + benefits + "</div></td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("            <tr>");
            jobPostingDetailsHTML.Append("                <td width=21%><b>Employment Length<b></td>");//1.6
            jobPostingDetailsHTML.Append("                <td>" + duration + "</td>");
            jobPostingDetailsHTML.Append("                <td width=21%><b>Travel Required<b></td>");//1.6
            jobPostingDetailsHTML.Append("                <td>" + travel + "</td>");
            jobPostingDetailsHTML.Append("            </tr>");
            jobPostingDetailsHTML.Append("        </table>");
            jobPostingDetailsHTML.Append("    </td>");
            jobPostingDetailsHTML.Append("</tr>");
            jobPostingDetailsHTML.Append("</table>");

            return jobPostingDetailsHTML.ToString();
        }

        private string GetJobDescription(JobPosting jobPosting, IFacade facade)
        {
            StringBuilder jobDescriptionHTML = new StringBuilder();
            jobDescriptionHTML.Append("<table cellpadding=0 cellspacing=0 border=0 width=100%>");
            jobDescriptionHTML.Append("<tr>");
            jobDescriptionHTML.Append("    <td>");
            jobDescriptionHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            jobDescriptionHTML.Append("            <tr>");
            jobDescriptionHTML.Append("                <td colspan=2 height=5 " + emailHeaderStyle + ">Job Description</td>");
            jobDescriptionHTML.Append("            </tr>");
            jobDescriptionHTML.Append("            <tr>");
            jobDescriptionHTML.Append("                <td colspan=2 valign=top>");
            jobDescriptionHTML.Append("                        <div align=justify>" + HttpUtility.HtmlDecode(jobPosting.JobDescription.ToString()) + "</div>");
            jobDescriptionHTML.Append("                </td>");
            jobDescriptionHTML.Append("            </tr>");
            jobDescriptionHTML.Append("        </table>");
            jobDescriptionHTML.Append("    </td>");
            jobDescriptionHTML.Append("</tr>");
            jobDescriptionHTML.Append("</table>");
            return jobDescriptionHTML.ToString();
        }

        private string GetClientJobDescription(JobPosting jobPosting, IFacade facade)
        {
            StringBuilder jobDescriptionHTML = new StringBuilder();
            jobDescriptionHTML.Append("<table cellpadding=0 cellspacing=0 border=0 width=100%>");
            jobDescriptionHTML.Append("<tr>");
            jobDescriptionHTML.Append("    <td>");
            jobDescriptionHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            jobDescriptionHTML.Append("            <tr>");
            //jobDescriptionHTML.Append("                <td colspan=2 height=5 " + emailHeaderStyle + ">Client Job Description</td>");
            jobDescriptionHTML.Append("                <td colspan=2 height=5 " + emailHeaderStyle + ">Job Description</td>");          //1.4
            jobDescriptionHTML.Append("            </tr>");
            jobDescriptionHTML.Append("            <tr>");
            jobDescriptionHTML.Append("                <td colspan=2 valign=top>");
            //jobDescriptionHTML.Append("                        <div align=justify>" + HttpUtility.HtmlDecode(jobPosting.ClientJobDescription.ToString()) + "</div>");
            jobDescriptionHTML.Append("                        <div align=justify>" + HttpUtility.HtmlDecode(jobPosting.JobDescription.ToString()) + "</div>");         //1.4
            jobDescriptionHTML.Append("                </td>");
            jobDescriptionHTML.Append("            </tr>");
            jobDescriptionHTML.Append("        </table>");
            jobDescriptionHTML.Append("    </td>");
            jobDescriptionHTML.Append("</tr>");
            jobDescriptionHTML.Append("</table>");
            return jobDescriptionHTML.ToString();
        }

        //private string GetJobPostingSkillSet(int jobPostingId, IFacade facade)
        //{            
        //    StringBuilder skillSetHTML = new StringBuilder();

        //    skillSetHTML.Append("<table cellpadding=0 cellspacing=0 border=0 width=100%>");
        //    skillSetHTML.Append("<tr>");
        //    skillSetHTML.Append("    <td>");
        //    skillSetHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
        //    skillSetHTML.Append("            <tr>");
        //    skillSetHTML.Append("                <td colspan=3 height=5 " + emailHeaderStyle + ">Required Skills</td>");
        //    skillSetHTML.Append("            </tr>");
        //    skillSetHTML.Append("            <tr>");
        //    skillSetHTML.Append("                <td width=50%><b>Skill</b></td>");
        //    skillSetHTML.Append("                <td width=25% align=center><b>Experience (Yrs)</b></td>");
        //    skillSetHTML.Append("                <td width=25% align=center><b>Proficiency</b></td>");
        //    skillSetHTML.Append("            </tr>");

        //    IList<JobPostingSkillSet> jobPostingSkillSetList = facade.GetAllJobPostingSkillSetByJobPostingId(jobPostingId);

        //    if (jobPostingSkillSetList != null)
        //    {
        //        foreach (JobPostingSkillSet jobPostingSkillSet in jobPostingSkillSetList)
        //        {
        //            if (jobPostingSkillSet != null)
        //            {
        //                skillSetHTML.Append("<tr>");
        //                skillSetHTML.Append("    <td>" + MiscUtil.GetSkillNameById(jobPostingSkillSet.SkillId, facade) + "</td>");
        //                skillSetHTML.Append("    <td align=center>" + jobPostingSkillSet.NoOfYearsExp.ToString() + "</td>");
        //                //    skillSetHTML.Append("    <td align=center>" + MiscUtil.GetLookupNameById(jobPostingSkillSet.ProficiencyLookupId, facade) + "</td>"); //1.0 
        //                skillSetHTML.Append("    <td align=center>" + jobPostingSkillSet.ProficiencyLookupId.ToString() + "</td>");
        //                skillSetHTML.Append("</tr>");
        //            }
        //        }
        //    }

        //    skillSetHTML.Append("        </table>");
        //    skillSetHTML.Append("    </td>");
        //    skillSetHTML.Append("</tr>");
        //    skillSetHTML.Append("</table>");
        //    return skillSetHTML.ToString();
        //}

        //private string GetJobPostingAssessment(int jobPostingId, IFacade facade)
        //{
        //    StringBuilder assessmentsHTML = new StringBuilder();

        //    assessmentsHTML.Append("<table cellpadding=0 cellspacing=0 border=0 width=100%>");
        //    assessmentsHTML.Append("<tr>");
        //    assessmentsHTML.Append("    <td>");
        //    assessmentsHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
        //    assessmentsHTML.Append("            <tr>");
        //    assessmentsHTML.Append("                <td height=5 " + emailHeaderStyle + ">Required Assessment Tests</td>");
        //    assessmentsHTML.Append("            </tr>");

        //    IList<JobPostingAssessment> jobPostingAssessmentList = facade.GetAllJobPostingAssessmentByJobPostingId(jobPostingId);

        //    if (jobPostingAssessmentList != null)// 0.2
        //    {
        //        if (jobPostingAssessmentList.Count > 0)
        //        {
        //            foreach (JobPostingAssessment jobPostingAssessment in jobPostingAssessmentList)
        //            {
        //                if (jobPostingAssessment != null)
        //                {
        //                    assessmentsHTML.Append("<tr>");
        //                    assessmentsHTML.Append("    <td align=left>" + MiscUtil.GetTestNameById(jobPostingAssessment.TestMasterId, facade) + "</td>");
        //                    assessmentsHTML.Append("</tr>");
        //                }
        //            }
        //        }
        //    }

        //    assessmentsHTML.Append("        </table>");
        //    assessmentsHTML.Append("    </td>");
        //    assessmentsHTML.Append("</tr>");
        //    assessmentsHTML.Append("</table>");

        //    return assessmentsHTML.ToString();
        //}

        //private string GetJobPostingGeneralQuestion(int jobPostingId, IFacade facade)
        //{
        //    StringBuilder questionsHTML = new StringBuilder();

        //    questionsHTML.Append("<table cellpadding=0 cellspacing=0 border=0 width=100%>");
        //    questionsHTML.Append("<tr>");
        //    questionsHTML.Append("    <td>");
        //    questionsHTML.Append("        <table width=100% border=0 cellspacing=1 cellpadding=5>");
        //    questionsHTML.Append("            <tr>");
        //    questionsHTML.Append("                <td colspan=3 height=5 " + emailHeaderStyle + ">Required General Screening Questions</td>");
        //    questionsHTML.Append("            </tr>");

        //    IList<JobPostingGeneralQuestion> jobPostingGeneralQuestionList = facade.GetAllJobPostingGeneralQuestionByJobPostingId(jobPostingId, JobPostingQuestionType.GeneralQuestion.ToString());

        //    if (jobPostingGeneralQuestionList != null)
        //    {
        //        foreach (JobPostingGeneralQuestion jobPostingGeneralQuestion in jobPostingGeneralQuestionList)
        //        {
        //            if (jobPostingGeneralQuestion != null)
        //            {
        //                questionsHTML.Append("<tr>");
        //                questionsHTML.Append("   <td colspan=3 align=left>" + jobPostingGeneralQuestion.Question + "</td>");
        //                questionsHTML.Append("</tr>");
        //            }
        //        }
        //    }

        //    questionsHTML.Append("        </table>");
        //    questionsHTML.Append("    </td>");
        //    questionsHTML.Append("</tr>");
        //    questionsHTML.Append("</table>");

        //    return questionsHTML.ToString();
        //}
        public void Dispose()
        {


        }
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (!disposed)
        //    {
        //        if (disposing)
        //        {
        //            this.Attachments = null;
        //                A
        //        }

        //        disposed = true;
        //    }
        //}
        #endregion

        #region Member Email Body

        public string GetHiringMatrixHTML(IFacade facade)
        {
            StringBuilder hiringMatrixHTML = new StringBuilder();

            hiringMatrixHTML.Append("<table width=780px align=center border=0 cellspacing=0 cellpadding=5>");

            //Introduction
            hiringMatrixHTML.Append("    <tr>");
            hiringMatrixHTML.Append("        <td><div align=justify>");
            hiringMatrixHTML.Append("            <br/><br/>Dear &nbsp;" + _recipientName + ", <br/><br/>");
            hiringMatrixHTML.Append("            My name is " + _senderName + " and I'm " + _senderPosition + " with Delphi Staffing. Please review the following short-listed candidates profiles. ");
            hiringMatrixHTML.Append("            Based on your feedback and interest I can arrange and coordinate the interviews.");
            hiringMatrixHTML.Append("        </div></td>");
            hiringMatrixHTML.Append("    </tr>");

            //0.3 starts here

            //Add Additional Message
            //if (_additionalMailMessage != "")
            //{
            //    hiringMatrixHTML.Append("<tr>");
            //    hiringMatrixHTML.Append("    <td>");
            //    hiringMatrixHTML.Append("        <br/>Additional Notes :<br/>");
            //    hiringMatrixHTML.Append(_additionalMailMessage.Replace("\r\n", "<br />"));
            //    hiringMatrixHTML.Append("    </td>");
            //    hiringMatrixHTML.Append("</tr>");
            //}

            //0.3 ends here

            hiringMatrixHTML.Append("</br>");
            //Middle Part: Contains All Job Posting Information
            hiringMatrixHTML.Append(GetApplicantHiringMatrix(facade));

            hiringMatrixHTML.Append("</br>");
            //Email footer
            hiringMatrixHTML.Append(GetEmailFooter());

            hiringMatrixHTML.Append("</table>");

            return hiringMatrixHTML.ToString();
        }

        public string GetApplicantHiringMatrix(IFacade facade)
        {
            StringBuilder applicantHiringMatrix = new StringBuilder();
            int id = 0;
            string[] hiringApplicant = _hiringMatrixApplicant.Split(',');

            foreach (string applicantId in hiringApplicant)
            {
                Int32.TryParse(applicantId, out id);
                if (id > 0)
                {
                    Member member = facade.GetMemberById(id);

                    if (member != null)
                    {
                        applicantHiringMatrix.Append("<tr><td><table width=780px align=center border=0 cellspacing=0 cellpadding=5>");
                        applicantHiringMatrix.Append("    <tr>");
                        applicantHiringMatrix.Append("        <td width=25% valign=top align=left><b>Name : " + member.FirstName + " " + member.LastName + "</b></td>");    //0.3
                        MemberExtendedInformation memberExtendedInformation = facade.GetMemberExtendedInformationByMemberId(id);
                        if (memberExtendedInformation != null)
                        {
                            applicantHiringMatrix.Append("        <td width=25% valign=top align=left><b>Job Title: " + memberExtendedInformation.CurrentPosition + "</b></td>");           //0.3
                            applicantHiringMatrix.Append("        <td width=25% valign=top align=left><b>Years of Exp: " + memberExtendedInformation.TotalExperienceYears + "</b></td>");   //0.3
                        }
                        else
                        {
                            applicantHiringMatrix.Append("        <td width=25% valign=top align=left><b>Job Title: </b></td>");        //0.3
                            applicantHiringMatrix.Append("        <td width=25% valign=top align=left><b>Years of Exp: </b></td>");     //0.3
                        }
                        MemberDetail memberDetail = facade.GetMemberDetailByMemberId(id);
                        if (memberDetail != null)
                        {
                            applicantHiringMatrix.Append("        <td width=25% valign=top align=left><b>Location: " + memberDetail.CurrentCity + "</b></td>");     //0.3
                        }
                        else
                        {
                            applicantHiringMatrix.Append("        <td width=25% valign=top align=left><b>Location: </b></td>");     //0.3
                        }
                        applicantHiringMatrix.Append("    </tr>");
                        applicantHiringMatrix.Append("    <tr>");
                        applicantHiringMatrix.Append("        <td colspan=4 height=5 " + emailHeaderStyle + "><SPACER height=1 width=1 type=block></td>");
                        applicantHiringMatrix.Append("    </tr>");
                        applicantHiringMatrix.Append("    <tr>");
                        applicantHiringMatrix.Append("        <td width=25% valign=top align=left>Skillset:</td>");
                        applicantHiringMatrix.Append("        <td width=75% colspan='3' valign=top align=left>" + MiscUtil.GetMemberSkillSetList(id, facade) + "</td>");
                        applicantHiringMatrix.Append("    </tr>");
                        applicantHiringMatrix.Append("    <tr>");
                        applicantHiringMatrix.Append("        <td width=25% valign=top align=left>Summary:</td>");
                        MemberObjectiveAndSummary memberObjectiveAndSummary = facade.GetMemberObjectiveAndSummaryByMemberId(id);
                        if (memberObjectiveAndSummary != null)
                        {
                            applicantHiringMatrix.Append("        <td width=75% colspan='3' valign=top align=left>" + HttpUtility.HtmlDecode(Regex.Replace(memberObjectiveAndSummary.Objective.Trim(), @"<(.|\n)*?>", string.Empty)) + "</td>");
                        }
                        else
                        {
                            applicantHiringMatrix.Append("        <td width=75% colspan='3' valign=top align=left></td>");
                        }
                        applicantHiringMatrix.Append("    </tr>");
                        applicantHiringMatrix.Append("    <tr>");
                        if (memberExtendedInformation != null)
                        {
                            applicantHiringMatrix.Append("        <td colspan='4'  valign=top align=left><b>Rate: </b>" + memberExtendedInformation.CurrentYearlyRate.ToString() + " Yearly" + ",&nbsp;&nbsp;&nbsp;&nbsp;<b>Available: </b>" + MiscUtil.GetLookupNameById(memberExtendedInformation.Availability, facade) + ",&nbsp;&nbsp;&nbsp;&nbsp;<b>Relocation: </b>" + (memberExtendedInformation != null && memberExtendedInformation.Relocation ? "Yes" : "No") + ",&nbsp;&nbsp;&nbsp;&nbsp;<a href='mailto:" + member.PrimaryEmail + "'>Receive Resume</a> </td>");
                        }
                        else
                        {
                            applicantHiringMatrix.Append("        <td colspan='4'  valign=top align=left><b>Rate: </b>" + " " + ",&nbsp;&nbsp;&nbsp;&nbsp;<b>Available: </b>" + " " + ",&nbsp;&nbsp;&nbsp;&nbsp;<b>Relocation: </b>" + " " + ",&nbsp;&nbsp;&nbsp;&nbsp;<a href='mailto:" + member.PrimaryEmail + "'>Receive Resume</a> </td>");
                        }
                        applicantHiringMatrix.Append("    </tr>");
                        applicantHiringMatrix.Append("</table></td></tr>");
                    }
                }
            }

            return applicantHiringMatrix.ToString();
        }

        //public string GetInterviewRequestHTML(Page currentPage,int jobPostingId,string applicantId, IFacade facade)
        //{
        //    StringBuilder interviewRequestHTML = new StringBuilder();

        //    interviewRequestHTML.Append("<table width=780px align=center border=0 cellspacing=0 cellpadding=5>");
        //    JobPosting jobPosting = facade.GetJobPostingById(jobPostingId);
        //    interviewRequestHTML.Append("    <tr>");
        //    interviewRequestHTML.Append("        <td><div align=justify>");
        //    interviewRequestHTML.Append("            <br/><br/>Dear &nbsp;" + _recipientName + ", <br/><br/><span>");
        //    interviewRequestHTML.Append(" We would like to request that you take this online self assessment test. It will take"
        //                     + " approximately 3-5 minutes, and your feedback will help us to know if your qualifications"
        //                     + " match with the following job opening. If you qualify, a senior recruiter will contact you ASAP."
        //                     + "<br/>Click here to start your self assessment: <a href='" + UrlConstants.GetCurrentDomainName(currentPage) + "ScreeningInterview.aspx?jid=" + jobPostingId.ToString() + "&mid=" + applicantId + "&option=assessment' target='_blank'><b>(" + _recipientName + ") for " + jobPosting.JobTitle + " - " + jobPosting.City + ", " + MiscUtil.GetStateNameById(jobPosting.StateId, facade) + @". </b></a>"
        //                     + "<br/><br/>If you have already found a job, are not available anymore, or would not like to receive anymore emails from us, then <a href='" + UrlConstants.GetCurrentDomainName(currentPage) + "ScreeningInterview.aspx?jid=" + jobPostingId + "&mid=" + applicantId + "&option=remove' target='_blank'><b>click here</b></a> to remove yourself from our list.");
        //    interviewRequestHTML.Append("        </td>");
        //    interviewRequestHTML.Append("    </tr>");

        //    //Middle Part: Contains All Job Posting Information
        //    _showScreeningQuestion = false;
        //    _showAssessment = false;
        //    interviewRequestHTML.Append(GetJobPostingInformation(jobPostingId, facade));

        //    interviewRequestHTML.Append("</br>");
        //    //Email footer
        //    interviewRequestHTML.Append(GetEmailFooter());

        //    interviewRequestHTML.Append("</table>");            

        //    return interviewRequestHTML.ToString();
        //}

        //public string GetSubmittedApplicantsResumesEmailBody(Page currentPage,CheckBoxList chkBoxListShowHideInfo, GridView grdViewUploadedResume, CheckBox chkRequisitionDescription, int jobPostingId, string clientRate, string ReqRate, string appDesiredRate, string subMissionRate, IFacade facade)
        //{
        //    int id = 0;
        //    StringBuilder applicantsResumesEmailHTML = new StringBuilder();

        //    applicantsResumesEmailHTML.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");

        //    //Add Intorductory Information and Cover Message

        //// 10320

        //    //applicantsResumesEmailHTML.Append("    <tr>");
        //    //applicantsResumesEmailHTML.Append("        <td align=left colspan=2><br /><br />Dear&nbsp;" + _recipientName + ",<br /><br /></td>");
        //    //applicantsResumesEmailHTML.Append("    </tr>");

        //// 10320

        //    applicantsResumesEmailHTML.Append("    <tr>");
        //    //applicantsResumesEmailHTML.Append("        <td align=left colspan=2>" + _additionalMailMessage.Replace("\r\n", "<br />") + "<br /></td>");
        //    applicantsResumesEmailHTML.Append("        <td align=left colspan=2>" + _additionalMailMessage.Replace("\r\n", "<br />") + "<br /></td>");
        //    applicantsResumesEmailHTML.Append("    </tr>");
        //    // 0.1 starts here
        //    //applicantsResumesEmailHTML.Append("    <tr>");
        //    //applicantsResumesEmailHTML.Append("        <td width=21% align=left>Client Rate:</td>");
        //    //applicantsResumesEmailHTML.Append("        <td align=left>" + clientRate + "</td>");
        //    //applicantsResumesEmailHTML.Append("    </tr>");
        //    //applicantsResumesEmailHTML.Append("    <tr>");
        //    //applicantsResumesEmailHTML.Append("        <td width=21% align=left>Requisition Rate:</td>");
        //    //applicantsResumesEmailHTML.Append("        <td align=left>" + ReqRate + "</td>");
        //    //applicantsResumesEmailHTML.Append("    </tr>");
        //    //applicantsResumesEmailHTML.Append("    <tr>");
        //    //applicantsResumesEmailHTML.Append("        <td width=21% align=left>Applicant Desired Rate:</td>");
        //    //applicantsResumesEmailHTML.Append("        <td align=left>" + appDesiredRate + "</td>");
        //    //applicantsResumesEmailHTML.Append("    </tr>");
        //    //applicantsResumesEmailHTML.Append("    <tr>");
        //    //applicantsResumesEmailHTML.Append("        <td width=21% align=left>Submission Rate:</td>");
        //    //applicantsResumesEmailHTML.Append("        <td align=left>" + subMissionRate + "</td>");
        //    //applicantsResumesEmailHTML.Append("    </tr>");
        //    // 0.1 ends here
        //    applicantsResumesEmailHTML.Append("</table>");

        //    string[] hiringApplicant = _hiringMatrixApplicant.Split(',');

        //    foreach (string applicantId in hiringApplicant)
        //    {
        //        Int32.TryParse(applicantId,out id);
        //        if (id > 0 )
        //        {
        //            Member member = facade.GetMemberById(id);

        //            applicantsResumesEmailHTML.Append("<br/><table width=780 border=1 align=center cellpadding=0 cellspacing=0><tr><td>");
        //            if (member != null)
        //            {
        //                //Add Personal Information                        
        //                applicantsResumesEmailHTML.Append(GetApplicantPersonalInformationEmailBody(chkBoxListShowHideInfo,member.Id, facade));                        

        //                //Add Uploaded Resumes
        //              //  applicantsResumesEmailHTML.Append(GetApplicantUploadedResumesEmailBody(currentPage,grdViewUploadedResume, id)); //1.1

        //                // Skill Set
        //                if (chkBoxListShowHideInfo.Items.FindByValue("SkillSet").Selected)
        //                {                            
        //                    applicantsResumesEmailHTML.Append("<br/>");
        //                    applicantsResumesEmailHTML.Append(GetApplicantSkillSetEmailBody(id, facade));                            
        //                }

        //                // References
        //                if (chkBoxListShowHideInfo.Items.FindByValue("References").Selected)
        //                {
        //                    applicantsResumesEmailHTML.Append("<br/>");
        //                    applicantsResumesEmailHTML.Append(GetApplicantReferencesEmailBody(id,facade));
        //                }

        //                // Summary from email body
        //                if (chkBoxListShowHideInfo.Items.FindByValue("Summary").Selected)
        //                {
        //                    applicantsResumesEmailHTML.Append("<br/>");
        //                    applicantsResumesEmailHTML.Append(GetApplicantSummaryEmailBodyFromEmailPage(id, facade));
        //                }
        //            }
        //            applicantsResumesEmailHTML.Append("</td></tr></table>");
        //        }
        //    }

        //    if (chkRequisitionDescription.Checked == true)
        //    {
        //        applicantsResumesEmailHTML.Append("<br/><br/>");
        //        applicantsResumesEmailHTML.Append("<table width=780 border=0 align=center cellpadding=0 cellspacing=0>");
        //        applicantsResumesEmailHTML.Append("<tr><td>");
        //        applicantsResumesEmailHTML.Append(GetJobPostingInformation(jobPostingId, facade));
        //        applicantsResumesEmailHTML.Append("</tr></td></table>");
        //    }

        //    if (true)
        //    {
        //        //End Section of mail
        //        applicantsResumesEmailHTML.Append("<br/><br/>");
        //        applicantsResumesEmailHTML.Append("<table width=780 border=0 align=center cellpadding=0 cellspacing=0>");
        //        applicantsResumesEmailHTML.Append("    <tr>");
        //        applicantsResumesEmailHTML.Append(GetEmailFooter());
        //        applicantsResumesEmailHTML.Append("    </tr>");
        //        applicantsResumesEmailHTML.Append("</table>");
        //    }
        //    return applicantsResumesEmailHTML.ToString();
        //}

        private string GetApplicantPersonalInformationEmailBody(CheckBoxList chkBoxListShowHideInfo, Member member, IFacade facade)
        {
            MemberExtendedInformation memberExtendedInformation = facade.GetMemberExtendedInformationByMemberId(member.Id);
            MemberDetail memberDetail = facade.GetMemberDetailByMemberId(member.Id);

            StringBuilder applicantPersonalInfoHTMLText = new StringBuilder();
            applicantPersonalInfoHTMLText.Append("<table width=780 border=0 align=center cellpadding=0 cellspacing=0>");
            applicantPersonalInfoHTMLText.Append("    <tr>");
            applicantPersonalInfoHTMLText.Append("        <td colspan=4>");
            applicantPersonalInfoHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            applicantPersonalInfoHTMLText.Append("                <tr>");
            applicantPersonalInfoHTMLText.Append("                    <td " + emailHeaderStyle + " >Personal Information</td>");
            applicantPersonalInfoHTMLText.Append("		        </tr>");
            applicantPersonalInfoHTMLText.Append("            </table>");
            applicantPersonalInfoHTMLText.Append("        </td>");
            applicantPersonalInfoHTMLText.Append("    </tr>");
            applicantPersonalInfoHTMLText.Append("    <tr>");
            applicantPersonalInfoHTMLText.Append("        <td valign=top align=left width=100%>");
            applicantPersonalInfoHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=5>");
            if (chkBoxListShowHideInfo.Items.FindByValue("ApplicantName").Selected)
            {
                applicantPersonalInfoHTMLText.Append("            <tr>");
                applicantPersonalInfoHTMLText.Append("                <td width=21% nowrap>Name</td>");
                applicantPersonalInfoHTMLText.Append("                <td colspan=3>" + member.FirstName + " " + member.LastName + "</td>");
                applicantPersonalInfoHTMLText.Append("            </tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("ApplicantID").Selected)
            {
                applicantPersonalInfoHTMLText.Append("            <tr>");
                applicantPersonalInfoHTMLText.Append("                <td width=21% nowrap>ID</td>");
                applicantPersonalInfoHTMLText.Append("                <td colspan=3>" + member.MemberCode + "</td>");
                applicantPersonalInfoHTMLText.Append("            </tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("CurrentPosition").Selected)
            {
                applicantPersonalInfoHTMLText.Append("            <tr>");
                applicantPersonalInfoHTMLText.Append("                <td width=21% nowrap>Current Position</td>");
                applicantPersonalInfoHTMLText.Append("                <td colspan=3>" + (memberExtendedInformation != null ? memberExtendedInformation.CurrentPosition : string.Empty) + "</td>");
                applicantPersonalInfoHTMLText.Append("            </tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("CurrentCompany").Selected)
            {
                applicantPersonalInfoHTMLText.Append("            <tr>");
                applicantPersonalInfoHTMLText.Append("                <td width=21% nowrap>Current Company</td>");
                applicantPersonalInfoHTMLText.Append("                <td colspan=3>" + (memberExtendedInformation != null ? memberExtendedInformation.LastEmployer : string.Empty) + "</td>");
                applicantPersonalInfoHTMLText.Append("            </tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("CurrentCiry").Selected)
            {
                applicantPersonalInfoHTMLText.Append("            <tr>");
                applicantPersonalInfoHTMLText.Append("                <td width=21% nowrap>Location</td>");
                applicantPersonalInfoHTMLText.Append("                <td colspan=3>" + member.PermanentCity + ", " + MiscUtil.GetStateNameById(member.PermanentStateId, facade) + "</td>");
                applicantPersonalInfoHTMLText.Append("            </tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("PrimaryEmail").Selected)
            {
                applicantPersonalInfoHTMLText.Append("            <tr>");
                applicantPersonalInfoHTMLText.Append("                <td width=21% nowrap>Primary Email</td>");
                applicantPersonalInfoHTMLText.Append("                <td colspan=3>" + member.PrimaryEmail + "</td>");
                applicantPersonalInfoHTMLText.Append("            </tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("AltEmail").Selected)
            {
                applicantPersonalInfoHTMLText.Append("            <tr>");
                applicantPersonalInfoHTMLText.Append("                <td width=21% nowrap>Alternate Email</td>");
                applicantPersonalInfoHTMLText.Append("                <td colspan=3>" + member.AlternateEmail + "</td>");
                applicantPersonalInfoHTMLText.Append("            </tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("HomePhone").Selected)
            {
                applicantPersonalInfoHTMLText.Append("            <tr>");
                applicantPersonalInfoHTMLText.Append("                <td width=21% nowrap>Home Phone</td>");
                applicantPersonalInfoHTMLText.Append("                <td colspan=3>" + (memberDetail != null ? memberDetail.HomePhone : string.Empty) + "</td>");
                applicantPersonalInfoHTMLText.Append("            </tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("CellPhone").Selected)
            {
                applicantPersonalInfoHTMLText.Append("            <tr>");
                applicantPersonalInfoHTMLText.Append("                <td width=21% nowrap>Cell Phone</td>");
                applicantPersonalInfoHTMLText.Append("                <td colspan=3>" + member.CellPhone + "</td>");
                applicantPersonalInfoHTMLText.Append("            </tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("AvailabilityStatus").Selected)
            {
                applicantPersonalInfoHTMLText.Append("            <tr>");
                applicantPersonalInfoHTMLText.Append("                <td width=21% nowrap>Availability</td>");
                applicantPersonalInfoHTMLText.Append("                <td colspan=3>" + (memberExtendedInformation != null ? MiscUtil.GetLookupNameById(memberExtendedInformation.Availability, facade) : string.Empty) + "</td>");
                applicantPersonalInfoHTMLText.Append("            </tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("TotalExperience").Selected)
            {
                applicantPersonalInfoHTMLText.Append("            <tr>");
                applicantPersonalInfoHTMLText.Append("                <td width=21% nowrap>Total Experience (yrs)(</td>");
                applicantPersonalInfoHTMLText.Append("                <td colspan=3>" + (memberExtendedInformation != null ? memberExtendedInformation.TotalExperienceYears : string.Empty) + "</td>");
                applicantPersonalInfoHTMLText.Append("            </tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("WillingToTravel").Selected)
            {
                applicantPersonalInfoHTMLText.Append("            <tr>");
                applicantPersonalInfoHTMLText.Append("                <td width=21% nowrap>Willing To Travel</td>");
                applicantPersonalInfoHTMLText.Append("                <td colspan=3>" + (memberExtendedInformation != null ? (memberExtendedInformation.WillingToTravel ? "Yes" : "No") : string.Empty) + "</td>");
                applicantPersonalInfoHTMLText.Append("            </tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("WorkPermitStatus").Selected)
            {
                applicantPersonalInfoHTMLText.Append("            <tr>");
                applicantPersonalInfoHTMLText.Append("                <td width=21% nowrap>Work Permit Status</td>");
                applicantPersonalInfoHTMLText.Append("                <td colspan=3>" + (memberExtendedInformation != null ? MiscUtil.GetLookupNameById(memberExtendedInformation.WorkAuthorizationLookupId, facade) : string.Empty) + "</td>");
                applicantPersonalInfoHTMLText.Append("            </tr>");
            }
            applicantPersonalInfoHTMLText.Append("            </table>");
            applicantPersonalInfoHTMLText.Append("        </td>");
            applicantPersonalInfoHTMLText.Append("    </tr>");
            applicantPersonalInfoHTMLText.Append("</table>");
            return applicantPersonalInfoHTMLText.ToString();
        }

        public string GetApplicantSkillSetEmailBody(int memberId, IFacade facade)
        {
            StringBuilder applicantSkillSetEmailHTML = new StringBuilder();

            applicantSkillSetEmailHTML.Append("<table width=100% border=0 align=center cellpadding=0 cellspacing=0>");
            applicantSkillSetEmailHTML.Append("    <tr>");
            applicantSkillSetEmailHTML.Append("        <td>");
            applicantSkillSetEmailHTML.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            applicantSkillSetEmailHTML.Append("                <tr>");
            applicantSkillSetEmailHTML.Append("                    <td height=5 " + emailHeaderStyle + ">Skill Set</td>");
            applicantSkillSetEmailHTML.Append("		        </tr>");
            applicantSkillSetEmailHTML.Append("            </table>");
            applicantSkillSetEmailHTML.Append("        </td>");
            applicantSkillSetEmailHTML.Append("    </tr>");
            applicantSkillSetEmailHTML.Append("    <tr>");
            applicantSkillSetEmailHTML.Append("        <td>");
            applicantSkillSetEmailHTML.Append("            <table width=100% border=0 cellspacing=0 cellpadding=0>");
            applicantSkillSetEmailHTML.Append("                <tr>");
            applicantSkillSetEmailHTML.Append("                    <td valign=top align=left width=60%>");
            applicantSkillSetEmailHTML.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            applicantSkillSetEmailHTML.Append("                            <tr>");
            applicantSkillSetEmailHTML.Append("                                <td width=25% align=left><b>Skill</b></td>");//0.6
            applicantSkillSetEmailHTML.Append("                                <td width=25% align=left><b>Experience (Yrs.)</b></td>");//0.6
            applicantSkillSetEmailHTML.Append("                                <td width=25% align=left><b>Proficiency Level</b></td>");//0.6
            applicantSkillSetEmailHTML.Append("                                <td width=25% align=left><b>Last Used</b></td>");//0.6
            applicantSkillSetEmailHTML.Append("                            </tr>");

            IList<MemberSkill> memberSkillList = facade.GetAllMemberSkillByMemberId(memberId);

            if (memberSkillList != null)
            {
                foreach (MemberSkill memberSkill in memberSkillList)
                {
                    applicantSkillSetEmailHTML.Append("                            <tr>");
                    applicantSkillSetEmailHTML.Append("                                <td align=left>" + memberSkill.Name + "</td>");
                    applicantSkillSetEmailHTML.Append("                                <td align=left>" + memberSkill.YearsOfExperience.ToString() + "</td>");
                    applicantSkillSetEmailHTML.Append("                                <td align=left>" + memberSkill.ProficiencyLookupId + "</td>");
                    applicantSkillSetEmailHTML.Append("                                <td align=left>" + (MiscUtil.IsValidDate(memberSkill.LastUsed) ? memberSkill.LastUsed.ToString("MMM") + " " + memberSkill.LastUsed.Year.ToString() : string.Empty) + "</td>"); //1.0
                    applicantSkillSetEmailHTML.Append("                            </tr>");
                }
            }

            applicantSkillSetEmailHTML.Append("                        </table>");
            applicantSkillSetEmailHTML.Append("                    </td>");
            applicantSkillSetEmailHTML.Append("                </tr>");
            applicantSkillSetEmailHTML.Append("            </table>");
            applicantSkillSetEmailHTML.Append("        </td>");
            applicantSkillSetEmailHTML.Append("    </tr>");
            applicantSkillSetEmailHTML.Append("</table>");
            applicantSkillSetEmailHTML.Append("<br/>");

            return applicantSkillSetEmailHTML.ToString();
        }

        public string GetApplicantReferencesEmailBody(int memberId, IFacade facade)
        {
            StringBuilder applicantReferencesHTML = new StringBuilder();

            applicantReferencesHTML.Append("<table width=100% border=0 align=center cellpadding=0 cellspacing=0>");
            applicantReferencesHTML.Append("    <tr>");
            applicantReferencesHTML.Append("        <td>");
            applicantReferencesHTML.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            applicantReferencesHTML.Append("                <tr>");
            applicantReferencesHTML.Append("                    <td height=5 " + emailHeaderStyle + ">References</td>");
            applicantReferencesHTML.Append("		        </tr>");
            applicantReferencesHTML.Append("            </table>");
            applicantReferencesHTML.Append("        </td>");
            applicantReferencesHTML.Append("    </tr>");
            applicantReferencesHTML.Append("    <tr>");
            applicantReferencesHTML.Append("        <td>");
            applicantReferencesHTML.Append("            <table width=100% border=0 cellspacing=0 cellpadding=0>");
            applicantReferencesHTML.Append("                <tr>");
            applicantReferencesHTML.Append("                    <td valign=top align=left width=60%>");
            applicantReferencesHTML.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            applicantReferencesHTML.Append("                            <tr>");
            applicantReferencesHTML.Append("                                <td width=20% align=left><b>Name</b></td>"); //0.6
            applicantReferencesHTML.Append("                                <td width=20% align=left><b>Company</b></td>");//0.6
            applicantReferencesHTML.Append("                                <td width=20% align=left><b>Position</b></td>");//0.6
            applicantReferencesHTML.Append("                                <td width=20% align=left><b>Email Address</b></td>");//0.6
            applicantReferencesHTML.Append("                                <td width=20% align=left><b>Phone Number</b></td>");//0.6
            applicantReferencesHTML.Append("                            </tr>");

            IList<MemberReference> memberReferenceList = facade.GetAllMemberReferenceByMemberId(memberId);
            if (memberReferenceList != null)
            {
                foreach (MemberReference memberReference in memberReferenceList)
                {
                    applicantReferencesHTML.Append("                        <tr>");
                    applicantReferencesHTML.Append("                            <td>" + memberReference.ReferenceName + "</td>");
                    applicantReferencesHTML.Append("                            <td>" + memberReference.CompanyName + "</td>");
                    applicantReferencesHTML.Append("                            <td>" + memberReference.ReferencePosition + "</td>");
                    applicantReferencesHTML.Append("                            <td>" + memberReference.Email + "</td>");
                    applicantReferencesHTML.Append("                            <td>" + memberReference.Phone + "</td>");
                    applicantReferencesHTML.Append("                        </tr>");
                }
            }

            applicantReferencesHTML.Append("                        </table>");
            applicantReferencesHTML.Append("                    </td>");
            applicantReferencesHTML.Append("                </tr>");
            applicantReferencesHTML.Append("            </table>");
            applicantReferencesHTML.Append("        </td>");
            applicantReferencesHTML.Append("    </tr>");
            applicantReferencesHTML.Append("</table>");
            return applicantReferencesHTML.ToString();
        }

        private string GetApplicantSummaryEmailBodyFromEmailPage(int memberId, IFacade facade)
        {
            MemberObjectiveAndSummary memberObjectiveAndSummary = facade.GetMemberObjectiveAndSummaryByMemberId(memberId);
            StringBuilder applicantSummaryHTMLText = new StringBuilder();
            if (memberObjectiveAndSummary != null)
            {
                applicantSummaryHTMLText.Append("<table width=780 border=0 align=center cellpadding=0 cellspacing=0>");
                applicantSummaryHTMLText.Append("    <tr>");
                applicantSummaryHTMLText.Append("        <td>");
                applicantSummaryHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
                applicantSummaryHTMLText.Append("                <tr>");
                applicantSummaryHTMLText.Append("                    <td height=5 " + emailHeaderStyle + ">Summary</td>");
                applicantSummaryHTMLText.Append("		        </tr>");
                applicantSummaryHTMLText.Append("            </table>");
                applicantSummaryHTMLText.Append("        </td>");
                applicantSummaryHTMLText.Append("    </tr>");
                applicantSummaryHTMLText.Append("    <tr>");
                applicantSummaryHTMLText.Append("        <td>");
                applicantSummaryHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=0>");
                applicantSummaryHTMLText.Append("                <tr>");
                applicantSummaryHTMLText.Append("                    <td valign=top align=left width=60%>");
                applicantSummaryHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");
                applicantSummaryHTMLText.Append("                         <tr>");
                applicantSummaryHTMLText.Append("                            <td>" + HttpUtility.HtmlDecode(memberObjectiveAndSummary.Summary) + "</td>");
                applicantSummaryHTMLText.Append("                         </tr>");
                applicantSummaryHTMLText.Append("                        </table>");
                applicantSummaryHTMLText.Append("                    </td>");
                applicantSummaryHTMLText.Append("                </tr>");
                applicantSummaryHTMLText.Append("            </table>");
                applicantSummaryHTMLText.Append("        </td>");
                applicantSummaryHTMLText.Append("    </tr>");
                applicantSummaryHTMLText.Append("</table>");
                applicantSummaryHTMLText.Append("<br/>");
            }
            return applicantSummaryHTMLText.ToString();
        }

        private string GetApplicantUploadedResumesEmailBody(Page currentPage, GridView grdViewUploadedResume, int applicantId)
        {
            StringBuilder applicantUploadedResumesHTML = new StringBuilder();
            applicantUploadedResumesHTML.Append("<br/>");
            applicantUploadedResumesHTML.Append("<table width=780 border=0 align=center cellpadding=0 cellspacing=0>");
            applicantUploadedResumesHTML.Append("    <tr>");
            applicantUploadedResumesHTML.Append("        <td colspan=4>");
            applicantUploadedResumesHTML.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            applicantUploadedResumesHTML.Append("                <tr>");
            applicantUploadedResumesHTML.Append("                    <td height=5 " + emailHeaderStyle + ">Selected Resumes</td>");
            applicantUploadedResumesHTML.Append("		        </tr>");
            applicantUploadedResumesHTML.Append("            </table>");
            applicantUploadedResumesHTML.Append("        </td>");
            applicantUploadedResumesHTML.Append("    </tr>");
            applicantUploadedResumesHTML.Append("    <tr>");
            applicantUploadedResumesHTML.Append("        <td>");
            applicantUploadedResumesHTML.Append("            <table width=100% border=0 cellspacing=1 cellpadding=5>");
            applicantUploadedResumesHTML.Append("                <tr>");
            applicantUploadedResumesHTML.Append("                    <td align=left>Resume Title</td>");
            applicantUploadedResumesHTML.Append("                    <td align=left>File Name</td>");
            applicantUploadedResumesHTML.Append("                </tr>");
            bool resumeFound = false;
            foreach (GridViewRow rwResume in grdViewUploadedResume.Rows)
            {
                CheckBox chkResume = (CheckBox)rwResume.FindControl("chkResume");
                if (chkResume != null)
                {
                    if (chkResume.Checked == true && applicantId.ToString() == ((Label)rwResume.Cells[0].FindControl("lblApplicantId")).Text)
                    {
                        applicantUploadedResumesHTML.Append("    <tr>");
                        applicantUploadedResumesHTML.Append("        <td>" + ((Label)rwResume.Cells[1].FindControl("lblResumeTitle")).Text + "</td>");
                        HyperLink lnkResume = (HyperLink)rwResume.Cells[2].FindControl("lnkResume");
                        //0.9 starts here
                        //string lcPath = UrlConstants.GetCurrentDomainName(currentPage) + "'/Uploads/Candidates/Upload/" + applicantId.ToString() + "/WordResume/" + lnkResume.Text;
                        if (string.IsNullOrEmpty(lnkResume.NavigateUrl))
                            applicantUploadedResumesHTML.Append("        <td>" + lnkResume.Text + "</td>");
                        else
                            applicantUploadedResumesHTML.Append("        <td><a href='" + lnkResume.NavigateUrl + "' target=_blank>" + lnkResume.Text + "</a></td>");
                        //0.9 ends here
                        applicantUploadedResumesHTML.Append("    </tr>");
                        resumeFound = true;
                    }
                }
            }
            applicantUploadedResumesHTML.Append("            </table>");
            applicantUploadedResumesHTML.Append("        </td>");
            applicantUploadedResumesHTML.Append("    </tr>");
            applicantUploadedResumesHTML.Append("</table>");

            if (!resumeFound)
            {
                applicantUploadedResumesHTML = new StringBuilder();
            }

            return applicantUploadedResumesHTML.ToString();
        }

        public string GetSubmittedApplicantsResumesEmailBody(Page currentPage, ListBox lstPersonalInfo, GridView grdViewUploadedResume, CheckBox chkRequisitionDescription, int jobPostingId, string clientRate, string ReqRate, string appDesiredRate, string subMissionRate, IFacade facade)      //0.7
        {
            int id = 0;
            StringBuilder applicantsResumesEmailHTML = new StringBuilder();
            string[] hiringApplicant = _hiringMatrixApplicant.Split(',');
            //      applicantsResumesEmailHTML.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0><tr><td>");


            //      applicantsResumesEmailHTML.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");

            //  // 10320

            //      //applicantsResumesEmailHTML.Append("    <tr>");
            //      ////applicantsResumesEmailHTML.Append("        <td align=left colspan=2><br />Dear&nbsp;" + _recipientName + " ("+ ToEmail +"),</td>"); //Defect id: 10321

            //      //applicantsResumesEmailHTML.Append("        <td align=left colspan=2><br />Dear&nbsp;" + _recipientName + ",</td>"); //Defect id: 10321  // Jaga

            //      //applicantsResumesEmailHTML.Append("    </tr>");

            //      //if (!string.IsNullOrEmpty(_recipientAddress))
            //      //    applicantsResumesEmailHTML.Append("    <tr><td>" + _recipientAddress + "</td></tr>");

            //// 10320

            //      applicantsResumesEmailHTML.Append("    <tr>");
            //      applicantsResumesEmailHTML.Append("        <td align=left colspan=2><br/>" + _additionalMailMessage.Replace("\r\n", "") + "<br /></td>"); // Defect id: 10311
            //      applicantsResumesEmailHTML.Append("    </tr>");

            //      applicantsResumesEmailHTML.Append("    <tr>");
            //      applicantsResumesEmailHTML.Append("        <td align=left colspan=2><b>Number of Resumes</b> : " + hiringApplicant.Length + "</td>");       //0.8
            //      applicantsResumesEmailHTML.Append("    </tr>");

            //      applicantsResumesEmailHTML.Append("</table></td></tr><tr><td></tr></td>");



            //if (chkRequisitionDescription.Checked == true)
            //{
            //    applicantsResumesEmailHTML.Append("<tr><td>");
            //    applicantsResumesEmailHTML.Append("<table width=780 border=0 align=center cellpadding=0 cellspacing=0>");
            //    applicantsResumesEmailHTML.Append("<tr><td>");
            //    applicantsResumesEmailHTML.Append(GetJobPostingInformation(jobPostingId, facade));
            //    applicantsResumesEmailHTML.Append("</tr></td></table></td></tr>");
            //}
            if (lstPersonalInfo.Items.Count > 0)
                applicantsResumesEmailHTML.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0><tr><td>");
            foreach (string applicantId in hiringApplicant)
            {
                Int32.TryParse(applicantId, out id);
                if (id > 0)
                {
                    Member member = facade.GetMemberById(id);

                    if (lstPersonalInfo.Items.Count > 0)//1.2 10311 
                        applicantsResumesEmailHTML.Append("<tr><td><br/><table width=780 border=1 align=center cellpadding=0 cellspacing=0><tr><td>");
                    //else
                    //    applicantsResumesEmailHTML.Append("<br/><table width=780 border=0 align=center cellpadding=0 cellspacing=0><tr><td>");//10311 

                    if (member != null)
                    {
                        JobInfoId = jobPostingId;
                        //Add Personal Information                        
                        applicantsResumesEmailHTML.Append(GetApplicantPersonalInformationEmailBody(lstPersonalInfo, member.Id, facade));

                        //Add Uploaded Resumes
                        //applicantsResumesEmailHTML.Append(GetApplicantUploadedResumesEmailBody(currentPage, grdViewUploadedResume, id)); //1.1

                        for (int i = 0; i < lstPersonalInfo.Items.Count; i++)
                        {
                            switch (lstPersonalInfo.Items[i].Value)
                            {
                                case "SkillSet":
                                    applicantsResumesEmailHTML.Append("<br/>");
                                    applicantsResumesEmailHTML.Append(GetApplicantSkillSetEmailBody(id, facade));
                                    break;
                                case "References":
                                    applicantsResumesEmailHTML.Append("<br/>");
                                    applicantsResumesEmailHTML.Append(GetApplicantReferencesEmailBody(id, facade));
                                    break;
                                case "Summary":
                                    applicantsResumesEmailHTML.Append("<br/>");
                                    //applicantsResumesEmailHTML.Append(GetApplicantSummaryEmailBodyFromEmailPage(id, facade));
                                    applicantsResumesEmailHTML.Append(GetApplicantObjectiveAndSummary(id, facade));
                                    break;
                                case "Education":
                                    applicantsResumesEmailHTML.Append("<br/>");
                                    applicantsResumesEmailHTML.Append(GetApplicantEducationEmailBody(id, facade));
                                    break;
                                case "Experience":
                                    applicantsResumesEmailHTML.Append("<br/>");
                                    applicantsResumesEmailHTML.Append(GetApplicantExperienceEmailBody(id, facade));
                                    break;

                            }
                        }

                    }
                    if (lstPersonalInfo.Items.Count > 0)//1.2 10311 
                        applicantsResumesEmailHTML.Append("</td></tr></table></td></tr>");
                }
            }

            //if (true)
            //{
            //    //End Section of mail
            //    applicantsResumesEmailHTML.Append("<tr><td>&nbsp;</tr></td><tr><td>");
            //    applicantsResumesEmailHTML.Append("<table width=780 border=0 align=center cellpadding=0 cellspacing=0>");
            //    applicantsResumesEmailHTML.Append("    <tr><td>");
            //    applicantsResumesEmailHTML.Append(GetEmailFooter());
            //    applicantsResumesEmailHTML.Append("    </td></tr>");
            //    applicantsResumesEmailHTML.Append("</table></tr></td></table>");
            //}
            return applicantsResumesEmailHTML.ToString();
        }

        private string GetApplicantPersonalInformationEmailBody(ListBox lstProfileInfo, int memberId, IFacade facade)   //0.7
        {
            string strAllPersonalInfo = string.Empty;
            string strHomePhone = string.Empty;
            string strCurrentCity = string.Empty;
            string strWorkPermitStatus = string.Empty;
            string strCurrentPosition = string.Empty;
            string strhourlyStatus = string.Empty;
            string strmonthlyStatus = string.Empty;
            string stryearlyStatus = string.Empty;
            string strTotalExperience = string.Empty;
            string strWillingToTravel = string.Empty;
            string strAvailabilityStatus = string.Empty;
            string strAvailabilityDate = string.Empty;
            string strMemberName = string.Empty;
            string strCandidateId = string.Empty;
            string strPrimaryEmail = string.Empty;
            string strAlternateEmail = string.Empty;
            string strCellPhone = string.Empty;
            string strFirstName = string.Empty;
            string strMiddleName = string.Empty;
            string strLastName = string.Empty;
            string strNickName = string.Empty;
            string strAddress = string.Empty;
            string strZipCode = string.Empty;
            string strCountry = string.Empty;
            string strState = string.Empty;
            string strOfficePhone = string.Empty;
            string strCurrentCompany = string.Empty;
            string strDOB = string.Empty;
            string strCityOfBirth = string.Empty;
            string strCountryOfBirth = string.Empty;
            string strCountryOfResidence = string.Empty;
            string strGender = string.Empty;
            string strMaritalStatus = string.Empty;
            string strSSN = string.Empty;
            string strCurrentYearlySalary = string.Empty;
            string strCurrentMonthlySalary = string.Empty;
            string strCurrentHourlySalary = string.Empty;
            string strSalaryPaymentType = string.Empty;
            string strJobLength = string.Empty;
            string strPassportStatus = string.Empty;
            string strSecurityClearance = string.Empty;
            string strPermanentAddress = string.Empty;
            string strPermanentCountry = string.Empty;
            string strPermanentState = string.Empty;
            string strPermanentCity = string.Empty;
            string strPermanentZipCode = string.Empty;
            string strPermanentMobile = string.Empty;

            if (memberId != 0)
            {
                MemberDetail memberDetails = facade.GetMemberDetailByMemberId(memberId);
                if (memberDetails != null)
                {
                    strHomePhone = memberDetails.HomePhone;

                    //strCurrentCity = memberDetails.CurrentCity; //Defect id :10347 

                    //strAddress = memberDetails.CurrentAddressLine1 + " " + memberDetails.CurrentAddressLine2; // 10343 


                    strZipCode = memberDetails.CurrentZip;

                    if (memberDetails.CurrentCountryId > 0)
                    {
                        Country _CurrentCountry = facade.GetCountryById(memberDetails.CurrentCountryId);
                        if (_CurrentCountry != null)
                        {
                            strCountry = _CurrentCountry.Name;
                        }
                    }


                    if (memberDetails.CurrentStateId > 0)
                    {
                        State _CurrentState = facade.GetStateById(memberDetails.CurrentStateId);
                        if (_CurrentState != null)
                        {
                            //     strState = _CurrentState.Name;
                        }
                    }
                    if (memberDetails.OfficePhoneExtension != "") //1.0
                    {
                        strOfficePhone = (memberDetails.OfficePhone == "") ? "" : memberDetails.OfficePhone + "  Ext:" + memberDetails.OfficePhoneExtension;
                    }
                    else
                    {
                        strOfficePhone = (memberDetails.OfficePhone == "") ? "" : memberDetails.OfficePhone;

                    }
                    strCityOfBirth = memberDetails.CityOfBirth;


                    if (memberDetails.CountryIdOfBirth != "")
                    {
                        Country _CountryOfBirth = facade.GetCountryById(Convert.ToInt32(memberDetails.CountryIdOfBirth));
                        if (_CountryOfBirth != null)
                        {
                            strCountryOfBirth = _CountryOfBirth.Name;
                        }
                    }

                    if (memberDetails.CountryIdOfCitizenship != "")
                    {
                        Country _CountryOfResidence = facade.GetCountryById(Convert.ToInt32(memberDetails.CountryIdOfCitizenship));
                        if (_CountryOfResidence != null)
                        {
                            strCountryOfResidence = _CountryOfResidence.Name;
                        }
                    }

                    GenericLookup GenderLookUP = facade.GetGenericLookupById(memberDetails.GenderLookupId);
                    if (GenderLookUP != null)
                    {
                        strGender = GenderLookUP.Name;
                    }

                    GenericLookup MaritalStateLookUP = facade.GetGenericLookupById(memberDetails.MaritalStatusLookupId);
                    if (MaritalStateLookUP != null)
                    {
                        strMaritalStatus = MaritalStateLookUP.Name;
                    }

                    strSSN = memberDetails.SSNPAN;

                }
            }

            StringBuilder htmlPersonalInfo = new StringBuilder();
            MemberExtendedInformation memberExtended = facade.GetMemberExtendedInformationByMemberId(memberId);

            if (memberExtended != null)
            {
                GenericLookup workPermitStatusGL = facade.GetGenericLookupById(memberExtended.WorkAuthorizationLookupId);
                if (workPermitStatusGL != null)
                {
                    strWorkPermitStatus = workPermitStatusGL.Name;
                }

                if (memberId != 0)
                {
                    MemberExtendedInformation _memberExtended = facade.GetMemberExtendedInformationByMemberId(memberId);
                    JobPosting jobTotalExp = facade.GetJobPostingById(JobInfoId);
                    if (_memberExtended != null)
                    {
                        strCurrentPosition = _memberExtended.CurrentPosition;
                        strhourlyStatus = _memberExtended.ExpectedHourlyRate.ToString();
                        strmonthlyStatus = _memberExtended.ExpectedMonthlyRate.ToString();
                        stryearlyStatus = _memberExtended.ExpectedYearlyRate.ToString();
                        strTotalExperience = _memberExtended.TotalExperienceYears;
                        strWillingToTravel = (_memberExtended.WillingToTravel) ? "Yes" : "No";
                        strCurrentCompany = _memberExtended.LastEmployer;

                        GenericLookup _CurrentYearlySalaryLookUP = facade.GetGenericLookupById(_memberExtended.CurrentYearlyCurrencyLookupId);
                        if (_CurrentYearlySalaryLookUP != null)
                        {
                            strCurrentYearlySalary = _memberExtended.CurrentYearlyRate + " " + _CurrentYearlySalaryLookUP.Name; //1.0
                        }
                        else
                        {
                            strCurrentYearlySalary = _memberExtended.CurrentYearlyRate.ToString();
                        }

                        GenericLookup _CurrentMonthlySalaryLookUP = facade.GetGenericLookupById(_memberExtended.CurrentMonthlyCurrencyLookupId);
                        if (_CurrentMonthlySalaryLookUP != null)
                        {
                            strCurrentMonthlySalary = _memberExtended.CurrentMonthlyRate + " " + _CurrentMonthlySalaryLookUP.Name; //1.0
                        }
                        else
                        {
                            strCurrentMonthlySalary = _memberExtended.CurrentMonthlyRate.ToString();
                        }

                        GenericLookup _CurrentHourlySalaryLookUP = facade.GetGenericLookupById(_memberExtended.CurrentHourlyCurrencyLookupId);
                        if (_CurrentHourlySalaryLookUP != null)
                        {
                            strCurrentHourlySalary = _memberExtended.CurrentHourlyRate + " " + _CurrentHourlySalaryLookUP.Name; //1.0
                        }
                        else
                        {
                            strCurrentHourlySalary = _memberExtended.CurrentHourlyRate.ToString();
                        }
                        // 10360 Shiva

                        if (_memberExtended.SalaryTypeLookupId > 0)
                        {
                            GenericLookup _SalaryTypeLookUP = facade.GetGenericLookupById(_memberExtended.SalaryTypeLookupId);
                            if (_SalaryTypeLookUP != null)
                            {
                                strSalaryPaymentType = _SalaryTypeLookUP.Name;
                            }
                        }
                        else
                        {
                            strSalaryPaymentType = "Any";
                        }

                        if (_memberExtended.JobTypeLookupId > 0)
                        {
                            GenericLookup _JobTypeLookUP = facade.GetGenericLookupById(_memberExtended.JobTypeLookupId);
                            if (_JobTypeLookUP != null)
                            {
                                strJobLength = _JobTypeLookUP.Name;
                            }
                        }
                        else
                        {
                            strJobLength = "No Preference";
                        }

                        //GenericLookup _SalaryTypeLookUP = facade.GetGenericLookupById(_memberExtended.SalaryTypeLookupId);
                        //if (_SalaryTypeLookUP != null)
                        //{
                        //    strSalaryPaymentType = _SalaryTypeLookUP.Name;
                        //}

                        //GenericLookup _JobTypeLookUP = facade.GetGenericLookupById(_memberExtended.JobTypeLookupId);
                        //if (_JobTypeLookUP != null)
                        //{
                        //    strJobLength = _JobTypeLookUP.Name;
                        //}

                        // 10360 Shiva

                        strPassportStatus = (_memberExtended.PassportStatus) ? "Yes" : "No";
                        strSecurityClearance = (_memberExtended.SecurityClearance) ? "Yes" : "No";

                    }

                    if (_memberExtended.Availability != 0)
                    {
                        GenericLookup nameAvailable = facade.GetGenericLookupById(_memberExtended.Availability);
                        if (nameAvailable != null)
                        {
                            if (_memberExtended.AvailableDate != Convert.ToDateTime("1/1/0001"))
                            {
                                strAvailabilityDate = _memberExtended.AvailableDate.ToShortDateString();
                                strAvailabilityStatus = nameAvailable.Name + " " + strAvailabilityDate;
                            }
                            else
                            {
                                strAvailabilityStatus = nameAvailable.Name;
                            }
                        }
                    }
                }

                if (memberId != 0)
                {
                    Member member = facade.GetMemberById(memberId);

                    if (member != null)
                    {
                        strMemberName = member.FirstName + " " + member.LastName;

                        strFirstName = member.FirstName;
                        strLastName = member.LastName;
                        strMiddleName = member.MiddleName;
                        strNickName = member.NickName;

                        strCandidateId = member.Id.ToString();
                        strPrimaryEmail = member.PrimaryEmail;
                        strAlternateEmail = member.AlternateEmail;
                        strCellPhone = member.CellPhone;
                        if (member.PrimaryPhoneExtension != "") //1.0
                        {
                            strHomePhone = strHomePhone + "  Ext:" + member.PrimaryPhoneExtension;
                        }
                        strCurrentCity = member.PermanentCity; //Defect id :10347
                        strDOB = member.DateOfBirth.ToShortDateString();
                        strPermanentAddress = member.PermanentAddressLine1 + "," + " " + member.PermanentAddressLine2;  // 10343 Rework

                        // 10343
                        //strAddress = member.PermanentAddressLine1;

                        //if (member.PermanentAddressLine2.IsNotNullOrEmpty)
                        //{
                        //    strAddress = strAddress + "," + member.PermanentAddressLine2;   //10343 
                        //}

                        // new

                        if (!string.IsNullOrEmpty(member.PermanentAddressLine1) && !string.IsNullOrEmpty(member.PermanentAddressLine2))
                        {
                            strAddress = member.PermanentAddressLine1 + "," + " " + member.PermanentAddressLine2;  // 10343 Rework
                        }
                        else if (string.IsNullOrEmpty(member.PermanentAddressLine1) && !string.IsNullOrEmpty(member.PermanentAddressLine2))
                        {
                            strAddress = member.PermanentAddressLine2;
                        }
                        else if (!string.IsNullOrEmpty(member.PermanentAddressLine1) && string.IsNullOrEmpty(member.PermanentAddressLine2))
                        {
                            strAddress = member.PermanentAddressLine1;
                        }

                        //10343

                        strPermanentCity = member.PermanentCity;

                        if (member.PermanentCountryId > 0)
                        {
                            Country _PermanentCountry = facade.GetCountryById(member.PermanentCountryId);
                            if (_PermanentCountry != null)
                            {
                                //  strPermanentCountry = _PermanentCountry.Name;
                                strCountry = _PermanentCountry.Name;
                            }
                        }

                        strPermanentMobile = member.PermanentMobile;

                        if (member.PermanentStateId > 0)
                        {
                            State PermanentState = facade.GetStateById(member.PermanentStateId);
                            if (PermanentState != null)
                            {
                                //strPermanentState = PermanentState.Name;
                                strState = PermanentState.Name;
                            }
                        }

                        //     strPermanentZipCode = member.PermanentZip;
                        strZipCode = member.PermanentZip;
                    }
                }

                for (int i = 0; i < lstProfileInfo.Items.Count; i++)
                {
                    switch (lstProfileInfo.Items[i].Value)
                    {
                        case "ApplicantID":
                            strAllPersonalInfo = strAllPersonalInfo + "`" + "Candidate Id~" + strCandidateId;
                            break;
                        case "FirstName":
                            strAllPersonalInfo = strAllPersonalInfo + "`First Name~" + strFirstName;
                            break;
                        case "MiddleName":
                            strAllPersonalInfo = strAllPersonalInfo + "`Middle Name~" + strMiddleName;
                            break;
                        case "LastName":
                            strAllPersonalInfo = strAllPersonalInfo + "`Last Name~" + strLastName;
                            break;
                        case "NickName":
                            strAllPersonalInfo = strAllPersonalInfo + "`Nick Name~" + strNickName;
                            break;
                        case "Address":
                            strAllPersonalInfo = strAllPersonalInfo + "`Address~" + strAddress;
                            break;
                        case "CurrentCiry":
                            strAllPersonalInfo = strAllPersonalInfo + "`Current Location~" + strCurrentCity;
                            break;
                        case "ZipCode":
                            strAllPersonalInfo = strAllPersonalInfo + "`Zip Code~" + strZipCode; //1.0
                            break;
                        case "Country":
                            strAllPersonalInfo = strAllPersonalInfo + "`Country~" + strCountry;
                            break;
                        case "State":
                            strAllPersonalInfo = strAllPersonalInfo + "`State~" + strState;
                            break;
                        case "PrimaryEmail":
                            strAllPersonalInfo = strAllPersonalInfo + "`Primary Email~" + strPrimaryEmail;
                            break;
                        case "AltEmail":
                            strAllPersonalInfo = strAllPersonalInfo + "`Alternate Email~" + strAlternateEmail;
                            break;
                        case "HomePhone":
                            strAllPersonalInfo = strAllPersonalInfo + "`Home Phone~" + strHomePhone;
                            break;
                        case "CellPhone":
                            strAllPersonalInfo = strAllPersonalInfo + "`Cell Phone~" + strCellPhone;
                            break;
                        case "OfficePhone":
                            strAllPersonalInfo = strAllPersonalInfo + "`Office Phone~" + strOfficePhone;
                            break;
                        case "CurrentPosition":
                            strAllPersonalInfo = strAllPersonalInfo + "`Current Position~" + strCurrentPosition;
                            break;
                        case "CurrentCompany":
                            strAllPersonalInfo = strAllPersonalInfo + "`Current Company~" + strCurrentCompany;
                            break;
                        case "TotalExperience":
                            strAllPersonalInfo = strAllPersonalInfo + "`Total Experience (yrs)~" + strTotalExperience;
                            break;
                        case "WillingToTravel":
                            if (strWillingToTravel == "Yes")
                            {
                                strAllPersonalInfo = strAllPersonalInfo + "`Willing To Travel~Yes";
                            }
                            else
                            {
                                strAllPersonalInfo = strAllPersonalInfo + "`Willing To Travel~No";
                            }
                            break;
                        case "DOB":
                            strAllPersonalInfo = strAllPersonalInfo + "`Date of Birth~" + strDOB;
                            break;
                        case "CityOfBirth":
                            strAllPersonalInfo = strAllPersonalInfo + "`City of Birth~" + strCityOfBirth;
                            break;
                        case "CountryOfBirth":
                            strAllPersonalInfo = strAllPersonalInfo + "`Country of Birth~" + strCountryOfBirth;
                            break;
                        case "CountryOfResidence":
                            strAllPersonalInfo = strAllPersonalInfo + "`Country of Residence~" + strCountryOfResidence;
                            break;
                        case "Gender":
                            strAllPersonalInfo = strAllPersonalInfo + "`Gender~" + strGender;
                            break;
                        case "MaritalStatus":
                            strAllPersonalInfo = strAllPersonalInfo + "`Marital Status~" + strMaritalStatus;
                            break;
                        case "SSN":
                            strAllPersonalInfo = strAllPersonalInfo + "`SSN~" + strSSN;
                            break;
                        case "WorkPermitStatus":
                            strAllPersonalInfo = strAllPersonalInfo + "`Work Permit Status~" + strWorkPermitStatus;
                            break;
                        case "CurrentYearlySalary":
                            strAllPersonalInfo = strAllPersonalInfo + "`Current Yearly Salary~" + strCurrentYearlySalary;
                            break;
                        case "CurrentMonthlySalary":
                            strAllPersonalInfo = strAllPersonalInfo + "`Current Monthly Salary~" + strCurrentMonthlySalary;
                            break;
                        case "CurrentHourlySalary":
                            strAllPersonalInfo = strAllPersonalInfo + "`Current Hourly Salary~" + strCurrentHourlySalary;
                            break;
                        case "SalaryPaymentType":
                            strAllPersonalInfo = strAllPersonalInfo + "`Salary Payment Type~" + strSalaryPaymentType;
                            break;
                        case "JobLength":
                            strAllPersonalInfo = strAllPersonalInfo + "`Job Length~" + strJobLength;
                            break;
                        case "PassportStatus":
                            strAllPersonalInfo = strAllPersonalInfo + "`Passport Status~" + strPassportStatus;
                            break;
                        case "SecurityClearance":
                            strAllPersonalInfo = strAllPersonalInfo + "`Security Clearance~" + strSecurityClearance;
                            break;
                        case "PermanentAddress":
                            strAllPersonalInfo = strAllPersonalInfo + "`Permanent Address~" + strPermanentAddress;
                            break;
                        case "PermanentCountry":
                            strAllPersonalInfo = strAllPersonalInfo + "`Permanent Country~" + strPermanentCountry;
                            break;
                        case "PermanentState":
                            strAllPersonalInfo = strAllPersonalInfo + "`Permanent State~" + strPermanentState;
                            break;
                        case "PermanentCity":
                            strAllPersonalInfo = strAllPersonalInfo + "`Permanent City~" + strPermanentCity;
                            break;
                        case "PermanentZipCode":
                            strAllPersonalInfo = strAllPersonalInfo + "`Permanent Zip Code~" + strPermanentZipCode; //1.0
                            break;
                        case "PermanentMobile":
                            strAllPersonalInfo = strAllPersonalInfo + "`Permanent Mobile~" + strPermanentZipCode;
                            break;
                        case "hourlyStatus":
                            strAllPersonalInfo = strAllPersonalInfo + "`Hourly Status~" + strhourlyStatus;
                            break;
                        case "monthlyStatus":
                            strAllPersonalInfo = strAllPersonalInfo + "`Monthly Status~" + strmonthlyStatus;
                            break;
                        case "yearlyStatus":
                            strAllPersonalInfo = strAllPersonalInfo + "`Yearly Status~" + stryearlyStatus;
                            break;
                        case "AvailabilityStatus":
                            strAllPersonalInfo = strAllPersonalInfo + "`Availability Status~" + strAvailabilityStatus;      //10910
                            break;
                        default:
                            break;
                    }
                }


                if (memberExtended != null)
                {
                    arrAllPersonalInfo = strAllPersonalInfo.Split('`');

                    ArrayList arrlstPersonalInfoKeyVal = new ArrayList();

                    for (int i = 0; i < arrAllPersonalInfo.Length; i++)
                    {
                        arrPersonalInfoKeyValue = arrAllPersonalInfo[i].Split('~');

                        for (int j = 0; j < arrPersonalInfoKeyValue.Length; j++)
                        {
                            arrlstPersonalInfoKeyVal.Add(arrPersonalInfoKeyValue[j]);
                        }
                    }
                    if (arrlstPersonalInfoKeyVal.Count > 1) // Defect id 10311
                    {
                        string value = ""; //<br /><br />
                        htmlPersonalInfo.Append(@"
                   <table width=780 border=0 align=center cellpadding=0 cellspacing=0>
                    <tr>
                        <td colspan=4>
                        <table width=100% border=0 cellspacing=0 cellpadding=5>
                            <tr> 
                                <td " + emailHeaderStyle + @">Personal Information of " + strMemberName + @"</td> 
                            </tr>  
                        </table>
                        </td> 
                    </tr>
                    <tr>
                        <td>
                        <table width=100% border=0 cellspacing=1 cellpadding=0>
                           <tr>
                               <td valign=top align=left width=100%>
                               <table width=100% border=0 cellspacing=1 cellpadding=5>");

                        int i1 = 1;
                        int j1 = 2;
                        while (j1 < arrlstPersonalInfoKeyVal.Count)
                        {
                            htmlPersonalInfo.Append(@"
                                <tr> 
                                    <td width=21% nowrap><b>" + arrlstPersonalInfoKeyVal[i1].ToString() + @"</b></td>
                                    <td colspan=5>" + arrlstPersonalInfoKeyVal[j1].ToString() + @"</td>  
                                </tr>");
                            i1 = i1 + 2;
                            j1 = j1 + 2;
                        }

                        htmlPersonalInfo.Append(@"
                                        </table>
                                    </td>
                                </tr>
                            </table>
                          </td>
                        </tr>
                      </table><br/>");
                    }
                }
            }
            return htmlPersonalInfo.ToString();
        }

        public string GetApplicantEducationEmailBody(int memberId, IFacade facade)
        {
            StringBuilder applicantEducationEmailHTML = new StringBuilder();

            applicantEducationEmailHTML.Append("<table width=100% border=0 align=center cellpadding=0 cellspacing=0>");
            applicantEducationEmailHTML.Append("    <tr>");
            applicantEducationEmailHTML.Append("        <td>");
            applicantEducationEmailHTML.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            applicantEducationEmailHTML.Append("                <tr>");
            applicantEducationEmailHTML.Append("                    <td height=5 " + emailHeaderStyle + ">Education</td>");
            applicantEducationEmailHTML.Append("		        </tr>");
            applicantEducationEmailHTML.Append("            </table>");
            applicantEducationEmailHTML.Append("        </td>");
            applicantEducationEmailHTML.Append("    </tr>");
            applicantEducationEmailHTML.Append("    <tr>");
            applicantEducationEmailHTML.Append("        <td>");
            applicantEducationEmailHTML.Append("            <table width=100% border=0 cellspacing=0 cellpadding=0>");
            applicantEducationEmailHTML.Append("                <tr>");
            applicantEducationEmailHTML.Append("                    <td valign=top align=left width=60%>");
            applicantEducationEmailHTML.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            applicantEducationEmailHTML.Append("                            <tr>");
            applicantEducationEmailHTML.Append("                                <td width=25% align=left><b>Degree</b></td>");//0.6
            applicantEducationEmailHTML.Append("                                <td width=25% align=left><b>Institute Name</b></td>");//0.6
            applicantEducationEmailHTML.Append("                                <td width=25% align=left><b>Year of Passing</b></td>");//0.6
            applicantEducationEmailHTML.Append("                                <td width=25% align=left><b>Major</b></td>");//0.6
            applicantEducationEmailHTML.Append("                            </tr>");

            IList<MemberEducation> memberEducationList = facade.GetAllMemberEducationByMemberId(memberId);

            if (memberEducationList != null)
            {
                foreach (MemberEducation memberEducation in memberEducationList)
                {
                    applicantEducationEmailHTML.Append("                            <tr>");
                    applicantEducationEmailHTML.Append("                                <td align=left>" + memberEducation.DegreeTitle + "</td>");
                    applicantEducationEmailHTML.Append("                                <td align=left>" + memberEducation.InstituteName + "</td>");
                    applicantEducationEmailHTML.Append("                                <td align=left>" + memberEducation.DegreeObtainedYear + "</td>");
                    applicantEducationEmailHTML.Append("                                <td align=left>" + memberEducation.MajorSubjects + "</td>");
                    applicantEducationEmailHTML.Append("                            </tr>");
                }
            }

            applicantEducationEmailHTML.Append("                        </table>");
            applicantEducationEmailHTML.Append("                    </td>");
            applicantEducationEmailHTML.Append("                </tr>");
            applicantEducationEmailHTML.Append("            </table>");
            applicantEducationEmailHTML.Append("        </td>");
            applicantEducationEmailHTML.Append("    </tr>");
            applicantEducationEmailHTML.Append("</table>");
            applicantEducationEmailHTML.Append("<br/>");

            return applicantEducationEmailHTML.ToString();
        }

        //public string GetApplicantSkillSetEmailBody(int memberId, IFacade facade)
        public string GetApplicantExperienceEmailBody(int memberId, IFacade facade)
        {
            StringBuilder applicantExperienceEmailHTML = new StringBuilder();

            applicantExperienceEmailHTML.Append("<table width=100% border=0 align=center cellpadding=0 cellspacing=0>");
            applicantExperienceEmailHTML.Append("    <tr>");
            applicantExperienceEmailHTML.Append("        <td>");
            applicantExperienceEmailHTML.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            applicantExperienceEmailHTML.Append("                <tr>");
            applicantExperienceEmailHTML.Append("                    <td height=5 " + emailHeaderStyle + ">Experience</td>");
            applicantExperienceEmailHTML.Append("		        </tr>");
            applicantExperienceEmailHTML.Append("            </table>");
            applicantExperienceEmailHTML.Append("        </td>");
            applicantExperienceEmailHTML.Append("    </tr>");
            applicantExperienceEmailHTML.Append("    <tr>");
            applicantExperienceEmailHTML.Append("        <td>");
            applicantExperienceEmailHTML.Append("            <table width=100% border=0 cellspacing=0 cellpadding=0>");
            applicantExperienceEmailHTML.Append("                <tr>");
            applicantExperienceEmailHTML.Append("                    <td valign=top align=left width=60%>");
            applicantExperienceEmailHTML.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            applicantExperienceEmailHTML.Append("                            <tr>");
            applicantExperienceEmailHTML.Append("                                <td width=16.66% align=left><b>Duration</b></td>");
            applicantExperienceEmailHTML.Append("                                <td width=16.66% align=left><b>Job Title</b></td>");
            applicantExperienceEmailHTML.Append("                                <td width=16.66% align=left><b>Employer Name</b></td>");
            applicantExperienceEmailHTML.Append("                                <td width=16.66% align=left><b>Location</b></td>");
            applicantExperienceEmailHTML.Append("                                <td width=16.66% align=left><b>Industry Category</b></td>");
            applicantExperienceEmailHTML.Append("                                <td width=16.66% align=left><b>Functional Category</b></td>");
            applicantExperienceEmailHTML.Append("                            </tr>");

            //IList<MemberExperience> memberExperienceList = facade.GetAllMemberExperienceByMemberId(memberId); //Defect id 10340
            IList<MemberExperienceDetail> memberExperienceList = facade.GetAllMemberExperienceDetailByMemberId(memberId);

            if (memberExperienceList != null)
            {
                foreach (MemberExperienceDetail memberExperience in memberExperienceList)
                {
                    string strIndustryCategoryLookup = "";
                    //GenericLookup _IndustryCategoryLookup = facade.GetGenericLookupById(memberExperience.IndustryCategoryLookupId);
                    //if (_IndustryCategoryLookup != null)
                    //{
                    //    strIndustryCategoryLookup = _IndustryCategoryLookup.Name;
                    //}

                    //string strFunctionalCategoryLookup = "";
                    //GenericLookup _FunctionalCategoryLookup = facade.GetGenericLookupById(memberExperience.FunctionalCategoryLookupId);
                    //if (_FunctionalCategoryLookup != null)
                    //{
                    //    strFunctionalCategoryLookup = _FunctionalCategoryLookup.Name;
                    //}

                    applicantExperienceEmailHTML.Append("                            <tr>");
                    applicantExperienceEmailHTML.Append("                                <td align=left>" + memberExperience.DateFrom.ToShortDateString() + " To " + memberExperience.DateTo.ToShortDateString() + "</td>");
                    applicantExperienceEmailHTML.Append("                                <td align=left>" + memberExperience.PositionName + "</td>");
                    applicantExperienceEmailHTML.Append("                                <td align=left>" + memberExperience.CompanyName + "</td>");
                    applicantExperienceEmailHTML.Append("                                <td align=left>" + memberExperience.City + "</td>");
                    applicantExperienceEmailHTML.Append("                                <td align=left>" + memberExperience.IndustryCategory + "</td>");//strIndustryCategoryLookup
                    applicantExperienceEmailHTML.Append("                                <td align=left>" + memberExperience.FunctionalCategory + "</td>");//strFunctionalCategoryLookup
                    applicantExperienceEmailHTML.Append("                            </tr>");
                }
            }

            applicantExperienceEmailHTML.Append("                        </table>");
            applicantExperienceEmailHTML.Append("                    </td>");
            applicantExperienceEmailHTML.Append("                </tr>");
            applicantExperienceEmailHTML.Append("            </table>");
            applicantExperienceEmailHTML.Append("        </td>");
            applicantExperienceEmailHTML.Append("    </tr>");
            applicantExperienceEmailHTML.Append("</table>");
            applicantExperienceEmailHTML.Append("<br/>");

            return applicantExperienceEmailHTML.ToString();
        }

        #endregion

        #region Resume Submission and Broad Casting

        private string GetApplicantPersonalInformationEmailBody(CheckBoxList ckbListProfile, int memberId, IFacade facade)
        {
            StringBuilder htmlPersonalInfo = new StringBuilder();
            MemberExtendedInformation memberExtended = facade.GetMemberExtendedInformationByMemberId(memberId);

            string strCandidateName = String.Empty;
            string strMemberId = String.Empty;
            string strPrimaryEmail = String.Empty;
            string strAlternativeEmail = String.Empty;
            string strCellPhone = String.Empty;
            string strCurrentCity = String.Empty;

            if (memberId != 0)
            {
                Member member = facade.GetMemberById(memberId);

                if (member != null)
                {
                    if (ckbListProfile.Items.FindByValue("candidateName").Selected == true)
                    {
                        // 1.8
                        strCandidateName = member.FirstName + " " + member.LastName;
                        // 1.8
                    }
                    if (ckbListProfile.Items.FindByValue("candidateID").Selected == true)
                    {
                        strMemberId = memberId.ToString();
                    }
                    if (ckbListProfile.Items.FindByValue("primaryEmail").Selected == true)
                    {
                        strPrimaryEmail = member.PrimaryEmail;
                    }
                    if (ckbListProfile.Items.FindByValue("altEmail").Selected == true)
                    {
                        strAlternativeEmail = member.AlternateEmail;
                    }
                    if (ckbListProfile.Items.FindByValue("cellPhone").Selected == true)
                    {
                        strCellPhone = member.CellPhone;
                    }
                    if (ckbListProfile.Items.FindByValue("currentCiry").Selected == true)//10305 Gopal
                    {
                        strCurrentCity = member.PermanentCity;
                    } //Defect id :10347

                }
            }
            string strHomePhone = String.Empty;
            if (memberId != 0)
            {
                MemberDetail memberDetails = facade.GetMemberDetailByMemberId(memberId);

                if (memberDetails != null)
                {
                    if (ckbListProfile.Items.FindByValue("homePhone").Selected == true)
                    {
                        strHomePhone = memberDetails.HomePhone;
                    }
                    /*   if (ckbListProfile.Items.FindByValue("currentCiry").Selected == true)//10305 Gopal
                       {
                           strCurrentCity = memberDetails.CurrentCity;
                       }*/
                    //Defect id :10347
                }
            }
            if (memberExtended != null)
            {
                GenericLookup workPermitStatusGL = facade.GetGenericLookupById(memberExtended.WorkAuthorizationLookupId);
                string strWorkPermit = String.Empty;
                if (workPermitStatusGL != null)
                {
                    if (ckbListProfile.Items.FindByValue("workPermitStatus").Selected == true)
                    {
                        strWorkPermit = workPermitStatusGL.Name;
                    }
                }

                string strCurrentPosition = String.Empty;
                string decExpectedHourlyRate = String.Empty;
                string decExpectedMonthlyRate = String.Empty;
                string decExpectedYearlyRate = String.Empty;
                string strTotalExperience = String.Empty;
                string strWillingToTravel = String.Empty;

                if (memberId != 0)
                {
                    MemberExtendedInformation _memberExtended = facade.GetMemberExtendedInformationByMemberId(memberId);

                    if (_memberExtended != null)
                    {

                        if (ckbListProfile.Items.FindByValue("currentPosition").Selected == true)
                        {
                            strCurrentPosition = _memberExtended.CurrentPosition;
                        }
                        if (ckbListProfile.Items.FindByValue("hourlyStatus").Selected == true)
                        {
                            decExpectedHourlyRate = _memberExtended.ExpectedHourlyRate.ToString();
                        }
                        if (ckbListProfile.Items.FindByValue("monthlyStatus").Selected == true)
                        {
                            decExpectedMonthlyRate = _memberExtended.ExpectedMonthlyRate.ToString();
                        }
                        if (ckbListProfile.Items.FindByValue("yearlyStatus").Selected == true)
                        {
                            decExpectedYearlyRate = _memberExtended.ExpectedYearlyRate.ToString();
                        }
                        if (ckbListProfile.Items.FindByValue("totalExperience").Selected == true)
                        {
                            strTotalExperience = _memberExtended.TotalExperienceYears;
                        }
                        if (ckbListProfile.Items.FindByValue("willingToTravel").Selected == true)
                        {
                            if (_memberExtended.WillingToTravel)
                            {
                                strWillingToTravel = "Yes";
                            }
                            else
                            {
                                strWillingToTravel = "No";
                            }
                        }
                    }

                    string strNameAvailable = String.Empty;
                    if (_memberExtended.Availability != 0)
                    {
                        GenericLookup nameAvailable = facade.GetGenericLookupById(_memberExtended.Availability);
                        if (nameAvailable != null)
                        {
                            if (ckbListProfile.Items.FindByValue("availabilityStatus").Selected == true)
                            {
                                strNameAvailable = nameAvailable.Name;
                            }
                        }
                    }

                    string value = "";
                    htmlPersonalInfo.Append(@"
                   <br /><br /><table width=780 border=0 align=center cellpadding=0 cellspacing=0>
                    <tr>
                        <td colspan=4>
                        <table width=100% border=0 cellspacing=0 cellpadding=5>
                            <tr> 
                                <td " + emailHeaderStyle + @">Personal Information</td> 
                            </tr>  
                        </table>
                        </td> 
                    </tr>
                    <tr>
                        <td>
                        <table width=100% border=0 cellspacing=1 cellpadding=0>
                           <tr>
                               <td valign=top align=left width=100%>
                               <table width=100% border=0 cellspacing=1 cellpadding=5>
                                <tr> 
                                    <td width=21% nowrap><b>Candidate Name:</b></td>
                                    <td colspan=5>" + strCandidateName + @"</td>  
                                </tr> 
                                <tr>
                                    <td width=21% nowrap><b>Candidate ID:</b></td> 
                                    <td>" + strMemberId + @"</td>                                
                                    <td width=21% nowrap><b>Current Position:</b></td>                                
                                    <td  colspan=3>" + strCurrentPosition + @"</td>
                                </tr> 
                                <tr>
                                    <td width=21% nowrap><b>Current City:</b></td>
                                    <td>" + strCurrentCity + @"</td> 
                                    <td width=21% nowrap><b>Current Company:</b></td> 
                                    <td colspan=3>" + value + @"</td>
                                </tr>
                                <tr> 
                                    <td width=21% nowrap><b>Primary Email:</b></td>
                                    <td>" + strPrimaryEmail + @"</td>
                                    <td width=21% nowrap><b>Alternative Email:</b></td> 
                                    <td colspan=3>" + strAlternativeEmail + @"</td> 
                                </tr>
                                <tr>
                                    <td width=21% nowrap><b>Expected Salary:</b></td>
                                    <td colspan=5>" + value + @"</td>
                                </tr>
                                <tr> 
                                     <td width=7% nowrap><b>Hourly Salary:</b></td>
                                     <td>" + decExpectedHourlyRate + @"</td>
                                     <td width=7% nowrap><b>Monthly Salary:</b></td>
                                     <td>" + decExpectedMonthlyRate + @"</td>
                                     <td width=7% nowrap><b>Yearly Salary:</b></td>
                                     <td>" + decExpectedYearlyRate + @"</td>                                 
                                </tr>
                                <tr>
                                     <td width=21% nowrap><b>Availability:</b></td>  
                                     <td>" + strNameAvailable + @"</td>  
                                     <td width=21% nowrap><b>Total Experience (yrs):</b></td>
                                     <td colspan=3>" + strTotalExperience + @"</td>
                                </tr>
                                 <tr>
                                     <td width=21% nowrap><b>Home Phone:</b></td>
                                     <td>" + strHomePhone + @"</td>
                                     <td width=21% nowrap><b>Willing To Travel:</b></td>
                                     <td colspan=3>" + strWillingToTravel + @"</td>   
                                 </tr>
                                 <tr>
                                    <td width=21% nowrap><b>Cell Phone:</b></td> 
                                    <td>" + strCellPhone + @"</td>  
                                    <td width=21% nowrap><b>Work Permit Status:</b></td>  
                                    <td colspan=3>" + strWorkPermit + @"</td>  
                                 </tr>
                            </table>
                            </td>
                        </tr>
                    </table>
                  </td>
                </tr>
              </table><br/>");
                }
            }
            return htmlPersonalInfo.ToString();
        }

        public string GetApplicantObjectiveAndSummary(int memberId, IFacade facade)
        {
            StringBuilder htmlObjectiveSummary = new StringBuilder();
            MemberObjectiveAndSummary memberObjectiveSummary = facade.GetMemberObjectiveAndSummaryByMemberId(memberId);

            if (memberObjectiveSummary != null)
            {
                htmlObjectiveSummary.Append(@"<table width=100% border=0 align=center cellpadding=4 cellspacing=0>
                    <tr>
                        <td colspan=2 " + emailHeaderStyle + @">
                            Objective/Summary
                        </td> 
                    </tr>
                    <tr>
                        <td width=21% nowrap valign=baseline><b>Objective:</b></td>
                        <td><p align=justify>");
                htmlObjectiveSummary.Append(memberObjectiveSummary.Objective);
                htmlObjectiveSummary.Append("</p></td></tr><br>");
                htmlObjectiveSummary.Append(@"<tr> 
                        <td width=21% nowrap valign=baseline><b>Summary:</b></td>
                        <td><p align=justify>");
                htmlObjectiveSummary.Append(memberObjectiveSummary.Summary);
                htmlObjectiveSummary.Append("</p></td>");
                htmlObjectiveSummary.Append(@"</tr> 
                    </table><br /><br /");
            }
            return htmlObjectiveSummary.ToString();
        }

        private string GetApplicantUploadedResumesEmailBody(ListView lsvUploadedResume, int applicantId)
        {
            StringBuilder applicantUploadedResumesHTML = new StringBuilder();
            //applicantUploadedResumesHTML.Append("<br/>");
            applicantUploadedResumesHTML.Append("<table width=780 border=0 align=center cellpadding=0 cellspacing=0>");
            applicantUploadedResumesHTML.Append("    <tr>");
            applicantUploadedResumesHTML.Append("        <td colspan=4>");
            applicantUploadedResumesHTML.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            applicantUploadedResumesHTML.Append("                <tr>");
            applicantUploadedResumesHTML.Append("                    <td height=5 " + emailHeaderStyle + ">Selected Resumes</td>");
            applicantUploadedResumesHTML.Append("		        </tr>");
            applicantUploadedResumesHTML.Append("            </table>");
            applicantUploadedResumesHTML.Append("        </td>");
            applicantUploadedResumesHTML.Append("    </tr>");
            applicantUploadedResumesHTML.Append("    <tr>");
            applicantUploadedResumesHTML.Append("        <td>");
            applicantUploadedResumesHTML.Append("            <table width=100% border=0 cellspacing=1 cellpadding=5>");
            applicantUploadedResumesHTML.Append("                <tr>");
            applicantUploadedResumesHTML.Append("                    <td align=left><b>Document Title</b></td>");
            applicantUploadedResumesHTML.Append("                    <td align=left><b>Document Type</b></td>");
            applicantUploadedResumesHTML.Append("                    <td align=left><b>Date Uploaded</b></td>");
            applicantUploadedResumesHTML.Append("                    <td align=left><b>File Name</b></td>");
            applicantUploadedResumesHTML.Append("                </tr>");
            bool resumeFound = false;

            foreach (ListViewDataItem rwResume in lsvUploadedResume.Items)
            {
                CheckBox chkResume = (CheckBox)rwResume.FindControl("chkResume");
                if (chkResume != null)
                {
                    if (chkResume.Checked == true)
                    {
                        Label lblDocumentLink = rwResume.FindControl("lblDocumentLink") as Label;
                        Label lblDocumentTitle = rwResume.FindControl("lblDocumentTitle") as Label;
                        Label lblDateUploaded = rwResume.FindControl("lblDateUploaded") as Label;
                        Label lblDocumentType = rwResume.FindControl("lblDocumentType") as Label;

                        applicantUploadedResumesHTML.Append("    <tr>");
                        applicantUploadedResumesHTML.Append("        <td>" + lblDocumentTitle.Text + "</td>");
                        applicantUploadedResumesHTML.Append("        <td>" + lblDocumentType.Text + "</td>");
                        applicantUploadedResumesHTML.Append("        <td>" + lblDateUploaded.Text + "</td>");

                        applicantUploadedResumesHTML.Append("        <td>" + lblDocumentLink.Text + "</td>");
                        applicantUploadedResumesHTML.Append("    </tr>");
                        resumeFound = true;
                    }
                }
            }
            applicantUploadedResumesHTML.Append("            </table>");
            applicantUploadedResumesHTML.Append("        </td>");
            applicantUploadedResumesHTML.Append("    </tr>");
            applicantUploadedResumesHTML.Append("</table>");

            if (!resumeFound)
            {
                applicantUploadedResumesHTML = new StringBuilder();
            }

            return applicantUploadedResumesHTML.ToString();
        }

        public string GetApplicantResumeForSubmission(CheckBoxList chkBoxListShowHideInfo, ListView lsvUploadedDocument, int memberId, IFacade facade)
        {
            StringBuilder strHTMLText = new StringBuilder();
            //strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");

            // 10320

            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td align=left><br /><br />Dear&nbsp;Recipient,<br /><br /></td>");
            //strHTMLText.Append("</tr>");
            // 10320

            strHTMLText.Append("<tr>");
            strHTMLText.Append("    <td align=left> ****** <br /></td>"); // 0.5 
            strHTMLText.Append("    <td align=left>" + _additionalMailMessage.Replace("\r\n", "<br />") + "<br /></td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("<tr>");
            strHTMLText.Append("    <td>");
            strHTMLText.Append(GetApplicantPersonalInformationEmailBody(chkBoxListShowHideInfo, memberId, facade));
            strHTMLText.Append("    </td>");
            strHTMLText.Append("</tr>");
            if (chkBoxListShowHideInfo.Items.FindByValue("summaryStatus").Selected == true)
            {
                strHTMLText.Append("<tr>");
                strHTMLText.Append("    <td>");
                strHTMLText.Append(GetApplicantObjectiveAndSummary(memberId, facade));
                strHTMLText.Append("    </td>");
                strHTMLText.Append("</tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("skillSet").Selected == true)
            {
                strHTMLText.Append("<tr>");
                strHTMLText.Append("    <td>");
                strHTMLText.Append(GetApplicantSkillSetEmailBody(memberId, facade));
                strHTMLText.Append("    </td>");
                strHTMLText.Append("</tr>");
            }
            if (chkBoxListShowHideInfo.Items.FindByValue("refStatus").Selected == true)
            {
                strHTMLText.Append("<tr>");
                strHTMLText.Append("    <td>");
                strHTMLText.Append(GetApplicantReferencesEmailBody(memberId, facade));
                strHTMLText.Append("    </td>");
                strHTMLText.Append("</tr>");
            }
            strHTMLText.Append("<tr>");
            strHTMLText.Append("    <td>");
            // strHTMLText.Append(GetApplicantUploadedResumesEmailBody(lsvUploadedDocument, memberId)); // 1.1
            strHTMLText.Append("    </td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("</table>");
            return strHTMLText.ToString();
        }

        #endregion

        #region Interview Notification

        public String GetInterviewScheduleEmailBody()
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Interview Schedule</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=21% nowrap>Date:</td>");

            strHTMLText.Append("                                <td>" + _interviewDate + "</td>");
            strHTMLText.Append("                            </tr>");
            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=21% nowrap>Time:</td>");

            strHTMLText.Append("                                <td>" + _interviewTime + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=21% nowrap>Reason:</td>");
            // Defect id 9522
            if (string.IsNullOrEmpty(_interviewRequisition))
                strHTMLText.Append("                                <td>" + _interviewType + "</td>");
            else
                strHTMLText.Append("                                <td>" + _interviewType + " for " + _interviewRequisition + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=21% nowrap>Location:</td>");
            strHTMLText.Append("                                <td>" + _interviewLocation + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% colspan=2 nowrap>Remarks:</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td colspan=2>" + _interviewNote.Replace("\r\n", "<br/>") + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append(GetEmailFooter());

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        #endregion

        #region Tps360Access

        public string PrepareViewforTps360Access(Member member)
        {
            MembershipUser user = Membership.GetUser(member.UserId);
            if (user.IsLockedOut)
            {
                return string.Empty;
            }
            //8990
            string PassWord = "**********"; //user.GetPassword();
            string UserName = user.UserName;
            SecureUrl url = null;
            StringBuilder strHTMLText = new StringBuilder();

            strHTMLText.Append("<table border=0 align=left cellpadding=0 cellspacing=0 width=100%>");

            //Add header image

            strHTMLText.Append("<tr>");
            strHTMLText.Append("    <td  align=left>Dear " + member.FirstName + " " + member.LastName + ",<br /><br /></td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("<tr>");
            strHTMLText.Append("    <td  align=left>You are invited to login and view our web site. Please find enclosed your user id and password:<br /></td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("<tr>");
            strHTMLText.Append("  <td>");
            strHTMLText.Append("      <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("     <tr>");
            strHTMLText.Append("          <td>Login Details:</td>");
            strHTMLText.Append("       </tr>");

            strHTMLText.Append("        </table>");
            strHTMLText.Append("    </td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("<tr>");
            strHTMLText.Append("    <td>");
            strHTMLText.Append("        <table width=100% border=0 cellspacing=1 cellpadding=3>");
            strHTMLText.Append("        <tr>");
            strHTMLText.Append("            <td width=21% nowrap align=\"right\">User Id :</td>"); // 8990
            strHTMLText.Append("            <td >" + UserName + "</td>");
            strHTMLText.Append("        </tr>");
            strHTMLText.Append("        <tr>");
            strHTMLText.Append("            <td width=21% nowrap align=\"right\">Password :</td>"); // 8990
            strHTMLText.Append("            <td >" + PassWord + "</td>");
            strHTMLText.Append("        </tr>");
            strHTMLText.Append("        <tr>");
            strHTMLText.Append("            <td width=21% nowrap align=\"right\">Application Link :</td>"); // 8990
            if (Roles.IsUserInRole(member.PrimaryEmail, ContextConstants.ROLE_CANDIDATE))
            {
                url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.CandidatePortal.LOGIN.Replace("~/", string.Empty), string.Empty);
            }
            else
            {
                url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.LOGIN_PAGE, string.Empty);
            }
            strHTMLText.Append("            <td ><a href=\"" + url.ToString() + "\">" + url.ToString() + "</a></td>");
            strHTMLText.Append("        </tr>");
            strHTMLText.Append("        </table>");
            strHTMLText.Append("    </td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("</table>");
            return strHTMLText.ToString();






        }

        #endregion


        #region Tps360Access
        //************New code added by pravin khot on 16/May/2016********
        public string PrepareViewforTps360AccessCandidate(Member member)
        {
            MembershipUser user = Membership.GetUser(member.UserId);
            if (user.IsLockedOut)
            {
                return string.Empty;
            }
            //8990
            string PassWord = "**********"; //user.GetPassword();
            string UserName = user.UserName;
            SecureUrl url = null;
            StringBuilder strHTMLText = new StringBuilder();

            strHTMLText.Append("<table border=0 align=left cellpadding=0 cellspacing=0 width=100%>");

            //Add header image

            strHTMLText.Append("<tr>");
            strHTMLText.Append("    <td  align=left>Dear " + member.FirstName + " " + member.LastName + ",<br /><br /></td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("<tr>");
            strHTMLText.Append("    <td  align=left>Please find below the login credentials to your profile on our system, where you can<br /></td></tr>");
            strHTMLText.Append("<tr><td  align=left>update your profile, apply for current openings and upload documents.<br /></td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("<tr>");
            strHTMLText.Append("  <td>");
            strHTMLText.Append("      <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("     <tr>");
            strHTMLText.Append("          <td>Login Details:</td>");
            strHTMLText.Append("       </tr>");

            if (Roles.IsUserInRole(member.PrimaryEmail, ContextConstants.ROLE_CANDIDATE))
            {
                url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.CandidatePortal.LOGIN.Replace("~/", string.Empty), string.Empty);
            }
            else
            {
                url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.LOGIN_PAGE, string.Empty);
            }

            strHTMLText.Append("        </table>");
            strHTMLText.Append("    </td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("<tr>");
            strHTMLText.Append("    <td>");
            strHTMLText.Append("        <table width=100% border=0 cellspacing=1 cellpadding=3>");
            strHTMLText.Append("        <tr>");
            strHTMLText.Append("            <td width=18% nowrap align=\"right\">Application Link :</td>"); // 8990
            strHTMLText.Append("            <td ><a href=\"" + url.ToString() + "\">" + url.ToString() + "</a></td>");
            strHTMLText.Append("        </tr>");
            strHTMLText.Append("        <tr>");
            strHTMLText.Append("            <td width=18% nowrap align=\"right\">Login Id :</td>"); // 8990
            strHTMLText.Append("            <td >" + UserName + "</td>");
            strHTMLText.Append("        </tr>");
            strHTMLText.Append("        <tr>");
            strHTMLText.Append("            <td width=18% nowrap align=\"right\">Password :</td>"); // 8990
            strHTMLText.Append("            <td >" + PassWord + "</td>");
            strHTMLText.Append("        </tr>");
            strHTMLText.Append("        </table>");
            strHTMLText.Append("    </td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("     <tr>");
            strHTMLText.Append("       <td>  </td>");
            strHTMLText.Append("     </tr>");

            strHTMLText.Append("     <tr>");
            strHTMLText.Append("          <td>Best Regards,</td>");
            strHTMLText.Append("       </tr>");

            strHTMLText.Append("    <tr>");
            strHTMLText.Append("    <tr></tr>");
            strHTMLText.Append("    <tr><td>Talent Acquisition Team <br>[COMPANYNAME] </td></tr>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            Hashtable siteSettingTable = null;
            TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
            SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);

            strHTMLText = strHTMLText.Replace("[COMPANYNAME]", siteSettingTable[DefaultSiteSetting.CompanyName.ToString()].ToString());

            return strHTMLText.ToString();
        }
        //***************************END***************************
        #endregion


        #region Tps360Access
        //************New code added by pravin khot on 20/May/2016********
        public string CandidateChangePassword(Member member)
        {
            MembershipUser user = Membership.GetUser(member.UserId);
            if (user.IsLockedOut)
            {
                return string.Empty;
            }
            //8990
            string PassWord = "**********"; //user.GetPassword();
            string UserName = user.UserName;
            SecureUrl url = null;
            StringBuilder strHTMLText = new StringBuilder();

            strHTMLText.Append("<table border=0 align=left cellpadding=0 cellspacing=0 width=100%>");

            //Add header image

            strHTMLText.Append("<tr>");
            strHTMLText.Append("    <td  align=left>Dear " + member.FirstName + " " + member.LastName + ",<br /><br /></td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("<tr>");
            strHTMLText.Append("    <td  align=left>Please find below the login credentials to your profile on our system, where you can<br /></td></tr>");
            strHTMLText.Append("<tr><td  align=left>update your profile, apply for current openings and upload documents.<br /></td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("<tr>");
            strHTMLText.Append("  <td>");
            strHTMLText.Append("      <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("     <tr>");
            strHTMLText.Append("          <td>Login Details:</td>");
            strHTMLText.Append("       </tr>");

            url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.CandidatePortal.LOGIN.Replace("~/", string.Empty), string.Empty);

            strHTMLText.Append("        </table>");
            strHTMLText.Append("    </td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("<tr>");
            strHTMLText.Append("    <td>");
            strHTMLText.Append("        <table width=100% border=0 cellspacing=1 cellpadding=3>");
            strHTMLText.Append("        <tr>");
            strHTMLText.Append("            <td width=18% nowrap align=\"right\">Application Link :</td>"); // 8990
            strHTMLText.Append("            <td ><a href=\"" + url.ToString() + "\">" + url.ToString() + "</a></td>");
            strHTMLText.Append("        </tr>");
            strHTMLText.Append("        <tr>");
            strHTMLText.Append("            <td width=18% nowrap align=\"right\">Login Id :</td>"); // 8990
            strHTMLText.Append("            <td >" + UserName + "</td>");
            strHTMLText.Append("        </tr>");
            strHTMLText.Append("        <tr>");
            strHTMLText.Append("            <td width=18% nowrap align=\"right\">Password :</td>"); // 8990
            strHTMLText.Append("            <td >[Password]</td>");
            strHTMLText.Append("        </tr>");
            strHTMLText.Append("        </table>");
            strHTMLText.Append("    </td>");
            strHTMLText.Append("</tr>");
            strHTMLText.Append("     <tr>");
            strHTMLText.Append("       <td>  </td>");
            strHTMLText.Append("     </tr>");

            strHTMLText.Append("     <tr>");
            strHTMLText.Append("          <td>Best Regards,</td>");
            strHTMLText.Append("       </tr>");

            strHTMLText.Append("    <tr>");
            strHTMLText.Append("    <tr></tr>");
            strHTMLText.Append("    <tr><td>Talent Acquisition Team <br>[COMPANYNAME] </td></tr>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            Hashtable siteSettingTable = null;
            TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
            SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);

            strHTMLText = strHTMLText.Replace("[COMPANYNAME]", siteSettingTable[DefaultSiteSetting.CompanyName.ToString()].ToString());

            return strHTMLText.ToString();
        }
        //***************************END***************************
        #endregion

        #region Common

        public string GetEmailFooter()
        {


            StringBuilder emailFooter = new StringBuilder();
            emailFooter.Append(" <table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            emailFooter.Append("    <tr>");
            emailFooter.Append("        <td><div align=justify>");
            if (!string.IsNullOrEmpty(_senderSignature))
            {
                //emailFooter.Append("<br/>"); //1.2 - Defect id 10311 Gopal
                emailFooter.Append(_senderSignature);
            }
            emailFooter.Append("<br/>");

            if (_signatureFile != "")
            {
                if (_isPreview)
                {
                    if (File.Exists(HttpContext.Current.Server.MapPath(_signatureFile)))
                    {
                        emailFooter.Append("<img src='" + _signatureFile.Replace("../", UrlConstants.ApplicationBaseUrl) + "' style='border-width:0px;' />");//1.2 Defect Id:10311
                        emailFooter.Append("<br/><br/>");//1.2 Defect Id:10311
                    }
                }
                else
                {
                    if (File.Exists(HttpContext.Current.Server.MapPath(_signatureFile)))
                    {
                        emailFooter.Append("<img src=\"cid:Member_Signature\" style='border-width:0px;'/>");
                        emailFooter.Append("<br/><br/>");//Defect Id: 10311
                    }

                }
            }

            // emailFooter.Append("<br/><strong>Note:</strong> Some spam filters may prevent this message from reaching you. If you use a filter, please take a moment to add (<a href='mailto:" + _fromEmail + "'>" + _fromEmail + "</a>) to your email address book or safe list to prevent blocking. To remove yourself from this mailing list, please respond to this message with 'Remove' in the subject line.");

            emailFooter.Append("        </div></td>");
            emailFooter.Append("    </tr>");
            emailFooter.Append("    <tr>");

            if (_isPreview)
            {
                //emailFooter.Append("<td align=left>");
                //emailFooter.Append("Powered by&nbsp;&nbsp;<a href='http://www.tps360.com' target='_blank'>www.tps360.com</a>");
                //   emailFooter.Append("&nbsp;&nbsp;<img src='" + _companyLogo + "' style='height:56px;width:150px;'/>");
                //if (_companyLogo != null)//1.2 10311 gopala
                //{
                //    emailFooter.Append("&nbsp;&nbsp;<img src='" + _companyLogo.Replace("../", UrlConstants.ApplicationBaseUrl) + "' style='height:56px;width:150px;'/>");//1.2 10311 gopala
                //    emailFooter.Append("</td>");
                //}
                //else
                //{
                //    emailFooter.Append("</td>");
                //}

            }
            else
            {
                //emailFooter.Append("<td align=left>");
                //emailFooter.Append("Powered by&nbsp;&nbsp;<a href='http://www.tps360.com' target='_blank'>www.tps360.com</a>");
                //emailFooter.Append("&nbsp;&nbsp;<img src=\"cid:Email_Company_Logo\" style='height:56px;width:150px;'/>");
                //emailFooter.Append("</td>");
            }

            emailFooter.Append("    </tr>");
            emailFooter.Append("</table>");
            return emailFooter.ToString();
        }

        private string GetStyle()
        {
            StringBuilder strStyle = new StringBuilder();
            strStyle.Append(@"  <link href=http://www.delphistaffing.net/StyleSheets/Client/styles.css rel=stylesheet type=text/css />
                                <style type=text/css>
                                TD.JSBorderLine
                                {
                                    background-color:#D3D3D3;
                                }
                                TD.JSFormHeading
                                {
                                    BACKGROUND-COLOR: #006699; font-weight:bold; Color:white;font-family:Verdana; 
                                    text-align:left;
                                    font-size:12px;
                                }
                                .JSFormHeading
                                {
                                    BACKGROUND-COLOR: #006699; font-weight:bold; Color:white;font-family:Verdana; 
                                    text-align:center;
                                    font-size:12px;
                                }
                                TD.NewFormValue
                                {
                                    BACKGROUND-COLOR: white;
                                    font-weight:normal;
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                .NewFormValue
                                {
                                    BACKGROUND-COLOR: white;
                                    font-weight:normal;
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                .NewFormValue1
                                {
                                    BACKGROUND-COLOR: #aa97f0;
                                    font-weight:normal;
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                .NewFormValue2
                                {
                                    BACKGROUND-COLOR: #cce599
                                    font-weight:normal;
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                .AlternateItem
                                {
                                    BACKGROUND-COLOR: #e1f2fb;
                                    font-weight:normal; 
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                TD.JSFormField
                                {
                                    BACKGROUND-COLOR: #e1f2fb;
                                    font-weight:bold; 
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                .JSFormField
                                {
                                    BACKGROUND-COLOR: #e1f2fb;
                                    font-weight:normal; 
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                </style>");
            return strStyle.ToString();
        }

        #endregion

        #region ATS Alerts

        public String GetAlertResumeUpdate(string strCandidateName, string strCandidateEmail)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Resume Updated</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Name</td>");
            strHTMLText.Append("                                <td>" + strCandidateName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Email</td>");
            strHTMLText.Append("                                <td>" + strCandidateEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append(GetEmailFooter());

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertAppliedForJob(string strCandidateName, string strCandidateEmail, string strRequisitionTitle)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Applied For a Job</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Name</td>");
            strHTMLText.Append("                                <td>" + strCandidateName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Email</td>");
            strHTMLText.Append("                                <td>" + strCandidateEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Requisition</td>");
            strHTMLText.Append("                                <td>" + strRequisitionTitle + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append(GetEmailFooter());

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertInterviewFeedback(string strCandidateName, string strCandidateEmail, DateTime dtInterviewTime, string strLocation, string strJob, string strFeedback)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Feedback For Interview</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Name</td>");
            strHTMLText.Append("                                <td>" + strCandidateName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Email</td>");
            strHTMLText.Append("                                <td>" + strCandidateEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td nowrap>Interview Date & Time:</td>");
            strHTMLText.Append("                                <td>" + dtInterviewTime.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=21% nowrap>Location:</td>");
            strHTMLText.Append("                                <td>" + strLocation + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=21% nowrap>For Job:</td>");
            strHTMLText.Append("                                <td>" + strJob + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Feedback</td>");
            strHTMLText.Append("                                <td>" + strFeedback + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append(GetEmailFooter());

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertCompleteTest(string strCandidateName, string strCandidateEmail, string strTestName, string strScore)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Completed a Test</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Name</td>");
            strHTMLText.Append("                                <td>" + strCandidateName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Email</td>");
            strHTMLText.Append("                                <td>" + strCandidateEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Test Name</td>");
            strHTMLText.Append("                                <td>" + strTestName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Score</td>");
            strHTMLText.Append("                                <td>" + strScore + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append(GetEmailFooter());

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertReferFriend(string strCandidateName, string strCandidateEmail, string strFriendName, string strFriendEmail)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Refere a Friend</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Name</td>");
            strHTMLText.Append("                                <td>" + strCandidateName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Email</td>");
            strHTMLText.Append("                                <td>" + strCandidateEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Friend Name</td>");
            strHTMLText.Append("                                <td>" + strFriendName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Friend Email</td>");
            strHTMLText.Append("                                <td>" + strFriendEmail + "</td>");
            strHTMLText.Append("                            </tr>");


            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append(GetEmailFooter());

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertChangeAvailability(string strCandidateName, string strCandidateEmail, string strCurrentAvailability)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Change Availability Status</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Name</td>");
            strHTMLText.Append("                                <td>" + strCandidateName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Email</td>");
            strHTMLText.Append("                                <td>" + strCandidateEmail + "</td>");
            strHTMLText.Append("                            </tr>");


            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Current Availability Status</td>");
            strHTMLText.Append("                                <td>" + strCurrentAvailability + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append(GetEmailFooter());

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertSubmitReference(string strCandidateName, string strCandidateEmail, string strReferenceType, string strReferenceName, string strReferenceEmail)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Add New Reference</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Name</td>");
            strHTMLText.Append("                                <td>" + strCandidateName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Email</td>");
            strHTMLText.Append("                                <td>" + strCandidateEmail + "</td>");
            strHTMLText.Append("                            </tr>");


            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Reference Type</td>");
            strHTMLText.Append("                                <td>" + strReferenceType + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Reference Name</td>");
            strHTMLText.Append("                                <td>" + strReferenceName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Reference Email</td>");
            strHTMLText.Append("                                <td>" + strReferenceEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append(GetEmailFooter());

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertAddToHotList(string strCandidateName, string strCandidateEmail, string strHotlistName)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Added To a HotList</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Name</td>");
            strHTMLText.Append("                                <td>" + strCandidateName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Email</td>");
            strHTMLText.Append("                                <td>" + strCandidateEmail + "</td>");
            strHTMLText.Append("                            </tr>");


            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Hot List Title</td>");
            strHTMLText.Append("                                <td>" + strHotlistName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append(GetEmailFooter());

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertConvertedToConsultant(string strCandidateName, string strCandidateEmail)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Converted To Consultant</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Name</td>");
            strHTMLText.Append("                                <td>" + strCandidateName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Email</td>");
            strHTMLText.Append("                                <td>" + strCandidateEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append(GetEmailFooter());

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        #endregion


        //Code introduced by Prasanth on 21/Oct/2016
        #region Amezon
        public string AmezonSend(int CurrentUserID)
        {

            int count = 0;
            if (mailFrom != String.Empty)
            {
                if (To != null)
                {

                    if (mailSubject != string.Empty || mailBody != string.Empty || Attachments.Count > 0)
                    {

                        MailMessage mailMessage = new MailMessage();

                        //SmtpClient sendMail = new SmtpClient();

                        MailAddress addressFrom = new MailAddress(mailFrom);
                        mailMessage.From = addressFrom;
                        foreach (String strTo in To)
                        {
                            try
                            {
                                string[] candidatesids;
                                if (strTo.Contains(";"))
                                    candidatesids = strTo.Split(';');
                                else
                                    candidatesids = new string[] { strTo };
                                foreach (string applicantId in candidatesids)
                                {
                                    mailMessage.To.Add(applicantId);
                                }
                            }
                            catch
                            {
                                errorMessage = errorMessage + "Invalid Email";
                            }
                        }
                        mailMessage.Subject = mailSubject;

                        //CC
                        if (Cc != null)
                        {
                            foreach (String strCc in Cc)
                            {
                                try
                                {
                                    string[] candidatesids;
                                    if (strCc.Contains(";"))
                                        candidatesids = strCc.Split(';');
                                    else
                                        candidatesids = new string[] { strCc };
                                    foreach (string applicantId in candidatesids)
                                    {
                                        mailMessage.CC.Add(applicantId);
                                    }
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }
                        //Bcc
                        if (Bcc != null)
                        {
                            foreach (String strBcc in Bcc)
                            {
                                try
                                {
                                    string[] candidatesids;
                                    if (strBcc.Contains(";"))
                                        candidatesids = strBcc.Split(';');
                                    else
                                        candidatesids = new string[] { strBcc };
                                    foreach (string applicantId in candidatesids)
                                    {
                                        mailMessage.Bcc.Add(applicantId);
                                    }
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }

                        //Add Mail Attachments
                        if (Attachments != null)
                        {
                            foreach (String attachmentPath in Attachments)
                            {
                                try
                                {
                                    Attachment newAttachment = new Attachment(attachmentPath);
                                    mailMessage.Attachments.Add(newAttachment);
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }

                        //Add Email Banner Image
                        System.Web.UI.Page page = new Page();
                        if (LinkedResources != null)
                        {
                            AlternateView avMailMessage = AlternateView.CreateAlternateViewFromString(mailBody, null, MediaTypeNames.Text.Html);
                            foreach (ListItem maillinkedResource in LinkedResources)
                            {
                                try
                                {
                                    string resourcePath = maillinkedResource.Text;
                                    string contentID = maillinkedResource.Value;
                                    if (resourcePath != "" && contentID != "")
                                    {
                                        LinkedResource newLinkedResource = new LinkedResource(resourcePath);
                                        newLinkedResource.ContentId = contentID;
                                        newLinkedResource.ContentType.Name = resourcePath;
                                        avMailMessage.LinkedResources.Add(newLinkedResource);
                                    }
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                            mailMessage.AlternateViews.Add(avMailMessage);
                        }
                        else
                        {
                            mailMessage.Body = mailBody;
                        }

                        mailMessage.IsBodyHtml = true;

                        //SmtpClient mailSender = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]);

                        //bool IsError = false;
                        string strSMTP = string.Empty;
                        string strUser = string.Empty;
                        string strPwd = string.Empty;
                        string strssl = string.Empty;
                        int intPort = 587;
                        strSMTP = "email-smtp.us-west-2.amazonaws.com";

                        strUser = "AKIAJVKI7WVPBD7X67EA";
                        strPwd = "AoQ3k8IiDvdaST94l8MNZjCEP5q8irh8iMsDZNYInWcE";

                        //strssl = siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString();



                        SmtpClient mailSender = new SmtpClient(strSMTP, intPort);
                        //mailSender.Port = intPort;
                        //mailSender.Host = "email-smtp.us-west-2.amazonaws.com";
                        mailSender.Credentials = new System.Net.NetworkCredential(strUser, strPwd);
                        mailSender.EnableSsl = true;

                        //        NetworkCredential SMTPUserInfo = new NetworkCredential(strUser, strPwd);

                        //        mailSender.UseDefaultCredentials = false;
                        //        mailSender.Credentials = SMTPUserInfo;



                        //Hashtable MailSettingTable = null;
                        //Hashtable siteSettingTable = null;


                        //TPS360.BusinessFacade.Facade f = new TPS360.BusinessFacade.Facade();
                        //MemberExtendedInformation MailSetting = f.GetMemberExtendedInformationByMemberId(CurrentUserID);

                        //TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
                        //SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);

                        //siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);

                        //if (MailSetting.MailSetting != null)
                        //{
                        //    MailSettingTable = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);
                        //    try
                        //    {
                        //        strSMTP = siteSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();

                        //        strUser = siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                        //        strPwd = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();

                        //        strssl = siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString();
                        //    }
                        //    catch (Exception ex)
                        //    {

                        //    }

                        //    if (strSMTP != null && strSMTP != "")
                        //    {
                        //        if (MailSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != null && MailSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != "")
                        //        {
                        //            try
                        //            {
                        //                intPort = Convert.ToInt32(siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString());
                        //            }
                        //            catch (FormatException)
                        //            {
                        //                return "SMTP Port data is incorrect in settings";
                        //            }
                        //        }
                        //        mailSender = new SmtpClient(strSMTP, intPort);
                        //        NetworkCredential SMTPUserInfo = new NetworkCredential(strUser, strPwd);

                        //        mailSender.UseDefaultCredentials = false;
                        //        mailSender.Credentials = SMTPUserInfo;
                        //        mailSender.EnableSsl = strssl == "True" ? true : false;
                        //        count = count + 1;
                        //    }

                        //}
                        //else
                        //{
                        //    return "Mail Settings not available."; //added by pravin khot on 20/June/2016
                        //}


                        //if (count != 1 && count != 2)
                        //{
                        //    //mailSender = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]); //commented by pravin khot on 20/June/2016
                        //}
                        try
                        {
                            IFacade facade = new Facade();
                            Member mem = facade.GetMemberById(CurrentUserID);
                            MailAddress add = new MailAddress(mailMessage.From.ToString(), mem.FirstName + " " + mem.LastName);

                            if (mailMessage.To.Count > 0 || mailMessage.Bcc.Count > 0 || mailMessage.CC.Count > 0)
                            {
                                mailMessage.From = add;
                                mailMessage.Sender = add;
                                mailSender.Send(mailMessage);
                                mailMessage.Attachments.Dispose();
                                return "1";

                            }
                        }
                        catch (Exception ex)
                        {
                            BasePage.writeLog(ex);
                            errorMessage = errorMessage + "Error:" + ex.Message;
                        }
                        return errorMessage;
                    }
                    else
                    {
                        return "No Mail subject or body or attachment";
                    }

                }
                else
                {
                    return "To Address is absent";
                }
            }
            else
            {
                return "From Address is absent";
            }
        }
        #endregion

    }
}