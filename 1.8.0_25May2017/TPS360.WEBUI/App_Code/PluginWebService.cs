﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Web.Security;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Web.Configuration;
using System.Xml;
using TPS360.Common.BusinessEntities;
using TPS360.BusinessFacade;
using System.Data;
using TPS360.Common.Shared;
using System.IO.Compression;
using System.Collections.Generic;

/// <summary>
/// Summary description for PluginWebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class PluginWebService : System.Web.Services.WebService
{

    public PluginWebService()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent();
    }

    [WebMethod]
    public String ValidateWebService()
    {
        return "Success";
    }

    [WebMethod]
    public String ValidateUserLogin(String strUserName, String strPassword)
    {
        String strValidationResult = String.Empty;
        bool isValid = false;
        try
        {
            MembershipUser user = Membership.GetUser(strUserName);
            isValid = Membership.ValidateUser(user.UserName, strPassword);
            if (isValid)
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TPS360CS"].ConnectionString);
                conn.Open();

                //String strQuery = String.Empty;
                //strQuery = "select M.id from Member M " +
                //           "inner join aspnet_Users au on au.UserId = M.UserId " +
                //           "where au.UserName='"+ user.UserName + "'";
                //SqlCommand cmd = new SqlCommand(strQuery, conn);
                
                SqlCommand cmd = new SqlCommand("SP_PluginResumes_GetIDByUserName", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@UserName", user.UserName));
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        strValidationResult = dr["ID"].ToString();
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
        return strValidationResult;
    }

    [WebMethod]
    public String GetAllRequisitions(String strUserId)
    {
        String strAllRequisitions = String.Empty;
        
        try
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TPS360CS"].ConnectionString);
            conn.Open();

            //String strQuery = String.Empty;
            //  strQuery = "Select jobposting.Id as JobID, JobTitle, CompanyName from jobposting " +
            //             "Join Company " +
            //             "On JobPosting.ClientId=Company.Id and JobPosting.JobStatus=(select Id from GenericLookup where Name like 'Open for Recruitment') " +
            //             "AND jobposting.Id IN (SELECT DISTINCT JobPostingId FROM dbo.JobPostingHiringTeam WHERE MemberId=" + strUserId + ")";
            //SqlCommand cmd = new SqlCommand(strQuery, conn);
     
            SqlCommand cmd = new SqlCommand("SP_PluginResumes_GetRequisitionsByUserID", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@UserID", strUserId));
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    String strJobID = dr["JobID"].ToString();
                    String strJobTitle = dr["jobtitle"].ToString();
                    String strCompany = dr["CompanyName"].ToString();

                    if (strAllRequisitions == String.Empty)
                    {
                        strAllRequisitions = "{ \"id\" : \"" + strJobID + "\", \"title\" : \"" + strJobTitle + "\", \"parent\" : \"" + strCompany + "\"   }";
                    }
                    else
                    {
                        strAllRequisitions = strAllRequisitions + "," + "{ \"id\" : \"" + strJobID + "\", \"title\" : \"" + strJobTitle + "\", \"parent\" : \"" + strCompany + "\"   }";
                    }
                }
            }

            strAllRequisitions = "[" + strAllRequisitions + "]";
        }
        catch (Exception ex)
        {
            strAllRequisitions = ex.Message;
        }
        return strAllRequisitions;
    }

    [WebMethod]
    public String SaveResumeDocument(String strBinaryDocument64String, String strRequisitionID, String strCandName, String strResumeSource, String strCreatorID)
    {
        try
        {
            String strDocPath = String.Empty;
            strCandName = strCandName.Replace(" ", "");
            String strProfileName = DateTime.Now.ToString("yyyy_MM_dd__hhMMss") + ".docx";
            strProfileName = Path.GetFileName(strProfileName);

            if (strBinaryDocument64String.Contains("2007"))
                strProfileName = DateTime.Now.ToString("yyyy.MM.dd__") + strCandName + ".doc";
            else
                strProfileName = DateTime.Now.ToString("yyyy.MM.dd__") + strCandName + ".doc";

            byte[] BinaryDocument = Convert.FromBase64String(strBinaryDocument64String);

            String strFolderNamePath = ConfigurationManager.AppSettings.Get("ResumeSaveFolderPath");
            if (!Directory.Exists(strFolderNamePath))
                Directory.CreateDirectory(strFolderNamePath);

            strDocPath = strFolderNamePath + "\\" + strProfileName;

            byte[] byt = Convert.FromBase64String(strBinaryDocument64String);
            Stream readStream = new MemoryStream(byt);
            using (FileStream writeStream = new FileStream(strDocPath, FileMode.Create, FileAccess.Write))
            {
                writeStream.Write(byt, 0, byt.Length);
            }

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TPS360CS"].ConnectionString);
            conn.Open();
            
            //String strQuery = String.Empty;
            //strQuery = "INSERT INTO PluginResumes(RequisitionID, CandidateName, ResumePhysicalPath, IsParsed, ResumeSource, CreatorID) " +
            //           "VALUES ('" + strRequisitionID + "','" + strCandName + "','" + strDocPath + "','Not Started','" + strResumeSource + "','" + strCreatorID + "')";
            //SqlCommand cmd = new SqlCommand(strQuery, conn);

            SqlCommand cmd = new SqlCommand("SP_PluginResumes_InsertResumeInformation", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@RequisitionID", strRequisitionID));
            cmd.Parameters.Add(new SqlParameter("@CandidateName", strCandName));
            cmd.Parameters.Add(new SqlParameter("@ResumePhysicalPath", strDocPath));
            cmd.Parameters.Add(new SqlParameter("@IsParsed", "Not Started"));
            cmd.Parameters.Add(new SqlParameter("@ResumeSource", strResumeSource));
            cmd.Parameters.Add(new SqlParameter("@CreatorID", strCreatorID));
            
            cmd.ExecuteNonQuery();
            return strDocPath;
        }
        catch (Exception ex)
        {
            return "strRes + Exception: " + ex.StackTrace.ToString();
        }
    }

    IFacade facade = new Facade();
    String strParserAccountID = String.Empty;
    String strParserServiceKey = String.Empty;
    String strParserWebServiceUrl = String.Empty;
    String strParserConfiguration = String.Empty;
    String _role = String.Empty;
    int CreatorID = 0;
    Member m_member;

    [WebMethod]
    public String ParseResumes(String userId)
    {
        String strResult = String.Empty;
        try
        {
            m_member = new Member();

            String strStatus = String.Empty;

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TPS360CS"].ConnectionString);
            conn.Open();

            //String strQuery = "select * from PluginResumes where IsParsed='Not Started'";
            //SqlCommand cmd = new SqlCommand(strQuery, conn);

            strStatus = "Not Started";
            SqlCommand cmd = new SqlCommand("SP_PluginResumes_GetResumesByStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Status", strStatus));
            
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    String strResumePhysicalPath = dr["ResumePhysicalPath"].ToString();
                    String strCreatorID = dr["CreatorID"].ToString();
                    String strResumeSource = dr["ResumeSource"].ToString();

                    SqlConnection connInside = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TPS360CS"].ConnectionString);
                    connInside.Open();

                    strStatus = "In-Progress";
                    //SqlCommand cmdInside = new SqlCommand("Update PluginResumes set IsParsed='" + strStatus + "' where ResumePhysicalPath='" + strResumePhysicalPath + "'", connInside);

                    SqlCommand cmdInside = new SqlCommand("SP_PluginResumes_UpdateStatusByPath", connInside);
                    cmdInside.CommandType = CommandType.StoredProcedure;
                    cmdInside.Parameters.Add(new SqlParameter("@Status", strStatus));
                    cmdInside.Parameters.Add(new SqlParameter("@ResumePath", strResumePhysicalPath));
                    cmdInside.ExecuteNonQuery();

                    int CandidateId = ParserandInsert(strResumePhysicalPath, strCreatorID, strResumeSource, connInside);
                    if (CandidateId != 0)
                    {
                        strStatus = "Completed";
                        //cmdInside = new SqlCommand("Update PluginResumes set IsParsed='" + strStatus + "', CandidateID='" + CandidateId + "' where ResumePhysicalPath='" + strResumePhysicalPath + "'", connInside);
                        cmdInside = new SqlCommand("SP_PluginResumes_UpdateStatusandCandidateByPath", connInside);
                        cmdInside.CommandType = CommandType.StoredProcedure;
                        cmdInside.Parameters.Add(new SqlParameter("@Status", strStatus));
                        cmdInside.Parameters.Add(new SqlParameter("@CandidateID", CandidateId));
                        cmdInside.Parameters.Add(new SqlParameter("@ResumePath", strResumePhysicalPath));
                        cmdInside.ExecuteNonQuery();
                    }
                }
            }
            strResult = "Success";
        }
        catch (Exception ex)
        {
            strResult = ex.Message + " -- " + ex.InnerException;
        }
        return strResult;
    }


    public int ParserandInsert(String strResumePhysicalPath, String strMemberID, String strResumeSource, SqlConnection connInside) //this is the method where you need to implement the parser technique
    {
        int candidateId = 0;
        try
        {
            String strPassword = String.Empty;
            String strResetPassword = String.Empty;
            Boolean ResumeUpdateDuplicate = true;
            Boolean StatusResumeUpdateDuplicate = true;
            CreatorID = Convert.ToInt32(strMemberID);

            if (WebConfigurationManager.AppSettings["ParserAccountID"].ToString() != string.Empty)
            {
                strParserAccountID = Convert.ToString(WebConfigurationManager.AppSettings["ParserAccountID"]);
            }
            if (WebConfigurationManager.AppSettings["ParserServiceKey"].ToString() != string.Empty)
            {
                strParserServiceKey = Convert.ToString(WebConfigurationManager.AppSettings["ParserServiceKey"]);
            }
            if (WebConfigurationManager.AppSettings["ParserWebServiceUrl"].ToString() != string.Empty)
            {
                strParserWebServiceUrl = Convert.ToString(WebConfigurationManager.AppSettings["ParserWebServiceUrl"]);
            }
            if (WebConfigurationManager.AppSettings["ParserConfiguration"].ToString() != string.Empty)
            {
                strParserConfiguration = Convert.ToString(WebConfigurationManager.AppSettings["ParserConfiguration"]);
            }

            byte[] fileBytes = File.ReadAllBytes(strResumePhysicalPath);
            fileBytes = Gzip(fileBytes);

            ParserService.ParsingService objClient = new ParserService.ParsingService();
            objClient.Url = strParserWebServiceUrl;

            ParserService.ParseResumeRequest objRequest = new ParserService.ParseResumeRequest();
            objRequest.AccountId = strParserAccountID;
            objRequest.ServiceKey = strParserServiceKey;
            objRequest.Configuration = strParserConfiguration;
            objRequest.OutputHtml = true;
            objRequest.FileBytes = fileBytes;

            ParserService.ParseResumeResponse objResponse = objClient.ParseResume(objRequest);
            if (objResponse.Code.ToString() == "Success")
            {
                XmlDocument xmldatadoc = new XmlDocument();
                xmldatadoc.LoadXml(objResponse.Xml);

                #region
                MembershipUser objnewUser;
                Member objMember = new Member();
                MemberExperience objMemberExperience = new MemberExperience();
                MemberDetail objMemberdetail = new MemberDetail();
                MemberExtendedInformation objMemberExtendedInfo = new MemberExtendedInformation();
                MemberObjectiveAndSummary objMemberObjectiveAndSummary = new MemberObjectiveAndSummary();

                String strUserEmail = ResumeValue_str(xmldatadoc, "sov:EmailAddresses", "sov:EmailAddress");
                if (strUserEmail != "")
                {
                    Member objMemberByemail = facade.GetMemberByMemberEmail(strUserEmail.Trim());
                    //_role = ContextConstants.ROLE_CANDIDATE.ToLower();
                    _role = "candidate";
                    if (objMemberByemail == null)
                    {
                        StatusResumeUpdateDuplicate = true;
                        objnewUser = Membership.GetUser(strUserEmail.Trim());
                        if (objnewUser != null)
                        {
                            Member objMemberOfUser = facade.GetMemberByMemberEmail(strUserEmail.Trim());
                            if (objMemberOfUser != null)
                            {
                                objnewUser.IsApproved = true;
                            }
                            else
                            {
                                objnewUser.IsApproved = true;
                            }
                        }
                        else
                        {
                            strPassword = "changeme";
                            try
                            {
                                objnewUser = Membership.CreateUser(strUserEmail.Trim(), strPassword, strUserEmail.Trim());
                            }
                            catch
                            {
                                throw new MembershipCreateUserException(MembershipCreateStatus.DuplicateUserName);
                            }
                        }
                        strUserEmail = strUserEmail.Trim();
                        System.Web.Security.MembershipUser user = System.Web.Security.Membership.GetUser(strUserEmail);
                        try
                        {
                            strResetPassword = user.ResetPassword();
                            user.ChangePassword(strPassword, strResetPassword);
                            Membership.UpdateUser(user);
                        }
                        catch (Exception ex)
                        {
                        }

                        if (objnewUser.IsApproved == true)
                        {
                            if (!Roles.IsUserInRole(objnewUser.UserName, _role))
                            {
                                Roles.AddUserToRole(objnewUser.UserName, _role);
                            }
                            objMember.UserId = (Guid)objnewUser.ProviderUserKey;
                        }

                    }
                    else
                    {
                        if (ResumeUpdateDuplicate == true)
                        {
                            StatusResumeUpdateDuplicate = true;
                            objMember.Id = objMemberByemail.Id;
                            objMember.UserId = objMemberByemail.UserId;
                            if (!Roles.IsUserInRole(strUserEmail, _role.ToLower()))
                            {
                                Roles.AddUserToRole(strUserEmail, _role.ToLower());
                            }
                        }
                        else
                        {
                            StatusResumeUpdateDuplicate = false;
                        }
                    }

                    if (StatusResumeUpdateDuplicate == true)
                    {
                        BuildMember_Online(xmldatadoc, objMember);
                        Member objMemberNew = facade.GetMemberByMemberEmail(strUserEmail.Trim());
                        BuildMemberManager_Online(objMemberNew.Id, CreatorID);

                        objMember.Id = objMemberNew.Id;

                        candidateId = objMember.Id;

                        DeleteDetails(objMember.Id);

                        BuildMemberExperience_Online(xmldatadoc, objMemberExperience, "", objMember);
                        BuildMemberDetails_Online(xmldatadoc, objMemberdetail, objMember);
                        BuildMemberExtentedInformation_Online(xmldatadoc, objMemberExtendedInfo, objMember, strResumeSource);
                        BuildMemberObjectiveAndSummary_Online(xmldatadoc, objMemberObjectiveAndSummary, objMember, strResumePhysicalPath);
                        BuildMemberEducation_Online(xmldatadoc, objMember);
                        AddSkillMap_Online(xmldatadoc, objMember);
                        AddCertificationMap_Online(xmldatadoc, objMember);
                        UploadDocument(strResumePhysicalPath, objMember);
                    }
                }
                else
                {
                    String strComments = "Email Id not found or Insufficient Data";
                    String strStatus = "Failed";

                    SqlCommand cmdInside = new SqlCommand("SP_PluginResumes_UpdateCommentsByPath", connInside);
                    cmdInside.CommandType = CommandType.StoredProcedure;
                    cmdInside.Parameters.Add(new SqlParameter("@Comments", strComments));
                    cmdInside.Parameters.Add(new SqlParameter("@ResumePath", strResumePhysicalPath));
                    cmdInside.Parameters.Add(new SqlParameter("@Status", strStatus));
                    cmdInside.ExecuteNonQuery();
                }
                #endregion
            }
        }
        catch (MembershipCreateUserException ex)
        {
            switch (ex.StatusCode)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    {
                        // TPS360.Web.UI.MiscUtil.ShowMessage(lblMessage, "User already exists.", true);  
                        try
                        {
                            facade.DeleteMemberById(m_member.Id);
                            Membership.DeleteUser(m_member.PrimaryEmail);
                        }
                        catch { Membership.DeleteUser(m_member.PrimaryEmail); }
                        break;
                    }
                case MembershipCreateStatus.InvalidUserName:
                    {
                        //  TPS360.Web.UI.MiscUtil.ShowMessage(lblMessage, "Invalid user name.", true);    
                        try
                        {
                            facade.DeleteMemberById(m_member.Id);
                            Membership.DeleteUser(m_member.PrimaryEmail);
                        }
                        catch { Membership.DeleteUser(m_member.PrimaryEmail); }
                        break;
                    }
            }
        }
        return candidateId;
    }


    private string ResumeValue_str(XmlDocument xmldatadoc, string Parent_Node, string Child_Node)
    {
        String rtn = "";
        XmlNodeList xmlNodeList = xmldatadoc.GetElementsByTagName(Parent_Node);
        foreach (XmlNode childNode in xmlNodeList)
        {
            foreach (XmlNode xmlnode in childNode)
            {
                if (xmlnode.Name == Child_Node)
                {
                    rtn = xmlnode.InnerText;
                }
            }
        }
        return rtn;
    }

    private DateTime ResumeValue_datetime(XmlDocument xmldatadoc, string Parent_Node, string Child_Node)
    {
        DateTime rtn = DateTime.MinValue;

        XmlNodeList xmlNodeList = xmldatadoc.GetElementsByTagName(Parent_Node);
        foreach (XmlNode childNode in xmlNodeList)
        {
            foreach (XmlNode xmlnode in childNode)
            {
                if (xmlnode.Name == Child_Node)
                {
                    if ((xmlnode.InnerText) != null || (xmlnode.InnerText != ""))
                    {
                        rtn = Convert.ToDateTime(xmlnode.InnerText);
                    }
                }

            }
        }
        return rtn;
    }

    public void BuildMember_Online(XmlDocument xmldatadoc, Member member)
    {
        String OutValue = string.Empty;
        String country = string.Empty;

        member.PrimaryEmail = ResumeValue_str(xmldatadoc, "sov:EmailAddresses", "sov:EmailAddress");
        member.FirstName = ResumeValue_str(xmldatadoc, "PersonName", "GivenName");
        member.MiddleName = ResumeValue_str(xmldatadoc, "PersonName", "MiddleName");
        member.LastName = ResumeValue_str(xmldatadoc, "PersonName", "FamilyName");
        member.NickName = OutValue;
        member.PermanentAddressLine1 = ResumeValue_str(xmldatadoc, "DeliveryAddress", "AddressLine");
        member.PermanentAddressLine2 = ResumeValue_str(xmldatadoc, "PostalAddress", "Municipality");
        member.PermanentCity = ResumeValue_str(xmldatadoc, "PostalAddress", "Municipality");
        //baseparser.GetState(out OutValue);
        //member.PermanentStateId = Facade.GetStateIdByStateCode(OutValue);  
        country = ResumeValue_str(xmldatadoc, "sov:Culture", "sov:Country");

        string countrycode = ResumeValue_str(xmldatadoc, "PostalAddress", "CountryCode");
        member.PermanentCountryId = facade.GetCountryIdByCountryCode(countrycode);
        member.PermanentZip = ResumeValue_str(xmldatadoc, "PostalAddress", "PostalCode");

        member.PrimaryPhone = ResumeValue_str(xmldatadoc, "Telephone", "FormattedNumber");
        member.CellPhone = ResumeValue_str(xmldatadoc, "Mobile", "FormattedNumber");
        member.DateOfBirth = ResumeValue_datetime(xmldatadoc, "sov:PersonalInformation", "sov:DateOfBirth");

        //baseparser.GetState(out OutValue);
        member.PermanentStateId = facade.GetStateIdByStateCode(OutValue);
        member.PermanentCountryId = facade.GetCountryIdByCountryCode(OutValue);

        //member.CreatorId = CurrentMember.Id;
        //member.UpdatorId = CurrentMember.Id;
        member.CreatorId = CreatorID;
        member.UpdatorId = CreatorID;
        EmployeeReferral reff = facade.EmployeeReferral_GetByMemberId(member.Id);
        if (reff == null)
        {
            member.Status = 1;
        }
        if (member.Id > 0)
        {
            facade.UpdateMember(member);
        }
        else
        {
            facade.AddMember(member);
        }
    }

    public void DeleteDetails(int memberid)
    {
        //Change MemberDetail
        facade.DeleteMemberDetailByMemberId(memberid);

        //Delete MemberExperience
        facade.DeleteMemberExperienceByMemberId(memberid);

        //DeleteMemberEducation
        facade.DeleteMemberEducationByMemberId(memberid);

        //Delete MemberObjective and Summary
        //Facade.DeleteMemberObjectiveAndSummaryByMemberId(_memberId);

        //Delete MemberSkillmap
        facade.DeleteMemberSkillMapByMemberId(memberid);

        //Delete MemberCertificationMap
        facade.DeleteMemberCertificationMapByMemberId(memberid);

        //Delete MemberExtended information
        facade.DeleteMemberExtendedInformationByMemberId(memberid);
    }

    public void BuildMemberExperience_Online(XmlDocument xmldatadoc, MemberExperience m_memberExperience, string CountryName, Member member)
    {
        XmlNodeList EmploymentHistory_xmlNodeList = xmldatadoc.GetElementsByTagName("EmploymentHistory");
        m_memberExperience.MemberId = member.Id;

        if (EmploymentHistory_xmlNodeList != null)
        {
            if (m_memberExperience.MemberId > 0)
            {
                facade.DeleteMemberExperienceByMemberId(m_memberExperience.MemberId);
            }
        }
        foreach (XmlNode EmploymentHistory_childNode in EmploymentHistory_xmlNodeList)
        {
            foreach (XmlNode xmlnode in EmploymentHistory_childNode)
            {
                if (xmlnode.Name == "EmployerOrg")
                {
                    //foreach (XmlNode node in xmlnode)
                    //{
                    //if (node.Name == "EmployerOrg")
                    //{
                    foreach (XmlNode EmploymentHistory_node in xmlnode)
                    {
                        //MemberExperience m_memberExperience = new MemberExperience();
                        //m_memberExperience.MemberId = _memberId;
                        if (EmploymentHistory_node.Name == "EmployerOrgName")
                        {
                            m_memberExperience.CompanyName = EmploymentHistory_node.InnerText;
                        }
                        if (EmploymentHistory_node.Name == "Description") //roles and Responsibilities
                        {
                            m_memberExperience.Responsibilities = EmploymentHistory_node.InnerText;
                        }
                        if (EmploymentHistory_node.Name == "PositionHistory")
                        {
                            //m_memberExperience.PositionName = EmploymentHistory_node.ChildNodes.Item(0).InnerText;
                            //m_memberExperience.DateFrom = EmploymentHistory_node.ChildNodes.Item(3).InnerText.ToString() == "" ? Convert.ToDateTime("01/01/1970") : Convert.ToDateTime(EmploymentHistory_node.ChildNodes.Item(3).InnerText.ToString());
                            //m_memberExperience.DateTo = EmploymentHistory_node.ChildNodes.Item(4).InnerText.ToString() == "" ? Convert.ToDateTime("01/01/1970") : Convert.ToDateTime(EmploymentHistory_node.ChildNodes.Item(4).InnerText.ToString());
                            //m_memberExperience.Responsibilities = EmploymentHistory_node.ChildNodes.Item(2).InnerText;

                            foreach (XmlNode EmploymentHistory_Chilednode in EmploymentHistory_node)
                            {
                                if (EmploymentHistory_Chilednode.Name == "StartDate")
                                {
                                    foreach (XmlNode AnyDate_node in EmploymentHistory_Chilednode)
                                    {
                                        if (AnyDate_node.Name == "AnyDate" || AnyDate_node.Name == "YearMonth")
                                        {
                                            try
                                            {
                                                m_memberExperience.DateFrom = Convert.ToDateTime(AnyDate_node.InnerText);
                                            }
                                            catch
                                            {
                                            }
                                        }
                                    }
                                }
                                else if (EmploymentHistory_Chilednode.Name == "EndDate")
                                {
                                    foreach (XmlNode AnyDate_node in EmploymentHistory_Chilednode)
                                    {
                                        if (AnyDate_node.Name == "AnyDate" || AnyDate_node.Name == "StringDate")
                                        {
                                            try
                                            {
                                                m_memberExperience.DateTo = Convert.ToDateTime(AnyDate_node.InnerText);
                                            }
                                            catch
                                            {
                                            }
                                        }
                                    }
                                }
                                else if (EmploymentHistory_Chilednode.Name == "Description")
                                {

                                    m_memberExperience.Responsibilities = EmploymentHistory_Chilednode.InnerText;

                                }
                                else if (EmploymentHistory_Chilednode.Name == "Title")
                                {

                                    m_memberExperience.PositionName = EmploymentHistory_Chilednode.InnerText;

                                }
                                else if (EmploymentHistory_Chilednode.Name == "OrgInfo")
                                {
                                    foreach (XmlNode OrgInfo_node in EmploymentHistory_Chilednode)
                                    {
                                        if (OrgInfo_node.Name == "PositionLocation")
                                        {
                                            foreach (XmlNode PositionLocation_node in OrgInfo_node)
                                            {
                                                if (PositionLocation_node.Name == "CountryCode")
                                                {
                                                    //m_memberExperience.CountryLookupId = PositionLocation_node.InnerText;
                                                }
                                                else if (PositionLocation_node.Name == "Region")
                                                {
                                                    //m_memberExperience.StateId = PositionLocation_node.InnerText;
                                                }
                                                else if (PositionLocation_node.Name == "Municipality")
                                                {
                                                    m_memberExperience.City = PositionLocation_node.InnerText;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (EmploymentHistory_node.Name == "OrgInfo")
                        {
                            foreach (XmlNode OrgInfo_node in xmlnode)
                            {
                                if (OrgInfo_node.Name == "PositionLocation")
                                {
                                    foreach (XmlNode OrgInfo1_node in xmlnode)
                                    {
                                        if (OrgInfo1_node.Name == "CountryCode")
                                        {
                                            m_memberExperience.CountryLookupId = facade.GetCountryIdByCountryCode(OrgInfo_node.InnerText);
                                        }
                                        if (OrgInfo1_node.Name == "Region")
                                        {
                                            //m_memberExperience.StateId = Facade.GetCountryIdByCountryCode(Region_node.InnerText);
                                        }
                                        if (OrgInfo1_node.Name == "Municipality")
                                        {
                                            m_memberExperience.City = OrgInfo1_node.InnerText;
                                        }
                                    }
                                }
                            }
                        }
                        //Write code here for saving values into database
                        //}
                    }
                    m_memberExperience.CreatorId = m_memberExperience.UpdatorId = CreatorID;
                    facade.AddMemberExperience(m_memberExperience);
                }
            }
        }
    }
    public void BuildMemberManager_Online(int memberid, int CurrentMember)
    {
        facade.BuildMemberManager_ByMemberId(memberid, CurrentMember);
    }
    public void BuildMemberDetails_Online(XmlDocument xmldatadoc, MemberDetail memberDetail, Member member)
    {
        memberDetail.MemberId = member.Id;
        //if (SelectedState != "") if (memberDetail.CurrentStateId == 0) memberDetail.CurrentStateId = Convert.ToInt32(SelectedState);
        //if (SelectedCountry != "") if (memberDetail.CurrentCountryId == 0) memberDetail.CurrentCountryId = Convert.ToInt32(SelectedCountry); ;
        //memberDetail.OfficePhone = OfficeNumber;
        //memberDetail.HomePhone = HomeNumber;
        string strGend = string.Empty;

        XmlNodeList PersonalInfo_xmlNodeList = xmldatadoc.GetElementsByTagName("sov:PersonalInformation");
        if (PersonalInfo_xmlNodeList.Count > 0)
        {
            foreach (XmlNode PersonalInfo_childNode in PersonalInfo_xmlNodeList)
            {
                foreach (XmlNode xmlnode in PersonalInfo_childNode)
                {
                    if (xmlnode.Name == "sov:Gender")
                    {
                        if (xmlnode.InnerText.Contains(" Male ")) strGend = "Male";
                        else if (xmlnode.InnerText.Contains(" Female ")) strGend = "Female";

                        if (strGend == "Male" || strGend == "Female")
                        {
                            IList<GenericLookup> lookup = facade.GetAllGenericLookupByLookupTypeAndName(LookupType.Gender, strGend.Trim());
                            foreach (GenericLookup gen in lookup)
                            {
                                if (gen.Name == strGend)
                                    memberDetail.GenderLookupId = gen.Id;
                            }
                        }
                        else
                            memberDetail.GenderLookupId = 0;

                    }
                    if (xmlnode.Name == "sov:Gender")
                    {
                        string strMrStatus = string.Empty;
                        if (xmlnode.InnerText.Contains("Married")) strMrStatus = "Married";
                        else if (xmlnode.InnerText.Contains("Single") || xmlnode.InnerText.Contains(" Unmarried ") || xmlnode.InnerText.Contains(" UnMarried ") || xmlnode.InnerText.Contains(" Un Married ")) strMrStatus = "Single";
                        else if (xmlnode.InnerText.Contains("Divorced")) strMrStatus = "Divorced";
                        else if (xmlnode.InnerText.Contains("Widow")) strMrStatus = "Widow";
                        else if (xmlnode.InnerText.Contains("Separated ")) strMrStatus = "Separated";
                        if (strMrStatus == "Married" || strMrStatus == "Single" || strMrStatus == "Divorced" || strMrStatus == "Widow" || strMrStatus == "Separated")
                        {
                            IList<GenericLookup> lookup = facade.GetAllGenericLookupByLookupTypeAndName(LookupType.MaritalStatus, strMrStatus.Trim());
                            foreach (GenericLookup gen in lookup)
                            {
                                if (gen.Name == strMrStatus)
                                    memberDetail.MaritalStatusLookupId = gen.Id;
                            }
                        }
                        else
                            memberDetail.MaritalStatusLookupId = 0;
                    }
                }
                memberDetail.NumberOfChildren = 0;
                //memberDetail.CreatorId = memberDetail.UpdatorId = CurrentMember.Id;
                memberDetail.CreatorId = memberDetail.UpdatorId = CreatorID;
                if (memberDetail.Id > 0) facade.UpdateMemberDetail(memberDetail);
                else facade.AddMemberDetail(memberDetail);
            }
        }
        else
        {
            if (memberDetail.Id > 0) facade.UpdateMemberDetail(memberDetail);
            else facade.AddMemberDetail(memberDetail);
        }

    }
    public void BuildMemberExtentedInformation_Online(XmlDocument xmldatadoc, MemberExtendedInformation memberExtendedInfo, Member member, String strResumeSource)
    {
        memberExtendedInfo.MemberId = member.Id;
        String OutValue = String.Empty;
        float dblValue = 0;
        int intValue = 0;
        //baseparser.GetWorkPermit(out intValue); memberExtendedInfo.WorkAuthorizationLookupId = intValue;
        //baseparser.GetCurrentPosition(out OutValue); memberExtendedInfo.CurrentPosition = OutValue;
        memberExtendedInfo.WorkAuthorizationLookupId = intValue;
        memberExtendedInfo.CurrentPosition = OutValue;
        memberExtendedInfo.LastEmployer = OutValue;

        XmlNode xn;
        XmlNodeList EmploymentHistory_xmlNodeList = xmldatadoc.GetElementsByTagName("EmploymentHistory");
        foreach (XmlNode EmploymentHistory_childNode in EmploymentHistory_xmlNodeList)
        {
            foreach (XmlNode xmlnode in EmploymentHistory_childNode)
            {

                if (xmlnode.Name == "PositionHistory")
                {
                    xn = xmlnode.Attributes.GetNamedItem("currentEmployer");
                    if (xn.Value == "true")
                    {
                        memberExtendedInfo.LastEmployer = xmlnode.ChildNodes.Item(0).InnerText;
                    }
                }
            }
        }

        memberExtendedInfo.TotalExperienceYears = ResumeValue_str(xmldatadoc, "sov:ExperienceSummary", "sov:YearsOfWorkExperience");
        //memberExtendedInfo.PassportStatus = ResumeValue_str(xmldatadoc, "sov:PersonalInformation", "sov:PassportStatus");
        //memberExtendedInfo.SecurityClearance = ResumeValue_str(xmldatadoc, "sov:PersonalInformation", "sov:SecurityClearance");
        memberExtendedInfo.PassportNumber = ResumeValue_str(xmldatadoc, "sov:PersonalInformation", "sov:PassportNumber");

        if (strResumeSource == "Naukri")
        {
            memberExtendedInfo.SourceLookupId = 1010;
            memberExtendedInfo.SourceDescription = "1016";
        }
        else if (strResumeSource == "Monster")
        {
            memberExtendedInfo.SourceLookupId = 1010;
            memberExtendedInfo.SourceDescription = "1015";
        }
        else if (strResumeSource == "LinkedIn")
        {
            memberExtendedInfo.SourceLookupId = 1011;
            memberExtendedInfo.SourceDescription = "LinkedIn";
        }
        
        memberExtendedInfo.IdCardLookUpId = 22;
        memberExtendedInfo.IdCardDetail = "";
        memberExtendedInfo.LinkedinProfile = "";

        //memberExtendedInfo.CreatorId = memberExtendedInfo.UpdatorId = CurrentMember.Id;
        memberExtendedInfo.CreatorId = memberExtendedInfo.UpdatorId = CreatorID;
        if (memberExtendedInfo.Id > 0) facade.UpdateMemberExtendedInformation(memberExtendedInfo);
        else facade.AddMemberExtendedInformation(memberExtendedInfo);
    }
    public void BuildMemberObjectiveAndSummary_Online(XmlDocument xmldatadoc, MemberObjectiveAndSummary memberObjectiveAndSummary, Member member, String PreviewDOCFile)
    {
        String RawResumeText = String.Empty;
        String HTMLCopyPasteResume = String.Empty;
        Parser.DocumentConverter document = new Parser.DocumentConverter();

        memberObjectiveAndSummary.MemberId = member.Id;
        String OutValue = String.Empty;
        memberObjectiveAndSummary.ResumeHighlight = ResumeValue_str(xmldatadoc, "sov:ExperienceSummary", "sov:Description");
        memberObjectiveAndSummary.Summary = ResumeValue_str(xmldatadoc, "StructuredXMLResume", "ExecutiveSummary");


        XmlNodeList ObjectiveNodeList = xmldatadoc.GetElementsByTagName("Objective");

        foreach (XmlNode ObjectiveNodeList_childNode in ObjectiveNodeList)
        {
            foreach (XmlNode xmlnode in ObjectiveNodeList_childNode)
            {
                if (xmlnode.Name == "#text")
                {
                    memberObjectiveAndSummary.Objective = xmlnode.InnerText;
                }
            }
        }


        //baseparser.GetObjective(out OutValue); memberObjectiveAndSummary.Objective = OutValue;
        //baseparser.GetSummary(out OutValue); memberObjectiveAndSummary.Summary = OutValue;


        MemberObjectiveAndSummary objectiveandSummary = facade.GetMemberObjectiveAndSummaryByMemberId(member.Id);

        if (File.Exists(PreviewDOCFile))
        {
            RawResumeText = Parser.DocumentConverter.GetResumeText(PreviewDOCFile);
            try
            {
                byte[] data = document.ConvertTo(PreviewDOCFile, Parser.DocumentConverter.OutputTypes.HtmlFormatted);
                HTMLCopyPasteResume = System.Text.ASCIIEncoding.ASCII.GetString(data);
            }
            catch (System.Exception ex)
            {
            }
            memberObjectiveAndSummary.RawCopyPasteResume = HttpUtility.HtmlDecode(RawResumeText);
            memberObjectiveAndSummary.CopyPasteResume = HTMLCopyPasteResume;
        }


        if (objectiveandSummary != null)
        {
            objectiveandSummary.MemberId = member.Id;
            //objectiveandSummary.CreatorId = objectiveandSummary.UpdatorId = CurrentMember.Id;
            objectiveandSummary.CreatorId = objectiveandSummary.UpdatorId = CreatorID;
            objectiveandSummary.Id = objectiveandSummary.Id;
            objectiveandSummary.Objective = memberObjectiveAndSummary.Objective;
            objectiveandSummary.Summary = memberObjectiveAndSummary.Summary;
            objectiveandSummary.RawCopyPasteResume = memberObjectiveAndSummary.RawCopyPasteResume;
            objectiveandSummary.CopyPasteResume = memberObjectiveAndSummary.CopyPasteResume;
            //objectiveandSummary.ResumeHighlight = memberObjectiveAndSummary.ResumeHighlight;
            facade.UpdateMemberObjectiveAndSummary(objectiveandSummary);
        }
        else
        {
            memberObjectiveAndSummary.MemberId = member.Id;
            //memberObjectiveAndSummary.CreatorId = memberObjectiveAndSummary.UpdatorId = CurrentMember.Id;
            memberObjectiveAndSummary.CreatorId = memberObjectiveAndSummary.UpdatorId = CreatorID;
            facade.AddMemberObjectiveAndSummary(memberObjectiveAndSummary);
        }

    }
    public void BuildMemberEducation_Online(XmlDocument xmldatadoc, Member member)
    {
        MemberEducation m_memberEducation = new MemberEducation();
        XmlNodeList EducationHistory_xmlNodeList = xmldatadoc.GetElementsByTagName("EducationHistory");
        foreach (XmlNode EducationHistory_childNode in EducationHistory_xmlNodeList)
        {
            if (EducationHistory_childNode.Name == "EducationHistory")
            {
                foreach (XmlNode SchoolOrInstitutionMain_xmlnode in EducationHistory_childNode)
                {
                    if (SchoolOrInstitutionMain_xmlnode.Name == "SchoolOrInstitution")
                    {
                        foreach (XmlNode SchoolOrInstitution_xmlnode in SchoolOrInstitutionMain_xmlnode)
                        {
                            if (SchoolOrInstitution_xmlnode.Name == "School")
                            {
                                foreach (XmlNode SchoolOrInstitution_childnode in SchoolOrInstitution_xmlnode)
                                {
                                    if (SchoolOrInstitution_childnode.Name == "SchoolName")
                                    {
                                        m_memberEducation.InstituteName = SchoolOrInstitution_childnode.InnerText;
                                    }
                                }
                            }
                            else if (SchoolOrInstitution_xmlnode.Name == "Degree")
                            {
                                //foreach (XmlNode SchoolOrInstitution_childnode in SchoolOrInstitution_xmlnode)
                                //{
                                //    if (SchoolOrInstitution_childnode.Name == "Degree")
                                //    {
                                foreach (XmlNode Degree_xmlnode in SchoolOrInstitution_xmlnode)
                                {
                                    if (Degree_xmlnode.Name == "DegreeName")
                                    {
                                        m_memberEducation.DegreeTitle = Degree_xmlnode.InnerText;
                                    }
                                    else if (Degree_xmlnode.Name == "DegreeDate")
                                    {

                                        foreach (XmlNode DegreeDate_xmlnode in Degree_xmlnode)
                                        {
                                            if ((DegreeDate_xmlnode.Name == "AnyDate") || (Degree_xmlnode.InnerText != "") || (Degree_xmlnode.InnerText != ""))
                                            {
                                                try
                                                {
                                                    m_memberEducation.DegreeObtainedYear = Degree_xmlnode.InnerText;
                                                }
                                                catch
                                                {
                                                }

                                            }
                                        }

                                    }
                                    else if (Degree_xmlnode.Name == "DegreeMajor")
                                    {

                                        foreach (XmlNode DegreeMajor_xmlnode in Degree_xmlnode)
                                        {
                                            if (DegreeMajor_xmlnode.Name == "Name")
                                            {
                                                m_memberEducation.MajorSubjects = DegreeMajor_xmlnode.InnerText;

                                            }
                                        }
                                    }
                                    else if (Degree_xmlnode.Name == "DatesOfAttendance")
                                    {
                                        foreach (XmlNode DatesOfAttendance_xmlnode in Degree_xmlnode)
                                        {
                                            if (DatesOfAttendance_xmlnode.Name == "StartDate")
                                            {
                                                foreach (XmlNode StartDate_xmlnode in DatesOfAttendance_xmlnode)
                                                {
                                                    if ((StartDate_xmlnode.Name == "AnyDate") || ((StartDate_xmlnode.InnerText != "notKnown") && (StartDate_xmlnode.InnerText != "")))
                                                    {
                                                        try
                                                        {
                                                            m_memberEducation.AttendedFrom = Convert.ToDateTime(StartDate_xmlnode.InnerText);
                                                        }
                                                        catch
                                                        {
                                                        }

                                                    }
                                                }
                                            }
                                            else if (DatesOfAttendance_xmlnode.Name == "EndDate")
                                            {
                                                foreach (XmlNode EndDate_xmlnode in DatesOfAttendance_xmlnode)
                                                {
                                                    if ((EndDate_xmlnode.Name == "AnyDate") || (EndDate_xmlnode.InnerText != "notKnown") || (EndDate_xmlnode.InnerText != ""))
                                                    {
                                                        try
                                                        {
                                                            m_memberEducation.AttendedTo = Convert.ToDateTime(EndDate_xmlnode.InnerText);
                                                        }
                                                        catch
                                                        {
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (Degree_xmlnode.Name == "Comments")
                                    {
                                        m_memberEducation.BriefDescription = Degree_xmlnode.InnerText;
                                    }

                                }

                            }
                            //Write code here to save values into database
                            m_memberEducation.MemberId = member.Id;
                            facade.AddMemberEducation(m_memberEducation);
                        }
                    }
                }
            }
        }
        //DataTable EducatationInfo = new DataTable();
        //baseparser.GetEducation(out EducatationInfo);
        //IList<MemberEducation> membereducation = new List<MemberEducation>();
        //if (membereducation != null && EducatationInfo != null)
        //{
        //    for (int i = 0; i < EducatationInfo.Rows.Count; i++)
        //    {
        //        MemberEducation m_memberEducation = new MemberEducation();
        //        m_memberEducation.MemberId = _memberId;
        //        m_memberEducation.DegreeTitle = EducatationInfo.Rows[i][1].ToString();
        //        m_memberEducation.InstituteName = EducatationInfo.Rows[i][8].ToString();
        //        m_memberEducation.DegreeObtainedYear = EducatationInfo.Rows[i][7].ToString();
        //        m_memberEducation.MajorSubjects = EducatationInfo.Rows[i][2].ToString();
        //        m_memberEducation.LevelOfEducationLookupId = Convert.ToInt32(EducatationInfo.Rows[i][14]);
        //        m_memberEducation.GPA = EducatationInfo.Rows[i][3].ToString();
        //        m_memberEducation.GpaOutOf = EducatationInfo.Rows[i][4].ToString();
        //        m_memberEducation.AttendedFrom = EducatationInfo.Rows[i][5].ToString() == "" ? Convert.ToDateTime("01/01/1753") : Convert.ToDateTime(EducatationInfo.Rows[i][5]);
        //        m_memberEducation.AttendedTo = EducatationInfo.Rows[i][6].ToString() == "" ? Convert.ToDateTime("01/01/1753") : Convert.ToDateTime(EducatationInfo.Rows[i][6]);
        //        m_memberEducation.InstituteCountryId = Convert.ToInt32(EducatationInfo.Rows[i][15]);
        //        m_memberEducation.InstituteCity = EducatationInfo.Rows[i][11].ToString();
        //        m_memberEducation.BriefDescription = EducatationInfo.Rows[i][12].ToString();
        //        m_memberEducation.IsHighestEducation = Convert.ToBoolean(EducatationInfo.Rows[i][13]);
        //        m_memberEducation.StateId = Convert.ToInt32(EducatationInfo.Rows[i][16]);
        //        Facade.AddMemberEducation(m_memberEducation);
        //    }
        //}
    }

    public void AddSkillMap_Online(XmlDocument xmldatadoc, Member member)
    {

        MemberSkillMap memberskill = new MemberSkillMap();
        string skillname;
        XmlNodeList Qualifications_xmlNodeList = xmldatadoc.GetElementsByTagName("Qualifications");
        foreach (XmlNode Qualifications_childNode in Qualifications_xmlNodeList)
        {
            if (Qualifications_childNode.Name == "Qualifications")
            {
                foreach (XmlNode QualificationsMain_childNode in Qualifications_childNode)
                {

                    if (QualificationsMain_childNode.Name == "Competency")
                    {
                        foreach (XmlNode Competency_xmlnode in QualificationsMain_childNode)
                        {
                            if (Competency_xmlnode.Name == "CompetencyEvidence")
                            {
                                foreach (XmlNode Competency_childnode in Competency_xmlnode)
                                {
                                    if (Competency_childnode.Name == "NumericValue")
                                    {
                                        memberskill.YearsOfExperience = Convert.ToInt32(Competency_childnode.InnerText) / 12;
                                        skillname = Competency_xmlnode.Attributes.Item(0).Value;
                                        memberskill.SkillId = facade.GetSkillIdBySkillName(skillname);
                                        if (Competency_xmlnode.Attributes.Count > 3)
                                        {
                                            memberskill.LastUsed = Convert.ToDateTime(Competency_xmlnode.Attributes.Item(3).Value);
                                        }
                                        else
                                        {
                                            memberskill.LastUsed = DateTime.MinValue;
                                        }

                                        if (memberskill.SkillId > 0)
                                        {
                                            memberskill.MemberId = member.Id;
                                            facade.AddMemberSkillMap(memberskill);
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void AddCertificationMap_Online(XmlDocument xmldatadoc, Member member)
    {
        DataTable certicationmap = new DataTable();
        int j = 0;
        XmlNodeList Qualifications_xmlNodeList = xmldatadoc.GetElementsByTagName("Qualifications");
        foreach (XmlNode Qualifications_childNode in Qualifications_xmlNodeList)
        {
            if (Qualifications_childNode.Name == "Qualifications")
            {
                foreach (XmlNode QualificationsMain_childNode in Qualifications_childNode)
                {
                    if (QualificationsMain_childNode.Name == "Competency")
                    {
                        foreach (XmlNode Competency_xmlnode in QualificationsMain_childNode)
                        {
                            //certicationmap.Rows[j][0] = Competency_xmlnode.Name;
                            //certicationmap.Rows[j][1] = "";
                            //certicationmap.Rows[j][2] = "";
                            //certicationmap.Rows[j][3] = Competency_xmlnode.OwnerDocument;
                            //j++;
                        }
                    }
                }
            }
        }
        for (int i = 0; i < certicationmap.Rows.Count; i++)
        {
            MemberCertificationMap memberCertificationMap = new MemberCertificationMap();
            memberCertificationMap.MemberId = member.Id;
            memberCertificationMap.CerttificationName = certicationmap.Rows[i][0].ToString();
            if (certicationmap.Rows[i][1] != System.DBNull.Value && certicationmap.Rows[i][1].ToString().Trim() != "") memberCertificationMap.ValidFrom = Convert.ToDateTime(certicationmap.Rows[i][1].ToString());
            if (certicationmap.Rows[i][2] != System.DBNull.Value && certicationmap.Rows[i][2].ToString().Trim() != "") memberCertificationMap.ValidTo = Convert.ToDateTime(certicationmap.Rows[i][2].ToString());
            memberCertificationMap.IssuingAthority = certicationmap.Rows[i][3].ToString();
            facade.AddMemberCertificationMap(memberCertificationMap);
        }
    }

    private void UploadDocument(string PreviewDOCFile, Member newMember)
    {
        if (PreviewDOCFile != "")
        {
            Member member = facade.GetMemberById(newMember.Id);
            String strMessage = String.Empty;
            String _fileName = String.Empty;
            bool boolError = false;
            String UploadedFilename = Convert.ToString(PreviewDOCFile);
            String[] FileName_Split = UploadedFilename.Split('.');
            String ResumeName = member.FirstName + member.LastName + " - Resume." + FileName_Split[FileName_Split.Length - 1];
            UploadedFilename = ResumeName;
            //UploadedFilename = Title;

            String strFilePath = TPS360.Web.UI.MiscUtil.GetMemberDocumentPath(newMember.Id, UploadedFilename, "Word Resume", false);//1.6
            if (CheckFileSize(PreviewDOCFile))
            {
                if (File.Exists(strFilePath))
                {
                    System.IO.File.Copy(PreviewDOCFile, strFilePath,true);
                    File.Delete(PreviewDOCFile);
                    MemberDocument memberDocument = facade.GetMemberDocumentByMemberIdAndFileName(newMember.Id, UploadedFilename);//1.6
                    if (memberDocument == null)
                    {
                        MemberDocument newDoc = new MemberDocument();
                        newDoc.FileName = UploadedFilename;
                        newDoc.MemberId = newMember.Id;
                        newDoc.FileTypeLookupId = 55;
                        newDoc.Title = "Primary Resume";
                        newDoc.CreatorId = CreatorID;
                        facade.AddMemberDocument(newDoc);
                        TPS360.Web.UI.MiscUtil.AddActivity(_role, newMember.Id, 0, ActivityType.UploadDocument, facade);
                    }
                    else
                    {
                        memberDocument.FileName = UploadedFilename;
                        memberDocument.MemberId = newMember.Id;
                        memberDocument.UpdatorId = CreatorID;
                        facade.UpdateMemberDocument(memberDocument);
                        TPS360.Web.UI.MiscUtil.AddActivity(_role, newMember.Id, CreatorID, ActivityType.UploadDocument, facade);
                    }
                }
                else
                {
                    System.IO.File.Copy(PreviewDOCFile, strFilePath);
                    File.Delete(PreviewDOCFile);
                    MemberDocument memberDocument = facade.GetMemberDocumentByMemberIdAndFileName(newMember.Id, UploadedFilename);//1.6
                    if (memberDocument == null)
                    {
                        MemberDocument newDoc = new MemberDocument();
                        newDoc.FileName = UploadedFilename;
                        newDoc.MemberId = newMember.Id;
                        newDoc.FileTypeLookupId = 55;
                        newDoc.Title = "Primary Resume";
                        newDoc.CreatorId = CreatorID;
                        facade.AddMemberDocument(newDoc);
                        TPS360.Web.UI.MiscUtil.AddActivity(_role, newMember.Id, 0, ActivityType.UploadDocument, facade);
                    }
                    else
                    {
                        memberDocument.FileName = UploadedFilename;
                        memberDocument.MemberId = newMember.Id;
                        memberDocument.UpdatorId = CreatorID;
                        facade.UpdateMemberDocument(memberDocument);
                        TPS360.Web.UI.MiscUtil.AddActivity(_role, newMember.Id, CreatorID, ActivityType.UploadDocument, facade);
                    }
                }

                ////fuDocument.SaveAs(strFilePath);                   
                //if (File.Exists(strFilePath))
                //{
                //    //File.Delete(strFilePath);
                //}
                ////System.IO.File.Move (PreviewDOCFile, strFilePath);
                //System.IO.File.Copy(PreviewDOCFile, strFilePath);
                //MemberDocument memberDocument = facade.GetMemberDocumentByMemberIdAndFileName(newMember.Id, UploadedFilename);//1.6
                //if (memberDocument == null)
                //{
                //    MemberDocument newDoc = new MemberDocument();
                //    newDoc.FileName = UploadedFilename;
                //    newDoc.MemberId = newMember.Id;
                //    newDoc.FileTypeLookupId = 55;
                //    newDoc.Title = "Primary Resume";
                //    facade.AddMemberDocument(newDoc);
                //    TPS360.Web.UI.MiscUtil.AddActivity(_role, newMember.Id, 0, ActivityType.UploadDocument, facade);
                //    //if (string.IsNullOrEmpty(strMessage))
                //    //{
                //    //    strMessage = "Successfully uploaded the file";
                //    //}
                //}
                //else
                //{
                //    memberDocument.FileName = UploadedFilename;
                //    memberDocument.MemberId = newMember.Id;
                //    facade.UpdateMemberDocument(memberDocument);
                //    //MiscUtil.AddActivity(_role, newMember.Id, base.CurrentMember != null ? base.CurrentMember.Id : 0, ActivityType.UploadDocument, facade);
                //    TPS360.Web.UI.MiscUtil.AddActivity(_role, newMember.Id, 0, ActivityType.UploadDocument, facade);
                //    // strMessage = "Successfully updated the file";
                //}

            }
            else
            {
                boolError = true;
                strMessage = "File size should be less than 10 MB";
            }
        }

    }

    private static byte[] Gzip(byte[] bytes)
    {
        if (bytes == null)
        {
            return new byte[0];
        }
        if (bytes.Length == 0)
        {
            return bytes;
        }
        using (MemoryStream memStream = new MemoryStream(bytes.Length / 2))
        {
            using (GZipStream gzipStream = new GZipStream(memStream, CompressionMode.Compress))
            {
                gzipStream.Write(bytes, 0, bytes.Length);
            }
            return memStream.ToArray();
        }
    }

    private bool CheckFileSize(string PreviewDOCFile)
    {
        //PreviewDOCFile
        //int fileSize = Convert.ToInt32(fuDocument.FileContent.Length / (1024 * 1024));
        //if (fileSize > ContextConstants.MAXIMUM_UPLOAD_SIZE)
        //{
        //    return false;
        //}
        //else
        //{
        //    return true;
        //}
        return true;
    }







}