using System;
using System.Diagnostics;
using System.Web.Security;

using TPS360.BusinessFacade;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;

namespace TPS360.Web.UI
{
    public class EmployeeMasterPage : BaseMasterPage
    {
        #region Member Variables

        #endregion

        #region Properties

        public int CurrentEmployeeId
        {
            get
            {
                return ((EmployeeBasePage)this.Page).CurrentEmployeeId;

            }
        }

        public Member CurrentEmployee
        {
            get
            {
                return ((EmployeeBasePage)this.Page).CurrentEmployee;

            }
            set
            {
                ((EmployeeBasePage)this.Page).CurrentEmployee = value;
            }
        }

        #endregion

        #region Methods

        #endregion

        #region Events

        #endregion
    }
}