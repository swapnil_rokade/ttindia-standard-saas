/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: BasePage.cs
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
     0.1            20/May/2016         Sumit Sonawane    
 *  0.2             10/Jun/2016           Prasanth Kumar G    LDAP log file generation
 *  0.3             24Apr2017           Prasanth Kumar G      issue id 1044 (writeLog_DB)
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;
using System.Diagnostics;
using System.Web.Security;
using System.Web.UI;
using System.Collections;
using System.Linq;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using System.Web;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI.WebControls;
using System.DirectoryServices.Protocols;
using System.DirectoryServices;

namespace TPS360.Web.UI
{
    public class BasePage : System.Web.UI.Page
    {
        public BasePage()
        {
            this.Error += new EventHandler(BasePage_Error);
        }
        #region Member Variables

        private WebHelper _helper;
        private IFacade _facade;
        private static Regex _WhitespaceRemovalRegex = new Regex(@" {2,}|\t+", RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.CultureInvariant);
        private static Regex _LineTerminationRemoval = new Regex(@"\r\n\r\n|\r|^\s+|\s+$", RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.CultureInvariant);

        #endregion

        #region Properties

        protected MembershipUser CurrentUser
        {
            [DebuggerStepThrough()]
            get
            {
                return Membership.GetUser(CurrentUserName, true);
            }
        }

        protected bool IsUserAuthenticated
        {
            [DebuggerStepThrough]
            get
            {
                return Page.User.Identity.IsAuthenticated;
            }
        }

        protected string CurrentUserName
        {
            [DebuggerStepThrough]
            get
            {
                return IsUserAuthenticated ? Page.User.Identity.Name : "Anonymous";
            }
        }

        protected string CurrentUserRole
        {
            [DebuggerStepThrough]
            get
            {
                return (Roles.GetRolesForUser(CurrentUserName)).GetValue(0).ToString();
            }
        }

        protected Guid CurrentUserId
        {
            [DebuggerStepThrough]
            get
            {
                if (!IsUserAuthenticated)
                {
                    return Guid.Empty;
                }

                return (Guid)CurrentUser.ProviderUserKey;
            }
        }

        protected Member CurrentMember
        {
            [DebuggerStepThrough()]
            get
            {
                return Context.Items[ContextConstants.MEMBER] as Member;
            }
        }

        protected bool IsUserAdmin
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_ADMIN);
            }
        }

        protected bool IsUserEmployee
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_EMPLOYEE);
            }
        }

        protected bool IsUserConsultant
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_CONSULTANT);
            }
        }

        protected bool IsUserCandidate
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_CANDIDATE);
            }
        }

        protected bool IsUserVendor
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_VENDOR);
            }
        }

        protected bool IsUserClient
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_CLIENT);
            }
        }

        protected bool IsUserPartner
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_PARTNER);
            }
        }

        protected IFacade Facade
        {
            [DebuggerStepThrough()]
            get
            {
                if (_facade == null)
                {
                    _facade = Context.Items[ContextConstants.Facade] as IFacade;
                }

                return _facade;
            }
        }

        protected WebHelper Helper
        {
            [DebuggerStepThrough()]
            get
            {
                if (_helper == null)
                {
                    _helper = new WebHelper(this);
                }

                return _helper;
            }
        }

        protected Hashtable SiteSetting
        {
            [DebuggerStepThrough()]
            get
            {
                return Context.Items[ContextConstants.SITESETTING] as Hashtable;
            }
        }
        //protected bool isMainApplication
        //{
        //    get
        //    {
        //        if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"] != null)
        //        {
        //            if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower() == "landt")
        //            {
        //                return false;
        //            }
        //            return true;
        //        }
        //        return true;
        //    }
        //}

        protected ApplicationSource ApplicationSource
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"] != null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower() == "landt")
                    {
                        return ApplicationSource.LandTApplication;
                    }
                    else if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower() == "selectigence")
                    {
                        return ApplicationSource.SelectigenceApplication;
                    }
                    else if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower() == "genisys")
                    {
                        return ApplicationSource.GenisysApplication;
                    }
                    else
                    {
                        return ApplicationSource.MainApplication;
                    }

                }
                return ApplicationSource.MainApplication;
            }
        }


        /////////////Code Added by Sumit Sonawane on 20/May/2016/////////////////////////
        protected string ReportType
        {
            [DebuggerStepThrough()]
            get
            {
                string ParentID = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_ID];
                string reportType = "";
                if (ParentID == "120")
                {
                    reportType = ContextConstants.CANDIDATE_REPORT;
                }
                else if (ParentID == "669")
                {
                    reportType = ContextConstants.MY_CANDIDATE_REPORT;
                }
                else if (ParentID == "680")
                {
                    reportType = ContextConstants.TEAM_CANDIDATE_REPORT;
                }
                else if (ParentID == "636")
                {
                    reportType = ContextConstants.PRODUCTIVITY_REPORT;
                }
                else if (ParentID == "674")
                {
                    reportType = ContextConstants.MY_PRODUCTIVITY_REPORT;
                }
                else if (ParentID == "684")
                {
                    reportType = ContextConstants.TEAM_PRODUCTIVITY_REPORT;
                }
                else if (ParentID == "666")
                {
                    reportType = ContextConstants.OFFERANDJOINEDREPORT;
                }
                else if (ParentID == "666")
                {
                    reportType = ContextConstants.OFFERANDJOINEDREPORT;
                }
                return reportType;
            }
        }
        /////////////////////////////////////////////////////////////////////////////////

        #endregion

        #region Methods

        protected void ChangeTheam(string colorName)
        {
            Page.Theme = colorName;
        }

        #endregion

        #region Events

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            string themeValue = Helper.Url.SecureUrl["theme"];

            //string finalTheme = string.Empty;

            //UserProfile profile = UserProfile.GetUserProfile(CurrentUserName);
            ////profile.CurrentUserTheme = themeValue;

            //if (!string.IsNullOrEmpty(themeValue))
            //{
            //    //Session.Add("MyTheme", themeValue);
            //    finalTheme = themeValue;

            //    if (profile != null && !profile.IsAnonymous)
            //    {
            //        profile.CurrentUserTheme = themeValue;
            //        profile.Save();
            //    }
            //}
            //else if (profile != null && !string.IsNullOrEmpty(profile.CurrentUserTheme))
            //{
            //    finalTheme = profile.CurrentUserTheme;
            //}
            //else if (string.IsNullOrEmpty((string)Session["MyTheme"]))
            //{
            //    finalTheme = "Default";
            //}
            //else
            //{
            //    finalTheme = (string)Session["MyTheme"];
            //}

            //if ((string)Session["MyTheme"] != finalTheme)
            //{
            //    Session.Add("MyTheme", finalTheme);
            //}

            Page.Theme = "Default";

            if (!string.IsNullOrEmpty(Helper.Url.SecureUrl.ReturnUrl) && (!string.IsNullOrEmpty(themeValue)))
            {
                Response.Redirect(Helper.Url.SecureUrl.ReturnUrl);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            var theme = this.Theme;

            foreach (var link in Page.Header.Controls.OfType<HtmlLink>().ToList())
            {
                var href = link.Attributes["href"];
                var type = link.Attributes["type"];

                var isCss = type.EqualsIgnoreCase("text/css");

                if (isCss)
                {
                    //var url = ResolveUrl(href);                    
                    //do not allow css.ashx to be removed
                    if (link.Href.ContainsIgnoreCase("css.axd"))
                    {
                        var query = link.Href + "&t=" + theme + "&" + UrlConstants.CssSuffix;
                        link.Href = query;
                        continue;
                    }

                    //remove theme css
                    if (link.Href.ContainsIgnoreCase("App_Themes/{0}".Fill(theme)))
                    {
                        link.Parent.Controls.Remove(link);
                    }
                }
            }

            foreach (Control c in Controls)
            {
                IterateControlsToAddSuffixAndPrefix(c);
            }

            base.Render(writer);
        }

        private void IterateControlsToAddSuffixAndPrefix(Control control)
        {
            if (control is HtmlImage)
            {
                HtmlImage img = control as HtmlImage;
                img.Src = MiscUtil.AppendSuffixAndPrefix(img.Src, UrlConstants.ImageSuffix, UrlConstants.ImagePrefix);
            }
            else if (control is Image)
            {
                Image img = control as Image;
                //Condition added by Fazlur.No need to add prefix and suffix in case of Member personal resources
                if (img.ImageUrl.IndexOf("Resources") == -1)
                {
                    img.ImageUrl = MiscUtil.AppendSuffixAndPrefix(img.ImageUrl, UrlConstants.ImageSuffix, UrlConstants.ImagePrefix);
                }
            }
            else if (control is HyperLink)
            {
                HyperLink hyperLinkImg = control as HyperLink;
                //Condition added by Fazlur.No need to add prefix and suffix in case of Member personal resources
                if (hyperLinkImg.ImageUrl.IsNotNullOrEmpty() && hyperLinkImg.ImageUrl.IndexOf("Resources") == -1)
                {
                    hyperLinkImg.ImageUrl = MiscUtil.AppendSuffixAndPrefix(hyperLinkImg.ImageUrl, UrlConstants.ImageSuffix, UrlConstants.ImagePrefix);
                }
            }
            else
            {
                if (control.Controls.Count > 0)
                {
                    for (int i = 0; i < control.Controls.Count; i++)
                    {
                        IterateControlsToAddSuffixAndPrefix(control.Controls[i]);
                    }
                }
            }
        }
        private void BasePage_Error(object sender, EventArgs e)
        {
            // Get the last exception thrown
            Exception ex = Server.GetLastError();
            writeLog(ex);
            Session["CurrentErrorMessage"] = ex.Message;
            try
            {
                Session["CurrentErrorInnerException"] = ex.InnerException.Message;
            }
            catch { Session["CurrentErrorInnerException"] = "Server returned an Error."; }
            Session["CurrentErrorStackTrace"] = ex.StackTrace;
            Session["CurrentErrorData"] = ex.Data;

            Response.Redirect("~/ErrorPage.aspx");
            // Set the error page
            // this.ErrorPage = "/ErrorHandling/ErrorPages/BaseError.html";

            //			// Throw the error to prevent wrapping
            //			throw Server.GetLastError();
        }
        public static void writeLog(Exception ex)
        {
            try
            {

                string error = ex.Data + "\n" + ex.Message + "\n" + ex.StackTrace;
                string filename = GetLogFileName();
                // WriteLogintoFile(error, filename);

                if (System.Configuration.ConfigurationManager.AppSettings["ErrorLog"] != null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ErrorLog"].ToString().ToLower() == "yes")
                    {
                        WriteLogintoFile("", ex.Data.ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), GetLogFileName());

                    }

                }
            }
            catch (Exception e)
            {
            }
        }
        static string GetLogFileName()
        {
            string filepath = System.Configuration.ConfigurationManager.AppSettings["ErrorLogLocation"].ToString();
            string dirName = filepath + "\\" + DateTime.Now.ToString("yyyyMMdd");
            string tempName = null;
            try
            {
                DirectoryInfo dir = Directory.GetParent(filepath);
                bool direxist = dir.Root.Exists;
                if (direxist)
                {
                    if (!Directory.Exists(dirName))
                    {
                        Directory.CreateDirectory(dirName);
                    }

                    if (Directory.Exists(dirName))
                    {
                        Random rnd = new Random();
                        string ranNo = Convert.ToString(rnd.Next(1, 1001));
                        string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                        tempName = Path.GetFileName(Path.GetTempFileName());
                        tempName = tempName.Replace(".tmp", "");
                        tempName = ranNo + "_" + date;

                    }
                }
            }
            catch (DirectoryNotFoundException ex)
            {

            }
            return dirName + "\\" + tempName + "-talentrackr.txt";

        }
        static void WriteLogintoFile(string errortext, string ExceptionData, string ExceptionMessage, string ExceptionStackTrace, string filename)
        {
            lock (filename)
            {

                StreamWriter writer = new StreamWriter(filename, true);
                writer.WriteLine(errortext);
                writer.WriteLine("---------------------------------------------");
                writer.WriteLine("");
                writer.WriteLine("");
                writer.WriteLine("Exception Data");
                writer.WriteLine("---------------------------------------------");
                writer.WriteLine(ExceptionData);
                writer.WriteLine("");
                writer.WriteLine("");
                writer.WriteLine("Exception Message");
                writer.WriteLine("---------------------------------------------");
                writer.WriteLine(ExceptionMessage);
                writer.WriteLine("");
                writer.WriteLine("");
                writer.WriteLine("Exception StackTrace");
                writer.WriteLine("---------------------------------------------");
                writer.WriteLine(ExceptionStackTrace);
                writer.Flush();
                writer.Close();
            }
        }
        //Function intorduced by prasanth on 10/Jun/2016
        public static void writeLog_LdapException(LdapException ex)
        {
            try
            {

                string error = ex.Data + "\n" + ex.Message + "\n" + ex.StackTrace;
                string filename = GetLogFileName();
                // WriteLogintoFile(error, filename);

                if (System.Configuration.ConfigurationManager.AppSettings["ErrorLog"] != null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ErrorLog"].ToString().ToLower() == "yes")
                    {
                        WriteLogintoFile("", ex.Data.ToString(), ex.Message.ToString() + " Error Code = " + ex.ErrorCode, ex.StackTrace.ToString(), GetLogFileName());

                    }

                }
            }
            catch (Exception e)
            {
            }
        }

        //Function intorduced by prasanth on 10/Jun/2016
        public static void writeLog_DirectoryServicesCOMException(DirectoryServicesCOMException ex, string LdapPath, string LdapUserName)
        {
            try
            {

                string error = ex.Data + "\n" + ex.Message + "\n" + ex.StackTrace;
                string filename = GetLogFileName();
                // WriteLogintoFile(error, filename);


                if (System.Configuration.ConfigurationManager.AppSettings["ErrorLog"] != null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ErrorLog"].ToString().ToLower() == "yes")
                    {
                        WriteLogintoFile("  User Name = " + LdapUserName + "   LDAP Connection = " + LdapPath, ex.Data.ToString(), ex.ExtendedErrorMessage.ToString(), ex.StackTrace.ToString(), GetLogFileName());

                    }

                }
            }
            catch (Exception e)
            {
            }
        }


        //Function intorduced by prasanth on 24/Apr/2017 issue id 1044  
        //public static void Mailqueue_ErrorlogDB(Exception ex,int emailid)
        //{
        //    try
        //    {


        //        ErrorLogDB Edb;
        //        IFacade fd ;
        //        Edb.Id = emailid;
        //        Edb.ExceptionData = ex.Data.ToString();
        //        Edb.ExceptionMessage = ex.Message.ToString();
        //        Edb.ExceptionStackTrace = ex.StackTrace.ToString();
        //        ErrorLogDB i = fd.Update_MailQueue_ErrorLogDB(Edb);
                
        //    }
        //    catch (Exception e)
        //    {
        //    }
        //}


        #endregion
    }
}