using System;
using System.Web.UI;

namespace TPS360.Web.UI
{
    public class AdminBasePage : BasePage
    {
        #region Member Variables

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Events

        protected override void OnPreInit(EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["MyTheme"]) || (string)Session["MyTheme"] == "Default")
            {
                Page.Theme = "Default";
            }

            base.OnPreInit(e);
        }

        #endregion
    }
}