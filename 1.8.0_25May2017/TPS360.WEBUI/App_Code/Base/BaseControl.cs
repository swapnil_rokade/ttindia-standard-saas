/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: BaseControl.cs
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
     0.1            20/May/2016         Sumit Sonawane      isMainApplication
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;
using System.Diagnostics;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using System.Collections;
using System.IO;
namespace TPS360.Web.UI
{
    public class BaseControl : System.Web.UI.UserControl
    {
        #region Member Variables

        private WebHelper _helper;
        private IFacade _facade;

        #endregion

        #region Properties


        public BaseControl()
        {
            this.Error += new EventHandler(BaseControl_Error);
        }
        protected void BaseControl_Error(object sender, EventArgs e)
        {
            // Get the last exception thrown
            Exception ex = Server.GetLastError();
            writeLog(ex);
            // Set the error page
            // this.ErrorPage = "/ErrorHandling/ErrorPages/BaseError.html";

            //			// Throw the error to prevent wrapping
            //			throw Server.GetLastError();
        }

        protected void writeLog(Exception ex)
        {
            string error = ex.Data + "\n" + ex.Message + "\n" + ex.StackTrace;
            string filename = GetLogFileName();
            // WriteLogintoFile(error, filename);
            WriteLogintoFile("", ex.Data.ToString(), ex.Message.ToString(), ex.StackTrace.ToString(), GetLogFileName());
        }
        protected string GetLogFileName()
        {
            string filepath = System.Configuration.ConfigurationManager.AppSettings["ErrorLogLocation"].ToString();
            string dirName = filepath + "\\Exeception\\" + DateTime.Now.ToString("yyyyMMdd");
            if (!Directory.Exists(dirName))
            {
                Directory.CreateDirectory(dirName);
            }
            string tempName = Path.GetFileName(Path.GetTempFileName());
            tempName = tempName.Replace(".tmp", "");
            return dirName + "\\" + tempName + "-talentrackr.txt";
        }
        protected void WriteLogintoFile(string errortext, string ExceptionData, string ExceptionMessage, string ExceptionStackTrace, string filename)
        {
            lock (filename)
            {
                StreamWriter writer = new StreamWriter(filename, true);
                writer.WriteLine(errortext);
                writer.WriteLine("");
                writer.WriteLine("");
                writer.WriteLine("Exception Data");
                writer.WriteLine(ExceptionData);
                writer.WriteLine("");
                writer.WriteLine("");
                writer.WriteLine("Exception Message");
                writer.WriteLine(ExceptionMessage);
                writer.WriteLine("");
                writer.WriteLine("");
                writer.WriteLine("Exception StackTrace");
                writer.WriteLine(ExceptionStackTrace);
                writer.Flush();
                writer.Close();
            }
        }
        protected MembershipUser CurrentUser
        {
            [DebuggerStepThrough()]
            get
            {
                return Membership.GetUser(CurrentUserName, true);
            }
        }

        protected Member CurrentMember
        {
            //[DebuggerStepThrough()]
            get
            {
                return Context.Items[ContextConstants.MEMBER] as Member;
            }
        }

        protected bool IsUserAuthenticated
        {
            [DebuggerStepThrough]
            get
            {
                return Page.User.Identity.IsAuthenticated;
            }
        }

        protected string CurrentUserName
        {
            [DebuggerStepThrough]
            get
            {
                return IsUserAuthenticated ? Page.User.Identity.Name : "Anonymous";
            }
        }

        protected string CurrentUserRole
        {
            //[DebuggerStepThrough]
            get
            {
                return (Roles.GetRolesForUser(CurrentUserName)).GetValue(0).ToString();
            }
        }

        protected Guid CurrentUserId
        {
            [DebuggerStepThrough]
            get
            {
                if (!IsUserAuthenticated)
                {
                    return Guid.Empty;
                }

                return (Guid)CurrentUser.ProviderUserKey;
            }
        }

        protected bool IsUserAdmin
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_ADMIN);
            }
        }

        protected bool IsUserEmployee
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_EMPLOYEE);
            }
        }

        protected bool IsUserConsultant
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_CONSULTANT);
            }
        }

        protected bool IsUserCandidate
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_CANDIDATE);
            }
        }

        protected bool IsUserVendor
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_VENDOR);
            }
        }

        protected bool IsUserClient
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_CLIENT);
            }
        }

        protected bool IsUserPartner
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_PARTNER);
            }
        }
        protected bool IsHiringManager
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_DEPARTMENT_CONTACT);
            }
        }
        protected IFacade Facade
        {
            get
            {
                if (_facade == null)
                {
                    _facade = Context.Items[ContextConstants.Facade] as IFacade;
                }

                return _facade;
            }
        }

        protected WebHelper Helper
        {
            [DebuggerStepThrough]
            get
            {
                if (_helper == null)
                {
                    _helper = new WebHelper(this);
                }

                return _helper;
            }
        }

        public virtual bool IsValid
        {
            //[DebuggerStepThrough]
            get
            {
                foreach (Control c in Controls)
                {
                    IterateControlsToValidate(c);
                }

                foreach (Control c in Controls)
                {
                    bool isValid = IterateControlsToCheckIsValid(c);

                    if (!isValid)
                    {
                        return isValid;
                    }
                }

                return true;
            }
        }

        protected Hashtable SiteSetting
        {
            [DebuggerStepThrough()]
            get
            {
                return Context.Items[ContextConstants.SITESETTING] as Hashtable;
            }
        }

        //protected bool isMainApplication
        //{
        //    get
        //    {
        //        if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"] != null)
        //        {
        //            if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower() == "landt")
        //            {
        //                return false;
        //            }
        //            return true;
        //        }
        //        return true;
        //    }
        //}
        protected ApplicationSource ApplicationSource
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"] != null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower() == "landt")
                    {
                        return ApplicationSource.LandTApplication;
                    }
                    else if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower() == "selectigence")
                    {
                        return ApplicationSource.SelectigenceApplication;
                    }
                    else if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower() == "genisys")
                    {
                        return ApplicationSource.GenisysApplication;
                    }
                    else
                    {
                        return ApplicationSource.MainApplication;
                    }

                }
                return ApplicationSource.MainApplication;
            }
        }

        //*********Code added by Sumit Sonawane on 20/May/2016*****START******************
        protected bool isMainApplication
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"] != null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower() == "landt")
                    {
                        return false;
                    }
                    return true;
                }
                return true;
            }
        }
        //**********************END***********************************

        #endregion

        #region Methods

        private void IterateControlsToValidate(Control control)
        {
            BaseValidator validator = control as BaseValidator;

            if (validator != null)
            {
                if (validator.Enabled)
                {
                    validator.Validate();
                }
            }
            else
            {
                if (control.Controls.Count > 0)
                {
                    for (int i = 0; i < control.Controls.Count; i++)
                    {
                        IterateControlsToValidate(control.Controls[i]);
                    }
                }
            }
        }

        private bool IterateControlsToCheckIsValid(Control control)
        {
            BaseValidator validator = control as BaseValidator;

            if (validator != null)
            {
                if (validator.Enabled)
                {
                    return validator.IsValid;
                }
            }
            else
            {
                if (control.Controls.Count > 0)
                {
                    for (int i = 0; i < control.Controls.Count; i++)
                    {
                        bool isValid = IterateControlsToCheckIsValid(control.Controls[i]);

                        if (!isValid)
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        #endregion
    }
}