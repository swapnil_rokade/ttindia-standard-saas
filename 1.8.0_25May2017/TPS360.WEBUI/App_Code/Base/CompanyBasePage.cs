using System;
using System.Diagnostics;
using System.Web.Security;
using System.Web.UI;

using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;

namespace TPS360.Web.UI
{
    public class CompanyBasePage : AdminBasePage
    {
        #region Member Variables

        Company _company;

        #endregion

        #region Properties

        public int CurrentCompanyId
        {
            get
            {
                int id = 0;

                int.TryParse(StringHelper.Convert(ViewState[UrlConstants.PARAM_COMPANY_ID]), out id);

                if (id == 0)
                {
                    int.TryParse(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID], out id);
                }

                return id;
            }
            set
            {
                ViewState[UrlConstants.PARAM_COMPANY_ID] = value;
            }
        }

        public Company CurrentCompany
        {
            get
            {
                if (_company == null)
                {
                    if (CurrentCompanyId > 0)
                    {
                        _company = Facade.GetCompanyById(CurrentCompanyId);
                    }

                    if (_company == null)
                    {
                        _company = new Company();
                    }
                }

                return _company;
            }
            set
            {
                _company = value;
            }
        }

        public CompanyStatus RequestedCompanyStatus
        {
            get
            {
                string companyType = Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_TYPE];

                if (string.Equals(companyType, "Vendor"))
                {
                    return CompanyStatus.Vendor;
                }
                else if (string.Equals(companyType, "Client"))
                {
                    return CompanyStatus.Client;
                }
                else if (string.Equals(companyType, "Partner"))
                {
                    return CompanyStatus.Partner;
                }
                else
                {
                    return CompanyStatus.Company;
                }
            }
        }

        public CompanyStatus CurrentCompanyStatus
        {
            get
            {
                string ParentID = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID];
                if (ParentID == "11")
                    return CompanyStatus.Client;
                else if (ParentID == "638")
                    return CompanyStatus.Vendor ;
                else return CompanyStatus.Department ;

            }
        }
        #endregion

        #region Methods

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            if (string.IsNullOrEmpty((string)Session["MyTheme"]) || (string)Session["MyTheme"] == "Default")
            {
                Page.Theme = "Default";
            }
        }

        #endregion

        #region Events

        #endregion
    }
}