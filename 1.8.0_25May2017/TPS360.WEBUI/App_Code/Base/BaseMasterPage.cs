using System;
using System.Diagnostics;
using System.Web.Security;
using System.Configuration;
using TPS360.BusinessFacade;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;

namespace TPS360.Web.UI
{
    public class BaseMasterPage : System.Web.UI.MasterPage
    {
        #region Member Variables

        private IFacade _facade;
        private WebHelper _helper;

        #endregion
        
        #region Properties

        protected MembershipUser CurrentUser
        {
            [DebuggerStepThrough()]
            get
            {
                return Membership.GetUser(CurrentUserName, true);
            }
        }

        protected Member CurrentMember
        {
            [DebuggerStepThrough()]
            get
            {
                return Context.Items[ContextConstants.MEMBER] as Member;
            }
        }

        protected bool IsUserAuthenticated
        {
            [DebuggerStepThrough]
            get
            {
                return Page.User.Identity.IsAuthenticated;
            }
        }

        protected string CurrentUserName
        {
            [DebuggerStepThrough]
            get
            {
                return IsUserAuthenticated ? Page.User.Identity.Name : "Anonymous";
            }
        }

        protected string CurrentUserRole
        {
            [DebuggerStepThrough]
            get
            {
                return (Roles.GetRolesForUser(CurrentUserName)).GetValue(0).ToString();
            }
        }

        protected Guid CurrentUserId
        {
            [DebuggerStepThrough]
            get
            {
                if (!IsUserAuthenticated)
                {
                    return Guid.Empty;
                }

                return (Guid)CurrentUser.ProviderUserKey;
            }
        }

        protected bool IsUserAdmin
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_ADMIN);
            }
        }

        protected bool IsUserEmployee
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_EMPLOYEE);
            }
        }

        protected bool IsUserConsultant
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_CONSULTANT);
            }
        }

        protected bool IsUserCandidate
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_CANDIDATE);
            }
        }

        protected bool IsUserVendor
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_VENDOR);
            }
        }

        protected bool IsUserClient
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_CLIENT);
            }
        }

        protected bool IsUserPartner
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_PARTNER);
            }
        }

        protected bool IsUserDepartmentContact
        {
            [DebuggerStepThrough()]
            get
            {
                return Page.User.IsInRole(ContextConstants.ROLE_DEPARTMENT_CONTACT );
            }
        }
        protected IFacade Facade
        {
            get
            {
                if (_facade == null)
                {
                    _facade = Context.Items[ContextConstants.Facade] as IFacade;
                }

                return _facade;
            }
        }

        protected WebHelper Helper
        {
            get
            {
                if (_helper == null)
                {
                    _helper = new WebHelper(this);
                }

                return _helper;
            }
        }

        #endregion

        #region Methods

        #endregion

        #region Events

        #endregion
    }
}