using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;


[System.Web.Script.Services.ScriptService]
public class WidgetService : WebServiceBase
{
    public WidgetService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = false, XmlSerializeString = true)]
    public void MoveWidgetInstance(int widgetId, int toColumn, int toRow)
    {
        //new DashboardFacade(Profile.UserName).MoveWidgetInstance(widgetId, toColumn, toRow);
        //Context.Cache.Remove(Profile.UserName);
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = false, XmlSerializeString = true)]
    public void DeleteWidgetInstance(int widgetId)
    {
        new WidgetInstanceDataAccess().DeleteWidgetInstance(widgetId);

        //Context.Cache.Remove(Profile.UserName);
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = false, XmlSerializeString = true)]
    public void UpdateWidgetInstancePosition(string args)
    {

        char[] semi = { ';' };
        char[] comma = { ',' };
        string[] widgets = args.Split(semi, StringSplitOptions.RemoveEmptyEntries);
        foreach (string s in widgets)
        {
            string[] wid = s.Split(comma, StringSplitOptions.RemoveEmptyEntries);
            new WidgetInstanceDataAccess().UpdateWidgetPosition(Convert.ToInt32(wid[0]), Convert.ToInt32(wid[1]), Convert.ToInt32(wid[2]));
        }
        //new WidgetInstanceDataAccess().UpdateWidgetPosition  (widgetId,columnNo ,RowNo );

        //Context.Cache.Remove(Profile.UserName);
    }
}

