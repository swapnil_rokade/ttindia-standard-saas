using System;
using System.Configuration;
using System.Web.Profile;
using System.Web.Security;

[Serializable]
public class UserProfile: System.Web.Profile.ProfileBase
{
    public UserProfile()
    {
       
    }

    public static UserProfile GetUserProfile(string username)
    {
        return Create(username) as UserProfile;
    }
    
    public static UserProfile GetUserProfile()
    {
        return Create(Membership.GetUser().UserName) as UserProfile;
    }

    [SettingsAllowAnonymousAttribute(false)]
    [DefaultSettingValue("true")]
    public virtual bool IsFirstVisit
    {
        get
        {

            return ((bool)(this.GetPropertyValue("IsFirstVisit")));
        }
        set
        {
            this.SetPropertyValue("IsFirstVisit", value);
        }
    }

    [SettingsAllowAnonymousAttribute(false)]    
    public virtual string CurrentUserTheme
    {
        get
        {
            return ((string)(this.GetPropertyValue("CurrentUserTheme")));
        }
        set
        {
            this.SetPropertyValue("CurrentUserTheme", value);
        }
    }
}
