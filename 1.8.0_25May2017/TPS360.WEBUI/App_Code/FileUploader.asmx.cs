/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: FileUploader.asmx.cs
    Description: This is the web service application to send email
    Created By: Yogeesh Bhat
    Created On: July-24-2009
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1         Sep-24-2009          Yogeesh Bhat        Defect ID: 11111,11076; Changes made in UploadFile() method, 
                                                         Added new method DownloadFile()
    0.2         Sep-24-2009          Yogeesh Bhat        Rework (11111, 11076); Added new enum UploadStatus
    0.3         Sep-29-2009          Yogeesh Bhat        Rework (11111, 11076): Added new structure UploadDownloadDetails
    0.4         10/Jun/2016          Prasanth Kumar G    Introduced Ldap condition
_______________________________________________________________________________________________________________________________
 */
namespace Uploader
{
    #region Namespaces
    using System;
    using System.Data;
    using System.Web;
    using System.Collections;
    using System.Web.Services;
    using System.Web.Services.Protocols;
    using System.ComponentModel;
    using System.IO;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Web.Security;
    using System.Linq;
    using System.Xml.Linq;
    using ParserLibrary;
    using TPS360.BusinessFacade;
    using TPS360.Common;
    using TPS360.DataAccess;
    using TPS360.Controls;
    using TPS360.Common.BusinessEntities;
    using System.Data;
    using System.IO;
    using System.Text;
    using System.Xml.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Web.Security;
    using System.Collections.Generic;
    using System.Collections;
    using System.Text.RegularExpressions;
    using System.DirectoryServices;
    using System.DirectoryServices.ActiveDirectory;
    using TPS360.Web.UI.Helper;
    #endregion
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class FileUploader : System.Web.Services.WebService
    {

        string str = string.Empty;
        string strUserName = string.Empty;
        string strPassword = string.Empty;
        string strCredentials = string.Empty;
        string filenames = string.Empty;
        public string ResumeLocation = "";
        public string result = "";
        public string copypastresume;
        private Member m_member;
       
        private MemberExtendedInformation m_memberExtendedInformation;
        private MemberDetail m_memberDetail;
        private MemberObjectiveAndSummary m_memberObjectiveAndSummary;
        private MemberJobCart m_memberJobCart;
        private Resource m_resource;
   
        public int CreatorID = 0;
        public DataTable workhistory=null;
        public DataTable EducatationInfo=null;
	public DataTable SkillInfo=null;
	public DataTable LicenseInfo=null; 

        IFacade facade = new Facade();
        UploadDownloadDetailsDB Details = new UploadDownloadDetailsDB();
        UploadDownloadDetails updetails = new UploadDownloadDetails();
     
        public FileUploader()
        {
            SiteSetting siteSetting = facade.GetSiteSettingBySettingType((int)TPS360.Common.Shared .SettingType.SiteSetting);
            if (siteSetting != null)
            {
                Country country = facade.GetCountryById(Convert.ToInt32(TPS360.Common.Helper.ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration)[TPS360.Common.Shared.DefaultSiteSetting.Country.ToString()]));
                if (country != null)
                {
                    if (country.Name == "India")
                    {
                        System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                        System.Globalization.DateTimeFormatInfo info = new System.Globalization.DateTimeFormatInfo();
                        info.ShortDatePattern = "dd/MM/yyyy";
                        culture.DateTimeFormat = info;

                        System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                        System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
                    }
                }
            }
            m_member = new Member();
            m_memberExtendedInformation = new MemberExtendedInformation();
            m_memberDetail = new MemberDetail();
            m_memberObjectiveAndSummary = new MemberObjectiveAndSummary();
            m_memberJobCart = new MemberJobCart();
          
            m_resource = new Resource();
        }

		[WebMethod]
        public string ValidateWebService()
        {
           return "Success";
        }
        [WebMethod]
        public int GetMemberIdByMemberEmail(string Email,string Password)
        {
            MembershipUser user = Membership.GetUser(Email);
            int Memberid = 0;
            if (user != null)
            {
                try
                {
                    if (!user .IsLockedOut && user.GetPassword() == Password.Trim() && user.IsApproved)
                    {
                        Memberid = facade.GetMemberIdByMemberEmail(Email);
                    }
                }
                catch
                {
                }
            }
            return Memberid;
        }
        [WebMethod]
        public string  GetJobPostingIdByJobPostingCode(byte [] JobPostingCode)
        {
            string  JobPostingId = string .Empty ;
            IList<string> codeList = new List<string>();
            ConvertByteArrayToIList(JobPostingCode,out codeList );
            try
            {
                foreach (string code in codeList)
                {
                    if (code!= string.Empty)
                        JobPostingId = facade.GetJobPostingIdByJobPostingCode(code.Substring(1).Trim());
                    if (JobPostingId != null && JobPostingId != string.Empty)
                        break;
                }
            }
            catch
            {
            }
            return JobPostingId;
        }
        [WebMethod]
        public UploadDownloadDetails UploadFile(UploadDownloadDetails upload)
        {
           
            updetails = upload;
            BinaryFormatter binary = new BinaryFormatter();
            MemoryStream mem =null;
            MemoryStream memwork = null;
	    MemoryStream memskill = null;
	    MemoryStream memlicense = null;
            if (upload.EducationInfo.Length > 0)
            {
                mem = new MemoryStream(upload.EducationInfo);
                EducatationInfo = (DataTable)binary.Deserialize(mem);
                mem.Close();
            }

            if (upload.WorkHistoryInfo.Length > 0)
            {
                memwork = new MemoryStream(upload.WorkHistoryInfo);
                workhistory = (DataTable)binary.Deserialize(memwork);
                memwork.Close();
            }
 	   if (upload.SkillsArray.Length > 0)
            {
                memskill = new MemoryStream(upload.SkillsArray);
                SkillInfo = (DataTable)binary.Deserialize(memskill);
               memskill .Close();
            }

	   if (upload.License.Length > 0)
            {
                memlicense = new MemoryStream(upload.License);
                LicenseInfo = (DataTable)binary.Deserialize(memlicense);
                memlicense.Close();
            }

             
           // mem.Close();
            //return educa.Rows .Count + workhis .Rows .Count + "";
            XmlSerializer ob = new XmlSerializer(typeof(UploadDownloadDetailsDB));
            Details = (UploadDownloadDetailsDB)ob.Deserialize(new StringReader(upload.Data));
            //insert(r, workhis, educa);
            CreatorID = upload.CreatorID;
            if (CreatorID == 0)
            {
                
            }
            copypastresume = upload.CopyPastResume;
            m_member = upload.m_member;
            m_memberDetail = upload.m_memberDetail;
            m_memberExtendedInformation = upload.m_memberExtendedInformation;
            m_memberObjectiveAndSummary = upload.m_memberObjectiveAndSummary;
            ResumeLocation = upload.ResumeLocation;
            m_resource = upload.m_resource;
            m_memberJobCart = upload.m_memberJobCart;
            filenames = upload.FileName;
            UpdateStatus s = ADDToATS(upload .UpdateDuplicateResumes,upload  );
            UploadDownloadDetails results = new UploadDownloadDetails();
            results.Status = s;

            return results;
        }
        public enum UpdateStatus
        {
            Inserted,
            Updated,
            DuplicateUserName,
            InvalidUserName,
            Error,
            InsertedWithDocument,
            InsertedWithoutDocument

        }
        Random randomnumber = new Random();
        private UpdateStatus ADDToATS(bool updateDuplicateResume, UploadDownloadDetails upload)
        {
            MembershipUser newUser;
            string Email = string.Empty;
            try
            {

                if (facade.CheckCandidateDuplication(m_member.FirstName.Trim(), m_member.LastName.Trim(), m_member.CellPhone.Trim(), DateTime.MinValue, m_member.PrimaryEmail.Trim(), m_memberExtendedInformation.IdCardLookUpId, m_memberExtendedInformation.IdCardDetail.Trim())) 
                {
                    throw new System.Web.Security.MembershipCreateUserException(MembershipCreateStatus.DuplicateUserName);
                
                }
                if (m_member.PrimaryEmail.Trim() == "")
                {
                    Email = randomnumber.Next() + "@placeholder.com";
                    try
                    {
                        newUser = Membership.CreateUser(Email, "changeme", Email);
                        m_member.PrimaryEmail = Email;
                    }
                    catch
                    {
                        if (Email.Contains("@placeholder.com"))
                        {
                            Email = randomnumber.Next() + "@placeholder.com";
                        }
                        newUser = Membership.CreateUser(Email, "changeme", Email);
                        m_member.PrimaryEmail = Email;
                    }
                }
                else 
                {
                    Email = m_member.PrimaryEmail;
                    newUser = Membership.CreateUser(Email, "changeme", Email);
                }
                
                if (newUser.IsApproved == true)
                {
                    newUser.IsApproved = false;
                    Membership.UpdateUser(newUser);
                    //jvm BuildMemberData();

                    //insertResume(newUser);
                    if (insertResume(newUser))
                    {
                        return UpdateStatus.Inserted;
                    }
                    else
                    {
                        return UpdateStatus.InsertedWithoutDocument;
                    }

                }
                
               //if (insertResume())
               // {
               //     return UpdateStatus.Inserted;
               // }
               // else
               // {
               //     return UpdateStatus.InsertedWithoutDocument;
               // }

                

            }
            catch (System.Web.Security.MembershipCreateUserException ex)
            {
                switch (ex.StatusCode)
                {
                    case MembershipCreateStatus.DuplicateUserName:
                        if (facade.getCandidateIdbyPrimaryEmail(m_member.PrimaryEmail) > 0)
                        {
                            if (updateDuplicateResume)
                            {
                                BuildMemberData();
                                UpdateResume(upload);
                            }
                            return UpdateStatus.DuplicateUserName;
                        }
                        else return UpdateStatus.Error;
                        break;
                    case MembershipCreateStatus.InvalidUserName:
                        return UpdateStatus.InvalidUserName;
                }
            }
            return UpdateStatus.Error;

        }
        private bool insertResume(MembershipUser newUser)
        {
            try
            {
                BuildMember(CreatorID);
                m_member.Status = 1;
                if (Details.RoleName == true)
                {
                    Roles.AddUserToRole(newUser.UserName, "candidate");
                }
                else
                {
                    Roles.AddUserToRole(newUser.UserName, "consultant");
                }
                m_member.UserId = (Guid)newUser.ProviderUserKey;
                //m_member = facade.AddMember(m_member);
              Member tempm=   facade.AddFullMemberInfo(m_member, true, 315, false);
              m_member.Id = tempm.Id;
              if (tempm.Id == 0)
                {
                    Membership.DeleteUser(m_member.PrimaryEmail);
                    return false;
                }
                result = "ADDED member";
                
                string filepath = "";
                try
                {
                    filepath = AddMemberDocument();
                }
                catch{ }

                bool IsfileExists = false;
                if (filepath.Trim() != "")
                {
                    if (File.Exists(filepath))
                    {
                        IsfileExists = true;
                    }
                }
                if (IsfileExists)
                {
                                //m_memberDetail.MemberId = m_member.Id;
                                //facade.AddMemberDetail(m_memberDetail);
                                //m_memberExtendedInformation.MemberId = m_member.Id;
                                //if (m_memberExtendedInformation.Availability == null || m_memberExtendedInformation.Availability == 0)
                                //    m_memberExtendedInformation.Availability = 315;
                                //facade.AddMemberExtendedInformation(m_memberExtendedInformation);
                                //m_memberObjectiveAndSummary.MemberId = m_member.Id;
                                //facade.AddMemberObjectiveAndSummary(m_memberObjectiveAndSummary);

                    //JVM
                    facade.UpdateMember(m_member);

                    m_memberDetail.MemberId = m_member.Id;
                   MemberDetail newMmberDetail = facade.GetMemberDetailByMemberId(m_member.Id);
                   m_memberDetail.Id = newMmberDetail.Id;
                    BuildMemberDetail(CreatorID);
                    facade.UpdateMemberDetail(m_memberDetail);

                    m_memberExtendedInformation.MemberId = m_member.Id;
                    if (m_memberExtendedInformation.Availability == null || m_memberExtendedInformation.Availability == 0)
                        m_memberExtendedInformation.Availability = 315;
                 MemberExtendedInformation info= facade.GetMemberExtendedInformationByMemberId(m_member.Id);
                 m_memberExtendedInformation.Id = info.Id;
                    BuildMemberExtendedInformation(CreatorID);
                    facade.UpdateMemberExtendedInformation(m_memberExtendedInformation);

                    m_memberObjectiveAndSummary.MemberId = m_member.Id;
                 MemberObjectiveAndSummary objt= facade.GetMemberObjectiveAndSummaryByMemberId(m_member.Id);
                 m_memberObjectiveAndSummary.Id  = objt.Id;
                    BuildMemberObjectiveAndSummary(CreatorID);
                    facade.UpdateMemberObjectiveAndSummary(m_memberObjectiveAndSummary);




                    AddMemberExperience();
                    AddMemberEducation();
                    AddMemberSkillMap();
                    AddMemberLicense();
                    AssignMemberManager();
                    // int ParsedSkillID = InsertSkillHeader(facade, CreatorID);
                    // AddMemberSkills(facade, ParsedSkillID, CreatorID);
                    //string filepath="";
                    //try
                    //{
                    //    filepath = AddMemberDocument();
                    //}
                    //catch
                    //{
                    //}
                    try
                    {

                        HiringMatrixLevels level = facade.HiringMatrixLevel_GetInitialLevel();
                       

                        if (Details.JobPostingId != 0) 
                        {
                            facade.MemberJobCart_AddCandidateToRequisition(CreatorID, m_member.Id.ToString(), Details.JobPostingId);
                        }
                        
                    }
                    catch (System.Exception)
                    {

                    }
                    try
                    {
                        BuildMemberGroupMap(Details.HotlistId, m_member.Id, CreatorID);

                    }
                    catch (System.Exception)
                    {

                    }
                    BuildResource(m_member);
                    facade.AddResource(m_resource);
                    // AddNote
                    try
                    {
                        SaveNotsAndActivities();
                    }
                    catch { }
                    //bool IsfileExists = false;
                    //if (filepath.Trim() != "")
                    //{
                    //    if (File.Exists(filepath))
                    //    {
                    //        IsfileExists= true;
                    //    }
                    //}
                    return true;
                }
                else
                {
                    try
                    {
                        facade.DeleteMemberById(m_member.Id);
                        Membership.DeleteUser(m_member.PrimaryEmail);
                    }
                    catch { Membership.DeleteUser(m_member.PrimaryEmail); }
                    return false;
                }
                //return IsfileExists;
            }
            catch (System.Exception ex)
            {
                // ErrorLogger.Log(ex);
                try
                {
                    facade.DeleteMemberById(m_member.Id);
                    Membership.DeleteUser(m_member.PrimaryEmail);
                }
                catch { Membership.DeleteUser(m_member.PrimaryEmail); }
                return false;
            }
        }


        private void SaveNotsAndActivities()
        {
           
                try
                {
                    CommonNote notsAndActivities = BuildNotsAndActivities();
                    MemberNote memberNote = BuildMemberNot();
                    notsAndActivities = facade.AddCommonNote(notsAndActivities);
                    memberNote.CommonNoteId = notsAndActivities.Id;
                    facade.AddMemberNote(memberNote);
                }
                catch (ArgumentException ex)
                {
                    
                }
           
        }

        private CommonNote BuildNotsAndActivities()
        {
            CommonNote commonNote = null;
            commonNote = new CommonNote();
            //commonNote.NoteCategoryLookupId = 0;// Convert.ToInt32(ddlNoteCategory.SelectedValue);
            commonNote.NoteDetail = Details.Note;
            IList<GenericLookup> ge = facade.GetAllGenericLookupByLookupTypeAndName(TPS360.Common.Shared.LookupType.NotesType, "Added from Parser");
            if (ge.Count > 0)
                commonNote.NoteCategoryLookupId = ge[0].Id;
            commonNote.CreatorId = CreatorID ;
            commonNote.UpdatorId =CreatorID ;
            return commonNote;
        }

        private MemberNote BuildMemberNot()
        {
            MemberNote memberNote = new MemberNote();
            memberNote.MemberId = m_member .Id ;
            memberNote.CreatorId = CreatorID ;
            return memberNote;
        }



        [WebMethod]
        public UploadDownloadDetails UpdateResume(UploadDownloadDetails upload)
        {
            try
            {
                Member tempMember = facade.GetMemberByMemberEmail(m_member.PrimaryEmail);
                MemberDetail tempMemberDetail = facade.GetMemberDetailByMemberId(tempMember.Id);
                MemberExtendedInformation tempMemberExtendedInformatiaon = facade.GetMemberExtendedInformationByMemberId(tempMember.Id);
                MemberObjectiveAndSummary tempMemberObjectiveandSummary = facade.GetMemberObjectiveAndSummaryByMemberId(tempMember.Id);
                m_member.Id = tempMember.Id;
                m_member.Status = facade.GetMemberStatusByMemberId(m_member.Id);
                m_member.CreatorId = tempMember.CreatorId;
                m_member.CreateDate = tempMember.CreateDate;
                m_memberDetail.Id = tempMemberDetail.Id;
                m_memberExtendedInformation.Id = tempMemberExtendedInformatiaon.Id;
                m_memberObjectiveAndSummary.Id = tempMemberObjectiveandSummary.Id;
                PropertyCheck(tempMember, m_member);
                PropertyCheck(tempMemberDetail, m_memberDetail);
                PropertyCheck(tempMemberExtendedInformatiaon, m_memberExtendedInformation);
                PropertyCheck(tempMemberObjectiveandSummary, m_memberObjectiveAndSummary);

                m_member = tempMember;

                if (workhistory.Rows.Count > 0)
                {
                    facade.DeleteMemberExperienceByMemberId(tempMember.Id);
                    AddMemberExperience();
                }
                if (SkillInfo.Rows.Count > 0)
                {
                    facade.DeleteMemberSkillMapByMemberId(tempMember.Id);
                    AddMemberSkillMap();
                }
                if (EducatationInfo.Rows.Count > 0)
                {
                    facade.DeleteMemberEducationByMemberId(tempMember.Id);
                    AddMemberEducation();
                }
                if (LicenseInfo.Rows.Count > 0)
                {
                    facade.DeleteMemberCertificationMapByMemberId(tempMember.Id);
                    AddMemberLicense();
                }

                tempMember.UpdateDate = tempMemberDetail.UpdateDate = tempMemberExtendedInformatiaon.UpdateDate = tempMemberObjectiveandSummary.UpdateDate = DateTime.Now;
                tempMember.UpdatorId = tempMemberDetail.UpdatorId = tempMemberExtendedInformatiaon.UpdatorId = tempMemberObjectiveandSummary.UpdatorId = upload.CreatorID;
                facade.UpdateMember(tempMember);
                facade.UpdateMemberDetail(tempMemberDetail);
                facade.UpdateMemberExtendedInformation(tempMemberExtendedInformatiaon);
                facade.UpdateMemberObjectiveAndSummary(tempMemberObjectiveandSummary);

                AssignMemberManager();
                try
                {
                    HiringMatrixLevels level = facade.HiringMatrixLevel_GetInitialLevel();
                    //AddToRequisition(Details.JobPostingId, m_member.Id, CreatorID, (level == null ? 1 : level.Id), facade);
                    //TPS360.Web.UI.MiscUtil.EventLogForCandidate(TPS360.Common.Shared.EventLogForCandidate.CandidateAddedToRequisition, Details.JobPostingId, m_member.Id.ToString(), CreatorID, facade);
                    if (Details.JobPostingId != 0)
                    {
                        facade.MemberJobCart_AddCandidateToRequisition(CreatorID, m_member.Id.ToString(), Details.JobPostingId);
                    }
                }
                catch (System.Exception)
                {

                }
                try
                {
                    BuildMemberGroupMap(Details.HotlistId, m_member.Id, CreatorID);

                }
                catch (System.Exception)
                {

                }
                BuildResource(m_member);
                facade.AddResource(m_resource);


                IList<MemberDocument> docList = new List<MemberDocument>();
                docList = facade.GetAllMemberDocumentByMemberId(tempMember.Id);
                bool isValidFileName = false;
                int count = 0;
                string filena = upload.FileName;
                while (!isValidFileName)
                {
                    var s = docList.Where(x => x.FileName == upload.FileName);
                    if (s.Count() == 0)
                    {
                        isValidFileName = true;
                        break;
                    }
                    else
                    {
                        count++;
                        upload.FileName = filena.Remove(filena.LastIndexOf('.'), filena.Length - filena.LastIndexOf('.')) + "(" + count + ")";
                        upload.FileName = upload.FileName + ".doc";
                    }
                }
                MemberDocument memDocument = BuildMemberDocument(CreatorID, upload.FileName);
                memDocument.MemberId = m_member.Id;
                facade.AddMemberDocument(memDocument);
                UploadFiles(upload);
                SaveNotsAndActivities();
            }
            catch
            {
            }
            return upload;
            //updetails = upload;
            //BinaryFormatter binary = new BinaryFormatter();
            //MemoryStream mem = new MemoryStream(upload.EducationInfo);
            //EducatationInfo = (DataTable)binary.Deserialize(mem);
            //mem = new MemoryStream(upload.WorkHistoryInfo);
            //workhistory = (DataTable)binary.Deserialize(mem);
            //mem.Close();
            ////return educa.Rows .Count + workhis .Rows .Count + "";
            //XmlSerializer ob = new XmlSerializer(typeof(UploadDownloadDetailsDB));
            //Details = (UploadDownloadDetailsDB)ob.Deserialize(new StringReader(upload.Data));
            ////insert(r, workhis, educa);
            //CreatorID = upload.CreatorID;
            //copypastresume = upload.CopyPastResume;
            //m_member = upload.m_member;
            //m_memberDetail = upload.m_memberDetail;
            //m_memberExtendedInformation = upload.m_memberExtendedInformation;
            //m_memberObjectiveAndSummary = upload.m_memberObjectiveAndSummary;
            //ResumeLocation = upload.ResumeLocation;
            //m_resource = upload.m_resource;
            //m_memberJobCart = upload.m_memberJobCart;
            //filenames = upload.FileName;
            //UpdateStatus s = Update();
            //UploadDownloadDetails result = new UploadDownloadDetails();
            //result.Status = s;

            //return result;


        }
        public UpdateStatus Update()
        {
            try
            {
                BuildMemberData();

                Member updateMember = facade.GetMemberByUserName(Details.PrimaryEmail);

                m_member.Id = updateMember.Id;
                facade.UpdateMember(m_member);
                MemberDetail memdet = facade.GetMemberDetailByMemberId(updateMember.Id);
                m_memberDetail.MemberId = updateMember.Id;
                m_memberDetail.Id = memdet.Id;
                facade.UpdateMemberDetail(m_memberDetail);
                MemberExtendedInformation memExt = facade.GetMemberExtendedInformationByMemberId(updateMember.Id);
                m_memberExtendedInformation.MemberId = updateMember.Id;
                m_memberExtendedInformation.Id = memExt.Id;
                facade.UpdateMemberExtendedInformation(m_memberExtendedInformation);
                MemberObjectiveAndSummary memObj = facade.GetMemberObjectiveAndSummaryByMemberId(updateMember.Id);
                m_memberObjectiveAndSummary.MemberId = updateMember.Id;
                m_memberObjectiveAndSummary.Id = memObj.Id;
                facade.UpdateMemberObjectiveAndSummary(m_memberObjectiveAndSummary);
                try
                {
                    //m_memberJobCart.MemberId = updateMember.Id;
                    //m_memberJobCart.Id = updateMember.Id;
                    //facade.UpdateMemberJobCart(m_memberJobCart);
                    AddToRequisition(Details.JobPostingId, m_member.Id, CreatorID, 1, facade);
                }
                catch (System.Exception ex)
                {
                    //                ErrorLogger.Log(ex);
                }
                bool bval = facade.DeleteMemberExperienceByMemberId(m_member.Id);
                AddMemberExperience();
                bool bVal = facade.DeleteMemberEducationByMemberId(m_member.Id);
                AddMemberEducation();
                // Code Added For Defect Fix 11388
                AssignMemberManager();
                // End of 11388
                UpdateMemberDocument(facade, updateMember, CreatorID);
                try
                {
                    BuildMemberGroupMap(Details.HotlistId, m_member.Id, CreatorID);
                }
                catch (System.Exception)
                {

                }

                //Add Note
             

                return UpdateStatus.Updated;
            }
            catch (System.Exception ex)
            {
                // ErrorLogger.Log(ex);
            }
            return UpdateStatus.Error;
        }
        private void UpdateMemberDocument(IFacade facade, Member updateMember, int CreatorID)
        {
            try
            {
                string fileName = updetails.FileName;
                MemberDocument memDoc = facade.GetMemberDocumentByMemberIdAndFileName(updateMember.Id, fileName);
                if (memDoc == null)
                {
                    //memDoc = new MemberDocument();
                    //memDoc.CreatorId = CreatorID;
                    AddMemberDocument(facade, updateMember, CreatorID);
                    #region Webservice Implementation
                    //UploadResume(updateMember, fileName);
                    UploadFiles(updetails);
                    #endregion
                }
                else
                {
                    memDoc.FileName = fileName;
                    memDoc.Description = "";
                    memDoc.FileTypeLookupId = 55;
                    memDoc.UpdatorId = CreatorID;
                    memDoc.Title = Details.FirstName + "" + Details.MiddleName + "" + Details.LastName + " - Resume";
                    memDoc.Id = memDoc.Id;
                    facade.UpdateMemberDocument(memDoc);
                    #region Webservice Implementation
                    // UploadResume(updateMember, fileName);
                    UploadFiles(updetails);
                    #endregion
                }


            }
            catch (System.Exception ex)
            {
                //ErrorLogger.Log(ex);
            }
        }
        private void AddMemberDocument(IFacade facade, Member updateMember, int CreatorID)
        {
            MemberDocument memDocument = BuildMemberDocument(CreatorID);
            memDocument.MemberId = updateMember.Id;
            facade.AddMemberDocument(memDocument);
        }
        private void BuildResource(Member member)
        {
            m_resource.EmailAddress = member.PrimaryEmail;
            m_resource.ResourceName = member.Id.ToString();
        }
        private void BuildMemberGroupMap(int m_HotlistId, int memberId, int CreatorID)
        {
            MemberGroupMap memberGroupMap = facade.GetMemberGroupMapByMemberIdandMemberGroupId(memberId, m_HotlistId);
            if (memberGroupMap == null)
            {
                MemberGroupMap newMemberGroupMap = new MemberGroupMap();
                newMemberGroupMap.CreateDate = DateTime.Now;
                newMemberGroupMap.CreatorId = CreatorID;
                newMemberGroupMap.UpdateDate = DateTime.Now;
                newMemberGroupMap.UpdatorId = CreatorID;
                newMemberGroupMap.MemberGroupId = m_HotlistId;
                newMemberGroupMap.MemberId = memberId;
                facade.AddMemberGroupMap(newMemberGroupMap);
            }
            else if (memberGroupMap.IsRemoved)
            {
                memberGroupMap.IsRemoved = false;
                memberGroupMap.UpdateDate = DateTime.Now;
                memberGroupMap.UpdatorId = CreatorID;
                facade.UpdateMemberGroupMap(memberGroupMap);
            }
        }
        private void AddToRequisition(int jobpostingid, int memberid, int creatorId, int levelToMove, IFacade facade)
        {
            int level = levelToMove;
            MemberJobCart memberJobCart = facade.GetMemberJobCartByMemberIdAndJobPostringId(memberid, jobpostingid);
            if (memberJobCart == null)
            {
                memberJobCart = new MemberJobCart();
                memberJobCart.JobPostingId = jobpostingid;
                memberJobCart.MemberId = memberid;
                memberJobCart.IsPrivate = false;
                memberJobCart.UpdateDate = DateTime.Now;
                memberJobCart.UpdatorId = creatorId;
                memberJobCart.CreatorId = creatorId;
                memberJobCart.ApplyDate = DateTime.Now;
                memberJobCart = facade.AddMemberJobCart(memberJobCart);
            }
            else if (memberJobCart.IsRemoved)
            {
                memberJobCart.IsRemoved = false;
                memberJobCart.UpdatorId = creatorId;
                memberJobCart.UpdateDate = DateTime.Now;
                facade.UpdateMemberJobCart(memberJobCart);
            }
            if (memberJobCart != null)
            {
                IList<MemberJobCartDetail> memberJobCartDetailList = facade.GetAllMemberJobCartDetailByMemberJobCartId(memberJobCart.Id);
                if (memberJobCartDetailList != null && memberJobCartDetailList.Count > 0)
                {
                    foreach (MemberJobCartDetail memberJobCartDetail in memberJobCartDetailList)
                    {
                        if (memberJobCartDetail.IsRemoved == false)
                            level = memberJobCartDetail.SelectionStepLookupId;
                        memberJobCartDetail.IsRemoved = true;
                        memberJobCartDetail.UpdatorId = creatorId;
                        memberJobCartDetail.UpdateDate = DateTime.Now;
                        facade.UpdateMemberJobCartDetail(memberJobCartDetail);
                    }
                }

                MemberJobCartDetail memberJobCartDetailNew = facade.GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(memberJobCart.Id, level);
                if (memberJobCartDetailNew != null)
                {
                    memberJobCartDetailNew.IsRemoved = false;
                    memberJobCartDetailNew.UpdatorId = creatorId;
                    memberJobCartDetailNew.UpdateDate = DateTime.Now;
                    facade.UpdateMemberJobCartDetail(memberJobCartDetailNew);
                }
                else
                {
                    memberJobCartDetailNew = new MemberJobCartDetail();
                    memberJobCartDetailNew.MemberJobCartId = memberJobCart.Id;
                    memberJobCartDetailNew.SelectionStepLookupId = levelToMove;
                    memberJobCartDetailNew.AddedDate = DateTime.Now;
                    memberJobCartDetailNew.AddedBy = creatorId;
                    memberJobCartDetailNew.CreatorId = creatorId;
                    facade.AddMemberJobCartDetail(memberJobCartDetailNew);
                }
            }
            TPS360.Web.UI.MiscUtil.EventLogForCandidate(TPS360.Common.Shared.EventLogForCandidate.CandidateAddedToRequisition, jobpostingid, memberid.ToString (), creatorId, facade);
        }
        private string AddMemberDocument()
        {
            MemberDocument memDocument = BuildMemberDocument(CreatorID);
            memDocument.MemberId = m_member.Id;
            facade.AddMemberDocument(memDocument);
            #region Webservice Implementation
            // UploadResume(m_member, filenames );
           return UploadFiles(updetails);

            #endregion
        }
        private MemberDocument BuildMemberDocument(int CreatorID)
        {
            MemberDocument memDocument = new MemberDocument();
            memDocument.CreatorId = CreatorID;
            memDocument.Description = "";
            memDocument.CreateDate = DateTime.Today;
            memDocument.FileName = GetMemberFileName();
            memDocument.Title = Details.FirstName + "" + Details.MiddleName + "" + Details.LastName + " - Resume";
            memDocument.IsRemoved = false;
            memDocument.UpdatorId = CreatorID;
            memDocument.FileTypeLookupId = 55;
            return memDocument;
        }
        private MemberDocument BuildMemberDocument(int CreatorID,string fileName)
        {
            MemberDocument memDocument = new MemberDocument();
            memDocument.CreatorId = CreatorID;
            memDocument.Description = "";
            memDocument.CreateDate = DateTime.Today;
            memDocument.FileName = fileName ;
            memDocument.Title = fileName;
            memDocument.IsRemoved = false;
            memDocument.UpdatorId = CreatorID;
            memDocument.FileTypeLookupId = 55;
            return memDocument;
        }
        private string GetMemberFileName()
        {
            string ext = Path.GetExtension(ResumeLocation).Equals(".tmp") ? ".txt" : Path.GetExtension(ResumeLocation);
            string fileName = Details.FirstName + Details.LastName + " - Resume" + ext;
            return fileName;
        }
        private void AssignMemberManager()
        {
            if (Details.AssignToMyself == true)
            {
                MemberManager manager = new MemberManager();
                manager = facade.GetMemberManagerByMemberIdAndManagerId(m_member.Id, CreatorID);
                if (manager == null)
                {
                    MemberManager AddManager = new MemberManager();
                    BuildMemberManager(AddManager, CreatorID);
                    facade.AddMemberManager(AddManager);
                }
            }
        }
        private void BuildMemberManager(MemberManager manager, int CreatorID)
        {
            manager.IsPrimaryManager = true;
            manager.IsRemoved = false;
            manager.MemberId = m_member.Id;
            manager.ManagerId = CreatorID;
            manager.CreatorId = CreatorID;
            manager.UpdatorId = CreatorID;
            manager.CreateDate = DateTime.Today;
            manager.UpdateDate = DateTime.Today;
        }
        private void AddMemberSkillMap()
        {
            try
            {
                if (SkillInfo != null)
                {

                    for (int i = 0; i < SkillInfo.Rows.Count; i++)
                    {
                        try
                        {
                            MemberSkillMap skillMap = BuildMemberSkillMap(i, CreatorID);
                            facade.AddMemberSkillMap(skillMap);
                        }
                        catch
                        {
                        }
                    }
                    SkillInfo = null;

                }
            }
            catch (System.Exception ex)
            {
                // ErrorLogger.Log(ex);
            }
        }
        private MemberSkillMap BuildMemberSkillMap(int i, int CreatorID)
	    {
		        MemberSkillMap m_members=new MemberSkillMap ();
                int skillid=facade .GetSkillIdBySkillName (SkillInfo.Rows[i][0].ToString() .Trim ());
                if (skillid == 0)
                {
                    Skill newskill = new Skill();
                    newskill.Name = SkillInfo.Rows[i][0].ToString() .Trim ();
                    skillid =facade.AddSkill(newskill).Id;
                }
		        m_members.SkillId=skillid;
                m_members.CreatorId=CreatorID;
                m_members.UpdatorId=CreatorID;
                m_members.MemberId = m_member.Id;
                try
                {
                    m_members.YearsOfExperience = Convert.ToInt32(Math.Round(Convert.ToDouble(SkillInfo.Rows[i][1].ToString()), 0));
                }
                catch
                {
                    m_members.YearsOfExperience = 0;
                }
                try
                {
                    m_members.LastUsed = SkillInfo.Rows[i][2].ToString().Trim() == string.Empty ? DateTime.MinValue : new DateTime(Convert.ToInt32(SkillInfo.Rows[i][2].ToString()), 01, 01);
                }
                catch
                {
                    m_members.LastUsed = DateTime.MinValue;
                }
               return m_members;

	  }

	 private void AddMemberLicense()
        {
            try
            {
                if (LicenseInfo != null)
                {
                    try
                    {
                        for (int i = 0; i < LicenseInfo.Rows.Count; i++)
                        {
                            MemberCertificationMap certificationMap = BuildMemberCertificationMap(i, CreatorID);
                            facade.AddMemberCertificationMap(certificationMap);
                        }
                        LicenseInfo = null;
                    }
                    catch
                    {
                    }

                }
            }
            catch (System.Exception ex)
            {
                // ErrorLogger.Log(ex);
            }
        }
     public void ConvertByteArrayToIList<T>(byte[] byteArray, out IList<T> obj)
     {

         AppDomain currentDomain = AppDomain.CurrentDomain;
         BinaryFormatter bf = new BinaryFormatter();
         using (MemoryStream ms = new MemoryStream(byteArray))
         {
             obj = (IList<T>)bf.Deserialize(ms);
         }
     }
        private  MemberCertificationMap BuildMemberCertificationMap(int i, int CreatorID)
	{
		        MemberCertificationMap memberCertificationMap = new MemberCertificationMap();
                memberCertificationMap.MemberId = m_member.Id;
		        memberCertificationMap.UpdatorId = CreatorID;
                try
                {
                    memberCertificationMap.CerttificationName = LicenseInfo.Rows[i][0].ToString();
                    if (LicenseInfo.Rows[i][1] != System.DBNull.Value && LicenseInfo.Rows[i][1].ToString().Trim() != "") memberCertificationMap.ValidFrom = Convert.ToDateTime(LicenseInfo.Rows[i][1].ToString());
                    if (LicenseInfo.Rows[i][2] != System.DBNull.Value && LicenseInfo.Rows[i][2].ToString().Trim() != "") memberCertificationMap.ValidTo = Convert.ToDateTime(LicenseInfo.Rows[i][2].ToString());
                    memberCertificationMap.IssuingAthority = LicenseInfo.Rows[i][3].ToString();
                }
                catch
                {
                }
               return memberCertificationMap;

	}


       
        private void AddMemberEducation()
        {
            try
            {
                if (EducatationInfo != null)
                {

                    for (int i = 0; i < EducatationInfo.Rows.Count; i++)
                    {
                        try
                        {
                            MemberEducation education = BuildMemberEducation(i, CreatorID);
                            facade.AddMemberEducation(education);
                        }
                        catch
                        {
                        }
                    }
                    EducatationInfo = null;

                }
            }
            catch (System.Exception ex)
            {
                // ErrorLogger.Log(ex);
            }
        }
        private MemberEducation BuildMemberEducation(int i, int CreatorID)
        {

            MemberEducation m_memberEducation = new MemberEducation();
        
            m_memberEducation.DegreeTitle = EducatationInfo.Rows[i][1].ToString();
            m_memberEducation.InstituteName = EducatationInfo.Rows[i][8].ToString();
            m_memberEducation.DegreeObtainedYear = EducatationInfo.Rows[i][7].ToString();
            m_memberEducation.MajorSubjects = EducatationInfo.Rows[i][2].ToString();
            if (EducatationInfo.Rows[i][14] == DBNull.Value || EducatationInfo.Rows[i][14].ToString ()=="0")
            {
                try
                {
                    if (EducatationInfo.Rows[i][0].ToString() != string.Empty)
                    {
                      IList <GenericLookup > level=  facade.GetAllGenericLookupByLookupTypeAndName(TPS360.Common.Shared.LookupType.EducationalQualificationType, EducatationInfo.Rows[i][0].ToString());
                      if (level != null && level.Count > 0) m_memberEducation.LevelOfEducationLookupId = level[0].Id;
                      else  m_memberEducation.LevelOfEducationLookupId = 0;
                    }
                    else  m_memberEducation.LevelOfEducationLookupId = 0;
                }
                catch
                {
                    m_memberEducation.LevelOfEducationLookupId = 0;
                }
            }
            else 
            m_memberEducation.LevelOfEducationLookupId = Convert.ToInt32(EducatationInfo.Rows[i][14]);
            m_memberEducation.GPA = EducatationInfo.Rows[i][3].ToString();
            m_memberEducation.GpaOutOf = EducatationInfo.Rows[i][4].ToString();
            try
            {
                m_memberEducation.AttendedFrom = EducatationInfo.Rows[i][5].ToString().Trim() == "" ? Convert.ToDateTime("01/01/0001") : Convert.ToDateTime(EducatationInfo.Rows[i][5]);
            }
            catch
            {
                m_memberEducation.AttendedFrom = Convert.ToDateTime("01/01/0001");
            }
            try
            {
                m_memberEducation.AttendedTo = EducatationInfo.Rows[i][6].ToString().Trim() == "" ? Convert.ToDateTime("01/01/0001") : Convert.ToDateTime(EducatationInfo.Rows[i][6]);
            }
            catch
            {
                m_memberEducation.AttendedTo = Convert.ToDateTime("01/01/0001");
            }
            if (EducatationInfo.Rows[i][15] == DBNull.Value)
                m_memberEducation.InstituteCountryId = 0;
            else 
            m_memberEducation.InstituteCountryId = Convert.ToInt32(EducatationInfo.Rows[i][15]);
            m_memberEducation.InstituteCity = EducatationInfo.Rows[i][11].ToString();
            m_memberEducation.BriefDescription = EducatationInfo.Rows[i][12].ToString();
            if (EducatationInfo.Rows[i][13] == DBNull.Value)
                m_memberEducation.IsHighestEducation = false;
            else 
            m_memberEducation.IsHighestEducation = Convert.ToBoolean(EducatationInfo.Rows[i][13]);
            // Code Added For Defect Fix 11081
            if (EducatationInfo.Rows[i][16] == DBNull.Value)
                m_memberEducation.StateId = 0;
            else 
            m_memberEducation.StateId = Convert.ToInt32(EducatationInfo.Rows[i][16]);
            // End
            m_memberEducation.CreatorId = CreatorID;
            m_memberEducation.UpdatorId = CreatorID;
            m_memberEducation.CreateDate = DateTime.Today;
            m_memberEducation.UpdateDate = DateTime.Today;
            m_memberEducation.MemberId = m_member.Id;
            return m_memberEducation;
        }
        private void AddMemberExperience()
        {
            try
            {
                if (workhistory != null)
                {
                    for (int e = 0; e < workhistory.Rows.Count; e++)
                    {
                        try
                        {
                            MemberExperience experience = BuildMemberExperience(e, CreatorID);
                            facade.AddMemberExperience(experience);
                        }
                        catch
                        {
                        }
                       
                    }
                    workhistory = null;
                }
            }
            catch (System.Exception ex)
            {
               // ErrorLogger.Log(ex);
            }
        }
        private MemberExperience BuildMemberExperience(int e, int CreatorID)
        {
        
            bool bTillDate=false ;
            if(workhistory .Rows [e][9]==DBNull .Value )
                bTillDate =false ;
            else
                bTillDate = Convert.ToBoolean(workhistory.Rows[e][9].ToString().Trim() == "1" || workhistory.Rows[e][9].ToString().Trim().ToLower () =="true" ? true : false );
         
            MemberExperience m_memberExperience = new MemberExperience();
            m_memberExperience.CompanyName = workhistory.Rows[e][0].ToString();
            m_memberExperience.PositionName = workhistory.Rows[e][1].ToString();
            if (workhistory.Rows[e][11] == DBNull.Value)
                m_memberExperience.FunctionalCategoryLookupId = 0;
            else 
            m_memberExperience.FunctionalCategoryLookupId = Convert.ToInt32(workhistory.Rows[e][11]);
            if (workhistory.Rows[e][12] == DBNull.Value)
                m_memberExperience.IndustryCategoryLookupId = 0;
            else 
            m_memberExperience.IndustryCategoryLookupId = Convert.ToInt32(workhistory.Rows[e][12]);
            if (workhistory.Rows[e][13] == DBNull.Value)
                m_memberExperience.CountryLookupId = 0;
            else 
            m_memberExperience.CountryLookupId = Convert.ToInt32(workhistory.Rows[e][13]);
            if (workhistory.Rows[e][14] == DBNull.Value)
                m_memberExperience.StateId = 0;
            else
            m_memberExperience.StateId = Convert.ToInt32(workhistory.Rows[e][14]);
            m_memberExperience.City = workhistory.Rows[e][6].ToString();
            m_memberExperience.DateFrom = workhistory.Rows[e][7].ToString().Trim () == "" ? Convert.ToDateTime("01/01/0001") : Convert.ToDateTime(workhistory.Rows[e][7]);
            if (bTillDate)
            {
                m_memberExperience.DateTo = System.DateTime.Today;
            }
            else
            {
                try
                {
                    m_memberExperience.DateTo = workhistory.Rows[e][8].ToString().Trim() == "" ? Convert.ToDateTime("01/01/0001") : Convert.ToDateTime(workhistory.Rows[e][8]);
                }
                catch
                {
                }
            }
            m_memberExperience.IsTillDate = bTillDate;
            m_memberExperience.Responsibilities = workhistory.Rows[e][10].ToString();
            // Code Added For Defect Fix 11081
            // End
            m_memberExperience.CreatorId = CreatorID;
            m_memberExperience.UpdatorId = CreatorID;
            m_memberExperience.CreateDate = DateTime.Today;
            m_memberExperience.UpdateDate = DateTime.Today;
            m_memberExperience.MemberId = m_member.Id;
            return m_memberExperience;
        }

        private T PropertyCheck<T>(T ObFromDb, T ObFromParser)
        {

            foreach (System.Reflection.PropertyInfo info in ObFromDb.GetType().GetProperties())
            {
                if (info.PropertyType.Name == "String" || info.PropertyType.Name == "Int" || info.PropertyType.Name == "Int32" || info.PropertyType.Name == "Double" || info.PropertyType.Name == "Decimal" || info.PropertyType.Name == "Boolean" || info.PropertyType.Name == "DateTime")
                {
                    try
                    {
                        object value = ObFromDb.GetType().GetProperty(info.Name).GetValue(ObFromDb, null);
                        object ParsedValue = ObFromParser.GetType().GetProperty(info.Name).GetValue(ObFromParser, null);
                        if (value !=null && ParsedValue !=null && (value.ToString() == string.Empty || value.ToString() != ParsedValue.ToString()) && ParsedValue.ToString() != string.Empty)
                        {

                            ObFromDb.GetType().GetProperty(info.Name).SetValue(ObFromDb, (object)ParsedValue, null);
                        }

                    }
                    catch
                    {
                    }
                }
                string m1 = info.Name;
            }
            return ObFromDb;
        }

        private void  BuildMemberData()
        {
            BuildMember(CreatorID);
            BuildMemberDetail(CreatorID);
            BuildMemberExtendedInformation(CreatorID);
            BuildMemberObjectiveAndSummary(CreatorID);

        }
        private void BuildMember(int CreatorID)
        {
            m_member.IsRemoved = false;
            m_member.AutomatedEmailStatus = true;
            //m_member.Status = 1;
            m_member.CreateDate = DateTime.Today;
            m_member.UpdateDate = DateTime.Today;
            m_member.CreatorId = CreatorID;
            m_member.UpdatorId = CreatorID;
            // Code Added for Defect Fix 11388
            m_member.ResumeSource = Details.Source;
            // End of 11388
        }
        private void BuildMemberDetail(int CreatorID)
        {
            m_memberDetail.CreatorId = CreatorID;
            m_memberDetail.UpdatorId = CreatorID;
            m_memberDetail.CreateDate = DateTime.Today;
            m_memberDetail.UpdateDate = DateTime.Today;

        }
        private void BuildMemberExtendedInformation(int CreatorID)
        {
            m_memberExtendedInformation.CreatorId = CreatorID;
            m_memberExtendedInformation.UpdatorId = CreatorID;
            m_memberExtendedInformation.CreateDate = DateTime.Today;
            m_memberExtendedInformation.UpdateDate = DateTime.Today;
        }
        private void BuildMemberObjectiveAndSummary(int CreatorID)
        {

            m_memberObjectiveAndSummary.CreatorId = CreatorID;
            m_memberObjectiveAndSummary.UpdatorId = CreatorID;
            m_memberObjectiveAndSummary.CopyPasteResume = copypastresume;
            m_memberObjectiveAndSummary.CreateDate = DateTime.Today;
            m_memberObjectiveAndSummary.UpdateDate = DateTime.Today;
            //evan
            m_memberObjectiveAndSummary.RawCopyPasteResume = StripTagsCharArray(m_memberObjectiveAndSummary.CopyPasteResume);
        }
        public static string StripTagsCharArray(string source)
        {
            if (source == null)
                return "";

            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
        public enum UploadStatus       //0.2
        {
            Successful,
            UnSuccessful,
            NotAuthorized
        };
        public struct UploadDownloadDetails     //0.3
        {
            public bool UpdateDuplicateResumes;
            public string Data;
            public MemberJobCart m_memberJobCart;
            public Resource m_resource;
            public string ResumeLocation;
            public Member m_member;
            public MemberExtendedInformation m_memberExtendedInformation;
            public MemberDetail m_memberDetail;
            public MemberObjectiveAndSummary m_memberObjectiveAndSummary;



            public string CopyPastResume
            {
                get;
                set;
            }
            public int CreatorID
            {
                get;
                set;
            }
            public byte[] EducationInfo
            {
                get;
                set;
            }
            public byte[] WorkHistoryInfo
            {
                get;
                set;
            }

	   public byte[]  SkillsArray
          {
                get;
                set;
            }
           public byte[] License {
                get;
                set;
            }
            private UpdateStatus status;

            public UpdateStatus Status
            {
                get;
                set;
            }

            [System.Xml.Serialization.XmlElementAttribute(DataType = "base64Binary")]
            public byte[] DownloadedByteArray
            {
                get;
                set;
            }

            /// <remarks/>
            public string FileName
            {
                get;
                set;
            }

            /// <remarks/>
            public int MemberId
            {
                get;
                set;
            }


            public string UserName
            {
                get;
                set;
            }

            public string Password
            {
                get;
                set;
            }

        }

        [WebMethod]
        public string GetFilePath()
        {
            return str;
        }

        //[WebMethod]
        //public string SetUserNamePassword(string UserName, string Password)
        //{
        //    strCredentials = UserName + "~" + Password;
        //    return strCredentials;
        //}

        [WebMethod]
        public string SetFilePath(string value)
        {
            str = value;
            return str;
        }

        [WebMethod]
        public LicenceValidator GetLicenseArray(LicenceValidator license)
        {
            LicenceValidator result = new LicenceValidator();
            result.Licence = facade.GetLicenseKeybyId(license.licenceKey, license.Domain);
            return result;
        }



        //public string UploadFile(byte[] f, string fileName, int intMemberId)
        public string UploadFiles(UploadDownloadDetails upload)       //0.3
        {
            string fileName = upload.FileName;                                      //0.3
            string strMemberId = m_member.Id.ToString();                                      //0.3
            //Line commented by Prasanth on 10/Jun/2016//0.3
            //bool IsUserValid = Membership.ValidateUser(upload.UserName, upload.Password);
            //Code introduced by Prasanth on 10/Jun/2016 Start

            bool IsUserValid;
            Member Mem = new Member();
            bool isldap;
            Mem = facade.GetMemberByUserName(upload.UserName);
            if (Mem == null)
            {
                isldap = false;
            }
            else
            {
                isldap = Mem.IsLDAP;
            }

            if (isldap == true)
            {
                IsUserValid = ActiveDirectoryConnector.IsUserLoggedIn(upload.UserName, upload.Password);
            }
            else
            {
                IsUserValid = Membership.ValidateUser(upload.UserName, upload.Password);
            }
            //*******************END**************************

            if (IsUserValid)    //0.1
            {
                try
                {
                    byte[] f = upload.DownloadedByteArray;
                    MemoryStream ms = new MemoryStream(f);

                    string strResPath = GetFilePath();

                   String strFilePath = string.Empty;
                    if (strResPath == string.Empty)
                    {
                        strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Resources/Member");
                    }
                    else
                    {
                        strFilePath = System.Web.Hosting.HostingEnvironment.MapPath(strResPath);
                    }
                    strFilePath = strFilePath + "\\" + strMemberId;
                  
                    if (!Directory.Exists(strFilePath))
                    {
                        Directory.CreateDirectory(strFilePath);
                    }
                    strFilePath = strFilePath + "\\Word Resume";

                    if (!Directory.Exists(strFilePath))
                    {
                        Directory.CreateDirectory(strFilePath);
                    }
                      FileStream fs = new FileStream
                     ( strFilePath + "\\" + fileName, FileMode.Create);
                      ms.WriteTo(fs);
                    ms.Close();
                    fs.Close();
                    fs.Dispose();
                    return strFilePath + "\\" + fileName;
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
            else
            {
            }
            return "";
        }



        [WebMethod]
        //public byte[] DownloadFile(string fileName, int intMemberId)
        public UploadDownloadDetails DownloadFile(UploadDownloadDetails download)       //0.3
        {
            string fileName = download.FileName;                                        //0.3
            int intMemberId = download.MemberId;                                        //0.3
            UploadDownloadDetails tempDetails = new UploadDownloadDetails();            //0.3

            //bool IsUserValid = Membership.ValidateUser(System.Net.CredentialCache.DefaultNetworkCredentials.UserName, System.Net.CredentialCache.DefaultNetworkCredentials.Password);   //0.1

            bool IsUserValid = Membership.ValidateUser(download.UserName, download.Password);

            if (IsUserValid)
            {

                string strResPath = string.Empty;

                String strFilePath = string.Empty;

                if (strResPath == string.Empty)
                {
                    strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Resources/Member");
                }
                else
                {
                    strFilePath = System.Web.Hosting.HostingEnvironment.MapPath(strResPath);
                }

                strFilePath = strFilePath + "\\" + intMemberId.ToString() + "\\Word Resume" + "\\" + fileName;

                System.IO.FileStream fileStream = null;
                if (System.IO.File.Exists(strFilePath))
                {
                    fileStream = System.IO.File.Open(strFilePath, FileMode.Open, FileAccess.Read);
                }
                //byte[] arrByte = new byte[fileStream.Length];
                //fileStream.Read(arrByte, 0, (int)fileStream.Length);
                //fileStream.Close();
                //return arrByte;

                tempDetails.DownloadedByteArray = new byte[fileStream.Length];                  //0.3
                fileStream.Read(tempDetails.DownloadedByteArray, 0, (int)fileStream.Length);    //0.3
                fileStream.Close();

                //tempDetails.Status = UploadStatus.Successful;
                tempDetails.FileName = fileName;
            }
            else
            {
                //tempDetails.Status = UploadStatus.NotAuthorized;       //0.3
            }

            return tempDetails;     //0.3
        }
        [WebMethod]
        public GetValidSender GetValidSenderID(GetValidSender value)
        {
            GetValidSender objsender = new GetValidSender();
            BinaryFormatter binary = new BinaryFormatter();
            MemoryStream mem = null;
            DataTable senderid = new DataTable();

            if (value.SenderId.Length > 0)
            {
                mem = new MemoryStream(value.SenderId);
                senderid = (DataTable)binary.Deserialize(mem);
                mem.Close();
            }
            DataTable dtresule = new DataTable();
            dtresule.Columns.Add("SenderId");

            foreach (DataRow dr in senderid.Rows)
            {
                facade = new Facade();
                try
                {
                    if (facade.ValidateSenderEmailID(dr[0].ToString()) == true)
                    {
                        DataRow drow = dtresule.NewRow();
                        drow[0] = dr[0].ToString();
                        dtresule.Rows.Add(drow);
                    }
                }
                catch (Exception ex)
                {
                    objsender.errormessage = ex.Message + ex.Data;
                }
            }

            //serialize
            MemoryStream memresule = new MemoryStream();
            if (dtresule != null)
            {
                binary.Serialize(memresule, dtresule);
            }
            byte[] se = memresule.ToArray();

            memresule.Close();

            objsender.SenderId = se;
            return objsender;

        }
        [WebMethod]
        public SyncUploadDownloadDetails SentItemSync(SyncUploadDownloadDetails value)
        {
           

            return sentitem(value);
            //if (value.mtype == MailType.SentMail) return sentitem(value);
            //else if (value.mtype == MailType.ReceivedMail) return receiveditem(value);
            //else return null;
        }
        public SyncUploadDownloadDetails sentitem(SyncUploadDownloadDetails value)
        {
            SyncUploadDownloadDetails newobj = new SyncUploadDownloadDetails();
           // try
           // {
                MemberEmail _member = new MemberEmail();
                _member.SenderId = value.SenderID;
                _member.SenderEmail = value.SenderEmail;
                _member.ReceiverEmail = value.ReceiverEmail;
                _member.ReceiverId = value.ReceiverID;
                _member.IsRemoved = value.isRemoved;
                _member.ParentId = value.ParentId;
                _member.EmailTypeLookupId = value.EmailTypeLookupId;
                _member.Subject = value.Subject;
                _member.EmailBody = value.EmailBody;
                _member.Status = value.Status;
                _member.CreatorId = value.CreatorId;

                _member.BCC = value.BCC;
                _member.CC = value.CC;
                _member.SentDate =value.SentDate;
                _member.AttachedFileNames = value.AttachedFileNames;
                _member.NoOfAttachedFiles = value.Attcount;
                MemberEmail _memberemail = new MemberEmail();
               _memberemail= facade.AddMemberEmail(_member);

               int emailid = _memberemail.Id;
                int i = 0;
                //MemoryStream ms = new MemoryStream(value.att[i]);
                //      FileStream fs = new FileStream("C:\\Develop\\" + value.FileName, FileMode.Create);
                //        ms.WriteTo(fs);
                //        ms.Close();
                //        fs.Close();
                if( value.Attcount>0)
                {
                string strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Resources/Member");
                strFilePath = strFilePath + "\\" + 9999; ;
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }

                strFilePath = strFilePath + "\\Outlook Sync";
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
                strFilePath = strFilePath + "\\" + emailid; ;
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
                for (i = 0; i < value.Attcount; i++)
                {
                    if (value.att[i] != null)
                    {

                        MemoryStream ms = new MemoryStream(value.att[i]);
                        FileStream fs = new FileStream
                       (strFilePath + "\\" + value.FileName[i], FileMode.Create);
                        ms.WriteTo(fs);
                        ms.Close();
                        fs.Close();
                        fs.Dispose();



                    }
                        //FileStream fs = new FileStream("C:\\Develop\\" + value.FileName, FileMode.Create);
                        //ms.WriteTo(fs);
                        //ms.Close();
                        //fs.Close();
                    }
                }




                newobj.SyncStatus = true;
           // }
          //  catch
            //{
             //   newobj.SyncStatus = false;
           // }
            return newobj;
        }
        public SyncUploadDownloadDetails receiveditem(SyncUploadDownloadDetails value)
        {
            return value;
            //SyncUploadDownloadDetails newobj = new SyncUploadDownloadDetails();
            //try
            //{
            //   // MemberEmail _memberemail = new MemberEmail();
            //    if(facade.ValidateSenderEmailID(value.SenderEmail))
            //       {
            //           newobj .ReceiverEmail ="No such email";

            //       }
            //       else
            //       {
            //            newobj .ReceiverEmail ="There is Email";
            //       }


            //    MemberEmail _member = new MemberEmail();
            //    _member.SenderId = value.SenderID;
            //    _member.SenderEmail = value.SenderEmail;
            //    _member.ReceiverEmail = value.ReceiverEmail;
            //    _member.ReceiverId = value.ReceiverID;
            //    _member.IsRemoved = value.isRemoved;
            //    _member.ParentId = value.ParentId;
            //    _member.EmailTypeLookupId = value.EmailTypeLookupId;
            //    _member.Subject = value.Subject;
            //    _member.EmailBody = value.EmailBody;
            //    _member.Status = value.Status;
            //    _member.CreatorId = value.CreatorId;

            //    _member.BCC = value.BCC;
            //    _member.CC = value.CC;
            //    _member.SentDate = value.SentDate;
            //    facade.AddMemberEmail(_member);

            //    newobj.SyncStatus = true;
            //}
            //catch
            //{
            //    newobj.SyncStatus = false;
            //}
            //return newobj;
        }

    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class UploadDownloadDetailsDB
    {
     
        private string s;
        public string FirstName
        {
            get { return this.s; }
            set { this.s = value; }
        }
        public int MemberType;
        public string MiddleName;
        public string LastName;
        public string NickName;
        public string AddressLine1;
        public string AddressLine2;
        public string City;
        private string m_state;
        public string State;
        public int PermanentStateId;
        private string m_country;
        public string Country;
        public int PermanentCountryId;
        private string m_zip;
        public string Zip;
        public string PrimaryEmail;
        public string AlternateEmail;
        public DateTime DateOfBirth;
        public string PermenantPhone;
        public string PrimaryPhone;
        public string PermenantMobile;
        public string CellPhone;
        public string DesiredPosition;
        public string CurrentAddressLine1;
        public string CurrentAddressLine2;
        public string CurrentCity;
        public bool IsCurrentAddressSameAsPermanant;
        private string m_currentstate;
        public string CurrentState;
        public int CurrentStateId;
        private string m_currentcountry;
        public string CurrentCountry;
        public int CurrentCountryID;
        public string CurrentZip;
        public string HomePhone;
        public string OfficePhone;
        public string SSN;
        public int CountryofBirth;
        public int CountryofResidence;
        public string CityofBirth;
        public string HomePhoneExt;
        public string OfficePhoneExt;
        public string Experience;
        public string CurrentCompany;
        public decimal CurrentYearlyRate;
        public int CurrentYearlyRateLookupID;
        public decimal CurrentMonthlyRate;
        public int CurrentMonthlyRateLookupID;
        public decimal CurrentHourlyRate;
        public int CurrentHourlyRateLookupID;
        public decimal ExpectedYearlyRate;
        public int ExpectedYearlyRateLookupID;
        public decimal ExpectedMonthlyRate;
        public int ExpectedMonthlyRateLookupID;
        public decimal ExpectedHourlyRate;
        public int ExpectedHourlyRateLookupID;
        public decimal ExpectedYearlyMaxRate;
        public decimal ExpectedMonthlyMaxRate;
        public decimal ExpectedHourlyMaxRate;
        public int WorkPermit;
        public bool WillingToTravel;
        public bool WillingToRelocate;
        public bool SecurityClearance;
        public int CandidateRating;
        public string Remarks;
        public string Note;
        public int Gender;
        public int MartialStatus;
        public int SalaryPaymentType;
        public int Joblength;
        public bool PassportStatus;
        public int Availability;
        public DateTime AvailableDate;
        public string Objective;
        public string Summary;
        public string Skills;
        public bool AssignToMyself;
        public bool RoleName;
        public int JobPostingId;
        public int HotlistId;
        public bool IsDuplicateName;
        public bool IsDuplicateEmail;
        public bool IsDuplicateDOB;
        public bool IsDuplicateSSN;
        public int Source;

    }
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class kj
    {
        private string str;
        //  [System.Xml.Serialization.XmlElementAttribute(DataType="string")]
        public string st
        {
            get { return this.str; }
            set { this.str = value; }
        }
        private string sy;
        public string st1
        {
            get { return this.sy; }
            set { this.sy = value; }

        }


    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class LicenceValidator
    {
        public ArrayList Licence
        {
            get;
            set;
        }
        public string Domain
        {
            get;
            set;
        }
        public string licenceKey
        {
            get;
            set;
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class DuplicateRecords
    {
        bool namefield;
        private IList<DuplicateMember> Datasourcefield;
        private string FirstnameField;
        private string LastNameField;
        private string middleNameField;
        bool Emailfield;
        private string PrimaryEmailfield;
        bool DOBfield;
        DateTime DateOfBirthfield;
        bool SSNfield;
        string strSSNfield;
        public bool Name
        {
            get { return this.namefield; }
            set { this.namefield = value; }
        }
        public string FirstName
        {
            get { return this.FirstnameField; }
            set { this.FirstnameField = value; }
        }
        public string LastName
        {
            get { return this.LastNameField; }
            set { this.LastNameField = value; }
        }
        public string MiddleName
        {
            get { return this.middleNameField; }
            set { this.middleNameField = value; }
        }
        public bool Email
        {
            get { return this.Emailfield; }
            set { this.Emailfield = value; }
        }
        public string PrimaryEmail
        {
            get { return this.PrimaryEmailfield; }
            set { this.PrimaryEmailfield = value; }
        }
        public bool DOB
        {
            get { return this.DOBfield; }
            set { this.DOBfield = value; }
        }
        public DateTime DateOfBirth
        {
            get { return this.DateOfBirthfield; }
            set { this.DateOfBirthfield = value; }
        }
        public bool SSN
        {
            get { return this.SSNfield; }
            set { this.SSNfield = value; }
        }
        public string strSSN
        {
            get { return this.strSSNfield; }
            set { this.strSSNfield = value; }
        }
        public IList<DuplicateMember> Datasource
        {
            get { return this.Datasourcefield; }
            set { this.Datasourcefield = value; }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class SyncUploadDownloadDetails
    {
        public byte[][] att;
        public MailType mtype;
        public bool SyncStatus;
        public int SenderID;
        public string SenderEmail;
        public string ReceiverEmail;
        public int ReceiverID;
        public bool isRemoved;
        public int ParentId;
        public int EmailTypeLookupId;
        public string Subject;
        public string EmailBody;
        public int Status;
        public int CreatorId;
        public int Attcount;
        public string BCC;
        public string CC;
        public string SentDate;
        public string[] FileName;
        public string AttachedFileNames;
     
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public enum MailType
    {
        SentMail,
        ReceivedMail,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class GetValidSender
    {
        public byte[] SenderId;
        public string errormessage;
    }
}
