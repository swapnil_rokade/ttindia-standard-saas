﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: DataBaseAccessWebService.cs
    Description: This is the page used for Login of the user.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               10/Jun/2016          Prasanth             introduced AD Login for parser
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
namespace DataBase
{
    using System;
    using System.Collections;
    using System.Linq;
    using System.Web;
    using System.Web.Services;
    using System.Web.Services.Protocols;
    using System.Xml.Linq;
    using TPS360.BusinessFacade;
    using TPS360.DataAccess;
    using TPS360.Controls;
    using TPS360.Common.BusinessEntities;
	 using TPS360.Common.Shared;
	     using System.Web.Security;
    using System.IO;
    using System.Text;
    using System.Xml.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
	using System.ComponentModel;
	using System.Collections.Generic;
    using System.Collections;
    using System.DirectoryServices;
    using System.DirectoryServices.ActiveDirectory;
    using TPS360.Web.UI.Helper;
    /// <summary>
    /// Summary description for DataBaseAccessWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class DataBaseAccessWebService : System.Web.Services.WebService
    {

        public DataBaseAccessWebService()
        {
            IFacade facade = new Facade();
            SiteSetting siteSetting = facade.GetSiteSettingBySettingType((int)TPS360.Common.Shared.SettingType.SiteSetting);
            if (siteSetting != null)
            {
                Country country = facade.GetCountryById(Convert.ToInt32(TPS360.Common.Helper.ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration)[TPS360.Common.Shared.DefaultSiteSetting.Country.ToString()]));
                if (country != null)
                {
                    if (country.Name == "India")
                    {
                        System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                        System.Globalization.DateTimeFormatInfo info = new System.Globalization.DateTimeFormatInfo();
                        info.ShortDatePattern = "dd/MM/yyyy";
                        culture.DateTimeFormat = info;

                        System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                        System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
                    }
                }
            }
            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }

        [WebMethod]
        public string GetDuplicateResumes(string value)
        {
            string duplicateIds = "";
            char[] deliemcomma={','};
            char[] delimCollon={';'};
            string[] ResumeList = value.Split(deliemcomma, StringSplitOptions.RemoveEmptyEntries);
            foreach (string resume in ResumeList)
            {
                string[] pair = resume.Split(delimCollon, StringSplitOptions.RemoveEmptyEntries);
                MembershipUser newUser = Membership.GetUser(pair[1].Trim ());//.CreateUser(m_member.PrimaryEmail, "changeme", m_member.PrimaryEmail);
                if (newUser != null)
                {
                    duplicateIds += pair[0] + ",";
                }
            }
            return duplicateIds;

        }
        [WebMethod(BufferResponse = true, Description = "Insert new employee Details")]
        [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Xml, XmlSerializeString = true)]
        public string ValidateWebService()
        {
            return "Success";
        }

        [WebMethod]
        public byte[] GetAllCategory()
        {
            IFacade facade = new Facade();
            IList<Category> cat = facade.GetAllCategory();
            Category SelectCategory = new Category();
            SelectCategory.Name = "Select";
            SelectCategory.Id = 0;
            cat.Insert(0, SelectCategory);
            return ConvertIListToByteArray(cat);
        }
        [WebMethod]
        public string GetServerDateTimeFormat()
        {
            return System.Threading.Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern;
        }


        [WebMethod]
        public int GetStateCountry(StateOrCountryDetails value)
        {
            int countryid = 0;
            int statecode = 0;
            if (value.Type == "State")
            {
                IFacade facade = new Facade();
                if (value.Code != null && value.CountryId != 0)
                {
                    statecode = facade.GetStateIdByStateNameAndCountryId(value.Code, value.CountryId);
                }

                return statecode;
            }
            else if (value.Type == "Country")
            {
                IFacade facade = new Facade();
                if (value.Code != null)
                {
                    try
                    {
                        countryid = facade.GetCountryIdByCountryName(value.Code);
                    }
                    catch
                    {
                    }
                }

                return countryid;
            }
            return 0;

        }

        [WebMethod]
        public int GetStateId(string value)
        {

            int statecode = 0;
            IFacade facade = new Facade();
            try
            {
                statecode = facade.GetStateIdByStateName(value);
                return statecode;
            }
            catch
            {
                return 0;
            }

            return statecode;
        }


        [WebMethod]
        public StateCountryById GetStateCountryById(StateCountryById value)
        {
            StateCountryById Result = new StateCountryById();
            if (value.Type == "State")
            {

                IFacade facade = new Facade();
                if (value.StateOrCountryId != 0)
                {
                    State stateList = facade.GetStateById(value.StateOrCountryId);
                    Result.StateOrCountry = ConvertObjectToByteArray(stateList);
                }

                return Result;
            }
            else if (value.Type == "Country")
            {
                IFacade facade = new Facade();
                if (value.StateOrCountryId != 0)
                {
                    Country country = facade.GetCountryById(value.StateOrCountryId);
                    Result.StateOrCountry = ConvertObjectToByteArray(country);
                }

                return Result;
            }
            return Result;
        }

        [WebMethod]
        public StateOrCountryDetails GetStateOrCountry(StateOrCountryDetails value)
        {
            StateOrCountryDetails Result = new StateOrCountryDetails();

            if (value.Type == "State")
            {

                IFacade facade = new Facade();
                if (value.CountryId != 0)
                {
                    if (value.Code == null)
                    {

                        IList<State> stateList = facade.GetAllStateByCountryId(value.CountryId);

                        Result.StateOrCountry = ConvertIListToByteArray(stateList);
                    }
                    else if (value.Code != null)
                    {
                        int statecode = facade.GetStateIdByStateCodeAndCountryId(value.Code, value.CountryId);
                        if (statecode != 0)
                        {
                            State stateList = facade.GetStateById(statecode);
                            Result.StateOrCountry = ConvertObjectToByteArray(stateList);
                        }
                    }
                }
                else
                {
                    if (value.Code != null && value.Code != string.Empty)
                    {
                        int stcode = facade.GetStateIdByStateCode(value.Code.Trim());
                        State stat = null;
                        if (stcode > 0) stat = facade.GetStateById(stcode);
                        Result.StateOrCountry = ConvertObjectToByteArray(stat);
                    }
                }

                return Result;
            }
            else if (value.Type == "Country")
            {
                IFacade facade = new Facade();
                if (value.Code == null)
                {
                    IList<Country> countryList = facade.GetAllCountry();

                    Result.StateOrCountry = ConvertIListToByteArray(countryList);
                }
                else
                {
                    int countryid = facade.GetCountryIdByCountryCode(value.Code);
                    if (countryid != 0)
                    {
                        Country country = facade.GetCountryById(countryid);
                        Result.StateOrCountry = ConvertObjectToByteArray(country);
                    }

                }

                return Result;
            }
            return Result;
        }
        [WebMethod]
        public LoginData GetMemberByUserName(LoginData value)
        {
            IFacade facade = new Facade();
            Member _Member = facade.GetMemberByUserName(value.UserName);
            IList<Member> MemList = new List<Member>();
            if (_Member != null) MemList.Add(_Member);

            LoginData LoginResult = new LoginData();
            if (MemList != null) LoginResult.LoginMember = ConvertIListToByteArray(MemList);
            return LoginResult;
        }

        [WebMethod]
        public LicenseData GetLicenseKeybyId(LicenseData license)
        {
            try
            {
                IFacade facade = new Facade();
                ArrayList licensearray = facade.GetLicenseKeybyId(license.LicenseKey, license.Domain);
                LicenseData LicenseResult = new LicenseData();
                LicenseResult.LicenseKey = license.LicenseKey;
                LicenseResult.Domain = license.Domain;
                LicenseResult.License = ConvertArrayListToByteArray(licensearray);
                return LicenseResult;
            }
            catch
            {
                return license;
            }
        }

        [WebMethod]
        public GetMember GetMemberbyId(GetMember value)
        {
            IFacade facade = new Facade();
            Member member = facade.GetMemberById(value.MemberId);
            IList<Member> memberList = new List<Member>();
            memberList.Add(member);

            GetMember MemberResult = new GetMember();
            MemberResult.MemberId = value.MemberId;
            MemberResult.MemberResult = ConvertIListToByteArray(memberList);
            return MemberResult;
        }

        [WebMethod]
        public DuplicateRecords GetDuplicateRecords(DuplicateRecords dup)
        {
            IFacade facade = new Facade();
            IList<DuplicateMember> Duplicate = new List<DuplicateMember>();
            Duplicate = facade.GetDuplicateRecords(dup.Name, dup.FirstName,
                              dup.Name, dup.MiddleName, dup.Name, dup.LastName,
                              dup.Email, dup.PrimaryEmail, dup.DOB, dup.DateOfBirth, dup.SSN, dup.strSSN);

            DuplicateRecords DupMemberResult = new DuplicateRecords();
            DupMemberResult.Datasource = ConvertIListToByteArray(Duplicate);
            return DupMemberResult;
        }


        [WebMethod]
        public GenericLookUp GetAllGenericLookupByLookupType(GenericLookUp value)
        {
            IFacade facade = new Facade();

            IList<LookupType> Lookup_Type = new List<LookupType>();
            ConvertByteArrayToIList(value.LookupId, out Lookup_Type);

            IList<GenericLookup> Generic_Lookup = new List<GenericLookup>();
            Generic_Lookup = facade.GetAllGenericLookupByLookupType(Lookup_Type[0]);

            GenericLookUp Lookup_Result = new GenericLookUp();
            Lookup_Result.LookupResult = ConvertIListToByteArray(Generic_Lookup);
            return Lookup_Result;
        }

        [WebMethod]
        public GenericLookUp GetAllGenericLookupByLookupTypeAndName(GenericLookUp value)
        {
            IFacade facade = new Facade();

            IList<LookupType> Lookup_Type = new List<LookupType>();
            ConvertByteArrayToIList(value.LookupId, out Lookup_Type);

            IList<GenericLookup> Generic_Lookup = new List<GenericLookup>();
            Generic_Lookup = facade.GetAllGenericLookupByLookupTypeAndName(Lookup_Type[0], value.LookupName);

            GenericLookUp Lookup_Result = new GenericLookUp();
            Lookup_Result.LookupResult = ConvertIListToByteArray(Generic_Lookup);
            return Lookup_Result;
        }



        [WebMethod]
        public JobPosting GetAllJobPostingByStatus(JobPosting value)
        {
            IFacade facade = new Facade();
            int JobStatusid = 0;
            IList<GenericLookup> RequisitionStatusList = facade.GetAllGenericLookupByLookupTypeAndName(LookupType.RequisitionStatus, JobStatus.Open.ToString());
            if (RequisitionStatusList != null)
            {
                JobStatusid = RequisitionStatusList[0].Id;
            }
            ArrayList jobid = facade.GetAllJobPostingByStatusAndManagerId(JobStatusid, value.CreatorId);
            JobPosting Result = new JobPosting();
            Result.JobId = ConvertArrayListToByteArray(jobid);
            return Result;
        }

        [WebMethod]
        public HotListDetails GetAllMemberGroupNameByManagerId(HotListDetails value)
        {
            IFacade facade = new Facade();
            ArrayList list = facade.GetAllMemberGroupNameByManagerId(value.ManagerId, value.GroupType);
            if (list != null) value.HotList = ConvertArrayListToByteArray(list);
            return value;

        }


        [WebMethod]
        public MemberShipDetails GetMemberShipUser(string uname, string pword, string domainname, string MacId, string AppName, int allowedusers)
        {
            IFacade facade = new Facade();
            MemberShipDetails result = new MemberShipDetails();
            MembershipUser user = Membership.GetUser( TPS360 .Web .UI.CryptorEngine .Decrypt ( uname,true ));
            //Line Commented by Prasanth on 10/Jun/2016
            //bool validuser = Membership.ValidateUser(TPS360.Web.UI.CryptorEngine.Decrypt(uname, true), TPS360.Web.UI.CryptorEngine.Decrypt(pword, true));
            //Code introduced by Prasanth on 10/Jun/2016 Start
            bool validuser;
            Member Mem = new Member();
            bool isldap;
            Mem = facade.GetMemberByUserName(TPS360.Web.UI.CryptorEngine.Decrypt(uname, true));
            if (Mem == null)
            {
                isldap = false;
            }
            else
            {
                isldap = Mem.IsLDAP;
            }

            if (isldap == true)
            {
                validuser = ActiveDirectoryConnector.IsUserLoggedIn(TPS360.Web.UI.CryptorEngine.Decrypt(uname, true), TPS360.Web.UI.CryptorEngine.Decrypt(pword, true));
            }
            else
            {
                validuser = Membership.ValidateUser(TPS360.Web.UI.CryptorEngine.Decrypt(uname, true), TPS360.Web.UI.CryptorEngine.Decrypt(pword, true));
            }
            //*******************END**************************
            if (user != null && validuser)
            {
                Member _member = facade.GetMemberByMemberEmail(user.Email.Trim());
                string RoleName = facade.GetCustomRoleNameByMemberId(_member.Id);
                if (RoleName.ToLower().Trim() != "candidate" && RoleName.ToLower().Trim() != "vendor" && RoleName != string.Empty)
                {
                    result.member = ConvertObjectToByteArray(user);
                    result.validuser = validuser;
                    int i = facade.EntryofUserAccessApp(TPS360.Web.UI.CryptorEngine.Decrypt(uname, true), "", AppName, domainname, MacId, allowedusers, Context.Request.UserHostName);
                    result.IsnumOfUserGreater = i;
                    result.Ip = Context.Request.UserHostName;
                }
                else
                {
                    result.member = ConvertObjectToByteArray(user);
                    result.validuser = false;
                }
            }

            return result;

        }

        [WebMethod]
        public void UpdateUserAccessApp(string uname, string AppName, string domainname)
        {
            IFacade facade = new Facade();
            facade.UpdateUserAccessApp(uname, AppName, domainname);
        }

        [WebMethod]
        public int DeleteUserAccessApp(string uname, string AppName, string domainname)
        {
            IFacade facade = new Facade();
            return facade.DeleteUserAccessApp(uname, AppName, domainname);
        }
        [WebMethod]
        public int InsertMemberSessionOutTime(string uname, int time, string AppName, string DomainName)
        {
            IFacade facade = new Facade();
            return facade.InsertSessionTimeout(uname, time, "", AppName, DomainName);
        }

        [WebMethod]
        public int GetMemberSessionOutTime(string uname, string AppName, string DomainName)
        {
            IFacade facade = new Facade();
            return facade.GetSessionTimeOutValue(uname, AppName, DomainName);
        }

        [WebMethod]
        public GetDetailsForLocalDB GetLocalDBDetails()
        {
            IFacade facade = new Facade();
            GetDetailsForLocalDB localdb = new GetDetailsForLocalDB();
            IList<State> st = facade.GetAllState();
            localdb.State = ConvertIListToByteArray(st);
            IList<Country> cou = facade.GetAllCountry();
            localdb.Country = ConvertIListToByteArray(cou);
            IList<GenericLookup> gl = facade.GetAllGenericLookup();
            localdb.GenericLookUp = ConvertIListToByteArray(gl);

            return localdb;
        }

        [WebMethod]
        public PartialMemberDetails GetPartialUserDetails(string email, string AppName, string Domain)
        {
            IFacade facade = new Facade();
            Member _Member = facade.GetMemberByUserName(email);
            PartialMemberDetails pmd = new PartialMemberDetails();
            pmd.MemberId = _Member.Id;
            pmd.firstname = _Member.FirstName;
            pmd.lastname = _Member.LastName;
            pmd.userIdleTimeValue = facade.GetSessionTimeOutValue(email, AppName, Domain);
            pmd.ServerDateTimeFormat = System.Threading.Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern;

            ArrayList jobid = facade.GetAllJobPostingByStatusAndManagerId(0, _Member.Id);
            pmd.JobPosting = ConvertArrayListToByteArray(jobid);
            ArrayList list = facade.GetAllMemberGroupNameByManagerId(_Member.Id, 1);
            pmd.MemberGroup = ConvertArrayListToByteArray(list);
            try
            {
                ArrayList Company = new ArrayList();
                SiteSetting sitesetting = facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                if (sitesetting != null)
                {
                    Hashtable config =TPS360 .Common.Helper .ObjectEncrypter.Decrypt<Hashtable>(sitesetting.SettingConfiguration);
                    int status = (int)TPS360.Web.UI.MiscUtil.GetCompanyStatusFromDefaultSiteSetting(config);
                    if (status != null && status > 0)
                        Company = facade.GetAllClientsByStatus(status);
                }
                if(Company ==null)Company = facade.GetAllCompanyList ();
                pmd.Company = ConvertArrayListToByteArray(Company);
            }
            catch (Exception ex) { }
            return pmd;
        }


        public partial class PartialMemberDetails
        {
            public int MemberId;
            public string firstname;
            public string lastname;
            public int userIdleTimeValue;
            public string ServerDateTimeFormat;

            public byte[] JobPosting;
            public byte[] MemberGroup;
            public byte[] Company;
        }

        public partial class GetDetailsForLocalDB
        {
            public byte[] State;
            public byte[] Country;
            public byte[] GenericLookUp;


        }




        public struct MemberShipDetails
        {
            public byte[] member;
            public bool validuser;
            public int IsnumOfUserGreater; public string Ip;
        }

        public byte[] ConvertObjectToByteArray<T>(T obj)
        {
            BinaryFormatter binary = new BinaryFormatter();
            MemoryStream mem = new MemoryStream();
            if (obj != null)
            {
                binary.Serialize(mem, obj);
            }
            byte[] byteResult = mem.ToArray();
            mem.Close();
            return byteResult;
        }
        public void ConvertByteArrayToObject<T>(byte[] byteArray, out T obj)
        {

            AppDomain currentDomain = AppDomain.CurrentDomain;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream(byteArray))
            {
                obj = (T)bf.Deserialize(ms);
            }
        }

        private byte[] ConvertArrayListToByteArray(ArrayList obj)
        {
            BinaryFormatter binary = new BinaryFormatter();
            MemoryStream mem = new MemoryStream();
            if (obj != null)
            {
                binary.Serialize(mem, obj);
            }
            byte[] byteResult = mem.ToArray();
            mem.Close();
            return byteResult;
        }


        public ArrayList ConvertByteArrayToArrayList(byte[] byteArray)
        {
            ArrayList obj;
            AppDomain currentDomain = AppDomain.CurrentDomain;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream(byteArray))
            {
                obj = (ArrayList)bf.Deserialize(ms);
            }
            return obj;
        }
        public byte[] ConvertIListToByteArray<T>(IList<T> obj)
        {
            BinaryFormatter binary = new BinaryFormatter();
            MemoryStream mem = new MemoryStream();
            if (obj != null)
            {
                binary.Serialize(mem, obj);
            }
            byte[] byteResult = mem.ToArray();
            mem.Close();
            return byteResult;
        }
        public void ConvertByteArrayToIList<T>(byte[] byteArray, out IList<T> obj)
        {

            AppDomain currentDomain = AppDomain.CurrentDomain;
            BinaryFormatter bf = new BinaryFormatter();

            using (MemoryStream ms = new MemoryStream(byteArray))
            {
                obj = (IList<T>)bf.Deserialize(ms);
            }
        }


        public struct LoginData
        {
            public string UserName;
            public byte[] LoginMember;
        }


        public struct GetMember
        {
            public int MemberId;
            public byte[] MemberResult;
        }

        public struct LicenseData
        {
            public string LicenseKey;
            public string Domain;
            public byte[] License;
        }

        public struct DuplicateRecords
        {

            public bool Name;
            public string FirstName;
            public string LastName;
            public string MiddleName;
            public bool Email;
            public string PrimaryEmail;
            public bool DOB;
            public DateTime DateOfBirth;
            public bool SSN;
            public string strSSN;
            public byte[] Datasource;

        }

        public struct GenericLookUp
        {
            public string LookupName;
            public byte[] LookupId;
            public byte[] LookupResult;
        }

        public struct JobPosting
        {
            public int CreatorId;
            public byte[] JobId;
        }

        public struct StateOrCountryDetails
        {
            public string Type;
            public int CountryId;
            public string Code;
            public byte[] StateOrCountry;
        }
        public struct StateCountryById
        {
            public string Type;
            public int StateOrCountryId;
            public byte[] StateOrCountry;
        }

        public struct HotListDetails
        {
            public int ManagerId;
            public int GroupType;
            public byte[] HotList;
        }

    }



}