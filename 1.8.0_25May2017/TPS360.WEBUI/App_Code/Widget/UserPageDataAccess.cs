using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System;

public class UserPage
{
    private int _id;
    private int _userId;
    private string _title;
    private int _layoutType;

    public int Id
    {
        get
        {
            return _id;
        }
        set
        {
            _id = value;
        }
    }

    public string Title
    {
        get
        {
            return _title;
        }
        set
        {
            _title = value;
        }
    }

    public int UserId
    {
        get
        {
            return _userId;
        }
        set
        {
            _userId = value;
        }
    }

    public int LayoutType
    {
        get
        {
            return _layoutType;
        }
        set
        {
            _layoutType = value;
        }
    }
}

public partial class UserPageDataAccess
{
    public bool AddUserPage(UserPage page)
    {
        const string SQL = "INSERT INTO [UserPage]([Title], [UserId], [LayoutType], [CreateDate], [UpdateDate])               " +
                            "VALUES (@p1, @p2, @p3, @p4, @p5)" +
                            "SELECT [Id] FROM [UserPage] where [Id]= @@IDENTITY";
        
        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", page.Title));
                cmd.Parameters.Add(new SqlParameter("@p2", page.UserId));
                cmd.Parameters.Add(new SqlParameter("@p3", page.LayoutType));
                cmd.Parameters.Add(new SqlParameter("@p4", DateTime.Now));
                cmd.Parameters.Add(new SqlParameter("@p5", DateTime.Now));

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        page.Id = rdr.GetInt32(0);
                    }
                }
            }
        }

        return false;
    }

    public UserPage GetUserPage(int Id)
    {
        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = "SELECT [Id], [Title], [UserId] FROM [UserPage] WHERE [UserPage].[Id] = @Id";
                cmd.Parameters.Add(new SqlParameter("@Id", Id));
                
                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        return BuildUserPage(rdr);
                    }
                }
            }
        }

        return null;
    }

    public UserPage GetTopUserPage(int userId)
    {
        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = "SELECT TOP 1 [Id], [Title], [UserId] FROM [UserPage] WHERE [UserId] = @UserId ";
                cmd.Parameters.Add(new SqlParameter("@UserId", userId));

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        return BuildUserPage(rdr);
                    }
                }
            }
        }

        return null;
    }

    public int GetUserPageCount(int userId)
    {
        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = "SELECT Count([Id]) FROM [UserPage] WHERE [UserPage].[UserId] = @UserId";
                cmd.Parameters.Add(new SqlParameter("@UserId", userId));

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        return rdr.IsDBNull(0) ? int.MinValue : rdr.GetInt32(0);
                    }
                }
            }
        }

        return 0;
    }

    public List<UserPage> GetAllUserPagesByUserId(int userId)
    {
        List<UserPage> list = new List<UserPage>();

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = "SELECT [Id], [Title], [UserId] FROM [UserPage] WHERE [UserPage].[UserId] = @userId ORDER BY [Id] ASC";
                cmd.Parameters.Add(new SqlParameter("@userId", userId));

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        UserPage c = new UserPage();

                        list.Add(BuildUserPage(rdr));
                    }
                }
            }
        }

        if (list.Count == 0)
        {
            return null;
        }

        return list;
    }

    public bool UpdateUserPageTitle(int pageId, string title)
    {
        const string SQL = "UPDATE [UserPage]              " +
                            "SET       [Title] = @p1  " +
                            "WHERE                          " +
                            "       [Id] = @p2";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", title));
                cmd.Parameters.Add(new SqlParameter("@p2", pageId));

                if (cmd.ExecuteNonQuery() > -1)
                {
                    return true;
                }
            }
        }
        return false;

    }

    public bool DeleteUserPage(int pageId)
    {
        //delete all widget instance of the page first

        new WidgetInstanceDataAccess().DeleteWidgetInstanceByPageId(pageId);

        const string SQL = "DELETE [UserPage]       " +
                            "WHERE                   " +
                            "       [Id] = @P1";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", pageId));

                if (cmd.ExecuteNonQuery() > -1)
                {
                    return true;                    
                }
            }
        }

        return false;
    }

    private static UserPage BuildUserPage(IDataReader reader)
    {
        UserPage userPage = new UserPage();

        userPage.Id = reader.IsDBNull(0) ? int.MinValue : reader.GetInt32(0);
        userPage.Title = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
        userPage.UserId = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);

        return userPage;
    }

    private static IDbConnection CreateConnection()
    {
        IDbConnection cnn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["TPS360CS"].ConnectionString);
        cnn.Open();

        return cnn;
    }
}