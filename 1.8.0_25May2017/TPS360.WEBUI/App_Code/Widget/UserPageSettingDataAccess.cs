using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public class UserPageSetting
{
    private int _currentPageId;
    private int _userId;
    
    public int CurrentPageId
    {
        get
        {
            return _currentPageId;
        }
        set
        {
            _currentPageId = value;
        }
    }

    public int UserId
    {
        get
        {
            return _userId;
        }
        set
        {
            _userId = value;
        }
    }
}

public partial class UserPageSettingDataAccess
{
    public bool AddUserPageSetting(UserPageSetting pageSetting)
    {
        const string SQL = "INSERT INTO [UserPageSetting]([UserId], [CurrentPageId])               " +
                            "VALUES (@p1, @p2)";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p2", pageSetting.CurrentPageId));
                cmd.Parameters.Add(new SqlParameter("@p1", pageSetting.UserId));                

                if (cmd.ExecuteNonQuery() > -1)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public bool UpdateUserPageSetting(UserPageSetting page)
    {
        const string SQL = "UPDATE [UserPageSetting]              " +
                            "SET       [CurrentPageId]    = @p1  " +
                            "WHERE                          " +
                            "       [UserId] = @p2";

        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = SQL;
                cmd.Parameters.Add(new SqlParameter("@p1", page.CurrentPageId));
                cmd.Parameters.Add(new SqlParameter("@p2", page.UserId));                
                
                if (cmd.ExecuteNonQuery() > -1)
                {
                    return true;
                }
            }
        }
        return false;

    }

    public UserPageSetting GetUserPageSetting(int userId)
    {
        using (IDbConnection cnn = CreateConnection())
        {
            using (IDbCommand cmd = cnn.CreateCommand())
            {
                cmd.CommandText = "SELECT [UserId], [CurrentPageId] FROM [UserPageSetting] WHERE [UserPageSetting].[UserId] = @UserId";
                cmd.Parameters.Add(new SqlParameter("@UserId", userId));
                
                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        return BuildUserPageSetting(rdr);
                    }
                }
            }
        }

        return null;
    }

    private static UserPageSetting BuildUserPageSetting(IDataReader reader)
    {
        UserPageSetting userPageSetting = new UserPageSetting();

        userPageSetting.UserId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
        userPageSetting.CurrentPageId = reader.IsDBNull(1) ? int.MinValue : reader.GetInt32(1);

        return userPageSetting;
    }

    private static IDbConnection CreateConnection()
    {
        IDbConnection cnn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["TPS360CS"].ConnectionString);
        cnn.Open();

        return cnn;
    }
}