﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:AutoMail.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.     Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1         21/Oct/2016         Prasanth Kumar G    Introduced Amazonses (email sending service)
 
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

namespace AutoMail
{
    using System;
    using System.Collections;
    using System.Linq;
    using System.Web;
    using System.Web.Services;
    using System.Web.Services.Protocols;
    using System.Xml.Linq;
    using System.ComponentModel;
    using System.Data;
    using System.Diagnostics;
    using System.Net.Mail;
    using System.Timers;
    using TPS360.Common.BusinessEntities;
    using TPS360.BusinessFacade;
    using TPS360.Controls;
    using TPS360.Common.Shared;
    using TPS360.Common.Helper;
    using TPS360.Common.Utility;
    using System.Collections.Generic;
    using System.Text;
    using System.Text.RegularExpressions;
    using TPS360.Web.UI.Helper;
    using System.IO;
    using TPS360.Web.UI;
    using System.Web.Configuration;
    /// <summary>
    /// Summary description for MailSendingWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MailSendingWebService : System.Web.Services.WebService
    {
        IFacade Facade = new Facade();
        public string strUser = string.Empty;
        public string strSMTP = string.Empty;
        public string strssl = string.Empty;
        public string strPwd = string.Empty;
        public int intPort = 25;
        public bool sentStatus = false;
        public bool CheckingStatus = false;
        System.Timers.Timer time = new System.Timers.Timer();
        public MailSendingWebService()
        {

        }

        public bool SendEmail(string From, string server, int port, string pass, string strssl, string ReceiverEmail, string MailBody, string CandidateName, int CandidateId, string Subject, int SenderId, int JobPostingId)
        {
            bool flag = false;
            MailMessage mailMsg = new MailMessage();
            MailAddress mailAddress = null;
            int count = 0;
            try
            {
                mailMsg.To.Add(ReceiverEmail);
                mailAddress = new MailAddress(From);
                mailMsg.From = mailAddress;
                mailMsg.Subject = Subject;
                MailBody = MailBody.Replace("&lt;candidate_first_name&gt;", CandidateName);
                MailBody = MailBody.Replace("/ATS/UnSubscribe.aspx", "/ATS/UnSubscribe.aspx?Id=" + CandidateId);
                mailMsg.Body = MailBody;
                mailMsg.IsBodyHtml = true;
                SmtpClient smtpClient = new SmtpClient(server, port);
                smtpClient.UseDefaultCredentials = false;
                smtpClient.EnableSsl = strssl.ToLower() == "true" ? true : false;
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(From, pass);
                smtpClient.Credentials = credentials;
                smtpClient.Send(mailMsg);
                flag = true;
            }

            catch (Exception ex)
            {
            }

            finally
            {

                mailMsg = null;

                mailAddress = null;

            }
            if (flag)
            {
                SaveMemberEmail(strUser, SenderId, ReceiverEmail, CandidateId, Subject, MailBody, JobPostingId);

            }
            return flag;

        }

        private bool AutoMailSearchAgent(MemberExtendedInformation MailSetting, string ReceiverEmail, string CandidateName, int CandidateId, string MailBody, string Subject, int SenderId, int JobPostingId)
        {
            try
            {
                Hashtable MailSettingTable = null;
                MailSettingTable = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);
                strSMTP = MailSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();
                strUser = MailSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                strPwd = MailSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                strssl = MailSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString();
                if (strSMTP != null && strSMTP != "")
                {
                    if (MailSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != null && MailSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != "")
                    {
                        try
                        {
                            intPort = Convert.ToInt32(MailSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString());
                        }
                        catch (FormatException)
                        {
                        }
                    }
                }
                sentStatus = SendEmail(strUser, strSMTP, intPort, strPwd, strssl, ReceiverEmail, MailBody, CandidateName, CandidateId, Subject, SenderId, JobPostingId);
            }
            catch (Exception ex)
            {
            }
            return sentStatus;
        }

        private void SaveMemberEmail(string SenderEmail, int SenderId, string ReceiverEmail, int ReceiverId, string Subject, string Body, int JobPostingId)
        {
            if (true) // Isvalid
            {
                try
                {
                    MemberEmail memberEmail = BuildMemberEmail(SenderEmail, SenderId, ReceiverEmail, ReceiverId, Subject, Body);
                    memberEmail = Facade.AddMemberEmail(memberEmail);
                    if (memberEmail != null)
                    {
                        SaveMemberEmailDetail(memberEmail, SenderId, SenderEmail);
                        SearchAgentMailToCandidate candidate = new SearchAgentMailToCandidate();
                        candidate.JobPostingId = JobPostingId;
                        candidate.MemberEmailId = memberEmail.Id;
                        candidate.MemberId = ReceiverId;
                        Facade.AddCandidateForMail(candidate);
                        SearchAgentSchedule schedule = new SearchAgentSchedule();
                        schedule = Facade.GetSearchAgentScheduleByJobPostingId(JobPostingId);
                        if (schedule != null)
                        {
                            if (schedule.Repeat == "1")
                            {
                                schedule.LastSentDate = DateTime.Now;
                                schedule.NextSendDate = DateTime.Now.AddDays(1);
                            }
                            else if (schedule.Repeat == "7")
                            {
                                schedule.LastSentDate = DateTime.Now;
                                schedule.NextSendDate = DateTime.Now.AddDays(7);
                            }
                            else if (schedule.Repeat == "0")
                                schedule.LastSentDate = DateTime.Now;
                            schedule = Facade.UpdateSearchAgentSchedule(schedule);
                        }
                    }
                }
                catch (ArgumentException ex)
                {

                }

            }
        }

        private MemberEmail BuildMemberEmail(string SenderEmail, int SenderId, string ReceiverEmail, int ReceiverId, string Subject, string Body)
        {
            MemberEmail memberEmail = new MemberEmail();
            try
            {
                memberEmail.SenderId = SenderId;
                memberEmail.SenderEmail = SenderEmail.Trim();
                memberEmail.ReceiverId = ReceiverId;
                memberEmail.ReceiverEmail = ReceiverEmail;
                memberEmail.EmailTypeLookupId = (int)EmailType.Sent;
                memberEmail.ParentId = 0;
                memberEmail.Subject = Subject;
                memberEmail.EmailBody = Body;
                memberEmail.Status = 0;
                memberEmail.IsRemoved = false;
                memberEmail.CreatorId = memberEmail.UpdatorId = SenderId;
                memberEmail.SentDate = DateTime.Now.ToString();
                memberEmail.NoOfAttachedFiles = 0;
                memberEmail.AttachedFileNames = string.Empty;
            }
            catch
            {
            }
            return memberEmail;
        }

        private void SaveMemberEmailDetail(MemberEmail memberEmail, int MemberId, string To)
        {
            MemberEmailDetail memberEmailDetail = new MemberEmailDetail();
            try
            {
                memberEmailDetail.MemberId = MemberId;
                memberEmailDetail.SendingTypeId = (int)EmailSendingType.Broadcast;
                memberEmailDetail.MemberEmailId = memberEmail.Id;
                memberEmailDetail.CreatorId = MemberId;
                memberEmailDetail.AddressTypeId = (int)EmailAddressType.To;
                memberEmailDetail.EmailAddress = To.Trim();
                memberEmailDetail = Facade.AddMemberEmailDetail(memberEmailDetail);
            }
            catch
            {
            }

        }

        private string RemovePrePostComma(string value)
        {
            if (value.StartsWith(","))
                value = value.Remove(0, 1);
            if (value.EndsWith(","))
                value = value.Remove(value.Length - 1, 1);
            return value;
        }

        private string ReturnEmpty(string value)
        {
            if (value != "0")
                return value;
            else
                return string.Empty;
        }

        private string getOrKeyword(string keyword)
        {
            char[] delimiters = new char[] { ',', '\n' };
            string[] key = keyword.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            string result = string.Empty;
            if (keyword.StartsWith(",")) result = "|";
            string lastword = string.Empty;
            for (int i = 0; i < key.Length; i++)
            {
                if (key[i].ToLower() != "or")
                {
                    if (key[i].ToLower() == ("not") || key[i].Contains("-"))
                    {
                        string s = string.Empty; s = getNotKeyword(key[i]);
                        if (s.StartsWith("&!"))
                        {
                            result = (result.EndsWith("&")) ? result.Substring(0, result.Length - 1) : result;
                            result = (result.EndsWith("|")) ? result.Substring(0, result.Length - 1) : result;
                            result = (result.EndsWith("&!")) ? result.Substring(0, result.Length - 2) : result;
                            result += s;
                        }
                        else result += s;
                    }
                    else result += key[i];
                    if (!result.EndsWith("&") && !result.EndsWith("|") && !result.EndsWith("&!") && result != string.Empty)
                        result += "|";
                }
            }
            return result == string.Empty ? "|" : result;
        }

        private string getNotKeyword(string keyword)
        {
            char[] delimiters = new char[] { '-', '\n' };
            string[] key = keyword.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            string result = string.Empty;
            if (keyword.StartsWith("&!")) result = "&!";
            string lastword = string.Empty;
            for (int i = 0; i < key.Length; i++)
            {
                if (key[i].ToLower() != "not")
                {
                    result += key[i];
                    if (!result.EndsWith("&") && !result.EndsWith("|") && !result.EndsWith("&!"))
                        result += "&!";
                }
            }
            return result == string.Empty ? "&!" : result;
        }

        private string BuildKeyWords(string keyword)
        {
            char[] delimiters = new char[] { ' ', '\n' };
            string[] key = keyword.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            string result = string.Empty;
            string lastword = string.Empty;
            for (int i = 0; i < key.Length; i++)
            {
                if (key[i].EndsWith("*")) key[i] = "\"" + key[i] + "\"";
                if (key[i].ToLower() != "and")
                {
                    if (key[i].ToLower() == ("not") || key[i].Contains("-") || key[i].ToLower() == ("or") || key[i].Contains(","))
                    {
                        string s = string.Empty; s = getOrKeyword(key[i]);
                        if (s.StartsWith("|") || s.StartsWith("&!"))
                        {
                            result = (result.EndsWith("&")) ? result.Substring(0, result.Length - 1) : result;
                            result = (result.EndsWith("|")) ? result.Substring(0, result.Length - 1) : result;
                            result = (result.EndsWith("&!")) ? result.Substring(0, result.Length - 2) : result;
                            result += s;
                        }
                        else result += s;
                    }
                    else result += key[i];
                    if (!result.EndsWith("&") && !result.EndsWith("|") && !result.EndsWith("&!"))
                        result += "&";
                }


            }
            result = (result.EndsWith("&")) ? result.Substring(0, result.Length - 1) : result;
            result = (result.EndsWith("|")) ? result.Substring(0, result.Length - 1) : result;
            result = (result.EndsWith("&!")) ? result.Substring(0, result.Length - 2) : result;
            result = (result.StartsWith("&")) ? result.Substring(1, result.Length - 1) : result;
            result = (result.StartsWith("|")) ? result.Substring(1, result.Length - 1) : result;
            result = (result.StartsWith("!")) ? result.Substring(1, result.Length - 1) : result;
            return result;
        }

        [WebMethod]
        public bool MailToSearchAgent()
        {
            IList<AutoMailSending> agent = new List<AutoMailSending>();
            agent = Facade.GetSearchDetails();
            if (agent != null)
            {
                foreach (AutoMailSending mail in agent)
                {

                    MemberExtendedInformation MailSetting = new MemberExtendedInformation();
                    MailSetting = Facade.GetMemberExtendedInformationByMemberId(mail.SenderId);
                    if (MailSetting.MailSetting != null)
                    {
                        string salaryType = string.Empty;
                        if (mail.SalaryFrom != string.Empty && mail.SalaryTo != string.Empty)
                            salaryType = mail.SalaryType;
                        IList<Candidate> candidate = new List<Candidate>();
                        try
                        {
                            candidate = Facade.GetAllCandidateOnSearchForSearchAgent(mail.KeyWord != string.Empty ? BuildKeyWords(mail.KeyWord.Replace("&", "").Replace("|", "").Replace("!", "")).Replace("'", "\"") : string.Empty, mail.JobTitle, mail.City, ReturnEmpty(mail.CountryId.ToString()), ReturnEmpty(mail.EmployementTypeLookUpId.ToString()), RemovePrePostComma(mail.WorkStatusLookUpId.ToString()), mail.SecurityClearance == true ? "1" : "0", mail.AvailableAfter == DateTime.MinValue ? string.Empty : mail.AvailableAfter.ToShortDateString(), ReturnEmpty(mail.ResumeLastUpdated), salaryType, mail.SalaryFrom, mail.SalaryTo, ReturnEmpty(mail.CurrencyLookUpId.ToString()), ReturnEmpty(mail.HotListId.ToString()), mail.MinExperience, mail.MaxExperience, ReturnEmpty(mail.StateId.ToString()), 0, ReturnEmpty(mail.CandidateTypeLookUpId.ToString()), RemovePrePostComma(mail.EducationLookupId.ToString()), ReturnEmpty(mail.WorkScheduleLookupId.ToString()), RemovePrePostComma(mail.HiringStatusLookUpId.ToString()), mail.JobPostingId);
                            if (candidate != null)
                            {
                                foreach (Candidate can in candidate)
                                {
                                    CheckingStatus = AutoMailSearchAgent(MailSetting, can.PrimaryEmail, can.FirstName, can.Id, mail.EmailBody, mail.Subject, mail.SenderId, mail.JobPostingId);
                                }
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }
            return CheckingStatus;
        }
        private string getTargetPath(int memberId, int MemberEmailID)
        {
            return UrlConstants.MemberSignatureDirectory + "\\" + memberId + "\\" + "Outlook Sync" + "\\" + MemberEmailID;
        }
        private string getSourcePath(int mailQueueId)
        {
            return UrlConstants.EMAIL_UPLOADEDFILES_DIR + "\\" + mailQueueId;
        }
        private string getAttachementPath(int mailQueueId, string fileName)
        {
            return UrlConstants.EMAIL_UPLOADEDFILES_DIR + "\\" + mailQueueId + "\\" + fileName;
        }
        private static readonly object _locker = new object();

        private static readonly object _MailsendingServiceLocker = new object(); //Code introduced by Prasanth on 21/Oct/2016
        [WebMethod]
        public void CheckMailQueue()
        {
            System.Threading.Thread t1 = new System.Threading.Thread(SendMails);
            t1.Start();
        }
        public void SendMails()
        {
            System.Threading.Monitor.Enter(_locker);
            try
            {
                IList<MailQueue> mailQueue = new List<MailQueue>();
                mailQueue = Facade.GetAllMailInQueue();
                string ApplicationSource = WebConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower();
                string sourcePath = "";
                string targetPath = "";
                string senderEmailId = "";
                string sendStatus = "";
                string filename = "";
                string targetFile = "";
                List<Employee> EmployeeList = new List<Employee>();
                string[] attachment = null;
                string CandidateName = "";
                string strOne = "";
                string strTwo = "";
                string strThree = "";
                if (mailQueue == null)
                {
                    return;
                }
                foreach (MailQueue Email in mailQueue)
                {
                    EmailHelper emailManager = new EmailHelper();
                    emailManager.Subject = Email.Subject;
                    emailManager.To.Clear();
                    senderEmailId = Facade.GetMemberUserNameById(Email.SenderId);
                    emailManager.From = senderEmailId;
                    emailManager.To.Add(Email.ReceiverEmailId);
                    emailManager.Bcc.Add(Email.bcc);
                    emailManager.Cc.Add(Email.cc);
                    attachment = Email.AttachedFileNames.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < attachment.Count(); i++)
                    {
                        emailManager.Attachments.Add(getAttachementPath(Email.Id, attachment[i]));
                    }
                    emailManager.Attachments.Add(Email.AttachedFileNames);

                    emailManager.Body = Email.EmailBody;

                    int senderId = Email.SenderId;

                    if (ApplicationSource == "landt" && Email.Subject == "L&T IES - Feedback")
                    {
                        ArrayList EmpList = Facade.GetEmployeeNameAndContactNumber(Email.SenderId);
                        string MailBody = Email.EmailBody;
                        CandidateName = Facade.GetCandidateNameByEmailId(Email.ReceiverEmailId);


                        if (MailBody.Contains("CANDIDATENAME"))
                            strOne = Regex.Replace(MailBody, "CANDIDATENAME", CandidateName, RegexOptions.IgnoreCase);


                        foreach (Employee emp in EmpList)
                        {
                            if (strOne.Contains("EMPLOYEENAME"))
                            {
                                strTwo = Regex.Replace(strOne, "EMPLOYEENAME", emp.FirstName, RegexOptions.IgnoreCase);
                                strThree = Regex.Replace(strTwo, "EMPLOYEECONTACT", emp.PrimaryPhone, RegexOptions.IgnoreCase);
                            }

                        }

                        emailManager.Body = strThree;
                    }

                    if (senderId != 0)
                    {
                        sendStatus = emailManager.Send(Email.SenderId);
                    }
                    else
                    {
                        sendStatus = emailManager.Send();
                    }

                    if (sendStatus == "1")
                    {

                        int MemberEmailId;
                        bool status = Facade.DeleteMailQueueById(Email.Id, out MemberEmailId);
                        if (ApplicationSource == "landt" && strThree != "")
                        {
                            Facade.UpdateMailbodyForInterviewTemplate(strThree, MemberEmailId);
                        }


                        if (status == true)
                        {
                            sourcePath = getSourcePath(Email.Id);
                            targetPath = getTargetPath(Email.SenderId, MemberEmailId);

                            if (!Directory.Exists(targetPath) && Directory.Exists(sourcePath))
                            {
                                Directory.CreateDirectory(targetPath);

                                string[] files = Directory.GetFiles(sourcePath);

                                foreach (string filePath in files)
                                {
                                    filename = Path.GetFileName(filePath);
                                    targetFile = Path.Combine(targetPath, filename);

                                    try
                                    {
                                        File.Move(filePath, targetFile);
                                    }
                                    catch { }


                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exe)
            {
                BasePage.writeLog(exe);
            }
            finally
            {
                System.Threading.Monitor.Exit(_locker);
            }
        }



        //Code introduced by Prasanth on 21/Oct/2016 Start
        [WebMethod]
        public void ExternalMailsendingService_CheckMailQueue()
        {
            System.Threading.Thread t1 = new System.Threading.Thread(ExternalMailsendingService_SendMails);
            t1.Start();
        }
        public void ExternalMailsendingService_SendMails()
        {
            System.Threading.Monitor.Enter(_MailsendingServiceLocker);
            try
            {
                IList<MailQueue> mailQueue = new List<MailQueue>();
                mailQueue = Facade.GetAllMailInQueue();
                string ApplicationSource = WebConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower();
                string sourcePath = "";
                string targetPath = "";
                string senderEmailId = "";
                string sendStatus = "";
                string filename = "";
                string targetFile = "";
                List<Employee> EmployeeList = new List<Employee>();
                string[] attachment = null;
                string CandidateName = "";
                string strOne = "";
                string strTwo = "";
                string strThree = "";
                if (mailQueue == null)
                {
                    return;
                }
                foreach (MailQueue Email in mailQueue)
                {
                    EmailHelper emailManager = new EmailHelper();
                    emailManager.Subject = Email.Subject;
                    emailManager.To.Clear();
                    senderEmailId = Facade.GetMemberUserNameById(Email.SenderId);
                    emailManager.From = senderEmailId;
                    emailManager.To.Add(Email.ReceiverEmailId);
                    emailManager.Bcc.Add(Email.bcc);
                    emailManager.Cc.Add(Email.cc);
                    attachment = Email.AttachedFileNames.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < attachment.Count(); i++)
                    {
                        emailManager.Attachments.Add(getAttachementPath(Email.Id, attachment[i]));
                    }
                    emailManager.Attachments.Add(Email.AttachedFileNames);

                    emailManager.Body = Email.EmailBody;

                    int senderId = Email.SenderId;

                    if (ApplicationSource == "landt" && Email.Subject == "L&T IES - Feedback")
                    {
                        ArrayList EmpList = Facade.GetEmployeeNameAndContactNumber(Email.SenderId);
                        string MailBody = Email.EmailBody;
                        CandidateName = Facade.GetCandidateNameByEmailId(Email.ReceiverEmailId);


                        if (MailBody.Contains("CANDIDATENAME"))
                            strOne = Regex.Replace(MailBody, "CANDIDATENAME", CandidateName, RegexOptions.IgnoreCase);


                        foreach (Employee emp in EmpList)
                        {
                            if (strOne.Contains("EMPLOYEENAME"))
                            {
                                strTwo = Regex.Replace(strOne, "EMPLOYEENAME", emp.FirstName, RegexOptions.IgnoreCase);
                                strThree = Regex.Replace(strTwo, "EMPLOYEECONTACT", emp.PrimaryPhone, RegexOptions.IgnoreCase);
                            }

                        }

                        emailManager.Body = strThree;
                    }

                    //if (senderId != 0)
                    //{
                    sendStatus = emailManager.AmezonSend(Email.SenderId);
                    //sendStatus = AmazonMailSending(emailManager);
                    //}

                    //else
                    //{
                    //    sendStatus = emailManager.Send();

                    //}
                    //sendStatus == "1";
                    if (sendStatus == "1")
                    {

                        int MemberEmailId;
                        bool status = Facade.DeleteMailQueueById(Email.Id, out MemberEmailId);
                        if (ApplicationSource == "landt" && strThree != "")
                        {
                            Facade.UpdateMailbodyForInterviewTemplate(strThree, MemberEmailId);
                        }


                        if (status == true)
                        {
                            sourcePath = getSourcePath(Email.Id);
                            targetPath = getTargetPath(Email.SenderId, MemberEmailId);

                            if (!Directory.Exists(targetPath) && Directory.Exists(sourcePath))
                            {
                                Directory.CreateDirectory(targetPath);

                                string[] files = Directory.GetFiles(sourcePath);

                                foreach (string filePath in files)
                                {
                                    filename = Path.GetFileName(filePath);
                                    targetFile = Path.Combine(targetPath, filename);

                                    try
                                    {
                                        File.Move(filePath, targetFile);
                                    }
                                    catch { }


                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exe)
            {
                BasePage.writeLog(exe);
            }
            finally
            {
                System.Threading.Monitor.Exit(_MailsendingServiceLocker);
            }
        }

        //**********************END******************************


        //private string AmazonMailSending(EmailHelper emailManager)
        //{
        //    FROM = "prasanth@talentrackr.com";   // Replace with your "From" address. This address must be verified.            
        //    TO = "sumit.s@talentrackr.com";  // Replace with a "To" address. If your account is still in the                                                        // sandbox, this address must be verified.
        //    SUBJECT = "Amazon SES test (SMTP interface accessed using C#)";
        //    BODY = "This email was sent through the Amazon SES SMTP interface by using C#.";

        //    // Supply your SMTP credentials below. Note that your SMTP credentials are different from your AWS credentials.            
        //    const String SMTP_USERNAME = "AKIAJVKI7WVPBD7X67EA";  // Replace with your SMTP username.             
        //    const String SMTP_PASSWORD = "AoQ3k8IiDvdaST94l8MNZjCEP5q8irh8iMsDZNYInWcE";  // Replace with your SMTP password.

        //    // Amazon SES SMTP host name. This example uses the US West (Oregon) region.            
        //    const String HOST = "email-smtp.us-west-2.amazonaws.com";

        //    // The port you will connect to on the Amazon SES SMTP endpoint. We are choosing port 587 because we will use            
        //    // STARTTLS to encrypt the connection.            
        //    const int PORT = 587;

        //    // Create an SMTP client with the specified host name and port.            
        //    System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(HOST, PORT);
        //    // Create a network credential with your SMTP user name and password.                
        //    client.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
        //    // Use SSL when accessing Amazon SES. The SMTP session will begin on an unencrypted connection, and then                
        //    // the client will issue a STARTTLS command to upgrade to an encrypted connection using SSL.                
        //    client.EnableSsl = true;
        //    // Send the email.                 
        //    try
        //    {
        //        //Console.WriteLine("Attempting to send an email through the Amazon SES SMTP interface...");
        //        client.Send(FROM, TO, SUBJECT, BODY);

        //        return "1";
        //    }
        //    catch (Exception ex)
        //    {
        //        BasePage.writeLog(ex);

        //        return "0";
        //    }
        //}



    }
}
