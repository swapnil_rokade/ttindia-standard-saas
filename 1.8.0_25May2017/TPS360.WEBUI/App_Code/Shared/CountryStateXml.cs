
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Web.Security;
using System.Net;
using System.Net.Sockets;
using System.Web.Mail;
using System.Web;
using System.Text;

namespace TPS360.Web.UI
{
	/// <summary>
	///This class acts as a data source for countries and states data. Following are the list of uses. 
	///		Retuning list of countries present in CountriesAndStates.xml file as ArrayList
	///		Retuning list of states for a given country as ArrayList or XML Document in a string
	/// </summary>
	/// 

    public class CountryStateXml:BaseControl 
    {
       
        /// <summary>
        /// This function retuns list of states for a given country as XML Document in a string 
        /// and this value is used in client side java script to populate state combo box.
        /// Functionality: Transform the CountriesAndStates xml string into another XML string having the single country 
        /// and states under that country. 
        /// </summary>
        public string GetStatesXMLString(int countryid)
        {
            string s = "<country>";
            IList<State> a = MiscUtil.getstate(countryid, Facade);
            try
            {
                s += "<state><Id>0</Id><Name>Please Select</Name></state>";
                foreach (State r in a)
                {

                    string ss = r.Name.Replace("&", "&amp;");
                    ss = ss.Replace(">", "&gt;");
                    ss = ss.Replace("<", "&lt;");
                    ss = ss.Replace("'", "&apos;");
                    s += "<state><Id>" + r.Id + "</Id><Name>" + ss + "</Name></state>";
                }

            }
            catch (Exception ex)
            {
                s += "<state>Please Select</state>";
            } s += "</country>";
            return s;

        }

        public string GetCompanyContactXMLString(int coumpanyId, string ValueType)
        {
            string s = "<company>";
            IList<CompanyContact> a = Facade.GetAllCompanyContactByComapnyId(coumpanyId);
            try
            {
                s += "<contact><Id>0</Id><Name>Please Select</Name></contact>";
                foreach (CompanyContact   r in a)
                {

                    string ss =MiscUtil .RemoveScript ( r.FirstName,string .Empty ) + " " +MiscUtil .RemoveScript ( r.LastName,string .Empty );
                   
                    ss=ss.Replace("&", "&amp;");
                    ss = ss.Replace(">", "&gt;");
                    ss = ss.Replace("<", "&lt;");
                    ss = ss.Replace("'", "&apos;");
                    if (ValueType == "Email")
                    {
                        if(r.Email.Trim () !=string .Empty )
                        s += "<contact><Id>" +  r.Email + "</Id><Name>" + ss + "</Name></contact>";
                    }
                    else if (ValueType == "MemberID")
                    {
                        s += "<contact><Id>" +  r.MemberId  + "</Id><Name>" + ss + "</Name></contact>";
                    }
                    else 
                        s += "<contact><Id>" + r.Id + "</Id><Name>" + ss + "</Name></contact>";
                }

            }
            catch (Exception ex)
            {
                s += "<contact>Please Select</contact>";
            } s += "</company>";
            return s;

        }
    }
}
