/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: UIconstants.cs
    Description: This page is for defining public constant values.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Nov-07-2008          Yogeesh Bhat          Defect id: 8860; changed EMAIL_COMPANY_LOGO path
 *  0.2              Dec-28-2009          Basavaraj Angadi      Defect id:12004 ; changed Hotlist name is inconsistent
 *  0.3               8/Aug/2016          pravin khot           added-DROP_DOWNL_ITEM_PLEASE_SELECT_BUContact
-------------------------------------------------------------------------------------------------------------------------------------------       
*/

namespace TPS360.Web.UI
{
    public static class UIConstants
    {
        public const int DEFAULT_ROW_PER_PAGE = 25;
        public const int DEFAULT_PAGE_INDEX = 1;

        public const string LAST_ROW_PAGE_VALUE = "lrpv";
        public const string LAST_CURRENT_PAGE = "lcp";
        public const string SORT_COLUMN_KEY = "sc";
        public const string SORT_ORDER_KEY = "so";
        public const string SORT_ORDER_ASC = "asc";
        public const string SORT_ORDER_DESC = "desc";

        public const string DROP_DOWNL_ITEM_PLEASE_SELECT = "Please Select";
        public const string DROP_DOWNL_ITEM_ROOT = "Root";
        public const string DROP_DOWNL_ITEM_NONE = "None";
        public const string DROP_DOWNL_ITEM_ALL = "All";
        public const string DROP_DOWNL_ITEM_ANY = "Any";
        public const string DROP_DOWNL_ITEM_UNSPECIFIED = "--";

        public const string CURRENCY_FORMAT = "c";
        public const string SHORT_DATE_FORMAT = "MM/dd/yyyy";

        public const string SHOW_THUMB = "showThumb";

        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_COUNTRY = "Please Select Country";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_STATE = "Please Select State";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_QUALIFICATION = "Please Select Qualification";        
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_CATEGORY = "Please Select Category";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_INDUSTRY = "Please Select Industry";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_VISATYPE = "Please Select Visa Type";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_CURRENCY = "Select Currency";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_NOTECATEGORY = "Select note category";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_INTERNALRATING = "Please select internal rating";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_INTERVIEWTYPE = "Please select interview type";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_QUESTIONCATEGORY = "Please select question category type";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_CLIENT = "Select Account";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_PROJECT = "Select Project";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_APPROVAL_MANAGER = "Select Approval Manager";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_CLIENT_MANAGER = "Select Client Manager";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_INTERNAL_MANAGER = "Select Internal Manager";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_BUContact = "Select Contact"; //added by pravin khot on 8/Aug/2016

        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_HOTLIST = "Select Hot List"; //0.2
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_JOBPOSTING = "Select Requisition";

        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_FUNCTIONAL_CAGETORY = "Please select functional category";

        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_INDUSTRY_CAGETORY = "Please select industry category";
        public const string DROP_DOWNL_ITEM_PLEASE_SELECT_INTERVIEW_LEVEL = "Please select interview level";
        public const string SUBJECT_CHANGEAVAILABILITY_ALERT = "Alert : Candidate Changed the Availability Status";
        public const string TEMPLATE_REQUISITION = "TemplateRequisition";

        public const string EMAIL_COMPANY_LOGO = "../Images/logo-left-75px.png";        //0.1

        public const string EMAIL_COMPANY_THANKS = "../Images/ezestCOMPANY_LOGO.PNG";
        public const string EMAIL_THANKS_LOGO = "../Images/THANKS_LOGO.png";  

        public const string DEFAULT_ORGANIZATION_LOGO = "Images/logo-left-75px.png";

        public const string INTERVIEW_NOW = "Interview Now";
        public const string SEND_EMAIL = "Send Email";
        public const string SEND_SMS = "Send SMS";
        public const string INTERVIEW_SCHEDULE = "Interview_Schedule";        
        public const string APPLICANT_FEEDBACK = "Applicant_Feedback";
        public const string SEND_SELF_INTERVIEW = "Send Self Interview";

        #region Jobposting Additional Questions

        public const string JOBPOSTING_SKILLSET_QUESTION = "Have you worked with the following skill set: ";
        public const string JOBPOSTING_ADDITIONAL_QUESTION1 = "Why do you think this job is good for you, or why should I hire you?";
        public const string JOBPOSTING_ADDITIONAL_QUESTION2 = "Why did you leave your last job, or why are you leaving your current job?";
        public const string JOBPOSTING_ADDITIONAL_QUESTION3 = "What industry experience do you have?";
        public const string JOBPOSTING_ADDITIONAL_QUESTION4 = "What is your desired best salary rate for this job?";
        public const string JOBPOSTING_ADDITIONAL_QUESTION5 = "Where is your current location? Would you like to relocate? If yes, where?";

        #endregion

        public const string FORMAT_WORD = "word";
        public const string FORMAT_EXCEL = "excel";
        public const string FORMAT_PDF = "pdf";

        public const string LEFTMENUCONTROL_ID = "ctl00$cphHomeMaster$ctlLeftPanel";

        public const string STAFFING_FIRM = "Staffing_firm";
        public const string INTERNAL_HIRING = "Internal_hiring";
        public const string GOVERNMENT_PORTAL = "Government_portal";
        public const string ALLOW_ANY_USER_ASSINGED_TO_A_REQUISITION_TO_CHANGE_THE_REQ_STATUS = "Allow any user assigned to a requisition to change the req status";
        public const string ONLY_THE_REQUISITION_CREATOR_WILL_HAVE_PERMISSION_CHANGE_THE_REQ_STATUS = "Only the requisition creator will have permission to change the req status";


        public const string AccountType_Client = "Client";
        public const string AccountType_Vendor = "Vendor";
        public const string AccountType_Department = "Department";
    }
}