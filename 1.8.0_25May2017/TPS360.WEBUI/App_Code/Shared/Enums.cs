using System;
using TPS360.Common;
namespace TPS360.Web.UI
{
    [Serializable()]
    public enum SearchType
    {
        MasterSearch,
        MySearch,
        SearchByEmployee,
        Other
    }

    [Serializable()]
    public enum SelectionType
    {
        CheckBox,
        RadioButton
    }

    [Serializable()]
    public enum JobPostingQuestionType
    {
        GeneralQuestion,
        AdditionalQuestions
    }

    [Serializable()]
    public enum EmailFromType
    {
        Reference,
        CampaignEmail,
        TPS360Access,
        CompanyEmail,
        Dashboard,
        EmailHistory,
        EmployeeEmail
    }
    public enum TimeSheetFromType
    {
        MasterTimeSheet,
        ConsultantInternalTimeSheet,
        MyConsultantTimeSheet,
        ConsultantTimeSheet
    }

    [Serializable()]
    public enum OverviewType
    {
        Unknown,
        CandidateOverview,
        ConsultantOverview,
        EmployeeOverview,
        CompanyOverview,
        CampaignOverview,
        DepartmentOverview,
        VendorProfileMenu
    }

    public enum ApplicationSource : int
    {
        [EnumDescription("Main Application")]
        MainApplication = 0,
        [EnumDescription("LandT Application")]
        LandTApplication = 1,
        [EnumDescription("Selectigence Application")]
        SelectigenceApplication = 2,
        [EnumDescription("Genisys Application")]
        GenisysApplication = 3
    }


}