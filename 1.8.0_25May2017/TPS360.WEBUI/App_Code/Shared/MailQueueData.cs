﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Text.RegularExpressions;
using TPS360.Web.UI;
using TPS360.BusinessFacade;
using System.IO;
using System.Text;
using System.Collections.Specialized;
using TPS360.Common.BusinessEntities;
using System.Collections.Generic;
/// <summary>
/// Summary description for MailQueue
/// </summary>
public static class MailQueueData
{

    //public static string GetAttachedFileNames(StringDictionary Files, out int count)
    //{
    //    //StringBuilder filename = new StringBuilder();
    //    IList<string> strArray = new List<string>();
    //    Random rand = new Random();
    //    int randomValue = 0;
    //    int i = 0;
    //    string[] strSplit = null;
    //    foreach (DictionaryEntry file in Files)
    //    {
    //        if (!strArray.Contains(file.Key))
    //        {
    //            strArray.Add(file.Key.ToString() + ";");
    //        }
    //        else 
    //        {
    //            randomValue = rand(100);
    //            strSplit = file.Key.ToString().Split('.');
    //            strArray.Add(strSplit[0] + "-" + randomValue + strSplit[1] + ";");
    //        }
    //        //filename.Append(file.Key + ";");
    //        i++;
    //    }
    //    count = i;
    //    return strArray.ToString();
    //}
    public static string[] GetSourcePath(StringDictionary UploadeFiles) 
    {
        string[] returnPath = null;
        IList<string> sourcePaths = new List<string>(); 
        int index = 0;
        //string result = "";
        foreach (DictionaryEntry path in UploadeFiles) 
        {
            if (path.Value.ToString().Contains("Temp"))
            {
                returnPath = path.Value.ToString().Split(new string[] { "?" }, StringSplitOptions.None);
                index = returnPath[0].LastIndexOf("\\");
                if (index > 0)
                    sourcePaths.Add(returnPath[0].Substring(0, index));

            }
            else 
            {
                sourcePaths.Add(path.Value.ToString());
            }
            }
        return sourcePaths.ToArray();
    }
    public static string GetTargetPath(int mailQueueId)
    {
        string path = UrlConstants.EMAIL_UPLOADEDFILES_DIR + "\\" + mailQueueId ;

        return path;

    }
    public static string UpLoadDocuments(int mailQueueId, StringDictionary UploadedFiles,out int NoOfAttachments)
    {
        string filename = "";
        string fileRename = "";
        string targetPath = "";
        string[] sourcePath = null;
        string targetFile = "";
        string[] splitFilename = null;
        StringBuilder sbFileNames = new StringBuilder();
        sourcePath = GetSourcePath(UploadedFiles);
        targetPath = GetTargetPath(mailQueueId);
        int count = 0;
        for (int f = 0; f < sourcePath.Length; f++) 
        {
            if (!Directory.Exists(targetPath)) 
            {
                Directory.CreateDirectory(targetPath);
            }

            if (Directory.Exists(sourcePath[f])) 
            {
                string[] files = Directory.GetFiles(sourcePath[f]);
                int i = 1;
              
                foreach (string filePath in files)
                {
                    filename = Path.GetFileName(filePath);
                    targetFile = Path.Combine(targetPath, filename);
                    if (!File.Exists(targetFile))
                    {
                        File.Copy(filePath, targetFile);
                        sbFileNames.Append(filename + ";");
                    }
                    else
                    {
                        splitFilename = filename.Split('.');
                        fileRename = splitFilename[0] + i++ + "." + splitFilename[1];
                        targetFile = Path.Combine(targetPath, fileRename);
                        File.Copy(filePath, targetFile);
                        sbFileNames.Append(fileRename + ";");
                    }
                    count++;
                }
            }
        }
        NoOfAttachments = count;
        return sbFileNames.ToString();
    }
    public static void AddMailToMailQueue(int SenderId, string ReceiverEmailId, string Subject, string EmailBody, string bcc, string cc, StringDictionary UploadedFiles,IFacade facade)
    {
        int count = 0;
        string fileNames = "";
        MailQueue mailqueue = new MailQueue();
        mailqueue.SenderId = SenderId;
        mailqueue.ReceiverEmailId = ReceiverEmailId;
        mailqueue.Subject = Subject;
        mailqueue.EmailBody = EmailBody;
        mailqueue.bcc = bcc;
        mailqueue.cc = cc;
        //if (UploadedFiles != null) 
        //{
        //    mailqueue.AttachedFileNames = GetAttachedFileNames(UploadedFiles, out count);
        //    mailqueue.NoOfAttachments = count;
        //}
        int mailQueueId = facade.AddMailInMailQueue(mailqueue);
        if (UploadedFiles != null)
        {
            int NoOfAttachments = 0;
            fileNames = UpLoadDocuments(mailQueueId, UploadedFiles,out NoOfAttachments);
            facade.UpdateFileNamesInMailQueue(fileNames, mailQueueId, NoOfAttachments);
        }       
    
    }

}
