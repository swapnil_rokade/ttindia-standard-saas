﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPSTool.Parser;

namespace TPS360.Web.UI
{
    /// <summary>
    /// Summary description for ResumeParser
    /// </summary>
    public class ResumeParser
    {
        string currentMemberRole = string.Empty;
        int currentMemberId = 0;
        int processedMemberId = 0;
        private Page currentPage;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userRole">the role of the user currently logged in</param>
        /// <param name="userId">member id of currently logged in user</param>
        /// <param name="memberId">member id of the applicant whome resume is being parsed</param>
        /// <param name="requestedPage">current page using this class</param>
        public ResumeParser(string userRole,int userId,int memberId,Page requestedPage)
        {
            currentMemberRole = userRole;
            currentMemberId = userId;
            processedMemberId = memberId;
            currentPage = requestedPage;
        }

        public bool ParseAndSaveWordResume(string filePath)
        {
            TPSResumePath resumePath = new TPSResumePath(filePath);
            if (resumePath.IsValid())
            {
                TPSFileParser fileParser = new TPSFileParser(resumePath, GetCurrentResumeSource(),currentMemberId,true);
                fileParser.MemberId = processedMemberId;
                fileParser.WebServiceUrl = MiscUtil.GetResumeServiceUrl(currentPage);
                if (fileParser.ParseAndSave())
                {
                    return true;
                }
            }
            return false;
        }

        public bool ParseAndSaveCopyPasteResume(string resumeContent)
        {
            TPSResume resume = new TPSResume(resumeContent);
            resume.IsValidated = true;
            using (TPSFileParser fileParser = new TPSFileParser(resume, GetCurrentResumeSource(), currentMemberId,true))
            {
                fileParser.MemberId = processedMemberId;
                fileParser.WebServiceUrl = MiscUtil.GetResumeServiceUrl(currentPage);
                if (fileParser.ParseAndSave())
                {
                    return true;
                }
                return false;
            }
        }

        private TPSTool.Common.ResumeSource GetCurrentResumeSource()
        {
            TPSTool.Common.ResumeSource resumeSource = TPSTool.Common.ResumeSource.None;
            if (processedMemberId == currentMemberId)
            {
                resumeSource = TPSTool.Common.ResumeSource.SelfRegistration;
            }
            else if (currentMemberRole == ContextConstants.ROLE_ADMIN)
            {
                resumeSource = TPSTool.Common.ResumeSource.Admin;
            }
            else if (currentMemberRole == ContextConstants.ROLE_EMPLOYEE)
            {
                resumeSource = TPSTool.Common.ResumeSource.Employee;
            }
            else if (currentMemberRole == ContextConstants.ROLE_COMPANY || currentMemberRole == ContextConstants.ROLE_CLIENT || currentMemberRole == ContextConstants.ROLE_COMPANY_CONTACT)
            {
                resumeSource = TPSTool.Common.ResumeSource.Client;
            }
            else if (currentMemberRole == ContextConstants.ROLE_PARTNER)
            {
                resumeSource = TPSTool.Common.ResumeSource.Partner;
            }
            else if (currentMemberRole == ContextConstants.ROLE_VENDOR)
            {
                resumeSource = TPSTool.Common.ResumeSource.Supplier;
            }
            return resumeSource;
        }

    }
}
