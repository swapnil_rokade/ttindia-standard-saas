/*
----------------------------------------------------------------------------------------------------------------------------------
    FileName: UrlConstants.cs
    Description: This page contains URL constants.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date              Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------ 
    0.1            Oct-14-2008       Shivanand          Defect Id 8907; Constant "JOB_POSTING_MY_REQUISITION_LIST_PAGE" is added under Requisition class.
    0.2            Jan-29-2009       Jagadish           Defect Id 9360; Constant "App_Data" is added.
    0.3            Feb-05-2009       Shivanand          Defect #9252; Constant "PARAM_PRECISESE_SEARCH" is added.
    0.4            AUG--2009         Gopala Swamy J     Defet #11209; Constants "ATS_HOME" and "CONSULTANT_HOME"
 *  0.5            23-Nov-2009         Sandeesh         Enhancement id:11645 - Changes made  for adding internal rating for an employee 
 *  0.6             12/July/2016      pravin khot       added - CANDIDATE_INTERNALNOTESSHARING

   --------------------------------------------------------------------------------------------------------------------------------  
*/

using System;
using System.IO;
using System.Configuration;
using System.Web;
using System.Web.UI;

namespace TPS360.Web.UI
{
    public static class UrlConstants
    {
        public static string ApplicationBaseUrl
        {
            get
            {
                HttpContext context = HttpContext.Current;
                string baseUrl = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/') + '/';
                return baseUrl;
            }
        }

        public const string PARAM_ID = "id";
        public const string PARAM_INTERVIEWID = "Iid";
        public const string PARAM_INTERVIEWTITLE = "ITitle";
        public const string PARAM_CONTACT_ID = "ContactId";
        public const string PARAM_CLIENTWORKORDER_ID = "cwoid";
        public const string PARAM_VENDORWORKORDER_ID = "vwoid";
        public const string PARAM_COMPANY_ID = "companyid";
        public const string PARAM_CAMPAIGN_ID = "campaignid";
        public const string PARAM_CAMPAIGN_COMPANY_ID = "campaigncompanyid";
        public const string PARAM_MSG = "msg";
        public const string PARAM_EMAIL_TYPE = "mailtype";
        public const string PARAM_OFCMI = "OrganizationFunctionalityCategoryMapId";
        public const string PARAM_BRANCHOID = "BranchOfficeId";
        public const string PARAM_MTIERID = "MemberTierId";
        public const string PARAM_ERR = "err";
        public const string PARAM_SITEMAP_ID = "sitemapid";
        public const string PARAM_SITEMAP_PARENT_ID = "sitemapparentid";
        public const string PARAM_MEMBER_ID = "mid";
        public const string PARAM_APPLICANT_ID = "aid";
        public const string PARAM_FLAG_ID = "fid";
        public const string PARAM_USER_ID = "mid";  // for membership provider id
        public const string PARAM_CHANGE_REQ_ID = "changereqid";
        public const string PARAM_JOB_ID = "jid";
        public const string PARAM_JOBCART_ID = "jcid";
        public const string PARAM_CAREER_PORTAL = "careerportal";
        public const string PARAM_HIRING_MATRIX_INTERVIEW_LAVEL = "hmil";
        public const string PARAM_INTERVIEWSCHEDULE_ID = "isd";
        public const string PARAM_INTERVIEWSCHEDULE_LOCATION = "iloc";
        public const string PARAM_INTERVIEWSCHEDULE_DATETIME = "idt";
        public const string PARAM_INTERVIEWSCHEDULE_JOBTITLE = "ijt";
        public const string PARAM_INTERVIEWSCHEDULE_FEEDBACK = "ifd";
        public const string PARAM_RESUMEBUILD_OPTION = "rbo";
        public const string PARAM_MemberEmail_ID = "meid";
        public const string PARAM_VENDOR = "Vendor";
        public const string PARAM_TEST_ID = "tid";
        public const string PARAM_TEST_LIST_TYPE_ID = "testlisttpyeid";
        public const string PARAM_TEST_QUESTION_ID = "questionid";
        public const string PARAM_MEMBER_TESTMASTER_MAP_ID = "membertestmastermapid";
        public const string PARAM_AAPEAR_EXAM_TIME_OUT = "appearexamtimeout";

        public const string PARAM_TASK_CATEGORY_ID = "taskcategoryid";
        public const string PARAM_COMMON_TASK_ID = "commontaskid";
        public const string PARAM_MEMBER_TASK_ID = "membertaskid";

        public const string PARAM_ROLE_TYPE = "role";
        public const string PARAM_TAB = "tab";
        public const string PARAM_SELECTEDTAB = "Stab";
        public const string PARAM_CATEGORY_ID = "catId";
        public const string PARAM_COMPANY_TYPE = "companyType";
        public const string PARAM_COMPANY_GROUP = "companyGroupType";
        public const string PARAM_PENDING_TYPE = "pendingType";
        public const string PARAM_PRODUCT_ID = "prodId";

        public const string PARAM_SEARCH_TYPE = "searchType";

        public const string PARAM_HR_OFFER_LETTER_ID = "hrolid";
        public const string PARAM_HR_OFFER_LETTER_ALLOUNCE_ID = "hrolaid";
        public const string PARAM_HR_OFFER_LETTER_BENEFIT_ID = "hrolbid";
        public const string PARAM_HR_OFFER_LETTER_BONUS_ID = "hrolboid";
        public const string PARAM_HR_OFFER_LETTER_LEAVE_ID = "hrollid";

        public const string PARAM_GLOBAL_SEARCH_KEYWORD = "globalsearchkey";

        public static readonly string PARAM_IMG_FILE = "FilePath";

        public static readonly string PARAM_PAGE_FROM = "PageFrom";

        public static readonly string PARAM_SELECTED_IDS = "SelectedID";

        public const string PORTAL_PASSWORDRESET_ID = "ResetID";

        public static readonly string WEB_FOLDER_SEP = Path.AltDirectorySeparatorChar.ToString();
        public static readonly string ROOT_PREFIX = "~" + WEB_FOLDER_SEP;

        public static readonly string ApplicationPath = AppDomain.CurrentDomain.BaseDirectory;
        //public static string ApplicationBaseUrl = BaseSiteUrl;

        public static readonly string CssPrefix = ConfigurationManager.AppSettings["Constants.Prefix.Css"];
        public static readonly string CssSuffix = ConfigurationManager.AppSettings["Constants.Suffix.Css"];

        public static readonly string ImagePrefix = ConfigurationManager.AppSettings["Constants.Prefix.Img"];
        public static readonly string ImageSuffix = ConfigurationManager.AppSettings["Constants.Suffix.Img"];

        public static readonly string JavaScriptPrefix = ConfigurationManager.AppSettings["Constants.Prefix.Js"];
        public static readonly string JavaScriptSuffix = ConfigurationManager.AppSettings["Constants.Suffix.Js"];
        
        public static readonly string IMAGE_DIR = ROOT_PREFIX + "Images" + WEB_FOLDER_SEP;
        public static readonly string TempDirectory = Path.Combine(ApplicationPath, "Temp");
        public static readonly string App_Data = Path.Combine(ApplicationPath, "App_Data"); // 0.2
        public static readonly string ProductDocumentDirectory = Path.Combine(ApplicationPath, @"Resources\ProductDocuments");
        public static readonly string CompanyDocumentDirectory = Path.Combine(ApplicationPath, @"Resources\CompanyDocuments");
        public static readonly string CampaignDocumentDirectory = Path.Combine(ApplicationPath, @"Resources\CampaignDocuments");
        public static readonly string CompanyLogoDirectory = Path.Combine(ApplicationPath, @"Resources\CompanyLogo");
        public static readonly string OrganizationLogoDirectory = Path.Combine(ApplicationPath, @"Resources\OrganizationLogo");
        public static readonly string OrgBranchLogoDirectory = Path.Combine(ApplicationPath, @"Resources\OrgBranchLogo");
        public static readonly string CompanyContactDirectory = Path.Combine(ApplicationPath, @"Resources\CompanyContact");
        public static readonly string ProjectDocumentDirectory = Path.Combine(ApplicationPath, @"Resources\ProjectDocuments");
        public static readonly string ContentDocumentDirectory = Path.Combine(ApplicationPath, @"Resources\ContentDocuments");
        public static readonly string EmailDocumentDirectory = Path.Combine(ApplicationPath, @"Resources\EmailDocuments");
        public static readonly string MemberSignatureDirectory = Path.Combine(ApplicationPath, @"Resources\Member");
        public static readonly string RequisitionDocumentDirectory = Path.Combine(ApplicationPath, @"Resources\Requisition");
       
        public static readonly string GuestHousePhotoDirectory = Path.Combine(ApplicationPath, @"Resources\GuestHousePhoto");
        public static readonly string GuestHouseSeatPhotoDirectory = Path.Combine(ApplicationPath, @"Resources\GuestHouseSeatPhoto");
        public static readonly string GuestHouseDocumentDirectory = Path.Combine(ApplicationPath, @"Resources\GuestHouseDocument");

        public static readonly string RESOURCES_DIR = ROOT_PREFIX + "Resources" + WEB_FOLDER_SEP;
        public static readonly string COMPANY_CONTACT_DIR = RESOURCES_DIR + "CompanyContact" + WEB_FOLDER_SEP;
        public static readonly string COMPANY_LOGO_DIR = RESOURCES_DIR + "CompanyLogo" + WEB_FOLDER_SEP;
        public static readonly int COMPANY_REMARK = 403;

        public static readonly string ORGANIZATION_LOGO_DIR = RESOURCES_DIR + "OrganizationLogo" + WEB_FOLDER_SEP;
        public static readonly string ORGBRANCH_LOGO_DIR = RESOURCES_DIR + "OrgBranchLogo" + WEB_FOLDER_SEP;

        public static readonly string GUEST_HOUSE_PHOTO_DIR = RESOURCES_DIR + "GuestHousePhoto" + WEB_FOLDER_SEP;
        public static readonly string GUEST_HOUSE_SEAT_PHOTO_DIR = RESOURCES_DIR + "GuestHouseSeatPhoto" + WEB_FOLDER_SEP;
        public static readonly string GUEST_HOUSE_DOCUMENT_DIR = RESOURCES_DIR + "GuestHouseDocument" + WEB_FOLDER_SEP;

        public static readonly string H1B1_DOCUMENT_DIR = RESOURCES_DIR + "H1B1Document" + WEB_FOLDER_SEP;

        public static readonly string MEMBER_EXPENSE_RECEIPT_DIR = RESOURCES_DIR + "MemberExpenseReceipt" + WEB_FOLDER_SEP;

        public static readonly string MEMBER_DOC_DIR = "Resources/Member";
        public static readonly string ONLINE_CV_DIR = "Resources/NewCV";
        public static readonly string ONLINE_PARSE_DIR = "Resources/ParseCV"; 
        public static readonly string EMAIL_UPLOADEDFILES_DIR = Path.Combine(ApplicationPath, @"Resources\MailQueueAttachements");
        public static readonly string REQUISITION_DOC_DIR = "Resources/Requisition";
        public static readonly string PROJECT_DOC_DIR = "Resources/ProjectDocuments";
        public static readonly string COMPANY_DOC_DIR = "Resources/CompanyDocuments";
        public const string HOME_PAGE = "Default.aspx";
        public const string LOGIN_PAGE = "Login.aspx";
        public const string LOGIN_RESET = "LoginReset.aspx";
        public const string SIGN_UP_PAGE = "Register.aspx";
        public const string DRAW_MEDIA_PAGE = "DrawMedia.ashx";
        public const string REFERENCE_CHECKER_PAGE = "ReferenceChecker.aspx";

        public const string CMS_EDITOR_PAGE = "ContentEditor.aspx";

        public const string FORGOT_PASSWORD_PAGE = "ForgotPassword.aspx";

        public const string EXTERNAL_REQUISITION_PREVIEW = "ExtarnalRequisitionPreview.aspx";

        public const string SEND_REQUISITION_BY_MAIL_PAGE = "SendRequisitionByMail.aspx";

        public const string TPS360_VEDIO_PLAYER = "~/TPS360VideoPlayer.aspx";
        public const string VHAPPLICANT_OVERVIEW_PDF = "~/VHApplicantOverview.aspx";

        public const string PARAM_OVERVIEW_INTERNALRATING = "Internal Rating";  
        public const string PARAM_PRECISE_SEARCH = "Precise Search"; 
        public const string PARAM_SEARCH_ALLKEYWORDS = "SearchAllKeywords"; 
        public const string PARAM_SEARCH_ANYKEYWORD = "SearchAnyKeyWord"; 
        public const string EMAIL_SUBJECT = "Subject"; 

        public static string GetApplicationPath()
        {
            string applicationPath = "/";

            if (HttpContext.Current != null)
            {
                applicationPath = HttpContext.Current.Request.ApplicationPath;
            }

            // Are we in an application?
            //
            if (applicationPath == "/")
            {
                return string.Empty;
            }
            else
            {
                return applicationPath;
            }
        }

        public static string GetProductDocumentDirectory(string productId)
        {
            return Path.Combine(ProductDocumentDirectory, productId);
        }

        public static string GetCompanyDocumentDirectory(string companyId)
        {
            return Path.Combine(CompanyDocumentDirectory, companyId);
        }

        public static string GetCampaignDocumentDirectory(string campaignId)
        {
            return Path.Combine(CampaignDocumentDirectory, campaignId);
        }

        public static string GetContentDocumentDirectory(string contentId)
        {
            return Path.Combine(ContentDocumentDirectory, contentId);
        }
        public static string GetEmailDocumentDirectory(string memberId)
        {
            return Path.Combine(EmailDocumentDirectory, memberId);
        }

        public static string GetPhysicalCompanyUploadDirectory()
        {
            return System.Web.HttpContext.Current.Server.MapPath(COMPANY_LOGO_DIR);
        }

        public static string GetPhysicalOrganizationUploadDirectory()
        {
            return System.Web.HttpContext.Current.Server.MapPath(ORGANIZATION_LOGO_DIR);
        }

        public static string GetPhysicalGuestHouseSeatUploadDirectory()
        {
            return System.Web.HttpContext.Current.Server.MapPath(GUEST_HOUSE_SEAT_PHOTO_DIR);
        }

        public static string GetPhysicalOrgBranchUploadDirectory()
        {
            return System.Web.HttpContext.Current.Server.MapPath(ORGBRANCH_LOGO_DIR);
        }

        public static string GetPhysicalCompanyContactResumeUploadDirectory()
        {
            return System.Web.HttpContext.Current.Server.MapPath(COMPANY_CONTACT_DIR);
        }

        public static string GetCurrentDomainName(Page currentPage)
        {
            string domainName = string.Empty;
            domainName = "http://" + currentPage.Request.Url.Host + "/";
            return domainName;
        }
        //public static string GetApplicationBaseURL() 
        //{
        //    return BaseSiteUrl;
        //}
        public static class Admin
        {
            public const string DIR = "Admin/";

            public const string GUEST_HOUSE_PHOTO_DIR = "~/Resources/GuestHousePhoto/";

            public const string HOME_PAGE = "~/" + DIR + "/Default.aspx";
            public const string LOCALIZATION_RESOURCE_LIST_PAGE = "~/" + DIR + "/LocalizationResourceList.aspx";
            public const string LOCALIZATION_RESOURCE_EDITOR_PAGE = "~/" + DIR + "/LocalizationResourceEditor.aspx";

            public const string CUSTOM_ROLE_LIST_PAGE = "~/" + DIR + "/CustomRoleList.aspx";
            public const string CUSTOM_ROLE_EDITOR_PAGE = "~/" + DIR + "/CustomRoleEditor.aspx";

            public const string SITEMAP_LIST_PAGE = "~/" + DIR + "/SiteMapList.aspx";
            public const string SITEMAP_EDITOR_PAGE = "~/" + DIR + "/SiteMapEditor.aspx";

            public const string LOOKUP_LIST_PAGE = "~/" + DIR + "/LookupList.aspx";
            public const string LOOKUP_EDITOR_PAGE = "~/" + DIR + "/LookupEditor.aspx";

            public const string ACTIVITY_TYPE_PAGE = "~/" + DIR + "/ActivityTypeEditor.aspx";

            public const string MEMBER_GROUP_LIST_PAGE = "~/" + DIR + "/MemberGroupList.aspx";
            public const string MEMBER_GROUP_EDITOR_PAGE = "~/" + DIR + "/MemberGroupEditor.aspx";
            public const string MEMBER_HOTLIST = "~/" + "Candidate/MasterHotList.aspx";

            public const string SKILL_LIST_PAGE = "~/" + DIR + "/SkillList.aspx";
            public const string SKILL_EDITOR_PAGE = "~/" + DIR + "/SkillEditor.aspx";

            public const string FUNCTIONAL_CAPABILITY_LIST_PAGE = "~/" + DIR + "/FunctionalCapabilityList.aspx";
            public const string FUNCTIONAL_CAPABILITY_EDITOR_PAGE = "~/" + DIR + "/FunctionalCapabilityEditor.aspx";
            public const string FUNCTIONAL_CAPABILITY_VIEWER_PAGE = "~/" + DIR + "/FunctionalCapabilityViewer.aspx";

            public const string INDUSTRY_CAPABILITY_LIST_PAGE = "~/" + DIR + "/IndustryCapabilityList.aspx";
            public const string INDUSTRY_CAPABILITY_EDITOR_PAGE = "~/" + DIR + "/IndustryCapability.aspx";
            public const string INDUSTRY_CAPABILITY_VIEWER_PAGE = "~/" + DIR + "/IndustryCapabilityViewer.aspx";

            public const string SKILL_VIEWER_PAGE = "~/" + DIR + "/SkillViewer.aspx";

            public const string POSITION_LIST_PAGE = "~/" + DIR + "/PositionList.aspx";
            public const string POSITION_EDITOR_PAGE = "~/" + DIR + "/PositionEditor.aspx";

            public const string CONTENT_LIST_PAGE = "~/" + DIR + "/ContentList.aspx";
            public const string CONTENT_EDITOR_PAGE = "~/" + DIR + "/ContentEditor.aspx";

            public const string ROLE_ACCESS_PAGE = "~/" + DIR + "/RoleAccess.aspx";

         

            public const string REFERENCE_CHECK_QUERY_EDITOR_PAGE = "~/" + DIR + "/ReferenceCheckQueryEditor.aspx";

            public const string GUEST_HOUSE_LIST_PAGE = "~/" + DIR + "/GuestHouseList.aspx";
            public const string GUEST_HOUSE_EDITOR_PAGE = "~/" + DIR + "/GuestHouseEditor.aspx";


            public const string TABLE_ALIAS_LIST_PAGE = "~/" + DIR + "/TableAliasList.aspx";
            public const string TABLE_ALIAS_EDITOR_PAGE = "~/" + DIR + "/TableAliasEditor.aspx";
            public const string COLUMN_ALIAS_EDITOR_PAGE = "~/" + DIR + "/ColumnAliasEditor.aspx";

            public const string TABLE_ALIAS_MASTER_PAGE = "~/" + DIR + "/TableAliasMaster.aspx";


            public const string TEMPLATE_TABLE_EDITOR_PAGE = "~/" + DIR + "/TemplateTableEditor.aspx";

            public const string ATTORNEY_INFORMATION_VIEWER_PAGE = "~/" + DIR + "/AttorneyInformationView.aspx";
            public const string ATTORNEY_INFORMATION_EDITOR_PAGE = "~/" + DIR + "/AttorneyEditor.aspx";


        }

        public static class CommonSite
        {
            public const string DIR = "";
            public const string LOGIN_PAGE = DIR + "Login.aspx";
            public const string HOME_PAGE = "~/" + DIR + "Default.aspx";
            public const string REGISTER_PAGE = "~/" + DIR + "Registration.aspx";
            public const string COMPLETE_REGISTER_PAGE = "~/" + DIR + "CompleteRegistration.aspx";
        }

        public static class Employee
        {
            public const string DIR = "Employee/";
            public const string COPYPASTE = "7";

            public const string HOME_PAGE = "~/Dashboard/Dashboard.aspx";
            public const string EMPLOYEE_REGISTER_PAGE = "~/" + DIR + "RegistrationInfoEditor.aspx";
            public const string EMPLOYEE_BASICINFO = "~/" + DIR + "BasicInfoEditor.aspx";
            public const string EMPLOYEE_CHANGEPASSWORD = "~/" + DIR + "ChangePassword.aspx";
            public const string EMPLOYEE_EDUCATION = "~/" + DIR + "EducationEditor.aspx";
            public const string EMPLOYEE_EXPERIENCE = "~/" + DIR + "ExperienceEditor.aspx";
            public const string EMPLOYEE_REFERENCEN = "~/" + DIR + "ReferenceEditor.aspx";
            public const string EMPLOYEE_LIST = "~/" + DIR + "EmployeeList.aspx";

            public const string EMPLOYEE_LISTFORBRANCHMAP = "../" + DIR + "EmployeeListForBranchOfficeMap.aspx";
            public const string EMPLOYEE_LISTFORTIERMAP = "../" + DIR + "EmployeeListForMemberTierMap.aspx";

            public const string EMPLOYEE_INTERNALASSIGNEDMANAGER = "~/" + DIR + "AssignedManager.aspx";
            public const string EMPLOYEE_INTERNALRESUMEBUILDER = "~/" + DIR + "InternalResumeBuilder.aspx";
            public const string EMPLOYEE_INTERNALDOCUMENT = "~/" + DIR + "InternalDocumentUpload.aspx";
            public const string EMPLOYEE_INTERNALACCESS = "~/" + DIR + "TPS360Access.aspx";
            public const string EMPLOYEE_INTERNALTIMESHEET = "~/" + DIR + "InternalTimeSheet.aspx";
            public const string EMPLOYEE_INTERALREFERENCES = "~/" + DIR + "InternalReferences.aspx";
            public const string EMPLOYEE_INTERNALNOTESACTIVITIES = "~/" + DIR + "InternalNotesAndActivities.aspx";
            public const string EMPLOYEE_INTERNALCOPYPASTE = "~/" + DIR + "InternalResumeBuilder.aspx?" + PARAM_TAB + "=" + COPYPASTE;

            public const string EMPLOYEE_INTERNALOVERVIEW = "~/" + DIR + "Overview.aspx";
            public const string EMPLOYEE_EMPLOYEEOVERVIEW = "~/" + DIR + "EmployeeResumeOverview.aspx";
            public const string EMPLOYEE_SALESLIST = "~/" + DIR + "EmployeeSalesList.aspx";
            public const string EMPLOYEE_EMAILPAGE = "~/" + DIR + "InternalEmailEditor.aspx";
            public const string EMPLOYEE_OFFICEPROFILE = "~/" + DIR + "OfficeProfile.aspx";            

            public const string EMPLOYEE_HUMANRESOURCES = "~/" + DIR + "HumanResources.aspx";
            public const string EMPLOYEE_TRAININGPROGRAM = "~/" + DIR + "Training.aspx";
            public const string EMPLOYEE_TASKSSCHEDULE = "~/" + DIR + "TaskAndSchedule.aspx";
            public const string EMPLOYEE_TASKS_SCHEDULE_WITH_MENU = "~/" + DIR + "EmployeeTaskAndSchedule.aspx";

            public const string EMPLOYEE_ATTENDENCEREPORT = "../" + DIR + "EmployeeAttendenceYearlyReport.aspx";

            public const string EMPLOYEE_H1B1Information = "../" + DIR + "HumanResources.aspx";

            public const string EMPLOYEE_TIMESHEET = "~/" + DIR + "EmployeeTimeSheet.aspx";
            public const string EMPLOYEE_TIMESHEETX = "~/" + DIR + "EmployeeTimeSheetx.aspx";

            public const string EMPLOYEE_EMAIL_SUBJECT_PREVIEW = "~/" + DIR + "EmailSubjectPreView.aspx";
            public const string EMPLOYEE_EMAIL_BODY_PREVIEW = "~/" + DIR + "EmailPreview.aspx";
            public const string EMPLOYEE_INTERNALRATING = "~/" + DIR + "InternalRating.aspx";//0.5
            public const string ACCESS_SITEMAP_ID = "380";
        }

        public static class Candidate
        {
            public const string COPYPASTE = "7";
            public const string DIR = "ATS/";

            public const string HOME_PAGE = "~/" + DIR + "CareerPortalWelcome.aspx";
            public const string ATS_OVERVIEW = "~/" + DIR + "CandidateLandingPage.aspx";
            public const string ATS_OVERVIEW_SITEMAP_ID = "350";
            public const string ATS_OVERVIEW_SITEMAP_PARENTID = "12";
            public const string ATS_HOME = "~/" + DIR + "MasterCandidateList.aspx"; //0.4

            public const string CANDIDATE_REGISTER_PAGE_INTERNAL = "~/" + DIR + "RegistrationInfoEditor.aspx";
            public const string CANDIDATE_REGISTER_PAGE_EXTERNAL = "~/SelfRegistration.aspx";
            public const string CANDIDATE_BASICINFO = "~/" + DIR + "BasicInfoEditor.aspx";

            public const string CANDIDATE_EDUCATION = "~/" + DIR + "EducationEditor.aspx";
            public const string CANDIDATE_EXPERIENCE = "~/" + DIR + "ExperienceEditor.aspx";
            public const string CANDIDATE_REFERENCEN = "~/" + DIR + "ReferenceEditor.aspx";

            public const string CANDIDATE_UPLOADRESUME = "~/" + DIR + "DocumentUpload.aspx";
            public const string CANDIDATE_COPYPASTERESUME = "~/" + DIR + "CopyPasteResumeEditor.aspx";

            public const string CANDIDATE_INTERNALCHANGEAPPLICANT = DIR + "ChangeApplicantType.aspx";//12373 

            public const string CANDIDATE_INTERNALASSIGNEDMANAGER =  DIR + "AssignedManager.aspx";
            public const string CANDIDATE_INTERNALRESUMEBUILDER = "~/" + DIR + "InternalResumeBuilder.aspx";
            public const string CANDIDATE_INTERNALDOCUMENT = "~/" + DIR + "InternalDocumentUpload.aspx";
            public const string CANDIDATE_INTERNALJOBCART = "~/" + DIR + "InternalJobCart.aspx";
            public const string CANDIDATE_INTERNALINTERVIEWSCHEDULE = "~/" + DIR + "InternalInterviewSchedule.aspx";
            public const string CANDIDATE_INTERALREFERENCES = "~/" + DIR + "InternalReferences.aspx";
            public const string CANDIDATE_INTERNALREFERRAL = "~/" + DIR + "InternelReferrals.aspx";
            public const string CANDIDATE_INTERNALASSESSMENT = "~/" + DIR + "InternalAssessment.aspx";
            public const string CANDIDATE_INTERNALRESUMESUBMITBROADCAST = "~/" + DIR + "InternalResumeSubmitBroadcast.aspx";
            public const string CANDIDATE_INTERNALNOTESACTIVITIES = "~/" + DIR + "InternalNotesAndActivities.aspx";
            public const string CANDIDATE_INTERNALRATINGS = DIR + "CandidateInternalRating.aspx";
            public const string CANDIDATE_INTERNALPRIVACYSHARING =  DIR + "InternalPrivacyAvailability.aspx";
            public const string CANDIDATE_INTERNALNOTESSHARING = DIR + "InternalNotesAvailability.aspx"; //ADDED BY PRAVIN KHOT ON 12/July/2016**********
            public const string CANDIDATE_INTERNALHOTLIST = "~/" + DIR + "InternalCandidateHotLists.aspx";
            public const string CANDIDATE_INTERNALACCESS = "~/" + DIR + "TPS360Access.aspx";
            public const string CANDIDATE_INTERNALEMAIL = "~/" + DIR + "InternalEmailEditor.aspx";
            public const string CANDIDATE_INTERNALCONVERTCONSULTANT = "~/" + DIR + "ConvertToConsultant.aspx";
            public const string CANDIDATE_INTERNALCOPYPASTE = "~/" + DIR + "InternalResumeBuilder.aspx?" + PARAM_TAB + "=" + COPYPASTE;
            public const string OVERVIEW = "~/" + DIR + "TPS360Overview.aspx";
            public const string OVERVIEW_SITEMAP_ID = "361";
            public const string OVERVIEW_SITEMAP_PARENTID = "358";
            public const string CANDIDATE_RESUMEBUILDER_SITEMAP = "362";
            public const string CANDIDATE_EXTRENALOVERVIEW = "~/" + DIR + "CandidateResumeOverview.aspx";
            public const string CANDIDATE_MANAGEHOTLIST = "../" + DIR + "ManageHotList.aspx";

            public const string CANDIDATE_FEEDBACK = "../" + DIR + "FeedbackEditor.aspx";

            public const string CANDIDATE_EXTERNAL_JOBSEARCH = "~/" + "JobSerach.aspx";


            public const string VENDOR_CANDIDATE_OVERVIEW_SITEMAP_ID = "659";
            public const string VENDOR_CANDIDATE_RESUMEBUILDER_SITEMAP_ID = "660";

        }

        public static class ATS
        {
            public const string COPYPASTE = "7";
            public const string DIR = "ATS/";
            public const string ATS_FIRSTINTERVIEW = "~/" + DIR + "FirstInterview.aspx";
            public const string ATS_SECONDINTERVIEW = "~/" + DIR + "SecondInterview.aspx";
            public const string ATS_FINALINTERVIEW = "~/" + DIR + "FinalInterview.aspx";
            public const string ATS_INTERVIEW_SCHEDULE = "InternalInterviewSchedule.aspx";
            public const string ATS_INTERNALEMAIL_EDITOR = "~/" + DIR + "InternalEmailEditor.aspx";
            public const string ATS_EMAIL_SUBJECT_PREVIEW = "~/" + DIR + "EmailSubjectPreView.aspx";
            public const string ATS_EMAIL_BODY_PREVIEW = "~/" + DIR + "EmailPreview.aspx";
            public const string ATS_INTERNALPREVIEW = "~/" + DIR + "InternalPreview.aspx";
            public const string ATS_FEEDBACKEDITOR = "~/" + DIR + "FeedBackEditor.aspx";
            public const string ATS_INTERNALINTERVIEW_SCHEDULE = "~/" + DIR + "InternalInterviewSchedule.aspx";
            public const string ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX = "~/" + DIR + "PreSelectedInterviewHiringMatrix.aspx";
            public const string ATS_LEVELI_INTERVIEW_HIRINGMATRIX = "~/" + DIR + "LevelIInterviewHiringMatrix.aspx";
            public const string ATS_LEVELII_INTERVIEW_HIRINGMATRIX = "~/" + DIR + "LevelIIInterviewHiringMatrix.aspx";
            public const string ATS_LEVELIII_INTERVIEW_HIRINGMATRIX = "~/" + DIR + "LevelIIIInterviewHiringMatrix.aspx";
            public const string ATS_FINAL_HIRED_HIRINGMATRIX = "~/" + DIR + "FinalHiredHiringMatrix.aspx";

            public const string ATS_SENDREQUISITIONTOAPPLICANT = "~/" + DIR + "SendRequisitionToApplicant.aspx";
            public const string ATS_SENDMATRIXTOMANAGER = "~/" + DIR + "SendMatrixToManager.aspx";
            public const string ATS_SUBMITTOCLIENT = "~/" + DIR + "SubmitToClient.aspx";

            public const string ATS_SCREENINGINTERVIEW = "~/" + "ScreeningInterview.aspx";
            public const string ATS_PRECISE_SEARCH_SAVED_QUERY_PAGE = "~/" + DIR + "/PreciseSearchSavedQuery.aspx";

            public const string ATS_REFERENCE_EDITOR_PAGE = "~/" + DIR + "/ReferenceEditor.aspx";
            public const string ATS_REFERRAL_EDITOR_PAGE = "~/" + DIR + "/CandidateReferrals.aspx";
            public const string ATS_INTERNALREFERENCE_PAGE = "~/" + DIR + "/InternalReferences.aspx";
            public const string ATS_CAREERPORTAL_JOBCART = "~/" + DIR + "/CareerPortalJobCart.aspx";
            public const string ATS_RESUMEBUILDER = "~/" + DIR + "InternalResumeBuilder.aspx";
            public const string ATS_ADDITIONALINFOEDITOR = "~/" + DIR + "AdditionalInfoEditor.aspx";
            public const string ATS_OBJECTIVEANDSUMMARYEDITOR = "~/" + DIR + "ObjectiveAndSummaryEditor.aspx";
            public const string ATS_SKILLEDITOR = "~/" + DIR + "SkillEditor.aspx";
            public const string ATS_EDUCATIONEDITOR = "~/" + DIR + "EducationEditor.aspx";
            public const string ATS_EXPERIENCEEDITOR = "~/" + DIR + "ExperienceEditor.aspx";
            public const string ATS_CERTIFICATIONEDITOR = "~/" + DIR + "CertificationEditor.aspx";
            public const string ATS_COPYPASTERESUMEEDITOR = "~/" + DIR + "CopyPasteResumeEditor.aspx";
            public const string ATS_BROADCAST_RESUME = "~/" + DIR + "BroadcastResume.aspx";
            public const string ATS_INTERNALRESUMEBUILDER = "InternalResumeBuilder.aspx";
            public const string ATS_PROFILE_PDF = "GenerateProfilePdf.aspx";

            public const string ATS_INTERVIEWREMARKS = "~/" + DIR + "InterviewRemarks.aspx";
            public const string ATS_NOTESANDACTIVITIES = "~/" + DIR + "InternalNotesAndActivities.aspx";
            public const string ATS_PRECISESEARCH = "~/" + DIR + "PreciseSearch.aspx";
            public const string ATS_PRECISESEARCH_SITE_MAP_ID = "54";
        }

        public static class Company
        {
            public const string DIR = "Company/";

            public const string HOME_PAGE = "~/" + DIR + "Default.aspx";

            public const string CANDIDATE_RESUMEBUILDER = "~/" + DIR + "ApplicantResumeBuilder.aspx";

            public const string CONSULTANT_RESUMEBUILDER = "~/" + DIR + "ConsultantEditor.aspx";

            public const string MY_CONSULTANT_TIME_SHEET_EDITOR = "~/" + DIR + "MyConsultantTimesheetList.aspx";
            public const string COMPANY_INTERNALNOTESACTIVITIES = "~/" + DIR + "InternalNotesAndActivities.aspx";
        }

        public static class CompanyContact
        {
            public const string DIR = "Company/";

            public const string HOME_PAGE = "~/" + DIR + "Default.aspx";
        }

        public static class Requisition
        {
            public const string DIR = "Requisition/";
            public const string RESOURCE_DIR = "~/Resources/Requisition/";

            public const string JOB_POSTING_MASTER_LIST_PAGE = "~/" + DIR + "/MasterRequisitionList.aspx";
            public const string JOB_POSTING_OPEN_LIST_PAGE = "~/" + DIR + "/OpenRequisitionList.aspx";
            public const string JOB_POSTING_DRAFT_LIST_PAGE = "~/" + DIR + "/DraftRequisitionList.aspx";
            public const string JOB_POSTING_EDITOR_PAGE = "~/" + DIR + "/BasicInfoEditor.aspx";
            public const string JOB_POSTING_DESCRIPTION_PAGE = "~/" + DIR + "/Description.aspx";
            public const string JOB_POSTING_SKILLSET_PAGE = "~/" + DIR + "/SkillSet.aspx";
            public const string JOB_POSTING_CAPABILITY_PAGE = "~/" + DIR + "/RequireCapability.aspx";
            public const string JOB_POSTING_ASSESSMENT_PAGE = "~/" + DIR + "/Assessment.aspx";
            public const string JOB_POSTING_HIRINGMATRIX_PAGE = "~/" + DIR + "/HiringMatrix.aspx";
            public const string JOB_POSTING_SCREENINGQUESTION_PAGE = "~/" + DIR + "/ScreeningQuestion.aspx";
            public const string JOB_POSTING_WORKFLOW_PAGE = "~/" + DIR + "/InternalWorkFlow.aspx";
            public const string JOB_POSTING_PREVIEWPUBLISH_PAGE = "~/" + DIR + "/PreviewPublish.aspx";
            public const string JOB_POSTING_INTERNAL_PREVIEW_PAGE = "~/" + DIR + "/InternalPreview.aspx";
            public const string JOB_POSTING_STATUSREPORT_PAGE = "~/" + DIR + "/StatusReport.aspx";
            public const string JOB_POSTING_EXTERNAL_PREVIEW_PAGE = "~/" + DIR + "/InternalPreview.aspx";           

            public const string JOB_POSTING_TEMPLATE_LIST_PAGE = "~/" + DIR + "/TemplateRequisitionList.aspx";
            public const string JOB_POSTING_TEMPLATE_EDITOR_PAGE = "~/" + DIR + "/TemplateBasicInfoEditor.aspx";
            public const string JOB_POSTING_TEMPLATE_DESCRIPTION_PAGE = "~/" + DIR + "/TemplateDescription.aspx";
            public const string JOB_POSTING_TEMPLATE_SKILLSET_PAGE = "~/" + DIR + "/TemplateSkillSet.aspx";
            public const string JOB_POSTING_TEMPLATE_ASSESSMENT_PAGE = "~/" + DIR + "/TemplateAssessment.aspx";
            public const string JOB_POSTING_TEMPLATE_HIRINGMATRIX_PAGE = "~/" + DIR + "/TemplateHiringMatrix.aspx";
            public const string JOB_POSTING_TEMPLATE_SCREENINGQUESTION_PAGE = "~/" + DIR + "/TemplateScreeningQuestion.aspx";
    
            public const string JOB_POSTING_MY_REQUISITION_LIST_PAGE = "~/" + DIR + "/MyRequisitionList.aspx";  // 0.1    
            public const string JOB_POSTING_NEW_REQUISITION_LIST_SITEMAP_ID = "56";  
            public const string JOB_POSTING_MY_REQUISITION_LIST_SITEMAP_ID = "62";  // 8907
            public const string JOB_POSTING_MASTER_REQUISITION_LIST_SITEMAP_ID = "63";
            public const string JOB_POSTING_HOTJOBS_PREVIEW_PAGE = "~/HotJobs/HotJobRequisitionPreview.aspx";  // 10531   

            public const string PUBLIC_JOBS_SEND_EMAIl_PAGE = "~/HotJobs/PublicJobSendEmail.aspx";  // 10715
            public const string JOB_POSTING_PREVIEW = "InternalPreview.aspx";
            public const string JOB_POSTING_PRINT="JobDetailsPrintView.aspx";
        }

        public static class SFA
        {
            public const string DIR = "SFA/";

            public const string PRODUCT_OVERVIEW_PAGE = "~/" + DIR + "/ProductOverview.aspx";
            public const string PRODUCT_LIST_PAGE = "~/" + DIR + "/ProductList.aspx";
            public const string PRODUCT_EDITOR_PAGE = "~/" + DIR + "/ProductEditor.aspx";
            public const string COMPANY_EDITOR = "CompanyEditor.aspx";        
            public const string COMPANY_LIST_PAGE = "~/" + DIR + "/MyCompanyList.aspx";
            public const string COMPANY_EDITOR_PAGE = "~/" + DIR + "/CompanyEditor.aspx";
            public const string COMPANY_CONTACT_PAGE = "~/" + DIR + "/CompanyContact.aspx";
            public const string COMPANY_DOCUMENT_PAGE = "~/" + DIR + "/CompanyDocument.aspx";
            public const string COMPANY_EMAIL_PAGE = "~/" + DIR + "CompanyEmail.aspx";
            public const string COMPANY_LEAD_PAGE = "~/" + DIR + "/CompanyLead.aspx";
            public const string COMPANY_NOTES_PAGE = "~/" + DIR + "/CompanyNote.aspx";
            public const string COMPANY_LEVEL_PAGE = "~/" + DIR + "/CompanyTierLevel.aspx";
            public const string COMPANY_ACCESS_PAGE = "~/" + DIR + "/TPS360Access.aspx";
            public const string COMPANY_TEAM_PAGE = "~/" + DIR + "/CompanyAssignManager.aspx";
            public const string COMPANY_GROUP_EDITOR_PAGE = "~/" + DIR + "/CompanyGroupEditor.aspx";
            public const string COMPANY_VENDOR_PAGE = "~/" + DIR + "/CompanyVendorPortalAccess.aspx";
            public const string COMPANY_OVERVIEW_PAGE = "~/" + DIR + "/Company360OverView.aspx";
            public const string COMPANY_PREVIEW_PAGE = "~/" + DIR + "/CompanyPreview.aspx";
            public const string COMPANY_MANAGE_COLD_CALL_PAGE = "~/" + DIR + "/ManageColdCall.aspx";
            public const string COMPANY_INTERNALNOTESACTIVITIES = "~/" + DIR + "InternalNotesAndActivities.aspx";
            public const string CAMPAIGN_OVERVIEW_PAGE = "~/" + DIR + "/CampaignOverview.aspx";
            public const string COMPANY_OVERVIEW = "Company360OverView.aspx";
            public const string COMPANY_REQUEST_TO_CHANGE_STATUS_PAGE = "~/" + DIR + "/RequestToChangeStatus.aspx";
            public const string COMPANY_APP_PROCESSING = "~/" + DIR + "/ApplicationProcessing.aspx";

            public const string CAMPAIGN_LIST_PAGE = "~/" + DIR + "/CampaignList.aspx";
            public const string MY_CAMPAIGN_LIST_PAGE = "~/" + DIR + "/MyCampaignList.aspx";
            public const string CAMPAIGN_EDITOR_PAGE = "~/" + DIR + "/CampaignEditor.aspx";

            public const string CAMPAIGN_TEAM_PAGE = "~/" + DIR + "/CampaignTeam.aspx";
            public const string CAMPAIGN_COMPANY_PAGE = "~/" + DIR + "/CampaignCompany.aspx";
            public const string CAMPAIGN_EMAIL_PAGE = "~/" + DIR + "/CampaignEmail.aspx";
            public const string CAMPAIGN_COLD_CALLS_SCRIPTS_PAGE = "~/" + DIR + "/CampaignColdCallScript.aspx";
            public const string CAMPAIGN_COLD_CALLS_PAGE = "~/" + DIR + "/CampaignColdCall.aspx";
            public const string CAMPAIGN_POST_MAIL_PAGE = "~/" + DIR + "/CampaignPostMailMarge.aspx";
            public const string CAMPAIGN_LEAD_PAGE = "~/" + DIR + "/CampaignLead.aspx";
            public const string CAMPAIGN_NOTES_PAGE = "~/" + DIR + "/CampaignNote.aspx";

            public const string LEAD_LIST_PAGE = "~/" + DIR + "/MyLeadList.aspx";
            public const string LEAD_EDITOR_PAGE = "~/" + DIR + "/LeadEditor.aspx";

            public const string BY_PRODUCT_LIST_PAGE = "~/" + DIR + "/ByProductList.aspx";
            public const string BY_PRODUCT_EDITOR_PAGE = "~/" + DIR + "/ByProductEditor.aspx";

        }

        public static class InternelRating
        {
            private const string DIR = "Images";
            public const string OneStar = "~/" + DIR + "/InternalRatingOneStar.png";
            public const string None = "~/" + DIR + "/InternalRating0.png";
            public const string One = "~/" + DIR + "/InternalRating1.png";
            public const string Two = "~/" + DIR + "/InternalRating2.png";
            public const string Three = "~/" + DIR + "/InternalRating3.png";
            public const string Four = "~/" + DIR + "/InternalRating4.png";
            public const string Five = "~/" + DIR + "/InternalRating5.png";
        }

        public static class CommonPages
        {
            public const string DIR = "CommonPages/";

            public const string EMAIL_PREVIEW = "../" + DIR + "EmailPreview.aspx";
            public const string SubmissionEmailPreview = "../" + DIR + "SubmissionEmailPreview.aspx";
        }

        public static class Dashboard
        {
            public const string DIR = "Dashboard/";

            public const string DEFAULT_PAGE = "~/" + DIR + "Dashboard.aspx";
            public const string NEWMAIL =  "NewMail.aspx";
        }
        public static class Vendor
        {
            public const string DIR = "Vendor/";
            public const string LOGIN = "Vendor/Login.aspx";
            public const string HOME = "~/" +  DIR + "Dashboard.aspx";
            public const string VENDOR_CHANGEPASSWORD = "~/" + DIR + "ChangePassword.aspx";

            public const string OVERVIEW = "~/" + DIR + "TPS360Overview.aspx";
            public const string CANDIDATE_INTERNALRESUMEBUILDER = "~/" + DIR + "InternalResumeBuilder.aspx";
        }

        public static class CandidatePortal
        {
            public const string DIR = "CandidatePortal/";
            public const string LOGIN = "~/CandidatePortal/Login.aspx";
            public const string HOME = "~/CandidatePortal/CandidateProfile.aspx";


        }

        public static class Teamplate
        {
            public const string RESOURCE_DIR = "~/Resources/Template/Tracker/";
            public const string BU_TRACKER_TEAMPLATE_FILENAME = "BUTracker.xlsx";

            public const string RESOURCE_LOC = "~/Resources/Template/OfferLetter/";
            public const string OFFER_LETTER_TEMPLATE = "OfferTemplate.doc";
        }
    }
}