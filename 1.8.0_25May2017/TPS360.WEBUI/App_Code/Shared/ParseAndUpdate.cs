/*-----------------------------------------------------------------------------
 Ver  Date               Name                Description
-----------------------------------------------------------------------------
0.1    25/April/2016    pravin khot       modify Globalization.CultureInfo date time format
0.2    05/05/2016       Sakthi              Modified "BuildMember" function modified.
0.3    11/Aug/2016      pravin khot         modify - memberExtendedInfo.CurrentYearlyRate= Convert.ToDecimal(d); ,  memberExtendedInfo.ExpectedYearlyRate = Convert.ToDecimal(d);
-----------------------------------------------------------------------------*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Web.Security;
using System.Net;
using System.Net.Sockets;
using System.Web.Mail;
using System.Web;
using System.Text;
using TPS360.BusinessFacade;
using ParserLibrary;
using System.Data;
using System.Net;
using System.Threading;



namespace TPS360.Web.UI
{
	/// <summary>
	///This class acts as a data source for countries and states data. Following are the list of uses. 
	///		Retuning list of countries present in CountriesAndStates.xml file as ArrayList
	///		Retuning list of states for a given country as ArrayList or XML Document in a string
	/// </summary>
	/// 

    public class ParseAndUpdate:BaseControl 
    {
        int _memberId = 0;
        string SelectedState = string.Empty;
        string SelectedCountry = string.Empty;
        string OfficeNumber = string.Empty;
        string HomeNumber = string.Empty;
        string Address1 = string.Empty;
        string Address2 = string.Empty;
        string Addtype = string.Empty;
 
       public  ParseAndUpdate(int memberid,string state,string country,string officenumber,string homenumber,string address1,string address2)
        {
            _memberId = memberid;
            SelectedState = state;
            SelectedCountry = country;
            OfficeNumber = officenumber;
            HomeNumber = homenumber;
            Address1 = address1;
            Address2 = address2;
         
        }
       public void DeleteDetails()
       {
           //Change Member 
           Member member=Facade .GetMemberById (_memberId );
           Member Changemember=new Member ();
           Changemember .Id=member .Id ;
           Changemember .FirstName =member .FirstName ;
           Changemember .LastName =member .LastName ;
           Changemember .PrimaryEmail =member .PrimaryEmail ;
           Facade .UpdateMember (Changemember );

           //Change MemberDetail
           Facade.DeleteMemberDetailByMemberId(_memberId);

           //Delete MemberExperience
           Facade.DeleteMemberExperienceByMemberId(_memberId);

           //DeleteMemberEducation
           Facade.DeleteMemberEducationByMemberId(_memberId);

           //Delete MemberObjective and Summary
           //Facade.DeleteMemberObjectiveAndSummaryByMemberId(_memberId);
           MemberObjectiveAndSummary objectiveandSummary = new MemberObjectiveAndSummary();
           MemberObjectiveAndSummary old_ObjectiveandSummary = Facade.GetMemberObjectiveAndSummaryByMemberId(_memberId);
           objectiveandSummary.MemberId = old_ObjectiveandSummary.MemberId;
           objectiveandSummary.Id = old_ObjectiveandSummary.Id;
           Facade.UpdateMemberObjectiveAndSummary(old_ObjectiveandSummary);

           //Delete MemberSkillmap
           Facade.DeleteMemberSkillMapByMemberId(_memberId);

           //Delete MemberCertificationMap
           Facade.DeleteMemberCertificationMapByMemberId(_memberId);

           //Delete MemberExtended information
           Facade.DeleteMemberExtendedInformationByMemberId(_memberId);


            
   
           
       }
      
        public void GetParsedResume(Member newmember,string copypasteresume,string type,string filename,string countryName,string MobileNumber,string City, string PinCode, string StateId,string CountryId,string CurrentCompany,int sourceid,string sourcedescription,string totalyearsofExp,string currentCTC,int currentCTCCurrency, int CurrentCTCPayCycle,string ExpectedCTC,int ExpectedCTCCurrency,int ExpectedCTCPayCycle,int IdCardLookUpId,string IdCardDetail,string LinkedInProfile)
       {
           copypasteresume = copypasteresume.Replace("\r\n", "<br/>");
           string ActualText = string.Empty;
           string RawResumeText = string.Empty;
           string HTMLCopyPasteResume = string.Empty;

           Parser.DocumentConverter document = new Parser.DocumentConverter();
           if (type != "CopyPaste")
           {
               string strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Resources/Member");
               filename  = System.Text.RegularExpressions.Regex.Replace(filename , @"[\/:?<>|*]", "");
               strFilePath =strFilePath + "\\" + newmember.Id.ToString() + "\\Word Resume" + "\\" + filename;// newmember.FirstName + newmember.LastName + "-" + "Resume.doc";
               
               
               if (File.Exists(strFilePath))
               {
                   RawResumeText = Parser.DocumentConverter.GetResumeText(strFilePath);
                   //evan
                   try
                   {
                       byte[] data = document.ConvertTo(strFilePath,Parser .DocumentConverter . OutputTypes .HtmlFormatted );
                       HTMLCopyPasteResume = System.Text.ASCIIEncoding.ASCII.GetString(data);
                   }
                   catch (System.Exception ex)
                   {
                   }
                   ActualText = "";
               }
               else
               {
                   RawResumeText = copypasteresume;
                   HTMLCopyPasteResume = copypasteresume;
               }
           }
           else
           {
               string strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Resources/");
               string strFullfilePath = strFilePath + "Copy Paste Resume" + "\\" + newmember.Id.ToString();
               string filenameandfullpath = strFullfilePath + "\\" + newmember.FirstName+newmember.LastName+".txt";
               if (File.Exists(filenameandfullpath))
               {
                   RawResumeText = Parser.DocumentConverter.GetResumeText(filenameandfullpath);
                   //evan
                   try
                   {
                       byte[] data = document.ConvertTo(filenameandfullpath, Parser.DocumentConverter.OutputTypes.HtmlFormatted);
                       HTMLCopyPasteResume = System.Text.ASCIIEncoding.ASCII.GetString(data);
                   }
            catch (System.Exception ex)
            {
            }
                   ActualText = "";

                   try
                   {
                       if (Directory.Exists(strFullfilePath))
                       {
                           Directory.Delete(strFullfilePath, true);
                       }
                   }
                   catch { }
               }
               //RawResumeText = copypasteresume;
               //HTMLCopyPasteResume = copypasteresume;
                      
               else
               {
                   RawResumeText = copypasteresume;
                   HTMLCopyPasteResume = copypasteresume;
               }
           }
  
           System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
           System.Globalization.DateTimeFormatInfo dateinfo = new System.Globalization.DateTimeFormatInfo();
           System.Globalization.CultureInfo c = new System.Globalization.CultureInfo("es-ES");//New line added by pravin khot on 25/April/2016
           if (countryName == "India")
           {
               //****Code commented by pravin khot on 25/April/2016*****************
               //dateinfo.ShortDatePattern = "dd/MM/yyyy";
               //culture.DateTimeFormat = dateinfo;
               //System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat = dateinfo;
               //***********************END*************************

               //****************New code added by pravin khot on 25/April/2016******************
               c.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
               Thread.CurrentThread.CurrentCulture = c;
               //******************************END*****************************************            

           }

            ParserLibrary.BaseParser baseparser = new ParserLibrary.BaseParser(c);
            
            baseparser.ResumeText = RawResumeText;

          
            //Building Member
            newmember.CellPhone = MobileNumber;
            newmember.PermanentCity = City;
            newmember.PermanentStateId = Convert .ToInt32 ( StateId==""?"0":StateId );
            newmember.PermanentZip = PinCode;
            newmember.PermanentCountryId = Convert .ToInt32( CountryId);
            BuildMember(baseparser, newmember);
        

            //Building Memberdetails
            MemberDetail memberdetails = Facade.GetMemberDetailByMemberId(_memberId);
            if (memberdetails == null) memberdetails = new MemberDetail();
            memberdetails.CurrentCity = City;
            memberdetails.CurrentZip = PinCode;
            memberdetails.CurrentStateId = Convert.ToInt32(StateId==""?"0":StateId );
            memberdetails.CurrentCountryId = Convert.ToInt32(CountryId);
            BuildMemberDetails(baseparser, memberdetails);
            

            //Building MemberExperience
            MemberExperience memberExperience = new MemberExperience();
            BuildMemberExperience(baseparser, memberExperience,countryName );
            //Building MemberExtentedInformation
            MemberExtendedInformation memberExtentedInformation = Facade.GetMemberExtendedInformationByMemberId(_memberId);
            if (memberExtentedInformation == null) memberExtentedInformation = new MemberExtendedInformation();
            BuildMemberExtentedInformation(baseparser, memberExtentedInformation,CurrentCompany ,sourceid ,sourcedescription ,totalyearsofExp ,currentCTC ,currentCTCCurrency ,CurrentCTCPayCycle ,ExpectedCTC ,ExpectedCTCCurrency ,ExpectedCTCPayCycle,IdCardLookUpId,IdCardDetail,LinkedInProfile  );
            //Building MemberEducation
            BuildMemberEducation(baseparser);
            //BuildObjective and Summary
            MemberObjectiveAndSummary memberObjectiveAndSummary = Facade.GetMemberObjectiveAndSummaryByMemberId(_memberId);
            if(memberObjectiveAndSummary ==null)memberObjectiveAndSummary = new MemberObjectiveAndSummary();
            memberObjectiveAndSummary = Facade .GetMemberObjectiveAndSummaryByMemberId(newmember .Id );
            memberObjectiveAndSummary.RawCopyPasteResume =  HttpUtility.HtmlDecode(RawResumeText);
            memberObjectiveAndSummary.CopyPasteResume = HTMLCopyPasteResume;
            BuildMemberObjectiveAndSummary(baseparser, memberObjectiveAndSummary);
            //Building MemberSkillMaps
            AddSkillMap(baseparser);
            AddCertificationMap(baseparser);
        }
        public void BuildMember(ParserLibrary.BaseParser baseparser, Member member)
        {
            string OutValue = string.Empty;
            DateTime dob;
            baseparser.GetMiddleName(out OutValue); member.MiddleName = OutValue;
            baseparser.GetNickName(out OutValue); member.NickName = OutValue;
            baseparser.GetAddressLine1(out OutValue); member.PermanentAddressLine1 = OutValue;
            baseparser.GetCity(out OutValue); if(member.PermanentCity.Trim ()=="")member.PermanentCity = OutValue;
            baseparser.GetState(out OutValue);
            if (OutValue != null && OutValue.Trim() != string.Empty)if(member.PermanentStateId==0) member.PermanentStateId = Facade.GetStateIdByStateCode(OutValue);
            baseparser.GetCountry(out OutValue);
            if (OutValue != null && OutValue.Trim() != string.Empty) if (member.PermanentCountryId == 0) member.PermanentCountryId = Facade.GetCountryIdByCountryCode(OutValue);
            baseparser.GetZip(out OutValue); if(member .PermanentZip .Trim ()=="") member.PermanentZip = OutValue;
            baseparser.GetHomePhone(out OutValue); member.PermanentPhone = OutValue; member.PrimaryPhone = OutValue;
            baseparser.GetMobilePhone(out OutValue); if(member .CellPhone =="") member.CellPhone = OutValue;
            baseparser.GetDOB(out dob); member.DateOfBirth = dob != null ? dob : DateTime.MinValue;
            //****************** Code Added By Sakthi On 05/05/2016 *************
            EmployeeReferral reff=Facade .EmployeeReferral_GetByMemberId (member.Id );
            if (reff == null)
            {
                member.Status = 1;
            }
            //****************** Code END By Sakthi On 05/05/2016 *************
            Facade.UpdateMember(member);
        }
        public void BuildMemberExperience(ParserLibrary.BaseParser baseparser, MemberExperience memberExperience,string CountryName)
        {
            IList<MemberExperienceDetail> memberexperience = new List<MemberExperienceDetail>();
            DataTable workhistory = new DataTable();

             baseparser.GetTpsWorkHistory(out workhistory);

            for (int e = 0; e < workhistory.Rows.Count; e++)
            {
                string s = workhistory.Rows[e][8].ToString();
                bool bTillDate = Convert.ToBoolean(workhistory.Rows[e][9]);
                MemberExperienceDetail m_memberExperience = new MemberExperienceDetail();
                m_memberExperience.MemberId = _memberId;
                m_memberExperience.CompanyName = workhistory.Rows[e][0].ToString();
                m_memberExperience.PositionName = workhistory.Rows[e][1].ToString();
                m_memberExperience.FunctionalCategoryLookupId = Convert.ToInt32(workhistory.Rows[e][11]);
                m_memberExperience.IndustryCategoryLookupId = Convert.ToInt32(workhistory.Rows[e][12]);
                m_memberExperience.CountryLookupId = Convert.ToInt32(workhistory.Rows[e][13]);
                m_memberExperience.StateId = Convert.ToInt32(workhistory.Rows[e][14]);
                m_memberExperience.City = workhistory.Rows[e][6].ToString();
                m_memberExperience.DateFrom = workhistory.Rows[e][7].ToString() == "" ? Convert.ToDateTime("01/01/1970") : Convert.ToDateTime(workhistory.Rows[e][7]);
                if (bTillDate)
                {
                    m_memberExperience.DateTo = System.DateTime.Today;
                }
                else
                {
                    try
                    {
                        m_memberExperience.DateTo = workhistory.Rows[e][8].ToString() == "" ? Convert.ToDateTime("01/01/1970") : Convert.ToDateTime(workhistory.Rows[e][8]);
                    }
                    catch
                    {
                    }
                }
                m_memberExperience.IsTillDate = bTillDate;
                m_memberExperience.Responsibilities = workhistory.Rows[e][10].ToString();

                Facade.AddMemberExperience(m_memberExperience);
            }
        }
        public void BuildMemberDetails(ParserLibrary.BaseParser baseparser, MemberDetail memberDetail)
        {
            memberDetail.MemberId = _memberId;
           if(SelectedState!="") if(memberDetail.CurrentStateId==0)memberDetail.CurrentStateId =  Convert.ToInt32(SelectedState  );
            if(SelectedCountry!="")if(memberDetail.CurrentCountryId ==0)memberDetail.CurrentCountryId = Convert.ToInt32(SelectedCountry ); ;
            memberDetail.OfficePhone = OfficeNumber ;
            memberDetail.HomePhone = HomeNumber ;
            string strGend = string.Empty;

            if (baseparser.ResumeText != null)
            {
                if (baseparser.ResumeText.Contains(" Male ")) strGend = "Male";
                else if (baseparser.ResumeText.Contains(" Female ")) strGend = "Female";

                if (strGend == "Male" || strGend == "Female")
                {
                    IList<GenericLookup> lookup = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.Gender, strGend.Trim());
                    foreach (GenericLookup gen in lookup)
                    {
                        if (gen.Name == strGend)
                            memberDetail.GenderLookupId = gen.Id;
                    }
                }
                else
                    memberDetail.GenderLookupId = 0;
                //IList<GenericLookup> ethnicGroupList = Facade.GetAllGenericLookupByLookupType(LookupType.EthnicGroup);
              //  memberDetail.EthnicGroupLookupId = ethnicGroupList[0].Id;
                string strMrStatus = string.Empty;
                if (baseparser.ResumeText.Contains("Married")) strMrStatus = "Married";
                else if (baseparser.ResumeText.Contains("Single") || baseparser.ResumeText.Contains(" Unmarried ") || baseparser.ResumeText.Contains(" UnMarried ") || baseparser.ResumeText.Contains(" Un Married ")) strMrStatus = "Single";
                else if (baseparser.ResumeText.Contains("Divorced")) strMrStatus = "Divorced";
                else if (baseparser.ResumeText.Contains("Widow")) strMrStatus = "Widow";
                else if (baseparser.ResumeText.Contains("Separated ")) strMrStatus = "Separated";
                if (strMrStatus == "Married" || strMrStatus == "Single" || strMrStatus == "Divorced" || strMrStatus == "Widow" || strMrStatus == "Separated")
                {
                    IList<GenericLookup> lookup = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.MaritalStatus, strMrStatus.Trim());
                    foreach (GenericLookup gen in lookup)
                    {
                        if (gen.Name == strMrStatus)
                            memberDetail.MaritalStatusLookupId = gen.Id;
                    }
                }
                else
                    memberDetail.MaritalStatusLookupId = 0;
                memberDetail.NumberOfChildren = 0;
                IList<GenericLookup> bloodGroupList = Facade.GetAllGenericLookupByLookupType(LookupType.MaritalStatus);
                memberDetail.BloodGroupLookupId = bloodGroupList[0].Id;
                memberDetail.AnniversaryDate = new DateTime(1911, 01, 01);
                string OutValue = string.Empty;
                baseparser.GetCityOfBirth(out OutValue); memberDetail.CityOfBirth = OutValue;
                baseparser.GetCountryOfBirth(out OutValue);
                if (OutValue != string.Empty)
                {
                    int citizenship = 0;
                    citizenship = Facade.GetCountryIdByCountryName(OutValue.Trim());
                    if (citizenship == 0)
                        citizenship = Facade.GetCountryIdByCountryCode(OutValue.Trim());
                    memberDetail.CountryIdOfBirth = citizenship > 0 ? citizenship.ToString() : string.Empty;
                }
                else
                    memberDetail.CountryIdOfBirth = OutValue;

                baseparser.GetCountryOfResidence(out OutValue); memberDetail.CountryIdOfCitizenship = OutValue;
                baseparser.GetSSN(out OutValue); memberDetail.SSNPAN = OutValue;
                baseparser.GetZip(out OutValue); if(memberDetail .CurrentZip =="") memberDetail.CurrentZip = OutValue;
            }

            if (memberDetail.Id > 0) Facade.UpdateMemberDetail(memberDetail);
            else Facade.AddMemberDetail(memberDetail);
        }
        public void BuildMemberExtentedInformation(ParserLibrary.BaseParser baseparser, MemberExtendedInformation memberExtendedInfo, string CurrentCompany, int sourceid, string sourcedescription, string totalyearsofExp, string currentCTC, int currentCTCCurrency, int CurrentCTCPayCycle, string ExpectedCTC, int ExpectedCTCCurrency, int ExpectedCTCPayCycle,int IdCardLookUpId,string IdCardDetail, string LinkedInProfile)
        {
            memberExtendedInfo.MemberId = _memberId;
            string OutValue = string.Empty;
            float dblValue = 0;
            int intValue = 0;
            baseparser.GetWorkPermit(out intValue); memberExtendedInfo.WorkAuthorizationLookupId = intValue;
            baseparser.GetCurrentPosition(out OutValue); memberExtendedInfo.CurrentPosition = OutValue;
            if (CurrentCompany.Trim() != string.Empty)
                memberExtendedInfo.LastEmployer = CurrentCompany;
            else
            {
                baseparser.GetCurrentCompany(out OutValue); memberExtendedInfo.LastEmployer = OutValue;
            }
            if (totalyearsofExp.Trim() != string.Empty)
                memberExtendedInfo.TotalExperienceYears = totalyearsofExp;
            else
            {
                baseparser.GetTotalExperience(out dblValue);
                if (dblValue >= 0) memberExtendedInfo.TotalExperienceYears = dblValue.ToString();
            }
            if (currentCTC.Trim() != string.Empty)
            {
                try
                {
                    //code modify by pravin khot on 11/Aug/2016**************
                    double d = double.Parse(currentCTC.ToString(), System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
                    memberExtendedInfo.CurrentYearlyRate = Convert.ToDecimal(d);
                   // memberExtendedInfo.CurrentYearlyRate = Convert.ToDecimal(currentCTC);
                    //***************END**************************************
                    memberExtendedInfo.CurrentYearlyCurrencyLookupId = currentCTCCurrency;
                    memberExtendedInfo.CurrentSalaryPayCycle = CurrentCTCPayCycle;
                }
                catch
                {
                }
            }
            else
            {
                baseparser.GetCurrentSalary(out dblValue, out OutValue);             
               memberExtendedInfo.CurrentYearlyRate = dblValue != 0 ? Convert.ToDecimal(dblValue) : 0;
                IList<GenericLookup> lookup = new List<GenericLookup>();
                try { lookup = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.Currency, OutValue.Trim()); }
                catch { }
                if (lookup != null)
                {
                    foreach (GenericLookup look in lookup)
                    {
                        if (look.Name.Contains(OutValue) == true)
                            memberExtendedInfo.CurrentYearlyCurrencyLookupId = look.Id;
                    }
                }
            }

            if (ExpectedCTC.Trim() != string.Empty)
            {
                try
                {             
                    //code modify by pravin khot on 11/Aug/2016**************      
                    double d = double.Parse(ExpectedCTC.ToString(), System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
                    memberExtendedInfo.ExpectedYearlyRate = Convert.ToDecimal(d);                   
                    //memberExtendedInfo.ExpectedYearlyRate   = Convert.ToDecimal(ExpectedCTC );
                    //***************END**************************************
                    memberExtendedInfo.ExpectedYearlyCurrencyLookupId = ExpectedCTCCurrency ;
                    memberExtendedInfo.ExpectedSalaryPayCycle  = ExpectedCTCPayCycle ;
                }
                catch
                {
                }
            }
            else
            {
                baseparser.GetRequiredSalary(out dblValue, out OutValue);
                memberExtendedInfo.ExpectedYearlyRate = dblValue != 0 ? Convert.ToDecimal(dblValue) : 0;
                IList<GenericLookup> lookup = new List<GenericLookup>();
                try {
                  
                    lookup = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.Currency, OutValue.Trim()); }
                catch { }
                if (lookup != null)
                {
                    foreach (GenericLookup look in lookup)
                    {
                        if (look.Name.Contains(OutValue) == true)
                            memberExtendedInfo.ExpectedYearlyCurrencyLookupId = look.Id;
                    }
                }
            }
            memberExtendedInfo.PassportStatus = baseparser.GetPassportstatus();
            memberExtendedInfo.SecurityClearance = baseparser.GetSecurityClearance(out OutValue);
            memberExtendedInfo.SourceLookupId = sourceid;
            memberExtendedInfo.SourceDescription = sourcedescription;
            memberExtendedInfo.IdCardLookUpId = IdCardLookUpId;
            memberExtendedInfo.IdCardDetail = IdCardDetail;
            memberExtendedInfo.LinkedinProfile = LinkedInProfile;
            if (memberExtendedInfo.Id > 0) Facade.UpdateMemberExtendedInformation(memberExtendedInfo);
            else Facade.AddMemberExtendedInformation(memberExtendedInfo);
        }
        public void BuildMemberObjectiveAndSummary(ParserLibrary.BaseParser baseparser, MemberObjectiveAndSummary memberObjectiveAndSummary)
        {
            memberObjectiveAndSummary.MemberId = _memberId;
            string OutValue = string.Empty;
            baseparser.GetObjective(out OutValue); memberObjectiveAndSummary.Objective = OutValue;
            baseparser.GetSummary(out OutValue); memberObjectiveAndSummary.Summary = OutValue;
            if (memberObjectiveAndSummary.Id > 0) Facade.UpdateMemberObjectiveAndSummary(memberObjectiveAndSummary);
            else Facade.UpdateMemberObjectiveAndSummary(memberObjectiveAndSummary);
        }
        public void BuildMemberEducation(ParserLibrary.BaseParser baseparser)
        {
            DataTable EducatationInfo = new DataTable();
            baseparser.GetEducation(out EducatationInfo);
            IList<MemberEducation> membereducation = new List<MemberEducation>();
            if (membereducation != null && EducatationInfo !=null)
            {
                for (int i = 0; i < EducatationInfo.Rows.Count; i++)
                {
                    MemberEducation m_memberEducation = new MemberEducation();
                    m_memberEducation.MemberId = _memberId;
                    m_memberEducation.DegreeTitle = EducatationInfo.Rows[i][1].ToString();
                    m_memberEducation.InstituteName = EducatationInfo.Rows[i][8].ToString();
                    m_memberEducation.DegreeObtainedYear = EducatationInfo.Rows[i][7].ToString();
                    m_memberEducation.MajorSubjects = EducatationInfo.Rows[i][2].ToString();
                    m_memberEducation.LevelOfEducationLookupId = Convert.ToInt32(EducatationInfo.Rows[i][14]);
                    m_memberEducation.GPA = EducatationInfo.Rows[i][3].ToString();
                    m_memberEducation.GpaOutOf = EducatationInfo.Rows[i][4].ToString();
                    m_memberEducation.AttendedFrom = EducatationInfo.Rows[i][5].ToString() == "" ? Convert.ToDateTime("01/01/1753") : Convert.ToDateTime(EducatationInfo.Rows[i][5]);
                    m_memberEducation.AttendedTo = EducatationInfo.Rows[i][6].ToString() == "" ? Convert.ToDateTime("01/01/1753") : Convert.ToDateTime(EducatationInfo.Rows[i][6]);
                    m_memberEducation.InstituteCountryId = Convert.ToInt32(EducatationInfo.Rows[i][15]);
                    m_memberEducation.InstituteCity = EducatationInfo.Rows[i][11].ToString();
                    m_memberEducation.BriefDescription = EducatationInfo.Rows[i][12].ToString();
                    m_memberEducation.IsHighestEducation = Convert.ToBoolean(EducatationInfo.Rows[i][13]);
                    m_memberEducation.StateId = Convert.ToInt32(EducatationInfo.Rows[i][16]);
                    Facade.AddMemberEducation(m_memberEducation);
                }
            }
        }
        public void AddSkillMap(ParserLibrary.BaseParser baseparser)
        {
            DataTable SkillMap = new DataTable();
            baseparser.GetSkillsets(out SkillMap);
            for (int i = 0; i < SkillMap.Rows.Count; i++)
            {
                MemberSkillMap memberskill = new MemberSkillMap();
                memberskill.MemberId = _memberId;
                int skillid = Facade.GetSkillIdBySkillName(SkillMap.Rows[i][0].ToString());
                if (skillid == 0)
                {
                    Skill newskill = new Skill();
                    newskill.Name = SkillMap.Rows[i][0].ToString();
                    skillid = (Facade.AddSkill(newskill)).Id;
                }
                memberskill.SkillId = skillid;
                memberskill.YearsOfExperience = Convert.ToInt32(Math.Round(Convert.ToDouble(SkillMap.Rows[i][1].ToString()), 0));
                memberskill.LastUsed = SkillMap.Rows[i][2].ToString().Trim() == string.Empty ? DateTime.MinValue : new DateTime(Convert.ToInt32(SkillMap.Rows[i][2].ToString()), 01, 01);//= SkillMap.Rows[i][2].ToString() == string.Empty ? DateTime.MinValue : DateTime.Now.AddYears(Convert.ToInt32(SkillMap.Rows[i][2].ToString()) - DateTime.Now.Year);
                Facade.AddMemberSkillMap(memberskill);
            }
        }
        public  void AddCertificationMap(ParserLibrary.BaseParser baseparser)
        {
            DataTable certicationmap = new DataTable();
            baseparser.GetCertification(out certicationmap);
            for (int i = 0; i < certicationmap.Rows.Count; i++)
            {
                MemberCertificationMap memberCertificationMap = new MemberCertificationMap();
                memberCertificationMap.MemberId = _memberId;
                memberCertificationMap.CerttificationName = certicationmap.Rows[i][0].ToString();
                if (certicationmap.Rows[i][1] != System.DBNull.Value && certicationmap.Rows[i][1].ToString().Trim() != "") memberCertificationMap.ValidFrom = Convert.ToDateTime(certicationmap.Rows[i][1].ToString());
                if (certicationmap.Rows[i][2] != System.DBNull.Value && certicationmap.Rows[i][2].ToString().Trim() != "") memberCertificationMap.ValidTo = Convert.ToDateTime(certicationmap.Rows[i][2].ToString());
                memberCertificationMap.IssuingAthority = certicationmap.Rows[i][3].ToString();
                Facade.AddMemberCertificationMap(memberCertificationMap);
            }

        }
        public enum OutputTypes
        {
            PlainText = 0,
            NormalizedText = 1,
            HtmlFormatted = 2,
            HtmlPlain = 3,
            Rtf = 4,
            WordXml = 5,
        }
    }
}
