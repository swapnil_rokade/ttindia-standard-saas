/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MiscUtil.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sep-16-2008           Yogeesh Bhat        Defect Id: 8691; Changes made in GetRequistionPreviewPageLink() method.
                                                             (changed UrlConstants.PARAM_ID to  UrlConstants.PARAM_JOB_ID)
    0.2            Sep-26-2008           Shivanand           Defect Id: 8792: In the method PopulateCompanyStatusForChangeRequest(), the 
                                                             code to bind item "Partner" to the dropdown-"New status" is commented.
    0.3            Jan-23-2009           N.Srilakshmi        In PopulateMemberListByRole method this line is added.
                                                             list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);   
    0.4            Feb-05-2009           N.Srilakshmi        Defect Id: 9838; added static method PopulatePaymentType 
    0.5            Feb-24-2009           Yogeesh Bhat        Defect Id: 9997; Added new overloaded method LoadAllControlState()
    0.6            Feb-27-2009           Nagarathna V.B      Defect ID: 9955; changes are made in PopulateSkillSetView().changed column from 4 to 3
    0.7            Mar-06-2009           Jagadish            Defect ID: 9884; Changes are made in method 'ShowMessage()'.
    0.8            Mar-18-2009           Jagadish            Defect ID: 8692 and 8693; Change made in method 'ShowMessage(IsFloat)'.
    0.9            Mar-19-2009          N.Srilakshmi         Defect Id:10089 - Changes made in GetMemberDocumentPath
    1.0            Apr-25-2009           Shivanand           Defect #8911; Changes made in method ShowMessage().
    1.1            May-06-2009          N.Srilakshmi         Defect Id:10417 - Changes made in ShowMessage()
 *  1.2            May-12-2009           Sandeesh            Defect id:10440 :Populated the Requisition status dropdown list from the database
    1.3            Jun-10-2009             Veda              Defect id:10528 :Files are getting deleted after the mail sent.
    1.4            Jul-01-2009          Nagarathna V.B       Enhancement 10778 :changed in "PopulateRequistionStatus()" commented Please select in drop down.
    1.5            Aug-5-2009           Gopala Swamy J       Enhancement 10944 :Changed If condition
    1.6            Sep-24-2009             Veda              Defect id:11553:If a candidate's internal rating is undefined, the default should be "None" instead of the current "-1". 
 * 1.7              Feb-24-2010         Nagarathna V.B       Enhancement 12140; PopulateCandidateType();
 * 1.8             Apr-09-2010          Basavaraj A          Defect 12268 , added TPS360.WebUI 
 * 1.9             Apr-16-2010          Sudarshan.R.         Defect Id:12520    Added n/a if state parameter is blank in GetLocation().
 * 2.0             May-07-2010          Basavaraj A          Defect Id:12581, Added the method 'GetCandidateEducationById' 
 * 2.1             20/Jul/2015          Prasanth Kumar G     Introduced Region for Interviewer (PopulateInterviewerDocumentyType)
 * 2.2             16/Oct/2015          Prasanth Kumar G     Introduced QuestionBankType, AnswerType
 * 2.4             02/Dec/2015          pravin khot          code added pravin khot using fetching user name for InterviewPanel 02/Dec/2015 
 * 2.5             09/Dec/2015          pravin khot          code added pravin khot PopulateSuggestedInterviewerWithEmail fetch Suggested Interviewer name using Interview schedule
   2.6             23/Dec/2015          Prasanth Kumar G     Introduced function GetMemberInterviewFeedbackDocumentPath
 * 2.7             3/March/2016         pravin khot          Introduced function PopulateCustomUser.
 * 2.8             7/March/2016         pravin khot          Introduced function PopulateMemberListWithEmailByRoleIfExisting 
 * 2.9             16/March/2016        pravin khot          added new GetAllMemberNameByTeamMemberId
 * 3.0             24/May/2016          pravin khot          added new -PopulateVenueMaster
 * 3.1              8/June/2016         pravin khot          added new-GetAllByCleintIdByCandidateId
 * 3.2             1/Mar/2017           Sumit Sonawane       changed MoveApplicantToHiringLevel
 * 3.3             20Jan2017           Prasanth Kumar        issue Id : 1153  Introduced Function Check_FileExtension_Permission
 * 3.4             17Feb2017           Prasanth Kumar        iisue id 1153 Introduced Function Check_FileMIMEType
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using TPS360.BusinessFacade;
using TPS360.Common;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Configuration;
using AjaxControlToolkit;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml;
namespace TPS360.Web.UI
{
    public static class MiscUtil
    {       
        public static BusinessFacade.IFacade CreateFacade()
        {
            return new BusinessFacade.Facade();
        }

        public static string BuildFileName(string charName)
        {
            string file_Name = string.Empty;
            for (int i = 0; i < charName.Length; i++)
            {
                if (char.IsLetter(charName[i]) || charName[i] == '-' || charName[i] == '.' || char.IsNumber(charName[i]))
                {
                    file_Name = file_Name + charName[i].ToString();
                }
            }
            return file_Name;
        }
        
        public static void PopulateCountry(ListControl[] list, IFacade facade)
        {
            IList<Country> stateList = facade.GetAllCountry();
            foreach (ListControl li in list)
            {
                li.Items.Clear();
                li.DataSource = stateList;
                li.DataTextField = "Name";
                li.DataValueField = "Id";
                li.DataBind();
                li.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
                string defaultCountry = ConfigurationManager.AppSettings["defaultCountry"].ToString();
                ControlHelper.SelectListByText(li, defaultCountry);
            }
        }
        public static void PopulateCountry(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            IList<Country> stateList = facade.GetAllCountry();
            list.DataSource = stateList;
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
            string defaultCountry = ConfigurationManager.AppSettings["defaultCountry"].ToString();
            ControlHelper.SelectListByText(list, defaultCountry);
        }
      
        //************Code added by pravin khot on 16/May/2016*******
        public static void PopulateMemberListByTeamMemberId(ListControl list, string roleName, int memberid, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllMemberNameByTeamMemberId(roleName, memberid);
            list.DataTextField = "FirstName";
            list.DataValueField = "Id";
            list.DataBind();
            list = RemoveScriptForDropDown(list);
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ALL, "0"));   // Integer conversion error handled.
        }
        //*****************************END***********************
        public static void PopulateBussinessType(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            IList<GenericLookup> genericLookup = facade.GetAllGenericLookup();
            list.DataSource = genericLookup;
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        public static IList<State >  getstate(int countryId, IFacade facade)
        {
            string list = "<country>";
            IList<State> stateList = facade.GetAllStateByCountryId(countryId);
            return  stateList;
        }
        public static void EventLogForRequisition(EventLogForRequisition ActionType,int JobPostingId,int CreatorId,IFacade facade)
        {
            EventLogForRequisitionAndCandidate log = new EventLogForRequisitionAndCandidate();
            log.ActionType = EnumHelper.GetDescription(ActionType);
            log.JobPostingID = JobPostingId;
            log.CreatorId  = CreatorId ;
            facade.AddEventLog(log, "0");
        }
        public static void EventLogForCandidate(EventLogForCandidate ActionType, int JobPostingId, string CandidateId, int CreatorId, IFacade facade)
        {
            EventLogForRequisitionAndCandidate log = new EventLogForRequisitionAndCandidate();
            log.ActionType = EnumHelper.GetDescription(ActionType);
            log.JobPostingID = JobPostingId;
            log.CreatorId = CreatorId;
            facade.AddEventLog(log, CandidateId);
        }
        public static void EventLogForCandidateStatus(EventLogForCandidate ActionType, int JobPostingId, string CandidateId, int CreatorId,string Status, IFacade facade)
        {
            EventLogForRequisitionAndCandidate log = new EventLogForRequisitionAndCandidate();
            log.ActionType = EnumHelper.GetDescription(ActionType);
            if (log.ActionType.Contains("<new status level>"))
                log.ActionType = log.ActionType.Replace("<new status level>", Status);
            log.JobPostingID = JobPostingId;
            log.CreatorId = CreatorId;
            facade.AddEventLog(log, CandidateId);
        }
        public static void PopulateState(ListControl list, int countryId, IFacade facade)
        {
            list.Items.Clear();
            IList<State> stateList = facade.GetAllStateByCountryId(countryId);
            if (stateList != null)
            {
                try
                {
                    list.DataSource = stateList;
                    list.DataTextField = "Name";
                    list.DataValueField = "Id";
                    list.DataBind();
                }
                catch
                {
                }
            }
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        public static void PopulateMemberByRole(ListControl list, int roleId, IFacade facade)
        {
            list.Items.Clear();
            IList<Member> memberList = facade.GetAllMemberByCustomrRoleId(roleId);
            if (memberList != null)
            {
                try
                {

                    list.DataSource = memberList;
                    list.DataTextField = "FirstName";
                    list.DataValueField = "Id";
                    list.DataBind();
                }
                catch
                {
                }
            }
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static void SelectDefaultCountry(ListControl list)
        {
            ControlHelper.SelectListByText(list, "United States");
        }

        public static void PopulateParentSiteMap(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllParentCustomSiteMap();
            list.DataTextField = "Title";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ROOT, "0"));
        }

        public static void PopulateParentSiteMapTree(ListControl list, SiteMapDataSource source)
        {
            list.Items.Clear();
            SiteMapNodeCollection nodeList = source.Provider.GetChildNodes(source.Provider.RootNode);
            PopulateParentSiteMapTree(list, nodeList, string.Empty);
        }

        public static void PopulateParentSiteMapTree(ListControl list, SiteMapNodeCollection nodeList, string prefix)
        {
            if (nodeList != null && nodeList.Count > 0)
            {
                foreach (SiteMapNode node in nodeList)
                {
                    if (string.IsNullOrEmpty(node.Url))
                    {
                        list.Items.Add(new ListItem(prefix + node.Title, StringHelper.Convert(node["Id"])));

                        if (node.HasChildNodes)
                        {
                            string oldPrefix = prefix;
                            prefix = prefix + "----";
                            PopulateParentSiteMapTree(list, node.ChildNodes, prefix);
                            prefix = oldPrefix;
                        }
                    }
                }
            }
        }

        //public static void PopulateMemberTier(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.PositionTier);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}
        

        //public static void PopulateResourceGroup(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.ResourceGroup);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        //}

        //public static void PopulateTimeSheetList(ListControl list, IFacade facade,int MemberId)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetMemberTimeSheetByMemberId(MemberId);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        //}

        public static void PopulateCustomRole(ListControl list, IFacade facade)
        {
            list.Items.Clear();

            list.DataSource = facade.GetAllCustomRole();
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            
            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT); 
        }
        //*********Pravin khot � Candidate Hiring Status Update � 3/March/2017� Start *****************            
        public static void PopulateCustomUserRole(ListControl list, IFacade facade)
        {
            list.Items.Clear();

            list.DataSource = facade.GetAllCustomUserRole();
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();

            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        }
        //*********Pravin khot � Candidate Hiring Status Update � 3/March/2017 � End *****************
        //********Code added by pravin khot on 3/March/2016*********START*********
        public static void PopulateCustomUser(ListControl list, IFacade facade)
        {
            list.Items.Clear();

            list.DataSource = facade.GetAllCustomUser();
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            
            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT); 
        }
        //*******************************END********************************************

        //To populate Category
        public static void PopulateCategory(ListControl checklist, IFacade facade)
        {
            IList<Category> category = facade.GetAllCategory();
            if (category != null)
            {
                try
                {
                    checklist.DataSource = category;
                    checklist.DataTextField = "Name";
                    checklist.DataValueField = "Id";
                    checklist.DataBind();
                }
                catch
                {
                }
            }
            ListItem item = new ListItem();
            item.Value = "0";
            item.Text = "Please Select";
            checklist.Items.Insert(0, item);
        }
        //public static void PopulateOfficeType(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.OfficeType);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        //}

        //public static void PopulateGuestHouseType(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.GuestHouse);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateExpenseCategory(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.ExpenseCategory);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    //list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        //    list.Items.Insert(0, new ListItem("Please select", "0"));
        //}

        //public static void PopulateCampaignStatus(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.CampaignStatus);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        //}

        //public static void PopulateColdCallScript(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllCampaignColdCallScript();
        //    list.DataTextField = "Subject";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        //}

        //public static void PopulateContent(ListControl list, IFacade facade, int contentCategory)
        //{
        //    list.Items.Clear();

        //    IList<TPS360.Common.BusinessEntities.Content> contentList = null;

        //    if (contentCategory == 0)
        //    {
        //        contentList = facade.GetAllContent();
        //    }
        //    else
        //    {
        //        contentList = facade.GetAllContentByCategory((ContentCategory)contentCategory);
        //    }

        //    list.DataSource = contentList;
        //    list.DataTextField = "Subject";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        //}

        //public static void PopulateProduct(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllProduct();
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        public static void PopulateCultureCode(ListControl list, IFacade facade)
        {
            //list.Items.Clear();

            //list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.CultureCode);
            //list.DataTextField = "Name";
            //list.DataValueField = "Id";
            //list.DataBind();

            //list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        }

        public static void PopulateLookupType(ListControl list, IFacade facade)
        {
            list.Items.Clear();

            Type lookupType = typeof(LookupType);

            ControlHelper.PopulateEnumIntoList(list, lookupType);

            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        }
        public static string ChangeDatePattern(string  date)
        {
            string from = ((date == null || date == string.Empty) ? string.Empty : (Convert.ToDateTime(date.Split('/')[1] + "/" + date.Split('/')[0] + "/" + date.Split('/')[2])).ToShortDateString()).ToString();
            return from;
        }

        public static void AddStatusMessageToSession(System.Web.SessionState.HttpSessionState session, string message)
        {
            session["StatusMessage"] = message;
        }
        public static void ShowStatusMessageFromSession(System.Web.SessionState.HttpSessionState session,Label label, IFacade facade)
        {
            if(session["StatusMessage"]!=null &&session["StatusMessage"]!=string .Empty )
                ShowMessage(label, Convert.ToString(session["StatusMessage"]), false);
            session["StatusMessage"] = "";
        }
        public static void ShowMessage(Label label, string message, bool isError)
        {  
            string strInterval = string.Empty;

            AlwaysVisibleControlExtender avce = new AlwaysVisibleControlExtender();
            avce.TargetControlID = label.ID;
            avce.ID = "avce";
            avce.ScrollEffectDuration = .1f;
            //label.ForeColor = System.Drawing.Color.Black;

            label.Text = "";
            label.Style .Add ("width","40%");
            label.Style .Add ("textAlign","center");
            label.Style .Add ("position","fixed");
            label.Style .Add ("top","10px");
            label.Style .Add ("left","0");
            label.Style.Add("margin-left", "30%");


            label.Text += "<div id=\"showMessageContainer\" style=\"width: 100%;margin: 10px auto;\" class=\"alert" + (!isError ? " alert-info" : "") + "\">";
            label.Text += message;
            label.Text += "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">\u00D7</button>";
            label.Text += "</div>";


            //label.Text = "<div id=\"divParent\" style=\"width: 100%;\">";
            //label.Text += "<div style=\"float: left; width: 80%; text-align: left;\">";
            //label.Text += "<img src=\"../Images/messagebox_info.png\" />";
            //label.Text += "  " + "" + message + "";
            //label.Text += "</div>";

            //label.Text += "<div style=\"float: right; width: 18%; text-align: right; padding-right: 10px; white-space: nowrap\">";
            //label.Text += " <a href=\"javascript:void(0)\" onclick=\"javascript:hideMeClick('" + label.ClientID + "')\" title=\"Hide Me\">Hide Me</a></div>";
            //label.Text += "</div>";
            strInterval = ConfigurationManager.AppSettings["AlertInterval"].ToString();
            ScriptManager.RegisterClientScriptBlock(label, typeof(MiscUtil), "MiscUtil",
            "window.setTimeout(function(){hideMeClick('" + label.ClientID + "'); }, '" + strInterval + "')", true);
            //window.setTimeout(function(){hideMeClick('" + label.ClientID + "'); }, '" + strInterval + "')
            //if (isError)
            //{
            //    label.ForeColor = System.Drawing.Color.Red;
            //    label.Font.Bold = true;
            //}

            try
            {
            //  AlwaysVisibleControlExtender newavce = (AlwaysVisibleControlExtender)label.Parent.FindControl("avce");
            //  if (newavce != null) label.Parent.Controls.Remove(newavce);
            label.Parent.Controls.Add(avce);
            }
            catch (Exception ex)
            {
            label.Style.Add("Right", "10px"); // 1.0
            label.Style.Add("Top", "10px"); // 1.0  
            }                

            //label.CssClass = "CommonAlertMessage";
            
        }           

        public static void ShowMessage(Label label, string message, bool isError, bool IsFloat)
        {
            string strInterval = string.Empty;
            try
            {
                AlwaysVisibleControlExtender avce = new AlwaysVisibleControlExtender();
                avce.TargetControlID = label.ID;
                avce.ScrollEffectDuration = .1f;
                label.Parent.Controls.Add(avce);
                label.ForeColor = System.Drawing.Color.Black;

                label.Text = "<div id=\"divParent\" style=\"width: 100%; padding: 5px;\">";
                label.Text += "<div style=\"float: left; width: 65%; text-align: left;\">";
                //label.Text += "<img src=\"../Images/messagebox_info.png\" />"; //0.8
                label.Text += "<img src=\"Images/messagebox_info.png\" />";
                label.Text += "  " + "" + message + "";
                label.Text += "</div>";

                label.Text += "<div style=\"float: right; width: 30%; text-align: right;\">";
                label.Text += " <a href=\"javascript:void(0)\" onclick=\"javascript:hideMeClick('" + label.ClientID + "')\" title=\"Hide Me\">Hide Me</a></div>";
                label.Text += "</div>";
                strInterval = ConfigurationManager.AppSettings["AlertInterval"].ToString();
                ScriptManager.RegisterClientScriptBlock(label, typeof(MiscUtil), "MiscUtil",
                   "window.setTimeout(function(){hideMeClick('" + label.ClientID + "');}, '" + strInterval + "')", true);

                if (isError)
                {
                    label.ForeColor = System.Drawing.Color.Red;
                    label.Font.Bold = true;
                }

                label.CssClass = "CommonAlertMessage";
            }
            catch (Exception ex)
            {
                label.ForeColor = System.Drawing.Color.DodgerBlue;
                label.Text = message;
                if (isError)
                {
                    label.ForeColor = System.Drawing.Color.Red;
                    label.Font.Bold = true;
                }
            }   
        }

        //public static void ResetMessage(object sender, EventArgs e)
        //{
        //    //Label label = sender as Label;
        //    //label.Visible = false;
        //}

        public static void ResetMessage(Label label)
        {
            label.Text = string.Empty;
            label.ForeColor = System.Drawing.Color.Black;
            label.Font.Bold = false;
        }

        public static bool IsValidDate(DateTime value)
        {
            if ((value == DateTime.MinValue) || (value == DateTime.MaxValue))
            {
                return false;
            }

            return true;
        }

        public static Control GetPostBackControl(Page page)
        {
            Control control = null;

            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if (ctrlname != null && ctrlname != string.Empty)
            {
                control = page.FindControl(ctrlname);
            }
            else
            {
                foreach (string ctl in page.Request.Form)
                {
                    Control c = null;

                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        c = page.FindControl(ctl.Substring(0, ctl.Length - 2));

                        if (c == null)
                        {
                            c = page.Master.FindControl(ctl.Substring(0, ctl.Length - 2));
                        }
                    }
                    else
                    {
                        c = page.FindControl(ctl);

                        if (c == null)
                        {
                            c = page.Master.FindControl(ctl.Substring(0, ctl.Length - 2));
                        }
                    }

                    if (c is System.Web.UI.WebControls.Button || c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            return control;
        }

        public static void PopulateWorkAuthorization(ListControl list, IFacade facade)
        {
            list.Items.Clear();

            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.WorkAuthorizationType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();

           // list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        public static void PopulatePriority(ListControl list, IFacade facade)
        {
            list.Items.Clear();

            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.Priority);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();

            // list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        public static void PopulateWorkSchedule(ListControl list, IFacade facade)
        {
            list.Items.Clear();

            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.WorkSchedule);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();

             list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static void PopulateSalesRegion(ListControl list, IFacade facade) 
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.SalesRegion);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
            
        }
        public static void PopulateJobCategory(ListControl list, IFacade facade) 
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.JobCategory);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        
        }
        public static void PopulateSalesGroup(ListControl list, IFacade facade) 
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.SalesGroup);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));

        }
        public static void PopulateRequisitionType(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.RequisitionType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));

        }

        /*public static void PopulateRequestionBranch(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.RequestionBranch);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));

        }

        public static void PopulateRequestionGrade(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.RequestionGrade);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));

        }*/



        public static void PopulateEmploymentType(ListControl list, IFacade facade) 
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.EmploymentType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        
        }
        public static void PopulateJobLocation(ListControl list, IFacade facade) 
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.EmploymentType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        
        }
        public static void PopulateCity(ListControl list, IFacade facade) 
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.JobLocation);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        
        }

        public static void PopulateEducationQualification(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.EducationalQualificationType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_QUALIFICATION, "0"));
        }
        //*********Code added by pravin khot on 8/March/2017*************
        public static void PopulateDivisionInterviewers(ListControl list, int divisionValue, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllCompanyContactByComapnyId(divisionValue);
            list.DataTextField = "FirstName";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        //********************END************************************
        public static void PopulateReasonForRejectionList(ListControl list,IFacade facade) 
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.ReasonForRejection);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        
        }


        //public static void PopulateFieldOfStudy(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.FieldOfStudy);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        public static void PopulateEmploymentDuration(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.EmploymentDurationType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();

            if (list.GetType().Name != "CheckBoxList")
            {
                list.Items.Insert(0, new ListItem("No Preference", "0"));    
            }
        }

        public static void PopulateDuration(ListControl list, IFacade facade)     //yogeesh
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.JobDurationType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();

            if (list.GetType().Name != "CheckBoxList")
            {
                list.Items.Insert(0, new ListItem("Select Duration", "0"));
            }
        }

        //public static void PopulateJobCategoryType(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.JobCategoryType);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_CATEGORY, "0"));
        //}

        //public static void PopulateJobTitle(ListControl list, IFacade facade, int _memberId)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllMemberPositionByMemberId(_memberId);
        //    list.DataTextField = "PositionName";
        //    list.DataValueField = "PositionId";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        public static void PopulateJobPosting(ListControl list, IFacade facade, int _memberId)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllJobPostingByMemberId(_memberId);
            list.DataTextField = "JobTitle";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem("Please select job posting", "0"));
        }
        //added by pravin khot on 8/June/2016**************
        public static void GetAllByCleintIdByCandidatesId(ListControl list, IFacade facade, int ClientId, int CandidateId)
        {
            //list.Items.Clear();
            //ArrayList jobList;
            //jobList = facade.GetAllJobPostingByCandidatesId(ClientId, CandidateId);
            //if (jobList == null) jobList = new ArrayList();
            //list.SelectedValue = "0";
            //jobList.Insert(0, new JobPosting() { Id = 0, JobTitle = (jobList.Count > 0 ? UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_JOBPOSTING : "No Requisitions") });
            //list.DataSource = jobList;
            //list.DataValueField = "Id";
            //list.DataTextField = "JobTitle";
            //list.DataBind();
            ////list.Items.Insert(0, new ListItem((list.Items.Count > 0 ? UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_JOBPOSTING : "No Requisitions"), "0"));
            //if (list.Items.Count == 1) list.Enabled = false;
            //else list.Enabled = true;

            list.Items.Clear();
            ArrayList jobList;
            jobList = facade.GetAllJobPostingByCandidatesId(ClientId, CandidateId);
            if (jobList != null)
            {
                try
                {
                    list.DataSource = jobList;
                    list.DataTextField = "JobTitle";
                    list.DataValueField = "Id";
                    list.DataBind();
                }
                catch
                {
                }
            }
            ListItem item = new ListItem();
            item.Value = "0";
            item.Text = "Please select job posting";
            //list.Items.Insert(0, item);
            list.Items.Insert(0, new ListItem((list.Items.Count > 0 ? UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_JOBPOSTING : "No Requisitions"), "0"));
            if (list.Items.Count == 1) list.Enabled = false;
            else list.Enabled = true;

            //list.DataSource = facade.GetAllJobPostingByCandidatesId(ClientId, CandidateId);
            //list.DataTextField = "JobTitle";
            //list.DataValueField = "Id";
            //list.DataBind();
            //list.Items.Insert(0, new ListItem("Please select job posting", "0"));
            //if (list.Items.Count == 1) list.Enabled = false;
            //else list.Enabled = true;
        }
        //*******************END************************************
        public static void PopulateJobPostingByClientIdAndManagerId(ListControl list, IFacade facade, int ClientId, int ManagerId)
        {
           
            list.Items.Clear();
             ArrayList jobList;
             if (ManagerId > 0)
             {
                 if (ClientId > 0)
                     jobList = facade.GetAllJobPostingByClientIdAndManagerId(ClientId, ManagerId);
                 else jobList = facade.GetAllJobPostingByStatusAndManagerId(0, ManagerId);
             }
             else
             {
                 if(ClientId >0)
                    jobList = facade.GetAllJobPostingByClientId(ClientId);
                 else jobList = facade.GetAllJobPostingByStatus(0);
             }
             if (jobList == null) jobList = new ArrayList();

             list.SelectedValue = "0";
             jobList.Insert (0,new JobPosting() { Id = 0, JobTitle = (jobList .Count  > 0 ? UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_JOBPOSTING : "No Requisitions") });
            list.DataSource = jobList;
            list.DataValueField = "Id";
            list.DataTextField = "JobTitle";
            list.DataBind();
            //list.Items.Insert(0, new ListItem((list.Items.Count > 0 ? UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_JOBPOSTING : "No Requisitions"), "0"));
            if (list.Items.Count == 1) list.Enabled = false ;
            else list.Enabled = true;
        }
        public static void PopulateJobPosting(ListControl list, IFacade facade, JobStatus jobStatus, int _memberId)
        {
            list.Items.Clear();
            //int JobStatusid=0;
            //IList<GenericLookup> RequisitionStatusList = facade.GetAllGenericLookupByLookupTypeAndName(LookupType.RequisitionStatus, jobStatus.ToString());
            //if (RequisitionStatusList != null)
            //{
            //     JobStatusid = RequisitionStatusList[0].Id;
            //}
            if (_memberId == 0)
            {               
                list.DataSource = facade.GetAllJobPostingByStatus(0);//1.2
            }
            else
            {
                list.DataSource = facade.GetAllJobPostingByStatusAndManagerId(0, _memberId);//1.2
            }
            list.DataTextField = "JobTitle";
            list.DataValueField = "Id";
            list.DataBind();
            list = RemoveScriptForDropDown(list);
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_JOBPOSTING, "0"));
        }

        public static void PopulateInternalInterviewers(ListControl list, IFacade facade, int _jobpostingId)
        {
            IList<JobPostingHiringTeam> lstHiringTeam = facade.GetAllJobPostingHiringTeamByJobPostingId(_jobpostingId);
            list.Items.Clear();
            if (lstHiringTeam != null)
            {
                foreach (JobPostingHiringTeam hiringteammember in lstHiringTeam)
                {
                    Member member = facade.GetMemberById(hiringteammember.MemberId);
                    if (member != null)
                    {
                        string name = member.FirstName + " " + member.LastName;
                        list.Items.Add(new ListItem(name, hiringteammember.Id.ToString()));
                    }
                }
            }
        }

        public static void PopulateNotesType(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.NotesType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_NOTECATEGORY, "0"));
        }

        public static void PopulateInterviewType(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.InterviewType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT , "0"));
        }
        //*******************Code added by pravin khot on 24/May/2016***************
        public static void PopulateVenueMaster(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.VenueMaster);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        //***************************END**************************
        //Code introduced by Prasanth on 16/Oct/2015 START
        public static void PopulateQuestionBankType(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.QuestionBankType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }


        public static void PopulateAnswerType(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.AnswerType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }



        //*******************END*****************************
        public static void PopulateInterviewRounds(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.InterviewRounds);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static void PopulateInternalRating(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.InternalRating);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_NONE, "0")); //1.6
        }


        //public static void PopulateVHInternalRating(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.VHInternalRating);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //}

        public static void PopulateIndustryType(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.IndustryType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        //public static void PopulateFunctionalCategory(ListControl list, IFacade facade)
        //{
        //    IList<Position> lstPosition = facade.GetAllParentPosition();
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataSource = lstPosition;
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateOrganizationaFunctionalCategory(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.FunctionalCategory);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}
        //12140
        public static void PopulateCandidateType(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.MemberType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static void PopulateGenderType(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.Gender);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }



        public static void PopulateIndustry(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.Industry);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static void PopulateJobFunction(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.JobFunction);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }






        //public static void PopulateBloodGroupType(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.BloodGroup);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        public static void PopulateEthnicGroupType(ListControl list, IFacade facade)
        {
            //list.Items.Clear();
            //list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.EthnicGroup);
            //list.DataTextField = "Name";
            //list.DataValueField = "Id";
            //list.DataBind();
            //list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static void PopulateMaritalStatusType(ListControl list, IFacade facade)
        {

            
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.MaritalStatus);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        //public static void PopulateTaxTerm(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.TaxTermType); 
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //}

        public static void PopulateOccupationalSeries(ListControl list, IFacade facade)
        {
           

            list.Items.Clear();
            IList<OccupationalSeries> occupationalSeries = facade.GetAllOccupationalSeries();
            ListItem it = new ListItem();
            it.Value = "0";
            it.Text = "Please Select";
            list.Items.Add(it);
            foreach (OccupationalSeries occ in occupationalSeries)
            {
                ListItem item = new ListItem();
                item.Value = occ.Id + "";
                item.Text = occ.Id.ToString ("0000") + " - " + occ.SeriesTitle;
                list.Items.Add(item);
            }

        }



        //public static void PopulateTestModuleNameByTestMasterId(ListControl list, IFacade facade, int id)
        //{
        //    list.Items.Clear();
        //    IList<TestModule> modules = facade.GetAllTestModuleByTestMasterId(id);
        //    list.DataSource = modules;
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //}

        //public static void PopulateVisaType(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.VisaType);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_VISATYPE, "0"));
        //}
        //1.2 Start
        public static void PopulateRequistionStatus(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.RequisitionStatusValues);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            //list.Items.Insert(0, new ListItem("Please Select", "0"));//1.4
        }
        //1.2 End
        public static void PopulateBenifits(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.Benifits);
            list.DataTextField = "Name";
            list.DataValueField = "Id";

            list.DataBind();
            // list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_VISATYPE, "0"));
        }
        public static void PopulateManagementBand(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllManagementBand();
            list.DataTextField = "ManagementBand";
            list.DataValueField = "ManagementBandId";

            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static void PopulateLocation(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllLocation();
            list.DataTextField = "City";
            list.DataValueField = "CityId";

            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        public static void PopulateCandidateRequisitionStatus(ListControl list, IFacade facade)
        {
            //list.Items.Clear();
            //list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.CandidateRequisitionStatus).OrderBy (x=>x.SortOrder );
            //list.DataTextField = "Name";
            //list.DataValueField = "Id";
       
            //list.DataBind();
        }
        public static void PopulateCurrency(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.Currency);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_CURRENCY, "0"));
            string defaultCurrency = ConfigurationManager.AppSettings["defaultCurrency"].ToString();
            ControlHelper.SelectListByText(list, defaultCurrency);
        }

        public static void PopulateFollowUpAction(ListControl list, IFacade facade)
        {
            //list.Items.Clear();
            //list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.FollowupAction);
            //list.DataTextField = "Name";
            //list.DataValueField = "Id";
            //list.DataBind();
            //list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static void PopulateCompanySize(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.CompanySize);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static void PopulateOwnerType(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.OwnerType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static void PopulateQuestionCategory(ListControl list, IFacade facade)
        {
            //list.Items.Clear();
            //list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.QueryCheckCategory);
            //list.DataTextField = "Name";
            //list.DataValueField = "Id";
            //list.DataBind();
            //list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static void PopulateCompanyByCompanyStatus(ListControl list,CompanyStatus companyStatus, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllCompanyNameByCompanyStatus(companyStatus);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            InsertPleaseSelect(list);
        }

        public static void PopulateCompanyNoteType(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.CompanyNotesType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            InsertPleaseSelect(list);
        }

        public static void PopulateCampaignNoteType(ListControl list, IFacade facade)
        {
            //list.Items.Clear();
            //list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.CampaignNotesType);
            //list.DataTextField = "Name";
            //list.DataValueField = "Id";
            //list.DataBind();
            //InsertPleaseSelect(list);
        }

        //public static void PopulateConsultant(ListControl list,IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllConsultantName();
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    InsertPleaseSelect(list);
        //}


        public static void InsertPleaseSelect(ListControl list)
        {
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static string PopulateSkillSetView(string searchSkill, IFacade facade)
        {
            StringBuilder skillSetTableHTML = new StringBuilder();
            skillSetTableHTML.Append("<table cellSpacing=1 cellPadding=5 width=100% border=0>");
            skillSetTableHTML.Append("<tbody>");

            IList<Skill> parentSkills;
            IList<Skill> childSkills;
            parentSkills = facade.GetAllParentSkill();

            if (parentSkills != null)
            {
                foreach (Skill pskill in parentSkills)
                {
                    int parentSkillId = pskill.Id;
                    string parentSkillName = pskill.Name;
                    childSkills = facade.GetAllSkillByParentId(parentSkillId, searchSkill);

                    if (childSkills != null)
                    {
                        skillSetTableHTML.Append("<tr bgColor=#E7E8E2>");
                        skillSetTableHTML.Append("<td align=left width=90% colSpan=4><b>" + parentSkillName + "</b></td>");//0.6
                        skillSetTableHTML.Append("</tr><tr>");

                        int count = 0;
                        foreach (Skill cskill in childSkills)
                        {
                            int childSkillId = cskill.Id;
                            string childSkillName = cskill.Name;

                            skillSetTableHTML.Append("<td align=left style=white-space:nowrap;width:30%><INPUT onClick='AddId(this)' type='checkbox' value='" + childSkillId + "' name='" + childSkillName + "'>&nbsp;" + childSkillName + "</td>");
                            count++;
                            if (count % 4 == 0)//0.6
                            {
                                skillSetTableHTML.Append("</tr>");
                            }
                        }
                    }
                }
            }
            skillSetTableHTML.Append("</tr>");
            skillSetTableHTML.Append("</tbody>");
            skillSetTableHTML.Append("</table>");
            return skillSetTableHTML.ToString();
        }

        //public static string PopulatePositionsView(Int32 intParentId, string searchPosition, IFacade facade)
        //{
        //    StringBuilder positionTableHTML = new StringBuilder();
        //    positionTableHTML.Append("<table cellSpacing=1 cellPadding=5 width=100% border=0>");
        //    positionTableHTML.Append("<tbody>");

        //    IList<Position> parentPositions;
        //    IList<Position> childPositions;
        //    if (intParentId == 0)
        //    {
        //        parentPositions = facade.GetAllParentPosition();
        //    }
        //    else
        //    {
        //        parentPositions = new List<Position>();
        //        Position currentPosition = facade.GetPositionById(intParentId);
        //        parentPositions.Add(currentPosition);
        //    }

        //    if (parentPositions != null)
        //    {
        //        foreach (Position pposition in parentPositions)
        //        {
        //            int parentPositionId = pposition.Id;
        //            string parentPositionName = pposition.Name;
        //            childPositions = facade.GetAllPositionByParentId(parentPositionId, searchPosition);

        //            if (childPositions != null)
        //            {
        //                positionTableHTML.Append("<tr bgColor=#E7E8E2>");
        //                positionTableHTML.Append("<td align=left width=90% colSpan=5><b>" + parentPositionName + "</b></td>");
        //                positionTableHTML.Append("</tr><tr>");

        //                int count = 0;
        //                foreach (Position cposition in childPositions)
        //                {
        //                    int childPositionId = cposition.Id;
        //                    string childPositionName = cposition.Name;

        //                    positionTableHTML.Append("<td align=left style=white-space:nowrap><INPUT onClick='AddPositionId(this)' type='checkbox' value='" + childPositionId + "' name='" + childPositionName + "'/>&nbsp;" + childPositionName + "</td>");
        //                    count++;
        //                    if (count % 3 == 0)
        //                    {
        //                        positionTableHTML.Append("</tr>");
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    positionTableHTML.Append("</tr>");
        //    positionTableHTML.Append("</tbody>");
        //    positionTableHTML.Append("</table>");
        //    return positionTableHTML.ToString();
        //}

        public static void PopulateMenuType(ListControl list)
        {
            list.Items.Clear();

            Type lookupType = typeof(SiteMapType);

            ControlHelper.PopulateEnumIntoList(list, lookupType);
            list.Items.RemoveAt(0);

            //list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        }

        public static string FileNameCreator(string fileName, string fileExt)
        {
            string encryptFileName = getMd5Hash(fileName);
            encryptFileName = encryptFileName.Substring(0, 7);

            string newFileName =
                fileName
                + "_"
                + System.DateTime.Now.Millisecond.ToString()
                + encryptFileName
                + "."
                + fileExt;
            return newFileName;//
        }

        public static string getMd5Hash(string input)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        public static string GetWorkAuthorizationById(string workAuthorizationTypeLookupIds, IFacade facade)
        {
            string workAuthorization = string.Empty;
            int workAuthorizationLookupId = 0;
            GenericLookup genericlookUp = null;
            string[] workAuth = workAuthorizationTypeLookupIds.Split(new char[] { ',' });

            foreach (string ws in workAuth)
            {
                Int32.TryParse(ws, out workAuthorizationLookupId);
                if (workAuthorizationLookupId > 0)
                {
                    genericlookUp = facade.GetGenericLookupById(workAuthorizationLookupId);
                    if (genericlookUp != null)
                    {
                        if (string.IsNullOrEmpty(workAuthorization))
                        {
                            workAuthorization = genericlookUp.Name;
                        }
                        else
                        {
                            workAuthorization = workAuthorization + ", " + genericlookUp.Name;
                        }
                    }
                }
            }

            return workAuthorization;
        }

        public static string GetBenifitsById(string benifitsLookupIds, IFacade facade)
        {
            string benifits = string.Empty;
            int benifitLookupId = 0;
            GenericLookup genericlookUp = null;
            string[] benifitIds = benifitsLookupIds.Split(new char[] { '!' });

            foreach (string bs in benifitIds)
            {
                Int32.TryParse(bs, out benifitLookupId);
                if (benifitLookupId > 0)
                {
                    genericlookUp = facade.GetGenericLookupById(benifitLookupId);
                    if (genericlookUp != null)
                    {
                        if (string.IsNullOrEmpty(benifits))
                        {
                            benifits = genericlookUp.Name;
                        }
                        else
                        {
                            benifits = benifits + ", " + genericlookUp.Name;
                        }
                    }
                }
            }

            return benifits;
        }

        public static string GetLookupNameById(int genericLookupId, IFacade facade)
        {
            string lookupName = string.Empty;
            if (genericLookupId > 0)
            {
                GenericLookup genericlookUp = facade.GetGenericLookupById(genericLookupId);
                lookupName = (genericlookUp != null ? genericlookUp.Name.ToString() : string.Empty);
            }
            return lookupName;
        }


        //2.0 starts
        public static string GetCandidateEducationById(int MemberId, IFacade facade)
        {
            string lookupName = string.Empty;
            if (MemberId > 0)
            {
                MemberEducation education = facade.GetHighestEducationByMemberId(MemberId);

                  if (education != null)
                  {

                      lookupName = education.DegreeTitle;
                    
                  }
               return lookupName;
            }

            return lookupName;
        }
        // 2.0 ends
        
        public static string GetMemberAvailability(int availability,DateTime availableDate, IFacade facade)
        {
            string lookupName = GetLookupNameById(availability, facade);
            if (availableDate != null && availableDate != DateTime.MinValue)
            {
                if (availability == 316)         //1.5...Previously it was 314 now changed to 316
                {
                    lookupName = lookupName + " " + availableDate.ToShortDateString();
                }
            }
            return lookupName;
        }

        //// Returns all requisition skills in a list with comma seperated
        //public static string GetRequisitionSkillSetList(int jobPostingId, IFacade Facade)
        //{
        //    string skills = string.Empty;
        //    int skillId = 0;
        //    IList<JobPostingSkillSet> jobPostingSkillSetList = Facade.GetAllJobPostingSkillSetByJobPostingId(jobPostingId);

        //    if (jobPostingSkillSetList != null)
        //    {
        //        foreach (JobPostingSkillSet jobPostingSkill in jobPostingSkillSetList)
        //        {
        //            skillId = jobPostingSkill.SkillId;
        //            Skill skill = Facade.GetSkillById(skillId);

        //            if (skill != null)
        //            {
        //                if (string.IsNullOrEmpty(skills))
        //                {
        //                    skills = skill.Name;
        //                }
        //                else
        //                {
        //                    skills = skills + ", " + skill.Name;
        //                }
        //            }
        //        }
        //    }

        //    return skills;
        //}

        public static Size GetImageSize(byte[] content)
        {
            using (MemoryStream ms = new MemoryStream(content))
            {
                using (Bitmap bmp = new Bitmap(ms))
                {
                    return bmp.Size;
                }
            }
        }

        public static Size GetImageSize(string fileName)
        {
            Size size = new Size();

            try
            {
                if (File.Exists(fileName))
                {
                    using (Bitmap bmp = new Bitmap(fileName))
                    {
                        size = bmp.Size;
                    }
                }
            }
            catch (FileNotFoundException)
            {
            }
            catch (Exception)
            {

            }
            return size;
        }

        public static ImageFormat GetImageFormat(string contentType)
        {
            contentType = StringHelper.ToLower(contentType);

            ImageFormat format;

            if (contentType.IndexOf("bmp") > -1)
            {
                format = System.Drawing.Imaging.ImageFormat.Bmp;
            }
            else if (contentType.IndexOf("emf") > -1)
            {
                format = System.Drawing.Imaging.ImageFormat.Emf;
            }
            else if (contentType.IndexOf("exif") > -1)
            {
                format = System.Drawing.Imaging.ImageFormat.Exif;
            }
            else if (contentType.IndexOf("gif") > -1)
            {
                format = System.Drawing.Imaging.ImageFormat.Gif;
            }
            else if (contentType.IndexOf("icon") > -1)
            {
                format = System.Drawing.Imaging.ImageFormat.Icon;
            }
            else if (contentType.IndexOf("png") > -1)
            {
                format = System.Drawing.Imaging.ImageFormat.Png;
            }
            else if (contentType.IndexOf("tiff") > -1)
            {
                format = System.Drawing.Imaging.ImageFormat.Tiff;
            }
            else if (contentType.IndexOf("wmf") > -1)
            {
                format = System.Drawing.Imaging.ImageFormat.Wmf;
            }
            else
            {
                format = System.Drawing.Imaging.ImageFormat.Jpeg;
            }

            return format;
        }

        public static string GetOnlyFileName(string fileName)
        {
            int fileStart = fileName.LastIndexOf("\\");

            if (fileStart > 0)
            {
                return fileName.Substring(fileStart + 1);
            }
            else
            {
                fileStart = fileName.LastIndexOf("/");

                if (fileStart > 0)
                {
                    return fileName.Substring(fileStart + 1);
                }
                else
                {
                    return fileName;
                }
            }
        }

        public static bool IsValidImageFile(FileUpload file)
        {
            try
            {
                using (Bitmap bmp = new Bitmap(file.PostedFile.InputStream))
                {
                    return true;
                }
            }
            catch (ArgumentException)
            {
                //throws exception if not valid image
            }

            return false;
        }

        public static string ReplaceCharsFromFileName(string strText, char[] chrItems)
        {
            if (chrItems == null)
            {
                chrItems = new char[9];
                chrItems[0] = '/';
                chrItems[1] = '\\';
                chrItems[2] = ':';
                chrItems[3] = '*';
                chrItems[4] = '?';
                chrItems[5] = '\"';
                chrItems[6] = '<';
                chrItems[7] = '>';
                chrItems[8] = '|';
            }

            for (int i = 0; i < chrItems.Length; i++)
            {
                strText = strText.Replace(chrItems[i], ' ');
            }
            return strText;
        }

        public static Size CalculateThumbSize(Size originalSize)
        {
            const double SZ_96 = 96;

            double widthRatio = originalSize.Width / SZ_96;
            double heightRatio = originalSize.Height / SZ_96;
            double ratio = Math.Max(widthRatio, heightRatio);

            if (ratio < 1) ratio = 1;

            int thumbWidth = Convert.ToInt32((originalSize.Width / ratio));
            int thumbHeight = Convert.ToInt32((originalSize.Height / ratio));

            return new Size(thumbWidth, thumbHeight);
        }

        public static string GetCode(ModuleType moduleType, int id)
        {
            string generatedCode = string.Empty;

            switch (moduleType)
            {
                case ModuleType.Candidate:
                    generatedCode = "CAN" + id.ToString();
                    break;
                case ModuleType.Consultant:
                    generatedCode = "CON" + id.ToString();
                    break;
                case ModuleType.Employee:
                    generatedCode = "EMP" + id.ToString();
                    break;
                case ModuleType.Requisition:
                    generatedCode = "REQ" + id.ToString();
                    break;
                case ModuleType.ProjectManagement:
                    generatedCode = "PMP" + id.ToString();
                    break;
                case ModuleType.TimeSheet:
                    generatedCode = "TMS" + id.ToString();
                    break;
                case ModuleType.SearchQuery:
                    generatedCode = "QRY" + id.ToString().PadLeft(5, '0');
                    break;
                case ModuleType.Invoice:
                    generatedCode = "INV" + id.ToString().PadLeft(5, '0');
                    break;
                default:
                    generatedCode = string.Empty;
                    break;
            }

            return generatedCode;
        }

        public static bool IsNewFile(string file)
        {
            return file.Contains(UrlConstants.TempDirectory);
        }

        public static void CopyFiles(StringDictionary files, string directory)
        {
            string fileName;
            string filePath;
            string newPath;

            foreach (DictionaryEntry entry in files)
            {
                fileName = Path.GetFileName((string)entry.Value);
                filePath = (string)entry.Value;

                if (IsNewFile(filePath))
                {
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    newPath = Path.Combine(directory, fileName);

                    try
                    {
                        File.Copy(filePath, newPath, true);
                        File.Delete(filePath); 
                    }
                    catch
                    {
                    }
                }
            }
        }

        public static void CopyEmailFiles(StringDictionary files, string directory)
        {
            string[] fileName;
            string[] filePath;
            string newPath;

            foreach (DictionaryEntry entry in files)
            {
                fileName = Path.GetFileName((string)entry.Value).Split((char)Convert.ToChar("?"));
                filePath = ((string)entry.Value).Split((char)Convert.ToChar("?"));

                if (IsNewFile(filePath[0]))
                {
                    if (!Directory.Exists(directory))
                    {
                        
                        Directory.CreateDirectory(directory);
                    }

                    newPath = Path.Combine(directory, fileName[0]);

                    try
                    {
                        File.Copy(filePath[0], newPath, true);
                        //File.Delete(filePath[0]); //1.3
                    }
                    catch
                    {
                    }
                }
            }
        }

        //public static void PopulatePublishedTest(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    IList<TestMaster> testMasterList = facade.GetAllTestMasterByTypeAndPublishStatus((int)AssessmentType.Online, (int)AssessmentPusblishStatus.Published);
        //    list.DataSource = testMasterList;
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        public static void PopulateDailyWorkSchedule(ListControl list, IFacade facade)
        {
            //list.Items.Clear();
            //IList<GenericLookup> genericLookupList = facade.GetAllGenericLookupByLookupType(LookupType.DailyWorkSchedule);
            //list.DataSource = genericLookupList;
            //list.DataTextField = "Name";
            //list.DataValueField = "Id";
            //list.DataBind();

            //list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static void PopulateDaysOfMonth(ListControl list)
        {
            list.Items.Clear();
            for (int i = 1; i <= 31; i++)
            {
                list.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        //Code introduced by Prasanth on 20/Jul/2015 Start
        #region Interviewer
        public static void PopulateInterviewerDocumentType(DropDownList ddlDocumentType, IFacade facade)
        {
            ddlDocumentType.Items.Clear();

            ddlDocumentType.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.InterviewerDocumentType);
            ddlDocumentType.DataTextField = "Name";
            ddlDocumentType.DataValueField = "Id";
            ddlDocumentType.DataBind();

            ddlDocumentType.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        #endregion

        #region Member

        public static void PopulateDocumentType(DropDownList ddlDocumentType, IFacade facade)
        {
            ddlDocumentType.Items.Clear();

            ddlDocumentType.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.DocumentType);
            ddlDocumentType.DataTextField = "Name";
            ddlDocumentType.DataValueField = "Id";
            ddlDocumentType.DataBind();

            ddlDocumentType.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static void PopulateMemberListByRole(ListControl list, string roleName, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllMemberNameByRoleName(roleName);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
                list = RemoveScriptForDropDown(list);
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));   // Integer conversion error handled.
        }

        public static void PopulateHiringMatrixLevelsList(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllHiringMatrixLevels();
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list = RemoveScriptForDropDown(list);
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));   // Integer conversion error handled.
        }
        //*********[Pravin khot] - [Candidate Hiring Status Update] � [6/March/2016] � Start *****************
        public static void PopulateHiringMatrixLevelsForHiringManagerList(ListControl list, IFacade facade, int MemberId)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllHiringMatrixLevelsForHiringManager(MemberId);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list = RemoveScriptForDropDown(list);
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        //*********[Pravin khot] - [Candidate Hiring Status Updtae] � [6/March/2016] � End *****************
        public static void PopulateMemberListWithEmailByRole(ListControl list, string roleName, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllMemberNameWithEmailByRoleName(roleName);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list = RemoveScriptForDropDown(list);
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));   // Integer conversion error handled.
        }
        //-------------------code added pravin khot using Company Contact Existing Users. 7/March/2016 ---------- 
        public static void PopulateMemberListWithEmailByRoleIfExisting(ListControl list, string roleName, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllMemberNameWithEmailByRoleNameIfExisting(roleName);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list = RemoveScriptForDropDown(list);
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));   // Integer conversion error handled.
        }
        //*************************************END******************************************************

        //-------------------code added pravin khot using fetching Suggested Interviewer Name for Interview Schedule 09/Dec/2015 ---------- 
        public static void PopulateSuggestedInterviewerWithEmail(ListControl list, int RequisitionId, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllSuggestedInterviewerWithEmail(RequisitionId);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list = RemoveScriptForDropDown(list);
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        //-------------------------------------------End--------------------------------------


        //-------------------code added pravin khot using fetching user name for InterviewPanel 02/Dec/2015 ---------- 

        public static void PopulateMemberListWithEmailByRole_IP(ListControl list, string roleName, int paneltypeid, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllMemberNameWithEmailByRoleName_IP(roleName, paneltypeid);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list = RemoveScriptForDropDown(list);
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));   // Integer conversion error handled.
        }
        //-----------------------End-----------------------------


        public static void PopulateAssignedMemberTeamByJobPosting(ListControl list, int JobPostingid, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllJobPostingHiringTeamByJobPostingId(JobPostingid);
            list.DataTextField = "Name";
            list.DataValueField = "MemberId";
            list.DataBind();
            list = RemoveScriptForDropDown(list);
            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }

        public static ListControl RemoveScriptForDropDown(ListControl list)
        {
            if (list.Items.Count > 0)
            {
                foreach (ListItem item in list.Items)
                {
                    if (item.Text.Contains("&lt;") || item.Text.Contains("&amp;")) 
                        item.Text = RemoveScript(item.Text, string.Empty);
                    item.Attributes.Add("title", item.Text);
                }
            }
            return list;
        }
        public static ListControl RemoveScriptForCheckBoxList(ListControl list)
        {
            if (list.Items.Count > 0)
            {
                foreach (ListItem item in list.Items)
                {
                    item.Text = RemoveScript(item.Text);
                    item.Attributes.Add("title", item.Text);
                }
            }
            return list;
        }
        #endregion

        public static void PopulateSalesLifeCycle(ListControl list, IFacade facade)
        {
            //list.Items.Clear();

            //list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.SalesLifeCycle);
            //list.DataTextField = "Name";
            //list.DataValueField = "Id";
            //list.DataBind();

            //list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        }

        public static void PopulateSalesProbability(ListControl list, IFacade facade)
        {
            //list.Items.Clear();

            //list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.SalesProbability);
            //list.DataTextField = "Name";
            //list.DataValueField = "Id";
            //list.DataBind();
            //list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        }

        public static void PopulateSalesStatus(ListControl list)
        {
            list.Items.Clear();

            list.Items.Add(new ListItem(EnumHelper.GetDescription(SalesStatus.BadLead), StringHelper.Convert((int)SalesStatus.BadLead)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(SalesStatus.Lead), StringHelper.Convert((int)SalesStatus.Lead)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(SalesStatus.LostOpportunity), StringHelper.Convert((int)SalesStatus.LostOpportunity)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(SalesStatus.Opportunity), StringHelper.Convert((int)SalesStatus.Opportunity)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(SalesStatus.SuccessfullSales), StringHelper.Convert((int)SalesStatus.SuccessfullSales)));

            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        }

        public static void PopulateLeadPriority(ListControl list)
        {
            list.Items.Clear();

            list.Items.Add(new ListItem(EnumHelper.GetDescription(LeadPriority.High), StringHelper.Convert((int)LeadPriority.High)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(LeadPriority.Low), StringHelper.Convert((int)LeadPriority.Low)));

            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        }

        public static void PopulateLeadSource(ListControl list)
        {
            list.Items.Clear();

            list.Items.Add(new ListItem(EnumHelper.GetDescription(LeadSource.Internal), StringHelper.Convert((int)LeadSource.Internal)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(LeadSource.External), StringHelper.Convert((int)LeadSource.External)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(LeadSource.Other), StringHelper.Convert((int)LeadSource.Other)));

            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        }

        public static void PopulateCompanyStatus(ListControl list)
        {
            string allValues = StringHelper.Convert((int)CompanyStatus.Company) + ", " +
                                StringHelper.Convert((int)CompanyStatus.ProspectiveClient) + ", " +
                                StringHelper.Convert((int)CompanyStatus.Client) + ", " +
                                StringHelper.Convert((int)CompanyStatus.Vendor) + ", " +
                                StringHelper.Convert((int)CompanyStatus.Partner);

            list.Items.Clear();

            list.Items.Add(new ListItem(UIConstants.DROP_DOWNL_ITEM_ALL, allValues));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(CompanyStatus.Company), StringHelper.Convert((int)CompanyStatus.Company)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(CompanyStatus.ProspectiveClient), StringHelper.Convert((int)CompanyStatus.ProspectiveClient)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(CompanyStatus.Client), StringHelper.Convert((int)CompanyStatus.Client)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(CompanyStatus.Vendor), StringHelper.Convert((int)CompanyStatus.Vendor)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(CompanyStatus.Partner), StringHelper.Convert((int)CompanyStatus.Partner)));
        }

        public static void PopulateCompanyStatusForVendorGroup(ListControl list)
        {
            string allValues = StringHelper.Convert((int)CompanyStatus.Vendor) + ", " +
                                StringHelper.Convert((int)CompanyStatus.Partner);

            list.Items.Clear();
            list.Items.Add(new ListItem(UIConstants.DROP_DOWNL_ITEM_ALL, allValues));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(CompanyStatus.Vendor), StringHelper.Convert((int)CompanyStatus.Vendor)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(CompanyStatus.Partner), StringHelper.Convert((int)CompanyStatus.Partner)));
        }

        public static void PopulateCompanyStatusForChangeRequest(ListControl list)
        {
            list.Items.Clear();

            list.Items.Add(new ListItem(EnumHelper.GetDescription(CompanyStatus.Client), StringHelper.Convert((int)CompanyStatus.Client)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(CompanyStatus.Vendor), StringHelper.Convert((int)CompanyStatus.Vendor)));


            //list.Items.Add(new ListItem(EnumHelper.GetDescription(CompanyStatus.Partner), StringHelper.Convert((int)CompanyStatus.Partner)));  // 0.2..Defect Id 8792..
            
            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        }

        //public static void PopulateCampaign(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllCampaign();
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //}

        public static void PopulateDataCheckList(ListControl list, IFacade facade)
        {
            list.Items.Clear();

            list.Items.Add(new ListItem(EnumHelper.GetDescription(DataCheckList.CompanyData), StringHelper.Convert((int)DataCheckList.CompanyData)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(DataCheckList.ManagementData), StringHelper.Convert((int)DataCheckList.ManagementData)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(DataCheckList.FinancialData), StringHelper.Convert((int)DataCheckList.FinancialData)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(DataCheckList.References), StringHelper.Convert((int)DataCheckList.References)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(DataCheckList.InsuranceData), StringHelper.Convert((int)DataCheckList.InsuranceData)));
            list.Items.Add(new ListItem(EnumHelper.GetDescription(DataCheckList.CompanyDocuments), StringHelper.Convert((int)DataCheckList.CompanyDocuments)));
        }

        public static string GetMemberNameById(int id, IFacade facade)
        {
            string memberName = string.Empty;
            if (id > 0)
            {
                memberName = facade.GetMemberNameById(id);
            }
            return memberName;
        }
        public static string GetCompanyNameById(int id, IFacade facade)
        {
            string CompanyName = string.Empty;
            if (id > 0)
            {
                CompanyName = facade.GetCompanyNameById(id);
            }
            return CompanyName;
        }
        //public static void PopulateTaskCategoryList(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllTaskCategory();
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //}

        public static void PopulateHoursList(ListControl list)
        {
            list.Items.Clear();
            list.Items.Add(new ListItem("12:00 AM", "0"));
            list.Items.Add(new ListItem("1:00 AM", "1"));
            list.Items.Add(new ListItem("2:00 AM", "2"));
            list.Items.Add(new ListItem("3:00 AM", "3"));
            list.Items.Add(new ListItem("4:00 AM", "4"));
            list.Items.Add(new ListItem("5:00 AM", "5"));
            list.Items.Add(new ListItem("6:00 AM", "6"));
            list.Items.Add(new ListItem("7:00 AM", "7"));
            list.Items.Add(new ListItem("8:00 AM", "8"));
            list.Items.Add(new ListItem("9:00 AM", "9"));
            list.Items.Add(new ListItem("10:00 AM", "10"));
            list.Items.Add(new ListItem("11:00 AM", "11"));
            list.Items.Add(new ListItem("12:00 PM", "12"));
            list.Items.Add(new ListItem("1:00 PM", "13"));
            list.Items.Add(new ListItem("2:00 PM", "14"));
            list.Items.Add(new ListItem("3:00 PM", "15"));
            list.Items.Add(new ListItem("4:00 PM", "16"));
            list.Items.Add(new ListItem("5:00 PM", "17"));
            list.Items.Add(new ListItem("6:00 PM", "18"));
            list.Items.Add(new ListItem("7:00 PM", "19"));
            list.Items.Add(new ListItem("8:00 PM", "20"));
            list.Items.Add(new ListItem("9:00 PM", "21"));
            list.Items.Add(new ListItem("10:00 PM", "22"));
            list.Items.Add(new ListItem("11:00 PM", "23"));
        }

        public static void PopulateProjectStatusCategory(ListControl list, IFacade facade)
        {
            //list.Items.Clear();

            //list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.ProjectStatusCategory);
            //list.DataTextField = "Name";
            //list.DataValueField = "Id";
            //list.DataBind();
        }

        public static void PopulateLastTenYear(ListControl list)
        {
            list.Items.Clear();

            DateTime date = DateTime.Today;

            for (int i = date.Year; i >= date.Year - 10; i--)
            {
                list.Items.Add(i.ToString());
            }

            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        }

        public static string GetFullName(string strFirstName, string strMiddleName, string strLastName)
        {
            string strFullName = strFirstName.Trim();
            strMiddleName = strMiddleName.Trim();
            if (!String.IsNullOrEmpty(strMiddleName))
            {
                strFullName = strFullName + " " + strMiddleName.Substring (0,1).ToUpper ()+".";
            }

            strLastName = strLastName.Trim();
            if (!String.IsNullOrEmpty(strLastName))
            {
                strFullName = strFullName + " " + strLastName;
            }
            return strFullName;
        }

        public static string GetFirstAndLastName(string strFirstName, string strLastName)
        {
            string strFullName = strFirstName.Trim();
            if (!String.IsNullOrEmpty(strLastName))
            {
                strFullName = strFullName + " " + strLastName;
            }
            return strFullName;
        }

        public static string GetLocation(string strCityName, string strState)
        {
            string strLocation = strCityName.Trim();
            strState = strState.Trim();
            if (!String.IsNullOrEmpty(strState))
            {
                strLocation = strLocation + ", " + strState;
            }
            //1.9
            //else
            //{
            //    strLocation = strLocation + ", " + "n/a";
            //}
            //1.9 ends
            return strLocation;
        }

        public static string GetLocation(string strCity, string strState, string strZipCode)
        {
            string strCurrentLocation = string.Empty;

            if (strCity != "")
            {
                strCurrentLocation = strCity;
                if (strState != "")
                {
                    strCurrentLocation = strCurrentLocation + ", " + strState;
                }
                if (strZipCode != "")
                {
                    strCurrentLocation = strCurrentLocation + ", " + strZipCode;
                }
            }
            else if (strState != "")
            {
                strCurrentLocation = strState;
                if (strZipCode != "")
                {
                    strCurrentLocation = strCurrentLocation + ", " + strZipCode;
                }
            }
            else if (strZipCode != "")
            {
                strCurrentLocation = strZipCode;
            }
            return strCurrentLocation;
        }

        public static string GetInternalRatingImage(string rating)
        {
            switch (rating)
            {
                case "-1":
                    return UrlConstants.InternelRating.OneStar;
                case "0":
                    return UrlConstants.InternelRating.None;
                case "1":
                    return UrlConstants.InternelRating.One;
                case "2":
                    return UrlConstants.InternelRating.Two;
                case "3":
                    return UrlConstants.InternelRating.Three;
                case "4":
                    return UrlConstants.InternelRating.Four;
                case "5":
                    return UrlConstants.InternelRating.Five;
                default:
                    return UrlConstants.InternelRating.None;
            }
        }

        //public static string GetAttendanceTypeImage(int intAttendanceType)
        //{
        //    AttendanceType attendanceType = (AttendanceType)intAttendanceType;
        //    switch (attendanceType)
        //    {
        //        case AttendanceType.AbsentWithoutReason:
        //            return UrlConstants.AttendanceType.AbsentWithoutReason;
        //        case AttendanceType.AnnualVacation:
        //            return UrlConstants.AttendanceType.AnnualVacation;
        //        case AttendanceType.EducationTraining:
        //            return UrlConstants.AttendanceType.EducationTraining;
        //        case AttendanceType.HolidayCompany:
        //            return UrlConstants.AttendanceType.HolidayCompany;
        //        case AttendanceType.HolidayGovernment:
        //            return UrlConstants.AttendanceType.HolidayGovernment;
        //        case AttendanceType.HolidayReligious:
        //            return UrlConstants.AttendanceType.HolidayReligious;
        //        case AttendanceType.JuryDuty:
        //            return UrlConstants.AttendanceType.JuryDuty;
        //        case AttendanceType.LateStart:
        //            return UrlConstants.AttendanceType.LateStart;
        //        case AttendanceType.LeaveEarly:
        //            return UrlConstants.AttendanceType.LeaveEarly;
        //        case AttendanceType.NoWork:
        //            return UrlConstants.AttendanceType.NoWork;
        //        case AttendanceType.ParentalLeave:
        //            return UrlConstants.AttendanceType.ParentalLeave;
        //        case AttendanceType.Present:
        //            return UrlConstants.AttendanceType.Present;
        //        case AttendanceType.SickLeave:
        //            return UrlConstants.AttendanceType.SickLeave;
        //        case AttendanceType.Suspended:
        //            return UrlConstants.AttendanceType.Suspended;
        //        case AttendanceType.Tardy:
        //            return UrlConstants.AttendanceType.Tardy;
        //        case AttendanceType.UnpaidLeave:
        //            return UrlConstants.AttendanceType.UnpaidLeave;
        //        case AttendanceType.WorkersCompensation:
        //            return UrlConstants.AttendanceType.WorkersCompensation;
        //        default:
        //            return UrlConstants.AttendanceType.Present;

        //    }
        //}

        public static string GetRequistionPreviewPageLink(int jobpostingid, string jobtitle)
        {
            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE, "", UrlConstants.PARAM_JOB_ID, jobpostingid.ToString());   //0.1
            string strLink = "<a href='../" + url.ToString().Replace("~/", "") + "' target='_blank'>" + jobtitle + "</a>";
            strLink = strLink.Replace("//", "/");
            return strLink;
        }

        #region JobPosting

        public static void GetJobPostingAssignedManagers(int jobId, System.Web.UI.Control ctrlAssignedManagers, System.EventHandler clickEvent, int rowNo, int currentUserId, IFacade Facade)
        {
            if (jobId > 0)
            {
                IList<JobPostingHiringTeam> jobPostingHiringTeamList = Facade.GetAllJobPostingHiringTeamByJobPostingId(jobId);
                if (jobPostingHiringTeamList != null)
                {
                    foreach (JobPostingHiringTeam jobPostingHiringTeam in jobPostingHiringTeamList)
                    {
                        if (jobPostingHiringTeam != null)
                        {
                            if (jobPostingHiringTeam.MemberId > 0)
                            {
                                Member member = Facade.GetMemberById(jobPostingHiringTeam.MemberId);
                                if (member != null)
                                {
                                    Label lblManagerName = new Label();
                                    lblManagerName.Text = member.FirstName + " " + member.LastName + " ";
                                    lblManagerName.ID = "lblAssignedManager," + jobId + "," + member.Id.ToString();
                                    ctrlAssignedManagers.Controls.Add(lblManagerName);

                                    LinkButton lnkButton = new LinkButton();
                                    if (member.Id == currentUserId)
                                    {
                                        lnkButton.Text = "Self Un-Assign</br>";
                                    }
                                    else
                                    {
                                        lnkButton.Text = "Un-Assign</br>";
                                    }
                                    lnkButton.ID = "lnkAssignedManager," + jobId + "," + rowNo + "," + member.Id.ToString();
                                    lnkButton.CommandArgument = jobPostingHiringTeam.Id.ToString();
                                    lnkButton.Click += clickEvent;
                                    ctrlAssignedManagers.Controls.Add(lnkButton);
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region JobPostingDocument

        public static string GetJobPostingDocumentPath(Page request, int CurrentJobPostingId, string strFileName, string strDocumenType, bool isVirtual)
        {
            string splitter = "";
            if (isVirtual) splitter = "/";
            else splitter = "\\";
            string strFilePath = String.Empty;
            if (isVirtual)
            {
                strFilePath = "../" + UrlConstants.REQUISITION_DOC_DIR;
            }
            else
            {
                strFilePath = request.Server.MapPath("../" + UrlConstants.REQUISITION_DOC_DIR);
            }
            strFilePath = strFilePath + splitter + CurrentJobPostingId.ToString();
            if (!isVirtual)
            {
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            if (strDocumenType.Trim () != string.Empty)
            {
                strFilePath = strFilePath + splitter + strDocumenType;
                if (!isVirtual)
                {
                    if (!Directory.Exists(strFilePath))
                    {
                        Directory.CreateDirectory(strFilePath);
                    }
                }
            }
            strFilePath = strFilePath + splitter + strFileName;
            if (isVirtual)
            {
                string strPhysicalPath = request.Server.MapPath(strFilePath);
                if (File.Exists(strPhysicalPath))
                {
                    return strFilePath;
                }
                else
                {
                    return strFileName;
                }
            }
            else
            {
                return strFilePath;
            }

        }

        #endregion

        //public static void PopulateProjectList(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllProject();
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //}

        //public static void PopulateInsuranceType(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.Insurance);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        //}

        // Returns all member skills in a list with comma seperated
        public static string GetMemberSkillSetList(int memberId, IFacade Facade)
        {
            string skills = string.Empty;
            int skillId = 0;
            IList<MemberSkillMap> memberSkillMapList = Facade.GetAllMemberSkillMapByMemberId(memberId);

            if (memberSkillMapList != null)
            {
                foreach (MemberSkillMap memberSkillMap in memberSkillMapList)
                {
                    skillId = memberSkillMap.SkillId;
                    Skill skill = Facade.GetSkillById(skillId);

                    if (skill != null)
                    {
                        if (string.IsNullOrEmpty(skills))
                        {
                            skills = skill.Name;
                        }
                        else
                        {
                            skills = skills + ", " + skill.Name;
                        }
                    }
                }
            }

            return skills;
        }

        // This is used in interview pages
        public static string GetMemberMatchingData(int memberId, int questionNo, IFacade facade)
        {
            string matchingData = string.Empty;
            Member member;
            MemberExtendedInformation memberExtendedInformation;
            switch (questionNo)
            {
                case 1: //JobTitle
                    memberExtendedInformation = facade.GetMemberExtendedInformationByMemberId(memberId);
                    matchingData = memberExtendedInformation != null ? memberExtendedInformation.CurrentPosition : string.Empty;
                    break;
                case 2: //Location
                    member = facade.GetMemberById(memberId);
                    matchingData = member != null ? member.PermanentCity.ToString() : string.Empty;
                    break;
                case 3: //Industry Experience                    
                    memberExtendedInformation = facade.GetMemberExtendedInformationByMemberId(memberId);
                    matchingData = memberExtendedInformation != null ? GetLookupNameById(memberExtendedInformation.IndustryTypeLookupId, facade) : string.Empty;
                    break;
                case 4: //Educational degree
                    memberExtendedInformation = facade.GetMemberExtendedInformationByMemberId(memberId);
                    matchingData = memberExtendedInformation != null ? GetLookupNameById(memberExtendedInformation.HighestEducationLookupId, facade) : string.Empty;
                    break;
                case 5: //Work Status
                    memberExtendedInformation = facade.GetMemberExtendedInformationByMemberId(memberId);
                    matchingData = memberExtendedInformation != null ? GetLookupNameById(memberExtendedInformation.WorkAuthorizationLookupId, facade) : string.Empty;
                    break;
                case 6: //Salary
                    memberExtendedInformation = facade.GetMemberExtendedInformationByMemberId(memberId);
                    matchingData = memberExtendedInformation != null ? memberExtendedInformation.ExpectedYearlyRate.ToString() : string.Empty;
                    break;
                case 7: //Skill set
                    matchingData = GetMemberSkillSetList(memberId, facade);
                    break;
                default: matchingData = string.Empty;
                    break;
            }

            return string.IsNullOrEmpty(matchingData) ? "(Not Mentioned)" : "(" + matchingData + ")";
        }

        public static void PopulateProficiencyLevel(ListControl list, IFacade facade)
        {
            list.Items.Clear();

            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.Proficiency);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
        }

        public static string GetStateNameById(int stateId, IFacade facade)
        {
            string stateName = string.Empty;
            if (stateId > 0)
            {
                stateName = facade.GetStateNameById(stateId);
            }
            return MiscUtil .RemoveScript ( stateName);
        }

        public static string GetCountryNameById(int countryId, IFacade facade)
        {
            string countryName = string.Empty;
            if (countryId > 0)
            {
                countryName = facade.GetCountryNameById(countryId);
            }
            return MiscUtil .RemoveScript ( countryName);
        }

        //#region Project

        //public static void PopulateProjects(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllProjects();
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_PROJECT);
        //}

        //#endregion

        #region Client

        public static CompanyStatus GetCompanyStatusFromDefaultSiteSetting(Hashtable  siteSetting)
        {
            if (siteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == TPS360.Web.UI.UIConstants.AccountType_Client)
                return CompanyStatus.Client;
            else if (siteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == TPS360.Web.UI.UIConstants.AccountType_Department)
                return CompanyStatus.Department;
            else return CompanyStatus.Vendor;
        }
            
          public static void PopulateClients(ListControl list,int CompanyStatus, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllClientsByStatus(CompanyStatus);
            list.DataTextField = "CompanyName";
            list.DataValueField = "Id";
            list.DataBind();
            list = RemoveScriptForDropDown(list);
            list.Items.Insert(0, new ListItem (UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_CLIENT,"0"));
        }
        public static void PopulateClients(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllClients();
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list = RemoveScriptForDropDown(list);
            list.Items.Insert(0, new ListItem (UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_CLIENT,"0"));
        }

        public static void PopulateCompanyPrimaryContact(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            list.DataSource = facade.GetPrimaryContact();
            list.DataTextField = "ReceiverEmail";
            //list.DataValueField = "Id";
            list.DataValueField = "ReceiverEmail";
            list.DataBind();
            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        }
        #endregion

        #region Hot List

        public static void PopulateMemberGroupByMemberId(ListControl list, IFacade facade, int memberId)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllMemberGroupNameByManagerId(memberId);

            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_HOTLIST);
        }

        public static void PopulateMemberGroupForTimeSheetByMemberId(ListControl list, IFacade facade, int memberId)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllMemberGroupNameByManagerId(memberId);

            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_CLIENT_MANAGER);

        }

        public static void PopulateMemberGroupByMemberId(ListControl list, IFacade facade, int memberId, int groupType)
        {
            list.Items.Clear();
            list.DataSource = facade.GetAllMemberGroupNameByManagerId(memberId, groupType);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list = RemoveScriptForDropDown(list);
            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_HOTLIST);
        }

        public static void PopulateMemberManagerByMemberId(ListControl list, IFacade facade, int memberId)
        {
            list.Items.Clear();
            list.DataSource = facade.GetMemberManagerListByMemberId(memberId);

            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();
            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_INTERNAL_MANAGER);

        }

        public static string GetResumeSourceByMemberId(IFacade facade, int resumeSourceId, int resumeSourceType)
        {
            try
            {
                ResumeSource resumeSource = (ResumeSource)resumeSourceType;
                string strResumeSource = EnumHelper.GetDescription(resumeSource);
                if (resumeSource != ResumeSource.SelfRegistration)
                {
                    Member _member = facade.GetMemberById(resumeSourceId);
                    if (_member != null)
                    {
                        strResumeSource = strResumeSource + " (" + GetFirstAndLastName(_member.FirstName, _member.LastName) + ")";
                    }
                }
               
                return strResumeSource;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static void PopulateMasterHotlist(ListControl list, IFacade facade)
        {
            list.Items.Clear();
            IList<MemberGroup> memberGroupList = facade.GetAllMemberGroup();
            if (memberGroupList != null)
            {
                list.DataSource = memberGroupList;
                list.DataTextField = "Name";
                list.DataValueField = "Id";
                list.DataBind();
            }
            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_HOTLIST);
        }

        public static void PopulateMasterHotlist(ListControl list, IFacade facade, int groupType)
        {
            list.Items.Clear();
            IList<MemberGroup> memberGroupList = facade.GetAllMemberGroup(groupType);
            if (memberGroupList != null)
            {
                list.DataSource = memberGroupList;
                list.DataTextField = "Name";
                list.DataValueField = "Id";
                list.DataBind();
                list = RemoveScriptForDropDown(list);
            }
            list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_HOTLIST);
        }


        #endregion
        #region MemberDocument

        public static string GetMemberDocumentPath(Page request, int memberId, string strFileName, string strDocumenType, bool isVirtual)
        {
            strFileName = Regex.Replace (strFileName, @"[\/:?<>|*]", "");
            
            string strFilePath = String.Empty;
            string strFileDecode = string.Empty;
            if (isVirtual)
            {
                strFilePath = "../" + UrlConstants.MEMBER_DOC_DIR;
            }
            else
            {
                strFilePath = HttpContext.Current.Server.MapPath("../" + UrlConstants.MEMBER_DOC_DIR); 
            }
            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + memberId.ToString();
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            else
            {
                strFilePath = strFilePath + "/" + memberId.ToString();
            }


            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + strDocumenType;
				if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            else
            {
                strFilePath = strFilePath + "/" + strDocumenType;
				if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
            }

            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + strFileName;
                return strFilePath;

            }
            else
            {
                try
                {
                    strFileDecode = strFilePath + "/" + strFileName;
                    string strPhysicalPath = request.Server.MapPath(strFileDecode);
                    if (File.Exists(strPhysicalPath))
                    {
                        strFilePath = strFilePath + "/" + System.Uri.EscapeDataString(strFileName);
                        return strFilePath;
                    }
                    else
                    {
                        return strFileName;
                    }
                }
                catch
                {
                    return "";
                }
            }
        }

        public static string GetMemberDocumentPath(int memberId, string strFileName, string strDocumenType, bool isVirtual)
        {
            strFileName = Regex.Replace(strFileName, @"[\/:?<>|*]", "");

            string strFilePath = String.Empty;
            string strFileDecode = string.Empty;

            if (isVirtual)
            {
                String strIISSiteName = ConfigurationManager.AppSettings.Get("IISSiteName");
                strFilePath = "../" + strIISSiteName + "/" + UrlConstants.MEMBER_DOC_DIR;
            }
            else
            {
                String strIISSiteName = ConfigurationManager.AppSettings.Get("IISSiteName");
                strFilePath = HttpContext.Current.Server.MapPath("../" + strIISSiteName + "/" + UrlConstants.MEMBER_DOC_DIR);
            }
            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + memberId.ToString();
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            else
            {
                strFilePath = strFilePath + "/" + memberId.ToString();
            }


            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + strDocumenType;
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            else
            {
                strFilePath = strFilePath + "/" + strDocumenType;
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
            }

            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + strFileName;
                return strFilePath;

            }
            else
            {
                try
                {
                    strFileDecode = strFilePath + "/" + strFileName;
                    string strPhysicalPath = HttpContext.Current.Server.MapPath(strFileDecode);
                    if (File.Exists(strPhysicalPath))
                    {
                        strFilePath = strFilePath + "/" + System.Uri.EscapeDataString(strFileName);
                        return strFilePath;
                    }
                    else
                    {
                        return strFileName;
                    }
                }
                catch
                {
                    return "";
                }
            }
        }

        //added by pravin khot on 5/Dec/2016
        public static string GetMemberDocumentPathOnly(Page request, int memberId, string strFileName, string strDocumenType, bool isVirtual)
        {
            strFileName = Regex.Replace(strFileName, @"[\/:?<>|*]", "");

            string strFilePath = String.Empty;
            string strFileDecode = string.Empty;
            if (isVirtual)
            {
                strFilePath = "../" + UrlConstants.MEMBER_DOC_DIR;
            }
            else
            {
                strFilePath = HttpContext.Current.Server.MapPath("../" + UrlConstants.MEMBER_DOC_DIR);
            }
            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + memberId.ToString();
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            else
            {
                strFilePath = strFilePath + "/" + memberId.ToString();
            }


            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + strDocumenType;
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            else
            {
                strFilePath = strFilePath + "/" + strDocumenType;
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
            }

            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + strFileName;
                return strFilePath;
            }
            else
            {
                try
                {
                    strFileDecode = strFilePath + "/" + strFileName;
                    string strPhysicalPath = request.Server.MapPath(strFileDecode);
                    if (Directory.Exists(strPhysicalPath))
                    {
                        strFilePath = strFilePath + "/" + System.Uri.EscapeDataString(strFileName);
                        return strFilePath;
                    }
                    else
                    {
                        return strFileName;
                    }
                }
                catch
                {
                    return "";
                }
            }
        }

        //Function introduced by Prasanth on 23/Dec/2015 Start
        public static string GetMemberInterviewFeedbackDocumentPath(Page request, int memberId,int interviewId, string InterviewerEmail, string strFileName, string strDocumenType, bool isVirtual)
        {
            strFileName = Regex.Replace(strFileName, @"[\/:?<>|*]", "");

            string strFilePath = String.Empty;
            string strFileDecode = string.Empty;
            if (isVirtual)
            {
                strFilePath = "../" + UrlConstants.MEMBER_DOC_DIR;
            }
            else
            {
                strFilePath = HttpContext.Current.Server.MapPath("../" + UrlConstants.MEMBER_DOC_DIR);
            }
            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + memberId.ToString();
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            else
            {
                strFilePath = strFilePath + "/" + memberId.ToString();
            }




            //Create Folder For InterviewFeedback
            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\InterviewFeedback";
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            else
            {
                strFilePath = strFilePath + "/InterviewFeedback";
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
            }


            //Create Folder for InterviewId
            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + interviewId.ToString();
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            else
            {
                strFilePath = strFilePath + "/" + interviewId.ToString();
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
            }


            //Create Folder for InterviewerEmail
            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + InterviewerEmail;
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            else
            {
                strFilePath = strFilePath + "/" + InterviewerEmail;
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
            }


            //Create Folder for DocumentType

            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + strDocumenType;
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            else
            {
                strFilePath = strFilePath + "/" + strDocumenType;
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
            }

            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + strFileName;
                return strFilePath;

            }
            else
            {
                try
                {
                    strFileDecode = strFilePath + "/" + strFileName;
                    string strPhysicalPath = request.Server.MapPath(strFileDecode);
                    if (File.Exists(strPhysicalPath))
                    {
                        strFilePath = strFilePath + "/" + System.Uri.EscapeDataString(strFileName);
                        return strFilePath;
                    }
                    else
                    {
                        return strFileName;
                    }
                }
                catch
                {
                    return "";
                }
            }
        }



        #endregion
        #region CompanyDocument
        public static string GetCompanyDocumentPath(Page request, int CompanyId, string strFileName, string strDocumenType, bool isVirtual)
        {

            string strFilePath = String.Empty;
            string strFileDecode = string.Empty;
            if (isVirtual)
            {
                strFilePath = "../" + UrlConstants.COMPANY_DOC_DIR;
            }
            else
            {
                strFilePath = HttpContext.Current.Server.MapPath("../" + UrlConstants.COMPANY_DOC_DIR );
            }
            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + CompanyId.ToString();
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            else
            {
                strFilePath = strFilePath + "/" + CompanyId.ToString();
            }


            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + strDocumenType;
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
            }
            else
            {
                strFilePath = strFilePath + "/" + strDocumenType;
                if (strFilePath.Contains("'"))
                {
                    strFilePath = strFilePath.Replace("'", "");
                }
            }

            if (!isVirtual)
            {
                strFilePath = strFilePath + "\\" + strFileName;
                return strFilePath;

            }
            else
            {
                strFileDecode = strFilePath + "/" + strFileName;
                string strPhysicalPath = request.Server.MapPath(strFileDecode);
                if (File.Exists(strPhysicalPath))
                {
                    strFilePath = strFilePath + "/" + System.Uri.EscapeDataString(strFileName);
                    return strFilePath;
                }
                else
                {
                    return strFileName;
                }
            }
        }

        #endregion

        //#region TaskDocument

        //public static string GetTaskDocumentPath(Page request, string strFileName, string strCommonTaskID, bool isVirtual)
        //{
        //    string strFilePath = String.Empty;
        //    if (isVirtual)
        //    {
        //        strFilePath = "../" + UrlConstants.TaskManagement.TASK_DOC_DIR;
        //    }
        //    else
        //    {
        //        strFilePath = request.Server.MapPath("../" + UrlConstants.TaskManagement.TASK_DOC_DIR);
        //    }
        //    if (!isVirtual)
        //    {
        //        strFilePath = strFilePath + "\\" + strCommonTaskID;
        //        if (!Directory.Exists(strFilePath))
        //        {
        //            Directory.CreateDirectory(strFilePath);
        //        }
        //    }
        //    else
        //    {
        //        strFilePath = strFilePath + "/" + strCommonTaskID;
        //    }

        //    if (!isVirtual)
        //    {
        //        strFilePath = strFilePath + "\\" + strFileName;
        //        return strFilePath;

        //    }
        //    else
        //    {
        //        strFilePath = strFilePath + "/" + strFileName;
        //        string strPhysicalPath = request.Server.MapPath(strFilePath);
        //        if (File.Exists(strPhysicalPath))
        //        {
        //            return strFilePath;
        //        }
        //        else
        //        {
        //            return strFileName;
        //        }
        //    }
        //}

        //#endregion
        //#region TestQuestionDocument

        //public static string GetTestQuestionDocumentPath(Page page, string strFileName, string strTestQuestionId, bool isVirtual)
        //{
        //    string strFilePath = String.Empty;
        //    if (isVirtual)
        //    {
        //        strFilePath = "../" + UrlConstants.Assessment.TEST_QUESTION_DIR;
        //    }
        //    else
        //    {
        //        strFilePath = page.Server.MapPath("../" + UrlConstants.Assessment.TEST_QUESTION_DIR);
        //    }

        //    if (!isVirtual)
        //    {
        //        strFilePath = strFilePath + "\\" + strTestQuestionId;
        //        if (!Directory.Exists(strFilePath))
        //        {
        //            Directory.CreateDirectory(strFilePath);
        //        }
        //    }
        //    else
        //    {
        //        strFilePath = strFilePath + "/" + strTestQuestionId;
        //    }

        //    if (!isVirtual)
        //    {
        //        strFilePath = strFilePath + "\\" + strFileName;
        //        return strFilePath;

        //    }
        //    else
        //    {
        //        strFilePath = strFilePath + "/" + strFileName;
        //        string strPhysicalPath = page.Server.MapPath(strFilePath);
        //        if (File.Exists(strPhysicalPath))
        //        {
        //            return strFilePath;
        //        }
        //        else
        //        {
        //            return strFileName;
        //        }
        //    }
        //}

        //#endregion
        //public static bool AddToPreselectedList(int jobpostingid, int memberid, int creatorId, IFacade facade)
        //{
        //    int jobCartId = 0;
        //    MemberJobCart memberJobCart = facade.GetMemberJobCartByMemberIdAndJobPostringId(memberid, jobpostingid);
        //    if (memberJobCart == null)
        //    {
        //        MemberJobCart newJobCart = new MemberJobCart();
        //        newJobCart.JobPostingId = jobpostingid;
        //        newJobCart.MemberId = memberid;
        //        newJobCart.IsPrivate = false;
        //        newJobCart.UpdateDate = DateTime.Now;
        //        newJobCart.UpdatorId = creatorId;
        //        newJobCart.CreatorId = creatorId;
        //        newJobCart.ApplyDate = DateTime.Now;
        //        newJobCart = facade.AddMemberJobCart(newJobCart);
        //        jobCartId = newJobCart.Id;
        //    }
        //    else if (memberJobCart.IsRemoved)
        //    {
        //        memberJobCart.IsRemoved = false;
        //        memberJobCart.UpdatorId = creatorId;
        //        memberJobCart.UpdateDate = DateTime.Now;
        //        facade.UpdateMemberJobCart(memberJobCart);
        //        jobCartId = memberJobCart.Id;
        //    }
        //    else
        //    {
        //        jobCartId = memberJobCart.Id;
        //    }
        //    if (jobCartId != 0)
        //    {
        //        MemberJobCartDetail jobCartDetail = facade.GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(jobCartId, (int)MemberSelectionStep.PreSelectd);
        //        if (jobCartDetail == null)
        //        {
        //            jobCartDetail = new MemberJobCartDetail();
        //            AddToHiringLevel((int)MemberSelectionStep.PreSelectd, jobCartDetail, creatorId, jobCartId, false, facade);
        //            return true;
        //        }
        //        else if (jobCartDetail.IsRemoved)
        //        {
        //            AddToHiringLevel((int)MemberSelectionStep.PreSelectd, jobCartDetail, creatorId, jobCartId, false, facade);
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //public static bool AddToLevelIList(int jobpostingid, int memberid, int creatorId, IFacade facade)
        //{
        //    int jobCartId = 0;
        //    MemberJobCart memberJobCart = facade.GetMemberJobCartByMemberIdAndJobPostringId(memberid, jobpostingid);
        //    if (memberJobCart == null)
        //    {
        //        MemberJobCart newJobCart = new MemberJobCart();
        //        newJobCart.JobPostingId = jobpostingid;
        //        newJobCart.MemberId = memberid;
        //        newJobCart.IsPrivate = false;
        //        newJobCart.UpdateDate = DateTime.Now;
        //        newJobCart.UpdatorId = creatorId;
        //        newJobCart.CreatorId = creatorId;
        //        newJobCart.ApplyDate = DateTime.Now;
        //        newJobCart = facade.AddMemberJobCart(newJobCart);
        //        jobCartId = newJobCart.Id;
        //    }
        //    else if (memberJobCart.IsRemoved)
        //    {
        //        memberJobCart.IsRemoved = false;
        //        memberJobCart.UpdatorId = creatorId;
        //        memberJobCart.UpdateDate = DateTime.Now;
        //        facade.UpdateMemberJobCart(memberJobCart);
        //        jobCartId = memberJobCart.Id;
        //    }
        //    else
        //    {
        //        jobCartId = memberJobCart.Id;
        //    }
        //    if (jobCartId != 0)
        //    {
        //        MemberJobCartDetail jobCartDetail = facade.GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(jobCartId, (int)MemberSelectionStep.PreSelectd);
        //        if (jobCartDetail != null && !jobCartDetail.IsRemoved)
        //        {
        //            AddToHiringLevel((int)MemberSelectionStep.PreSelectd, jobCartDetail, creatorId, jobCartId, true, facade);
        //        }
        //        jobCartDetail = facade.GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(jobCartId, (int)MemberSelectionStep.LevelIInterview);
        //        if (jobCartDetail == null)
        //        {
        //            jobCartDetail = new MemberJobCartDetail();
        //            AddToHiringLevel((int)MemberSelectionStep.LevelIInterview, jobCartDetail, creatorId, jobCartId, false, facade);
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //public static bool AddToLevelIIList(int jobpostingid, int memberid, int creatorId, IFacade facade)
        //{
        //    int jobCartId = 0;
        //    MemberJobCart memberJobCart = facade.GetMemberJobCartByMemberIdAndJobPostringId(memberid, jobpostingid);
        //    if (memberJobCart == null)
        //    {
        //        MemberJobCart newJobCart = new MemberJobCart();
        //        newJobCart.JobPostingId = jobpostingid;
        //        newJobCart.MemberId = memberid;
        //        newJobCart.IsPrivate = false;
        //        newJobCart.UpdateDate = DateTime.Now;
        //        newJobCart.UpdatorId = creatorId;
        //        newJobCart.CreatorId = creatorId;
        //        newJobCart.ApplyDate = DateTime.Now;
        //        newJobCart = facade.AddMemberJobCart(newJobCart);
        //        jobCartId = newJobCart.Id;
        //    }
        //    else if (memberJobCart.IsRemoved)
        //    {
        //        memberJobCart.IsRemoved = false;
        //        memberJobCart.UpdatorId = creatorId;
        //        memberJobCart.UpdateDate = DateTime.Now;
        //        facade.UpdateMemberJobCart(memberJobCart);
        //        jobCartId = memberJobCart.Id;
        //    }
        //    else
        //    {
        //        jobCartId = memberJobCart.Id;
        //    }
        //    if (jobCartId != 0)
        //    {
        //        MemberJobCartDetail jobCartDetail = facade.GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(jobCartId, (int)MemberSelectionStep.PreSelectd);
        //        if (jobCartDetail != null && !jobCartDetail.IsRemoved)
        //        {
        //            AddToHiringLevel((int)MemberSelectionStep.PreSelectd, jobCartDetail, creatorId, jobCartId, true, facade);
        //        }
        //        jobCartDetail = facade.GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(jobCartId, (int)MemberSelectionStep.LevelIInterview);
        //        if (jobCartDetail != null && !jobCartDetail.IsRemoved)
        //        {
        //            AddToHiringLevel((int)MemberSelectionStep.LevelIInterview, jobCartDetail, creatorId, jobCartId, true, facade);
        //        }
        //        jobCartDetail = facade.GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(jobCartId, (int)MemberSelectionStep.LevelIIInterview);
        //        if (jobCartDetail == null)
        //        {
        //            jobCartDetail = new MemberJobCartDetail();
        //            AddToHiringLevel((int)MemberSelectionStep.LevelIIInterview, jobCartDetail, creatorId, jobCartId, false, facade);
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //public static bool AddToLevelIIIList(int jobpostingid, int memberid, int creatorId, IFacade facade)
        //{
        //    int jobCartId = 0;
        //    MemberJobCart memberJobCart = facade.GetMemberJobCartByMemberIdAndJobPostringId(memberid, jobpostingid);
        //    if (memberJobCart == null)
        //    {
        //        MemberJobCart newJobCart = new MemberJobCart();
        //        newJobCart.JobPostingId = jobpostingid;
        //        newJobCart.MemberId = memberid;
        //        newJobCart.IsPrivate = false;
        //        newJobCart.UpdateDate = DateTime.Now;
        //        newJobCart.UpdatorId = creatorId;
        //        newJobCart.CreatorId = creatorId;
        //        newJobCart.ApplyDate = DateTime.Now;
        //        newJobCart = facade.AddMemberJobCart(newJobCart);
        //        jobCartId = newJobCart.Id;
        //    }
        //    else if (memberJobCart.IsRemoved)
        //    {
        //        memberJobCart.IsRemoved = false;
        //        memberJobCart.UpdatorId = creatorId;
        //        memberJobCart.UpdateDate = DateTime.Now;
        //        facade.UpdateMemberJobCart(memberJobCart);
        //        jobCartId = memberJobCart.Id;
        //    }
        //    else
        //    {
        //        jobCartId = memberJobCart.Id;
        //    }
        //    if (jobCartId != 0)
        //    {
        //        MemberJobCartDetail jobCartDetail = facade.GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(jobCartId, (int)MemberSelectionStep.PreSelectd);
        //        if (jobCartDetail != null && !jobCartDetail.IsRemoved)
        //        {
        //            AddToHiringLevel((int)MemberSelectionStep.PreSelectd, jobCartDetail, creatorId, jobCartId, true, facade);
        //        }
        //        jobCartDetail = facade.GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(jobCartId, (int)MemberSelectionStep.LevelIInterview);
        //        if (jobCartDetail != null && !jobCartDetail.IsRemoved)
        //        {
        //            AddToHiringLevel((int)MemberSelectionStep.LevelIInterview, jobCartDetail, creatorId, jobCartId, true, facade);
        //        }
        //        jobCartDetail = facade.GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(jobCartId, (int)MemberSelectionStep.LevelIIInterview);
        //        if (jobCartDetail != null && !jobCartDetail.IsRemoved)
        //        {
        //            AddToHiringLevel((int)MemberSelectionStep.LevelIIInterview, jobCartDetail, creatorId, jobCartId, true, facade);
        //        }
        //        jobCartDetail = facade.GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(jobCartId, (int)MemberSelectionStep.LevelIIIInterview);
        //        if (jobCartDetail == null)
        //        {
        //            jobCartDetail = new MemberJobCartDetail();
        //            AddToHiringLevel((int)MemberSelectionStep.LevelIIIInterview, jobCartDetail, creatorId, jobCartId, false, facade);
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //public static void AddToHiringLevel(int level, MemberJobCartDetail memberJobCartDetail, int creatorId, int jobCartId, bool isRemoved, IFacade facade)
        //{
        //    if (memberJobCartDetail.IsNew)
        //    {
        //        memberJobCartDetail.AddedBy = creatorId;
        //        memberJobCartDetail.AddedDate = DateTime.Now;
        //        memberJobCartDetail.CreateDate = DateTime.Now;
        //        memberJobCartDetail.CreatorId = creatorId;
        //        memberJobCartDetail.UpdateDate = DateTime.Now;
        //        memberJobCartDetail.UpdatorId = creatorId;
        //        memberJobCartDetail.MemberJobCartId = jobCartId;
        //        memberJobCartDetail.SelectionStepLookupId = level;
        //        memberJobCartDetail.IsRemoved = isRemoved;
        //        facade.AddMemberJobCartDetail(memberJobCartDetail);
        //    }
        //    else if (!memberJobCartDetail.IsNew)
        //    {
        //        memberJobCartDetail.IsRemoved = isRemoved;
        //        memberJobCartDetail.UpdatorId = creatorId;
        //        memberJobCartDetail.UpdateDate = DateTime.Now;
        //        facade.UpdateMemberJobCartDetail(memberJobCartDetail);
        //    }
        //}
        public static bool AddToHotList(int hotlistid, int memberid, int creatorId, IFacade facade)
        {
            MemberGroupMap memberGroupMap = facade.GetMemberGroupMapByMemberIdandMemberGroupId(memberid, hotlistid);
            if (memberGroupMap == null)
            {
                MemberGroupMap newMemberGroupMap = new MemberGroupMap();
                newMemberGroupMap.CreateDate = DateTime.Now;
                newMemberGroupMap.CreatorId = creatorId;
                newMemberGroupMap.UpdateDate = DateTime.Now;
                newMemberGroupMap.UpdatorId = creatorId;
                newMemberGroupMap.MemberGroupId = hotlistid;
                newMemberGroupMap.MemberId = memberid;
                facade.AddMemberGroupMap(newMemberGroupMap);
                return true;
            }
            else if (memberGroupMap.IsRemoved)
            {
                memberGroupMap.IsRemoved = false;
                memberGroupMap.UpdateDate = DateTime.Now;
                memberGroupMap.UpdatorId = creatorId;
                facade.UpdateMemberGroupMap(memberGroupMap);
                return true;
            }
            return false;
        }
        public static bool RemoveFromHotList(int hotlistid, int memberid, IFacade facade)
        {
            if (facade.DeleteMemberGroupMapByIdandMemberGroupId(memberid, hotlistid))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool AddToMyList(int managerId, int memberId, int creatorId, IFacade facade)
        {
            if (managerId > 0 && memberId > 0)
            {
                MemberManager memberManager = facade.GetMemberManagerByMemberIdAndManagerId(memberId, managerId);
                if (memberManager == null)
                {
                    memberManager = new MemberManager();
                    memberManager.ManagerId = managerId;
                    memberManager.MemberId = memberId;
                    memberManager.IsPrimaryManager = false;
                    memberManager.UpdatorId = creatorId;
                    memberManager.CreatorId = creatorId;
                    memberManager.UpdateDate = DateTime.Now;
                    memberManager.CreateDate = DateTime.Now;
                    facade.AddMemberManager(memberManager);
                    return true;
                }
                else if (memberManager.IsRemoved)
                {
                    memberManager.IsRemoved = false;
                    memberManager.UpdateDate = DateTime.Now;
                    memberManager.UpdatorId = creatorId;
                    facade.UpdateMemberManager(memberManager);
                    return true;
                }
            }
            return false;
        }
        //public static void PopulateFollowupAction(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.FollowupAction);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT);
        //}
        public static string GetSkillNameById(int skillId, IFacade facade)
        {
            string skillName = string.Empty;

            Skill skill = facade.GetSkillById(skillId);
            skillName = (skill != null ? skill.Name : string.Empty);

            return skillName;
        }
        //public static string GetTestNameById(int testId, IFacade facade)
        //{
        //    string testName = string.Empty;

        //    TestMaster testMaster = facade.GetTestMasterById(testId);
        //    testName = (testMaster != null ? testMaster.Name : string.Empty);

        //    return testName;
        //}
        public static bool AddActivity(string memberRole, int memberId, int creatorId, ActivityType activityType, IFacade facade)
        {
            MemberActivityType memberActivityType = facade.GetMemberActivityType((int)activityType);
            if (memberActivityType != null && memberRole.Trim() != string.Empty && memberId != 0 && creatorId != 0)
            {
                MemberActivity memberActivity = new MemberActivity();

                if (memberRole.ToLower() == ContextConstants.ROLE_CANDIDATE.ToLower())
                {
                    memberActivity.ActivityOn = (int)MemberType.Candidate;
                }
                else if (memberRole.ToLower() == ContextConstants.ROLE_CONSULTANT.ToLower())
                {
                    memberActivity.ActivityOn = (int)MemberType.Consultant;
                }
                else if (memberRole.ToLower() == ContextConstants.ROLE_EMPLOYEE.ToLower())
                {
                    memberActivity.ActivityOn = (int)MemberType.Employee;
                }
                
                else
                {
                    return false;
                }
                memberActivity.ActivityOnObjectId = memberId;
                memberActivity.CreateDate = DateTime.Now;
                memberActivity.CreatorId = creatorId;
                memberActivity.UpdateDate = DateTime.Now;
                memberActivity.UpdatorId = creatorId;
                memberActivity.MemberActivityTypeId = memberActivityType.Id;
                memberActivity = facade.AddMemberActivity(memberActivity);
                if (memberActivity.Id != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public static bool AddActivityOnObject(MemberType memberType, int objectId, int creatorId, ActivityType activityType, IFacade facade)
        {
            MemberActivityType memberActivityType = facade.GetMemberActivityType((int)activityType);

            if (memberActivityType != null && objectId != 0 && creatorId != 0)
            {
                MemberActivity memberActivity = new MemberActivity();

                memberActivity.ActivityOn = (int)memberType;

                memberActivity.ActivityOnObjectId = objectId;
                memberActivity.CreateDate = DateTime.Now;
                memberActivity.CreatorId = creatorId;
                memberActivity.UpdateDate = DateTime.Now;
                memberActivity.UpdatorId = creatorId;
                memberActivity.MemberActivityTypeId = memberActivityType.Id;
                memberActivity = facade.AddMemberActivity(memberActivity);

                if (memberActivity.Id != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        //public static void PopulateMyTemplates(ListControl list, IFacade facade, int creatorId)
        //{
        //    list.Items.Clear();
        //    ArrayList dataSource = facade.GetAllContentByCreatorId(creatorId);
        //    if (dataSource != null && dataSource.Count > 0)
        //    {
        //        list.DataSource = dataSource;
        //        list.DataTextField = "Name";
        //        list.DataValueField = "Id";
        //        list.DataBind();
        //    }
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateMasterTemplates(ListControl list, IFacade facade, int creatorId)
        //{
        //    list.Items.Clear();
        //    ArrayList dataSource = facade.GetAllMasterContent(creatorId);
        //    if (dataSource != null && dataSource.Count > 0)
        //    {
        //        for (int i = 0; i < dataSource.Count;i++ )
        //        {
        //            string obj = dataSource[0].ToString();
        //            list.DataSource = dataSource;
        //            //list.DataTextField = "Name";
        //            //list.DataValueField = "Id";
        //            //list.DataBind();
        //        }
        //    }
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}
        public static void PopulateSubmissionStage(ListControl list, IFacade facade)
        {
            try
            {
                //list.Items.Clear();
                //list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.MemberSubmissionStage);
                //list.DataTextField = "Name";
                //list.DataValueField = "Id";
                //list.DataBind();
            }
            catch { }
        }
        //public static void PopulateCompanyContactEmails(ListControl list, int companyId, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllCompanyContactByComapnyId(companyId);
        //    list.DataTextField = "Email";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateCompanyContactLoginEmails(ListControl list, int companyId, IFacade facade, bool loginOnly)
        //{
        //    list.Items.Clear();

        //    IList<CompanyContact> contactList = facade.GetAllCompanyContactByComapnyId(companyId);

        //    if (contactList != null && contactList.Count > 0)
        //    {
        //        foreach (CompanyContact contact in contactList)
        //        {
        //            if (contact.MemberId > 0)
        //            {
        //                Member member = facade.GetMemberById(contact.MemberId);

        //                if (member != null)
        //                {
        //                    MembershipUser user = Membership.GetUser(member.UserId);

        //                    if (user != null)
        //                    {
        //                        list.Items.Add(new ListItem(user.Email, StringHelper.Convert(contact.Id)));
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (!loginOnly)
        //                {
        //                    list.Items.Add(new ListItem(contact.Email, StringHelper.Convert(contact.Id)));
        //                }
        //            }
        //        }
        //    }

        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}
        public static string GetMemberSignature(int memberId, IFacade facade)
        {
            string signature = string.Empty;
            MemberSignature memberSignature = facade.GetActiveMemberSignatureByMemberId(memberId);
            signature = (memberSignature != null ? memberSignature.Signature : string.Empty);
            return signature;
        }
        public static string GetMemberEmail(int memberId, IFacade facade)
        {
            Member member = facade.GetMemberById(memberId);
            return (member != null ? member.PrimaryEmail : string.Empty);
        }
        public static string GetMemberSignatureLogo(int memberId, IFacade facade)
        {
            string signatureLogo = string.Empty;
            if (memberId > 0)
            {
                MemberSignature memberSignature = facade.GetActiveMemberSignatureByMemberId(memberId);
                if (memberSignature != null) //Defect id 10300
                    signatureLogo = "../" + UrlConstants.MEMBER_DOC_DIR + "/" + memberId.ToString() + "/Signature/" + memberSignature.Logo;//(memberSignature != null ? memberSignature.Logo : string.Empty);
                else
                    signatureLogo = string.Empty;
            }
            return signatureLogo;
        }
        //public static void PopulateGuestHouse(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllGuestHouse();
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateGuestHouseDetail(ListControl list,int guestHouseId, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllGuestHouseDetailByGuestHouseId(guestHouseId);
        //    list.DataTextField = "SeatNumber";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateCompanyGroup(ListControl list, CompanyGroupType groupType, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllCompanyGroupByGroupType((int)groupType);
        //    list.DataTextField = "GroupName";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}
        public static string GetQueryStringPrefix(string url)
        {
            if (!url.Contains("?"))
            {
                return "?";
            }
            else
            {
                return "&";
            }
        }
        public static string GetClientNameByJobId(int jobId, IFacade facade)
        {
            string requisitionClientName = string.Empty;
            JobPosting jobPosting = facade.GetJobPostingById(jobId);
            int clientId = 0;
            if (jobPosting != null)
            {
                clientId = (jobPosting.ClientId > 0 ? jobPosting.ClientId : 0);
                if (clientId > 0)
                {
                    Company company = facade.GetCompanyById(clientId);
                    requisitionClientName = (company != null ? company.CompanyName : string.Empty);
                }
            }
            return requisitionClientName;
        }
        //public static void PopulateTrainingCourseModule(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllTrainingCourseModule();
        //    list.DataTextField = "Title";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateTrainingCourse(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllTrainingCourse();
        //    list.DataTextField = "Subject";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateHROfferLetter(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetHROfferLetterList();

        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateHROfferLetterWithEmail(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    IList<HROfferLetter> hROfferLetterList = facade.GetAllHROfferLetter();

        //    if (hROfferLetterList != null && hROfferLetterList.Count > 0)
        //    {
        //        foreach (HROfferLetter hROfferLetter in hROfferLetterList)
        //        {
        //            Member member = facade.GetMemberById(hROfferLetter.MemberId);
                    
        //            string memberEmail = string.Empty;

        //            if(member != null && member.Id > 0)
        //            {
        //                memberEmail = " ( " + member.PrimaryEmail + " )";
        //            }

        //            list.Items.Add(new ListItem(hROfferLetter.Position + memberEmail, hROfferLetter.Id.ToString()));
        //        }
        //    }

        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateHRTemplate(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllHRTemplate();

        //    list.DataTextField = "Title";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}
        public static void PopulateJobStatusList(DropDownList ddlJobStatus, int currentJobStatusValue)
        {
            ListItem removedItem;
            ControlHelper.PopulateEnumIntoList(ddlJobStatus, typeof(JobStatus));
            for (int i = ddlJobStatus.Items.Count - 1; i >= 0; i--)
            {
                if (Int32.Parse(ddlJobStatus.Items[i].Value) > 4)
                {
                    removedItem = ddlJobStatus.Items.FindByValue(ddlJobStatus.Items[i].Value);
                    ddlJobStatus.Items.Remove(removedItem);
                }
            }
            removedItem = ddlJobStatus.Items.FindByValue(currentJobStatusValue.ToString());
            if (removedItem != null)
            {
                ddlJobStatus.Items.Remove(removedItem);
            }
            ddlJobStatus.Items.Insert(0, new ListItem("Select status", "0"));
        }
        //public static void PopulateTrainingProgram(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    list.DataSource = facade.GetAllTrainingProgram();
        //    list.DataTextField = "Title";
        //    list.DataValueField = "Id";
        //    list.DataBind();

        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}
        public static string FindDateDiffFrmEndtoStart(DateTime dtEndDate, DateTime dtStartDate)
        {
            if (dtStartDate.Year == 1 || dtEndDate.Year == 1 || dtStartDate >dtEndDate ) return "N/A";
            string result;
            int[] arr = { 31, 28, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
            int Year, Day, Month, StartYear, EndYear, StartDay, EndDay, StartMonth, EndMonth;
            StartYear = dtStartDate.Year;
            EndYear = dtEndDate.Year;
            StartMonth = dtStartDate.Month;
            EndMonth = dtEndDate.Month;
            StartDay = dtStartDate.Day;
            EndDay = dtEndDate.Day;

           TimeSpan ts = dtEndDate.Subtract(dtStartDate);
           int noofdays = ts.Days + 1;
           float week = noofdays / 7;
            if(week>1)
            return result = week+" Weeks";
            return result = week + " Week";

            //
            if (EndDay < StartDay)
            {
                if (EndMonth == 1)
                    EndDay += arr[EndMonth - 1];
                else if (EndMonth == 2)
                {
                    if (DateTime.IsLeapYear(EndYear))
                        EndDay += arr[EndMonth];
                    else
                        EndDay += arr[EndMonth - 1];
                }
                else
                    EndDay += arr[EndMonth];

                EndMonth--;
            }

            if (EndMonth < StartMonth)
            {
                EndMonth += 12;
                EndYear--;
            }

            Year = EndYear - StartYear;
            Month = EndMonth - StartMonth;
            Day = EndDay - StartDay;

            result = (Year > 0 ? Year.ToString() + " Year(s) " : string.Empty) + (Month > 0 ? Month.ToString() + " Month(s) " : string.Empty) + (Day > 0 ? Day.ToString() + " Day(s)" : string.Empty);

            return result;
        }
        //public static void PopulateTrainingProgramTraineeMap(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();

        //    IList<TrainingProgramTraineeMap> traineeMapList = facade.GetAllTrainingProgramTraineeMap();
        //    if (traineeMapList != null & traineeMapList.Count > 0)
        //    {
        //        foreach (TrainingProgramTraineeMap traineeMap in traineeMapList)
        //        {
        //            list.Items.Add(new ListItem(GetMemberNameById(traineeMap.MemberId, facade), traineeMap.Id.ToString()));
        //        }
        //    }
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}
        public static void PopulateCompanyColumns(ListControl list)
        {
            list.Items.Clear();

            list.DataSource = new ArrayList{
                                            new { Key = "CompanyName", Value = "Company Name" },
                                            new { Key = "Address1", Value = "Address 1" },
                                            new { Key = "Address2", Value = "Address 2" },
                                            new { Key = "City", Value = "City" },
                                            new { Key = "State", Value = "State" },
                                            new { Key = "Country", Value = "Country" },
                                            new { Key = "CompanySize", Value = "Company Size" },
                                            new { Key = "IndustryType", Value = "Industry Type" },
                                            new { Key = "OwnerType", Value = "Owner Type" },
                                            new { Key = "TickerSymbol", Value = "Ticker Symbol" },
                                            new { Key = "OfficePhone", Value = "Office Phone" },
                                            new { Key = "TollFreePhone", Value = "Toll Free Phone" },
                                            new { Key = "FaxNumber", Value = "Fax Number" },
                                            new { Key = "PrimaryEmail", Value = "Primary Email" },
                                            new { Key = "SecondaryEmail", Value = "Secondary Email" },
                                            new { Key = "EINNumber", Value = "EIN Number" },
                                            new { Key = "AnnualRevenue", Value = "Annual Revenue" },
                                            new { Key = "NumberOfEmployee", Value = "Number of Employee" },
                                            new { Key = "IsEndClient", Value = "Is End Client" },
                                            new { Key = "IsTier1Client", Value = "Is Tier1 Client" },
                                            new { Key = "IsTier2Client", Value = "Is Tier2 Client" },
                                            new { Key = "IsVolumeHireClient", Value = "Is Volume Hire Client" },
                                            new { Key = "CreatorId", Value = "CreatorId" },
                                            new { Key = "UpdatorId", Value = "UpdatorId" },
                                            new { Key = "CreateDate", Value = "Create Date" },
                                            new { Key = "UpdateDate", Value = "Update Date" }
                                        };

            list.DataTextField = "Value";
            list.DataValueField = "Key";
            list.DataBind();
        }
        public static void PopulateLeadColumns(ListControl list)
        {
            list.Items.Clear();

            list.DataSource = new ArrayList{
                                            new { Key = "CompanyName", Value = "Company Name" },
                                            new { Key = "Description", Value = "Description" },
                                            new { Key = "LeadPriority", Value = "Lead Priority" },
                                            new { Key = "SalesProbability", Value = "Sales Probability" },
                                            new { Key = "ExpectedRevenue", Value = "Expected Revenue" },
                                            new { Key = "SalesLifeTime", Value = "Sales Life Time" },
                                            new { Key = "Status", Value = "Status" },                                            
                                            new { Key = "Source", Value = "Source" },
                                            new { Key = "CreatorId", Value = "Creator" },
                                            new { Key = "UpdatorId", Value = "Updator" },
                                            new { Key = "CreateDate", Value = "Create Date" },
                                            new { Key = "UpdateDate", Value = "Update Date" }
                                        };

            list.DataTextField = "Value";
            list.DataValueField = "Key";
            list.DataBind();
        }
        public static void SavePdfFromHtmlStringToFile(string filePath, string htmlString)
        {
            PdfConverter pdfConverter = new PdfConverter();
            UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            pdfConverter.PdfDocumentOptions.LeftMargin = 15;
            pdfConverter.PdfDocumentOptions.RightMargin = 5;
            pdfConverter.PdfDocumentOptions.TopMargin = 15;
            pdfConverter.PdfDocumentOptions.BottomMargin = 5;
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
            pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
            pdfConverter.PdfFooterOptions.DrawFooterLine = true;
            pdfConverter.PdfFooterOptions.PageNumberText = "Page";
            pdfConverter.PdfFooterOptions.ShowPageNumber = true;
            pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
            pdfConverter.SavePdfFromHtmlStringToFile(htmlString, filePath);
        }
        public static string CreateUserName(string userName, IFacade facade)
        {
            string memberId = facade.GetNewMermberIdentityId();
            userName += memberId + "@tps360bulkhire.com";
            return userName;
        }
        public static void PopulatePayCycle(ListControl list, IFacade facade)
        {
            list.Items.Clear();

            list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.PayCycle);
            list.DataTextField = "Name";
            list.DataValueField = "Id";
            list.DataBind();

            list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        }
        public static bool IsUserAdmin(string userName)
        {
            return Roles.IsUserInRole(userName, ContextConstants.ROLE_ADMIN);
        }
        //public static void PopulateHolidayCategory(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.HolidayCategory);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}
        public static string GetMailFooter(string receiverEmail, int memberId, bool isRemovalLinkAttached)
        {
            SecureUrl unSubscribeUrl = UrlHelper.BuildSecureUrl(ConfigurationManager.AppSettings["WebRoot"].ToString() + "/UnsubscribeMe.aspx", string.Empty, UrlConstants.PARAM_MEMBER_ID, memberId.ToString());//1.8

            StringBuilder sb = new StringBuilder();

            sb.Append(@"<table width='100%' border='0' cellspacing='1' cellpadding='3'>");

            if (isRemovalLinkAttached && !string.IsNullOrEmpty(receiverEmail) && memberId > 0)
            {
                sb.Append(@"<tr>
                            <td style='font-size: 10px;'>You have been subscribed as ");
                sb.Append(receiverEmail);
                sb.Append(". If you want to unsubscribe please ");
                sb.Append("<a href='");
                sb.Append(unSubscribeUrl.ToString());
                sb.Append(@"'>click here</a>.</td>
                          </tr>");
            }

            sb.Append(@"              
                          <tr>
                            <td style='font-size: 10px;'>Powered by: <a href='http://www.tps360.com' target='_blank' style='text-decoration: none;'>TPS360<sup>o</sup></a></td>
                          </tr>
                        </table>");

            return sb.ToString();
        }
        public static string GetAdministratorEmail()
        {
            return "faisal@tps360.com";
        }
        //public static void PopulateVHResumeSource(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    IList<CompanyContact> companyContactList = facade.GetAllCompanyContactByVolumeHireGroup();
        //    if (companyContactList != null && companyContactList.Count > 0)
        //    {
        //        foreach (CompanyContact contact in companyContactList)
        //        {
        //            list.Items.Add(new ListItem(contact.FirstName + " " + contact.LastName, contact.Id.ToString()));
        //        }
        //    }
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateJobPostingVolumeHireByStatus(ListControl list, IFacade facade, int jobStatus)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllJobPostingVolumeHireByStatus(jobStatus);
        //    list.DataTextField = "JobTitle";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem("Please select a requisition", "0"));
        //}
        public static string AlertInfo(string ago)
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(ago))
            {
                sb.Append(ago);
            }

            sb.Append(" ago.");

            return sb.ToString();
        }
        //public static void PopulatePercentageList(ListControl ddlList)
        //{
        //    for (int i = 0; i <= 100; i += 10)
        //    {
        //        ddlList.Items.Add(new ListItem(i.ToString() + "%", i.ToString()));
        //    }
        //    ddlList.Items.Insert(0, new ListItem("Please select", "0"));
        //}

        //public static void PopulateOfferValidityPeriod(ListControl list)
        //{
        //    list.Items.Clear();
        //    list.Items.Add(new ListItem("7", "7"));
        //    list.Items.Add(new ListItem("15", "15"));
        //    list.Items.Add(new ListItem("30", "30"));
        //    list.Items.Add(new ListItem("45", "45"));
        //    list.Items.Add(new ListItem("60", "60"));
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateProvisionPeriod(ListControl list)
        //{
        //    list.Items.Clear();

        //    list.Items.Add(new ListItem("30", "30"));
        //    list.Items.Add(new ListItem("60", "60"));
        //    list.Items.Add(new ListItem("90", "90"));
        //    list.Items.Add(new ListItem("120", "120"));
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateNoticePeriod(ListControl list)
        //{
        //    list.Items.Clear();

        //    list.Items.Add(new ListItem("7", "7"));
        //    list.Items.Add(new ListItem("15", "15"));
        //    list.Items.Add(new ListItem("30", "30"));
        //    list.Items.Add(new ListItem("45", "45"));
        //    list.Items.Add(new ListItem("60", "60"));
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateLeaveAllowedDays(ListControl list)
        //{
        //    for (int i = 1; i <= 12; i++)
        //    {
        //        list.Items.Add(new ListItem(i.ToString(), i.ToString()));
        //    }

        //    list.Items.Insert(0, new ListItem("Please select", "0"));
        //}

        //public static void PopulatePayDayList(ListControl list)
        //{
        //    list.Items.Clear();

        //    list.Items.Add(new ListItem("Monday", "Monday"));
        //    list.Items.Add(new ListItem("1st and 15th", "1st and 15th"));
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateSalaryPaymentTypeList(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.TaxTermType);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateSalaryPaymentByList(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.SalaryPaymentBy);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}

        //public static void PopulateSalaryPaymentNotification(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAllGenericLookupByLookupType(LookupType.SalaryPaymentNotification);
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //}

        //public static void PopulateNumberOfSale(ListControl list, int index, int lenght, int increment)
        //{
        //    for (int i = index; i <= lenght; i = i + increment)
        //    {
        //        list.Items.Add(new ListItem(i.ToString(), i.ToString()));
        //    }

        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}
        public static void MoveApplicantToHiringLevel(int jobpostingid, int memberid, int creatorId,int levelToMove, IFacade facade)
        {
            int level = levelToMove;
            MemberJobCart memberJobCart = facade.GetMemberJobCartByMemberIdAndJobPostringId(memberid, jobpostingid);
            if (memberJobCart == null)
            {
                memberJobCart = new MemberJobCart();
                memberJobCart.JobPostingId = jobpostingid;
                memberJobCart.MemberId = memberid;
                memberJobCart.IsPrivate = false;
                memberJobCart.UpdateDate = DateTime.Now;
                memberJobCart.UpdatorId = creatorId;
                memberJobCart.CreatorId = creatorId;
                memberJobCart.ApplyDate = DateTime.Now;
                memberJobCart = facade.AddMemberJobCart(memberJobCart);                
            }
            else if (memberJobCart.IsRemoved)
            {
                memberJobCart.IsRemoved = false;
                memberJobCart.UpdatorId = creatorId;
                memberJobCart.UpdateDate = DateTime.Now;
                facade.UpdateMemberJobCart(memberJobCart);                
            }
            if (memberJobCart != null)
            {
                IList<MemberJobCartDetail> memberJobCartDetailList = facade.GetAllMemberJobCartDetailByMemberJobCartId(memberJobCart.Id);
                if (memberJobCartDetailList != null && memberJobCartDetailList.Count > 0)
                {
                    foreach (MemberJobCartDetail memberJobCartDetail in memberJobCartDetailList)
                    {
                        if (memberJobCartDetail.IsRemoved == false)
                            //level = memberJobCartDetail.SelectionStepLookupId;
                            level = levelToMove;  // Sumit Sonawane on 1/Mar/2017
                        memberJobCartDetail.IsRemoved = true;
                        memberJobCartDetail.UpdatorId = creatorId;
                        memberJobCartDetail.UpdateDate = DateTime.Now;
                        facade.UpdateMemberJobCartDetail(memberJobCartDetail);
                    }
                }

                MemberJobCartDetail memberJobCartDetailNew = facade.GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(memberJobCart.Id, level);
                if (memberJobCartDetailNew != null)
                {
                    memberJobCartDetailNew.IsRemoved = false;
                    memberJobCartDetailNew.UpdatorId = creatorId;
                    memberJobCartDetailNew.UpdateDate = DateTime.Now;
                    facade.UpdateMemberJobCartDetail(memberJobCartDetailNew);
                }
                else
                {
                    memberJobCartDetailNew = new MemberJobCartDetail();
                    memberJobCartDetailNew.MemberJobCartId = memberJobCart.Id;
                    memberJobCartDetailNew.SelectionStepLookupId = levelToMove;
                    memberJobCartDetailNew.AddedDate = DateTime.Now;
                    memberJobCartDetailNew.AddedBy = creatorId;
                    memberJobCartDetailNew.CreatorId = creatorId;
                    facade.AddMemberJobCartDetail(memberJobCartDetailNew);
                }                
            }
        }
        //public static void PopulateAttorneyList(ListControl list, IFacade facade)
        //{
        //    list.Items.Clear();
        //    list.DataSource = facade.GetAttorneyList();
        //    list.DataTextField = "Name";
        //    list.DataValueField = "Id";
        //    list.DataBind();
        //    list.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT, "0"));
        //}
        public static string ConnectionString
        {
            get 
            {
                return WebConfigurationManager.ConnectionStrings[0].ConnectionString;
            }
        }
        public static string AppendSuffixAndPrefix(string src, string suffix, string prefix)
        {
            if (src.IsNullOrEmpty())
            {
                return string.Empty;
            }

            StringBuilder result = new StringBuilder();

            if ((suffix.IsNotNullOrEmpty()) && (suffix.StartsWith("?")))
            {
                suffix = suffix.Replace("?", string.Empty);
            }

            if (prefix.IsNullOrEmpty())
            {
                prefix += UrlConstants.ApplicationBaseUrl;
            }

            if ((prefix.IsNotNullOrEmpty()) && (!src.ContainsIgnoreCase(prefix)))
            {
                result.Append(prefix);
            }

            if (src.IsNotNullOrEmpty())
            {                
                result.Append(FixPath(src));
            }

            if ((suffix.IsNotNullOrEmpty()) && (!src.Contains(suffix)))
            {
                if (src.Contains("?"))
                {
                    suffix = "&" + suffix;
                }
                else
                {
                    suffix = "?" + suffix;
                }

                result.Append(suffix);
            }

            return result.ToString();
        }
        public static string FixPath(string src)
        {
            src = src.Replace("../", string.Empty);

            if (src.StartsWith("~/"))
            {
                src = src.Substring("~/".Length);
            }

            return src;
        }
        public static Control FindControlRecursive(Control root, string Id)
        {
            if (root.ID == Id)
                return root;

            foreach (Control control in root.Controls)
            {
                Control foundControl = FindControlRecursive(control, Id);

                if (foundControl != null)

                    return foundControl;
            }

            return null;
        }
        public static void LoadAllControlState(HttpRequest request, Control container)
        {
            foreach (string key in request.Form.AllKeys)
            {
                string value = string.Empty, nameOfControl = string.Empty;
                int lastPos = 0;
                if (key != null)
                {
                    value = request.Form[key];
                    lastPos = key.LastIndexOf('$');
                    nameOfControl = key;

                    if (lastPos > 0) nameOfControl = key.Substring(lastPos + 1);

                    Control c = FindControlRecursive(container, nameOfControl);
                    if (c != null)
                    {
                        if (c is DropDownList)
                        {
                            try
                            {
                                (c as DropDownList).SelectedValue = value;
                            }
                            catch
                            {
                            }
                        }
                        else if (c is TextBox)
                        {
                            (c as TextBox).Text = value;
                        }
                        else if (c is Label)
                        {
                            (c as Label).Text = value;
                        }                        
                        else if (c is RadioButton)
                        {
                            (c as CheckBox).Checked = (value != "" && value != "off" && value != "false");
                        }
                        else if (c is RadioButtonList)
                        {
                        }
                        else if (c is CheckBox)
                        {
                            (c as CheckBox).Checked = (value != "" && value != "off" && value != "false");
                        }
                        else if (c is HiddenField)
                        {
                            (c as HiddenField).Value = value;
                        }
                    }
                }
            }
        }
        public static bool IsValidMailSetting(IFacade Facade,int MemberId)
        {
            bool _mailsetting = false;
            Hashtable siteSettingTable = null;
            TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
            SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            if (siteSetting != null)
            {
                siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
            }
            
            
            MemberExtendedInformation MailSetting = Facade.GetMemberExtendedInformationByMemberId(MemberId);
            if (MailSetting.MailSetting != null)
            {
                Hashtable MailSettingTable = ObjectEncrypter.Decrypt<Hashtable>(MailSetting.MailSetting);
                if (siteSettingTable != null)
                {
                    if (siteSettingTable[DefaultSiteSetting.SMTP.ToString()] != string.Empty && MailSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString() != string.Empty && MailSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString() != string.Empty)
                        _mailsetting = true;
                    else
                        _mailsetting = false;
                }
            }
            else
                _mailsetting = false;
            return _mailsetting;
        }
        public static void LoadAllControlState(HttpRequest request, Control container, bool IsPageFromList)     //9997 added new overloaded method.
        {
            foreach (string key in request.Form.AllKeys)
            {
                string value = string.Empty, nameOfControl = string.Empty;
                int lastPos = 0;
                if (key != null)
                {
                    value = request.Form[key];
                    lastPos = key.LastIndexOf('$');
                    nameOfControl = key;

                    if (lastPos > 0) nameOfControl = key.Substring(lastPos + 1);

                    Control c = FindControlRecursive(container, nameOfControl);
                    if (c != null)
                    {
                        if (c is DropDownList)
                        {
                            (c as DropDownList).SelectedValue = value;
                        }
                        else if (c is TextBox)
                        {
                            (c as TextBox).Text = value;
                        }
                        else if (c is Label)
                        {
                            (c as Label).Text = value;
                        }
                        else if (c is RadioButton)
                        {
                            (c as CheckBox).Checked = (value != "" && value != "off" && value != "false");
                        }
                    }
                }
            }
        }
        public static bool IsDeleteButtonVisible(bool isAdmin,bool isCareerPortal)
        {
            if (isAdmin || isCareerPortal)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static string GetResumeServiceUrl(Page page)
        {
            string url = page.ResolveUrl("~/ResumeParserService.asmx");
            string port = string.Empty;
            if (HttpContext.Current.Request.Url.Port!=0)
            {
                port = ":" + HttpContext.Current.Request.Url.Port;
            }
            url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + port + url;
            return url;
        }
        public static void PopulatePaymentType(ListControl ddlPaymentType)
        {
            ControlHelper.PopulateEnumDescriptionIntoList(ddlPaymentType, typeof(PaymentType));
            ddlPaymentType.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_NONE, "0")); // Integer conversion error handled.
        }
        public  static string GetApplicationEdition(IFacade Facade)
        {
            SiteSetting sitesetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);

            if (sitesetting != null)
            {
                Hashtable config = ObjectEncrypter.Decrypt<Hashtable>(sitesetting.SettingConfiguration);
                if (config[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() != null)
                    return config[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() ;
                return UIConstants .STAFFING_FIRM ;
            }
            return UIConstants.STAFFING_FIRM;
        }
        public static CompanyStatus  GetCompanyStatusFromDefaultSiteSetting(IFacade Facade)
        {
            SiteSetting sitesetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);

            if (sitesetting != null)
            {
                Hashtable config = ObjectEncrypter.Decrypt<Hashtable>(sitesetting.SettingConfiguration);
                if (config[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule .ToString()].ToString() == TPS360.Web.UI.UIConstants.AccountType_Client)
                    return CompanyStatus.Client;
                else if (config[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule .ToString()].ToString() == TPS360.Web.UI.UIConstants.AccountType_Department )
                    return CompanyStatus.Department ;
                return CompanyStatus.Vendor ;
            }
            return CompanyStatus.Vendor;
        }
        public static string GetDefaultSiteSettings(DefaultSiteSetting defaultSiteSetting,  IFacade Facade)
        {
            SiteSetting sitesetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);

            if (sitesetting != null)
            {
                Hashtable config = ObjectEncrypter.Decrypt<Hashtable>(sitesetting.SettingConfiguration);
                if (config[defaultSiteSetting.ToString()].ToString() != null)
                    return config[defaultSiteSetting.ToString()].ToString();
                return "";
            }
            return "";
        }
        public static string getDefaultSearchAgentMailTemplate(int JobPostingId,int MemberId,out string JobTitle, IFacade Facade)
        {

            string strMess = "Hello &lt;candidate_first_name&gt;,<br/><br/>I came across your resume on a Job portal and wanted to know if you would be interested in the position below. If so, please respond with your updated resume and availability to discuss the position.";
            strMess += "<br/><br/><hr/>";

            string JobPostingCode = "";
            strMess += MiscUtil.GetJobDetailTable(JobPostingId, Facade, out JobTitle, out JobPostingCode, "",null,MemberId );
            JobTitle = "Open Job Position: " + JobTitle;

            MemberSignature signat = new MemberSignature();
            signat = Facade.GetActiveMemberSignatureByMemberId(MemberId);
            if (signat != null)
            {
                //Signature = signat.Signature;
                strMess += signat.Signature;
            }
            strMess += "<br/><br/>";
            string url = UrlConstants.ApplicationBaseUrl + "ATS/UnSubscribe.aspx";
            strMess += "If you do not wish to receive job opening notices from us, please <a href=\""+url+"\">click here to unsubscribe.</a>";
            return strMess;

        }

        public static bool isMainApplication
        {
            get
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"] != null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower() == "landt")
                    {
                        return false;
                    }
                    return true;
                }
                return true;
            }
        }
        public static string GetJobDetailTable(int JobID, IFacade Facade,out string JobTitle, out string JobPostingCode,string FromPage,JobPosting jobposting,int CurrentMemberID)
        {
            string PrintDetails = string.Empty;
            PrintDetails += "<Table width=\"100%\"; style=\" text-align :left; font-size :13px;\">";
            PrintDetails += "<TR style=\" width:100%\">";
            if (JobID > 0)
                jobposting = Facade.GetJobPostingById(JobID);
            //   this.Page.Title = jobposting.JobTitle;
            JobTitle = jobposting.JobTitle;
            JobPostingCode = jobposting.JobPostingCode;
            
            if (jobposting.ClientId > 0)
            {
                string accountName = "";
                Company com = Facade.GetCompanyById(Convert.ToInt32(jobposting.ClientId));
                if (com != null)
                {
                    CompanyStatus status = GetCompanyStatusFromDefaultSiteSetting(Facade);
                    int targetsitemap = 0;
                    int SiteMapParentID = 0;
                    if (status == CompanyStatus.Client)
                    {
                        targetsitemap = 235;
                        SiteMapParentID = 11;
                        accountName = "Company";
                        PrintDetails += AddNewTableData("Company :", 0);
                    }
                    else if (status == CompanyStatus.Department && FromPage != "Vendor" && FromPage!="Email")
                    {
                        targetsitemap = 626;
                        SiteMapParentID = 622;
                        accountName = "BU";
                        PrintDetails += AddNewTableData("BU :", 0);
                    }
                    else if(FromPage!="Vendor" && FromPage!="Email")
                    {
                        targetsitemap = 642;
                        SiteMapParentID = 638;
                        accountName = "Vendor";
                        PrintDetails += AddNewTableData("Vendor :", 0);
                    }
                    string comname = com.CompanyName;
                    if (FromPage == "JobDetail" ||FromPage=="Print" && FromPage!="Vendor")
                    {
                        CustomSiteMap CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(targetsitemap, CurrentMemberID);
                        if (CustomMap != null)
                        {
                            SecureUrl url = UrlHelper.BuildSecureUrl("../" + CustomMap.Url, string.Empty, UrlConstants.PARAM_COMPANY_ID, jobposting.ClientId.ToString(), UrlConstants.PARAM_SITEMAP_ID, CustomMap.Id.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, SiteMapParentID.ToString());

                            comname = "<a href='" + url.ToString() + "' Target='_blank'>" + com.CompanyName + "</a>";
                        }
                        PrintDetails += AddNewTableData(comname, 1);
                        PrintDetails += AddNewRow();
                    }
                   

                }
                if (FromPage == "Print" || FromPage == "JobDetail" && FromPage != "Vendor" && FromPage!="Email")
                {
                    if (jobposting.ClientContactId > 0)
                    {
                        CompanyContact contact = Facade.GetCompanyContactById(jobposting.ClientContactId);
                        if (contact != null)
                        {
                            PrintDetails += AddNewTableData(accountName + " Contact :", 0);
                            PrintDetails += AddNewTableData(contact.FirstName + " " + contact.LastName, 1);
                            PrintDetails += AddNewRow();
                        }
                    }
                }
            }
            string ApplicationEdition = MiscUtil.GetApplicationEdition(Facade);
            if (FromPage == "Print" || FromPage == "JobDetail")
            {
                if (jobposting.ClientJobId.Trim() != string.Empty)
                {
                    if (ApplicationEdition == UIConstants.GOVERNMENT_PORTAL)
                    {
                        PrintDetails += AddNewTableData("Announcement # :", 0);
                    }
                    else if (ApplicationEdition == UIConstants.INTERNAL_HIRING)
                    {
                        PrintDetails += AddNewTableData("Job ID :", 0);
                    }
                    //commented because the Client Job ID should not be known to the candidate
                    else
                    {
                        PrintDetails += AddNewTableData("Client Job ID :", 0);
                    }
                    PrintDetails += AddNewTableData(jobposting.ClientJobId, 1);
                    PrintDetails += AddNewRow();
                }
            }

            //Add Job Title 
            PrintDetails += AddNewTableData("Job Title :", 0);
            PrintDetails += AddNewTableData(jobposting.JobTitle, 1);
            PrintDetails += AddNewRow();

            if (jobposting.JobCategoryLookupId > 0 && FromPage!="Vendor" && FromPage!="Email")
            {
         
                PrintDetails += AddNewTableData("Job Category :", 0);
                if (isMainApplication)
                {
                    Category catefory = Facade.GetCategoryById(jobposting.JobCategoryLookupId);
                    if (catefory!=null)
                        PrintDetails += AddNewTableData(catefory.Name, 1);
               
                }
                else {
                    string nameValue = "";
                    GenericLookup generic=Facade.GetGenericLookupById(jobposting.JobCategoryLookupId);
                    if(generic!=null)
                    nameValue = generic.Name;
                    if (nameValue !="")
                    {
                        PrintDetails += AddNewTableData(Facade.GetGenericLookupById(jobposting.JobCategoryLookupId).Name, 1);
               
                    }

                    
                }
                PrintDetails += AddNewRow();
            }
            if (jobposting.NoOfOpenings > 0 && FromPage!="Vendor" && FromPage!="Email")
            {
                PrintDetails += AddNewTableData("No. of Openings :", 0);
                PrintDetails += AddNewTableData(jobposting.NoOfOpenings.ToString(), 1);
                PrintDetails += AddNewRow();
            }
            
            //if (ApplicationEdition == UIConstants.INTERNAL_HIRING)
            //{
            //    //Department
            //    if (jobposting.JobDepartmentLookUpId != null)
            //    {
            //        if (jobposting.JobDepartmentLookUpId > 0)
            //        {
            //            PrintDetails += AddNewTableData("Department :", 0);
            //            PrintDetails += AddNewTableData(Facade.GetDepartmentById(jobposting.JobDepartmentLookUpId).Name, 1);
            //            PrintDetails += AddNewRow();
            //        }
            //    }
            //}
            //else              


            if (jobposting.OccuptionalSeriesLookupId > 0)
            {
                PrintDetails += AddNewTableData("Occupational Series :", 0);
                OccupationalSeries Occuseries = Facade.GetOccupationalSeriesById(jobposting.OccuptionalSeriesLookupId);
                PrintDetails += AddNewTableData(Occuseries == null ? "" : Occuseries.SeriesTitle, 1);
                PrintDetails += AddNewRow();

            }            
            
            if (jobposting.WorkScheduleLookupId > 0)
            {

                GenericLookup glookup = Facade.GetGenericLookupById(jobposting.WorkScheduleLookupId);
                if (glookup != null)
                {
            
                    PrintDetails += AddNewTableData("Work Schedule :", 0);
                    PrintDetails += AddNewTableData(glookup.Name  .ToString(), 1);
                    PrintDetails += AddNewRow();
                }

            }

            if (jobposting.PayGradeLookupId > 0)
            {
                PrintDetails += AddNewTableData("Pay Grade(GS) :", 0);
                PrintDetails += AddNewTableData(jobposting.PayGradeLookupId.ToString(), 1);
                PrintDetails += AddNewRow();
            }



            //If Start Date Available add  startdate data
            if (jobposting.StartDate.ToLower().Trim() != string.Empty && FromPage!="Vendor" && FromPage!="Email")
            {

                PrintDetails += AddNewTableData("Date of Requisition :", 0);
                 string startdate = string.Empty;
                if (System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern  == "dd/MM/yyyy")
                {startdate = jobposting.StartDate.ToString() == "" || jobposting.StartDate.ToString().Trim() == "ASAP" ? jobposting.StartDate.ToString() : (jobposting.StartDate.ToString().Split('/')[1] + "/" + jobposting.StartDate.ToString().Split('/')[0] + "/" + jobposting.StartDate.ToString().Split('/')[2]);
                }
                else
                {startdate = jobposting.StartDate.Trim() != string.Empty ? (jobposting.StartDate.Trim() != "ASAP" ? Convert.ToDateTime(jobposting.StartDate).ToShortDateString() : "ASAP") : " ";
                }
                PrintDetails += AddNewTableData(startdate , 1);
                PrintDetails += AddNewRow();
            }
            if (FromPage == "Print" ||  FromPage =="JobDetail")
            {
                if (jobposting.FinalHiredDate != DateTime.MinValue)
                {
                    PrintDetails += AddNewTableData("Expected Fulfillment Date :", 0);
                    PrintDetails += AddNewTableData(jobposting.FinalHiredDate.ToShortDateString(), 1);
                    PrintDetails += AddNewRow();
                }
            }
            if (FromPage == "Print" || FromPage == "JobDetail")
            {
                if (jobposting.OpenDate != DateTime.MinValue)
                {
                    PrintDetails += AddNewTableData("Open Date :", 0);
                    PrintDetails += AddNewTableData(jobposting.OpenDate.ToShortDateString(), 1);
                    PrintDetails += AddNewRow();
                }
            }
            //if (FromPage == "Print" || FromPage == "JobDetail")
            //{
            //    if (jobposting.ClientHourlyRate != null && jobposting.ClientHourlyRate.Trim() != "")
            //    {
            //        GenericLookup gsalary1 = new GenericLookup();
            //        string pay = "";
            //        gsalary1 = Facade.GetGenericLookupById(jobposting.ClientHourlyRateCurrencyLookupId);
            //        if (gsalary1 != null)
            //        {
            //            if (jobposting.PayRateCurrencyLookupId == 49)
            //            {
            //                pay = gsalary1.Name.Substring(0, 1);
            //                pay += jobposting.ClientHourlyRate;
            //            }
            //            else
            //            {
            //                pay = jobposting.ClientHourlyRate;
            //                pay += " " + gsalary1.Name;
            //            }
            //        }
            //        else
            //        {
            //            pay = jobposting.ClientHourlyRate;
            //        }
            //        string paycycle1 = "";
            //        switch (jobposting.ClientRatePayCycle)
            //        {
            //            case "1":
            //                paycycle1 = "Hour";
            //                break;
            //            case "4":
            //                paycycle1 = "Year";
            //                break;
            //            case "3":
            //                paycycle1 = "Month";
            //                break;
            //            case "2":
            //                paycycle1 = "Day";
            //                break;
            //        }
            //        if (paycycle1.Trim() != string.Empty) pay += " / " + paycycle1;

            //        PrintDetails += AddNewTableData("Bill Rate :", 0);
            //        PrintDetails += AddNewTableData(pay, 1);
            //        PrintDetails += AddNewRow();

            //    }



            //}
            if (FromPage != "Vendor" && FromPage!="Email")
            {
                string payrate = "";
                payrate = jobposting.PayRate;
                if (Convert.ToInt32(jobposting.MaxPayRate) == jobposting.MaxPayRate) jobposting.MaxPayRate = Convert.ToInt32(jobposting.MaxPayRate);
                if (payrate != string.Empty && jobposting.MaxPayRate > 0 && payrate.ToLower().Trim() != "open") payrate += "-";
                if (jobposting.MaxPayRate > 0 && payrate.ToLower().Trim() != "open") payrate += jobposting.MaxPayRate;
                GenericLookup gsalary = new GenericLookup();
                string salary = "";
                if (jobposting.PayRateCurrencyLookupId > 0 && jobposting.PayRate.Trim().ToLower() != "open") gsalary = Facade.GetGenericLookupById(jobposting.PayRateCurrencyLookupId);
                else gsalary = null;
                if (gsalary != null)
                {
                    if (jobposting.PayRateCurrencyLookupId == 49)
                    {
                        salary = gsalary.Name.Substring(0, 1);
                        salary += payrate;
                    }
                    else
                    {
                        salary = payrate;
                        salary += " " + "$USD";
                    }
                }
                else
                {
                    salary = payrate;
                }


                string paycycle = "";
                switch (jobposting.PayCycle)
                {
                    case "1":
                        paycycle = "Hour";
                        break;
                    case "4":
                        paycycle = "Year";
                        break;
                    case "3":
                        paycycle = "Month";
                        break;
                    case "2":
                        paycycle = "Day";
                        break;
                }
                if (paycycle.Trim() != string.Empty) salary += " / " + paycycle;

                if (!isMainApplication)
                {
                    PrintDetails += AddNewTableData("Bill Rate :", 0);
                    PrintDetails += AddNewTableData(salary, 1);
                }
                PrintDetails += AddNewRow();
                if (jobposting.SalesRegionLookUpId != 0)
                {
                    PrintDetails += AddNewTableData("Sales Region :", 0);
                    PrintDetails += AddNewTableData(Facade.GetGenericLookupById(jobposting.SalesRegionLookUpId).Name, 1);
                    PrintDetails += AddNewRow();
                }
                if (jobposting.SalesGroupLookUpId != 0)
                {
                    PrintDetails += AddNewTableData("Sales Group :", 0);
                    PrintDetails += AddNewTableData(Facade.GetGenericLookupById(jobposting.SalesGroupLookUpId).Name, 1);
                    PrintDetails += AddNewRow();
                }
            }
            //State Data
            //if (jobposting.StateId > 0)
            //{
            //    string add = jobposting.JobAddress1;
            //    add = add == string.Empty ? jobposting.JobAddress2 : add + ", " + jobposting.JobAddress2;
            //    add = add == string.Empty ? jobposting.City : add + ", " + jobposting.City;
            //    add = jobposting.StateId != 0 ? add + (add != "" ? ", " : "") + Facade.GetStateById(jobposting.StateId).Name : add;
            //    add = jobposting.CountryId != 0 ? add + (add != "" ? ", " : "") + Facade.GetCountryById(jobposting.CountryId).Name : add;
            //    add = jobposting.ZipCode != "" ? add + ", " + jobposting.ZipCode : add + "";

            //}
           
            if (jobposting.SalesGroupLookUpId != 0  )
            {
                PrintDetails += AddNewTableData("City :", 0);
                PrintDetails += AddNewTableData(jobposting.City, 1);
                PrintDetails += AddNewRow();
            }

            if (isMainApplication)
            {
                if (jobposting.City != null)
                {
                    PrintDetails += AddNewTableData("City :", 0);
                    PrintDetails += AddNewTableData(jobposting.City, 1);
                    PrintDetails += AddNewRow();
                }
            }

            if (jobposting.CountryId != 0)
            {
                PrintDetails += AddNewTableData("Country :", 0);
                PrintDetails += AddNewTableData(Facade.GetCountryById(jobposting.CountryId).Name, 1);
                PrintDetails += AddNewRow();
            }
            if (jobposting.ReportingTo!=null && jobposting.ReportingTo.Trim() != string.Empty)
            {
                PrintDetails += AddNewTableData("Reporting To :", 0);
                PrintDetails += AddNewTableData(jobposting .ReportingTo , 1);
                PrintDetails += AddNewRow();
            }

            if (jobposting.NoOfReportees >0)
            {
                PrintDetails += AddNewTableData("No. of Reportees :", 0);
                PrintDetails += AddNewTableData(jobposting.NoOfReportees .ToString (), 1);
                PrintDetails += AddNewRow();
            }
            if (jobposting.JobLocationLookUpID != 0)
            {
                PrintDetails += AddNewTableData("Job Location :", 0);
                PrintDetails += AddNewTableData(Facade.GetGenericLookupById(jobposting.JobLocationLookUpID).Name, 1);
                PrintDetails += AddNewRow();
            }


            if (jobposting.JobSkillLookUpId.Trim() != string.Empty && FromPage != "JobDetail")
            {
                //SkillsData
                PrintDetails += AddNewTableData("Skills :", 0);
                PrintDetails += AddNewTableData(SplitSkillValues(jobposting.JobSkillLookUpId, '!',Facade ), 1);
                PrintDetails += AddNewRow();
            }

            if ( FromPage !="JobDetail" && jobposting.MinimumQualifyingParameters != null && jobposting.MinimumQualifyingParameters.Trim() != string.Empty)
            {
                //Minimum Qulaifying Parameters
                PrintDetails += AddNewTableData("Minimum Qualifying Parameters:", 0);
                char[] delim = { ',' };
                string[] MQP = jobposting.MinimumQualifyingParameters.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                string strMQP = "<ui>";
                foreach (string s in MQP)
                {
                    strMQP += "<li>" + s + " </li>";

                }
                strMQP += "</ui>";
                PrintDetails += "<TD style=\" text-align :left; Width:72%; vertical-align :top  \" align=\"left\"> " + strMQP + " </TD>";
                PrintDetails += AddNewRow();
            }
            if (jobposting.JobDurationLookupId !=string .Empty && jobposting .JobDurationLookupId !="0" )
            {
                //GenericLookup glookup = Facade.GetGenericLookupById(Convert .ToInt32 ( jobposting.JobDurationLookupId));
                PrintDetails += AddNewTableData("Employment Type :", 0);
                PrintDetails += AddNewTableData(SplitValues (jobposting .JobDurationLookupId ,',',Facade ), 1);
                PrintDetails += AddNewRow();
            }


            if (jobposting.MinExpRequired.Trim() != string.Empty || jobposting.MaxExpRequired.Trim() != string.Empty)
            {
                PrintDetails += AddNewTableData("Experience Required :", 0);
                PrintDetails += AddNewTableData(jobposting.MinExpRequired + ((jobposting.MinExpRequired.Trim() != string.Empty && jobposting.MaxExpRequired.Trim() != string.Empty) ? " - " : "") + jobposting.MaxExpRequired.Trim() + " years", 1);
                PrintDetails += AddNewRow();
            }

            if (jobposting .JobDurationMonth !=null && jobposting .JobDurationMonth !="" && jobposting.JobDurationMonth != "0")
            {
                PrintDetails += AddNewTableData("Duration :", 0);
                GenericLookup glookup = Facade.GetGenericLookupById(Convert.ToInt32(jobposting.JobDurationMonth));
                PrintDetails += AddNewTableData(HttpUtility .HtmlEncode ( glookup.Name), 1);
                PrintDetails += AddNewRow();
            }
            if (jobposting.TaxTermLookupIds!=null && jobposting.TaxTermLookupIds.Trim() != string.Empty)
            {
                PrintDetails += AddNewTableData("Payment Type :", 0);
                PrintDetails += AddNewTableData(SplitValues(jobposting.TaxTermLookupIds, ',',Facade ), 1);
                PrintDetails += AddNewRow();

            }

            if (jobposting.RequiredDegreeLookupId !=string .Empty && jobposting .RequiredDegreeLookupId !="0")
            {
                PrintDetails += AddNewTableData("Education :", 0);
                PrintDetails += AddNewTableData(SplitValues( jobposting.RequiredDegreeLookupId,',',Facade ), 1);
                PrintDetails += AddNewRow();
            }

            if (jobposting.TeleCommunication == true)
            {
                PrintDetails += AddNewTableData("Telecommuting :", 0);
                PrintDetails += AddNewTableData("Yes", 1);
                PrintDetails += AddNewRow();
            }

            if (jobposting.TravelRequired == true)
            {
                PrintDetails += AddNewTableData("Travel Required :", 0);
                PrintDetails += AddNewTableData("Yes" + (jobposting.TravelRequiredPercent=="0"?"": "-" + jobposting.TravelRequiredPercent + "%"), 1);
                PrintDetails += AddNewRow();
            }

            if (jobposting.AuthorizationTypeLookupId.Trim() != string.Empty)
            {
                PrintDetails += AddNewTableData("Work Authorization :", 0);
                PrintDetails += AddNewTableData(SplitValues(jobposting.AuthorizationTypeLookupId, ',',Facade), 1);
                PrintDetails += AddNewRow();
            }

            if (jobposting.SecurityClearance == true)
            {
                PrintDetails += AddNewTableData("Security Clearance :", 0);
                PrintDetails += AddNewTableData("Yes", 1);
                PrintDetails += AddNewRow();
            }

            if (jobposting.IsExpensesPaid == true)
            {
                PrintDetails += AddNewTableData("Expenses Paid :", 0);
                PrintDetails += AddNewTableData("Yes", 1);
                PrintDetails += AddNewRow();
            }
            if (jobposting.OtherBenefits.Trim() != string.Empty && FromPage !="JobDetail")
            {

                PrintDetails += AddNewTableData("Benefits :", 0);
                PrintDetails += AddNewTableData(SplitValues(jobposting.OtherBenefits, '!', Facade), 1);
                PrintDetails += AddNewRow();
            }
            if (jobposting.CustomerName.Trim() != string.Empty && FromPage != "Vendor" && FromPage!="Email")
            {
                PrintDetails += AddNewTableData("Customer Name :", 0);
                PrintDetails += AddNewTableData(jobposting.CustomerName, 1);
                PrintDetails += AddNewRow();

            }
            if (FromPage != "Vendor" && FromPage!="Email" && jobposting .POAvailability !=0) 
            {
                string PoAvailability = "";
                if (jobposting.POAvailability == 1)
                {
                    PoAvailability = "Yes";
                }
                else if (jobposting.POAvailability == 2)
                {
                    PoAvailability = "No";
                }
                else if (jobposting.POAvailability == 3)
                {
                    PoAvailability = "NA";
                }
                PrintDetails += AddNewTableData("PO Availability :", 0);
                PrintDetails += AddNewTableData(PoAvailability, 1);
                PrintDetails += AddNewRow();
                
            }
            if (jobposting.RequisitionType != string.Empty && jobposting.RequisitionType != "" && jobposting.RequisitionType != "0")
            {
                PrintDetails += AddNewTableData("Requisition Type :", 0);
                try
                {
                    PrintDetails += AddNewTableData(Facade.GetGenericLookupById(Convert.ToInt32(jobposting.RequisitionType)).Name, 1);
                }
                catch { }
                PrintDetails += AddNewRow();

            }
            if (jobposting.JobDescription != string.Empty && FromPage != "JobDetail" || FromPage=="Email")
            {
                PrintDetails += "<TD style=\" text-align :left;font-weight :bold;\"> Job Description: </TD>";
                PrintDetails += "<TD style=\" width:60%\" align=\"left\"> </TD>";
                PrintDetails += "</TR>";

                PrintDetails += "<TR style=\" width:100%\">";
                PrintDetails += "<TD colspan=\"2\" align=\"justify\" style=\" width:80%; white-space : normal ;\"  width=\"80%\"> " + jobposting.JobDescription.Trim() + "  </TD>";
                PrintDetails += "</TR>";
                //PrintDetails += "<TR style=\" width:100%\">";
            }
            
            if (FromPage == "Print")
            {
                if (jobposting.ClientBrief != string.Empty)
                {
                    if (jobposting.RawDescription != string.Empty) PrintDetails += AddNewRow();
                    PrintDetails += "<TD style=\" text-align :left;font-weight :bold;\" class=\"TableFormLeble\"> Client Brief: </TD>";
                    PrintDetails += "<TD style=\" width:60%\" align=\"left\"> </TD>";
                    PrintDetails += "</TR>";

                    PrintDetails += "<TR style=\" width:100%\">";
                    PrintDetails += "<TD colspan=\"2\" align=\"justify\" style=\" width:80%; white-space : normal ;\"  width=\"80%\"> " + jobposting.ClientBrief.Trim() + "  </TD>";
                }

            }

           
            //else
            //    PrintDetails = "</TR>";//PrintDetails.Substring(0, PrintDetails.Length - 24);
            PrintDetails += "</TR></Table>";
            return PrintDetails;
        }
        private static string AddNewTableData(string Content, int side)
        {
            if (side == 0)
            {
                return "<TD style=\" text-align :left;font-weight :bold; Width:28%; vertical-align :top  \" > " + Content + " </TD>";
            }
            else
            {
                return "<TD style=\" text-align :left; Width:72%; vertical-align :top; white-space : normal ;  \" align=\"left\" > " + Content + " </TD>";
            }
        }
        private static string AddNewRow()
        {
            return "</TR><TR style=\" width:100%\">";
        }
        public  static string  SplitValues(string value, char delim,IFacade Facade)
        {
            if (value == null) value = "";
            string result = "";
            try
            {

                char[] delimiters = new char[] { delim, '\n' };
                IList<string> splitarray = value.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in splitarray)
                {
                    if (Convert.ToInt32(s) > 0)
                    {
                        if (result.Length > 0) result += ", ";
                        result += Facade.GetGenericLookupById(Convert.ToInt32(s)).Name;
                    }
                }
            }
            catch
            {
            }
            return MiscUtil .RemoveScript ( result);
        }
        public  static string SplitSkillValues(string value, char delim,IFacade Facade)
        {
            string result = string.Empty;
            char[] delimiters = new char[] { delim, '\n' };
            IList<string> splitarray = value.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in splitarray)
            {
                if (Convert.ToInt32(s) > 0)
                {
                    if (result.Length > 0) result += ", ";
                    Skill skill=Facade.GetSkillById(Convert.ToInt32(s));
                    string str=string .Empty ;
                    if(skill !=null)
                    {
                        if(skill .Name .Trim ().Contains (",") || skill .Name .Trim ().Contains (" ")) str="'" + skill .Name .Trim () + "'";
                        else str=skill .Name .Trim ();
                    }
                    if (str != string.Empty)
                        result += str;
                }
            }
            return result;
        }
        public  static System.Globalization.CultureInfo GetDateCulture(int CountryID, IFacade Facade)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
            culture.DateTimeFormat.DateSeparator = "/";
            if (CountryID > 0)
            {
                Country country = Facade.GetCountryById(CountryID);
                if (country != null)
                {
                    if (country.Name == "India")
                    {
                       
                        culture.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
                        return culture;
                    }
                }

            }
           
            culture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy";
            return culture;
        }
        public static bool ReturnDashboard(string url, int MemberId, IFacade facade)
        {
            string returnUrl = url;
            string RedirectUrl = string.Empty;
            bool IsRedirect = false;
            if (!string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = returnUrl.Replace("%2f", "/");
                string[] returnUrlPart = returnUrl.Split('/');

                if (returnUrl.Length > 2)
                {
                    //foreach (string Urlstr in returnUrlPart)
                    for (int i = 0; i < returnUrlPart.Length; i++)
                    {
                        if (returnUrlPart[i].Contains("?"))
                        {
                            RedirectUrl = returnUrlPart[i-1].ToString() + "/" + returnUrlPart[i].Substring(0, returnUrlPart[i].IndexOf('?'));
                        }
                    }
                    if (RedirectUrl == string.Empty)
                    {
                        RedirectUrl = returnUrlPart[returnUrlPart.Length - 2].ToString() + "/" + returnUrlPart[returnUrlPart.Length - 1];
                    }
                    IsRedirect = facade.GetMemberPrivilegeForMemberIdAndRequiredURL(MemberId, RedirectUrl.Trim());
                     return IsRedirect;
                }

                
            }
            return false;
        }
        
        public static  string RemoveScript(string script)
        {
            script = HttpUtility.HtmlDecode(script);
            script = HttpUtility.HtmlEncode(script);
            return script;
        }
        public static  string RemoveScript(string script,string text)
        {
            script = HttpUtility.HtmlDecode(script);
            return script;
        }
        public static string ScriptRemove(string script)
        {
            string text = Regex.Replace(script, "<script.*?</script>", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            text = Regex.Replace(text, "<.*?>", string.Empty);
            return Remove(text);
        }
        public static string Remove(string script)
        {

            if (script.Contains("<script"))
            {
                int i = script.IndexOf("<script");
                int j = script.IndexOf(">");
                if (j > i) script = script.Substring(0, i) + script.Substring(j, script.Length);
                else script = script.Substring(0, i);

            }
            return script;
        }
        public static DateTime  GetMMddyyyyFormat(string date)
        {
            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.ShortDatePattern = "dd/MM/yyyy";
            DateTime ClosignDate = Convert.ToDateTime(date, dateInfo);
            dateInfo.ShortDatePattern = "MM/dd/yyyy";
            ClosignDate = Convert.ToDateTime(ClosignDate, dateInfo);
            return ClosignDate;
        }
        public static string ToRoman(int _n)
        {
            if (_n <= 0) return "";

            int[] Nums = { 1 , 4 , 5 , 9 , 10 , 40 ,
50, 90 , 100, 400, 500, 900, 1000};
            string[] RomanNums = {"I", "IV", "V", "IX", "X", "XL",
"L", "XC", "C", "CD", "D", "CM", "M"};

            string sRtn = ""; int n = _n;
            for (int i = Nums.Length - 1; i >= 0; --i)
            {
                while (n >= Nums[i])
                {
                    n -= Nums[i];
                    sRtn += RomanNums[i];
                }
            }
            return sRtn;
        }

        public static string getEventLogReport(int JobPostingId, int CandidateId, int CreatorId,bool ShowUser, bool ShowReq, IFacade facade)
        {
             
            StringBuilder builder = new StringBuilder();
             string LogoPath = UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png";

             builder.Append("<table style='width:100%;' align='left' border = '1' bordercolor='#000000' cellspacing='0'>");
             builder.Append("    <tr>");
             builder.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + LogoPath + "' style= style='height:56px;width:56px'/></td>"); //0.7
             builder.Append("    </tr>");

             builder.Append(" <tr>");

            builder.Append("<tr><th>Date & Time </th>");
            if(ShowUser){
                builder.Append("<th>User</th>");
            }
            if(ShowReq){
                builder.Append("<th>Requisition</th>");
            }
            builder.Append("<th>Action</th></tr>");
            IList<EventLogForRequisitionAndCandidate> log = facade.GetAllEventLog(JobPostingId, CandidateId,CreatorId );
            if (log != null)
            {
                foreach (EventLogForRequisitionAndCandidate logitem in log)
                {
                    builder.Append("<tr>");
                    builder.Append("<td align=left>" + logitem.ActionDate.ToShortDateString()+" "+ logitem.ActionDate.ToShortTimeString()+ "</td>");                                     
                    if(ShowUser){
                        builder.Append("<td>");      
                        builder.Append(logitem.UserName);
                        builder.Append("</td>");
                    }
                    if(ShowReq){
                        builder.Append("<td>");      
                        builder.Append(logitem .JobTitle);
                        builder.Append("</td>");
                    }                    
                    builder.Append("<td>" + logitem.ActionType + "</td>");
                    builder.Append("</tr>");

                }
            }

            builder.Append("    <tr>");
            builder.Append("        <td align=left  width='300' height='75' >&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png'  style= style='height:56px;width:56px;'/></td>");//0.1
            builder.Append("    </tr>");




           
            builder.Append("</Table>");
            return builder.ToString();
        }
        //added by pravin khot on 9/Aug/2016***********       
        public static void PopulateBUContactIdByBUId(ListControl list, IFacade facade, int BUId)
        {

            list.Items.Clear();
            ArrayList jobList;
            if (BUId > 0)
                jobList = facade.GetAllBUContactIdByBUId(BUId);
            else jobList = null;
            if (jobList == null) jobList = new ArrayList();

            list.SelectedValue = "0";
            jobList.Insert(0, new JobPosting () { Id = 0, Name = (jobList.Count > 0 ? UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_BUContact : "No Contact") });
            list.DataSource = jobList;
            list.DataValueField = "Id";
            list.DataTextField = "Name";
            list.DataBind();
            if (list.Items.Count == 1) list.Enabled = false;
            else list.Enabled = true;
        }
        //*****************************END************************************
        //*****************************END************************************
        //Function introduced by Prasanth on 20Jan2017 iisue id 1153
        public static bool Check_FileExtension_Permission(string Extension, string ContentType)
        {
            bool result = false;

            if (ContentType == "application/vnd.ms-excel" && Extension.ToLower() == ".csv")
            {
                result = true;
            }
            else if (ContentType == "application/msword" && Extension.ToLower() == ".doc")
            {
                result = true;
            }
            else if (ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" && Extension.ToLower() == ".docx")
            {
                result = true;
            }
            else if (ContentType == "image/jpeg" && Extension.ToLower() == ".jpg")
            {
                result = true;
            }
            else if (ContentType == "application/pdf" && Extension.ToLower() == ".pdf")
            {
                result = true;
            }
            else if (ContentType == "application/vnd.ms-powerpoint" && Extension.ToLower() == ".ppt")
            {
                result = true;
            }
            else if (ContentType == "application/msword" && Extension.ToLower() == ".rtf")
            {
                result = true;
            }
            else if (ContentType == "text/plain" && Extension.ToLower() == ".txt")
            {
                result = true;
            }
            else if (ContentType == "application/vnd.ms-excel" && Extension.ToLower() == ".xls")
            {
                result = true;
            }
            else if (ContentType == "application/octet-stream" && Extension.ToLower() == ".zip")
            {
                result = true;
            }

            return result;
        }


        //Function introduced by Prasanth on 17Feb2017 iisue id 1153
        public static bool Check_FileMIMEType(FileUpload fudocument)
        {
            bool result = false;

            string response = string.Empty;
            HttpPostedFile file = fudocument.PostedFile;
            byte[] buffer = new byte[257];
            string xmlfilepath = System.Web.Hosting.HostingEnvironment.MapPath("~/MIMEType.xml");






            string MimeType = null;
            file.InputStream.Read(buffer, 0, 256);
            file.InputStream.Position = 0;

            string magicNumber = BitConverter.ToString(buffer);
            magicNumber = magicNumber.Replace("-", " ");




            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlfilepath);

            XmlNodeList nodes = xmlDoc.DocumentElement.SelectNodes("/MIMETYPE/MIME");
            string xMagicNumber = "";
            string xContentType = "";
            string ext = Path.GetExtension(fudocument.FileName);

            foreach (XmlNode node in nodes)
            {
                xMagicNumber = node.SelectSingleNode("MagicNumber").InnerText;

                string xsubstring = magicNumber.Substring(0, Convert.ToInt32(xMagicNumber.Length));

                if (xsubstring == xMagicNumber)
                {
                    MimeType = node.SelectSingleNode("ContentType").InnerText;

                    break; // TODO: might not be correct. Was : Exit For


                }

            }

            if (MimeType == fudocument.PostedFile.ContentType && Check_FileExtension_Permission(ext, fudocument.PostedFile.ContentType) == true)
            {

                result = true;

            }


            return result;
        }
    }

}