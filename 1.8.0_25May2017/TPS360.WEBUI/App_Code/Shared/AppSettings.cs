/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: AppSettings.cs
    Description: This page is used for application settings and to concatenate webroot
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sep-29-2008           Yogeesh Bhat        Defect ID: 8656,8657; Webroot concatenation is removed 
                                                             for ForgotPasswordTemplateUrl
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.IO;
using System.Configuration;

namespace TPS360.Web.UI
{
    public static class AppSettings
    {
        public static readonly string WebRoot = ConfigurationManager.AppSettings["webRoot"];

        public static readonly string ForgotPasswordTemplateUrl = ConfigurationManager.AppSettings["forgotPasswordTemplateUrl"];                    //0.1
        public static readonly string InterviewMailTemplateUrl = WebRoot + ConfigurationManager.AppSettings["InterviewMailTemplateUrl"];
        public static readonly string ChangePasswordTemplateUrl = WebRoot + ConfigurationManager.AppSettings["changePasswordTemplateUrl"];
        public static readonly string InvoiceTemplateUrl = WebRoot + ConfigurationManager.AppSettings["invoiceTemplateUrl"];
        public static readonly string PdfGenerateUrl = WebRoot + ConfigurationManager.AppSettings["pdfGenerateUrl"];
        public static readonly string ImmigrationReportUrl = WebRoot + ConfigurationManager.AppSettings["immigrationReportUrl"];
        public static readonly string CheckListReportUrl = WebRoot + ConfigurationManager.AppSettings["checkListReportUrl"];
        public static readonly string ReqMailToVendorURL = WebRoot + ConfigurationManager.AppSettings["ReqMailToVendorURL"];        
    }
}