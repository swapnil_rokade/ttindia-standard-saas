/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ContextConstants.cs
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
     0.1            20/May/2016         Sumit Sonawane      
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
*/

namespace TPS360.Web.UI
{
    public sealed class ContextConstants
    {
        public const string Facade = "facade";

        public const string ROLE_ADMIN = "Admin";
        public const string MEMBER = "Member";
        public const string ROLE_EMPLOYEE = "Employee";
        //public const string ROLE_EMPLOYEE = "Employee";
        public const string ROLE_CONSULTANT = "Consultant";
        public const string ROLE_CANDIDATE = "Candidate";
        public const string ROLE_COMPANY = "Company";
        public const string ROLE_COMPANY_CONTACT = "CompanyContact";
        public const string ROLE_CLIENT = "Client";
        public const string ROLE_VENDOR = "Vendor";
        public const string ROLE_PARTNER = "Partner";
        public const string ROLE_VOULME_CANDIDATE = "VolumeCandidate";
        public const string ROLE_DEPARTMENT_CONTACT = "Department Contact";
        public const string SITESETTING = "SiteSetting";

        public const string ASSESSMENT_CURRENT_TEST_ID = "CurrentTestId";

        public const string MEMBER_DOCUMENT_TYPE_PHOTO = "Photo";
        public const string MEMBER_DOCUMENT_TYPE_VIDEORESUME = "Video Resume";
        public const string MEMBER_DOCUMENT_TYPE_WORDRESUME = "Word Resume";
        public const int MAXIMUM_UPLOAD_SIZE = 100;

        public const string MEMBER_SIGNATURE = "Signature";

        public const string TRAINING_COURSE_MATERIAL = "Training Course Material";

        public const string EPHTMLTOPDFKEY = "QfZkVwV2hDbNj2yMMHythCGQoGqOIrOm149JxbnAs8fZwJYWU3k1CSXMglcti3GB";

        public const int MEMBER_DEFAULT_AVAILABILITY = 315;

        /////////////Code Added by Sumit Sonawane on 20/May/2016/////////////////////////
        public const string CANDIDATE_REPORT = "CandidateReport";

        public const string MY_CANDIDATE_REPORT = "MyCandidateReport";

        public const string TEAM_CANDIDATE_REPORT = "TeamCandidateReport";

        public const string PRODUCTIVITY_REPORT = "ProductivityReport";

        public const string MY_PRODUCTIVITY_REPORT = "MyProductivityReport";

        public const string TEAM_PRODUCTIVITY_REPORT = "TeamProductivityReport";

        public const string OFFERANDJOINEDREPORT = "OfferAndJoinedReport";

        public const string MYOFFERANDJOINEDREPORT = "MyOfferAndJoinedReport";

        public const string TEAMOFFERANDJOINEDREPORT = "TeamOfferAndJoinedReport";

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private ContextConstants()
        {
        }
    }
}