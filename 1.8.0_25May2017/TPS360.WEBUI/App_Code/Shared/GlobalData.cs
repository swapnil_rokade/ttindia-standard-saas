using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Web.Caching;
using System.Xml;

using TPS360.Common.Helper;

namespace TPS360.Web.UI
{
    internal static class GlobalData
    {
        public static StringCollection CountryList
        {
            [DebuggerStepThrough()]
            get
            {
                HttpContext context = HttpContext.Current;
                Cache cache = context.Cache;

                if (cache["countryList"] == null)
                {
                    StringCollection list = new StringCollection();

                    string filePath = context.Server.MapPath("~/Misc/countryList.data");

                    using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        XmlReader xtr = XmlReader.Create(stream);

                        while (xtr.Read())
                        {
                            if (xtr.NodeType == XmlNodeType.Element)
                            {
                                if (StringHelper.IsEqual(xtr.LocalName, "country"))
                                {
                                    list.Add(xtr.ReadString());
                                }
                            }
                        }
                    }

                    if (list.Count == 0)
                    {
                        return null;
                    }

                    cache.Insert("countryList", list, new CacheDependency(filePath));
                }

                return cache["countryList"] as StringCollection;
            }
        }
    }
}