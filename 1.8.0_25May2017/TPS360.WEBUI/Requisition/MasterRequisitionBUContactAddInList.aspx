﻿ <%--
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MasterRequisitionBUContactAddInList.aspx
    Description: This is the page which is used to display the Requisation list Add BU contact to multipal requisitions
    Created By: pravin khot
    Created On: 23/May/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.          Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>
<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="MasterRequisitionBUContactAddInList.aspx.cs" Inherits="TPS360.Web.UI.Requisition.MasterRequisitionBUContactAddInList" Title="Bulk Assignment of Requisition" EnableEventValidation ="false"  %>
<%@ Register Src ="~/Controls/RequisitionListBUContactAdd.ascx" TagName ="Requisition" TagPrefix ="ucl"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
Bulk Assignment of Requisition
</asp:Content>
<asp:Content ID="cntRequisitionList" ContentPlaceHolderID="cphHomeMaster" runat="Server">

<asp:UpdatePanel ID="pnlRequisition" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
<ucl:Requisition ID="uclRequisition" runat ="server" />
</ContentTemplate>                               
                               </asp:UpdatePanel>

</asp:Content>
