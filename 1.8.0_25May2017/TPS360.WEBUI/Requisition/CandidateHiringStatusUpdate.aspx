﻿
<%--Some code commented by Sumit Sonawane 09/June/2016--%>
<%--new field added by pravin khot on 23/June/2016 - Source --%>


<%@ Page Language="C#" MasterPageFile="~/Default-NoSection.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeFile="CandidateHiringStatusUpdate.aspx.cs" Inherits="TPS360.Web.UI.ATS.PreSelectedInterviewHiringMatrix" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RejectCandidateList.ascx" TagName="Rejection" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ModalTemplate.ascx" TagName="Template" TagPrefix="ucl" %>
<asp:Content ID="cntAssessmentTitle" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
</asp:Content>
<asp:Content ID="cntAssessmentEditor" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <style>
        .dropdown-menu:after
        {
            border-bottom: 7px solid #FFFFFF;
            border-left: 7px solid transparent;
            border-right: 7px solid transparent;
            content: "";
            display: inline-block;
            left: 80%;
            position: absolute;
            top: -6px;
        }
        .dropdown-menu::before
        {
            position: absolute;
            top: -7px;
            left: 80%;
            display: inline-block;
            border-right: 7px solid transparent;
            border-bottom: 7px solid #CCC;
            border-left: 7px solid transparent;
            border-bottom-color: rgba(0, 0, 0, 0.2);
            content: '';
        }
    </style>

    <script type="text/javascript" src="../js/AjaxModal/Main.js"></script>

    <script type="text/javascript" src="../Scripts/RequisitionHiringMatrix.js"></script>

    <script type="text/javascript" src="../Scripts/jsDatePick.jquery.min.1.3.js"></script>

    <script type="text/javascript" src="../js/AjaxScript.js"></script>

    <script type="text/javascript">
   
    function abc()
    {alert ('dfd');
    }
    
    
    function monitorClick(e) {
        try {
            var evt = (e) ? e : event;

            var theElem = (evt.srcElement) ? evt.srcElement : evt.target;

            while (theElem != null) {
                if (theElem.id.indexOf('refReq') < 0 && theElem.id.indexOf('ctl00_cphHomeMaster_ddlLevels')  && n != 0) {

                    n = 10;
                    var divReqs = document.getElementById('divReqs');
                    if (divReqs != null) {
                        $(divReqs).closest('.btn-group').removeClass('open');
                        divReqs.style.display = "none";
                    }
                  
                    return true;
                }
                else {
                    n = 10;
                    return true;
                }

                theElem = theElem.offsetParent;
            }
        }
        catch (e) {
        } n = 10;
        return true;
    }
    //document.onclick = monitorClick;
    
    
    
    var methodHandlers = {};
        methodHandlers.BeginRecieve = function(_result) {
        
            /// <summary>
            /// method that shows result from 
            /// page method "GetWizardPage"
            /// </summary>
            var res = false;
            if(_result.customStyle && _result.customStyle!="") {
                mainScreen.LoadStyleSheet(_result.customStyle);
            }
            if(_result.html && _result.html!="") {

                mainScreen.mainModalContentsDiv.innerHTML = _result.html; 
                var hdnDateTimeFormat=document .getElementById ('<%= hdnDateTimeFormat.ClientID %>');
                          
                try
              {
                var title=document .getElementById ('ctl00_cphHomeMaster_uclTemplateActionLog_lbModalTitle');
                title .innerHTML=mainScreen.ModalTitle;

               
              }
              catch (exp)
              {
 
              }
                try{
                BringContacts();
                }
                catch (e)
                {
                }
                setDate(hdnDateTimeFormat.value);
                setDate1(hdnDateTimeFormat.value);

            
            
                res = true;
            }
            if(_result.script && _result.script!="") {
                eval(_result.script);
            }
            if(!res) {
                mainScreen.CancelModal();
            } else {
                mainScreen.mainModalExtender._layout();
                setTimeout('mainScreen.mainModalExtender._layout()', 3000);
            }
            
        };
    </script>

    <script type="text/javascript">
       
   function validateSelectedCandidateList()
   {
         var canids=document.getElementById ('<%= txtSelectedIds.ClientID %>');
         if(canids.value.trim()=="")
         {
            ShowMessage('<%= lblMessage.ClientID %>','Please select at least one candidate',true  );
            
            var reqCandidateID =document .getElementById ('<%=reqCandidateID.ClientID %>');
            reqCandidateID .isValid=false ; 
             return false  ;
         }
    }
    function ValidateRemoveCandidate(type)
   {
         var canids=document.getElementById ('<%= txtSelectedIds.ClientID %>');
         
        if(type=='single')
        {
         var hdnrmvCandidate=document.getElementById ('<%= hdnrmvCandidate.ClientID %>');
     
            if (confirm("Are you sure that you want to remove these candidate(s)?")) {
                hdnrmvCandidate.value = "Yes";
            } else {
                hdnrmvCandidate.value = "No";
            }
        } 
        else
        {
        if(canids.value.trim()=="")
         {
            ShowMessage('<%= lblMessage.ClientID %>','Please select at least one candidate',true  );
            var reqCandidateID =document .getElementById ('<%=reqCandidateID.ClientID %>');
            reqCandidateID .isValid=false ; 
            return false  ;
         }
         else
         {
             var hdnrmvCandidate=document.getElementById ('<%= hdnrmvCandidate.ClientID %>');
     
            if (confirm("Are you sure that you want to remove these candidate(s)?")) {
                hdnrmvCandidate.value = "Yes";
            } else {
                hdnrmvCandidate.value = "No";
            }
            
         }
        }
         
    }
    
   function ShowMiscUtilMessage(control,message,isError)
   {
        ShowMessage(control,message ,isError   );
   }
       
   function CloseModal()
   {
   
     $('select[rel=HiringStatus]').each(function (){
           
           var s=document .getElementById ($(this).attr('id'));
           s.selectedIndex=$(this).next().val();
     });
   
    var iframe=document .getElementById ('iframe');

    if(iframe .src.indexOf('Interview')>=0)
    { 

    var s =document .getElementById ('<%= hdnNeedListPostBack.ClientID %>');
        s.value='1';
        __doPostBack('ctl00_cphHomeMaster_upPanelLevels','');
         s.value='';
////////////////////////////////////  ShowMessage('ctl00_lblMessage','Candidate moved successfully',false  );////Sumit Sonawane 09/June/2016
    }
    iframe .src ="about:blank";
         $find('mpeModal').hide();
                     $('body').css({'overflow':'auto'});
         return false ;
   }
    function ModalOperationCompleted(message)
    {
        var iframe=document .getElementById ('iframe');
        iframe .src ="about:blank";

        $find("mpeModal").hide();
                 $('body').css({'overflow':'auto'});
        var s =document .getElementById ('<%= hdnNeedListPostBack.ClientID %>');
        s.value='1';
        __doPostBack('ctl00_cphHomeMaster_upPanelLevels','');
        var canids=document.getElementById ('<%= txtSelectedIds.ClientID %>');
           ShowMessage('ctl00_lblMessage',message,false  );
        canids .value="";

         s.value='';
    }
   
Sys.Application.add_load(function() { 
try{

         var background = $find("mpeModal")._backgroundElement;
           var hdn1=document .getElementById ('<%= hdntemp.ClientID  %>');
          // alert(hdn1.value);
         background.onclick = function()
          {
           $('select[rel=HiringStatus]').each(function (){
           
           var s=document .getElementById ($(this).attr('id'));
           s.selectedIndex=$(this).next().val();
        //alert ($(this).selectedIndex);
     });
     
            var iframe=document .getElementById ('iframe');
             if(iframe .src.indexOf('Interview')>=0)
            {   
            if(hdn1.value=="WIP")
            {
                CloseModal();
                hdn1.value="";
                }
            }
            iframe .src ="about:blank";
            $find("mpeModal").hide(); 
                        $('body').css({'overflow':'auto'});
           }

    }
    catch (e){}   
    }
    );
    function SelectAllCheckbox(HeaderCheckbox) {

        // debugger;
        var isChecked = HeaderCheckbox.checked;
        var elements = HeaderCheckbox.form.elements;

        for (var i = 0; i < elements.length; i++) {
            if ((elements[i].type == 'checkbox') && (elements[i].id != HeaderCheckbox.id)) {
                if (!elements[i].disabled) {
                    if (elements[i].checked != isChecked) {
                        elements[i].checked = isChecked;
                        if (isChecked == true) {
                            var hndId = elements[i + 1];
                            if (IsNumeric(hndId.value) && hndId.value != "0")
                                ADDID(hndId.value, 'ctl00_cphHomeMaster_txtSelectedIds');

                        }
                        else {
                            var hndId = elements[i + 1];
                            if (IsNumeric(hndId.value) && hndId.value != "0")
                                RemoveID(hndId.value, 'ctl00_cphHomeMaster_txtSelectedIds');

                        }


                    }
                }
            }
        }
    }
    function EditModal(src,width,height)
    {   var iframe=document .getElementById ('iframe');
        iframe .src ="";
        iframe .width =width ;
        iframe .height = height ;
          iframe .src =src;
          $find('mpeModal').show();
                      $('body').css({'overflow':'hidden'});
           return false ;
    }
    function ShowOfferOrJoinedModal(src)
    {

    try{
       var iframe=document .getElementById ('iframe');
       
         iframe .width ="778px" ;
        iframe .height = "570px" ;
        
          iframe .src =src;
          if(iframe .src .indexOf ('OfferRejected')>=0 ||  iframe .src .indexOf ('JoiningDetails')>=0)
       {
         iframe .width ="470px" ;
        iframe .height = "370px" ;
        
       }
        }
        catch (e)
        {
        }
    }
     
     window.onload=function(){
     var hdn=document .getElementById ('<%= hdnStatus.ClientID  %>');
     if(hdn.value==1)
     {
       hdn.value=0;
       location.reload(true);
       }
       }
    
    function ModalChanged(ddl,canid)
    {   
        var hdn=document .getElementById ('<%= hdnStatus.ClientID  %>');
            hdn.value=1;
            var hdn1=document .getElementById ('<%= hdntemp.ClientID  %>');
            hdn1.value=ddl.options[ddl.selectedIndex].text;
        var iframe=document .getElementById ('iframe');
        iframe .src ="";
        iframe .width ="778px" ;
        iframe .height = "570px" ;
        var canids=document.getElementById ('<%= txtSelectedIds.ClientID %>');
        
        if(canids.value == "" && canid=="0")
        { 
            ShowMessage('<%= lblMessage.ClientID %>','Please select at least one candidate',true  );
            ddl.selectedIndex=0;
            return false ;
         }  
          if(ddl.options[ddl.selectedIndex].text=='Offer Decline')
          {
           iframe .width ="470px";
            iframe .height ="370px";
          }
            if(ddl.options[ddl.selectedIndex].text=='Joined')
          {
           iframe .width ="470px";
            iframe .height ="370px";
          }
        if(ddl.options[ddl.selectedIndex].text=='Rejected')
        {
            if(!ConfirmAction('Are you sure that you want to reject this candidate from the Hiring Matrix?')) 
            {
                location.reload();
                return false ;
            }
            iframe .width ="500px";
            iframe .height ="275px";
        }
        else if(IsNumeric(ddl.options[ddl.selectedIndex].value))
        { 
            var updatorid=document .getElementById ('<%= hdnCurrentUserId.ClientID  %>');
            var jobid=document .getElementById ('<%= hdnCurrentJobPostingId.ClientID  %>');       
            if(canid ==0) canid =canids .value;
            PageMethods.MoveCandidatesToLevel({'Member_Id':canid  ,'JobPosting_Id':jobid.value ,'StatusId':ddl.options[ddl.selectedIndex].value ,'UpdatorId':updatorid .value   },CandidateMovedCallBack);
            
            //ddl.selectedIndex=0;
            return false ;
        }
       // DropdownChange('divReqs');
       
       
        if(canid ==0) canid =canids .value;
        iframe .src =ddl.options[ddl.selectedIndex].value+ '&Canid=' + canid;
        if(iframe .src.indexOf('Interview')>=0)
        {
                iframe .width ="700px";
             iframe .height ="570";
        
        var statussid= getParameterByName("StatusId", iframe .src );
            var updatorid=document .getElementById ('<%= hdnCurrentUserId.ClientID  %>');
            var jobid=document .getElementById ('<%= hdnCurrentJobPostingId.ClientID  %>');       
            if(canid ==0) canid =canids .value;
            PageMethods.MoveCandidatesToLevel({'Member_Id':canid  ,'JobPosting_Id':jobid.value ,'StatusId':statussid ,'UpdatorId':updatorid .value   });
            ddl.selectedIndex=0;
          }  
        $find('mpeModal').show();
                    $('body').css({'overflow':'hidden'});
        ddl.selectedIndex=0;
        return false ;
    }
  function getParameterByName(name,url)
{

  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(url);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}
   function CandidateMovedCallBack()
   {
     var canids=document.getElementById ('<%= txtSelectedIds.ClientID %>');
        canids .value="";
        ModalOperationCompleted ('Candidate Moved Successfully');
   }

    function DropdownChange(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function HideContextMenu(divId) {
        if (n == 0) {
            var reqDiv = document.getElementById(divId);
        }
        if (reqDiv != null) reqDiv.style.display = "none";
    }
    function DropdownClicked(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }

    function ShowDiv(divId) {
        n = 10;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function ShowReqDiv(divId) {

        var a = document.getElementById("divReqs");
        var b = document.getElementById("divHotListdiv");
        if (a != null) a.style.display = "none";
        if (b != null) b.style.display = "none";

        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) {
            $(reqDiv).closest('.btn-group').addClass('open');
            reqDiv.style.display = "";
            reqDiv.parentElement.className = "btn-group open";
        }
        return false;
    }
    function ShowOrHide(v, img) {
        var tdDetail = document.getElementById(v);
        var image = document.getElementById(img);
        if (tdDetail.style.display == "none") {
            tdDetail.style.display = "";
            image.src = "../Images/collapse-minus.png";
        }
        else {
            tdDetail.style.display = "none"; image.src = "../Images/expand-plus.png";
        }

    }
      function IsNumeric(input) {
        return (input - 0) == input && input.length > 0;
    }


 function SelectRowCheckbox(param1, param2) {
 
 
  
        // debugger;
        var ischeckall = true;
        var HeaderCheckbox = document.getElementById('chkAllItem');
        var elements = HeaderCheckbox.form.elements;

        for (var i = 0; i < elements.length; i++) {
            if ((elements[i].type == 'checkbox') && (elements[i].id != HeaderCheckbox.id)) {
                if (elements[i].checked == true) {
                    var hndId = elements[i + 1];
                   
                    if (IsNumeric(hndId.value)) {
                        ADDID(hndId.value, 'ctl00_cphHomeMaster_txtSelectedIds');
                    }

                }
                else {
                    var hndId = elements[i + 1];
                    if (IsNumeric(hndId.value)) {
                        RemoveID(hndId.value, 'ctl00_cphHomeMaster_txtSelectedIds');
                    }
                }
            }
        }
        HeaderCheckbox.checked = ischeckall;
    }

  
    function CloseExtenderPopup(id) {
        var s = $find(id);
        s.hide();
                    $('body').css({'overflow':'auto'});
    }
    </script>
    
    <div style="width: 100%; overflow: inherit;" align="center">
    <asp:HiddenField ID="hdnStatus" runat="server" Value="0" />
        <asp:HiddenField ID="hdnDateTimeFormat" runat="server" Value="" />
        <ajaxtoolkit:modalpopupextender id="mpeModal" runat="server" backgroundcssclass="DarkModalBackground"
            dropshadow="false" popupcontrolid="Panel1" targetcontrolid="Label1" oncancelscript="ClosePopUP()"
            behaviorid="mbMain">
        </ajaxtoolkit:modalpopupextender>
        <asp:Panel ID="Panel1" runat="server" Style="display: none;">
            <asp:Label ID="Label1" runat="server"></asp:Label>
            <ucl:template id="uclTemplateActionLog" runat="server" contentdisplay="table-cell"
                modalbehaviourid="mbMain" contentheight="250px" contentwidth="600px">
                <Contents>
                    <div style="text-align: center; padding: 0px 10px 10px 10px;" visible="false" id="mainModalContents">
                    </div>
                </Contents>
            </ucl:template>
        </asp:Panel>
        <ajaxtoolkit:modalpopupextender id="mpeHiringDetails" behaviorid="mpeModal" runat="server"
            backgroundcssclass="DarkModalBackground" dropshadow="false" popupcontrolid="pnlHiringDetails"
            targetcontrolid="lblTarget">
        </ajaxtoolkit:modalpopupextender>
        <asp:Panel ID="pnlHiringDetails" runat="server" Style="display: none;">
            <asp:Label ID="lblTarget" runat="server"></asp:Label>
            <iframe id="iframe" width="778px" height="570px" frameborder="0" style="border-radius: 12px"
                scrolling="no"></iframe>
        </asp:Panel>
        <div id="divOuter">
        <asp:HiddenField ID="hdntemp" runat="server" />
            <asp:HiddenField ID="hdnCurrentJobPostingId" runat="server" />
            <asp:HiddenField ID="hdnCurrentUserId" runat="server" />
            <asp:HiddenField ID="hdncurretn" runat="server" />
            <asp:HiddenField ID="hdnCurname" runat="server" />
            <asp:HiddenField ID="hdnDivContextMenuids" runat="server" />
            <asp:HiddenField ID="hdnmodal" runat="server" Value="" />
            <asp:HiddenField ID="hdnmodalreject" runat="server" Value="" />
            <asp:HiddenField ID="hdnMoving" runat="server" />
            <ucl:confirm id="uclConfirm" runat="server"></ucl:confirm>
            <%--     Hidden Fields--%>
            <asp:HiddenField ID="hdnChkBox" runat="server" Value="" />
            <asp:HiddenField ID="hdnIsHide" runat="server" />
            <asp:HiddenField ID="hdnDetailRowIds" runat="server" />
            <asp:HiddenField ID="hdnRemove" runat="server" />
            <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
            <asp:HiddenField ID="hdnNextLevelName" runat="server" />
            <asp:HiddenField ID="hdnPreviousLevelName" runat="server" />
            <asp:HiddenField ID="hdnrmvCandidate" runat="server" />
            <div style="text-align: left;">
                <asp:HiddenField ID="hdnPageTitle" runat="server" />
                <asp:HiddenField ID="hdnCount" runat="server" Value="0" />
                <asp:HiddenField ID="hdnRejectCount" runat="server" />
                <asp:UpdatePanel ID="upPanelLevels" runat="server">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:TextBox ID="lblCurrentJobPostingID" runat="server"></asp:TextBox>
                            <asp:TextBox ID="txtSelectedIds" runat="server" EnableViewState="true"></asp:TextBox></div>
                        <asp:HiddenField ID="hdnNeedListPostBack" runat="server" />
                        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                        <div id="Body">
                            <div id="BodyMiddle" style="width: 99%;">
                                <div id="HMatrixContainer" style="height: 100%;">
                                    <div id="LeftSidebar" style="float: left; width: 13%;">
                                        <div class="HMatrixLeft" style="width: 100%;">
                                            <div class="header">
                                                <span class="title">Status Levels</span>
                                            </div>
                                            <asp:ListView ID="lvNavigationItems" runat="server" OnItemDataBound="lvNavigationItems_ItemDataBound">
                                                <LayoutTemplate>
                                                    <div class="content" style="overflow: hidden;">
                                                        <div class="levels">
                                                            <li id="itemPlaceholder" runat="server" />
                                                        </div>
                                                    </div>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                 <asp:LinkButton ID="lnkLeftMenuItem" runat="server" CssClass="level" OnClick="lnkLeftMenuItem_Click"
                                                        Height="100%" Width="100%">
                                                        <div id="dvspanName" runat="server" style="padding: 0px  0px 0px 10px">
                                                            <span class="name" id="spName" runat="server" style="width: 60%; float: left;"></span>
                                                        </div>
                                                        <div style="vertical-align: middle; float: right; width: 30%; padding-right: 5px">
                                                            <span class="count" id="spCount" runat="server"></span>
                                                        </div>
                                                    </asp:LinkButton>
                                                    <asp:HiddenField ID="hnSelectedLevel" runat="server" />
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                    </div>
                                    <div id="HMatrixContent" style="vertical-align: top; float: left; width: 86.5%; position: relative;">
                                        <div class="header">
                                            <asp:HyperLink ID="lnkReqTitle" runat="server" CssClass="title" Target="_blank"></asp:HyperLink>
                                        </div>
                                        <div class="content">
                                            <div id="dvbulkAction" class="btn-toolbar"  runat="server" style="padding-left: 6px;">
                                                <div class="pull-left"  style="display: none">
                                                    <div class="btn-group" >
                                                        <a href="Javascript:void(0)" onclick="ShowReqDiv('divReqs')" id="refReq" class="btn btn-medium dropdown-toggle">
                                                            Change Status <span class="caret"></span></a>
                                                        <ul class="dropdown-menu-custom" id="divReqs" style="display: none">
                                                            <li style="width: 150px; text-align: left; padding-top: 10;" id="list">
                                                                <asp:DropDownList  ID="ddlLevels" runat="server" CssClass="CommonDropDownList" Width="150px"
                                                                    onclick="DropdownClicked('divReqs')" onchange="ModalChanged(this,0);">
                                                                </asp:DropDownList>
                                                            </li>
                                                        </ul>
                                                    </div>                                              
                                                     <div class="btn-group" runat="server" id="liSubmit">                                                      
                                                        <asp:RequiredFieldValidator ID="reqCandidateID" runat="server" Display="Dynamic"
                                                            ValidationGroup="CandidateGroup" ControlToValidate="txtSelectedIds"></asp:RequiredFieldValidator>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                            <div class="CandidateGrid">
                                                <div id="dvhirng" runat="server">
                                                    <div style="clear: both; padding: 0px 5px 5px 5px" id="divList">
                                                        <asp:ObjectDataSource ID="odsHiringMatrix" runat="server" SelectMethod="GetPagedAll"
                                                            TypeName="TPS360.Web.UI.HiringMatrixDataSource" SelectCountMethod="GetListCountAll"
                                                            EnablePaging="True" SortParameterName="sortExpression">
                                                            <SelectParameters>
                                                                <asp:Parameter Name="JobPostingId" DefaultValue="0" Type="String" Direction="Input" />
                                                                <asp:Parameter Name="SelectionStepLookupId" DefaultValue="0" Type="String" Direction="Input" />
                                                            </SelectParameters>
                                                        </asp:ObjectDataSource>
                                                        <asp:ListView ID="lsvCandidateList" runat="server" DataKeyNames="Id" DataSourceID="odsHiringMatrix"
                                                            OnItemDataBound="lsvCandidateList_ItemDataBound" OnItemCommand="lsvCandidateList_ItemCommand"
                                                            OnPreRender="lsvCandidateList_PreRender" EnableViewState="true">
                                                            <LayoutTemplate>
                                                                <table id="tlbTemplate" class="Grid" cellspacing="0" border="0">
                                                                    <tr id="Tr1" runat="server">
                                                                    
                                                                          <th id="Th1" runat="server" style="vertical-align: middle; Width:165px">
                                                                             <asp:LinkButton ID="lnkRequisition" runat="server" Text="Requisition" CommandName="Sort"
                                                                                    CommandArgument="[J].[JOBTITLE]"></asp:LinkButton></div> 
                                                                            </th>  
                                                                        <th style="vertical-align: middle;">
                                                                            <div style="width: 24px;" Visible="false" runat="server" >
                                                                                <!--                                               <div style="float: left; width :12px;" id="divImg" ><asp:Image ID="imgShowHide" ImageUrl="~/Images/expand-plus.gif" runat="server" onClick="javascript:ShowHideAllDetail(this)"  /></div> -->
                                                                                <div style="float: left; width: 12px;" Visible="false">
                                                                                    <input id="chkAllItem" name="chkAllItem" type="checkbox" Visible="false" onclick="SelectAllCheckbox(this)"
                                                                                        disabled="disabled" /></div>
                                                                            </div>
                                                                           
                                                                            <div style="margin-left: 35px; margin-top: 3px;">
                                                                                <asp:LinkButton ID="lnkHeaderName" runat="server" Text="Candidate" CommandName="Sort"
                                                                                    CommandArgument="[C].[FirstName]"></asp:LinkButton></div>
                                                                            </th>
                                                                         <th style="white-space: nowrap;text-align:center; min-width: 40px;max-width: 40px;">
                                                                            <asp:LinkButton ID="lnkCandidateId" runat="server" Text="Candidate Id" CommandName="Sort"
                                                                                CommandArgument="[C].[id]" ></asp:LinkButton>
                                                                        </th>
                                                                        <th style="vertical-align: middle;">
                                                                            <asp:LinkButton ID="lnkHeaderPosition" runat="server" Text="Current Position" CommandName="Sort"
                                                                                CommandArgument="[C].[CurrentPosition]" Width="125px"></asp:LinkButton>
                                                                        </th>
                                                                                                                                                                                                            
                                                                       <%-- ****************Added by pravin khot on 23/June/2016******--%>
                                                                         <th style="vertical-align: middle; width: 140px !important">
                                                                            <asp:LinkButton ID="lnkSource" runat="server" Text="Source" CommandName="Sort"
                                                                                CommandArgument="[GS].[Name]"></asp:LinkButton>
                                                                        </th>
                                                                       <%-- *****************************END**************************--%>                                                                    
                                                                        <th style="vertical-align: middle;">
                                                                            Current Status
                                                                        </th>
                                                                        <th style="width: 45px !important">
                                                                            Action
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="6" style="padding: 0px 0px 0px 0px; border-style: none none none none">
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="itemPlaceholder" runat="server">
                                                                    </tr>
                                                                    <tr class="Pager">
                                                                        <td colspan="7" id="tdPager" runat="server">
                                                                            <ucl:pager id="pagerControl" runat="server" enableviewstate="true" noofrowscookie="HiringMatrixRowPerPage" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </LayoutTemplate>
                                                            <EmptyDataTemplate>
                                                                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                                                                    enableviewstate="true">
                                                                    <tr>
                                                                        <td>
                                                                            No candidates in status level.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </EmptyDataTemplate>
                                                            <ItemTemplate>
                                                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'
                                                                    align="left">
                                                                       <td style="border-bottom-style: none; min-width: 60px; overflow: hidden;" id="td4">
                                                                           <asp:Label ID="lblReqTitle" runat="server" Width="45px" Text=""></asp:Label>
                                                                        </td>
                                                                    <td style="border-bottom-style: none; white-space: normal; min-width: 90px;" id="td2">
                                                                    
                                                                      
                                                                        <div style="width: 25px;">
                                                                            <div style="float: left; width: 12px; display: none">
                                                                                <img class="ExpandImg" />
                                                                            </div>
                                                                            <div style="float: left; width: 12px;">
                                                                                <asp:CheckBox ID="chkCandidate" runat="server"  Visible="false" onClick="javascript:SelectRowCheckbox()" />
                                                                                <asp:HiddenField ID="hfCandidateId" runat="server" />
                                                                                <asp:HiddenField ID="hfstatuschangeId" runat="server" />
                                                                            </div>
                                                                        </div>
                                                                       
                                                                        <div>
                                                                            <div style="white-space: normal; margin-left: 35px; width: 60%; margin-top: 2px">
                                                                                <asp:HyperLink ID="hlnCandidateName" runat="server" Text="" Target="_blank" EnableViewState="true"></asp:HyperLink>
                                                                            </div>
                                                                            <div style="float: right; margin-top: -15px;">
                                                                                <span id="intervewImg" runat="server" visible="false" title="Upcoming Interview">
                                                                                    <img src="../Images/CalendarIcon.png" runat="server" id="imgInterv" /></span>
                                                                            </div>                                                                          
                                                                        </div>                                                                        
                                                                    </td>
                                                                    <td id="td3" runat="server" style="text-align:center;min-width: 40px;max-width: 40px;">
                                                                        <asp:Label ID="lblCandidateID" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                   
                                                                    <td style="border-bottom-style: none; width: 60px;"  id="td9">
                                                                        <asp:Label ID="lblCurrentPosition" runat="server" Text="" Style="font-size:10px"></asp:Label>
                                                                    </td>
                                                                   <%-- *******************Code added by pravin khot on 23/June/2016*******--%>
                                                                      <td style="border-bottom-style: none; width: 100px; overflow: hidden;" id="td1">
                                                                        <asp:Label ID="lblsource" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                   <%-- *************************END*****************************--%>
                                                                 
                                                                    <td style="border-bottom-style: none; min-width: 70px; max-width: 90px" id="tdStatus"
                                                                        runat="server">
                                                                        
                                                                          <asp:Label ID="lblStatus" runat="server"  Text="" Style="font-size:14px"></asp:Label>
                                                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="CommonDropDownList" AutoPostBack="false"
                                                                         Enabled="true"   Width="90%"  Visible="false"                                                              
                                                                          EnableViewState="true" rel="HiringStatus">
                                                                        </asp:DropDownList>
                                                                        <asp:HiddenField ID="hdnSelectedIndex" runat="server" />
                                                                        <asp:HiddenField ID="hdnCurrentlevelId" runat="server" />
                                                                        
                                                                    </td>
                                                                    <td style="overflow: inherit;">
                                                                        <ul class="nav" style="margin-bottom: 0px; padding-left: 0px;">
                                                                            <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle" style="width: 30px;
                                                                                margin-top: -15px;" href="#">
                                                                                <div class="container">
                                                                                    <button class="btn btn-mini" type="button">
                                                                                        <i class="icon icon-cog"></i><span class="caret"></span>
                                                                                    </button>
                                                                                </div>
                                                                            </a>
                                                                                <ul class="dropdown-menu" style="margin-left: -130px; margin-top: -25px;">
                                                                                   
                                                                                     <li>
                                                                                        <asp:HyperLink ID="lnkChangeHiringStatus" runat="server" Text="Change Hiring Status" Target="_blank" /></li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCandidateDetails" class='<%# Container.DataItemIndex % 2 == 0 ? "Expandrow" : "Expandaltrow" %>'
                                                                    style="display: none" runat="server">
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </div>
                                                </div>
                                                <div id="dvReje" runat="server">
                                                    <ucl:rejection id="uclRejection" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <asp:Panel ID="pnlmodal" runat="server" Style="display: none;">
                <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                    <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <ajaxtoolkit:modalpopupextender id="Modal" runat="server" targetcontrolid="pnlmodal"
                popupcontrolid="pnlmodal" backgroundcssclass="divModalBackground">
            </ajaxtoolkit:modalpopupextender>
        </div>
    </div>
</asp:Content>
