﻿<%--
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: BasicInfoEditor.ascx
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
     0.1           23/Feb/2016         pravin khot          Code added/replace by pravin khot on 23/Feb/2016 new code ,
                                                            DIV ID ADDED DIVID= [divworkflowarea, divpreviewarea]
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
--%>

<%@ Page Language="C#" MasterPageFile="~/Default-NoSection.master" EnableEventValidation="false" ValidateRequest="false"
    EnableViewState="true" AutoEventWireup="true" CodeFile="BasicInfoEditor.aspx.cs"
    Inherits="TPS360.Web.UI.Requisition.BasicInfoEditor" Title="Requisition Basic Info"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/RequisitionEditor.ascx" TagName="RequisitionEditor"
    TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RequisitionDescription.ascx" TagName="RequisitionDescription"
    TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RequisitionWorkflow.ascx" TagName="RequisitionWorkflow"
    TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RequisitionPreviewPublish.ascx" TagName="RequisitionPublish"
    TagPrefix="ucl" %>
<%@ Register Src="~/Controls/JobDetail.ascx" TagName="JobDetail" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ModalTemplate.ascx" TagName="Template" TagPrefix="ucl" %>
<asp:Content ID="cntBasicInfoTitle" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">

    <script type="text/javascript">         
  window.onerror = function (e) { return true; }        
    </script>

    <script type="text/javascript">

function load(){
 $('#basicinfonavtab').click(function(){
 

					$('html, body').animate({
						scrollTop: 0
					}, 400);
					
			       // $('#ctl00_ctlApplicationMenu_rptTopMenu_ctl03_liId').addClass('dropdown active');
			        
					return false;				   			   
				 });
 $('#descriptionnavtab').click(function(){

					$('html, body').animate({
						scrollTop: $("#descriptionarea").offset().top-45
					}, 400);

					// $('#ctl00_ctlApplicationMenu_rptTopMenu_ctl03_liId').addClass('dropdown active');
					 
					return false;				   			   
				 });
 $('#workflownavtab').click(function(){

					$('html, body').animate({
						scrollTop: $("#workflowarea").offset().top-45
					}, 400);

                   // $('#ctl00_ctlApplicationMenu_rptTopMenu_ctl03_liId').addClass('dropdown active');
                    
					return false;				   			   
				  });
 $('#previewnavtab').click(function(){
					$('html, body').animate({
						scrollTop: $("#previewarea").offset().top
					}, 400);

					//$('#ctl00_ctlApplicationMenu_rptTopMenu_ctl03_liId').addClass('dropdown active');
					return false;				   			   
				 });
				 
}

Sys.Application.add_load(function() {
    load();

    var background = $find("MPE")._backgroundElement;
    background.onclick = function() { $find("MPE").hide(); }


    // grab the initial top offset of the navigation 
    var sticky_navigation_offset_top = $('#reqNavScroll').offset().top;

    // our function that decides weather the navigation bar should have "fixed" css position or not.
    var reqNavScroll = function() {
        var scroll_top = $(window).scrollTop(); // our current vertical position from the top

        // if we've scrolled more than the navigation, change its position to fixed to stick to top,
        // otherwise change it back to relative

        if (scroll_top > sticky_navigation_offset_top) {
            $('#reqNavScroll').css({ 'position': 'fixed', 'top': '0', 'width': $('.section-body').width(), 'z-index': '100' });
        } else {
            $('#reqNavScroll').css({ 'position': 'relative', 'width': $('.section-body').width() });
        }

        //determine which section we are scrolled to and set the corresponding nav tab
        var newActiveTab = '';
        if (scroll_top >= $("#previewarea").offset().top)
            newActiveTab = "#previewnavtab";
        else if (scroll_top >= $("#workflowarea").offset().top-55)
            newActiveTab = "#workflownavtab";
        else if (scroll_top >= $("#descriptionarea").offset().top-55)
        
            newActiveTab = "#descriptionnavtab";
        else
            newActiveTab = "#basicinfonavtab";

        //if we are scrolled to the bottom of the window, activate the preview/publish nav tab
        if ($(window).scrollTop() + $(window).height() == $(document).height())
            newActiveTab = "#previewnavtab";

        //get the current active nav tab
        var oldActiveTab = "#" + $('#reqNavScroll .active').attr('id');

        //only replace the nav tab active class if we have scrolled to a different section
        if (oldActiveTab != newActiveTab) {
            $(oldActiveTab).removeClass("active");
            $(newActiveTab).addClass("active");
        }
    };


    // run our function on load
    reqNavScroll();
    // and run it again every time you scroll
    $(window).scroll(function() {
        reqNavScroll();
    });

    $(window).resize(function() {
        $('#reqNavScroll').css({ 'width': $('.section-body').width() });
    });

});
    </script>

    <style type="text/css">
        .basicinfo, .description, .workflow, .preview
        {
            cursor: pointer;
        }
        .basicinfotab, .descriptiontab, .workflowtab, .previewtab
        {
        }
        .navbar
        { *position:relative;overflow:visible;}
        .navbar .container
        {
            width: auto;
        }
        .navbar-static-top
        {
            position: static;
            width: 100%;
            margin-bottom: 0;
        }
        #reqNavScroll > #list .nav > li > a
        {
            display: inline;
            padding: 0 0px;
            color: #777777;
            float: right;
            margin: 0 0 0 3px;
            padding: 0 5px;
            height: 47px;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
        }
        #reqNavScroll > #list.nav > li > a:hover
        {
            color: #333333;
        }
        #reqNavScroll > #list .nav > li.active > a
        {
            background-color: #E5E5E5;
            box-shadow: 0 3px 8px rgba(0, 0, 0, 0.125) inset;
        }
        .navbar .nav
        {
            position: relative;
            left: 0;
            display: block;
            float: right;
            margin: 0 10px 0 0;
        }
    </style>
    <asp:UpdatePanel ID="upPanel" runat="server">
        <ContentTemplate>
            <ajaxToolkit:ModalPopupExtender Enabled="true" ID="ModalPopupExtender" runat="server"
                BackgroundCssClass="DarkModalBackground" DropShadow="false" PopupControlID="pnlJobDetail"
                TargetControlID="lblRough" BehaviorID="MPE">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnlJobDetail" runat="server" Style="display: none;">
                <asp:Label ID="lblRough" runat="server"></asp:Label>
                <ucl:Template ID="uclTemplate" runat="server" ContentDisplay="table-cell" ContentWidth="785px"
                    ContentHeight="470px" ModalBehaviourId="MPE">
                    <Contents>
                        <div style="overflow: auto; max-height: 500px; width: 785px;">
                            <ucl:JobDetail ID="uclJobDetail" runat="server"></ucl:JobDetail>
                        </div>
                    </Contents>
                </ucl:Template>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upReq" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false" Style="z-index: 9999"></asp:Label>
            <div id="BodyMiddle" style="width: 96.9%; overflow: hidden;">
                <div class="section">
                    <div id="reqNavScroll" class="section-header">
                        <h3>
                            <div id="divReqTitle" runat="server">
                                New Requisition Editor</div>
                        </h3>
                        <div style="float: right; width: 50%; margin-right: 0px;" id="list">
                            <ul class="nav" style="margin-top: 0px; top: 0px;">
                             <%--  *****Code added/replace by pravin khot on 23/Feb/2016 new code ****--%>
                                <li id="previewnavtab" runat="server"><a href="#previewarea">Preview/Publish</a> </li>
                                <li id="workflownavtab" runat="server"><a href="#workflowarea">Workflow</a> </li>
                                <li id="descriptionnavtab"><a href="#descriptionarea">Description</a> </li>
                                <li style="height: 20px; display: inline" id="basicinfonavtab" class="active"><a
                                    href="#basicinfoarea">Basic Info</a> </li>
                             <%--  **********************END**************************--%>
                             
                             <%--  *****Code commented by pravin khot on 23/Feb/2016 old code ****--%>
                              <%--  <li id="previewnavtab"><a href="#previewarea">Preview/Publish</a> </li>
                                <li id="workflownavtab"><a href="#workflowarea">Workflow</a> </li>
                                <li id="descriptionnavtab"><a href="#descriptionarea">Description</a> </li>
                                <li style="height: 20px; display: inline" id="basicinfonavtab" class="active"><a
                                    href="#basicinfoarea">Basic Info</a> </li>--%>
                            <%--  **********************END**************************--%>
                            </ul>
                        </div>
                    </div>
                    <div class="section-body">
                        <div class="section-body" data-target="#reqNavScroll" data-spy="scroll">
                            <div id="basicinfoarea">
                                <div class="basicinfotab TabPanelHeader" style="float: left; font-size: 19.5px;">
                                    Basic Info</div>
                                <ucl:RequisitionEditor ID="reqEditor" runat="server" />
                            </div>
                            <div id="descriptionarea">
                                <br />
                                <br />
                                <br />
                                <div class="basicinfotab TabPanelHeader" style="float: left; font-size: 19.5px">
                                    Description</div>
                                <ucl:RequisitionDescription ID="reqDescription" runat="server" />
                            </div>
                            <div id="workflowarea">
                               <br />
                                <br />
                                <br />
                               
                                 <%-- ******Code added/replace by pravin khot on 23/Feb/2016 ***********--%>
                                <div id="divworkflowarea" class="basicinfotab TabPanelHeader" style="float: left; font-size: 19.5px" runat="server">
                                    Workflow</div>
                               <%-- <div class="basicinfotab TabPanelHeader" style="float: left; font-size: 19.5px">
                                    Workflow</div>--%>
                                  <%-- **********************END**************************--%>   
                                <ucl:RequisitionWorkflow ID="reqWorkflow" runat="server" />
                            </div>                           
                                
                            <div id="previewarea">
                                <br />
                                <br />
                                <br />
                               <%-- ******Code added/replace by pravin khot on 23/Feb/2016 ***********--%>
                                 <div id="divpreviewarea" class="basicinfotab TabPanelHeader" style="float: left; font-size: 19.5px"  runat="server">
                                        Preview/Publish</div>
                              <%--  <div class="basicinfotab TabPanelHeader" style="float: left; font-size: 19.5px">
                                    Preview/Publish</div>--%>
                                  <%--  **********************END**************************--%>
                                <ucl:RequisitionPublish ID="reqPublish" runat="server" />
                            </div>
                          
                            <div class="form-actions centered" style="margin-bottom: 0px;">
                                <asp:Button ID="btnPreview" runat="server" CssClass="btn" Text="Preview Requisition"
                                    EnableViewState="true" OnClick="btnPreview_Click" ValidationGroup="ValGrpJobTitle"
                                    TabIndex="45" />
                                <asp:Button ID="btnSaveAsDraft" runat="server" CssClass="btn" Text="Save as Draft"
                                    OnClick="btnSaveasdraft_Click" Enabled="true" ValidationGroup="ValGrpJobTitle"
                                    TabIndex="46" />
                                <asp:Button ID="btnPublish" CssClass="btn btn-primary" runat="server" Text="Publish Now"
                                    OnClick="btnPublish_Click" ValidationGroup="ValGrpJobTitle" TabIndex="47" />
                            </div>
                            <br />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
