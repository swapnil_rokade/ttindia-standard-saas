﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MasterRequisitionBUContactAddInList.aspx.cs
    Description: This is the page which is used to display the Requisation list Add BU contact to multipal requisitions
    Created By: pravin khot 
    Created On: 23/May/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------       

*/


using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using TPS360.BusinessFacade;
namespace TPS360.Web.UI.Requisition
{
    public partial class MasterRequisitionBUContactAddInList : RequisitionBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ObjectDataSource odsRequisitionList = (ObjectDataSource)uclRequisition.FindControl("odsRequisitionList");

            odsRequisitionList.SelectParameters["IsCompanyContact"].DefaultValue = "false";
        }
        [System.Web.Services.WebMethod]
        public static object[] ChangeRequisitionStatus(object data)
        {
            IFacade Facade = new Facade();
            Dictionary<string, object> param =
                  (Dictionary<string, object>)data;
            try
            {
                JobPosting jobPosting = Facade.GetJobPostingById(Convert .ToInt32 (param ["RequisitionId"]));
                jobPosting.JobStatus = Convert.ToInt32(param["StatusId"]);
                jobPosting.UpdatorId = Convert.ToInt32(param["UpdatorId"]);
                ApplicationWorkflow applicationWorkflow = Facade.GetApplicationWorkflowByTitle(WorkFlowTitle.RequisitionManagementShouldSelectedApprovedUsersAbilityToChangeStats.ToString());
                if (applicationWorkflow != null && applicationWorkflow.RequireApproval)
                {
                    IList<ApplicationWorkflowMap> applicationWorkflowMapList = Facade.GetAllApplicationWorkflowMapByApplicationWorkflowId(applicationWorkflow.Id);
                    if (applicationWorkflowMapList != null && applicationWorkflowMapList.Count > 0)
                    {
                        foreach (ApplicationWorkflowMap applicationWorkflowMap in applicationWorkflowMapList)
                        {
                            if (applicationWorkflowMap.MemberId == Convert.ToInt32(param["UpdatorId"]))
                            {
                                Facade.UpdateJobPosting(jobPosting);
                                MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionStatusChange, jobPosting.Id, Convert.ToInt32(param["UpdatorId"]), Facade);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    Facade.UpdateJobPosting(jobPosting);
                    MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionStatusChange, jobPosting.Id, Convert.ToInt32(param["UpdatorId"]), Facade);
                    string[] result = { "Changed", "" };
                    return result;
                }
                
            }
            catch (ArgumentException ax)
            {
               
            }
            string[] result1 = { "Changed", "" };
            return result1;
        }
       
    }

}

