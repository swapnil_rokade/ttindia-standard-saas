﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: PreselectedInterviewHiringMatrix.aspx.cs
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
 *    0.1               10/March/2016       pravin khot         Change logo on the Generate Offer Letter. 
 *    0.2               25/May/2016         pravin khot         added status_id and Requisitionstatus on page load 
 *    0.3               10/June/2016        pravin khot         USING CHANGE temp file generated
 *    0.4               23/June/2016        pravin khot         added new field -source
 *    0.5               28/July/2016        pravin khot         added- RejectCandidate cand = facade.GetRejectCandidateByMemberIdAndJobPostingID(Int32.Parse(MemberId), JobPostingId);
 *    0.6               1/Mar/2017          Sumit Sonawane      added for Change Hiring Status Popup
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using TPS360.Common;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using WebChart;
using System.Configuration;
using AjaxControlToolkit;
using System.Data.SqlClient;
using System.Linq;
using System.Data;
using System.Web.Configuration;
using TPS360.BusinessFacade;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.IO;
using System.Net;
using System.Web.Security;
using System.Xml;
using iTextSharp.text.pdf;
using iTextSharp.text;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using System.Globalization;
//using msWord = Microsoft.Office.Interop.Word;

//using System.Text;
//using StringFormat = System.Drawing.StringFormat;


namespace TPS360.Web.UI.ATS
{
    public partial class PreSelectedInterviewHiringMatrix : ATSBasePage
    {
        #region Member Variables

        protected WebChart.ChartControl LineChart;
        SecureUrl url;
        public int MemberId;
        public int JobId;
        public int interviewLevel;
        public MemberHiringProcess memberHiringProcess;
        private bool _IsMemberMailAccountAvailable = false;
        private string CurrentJobPostingSkillNames = "";
        bool Remove = false;
        private bool isAccess = false;
        DropDownList dummyList = new DropDownList();
        public int HiringMatrixInterviewLevel
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_SELECTEDTAB]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_SELECTEDTAB]);
                }
                return 0;
            }
        }
        public int _level = 0;
        public int CurrentLevelId
        {
            get
            {
                return _level;
            }
            set
            {
                _level = value;
            }
        }
        public string BackFrom
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MSG]))
                {
                    return Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];
                }
                return string.Empty;
            }
        }
        public event Action OnMove;


        public bool isMainApplication
        {
            get
            {
                switch (ApplicationSource)
                {
                    case ApplicationSource.LandTApplication:
                        return false;
                        break;
                    case ApplicationSource.MainApplication:
                        return true;
                        break;
                    default:
                        return true;
                        break;
                }
                return true;
            }
        }
        #endregion

        #region Methods

        private void HiringDetailsAdded(string MemberID, int StatusId, bool IsAdded)
        {
            txtSelectedIds.Text = "";
            var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
            var offer = webConfig.AppSettings.Settings["Offer"];
            if (IsAdded)
            {
                if (StatusId > 0)
                {
                    Facade.MemberJobCart_MoveToNextLevel((hdncurretn.Value != string.Empty ? Convert.ToInt32(hdncurretn.Value) : 0), base.CurrentMember.Id, MemberID, CurrentJobPosting.Id, StatusId.ToString());
                    Facade.UpdateCandidateRequisitionStatus(CurrentJobPosting.Id, MemberID, CurrentMember.Id, StatusId);
                    MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, CurrentJobPosting.Id, MemberID, CurrentMember.Id, offer.Value, Facade);
                    BindList();
                    RefreshCandidateCountinLeftPanel();
                    MiscUtil.ShowMessage(lblMessage, "Selected candidate(s) moved Successfully.", false);
                }
                else
                {
                    BindList();
                    MiscUtil.ShowMessage(lblMessage, "Offer Details Updated Successfully.", false);
                }
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Offer Details Removed Successfully.", false);
            }
        }
        private void JoiningDetailsAdded(string MemberID, int StatusId, bool IsAdded)
        {
            txtSelectedIds.Text = "";
            var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
            var join = webConfig.AppSettings.Settings["Join"];
            if (IsAdded)
            {
                if (StatusId > 0)
                {
                    Facade.MemberJobCart_MoveToNextLevel((hdncurretn.Value != string.Empty ? Convert.ToInt32(hdncurretn.Value) : 0), base.CurrentMember.Id, MemberID, CurrentJobPosting.Id, StatusId.ToString());
                    Facade.UpdateCandidateRequisitionStatus(CurrentJobPosting.Id, MemberID, CurrentMember.Id, StatusId);
                    MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, CurrentJobPosting.Id, MemberID, CurrentMember.Id, join.Value, Facade);
                    BindList();
                    RefreshCandidateCountinLeftPanel();
                    MiscUtil.ShowMessage(lblMessage, "Selected candidate(s) moved Successfully.", false);
                }
                else
                {
                    BindList();
                    MiscUtil.ShowMessage(lblMessage, "Joining Details Updated Successfully.", false);
                }
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Joining Details Removed Successfully.", false);
            }
        }
        private void UndoRejectDetails()
        {
            RefreshCandidateCountinLeftPanel();
        }
        private void RejectDetailsAdded(int StatusId, string MemberId)
        {
            if (StatusId > 0)
            {
                // 
                //Facade.UpdateCandidateRequisitionStatus(CurrentJobPosting.Id, MemberId, CurrentMember.Id, StatusId);
                // MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateRejected, CurrentJobPostingId , MemberId , CurrentMember.Id, Facade);
                BindList();
                RefreshCandidateCountinLeftPanel();
                MiscUtil.ShowMessage(lblMessage, "Selected candidate(s) rejected Successfully.", false);
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Rejection Details Updated Successfully.", false);
            }



            //BindList();
            //RefreshCandidateCountinLeftPanel();
            txtSelectedIds.Text = "";

        }
        private void SubmissionDetailsAdded(int StatusId, string MemberId)
        {
            txtSelectedIds.Text = "";
            var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
            var submit = webConfig.AppSettings.Settings["Submission"];
            if (StatusId > 0)
            {
                Facade.MemberJobCart_MoveToNextLevel((hdncurretn.Value != string.Empty ? Convert.ToInt32(hdncurretn.Value) : 0), base.CurrentMember.Id, MemberId, CurrentJobPosting.Id, StatusId.ToString());
                Facade.UpdateCandidateRequisitionStatus(CurrentJobPosting.Id, MemberId, CurrentMember.Id, StatusId);
                MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, CurrentJobPosting.Id, MemberId, CurrentMember.Id, submit.Value, Facade);
                BindList();
                RefreshCandidateCountinLeftPanel();
                MiscUtil.ShowMessage(lblMessage, "Selected candidate(s) moved Successfully.", false);
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Submission Details Updated Successfully.", false);
            }
        }
        private DateTime getTime(DateTime date, int duration)
        {
            if (duration > 0)
            {
                duration = duration / 60;
            }
            return date.AddMinutes(Convert.ToDouble(duration));
        }

        //private void ShowRequisitionBriefDescription()
        //{
        //    JobPosting jobPosting = CurrentJobPosting;
        //    if (jobPosting != null)
        //    {
        //        lblJobTitle.Text =MiscUtil .RemoveScript ( jobPosting.JobTitle);
        //        lblJobLocation.Text =MiscUtil .RemoveScript ( jobPosting.City) + ((!string.IsNullOrEmpty(jobPosting.City) && jobPosting.StateId > 0) ? ", " : string.Empty) + MiscUtil.GetStateNameById(jobPosting.StateId, Facade);
        //        lblJobYearOfExp.Text =MiscUtil .RemoveScript ( jobPosting.MinExpRequired.ToString());
        //        if (lblJobYearOfExp.Text.Trim() != "" && jobPosting.MaxExpRequired.ToString().Trim() != "") lblJobYearOfExp.Text += " - ";
        //        lblJobYearOfExp.Text +=MiscUtil .RemoveScript ( jobPosting.MaxExpRequired.ToString());
        //        lblJobEducation.Text = MiscUtil.SplitValues(jobPosting.RequiredDegreeLookupId, ',', Facade);
        //        lblJobSalary.Text = string.Empty;
        //        if (MiscUtil .RemoveScript ( jobPosting.PayRate) == "Open")
        //            lblJobSalary.Text =MiscUtil .RemoveScript ( jobPosting.PayRate);
        //        else
        //        {
        //            lblJobSalary.Text =MiscUtil .RemoveScript ( jobPosting.PayRate) + "  " +MiscUtil .RemoveScript ( jobPosting.PayCycle);
        //            GenericLookup _PayRateCurrencyLookup = Facade.GetGenericLookupById(jobPosting.PayRateCurrencyLookupId);
        //            if (_PayRateCurrencyLookup != null)
        //                lblJobSalary.Text = _PayRateCurrencyLookup.Name + "  " +MiscUtil .RemoveScript ( jobPosting.PayRate )+ (_PayRateCurrencyLookup.Name == "INR" ? " Lacs" : "") + " /" + (jobPosting.PayCycle == "1" ? " Hour" : jobPosting.PayCycle == "2" ? " Day" : jobPosting.PayCycle == "3" ? " Month" : " Year");
        //        }
        //        if (jobPosting.JobSkillLookUpId.Trim() != string.Empty)
        //        {
        //            string skill =MiscUtil .RemoveScript ( CurrentJobPostingSkillNames);//MiscUtil.SplitSkillValues(jobPosting.JobSkillLookUpId, '!', Facade);
        //         lblSkills.Text = skill.Length <= 50 ? skill : (skill.Substring(0, 50) + "...");
        //         lblSkills.ToolTip = skill;
        //        }
        //    }
        //}

        //private void EnableDiableButtons()
        //{
        //    bool visible = lsvCandidateList != null && lsvCandidateList.Items.Count > 0;
        //    if (hdnNextLevelName .Value .Trim ()!= "")
        //    {
        //        //btnMoveToNextLevel.Visible = visible;
        //        btnSubmitToClient.Visible = visible;
        //    }
        //    //if(hdnPreviousLevelName.Value .Trim ()!="")   btnRemoveFromThisLevel.Visible = visible;
        //    btnEmail.Visible = ddlLevels.Visible = btnMoveToNextLevel .Visible = visible;
        //}

        private void SetButtonLevel()
        {

            //btnSubmitToClient.Visible = true;
            HiringMatrixLevels level = Facade.HiringMatrixLevel_GetNextLevelById(HiringMatrixInterviewLevel);
            if (level != null)
            {
                //btnMoveToNextLevel.Text = "Move To " + level.Name;
                hdnNextLevelName.Value = level.Name;
            }
            HiringMatrixLevels PreLevel = Facade.HiringMatrixLevel_GetPreviousLevelById(HiringMatrixInterviewLevel);
            if (PreLevel != null) hdnPreviousLevelName.Value = PreLevel.Name;
            else hdnPreviousLevelName.Value = ""; //btnRemoveFromThisLevel.Visible = false;
            HiringMatrixLevels levels = Facade.HiringMatrixLevel_GetLastLevel();

            if (levels.Id == HiringMatrixInterviewLevel)
            {
                // btnMoveToNextLevel.Visible = false;
                //btnSubmitToClient.Visible = false;
            }
            else if ((levels.SortingOrder == 0) && (levels.Id == HiringMatrixInterviewLevel))
            {
                //btnSubmitToClient.Visible = true;
            }


        }
        private void LoadHiringMatrixLevels()
        {
            IList<HiringMatrixLevels> level = Facade.GetAllHiringMatrixLevelsForHiringManager(CurrentMember.Id); //Facade.GetAllHiringMatrixLevels();//.Where(x => x.Id != HiringMatrixInterviewLevel).ToList();
            if (hdncurretn.Value == "") hdncurretn.Value = "0";
            if (level != null)
            {
                level = level.Where(x => x.Id != Convert.ToInt32(hdncurretn.Value)).ToList();
                HiringMatrixLevels newl = new HiringMatrixLevels();
                ddlLevels.SelectedIndex = -1;
                newl.Id = 0;
                newl.Name = "Please Select";
                level.Insert(0, newl);

                ddlLevels.DataSource = level;
                ddlLevels.DataTextField = "Name";
                ddlLevels.DataValueField = "Id";
                try { ddlLevels.DataBind(); }
                catch { }
            }

            ddlLevels = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlLevels);
            var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
            var submit = webConfig.AppSettings.Settings["Submission"];
            var offer = webConfig.AppSettings.Settings["Offer"];
            var join = webConfig.AppSettings.Settings["Join"];
            var interview = webConfig.AppSettings.Settings["Interview"];
            string AppPath = "";
            if (isMainApplication) AppPath = "Common";

            foreach (System.Web.UI.WebControls.ListItem list in ddlLevels.Items)
            {
                if (list.Text == submit.Value)
                {
                    list.Value = UrlConstants.ApplicationBaseUrl + "Modals/SubmissionDetails.aspx?StatusId=" + list.Value + "&JID=" + CurrentJobPostingId;
                }
                else if (list.Text == offer.Value)
                {
                    list.Value = UrlConstants.ApplicationBaseUrl + "Modals/" + AppPath + "HiringDetails.aspx?StatusId=" + list.Value + "&JID=" + CurrentJobPostingId;
                }
                else if (list.Text == join.Value)
                {
                    list.Value = UrlConstants.ApplicationBaseUrl + "Modals/" + AppPath + "JoiningDetails.aspx?StatusId=" + list.Value + "&JID=" + CurrentJobPostingId;
                }
                else if (list.Text == interview.Value)
                {
                    list.Value = UrlConstants.ApplicationBaseUrl + "Modals/ScheduleInterview.aspx?StatusId=" + list.Value + "&JID=" + CurrentJobPostingId;
                }
                else if (list.Text == "Rejected")
                {
                    list.Value = UrlConstants.ApplicationBaseUrl + "Modals/RejectionDetails.aspx?StatusId=" + list.Value + "&JID=" + CurrentJobPostingId;
                }
                else if (list.Text == "Offer Decline" && !isMainApplication)
                {
                    list.Value = UrlConstants.ApplicationBaseUrl + "Modals/OfferRejected.aspx?StatusId=" + list.Value + "&JID=" + CurrentJobPostingId;
                }
                else
                {
                }
            }
        }
        protected void ddlLevels_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnMoving.Value = ddlLevels.SelectedValue;
            btnMoveToNextLevel_Click(sender, e);

            ControlHelper.SelectListByValue(ddlLevels, odsHiringMatrix.SelectParameters["SelectionStepLookupId"].DefaultValue);
        }
        private void PrepareView()
        {
            LoadHiringMatrixLevels();
        }

        private void UncheckAllCheckBox()
        {
            if (lsvCandidateList != null)
            {
                foreach (ListViewDataItem item in lsvCandidateList.Items)
                {
                    CheckBox chkCandidate = (CheckBox)item.FindControl("chkCandidate");
                    if (chkCandidate != null) chkCandidate.Checked = false;
                }
            }
        }

        private int getNumberOfSelectedCandidates()
        {
            char[] delim = { ',' };
            string[] canArr = txtSelectedIds.Text.Split(delim, StringSplitOptions.RemoveEmptyEntries);
            return canArr == null ? 0 : canArr.Length;
        }

        private void SubmitToClient(string MemberIds)
        {
            if (base.CurrentJobPosting.ClientId > 0)
            {
                Company CurrentCompany = Facade.GetCompanyById(base.CurrentJobPosting.ClientId);

                if (CurrentCompany != null)
                {
                    if (CurrentCompany.PrimaryContact.Email == null)
                    {
                        string strInterval = string.Empty;
                        MiscUtil.ShowMessage(lblMessage, "  " + " Please add primary contact for " + CurrentCompany.CompanyName + " before submission", true);
                    }
                    else
                    {
                        string applicantIds = MemberIds;
                        if (!string.IsNullOrEmpty(applicantIds))
                            Helper.Url.Redirect(UrlConstants.ATS.ATS_SUBMITTOCLIENT, string.Empty, UrlConstants.PARAM_HIRING_MATRIX_INTERVIEW_LAVEL, (odsHiringMatrix.SelectParameters["SelectionStepLookupId"].DefaultValue).ToString(), UrlConstants.PARAM_JOB_ID, base.CurrentJobPostingId.ToString(), UrlConstants.PARAM_APPLICANT_ID, applicantIds);
                        else
                            MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);
                    }
                }
                else
                {
                    if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                    {
                        MiscUtil.ShowMessage(lblMessage, "Please associate the Requisition with a Department before submitting", true);
                    }
                    else MiscUtil.ShowMessage(lblMessage, "Please associate the Requisition with a Client before submitting", true);
                }
            }
            else
            {
                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                {
                    MiscUtil.ShowMessage(lblMessage, "Please associate the Requisition with a Department before submitting", true);
                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "Please associate the Requisition with a Client before submitting", true);
                }
            }
        }

        private string HighLightText(string OriginalText, string Compare)
        {
            if (Compare != "" && OriginalText != "")
            {
                if (OriginalText.ToLower().Contains(Compare.ToLower()))
                {
                    OriginalText = OriginalText.ToLower().Replace(Compare.ToLower(), "<span class=highlight>" + Compare + "</span>");
                    return OriginalText;

                }
            }
            return OriginalText;
        }

        private string HighLightExactText(string OriginalText, string Compare)
        {
            if (OriginalText != null)
            {
                if (Compare.ToLower() == OriginalText.ToLower() && OriginalText.Trim() != "")
                {
                    OriginalText = "<span class=highlight>" + OriginalText + "</span>";
                    return OriginalText;
                }
            }
            return OriginalText;
        }

        private string HighLightSkillName(string text)
        {
            char[] delim = { ',' };
            string[] oriArr = CurrentJobPostingSkillNames.Trim().Split(delim, StringSplitOptions.RemoveEmptyEntries);
            string[] curArr = text.Split(',');
            string ret = string.Empty;
            if (oriArr.Length > 0 && curArr.Length > 0)
            {
                foreach (string s in curArr)
                {
                    if (ret != string.Empty) ret += ",";
                    if (oriArr.Contains(s.Trim()))
                    {
                        ret += "<span class=highlight>" + s + "</span>";
                    }
                    else ret += s;
                }

            }
            else
                return text;
            return ret;
        }


        public void SetTabText()
        {
            //Infragistics.WebUI.UltraWebTab.UltraWebTab uwtRequisitionHiringMatrixNavigationTopMenu = (Infragistics.WebUI.UltraWebTab.UltraWebTab)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uwtRequisitionHiringMatrixNavigationTopMenu");


            int[] hiringMatrixCount = Facade.GetHiringMatrixMemberCount(base.CurrentJobPostingId);
            IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
            for (int i = 0; i < hiringMatrixLevels.Count; i++)
            {
                try
                {
                    //uwtRequisitionHiringMatrixNavigationTopMenu.Tabs[i].Text = hiringMatrixLevels[i].Name + " (" + hiringMatrixCount[i].ToString() + ")";
                }
                catch
                {
                }
            }
            //uwtRequisitionHiringMatrixNavigationTopMenu.Tabs[hiringMatrixLevels.Count].Text = "Rejected (" + Facade.GetRejectCandidateCount(base.CurrentJobPostingId) + ")";
        }

        private void PlaceUpDownArrow()
        {
            try
            {

                LinkButton lnk = (LinkButton)lsvCandidateList.FindControl(txtSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                if (lnk.CommandArgument.Contains("Experience") || lnk.CommandArgument.Contains("[MJC].[UpdateDate]"))
                {
                    im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Descending" : "Ascending"));
                }
                else
                    im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));


            }
            catch
            {
            }

        }

        private bool CheckSelectedList(int ID)
        {
            char[] del = { ',' };
            string[] arrID = txtSelectedIds.Text.Split(del, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in arrID)
            {
                int d = 0;
                Int32.TryParse(s, out d);
                if (d == ID)
                {
                    return true;
                }
            }
            return false;
        }

        protected void lnkReqStatus_Click(object sender, EventArgs e)
        {

            LinkButton lnkButton = (LinkButton)sender;
            switch (lnkButton.CommandArgument)
            {
                case "ChangeStatus":
                    //mpeChangeStatus.Enabled = true;
                    //uclTemplate.ModalTitle = base.CurrentJobPosting.JobTitle + " - Change Requisition Status";
                    ////lblJobStatusHeader.Text =
                    //mpeChangeStatus.Show();
                    break;
                case "Notes":
                    //mpeNotes.Enabled = true;
                    //uclTemplateNotes.ModalTitle = base.CurrentJobPosting.JobTitle + " - Notes";
                    //uclNotes.Bind();
                    //mpeNotes.Show();
                    break;
                case "AssignedManagers":
                    //mpeManagers.Enabled = true;
                    //uclTemplateManagers.ModalTitle = base.CurrentJobPosting.JobTitle + " - Assigned Team";
                    //uclManagers.JobPostingId = CurrentJobPostingId;
                    //mpeManagers.Show();
                    break;
                case "HiringLog":
                    //ASP.controls_candidateactionlog_ascx actionLog = (ASP.controls_candidateactionlog_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateActionLog").FindControl("uclActionLog");
                    //AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeActionLog");
                    //ASP.controls_modaltemplate_ascx modaltemplate = (ASP.controls_modaltemplate_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateActionLog");
                    //modaltemplate.ModalTitle = actionLog.getModalTitle();
                    //actionLog.MemberID = "";

                    //ext.Enabled = true;
                    //ext.Show();
                    break;
            }
        }

        #endregion

        #region Property

        #endregion

        #region Methods
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ArrayList AList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            uclConfirm.MsgBoxAnswered += MessageAnswered;

            if (AList.Contains(403))
            {  //**********Code added by pravin khot on 25/May/2016*****
                int status_id = Facade.GetRequisitionStatusBy_Id(CurrentJobPostingId);
                if (status_id > 0)
                {
                    isAccess = false ;
                }
                else
                {
                    isAccess = true;
                }
               //**********************END*******************
            }
            else
                isAccess = false;
            if (!IsPostBack)
            {
                //lnkReqTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + CurrentJobPostingId + "&FromPage=JobDetail" + "','700px','570px'); return false;");
            }
            hdnCurrentJobPostingId.Value = CurrentJobPostingId.ToString();
            hdnCurrentUserId.Value = CurrentMember.Id.ToString();
            FillDummyList();
            if (hdnNeedListPostBack.Value == "1")
            {
                this.Page.Title = hdnPageTitle.Value;

                RefreshCandidateCountinLeftPanel();
                lsvCandidateList.DataBind();
                uclRejection.BindList();
                hdnNeedListPostBack.Value = "";
                txtSelectedIds.Text = "";
                return;
            }

            this.Page.MaintainScrollPositionOnPostBack = true;
            //odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = CurrentJobPosting.Id.ToString();
            odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = base.CurrentMember.Id.ToString();
            _IsMemberMailAccountAvailable = MiscUtil.IsValidMailSetting(Facade, base.CurrentMember.Id);
            //CurrentJobPostingSkillNames = Facade.JobPosting_GetRequiredSkillNamesByJobPostingID(CurrentJobPostingId);
            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Dashboard/" + UrlConstants.Dashboard.NEWMAIL, "", UrlConstants.PARAM_JOB_ID, base.CurrentJobPostingId.ToString(), UrlConstants.PARAM_PAGE_FROM, "JobDetail");

            if (!Page.IsPostBack)
            {
                lblCurrentJobPostingID.Text = CurrentJobPostingId.ToString();
                PrepareView();
                Session["PreciseSearch"] = null;
                string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];
                if (!StringHelper.IsBlank(message))
                {
                    if (message != "BackFromSubmit")
                        MiscUtil.ShowMessage(lblMessage, message, false);
                }
                else if (Session["SentStatus"] != null)
                {
                    message = Session["SentStatus"].ToString();
                    MiscUtil.ShowMessage(lblMessage, message, false);
                    Session["SentStatus"] = null;
                }
                if (Session["Error"] != "")
                {
                    if (Convert.ToInt32(Session["Error"]) == 1)
                    {
                        MiscUtil.ShowMessage(lblMessage, "Remarks saved successfully.", false);
                    }
                    else if (Convert.ToInt32(Session["Error"]) == 2)
                    {
                        MiscUtil.ShowMessage(lblMessage, "Remarks updated successfully.", false);
                    }
                    Session.Remove("Error");
                }
                txtSortColumn.Text = "lnkHeaderRelevance";
                txtSortOrder.Text = "DESC";
                //PlaceUpDownArrow();
            }
            else
            {
                MiscUtil.LoadAllControlState(Request, this, true);
                Session["PreciseSearch"] = null;
            }
            string pagesize = "";

            //ControlHelper.SetHyperLink(lnkReqTitle, UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE, string.Empty, CurrentJobPosting.JobTitle + " - " + CurrentJobPosting.JobPostingCode, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(CurrentJobPostingId));
            string Requisitionstatus = Facade.GetRequisitionStatus_Id(CurrentJobPostingId);//added by pravin khot on 25/May/2016

            lnkReqTitle.Text = "Candidate Hiring Status Update";
            //lnkReqTitle.Text = CurrentJobPosting.JobTitle + " - " + CurrentJobPosting.JobPostingCode + " - [" + Requisitionstatus + "]"; //Requisitionstatus added by pravin khot on 25/May/2016
            this.Page.Title = hdnPageTitle.Value;
            if (!IsPostBack)
            {
                hdnCurname.Value = "All";
                this.Page.Title = "Candidate Hiring Status Update";
                hdnPageTitle.Value = this.Page.Title;
                //IList<HiringMatrixLevels> level = Facade.GetAllHiringMatrixLevelsWithCount(CurrentJobPostingId); ;
                IList<HiringMatrixLevels> level = Facade.GetAllHiringMatrixLevelsWithCountByMemberId(CurrentMember.Id);
                if (level != null)
                {
                    hdnCount.Value = level.Sum(x => x.Count).ToString();
                    HiringMatrixLevels l1 = new HiringMatrixLevels();
                    l1.Id = 0;
                    l1.Name = "All";
                    l1.Count = Convert.ToInt32(hdnCount.Value);
                    level.Insert(0, l1);
                    lvNavigationItems.DataSource = level;
                    lvNavigationItems.DataBind();
                }
                hdnCount.Value = "0";

                if (BackFrom == "BackFromSubmit" && HiringMatrixInterviewLevel > 0)
                {
                    odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = CurrentJobPosting.Id.ToString();
                    odsHiringMatrix.SelectParameters["SelectionStepLookupId"].DefaultValue = HiringMatrixInterviewLevel.ToString();
                    string LevelName = string.Empty;
                    foreach (ListViewDataItem item in lvNavigationItems.Items)
                    {
                        LinkButton button = (LinkButton)item.FindControl("lnkLeftMenuItem");
                        HiddenField hnSelectedLevel = (HiddenField)item.FindControl("hnSelectedLevel");
                        HtmlGenericControl spName = (HtmlGenericControl)item.FindControl("spName");
                        if (hnSelectedLevel.Value == HiringMatrixInterviewLevel.ToString())
                        {
                            button.CssClass = "active level";
                            button.Enabled = false;
                            LevelName = spName.InnerText;
                        }
                        else
                        {
                            button.CssClass = "level";
                            button.Enabled = true;
                        }
                        hdnCurname.Value = LevelName;
                        this.Page.Title = LevelName + " - " + "Candidate Hiring Status Update";
                        hdnPageTitle.Value = this.Page.Title;
                    }
                }
            }

            string s = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
            if (s.ToLower() == "dd/mm/yyyy")
                hdnDateTimeFormat.Value = "%d/%m/%Y";
            else hdnDateTimeFormat.Value = "%m/%d/%Y";

            if (!IsPostBack)
            {
                string apppath = "";
                if (isMainApplication) apppath = "Common";
                string selectedIds = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID].ToString();
                if (selectedIds != "")
                {
                    string modal = Helper.Url.SecureUrl[UrlConstants.PARAM_PAGE_FROM].ToString();
                    string TargetLevelId = Helper.Url.SecureUrl["TargetLevelId"].ToString();
                    if (modal == "Joined")
                    {
                        string src = UrlConstants.ApplicationBaseUrl + "Modals/" + apppath + "JoiningDetails.aspx?StatusId=" + TargetLevelId + "&JID=" + CurrentJobPostingId + "&CanId=" + selectedIds;
                        ScriptManager.RegisterStartupScript(lblMessage, typeof(MiscUtil), "dfdf", "javascript:ShowOfferOrJoinedModal('" + src + "');", true);
                        mpeHiringDetails.Show();
                    }
                    else if (modal == "Offered")
                    {
                        string src = UrlConstants.ApplicationBaseUrl + "Modals/" + apppath + "HiringDetails.aspx?StatusId=" + TargetLevelId + "&JID=" + CurrentJobPostingId + "&CanId=" + selectedIds;
                        ScriptManager.RegisterStartupScript(lblMessage, typeof(MiscUtil), "dfdf", "javascript:ShowOfferOrJoinedModal('" + src + "');", true);
                        mpeHiringDetails.Show();
                    }
                    else if (modal == "Offer Decline" && !isMainApplication)
                    {
                        string src = UrlConstants.ApplicationBaseUrl + "Modals/OfferRejected.aspx?StatusId=" + TargetLevelId + "&JID=" + CurrentJobPostingId + "&CanId=" + selectedIds;
                        ScriptManager.RegisterStartupScript(lblMessage, typeof(MiscUtil), "dfdf", "javascript:ShowOfferOrJoinedModal('" + src + "');", true);
                        mpeHiringDetails.Show();
                    }
                }
            }
        }

        #endregion

        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                if (txtSelectedIds.Text.Trim() != "")
                {
                    ASP.controls_confirmationwindow_ascx msg = (ASP.controls_confirmationwindow_ascx)sender;
                    if (msg.Args == "undo")
                    {
                        //Facade.MemberJobCart_MoveToNextLevel((hdncurretn.Value != string.Empty ? Convert.ToInt32(hdncurretn.Value) : 0), base.CurrentMember.Id, txtSelectedIds.Text.Trim(), CurrentJobPosting.Id, hdnMoving.Value);
                        //Facade.DeleteRejectCandidateByMemberIdAndJobPostingID(Int32.Parse(txtSelectedIds.Text.Trim()), CurrentJobPostingId);
                        //RefreshCandidateCountinLeftPanel();
                        //hdnMoving.Value = "";
                        //txtSelectedIds.Text = "";
                        //BindList();
                    }
                    else
                    {
                        //ASP.controls_rejectdetails_ascx candidate = (ASP.controls_rejectdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateReject").FindControl("uclRejectCandidate");
                        //ASP.controls_modaltemplate_ascx modalTemlate = (ASP.controls_modaltemplate_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateReject");
                        //AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeRejectCandidate");
                        //candidate.MemberID = txtSelectedIds.Text;
                        //candidate.HiringMatrixLevel = hdncurretn.Value != string.Empty ? Convert.ToInt32(hdncurretn.Value) : 0;
                        //candidate.JobPostingId = CurrentJobPostingId;
                        ////Label lblRejectTitle = (Label)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("lblRejectTitle");
                        ////lblRejectTitle.Text =
                        //candidate.StatusId = Convert.ToInt32(hdnMoving.Value == "" ? "0" : hdnMoving.Value);
                        //modalTemlate.ModalTitle = candidate.getModalTitle();
                        //ext.Enabled = true;
                        //ext.Show();
                        //txtSelectedIds.Text = "";
                        //BindList();
                    }
                    if (msg.Args == "delete")
                    {

                    }
                }
                else MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);

            }
        }
        public void FillDummyList()
        {
            IList<HiringMatrixLevels> levels = new List<HiringMatrixLevels>();
            levels = Facade.GetAllHiringMatrixLevels();
            //HiringMatrixLevels l = new HiringMatrixLevels();
            //l.Id = -1;
            //l.Name = "Rejected";
            //levels.Add(l);
            dummyList.DataSource = levels;
            dummyList.DataTextField = "Name";
            dummyList.DataValueField = "Id";
            dummyList.DataBind();
            dummyList = (DropDownList)MiscUtil.RemoveScriptForDropDown(dummyList);
            //dummyList.Items.Add(new ListItem("Rejected", "Reject"));

        }

        private void RefreshCandidateCountinLeftPanel()
        {
            IList<HiringMatrixLevels> level = Facade.GetAllHiringMatrixLevelsWithCount(CurrentJobPostingId); ;
            //  hdnRejectCount.Value = Facade.GetRejectCandidateCount(CurrentJobPostingId).ToString();
            hdnCount.Value = (level.Sum(x => x.Count)).ToString();// + Convert .ToInt32 (hdnRejectCount .Value )).ToString ();

            foreach (ListViewDataItem item in lvNavigationItems.Items)
            {
                LinkButton lnkLeftMenuItem = (LinkButton)item.FindControl("lnkLeftMenuItem");
                HtmlGenericControl spCount = (HtmlGenericControl)item.FindControl("spCount");
                switch (lnkLeftMenuItem.CommandArgument)
                {
                    case "0":
                        spCount.InnerText = "(" + hdnCount.Value + ")";
                        break;
                    //case "-1":
                    //    spCount.InnerText = "("+hdnRejectCount.Value +")";
                    //    break;
                    default:
                        var count = level.Where(x => x.Id == Convert.ToInt32(lnkLeftMenuItem.CommandArgument));
                        if (count.Count() > 0) spCount.InnerText = "(" + count.ToList()[0].Count.ToString() + ")";
                        break;
                }


            }
            hdnCount.Value = "0";
            // hdnRejectCount.Value = "0";
        }
        public void lnkLeftMenuItem_Click(object sender, EventArgs e)
        {
            txtSelectedIds.Text = "";
            LinkButton link = new LinkButton();
            link = (LinkButton)sender;
            string LevelName = string.Empty;

            //LevelName = spName.InnerText;
            foreach (ListViewDataItem item in lvNavigationItems.Items)
            {
                LinkButton button = (LinkButton)item.FindControl("lnkLeftMenuItem");
                HiddenField hnSelectedLevel = (HiddenField)item.FindControl("hnSelectedLevel");
                HtmlGenericControl spName = (HtmlGenericControl)item.FindControl("dvspanName").FindControl("spName");
                if (hnSelectedLevel.Value == link.CommandArgument)
                {
                    button.CssClass = "active level";
                    button.Enabled = false;
                    LevelName = spName.InnerText;
                }
                else
                {
                    button.CssClass = "level";
                    button.Enabled = true;
                }
                //button.CssClass = "level";
                //button.Enabled = true;
            }
            //link.CssClass = "active level";
            //link.Enabled = false;
            //odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = CurrentJobPosting.Id.ToString();
            odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = base.CurrentMember.Id.ToString();
            odsHiringMatrix.SelectParameters["SelectionStepLookupId"].DefaultValue = link.CommandArgument;
            hdncurretn.Value = link.CommandArgument;
            //ControlHelper.SelectListByValue(ddlLevels, link.CommandArgument);

            if (LevelName == "Rejected")
            {
                dvbulkAction.Visible = false;
                dvhirng.Visible = false;
                uclRejection.showList();
            }
            else
            {
                dvbulkAction.Visible = true;
                dvhirng.Visible = true;
                uclRejection.hideList();
            }
            hdnCurname.Value = LevelName;
            this.Page.Title =LevelName + " - " + "Candidate Hiring Status Update";
            lnkReqTitle.Text = "Candidate Hiring Status Update" + " - " + LevelName;
            hdnPageTitle.Value = this.Page.Title;
            txtSelectedIds.Text = "";

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {

                    pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            LoadHiringMatrixLevels();
        }
        //public void lnkAll_Click(object sender, EventArgs e)
        //{
        //    LinkButton link = new LinkButton();
        //    link = (LinkButton)sender;
        //    link.CssClass = "active level";
        //    odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = CurrentJobPosting.Id.ToString();
        //    odsHiringMatrix.SelectParameters["SelectionStepLookupId"].DefaultValue = "0";
        //    dvhirng.Visible = true ;
        //    dvReje.Visible = false ;
        //}
        //public void lnkRejected_Click(object sender, EventArgs e)
        //{
        //    LinkButton link = new LinkButton();
        //    link = (LinkButton)sender;
        //    link.CssClass = "active level";
        //    dvhirng.Visible = false;
        //    dvReje.Visible = true;
        //    uclRejection.RejectLevel = "Rejected";
        //}
        public void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList list = new DropDownList();
            list = (DropDownList)sender;
            var row = (ListViewDataItem)list.NamingContainer;
            int Id = Convert.ToInt32(((HiddenField)row.FindControl("hfCandidateId")).Value);
            int LevelId = Convert.ToInt32(((HiddenField)row.FindControl("hdnCurrentlevelId")).Value);
            string s = ((HiddenField)row.FindControl("hfstatuschangeId")).Value;
            int statusId = 0;
            hdncurretn.Value = LevelId.ToString();
            statusId = Convert.ToInt32(((HiddenField)row.FindControl("hfstatuschangeId")).Value);
            ControlHelper.SelectListByValue(list, statusId.ToString());
            if (list.SelectedItem.Text == "Rejected")
            {
                hdnMoving.Value = list.SelectedValue;
                txtSelectedIds.Text = Id.ToString();
                hdnRemove.Value = "Remove";
                uclConfirm.AddMessage("Are you sure that you want to remove this candidate  from the Hiring Matrix?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
                {
                }
            }
            else
            {


                //statusId = ((dr)row.FindControl("ddlStatus")).Value;
                if (Id > 0)
                {
                    ControlHelper.SelectListByValue(list, statusId.ToString());
                    var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
                    var submit = webConfig.AppSettings.Settings["Submission"];
                    var offer = webConfig.AppSettings.Settings["Offer"];
                    var join = webConfig.AppSettings.Settings["Join"];
                    var interview = webConfig.AppSettings.Settings["Interview"];
                    if (list.SelectedItem.Text == submit.Value)
                    {
                        //ASP.controls_submissiondetails_ascx submission = (ASP.controls_submissiondetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateSubmissions").FindControl("uclSubmissionDetails");
                        //AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeSubmissionDetails");
                        //submission.JobPostingId = CurrentJobPostingId;
                        //submission.MemberID = Id.ToString();
                        //submission.StatusId = statusId;

                        //ext.Enabled = true;
                        //ext.Show();
                    }
                    else if (list.SelectedItem.Text == offer.Value)
                    {
                        //ASP.controls_hiringdetails_ascx offered = (ASP.controls_hiringdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateHiringDetails").FindControl("uclHiringDetails");
                        //AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeHiringDetails");

                        //offered.JobPostingId = CurrentJobPostingId;
                        //offered.MemberID = Id.ToString();
                        //offered.StatusId = statusId;
                        //ext.Enabled = true;
                        //ext.Show();
                    }
                    else if (list.SelectedItem.Text == join.Value)
                    {
                        //ASP.controls_joiningdetails_ascx joined = (ASP.controls_joiningdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateJoining").FindControl("uclJoiningDetails");
                        //AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeJoiningDetails");
                        //joined.JobPostingId = CurrentJobPostingId;
                        //joined.MemberID = Id.ToString();
                        //joined.StatusId = statusId;
                        //ext.Enabled = true;
                        //ext.Show();
                    }
                    else if (list.SelectedItem.Text == interview.Value)
                    {
                    }
                    else
                    {
                        if (hdncurretn.Value == "-1")
                        {
                            txtSelectedIds.Text = Id.ToString();
                            hdnMoving.Value = statusId.ToString();
                            uclConfirm.AddMessage("Are you sure that you undo the rejection for this candidate?", ConfirmationWindow.enmMessageType.Attention, true, true, "undo");
                            {
                            }
                        }
                        else
                        {
                            Facade.MemberJobCart_MoveToNextLevel((hdncurretn.Value != string.Empty ? Convert.ToInt32(hdncurretn.Value) : 0), base.CurrentMember.Id, Id.ToString(), CurrentJobPosting.Id, statusId.ToString());
                            // Facade.UpdateCandidateRequisitionStatus(CurrentJobPosting.Id, Id.ToString(), CurrentMember.Id, statusId);
                            MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, CurrentJobPostingId, Id.ToString(), CurrentMember.Id, list.SelectedItem.Text, Facade);
                            BindList();
                            RefreshCandidateCountinLeftPanel();
                            MiscUtil.ShowMessage(lblMessage, MiscUtil.GetMemberNameById(Id, Facade) + " successfully moved to " + list.SelectedItem.Text, false);
                        }
                    }
                    //foreach (ListViewDataItem item in lsvCandidateList.Items)
                    //{
                    //    DropDownList ddlstatus = (DropDownList)item.FindControl("ddlStatus");
                    //    ddlstatus.SelectedValue = HiringMatrixInterviewLevel.ToString();
                    //}
                }
            }

        }
        protected void btnEmail_Click(object sender, EventArgs e)
        {
            string applicantIds = txtSelectedIds.Text;
            if (applicantIds != string.Empty)
            {
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Dashboard/" + UrlConstants.Dashboard.NEWMAIL, "", UrlConstants.PARAM_JOB_ID, base.CurrentJobPostingId.ToString(), UrlConstants.PARAM_PAGE_FROM, "HiringMatrix", UrlConstants.PARAM_SELECTED_IDS, applicantIds);
                UncheckAllCheckBox();
                txtSelectedIds.Text = "";
                ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(MiscUtil), "NewMail", "<script>btnMewMail_Click('" + url + "');</script>", false);
                lsvCandidateList.DataBind();
            }
            else
            {
                lsvCandidateList.DataBind();
                MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);
            }
        }

        private void BindList()
        {
            lsvCandidateList.Items.Clear();
            lsvCandidateList.DataBind();
            // SetTabText();
            // EnableDiableButtons();
        }
        protected void btnMoveToNextLevel_Click(object sender, EventArgs e)
        {
            if (txtSelectedIds.Text.Trim() != "")
            {
                var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
                var submit = webConfig.AppSettings.Settings["Submission"];
                var offer = webConfig.AppSettings.Settings["Offer"];
                var join = webConfig.AppSettings.Settings["Join"];
                if (ddlLevels.SelectedItem.Text == submit.Value)
                {
                    //ASP.controls_submissiondetails_ascx submission = (ASP.controls_submissiondetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateSubmissions").FindControl("uclSubmissionDetails");
                    //AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeSubmissionDetails");
                    //submission.BulkAction = "BulkAction";
                    //submission.JobPostingId = CurrentJobPostingId;
                    //submission.MemberID = txtSelectedIds.Text;

                    //submission.StatusId = Convert.ToInt32(ddlLevels.SelectedValue);
                    //ext.Enabled = true;
                    //ext.Show();
                }
                else if (ddlLevels.SelectedItem.Text == offer.Value)
                {
                    //ASP.controls_hiringdetails_ascx offered = (ASP.controls_hiringdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateHiringDetails").FindControl("uclHiringDetails");
                    //AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeHiringDetails");

                    //offered.JobPostingId = CurrentJobPostingId;
                    //offered.BulkAction = "BulkAction";
                    //offered.MemberID = txtSelectedIds.Text;
                    //offered.StatusId = Convert.ToInt32(ddlLevels.SelectedValue);
                    //ext.Enabled = true;
                    //ext.Show();
                }
                else if (ddlLevels.SelectedItem.Text == join.Value)
                {
                    //ASP.controls_joiningdetails_ascx joined = (ASP.controls_joiningdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateJoining").FindControl("uclJoiningDetails");
                    //AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeJoiningDetails");
                    //joined.JobPostingId = CurrentJobPostingId;
                    //joined.BulkAction = "BulkAction";
                    //joined.MemberID = txtSelectedIds.Text;
                    //joined.StatusId = Convert.ToInt32(ddlLevels.SelectedValue);
                    //ext.Enabled = true;
                    //ext.Show();
                }
                else if (ddlLevels.SelectedItem.Text == "Rejected")
                {
                    btnRemove_Click(sender, e);
                }
                else
                {
                    Facade.MemberJobCart_MoveToNextLevel((hdncurretn.Value != string.Empty ? Convert.ToInt32(hdncurretn.Value) : 0), base.CurrentMember.Id, txtSelectedIds.Text, CurrentJobPosting.Id, ddlLevels.SelectedValue);
                    Facade.UpdateCandidateRequisitionStatus(CurrentJobPosting.Id, txtSelectedIds.Text, CurrentMember.Id, Convert.ToInt32(ddlLevels.SelectedValue));
                    MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, CurrentJobPostingId, txtSelectedIds.Text, CurrentMember.Id, ddlLevels.SelectedItem.Text, Facade);

                    MiscUtil.ShowMessage(lblMessage, "Successfully moved " + getNumberOfSelectedCandidates() + " candidate(s) to " + MiscUtil.RemoveScript(ddlLevels.SelectedItem.Text), false);
                    txtSelectedIds.Text = "";
                    BindList();


                    RefreshCandidateCountinLeftPanel();
                }
            }
            else MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);

        }

        //protected void btnRemoveFromThisLevel_Click(object sender, EventArgs e)
        //{
        //    string applicantIds = txtSelectedIds.Text;
        //    if (!string.IsNullOrEmpty(applicantIds))
        //    {
        //        Facade.MemberJobCart_MoveToNextLevel(HiringMatrixInterviewLevel, base.CurrentMember.Id, txtSelectedIds.Text, CurrentJobPosting.Id, ("Previous"));
        //        BindList();
        //        MiscUtil.ShowMessage(lblMessage, "Successfully moved " + getNumberOfSelectedCandidates() + " candidate(s) to " + hdnPreviousLevelName .Value , false);
        //    }
        //    else MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);
        //    txtSelectedIds.Text = "";
        //}

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            hdnMoving.Value = ddlLevels.SelectedValue;// list.SelectedValue;
            //hdnRemove.Value = "Remove";
            hdncurretn.Value = ddlLevels.SelectedValue;
            string applicantIds = txtSelectedIds.Text;
            if (!string.IsNullOrEmpty(applicantIds))
            {
                uclConfirm.AddMessage("Are you sure that you want to reject this candidate(s)  from the Hiring Matrix?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
            }
            else MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);
        }

        protected void btnSubmitToClient_Click(object sender, EventArgs e)
        {
            SubmitToClient(txtSelectedIds.Text);
        }
        public EventLogForRequisitionAndCandidate BuildEventLog(int candidateid, int jobPostingId)
        {
            EventLogForRequisitionAndCandidate Log = new EventLogForRequisitionAndCandidate();

            Log.ActionType = Facade.GetMemberNameById(candidateid) + " Removed from Hiring Matrix";
            Log.CandidateId = candidateid;
            Log.CreatorId = CurrentMember.Id;
            Log.JobPostingID = JobId = jobPostingId;

            return Log;

        }
        public void RemoveCandidateFromHiringMatrix(string candidateids)
        {
            string[] Ids = candidateids.TrimEnd(',').Split(new string[] { "," }, StringSplitOptions.None);
            int jobPostingid = CurrentJobPostingId;
            foreach (string id in Ids)
            {
                MemberJobCart jobCart = Facade.GetMemberJobCartByMemberIdAndJobPostringId(Convert.ToInt32(id), jobPostingid);
                MemberHiringDetails hiringDetail = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(Convert.ToInt32(id), jobPostingid);
                MemberJoiningDetail joiningDetail = Facade.GetMemberJoiningDetailsByMemberIdAndJobPostingID(Convert.ToInt32(id), jobPostingid);
                IList<Interview> interviews = Facade.GetInterviewByMemberIdAndJobPostingID(Convert.ToInt32(id), jobPostingid);
                if (jobCart != null)
                {
                    Facade.DeleteMemberJobCartById(jobCart.Id);
                }
                if (hiringDetail != null)
                {
                    Facade.DeleteMemberHiringDetailsByID(hiringDetail.Id);
                }
                if (joiningDetail != null)
                {
                    Facade.DeleteMemberJoiningDetailsByID(joiningDetail.Id);
                }
                if (interviews != null)
                {
                    foreach (Interview interview in interviews)
                    {
                        Facade.DeleteInterviewById(interview.Id);
                    }
                }
                //EventLogForRequisitionAndCandidate log = BuildEventLog(Convert.ToInt32(id), jobPostingid);
                MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.RemoveCandidateFromHiringMatrix, jobPostingid, id, CurrentMember.Id, "", Facade);
                //Facade.AddEventLog(log, id);

            }
            lsvCandidateList.DataBind();
            RefreshCandidateCountinLeftPanel();
            hdnRemove.Value = "";
        }
        protected void btnRemoveCandidate_Click(object sender, EventArgs e)
        {
            //string applicantIds = txtSelectedIds.Text;
            //if (!string.IsNullOrEmpty(applicantIds))
            //{
            //    uclConfirm.AddMessage("Are you sure that you want to reject this candidate(s)  from the Hiring Matrix?", ConfirmationWindow.enmMessageType.Attention);
            //}
            //else MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);

            string confirmValue = hdnrmvCandidate.Value;

            if (confirmValue == "Yes")
            {
                RemoveCandidateFromHiringMatrix(txtSelectedIds.Text);

                MiscUtil.ShowMessage(lblMessage, "Selected Candidate(s) has been Removed successfully from the Hiring Matrix", false);
            }
            else
            {

            }
        }
        #region Listview Event
        protected void lvNavigationItems_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                HiringMatrixLevels node = ((ListViewDataItem)e.Item).DataItem as HiringMatrixLevels;

                if (node != null)
                {
                    LinkButton linkLeftMenuItem = (LinkButton)e.Item.FindControl("lnkLeftMenuItem");
                    HiddenField hnSelectedLevel = (HiddenField)e.Item.FindControl("hnSelectedLevel");
                    HtmlGenericControl spName = (HtmlGenericControl)e.Item.FindControl("spName");
                    HtmlGenericControl spCount = (HtmlGenericControl)e.Item.FindControl("spCount");
                    spName.InnerText = node.Name;
                    if (node.Name == "All")
                    {
                        linkLeftMenuItem.CssClass = "active level";
                        linkLeftMenuItem.Enabled = false;
                    }

                    hnSelectedLevel.Value = node.Id.ToString();
                    spCount.InnerText = "(" + node.Count + ")";
                    hdnCount.Value = hdnCount.Value != string.Empty ? (Convert.ToInt32(hdnCount.Value) + node.Count).ToString() : "0";
                    linkLeftMenuItem.CommandArgument = node.Id.ToString();
                }
            }
        }

        protected void lsvCandidateList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {

                TPS360.Common.BusinessEntities.HiringMatrix hiringMatrix = ((ListViewDataItem)e.Item).DataItem as TPS360.Common.BusinessEntities.HiringMatrix;


                HyperLink hlnCandidateName = (HyperLink)e.Item.FindControl("hlnCandidateName");
                ControlHelper.SetHyperLink(hlnCandidateName, UrlConstants.Candidate.OVERVIEW, string.Empty, hiringMatrix.FirstName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(hiringMatrix.MemberId), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.OVERVIEW_SITEMAP_ID, UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.OVERVIEW_SITEMAP_PARENTID, UrlConstants.PARAM_JOB_ID, CurrentJobPostingId.ToString());  //1.2
                Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
                Label lblsource = (Label)e.Item.FindControl("lblsource"); //added by pravin khot on 23/June/2016
                //Binding PrimaryEmail

                lblCandidateID.Text = "A" + hiringMatrix.MemberId.ToString();
                try
                {

                    lblsource.Text = hiringMatrix.SourceName.ToString();
                }
                catch
                {
                    lblsource.Text = "";
                }
         
                HiddenField hfCandidateId = (HiddenField)e.Item.FindControl("hfCandidateId");
                hfCandidateId.Value = hiringMatrix.MemberId.ToString();
                HiddenField hfstatuschangeId = (HiddenField)e.Item.FindControl("hfstatuschangeId");
                HiddenField hdnCurrentlevelId = (HiddenField)e.Item.FindControl("hdnCurrentlevelId");
                hdnCurrentlevelId.Value = hiringMatrix.CurrentLevel.ToString();
                DropDownList ddlStatus = (DropDownList)e.Item.FindControl("ddlStatus");

                if(ddlStatus != null)
                {
                    ScriptManager.GetCurrent(Page).RegisterPostBackControl(ddlStatus);
                }
                ddlStatus.DataValueField = dummyList.DataValueField;
                ddlStatus.DataTextField = dummyList.DataTextField;
                ddlStatus.DataSource = dummyList.DataSource;
                ddlStatus.DataBind();
                ddlStatus.Attributes.Add("onchange", "javascript:ModalChanged(this,'" + hiringMatrix.MemberId + "');");

                Label lblReqTitle = (Label)e.Item.FindControl("lblReqTitle");

                lblReqTitle.Text = hiringMatrix.TotalExperienceYears + " [" + hiringMatrix.CurrentCity + ']'; //HighLightText(hiringMatrix.CurrentPosition, CurrentJobPosting.JobTitle);
                lblReqTitle.ToolTip = hiringMatrix.TotalExperienceYears + " [" + hiringMatrix.CurrentCity + ']';
                Label lblCurrentPosition = (Label)e.Item.FindControl("lblCurrentPosition");
                lblCurrentPosition.Text = hiringMatrix.CurrentPosition;
                int JOBPOSTINGID = Convert.ToInt32(hiringMatrix.PrimaryEmail);
                //Code added by Sumit Sonawane on 1/Mar/2017 ***********
                HyperLink lnkChangeHiringStatus = (HyperLink)e.Item.FindControl("lnkChangeHiringStatus");
                if (lnkChangeHiringStatus != null)
                {
                    SecureUrl url = UrlHelper.BuildSecureUrl("../Modals/ChangeHiringStatus.aspx", string.Empty, UrlConstants.PARAM_MEMBER_ID, hiringMatrix.MemberId.ToString(), UrlConstants.PARAM_JOB_ID, JOBPOSTINGID.ToString(), UrlConstants.PARAM_SITEMAP_ID, "403");
                    //lnkSchedule.NavigateUrl = url.ToString();
                    lnkChangeHiringStatus.Attributes.Add("onclick", "EditModal('" + url.ToString() + "','778px','570px'); return false;");
                }
                //isAccess = false;

                if (isAccess)
                    lnkChangeHiringStatus.Visible = true;
                else
                    lnkChangeHiringStatus.Visible = true;
                //***************************END********************              
            }
        }
        protected void lsvCandidateList_PreRender(object sender, EventArgs e)
        {
            try
            {
                ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }
                    HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                    if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "HiringMatrixRowPerPage";
                }
                
                PlaceUpDownArrow();
              
                if (lsvCandidateList.Controls.Count == 0)
                {
                    lsvCandidateList.DataSource = null;
                    lsvCandidateList.DataBind();
                }
                HtmlTable tblEmptyData = (HtmlTable)lsvCandidateList.Controls[0].FindControl("tblEmptyData");
                dvbulkAction.Visible = tblEmptyData == null;
                var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
                var submit = webConfig.AppSettings.Settings["Submission"];
                var offer = webConfig.AppSettings.Settings["Offer"];
                var join = webConfig.AppSettings.Settings["Join"];
                var interview = webConfig.AppSettings.Settings["Interview"];
                foreach (ListViewDataItem item in lsvCandidateList.Items)
                {
                    DropDownList ddlStatus = (DropDownList)item.FindControl("ddlStatus");
                    HiddenField hdnCurrentlevelId = (HiddenField)item.FindControl("hdnCurrentlevelId");
                    HiddenField hdnSelectedIndex = (HiddenField)item.FindControl("hdnSelectedIndex");

                    Label lblStatus = (Label)item.FindControl("lblStatus");
                    HiringMatrixLevels hml = Facade.GetHiringMatrixLevelsById(Convert.ToInt32(hdnCurrentlevelId.Value));
                    lblStatus.Text = hml.Name;

                    ControlHelper.SelectListByValue(ddlStatus, hdnCurrentlevelId.Value);
                    hdnSelectedIndex.Value = ddlStatus.SelectedIndex.ToString();
                    string apppath = "";
                    if (isMainApplication) apppath = "Common";
                    foreach (System.Web.UI.WebControls.ListItem list in ddlStatus.Items)
                    {
                        if (list.Text == submit.Value)
                        {
                            if (!list.Value.Contains("StatusId")) list.Value = UrlConstants.ApplicationBaseUrl + "Modals/SubmissionDetails.aspx?StatusId=" + list.Value + "&JID=" + CurrentJobPostingId;
                        }
                        else if (list.Text == offer.Value)
                        {
                            if (!list.Value.Contains("StatusId")) list.Value = UrlConstants.ApplicationBaseUrl + "Modals/" + apppath + "HiringDetails.aspx?StatusId=" + list.Value + "&JID=" + CurrentJobPostingId;
                        }
                        else if (list.Text == join.Value)
                        {
                            if (!list.Value.Contains("StatusId")) list.Value = UrlConstants.ApplicationBaseUrl + "Modals/" + apppath + "JoiningDetails.aspx?StatusId=" + list.Value + "&JID=" + CurrentJobPostingId;
                        }
                        else if (list.Text == interview.Value)
                        {
                            if (!list.Value.Contains("StatusId")) list.Value = UrlConstants.ApplicationBaseUrl + "Modals/ScheduleInterview.aspx?StatusId=" + list.Value + "&JID=" + CurrentJobPostingId;
                        }

                        else if (list.Text == "Rejected")
                        {
                            if (!list.Value.Contains("StatusId")) list.Value = UrlConstants.ApplicationBaseUrl + "Modals/RejectionDetails.aspx?StatusId=" + list.Value + "&JID=" + CurrentJobPostingId;
                        }
                        else if (list.Text == "Offer Decline" && !isMainApplication)
                        {
                            if (!list.Value.Contains("StatusId")) list.Value = UrlConstants.ApplicationBaseUrl + "Modals/OfferRejected.aspx?StatusId=" + list.Value + "&JID=" + CurrentJobPostingId;
                        }
                        else
                        {
                        }
                    }

                    if (!isMainApplication)
                    {
                        string CurrentInterviewLevelId = hdnCurrentlevelId.Value;

                    }
                    CheckBox chkCandidate = (CheckBox)item.FindControl("chkCandidate");
                    chkCandidate.Enabled = ddlStatus.SelectedItem.Text != "Rejected";
                }
            }
            catch
            {
            }
        }      
        public string GetSalaryDetailTable() 
        {
            try 
            {
                StringBuilder table = new StringBuilder();
                table.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");
                table.Append("<tr style=''><th>ANNEXURE</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th><th>&nbsp;</th></tr>");
                table.Append("</table>");
                return table.ToString();
            }
            catch { }
            return "";
        }
        private static void SetWorkbookProperties(ExcelPackage p, string Title)
        {
            p.Workbook.Properties.Title = Title;
            
        }
        private static ExcelWorksheet CreateSheet(ExcelPackage p, string sheetName)
        {
            p.Workbook.Worksheets.Add(sheetName);
            ExcelWorksheet ws = p.Workbook.Worksheets[1];
            ws.Name = sheetName; //Setting Sheet's name
            ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
            ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet

            return ws;
        }
        private static DataTable CreateDataTable(int ColumnCount, int RowCount)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < ColumnCount; i++)
            {
                dt.Columns.Add(i.ToString());
                
            }

            for (int i = 0; i < RowCount; i++)
            {
                DataRow dr = dt.NewRow();
                foreach (DataColumn dc in dt.Columns)
                {
                    dr[dc.ToString()] = i;
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
        public string UploadDocument(int MemberId, string Filename, byte[] output,string Title)
        {
            string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, MemberId, Filename, "Compensation Annexure", false);

            //System.IO.Stream _Stream = new System.IO.Stream();
            IList<GenericLookup> gen = Facade.GetAllGenericLookup();

            var lookupId = (from gId in gen
                            where gId.Name == "Compensation Annexure"
                            select gId.Id).SingleOrDefault();

            BinaryWriter Writer = null;
            try
            {
                Writer = new BinaryWriter(File.OpenWrite(strFilePath));              
                Writer.Write(output);
                Writer.Flush();
                Writer.Close();

                if (File.Exists(strFilePath)) 
                {
                    MemberDocument memberDocument = Facade.GetMemberDocumentByMemberIdTypeAndFileName(MemberId, "Compensation Annexure", Filename);
                    
                    if (memberDocument == null)
                    {
                        MemberDocument newDoc = new MemberDocument();
                        newDoc.FileName = Filename;//1.6
                        newDoc.FileTypeLookupId = Convert.ToInt32(lookupId);
                        newDoc.Description = "Compensation Annexure";
                        newDoc.MemberId = MemberId;
                        newDoc.Title = Title;
                        if (newDoc.Title.ToString() != string.Empty)
                        {
                            Facade.AddMemberDocument(newDoc); 
                        }
                    }
                    else
                    {
                        
                    }
                }
            }
            catch
            {
                //...
                //return false;
            }

            return strFilePath;

        }
        public string UploadDocument(int MemberId, string Filename, StringBuilder output, string Title)
        {
            string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, MemberId, Filename, "Offer Letter", false);

            //System.IO.Stream _Stream = new System.IO.Stream();
            IList<GenericLookup> gen = Facade.GetAllGenericLookup();

            var lookupId = (from gId in gen
                            where gId.Name == "Offer Letter"
                            select gId.Id).SingleOrDefault();

            try
            {
                //string imgPath=HttpContext.Current.Server.MapPath("../Images/CandidateImages/pdf_background.jpg");
                PdfSharp.Pdf.PdfDocument document = new PdfSharp.Pdf.PdfDocument();
                document.Info.Title = Filename;

                PdfSharp.Pdf.PdfPage page = document.AddPage();

                XFont font = new XFont("Calibriz", 12, XFontStyle.BoldItalic);
                document.Save(strFilePath);
                PdfConverter pdfConverter = new PdfConverter();
                //pdfConverter.PdfDocumentOptions.LeftMargin = 105;
                //pdfConverter.PdfDocumentOptions.RightMargin = 25;
                //pdfConverter.PdfDocumentOptions.TopMargin = 60;
                //pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] arrBytData = pdfConverter.GetPdfBytesFromHtmlStringWithTempFile(output.ToString(),"");
                System.IO.File.WriteAllBytes(strFilePath, arrBytData);
                if (File.Exists(strFilePath))
                {
                    MemberDocument memberDocument = Facade.GetMemberDocumentByMemberIdTypeAndFileName(MemberId, "Offer Letter", Filename);

                    if (memberDocument == null)
                    {
                        MemberDocument newDoc = new MemberDocument();
                        newDoc.FileName = Filename;//1.6
                        newDoc.FileTypeLookupId = Convert.ToInt32(lookupId);
                        newDoc.Description = "Offer Letter";
                        newDoc.MemberId = MemberId;
                        newDoc.Title = Title;
                        if (newDoc.Title.ToString() != string.Empty)
                        {
                            Facade.AddMemberDocument(newDoc);
                        }
                    }
                    else
                    {

                    }
                }
            }
            catch
            {
                //...
                //return false;
            }

            return strFilePath;

        }
        public void SetIntialData(ExcelWorksheet ws, MemberHiringDetails details)
        {


            ws.Cells["C2:C6"].Style.Border.Right.Style = ExcelBorderStyle.Medium;
            ws.Cells["B2:B6"].Style.Border.Left.Style = ExcelBorderStyle.Medium;
            ws.Cells["A6:C6"].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
            ws.Cells["A2:C2"].Style.Border.Top.Style = ExcelBorderStyle.Medium;

            string range = "";
            for (int i = 2; i <= 6; i++)
            {
                range = "B" + i.ToString() + ":" + "C" + i.ToString();
                ws.Cells[range].Merge = true;
            }

            ws.Cells["A2"].Value = "Name";
            ws.Cells["A3"].Value = "Designation";
            ws.Cells["A4"].Value = "Band";
            ws.Cells["A5"].Value = "SBU";
            ws.Cells["A6"].Value = "Location";

            //ws.Cells["A2:A6"].Style.Font.Bold = true;
            ws.Cells["A2:A6"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            ws.Cells["A2:A6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


            ws.Cells["B2"].Value = details.CandidateName;
            ws.Cells["B3"].Value = details.Designation;
            ws.Cells["B4"].Value = details.Band;
            ws.Cells["B5"].Value = details.BU;
            ws.Cells["B6"].Value = details.Location;

        }

        public void SetSalaryComponents(ExcelWorksheet ws, MemberHiringDetails details) 
        {
            

            ws.Cells["C8:C42"].Style.Border.Right.Style = ExcelBorderStyle.Medium;
            ws.Cells["C8:C42"].Style.Border.Left.Style = ExcelBorderStyle.Medium;
            ws.Cells["B8:B42"].Style.Border.Left.Style = ExcelBorderStyle.Medium;
            ws.Cells["A42:C42"].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
            ws.Cells["A8:C8"].Style.Border.Top.Style = ExcelBorderStyle.Medium;

            ws.Cells["A8"].Value = "Components";
            ws.Cells["A8"].Style.Font.Bold = true;

            ws.Cells["B8"].Value = "PER MONTH";
            ws.Cells["B8"].Style.Font.Bold = true;

            ws.Cells["C8"].Value = "PER ANNUM";
            ws.Cells["C8"].Style.Font.Bold = true;

            ws.Cells["A9"].Value = "BASIC (40% of Gross Salary)";
            ws.Cells["B9"].Value = details.BasicEmbedded;
            ws.Cells["C9"].Formula = "=B9*12";

            ws.Cells["A10"].Value = "HRA (50% of BASIC)";
            ws.Cells["B10"].Value = details.HRAEmbedded;
            ws.Cells["C10"].Formula = "=B10*12";

            ws.Cells["A11"].Value = "Transport Allowance (Fixed)";
            ws.Cells["B11"].Value = details.Conveyance;
            ws.Cells["C11"].Formula = "=B11*12";

            ws.Cells["A12"].Value = "Special Allowance (Remaining amount)";
            ws.Cells["B12"].Value = details.SpecialAllowance;
            ws.Cells["C12"].Formula = "=B12*12";

            ws.Cells["A13"].Value = "Medical Allowance (20% of BASIC)";
            ws.Cells["B13"].Value = details.Medical;
            ws.Cells["C13"].Formula = "=B13*12";

            ws.Cells["A14"].Value = "Role Allowance";
            ws.Cells["B14"].Value = details.RoleAllowance;
            ws.Cells["C14"].Formula = "=B14*12";

            ws.Cells["A15"].Value = "Site Allowance";
            ws.Cells["B15"].Value = details.SiteAllowance;
            ws.Cells["C15"].Formula = "=B15*12";

            ws.Cells["A16"].Value = "Retention Bonus*";
            ws.Cells["B16"].Value = details.RetentionBonus;
            ws.Cells["C16"].Formula = "=B16*12";

            ws.Cells["A17"].Value = "Relocation Allowance";
            ws.Cells["B17"].Value = details.RelocationAllowanceForCom;
            ws.Cells["C17"].Formula = "=B17*12";

            ws.Cells["A18"].Value = "Reimbursement";
            ws.Cells["B18"].Value = details.Reimbursement;
            ws.Cells["C18"].Formula = "=B18*12";


            ws.Cells["A20"].Value = "Gross Salary (A)";
            ws.Cells["A20"].Style.Font.Bold = true;
            ws.Cells["B20"].Formula = "=SUM(B9:B18)";
            ws.Cells["B20"].Style.Font.Bold = true;
            ws.Cells["C20"].Formula = "=B20*12";
            ws.Cells["C20"].Style.Font.Bold = true;

            ws.Cells["A22"].Value = "Member  Contribution";
            ws.Cells["A22"].Style.Font.Bold = true;
            
            ws.Cells["A23"].Value = "PF";
            ws.Cells["B23"].Value = details.PFEmbedded;
            ws.Cells["C23"].Formula = "=B23*12";

            ws.Cells["A24"].Value = "ESIC";
            ws.Cells["B24"].Value = details.ESIC;
            ws.Cells["C24"].Formula = "=B24*12";

            ws.Cells["A25"].Value = "Take Home";
            ws.Cells["A25"].Style.Font.Bold = true;
            ws.Cells["B25"].Formula = "=B20-B23";
            ws.Cells["B25"].Style.Font.Bold = true;
            ws.Cells["C25"].Formula = "=B25*12";
            ws.Cells["C25"].Style.Font.Bold = true;

            ws.Cells["D25"].Value = "** Subject to tds";

            ws.Cells["A27"].Value = "Company Contribution";
            ws.Cells["A27"].Style.Font.Bold = true;

            ws.Cells["A28"].Value = "PF";
            ws.Cells["B28"].Value = details.PFEmbedded;
            ws.Cells["C28"].Formula = "=B28*12";

            ws.Cells["A29"].Value = "ESIC";
            ws.Cells["B29"].Value = details.ESIC;
            ws.Cells["C29"].Formula = "=B29*12";

            ws.Cells["A30"].Value = "Gratuity";
            ws.Cells["B30"].Value = details.GratuityEmbedded;
            ws.Cells["C30"].Formula = "=B30*12";

            ws.Cells["A31"].Value = "Bonus";
            ws.Cells["B31"].Value = details.Bonus;
            ws.Cells["C31"].Formula = "=B31*12";

            ws.Cells["A33"].Value = "Total (B)";
            ws.Cells["A33"].Style.Font.Bold = true;
            ws.Cells["B33"].Formula = "=SUBTOTAL(9,B28:B31)";
            ws.Cells["B33"].Style.Font.Bold = true;
            ws.Cells["C33"].Formula = "=B33*12";
            ws.Cells["C33"].Style.Font.Bold = true;

            ws.Cells["A35"].Value = "Total Fixed ( A+B )";
            ws.Cells["A35"].Style.Font.Bold = true;
            ws.Cells["B35"].Formula = "=B20+B33";
            ws.Cells["B35"].Style.Font.Bold = true;
            ws.Cells["C35"].Formula = "=B35*12";
            ws.Cells["C35"].Style.Font.Bold = true;

            ws.Cells["A37"].Value = "Performance Linked Incentive (C) **";
            ws.Cells["A37"].Style.Font.Bold = true;
            ws.Cells["B37"].Value = details.PerformanceLinkedIncentive;
            ws.Cells["B37"].Style.Font.Bold = true;
            ws.Cells["C37"].Formula = "=B37*12";
            ws.Cells["C37"].Style.Font.Bold = true;

            ws.Cells["A39"].Value = "Sales Incentive (D)***";
            ws.Cells["A39"].Style.Font.Bold = true;
            ws.Cells["B39"].Value = details.SalesIncentive;
            ws.Cells["C39"].Formula = "=B39*12";

            ws.Cells["D39"].Value = "**As per the scheme, calculated on 15X";
            //ws.Cells["D39"].Style.VerticalAlignment = ExcelVerticalAlignment.Le;
            ws.Cells["D39"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            
            
            
            ws.Cells["A40"].Value = "Total Variable (E)";
            ws.Cells["A40"].Style.Font.Bold = true;
            ws.Cells["B40"].Formula = "=B37+B39";
            ws.Cells["B40"].Style.Font.Bold = true;
            ws.Cells["C40"].Formula = "=B40*12";
            ws.Cells["C40"].Style.Font.Bold = true;

            ws.Cells["A42"].Value = "Total CTC = (A+B+E)";
            ws.Cells["A42"].Style.Font.Bold = true;
            ws.Cells["B42"].Formula = "=B25+B33+B40";
            ws.Cells["B42"].Style.Font.Bold = true;
            ws.Cells["C42"].Formula = "=B42*12";
            ws.Cells["C42"].Style.Font.Bold = true;

            ws.Cells["A44"].Value = "* Retention bonus shall be disbursed after 6 months which is applicable only for the first year from the D.O.J";
            ws.Cells["A45"].Value = "**  Performance Linked Incentive will be disbursed quarterly based on the individual & SBU performance.";
            ws.Cells["A46"].Value = "*** Sales Incentives are calculated as per 15X & will be paid quarterly based on the defined target achievement and as per the scheme.";
            ws.Cells["A47"].Value = "*#*  PF, ESIC, Gratuity, Bonus , PT & TDS are applicable as per respective acts.";
            
            ws.Cells["A44:A47"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            ws.Cells["A49:A54"].Style.Font.Color.SetColor(System.Drawing.Color.Blue);
            ws.Cells["A49:A54"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells["A49"].Value = "Other Benefits";
            ws.Cells["A49"].Style.Font.Bold = true;
            ws.Cells["A49"].Style.Font.UnderLine = true;

            ws.Cells["A50"].Value = "1. Mobile Connection - A corporate connection shall be provided with the defined limit & it shall be increased or decreased based on the usage & business need.";
            ws.Cells["A51"].Value = "2. Individual Health Insurance cover of Rs.2,00,000 & Accident Insurance Cover of Rs. 2,00,000.";
            ws.Cells["A52"].Value = "3. Gratuity Insurance cover as on date  Rs. 2,54,748.";
            ws.Cells["A53"].Value = "4. Sodexo Meal Coupons can be availed for the Tax Exemption.";
            ws.Cells["A54"].Value = "5. Conveyence reimbursement as per actuals and company policy in force.";
        
        }
        public void DownloadFile(string filePath, int memberId, string FileName)
        {
            try
            {
                string baseUrl = UrlConstants.ApplicationBaseUrl;

                string fullpath = "~/Resources/Member/" + memberId + "/" + "Compensation Annexure/" + FileName;

                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                response.Buffer = true;
                response.AddHeader("Content-Disposition", "attachment;filename=\"" + FileName + "\"");
                byte[] data = req.DownloadData(Server.MapPath(fullpath));
                response.BinaryWrite(data);
                response.End();

               
            }
            catch (Exception ex)
            {

            }

        }
        public string GenerateSalaryCompensation(int jobpostingId, int MemberId)
        {
            MemberHiringDetails details = Facade.GetSalaryComponentsByJobpostingIdAndMemberId(jobpostingId, MemberId);
            MemberHiringDetails memberHiringDetails = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(MemberId, jobpostingId);
            Member mem = Facade.GetMemberById(MemberId);
            //int Byear=0, Jyear=0;
            //if(mem.DateOfBirth!=null)
            // Byear = mem.DateOfBirth.Year;
            //if (details.JoiningDate != null)
            //    Jyear = memberHiringDetails.JoiningDate.Year;
            int Age = 0;
            if (memberHiringDetails.JoiningDate > mem.DateOfBirth)
                Age = Convert.ToInt32((memberHiringDetails.JoiningDate - mem.DateOfBirth).TotalDays / 365);
            string fullpath = "";
            StringBuilder reqReport = new StringBuilder();
            if (details != null)
            {
               
                    using (ExcelPackage pck = new ExcelPackage()) 
                    {
                        
                        string CandidateName = details.CandidateName.Replace(" ", "_");
                        string JobTitle = CurrentJobPosting.JobTitle.Replace(" ", "_");
                        if(JobTitle.Contains("amp;_"))
                            JobTitle=JobTitle.Replace("amp;_","_");
                        //string Filename = "Compensation_Annexure1-" + CandidateName + "-" + JobTitle + ".xlsx";
                        string Filename = "Compensation_Annexure1-" + CandidateName + "-" + JobTitle + ".pdf";
                        string Title = "Compensation_Annexure";
                        //reqReport.Append("<table background='" + UrlConstants.ApplicationBaseUrl + "Images/CandidateImages/new2.jpg'; >");
                        reqReport.Append("<table>");
                        reqReport.Append("<tr ><td height='150' width='180'></td><td></td></tr>");
                        reqReport.Append("<tr><td></td><td cellpadding='2px'>");
                        reqReport.Append("<table style='font-family : calibriz; font-size:20px; width:100%; border-collapse:collapse; border-spacing: 0px;' align='left' border = '0' bordercolor='#000000'  cellspacing='0' cellpadding='5'>");
                        reqReport.Append("    <tr>");
                        reqReport.Append(" <td colspan=5 align=center  height='25'><b>ANNEXURE</b></td>");
                        reqReport.Append("    </tr>");
                        CreateBasicDetails(reqReport, "<b>Name</b>", details.CandidateName);
                        CreateBasicDetails(reqReport, "Designation", details.Designation);
                        //CreateBasicDetailsD(reqReport, "Band", details.Band,"<b>For Spl Allowance</b>","<Spl>");
                        reqReport.Append("    <tr>");
                        reqReport.Append(" <td align=left height='25'>Band</td>");
                        reqReport.Append(" <td align=left height='25'>" + details.Band + "</td>");
                        reqReport.Append("<td colspan=5> </td>");
                        //reqReport.Append(" <td align=left height='25'><b>For Spl Allowance</b></td>");
                        //reqReport.Append(" <td align=right height='25'width='70'> "+Convert.ToDouble(details.SpecialAllowance)+" </td>");
                        reqReport.Append("    </tr>");
                        CreateBasicDetails(reqReport, "SBU", details.BU);
                        CreateBasicDetails(reqReport, "Location", details.Location);
                        reqReport.Append("    <tr height='25'></tr>");
                        reqReport.Append("<tr><td colspan=5  height='25'></td></tr>");
                        reqReport.Append("    <tr>");
                        reqReport.Append(" <td align=left height='25'><b>Particulars</b></td>");
                        reqReport.Append(" <td align=center height='25'><b>Monthly Components IN INR</b></td>");
                        reqReport.Append(" <td align=center height='25'><b>Yearly Components IN INR</b></td>");
                        reqReport.Append("<td/>");
                        reqReport.Append("<td/>");
                        reqReport.Append("    </tr>");
                        reqReport.Append("    <tr>");
                        reqReport.Append(" <td align=left height='25' colspan=5><b>PART A - Salary</b></td>");
                        reqReport.Append("    </tr>");
                        double Basic = Convert.ToDouble(details.OfferedSalary) * 0.40;
                        CreateData(reqReport, "BASIC (40% of Gross CTC)", Math.Round(Basic, 2));
                        //CreateData(reqReport, "BASIC (40% of Gross CTC)", Convert.ToDouble(details.BasicEmbedded));
                        double HRA = Basic * 0.50;
                        CreateData(reqReport, "HRA (50% of BASIC)", Math.Round(HRA, 2));
                        //CreateData(reqReport, "HRA (50% of BASIC)", Convert.ToDouble(details.HRAEmbedded));
                        CreateData(reqReport, "Transport Allowance (Fixed)", Convert.ToDouble(details.Conveyance));
                        //double FBP = Convert.ToDouble(details.OfferedSalary) - (Basic + HRA + Convert.ToDouble(details.Conveyance));
                        //CreateData(reqReport, "Flexi Benefits Plan (FBP)", Math.Round(FBP,2));
                        ////CreateData(reqReport, "Flexi Benefits Plan (FBP)", Convert.ToDouble(details.FlexiPay1));
                        //double GS = Basic+ HRA + Convert.ToDouble(details.Conveyance + details.FlexiPay1);
                        //CreateData(reqReport, "<b>Total Gross Salary<b>", Math.Round(GS,2));
                        //reqReport.Append("    <tr>");
                        //reqReport.Append("<tr><td colspan=5  height='25'></td></tr>");
                        //reqReport.Append(" <td align=left height='25' colspan=5><b>PART B - Benefits</b></td>");
                        //reqReport.Append("    </tr>");
                        double PF;
                        if (Basic > 15000)
                        {
                            PF = Convert.ToDouble(15000 * 0.12);
                        }
                        else
                        {
                            PF = Convert.ToDouble(Basic * 0.12);
                        }
                        double FBP = Convert.ToDouble(details.OfferedSalary) - (Basic + HRA + Convert.ToDouble(details.Conveyance)+PF);
                        CreateData(reqReport, "Flexi Benefits Plan (FBP)", FBP);
                        //CreateData(reqReport, "Flexi Benefits Plan (FBP)", Convert.ToDouble(details.FlexiPay1));
                        string G = (Basic + HRA + Convert.ToDouble(details.Conveyance + details.FlexiPay1)).ToString("N2");
                        double GS = Convert.ToDouble(G);
                        CreateData(reqReport, "<b>Total Gross Salary</b>", GS);
                        reqReport.Append("    <tr>");
                        reqReport.Append("<tr><td colspan=5  height='25'></td></tr>");
                        reqReport.Append(" <td align=left height='25' colspan=5><b>PART B - Benefits</b></td>");
                        reqReport.Append("    </tr>");
                        CreateData(reqReport, "Employer's Contribution to PF", Math.Round(PF, 2));
                        //CreateData(reqReport, "Employer's Contribution to PF", Convert.ToDouble(details.PFEmbedded));
                        double ESIC;
                        if (GS <= 15000)
                        {
                            ESIC = Convert.ToDouble(GS * 0.0175);
                        }
                        else
                        {
                            ESIC = 0;
                        }
                        CreateData(reqReport, "Employer's Contribution to ESIC", ESIC);
                        //CreateData(reqReport, "Employer's Contribution to ESIC", Convert.ToDouble(details.ESIC));
                        double Gratuity=Convert.ToDouble((Basic / 26)*(15.0 / 12.0));
                        CreateData(reqReport, "Gratuity", Gratuity);
                        //CreateData(reqReport, "Gratuity", Convert.ToDouble(details.GratuityEmbedded));
                        CreateData(reqReport, "Health & Personal Accidental Insurance", Convert.ToDouble(details.Medical));
                        double Statutory;
                        if (Basic <= 10000)
                        {
                            if (HRA * 0.0833 > 3500 / 12)
                            {
                                Statutory = Convert.ToDouble(3500 / 12);
                            }
                            else
                            {
                                Statutory = Convert.ToDouble(HRA * 0.0833 / 12);
                            }
                        }
                        else
                            Statutory = Convert.ToDouble(0);
                        CreateData(reqReport, "Statutory Bonus", Statutory);
                        string b = (PF + ESIC + Gratuity + Convert.ToDouble(details.Medical) + Statutory).ToString("N2");
                        double Benefits = Convert.ToDouble(b);
                        CreateData(reqReport, "<b>Total Benefits</b>", Benefits);
                        reqReport.Append("    <tr>");
                        reqReport.Append("<tr><td colspan=5  height='25'></td></tr>");
                        reqReport.Append(" <td align=left height='25' colspan=5><b>PART C - Variable Pay/Incentive</b></td>");
                        reqReport.Append("    </tr>");
                        CreateData(reqReport, "Performance Link Pay", Convert.ToDouble(details.PerformanceLinkedIncentive));
                        CreateData(reqReport, "Sales Incentive", Convert.ToDouble(details.SalesIncentive));
                        double totalvariable = Convert.ToDouble( details.PerformanceLinkedIncentive + details.SalesIncentive);
                        CreateData(reqReport, "<b>Total Variable Pay/ Incentive</b>", totalvariable);
                        CreateData(reqReport, "<b>Cost to Company (A+B+C)</b>", (GS+Benefits+totalvariable));
                        reqReport.Append("    </table>");
                        reqReport.Append("</td></tr>");
                        reqReport.Append("<tr><td colspan=5  height='550'></td></tr>");
                        reqReport.Append("<tr><td></td><td colspan=5 style='font-family : calibriz; font-size:20px;'>");
                        //reqReport.Append("</p> * Retention bonus shall be disbursed after 6 months which is applicable only for the first year from the D.O.J");
                        reqReport.Append("</p> **  Performance Linked Incentive will be disbursed quarterly based on the individual & SBU performance.");
                        reqReport.Append("</p> *** Sales Incentives are calculated as per 21X productivity & will be paid quarterly based on the defined target achievement and as per the scheme.");
                        reqReport.Append("</p> *#*  PF, ESIC, Gratuity, Bonus , PT & TDS are applicable as per respective acts.");
                        reqReport.Append("</p></p> <b>Other Benefits</b>");
                        reqReport.Append("</p> 1. Mobile Connection - A corporate connection shall be provided with the defined limit & it shall be increased or decreased based on the usage & business need.");
                        reqReport.Append("</p> 2. Individual Health Insurance cover of Rs.2,00,000 & Accident Insurance Cover of Rs. 2,00,000.");
                        int G1 = Convert.ToInt32(Gratuity * 12);
                        int value = G1 * (58 - Age);
                        NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
                        int[] mySizes = { 3, 2, 2 };
                        nfi.NumberGroupSizes = mySizes;
                        string[] GratuityIns = value.ToString("N", nfi).Split('.');
                        reqReport.Append("</p> 3. Gratuity Insurance cover as on date  Rs." + GratuityIns[0] + ".");
                        reqReport.Append("</p> 4. FBP: Please refer enclosed explanation of Compensation Components.");
                        reqReport.Append("</p> 5. Conveyence reimbursement as per actuals and company policy in force.");
                        reqReport.Append("<br/><br/><br/><br/>This is computer generated message and does not require signature.</p>");
                        reqReport.Append("</td></tr>");
                        //reqReport.Append("    <tr>");
                        //reqReport.Append("        <td align=left height='50'><br/><br/>This is computer generated message and does not require signature.</p></td>");
                        //reqReport.Append("    </tr>");
                        reqReport.Append("<tr><td colspan=5  height='700'></td></tr>");
                        reqReport.Append("    </table>");
                        //double SplAllowance = Convert.ToDouble(details.OfferedSalary) - Convert.ToDouble(details.PFEmbedded) - GS;
                        reqReport.Replace("<Spl>", Convert.ToString((Convert.ToDouble(details.OfferedSalary)-PF)-GS));
                        PdfConverter pdfConverter = new PdfConverter();
                        UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                        pdfConverter.PdfDocumentOptions.ShowHeader = false;
                        pdfConverter.PdfDocumentOptions.ShowFooter = false;
                        //pdfConverter.PdfDocumentOptions.LeftMargin = 105;
                        //pdfConverter.PdfDocumentOptions.RightMargin = 25;
                        //pdfConverter.PdfDocumentOptions.TopMargin = 60;
                        //pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                        pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                        pdfConverter.PdfDocumentOptions.ShowHeader = false;
                        pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                        pdfConverter.PdfFooterOptions.FooterTextColor = System.Drawing.Color.Black;
                        pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                        pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                        pdfConverter.PdfFooterOptions.ShowPageNumber = true;
                        pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                        byte[] arrBytData = pdfConverter.GetPdfBytesFromHtmlStringWithTempFile(reqReport.ToString(), "");
                        System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                        response.Clear();
                        response.AddHeader("Content-Type", "binary/octet-stream");
                        response.AddHeader("Content-Disposition", "attachment; filename=" + Filename + "; size=" + arrBytData.Length.ToString());
                        //response.AddHeader("Content-Disposition", "attachment; filename=" + Filename);
                        response.Flush();
                        response.BinaryWrite(arrBytData);
                        response.Flush();
                        response.End();

                        //SetWorkbookProperties(pck, Title);

                        //ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Demo");
                        //DataTable dt = CreateDataTable(3, 5);

                        //ws.Column(1).Width = 35.14;
                        //ws.Column(2).Width = 17.00;
                        //ws.Column(3).Width = 17.23;
                        //ws.Column(4).Width = 24;
                        //ws.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //ws.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        
                        //ws.Cells[1, 1].Value = "ANNEXURE";
                        //ws.Cells[1, 1, 1, 3].Merge = true;
                        //ws.Cells[1, 1, 1, 3].Style.Font.Bold = true;
                        //ws.Cells[1, 1, 1, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //ws.Cells[1, 1, 1, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        //SetIntialData(ws, details);

                        //SetSalaryComponents(ws, details);

                        //Response.Clear();
                        //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        //Response.AddHeader("content-disposition", String.Format(System.Globalization.CultureInfo.InvariantCulture, "attachment; filename={0}", Filename));
                        //Response.BinaryWrite(pck.GetAsByteArray());
                        //Response.End();
                        
                        
                        UploadDocument(MemberId, Filename, reqReport, Title);
                        //DownloadFile(UploadDocument(MemberId, Filename, pck.GetAsByteArray(), Title), MemberId, Filename);
                        



                        fullpath = UrlConstants.ApplicationBaseUrl + "Resources/Member/" + MemberId + "/" + "Compensation Annexure/" + Filename;
                    }

               
                
            }
            return fullpath;
        }

        public void CreateBasicDetails(StringBuilder reqReport, string column, string value)
        {
            reqReport.Append("    <tr>");
            reqReport.Append(" <td align=left height='25'>" + column + "</td>");
            reqReport.Append(" <td align=left height='25'>" + value + "</td>");
            reqReport.Append("    <td/>");
            reqReport.Append("    <td/>");
            reqReport.Append("    <td/>");
            reqReport.Append("    </tr>");
        }
        public void CreateBasicDetailsD(StringBuilder reqReport, string column1, string value1, string column2, string value2)
        {
            reqReport.Append("    <tr>");
            reqReport.Append(" <td align=left height='25'>" + column1 + "</td>");
            reqReport.Append(" <td align=left height='25'>" + value1 + "</td>");
            reqReport.Append("    <td/>");
            reqReport.Append(" <td align=left height='25'>" + column2 + "</td>");
            reqReport.Append(" <td align=right height='25'><b>" + value2 + "<b></td>");
            reqReport.Append("    </tr>");
        }
        public void CreateData(StringBuilder reqReport, string column, double value)
        {
            if (column == "<b>Total Gross Salary</b>" || column == "<b>Total Benefits</b>" || column == "<b>Total Variable Pay/ Incentive</b>" || column == "<b>Cost to Company (A+B+C)</b>")
            {
                double G = value;
                double G1;
                if (value < 1)
                    G1 = 0.00;
                else
                    G1 = value * 12;
                reqReport.Append("    <tr>");
                reqReport.Append(" <td align=left height='25'>" + column + "</td>");
                reqReport.Append(" <td align=right height='25'><b>" + G.ToString("N2") + "</b></td>");
                reqReport.Append(" <td align=right height='25'><b>" + G1.ToString("N2") + "</b></td>");
                reqReport.Append("    <td/>");
                reqReport.Append("    <td/>");
                reqReport.Append("    </tr>");
            }
            else if (column == "Statutory Bonus")
            {
                //int G = Convert.ToInt32(value);
                double G1 = value * 12;
                reqReport.Append("    <tr>");
                reqReport.Append(" <td align=left height='25'>" + column + "</td>");
                reqReport.Append(" <td align=right height='25'>" + value.ToString("N2") + "</td>");
                reqReport.Append(" <td align=right height='25'>" + G1.ToString("N2") + "</td>");
                reqReport.Append("    <td/>");
                reqReport.Append("    <td/>");
                reqReport.Append("    </tr>");
            }
            else
            {
                double G = value;
                double G1;
                if (value < 1)
                    G1 = 0.00;
                else
                    G1 = Convert.ToInt32(value * 12);
                reqReport.Append("    <tr>");
                reqReport.Append(" <td align=left height='25'>" + column + "</td>");
                reqReport.Append(" <td align=right height='25'>" + G.ToString("N2") + "</td>");
                reqReport.Append(" <td align=right height='25'>" + G1.ToString("N2") + "</td>");
                reqReport.Append("    <td/>");
                reqReport.Append("    <td/>");
                reqReport.Append("    </tr>");
            }
        }

        public string GenerateOfferLetter(MemberHiringDetails details, string Filename, int MemberId, MemberHiringDetails details1)
        {

            string fullpath = "";
            if (Filename.Contains("amp;_"))
                Filename = Filename.Replace("amp;_", "_");
            StringBuilder reqReport = new StringBuilder();
            // string date = details.JoiningDate.ToString();
            //string date1 = StringFormat("MMMMMMMM, d yyyy)", date);

            //string LogoPath = UrlConstants.ApplicationBaseUrl + "Images/ezest.jpg"; //line commented by pravin khot on 10/March/2016
            string LogoPath = UrlConstants.ApplicationBaseUrl + "Images/CustomerLogo.png"; //New line added by pravin khot on 10/March/2016

            if (details != null)
            {
                #region Offerletter
                /*reqReport.Append(@"
                                <html
                                xmlns:o='urn:schemas-microsoft-com:office:office'
                                xmlns:w='urn:schemas-microsoft-com:office:word'
                                xmlns='http://www.w3.org/TR/REC-html40'>
                                <head><title></title>
                                <xml>
                                <w:WordDocument>
                                <w:View>Print</w:View>
                                <w:Zoom>100</w:Zoom>
                                <w:DoNotOptimizeForBrowser/>
                                </w:WordDocument>
                                </xml>
                                <style>
                                p.MsoFooter, li.MsoFooter, div.MsoFooter
                                {
                                    margin:0in;
                                    margin-bottom:.0001pt;
                                    mso-pagination:widow-orphan;
                                    tab-stops:center 3.0in right 6.0in;
                                    font-size:12.0pt;
                                }
                                @page Section1
                                {
                                    size:8.5in 11.0in;
                                    margin:0.25in 1.0in 1.0in 0.5in ; //Top,Right,Bottom,Left
                                    mso-header-margin:.5in;
                                    mso-header:h1;
                                    mso-footer: f1;
                                    mso-footer-margin:.5in;
                                }
                                div.Section1 { page:Section1; }
                                </style></head>
                                <body lang=EN-US style='tab-interval:.5in'>
                                <div class=Section1>");*/
                //reqReport.Append("<img src='" + UrlConstants.ApplicationBaseUrl + "Images/ezest.jpg' align='right' />");


                reqReport.Append("<table style='font-family : Calibri ;font-size:18pt; width:100%; border-collapse:collapse; border-spacing: 0px;' align='left' border = '0' bordercolor='#000000'  cellspacing='0' cellpadding='5'  >");
                #region firstpage

                reqReport.Append("<tr><td>");
                // reqReport.Append("<img src='" + UrlConstants.ApplicationBaseUrl + "Images/ezest.jpg' align='right' />");//line commented by pravin khot on 10/March/2016
                reqReport.Append("<img src='" + UrlConstants.ApplicationBaseUrl + "Images/CustomerLogo.png' align='right' />");//New line added by pravin khot on 10/March/2016
                reqReport.Append("</td></tr>");
                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=center style='font-family : Calibri ;font-size:22pt; '><u><b>OFFER LETTER</b></u></td>");
                reqReport.Append("    </tr>");

                string CandidateName1 = details.CandidateName.Replace(" ", "_");

                string[] names = CandidateName1.Split('_');

                string FirstName = names[0];
                string SecondName = names[1];

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left><b>Dear " + FirstName + " " + SecondName + ",</b></p></td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify  >We are pleased to offer you to join e-Zest Solutions Ltd in the position of <b>'" + details1.OfferedPosition.ToString() + "'</b>.</td>");
                reqReport.Append("    </tr>");

                string[] JoiningDatestring = details.JoiningDate.ToLongDateString().Split(',');
                string JoiningDate1 = JoiningDatestring[0];
                string JoiningDate2 = JoiningDatestring[1];


                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify  >Your remuneration (CTC) will be &lsquo;<b>" + Convert.ToDouble((details1.BasicEmbedded + details1.HRAEmbedded + details1.RetentionBonus + details1.LTAEmbedded + details1.PerformanceLinkedIncentive + details1.Medical + details1.SpecialAllowance + details1.Conveyance + details1.PFEmbedded + details1.ESIC + details1.Bonus + details1.RoleAllowance + details1.GratuityEmbedded) * 12).ToString("N") + "</b>  <b>p.a.</b> The details of your salary package are mentioned in the Annexure I. We would like you to join e-Zest on <b>" + JoiningDate2 + "</b>.</td>");
                reqReport.Append("    </tr>");


                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify  >Your continued association with e-Zest is dependent on the successful completion of 3 months&rsquo; probation period. Regular performance reviews and support will be provided to understand your performance and suitability</td>");
                reqReport.Append("    </tr>");


                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify  >The company terms of association are mentioned along with this letter. These terms are reviewed  and informed from time to time by e-Zest.</td>");
                reqReport.Append("    </tr>");

                string[] OfferedDatestring1 = details.OfferedDate.AddDays(1).ToLongDateString().Split(',');
                string OfferedDateaddoneday1 = OfferedDatestring1[0];
                string OfferedDateaddoneday = OfferedDatestring1[1];



                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify  >Please call me if you have any questions or clarifications about the offer. We look forward to your acceptance of the terms and that you would be joining e-Zest on the given date. Please convey your acceptance by <b>" + OfferedDateaddoneday + "</b>.</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left ><i>Terms of our association:</i></td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify ><ol type=\"1\">");
                reqReport.Append("<li style='margin-bottom: 7px;'>The regular office working hours are 9.00 am to 6.30 pm from Monday to Friday.</p></li>");
                reqReport.Append("<li style='margin-bottom: 7px;'>You will be required to give an undertaking on confidentiality and non-competition as per the document given to you separately at the time of joining.</li>");
                reqReport.Append("<li style='margin-bottom: 7px;'>The Company reserves the right to change the different components/allowances in the total emoluments package, at its own discretion, at any time in future.</li>");
                reqReport.Append("<li style='margin-bottom: 7px;'>You shall be due for salary revision based on your performance as per the performance appraisal cycle and the terms of association.</li>");
                reqReport.Append("</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left ><i>Next Step- Joining Procedure:</i></td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify ><ol type=\"1\">");
                reqReport.Append("<li style='margin-bottom: 7px;'>Upon your acceptance of the offer &ldquo;Personal Information Form&rdquo; will be sent, you are requested to fill it completely and send it along with the soft copy of all the necessary documents mentioned in the <b>Annexure II.</b></p></li>");
                reqReport.Append("<li style='margin-bottom: 7px;'>A copy &ldquo;The Spirit of e-Zest&rdquo;, the organizational culture guide, will be provided to you on the day of your joining. Other guidelines will be shared with the welcome e-mail and also during the induction.</li>");
                reqReport.Append("<li style='margin-bottom: 7px;'>On submission of all the necessary documents &ldquo;Appointment Letter&rdquo; will be issued.</li>");
                reqReport.Append("</td>");
                reqReport.Append("    </tr>");


                string[] OfferedDatestring = details.OfferedDate.ToLongDateString().Split(',');
                string OfferedDate1 = OfferedDatestring[0];
                string OfferedDate2 = OfferedDatestring[1];


                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left ><b>For e-Zest Solutions Ltd.</b><br/><b>" + OfferedDate2 + "</b>.</p></td>");
                reqReport.Append("    </tr>");
                reqReport.Append("    <tr>");
                reqReport.Append("    <td height='160px'/>");
                reqReport.Append("    </tr>");
                //reqReport.Append("<tr><td style='margin-left:-20px'><img src='" + UrlConstants.ApplicationBaseUrl + "Images/Ezest-btm.png' align='left' /></td>"); //Line commented by pravin khot on 10/March/2016
                reqReport.Append("    </tr>");

                #endregion

                #region secondpage
                reqReport.Append("    <tr>");
                reqReport.Append("    <td height='160px'/>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=center style='font-family : Calibri ;font-size:22pt; '><br><br><u><b>Annexure I</b><br><br><br></u></td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td>");

                reqReport.Append("<table  style='font-family : Calibri;font-size:18pt;width:80%; border-collapse:collapse; border-spacing: 0px;' align='center' border = '1' bordercolor='#000000'  cellspacing='3' cellpadding='3'>");
                reqReport.Append("    <tr bgcolor='#B0C4DE'>");
                reqReport.Append("    <td align=center><b>COMPONENTS</b></td>");
                reqReport.Append("    <td align=center ><b><b>Monthly (Rs.)</b></b></td>");
                reqReport.Append("    <td align=center><b><b>Annual (Rs.) </b></b></td>");
                reqReport.Append("    </tr>");


                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>Basic Salary</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.BasicEmbedded).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.BasicEmbedded) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>HRA</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.HRAEmbedded).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.HRAEmbedded) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>E.A (Fixed)</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.RetentionBonus).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.RetentionBonus) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>LTC/LTA</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.LTAEmbedded).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.LTAEmbedded) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>Skill Allowance</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.PerformanceLinkedIncentive).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.PerformanceLinkedIncentive) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>Medical Allow/Reimbursement</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.Medical).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.Medical) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>Food Allow/Meal Vouchers</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.SpecialAllowance).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.SpecialAllowance) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>Conveyance</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.Conveyance).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.Conveyance) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                double totalA = Convert.ToDouble(details1.BasicEmbedded + details1.HRAEmbedded + details1.RetentionBonus + details1.LTAEmbedded + details1.PerformanceLinkedIncentive + details1.Medical + details1.SpecialAllowance + details1.Conveyance);
                double TotalGrossSalary = Convert.ToDouble(totalA);

                reqReport.Append("    <tr bgcolor='#FFDAB9'>");
                reqReport.Append("    <td align=left><b>Total Gross Salary (A)</b></td>");
                reqReport.Append("    <td align=right>" + "<b>" + Convert.ToDouble(TotalGrossSalary).ToString("N") + "</b>" + "</td>");
                reqReport.Append("    <td align=right>" + "<b>" + (Convert.ToDouble(TotalGrossSalary) * 12).ToString("N") + "</b>" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>P.F.( Employer contribution)</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.PFEmbedded).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.PFEmbedded) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>E.S.I. ( Employer contribution)</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.ESIC).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.ESIC) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>Statutory Bonus</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.Bonus).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.Bonus) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>Group Insurance</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.RoleAllowance).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.RoleAllowance) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>Gratuity</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.GratuityEmbedded).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.GratuityEmbedded) * 12).ToString("N") + "" + "</td>");

                double TotalDeferredBenefits = Convert.ToDouble(details1.PFEmbedded + details1.ESIC + details1.Bonus + details1.RoleAllowance + details1.GratuityEmbedded);
                double totalB = Convert.ToDouble(TotalDeferredBenefits);

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left><b>Total Deferred Benefits ( B)</b></td>");
                reqReport.Append("    <td align=right>" + "<b>" + Convert.ToDouble(totalB).ToString("N") + "</b>" + "</td>");
                reqReport.Append("    <td align=right>" + "<b>" + (Convert.ToDouble(totalB) * 12).ToString("N") + "</b>" + "</td>");
                reqReport.Append("    </tr>");

                double CTC = Convert.ToDouble(TotalGrossSalary + totalB);
                double annualCTC = Convert.ToDouble(CTC * 12);

                reqReport.Append("    <tr bgcolor='#FFDAB9'>");
                reqReport.Append("    <td align=left><b>CTC( A+B)</b></td>");
                reqReport.Append("    <td align=right>" + "<b>" + Convert.ToDouble(CTC).ToString("N") + "</b>" + "</td>");
                reqReport.Append("    <td align=right>" + "<b>" + (Convert.ToDouble(CTC) * 12).ToString("N") + "</b>" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>P.F.</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.PFEmbedded).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.PFEmbedded) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>E.S.I.</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.ESIC).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.ESIC) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left>Professional Tax</td>");
                reqReport.Append("    <td align=right>" + "" + Convert.ToDouble(details1.SiteAllowance).ToString("N") + "" + "</td>");
                reqReport.Append("    <td align=right>" + "" + (Convert.ToDouble(details1.SiteAllowance) * 12).ToString("N") + "" + "</td>");
                reqReport.Append("    </tr>");

                double TD = Convert.ToDouble(details1.PFEmbedded + details1.ESIC + details1.SiteAllowance);
                double TotalDeductions = Convert.ToDouble(TD);

                reqReport.Append("    <tr>");
                reqReport.Append("    <td align=left><b>Total Deductions</b></td>");
                reqReport.Append("    <td align=right>" + "<b>" + Convert.ToDouble(TotalDeductions).ToString("N") + "</b>" + "</td>");
                reqReport.Append("    <td align=right>" + "<b>" + (Convert.ToDouble(TotalDeductions) * 12).ToString("N") + "</b>" + "</td>");
                reqReport.Append("    </tr>");

                double NETPAYMENT = Convert.ToDouble(TotalGrossSalary - TotalDeductions);

                reqReport.Append("    <tr bgcolor='#B0C4DE'>");
                reqReport.Append("    <td align=left><b>Net Payout **</b></td>");
                reqReport.Append("    <td align=right>" + "<b>" + Convert.ToDouble(NETPAYMENT).ToString("N") + "</b>" + "</td>");
                reqReport.Append("    <td align=right style='border-top: 1px solid black'><b>" + (Convert.ToDouble(NETPAYMENT) * 12).ToString("N") + "</b>" + "</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    </tr>");

                reqReport.Append("    </table>");
                reqReport.Append("    </td>");
                reqReport.Append("    </tr>");
                reqReport.Append("    <tr>");
                reqReport.Append("    <td height='60px'/>");
                reqReport.Append("    </tr>");
                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left><ul>");
                reqReport.Append("<li>Please note Insurance benefit for Medical and Personal Accidental is available from the date of Joining.</li>");
                reqReport.Append("<li>Gratuity is payable on completion of 5 years with the Organization which is exempt from income tax as per prevailing provisions of Income Tax Act.</li>");
                reqReport.Append("<li>** Net Payout is subject to the reimbursement option choosen by you on joining and Income Tax Deductions as applicable.<br><br></li>");
                reqReport.Append("</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td height='360px'/>");
                reqReport.Append("    </tr>");
                #endregion
                #region thirdpage

                reqReport.Append("    <tr height='50px'>");
                reqReport.Append("        <td align=center style='font-family : Calibri ;font-size:22pt; '><u><b>Annexure II</b></u></td>");
                reqReport.Append("    </tr>");



                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify  ><b>Documents to be submitted on first day</b></td>");
                reqReport.Append("    </tr>");
                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify  >(Please Note: photocopies of the documents are to be submitted. Original documents should be carried for verification)</td>");
                reqReport.Append("    </tr>");


                reqReport.Append("<tr>");
                reqReport.Append("    <td>");
                reqReport.Append("<table  style='font-family : Calibri ;font-size:18pt;width:100%; border-collapse:collapse;border-spacing: 0px;' align='center' border = '1' bordercolor='#000000'  cellspacing='3' cellpadding='3'>");

                reqReport.Append("<tr>");
                reqReport.Append("<td align=center width='1'><b>Sr.No<b></td>");
                reqReport.Append("<td align=center ><b>Documents</b></td>");
                reqReport.Append("</tr>");

                reqReport.Append("<tr>");
                reqReport.Append("<td align=center><b>1</b></td>");
                reqReport.Append("<td align=left ><b>Educational qualification proof (Submit copies along with originals for verification)</b></td>");
                reqReport.Append("</tr>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center>i.</td>");
                reqReport.Append("<td align=left >Photocopies of Graduation degree</td>");
                reqReport.Append("</tr>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center>ii.</td>");
                reqReport.Append("<td align=left >PG Degree</td>");
                reqReport.Append("</tr>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center>iii.</td>");
                reqReport.Append("<td align=left>Certificates (if done any certification courses i.e. Microsoft/Sun etc.)</td>");
                reqReport.Append("</tr>");

                reqReport.Append("<tr>");
                reqReport.Append("<td align=center><b>2</b></td>");
                reqReport.Append("<td align=left><b>Photo Identity proof</b></td>");
                reqReport.Append("</tr>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center>i.</td>");
                reqReport.Append("<td align=left>Passport / Driving License / PAN card / voter Id card</td>");
                reqReport.Append("</tr>");

                reqReport.Append("<tr>");
                reqReport.Append("<td align=center><b>3</b></td>");
                reqReport.Append("<td align=left><b>Address proof</b></td>");
                reqReport.Append("</tr>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center>i.</td>");
                reqReport.Append("<td align=left>Permanent address proof (Passport /Ration card/Electricity bill/telephone bill/ Rent agreement/Index II etc.)</td>");
                reqReport.Append("</tr>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center >ii.</td>");
                reqReport.Append("<td align=left>Temporary address proof (Passport/Ration card/Electricity bill/telephone bill/ Rent agreement/Index II etc.)</td>");
                reqReport.Append("</tr>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center><b>4</b></td>");
                reqReport.Append("<td align=left><b>Previous employment Proof</b></td>");
                reqReport.Append("</tr>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center>i.</td>");
                reqReport.Append("<td align=left>Relieving letter</td>");
                reqReport.Append("</tr>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center>ii.</td>");
                reqReport.Append("<td align=left>Experience letter</td>");
                reqReport.Append("</tr>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center>iii.</td>");
                reqReport.Append("<td align=left>Last salary slip/Certificate</td>");
                reqReport.Append("</tr>");

                reqReport.Append("<tr>");
                reqReport.Append("<td align=center><b>5</b></td>");
                reqReport.Append("<td align=left><b>Other Documents</b></td>");
                reqReport.Append("</tr>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center>i.</td>");
                reqReport.Append("<td align=left>Permanent Account Number (PAN)&#45;2 copies</td>");
                reqReport.Append("</tr>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center>ii.</td>");
                reqReport.Append("<td align=left>Copies of Passport</td>");
                reqReport.Append("</tr>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center>iii.</td>");
                reqReport.Append("<td align=left>1 passport size photograph and soft copy</td>");
                reqReport.Append("<tr>");
                reqReport.Append("<td align=center>iv.</td>");
                reqReport.Append("<td align=left>Form 16 / 12B</td>");
                reqReport.Append("</tr>");
                reqReport.Append("    </tr>");

                reqReport.Append("</tr>");
                reqReport.Append("    </table>");
                reqReport.Append("    </td>");



                reqReport.Append("    </tr><tr>");
                reqReport.Append("        <td align=left ><b>Note:</b></td>");
                reqReport.Append("    </tr>");


                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify >");
                reqReport.Append("      1.All team members are to obtain passport before confirmation and provide copies thereof to HR team for updating their records.<br>");
                reqReport.Append("      2.Please report to HR at 9:00 AM on the date of joining at e&#45;Zest Solutions Ltd., Kothrud office.<br>");
                reqReport.Append("      3.Your work location is at client side &#45; Greenlight Technology Lohia Jain IT Park Kothrud Pune..<br");
                reqReport.Append("      4.For any queries please contact&#58; &#45; Priya Nag/Samruddhi Ghatwal or Adnan Haidermota Contact No. &#58; &#45;7767809434 Email Id&#58; &#45; adnan.haidermota&#64;e&#45zest.in.");
                reqReport.Append("</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td height='460px'/>");
                reqReport.Append("    </tr>");

                #endregion
                #region finalpage
                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=center style='font-family : Calibri ;font-size:22pt; '><u><b>Frequently Asked Questions (FAQ)</b></u><br><br><br></td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify  >Things that you would like to know before joining. </td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify  >Q1. What are the official working hours? <br><br> Ans. Regular office working hours are 9.00 am to 6.30 pm from Monday to Friday. However, we allow <b>flexibility</b> by expanding the hours at which team member may arrive at the beginning of the day and leave at the end of the day. It helps the team members be efficient and effective if they modify their working hours to accommodate work, study and family commitments, with prior information to the reporting person.<br><br></td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify  >Q2. What are the standard deductions in the CTC offered? / What would be my in Hand salary? <br><br> Ans. Your salary deductions are as mentioned in the annexure I of the offer letter. The standard salary deductions will be Professional Tax, Provident Fund (employer as well as Employee contribution), ESI (only if applicable) and Gratuity. The standard amount towards Group Insurance is also deducted. If you have further queries contact us.<br><br></td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify  >Q3. What is the leave policy? <br><br>Ans. 21 days of leave will be credited on quarterly basis throughout the year and those joining post January 15 will be eligible for the leave at a prorate basis.<br><br></td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify  >Q4. What are the terms for Probation period? <br><br>Ans. Confirmation will be communicated, if your performance is found satisfactory during theprobation period. Regular performance reviews and support will be provided to understand your performance and suitability.<br><br></td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left><ul>");
                reqReport.Append("<li>Duration of Probation is three months from the date of your joining.<br><br></li>");
                reqReport.Append("<li>The probation period can be extended at the discretion of the Company. You shall continue to be on probation, till your services are confirmed in writing by a letter of confirmation.<br><br></li>");
                reqReport.Append("<li>In case your performance is not found satisfactory during such period of probation, including extended period, if any, your services shall stand terminated on expiry of such period of probation or extended period of probation and you shall be informed of the same in writing.<br><br></li>");
                reqReport.Append("<li>During the probation your services are terminable with two weeks notice on either side. The Company may, at its discretion, choose to terminate your services with one month's. Basic salary in lieu of notice period.<br></li>");
                reqReport.Append("<li>During probationary period you may not avail any paid leaves however you will be entitled for the statutory leaves post confirmation.<br><br></li>");
                reqReport.Append("</td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=justify  >Q5.What is the process for Performance Appraisal?<br><br>Ans. e&#45;Zest undergoes a performance appraisal on the joining anniversary.<br></td>");
                reqReport.Append("    </tr>");
                reqReport.Append("    <tr/>");

                reqReport.Append("    <tr>");
                reqReport.Append("    <td height='160px'/>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("        <td align=left ><b>Benefits:</b></td>");
                reqReport.Append("    </tr>");

                reqReport.Append("    <tr>");
                reqReport.Append("<td align=left <ul class='circle'>");
                reqReport.Append("<li>Health :</li>");
                reqReport.Append("<ul><li>Corporate health tie&#45;up&#45;gymnasium plan with Talwalkars.</li></ul>");
                reqReport.Append("<ul><li>Personal insurance cover of upto 1, 00, 000 from Third Party, General Insurance.</li></ul>");
                reqReport.Append("<li>Dining:</li>");
                reqReport.Append("<ul><li>Corporate tie&#45;up with Yorkshire, Amore and other restaurants &#45; details available on Intranet.</li></ul>");

                reqReport.Append("<li>Others:</li>");
                reqReport.Append("<ul><li>Tie&#45;up with Central Mall (electronics), Samsung etc.</li></ul>");
                reqReport.Append("<ul><li>Corporate tie &#45; up with Vodafone, Airtel, Idea</li></ul>");
                reqReport.Append("<ul><li>Quench Library</li></ul>");
                reqReport.Append("<ul><li>Klisma for Shopping</li></ul>");
                reqReport.Append("</td>");
                reqReport.Append("    </tr>");
                #endregion
                reqReport.Append("</table>");
                #endregion


                /*reqReport.Append(@" </div><tr>
                                <div style='mso-element:footer' id=f1>
                                <p class=MsoFooter>
                                <span style=mso-tab-count:2'></span>Page <b><span style='mso-field-code:"" PAGE ""'></span></b>
                                of <b><span style='mso-field-code:"" NUMPAGES ""'></span></b></p></div>
                                </tr>
                                </table></body></html>");*/

                # region Interopofferletter

                //string[] names = details.CandidateName.Split(' ');

                //string FirstName = names[0];
                //string SecondName = names[1];
                //object fileName = System.Web.Hosting.HostingEnvironment.MapPath(UrlConstants.Teamplate.RESOURCE_LOC) + UrlConstants.Teamplate.OFFER_LETTER_TEMPLATE;
                //object newfile = System.Web.Hosting.HostingEnvironment.MapPath(UrlConstants.Teamplate.RESOURCE_LOC) + Filename;
                //string newfilestring = System.Web.Hosting.HostingEnvironment.MapPath(UrlConstants.Teamplate.RESOURCE_LOC) + Filename;
                //object missing = System.Reflection.Missing.Value;
                //msWord.ApplicationClass wApp = new msWord.ApplicationClass();
                //try
                //{
                //    msWord.Document wDoc = wApp.Documents.Open(ref fileName, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
                //    //Find findText = wApp.Selection.Find;
                //    replace("[first name]", FirstName, wDoc, wApp);
                //    replace("[last name]", SecondName, wDoc, wApp);
                //    replace("[Designation]", details.Designation, wDoc, wApp);
                //    replace("[Salary]", details1.OfferedSalary, wDoc, wApp);
                //    string[] JoiningDatestring = details.JoiningDate.ToLongDateString().Split(',');
                //    string JoiningDate2 = JoiningDatestring[1];
                //    replace("[Joining Date]", JoiningDate2, wDoc, wApp);
                //    string[] OfferedDatestringoneday = details.OfferedDate.AddDays(1).ToLongDateString().Split(',');
                //    string OfferedDateaddoneday = OfferedDatestringoneday[1];
                //    //DateTime NextOffered = details.OfferedDate.AddDays(1);
                //    replace("[Next Offered]", OfferedDateaddoneday, wDoc, wApp);
                //    string[] OfferedDatestring = details.OfferedDate.ToLongDateString().Split(',');
                //    string OfferedDate2 = OfferedDatestring[1];
                //    replace("[Offered Date]", OfferedDate2, wDoc, wApp);

                //    replace("[Basic Monthly]", Convert.ToDouble(details1.BasicEmbedded).ToString("N"), wDoc, wApp);
                //    replace("[HRA Monthly]", Convert.ToDouble(details1.HRAEmbedded).ToString("N"), wDoc, wApp);
                //    replace("[EA Monthly]", Convert.ToDouble(details1.RetentionBonus).ToString("N"), wDoc, wApp);
                //    replace("[LTC Monthly]", Convert.ToDouble(details1.LTAEmbedded).ToString("N"), wDoc, wApp);
                //    replace("[Skill Monthly]", Convert.ToDouble(details1.PerformanceLinkedIncentive).ToString("N"), wDoc, wApp);
                //    replace("[Medical Monthly]", Convert.ToDouble(details1.Medical).ToString("N"), wDoc, wApp);
                //    replace("[Food Monthly]", Convert.ToDouble(details1.SpecialAllowance).ToString("N"), wDoc, wApp);
                //    replace("[Conveyance Monthly]", Convert.ToDouble(details1.Conveyance).ToString("N"), wDoc, wApp);
                //    double totalA = Convert.ToDouble(details1.BasicEmbedded + details1.HRAEmbedded + details1.RetentionBonus + details1.LTAEmbedded + details1.PerformanceLinkedIncentive + details1.Medical + details1.SpecialAllowance + details1.Conveyance);
                //    double TotalGrossSalary = Convert.ToDouble(totalA);
                //    replace("[Gross Monthly]", Convert.ToDouble(TotalGrossSalary).ToString("N"), wDoc, wApp);
                //    replace("[PF Emp Monthly]", Convert.ToDouble(details1.PFEmbedded).ToString("N"), wDoc, wApp);
                //    replace("[ESI Emp Monthly]", Convert.ToDouble(details1.ESIC).ToString("N"), wDoc, wApp);
                //    replace("[Stat Monthly]", Convert.ToDouble(details1.Bonus).ToString("N"), wDoc, wApp);
                //    replace("[Group Monthly]", Convert.ToDouble(details1.RoleAllowance).ToString("N"), wDoc, wApp);
                //    replace("[Gratuity Monthly]", Convert.ToDouble(details1.GratuityEmbedded).ToString("N"), wDoc, wApp);
                //    double TotalDeferredBenefits = Convert.ToDouble(details1.PFEmbedded + details1.ESIC + details1.Bonus + details1.RoleAllowance + details1.GratuityEmbedded);
                //    double totalB = Convert.ToDouble(TotalDeferredBenefits);
                //    replace("[Deferred Monthly]", Convert.ToDouble(totalB).ToString("N"), wDoc, wApp);
                //    double CTC = Convert.ToDouble(TotalGrossSalary + totalB);
                //    double annualCTC = Convert.ToDouble(CTC * 12);
                //    replace("[CTC Monthly]", Convert.ToDouble(CTC).ToString("N"), wDoc, wApp);
                //    replace("[PF Monthly]", Convert.ToDouble(details1.PFEmbedded).ToString("N"), wDoc, wApp);
                //    replace("[ESI Monthly]", Convert.ToDouble(details1.ESIC).ToString("N"), wDoc, wApp);
                //    replace("[Tax Monthly]", Convert.ToDouble(details1.SiteAllowance).ToString("N"), wDoc, wApp);
                //    double TD = Convert.ToDouble(details1.PFEmbedded + details1.ESIC + details1.SiteAllowance);
                //    double TotalDeductions = Convert.ToDouble(TD);
                //    replace("[Ded Monthly]", Convert.ToDouble(TotalDeductions).ToString("N"), wDoc, wApp);
                //    double NETPAYMENT = Convert.ToDouble(TotalGrossSalary - TotalDeductions);
                //    replace("[Net Monthly]", Convert.ToDouble(NETPAYMENT).ToString("N"), wDoc, wApp);

                //    replace("[Basic Yearly]", (Convert.ToDouble(details1.BasicEmbedded) * 12).ToString("N"), wDoc, wApp);
                //    replace("[HRA Yearly]", (Convert.ToDouble(details1.HRAEmbedded) * 12).ToString("N"), wDoc, wApp);
                //    replace("[EA Yearly]", (Convert.ToDouble(details1.RetentionBonus) * 12).ToString("N"), wDoc, wApp);
                //    replace("[LTC Yearly]", (Convert.ToDouble(details1.LTAEmbedded) * 12).ToString("N"), wDoc, wApp);
                //    replace("[Skill Yearly]", (Convert.ToDouble(details1.PerformanceLinkedIncentive) * 12).ToString("N"), wDoc, wApp);
                //    replace("[Medical Yearly]", (Convert.ToDouble(details1.Medical) * 12).ToString("N"), wDoc, wApp);
                //    replace("[Food Yearly]", (Convert.ToDouble(details1.SpecialAllowance) * 12).ToString("N"), wDoc, wApp);
                //    replace("[Conveyance Yearly]", (Convert.ToDouble(details1.Conveyance) * 12).ToString("N"), wDoc, wApp);
                //    replace("[Gross Yearly]", (Convert.ToDouble(TotalGrossSalary) * 12).ToString("N"), wDoc, wApp);
                //    replace("[PF Emp Yearly]", (Convert.ToDouble(details1.PFEmbedded) * 12).ToString("N"), wDoc, wApp);
                //    replace("[ESI Emp Yearly]", (Convert.ToDouble(details1.ESIC) * 12).ToString("N"), wDoc, wApp);
                //    replace("[Stat Yearly]", (Convert.ToDouble(details1.Bonus) * 12).ToString("N"), wDoc, wApp);
                //    replace("[Group Yearly]", (Convert.ToDouble(details1.RoleAllowance) * 12).ToString("N"), wDoc, wApp);
                //    replace("[Gratuity Yearly]", (Convert.ToDouble(details1.GratuityEmbedded) * 12).ToString("N"), wDoc, wApp);
                //    replace("[Deferred Yearly]", (Convert.ToDouble(totalB) * 12).ToString("N"), wDoc, wApp);
                //    replace("[CTC Yearly]", (Convert.ToDouble(CTC) * 12).ToString("N"), wDoc, wApp);
                //    replace("[PF Yearly]", (Convert.ToDouble(details1.PFEmbedded) * 12).ToString("N"), wDoc, wApp);
                //    replace("[ESI Yearly]", (Convert.ToDouble(details1.ESIC) * 12).ToString("N"), wDoc, wApp);
                //    replace("[Tax Yearly]", (Convert.ToDouble(details1.SiteAllowance) * 12).ToString("N"), wDoc, wApp);
                //    replace("[Ded Yearly]", (Convert.ToDouble(TotalDeductions) * 12).ToString("N"), wDoc, wApp);
                //    replace("[Net Yearly]", (Convert.ToDouble(NETPAYMENT) * 12).ToString("N"), wDoc, wApp);


                //    object Readonly = false;
                //    object doNotSaveChanges = msWord.WdSaveOptions.wdDoNotSaveChanges; 
                //    object format = msWord.WdSaveFormat.wdFormatDocument;
                //    wDoc.SaveAs(ref newfile, ref format, ref missing, ref missing, ref missing, ref missing, ref Readonly, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
                //    wDoc.Close(ref doNotSaveChanges, ref missing, ref missing);
                //    wApp.Quit(ref doNotSaveChanges, ref missing, ref missing);
                //}
                //catch (Exception ex)
                //{
                //    MiscUtil.ShowMessage(lblMessage, ex.Message, false);
                //}
                #endregion
                UploadDocument(MemberId, Filename, reqReport, Title);
                fullpath = UrlConstants.ApplicationBaseUrl + "Resources/Member/" + MemberId + "/" + "Offer Letter/" + Filename;
                try
                {
                    PdfConverter pdfConverter = new PdfConverter();
                    UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                    pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                    pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                    pdfConverter.PdfDocumentOptions.ShowHeader = false;
                    pdfConverter.PdfDocumentOptions.ShowFooter = true;
                    pdfConverter.PdfDocumentOptions.LeftMargin = 55;
                    pdfConverter.PdfDocumentOptions.RightMargin = 50;
                    pdfConverter.PdfDocumentOptions.TopMargin = 30;
                    pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                    pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                    pdfConverter.PdfDocumentOptions.ShowHeader = false;
                    pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                    pdfConverter.PdfFooterOptions.FooterTextColor = System.Drawing.Color.Black;
                    pdfConverter.PdfFooterOptions.DrawFooterLine = false;
                    pdfConverter.PdfFooterOptions.FooterText = "";
                    pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                    pdfConverter.PdfFooterOptions.ShowPageNumber = true;
                    pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;



                    //************Code added/modify by pravin khot on 10/June/2016 USING CHANGE temp file generated***************
                    //byte[] arrBytData = pdfConverter.GetPdfBytesFromHtmlStringWithTempFile(reqReport.ToString(), ""); //COMMENTED
                    //****************ADDED****************
                    string Filename1 = MemberId + ".html";
                    string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, MemberId, Filename1, "Offer Letter", false);
                    string physicalPath = HttpContext.Current.Request.PhysicalApplicationPath;
                    fullpath = physicalPath + "Resources\\Member\\" + MemberId + "\\" + "Offer Letter\\" + Filename1;
                    System.IO.StreamWriter file = new System.IO.StreamWriter(fullpath);
                    file.WriteLine(reqReport.ToString());
                    file.Close();
                    byte[] arrBytData = pdfConverter.GetPdfFromUrlBytes(fullpath);
                    //*****************************END************************************



                    System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    response.Clear();
                    response.AddHeader("Content-Type", "binary/octet-stream");
                    response.AddHeader("Content-Disposition", "attachment; filename=" + Filename + "; size=" + arrBytData.Length.ToString());
                    //response.AddHeader("Content-Disposition", "attachment; filename=" + Filename);
                    response.Flush();
                    response.BinaryWrite(arrBytData);
                    response.Flush();
                    response.End();


                 



                }
                catch
                { }
                UploadDocument(MemberId, Filename, reqReport, Title);
                fullpath = UrlConstants.ApplicationBaseUrl + "Resources/Member/" + MemberId + "/" + "Offer Letter/" + Filename;





                //    Response.Clear();
                //    Response.ContentType = "application/msword";
                //    Response.AddHeader("content-disposition", String.Format(System.Globalization.CultureInfo.InvariantCulture, "attachment; filename={0}", Filename));
                //    //Response.AddHeader("content-disposition", String.Format(System.Globalization.CultureInfo.InvariantCulture, "attachment; filename={0}", Filename));
                //    //Response.AppendHeader("Content-Length", reqReport.Length.ToString());
                //    //Response.ContentType = "application/octet-stream";
                //    Parser.DocumentConverter document = new Parser.DocumentConverter();
                //    Parser.DocumentConverter.OutputTypes doctype = Parser.DocumentConverter.OutputTypes.Rtf;
                //    byte[] arrBytData = document.ConvertTo(newfilestring,doctype);
                //    Response.BinaryWrite(arrBytData);
                //    //Response.Output.Write(reqReport.ToString());
                //    Response.End();


                //    if (File.Exists(newfilestring))
                //        File.Delete(newfilestring);
                //}
                ////if (File.Exists(newfilestring))
                ////    File.Delete(newfilestring);
            }
            return fullpath;
        }
        //private void replace(string ToFind, string ToReplace, msWord.Document wDoc, msWord.Application wApp)
        //{
        //    object missing = System.Reflection.Missing.Value;
        //    msWord.Find findText = wApp.Selection.Find;
        //    findText.Text = ToFind;
        //    findText.Replacement.Text = ToReplace;
        //    object replace = msWord.WdReplace.wdReplaceAll;
        //    findText.Execute(ref missing, ref missing, ref missing, ref missing, ref missing,
        //        ref missing, ref missing, ref missing, ref missing, ref missing,
        //        ref replace, ref missing, ref missing, ref missing, ref missing);
        //}
        protected void lsvCandidateList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }
            int id;
            int.TryParse(e.CommandArgument.ToString(), out id);
            if (id > 0)
            {
                if (string.Equals(e.CommandName, "MoveToNextLevel"))
                {
                    Facade.MemberJobCart_MoveToNextLevel(HiringMatrixInterviewLevel, base.CurrentMember.Id, e.CommandArgument.ToString(), CurrentJobPosting.Id, "Next");
                    BindList();
                    MiscUtil.ShowMessage(lblMessage, "Successfully moved 1 candidate to " + hdnNextLevelName.Value, false);
                }
                else if (string.Equals(e.CommandName, "MoveToPreviousLevel"))
                {
                    txtSelectedIds.Text = e.CommandArgument.ToString();
                    Facade.MemberJobCart_MoveToNextLevel(HiringMatrixInterviewLevel, base.CurrentMember.Id, txtSelectedIds.Text, CurrentJobPosting.Id, ("Previous"));
                    BindList();
                    MiscUtil.ShowMessage(lblMessage, "Successfully moved 1 candidate to " + hdnPreviousLevelName.Value, false);
                }
                else if (string.Equals(e.CommandName, "Remove"))
                {
                    txtSelectedIds.Text = e.CommandArgument.ToString();
                    hdnRemove.Value = "Remove";
                    uclConfirm.AddMessage("Are you sure that you want to remove this candidate  from the Hiring Matrix?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
                    {
                    }
                }

                else if (string.Equals(e.CommandName, "EmailRequisition"))
                {
                    SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Dashboard/" + UrlConstants.Dashboard.NEWMAIL, "", UrlConstants.PARAM_JOB_ID, base.CurrentJobPostingId.ToString(), UrlConstants.PARAM_PAGE_FROM, "HiringMatrix", UrlConstants.PARAM_SELECTED_IDS, e.CommandArgument.ToString());
                    ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(Page), "NewMail", "<script>btnMewMail_Click('" + url + "');</script>", false);
                }
                else if (string.Equals(e.CommandName, "SubmitToClient"))
                {
                    SubmitToClient(e.CommandArgument.ToString());
                }
                else if (string.Equals(e.CommandName, "RemoveCandidateFromHM"))
                {
                    //HiddenField hdnrmvCandidate = (HiddenField)e.Item.FindControl("hdnrmvCandidate");

                    string confirmValue = hdnrmvCandidate.Value;

                    if (confirmValue == "Yes")
                    {
                        RemoveCandidateFromHiringMatrix(e.CommandArgument.ToString());
                        MiscUtil.ShowMessage(lblMessage, "Selected Candidate has been Removed successfully from Hiring Matrix", false);
                    }
                    else
                    {

                    }

                }
                else if (string.Equals(e.CommandName, "GenerateCompensation"))
                {

                    LinkButton lnkbutton = (LinkButton)e.CommandSource;

                    //string url = GenerateSalaryCompensation(CurrentJobPosting.Id, id);
                    
                    BindList();
                    //ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(Page), "NewMail", "<script>btnMewMail_Click('" + url + "');</script>", false);

                    //ScriptManager.RegisterClientScriptBlock(lblPayrateCurrency, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Offer Details Saved Successfully');", true);
                }
                else if (string.Equals(e.CommandName, "GenerateOfferLetter"))
                {
                    try 
                    {
                        MemberHiringDetails details = Facade.GetOfferDetailsByJobpostingIdAndMemberId(CurrentJobPosting.Id, id);//Facade.GetOfferDetailsByJobpostingIdAndMemberId(id, CurrentJobPosting.Id);

                        MemberHiringDetails details1 = Facade.GetSalaryComponentsByJobpostingIdAndMemberId(CurrentJobPosting.Id , id);
                        if (details != null) 
                        {
                            string CandidateName = details.CandidateName.Replace(" ", "_");
                            string JobTitle = details.JobTitle.Replace(" ", "_");
                            string Filename = "Offer_Letter-" + CandidateName.ToString().Trim() + "-" + JobTitle + ".pdf";
                            string url = "";
                            //Response.Clear();
                            //Response.ContentType = "application/msword";
                            //Response.AddHeader("content-disposition", String.Format(System.Globalization.CultureInfo.InvariantCulture, "attachment; filename={0}", Filename));
                            ////Response.AddHeader("content-disposition", String.Format(System.Globalization.CultureInfo.InvariantCulture, "attachment; filename={0}", Filename));

                            //Response.Output.Write(GenerateOfferLetter(details, Filename, id));
                            //Response.End();
                            GenerateOfferLetter(details, Filename, id, details1);
                            //ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(Page), "NewMail", "<script>btnMewMail_Click('" + GenerateOfferLetter(details, Filename, id) + "');</script>", false);
                            BindList();
                        }
                        
                    }
                    catch(Exception ex) 
                    {
                    
                    }
                    
                }
                else if (string.Equals(e.CommandName, "HiringDetails"))
                {
                    //ASP.controls_hiringdetails_ascx hireDetails = (ASP.controls_hiringdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateHiringDetails").FindControl("uclHiringDetails");
                    //AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeHiringDetails");
                    //hireDetails.JobPostingId = CurrentJobPostingId;
                    //hireDetails.MemberID = e.CommandArgument.ToString();
                    //hireDetails.StatusId = 0;
                    //ext.Enabled = true;
                    //ext.Show();
                }
                else if (string.Equals(e.CommandName, "RejectCandidate"))
                {
                    //ASP.controls_rejectdetails_ascx candidate = (ASP.controls_rejectdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateReject").FindControl("uclRejectCandidate");
                    //AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeRejectCandidate");
                    //candidate.MemberID = e.CommandArgument.ToString();
                    //candidate.HiringMatrixLevel = HiringMatrixInterviewLevel;
                    //candidate.JobPostingId = CurrentJobPostingId;
                    //ext.Enabled = true;
                    //ext.Show();
                }
                else if (string.Equals(e.CommandName, "SubmissionDetails"))
                {
                    //ASP.controls_submissiondetails_ascx submission = (ASP.controls_submissiondetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateSubmissions").FindControl("uclSubmissionDetails");
                    //AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeSubmissionDetails");
                    //submission.JobPostingId = CurrentJobPostingId;
                    //submission.MemberID = e.CommandArgument.ToString();

                    //ext.Enabled = true;
                    //ext.Show();
                }
                else if (string.Equals(e.CommandName, "JoiningDetails"))
                {
                    //ASP.controls_joiningdetails_ascx joined = (ASP.controls_joiningdetails_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateJoining").FindControl("uclJoiningDetails");
                    //AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeJoiningDetails");
                    //joined.JobPostingId = CurrentJobPostingId;
                    //joined.MemberID = e.CommandArgument.ToString();
                    //ext.Enabled = true;
                    //ext.Show();
                }
                else if (string.Equals(e.CommandName, "HiringLog"))
                {
                    //ASP.controls_candidateactionlog_ascx actionLog = (ASP.controls_candidateactionlog_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateActionLog").FindControl("uclActionLog");
                    //ASP.controls_modaltemplate_ascx modalTemplate = (ASP.controls_modaltemplate_ascx)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uclTemplateActionLog");
                    //AjaxControlToolkit.ModalPopupExtender ext = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("mpeActionLog");

                    //actionLog.MemberID = e.CommandArgument.ToString();
                    //modalTemplate.ModalTitle = actionLog.getModalTitleWithCandidateName();
                    //ext.Enabled = true;
                    //ext.Show();
                }
            }
        }

        T Cast<T>(object obj, T type)
        { return (T)obj; }

        #endregion
        [System.Web.Services.WebMethod]
        public static object[] MoveCandidatesToLevel(object data)
        {
            IFacade facade = new Facade();
            Dictionary<string, object> param =
                  (Dictionary<string, object>)data;
            string MemberId = param["Member_Id"].ToString();
            int JobPostingId = Convert.ToInt32(param["JobPosting_Id"]);
            string StatusId = param["StatusId"].ToString();
            int UpdatorId = Convert.ToInt32(param["UpdatorId"]);

            //*******Code added by pravin khot on 28/July/2016**************
            RejectCandidate cand = facade.GetRejectCandidateByMemberIdAndJobPostingID(Int32.Parse(MemberId), JobPostingId);
            if (cand != null)
            {
                facade.RejectToUnRejectCandidateStatusChange(MemberId, JobPostingId);
            }
            //************************END****************************

            facade.MemberJobCart_MoveToNextLevel(0, UpdatorId, MemberId, JobPostingId, StatusId);
            HiringMatrixLevels l = facade.GetHiringMatrixLevelsById(Convert.ToInt32(StatusId));
            MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, JobPostingId, MemberId, UpdatorId, l.Name, facade);
            facade.UpdateCandidateRequisitionStatus(JobPostingId, MemberId, UpdatorId, Convert.ToInt32(StatusId));
            string[] s = { "Moved", "" };
            return s;
        }

        [System.Web.Services.WebMethod]
        public static object[] RemoveHiringDetails(object data)
        {
            IFacade facade = new Facade();
            Dictionary<string, object> param =
                  (Dictionary<string, object>)data;

            int MemberId = Convert.ToInt32(param["Member_Id"]);
            int JobPostingId = Convert.ToInt32(param["JobPosting_Id"]);
            MemberHiringDetails hiringdetails = facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(MemberId, JobPostingId);
            facade.DeleteMemberHiringDetailsByID(hiringdetails.Id);
            string[] s = { "Deleted", "" };
            return s;
        }

        [System.Web.Services.WebMethod]
        public static object[] SaveSubmissionDetails(object data)
        {
            buildSubmissionDetails(data);
            string[] s = { "", "" };
            return s;
        }

        private static void buildSubmissionDetails(object data)
        {
            Dictionary<string, object> param =
                     (Dictionary<string, object>)data;
            IFacade facade = new Facade();
            int MemberId = Convert.ToInt32(param["MemberId"]);
            int JobPostingId = Convert.ToInt32(param["JobPostingId"]);

            MemberSubmission memberSubmission = new MemberSubmission();
            memberSubmission = facade.GetMemberSubmissionsByMemberIDAndJobPostingId(MemberId, JobPostingId);
            if (memberSubmission == null) memberSubmission = new MemberSubmission();
            memberSubmission.SubmittedDate = Convert.ToDateTime(param["DateSubmitted"].ToString());
            memberSubmission.CompanyId = Convert.ToInt32(param["Account"]);
            memberSubmission.MemberId = Convert.ToInt32(param["MemberId"]);
            memberSubmission.JobPostingId = Convert.ToInt32(param["JobPostingId"]);
            memberSubmission.IsRemoved = false;
            memberSubmission.CreatorId = Convert.ToInt32(param["SubmittedBy"]);
            memberSubmission.MemberEmailDetailId = 0;
            memberSubmission.ReceiverEmail = param["Contacts"].ToString(); ;
            //if (memberSubmission.Id == 0)
            //    facade.AddMemberSubmission(memberSubmission);
            //else facade.UpdateMemberSubmission(memberSubmission);
            int StatusId = Convert.ToInt32(param["StatusId"]);
            int CurrentMemberId = Convert.ToInt32(param["CurrentMemberId"]);
            try
            {
                if (StatusId != null && StatusId != 0)
                {
                    facade.UpdateByStatus(JobPostingId, MemberId.ToString(), StatusId, CurrentMemberId);
                    facade.UpdateCandidateRequisitionStatus(JobPostingId, MemberId.ToString(), CurrentMemberId, StatusId);
                }
            }
            catch
            {
            }
        }
        [System.Web.Services.WebMethod]
        public static object[] SaveOfferDetails(object data)
        {
            buildOfferDetails(data);
            string[] s = { "Inserted", "" };
            return s;
        }
        private static void buildOfferDetails(object data)
        {

            Dictionary<string, object> param =
                   (Dictionary<string, object>)data;
            IFacade facade = new Facade();
            int MemberId = Convert.ToInt32(param["Member_Id"]);
            int JobPostingId = Convert.ToInt32(param["JobPosting_Id"]);

            MemberHiringDetails details = facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(MemberId, JobPostingId);
            if (details == null) details = new MemberHiringDetails();

            details.JoiningDate = Convert.ToDateTime(param["JoiningDate"]);
            details.OfferAccepted = Convert.ToBoolean(param["OfferAccepted"]);
            details.OfferedDate = Convert.ToDateTime(param["DateOffered"]);
            details.OfferedPosition = param["Position"].ToString();
            details.OfferedSalary = param["Salary"].ToString();
            details.OfferedSalaryCurrency = Convert.ToInt32(param["SalaryCurrent"]);
            details.OfferedSalaryPayCycle = Convert.ToInt32(param["SalaryPayCycle"]);
            details.CommissionCurrency = Convert.ToInt32(param["CommisionCurrency"]);
            details.CommissionPayRate = param["Commision"].ToString();
            details.MemberId = Convert.ToInt32(param["Member_Id"]);
            details.JobPostingId = Convert.ToInt32(param["JobPosting_Id"]);
            details.UpdatorId = Convert.ToInt32(param["CurrentMember_Id"]);

            if (details.Id == 0)
            {
                details.CreatorId = Convert.ToInt32(param["CurrentMember_Id"]);
                // facade.AddMemberHiringDetails(details);
            }
            else facade.UpdateMemberHiringDetails(details);
            int StatusId = Convert.ToInt32(param["Status_Id"]);
            int CurrentMemberId = Convert.ToInt32(param["CurrentMember_Id"]);
            try
            {
                if (StatusId != null && StatusId != 0)
                {
                    facade.UpdateByStatus(JobPostingId, MemberId.ToString(), StatusId, CurrentMemberId);
                    facade.UpdateCandidateRequisitionStatus(JobPostingId, MemberId.ToString(), CurrentMemberId, StatusId);
                }
            }
            catch { }
        }
        [System.Web.Services.WebMethod]
        public static object[] SaveHiringMatrixSource(object data)
        {
            Dictionary<string, object> param =
               (Dictionary<string, object>)data;
            IFacade facade = new Facade();
            try
            {
                facade.UpdateMemberJobCartByMemberIdAndJobPostingID(Convert.ToInt32(param["Member_Id"]), Convert.ToInt32(param["JobPostingId"]), Convert.ToInt32(param["SelectedSource"]));
                MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateRecruterSourceEdited, Convert.ToInt32(param["JobPostingId"]), Convert.ToString(param["Member_Id"]), Convert.ToInt32(param["CurrentMember"]), facade);
            }
            catch
            {
            }
            string name = MiscUtil.GetMemberNameById(Convert.ToInt32(param["SelectedSource"]), facade);
            string[] s = { name };//{ hiringnote.Replace("\n", "<br/>"), UpdatorName, updatedate, updatetime };
            return s;
        }

        [System.Web.Services.WebMethod]
        public static object[] SaveHiringNotes(object data)
        {
            string updatedate = string.Empty;
            string updatetime = string.Empty;
            string hiringnote = string.Empty;
            string UpdatorName = buildHiringNotes(data, out updatedate, out updatetime, out hiringnote);
            string[] s = { hiringnote.Replace("\n", "<br/>"), UpdatorName, updatedate, updatetime };
            return s;
        }

        private static string buildHiringNotes(object data, out string updatedate, out string updatetime, out string hiringnote)
        {

            Dictionary<string, object> param =
                   (Dictionary<string, object>)data;
            IFacade facade = new Facade();
            CommonNote note = new CommonNote();
            MemberNote memberNote = new MemberNote();
            if (Convert.ToString(param["HiringNotes"]).Trim() != Convert.ToString(param["OlderNote"]))
            {
                note.NoteCategoryLookupId = -1;
                note.NoteDetail = MiscUtil.RemoveScript(Convert.ToString(param["HiringNotes"]));
                note.CreatorId = note.UpdatorId = Convert.ToInt32(param["UpdatorId"]);
                note = facade.AddCommonNote(note);
                memberNote.MemberId = Convert.ToInt32(param["Member_Id"]);
                memberNote.CommonNoteId = note.Id;
                memberNote.CreatorId = Convert.ToInt32(param["UpdatorId"]);
                memberNote.JobPostingId = Convert.ToInt32(param["JobPostingId"]);
                memberNote = facade.AddMemberNote(memberNote);
                MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateHiringNoteAdded, Convert.ToInt32(param["JobPostingId"]), Convert.ToString(param["Member_Id"]), Convert.ToInt32(param["UpdatorId"]), facade);
                updatedate = note.UpdateDate.ToShortDateString();
                updatetime = note.UpdateDate.ToShortTimeString();
                hiringnote = note.NoteDetail;
                return MiscUtil.GetMemberNameById(Convert.ToInt32(param["UpdatorId"]), facade);
            }
            else
            {
                updatedate = string.Empty;
                updatetime = string.Empty;
                hiringnote = string.Empty;
                return string.Empty;
            }

        }
        [System.Web.Services.WebMethod]
        public static object[] ExecuteCommand(string commandName, string targetMethod, object data)
        {
            try
            {
                object[] result = new object[2];
                result[0] = Command.Create(commandName).Execute(data);
                result[1] = targetMethod;

                return result;
            }
            catch (Exception ex)
            {
                // TODO: add logging functionality 
                throw;
            }
        }
    }
}