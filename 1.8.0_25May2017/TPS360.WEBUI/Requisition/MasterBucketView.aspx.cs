﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: BasicInfoEditor.ascx.cs
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
     0.1           23/feb/2016         pravin khot         Code added by pravin khot on 23/Feb/2016 [Using Hide workflow and publish section for specific roles]
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;
using System.Web;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections.Generic;
using System.Collections;
using TPS360.BusinessFacade;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using TPS360.Web.UI.Helper;
using System.Linq;
using TPS360.Common;
using System.Web.UI;
using System.Threading;

namespace TPS360.Web.UI.Requisition
{
    public partial class BucketView : RequisitionBasePage
    {
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //if (ddlMyRequisitions.SelectedIndex == 0)
            //{
            //    MiscUtil.ShowMessage(lblMessage, "Please select at least one Hiring Status of Candidate.", true);
            //    return;
            //}
            if (ddlMyRequisitions.SelectedIndex == 0)
            {
                Session["Status"] = "PageLoadAllReq";
            }
            else
            {
                Session["Status"] = "Click";
            }
            Session["1"] = "0";
            Session["2"] = "0";
            Session["MemberID"] = CurrentMember.Id;
           
            //Response.Cookies["RequisitionStatus"].Value = "";
            //Response.Cookies["RequisitionStatus"].Value = ddlJobStatus.SelectedItems;
            //Session["MemberID"] = CurrentMember.Id;
            fillctr();
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            //Response.Redirect("~/Requisition/BucketView.aspx");
            Session["1"] = "0";
            Session["2"] = "0";
            Session["Status"] = "PageLoad";
            Session["Matrix"] = "0";
            Session["Clear"] = "Clear";
            //Session["MemberID"] = CurrentMember.Id;
            foreach (ListItem item in ddlJobStatus.ListItem.Items)
            {
                item.Selected = false;
            }
            Response.Cookies["RequisitionStatus"].Value = ddlJobStatus.SelectedItems;
            ddlMyRequisitions.SelectedIndex = 0;
            //ddlMyRequisitionsStatus.SelectedIndex = 0;
            fillctr();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["1"] = 0;
            Session["2"] = 0;
            if (!IsPostBack)
            {
                FillDummyMatrixList();
                //FillRequisitionList(CurrentMember.Id);
                AllJobPostingList();
                //MiscUtil.PopulateRequistionStatus(ddlMyRequisitionsStatus, Facade);
                //MiscUtil.PopulateMemberListByRole(ddlReqCreator, ContextConstants.ROLE_EMPLOYEE, Facade);   // Creator List
                //SelectJobStatusList();
                //ddlMyRequisitionsStatus.Items.Insert(0, new ListItem("-Select Status-", "NA"));
                Session["MemberID"] = CurrentMember.Id;
                Session["Status"] = "PageLoad";
                Session["Matrix"] = "0";
                fillctr();
            }
        }

        private void SelectJobStatusList()
        {
            if (Request.Cookies["RequisitionStatus"] != null)
            {
                //ddlJobStatus.SelectedItems = Request.Cookies["RequisitionStatus"].Value;
            }
            else
            {
                foreach (ListItem item in ddlJobStatus.ListItem.Items)
                {
                    item.Selected = false;
                }
            }
        }

        
        private void fillctr()
        {
            System.Web.UI.Control myCnt;
            //PlaceHolder_1.Controls.Clear();
            Session["1"] = "0";
            Session["2"] = "0";
            //Session["MemberID"] = CurrentMember.Id;
            //Response.Cookies["RequisitionStatus"].Value = "";
            //Response.Cookies["RequisitionStatus"].Value = ddlJobStatus.SelectedItems;
            
            List<string> elementsddlMyRequisitions = new List<string>();
            foreach (ListItem lireq in ddlMyRequisitions.Items)
            {
                if (int.Parse(lireq.Value)!=0)
                {
                    elementsddlMyRequisitions.Add(lireq.Value);
                }
            }

            List<string> elementsddlMyRequisitions1 = new List<string>();
            //foreach (ListItem lireq in ddlMyRequisitionsStatus.Items)
            //{
            //    try
            //    {
            //        if (int.Parse(lireq.Value) != 0)
            //        {
            //            elementsddlMyRequisitions1.Add(lireq.Value);
            //        }
            //    }
            //    catch { }
            //}

            if (ddlMyRequisitions.SelectedIndex != 0)
            {
                Session["1"] = ddlMyRequisitions.SelectedValue;
            }
            //else { Session["1"] = 211; }
            //foreach (ListItem li in ddlJobStatus.ListItem.Items)
            //{
            //    if (li.Selected != true)
            //    {
            //        li.Selected = true;
            //        //elements.Add(li.Value);
            //    }
            //}


            List<string> elements = new List<string>();
            foreach (ListItem li in ddlJobStatus.ListItem.Items)
            {
                if (li.Selected == true)
                {
                    elements.Add(li.Value);
                }
            }

           
            

            List<string> elementsAll = new List<string>();
            foreach (ListItem li in ddlJobStatus.ListItem.Items)
            {
                if (li.Text!=null)
                {
                    elementsAll.Add(li.Value);
                }
            }

            int i;
            int cnt = 0;
            IList<HiringMatrixLevels> levels = new List<HiringMatrixLevels>();
            try
            {                
                levels = Facade.GetAllHiringMatrixLevels();
                cnt = levels.Count; //take matrix level count,id from database
            }
            catch
            {
                //cnt = 5;
            }
            if (elements != null)
            {
                //cnt = elements.Count;
                if (elements.Count > 0)
                {
                    cnt = elements.Count;
                }
            }
      
            PlaceHolder_1.Controls.Clear();
            for (i = 0; i <= cnt - 1; i++)
            {
                if (elements.Count != 0)
                {
                    if (elements.Count > 0)
                    {
                        Session["2"] = elements[i];
                    }                    
                }
                else
                {
                    if (elementsAll.Count > 0)
                    {
                        Session["2"] = elementsAll[i]; //take value from database matrix level
                    }
                    //Session["2"] = elementsAll[i]; //take value from database matrix level
                }
              
                myCnt = Page.LoadControl("~/Controls/BucketViewGrid.ascx");
                myCnt.ID = "Control" + i;
                //Div2.Controls.Add(myCnt);
                //Div2.Visible = true;

                PlaceHolder_1.Controls.Add(myCnt);
                PlaceHolder_1.Visible = true;
               
            }
            //if (ddlMyRequisitions.SelectedIndex == 0)
            //{
            //    //PlaceHolder_1.Visible = false;
            //}

        }
        public void FillDummyMatrixList()
        {
            IList<HiringMatrixLevels> levels = new List<HiringMatrixLevels>();
            levels = Facade.GetAllHiringMatrixLevels();
            //HiringMatrixLevels l = new HiringMatrixLevels();
            //l.Id = -1;
            //l.Name = "Rejected";
            //levels.Add(l);
            ddlJobStatus.DataSource = levels;
            ddlJobStatus.DataTextField = "Name";
            ddlJobStatus.DataValueField = "Id";
            ddlJobStatus.DataBind();
            SelectJobStatusList();

            //ddlHiringStatus.Items.Insert(0, new ListItem("-Select Matrix Level-", "0"));
            //dummyList = (DropDownList)MiscUtil.RemoveScriptForDropDown(dummyList);
            //dummyList.Items.Add(new ListItem("Rejected", "Reject"));

        }


        public void AllJobPostingList()
        {
            ddlMyRequisitions.DataSource = Facade.GetAllJobPostingByStatus(0);
            ddlMyRequisitions.DataTextField = "JobTitle";
            ddlMyRequisitions.DataValueField = "Id";
            ddlMyRequisitions.DataBind();
            ddlMyRequisitions.Items.Insert(0, new ListItem("-All Requisition-", "0"));
            //MiscUtil.PopulateJobPostingList(ddlMyRequisitions, Facade, CurrentMember.Id);
        }

        public void FillRequisitionList(int memberId)
        {

            //System.Collections.ArrayList arr = Facade.GetAllJobPostingListByStatusId(StatusId);
            System.Collections.ArrayList arr = Facade.GetAllJobPostingListBymemberId(memberId);
            JobPosting j = new JobPosting();
            j.Id = 0;
            j.JobTitle = "-All Requisition-";
            arr.Insert(0, j);
            ddlMyRequisitions.DataSource = arr;
            ddlMyRequisitions.DataTextField = "JobTitle";
            ddlMyRequisitions.DataValueField = "Id";
            ddlMyRequisitions.DataBind();
            //ddlMyRequisitions.Items.Insert(0, new ListItem("Any", "0"));
            //MiscUtil.PopulateJobPostingList(ddlMyRequisitions, Facade, CurrentMember.Id);

        }


        //protected void ddlMyRequisitionsStatus_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    //try
        //    //{
        //    //    int k1 = int.Parse(ddlMyRequisitionsStatus.SelectedValue);
        //    //    ArrayList vip = Facade.GetAllJobPostingListByStatusId(k1);
        //    //    JobPosting j = new JobPosting();
        //    //    j.Id = 0;
        //    //    j.JobTitle = "-All Requisition-";
        //    //    vip.Insert(0, j);
        //    //    ddlMyRequisitions.DataSource = vip;
        //    //    ddlMyRequisitions.DataTextField = "JobTitle";
        //    //    ddlMyRequisitions.DataValueField = "Id";
        //    //    ddlMyRequisitions.DataBind();
        //    //    Session["Status"] = "PageLoad";
        //    //    Session["Matrix"] = "0";
        //    //    fillctr();
        //    //    ddlMyRequisitions.Focus();
        //    //}
        //    //catch { }
           
        //}
}

}
