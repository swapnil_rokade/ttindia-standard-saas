﻿<%--
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MyRequisitionList.aspx
    Description: This is the page which is used to display the Requisation list specific to the users
    Created By: 
    Created On:
    Modification Log:
    -------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    -------------------------------------------------------------------------------------------------------------------------------------------
    0.1             29-Jan-2009          Sandeesh         Defect Id: 9766; Added table alias name to the column 'City' for enabling sorting 
    0.2             04-Feb-2009          Jagadish         Defect id: 9606; Changes made for Listview 'lsvJobPosting' and ObjectDataSource 'odsJobPostingList'.
    0.3             15-MAY-2009          Nagarathna       Defect id:10190;  sort expression is added to REQ Code Column.
    0.4             Jan -20-2010        Basavaraj              Defect Id 8879;Defect Id 12901; Changed the style attribute.
-------------------------------------------------------------------------------------------------------------------------------------------
--%>
<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="MyRequisitionList.aspx.cs" Inherits="TPS360.Web.UI.Requisition.MyRequisitionList" Title="My Requisition List"  EnableEventValidation ="false"  %>
<%@ Register Src ="~/Controls/RequisitionList.ascx" TagName ="Requisition" TagPrefix ="ucl"%>
<asp:Content ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
My Requisition List
</asp:Content>
<asp:Content ID="cntBasicInfoEditor" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <asp:UpdatePanel ID="pnlRequisition" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <ucl:Requisition ID="uclRequisition" runat ="server" />
        </ContentTemplate>                               
    </asp:UpdatePanel>
</asp:Content>