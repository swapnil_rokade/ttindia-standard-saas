﻿ <%--
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MasterRequisitionList.aspx
    Description: This page is used to display the Requisition list.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.          Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Apr-03-2009      Sandeesh            Defect ID:9225; Changed the div tag to display the horizontal scrollbar when the listview content overflows
    0.2              Apr-20-2009      Shivanand           Defect #10222; New columns added "Level III" and "Final Hired".
    0.3              Apr-16-2010      Sudarshan.R.        Defect Id:12520; Changed sort command in btnCity from [S].[Name] to [J].[City]
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>
<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="MasterRequisitionList.aspx.cs" Inherits="TPS360.Web.UI.Requisition.MasterRequisitionList" Title="Master Requisition List" EnableEventValidation ="false"  %>
<%@ Register Src ="~/Controls/RequisitionList.ascx" TagName ="Requisition" TagPrefix ="ucl"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
Master Requisition List
</asp:Content>
<asp:Content ID="cntRequisitionList" ContentPlaceHolderID="cphHomeMaster" runat="Server">

<asp:UpdatePanel ID="pnlRequisition" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
<ucl:Requisition ID="uclRequisition" runat ="server" />
</ContentTemplate>                               
                               </asp:UpdatePanel>

</asp:Content>
