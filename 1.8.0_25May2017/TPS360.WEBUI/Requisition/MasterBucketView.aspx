﻿<%--
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: BasicInfoEditor.ascx
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
     0.1           23/Feb/2016         pravin khot          Code added/replace by pravin khot on 23/Feb/2016 new code ,
                                                            DIV ID ADDED DIVID= [divworkflowarea, divpreviewarea]
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
--%>

<%@ Page Language="C#" MasterPageFile="~/Default-NoSection.master" EnableEventValidation="false"
    EnableViewState="true" AutoEventWireup="true" CodeFile="MasterBucketView.aspx.cs"
    Inherits="TPS360.Web.UI.Requisition.BucketView" Title="Master Bucket View"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<%@ Register Src ="~/Controls/MultipleItemPicker.ascx" TagName ="MultipleSelection" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/BucketViewGrid.ascx" TagName="BucketViewGrid" TagPrefix="ucl" %>


<asp:Content ID="cntBasicInfoTitle" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">

<style>
    .dropdown-menu:after
    {
        border-bottom: 7px solid  #FFFFFF;
        border-left: 7px solid transparent;
        border-right: 7px solid transparent;
        content: "";
        display: inline-block;
        left: 80%;
        position: absolute;
        top: -6px;
    }
    .dropdown-menu::before
    {
        position: absolute;
        top: -7px;
        left: 80%;
        display: inline-block;
        border-right: 7px solid transparent;
        border-bottom: 7px solid #CCC;
        border-left: 7px solid transparent;
        border-bottom-color: rgba(0, 0, 0, 0.2);
        content: '';
    }
    .pnlHide
    {
    }
    .pnlHide:first-child
    {
        overflow: hidden;
    }
</style>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
    
    <script type="text/javascript">
        document.body.onclick = function(e) {
            $('#ctl00_cphHomeMaster_uclRequisition_pnlSearchBoxContent').css({ 'overflow-y': 'inherit', 'overflow-x': 'inherit' });
            $('#ctl00_cphHomeMaster_uclRequisition_pnlSearchBoxContent div:first').css({ 'overflow-y': 'inherit', 'overflow-x': 'inherit' });
        }
        //To Keep the scroll bar in the Exact Place
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
        function PageLoadedReq(sender, args) {
            try {
                var hdnScroll = document.getElementById('ctl00_cphHomeMaster_uclRequisition_hdnScrollPos');
                var bigDiv = document.getElementById('bigDiv');
                bigDiv.scrollLeft = hdnScroll.value;
                var background = $find("mpeModal")._backgroundElement;
                background.onclick = function() {
                    CloseModal();
                }
            }
            catch (e) {
            }
        }
        function onShownManagers() {
            var background = $find("MPEManagers")._backgroundElement;
            background.onclick = function() { $find("MPEManagers").hide(); }
        }
        function SetScrollPosition() {
            var hdnScroll = document.getElementById('ctl00_cphHomeMaster_uclRequisition_hdnScrollPos');
            var bigDiv = document.getElementById('bigDiv');
            hdnScroll.value = bigDiv.scrollLeft;
        }


        function RefreshList() {
            var hdnDoPostPack = document.getElementById('<%=hdnDoPostPack.ClientID %>');
            hdnDoPostPack.value = "1";
            __doPostBack('ctl00_cphHomeMaster_uclRequisition_upcandidateList', ''); return false;
        }
        $('#ctl00_cphHomeMaster_uclRequisition_lsvJobPosting_lnkNoOfOpening').mouseover(function() {
            //$('#ctl00_cphHomeMaster_uclRequisition_lsvJobPosting_lnkNoOfOpening').append('<div>Handler for .mouseover() called.</div>');
            console.log('asdsaweqw');
            alert('asdasd');
        });

</script>

  
      <asp:HiddenField ID="hdnDoPostPack" runat="server" />
       <div class="section>
      
      <div class="section-body">
 <asp:UpdatePanel ID="upBucketView" runat="server">
        <ContentTemplate>
        
            <asp:Label ID="Label1" runat="server" EnableViewState="false" Style="z-index: 9999"></asp:Label>
           <div id="Div1" style="width: 96.9%; overflow: hidden; margin-left: 15px;">
              <div class="TitleContainer" style="width: 100%; border: thin  #f2f2f2" ><h3 style="margin-left: 3px; margin-top:6px; margin-bottom:6px; color: #555555;">MASTER RECRUITMENT DASHBOARD</h3></div>
           <div style="border: thin  #f2f2f2">
            
            <asp:Panel ID="pnlSearchBoxBody" runat="server" CssClass="well well-small nomargin"
                DefaultButton="btnSearch">
                <asp:Panel ID="pnlSearchRegion" runat="server">
                    <asp:CollapsiblePanelExtender ID="cpeSearch" runat="server" TargetControlID="pnlSearchBoxContent"
                        ExpandControlID="pnlSearchBoxHeader" CollapseControlID="pnlSearchBoxHeader" CollapsedImage="~/Images/expand-plus.png"
                        ExpandedImage="~/Images/collapse-minus.png" Collapsed="true" ImageControlID="imgSearchBoxToggle"
                        EnableViewState="true" SuppressPostBack="True">
                    </asp:CollapsiblePanelExtender>
                    <asp:Panel ID="pnlSearchBoxHeader" runat="server" >
                        <div class="SearchBoxContainer">
                            <div class="SearchTitleContainer">
                                <div class="ArrowContainer">
                                    <asp:ImageButton ID="imgSearchBoxToggle" runat="server" ImageUrl="~/Images/expand-plus.png"
                                        AlternateText="collapse" />
                                </div>
                                <div class="TitleContainer" style="color: #555555">
                                    Filter Options
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlSearchBoxContent" runat="server" CssClass ="filter-section"  Style="" Height="0">
                        <%--  <div class="BoxContainer" >--%>
                        <div class="TableRow spacer">
                            <div class="FormLeftColumn" style="width: 50%;">
                              <%-- <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="Label2" runat="server" Text="Requisition Status"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlMyRequisitionsStatus" runat="server" 
                                            CssClass="CommonDropDownList" AutoPostBack="false" 
                                            onselectedindexchanged="ddlMyRequisitionsStatus_SelectedIndexChanged" >
                                            
                                        </asp:DropDownList>
                                      
                                    </div>
                                </div>--%>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblMyRequisitions" runat="server" Text="Requisitions"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlMyRequisitions" runat="server" 
                                                CssClass="CommonDropDownList" AutoPostBack="False" >
                                        </asp:DropDownList>
                                      
                                    </div>
                                </div>
                             <%--     
                               <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblCreatorName" runat="server" Text="Requisition Creator"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlReqCreator" runat="server" 
                                            CssClass="CommonDropDownList" AutoPostBack="false" >
                                          
                                            
                                        </asp:DropDownList>
                                      
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="ldlRecruiter" runat="server" Text="Recruiter"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlRecruiter" runat="server" 
                                            CssClass="CommonDropDownList" AutoPostBack="false" >
                                          
                                            
                                        </asp:DropDownList>
                                      
                                    </div>
                                </div>
                                --%>
                                </div>
                                 <div class="FormRightColumn" style="width: 50%; height: 150px;">
                               <%--   <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblCandiName" runat="server" Text="Candidate Name"></asp:Label>:
                        </div>
                       <div class="TableFormContent">
                           <asp:TextBox ID="txtCandiName" CssClass="SearchTextbox" runat="server"></asp:TextBox>
                                      
                                    </div>
                                </div>
                                <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblCandiMail" runat="server" Text="Candidate Email"></asp:Label>:
                        </div>
                       <div class="TableFormContent">
                           <asp:TextBox ID="txtCandiMail" CssClass="SearchTextbox" runat="server"></asp:TextBox>
                                      
                                    </div>
                                </div>--%>
                         <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblJobStatusHeader" runat="server" Text="Hiring Status"></asp:Label>:
                        </div>
                          <div class="TableFormContent"  >
                          <div style="z-index: 9999;   position: absolute; margin-left: 20%;">
                        <ucl:MultipleSelection ID="ddlJobStatus" runat ="server"  />
                        </div>
                        </div>
                        
                     
                       <%--     <asp:DropDownList EnableViewState="true" ID="ddlJobStatus" runat="server" CssClass="CommonDropDownList"
                                AutoPostBack="false">
                            </asp:DropDownList>--%>
                     
                        <%-- <div style="height: 200px"></div>--%>
                      
                                </div>
                                </div>
                                
                                
                              <%--  <div style="z-index: 9999; height: 170px; overflow: auto; visibility: visible; width: 200px;">
                                </div>--%>
                                
                             <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px">
                            <asp:LinkButton ID="btnSearch" runat="server" AlternateText="Search" CssClass="btn btn-primary"
                                ValidationGroup="RequisitionList" EnableViewState="false" OnClick="btnSearch_Click"
                                CausesValidation="false"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                                
                                  <asp:LinkButton ID="btnClear" runat="server" AlternateText="Clear" CssClass="btn btn-primary"
                                ValidationGroup="RequisitionList" EnableViewState="false" OnClick="btnClear_Click"
                                CausesValidation="false"><i class=""></i> Clear</asp:LinkButton>
                                
                           <%--    <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                                EnableViewState="false" OnClick="btnClear_Click" />--%>
                        </div>
                    
                            </div>
                         
                           <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px">
                               <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
                        </div>
                        
                        <%--            </div>--%></asp:Panel>
                </asp:Panel>
            </asp:Panel>
            </div>
            
            
                <div class="section">
                    <div class="section-body">
                        <div class="section-body" data-target="#reqNavScroll" data-spy="scroll">
                        
                            <div id="Div2" runat="server">
                         
                            </div>
                       
                        </div>
                        <asp:PlaceHolder ID = "PlaceHolder_1" runat="server">
                        
                        </asp:PlaceHolder>
                         <br />
                        </div>
                    </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    </div>
    
</asp:Content>
