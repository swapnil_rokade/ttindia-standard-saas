﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MasterRequisitionList.aspx.cs
    Description: This is the page which is used to display the Requisation list 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Nov-27-2008          Gopala Swamy          Defect id: 9336;Added one line and modified functional prototype
    0.2             Dec-03-2008          Yogeesh Bhat          Defect ID: 9396: Changes made in lsvJobPosting_ItemDataBound() method
    0.3             Dec-09-2008          Yogeesh Bhat          Defect ID: 9398 Rework: Changes made in lsvJobPosting_ItemCommand() event
    0.4             Dec-18-2008          Yogeesh Bhat          Defect Id: 9529,9530,9531,9532: Added/Removed specified fields in 'lsvJobPosting' listview
    0.5             Feb-10-2009          Yogeesh Bhat          Defect Id: 9909: Changes made in lsvJobPosting_ItemDataBound()
 *  0.6             Apr-01-2009          Rajendra Prasad       Defect Id: 9380: Added one new Session.
    0.7             Apr-20-2009          Shivanand             Defect #10222; In the method lsvJobPosting_ItemDataBound(), Hyperlink is provided for new columns LevelIII & FinalHired.
 *  0.8             May-12-2009          Sandeesh              Defect id:10440 :Populated the Requisition status dropdown list from the database
 *  0.9             Aug-6-2009           Yogeesh Bhat          Defect Id:10958; Changes made in lsvJobPosting_ItemCommand();
    1.0             Aug-26-2009          Nagarathna V.B        Defect Id:10540;providing the user based access to requisition move.
    1.1             Sep-16-2009             Veda               Defect Id:11499;Assigned Recruiters will not retained onclicking 'Create New From Existing' icon.  
 *  1.2             Nov-27-2009          Gopala Swamy J        Defect Id:11588; Changed functions prototype
 *  1.3             Mar-26-2010          Basavaraj Angadi      Defect Id: 12382 and 12383 ,Changes made to PagerMethod , for DataPager Controls 
 *  1.4             Mar-30-2010          Sudarshan.R.          Defect ID: 12399. Added ScriptManager Proxy and MyScriptManager_Navigate event to enable paging on browser back button click.
 *  1.5             Apr-16-2010          Sudarshan.R.          Defect ID:12520  Changed the Location display to City + State.
-------------------------------------------------------------------------------------------------------------------------------------------       

*/
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using TPS360.BusinessFacade;
namespace TPS360.Web.UI.Requisition
{
    public partial class MasterRequisitionList : RequisitionBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ObjectDataSource odsRequisitionList = (ObjectDataSource)uclRequisition.FindControl("odsRequisitionList");

            odsRequisitionList.SelectParameters["IsCompanyContact"].DefaultValue = "false";
        }
        [System.Web.Services.WebMethod]
        public static object[] ChangeRequisitionStatus(object data)
        {
            IFacade Facade = new Facade();
            Dictionary<string, object> param =
                  (Dictionary<string, object>)data;
            try
            {
                JobPosting jobPosting = Facade.GetJobPostingById(Convert .ToInt32 (param ["RequisitionId"]));
                jobPosting.JobStatus = Convert.ToInt32(param["StatusId"]);
                jobPosting.UpdatorId = Convert.ToInt32(param["UpdatorId"]);
                ApplicationWorkflow applicationWorkflow = Facade.GetApplicationWorkflowByTitle(WorkFlowTitle.RequisitionManagementShouldSelectedApprovedUsersAbilityToChangeStats.ToString());
                if (applicationWorkflow != null && applicationWorkflow.RequireApproval)
                {
                    IList<ApplicationWorkflowMap> applicationWorkflowMapList = Facade.GetAllApplicationWorkflowMapByApplicationWorkflowId(applicationWorkflow.Id);
                    if (applicationWorkflowMapList != null && applicationWorkflowMapList.Count > 0)
                    {
                        foreach (ApplicationWorkflowMap applicationWorkflowMap in applicationWorkflowMapList)
                        {
                            if (applicationWorkflowMap.MemberId == Convert.ToInt32(param["UpdatorId"]))
                            {
                                Facade.UpdateJobPosting(jobPosting);
                                MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionStatusChange, jobPosting.Id, Convert.ToInt32(param["UpdatorId"]), Facade);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    Facade.UpdateJobPosting(jobPosting);
                    MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionStatusChange, jobPosting.Id, Convert.ToInt32(param["UpdatorId"]), Facade);
                    string[] result = { "Changed", "" };
                    return result;
                }
                
            }
            catch (ArgumentException ax)
            {
               
            }
            string[] result1 = { "Changed", "" };
            return result1;
        }
       
    }

}

