﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using System.Web.Configuration;
namespace TPS360.Web.UI
{
    public partial class PasswordReset : BasePage
    {

        private int  _ResetRequestId = 0;
        public int CurrentRequestId
        {
            get
            {

                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants .PORTAL_PASSWORDRESET_ID ]))
                    {
                        _ResetRequestId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PORTAL_PASSWORDRESET_ID]);
                    }


                    return _ResetRequestId;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Helper.Url.Redirect(UrlConstants.Dashboard .DEFAULT_PAGE    );

        }

        int noofallowedusers()
        {
            int allowedusers = 0;
            TPS360SectionHandler licenseSection = WebConfigurationManager.GetSection("tps360license") as TPS360SectionHandler; ;
            if (licenseSection != null)
            {
                try
                {
                    allowedusers = Convert.ToInt32(licenseSection.Details["NoofUsersAllowed"].Value);
                }
                catch { }
            }
            return allowedusers;
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            bool isvalid = false;
            TPS360.Common.BusinessEntities.PasswordReset reset = new TPS360.Common.BusinessEntities.PasswordReset();
            reset = Facade.GetPasswordResetById(CurrentRequestId);
            if (reset != null)
            {
                if (reset.IsValid == 1 && DateTime.Now <= reset.ExpiredDate)
                {
                    Member _member = Facade.GetMemberById(reset.MemberID);
                    if (_member != null)
                    {
                        try
                        {
                            isvalid = true;

                            MembershipCreateStatus status;
                            MembershipUser user = Membership.GetUser(_member.PrimaryEmail);
                            user.UnlockUser();
                            user.ChangePassword(user.GetPassword(), txtConfirmPassword.Text);
                            reset.IsValid = 0;
                            Facade.UpdatePasswordReset(reset);
                            FormsAuthentication.SetAuthCookie(user.UserName, true);
                            #region LoginDetailsCheck
                            string varchrDomainName = GettingCommonValues.GetDomainName(); //Request.ServerVariables["HTTP_HOST"];



                            int i = Facade.EntryofUserAccessApp(user.UserName, Session.SessionID, "TPS", varchrDomainName, "", noofallowedusers(),Request.UserHostName);
                            SecureUrl urlLogin;
                            if (i == -5)
                            {
                                urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
                                Response.Cookies["LoginStatus"].Value = "User login not permitted from IP : " + Request.UserHostName;// "Maximum number of users reached";
                                Helper.Url.Redirect(urlLogin.ToString());
                                return;
                            }
                            else if (i == -7)
                            {
                                urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
                                Response.Cookies["LoginStatus"].Value = "Only one session is allowed per browser.";// "Maximum number of users reached";
                                Helper.Url.Redirect(urlLogin.ToString());
                                return;
                            }
                             else if (i == -3)
                            {
                                urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
                                Response.Cookies["LoginStatus"].Value = "Number of concurrent users allowed by license has been met. <br> Please try again after another user logs off.";// "Maximum number of users reached";
                                Helper.Url.Redirect(urlLogin.ToString());
                            }
                            else if (i == -2)
                            {

                                urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_RESET, "", "UserID", user.UserName);
                                Response.Cookies["LoginStatus"].Value = "User already logged in";
                                Helper.Url.Redirect(urlLogin.ToString());
                            }
                            else if (i == 0)
                            {
                                FormsAuthentication.SetAuthCookie(user.UserName, true);
                                if ((Request.Browser.Cookies))
                                {
                                    Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(30);
                                    Response.Cookies["PBLOGIN"]["UNAME"] = base.CurrentUser.UserName;
                                    Response.Cookies["PBLOGIN"]["UPASS"] = "";
                                }


                        // Delete the cookies
                                else if (Request.Cookies["PBLOGIN"] != null)
                                {
                                    HttpCookie aCookie;
                                    string cookieName;

                                    cookieName = Request.Cookies["PBLOGIN"].Name;
                                    aCookie = new HttpCookie(cookieName);
                                    aCookie.Expires = DateTime.Now.AddDays(-1);
                                    Response.Cookies.Add(aCookie);

                                }
                                Session["Loggedin"] = "Yes";
                                //Response.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
                                Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE, string.Empty, UrlConstants.PARAM_MSG, "Password reset successfully.");
                            }
                           
                            #endregion
//                            Helper.Url.Redirect(UrlConstants.Dashboard .DEFAULT_PAGE,string .Empty ,UrlConstants .PARAM_MSG ,"Password reset successfully." );
                        }
                        catch
                        {
                            isvalid = false;
                        }
                    }
                }
            }
            if (!isvalid)
            {
                divReset.Visible = false;
                divResetFailed.Visible = true;
            }
        }
    }
}
