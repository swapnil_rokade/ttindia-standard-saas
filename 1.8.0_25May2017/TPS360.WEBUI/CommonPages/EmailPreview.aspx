﻿<%@ Page Language="C#" MasterPageFile="~/Popup.master" ValidateRequest="false" EnableViewState="false"
 EnableEventValidation="false" AutoEventWireup="true" CodeFile="EmailPreview.aspx.cs" Inherits="TPS360.Web.UI.CommonPages.EmailPreview" Title="Email Preview" %>


    
<asp:content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:content>
<asp:content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
</asp:content>
<asp:content ID="Content3" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <asp:label ID="lblMessage" runat="server" EnableViewState ="false" ></asp:label>
    <div class="MidBodyBoxRow" style="text-align: center; padding: 20px;">
        <div class="MainBox" style="width: 99%;">
            <div class="BoxHeader">
                <div class="BoxHeaderContainer">
                    <div class="TitleBoxLeft">
                        <asp:Image  EnableViewState="false" ID="imgArrow" runat="server" SkinID="sknArrow" AlternateText="" />
                        </div>
                    <div class="TitleBoxMid">
                        Email Preview
                    </div>
                </div>
            </div>
            <div class="HeaderDevider">
            </div>
            <div  class="MasterBoxContainer" >
                <div class="ChildBoxContainer">
                    <div style="clear: both">
                    </div>
                    <div class="BoxContainer" style ="overflow :auto" >
                             <div style ="width :100%; white-space :nowrap " >
                                     <table class="TableFormLeble" style ="width :100%">
                                         <tr style ="width :100%">
                                <td>
                                    <div class="TableFormLeble" style="width: 80px; text-align: left;">
                                        <asp:label ID="lblDateAndTime" runat="server" Text="Date/Time"></asp:label>
                                    </div>
                                </td>
                                <td style ="width :20%">
                                    <div class="TableFormContent" id="DateAndTime" runat="server">
                                    </div> 
                                </td>
                                </tr>
                                         <tr style ="width :100%">
                                <td>
                                    <div class="TableFormLeble" style="width: 80px; text-align: left;">
                                        <asp:label ID="lblFrom" runat="server" Text="From"></asp:label>
                                    </div>
                                </td>
                                <td style ="width :20%">
                                    <div class="TableFormContent" id="From" runat="server"></div> 
                                </td>
                                </tr>
                                         <tr style ="width :100%">
                                <td>
                                <div class="TableFormLeble" style="width: 80px; text-align: left;">
                                        <asp:label ID="lblTo" runat="server" Text="To"></asp:label>
                                    </div>
                                </td>
                                <td style ="width :20%">
                                <div class="TableFormContent" id="To" runat="server"></div> 
                                </td>
                                </tr>
                                         <tr style="width: 100%; text-align: left;" runat="server" id ="tr1">
                                <td>
                                        <div class="TableFormLeble" style="width: 80px; text-align: left;">
                                        <asp:label ID="lblCC" runat="server" Text="CC"></asp:label>
                                    </div>
                                </td>
                                <td style ="width :20%" >
                                <div class="TableFormContent" id="CC" runat="server"></div> 
                                </td>
                                </tr>
                                         <tr style="width: 100%; text-align: left;" runat ="server" id ="tr2" >
                                <td>
                                        <div class="TableFormLeble" style="width: 80px; text-align: left;">
                                        <asp:label ID="lblBCC" runat="server" Text="BCC"></asp:label>
                                    </div>
                                </td>
                                <td style ="width :20%">
                                 <div class="TableFormContent" id="BCC" runat="server"></div>
                                </td>
                                </tr>
                                         <tr style ="width :100%">
                                <td align = "left">
                                <div class="TableFormLeble" style="width: 80px; text-align: left;">
                                        <asp:label ID="lblSubject" runat="server" Text="Subject"></asp:label>
                                    </div>
                                </td>
                                <td style ="width :20%;vertical-align :top;">
                                    <div class="TableFormContent" id="Subject" runat="server">
                                    </div>
                                </td>
                                </tr>
                                         <tr style="width:100%; text-align: left; white-space :nowrap ; width :100%" runat ="server" id ="tr3" >
                                <td>
                                        <div class="TableFormLeble" style="width: 80px; text-align: left;">
                                        <asp:label ID="lblAttachments" runat="server" Text="Attachment(s)"></asp:label>
                                    </div>
                                </td>
                                <td id ="tt" runat ="server"  style =" white-space: nowrap; vertical-align :top; text-align: left; width :85%"  >
                                <div id ="dd" runat ="server"  style =" white-space: nowrap; width:100%;" >
                                        
                                 </div>
                                </td>
                                </tr>
                                         <tr style ="width :100%">
                                <td style ="vertical-align :top ;">
                                <div class="TableFormLeble" style="width:85px; text-align: left; ">
                                        <asp:label ID="lblEmailDescription" runat="server" Text="Email Body"></asp:label>
                                    </div>
                                </td>
                                <td style ="width :85%; vertical-align :top ;">
                                <div class="TableFormContent" id="EmailDescription" runat="server" style =" white-space :normal ; overflow :auto " ></div>
                                </td>
                                </tr>
                                    </table>
                               </div>
                    </div> 
                        <asp:Image  EnableViewState="false" ID="imgRightTopImage" runat="server" SkinID="sknRightTopImage" AlternateText="" />
                        <asp:Image  EnableViewState="false" ID="imgBoxLeftBottom" runat="server" SkinID="sknBoxLeftBottom" AlternateText="" />
                        <asp:Image  EnableViewState="false" ID="imgBoxRightBottom" runat="server" SkinID="sknBoxRightBottom" AlternateText="" />
                </div> 
            </div>
        </div>
    </div>

    </asp:content> 


