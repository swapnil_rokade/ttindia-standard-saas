﻿<%@ Page Language="C#" MasterPageFile="~/Popup.master" AutoEventWireup="true" CodeFile="GlobalSearchResult.aspx.cs"
    Inherits="TPS360.Web.UI.GlobalSearchResult" Title="Search Results" %>
<%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="ContentHome" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <asp:UpdatePanel ID="upGlobalSearch" runat="server">
        <ContentTemplate>
        <asp:TextBox ID="txtSortColumn_Candidate" runat ="server" Visible ="false"  ></asp:TextBox>
        <asp:TextBox ID="txtSortOrder_Candidate" runat ="server" Visible ="false"  ></asp:TextBox>
        <asp:TextBox ID="txtSortColumn_Company" runat ="server" Visible ="false"  ></asp:TextBox>
        <asp:TextBox ID="txtSortOrder_Company" runat ="server" Visible ="false"  ></asp:TextBox>
        <asp:TextBox ID="txtSortColumn_Employee" runat ="server" Visible ="false"  ></asp:TextBox>
        <asp:TextBox ID="txtSortOrder_Employee" runat ="server" Visible ="false"  ></asp:TextBox>
        
        <asp:TextBox ID ="SortColumn_Candidate" runat ="server"  EnableViewState ="true"  Visible ="false" />
        <asp:TextBox ID ="SortOrder_Candidate" runat ="server"  EnableViewState ="true" Visible ="false"  />
        <asp:TextBox ID ="SortColumn_Company" runat ="server"  EnableViewState ="true"  Visible ="false" />
        <asp:TextBox ID ="SortOrder_Company" runat ="server"  EnableViewState ="true" Visible ="false"  />
        <asp:TextBox ID ="SortColumn_Employee" runat ="server"  EnableViewState ="true"  Visible ="false" />
        <asp:TextBox ID ="SortOrder_Employee" runat ="server"  EnableViewState ="true" Visible ="false"  />

        <asp:HiddenField ID="hdnTotalCandidateCount" runat="server" value=""/>
        <asp:HiddenField ID="hdnTotalCompanyCount" runat="server" value=""/>
        <asp:HiddenField ID="hdnTotalEmployeeCount" runat="server" value=""/>
            <div class="TableRow" style="text-align: left; margin-left: 20px;">
                <asp:Label ID="lblSearchMessage" runat="server"></asp:Label>
                <asp:Label ID="lblSearchKey" runat="server" Font-Bold="true"></asp:Label>
            </div>
            <div class="TableRow" style="text-align: left; margin-left: 20px;">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="SearchTextbox" Width="250"></asp:TextBox>
                <asp:Button ID="btnGlobalSearch" runat="server" Text="Search" CssClass="CommonButton"
                    OnClick="btnGlobalSearch_Click" />
            </div>
            <div style="width: 90%;">
                <div class="CommonSubTitle" id="candidateSubTitle" runat="server" style="margin-left: 20px;
                    text-align: left;">
                    <asp:Label runat="server" ID="lblCandidateTotal" Text="0"/>
                </div>
                <div class="TableRow" style="margin-left: 20px;">
                    <div class="GridContainer" id="candidateList" runat="server">
                        <asp:ObjectDataSource runat="server" ID="odsCandidateQuickSearch" SelectMethod="GetPagedForQuickSearch"
                            TypeName="TPS360.Web.UI.CandidateDataSource" SelectCountMethod="GetListCounts"
                            EnablePaging="true" SortParameterName="sortExpression" 
                            >
                            <SelectParameters>
                                <asp:Parameter DefaultValue="" Name="quickSearchKeyWord" />
                                <asp:Parameter DefaultValue ="" Name ="CreatorId" />
                                <asp:Parameter Name ="SortOrder" DefaultValue ="asc" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        
                        <asp:UpdatePanel ID ="pnlCandidate" runat ="server" UpdateMode ="Conditional"  >
                        <ContentTemplate >
                            <asp:ListView ID="lsvCandidate" runat="server" DataKeyNames="Id" OnItemDataBound="lsvCandidate_ItemDataBound" OnPreRender="lsvCandidate_PreRender"
                            OnItemCommand="lsvCandidate_ItemCommand" >
                            <LayoutTemplate>
                                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                    <tr>
                                        <th style =" width :25%"><asp:LinkButton ID="btnName" runat="server" ToolTip="Sort By Name" CommandName="Sort" CommandArgument="[C].[FirstName]" Text="Name" /></th>
                                        <th style =" width :25%"><asp:LinkButton ID="btnPosition" runat="server" ToolTip="Sort By Position" CommandName="Sort" CommandArgument="[C].[CurrentPosition]" Text="Position" /></th>
                                        <th style =" width :25%">Mobile</th>
                                        <th style =" width :25%"><asp:LinkButton ID="btnEmail" runat="server" ToolTip="Sort By Position" CommandName="Sort" CommandArgument="[C].[PrimaryEmail]" Text="Email" /></th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                    <tr class="Pager">
                                        <td colspan="4">
                                             <ucl:Pager Id="pagerControl_Candidate" runat ="server" EnableViewState ="true"  />
                                        </td>
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <EmptyDataTemplate>
                                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                    <tr>
                                        <td>
                                            No search result found.
                                        </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                    <td align="left"><asp:HyperLink ID="lnkCandidateName" runat="server"></asp:HyperLink></td>
                                    <td align="left"><asp:Label ID="lblPosition" runat="server" /></td>
                                    <td align="left"><asp:Label ID="lblCandidateMobile" runat="server" /></td>
                                    <td align="left"><asp:LinkButton ID="lblPrimaryEmail" runat="server" /></td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        
                    </div>
                </div>
                
                <div class="CommonSubTitle" style="margin-left: 20px; text-align: left;" id="companySubTitle"
                    runat="server">
                    <asp:Label runat="server" ID="lblCompanyTotal" Text="0"/>
                </div>
                <div class="TableRow" style="margin-left: 20px;">
                    <div class="GridContainer" id="companyList" runat="server">
                        <asp:ObjectDataSource runat="server" ID="odsCompanyQuickSearch" SelectMethod="GetPagedForQuickSearch"
                            TypeName="TPS360.Web.UI.CompanyDataSource" SelectCountMethod="GetListCounts" EnablePaging="true"
                            SortParameterName="sortExpression" 
                           >
                            <SelectParameters>
                                <asp:Parameter DefaultValue="" Name="quickSearchKeyWord" />
                                <asp:Parameter DefaultValue ="" Name ="CreatorId" />
                                <asp:Parameter Name ="SortOrder" DefaultValue ="asc" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:UpdatePanel ID="pnlCompany" runat ="server" UpdateMode ="Conditional"  >
                        <ContentTemplate >
                              <asp:ListView ID="lsvCompany" runat="server" 
                            OnItemDataBound="lsvCompany_ItemDataBound" onprerender="lsvCompany_PreRender" OnItemCommand="lsvCompany_ItemCommand" EnableViewState ="true" >
                            <LayoutTemplate>
                                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0" >
                                    <tr >
                                        <th style =" width :25%"><asp:LinkButton ID="btnCompanyName" runat="server" ToolTip="Sort By Name" CommandName="Sort" CommandArgument="CompanyName" Text="Company Name" /></th>
                                        <th  style =" width :25%">Office Phone</th>
                                        <th style =" width :25%"><asp:LinkButton ID="btnPrimaryContact" runat="server" ToolTip="Sort By Name" CommandName="Sort" CommandArgument="[CC].[FirstName]" Text=" Primary Contact"  /></th>
                                        <th style =" width :25%"><asp:LinkButton ID="btnEmail" runat="server" ToolTip="Sort By Name" CommandName="Sort" CommandArgument="[CC].[Email]" Text="Email" /></th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                    <tr class="Pager">
                                        <td colspan="4">
                                           <ucl:Pager Id="pagerControl_Company" runat ="server" EnableViewState ="true"  />
                                        </td>
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <EmptyDataTemplate >
                                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server" enableviewstate ="true" >
                                    <tr>
                                        <td>
                                            No search result found.
                                        </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                    <td align="left"><asp:HyperLink ID="lnkCompanyName" runat="server"></asp:HyperLink></td>
                                    <td align="left"><asp:Label ID="lblOfficePhone" runat="server" /></td>
                                    <td align="left"><asp:Label ID="lblPrimaryContact" runat="server" /></td>
                                    <td align="left"><asp:LinkButton ID="lblPrimaryEmail" runat="server" /></td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                        </ContentTemplate>
                        </asp:UpdatePanel>
                      
                    </div>
                </div>
                <div class="CommonSubTitle" style="margin-left: 20px; text-align: left;" id="employeeSubTitle"
                    runat="server">
                    <asp:Label runat="server" ID="lblEmployeeTotal" Text="0"/>
                </div>
                <div class="TableRow" style="margin-left: 20px;">
                    <div class="GridContainer" id="employeeList" runat="server">
                        <asp:ObjectDataSource runat="server" ID="odsEmployeeQuickSearch" SelectMethod="GetPagedForQuickSearch" EnableViewState ="true" 
                            TypeName="TPS360.Web.UI.EmployeeDataSource" SelectCountMethod="GetListCount"
                            EnablePaging="true" SortParameterName="sortExpression"
                           >
                            <SelectParameters>
                                <asp:Parameter DefaultValue="" Name="quickSearchKeyWord" />
                                <asp:Parameter DefaultValue ="" Name ="CreatorId" />
                                <asp:Parameter Name ="SortOrder" DefaultValue ="asc" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:ListView ID="lsvEmployee" runat="server" OnItemDataBound="lsvEmployee_ItemDataBound" OnItemCommand="lsvEmployee_ItemCommand"
                             OnPreRender="lsvEmployee_PreRender">
                            <LayoutTemplate>
                                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                    <tr>
                                        <th style =" width :25%"><asp:LinkButton ID="btnEmployeeName" runat="server" ToolTip="Sort By Name" CommandName="Sort" CommandArgument="FirstName" Text=" Name" /></th>
                                        <th style =" width :25%"><asp:LinkButton ID="btnAccessRole" runat="server" ToolTip="Sort By Name" CommandName="Sort" CommandArgument="[CR].[Name]" Text=" Access Role" /></th>
                                        <th style =" width :25%">Mobile</th>
                                        <th style =" width :25%"><asp:LinkButton ID="btnEmail" runat="server" ToolTip="Sort By Name" CommandName="Sort" CommandArgument="PrimaryEmail" Text="Email" /></th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                    <tr class="Pager">
                                        <td colspan="4">
                                              <ucl:Pager Id="pagerControl_Employee" runat ="server" EnableViewState ="true"  />
                                        </td>
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <EmptyDataTemplate>
                                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                    <tr>
                                        <td>
                                            No search result found.
                                        </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                    <td align="left"><asp:HyperLink ID="lnkEmployeeName" runat="server"></asp:HyperLink></td>
                                    <td align="left"><asp:Label ID="lblPosition" runat="server" /></td>
                                    <td align="left"><asp:Label ID="lblEmployeeMobile" runat="server" /></td>
                                    <td align="left"><asp:LinkButton ID="lblPrimaryEmail" runat="server" /></td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
