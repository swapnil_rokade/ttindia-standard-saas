﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.UI;
using System.Collections;
using System.Text.RegularExpressions;
using System.Text;
namespace TPS360.Web.UI
{
    public partial class TeamBuilder : EmployeeBasePage
    {
        #region Member Variables
        private bool Isdelete = false;
        private static int _teamId = 0;
        private bool IsSave = false;

        #endregion

        #region Properties


        #endregion

        #region Methods
        private void PrepareEditView()
        {

            EmployeeTeamBuilder employeeteam = Facade.EmployeeTeamBuilder_GetByTeamId(Convert.ToInt32(hdnSelectedIDtoEdit.Value));
            if (employeeteam != null)
            {
                txtTeamTitleName.Text = employeeteam.Title;
                hdnLeaderID.Value = employeeteam.TeamLeader.ToString(); ;
                ControlHelper.SelectListByValue(ddlTeamLeader, employeeteam.TeamLeader.ToString());
                char[] delim = { ',' };
                string[] emplist = employeeteam.EmployeeId.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                IList<string> elist = new List<string>();
                foreach (string s in emplist)
                    elist.Add(s.Trim());
                IList<JobPostingHiringTeam> team = new List<JobPostingHiringTeam>();
                foreach (ListItem item in lstTeamMembers.Items)
                {
                    if (item.Selected || elist.Contains(item.Value))
                    {
                        JobPostingHiringTeam jobPostingHiringTeam = new JobPostingHiringTeam();
                        jobPostingHiringTeam.MemberId = Convert.ToInt32(item.Value);
                        jobPostingHiringTeam.Name = item.Text;
                        team.Add(jobPostingHiringTeam);
                    }
                }
                lsvAssignedTeam.DataSource = team;
                lsvAssignedTeam.DataBind();
            }
        }


        private EmployeeTeamBuilder BuildEmployeeTeamBuilder()
        {
            EmployeeTeamBuilder et = new EmployeeTeamBuilder();
            et.Title = txtTeamTitleName.Text;
            et.TeamLeader = Convert.ToInt32(ddlTeamLeader.SelectedValue);
            string employeeIs = "";
            foreach (int i in getAssignedRecruitersFromList())
            {
                if (employeeIs != "") employeeIs += ",";
                employeeIs += i;
            }
            et.EmployeeId = employeeIs;
            return et;
        }
        #endregion


        #region Button Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                MiscUtil.PopulateMemberListWithEmailByRole(ddlTeamLeader, ContextConstants.ROLE_EMPLOYEE, Facade);
                MiscUtil.PopulateMemberListWithEmailByRole(lstTeamMembers, ContextConstants.ROLE_EMPLOYEE, Facade);
                lstTeamMembers.Items.RemoveAt(0);
            }
        }
        private void clear()
        {
            txtTeamTitleName.Text = "";
            ddlTeamLeader.SelectedIndex = 0;
            lsvAssignedTeam.DataSource = null;
            lsvAssignedTeam.DataBind();
            lstTeamMembers.SelectedIndex = -1;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            EmployeeTeamBuilder et = new EmployeeTeamBuilder();
            EmployeeTeamBuilder employeeteam = new EmployeeTeamBuilder();
            et = BuildEmployeeTeamBuilder();
            if (hdnSelectedIDtoEdit.Value != "")
                employeeteam = Facade.EmployeeTeamBuilder_GetByTeamId(Convert.ToInt32(hdnSelectedIDtoEdit.Value));
            IList<int> AddedRecruiters = getAssignedRecruitersFromList();
            if (AddedRecruiters.Count > 1)
            {
                if (employeeteam.EmployeeId != null)
                {
                    et.Id = Convert.ToInt32(hdnSelectedIDtoEdit.Value);
                    et.UpdatorId = base.CurrentMember.Id;
                    Facade.UpdateEmployeeTeamBuilder(et);
                    hdnSelectedIDtoEdit.Value = "";
                    MiscUtil.ShowMessage(lblMessage, "User Team updated successfully", false);
                }
                else
                {
                    et.CreatorId = base.CurrentMember.Id;
                    Facade.AddEmployeeTeamBuilder(et);
                    MiscUtil.ShowMessage(lblMessage, "User Team added successfully", false);
                }
                lsvTeams.DataBind();
                IsSave = true;
                clear();
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Select at least one team member to add.", true);
            }

        }
        protected void btnAddTeamMembers_Click(object sender, EventArgs e)
        {
            AddSelectedMemberToTeam(lstTeamMembers, EmployeeType.Internal);
            lstTeamMembers.Focus();
        }

        private void AddSelectedMemberToTeam(ListBox listBox, EmployeeType employeeType)
        {
            bool iserror = false;
            int countSelected = 0;
            int countAdded = 0;
            int memberId = 0;

            IList<int> AddedRecruiters = getAssignedRecruitersFromList();
            IList<JobPostingHiringTeam> team = new List<JobPostingHiringTeam>();
            string message = string.Empty;
            {
                for (int i = 0; i <= listBox.Items.Count - 1; i++)
                {
                    Int32.TryParse(listBox.Items[i].Value, out memberId);
                    if (memberId > 0)
                    {
                        if (listBox.Items[i].Selected || AddedRecruiters.Contains(memberId))
                        {
                            if (listBox.Items[i].Selected) countSelected++;
                            JobPostingHiringTeam jobPostingHiringTeam = new JobPostingHiringTeam();
                            jobPostingHiringTeam.MemberId = memberId;

                            jobPostingHiringTeam.Name = listBox.Items[i].Text;
                            team.Add(jobPostingHiringTeam);
                            if (!AddedRecruiters.Contains(memberId)) countAdded++;

                        }
                    }

                }

                lsvAssignedTeam.DataSource = team;
                lsvAssignedTeam.DataBind();
                if (countSelected > 0)
                {
                    if (countAdded > 0)
                    {
                        int countNotAdded = countSelected - countAdded;
                        if (countNotAdded == 0)
                        {
                            message = "Successfully added all the selected members.";
                        }
                        else
                        {
                            message = "Successfully added " + countAdded + " of " + countSelected + " selected members. The remaining members are already in the list</b></font>";
                        }
                    }
                    else
                    {
                        message = "The selected member(s) are already in the list";
                        iserror = true;
                    }
                }
                else
                {
                    message = "Select at least one member to add.";
                    iserror = true;
                }
            }

            MiscUtil.ShowMessage(lblMessage, message, iserror);
        }

        public IList<int> getAssignedRecruitersFromList()
        {
            IList<int> ReqIds = new List<int>();

            foreach (ListViewDataItem item in lsvAssignedTeam.Items)
            {
                HiddenField hdnMemberID = (HiddenField)item.FindControl("hdnMemberID");
                if (hdnMemberID.Value != "")
                {
                    if (!ReqIds.Contains(Convert.ToInt32(hdnMemberID.Value)))
                        ReqIds.Add(Convert.ToInt32(hdnMemberID.Value));
                }

            }
            return ReqIds;

        }
        protected void lsvAssignedTeam_PreRender(object sender, EventArgs e)
        {
            Isdelete = lsvAssignedTeam.Items.Count >= 1;
            System.Web.UI.HtmlControls.HtmlTableCell thUnAssign = (System.Web.UI.HtmlControls.HtmlTableCell)lsvAssignedTeam.FindControl("thUnAssign");
            if (thUnAssign != null)
                thUnAssign.Visible = Isdelete;

            foreach (ListViewDataItem list in lsvAssignedTeam.Items)
            {
                System.Web.UI.HtmlControls.HtmlTableCell tdUnAssign = (System.Web.UI.HtmlControls.HtmlTableCell)list.FindControl("tdUnAssign");
                if (tdUnAssign != null)
                    tdUnAssign.Visible = Isdelete;
            }

            IList<int> selectedemp = getAssignedRecruitersFromList();
            if (selectedemp.Count == 0)
            {
                lsvAssignedTeam.DataSource = null;
                lsvAssignedTeam.DataBind();
            }
            if (selectedemp.Count != 0 && IsSave == false)
            {
                IList<JobPostingHiringTeam> team = new List<JobPostingHiringTeam>();
                foreach (ListItem item in lstTeamMembers.Items)
                {
                    if (selectedemp.Contains(Convert.ToInt32(item.Value)))
                    {
                        JobPostingHiringTeam jobPostingHiringTeam = new JobPostingHiringTeam();
                        jobPostingHiringTeam.MemberId = Convert.ToInt32(item.Value);
                        jobPostingHiringTeam.Name = item.Text;
                        team.Add(jobPostingHiringTeam);
                    }
                }
                lsvAssignedTeam.DataSource = team;
                lsvAssignedTeam.DataBind();
            }

        }
        protected void lsvAssignedTeam_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPostingHiringTeam jobPostingHiringTeam = ((ListViewDataItem)e.Item).DataItem as JobPostingHiringTeam;

                if (jobPostingHiringTeam != null)
                {

                    HiddenField hdnMemberID = (HiddenField)e.Item.FindControl("hdnMemberID");

                    Label lblMemberName = (Label)e.Item.FindControl("lblMemberName");
                    Label lblMemberEmail = (Label)e.Item.FindControl("lblMemberEmail");

                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    if (jobPostingHiringTeam.MemberId > 0)
                    {
                        hdnMemberID.Value = jobPostingHiringTeam.MemberId.ToString();
                        char[] delimiter = { '[', ']' };
                        string[] name = jobPostingHiringTeam.Name.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

                        if (name.Length == 2)
                        {
                            lblMemberName.Text = name[0];
                            lblMemberEmail.Text = name[1];
                        }
                        else
                        {
                            lblMemberName.Text = jobPostingHiringTeam.Name;
                            lblMemberEmail.Text = jobPostingHiringTeam.PrimaryEmail;
                        }
                        Isdelete = true;
                        btnDelete.Visible = true;
                    }
                    if (jobPostingHiringTeam.MemberId.ToString() == ddlTeamLeader.SelectedValue)
                    {
                        btnDelete.Visible = false;
                    }
                }

            }
        }
        protected void ddlTeamLeader_onselectedchanged(object sender, EventArgs e)
        {
            IList<int> selectedemployee = getAssignedRecruitersFromList();
            if (hdnLeaderID.Value != "")
            {
                if (selectedemployee.Contains(Convert.ToInt32(hdnLeaderID.Value)))
                    selectedemployee.Remove(Convert.ToInt32(hdnLeaderID.Value));
            }
            //if (hdnTL.Value != "")
            //{
            //    selectedemployee.Remove(Convert.ToInt32(hdnTL.Value));
            //}
            hdnLeaderID.Value = ddlTeamLeader.SelectedValue;
            
            if (!selectedemployee.Contains(Convert.ToInt32(ddlTeamLeader.SelectedValue)))
            {
                selectedemployee.Add(Convert.ToInt32(ddlTeamLeader.SelectedValue));
            }
            IList<JobPostingHiringTeam> team = new List<JobPostingHiringTeam>();
            foreach (ListItem item in lstTeamMembers.Items)
            {
                //if (item.Selected || selectedemployee.Contains(Convert.ToInt32(item.Value)))
                if (selectedemployee.Contains(Convert.ToInt32(item.Value)))
                {
                    JobPostingHiringTeam jobPostingHiringTeam = new JobPostingHiringTeam();
                    jobPostingHiringTeam.MemberId = Convert.ToInt32(item.Value);
                    jobPostingHiringTeam.Name = item.Text;
                    team.Add(jobPostingHiringTeam);
                }
            }
            lsvAssignedTeam.DataSource = team;
            lsvAssignedTeam.DataBind();

            lsvTeams.DataBind();

        }

        #endregion

        #region List View Events
        protected void lsvTeams_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                EmployeeTeamBuilder Et = ((ListViewDataItem)e.Item).DataItem as EmployeeTeamBuilder;
                Label lblTitle = (Label)e.Item.FindControl("lblTitle");
                Label lblTeamLeader = (Label)e.Item.FindControl("lblTeamLeader");
                Label lblTeamMembers = (Label)e.Item.FindControl("lblTeamMembers");

                lblTitle.Text = Et.Title;
                lblTeamLeader.Text = Convert.ToString(Et.TeamLeaderName);//== 0 ? "<1" : (Et.TeamLeader > 10 ? ">10" : Et.TeamLeader + "");
                lblTeamMembers.Text = Et.TeamMemberList;// Et.EmployeeId == 1 ? "" : (Et.EmployeeId + "");
                if (lblTeamMembers.Text.Trim().EndsWith(","))
                {
                    lblTeamMembers.Text = lblTeamMembers.Text.Trim().Substring(0, (lblTeamMembers.Text.Trim().Length - 1));

                }
                ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDeleteList");
                btnDelete.OnClientClick = "return ConfirmDelete('team');";
                btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(Et.Id);
                if (Page.Request.Url.AbsoluteUri.ToString().Contains("Overview"))
                {
                    System.Web.UI.HtmlControls.HtmlTableCell table = (System.Web.UI.HtmlControls.HtmlTableCell)lsvTeams.FindControl("thAction");
                    System.Web.UI.HtmlControls.HtmlTableCell tabledata = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAction");
                    table.Visible = false;
                    tabledata.Visible = false;

                }


            }

        }

        protected void lsvTeams_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvTeams.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("Pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null)
                        ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "TeamsRowPerPage";
            }

            PlaceUpDownArrow();
            if (lsvTeams.Items.Count == 0)
            {
                lsvTeams.DataSource = null;
                lsvTeams.DataBind();
            }
        }

        protected void lsvTeams_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                    if (hdnSortColumnName.Value == string.Empty || hdnSortColumnName.Value != e.CommandArgument.ToString())
                    {
                        hdnSortColumnOrder.Value = "asc";

                    }
                    else
                    {
                        hdnSortColumnOrder.Value = hdnSortColumnOrder.Value.ToLower() == "asc" ? "desc" : "asc";

                    }
                    hdnSortColumnName.Value = e.CommandArgument.ToString();

                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    _teamId = id;
                    hdnSelectedIDtoEdit.Value = id.ToString();
                    PrepareEditView();
                    txtTeamTitleName.Focus();
                    Page.ClientScript.RegisterStartupScript(typeof(Page), "MoveTop", "<script>self.moveTo(0,-300); </script>");
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteEmployeeTeamBuilderById(id))
                        {
                            lsvTeams.DataBind();

                            MiscUtil.ShowMessage(lblMessage, "Team successfully deleted.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }

        }

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvTeams.FindControl(hdnSortColumn.Value);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        #endregion
    }
}