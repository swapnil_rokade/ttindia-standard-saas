﻿using System;
using TPS360.Common.Helper;

namespace TPS360.Web.UI
{
    public partial class EmployeeInternalNotesAndActivities : EmployeeBasePage
    {
        # region Member Variables

        int _memberId = 0;

        #endregion

        # region event

        protected void Page_Load(object sender, EventArgs e)
        {
            
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                }
                if (_memberId > 0)
                {
                    string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                    this.Page.Title = name + " - " + "Notes And Activities";
                }
            
        }
        # endregion
    }
}
