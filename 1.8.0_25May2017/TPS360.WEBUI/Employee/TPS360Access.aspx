﻿<%@ Page Language="C#" MasterPageFile="~/Masters/EmployeeProfile.master" AutoEventWireup="true" ValidateRequest="false"
    CodeFile="TPS360Access.aspx.cs" Inherits="TPS360.Web.UI.TPS360AccessControl"
    Title="TPS360 Access " %>

<%@ Register Src="~/Controls/EmailEditor.ascx" TagName="EmailEditor" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AccessControl.ascx" TagName="AccessControl" TagPrefix="ucl" %>
<%@ Register Src="../Controls/MenuSelector.ascx" TagName="MenuSelector" TagPrefix="uc1" %>
<%@ Register Src="../Controls/ChangePassword.ascx" TagName="NewPassword" TagPrefix="uc2" %>
<asp:Content ID="cntTPS360Access" ContentPlaceHolderID="cphEmployeeMaster" runat="Server">

    <script src="../Scripts/fixWebkit.js" type="text/javascript"></script>

    <div class="tabbable">
        <div class="TabPanelHeader">
            Application Access</div>
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">Send Password Reset Email</a></li>
            <li><a href="#tab2" data-toggle="tab">Access Control</a></li>
            <li><a href="#tab3" data-toggle="tab">Assign Role</a></li>
            <li><a href="#tab4" data-toggle="tab">Assign Application Access</a></li>
            <li><a href="#tab5" data-toggle="tab">Change Password</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <uc1:EmailEditor ID="ucntrlEmailEditor" runat="server" />
            </div>
            <div class="tab-pane" id="tab2" style="width: 100%; overflow: auto;">
                <ucl:AccessControl ID="uclAccessControl" runat="server" />
            </div>
            <div class="tab-pane" id="tab3">
                <asp:UpdatePanel ID="updPnlRoleAccess" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="TableRow" style="text-align: center;">
                            <asp:Label EnableViewState="false" ID="lblMessageAssignRole" runat="server"></asp:Label>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label EnableViewState="false" ID="lblRole" runat="server" Text="Role"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList EnableViewState="false" ID="ddlRole" ValidationGroup="RoleAccess"
                                    runat="server" Width="200" />
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent">
                                <asp:RequiredFieldValidator ID="rfvRole" runat="server" ControlToValidate="ddlRole"
                                    ErrorMessage="Please select a role." ValidationGroup="RoleAccess" EnableViewState="False"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="TableRow" style="text-align: center;">
                            <asp:Button ID="btnAssignRole" runat="server" ValidationGroup="RoleAccess" Text="Assign Role"
                                CssClass="CommonButton" OnClick="btnAssignRole_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="tab-pane" id="tab4">
                <asp:UpdatePanel ID="updPnlMenuAccess" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="TableRow" style="text-align: center;">
                            <asp:Label ID="lblMessageAssignAccess" runat="server"></asp:Label>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                Access:
                            </div>
                            <div class="TableFormContent">
                                <uc1:MenuSelector ID="ctlMenuSelector" runat="server" />
                            </div>
                        </div>
                        <br />
                        <div class="TableRow" style="text-align: center">
                            <asp:Button ID="btnMenuAccess" CssClass="CommonButton" runat="server" Text="Save"
                                OnClick="btnSaveMenuAccess_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="tab-pane" id="tab5">
                <uc2:NewPassword ID="ucChangePassword" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
