﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:MailSetup.aspx.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Dec-7-2009          Sandeesh         Defect id: 11591 ; Changes made to save blank SMTP credentials .
-------------------------------------------------------------------------------------------------------------------------------------------       

>*/
using TPS360.Web.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Net.Sockets;
using System.Diagnostics;
using System.IO;
using System.Net.Security;
using AjaxControlToolkit;



namespace TPS360.Web.UI
{
    public partial class ChangeTimezone : EmployeeBasePage 
    {
        const string AppName = "TPS";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                PopulateTimezone(ddlTimezone);
                PrepareView();
                //Employee emp = Facade.GetTimeZoneByMemberId(CurrentMember.Id);
                //if (emp != null)
                //{
                //    ControlHelper.SelectListByValue(ddlTimezone, emp.Timezoneid.ToString());
                //}
                //else
                //{
                //    ControlHelper.SelectListByValue(ddlTimezone, "0");
                //}
            }
           
        }
        #region Methods

        private void PrepareView()
        {
            //string value = "0";
            TPS360.Common.BusinessEntities.Employee emp = Facade.GetTimeZoneByMemberId(CurrentMember.Id);
            if (emp != null)
            {
                ControlHelper.SelectListByValue(ddlTimezone, emp.Timezoneid.ToString());
            }
            else
            {
                ControlHelper.SelectListByValue(ddlTimezone, "0");
            }
        }

        public void PopulateTimezone(ListControl ddltimezone)
        {
            
            BusinessFacade.IFacade facade = new BusinessFacade.Facade();
            ddltimezone.DataSource = facade.GetAllTimeZone();
            ddltimezone.DataTextField = "TimeZone";
            ddltimezone.DataValueField = "Timezoneid";
            try
            {
                ddltimezone.DataBind();
            }
            catch { }
            //list = RemoveScriptForDropDown(list);
            ddltimezone.Items.Insert(0, new ListItem("Please Select", "0"));   // Integer conversion error handled.
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            BusinessFacade.IFacade facade = new BusinessFacade.Facade();
            try
            {
                facade.Employee_SaveTimezone(CurrentMember.Id, Convert.ToInt32(ddlTimezone.SelectedValue));
                MiscUtil.ShowMessage(lblMessage, "Timezone changed successfully", false);
            }
            catch (Exception ex)
            {
                MiscUtil.ShowMessage(lblMessage, ex.Message, true);
            }
        }
        #endregion
    }
}