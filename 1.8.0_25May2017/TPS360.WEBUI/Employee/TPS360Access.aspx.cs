﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: TPS360Access.aspx.cs
    Description: This page is used for Employee Access tab functionality.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Dec-12-2008           Jagadish             Defect ID: 8990; Changes made in Page_Load() method.
 *  0.2              Feb-02-2009           N.Srilakshmi         Defect ID: 9641,9642;Removed update panel for EmailEditor in .aspx
    0.3              Apr-16-2009           Gopala Swamy J       Defect Id:10305; put one  if statement ;   
 *  0.4              Nov-19-1009           Sandeesh             Enhancement Id:10998 -Users in the Admin role should have all of the access/deletion rights of the cst2 superuser
    0.5              5/July/2016           pravin khot          modify function-AssignRole()
 * -------------------------------------------------------------------------------------------------------------------------------------------        
*/
using TPS360.Common.BusinessEntities;
using System;
using System.Collections;
using System.Web.UI.WebControls;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Web.Security;
using System.Collections.Generic;

namespace TPS360.Web.UI
{
    public partial class TPS360AccessControl : EmployeeBasePage
    {
        #region Member Variables

        int _memberId = 0;

        #endregion

        #region Properties

        protected int WorkingEmployeeId
        {
            get
            {
                int memberId = 0;

                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                }
                else
                {
                    memberId = base.CurrentMember.Id;
                }

                return memberId;
            }
        }

        #endregion

        #region Methods

        private void PrepareView()
        {
            MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(this.WorkingEmployeeId);

            if (map != null)
            {
                ControlHelper.SelectListByValue(ddlRole, StringHelper.Convert(map.CustomRoleId));
               
            }

            SiteMapType[] siteMapTypes = new SiteMapType[] {SiteMapType.ApplicationTopMenu,
                                                                    SiteMapType.EmployeeOverviewMenu,
                                                                    SiteMapType.CandidateOverviewMenu,
                                                                    SiteMapType.CompanyOverviewMenu,
                                                                    SiteMapType.EmployeePortalMenu, 
                                                                    SiteMapType.CandidateCareerPortalMenu, 
                                                                      SiteMapType .DepartmentOverviewMenu ,
                                                                    SiteMapType .VendorProfileMenu,
                                                                    SiteMapType .VendorPortalTopMenu ,
                                                                    SiteMapType .CandidateProfileMenuForVendor 
                                                            };

            ctlMenuSelector.CurrentSiteMapTypes = siteMapTypes;

            ctlMenuSelector.SelectList = Facade.GetAllMemberPrivilegeIdsByMemberId(this.WorkingEmployeeId);
            ctlMenuSelector.BindList();

            int employeeId = 0;
            int currentMemberId = CurrentMember.Id;
            MemberManager memberManager = null;

            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                employeeId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }

            if (employeeId > 0)
            {
                memberManager = Facade.GetMemberManagerByMemberIdAndManagerId(employeeId, currentMemberId);
            }

            if (base.IsUserAdmin)
            {
                //uwtTPS360Access.Tabs[4].Visible = true;
            }
            else if (memberManager != null)
            {
            //    uwtTPS360Access.Tabs[4].Visible = true;
            }
        }

        private void AssignRole()
        {
            if (ddlRole.SelectedItem.Text.ToLower() == ContextConstants.ROLE_ADMIN.ToLower())
            {
                string strUserName = string.Empty;
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                }

                if (_memberId > 0)
                {
                    //************Code modify by pravin khot on 5/July/2016*******
                    //strUserName = Facade.GetMemberUserNameById(_memberId);
                    strUserName = Facade.GetMemberADUserNameById(_memberId);
                    //*******************END***************************
                    if (!Roles.IsUserInRole(strUserName, ContextConstants.ROLE_ADMIN))
                        Roles.AddUserToRole(strUserName, ContextConstants.ROLE_ADMIN);

                }

            }
            else{
                  if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                }

                  if (_memberId > 0)
                  {
                      //************Code modify by pravin khot on 5/July/2016*******
                      //string strUserName = Facade.GetMemberUserNameById(_memberId);
                      string strUserName = Facade.GetMemberADUserNameById(_memberId);
                      //*******************END***************************
                      if (Roles.IsUserInRole(strUserName, ContextConstants.ROLE_ADMIN))
                          Roles.RemoveUserFromRole(strUserName, ContextConstants.ROLE_ADMIN);


                  }

            }

            MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(this.WorkingEmployeeId);

            int roleId = Convert.ToInt32(ddlRole.SelectedItem.Value);

            if (map == null)
            {
                map = new MemberCustomRoleMap();
                map.CustomRoleId = roleId;
                map.MemberId = this.WorkingEmployeeId;
                map.CreatorId = base.CurrentMember.Id;

                Facade.AddMemberCustomRoleMap(map);
                AddDefaultMenuAccess();
            }
            else if (map.CustomRoleId != roleId)
            {
                map.CustomRoleId = roleId;
                map.UpdatorId = base.CurrentMember.Id;
                Facade.UpdateMemberCustomRoleMap(map);
                AddDefaultMenuAccess();
            }
       

            PrepareView();

            MiscUtil.ShowMessage(lblMessageAssignRole, "Role assigned successfully.", false);
        }

        private void AddDefaultMenuAccess()
        {
            int roleId = Convert.ToInt32(ddlRole.SelectedItem.Value);
            ArrayList previlegeList = Facade.GetAllCustomRolePrivilegeIdsByRoleId(roleId);

            if (previlegeList != null && previlegeList.Count >= 0)
            {
                Facade.DeleteMemberPrivilegeByMemberId(this.WorkingEmployeeId);

                for (int i = 0; i <= previlegeList.Count - 1; i++)
                {
                    MemberPrivilege previlege = new MemberPrivilege();

                    previlege.CustomSiteMapId = Convert.ToInt32(previlegeList[i]);
                    previlege.MemberId = this.WorkingEmployeeId;
                    Facade.AddMemberPrivilege(previlege);
                }
            }
        }

        private void SaveMenuAccess()
        {
            TreeNodeCollection checkedNodes = ctlMenuSelector.SiteMapTree.CheckedNodes;
            if (IsUserAdmin)
                Facade.DeleteMemberPrivilegeByMemberId(this.WorkingEmployeeId);
            else
                Facade.DeleteByMemberIdWithOutAdminAccess(this.WorkingEmployeeId);

            foreach (TreeNode node in checkedNodes)
            {
                MemberPrivilege memberPrivilege = new MemberPrivilege();
                memberPrivilege.CustomSiteMapId = Convert.ToInt32(node.Value);
                memberPrivilege.MemberId = this.WorkingEmployeeId;
                Facade.AddMemberPrivilege(memberPrivilege);
                        
            }
            MiscUtil.ShowMessage(lblMessageAssignAccess, "Permission assigned successfully.", false);
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ucntrlEmailEditor.EmailFromType = EmailFromType.TPS360Access;    
            //uwtTPS360Access.Tabs[4].Visible = false;

            MiscUtil.PopulateCustomRole(ddlRole, Facade);
            if (!base.IsUserAdmin)
            {
                ListItem list = new ListItem();
                list = ddlRole.Items.FindByText("Admin");
                ddlRole.Items.Remove(list);
            }
            if (!IsPostBack)
            {
                PrepareView();
                //ddlRole.Items.Remove(ddlRole.Items.FindByText("Candidate"));
                //ddlRole.Items.Remove(ddlRole.Items.FindByText("Vendor"));
            }

            if (IsPostBack)
            {
                MiscUtil.LoadAllControlState(Request, this);
            }

            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            if (_memberId > 0)
            {
                string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                this.Page.Title =MiscUtil .RemoveScript ( name,string .Empty ) + " - " + "Access";
            }
        }

        protected void btnAssignRole_Click(object sender, EventArgs e)
        {

            if (ddlRole.SelectedIndex == 0)
            {
                MiscUtil.ShowMessage(lblMessageAssignRole, "Please select role to assign.", false);//0.3
                return;
            }

            AssignRole();
           // updPnlMenuAccess.Update();
        }

        protected void btnSaveMenuAccess_Click(object sender, EventArgs e)
        {
            SaveMenuAccess();
        }

        #endregion
    }
}