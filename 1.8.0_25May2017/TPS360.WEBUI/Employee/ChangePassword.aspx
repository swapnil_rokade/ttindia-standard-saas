﻿<%@ Page Language="C#" MasterPageFile="~/Masters/MemberPortal.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs"
    Inherits="TPS360.Web.UI.EmployeeChangePasswordEditor" Title="Change Password" %>
<%@ Register Src="~/Controls/MemberChangePassword.ascx" TagName="ChangePassword" TagPrefix="uc1" %>
<asp:Content ID="cntChangePasswordHeader" ContentPlaceHolderID="head"
    runat="Server">
</asp:Content>
<asp:Content ID="cntChangePassword" ContentPlaceHolderID="cphHomeMaster"
    runat="Server">
    <div style =" margin-top : 30px">
                                <asp:UpdatePanel ID="pnlChangePassword" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <uc1:ChangePassword id="ucChangePassword" runat="server"  />
                                    </ContentTemplate>
                            </asp:UpdatePanel> 
                            </div>
</asp:Content>
