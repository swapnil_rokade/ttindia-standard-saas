﻿using System;

using AjaxControlToolkit;

namespace TPS360.Web.UI
{
    public partial class EmployeeChangePasswordEditor : EmployeeBasePage
    {
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                           
                ucChangePassword.MemberRole = ContextConstants.ROLE_EMPLOYEE;
            }
        }

        #endregion
    }
}