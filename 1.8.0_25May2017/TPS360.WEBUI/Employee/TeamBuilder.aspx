﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="TeamBuilder.aspx.cs"
    Inherits="TPS360.Web.UI.TeamBuilder" Title="Team Builder" EnableEventValidation="false"
    Async="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Content ID="cntTeamBuilder" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Team Builder</asp:Content>
<asp:Content ID="cntRegistrationInformation" ContentPlaceHolderID="cphHomeMaster"
    runat="Server">

    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(function() {

        $('.TableRow').show();
         });
        function RemoveRow(row)
        {
        
          $(row).parent().parent().remove();
        }
    </script>

    <asp:UpdatePanel ID="TeamBuildId" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnSortColumnName" runat="server" Value="[ET].[Title]" />
            <asp:HiddenField ID="hdnSortColumnOrder" runat="server" Value="ASC" />
            <asp:HiddenField ID="hdnSortColumn" runat="server" Value="btnTitle" />
            <asp:HiddenField ID="hdnSortOrder" runat="server" Value="ASC" />
            <asp:HiddenField ID="hdnSelectedIDtoEdit" runat="server" />
            <div id="dvTeamBuild" runat="server" style="width: 100%">
                <div class="TableRow" style="text-align: center">
                    <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
                    <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
                </div>
                <div class="TabPanelHeader">
                    Add Team
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblTitleName" runat="server" Text="Title"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <div id="divTeamTitle" runat="server">
                        </div>
                        <asp:TextBox ID="txtTeamTitleName" runat="server"  TabIndex="1" CssClass="CommonTextBox" AutoComplete="OFF"
                            EnableViewState="true"></asp:TextBox>
                        <span class="RequiredField">*</span>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvTeamTitle" runat="server" ControlToValidate="txtTeamTitleName"
                            ErrorMessage="Please enter Title." EnableViewState="False" Display="Dynamic"
                            ValidationGroup="TeamInfo"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblTeamLeader" runat="server" Text="Team Leader"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList EnableViewState="true" ID="ddlTeamLeader" TabIndex="2" runat="server" Width="150px"
                            CssClass="CommonDropDownList" AutoPostBack="true" OnSelectedIndexChanged="ddlTeamLeader_onselectedchanged">
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                        <asp:HiddenField ID="hdnLeaderID" runat="server" />
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:CompareValidator ID="rfvTeamLeader" runat="server" ControlToValidate="ddlTeamLeader"
                            ErrorMessage="Please select Team Leader." EnableViewState="False" Display="Dynamic"
                            ValueToCompare="0" Operator="GreaterThan"
                            ValidationGroup="TeamInfo"></asp:CompareValidator>
                    </div>
                </div>
                <div class="TableRow" style="">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblTeamMembers" runat="server" Text="Team Members"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:ListBox ID="lstTeamMembers" runat="server" SelectionMode="multiple" CssClass="CommonDropDownList"
                            Width="250px" Height="110px" TabIndex="3"></asp:ListBox>
                        <span class="RequiredField" style="height: 110px">*</span>
                        <asp:Button ID="btnAddTeamMembers" CssClass="CommonButton" runat="server" Text="Add"
                            OnClick="btnAddTeamMembers_Click" TabIndex="4" EnableViewState="true" ValidationGroup="TeamInfo1"/>
                    </div>
                </div>
                <%--<div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvTeamMembers" runat="server" ControlToValidate="lstTeamMembers"
                            ErrorMessage="Please select Team Member." EnableViewState="False" Display="Dynamic"
                            ValidationGroup="TeamInfo1"></asp:RequiredFieldValidator>
                    </div>
                </div>--%>
                <br />
                <div class="TableRow" style="width: 100%;">
                    <div class="TableFormLeble">
                    </div>
                    <asp:UpdatePanel ID="upPanel" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <div style="vertical-align: text-top; float: left; width: 400px;">
                                <asp:ListView ID="lsvAssignedTeam" runat="server" DataKeyNames="Id" OnItemDataBound="lsvAssignedTeam_ItemDataBound"
                                    OnPreRender="lsvAssignedTeam_PreRender" >
                                    <LayoutTemplate>
                                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <th style="white-space: nowrap;">
                                                    Name
                                                </th>
                                                <th style="white-space: nowrap;">
                                                    Email
                                                </th>
                                                <th id="thUnAssign" style="text-align: center; white-space: nowrap; width: 55px;">
                                                    Remove
                                                </th>
                                            </tr>
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                            <td style="text-align: left;">
                                                <asp:Label ID="lblMemberName" runat="server" />
                                                <asp:HiddenField ID="hdnMemberID" runat="server" />
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:Label ID="lblMemberEmail" runat="server" />
                                            </td>
                                            <td id="tdUnAssign" style="text-align: center;">
                                                <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem"
                                                    OnClientClick="RemoveRow(this); return;"></asp:ImageButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                </div>
                <br />
                <br />
                <div class="TableRow" style="text-align: center">
                    <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" ValidationGroup="TeamInfo"
                        OnClick="btnSave_Click" TabIndex="5"  />
                </div>
                <div class="TabPanelHeader">
                    List of Teams
                </div>
            </div>
            <div>
                <div class="GridContainer" style="text-align: left;">
                    <asp:ObjectDataSource ID="odsTeamsList" runat="server" SelectMethod="GetPagedEmployeeTeam"
                        TypeName="TPS360.Web.UI.EmployeeDataSource" EnablePaging="true" SelectCountMethod="GetListCountEmployeeTeam"
                        SortParameterName="sortExpression">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="hdnSortColumnOrder" Name="SortOrder" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    
                    <asp:UpdatePanel>
                    <ContentTemplate>
                  
                    <asp:ListView ID="lsvTeams" runat="server" DataKeyNames="Id" DataSourceID="odsTeamsList"
                        OnItemDataBound="lsvTeams_ItemDataBound" OnPreRender="lsvTeams_PreRender" OnItemCommand="lsvTeams_ItemCommand"
                        EnableViewState="true">
                        <LayoutTemplate>
                            <table id="tlbTemplateskill" class="Grid" cellspacing="0" border="0" style="width: 100%">
                                <tr id="Tr1" style="width: 100%" enableviewstate="true" runat="server">
                                    <th enableviewstate="true">
                                        <asp:LinkButton ID="btnTitle" runat="server" CommandName="Sort" ToolTip="Sort By Title"
                                            CommandArgument="[ET].[Title]" Text="Title" TabIndex="6" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnTeamLeader" runat="server" ToolTip="Sort By Team Leader" CommandName="Sort"
                                            CommandArgument="[E].[FirstName]" Text="Team Leader" TabIndex="7" />
                                    </th>
                                    <th>
                                        Team Members
                                    </th>
                                    <th style="width: 50px !important" runat="server" id="thAction">
                                        Action
                                    </th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td colspan="4" runat="server" id="tdPager" >
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true"   />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No Teams data available.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2== 0 ? "row" : "altrow" %>'>
                                <td>
                                    <asp:Label ID="lblTitle" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblTeamLeader" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblTeamMembers" runat="server" />
                                </td>
                                <td style="" runat="server" id="tdAction">
                                    <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" CausesValidation="false" runat="server"
                                        CommandName="EditItem" TabIndex="8"></asp:ImageButton>
                                    <asp:ImageButton ID="btnDeleteList" Visible="true" SkinID="sknDeleteButton" runat="server"
                                        CommandName="DeleteItem" TabIndex="9" ></asp:ImageButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                      </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
