﻿<%@ Page Language="C#" MasterPageFile="~/Masters/MemberPortal.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="MailSetup.aspx.cs" Inherits="TPS360.Web.UI.EmployeeMailSetup"
    Title="Mail Setup" %>

<asp:Content ID="cntMailSetup" ContentPlaceHolderID="cphHomeMaster" runat="Server">

    <script type="text/javascript" language="javascript">
function Remove()
{
document .getElementById ('<%=txtSMTPwd.ClientID %>').value="";
}
    </script>

    <div class="TabPanelHeader">
        Mail Setup
    </div>
    <div style="width: 100%; margin-top: 30px; vertical-align: middle;">
        <asp:UpdatePanel ID="pnlDynamic" runat="server">
            <ContentTemplate>
                <div style="text-align: left">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                    </div>
                    <div class="TableFormContent">
                    </div>
                </div>
                
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblSMTPUser" runat="server" Text="User:"></asp:Label>
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtSMTPUser" runat="server" AutoComplete="OFF"></asp:TextBox>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblSMTPwd" runat="server" Text="Password:"></asp:Label>
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtSMTPwd" runat="server" TextMode="Password" OnClick="javascript:Remove()"></asp:TextBox>
                    </div>
                </div>
                
               <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblSmtp" runat="server" Text="SMTP Server:"></asp:Label>
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtSMTP" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblSMTPort" runat="server" Text="Port:"></asp:Label>
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtSMTPort" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableformValidatorContent" style="margin-left: 43%">
                        <asp:RegularExpressionValidator ID="revSMTPPort" runat="server" ControlToValidate="txtSMTPort"
                            Display="Dynamic" ErrorMessage="Please enter valid port number." ValidationExpression="^(6553[0-5]|655[0-2]\d|65[0-4]\d\d|6[0-4]\d{3}|[1-5]\d{4}|[1-9]\d{0,3}|0)$"
                            ValidationGroup="g1"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblSMTPSslEnable" runat="server" Text="SSL:"></asp:Label>
                    </div>
                    <div class="TableFormContent">
                        <asp:CheckBox ID="chkSMTPSslEnable" runat="server" />
                    </div>
                </div>
                <div class="TableRow" align="center">
                    <asp:Button ID="btnTest" CssClass="CommonButton" runat="server" Text="Test" ValidationGroup="g1"
                        OnClick="btnTest_Click" OnClientClick="RemoveModal();" />
                    <asp:Button ID="btnSaveSetting" CssClass="CommonButton" runat="server" Text="Save"
                        ValidationGroup="g1" OnClick="btnSaveSetting_Click" />
                    <asp:UpdateProgress ID="UpdateProgress1" EnableViewState="false" runat="server" DisplayAfter="0"
                        DynamicLayout="true">
                        <ProgressTemplate>
                            <asp:Image runat="server" ID="imgLogo1" ImageUrl="~/Images/loading.gif" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div class="TableRow" style="text-align: center; width: 50%; margin-left: 25%; margin-top: 5px;">
                    <div id="divTestMessage" runat="server">
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
