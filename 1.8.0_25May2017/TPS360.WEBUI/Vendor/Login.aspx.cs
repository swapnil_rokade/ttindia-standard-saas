﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Login.aspx.cs
    Description: This is the page used for Login of the user.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               24-Sept-2008          Shivanand           Defect id: 8655; Implemented Remember me concept.
    0.2               17-Mar-2009           Jagadish            Defect id: 8655; Changed the code to remember Password.
 *  0.3               14-May-2009           Rajendra A.S        Defect id: 10465; Code modified in LoginUser_LoggedIn() event.
 *  0.4               04-Jun-2009           Veda                Defect Id:10554 ;User cannot goto login page by entering the the Login URL 
    0.5               15-Jun-2009           Veda                Defect Id:10628 ;License connection string is defined in a new section of web.config file 
 *  0.6               24-Jun-2009           Anand Dixit         Changes due to renaming of LicenseSectionHandler to TPS360Section handler are implemented
 *  0.7               08-Jul-2009           Veda                Defect Id:10871;Removed the links in Log in page.
    0.8              Jul-14-2009         Nagrathna .V.B         DefectId:9112 "Login" link has been removed.
 *  0.9               07-Oct-2009           Veda                Defect Id:11534 : Delete teh saved license details after teh expirydate
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Web.Security;

using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using TPS360.Common.Helper;
using System.Web;
using System.Configuration;
using TPS360.Common;
using System.Web.Configuration;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Net.NetworkInformation;

namespace TPS360.Web.UI
{
    public partial class Login : BasePage
    {
        #region Variables
        string varchrDomainName = GettingCommonValues.GetDomainName();
        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (SiteSetting != null)
            //{
            //    if (SiteSetting[DefaultSiteSetting.CompanyName.ToString()] != null)
            //    {
            //        Label lblCompanyname = (Label)FindControl("lblCompanyname");
            //        lblCompanyname.Text = SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString();

            //    }
            //}
            if (!IsPostBack)
            {
                if (Request.QueryString.Count > 0)
                {
                    string strTimeout = "";
                    try
                    {
                        strTimeout = Request.QueryString["SeOut"].ToString();
                    }
                    catch { }
                    if (strTimeout != "")
                    {
                        Literal lit = (Literal)LoginUser.FindControl("FailureText");
                        lit.Text = "Your Session has Expired.";
                    }
                }
            }

            LoginStatus save;
            save = (LoginStatus)Page.Master.FindControl("lgsLogin");
            if (save != null)
                save.Visible = false;
            try
            {
               
                    HtmlGenericControl HeaderDiv = (HtmlGenericControl)this.Master.FindControl("SettingMenuHolder");
                    HeaderDiv.Visible = false;
            }
            catch { }

            if (!IsPostBack)
            {

                if (Request.Cookies["LoginStatus"] != null && Request.Cookies["LoginStatus"].Value != "")
                {

                    Literal lit = (Literal)LoginUser.FindControl("FailureText");
                    Response.Cookies["LoginStatus"].Value = "";
                    lit.Text = Request.Cookies["LoginStatus"].Value;
                    //return;
                }


                    Context.Items[ContextConstants.MEMBER] = null;
                    Context.Items[ContextConstants.SITESETTING] = null;
                    //Check if the browser support cookies 
                    if (Request.Browser.Cookies)
                    {
                        //Check if the cookies with name PBLOGIN exist on user's machine & populate the textboxes.
                        if (Request.Cookies["PBLOGIN"] != null)
                        {
                            try { 
                            TextBox txtUsername = (TextBox)LoginUser.FindControl("UserName");
                            txtUsername.Text = Request.Cookies["PBLOGIN"]["UNAME"].ToString();
                            LoginUser.UserName = Request.Cookies["PBLOGIN"]["UNAME"].ToString();
                            TextBox txtPassword = (TextBox)LoginUser.FindControl("Password");
                            txtPassword.Attributes.Add("value", Request.Cookies["PBLOGIN"]["UPASS"].ToString());

                            CheckBox chkRemember = (CheckBox)LoginUser.FindControl("RememberMe");
                            chkRemember.Checked = true;

                            LoginUser.RememberMeSet = true;
                            }
                            catch (Exception ex) { }
                    }
                    }
                
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Helper.Url.Redirect(UrlConstants.CommonSite.HOME_PAGE);
        }
        protected void LoginUser_Error(object sender, EventArgs e)
        {
            TextBox txtPassword = (TextBox)LoginUser.FindControl("Password");
            txtPassword.Focus();
        }

       
        protected void LoginUser_LoggedIn(object sender, EventArgs e)
        {
            AfterLogin();
        }


        protected void LoginUser_LoggingIn(object sender, LoginCancelEventArgs e)
        {

            string UserName = LoginUser.UserName;
            UserName = UserName.Replace(",", ".");
            if (!Roles.IsUserInRole(UserName, ContextConstants.ROLE_VENDOR))
                 {
                     SecureUrl urlLogin = UrlHelper.BuildSecureUrl(UrlConstants.LOGIN_PAGE, "");
                     Response.Cookies["LoginStatus"].Value = "Your login attempt was not successful. Please try kkkk again. ";// +Request.UserHostName;// "Maximum number of users reached";
                     Helper.Url.Redirect(urlLogin.ToString());
                     return;
                 }

            
        }
        int Pagevalue_i = 0;
     

        void AfterLogin()
        {
            MembershipUser user = Membership.GetUser(LoginUser.UserName);
            CheckBox chkRemember = (CheckBox)LoginUser.FindControl("RememberMe");
            if (chkRemember.Checked)
            {
                FormsAuthentication.SetAuthCookie(user.UserName, true);
            }
            else
            {
                FormsAuthentication.SetAuthCookie(user.UserName, false);
            }

            if (chkRemember.Checked)
            {
                //Check if the browser support cookies 
                CookieStore();
            }

            // Delete the cookies
            else if (Request.Cookies["PBLOGIN"] != null)
            {
                HttpCookie aCookie;
                string cookieName;

                cookieName = Request.Cookies["PBLOGIN"].Name;
                aCookie = new HttpCookie(cookieName);
                aCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(aCookie);

            }

            Member member = Facade.GetMemberByUserId((Guid)user.ProviderUserKey);
            string returnUrl = FormsAuthentication.GetRedirectUrl(user.UserName, false);
            string RedirectUrl = string.Empty;
            bool IsRedirect = false;
            if (!string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = returnUrl.Replace("%2f", "/");
                string[] returnUrlPart = returnUrl.Split('/');

                if (returnUrlPart.Length > 3)
                {
                    if (returnUrlPart[3].ToString().Contains("?"))
                        RedirectUrl = returnUrlPart[2].ToString() + "/" + returnUrlPart[3].ToString().Substring(0, returnUrlPart[3].IndexOf('?'));
                    else
                        RedirectUrl = returnUrlPart[2].ToString() + "/" + returnUrlPart[3].ToString();
                    IsRedirect = Facade.GetMemberPrivilegeForMemberIdAndRequiredURL(member.Id, RedirectUrl.Trim());
                    if (IsRedirect)
                    {
                        FormsAuthentication.RedirectFromLoginPage(user.UserName, false);
                        return;
                    }
                }
            }

            SecureUrl url;


            MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(member.Id);

            if (member != null)
            {
                Session["Loggedin"] = "Yes";
                Helper.Url.Redirect(UrlConstants.Vendor .HOME  );
            }
          
        }

        private void DeleteLicenseDetails()
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
            TPS360SectionHandler license = config.GetSection("tps360license") as TPS360SectionHandler;
            TPS360ConfigElement lkey = new TPS360ConfigElement("LicenseKey", "");
            TPS360ConfigElement sdate = new TPS360ConfigElement("StartDate", "");
            TPS360ConfigElement expdate = new TPS360ConfigElement("ExpireDate", "");
            TPS360ConfigElement noofusersallowed = new TPS360ConfigElement("NoofUsersAllowed", "");
            license.Details.Remove(lkey);
            license.Details.Remove(sdate);
            license.Details.Remove(expdate);
            license.Details.Remove(noofusersallowed);
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("tps360license"); 
        }

        void CookieStore()
        {
            if ((Request.Browser.Cookies))
            {
                //Create a cookie with expiry of 30 days 
                Response.Cookies["PBLOGIN"].Expires = DateTime.Now.AddDays(30);

                //Write username to the cookie 
                TextBox txtUsername = (TextBox)LoginUser.FindControl("UserName");
                Response.Cookies["PBLOGIN"]["UNAME"] = txtUsername.Text;

                //Write password to the cookie 
                TextBox txtPassword = (TextBox)LoginUser.FindControl("Password");
                Response.Cookies["PBLOGIN"]["UPASS"] = txtPassword.Text;

            }
        }

        #endregion
      
}
}
