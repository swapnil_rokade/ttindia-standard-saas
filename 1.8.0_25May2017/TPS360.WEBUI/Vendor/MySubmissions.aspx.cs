﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using System.Text;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
namespace TPS360.Web.UI
{
    public partial class MySubmissions : BasePage
    {
   
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString() != null) 
            {
                hdnMySubmission.Value = SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString();
                this.Page.Title = hdnMySubmission.Value + " - " + "My Submissions";
            }
          
        }


       
    }
}