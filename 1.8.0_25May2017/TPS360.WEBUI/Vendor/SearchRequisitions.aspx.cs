﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using System.Text;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
namespace TPS360.Web.UI
{
    public partial class SearchRequisitions : BasePage 
    {
        private int VendorId
        {
            get
            {
                if (CurrentMember != null)
                {
                    CompanyContact com = new CompanyContact();
                    com = Facade.GetCompanyContactByMemberId(CurrentMember.Id);
                    return com.CompanyId;
                }
                else
                {
                    return 0;
                }

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if(SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString()!=null){
            hdnSearchJob.Value=SiteSetting[DefaultSiteSetting .CompanyName .ToString ()].ToString ();
            hdnVendorId.Value = VendorId.ToString();
            this.Page.Title = hdnSearchJob.Value + " - Search Job Openings";
            }
            if (CurrentMember == null) Response.Redirect(UrlConstants.CandidatePortal.LOGIN);
           
            if (!IsPostBack)
            {
                PrepareView();
                hdnSortColumn.Value = "btnPostedDate";
                hdnSortOrder.Value = "DESC";
            }

            string pagesize = "";
            pagesize = (Request.Cookies["VendorReqSearchRowPerPage"] == null ? "" : Request.Cookies["VendorReqSearchRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            }
        }
        private void PrepareView()
        {
           
        }
        private void Clear()
        {
            txtJobTitle.Text = "";
            txtSkills.Text = "";
            txtCity.Text = "";
            uclCountryState.SelectedCountryId = 0;
            uclCountryState.SelectedStateId = 0;
            lsvJobPosting.DataBind();
        }

        #region Button Events
        protected void btnClear_Click(object sender, EventArgs args)
        {
            Clear();
        }
        protected void btnSearch_Click(object sender, EventArgs args)
        {

            lsvJobPosting.DataBind();
            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
        }

        #endregion

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvJobPosting.FindControl(hdnSortColumn.Value);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                if (lnk.CommandArgument.Contains("[1]") || lnk.CommandArgument.Contains("[JMC].[ManagersCount]"))
                {
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Descending" : "Ascending"));
                }
                else
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        #region Listview Events

        protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
                if (jobPosting != null)
                {

                   
                    Label lblPostedDate = (Label)e.Item.FindControl("lblPostedDate");
                    HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    Label lblAppy = (Label)e.Item.FindControl("lblAppy");
                    LinkButton btnApply = (LinkButton)e.Item.FindControl("btnApply");
                    //ImageButton btnApply = (ImageButton)e.Item.FindControl("btnApply");
                    lblPostedDate.Text = jobPosting.PostedDate.ToShortDateString();
                    lnkJobTitle.Text = jobPosting.JobTitle;
                    lnkJobTitle.Attributes.Add("onclick", "javascript:EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "&FromPage=Vendor','700px','570px')");
                    lblCity.Text = jobPosting.City + ((!string.IsNullOrEmpty(jobPosting.City) && jobPosting.StateId > 0) ? ", " : string.Empty) + MiscUtil.GetStateNameById(jobPosting.StateId, Facade);
                    lblAppy.Visible = jobPosting.IsApplied;
                    btnApply.Visible = !jobPosting.IsApplied;
                    btnApply.CommandArgument = jobPosting.Id.ToString();
                    btnApply.Attributes.Add("onclick", "javascript:EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/VendorCandidateList.aspx?JID=" + jobPosting.Id + "','700px','570px'); return false;");
                }
            }
        }

        private void buildMemberJobApplied(string JID)
        {
            MemberJobApplied newjob = new MemberJobApplied();
            newjob.MemberId = CurrentMember.Id;
            newjob.JobPostingId = Convert.ToInt32(JID);
            Facade.AddMemberJobApplied(newjob);

        }
        protected void lsvJobPosting_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
           
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                }
                if (e.CommandName == "Apply")
                {
                    MemberDocument memberDocu = Facade.GetRecentResumeByMemberID(CurrentMember.Id);
                    if (memberDocu != null)
                    {
                        string JID = e.CommandArgument.ToString();
                        buildMemberJobApplied(JID);
                        Facade.MemberJobCart_AddCandidateToRequisition(CurrentMember.Id, CurrentMember.Id.ToString(), Convert.ToInt32(JID));
                        MiscUtil.ShowMessage(lblMessage, "success “Thank you for your application.”", false);
                        lsvJobPosting.DataBind();
                    }
                    else
                    {
                        MiscUtil.ShowMessage(lblMessage, "Please upload a resume before applying to jobs.", true);
                    }


                }
            }
            catch
            {
            }
           
        }

        protected void lsvJobPosting_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null)
                {
                   hdnRowPerPageName.Value = "VendorReqSearchRowPerPage";
                }
            }
            PlaceUpDownArrow();
        }
        protected void odsRequisitionList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
           
        }
        #endregion


    }
}