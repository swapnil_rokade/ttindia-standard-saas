﻿/*  Modification Log:
   Ver.No.             Date                Author              MOdification
   0.1               12-Feb-2009          Sandeesh              Defect Id :9870 - Added code to open the resume document in the new window.   
   0.2               19-Mar-2009          N.Srilakshmi          Defect Id:10089 - Changes made in lsvFileName_ItemDataBound,GetDocumentLink   
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.BusinessFacade;
namespace TPS360.Web.UI
{
    public partial class CandidateOverview : ATSBasePage 
    {
        private int _memberId = 0;

        #region  Method

        private void BindMemberDocument()
        {
          //  IList<MemberDocument> lstDocument = Facade.GetAllMemberDocumentByMemberId(_memberId);
            //lsvFileName.DataSource = lstDocument;
           // lsvFileName.DataBind();
        }

        private string GetDocumentLink(string strFileName, string strDocumenType)
        {
            try
            {
                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, _memberId, strFileName, strDocumenType, true);

                if (strFilePath != strFileName)
                {
                    return "<a href='" + strFilePath + "' TARGET='_blank' >" + strFileName + "</a>";
                }
                else
                {
                    return strFileName + "(File not found)";
                }
            }
            catch
            {
                return strFileName + "(File not found)";
            }
        }     

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            else
            {
                _memberId = base.CurrentMember.Id;
            }

            //if (_memberId > 0)
            //{
            //    BindMemberDocument();
            //}
            if (!IsPostBack)
            {   
                if (_memberId > 0)
                {
                    string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                    this.Page.Title = name + " - " + "Profile Overview";
                }
                hdnPageTitle.Value = this.Page.Title;
            }
            this.Page.Title = hdnPageTitle.Value;
        }

        protected void lsvFileName_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            string link = string.Empty;
            string path = string.Empty;

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberDocument memberDocument = ((ListViewDataItem)e.Item).DataItem as MemberDocument;

                if (memberDocument != null)
                {
                    GenericLookup documentType = Facade.GetGenericLookupById(memberDocument.FileTypeLookupId);
                    Label lblFileName = ((Label)e.Item.FindControl("lblFileName"));

                    lblFileName.Text = memberDocument.FileName;

                    if (documentType != null)     
                    {
                        link = GetDocumentLink(memberDocument.FileName, documentType.Name);
                    }
                    lblFileName.Text = link;
                }
            }
        }

        #endregion

        [System.Web.Services.WebMethod]
        public static object[] MoveCandidatesToLevel(object data)
        {
            IFacade facade = new Facade();
            Dictionary<string, object> param =
                  (Dictionary<string, object>)data;
            string MemberId = param["Member_Id"].ToString();
            int JobPostingId = Convert.ToInt32(param["JobPosting_Id"]);
            string StatusId = param["StatusId"].ToString();
            int UpdatorId = Convert.ToInt32(param["UpdatorId"]);
            facade.MemberJobCart_MoveToNextLevel(0, UpdatorId, MemberId, JobPostingId, StatusId);
            HiringMatrixLevels  lev = facade.GetHiringMatrixLevelsById(Convert.ToInt32(StatusId));
            MiscUtil.EventLogForCandidateStatus(TPS360.Common .Shared . EventLogForCandidate.CandidateStatusChange, JobPostingId, MemberId, UpdatorId,lev.Name , facade);
            facade.UpdateCandidateRequisitionStatus  (JobPostingId, MemberId, UpdatorId,Convert.ToInt32 (StatusId ));
            string[] s = { "Moved", "" };
            return s;
        }

    }
}