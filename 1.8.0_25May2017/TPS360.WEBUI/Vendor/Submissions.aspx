﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CandidateProfile.master" AutoEventWireup="true" CodeFile="Submissions.aspx.cs"  EnableEventValidation ="false" 
    Inherits="TPS360.Web.UI.Submissions" Title="New Candidate Registration" %>
<%@ Register Src="~/Controls/VendorMySubmission.ascx" TagName="VendorSubmission" TagPrefix="ucl" %>


<%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>  

<asp:Content ID="cntRegistrationInformation" ContentPlaceHolderID="cphCandidateMaster"
    runat="Server">
    <ucl:VendorSubmission ID="uclVendorSubmission" runat="server"  VendorCandidateProfile="True" />
  <%-- 
   <asp:UpdatePanel ID="upCart" runat ="server" >
<ContentTemplate >

<asp:HiddenField ID ="Level" runat ="server" />
<asp:HiddenField ID ="ReqId" runat ="server" />
 
<asp:TextBox ID="txtSortColumn" runat ="server" Visible ="false"  ></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat ="server" Visible ="false"  ></asp:TextBox>
 <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
           <asp:Panel ID="pnlJobCartContent" runat="server" >
            <div style="text-align: left; ">
              <asp:ObjectDataSource ID="odsJobCartList" runat="server" SelectMethod="GetPaged"
                            TypeName="TPS360.Web.UI.MemberJobCartDataSource" EnablePaging="true" SelectCountMethod="GetListCount"
                            SortParameterName="sortExpression">
                            <SelectParameters>
                                <asp:Parameter Name="memberId" DefaultValue="0" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    
                <asp:ListView ID="lsvJobCart" runat="server" DataKeyNames="Id" DataSourceID ="odsJobCartList" OnItemDataBound="lsvJobCart_ItemDataBound"  OnPreRender ="lsvJobCart_PreRender" OnItemCommand ="lsvJobCart_ItemCommand">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr>
                            
                             <th>
                                <asp:LinkButton ID ="lnkDateSubmitted" runat ="server" CommandArgument ="[MJC].[CreateDate]" CommandName  ="Sort" Text ="Date Submitted" ToolTip ="Date Submitted"></asp:LinkButton></th>  
                                <th >
                                <asp:LinkButton ID ="lnkSubmittedBy" runat ="server" CommandArgument ="[MJC].[CreatorID]" CommandName  ="Sort" Text ="Submitted By" ToolTip ="Submitted By"></asp:LinkButton></th>
                                <th>
                                <asp:LinkButton ID ="lnkJobTitle" runat ="server" CommandArgument ="[J].[JobTitle]" CommandName  ="Sort" Text ="Job Title" ToolTip ="Job Title"></asp:LinkButton> </th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                              <tr class="Pager">
                        <td colspan="3" runat ="server" id="tdPager">
                             <ucl:Pager Id="pagerControl" runat ="server" EnableViewState ="true"  />
                        </td>
                    </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>  No jobs in Job Cart. </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td><asp:Label ID="lblDateSubmitted" runat="server" /> </td>
                            <td><asp:HyperLink ID="hlnkSubmittedBy" runat ="server"  Target ="_blank" /></td>
                            <td> <asp:HyperLink ID="lblJobTitle" Target="_blank" runat="server"></asp:HyperLink><br /></td>
                           
                          </tr>
                    </ItemTemplate>
                </asp:ListView>
          
            </div>
        </asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>

--%>
</asp:Content>

