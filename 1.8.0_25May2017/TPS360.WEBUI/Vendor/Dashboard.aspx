<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/Dashboard.master"
    AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" Title="Employee Dashboard" %>

<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/VendorApplicants.ascx" TagName="VendorCandidate" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/VendorSubmissionSmallPager.ascx" TagName="VendorMySubmission" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/VendorMyPerformance.ascx" TagName="VendorMyPerformance" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/VendorActiveJobOpenings.ascx" TagName="VendorActiveJobOpenings" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/VendorCandidatePerformance.ascx" TagName="VendorCandidatePerformance" TagPrefix="ucl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <link href="../Dashboard/inettuts.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <div id="columns">
        <ul id="PnlLeft" class="column"  id="intro">
            <li class="widget WidgetColor">
                <div class="widget-head">
                    <h3>
                        Recently Posted Requisitions</h3>
                </div>
                <div class="widget-content">
                    <asp:ObjectDataSource ID="odsRequisitionList" runat="server" SelectMethod="GetPagedRequisitionListForVendorPortal"
                        OnSelecting="odsRequisitionList_Selecting" TypeName="TPS360.Web.UI.JobPostingDataSource"
                        SelectCountMethod="GetListCountForVendorPortal" EnablePaging="True" SortParameterName="sortExpression">
                        <SelectParameters>
                            <asp:Parameter Name="JobTitle" DefaultValue="" />
                            <asp:Parameter Name="ReqCode" DefaultValue="" />
                            <asp:Parameter Name="City" DefaultValue="" />
                            <asp:Parameter Name="StateID" DefaultValue="0" />
                            <asp:Parameter Name="CountryID" DefaultValue="0" />
                            <asp:ControlParameter ControlID="hdnVendorId" PropertyName="Value" Name="VendorId" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:UpdatePanel ID="upcandidateList" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="hdnSortColumn" runat="server" Value ="btnPostedDate" />
                            <asp:HiddenField ID="hdnSortOrder" runat="server" Value ="DESC" />
                            <asp:HiddenField ID="hdnEmployeeDashboard" runat="server" />
                            <asp:HiddenField ID="hdnVendorId" runat="server" />
                            <div class="GridContainer" style="padding-top: 5px;">
                                <div style="overflow: auto; overflow-y: hidden" id="bigDiv" onscroll='SetScrollPosition()'>
                                    <asp:UpdatePanel ID="upJobPostings" runat="server">
                                        <ContentTemplate>
                                            <asp:ListView ID="lsvJobPosting" runat="server" EnableViewState="true" DataKeyNames="Id"
                                                OnItemDataBound="lsvJobPosting_ItemDataBound" OnItemCommand="lsvJobPosting_ItemCommand"
                                                OnPreRender="lsvJobPosting_PreRender" DataSourceID="odsRequisitionList">
                                                <LayoutTemplate>
                                                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                                        <tr id="trHeadLevel" runat="server">
                                                            <th style="white-space: nowrap; width: 95px !important;">
                                                                <asp:LinkButton ID="btnPostedDate" runat="server" ToolTip="Sort By CreatedDate" CommandName="Sort" CommandArgument="[J].[CreateDate]"
                                                                    Width="60%" Text="Date Created" TabIndex="2" />
                                                            </th>
                                                            <th>
                                                                <asp:LinkButton ID="lnkPublishedBy" runat="server" ToolTip="Sort By Created By" CommandName="Sort" CommandArgument="[M].[FirstName]"
                                                                    Width="50%" Text="Created By" TabIndex="6" />
                                                            </th>
                                                            <th style="white-space: nowrap; min-width: 60px">
                                                                <asp:LinkButton ID="btnJobTitle" runat="server" ToolTip="Sort By Job Title" CommandName="Sort" CommandArgument="[J].[JobTitle]"
                                                                    Width="50%" Text="Job Title" TabIndex="5" />
                                                            </th>
                                                        </tr>
                                                        <tr id="itemPlaceholder" runat="server">
                                                        </tr>
                                                        <tr class="Pager">
                                                            <td colspan="3" runat="server" id="tdPager">
                                                                <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </LayoutTemplate>
                                                <EmptyDataTemplate>
                                                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                                        <tr>
                                                            <td>
                                                                No data was returned.
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EmptyDataTemplate>
                                                <ItemTemplate>
                                                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                                        <td>
                                                            <asp:Label ID="lblPostedDate" runat="server" Width="80px" />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblPublishedBy" runat="server" Width="80px" />
                                                        </td>
                                                        <td>
                                                            <asp:HyperLink ID="lnkJobTitle" runat="server"  TabIndex="8" Width="120px"></asp:HyperLink>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </li>
            <li class="widget WidgetColor">
                <div class="widget-head">
                    <h3>
                        My Candidates</h3>
                </div>
                <div class="widget-content">
                    <asp:UpdatePanel ID="vendorCandidate" runat="server">
                        <ContentTemplate>
                            <ucl:VendorCandidate ID="uclVendorCandidates" runat="server" IsVendor="true" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </li>
            <li class="widget WidgetColor">
                <div class="widget-head">
                    <h3>
                        Candidate Performance</h3>
                </div>
                <div class="widget-content">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <ucl:VendorCandidatePerformance ID="VendorCandidatePerformance" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </li>       
        </ul>
        <ul id="PnlRight" class="column" runat="server" style="float: right">
           <li class="widget WidgetColor">
                <div class="widget-head">
                    <h3>
                        My Submissions</h3>
                </div>
                <div class="widget-content">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <ucl:VendorMySubmission ID="uclVendorSubmissions" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </li>
            
              <%--  **************************************--%>
<%--          <ul id="Ul1" class="column" runat="server" style="float: right">
--%>            <li class="widget WidgetColor">
                <div class="widget-head">
                    <h3>
                        My Performance</h3>
                </div>
                <div class="widget-content">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <ucl:VendorMyPerformance ID="VendorMyPerformance" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </li>
       <%--      <ul id="Ul2" class="column" runat="server" style="float: right">--%>
            <li class="widget WidgetColor">
                <div class="widget-head">
                    <h3>
                        Active Job Openings</h3>
                </div>
                <div class="widget-content">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <ucl:VendorActiveJobOpenings ID="VendorActiveJobOpenings" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </li>
        <%--</ul>--%>
        <%--</ul>--%>
        </ul>       
    
         
     
       <%-- ************************************--%>
    </div>

  

</asp:Content>
