﻿<%@ Page Language="C#" MasterPageFile="~/Vendor/VendorPortalLeft.master" AutoEventWireup="true" CodeFile="MySubmissions.aspx.cs" Inherits="TPS360.Web.UI.MySubmissions" Title="Untitled Page" EnableEventValidation ="false"  %>

<%@ Register Src ="~/Controls/VendorMySubmission.ascx"  TagName ="VendorSub" TagPrefix ="ucl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHomeMasterTitle" Runat="Server">
Requisitions
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphVendorMaster" Runat="Server">
<div class="TabPanelHeader">
My Submissions
</div> 
<asp:HiddenField ID="hdnMySubmission" runat="server" />
<ucl:VendorSub ID="uclSubmission" runat ="server" />

</asp:Content>

