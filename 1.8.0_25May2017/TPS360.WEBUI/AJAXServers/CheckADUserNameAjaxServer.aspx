﻿<%---------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CheckADUserNameAjaxServer.aspx
    Description:  Validating ADUser Name
    Created By:   Prasanth Kumar G
    Created On: 10/Jun/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------------------------- 
--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CheckADUserNameAjaxServer.aspx.cs" Inherits="TPS360.Web.UI.CheckADUserNameAjaxServer"   %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript">
    
      function pageLoad() {
      }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    </div>
    </form>
</body>
</html>
