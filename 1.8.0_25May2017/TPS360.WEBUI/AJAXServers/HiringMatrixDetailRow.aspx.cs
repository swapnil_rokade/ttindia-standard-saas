﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: HiringMatrixDetailRow.aspx.cs
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
 *    0.1               15/March/2017       pravin khot          if (hmatrix.ExpectedYearlyRate != null && hmatrix.ExpectedYearlyRate != 0) expectedSalary =. 
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI;
using TPS360.Common.BusinessEntities;
using TPS360.BusinessFacade;
using TPS360.Web.UI.Helper;
using System.Collections.Generic;
using System.Text;

public partial class HiringMatrixDetailRow : BasePage
{
    private DateTime getTime(DateTime date, int duration)
    {
        if (duration > 0)
        {
            duration = duration / 60;
        }
        return date.AddMinutes(Convert.ToDouble(duration));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            int candidateId = Convert.ToInt32(Request["CandidateId"]);
            int JobPostingID = Convert.ToInt32(Request["JobPostingId"]);


            IFacade facade = new Facade();
            HiringMatrix hmatrix = facade.getHiringMatrixByJobPostingIDAndCandidateId(JobPostingID, candidateId);

            Response.Clear();
            Response.ContentType = "text/xml";


            StringBuilder strBUildResult = new StringBuilder();
            strBUildResult.Append("<td></td>");


            strBUildResult.Append("<td colspan =\"2\" align =\"left\"  style =\" font-weight :normal;\">");
            strBUildResult.Append("<b>Added By: </b>" + hmatrix.AddedBy + " on " + hmatrix.AddedOn.ToShortDateString() + " " + hmatrix.AddedOn.ToShortTimeString() + "<br />");

            string sourceName = string.Empty;
            if (hmatrix.SourceID != 0 && hmatrix.SourceName != "") { sourceName = hmatrix.SourceName; }
            else sourceName  = hmatrix.AddedBy;

            string sname = "  <a href=\"#\" onclick= \"EditHiringMatrixSource('0','" + hmatrix.MemberId + "','spSource" + hmatrix.MemberId + "','" + JobPostingID + "','" + hmatrix.FirstName + " " + hmatrix.LastName + "');window.event.returnValue=false; return false;\" >   Edit Source </a>";
            if (hmatrix.SourceID == hmatrix.MemberId  )
            {
                sourceName = "Candidate Portal";
                sname = "";
            }                
            
            //strBUildResult.Append("<b>Sourced By: </b><span id=\"spSource" + hmatrix .MemberId +"\">" + sourceName  + "</span>"+sname +"<br />");
            strBUildResult.Append("<b>Primary Manager:</b> " + hmatrix.PrimaryManager + " <br />");

            string sk = "<span Title=\"" + hmatrix.Skills + "\" >" + (hmatrix.Skills.Length <= 50 ? hmatrix.Skills : (hmatrix.Skills.Substring(0, 50) + "...") )+ "</span>";
            strBUildResult.Append("<b>Skills: </b>" + sk + "<br />");


            string avil = "<span>";
            if (hmatrix.AvailabilityText == "Available From") avil += hmatrix.AvailabilityText + " " + hmatrix.AvailableFrom.ToShortDateString();
            else avil += hmatrix.AvailabilityText;
            avil += "</span>";


            strBUildResult.Append("<b>Availability:</b>" + avil + " <br />");
            strBUildResult.Append("<b>Work Schedule:</b> " + hmatrix.WorkSchedule + "<br />");
           // strBUildResult.Append("<b>Work Status:</b> " + hmatrix.WorkAuthorization + "<br />");

            string obj = "<span Title=\""+ (MiscUtil.RemoveScript(hmatrix.Objective == null ? "" : hmatrix.Objective) )+"\">" + (hmatrix.Objective.Length <= 50 ? MiscUtil.RemoveScript(hmatrix.Objective) : MiscUtil.RemoveScript(hmatrix.Objective.Substring(0, 50) + "...")) + "</span>";//1.1
            strBUildResult.Append("<b>Objective:</b> " + obj + " <br />");

            string rem = "<span Title=\"" + (MiscUtil.RemoveScript(hmatrix.Remarks == null ? "" : hmatrix.Remarks)) + "\">" + (hmatrix.Remarks.Length <= 50 ? MiscUtil.RemoveScript(hmatrix.Remarks) : MiscUtil.RemoveScript(hmatrix.Remarks.Substring(0, 50) + "...")) + "</span>";//1.1
            strBUildResult.Append("<b>Remarks:</b> " + rem + " <br />");

            string hr = "";
            strBUildResult.Append("<b>Hiring Notes:</b>");
            
            strBUildResult.Append("<div id=\"divHiringNote" + hmatrix.MemberId + "\">" + (hmatrix .HiringNote .Trim ()==""?"":( hmatrix.HiringNote.Replace("\n", "<br/>") + "<br/>" + "Last updated by " + hmatrix.NoteUpdatorName + " on " + hmatrix.NoteUpdatedDate.ToShortDateString() + " " + hmatrix.NoteUpdatedDate.ToShortTimeString() + ".<br/>")) + "</div>");
                hr += "<input type=\"hidden\" id=\"hfHiringnote" + hmatrix.MemberId + "\" value=\"" + hmatrix.HiringNote + "\"/> ";
                hr += "<input type=\"hidden\" id=\"hfUpdatorName" + hmatrix.MemberId + "\" value=\"" + hmatrix.NoteUpdatedDate + "\"/> ";
            hr += "<input type=\"hidden\" id=\"hfUpdateTime" + hmatrix.MemberId + "\" value=\"" + (hmatrix .HiringNote .Trim ()==""?"":( (hmatrix.NoteUpdatedDate.ToShortDateString() + " " + hmatrix.NoteUpdatedDate.ToShortTimeString())))+ "\"/> ";

            hr += "<a id=\"anchorEditHiringNotes\" href='#' onclick=\"SaveHiringNote('hfHiringnote" + hmatrix.MemberId + "','" + hmatrix.MemberId + "','divHiringNote" + hmatrix.MemberId + "','hfUpdatorName" + hmatrix.MemberId + "','hfUpdateTime" + hmatrix.MemberId + "','" + JobPostingID + "','" + hmatrix.FirstName + " " + hmatrix.LastName + "'); window.event.returnValue=false; return  false;\">Edit Hiring Notes</a>";
               
            strBUildResult.Append(hr);
            strBUildResult.Append("</td>");

            strBUildResult.Append("<td colspan =\"4\" align =\"left\" style =\" vertical-align :top ; font-weight :normal;\">");




            //Binding PrimaryEmail
            string email="";
            if (MiscUtil.IsValidMailSetting(Facade, base.CurrentMember.Id))
            {
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_MEMBER_ID, hmatrix.MemberId.ToString());
                email  = " <a href=\"#\" onclick=\"window.open('" + url + "','NewMail','height=670,width=770,toolbar=no,directories=no,status=no,linemenubar=no,scrollbar=no,resizeable=no,modal=no');\">" + hmatrix .PrimaryEmail + " </a>";

            }
            else email = "<a href=mailto:" + hmatrix.PrimaryEmail + ">" + hmatrix.PrimaryEmail + "</a>";





            strBUildResult.Append("<b>Email:</b> " + email   + " <br />");
            strBUildResult.Append("<b>Experience (yrs):</b> " + hmatrix.TotalExperienceYears  + " <br />");
            

            string interview = string.Empty;
            IList<Interview> interviewlist = new List<Interview>();
            interviewlist = facade.GetInterviewByMemberIdAndJobPostingID(hmatrix.MemberId, JobPostingID );
            if (interviewlist != null)
            {

                          
                int i = 0;
                interviewlist = interviewlist.OrderByDescending(f => f.StartDateTime).ToList();
                foreach (Interview inter in interviewlist)
                {
                    SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ATS.ATS_INTERVIEW_SCHEDULE, string.Empty, UrlConstants.PARAM_MEMBER_ID, hmatrix.MemberId.ToString(), UrlConstants.PARAM_INTERVIEWSCHEDULE_ID, inter.Id.ToString(), UrlConstants.PARAM_SITEMAP_ID, "403");
                    string datetime = string.Empty;
                    if (inter.AllDayEvent)
                    {
                        datetime = inter.StartDateTime.ToShortDateString() + " All Day" + "";
                    }
                    else
                    {
                        DateTime date = new DateTime();

                        date = getTime(inter.StartDateTime, inter.Duration);
                        if (date.Hour > 12 || inter.StartDateTime.Hour > 12)
                            datetime = inter.StartDateTime.ToShortDateString() + " " + inter.StartDateTime.ToString("hh:mm tt") + " - " + getTime(inter.StartDateTime, inter.Duration).ToString("hh:mm tt") + "";
                        else
                            datetime = inter.StartDateTime.ToShortDateString() + " " + inter.StartDateTime.ToString("hh:mm") + " - " + getTime(inter.StartDateTime, inter.Duration).ToString("hh:mm tt") + "";
                    }

                    bool isAccess = false;
                    ArrayList AList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                    if (AList.Contains(403))
                        isAccess = true;
                    else
                        isAccess = false;


                    if (isAccess)
                    {
                        if (interview != "")
                        {
                            interview = interview + ",<br/>";
                        }
                        interview = interview + "<a href='" + url + "' TARGET='_blank' >" + datetime + "</a>";
                    }
                    else
                        interview = interview + datetime;
                }
                strBUildResult.Append("<b>Interviews:</b>"+ interview +" <br/>");
            }



         
            strBUildResult.Append("<b>Mobile Phone:</b> " + hmatrix.CellPhone + " <br />");
            


            string currentSalary = "";
            string expectedSalary = "";
            string[] payCycle = { "", " / Hour", " / Day", " / Month", " / Year" };
       

            string expectedrate = "";
            string expectedmaxRate = "";

            if (hmatrix.CurrentYearlyRate != null && hmatrix.CurrentYearlyRate != 0) currentSalary = hmatrix.CurrentYearlyCurrency + " " + hmatrix.CurrentYearlyRate.ToString() + (hmatrix.CurrentYearlyCurrency == "INR" ? " Lacs" : "") + payCycle[hmatrix.CurrentSalaryPayCycle];
            //*********Code modify by pravin khot on 15/March/2017***********
            //if (hmatrix.ExpectedYearlyRate != null && hmatrix.ExpectedYearlyRate != 0 && hmatrix.ExpectedYearlyMaxRate != null && hmatrix.ExpectedYearlyMaxRate != 0) expectedSalary = hmatrix.ExpectedYearlyCurrency + " " + hmatrix.ExpectedYearlyRate.ToString() + " - " + hmatrix.ExpectedYearlyMaxRate.ToString() + (hmatrix.ExpectedYearlyCurrency == "INR" ? " Lacs" : "") + payCycle[hmatrix.ExpectedSalaryPayCycle];
            if (hmatrix.ExpectedYearlyRate != null && hmatrix.ExpectedYearlyRate != 0) expectedSalary = hmatrix.ExpectedYearlyCurrency + " " + hmatrix.ExpectedYearlyRate.ToString() + (hmatrix.ExpectedYearlyCurrency == "INR" ? " Lacs" : "") + payCycle[hmatrix.ExpectedSalaryPayCycle];
            //****************END***************************

            //if (CurrentJobPosting.PayRate != "" && CurrentJobPosting.PayRate != "Open" && Convert.ToDouble(CurrentJobPosting.PayRate) > 0)
            //{
            //    if (Convert.ToDecimal(CurrentJobPosting.PayRate) >= hiringMatrix.ExpectedYearlyRate && Convert.ToDecimal(CurrentJobPosting.PayRate) <= hiringMatrix.ExpectedYearlyMaxRate)
            //    {
            //        if (hiringMatrix.ExpectedYearlyRate != null && hiringMatrix.ExpectedYearlyRate != 0 && hiringMatrix.ExpectedYearlyMaxRate != null && hiringMatrix.ExpectedYearlyMaxRate != 0) lblCandidateExpectedSalary.Text = "<span class=highlight>" + lblCandidateExpectedSalary.Text + "</span>";
            //    }
            //}

            strBUildResult.Append("<b>Current Salary:</b> " + currentSalary  +"<br/>");
            strBUildResult.Append("<b>Expected Salary:</b>" + expectedSalary  + " <br />");
            strBUildResult.Append("<b>Current Company:</b>" + hmatrix.LastEmployer + " <br />");
            strBUildResult.Append("<b>Highest Degree:</b> " + hmatrix.HighestDegree + "<br />");
            //strBUildResult.Append("<b>Candidate Type:</b>" + hmatrix.MemberType + "<br />");
            strBUildResult.Append("<b>Last Updated:</b> " + hmatrix.UpdateDate);
            strBUildResult.Append("</td>");
            Response.Write(strBUildResult.ToString());
            Response.End();
        }
    }

    string AppendHighLight(string value,bool IsAppendHighLight)
    {
        if (IsAppendHighLight)
            return "<span class=highlight>" + value + "</span>";
        else
            return value;
    }

}
