﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
public partial class GraphicalReport : System.Web.UI.Page
{
    bool isTeamReport = false;
    public enum ProductivityField 
    {
        [System.ComponentModel.Description("# of New Candidates")]
        NewCandidateCount = 1,
        
        [System.ComponentModel.Description("# of Requisitions")]
        JobCount = 2,
        
        [System.ComponentModel.Description("# of Candidate Sourced")]
        CandidateSourceCount = 3,

        [System.ComponentModel.Description("# of Candidate Submission")]
        CandidateSubmissionCount = 4,

        [System.ComponentModel.Description("# of Interview")]
        InterviewCount = 5,

        [System.ComponentModel.Description("# of Candidate Offered")]
        OfferCount = 6,

        [System.ComponentModel.Description("# of Candidate Offered Decline")]
        OfferDeclineCount = 7,

        [System.ComponentModel.Description("# of Candidate Joined")]
        JoinCount = 8,

        [System.ComponentModel.Description("# of Pending Joiners")]
        PendingJoinersCount = 9
    }
    public class ProductivityByEmployeeJsonClass 
    {
        
        public string name
        {
            get;
            set;
        }
        public List<int> data
        {
            get;
            set;
        }
        public List<string> empList
        {
            get;
            set;
        }
    }
    public struct dateAndValue
    {
        public int date;
        public int month;
        public int year;
        public int value;
    }
    public class ProductivityByDayJsonClass
    {

        [JsonProperty("name")]
        public string name;

        [JsonProperty("data")]
        public List<dateAndValue> data;
        
    }
   
    public class ProductivityJsonClass 
    {
       

        public List<ProductivityByEmployeeJsonClass> byEmployee
        {
            get;
            set;
        }
        public List<ProductivityByDayJsonClass> byDay
        {
            get;
            set;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        string RType = Request.QueryString["RType"].ToString();
        
        if (RType == "ER")
        {
            EmployeeReferralReport();
        }
        if (RType == "VS")
        {
            VendorSubmissionReport();
        }

        if (Request.QueryString["isTeamReport"].ToString() != "" && Request.QueryString["isTeamReport"].ToString() != null)
        {
            if (Convert.ToBoolean(Request.QueryString["isTeamReport"].ToString()))
            {
                isTeamReport = true;
            }
        }
        if (RType == "PRByE")
        {
            ProductivityReportEmployee();
        }
        
        Response.End();
    }
    
    public ProductivityByDayJsonClass GetJsonDataforSerialize(ProductivityField field,IList<EmployeeProductivity> listByDate)
    {
        ProductivityByDayJsonClass jsonData = new ProductivityByDayJsonClass();
        string name = "";

        List<dateAndValue> arr = new List<dateAndValue>();
        try 
        {
            DateTime date;
            dateAndValue obj = new dateAndValue();
            switch (field)
            {
                case ProductivityField.NewCandidateCount:
                    jsonData.name = "# of New Candidates";
                    foreach (EmployeeProductivity ep in listByDate) 
                    {
                        obj.year = Convert.ToInt32(ep.EmployeeName.Substring(0, 4));
                        obj.month = Convert.ToInt32(ep.EmployeeName.Substring(4, 2));
                        obj.date = Convert.ToInt32(ep.EmployeeName.Substring(6, 2));
                        obj.value = ep.NewCandidateCount;
                        arr.Add(obj);
                    }

                    jsonData.data = arr;
                    break;
                case ProductivityField.JobCount:
                    jsonData.name = "# of Requisitions";
                    foreach (EmployeeProductivity ep in listByDate)
                    {
                        obj.year = Convert.ToInt32(ep.EmployeeName.Substring(0, 4));
                        obj.month = Convert.ToInt32(ep.EmployeeName.Substring(4, 2));
                        obj.date = Convert.ToInt32(ep.EmployeeName.Substring(6, 2));
                        obj.value = ep.JobCount;
                        arr.Add(obj);
                    }

                    jsonData.data = arr;
                    break;
                case ProductivityField.CandidateSourceCount:
                    jsonData.name = "# of Candidate Sourced";
                    foreach (EmployeeProductivity ep in listByDate)
                    {
                        obj.year = Convert.ToInt32(ep.EmployeeName.Substring(0, 4));
                        obj.month = Convert.ToInt32(ep.EmployeeName.Substring(4, 2));
                        obj.date = Convert.ToInt32(ep.EmployeeName.Substring(6, 2));
                        obj.value = ep.CandidateSourcedCount;
                        arr.Add(obj);
                    }

                    jsonData.data = arr;
                    break;
                case ProductivityField.CandidateSubmissionCount:
                    jsonData.name = "# of Candidate Submission";
                    foreach (EmployeeProductivity ep in listByDate)
                    {
                        obj.year = Convert.ToInt32(ep.EmployeeName.Substring(0, 4));
                        obj.month = Convert.ToInt32(ep.EmployeeName.Substring(4, 2));
                        obj.date = Convert.ToInt32(ep.EmployeeName.Substring(6, 2));
                        obj.value = ep.SubmissionCount;
                        arr.Add(obj);
                    }

                    jsonData.data = arr;
                    break;
                case ProductivityField.InterviewCount:
                    jsonData.name = "# of Interview";
                    foreach (EmployeeProductivity ep in listByDate)
                    {
                        obj.year = Convert.ToInt32(ep.EmployeeName.Substring(0, 4));
                        obj.month = Convert.ToInt32(ep.EmployeeName.Substring(4, 2));
                        obj.date = Convert.ToInt32(ep.EmployeeName.Substring(6, 2));
                        obj.value = ep.InterviewsCount;
                        arr.Add(obj);
                    }

                    jsonData.data = arr;
                    break;
                case ProductivityField.OfferCount:
                    jsonData.name = "# of Candidate Offered";
                    foreach (EmployeeProductivity ep in listByDate)
                    {
                        obj.year = Convert.ToInt32(ep.EmployeeName.Substring(0, 4));
                        obj.month = Convert.ToInt32(ep.EmployeeName.Substring(4, 2));
                        obj.date = Convert.ToInt32(ep.EmployeeName.Substring(6, 2));
                        obj.value = ep.OfferCount;
                        arr.Add(obj);
                    }

                    jsonData.data = arr;
                    break;
                case ProductivityField.OfferDeclineCount:
                    jsonData.name = "# of Candidate Offered Decline";
                    foreach (EmployeeProductivity ep in listByDate)
                    {
                        obj.year = Convert.ToInt32(ep.EmployeeName.Substring(0, 4));
                        obj.month = Convert.ToInt32(ep.EmployeeName.Substring(4, 2));
                        obj.date = Convert.ToInt32(ep.EmployeeName.Substring(6, 2));
                        obj.value = ep.OfferRejectedCount;
                        arr.Add(obj);
                    }

                    jsonData.data = arr;
                    break;
                case ProductivityField.JoinCount:
                    jsonData.name = "# of Candidate Joined";
                    foreach (EmployeeProductivity ep in listByDate)
                    {
                        obj.year = Convert.ToInt32(ep.EmployeeName.Substring(0, 4));
                        obj.month = Convert.ToInt32(ep.EmployeeName.Substring(4, 2));
                        obj.date = Convert.ToInt32(ep.EmployeeName.Substring(6, 2));
                        obj.value = ep.JoinedCount;
                        arr.Add(obj);
                    }

                    jsonData.data = arr;
                    break;
                case ProductivityField.PendingJoinersCount:
                    jsonData.name = "# of Pending Joiners";
                    foreach (EmployeeProductivity ep in listByDate)
                    {
                        obj.year = Convert.ToInt32(ep.EmployeeName.Substring(0, 4));
                        obj.month = Convert.ToInt32(ep.EmployeeName.Substring(4, 2));
                        obj.date = Convert.ToInt32(ep.EmployeeName.Substring(6, 2));
                        obj.value = ep.PendingJoiners;
                        arr.Add(obj);
                    }

                    jsonData.data = arr;
                    break;
            } 
            
        }
        catch (Exception ex) { }
        return jsonData;
    }
    private void ProductivityReportEmployee()
    {

        string UID = Request.QueryString["UID"].ToString();
        string currentUserId = "";
        if (Request.QueryString["CurrentUserId"].ToString() != null && Request.QueryString["CurrentUserId"].ToString() != "") 
        {
            currentUserId = Request.QueryString["CurrentUserId"].ToString();
        }
        string TeamID= Request.QueryString["TID"].ToString();
        string StartDate = Request.QueryString["SD"].ToString();
        string EndDate = Request.QueryString["ED"].ToString();

        IFacade Facade = new Facade();
        TPS360.Common.Shared.PagedRequest request = new TPS360.Common.Shared.PagedRequest();
       if(UID !="" &&  UID !="0") request.Conditions.Add("MemberID", UID);
       if (TeamID !=""&& TeamID != "0")  request.Conditions.Add("TeamID", TeamID);
       string teamMember = "";
       if (isTeamReport)
       {
           
           if (TeamID == "" || TeamID == "0" && isTeamReport)
           {
               request.Conditions.Add("IsTeamReport", currentUserId);
           }
       }
       request.Conditions.Add("StartDate", new DateTime(Convert .ToInt32 ( StartDate.Substring(0, 4)),Convert .ToInt32 ( StartDate.Substring(4, 2)),Convert .ToInt32 ( StartDate.Substring(6, 2))).ToShortDateString());
       request.Conditions.Add("EndDate", new DateTime(Convert .ToInt32 (EndDate.Substring(0, 4)), Convert .ToInt32 (EndDate.Substring(4, 2)),Convert .ToInt32 ( EndDate.Substring(6, 2))).ToShortDateString());
        IList<EmployeeProductivity > list = Facade.GetEmployeeProductivityReport (request);

        string test = JsonConvert.SerializeObject(list, Formatting.Indented,
    new JsonSerializerSettings()
        {
            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
        }
    );

        StringBuilder ResultString = new StringBuilder();
        ResultString.Append("<Result>");

        if (list == null) list = new List<EmployeeProductivity>();

        StringBuilder builder = new StringBuilder();


        ProductivityByEmployeeJsonClass objEmpName = new ProductivityByEmployeeJsonClass();
        List<string> empList = new List<string>();
        foreach (EmployeeProductivity ep in list)
        {
            empList.Add(ep.EmployeeName);
        }
        objEmpName.name = "User";
        objEmpName.empList = empList;



        ProductivityByEmployeeJsonClass objNoOfCandidate = new ProductivityByEmployeeJsonClass();
        List<int> candidateCount = new List<int>();
        foreach (EmployeeProductivity ep in list)
        {
            candidateCount.Add(ep.NewCandidateCount);
        }
        objNoOfCandidate.name = "# of New Candidates";
        objNoOfCandidate.data = candidateCount;


        ProductivityByEmployeeJsonClass objJobCount = new ProductivityByEmployeeJsonClass();
        List<int> jobCount = new List<int>();
        foreach (EmployeeProductivity ep in list)
        {
            jobCount.Add(ep.JobCount);
        }
        objJobCount.name = "# of Requisitions";
        objJobCount.data = jobCount;

        ProductivityByEmployeeJsonClass objSourceCount = new ProductivityByEmployeeJsonClass();
        List<int> sourceCount = new List<int>();
        foreach (EmployeeProductivity ep in list)
        {
            sourceCount.Add(ep.CandidateSourcedCount);
        }
        objSourceCount.name = "# of Candidates Sourced";
        objSourceCount.data = sourceCount;


        ProductivityByEmployeeJsonClass objSubmissionCount = new ProductivityByEmployeeJsonClass();
        List<int> SubmissionCount = new List<int>();
        foreach (EmployeeProductivity ep in list)
        {
            SubmissionCount.Add(ep.SubmissionCount);
        }
        objSubmissionCount.name = "# of Candidate Submissions";
        objSubmissionCount.data = SubmissionCount;


        ProductivityByEmployeeJsonClass objInterviewsCount = new ProductivityByEmployeeJsonClass();
        List<int> InterviewCount = new List<int>();
        foreach (EmployeeProductivity ep in list)
        {
            InterviewCount.Add(ep.InterviewsCount);
        }
        objInterviewsCount.name = "# of Interviews";
        objInterviewsCount.data = InterviewCount;


        ProductivityByEmployeeJsonClass objOfferCount = new ProductivityByEmployeeJsonClass();
        List<int> OfferCount = new List<int>();
        foreach (EmployeeProductivity ep in list)
        {
            OfferCount.Add(ep.OfferCount);
        }
        objOfferCount.name = "# of Candidate Offers";
        objOfferCount.data = OfferCount;


        ProductivityByEmployeeJsonClass objOfferRejectedCount = new ProductivityByEmployeeJsonClass();
        List<int> OfferRejectedCount = new List<int>();
        foreach (EmployeeProductivity ep in list)
        {
            OfferRejectedCount.Add(ep.OfferRejectedCount);
        }
        objOfferRejectedCount.name = "# of Candidate Offers Decline";
        objOfferRejectedCount.data = OfferRejectedCount;

        ProductivityByEmployeeJsonClass objJoinedCount = new ProductivityByEmployeeJsonClass();
        List<int> JoinedCount = new List<int>();
        foreach (EmployeeProductivity ep in list)
        {
            JoinedCount.Add(ep.JoinedCount);
        }
        objJoinedCount.name = "# of Candidates Joined";
        objJoinedCount.data = JoinedCount;

        ProductivityByEmployeeJsonClass objPendingJoiners = new ProductivityByEmployeeJsonClass();
        List<int> PendingJoiners = new List<int>();
        foreach (EmployeeProductivity ep in list)
        {
            PendingJoiners.Add(ep.PendingJoiners);
        }
        objPendingJoiners.name = "# of Pending Joiners";
        objPendingJoiners.data = PendingJoiners;

       


        IList<EmployeeProductivity> listByDate = Facade.GetEmployeeProductivityReportByDate(UID, TeamID, Convert.ToInt32(StartDate), Convert.ToInt32(EndDate));
        List<ProductivityByDayJsonClass> jsonData2 = new List<ProductivityByDayJsonClass>();


        jsonData2.Add(GetJsonDataforSerialize(ProductivityField.NewCandidateCount, listByDate));
        jsonData2.Add(GetJsonDataforSerialize(ProductivityField.JobCount, listByDate));
        jsonData2.Add(GetJsonDataforSerialize(ProductivityField.CandidateSourceCount, listByDate));
        jsonData2.Add(GetJsonDataforSerialize(ProductivityField.CandidateSubmissionCount, listByDate));
        jsonData2.Add(GetJsonDataforSerialize(ProductivityField.InterviewCount, listByDate));
        jsonData2.Add(GetJsonDataforSerialize(ProductivityField.OfferCount, listByDate));
        jsonData2.Add(GetJsonDataforSerialize(ProductivityField.OfferDeclineCount, listByDate));
        jsonData2.Add(GetJsonDataforSerialize(ProductivityField.JoinCount, listByDate));
        jsonData2.Add(GetJsonDataforSerialize(ProductivityField.PendingJoinersCount, listByDate));
        

        



        


        List<ProductivityByEmployeeJsonClass> jsonData = new List<ProductivityByEmployeeJsonClass>();
        jsonData.Add(objNoOfCandidate);
        jsonData.Add(objJobCount);
        jsonData.Add(objSourceCount);
        jsonData.Add(objSubmissionCount);
        jsonData.Add(objInterviewsCount);
        jsonData.Add(objOfferCount);
        jsonData.Add(objOfferRejectedCount);
        jsonData.Add(objJoinedCount);
        jsonData.Add(objPendingJoiners);
        jsonData.Add(objEmpName);

       
        

        ProductivityJsonClass objJson = new ProductivityJsonClass();
        objJson.byEmployee = jsonData;
        objJson.byDay = jsonData2;


        string ttt = JsonConvert.SerializeObject(objJson);

        string tttt = JsonConvert.SerializeObject(jsonData2);


        Response.Clear();
        Response.ContentType = "text/json";
        

        Response.Write(ttt);
        
        Response.End();
    }


    private void EmployeeReferralReport()
    {

        string JID = Request.QueryString["JID"].ToString();

        string ReferrerId = Request.QueryString["RID"].ToString();
        string StartDate = Request.QueryString["SD"].ToString();
        string EndDate = Request.QueryString["ED"].ToString();
        
        IFacade Facade = new Facade();
        IList<ListWithCount> list = Facade.EmployeeReferral_GetCountGroupbyDate(JID ,Convert .ToInt32 (ReferrerId ),Convert .ToInt32 (StartDate ),Convert .ToInt32 (EndDate ));
        StringBuilder ResultString = new StringBuilder();
        ResultString.Append("<Result>");
        if (Convert.ToInt32(StartDate) > 0)
        {
            DateTime newstartdate = new DateTime(Convert.ToInt32(StartDate.Substring(0, 4)), Convert.ToInt32(StartDate.Substring(4, 2)), Convert.ToInt32(StartDate.Substring(6, 2)));
            DateTime newEnddate = new DateTime(Convert.ToInt32(EndDate.Substring(0, 4)), Convert.ToInt32(EndDate.Substring(4, 2)), Convert.ToInt32(EndDate.Substring(6, 2)));
            for (newstartdate = newstartdate; newstartdate <= newEnddate; newstartdate=newstartdate.AddDays(1))
            {
                ResultString.Append("<Row><Year>");
                ResultString.Append(newstartdate .Year .ToString ());
                ResultString.Append("</Year><Month>");
                ResultString.Append(newstartdate .Month .ToString ());
                ResultString.Append("</Month><Day>");
                ResultString.Append(newstartdate .Day .ToString ());
                ResultString.Append("</Day><Count>");
                var s = list.Where(x => Convert.ToDateTime(x.Name) == newstartdate);
                if (s.Count() > 0)
                    ResultString.Append(s.ToList()[0].Count.ToString());
                else ResultString.Append("0");
                ResultString.Append("</Count></Row>");
            }
        }
        else
        {
            foreach (ListWithCount l in list)
            {
                ResultString.Append("<Row><Year>");
                ResultString.Append(Convert.ToDateTime(l.Name).Year.ToString());
                ResultString.Append("</Year><Month>");
                ResultString.Append(Convert.ToDateTime(l.Name).Month.ToString());
                ResultString.Append("</Month><Day>");
                ResultString.Append(Convert.ToDateTime(l.Name).Day.ToString());
                ResultString.Append("</Day><Count>");
                ResultString.Append(l.Count.ToString());
                ResultString.Append("</Count></Row>");
            }
        }
        ResultString.Append("</Result>");
        Response.Clear();
        Response.ContentType = "text/xml";

        Response.Write(ResultString.ToString());
        //end the response
        Response.End();
    }

    private void VendorSubmissionReport()
    {

        string JID = Request.QueryString["JID"].ToString();

        string VendorID = Request.QueryString["VID"].ToString();
        string ContactID = Request.QueryString["CCID"].ToString();
        string StartDate = Request.QueryString["SD"].ToString();
        string EndDate = Request.QueryString["ED"].ToString();

        IFacade Facade = new Facade();
        IList<ListWithCount> list = Facade.GetAllVendorSubmissionsGroupByDate (Convert.ToInt32(JID), Convert.ToInt32(VendorID ),Convert .ToInt32 (ContactID ), Convert.ToInt32(StartDate), Convert.ToInt32(EndDate));
        StringBuilder ResultString = new StringBuilder();
        ResultString.Append("<Result>");
        if (Convert.ToInt32(StartDate) > 0)
        {
            DateTime newstartdate = new DateTime(Convert.ToInt32(StartDate.Substring(0, 4)), Convert.ToInt32(StartDate.Substring(4, 2)), Convert.ToInt32(StartDate.Substring(6, 2)));
            DateTime newEnddate = new DateTime(Convert.ToInt32(EndDate.Substring(0, 4)), Convert.ToInt32(EndDate.Substring(4, 2)), Convert.ToInt32(EndDate.Substring(6, 2)));
            for (newstartdate = newstartdate; newstartdate <= newEnddate; newstartdate = newstartdate.AddDays(1))
            {
                ResultString.Append("<Row><Year>");
                ResultString.Append(newstartdate.Year.ToString());
                ResultString.Append("</Year><Month>");
                ResultString.Append(newstartdate.Month.ToString());
                ResultString.Append("</Month><Day>");
                ResultString.Append(newstartdate.Day.ToString());
                ResultString.Append("</Day><Count>");
                var s = list.Where(x => Convert.ToDateTime(x.Name) == newstartdate);
                if (s.Count() > 0)
                    ResultString.Append(s.ToList()[0].Count.ToString());
                else ResultString.Append("0");
                ResultString.Append("</Count></Row>");
            }
        }
        else
        {
            foreach (ListWithCount l in list)
            {
                ResultString.Append("<Row><Year>");
                ResultString.Append(Convert.ToDateTime(l.Name).Year.ToString());
                ResultString.Append("</Year><Month>");
                ResultString.Append(Convert.ToDateTime(l.Name).Month.ToString());
                ResultString.Append("</Month><Day>");
                ResultString.Append(Convert.ToDateTime(l.Name).Day.ToString());
                ResultString.Append("</Day><Count>");
                ResultString.Append(l.Count.ToString());
                ResultString.Append("</Count></Row>");
            }
        }
        ResultString.Append("</Result>");
        Response.Clear();
        Response.ContentType = "text/xml";

        Response.Write(ResultString.ToString());
        //end the response
        Response.End();
    }
}
