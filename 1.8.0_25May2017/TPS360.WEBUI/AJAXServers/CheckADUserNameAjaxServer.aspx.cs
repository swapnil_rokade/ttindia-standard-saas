﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CheckADUserNameAjaxServer.aspx.cs
    Description:  Validating ADUser Name
    Created By:   Prasanth Kumar G
    Created On: 28/May/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TPS360.Common.BusinessEntities;
using TPS360.BusinessFacade;
namespace TPS360.Web.UI
{
    /// <summary>
    /// Summary description for AjaxServer.
    /// </summary>System.Web.UI.Page
    public partial class CheckADUserNameAjaxServer : BasePage  
    {
        //Sends the response back, with XML data.
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                string selectedUserId = Request["SelectedUserId"];
                string memberID = Request["MemberId"];
                if (memberID == null) memberID = "0";
                IFacade Facade = new Facade();
                if(selectedUserId !=null)
                {
                    if (selectedUserId.Length > 0 && (memberID == "0" || memberID == "") ) 
                {
                    Response.Clear();
                    
                    Member _member = Facade.GetMemberByMemberUserName(selectedUserId);
                    string firstName = "";
                    firstName += "<member>";
                    if (_member != null && !Facade.IsMemberCandidateSourceExpired(selectedUserId))
                    {
                        firstName += "<firstName>";
                        firstName +=MiscUtil .RemoveScript ( _member.FirstName,string .Empty );
                        firstName += "</firstName>";
                        firstName += "</member>";
                        Response.Clear();
                        Response.ContentType = "text/xml";
                        Response.Write(firstName);
                        //end the response
                        Response.End();
                    }
                    else
                    {
                      
                        firstName += "</member>";
                        Response.Clear();
                        Response.ContentType = "text/xml";
                        Response.Write(firstName);
                        //end the response
                        Response.End();
                    }
                  
                }
                    else if (selectedUserId.Length > 0 && Convert.ToInt32(memberID ) >= 0)
                    {
                        Response.Clear();

                        Member _member = Facade.GetMemberByMemberUserName(selectedUserId);
                        if (_member != null && _member.Id == Convert.ToInt32(memberID) && !Facade.IsMemberCandidateSourceExpired(selectedUserId))
                        {
                            string firstName = "";
                            firstName += "<member>";
                            firstName += "</member>";
                            Response.Clear();
                            Response.ContentType = "text/xml";
                            Response.Write(firstName);
                            Response.End();
                        }
                        else
                        {
                            string firstName = "";
                            firstName += "<member>";
                            if (_member != null)
                            {
                                firstName += "<firstName>";
                                firstName +=MiscUtil .RemoveScript ( _member.FirstName,string .Empty );
                                firstName += "</firstName>";
                                firstName += "</member>";
                                Response.Clear();
                                Response.ContentType = "text/xml";
                                Response.Write(firstName);
                                //end the response
                                Response.End();
                            }
                            else
                            {

                                firstName += "</member>";
                                Response.Clear();
                                Response.ContentType = "text/xml";
                                Response.Write(firstName);
                                //end the response
                                Response.End();
                            }
                        }
                    }
                else
                {
                    //clears the response written into the buffer and end the response.
                    Response.Clear();
                    Response.End();
                }
                }


            }
            else
            {
                //clears the response written into the buffer and end the response.
                Response.Clear();
                Response.End();
            }

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion
    }
}
