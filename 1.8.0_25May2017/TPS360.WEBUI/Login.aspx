<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Login.aspx
    Description: Login Page
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Dec-19-2008          Gopala Swamy J        Defect id:9320 Put one new javascript function called "HandleFailureText()" and modified the required field validator
    0.2              Jul-16-2009           Veda                 Defect id:10947 Blank white space below the 'Login In' button has been removed
    
                     oct-27-2015           pravin               change all login page format/ UI  
    
    0.3              15/May/2017           Sumit                changed allignment for Marquee #0.03
 
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>

<%@ Page Language="C#" MasterPageFile="~/Public.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="Login.aspx.cs"
    Inherits="TPS360.Web.UI.Login" Title="" %>
    
<asp:Content ID="cDefault" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <script language="javascript" type="text/javascript">
        function HandleFailureText() {
            
           var HandleFailureText = document.getElementById('FailureAlert');
            if (HandleFailureText.innerText == "Your login attempt was not successful. Please try again. ")
                HandleFailureText.innerText = "";
        }
        function showAlert() {
            
            var failureAlert = document.getElementById('FailureAlert');
            if (failureAlert.innerHTML.trim() != "") {
                failureAlert.style.visibility = "visible";
            }
        }
         function support() {
              window.open("http://talentrackr.zendesk.com")
//            window.location = 'mailto:nirbhay.ni@gmail.com?subject=TestSubject&body=TestBody';
          }

   function facebook() {
              window.open("https://www.facebook.com/talentrackrtechnologies")
 
          }
          
          
             function twitter() {
              window.open("https://twitter.com/talentrackr")
 
          }
          
          
             function linkedin() {
              window.open("https://www.linkedin.com/company/talentrackr")
 
          }



          function scrollWin() {
             iframe.scrolling 
              window.scrollBy(00, 00);
          }
          function scrollWin1() {
              alert("fsdfs"); 
          }
     
            window.onload = showAlert;
          
         
    </script>

    
    
    
    <table>
    <tr >
    <td style="width:5%">
  <%--  <div style="padding: 40px 10px 10px 20px;">--%>  <%--#0.03--%>
        <asp:Panel ID="Panel1" runat="server" >
          <asp:Login ID="LoginUser" runat="server" DisplayRememberMe="false" LoginButtonStyle-CssClass="CommonButton" OnLoggedIn="LoginUser_LoggedIn" 
                OnLoggingIn="LoginUser_LoggingIn" OnLoginError="LoginUser_Error" style="margin-top: 0px" TitleText="" Width="62%">
                <LoginButtonStyle CssClass="CommonButton" />
                <LayoutTemplate>
                    <div  style="padding: 10px 10px 10px 20px; width: 400px; background-color: #ffffff;">
                        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                        <div  style="padding: 0px 0px 0px 0px;">                             
                            <div style="text-align: left;font-size: 30px; color:  Black"  >
                                    &nbsp;&nbsp;&nbsp; Login                                    
                            </div>
                            <div  style="padding: 0px 0px 0px 0px;">
                                <div ID="LoginContainer" style="font-size: 14px; color: #333333">
                                    <div>
                                        <div style="text-align: center;">
                                        </div>
                                    </div>
                                    <div style="margin-left: auto; margin-right: auto;">
                                        <table border="0" cellpadding="0" 
                                            style="margin-left: auto; margin-right: auto;">
                                            <caption>
                                                &nbsp;
                                                <tr>
                                                    <td ID="FailureTextTd" align="center" style="color: Red;">
                                                        <div ID="FailureAlert" class="alert nomargin" style="visibility: hidden; white-space: normal;
                                                    width: 258px; padding: 8px 10px;">
                                                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                        </div>
                                                    </td>
                                               </tr>
                                            </caption>
                                            <tr>
                                                <td>
                                                    <label for="ctl00_cphHomeMaster_LoginUser_UserName" style="font-weight: bold">
                                                    Username</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-bottom: 8px;">
                                                    <asp:TextBox ID="UserName" runat="server" autocomplete="off" 
                                                        CssClass="CommonTextBox" TextMode="SingleLine" Width="325"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for="ctl00_cphHomeMaster_LoginUser_Password" style="font-weight: bold">
                                                    Password</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="Password" runat="server" autocomplete="off" 
                                                        CssClass="CommonTextBox" TextMode="Password" Width="325"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="padding-top: 10px; padding-bottom: 8px;">
                                                    <asp:Button ID="LoginButton" runat="server" CommandName="Login"
                                                       Style ="background-color:#088A85;" ForeColor="White" OnClientClick="HandleFailureText();" 
                                                        Text="Log In" ValidationGroup="LoginUser" Width="90" />
                                                        
                                                        <asp:CheckBox ID="RememberMe" runat="server" CssClass="LoginText" Visible="false" 
                                                            Text="Remember me" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                 <div style="float: left; height: 26px;">
                                                        <asp:LinkButton ID="lnkForgotPassword" runat="server" 
                                                            PostBackUrl="~/ForgotPassword.aspx" Text="Forgot Password?"></asp:LinkButton>
                                                         
                                                    </div>
                                                 
                                                 <div style="float: right;   height: 36px;" >
                                                    &nbsp;<a href="mailto:support@talentrackr.zendesk.com"><img src="Images/EmailSupport.png" title="E-Mail Support" style="width:25px;height:25px;" /></a>
                                                 </div>
                                                 
                                                 <div style="float: right; height: 36px; ">
                                                   &nbsp;<asp:ImageButton ID="ImgHelp" ToolTip="Support Desk" src="Images/Helpme.png" onclientclick="support()" style="width:25px;height:25px;" runat="server"    />
                                                   
                                                 </div>
                                                <div style="float: right; margin-right: 5px;  height: 36px;" >Support </div>
                                                 
                                               
                                                
                                                </td>
                                            </tr>
                                           
                                            
                                         </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                      <br />
                        <br />
                 </LayoutTemplate>
            </asp:Login>
       
        <br />
         </asp:Panel>
   <%-- </div>--%>
    </td>
    
    <td >
     <asp:Panel ID="Panel3" runat="server" Height="400px" Width="1px" BackColor="Black"    >
     </asp:Panel>          
             
    </td>
    
    <td style="width:100%; height:100%; ">
    
      <asp:Panel  id="NewsContainer" runat="server"  width="99%"   style="float: right; "> <%--Sumit removed height="50%" --%>
          <asp:Literal ID="Literal1" runat="server"></asp:Literal>
       <%-- <iframe id="iframe"  scrolling ="no" src="Images/News.html"  frameborder="0"   height="400px" width="99%"></iframe>--%>
 <%-- <object data="~/Images/NewsAtLogin.aspx" height="400px" width="99%">
    Alternative Content
</object>--%>
     </asp:Panel> 

  </td>
    
 </tr>
    </table>
    
    
    
    
    <br />
    <br />

    <script language="javascript" type="text/javascript">
        document.getElementById('ctl00_cphHomeMaster_LoginUser_UserName').focus();
    </script>

</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="head">

    <style type="text/css">
        .style1
        {
            width: 565px;
        }
    </style>

</asp:Content>

