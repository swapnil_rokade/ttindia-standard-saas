﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:Default.master.cs
    Description:This is the master page for displaying Links to "My Account","Logout","Home"
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date              Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Jan-13-2009          Gopala Swamy          Enhancement  id:8675; Made visibility for "My Account" and provided Navigation Path to Basic Info Page
    0.2             Feb-5-2009           Nagarathna.V.B        Defect id:9867  for Consultant Provided Navigation path to basic info page.
    0.3             Feb-26-2009          Shivanand             Defect #9354; Variable strWelcomePageName is declared as static.
                                                                             In the method Page_Load(), the logic to assign properties to hyperlink "Home" is placed outside the Postback.
    0.4             Feb-26-2009         Nagarathna.V.B         Defect id:9739 assigned path to home hyperlink button.
-------------------------------------------------------------------------------------------------------------------------------------------       
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Web.UI.HtmlControls;

namespace TPS360.Web.UI
{
    public partial class HomeMaster : BaseMasterPage
    {
        #region Variables

        ArrayList _permittedMenuIdList;
        static string strWelcomePageName; 

        #endregion

        #region Properties

        #endregion

        #region Methods

        private void InitializeInfragistics()
        {
            wsSqlCPAddToMyCalender.Connection = CreateConnection();
            this.wsSqlCPAddToMyCalender.WebScheduleInfo = this.wsInfoAddToMyCalender;
            this.wsInfoAddToMyCalender.ActiveResourceName = base.CurrentMember.Id.ToString();
            wsInfoAddToMyCalender.EnableReminders = false;
            wsInfoAddToMyCalender.AppointmentFormPath = System.Configuration .ConfigurationManager .AppSettings ["Appointment"].ToString ();
            wsInfoAddToMyCalender.ReminderFormPath = System.Configuration.ConfigurationManager.AppSettings["Reminder"].ToString();
        }

        private IDbConnection CreateConnection()
        {
            return new System.Data.SqlClient.SqlConnection(MiscUtil.ConnectionString);
        }

        private bool IsInPermitedMenuList(int menuId)
        {
            if (CurrentMember != null)
            {
                if (_permittedMenuIdList == null)
                {
                    _permittedMenuIdList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                }
            }

            if ((_permittedMenuIdList != null) && (_permittedMenuIdList.Count > 0))
            {
                if (_permittedMenuIdList.Contains(menuId))
                {
                    return true;
                }
            }

            return false;
        }

        private void AttachClientScript()
        {
            SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();

            SiteMapNode root = SiteMap.Providers["SqlSiteMap"].FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.ApplicationTopMenu));

            if (root != null)
            {
                SiteMapNodeCollection nodeList = root.ChildNodes;

                if (nodeList != null && nodeList.Count > 0)
                {
                    foreach (SiteMapNode node in nodeList)
                    {
                        if (IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
                        {
                            permittedNodeList.Add(node);
                        }
                    }
                }

                string script = "\n" +
                                "<script language=\"javascript\" type=\"text/javascript\">  " + "\n" +
                                "/* <![CDATA[ */                                            " + "\n" +
                                "                                                           " + "\n" +
                                "	GBL_NUMBER_OF_ITEMS = " + permittedNodeList.Count + "            " + "\n" +
                                "														    " + "\n" +
                                "/* ]]> */												    " + "\n" +
                                "</script>";

                if (!Page.ClientScript.IsClientScriptBlockRegistered("calc"))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "calc", script);
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            AttachClientScript();
            base.OnPreRender(e);
        }
        private void BuildCompanyLogo(int memberId)
        {
            if (memberId <= 0)
            {
                return;
            }

            CompanyContact contact = Facade.GetCompanyContactByMemberId(CurrentMember.Id);

            if (contact != null && contact.CompanyId > 0)
            {
                Company company = Facade.GetCompanyById(contact.CompanyId);

                if (!string.IsNullOrEmpty(company.CompanyLogo))
                {
                    string filePath = Path.Combine(UrlConstants.GetPhysicalCompanyUploadDirectory(), company.CompanyLogo);

                    if (File.Exists(filePath))
                    {
                        SecureUrl secureUrl = UrlHelper.BuildSecureUrl(UrlConstants.DRAW_MEDIA_PAGE, string.Empty, UrlConstants.PARAM_IMG_FILE, filePath, UIConstants.SHOW_THUMB, bool.TrueString);
                    }
                }
            }
        }

        private void LoggedinUserInfo()
        {
            if (CurrentMember != null)
            {
                if (CurrentMember.Id > 0)
                {
                    if (IsUserPartner || IsUserVendor || IsUserClient)
                    {
                        BuildCompanyLogo(CurrentMember.Id);
                    }
                }
            }
        }
       
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {            
            //if (!IsUserVendor)
            //{
            //    try
            //    {
            //        if (Facade.IsAvailableSessionId(Session.SessionID) != 1)
            //        {
            //            FormsAuthentication.SignOut();
            //            FormsAuthentication.RedirectToLoginPage();
            //            return;
            //        }
            //    }
            //    catch { }
            //}
            if (WebConfigurationManager.AppSettings["CopyRightsText"].ToString() != string.Empty)
            {
                lblCopyRights.Text = "Copyright © " + DateTime.Now.Year.ToString() + " " + WebConfigurationManager.AppSettings["CopyRightsText"].ToString() + "All rights reserved.";
            }
            if (WebConfigurationManager.AppSettings["VersionText"].ToString() != string.Empty)
            {
                lblVersionText.Text = WebConfigurationManager.AppSettings["VersionText"].ToString();
            }

            Response.Expires = -1;  
            //btnGlobalSearch.OnClientClick=  "if(checkEmpty('" + txtSearch.ClientID + "'))openGlobalSearchWindow('" + txtSearch.ClientID + "'); return false;";
            //SecureUrl todoUrl = UrlHelper.BuildSecureUrl(UrlConstants.Employee.EMPLOYEE_TASKS_SCHEDULE_WITH_MENU, "", UrlConstants.PARAM_MEMBER_ID, base.CurrentMember.Id.ToString());
            if (!IsPostBack)
            {
                if (!IsUserVendor)
                {
                    Facade.UpdateAspStateTempSessions(CurrentMember.PrimaryEmail, Session.SessionID, "TPS", GettingCommonValues.GetDomainName());
                    SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Dashboard/" + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_PAGE_FROM, "Dashboard");
                    //imgNewMail.Attributes.Add("Onclick", "btnMewMail_Click('" + url + "')");
                    lnkNewEmail.Attributes.Add("Onclick", "btnMewMail_Click('" + url + "')");
                    if (!MiscUtil.ReturnDashboard(Request.RawUrl, CurrentMember.Id, Facade))
                    {
                        Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE);
                    }
                }
                if (IsUserEmployee)
                {
                    strWelcomePageName = UrlConstants.Employee.HOME_PAGE;
                    //settingPipe.Visible = linkSettings.Visible = true;
                    linkSettings.HRef = UrlConstants.Employee.EMPLOYEE_CHANGEPASSWORD;

                    divHeaderBarRight.Visible = true;
                    //lnkTimesheet.HRef = UrlConstants.Employee.EMPLOYEE_TIMESHEETX;
                    //logoLink.HRef = UrlConstants.Employee.HOME_PAGE;
                }
                else if (IsUserCandidate)
                {
                    strWelcomePageName = UrlConstants.Candidate.HOME_PAGE;
                    //settingPipe.Visible = linkSettings.Visible = true; 
                    linkSettings.HRef = UrlConstants.Employee.EMPLOYEE_BASICINFO; 
                }
                
                else if (IsUserAdmin)
                {
                    strWelcomePageName = UrlConstants.Employee.HOME_PAGE;
                    divHeaderBarRight.Visible = true;
                    //divHeaderBarRight.Visible = true;
                    //lnkTimesheet.HRef = UrlConstants.Employee.EMPLOYEE_TIMESHEETX;
                    linkSettings.HRef = UrlConstants.Employee.EMPLOYEE_CHANGEPASSWORD;
                    //logoLink.HRef = UrlConstants.Employee.HOME_PAGE;
                }
                else if (IsUserVendor)
                {
                    A1.Attributes.Add("href", UrlConstants.Vendor.HOME);
                    strWelcomePageName = UrlConstants.Vendor.HOME;
                    divHeaderBarRight.Visible = true;
                    linkSettings.Visible = false;
                    lnkNewEmail.Visible = false;
                    lnkEmailHistory.Visible = false;

                    anchorNewAppointment.Visible = false;
                    linkSettings.Visible = false;
                    A2.Visible = true;
                    lgsLogin.LogoutPageUrl = "~/Vendor/Login.aspx";
                }
                else if (IsUserDepartmentContact) 
                {
                    strWelcomePageName = UrlConstants.Employee.HOME_PAGE;
                    divHeaderBarRight.Visible = true;
                    //lnkTimesheet.HRef = UrlConstants.Employee.EMPLOYEE_TIMESHEETX;
                    linkSettings.HRef = UrlConstants.Employee.EMPLOYEE_CHANGEPASSWORD;
                    lnkDownloadParser.Visible = lnkEmailHistory.Visible = lnkNewEmail.Visible = linkSettings.Visible = false;
                }
                //lnkTodo.HRef = todoUrl.ToString();
                LoggedinUserInfo();

                lnkDownloadParser.Visible = false;
                Hashtable siteSetting = Context.Items[ContextConstants.SITESETTING] as Hashtable;
                if (siteSetting != null)
                {
                    if (siteSetting[DefaultSiteSetting.ResumeParserDownloadLink.ToString()] != null && (siteSetting[DefaultSiteSetting.ResumeParserDownloadLink.ToString()].ToString().Trim() != string.Empty))
                    {
                        lnkDownloadParser.Attributes.Add("href", siteSetting[DefaultSiteSetting.ResumeParserDownloadLink.ToString()].ToString());
                        lnkDownloadParser.Visible = true;
                    }
                }
            }
            if (IsUserVendor) lnkDownloadParser.Visible = false;
            if (CurrentMember != null)
            {
                //string welcomeToolTip = "Dashboard: " + CurrentMember.FirstName + " " + CurrentMember.LastName;
                //string welcomeNote = "<img src='../Images/home.png' style='padding-right: 3px;' border='0' align='absmiddle' />Home";
                //lnkMemberWelcomeNote.NavigateUrl = strWelcomePageName;
                //lnkMemberWelcomeNote.Text = welcomeNote;
                //lnkMemberWelcomeNote.ToolTip = welcomeToolTip;
                lblUserName.Text = CurrentMember.FirstName + " " + CurrentMember.LastName;
            }        
            InitializeInfragistics();
        }

        protected void lgs_LogOut(object sender, EventArgs e)
        {
            string redirecturl = "";
            string role = (Roles.GetRolesForUser(base.CurrentUserName)).GetValue(0).ToString();
            MiscUtil.AddActivity(role, base.CurrentMember.Id, base.CurrentMember.Id, ActivityType.Logout, Facade);
            string dmnName = GettingCommonValues.GetDomainName();
            Facade.DeleteUserAccessApp(base.CurrentUser.UserName, "TPS", dmnName);
            MembershipUser membershipUser = Membership.GetUser(base.CurrentUser.UserName, false);
            if (membershipUser != null)
            {
                Facade.UpdateAllMemberDailyReportByLoginTimeAndMemberId(membershipUser.LastLoginDate, base.CurrentMember.Id);
            }
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Session.Abandon(); // Session Expire but cookie do exist
            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-30); //Delete the cookie
            Response.Expires = -1;
            Response.Clear();

            if (IsUserVendor) Response.Redirect("~/Vendor/Login.aspx");
            Response.Redirect("~/Login.aspx");
        }

        #endregion
    }
}