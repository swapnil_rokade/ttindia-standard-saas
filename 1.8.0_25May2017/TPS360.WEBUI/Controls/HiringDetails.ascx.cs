﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.IO;

namespace TPS360.Web.UI
{
    public partial class HiringDetails : ATSBaseControl
    {

        #region Member Variables

        #endregion

        #region Properties
        public string bulk = string.Empty;
        public string BulkAction
        {
            get
            {
                return hdnBulkAction.Value == string.Empty ? "" : hdnBulkAction.Value;
            }
            set
            {
                hdnBulkAction.Value = value.ToString();
                bulk = value;
            }
        }

        public string _MemberId = string.Empty;
        public string MemberID
        {
            get
            {
                return hfMemberId.Value == string.Empty ? "0" : hfMemberId.Value;
            }
            set
            {
                if (!IsPostBack)
                    Prepareview();
                hfMemberId.Value = value;
                _MemberId = value;
                if (hdnBulkAction.Value == string.Empty)
                    PrepareEditView();

                hdnBulkAction.Value = string.Empty;

            }
        }
        public int _statusID = 0;
        public int StatusId
        {
            get
            {
                return Convert.ToInt32(hfStatusId.Value == string.Empty ? "0" : hfStatusId.Value);
            }
            set
            {

                hfStatusId.Value = value.ToString();
                _statusID = value;
            }
        }
        public int _JobPostingId = 0;
        public int JobPostingId
        {
            get
            {

                return Convert.ToInt32(hfJobPostingId.Value == string.Empty ? "0" : hfJobPostingId.Value);
            }
            set
            {
                ClearControls();
                hfJobPostingId.Value = value.ToString();
                _JobPostingId = value;
                // Prepareview();
            }
        }
        public delegate void HiringDetailsEventHandler(string MemberId, int StatusId, bool IsAdded);
        public event HiringDetailsEventHandler HiringDetailsAdded;
        #endregion

        #region Methods

        private void Prepareview()
        {
            ddlSource.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.SourceType);
            ddlSource.DataValueField = "Id";
            ddlSource.DataTextField = "Name";
            ddlSource.DataBind();
            if (!IsUserAdmin)
            {
                ddlSource.Items.RemoveAt(3);
                ddlSource.Items.RemoveAt(3);
            }

            ddlSourceDescription.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.SourceDesciption).OrderBy(x => x.SortOrder).ToList();
            ddlSourceDescription.DataValueField = "Id";
            ddlSourceDescription.DataTextField = "Name";
            ddlSourceDescription.DataBind();

            ddlBVstatus.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.BVStatus).OrderBy(x => x.SortOrder).ToList();
            ddlBVstatus.DataValueField = "Id";
            ddlBVstatus.DataTextField = "Name";
            ddlBVstatus.DataBind();

            ddlfitmentpercentile.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.FitmentPercentile).OrderBy(x => x.SortOrder).ToList();
            ddlfitmentpercentile.DataValueField = "Id";
            ddlfitmentpercentile.DataTextField = "Name";
            ddlfitmentpercentile.DataBind();

            ddlOfferCategory.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.OfferCategory).OrderBy(x => x.SortOrder).ToList();
            ddlOfferCategory.DataValueField = "Id";
            ddlOfferCategory.DataTextField = "Name";
            ddlOfferCategory.DataBind();

            FillOfferGrade(LookupType.OfferedGradeEmbedded);

            /*ddlSalaryComponents.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.SalaryComponentsEmbedded).OrderBy(x => x.SortOrder).ToList();
            ddlSalaryComponents.DataValueField = "Id";
            ddlSalaryComponents.DataTextField = "Name";
            ddlSalaryComponents.DataBind();             */
            MiscUtil.PopulateMemberListWithEmailByRole(ddlActiveRecruiter, ContextConstants.ROLE_EMPLOYEE, Facade);
            ControlHelper.SelectListByValue(ddlActiveRecruiter, CurrentMember.Id.ToString());
            ddlActiveRecruiter.Items.RemoveAt(0);
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                //lblClientName.Text = "Department";
                //lblClientContactName.Text = "Department Contact";
            }
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() != UIConstants.STAFFING_FIRM)
            {
                ddlActiveRecruiter.Focus();
                //divStaffingFirm.Visible = false;
            }
            MiscUtil.PopulateCurrency(ddlcurrentCTC, Facade);
            MiscUtil.PopulateCurrency(ddlMaxCTC, Facade);
            GetDefaultsFromSiteSetting();

            FillVendorDetails();
            FillEmployeeReferrerList();


           

        }

        private void FillEmployeeReferrerList()
        {
                ddlEmployeeReferrer.DataSource = Facade.GetAllEmployeeReferrerList();
                ddlEmployeeReferrer.DataValueField = "Id";
                ddlEmployeeReferrer.DataTextField = "Name";
                ddlEmployeeReferrer.DataBind();
                ddlEmployeeReferrer.Items.Insert(0, new ListItem("Select Employee", "0"));
           

        }
        private void FillVendorContacts(int CompanyId)
        {
            // if (ddlVendor.SelectedIndex > 0)
            {
                ddlVendorContact.DataSource = Facade.GetAllCompanyContactsByCompanyId(CompanyId);
                ddlVendorContact.DataTextField = "FirstName";
                ddlVendorContact.DataValueField = "MemberId";
                ddlVendorContact.DataBind();
                ddlVendorContact = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlVendorContact);
                if (ddlVendorContact.Items.Count > 0)
                    ddlVendorContact.Enabled = true;
                else
                    ddlVendorContact.Enabled = false;
            }
            //else
            //   ddlVendorContact.Enabled = false;

            ddlVendorContact.Items.Insert(0, new ListItem("Please Select", "0"));
        }
        private void FillVendorDetails()
        {
            ddlVendor.DataSource = Facade.GetAllClientsByStatus((int)CompanyStatus.Vendor);
            ddlVendor.DataTextField = "CompanyName";
            ddlVendor.DataValueField = "Id";
            ddlVendor.DataBind();
            if (!String.IsNullOrEmpty(ddlVendor.SelectedValue))
                FillVendorContacts(Convert.ToInt32(ddlVendor.SelectedValue));
        }
        private void FillOfferGrade(LookupType type)
        {
            ddlOfferedGrade.DataSource = Facade.GetAllGenericLookupByLookupType(type);
            ddlOfferedGrade.DataValueField = "Id";
            ddlOfferedGrade.DataTextField = "Name";
            ddlOfferedGrade.DataBind();
            ddlOfferedGrade.Items.Insert(0, new ListItem("Please Select", "0"));
            hdnSelectedOfferGrade.Value = ddlOfferedGrade.SelectedValue;
        }
        private void PrepareEditView()
        {
            if (hfMemberId.Value.Contains(",")) return;
            MemberHiringDetails memberHiringDetails = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
            if (memberHiringDetails != null)
            {

                hfMemberHiringDetailsId.Value = memberHiringDetails.Id.ToString();

                if (memberHiringDetails.JoiningDate != DateTime.MinValue)
                {
                    txtJoiningDate.Text = memberHiringDetails.JoiningDate.ToShortDateString();
                    wdcJoiningDate.SelectedDate = memberHiringDetails.JoiningDate;
                }
                else { txtJoiningDate.Text = ""; wdcJoiningDate.SelectedDate = null; }
                if (memberHiringDetails.OfferedDate != DateTime.MinValue)
                {
                    txtDateOffered.Text = memberHiringDetails.OfferedDate.ToShortDateString();
                    wdcDateOffered.SelectedDate = memberHiringDetails.OfferedDate;
                }
                else
                {
                    txtDateOffered.Text = "";
                    wdcDateOffered.SelectedDate = null;
                }
                txtcurrentCTC.Text = memberHiringDetails.CurrentCTC;
                txtMinCTC.Text = memberHiringDetails.MinExpectedCTC;
                txtMaxCTC.Text = memberHiringDetails.MaxExpectedCTC;
                //txtGrade.Text = memberHiringDetails.Grade;

                uclExperience.Experience = memberHiringDetails.Experience;
                txtjoiningbonus.Text = memberHiringDetails.JoiningBonus;
                txtRelocationallowance.Text = memberHiringDetails.RelocationAllowance;
                txtSpecialLumpSum.Text = memberHiringDetails.SpecialLumpSum;
                txtNoticePeriodPayout.Text = memberHiringDetails.NoticePeriodPayout;
                txtPlacementFees.Text = memberHiringDetails.PlacementFees;
                ControlHelper.SelectListByValue(ddlSource, memberHiringDetails.Source.ToString());

                if (ddlSource.SelectedIndex == 3)
                {
                    ControlHelper.SelectListByValue(ddlEmployeeReferrer, memberHiringDetails.SourceDescription);
                    hdnSelectedVendorContact.Value = memberHiringDetails.SourceDescription;
                }
                else if (ddlSource.SelectedIndex == 4)
                {
                    CompanyContact con = Facade.GetCompanyContactByMemberId(Convert.ToInt32(memberHiringDetails.SourceDescription));
                    if (con != null)
                    {
                        ControlHelper.SelectListByValue(ddlVendor, con.CompanyId.ToString());
                        FillVendorContacts(con.CompanyId);
                        ControlHelper.SelectListByValue(ddlVendorContact, memberHiringDetails.SourceDescription);
                    }
                    hdnSelectedVendorContact.Value = memberHiringDetails.SourceDescription;
                }
                else if (ddlSource.SelectedItem.Text.ToString() != "Job Portals")
                {

                    txtsrcdesc.Text = memberHiringDetails.SourceDescription;
                    ddlSourceDescription.Style.Add("display", "none");
                    txtsrcdesc.Style.Add("display", "");

                }

                else
                {
                    txtsrcdesc.Style.Add("display", "none");
                    ControlHelper.SelectListByValue(ddlSourceDescription, memberHiringDetails.SourceDescription.ToString());
                    ddlSourceDescription.Style.Add("display", "");

                }

                ControlHelper.SelectListByValue(ddlfitmentpercentile, memberHiringDetails.FitmentPercentile.ToString());
                ControlHelper.SelectListByValue(ddlBVstatus, memberHiringDetails.BVStatus.ToString());
                if (ddlcurrentCTC.SelectedItem != null)
                {
                    if (ddlcurrentCTC.SelectedItem.Text == "INR") lbllak.Text = "(Lacs)";
                    else lbllak.Text = "";
                }
                if (ddlMaxCTC.SelectedItem != null)
                {
                    if (ddlMaxCTC.SelectedItem.Text == "INR") lbllacs.Text = "(Lacs)";
                    else lbllacs.Text = "";
                }
                ddlBVstatus.Style.Add(HtmlTextWriterStyle.BackgroundColor, ddlBVstatus.SelectedItem.Text == "Amber" ? "#FFC200" : ddlBVstatus.SelectedItem.Text);


                ControlHelper.SelectListByValue(ddlOfferCategory, memberHiringDetails.OfferCategory.ToString());

                ControlHelper.SelectListByValue(ddlActiveRecruiter, memberHiringDetails.ActiveRecruiterID.ToString());

               




                if (ddlOfferCategory.SelectedItem.Text == "Embedded")
                {
                    FillOfferGrade(LookupType.OfferedGradeEmbedded);
                    ControlHelper.SelectListByValue(ddlOfferedGrade, memberHiringDetails.Grade.ToString());
                    hdnSelectedOfferGrade.Value = memberHiringDetails.Grade.ToString();
                    divEmbedded.Style.Add("display", "");
                    divMech.Style.Add("display", "none");
                    txtBasicEmbedded.Text = memberHiringDetails.BasicEmbedded.ToString("#,0.###");// == "" ? "0" : memberHiringDetails.BasicEmbedded.ToString();
                    txtHRAEmbedded.Text = memberHiringDetails.HRAEmbedded.ToString("#,0.####");
                    txtConveyence.Text = memberHiringDetails.Conveyance.ToString("#,0.###");
                    txtMedical.Text = memberHiringDetails.Medical.ToString("#,0.###");
                    txtEducationalAllowanceEmbedded.Text = memberHiringDetails.EducationalAllowanceEmbedded.ToString("#,0.###");
                    txtLUFS.Text = memberHiringDetails.LUFS.ToString("#,0.###");
                    txtAdhocEmbedded.Text = memberHiringDetails.Adhoc.ToString("#,0.###");
                    txtMPP.Text = memberHiringDetails.MPP.ToString("#,0.###");
                    txtAGVI.Text = memberHiringDetails.AGVI.ToString("#,0.###");
                    txtLTAEmbedded.Text = memberHiringDetails.LTAEmbedded.ToString("#,0.###");
                    txtPFEmbedded.Text = memberHiringDetails.PFEmbedded.ToString("#,0.###");
                    txtGratuityEmbedded.Text = memberHiringDetails.GratuityEmbedded.ToString("#,0.###");
                    txtMaximunAnnualIncentive.Text = memberHiringDetails.MaximumAnnualIncentive.ToString("#,0.###");
                    txtEmbeddedAllowance.Text = memberHiringDetails.EmbeddedAllowance.ToString("#,0.###");
                    txtPLMAllowanceEmbedded.Text = memberHiringDetails.PLMAllowance.ToString("#,0.###");
                    txtSalesIncentiveEmbedded.Text = memberHiringDetails.SalesIncentive.ToString("#,0.###");
                    txtCarAllowanceEmbedded.Text = memberHiringDetails.CarAllowance.ToString("#,0.###");
                    txtGrandTotal.Text = memberHiringDetails.GrandTotal.ToString("#,0.###");

                }
                else
                {
                    FillOfferGrade(LookupType.OfferedGradeMech);
                    ControlHelper.SelectListByValue(ddlOfferedGrade, memberHiringDetails.Grade.ToString());
                    hdnSelectedOfferGrade.Value = memberHiringDetails.Grade.ToString();
                    divEmbedded.Style.Add("display", "none");
                    divMech.Style.Add("display", "");
                    txtBasic.Text = memberHiringDetails.BasicMech.ToString("#,0.###");
                    txtFlexiPay1.Text = memberHiringDetails.FlexiPay1.ToString("#,0.###");
                    txtFlexiPay2.Text = memberHiringDetails.FlexiPay2.ToString("#,0.###");
                    txtAdditionalAllowance.Text = memberHiringDetails.AdditionalAllowance.ToString("#,0.###");
                    txtAdHocAllowance.Text = memberHiringDetails.AdHocAllowance.ToString("#,0.###");
                    txtLocationAllowance.Text = memberHiringDetails.LocationAllowance.ToString("#,0.###");
                    txtSAFAllowance.Text = memberHiringDetails.SAFAllowance.ToString("#,0.###");
                    txtHRA.Text = memberHiringDetails.HRAMech.ToString("#,0.###");
                    txtHLISA.Text = memberHiringDetails.HLISA.ToString("#,0.###");
                    txtEducationAllowance.Text = memberHiringDetails.EducationAllowanceMech.ToString("#,0.###");
                    txtACLRA.Text = memberHiringDetails.ACLRA.ToString("#,0.###");
                    txtCLRA.Text = memberHiringDetails.CLRA.ToString("#,0.###");
                    txtSpecialAllowance.Text = memberHiringDetails.SpecialAllowance.ToString("#,0.###");
                    txtSpecialPerformancePay.Text = memberHiringDetails.SpecialPerformancePay.ToString("#,0.###");
                    txtECAL.Text = memberHiringDetails.ECAL.ToString("#,0.###");
                    txtCarMileageReimbursement.Text = memberHiringDetails.CarMileageReimbursement.ToString("#,0.###");
                    txtTelephoneReimbursement.Text = memberHiringDetails.TelephoneReimbursement.ToString("#,0.###");
                    txtLTA.Text = memberHiringDetails.LTAMech.ToString("#,0.###");
                    txtPF.Text = memberHiringDetails.PFMech.ToString("#,0.###");
                    txtGratuity.Text = memberHiringDetails.GratuityMech.ToString("#,0.###");
                    txtMedicalreimbursementsDomiciliary.Text = memberHiringDetails.MedicalreimbursementsDomiciliary.ToString("#,0.###");
                    txtPCScheme.Text = memberHiringDetails.PCScheme.ToString("#,0.###");
                    txtRetentionPay.Text = memberHiringDetails.RetentionPay.ToString("#,0.###");
                    txtPLRMech.Text = memberHiringDetails.PLRMech.ToString("#,0.###");
                    txtSalesIncentiveMech.Text = memberHiringDetails.SalesIncentiveMech.ToString("#,0.###");
                    txtTotalCTC.Text = memberHiringDetails.TotalCTC.ToString("#,0.###");



                }
            }
            else
            {
                ClearControls();
            }

        }
        private MemberHiringDetails BuildMemberHiringDetails()
        {
            MemberHiringDetails mem = new MemberHiringDetails();

            mem.JobPostingId = JobPostingId;
            /*mem.IsRemoved = false;*/
            //mem.MemberId = MemberID == 0 ? Convert.ToInt32(hfMemberId.Value) : MemberID;             
            DateTime value;
            DateTime.TryParse(txtJoiningDate.Text, out value);
            wdcJoiningDate.SelectedDate = value;
            if (txtJoiningDate.Text.Trim() != string.Empty && wdcJoiningDate.SelectedDate != null)
                mem.JoiningDate = Convert.ToDateTime(wdcJoiningDate.SelectedDate);// txtJoiningDate.Text.Trim());
            DateTime value1;
            DateTime.TryParse(txtDateOffered.Text, out value1);
            wdcDateOffered.SelectedDate = value1;
            if (txtDateOffered.Text.Trim() != string.Empty && wdcDateOffered.SelectedDate != null)
                mem.OfferedDate = Convert.ToDateTime(wdcDateOffered.SelectedDate);//

            mem.CurrentCTC = txtcurrentCTC.Text;
            mem.MinExpectedCTC = txtMinCTC.Text;
            mem.MaxExpectedCTC = txtMaxCTC.Text;
            // mem.Grade = txtGrade.Text;
            mem.Experience = uclExperience.Experience;
            mem.Source = Convert.ToInt32(ddlSource.SelectedValue);
            mem.SourceDescription = ddlSourceDescription.SelectedValue;
            if (ddlSource.SelectedIndex == 3)
            {
                mem.SourceDescription = ddlEmployeeReferrer.SelectedValue;
            }
            else if (ddlSource.SelectedIndex == 4)
            {
                mem.SourceDescription = hdnSelectedVendorContact.Value;
            }
            else if (ddlSource.SelectedItem.Text.ToString() != "Job Portals")
            {
                mem.SourceDescription = txtsrcdesc.Text;

            }
            mem.FitmentPercentile = Convert.ToInt32(ddlfitmentpercentile.SelectedValue);
            mem.JoiningBonus = txtjoiningbonus.Text;
            mem.RelocationAllowance = txtRelocationallowance.Text;
            mem.SpecialLumpSum = txtSpecialLumpSum.Text;
            mem.NoticePeriodPayout = txtNoticePeriodPayout.Text;
            mem.PlacementFees = txtPlacementFees.Text;
            mem.BVStatus = Convert.ToInt32(ddlBVstatus.SelectedValue);
            mem.CreatorId = base.CurrentMember.Id;
            mem.UpdatorId = base.CurrentMember.Id;

            //For basic and embedded
            mem.OfferCategory = Convert.ToInt32(ddlOfferCategory.SelectedValue);
            mem.Grade = hdnSelectedOfferGrade.Value;
            if (ddlOfferCategory.SelectedItem.Text == "Embedded")
            {
                mem.BasicEmbedded = Convert.ToDecimal(txtBasicEmbedded.Text == "" ? "0" : txtBasicEmbedded.Text);
                mem.HRAEmbedded = Convert.ToDecimal(txtHRAEmbedded.Text == "" ? "0" : txtHRAEmbedded.Text);
                mem.Conveyance = Convert.ToDecimal(txtConveyence.Text == "" ? "0" : txtConveyence.Text);
                mem.Medical = Convert.ToDecimal(txtMedical.Text == "" ? "0" : txtMedical.Text);
                mem.EducationalAllowanceEmbedded = Convert.ToDecimal(txtEducationalAllowanceEmbedded.Text == "" ? "0" : txtEducationalAllowanceEmbedded.Text);
                mem.LUFS = Convert.ToDecimal(txtLUFS.Text == "" ? "0" : txtLUFS.Text);
                mem.Adhoc = Convert.ToDecimal(txtAdhocEmbedded.Text == "" ? "0" : txtAdhocEmbedded.Text);
                mem.MPP = Convert.ToDecimal(txtMPP.Text == "" ? "0" : txtMPP.Text);
                mem.AGVI = Convert.ToDecimal(txtAGVI.Text == "" ? "0" : txtAGVI.Text);
                mem.LTAEmbedded = Convert.ToDecimal(txtLTAEmbedded.Text == "" ? "0" : txtLTAEmbedded.Text);
                mem.PFEmbedded = Convert.ToDecimal(txtPFEmbedded.Text == "" ? "0" : txtPFEmbedded.Text);
                mem.GratuityEmbedded = Convert.ToDecimal(txtGratuityEmbedded.Text == "" ? "0" : txtGratuityEmbedded.Text);
                mem.MaximumAnnualIncentive = Convert.ToDecimal(txtMaximunAnnualIncentive.Text == "" ? "0" : txtMaximunAnnualIncentive.Text);
                mem.EmbeddedAllowance = Convert.ToDecimal(txtEmbeddedAllowance.Text == "" ? "0" : txtEmbeddedAllowance.Text);
                mem.PLMAllowance = Convert.ToDecimal(txtPLMAllowanceEmbedded.Text == "" ? "0" : txtPLMAllowanceEmbedded.Text);
                mem.SalesIncentive = Convert.ToDecimal(txtSalesIncentiveEmbedded.Text == "" ? "0" : txtSalesIncentiveEmbedded.Text);
                mem.CarAllowance = Convert.ToDecimal(txtCarAllowanceEmbedded.Text == "" ? "0" : txtCarAllowanceEmbedded.Text);
                mem.GrandTotal = getSalarySum("Embedded", mem);
            }
            else
            {
                mem.BasicMech = Convert.ToDecimal(txtBasic.Text == "" ? "0" : txtBasic.Text);
                mem.FlexiPay1 = Convert.ToDecimal(txtFlexiPay1.Text == "" ? "0" : txtFlexiPay1.Text);
                mem.FlexiPay2 = Convert.ToDecimal(txtFlexiPay2.Text == "" ? "0" : txtFlexiPay2.Text);
                mem.AdditionalAllowance = Convert.ToDecimal(txtAdditionalAllowance.Text == "" ? "0" : txtAdditionalAllowance.Text);
                mem.AdHocAllowance = Convert.ToDecimal(txtAdHocAllowance.Text == "" ? "0" : txtAdHocAllowance.Text);
                mem.LocationAllowance = Convert.ToDecimal(txtLocationAllowance.Text == "" ? "0" : txtLocationAllowance.Text);
                mem.SAFAllowance = Convert.ToDecimal(txtSAFAllowance.Text == "" ? "0" : txtSAFAllowance.Text);
                mem.HRAMech = Convert.ToDecimal(txtHRA.Text == "" ? "0" : txtHRA.Text);
                mem.HLISA = Convert.ToDecimal(txtHLISA.Text == "" ? "0" : txtHLISA.Text);
                mem.EducationAllowanceMech = Convert.ToDecimal(txtEducationAllowance.Text == "" ? "0" : txtEducationAllowance.Text);
                mem.ACLRA = Convert.ToDecimal(txtACLRA.Text == "" ? "0" : txtACLRA.Text);
                mem.CLRA = Convert.ToDecimal(txtCLRA.Text == "" ? "0" : txtCLRA.Text);
                mem.SpecialAllowance = Convert.ToDecimal(txtSpecialAllowance.Text == "" ? "0" : txtSpecialAllowance.Text);
                mem.SpecialPerformancePay = Convert.ToDecimal(txtSpecialPerformancePay.Text == "" ? "0" : txtSpecialPerformancePay.Text);
                mem.ECAL = Convert.ToDecimal(txtECAL.Text == "" ? "0" : txtECAL.Text);
                mem.CarMileageReimbursement = Convert.ToDecimal(txtCarMileageReimbursement.Text == "" ? "0" : txtCarMileageReimbursement.Text);
                mem.TelephoneReimbursement = Convert.ToDecimal(txtTelephoneReimbursement.Text == "" ? "0" : txtTelephoneReimbursement.Text);
                mem.LTAMech = Convert.ToDecimal(txtLTA.Text == "" ? "0" : txtLTA.Text);
                mem.PFMech = Convert.ToDecimal(txtPF.Text == "" ? "0" : txtPF.Text);
                mem.GratuityMech = Convert.ToDecimal(txtGratuity.Text == "" ? "0" : txtGratuity.Text);
                mem.MedicalreimbursementsDomiciliary = Convert.ToDecimal(txtMedicalreimbursementsDomiciliary.Text == "" ? "0" : txtMedicalreimbursementsDomiciliary.Text);
                mem.PCScheme = Convert.ToDecimal(txtPCScheme.Text == "" ? "0" : txtPCScheme.Text);
                mem.RetentionPay = Convert.ToDecimal(txtRetentionPay.Text == "" ? "0" : txtRetentionPay.Text);
                mem.PLRMech = Convert.ToDecimal(txtPLRMech.Text == "" ? "0" : txtPLRMech.Text);
                mem.SalesIncentiveMech = Convert.ToDecimal(txtSalesIncentiveMech.Text == "" ? "0" : txtSalesIncentiveMech.Text);
                mem.TotalCTC = getSalarySum("Mech", mem);

            }
            mem.ActiveRecruiterID = Convert.ToInt32(ddlActiveRecruiter.SelectedValue);
            return mem;


        }

        private decimal getSalarySum(string type, MemberHiringDetails mem)
        {
            if (type == "Embedded")
            {
                return mem.BasicEmbedded + mem.HRAEmbedded +
                  mem.Conveyance
                  + mem.Medical
                  + mem.EducationalAllowanceEmbedded
                  + mem.LUFS
                  + mem.Adhoc
                  + mem.MPP
                  + mem.AGVI
                  + mem.LTAEmbedded
                  + mem.PFEmbedded
                  + mem.GratuityEmbedded
                  + mem.MaximumAnnualIncentive
                  + mem.EmbeddedAllowance
                  + mem.PLMAllowance
                  + mem.SalesIncentive
                  + mem.CarAllowance;
            }
            else
            {
                return
                      mem.BasicMech
               + mem.FlexiPay1
               + mem.FlexiPay2
               + mem.AdditionalAllowance
               + mem.AdHocAllowance
               + mem.LocationAllowance
               + mem.SAFAllowance
               + mem.HRAMech
               + mem.HLISA
               + mem.EducationAllowanceMech
               + mem.ACLRA
               + mem.CLRA
               + mem.SpecialAllowance
               + mem.SpecialPerformancePay
               + mem.ECAL
               + mem.CarMileageReimbursement
               + mem.TelephoneReimbursement
               + mem.LTAMech
               + mem.PFMech
               + mem.GratuityMech
               + mem.MedicalreimbursementsDomiciliary
               + mem.PCScheme
               + mem.RetentionPay
                + mem.PLRMech
                + mem.SalesIncentiveMech;
            }


        }
        private void GetDefaultsFromSiteSetting()
        {
            if (SiteSetting != null)
            {
                ddlcurrentCTC.SelectedValue = ddlcurrentCTC.SelectedValue = SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString();
                ddlMaxCTC.SelectedValue = ddlMaxCTC.SelectedValue = SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString();
            }
        }


        public void ClearControls()
        {
            try
            {
                ControlHelper.SelectListByValue(ddlBVstatus, "0");
                ControlHelper.SelectListByValue(ddlSource, "0");
                ControlHelper.SelectListByValue(ddlSourceDescription, "0");
                ControlHelper.SelectListByValue(ddlfitmentpercentile, "0");

                GetDefaultsFromSiteSetting();

                wdcJoiningDate.SelectedDate = null;
                txtJoiningDate.Text = "";
                wdcDateOffered.SelectedDate = null;
                txtDateOffered.Text = "";

                txtJoiningDate.Text = txtDateOffered.Text = DateTime.Now.ToShortDateString();
                wdcDateOffered.SelectedDate = wdcJoiningDate.SelectedDate = DateTime.Now;

                txtcurrentCTC.Text = "0";
                txtMinCTC.Text = "0";
                txtMaxCTC.Text = "0";
                // txtGrade.Text = "";
                // txtExperience.Text = "";
                txtsrcdesc.Text = "";

                txtjoiningbonus.Text = "0";
                txtRelocationallowance.Text = "0";
                txtSpecialLumpSum.Text = "0";
                txtNoticePeriodPayout.Text = "0";
                txtPlacementFees.Text = "0";

                if (ddlcurrentCTC.SelectedItem != null)
                {
                    if (ddlcurrentCTC.SelectedItem.Text == "INR") lbllak.Text = "(Lacs)";
                    else lbllak.Text = "";
                }
                if (ddlMaxCTC.SelectedItem != null)
                {
                    if (ddlMaxCTC.SelectedItem.Text == "INR") lbllacs.Text = "(Lacs)";
                    else lbllacs.Text = "";
                }
            }
            catch
            {
            }

        }
        #endregion

        #region PageEvents
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlVendor.Attributes.Add("onChange", "return Company_OnChange('" + ddlVendor.ClientID + "','" + ddlVendorContact.ClientID + "','" + hdnSelectedVendorContact.ClientID + "','MemberID')");
            ddlVendorContact.Attributes.Add("OnChange", "return Contact_OnChange('" + ddlVendorContact.ClientID + "','" + hdnSelectedVendorContact.ClientID + "')");
            ddlOfferCategory.Attributes.Add("onchange", "javascript:OfferCategoryOnChange('" + ddlOfferCategory.ClientID + "','" + ddlOfferedGrade.ClientID + "','" + hdnSelectedOfferGrade.ClientID + "','" + divEmbedded.ClientID + "','" + divMech.ClientID + "');");
            ddlOfferedGrade.Attributes.Add("onchange", "javascript:OfferGradeOnChange('" + ddlOfferedGrade.ClientID + "','" + hdnSelectedOfferGrade.ClientID + "');");
            wdcJoiningDate.Format = wdcDateOffered.Format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern; //"dd/MM/yyyy";           
            ddlcurrentCTC.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('" + ddlcurrentCTC.ClientID + "','" + lbllak.ClientID + "')");
            ddlMaxCTC.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('" + ddlMaxCTC.ClientID + "','" + lbllacs.ClientID + "')");

            HiddenField hdncheck = new HiddenField();
            try
            {
                hdncheck = (HiddenField)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uwtRequisitionHiringMatrixNavigationTopMenu").FindControl("uclRequisitionHiringMatrix").FindControl("hdnmodal");
            }
            catch
            {
            }
            if (!IsPostBack)
            {

                Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
                if (lbModalTitle != null)
                {
                    int memID = 0;
                    Int32.TryParse(MemberID, out memID);

                    if (memID > 0)
                    {
                        lbModalTitle.Text = Facade.GetMemberNameById(memID) + " - Offered Details";
                    }
                    else lbModalTitle.Text = "Offered Details";
                }

                txtJoiningDate.Text = txtDateOffered.Text = DateTime.Now.ToShortDateString();

                hdncheck.Value = "";
            }
            else
                hdncheck.Value = "s";


            txtDateOffered.Focus();

           




        }

        #endregion


        #region Button Events
        protected void btnSave_Click(object sender, EventArgs e)
        {
            MemberHiringDetails memberHiringDetails = BuildMemberHiringDetails();
            Facade.AddMemberHiringDetails(memberHiringDetails, hfMemberId.Value);
            if (StatusId > 0)
            {
                Facade.MemberJobCart_MoveToNextLevel(0, base.CurrentMember.Id, hfMemberId.Value, JobPostingId, StatusId.ToString());
                MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, JobPostingId, hfMemberId.Value, CurrentMember.Id, "Offered", Facade);
                Facade.UpdateCandidateRequisitionStatus(JobPostingId, hfMemberId.Value, base.CurrentMember.Id, StatusId);
            }
            if (HiringDetailsAdded != null) HiringDetailsAdded(hfMemberId.Value, Convert.ToInt32(hfStatusId.Value != string.Empty ? hfStatusId.Value : "0"), true);
            ClearControls();
            ScriptManager.RegisterClientScriptBlock(lbllacs, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Offer Details Saved Successfully');", true);
        }

        #endregion
    }
}