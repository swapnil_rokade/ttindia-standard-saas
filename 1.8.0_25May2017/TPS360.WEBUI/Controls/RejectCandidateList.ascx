﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RejectCandidateList.ascx.cs"
    Inherits="TPS360.Web.UI.RejectCandidateList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RejectDetails.ascx" TagName="Reject" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/CandidateActionLog.ascx" TagName="HiringLog" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ModalTemplate.ascx" TagName="Template" TagPrefix="ucl" %>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script type="text/javascript" language="javascript" src="../Scripts/Common.js"></script>

<script language="javascript" type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
    function PageLoadedReq(sender, args) {
        try {
            var chkBox = document.getElementById('chkAllItem');
            if (chkBox != null) chkBox.disabled = false;
            CheckUnCheckGridViewHeaderCheckbox('tlbTemplate1', 'chkAllItem', 0, 1);
        }
        catch (e) {
        }
    }
    function CLCheckUnCheckAllRowLevelCheckboxes(cntrlHeaderCheckbox, posCheckbox) 
    {
    
       
    
    
    }
    function  HeaderCheckBoxClicked(chkAllItem)
    {
         $("table#tlbTemplate1 tbody tr").each(function(){
            $(this).find(":checkbox").attr('checked',chkAllItem .checked);
        });

    }
    function ShowOrHideContexts(divContext, imgContextMenu) {
        n = 10;
        var alldivs = document.getElementById('ctl00_cphHomeMaster_hdnDivContextMenuids').value.split(';');
        for (var m = 0; m < alldivs.length; m++) {
            var cm = document.getElementById(alldivs[m]);
            if (cm != null) cm.style.display = "none";
        }
        document.getElementById(imgContextMenu).style.visibility = "hidden";

        var divContext = document.getElementById(divContext);

        divContext.style.display = "";

    }
    function onLeaveContextMenus(control) {//HideContextMenu(control );

        n = 0;
        t = setTimeout('HideContextMenus ("' + control + '");', 1000);

    }
    function HideContextMenus(control) {
        var v = document.getElementById(control);
        if (n == 0) v.style.display = "none";

    }
    function ShowContextMenus(control) {
        n = 10;
        document.getElementById(control).style.display = "";
    }

    function ShowOrHideContextMenus(control, operation, divId) {
        var con = document.getElementById(control);
        if (document.getElementById(divId).style.display == "none") {
            con.style.visibility = operation;
        }
    }


 function SelectRowCheckboxRejected(param1, param2) {
 
 
  
        // debugger;
        var ischeckall = true;
        var HeaderCheckbox = document.getElementById('chkAllItem');
        var elements = HeaderCheckbox.form.elements;

        for (var i = 0; i < elements.length; i++) {
            if ((elements[i].type == 'checkbox') && (elements[i].id != HeaderCheckbox.id)) {
                if (elements[i].checked == true) {
                    var hndId = elements[i + 1];
                   
                    if (IsNumeric(hndId.value)) {
                        ADDID(hndId.value, '<%= hdnSelectedIDS.ClientID %>');
                    }

                }
                else {
                    var hndId = elements[i + 1];
                    if (IsNumeric(hndId.value)) {
                        RemoveID(hndId.value, '<%= hdnSelectedIDS.ClientID %>');
                    }
                }
            }
        }
        HeaderCheckbox.checked = ischeckall;
    }

</script>

<div id="dvrdetails" runat="server" visible="false" style="padding: 5px;">
    <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
    <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
    <asp:ObjectDataSource ID="odsRejectCandidateList" runat="server" SelectMethod="GetPaged"
        TypeName="TPS360.Web.UI.RejectCandidateDataSource" SelectCountMethod="GetListCount"
        EnablePaging="True" SortParameterName="sortExpression">
        <SelectParameters>
            <asp:Parameter Name="JobPostingId" DefaultValue="0" />
            <asp:Parameter Name="SortOrder" DefaultValue="asc" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:UpdatePanel ID="updPanelRejectCandidateList" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnSelectedIDS" runat="server" />
            <asp:HiddenField ID="hdnDivContextMenuids" runat="server" />
            <asp:HiddenField ID="hdnDetailRowIds" runat="server" />
            <div class="TableRow" style="text-align: left;">
                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
            </div>
            <div class="content" style="vertical-align: top; position: relative;">
                <div class="pull-left" style="margin-bottom:3px">
                    
                            <asp:LinkButton ID="btnUndoRejection" runat="server" Text="Undo Rejection" OnClick="btnUndoRejection_Click" CssClass="btn"></asp:LinkButton>
                    
                    <%--<asp:Button ID ="btnUndoRejection" runat ="server" CssClass ="CommonButton" Text ="Undo Rejection" 
                    OnClick="btnUndoRejection_Click"/>--%>
                </div>
                <div class="GridContainer">
                    <asp:ListView ID="lsvCandidateList" runat="server" DataKeyNames="Id" OnItemDataBound="lsvRejectCandidateList_ItemDataBound"
                        EnableViewState="true" OnItemCommand="lsvRejectCandidateList_ItemCommand" OnPreRender="lsvRejectCandidateList_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate1" class="Grid" cellspacing="0" border="0">
                                <tr runat="server" id="trPrecise">
                                    <th style="width: 20px !important;  ">
                                        <input id="chkAllItem" name="chkAllItem" type="checkbox" onclick="HeaderCheckBoxClicked(this)" />
                                    </th>
                                    <th style="white-space: nowrap;">
                                        <asp:LinkButton ID="btnName" runat="server" ToolTip="Sort By Name" CommandName="Sort"
                                            CommandArgument="[M].[FirstName]" Text="Name" />
                                    </th>
                                    <th style="white-space: nowrap;">
                                        <asp:Label ID="btnMobilePhone" runat="server" Text="Mobile" />
                                    </th>
                                    <th style="white-space: nowrap;display:none">
                                        <asp:LinkButton ID="btnEmail" runat="server" ToolTip="Sort By Email" CommandName="Sort"
                                            CommandArgument="[M].[PrimaryEmail]" Text="Email" />
                                    </th>
                                    <th style="white-space: nowrap;">
                                        <asp:LinkButton ID="btnRejectedON" runat="server" ToolTip="Sort By Rejected On" CommandName="Sort"
                                            CommandArgument="[RC].[CreateDate]" Text="Rejected On" />
                                    </th>
                                    <th style="white-space: nowrap;">
                                        <asp:LinkButton ID="btnRejectedBy" runat="server" ToolTip="Sort By Rejected By" CommandName="Sort"
                                            CommandArgument="[MM].[FirstName]" Text="Rejected By" />
                                    </th>
                                    <th style="white-space: nowrap;">
                                        <asp:Label ID="lblRejectReason" runat="server" Text="Reason For Rejection" />
                                    </th>
                                    <th style="width: 45px !important">
                                                                            Action
                                                                        </th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td colspan="7" id="tdPager" runat="server">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No candidates in Rejected status level.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="rows" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td  style="width: 20px;">
                                    <asp:CheckBox runat="server" ID="chkItemCandidate" OnClick="SelectRowCheckboxRejected()" />
                                    <asp:HiddenField ID="hdnID" runat="server" />
                                    <asp:HiddenField ID="hfHiringId" runat="server" />
                                    <asp:HiddenField ID="hfCandidateId"  runat="server" />
                                </td>
                                <td>
                                        <asp:HyperLink ID="lnkCandidateName"  runat="server"></asp:HyperLink>
                                </td>
                                <td >
                                    <asp:Label ID="lblMobilePhone" runat="server" />
                                </td>
                                <td style="display:none">
                                    <asp:LinkButton ID="lblEmail" runat="server" />
                                </td>
                                <td >
                                    <asp:Label ID="lblRejectedOn" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblRejectedBy" runat="server"  />
                                </td>
                                <td style="min-width: 120px;">
                                    <asp:Label ID="lblReasonforRejection" runat="server" />
                                </td>
                                 <td style="overflow: inherit;">
                                     <ul class="nav" style="margin-bottom: 0px; padding-left: 0px;">
                                         <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle" href="#" style =" width : 40px; margin-top : -15px;">
                                             <div class="container">
                                                 <button class="btn btn-mini" type="button">
                                                     <i class="icon icon-cog"></i><span class="caret"></span>
                                                 </button>
                                             </div>
                                         </a>
                                             <ul class="dropdown-menu" style="margin-left: -115px; margin-top: -25px;">
                                                 <li>
                                                     <asp:LinkButton ID="lnkUndoRejectCandidate" runat="server" Text="Undo Rejection"
                                                         CommandName="UndoRejection" ></asp:LinkButton></li>
                                                 <li >
                                                     <asp:LinkButton ID="lnkEditRejectCandidate" runat="server" Text="Edit Rejection"
                                                         CommandName="EditRejection" ></asp:LinkButton></li>
                                                 <li>
                                                     <asp:LinkButton ID="lnkHiringLog" runat="server" Text="Hiring Log" CommandName="HiringLog"></asp:LinkButton></li>
                                             </ul>
                                         </li>
                                     </ul>
                                                                    </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
