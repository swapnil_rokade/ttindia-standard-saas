﻿/* 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RejectDetails.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               24/June/2016        pravin khot          added- Facade.RejectMemberJobCartById(jobCart.Id);//NEW CODE ADDED
 *  0.2               27/July/2016        pravin khot         commented - Facade.DeleteInterviewById(interview.Id);

 * -------------------------------------------------------------------------------------------------------------------------------------------       
*/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.IO;
namespace TPS360.Web.UI
{
    public partial class RejectDetails : ATSBaseControl
    {
        #region Variables
        RejectCandidate candidate = new RejectCandidate();
        #endregion

        #region Properties

        private bool _isEdit = false;
        public bool IsEdit
        {
            get
            {
                return hdnIsEdit.Value == "" ? false : Convert.ToBoolean(hdnIsEdit.Value);
            }
            set
            {
                _isEdit = value;
                hdnIsEdit.Value = value.ToString();
            }
        }

        public int _StatusId = 0;
        public int StatusId
        {
            get
            {
                return Convert.ToInt32(hfStatusId.Value == string.Empty ? "0" : hfStatusId.Value);
            }
            set
            {
                hfStatusId.Value = value.ToString();
                _StatusId = value;
            }
        }
        private int _jobPostingId = 0;
        public int JobPostingId
        {
            get { return Convert.ToInt32(hfJobPostingId.Value == "" ? "0" : hfJobPostingId.Value); }
            set { _jobPostingId = value; hfJobPostingId.Value = value.ToString(); }
        }
        public string _MemberId = string.Empty;
        public string MemberID
        {
            get
            {
                return _MemberId;
            }
            set
            {
                IsEdit = false;
                _MemberId = value;
            }
        }
        public int _HiringMatrixLevel = 0;
        public int HiringMatrixLevel
        {
            get
            {
                return _HiringMatrixLevel;
            }
            set
            {
                _HiringMatrixLevel = value;
            }
        }

        public bool isMainApplication
        {
            get
            {
                switch (ApplicationSource)
                {
                    case ApplicationSource.LandTApplication:
                        return false;
                        break;
                    case ApplicationSource.MainApplication:
                        return true;
                        break;
                    default:
                        return true;
                        break;
                }
                return true;
            }
        }
        public delegate void RejectCandidateEventHandler(int StatusId, string MemberId);
        public event RejectCandidateEventHandler RejectDetailsAdded;

        #endregion

        #region Method
        public string getModalTitle()
        {
            try
            {
                return "Reject " + MiscUtil.GetMemberNameById(Convert.ToInt32(_MemberId), Facade);
            }
            catch
            {
                return "Reject";
            }
        }
        private RejectCandidate BuildRejectDetails()
        {
            //candidate  = CurrentRejectDetails;
            try
            {
                candidate.JobPostingID = JobPostingId;
                candidate.MemberId = 0;//hfMemberId.Value != string.Empty ? Int32.Parse(hfMemberId.Value) : _MemberId;
                candidate.RejectDetails = MiscUtil.RemoveScript(txtReject.Text.Trim().Replace("'", "''"));
                candidate.UpdatorId = CurrentMember.Id;
                candidate.HiringMatrixLevel = hfHiringMatrixLevel.Value != string.Empty ? Int32.Parse(hfHiringMatrixLevel.Value) : _HiringMatrixLevel;
            }
            catch
            {
            }
            return candidate;
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
                if (lbModalTitle != null) lbModalTitle.Text = getModalTitle();
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {

            if (_MemberId != string.Empty)
            {
                hfMemberId.Value = _MemberId.ToString();
                hfHiringMatrixLevel.Value = _HiringMatrixLevel.ToString();
                if (hfMemberId.Value != string.Empty)
                {
                    if (!hfMemberId.Value.Contains(","))
                    {
                        candidate = Facade.GetRejectCandidateByMemberIdAndJobPostingID(Int32.Parse(hfMemberId.Value), JobPostingId);
                        if (candidate != null)
                            txtReject.Text = candidate.RejectDetails;
                        else
                            txtReject.Text = string.Empty;
                    }
                    else
                        txtReject.Text = string.Empty;
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (IsEdit)
            {
                RejectCandidate cand = new RejectCandidate();
                cand = Facade.GetRejectCandidateByMemberIdAndJobPostingID(Int32.Parse(hfMemberId.Value), JobPostingId);
                cand.RejectDetails = MiscUtil.RemoveScript(txtReject.Text.Trim().Replace("'", "''"));
                cand.CreatorId = CurrentMember.Id;
                Facade.AddRejectCandidate(cand, hfMemberId.Value);
                MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateRejected, JobPostingId, hfMemberId.Value, CurrentMember.Id, Facade);
                if (RejectDetailsAdded != null)
                    RejectDetailsAdded(Convert.ToInt32(hfStatusId.Value != string.Empty ? hfStatusId.Value : "0"), hfMemberId.Value);
                ScriptManager.RegisterClientScriptBlock(txtReject, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Rejection details updated successfully.');", true);
            }
            else
            {
                //  if ((hfHiringMatrixLevel.Value != string.Empty ? Int32.Parse(hfHiringMatrixLevel.Value) : _HiringMatrixLevel) > 0)
                {
                    if (!hfMemberId.Value.Contains(","))
                    {
                        RejectCandidate cand = Facade.GetRejectCandidateByMemberIdAndJobPostingID(Int32.Parse(hfMemberId.Value), JobPostingId);
                        if (cand == null)
                        {
                            cand = BuildRejectDetails();
                            cand.CreatorId = CurrentMember.Id;
                            Facade.AddRejectCandidate(cand, hfMemberId.Value);

                            //  Facade.MemberJobCart_MoveToNextLevel((hfHiringMatrixLevel.Value != string.Empty ? Int32.Parse(hfHiringMatrixLevel.Value) : _HiringMatrixLevel), base.CurrentMember.Id, hfMemberId.Value, JobPostingId, "Remove");
                            MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateRejected, JobPostingId, hfMemberId.Value, CurrentMember.Id, Facade);

                            MemberJobCart jobCart = Facade.GetMemberJobCartByMemberIdAndJobPostringId(Convert.ToInt32(hfMemberId.Value), JobPostingId);
                            MemberHiringDetails hiringDetail = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
                            MemberJoiningDetail joiningDetail = Facade.GetMemberJoiningDetailsByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
                           //Code commented by pravin khot 27/July/2016******
                            //IList<Interview> interviews = Facade.GetInterviewByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
                            if (jobCart != null)
                            {
                                //code modify by pravin khot on 24/June/2016**********
                                //Facade.DeleteMemberJobCartById(jobCart.Id);//OLD CODE
                                Facade.RejectMemberJobCartById(jobCart.Id);//NEW CODE ADDED
                                //***********END******************************
                            }
                            if (hiringDetail != null)
                            {
                                Facade.DeleteMemberHiringDetailsByID(hiringDetail.Id);
                            }
                            if (joiningDetail != null)
                            {
                                Facade.DeleteMemberJoiningDetailsByID(joiningDetail.Id);
                            }
                            //*********Code commented by pravin khot 27/July/2016******
                            //if (interviews != null)
                            //{
                            //    foreach (Interview interview in interviews)
                            //    {
                            //        Facade.DeleteInterviewById(interview.Id);
                            //    }
                            //}
                            //**************END*************************

                            //EventLogForRequisitionAndCandidate log = BuildEventLog(Convert.ToInt32(id), jobPostingid);
                            MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.RemoveCandidateFromHiringMatrix, JobPostingId, hfMemberId.Value, CurrentMember.Id, "", Facade);
                            //Facade.AddEventLog(log, id);

                            ScriptManager.RegisterClientScriptBlock(txtReject, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Successfully rejected selected candidate(s).');", true);

                        }
                        else
                        {
                            cand.UpdatorId = CurrentMember.Id;
                            // cand.HiringMatrixLevel = hfHiringMatrixLevel.Value != string.Empty ? Int32.Parse(hfHiringMatrixLevel.Value) : _HiringMatrixLevel;
                            cand.RejectDetails = MiscUtil.RemoveScript(txtReject.Text.Trim());
                            Facade.UpdateRejectCandidate(cand);

                            //added for remove the can from memberjobcart at second time of added in the same requisition
                            MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateRejected, JobPostingId, hfMemberId.Value, CurrentMember.Id, Facade);

                            MemberJobCart jobCart = Facade.GetMemberJobCartByMemberIdAndJobPostringId(Convert.ToInt32(hfMemberId.Value), JobPostingId);
                            MemberHiringDetails hiringDetail = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
                            MemberJoiningDetail joiningDetail = Facade.GetMemberJoiningDetailsByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
                            //Code commented by pravin khot 27/July/2016******
                            //IList<Interview> interviews = Facade.GetInterviewByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
                            if (jobCart != null)
                            {
                                //code modify by pravin khot on 24/June/2016**********
                                //Facade.DeleteMemberJobCartById(jobCart.Id);//OLD CODE
                                Facade.RejectMemberJobCartById(jobCart.Id);//NEW CODE ADDED
                                //***********END******************************
                            }
                            if (hiringDetail != null)
                            {
                                Facade.DeleteMemberHiringDetailsByID(hiringDetail.Id);
                            }
                            if (joiningDetail != null)
                            {
                                Facade.DeleteMemberJoiningDetailsByID(joiningDetail.Id);
                            }
                            //*********Code commented by pravin khot 27/July/2016******
                            //if (interviews != null)
                            //{
                            //    foreach (Interview interview in interviews)
                            //    {
                            //        Facade.DeleteInterviewById(interview.Id);
                            //    }
                            //}
                            //**************END*************************

                           //
                            ScriptManager.RegisterClientScriptBlock(txtReject, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Successfully updated Rejection Details.');", true);
                        }
                        if (StatusId > 0)
                        {
                            Facade.MemberJobCart_MoveToNextLevel((hfHiringMatrixLevel.Value != string.Empty ? Int32.Parse(hfHiringMatrixLevel.Value) : _HiringMatrixLevel), base.CurrentMember.Id, hfMemberId.Value, JobPostingId, StatusId.ToString());
                            Facade.UpdateCandidateRequisitionStatus(JobPostingId, hfMemberId.Value, CurrentMember.Id, StatusId);
                        }
                    }
                    else
                    {
                        RejectCandidate cand = new RejectCandidate();
                        cand = BuildRejectDetails();
                        cand.CreatorId = CurrentMember.Id;
                        Facade.AddRejectCandidate(cand, hfMemberId.Value);
                        Facade.MemberJobCart_MoveToNextLevel((hfHiringMatrixLevel.Value != string.Empty ? Int32.Parse(hfHiringMatrixLevel.Value) : _HiringMatrixLevel), base.CurrentMember.Id, hfMemberId.Value, JobPostingId, StatusId.ToString());
                        MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateRejected, JobPostingId, hfMemberId.Value, CurrentMember.Id, Facade);
                        Facade.UpdateCandidateRequisitionStatus(JobPostingId, hfMemberId.Value, CurrentMember.Id, StatusId);

                        string[] Ids = hfMemberId.Value.TrimEnd(',').Split(new string[] { "," }, StringSplitOptions.None);
                        int jobPostingid = JobPostingId;
                        foreach (string id in Ids)
                        {
                            MemberJobCart jobCart = Facade.GetMemberJobCartByMemberIdAndJobPostringId(Convert.ToInt32(id), jobPostingid);
                            MemberHiringDetails hiringDetail = Facade.GetMemberHiringDetailsByMemberIdAndJobPostingID(Convert.ToInt32(id), jobPostingid);
                            MemberJoiningDetail joiningDetail = Facade.GetMemberJoiningDetailsByMemberIdAndJobPostingID(Convert.ToInt32(id), jobPostingid);
                            //IList<Interview> interviews = Facade.GetInterviewByMemberIdAndJobPostingID(Convert.ToInt32(id), jobPostingid);
                            if (jobCart != null)
                            {
                                //code modify by pravin khot on 24/June/2016**********
                                //Facade.DeleteMemberJobCartById(jobCart.Id);//OLD CODE
                                Facade.RejectMemberJobCartById(jobCart.Id);//NEW CODE ADDED
                                //***********END******************************
                            }
                            if (hiringDetail != null)
                            {
                                Facade.DeleteMemberHiringDetailsByID(hiringDetail.Id);
                            }
                            if (joiningDetail != null)
                            {
                                Facade.DeleteMemberJoiningDetailsByID(joiningDetail.Id);
                            }
                            //*********Code commented by pravin khot 27/July/2016******
                            //if (interviews != null)
                            //{
                            //    foreach (Interview interview in interviews)
                            //    {
                            //        Facade.DeleteInterviewById(interview.Id);
                            //    }
                            //}
                            //**************END*************************
                            //EventLogForRequisitionAndCandidate log = BuildEventLog(Convert.ToInt32(id), jobPostingid);
                            MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.RemoveCandidateFromHiringMatrix, jobPostingid, id, CurrentMember.Id, "", Facade);
                            //Facade.AddEventLog(log, id);

                        }

                        ScriptManager.RegisterClientScriptBlock(txtReject, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Successfully rejected selected candidate(s).');", true);
                    }


                    //if (RejectDetailsAdded != null)
                    //   RejectDetailsAdded(Convert.ToInt32(hfStatusId.Value != string.Empty ? hfStatusId.Value : "0"), hfMemberId.Value);
                }
            }
            if (!isMainApplication)
            {
                IList<Member> member = Facade.GetAllMemberByIds(hfMemberId.Value);
                if (member != null)
                {
                    foreach (Member m in member)
                    {
                        string mailbody = Facade.Interview_GetInterviewTemplate(m.Id, 0, "ProfileReject", 0);
                        MailQueueData.AddMailToMailQueue(CurrentMember.Id, m.PrimaryEmail, "L&T IES - Feedback", mailbody, "", "", null, Facade);
                    }
                }
            }
            IsEdit = false;
        }

        #endregion
    }
}
