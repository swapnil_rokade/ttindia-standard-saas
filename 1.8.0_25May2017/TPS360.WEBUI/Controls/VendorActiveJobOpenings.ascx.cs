/*<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: VendorApplicants.ascx.cs
    Description: This is the user control page used to display the ooption like ToDo,PDF,Print and many more
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date              Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            15/March/2017     pravin khot         Added new column -ID,Contact Number,NP
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>*/
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Web.UI;
using System.Collections;

public partial class VendorActiveJobOpenings : BaseControl
{
    private bool _IsMemberMailAccountAvailable = false;
    private bool isAccess = false;
    private int VendorId
    {
        get
        {
            if (CurrentMember != null)
            {
                CompanyContact com = new CompanyContact();
                com = Facade.GetCompanyContactByMemberId(CurrentMember.Id);
                return com.CompanyId;
            }
            else
            {
                return 0;
            }

        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //uclVendorSubmissions.ContactId = base.CurrentMember.Id;
        hdnVendorId.Value = VendorId.ToString();
        if (SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString() != null)
        {
            hdnEmployeeDashboard.Value = SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString();
            this.Page.Title = hdnEmployeeDashboard.Value + " - Dashboard";

        }
        string pagesize = "";
        pagesize = (Request.Cookies["DashboardVendorJobPostingRowPerPage"] == null ? "" : Request.Cookies["DashboardVendorJobPostingRowPerPage"].Value); ;
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
        }

    }
    protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        {
            JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
            if (jobPosting != null)
            {
                Label lblPostedDate = (Label)e.Item.FindControl("lblPostedDate");
                HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                Label lblExperience = (Label)e.Item.FindControl("lblExperience");
                Label lblLocation = (Label)e.Item.FindControl("lblLocation");
                Label lblDueDate = (Label)e.Item.FindControl("lblDueDate");
                Label lblDueinDays = (Label)e.Item.FindControl("lblDueinDays");
               
                lblPostedDate.Text = jobPosting.PostedDate.ToShortDateString();
                lnkJobTitle.Text = jobPosting.JobTitle;
                lnkJobTitle.Attributes.Add("onclick", "javascript:EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "&FromPage=Vendor','700px','570px')");
                JobPosting js = Facade.GetJobPostingById(jobPosting.Id);
                if (js != null)
                {
                    lblExperience.Text = js.MinExpRequired + " - " + js.MaxExpRequired + " Years";
                    lblLocation.Text = js.City;
                    lblDueDate.Text = js.FinalHiredDate.ToShortDateString();
                    

                    DateTime d1 = DateTime.Now;
                    DateTime d2 = js.FinalHiredDate;

                    TimeSpan t = d2 - d1;
                    int NrOfDays = Convert.ToInt32(t.TotalDays);
                    lblDueinDays.Text = NrOfDays.ToString();
                }
            }
        }
    }

    protected void lsvJobPosting_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

        try
        {
            if (e.CommandName == "Sort")
            {
                LinkButton lnkbutton = (LinkButton)e.CommandSource;
                if (hdnSortColumn.Value == lnkbutton.ID)
                {
                    if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                    else hdnSortOrder.Value = "ASC";
                }
                else
                {
                    hdnSortColumn.Value = lnkbutton.ID;
                    hdnSortOrder.Value = "ASC";
                }
            }
        }
        catch
        {
        }

    }

    protected void lsvJobPosting_PreRender(object sender, EventArgs e)
    {
        ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
        if (PagerControl != null)
        {
            DataPager pager = (DataPager)PagerControl.FindControl("pager");
            if (pager != null)
            {
                if (pager.Controls.Count >= 1)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
            }
            HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardVendorJobPostingRowPerPage";
        }
        PlaceUpDownArrow();
    }
    protected void odsRequisitionList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {

    }
    private void PlaceUpDownArrow()
    {
        try
        {
            LinkButton lnk = (LinkButton)lsvJobPosting.FindControl(hdnSortColumn.Value);
            System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            im.EnableViewState = false;
            if (lnk.CommandArgument.Contains("[1]") || lnk.CommandArgument.Contains("[JMC].[ManagersCount]"))
            {
                im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Descending" : "Ascending"));
            }
            else
                im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
        }
        catch
        {
        }

    }
   
}
