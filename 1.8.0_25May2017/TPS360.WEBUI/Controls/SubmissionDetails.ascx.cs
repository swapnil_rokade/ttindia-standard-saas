﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.IO;

namespace TPS360.Web.UI
{
    public partial class SubmissionDetails : ATSBaseControl   
    {
        #region Member Variables
    
        #endregion

        #region Properties
        public string  bulk = string.Empty ;
        public string  BulkAction
        {
            get
            {
                return hdnBulkAction.Value == string.Empty ? "" : hdnBulkAction.Value;
            }
            set
            {
                hdnBulkAction.Value = value.ToString();
                bulk = value;
            }
        }
        public string _MemberId = string.Empty;
        public string MemberID
        {
            get
            {

                return hfMemberId.Value == string.Empty ? "0" : hfMemberId.Value;
            }
            set
            {
                StatusId = 0;
                hfMemberId.Value = value.ToString();
                _MemberId = value;
                if(!hfMemberId .Value .Contains (",") )
                    PrepareEditView();
                hdnBulkAction.Value = string.Empty;
            }
        }
        public int _StatusId = 0;
        public int StatusId
        {
            get
            {
                return Convert.ToInt32(hfStatusId.Value == string.Empty ? "0" : hfStatusId.Value);
            }
            set
            {
                hfStatusId.Value = value.ToString();
                _StatusId = value;
            }
        }
        public int _JobPostingId = 0;
        public int JobPostingId
        {
            get
            {
                return Convert.ToInt32(hfJobPostingId.Value == string.Empty ? "0" : hfJobPostingId.Value);
            }
            set
            {
                hfJobPostingId.Value =  value.ToString ()  ;
                _JobPostingId = value;
                PrepareView();
            }
        }
        private string AccountType
        {
            get
            {
                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                    return  "Account";
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                    return   "BU";
                else return  "Vendor";
            }
        }
        #endregion

        #region Methods
        private void PrepareEditView()
        {
            MemberSubmission membersumissions = Facade.GetMemberSubmissionsByMemberIDAndJobPostingId(Convert.ToInt32(hfMemberId.Value), JobPostingId);
            if (membersumissions != null)
            {

                if (membersumissions.SubmittedDate != DateTime.MinValue)
                {
                    wdcSubmissionDate.SelectedDate = membersumissions.SubmittedDate;
                    txtSubmissionDate.Text = membersumissions.SubmittedDate.ToShortDateString();
                }
                else { txtSubmissionDate.Text = ""; wdcSubmissionDate.SelectedDate = null; }
                ControlHelper.SelectListByValue(ddlSubmittedBy, membersumissions.CreatorId.ToString());

                JobPosting jobposting = Facade.GetJobPostingById(JobPostingId);
                if (jobposting.ClientId > 0)
                {
                    ControlHelper.SelectListByValue(ddlAccount, jobposting.ClientId.ToString());
                    hdnAccountNo.Value = jobposting.ClientId.ToString();
                }
                else
                {
                    ControlHelper.SelectListByValue(ddlAccount, membersumissions.CompanyId.ToString());
                    hdnAccountNo.Value = membersumissions.CompanyId.ToString();
                }
                if (hdnAccountNo.Value != string.Empty)
                {
                    //ScriptManager.RegisterClientScriptBlock(Page, typeof (Page ), "CompanyContact1", "<script>sss();</script>", false  );
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "sss", "sss()", true);
                }
                hdnReceiver.Value = membersumissions.ReceiverEmail;
                string[] email = Regex.Split(membersumissions.ReceiverEmail, ";", RegexOptions.IgnorePatternWhitespace);
                foreach (ListItem item in chkListSubmittedTo .Items )
                {
                    if (email.Contains(item.Value))
                        item.Selected = true;
                }
            }
        }

        public MemberSubmission BuildSubmissionDetails(MemberSubmission memberSubmission)
        {
            DateTime value;
            if (DateTime.Now.ToShortDateString().Contains(txtSubmissionDate.Text))
                value = DateTime.Now;
            else
                DateTime.TryParse(txtSubmissionDate.Text, out value);
            wdcSubmissionDate.SelectedDate = value;
            if (txtSubmissionDate.Text.Trim() != string.Empty && wdcSubmissionDate.SelectedDate != null)
                memberSubmission.SubmittedDate = Convert.ToDateTime(wdcSubmissionDate.SelectedDate);
            memberSubmission.CompanyId =Convert.ToInt32(hdnAccountNo .Value );
            memberSubmission.JobPostingId = JobPostingId;
            memberSubmission.IsRemoved = false;
            memberSubmission.CreatorId = Convert.ToInt32(ddlSubmittedBy.SelectedValue);
            memberSubmission.MemberEmailDetailId = 0;
            memberSubmission.ReceiverEmail = hdnReceiver.Value;
            memberSubmission.UpdatorId = CurrentMember.Id;
            return memberSubmission;
        }
        public void PrepareView()
        {
            //if (!IsPostBack)
            {
                if (JobPostingId > 0)
                {
                    try
                    {
                        MiscUtil.PopulateAssignedMemberTeamByJobPosting(ddlSubmittedBy, JobPostingId, Facade);
                        ddlSubmittedBy.Items.RemoveAt(0);
                        ddlSubmittedBy.SelectedValue = CurrentMember.Id.ToString();
                    }
                    catch
                    {
                    }
                }
            }

            Company company = new Company();
            if (JobPostingId > 0)
            {
                JobPosting CurrentJobPosting = Facade.GetJobPostingById(JobPostingId);
                if (CurrentJobPosting.ClientId > 0 && (company = Facade.GetCompanyById(CurrentJobPosting.ClientId)) != null)
                {
                    ddlAccount.Visible = false;
                    lblAccount.Visible = true;
                    lblAccount.Text = company.CompanyName;
                    hdnAccountNo.Value = company.Id.ToString();
                }
                else
                {
                    ddlAccount.Visible = true;
                    lblAccount.Visible = false;
                    lblAccount.Text = "";
                    MiscUtil .PopulateCompanyByCompanyStatus (ddlAccount ,MiscUtil .GetCompanyStatusFromDefaultSiteSetting (SiteSetting ),Facade );
                    ddlAccount.Items.RemoveAt(0);
                    ddlAccount = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlAccount);
                    ddlAccount.Items.Insert(0, new ListItem("Select " + AccountType , "0"));

                }
            }
        }
        #endregion

        #region PageEvents
        protected void Page_Load(object sender, EventArgs e)
        {
            wdcSubmissionDate.Format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern; //"dd/MM/yyyy";
            hfMemberId.Value = MemberID.ToString ();
            if (!IsPostBack) {
                lblClient.Text = AccountType;

                if(wdcSubmissionDate .SelectedDate ==null )
                wdcSubmissionDate.SelectedDate = DateTime.Now;
            Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
            if (lbModalTitle != null) lbModalTitle.Text = "Submission Details";
            
            }
           
        }
        #endregion

        #region ButtonEvents
        protected void btnSave_Click(object sender, EventArgs e)
        {
            MemberSubmission memberSubmission = new MemberSubmission();
            memberSubmission = BuildSubmissionDetails(memberSubmission);
            Facade.AddMemberSubmission(memberSubmission, hfMemberId.Value);

            //*******Code added by pravin khot on 28/July/2016**************
            RejectCandidate cand = Facade.GetRejectCandidateByMemberIdAndJobPostingID(Int32.Parse(hfMemberId.Value), JobPostingId);
            if (cand != null)
            {
                Facade.RejectToUnRejectCandidateStatusChange(hfMemberId.Value, JobPostingId);
            }
            //************************END****************************

            var webConfig = System.Web.Configuration . WebConfigurationManager.OpenWebConfiguration("~");
            var submit = webConfig.AppSettings.Settings["Submission"];
            if(submit !=null && submit.Value  !="")
                     MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, JobPostingId, hfMemberId.Value, CurrentMember.Id, submit .Value , Facade);

            if (hfStatusId.Value != "" && Convert.ToInt32(hfStatusId.Value) > 0)
            {
                Facade.MemberJobCart_MoveToNextLevel(0, base.CurrentMember.Id, hfMemberId .Value , JobPostingId , hfStatusId .Value );
                Facade.UpdateCandidateRequisitionStatus (JobPostingId, hfMemberId.Value, base.CurrentMember.Id, Convert .ToInt32 (hfStatusId .Value ));
                ScriptManager.RegisterClientScriptBlock(lblAccount, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Selected candidate(s) moved Successfully.');", true);
            }
            else
                ScriptManager.RegisterClientScriptBlock(lblAccount, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Submission Details Updated Successfully.');", true);
            //hdnAccountNo.Value = "";
            //hdnReceiver.Value = "";
            //wdcSubmissionDate.SelectedDate = DateTime.Now;
            //txtSubmissionDate.Text = DateTime.Now.ToShortDateString();
            // PrepareView();
        }
        #endregion

    }
}