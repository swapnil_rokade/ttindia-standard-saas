﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: NotesAndActivitiesEditor.ascx.cs
    Description: This is the user control page used for member notes edit functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author               Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Oct-16-2008           Yogeesh Bhat         Defect ID: 8968; Changes made in lsvNotesList_ItemDataBound() method.
                                                              (checked member object for null)
    0.2            Nov-14-2008           Jagadish             Defect ID: 9070; Alert message was repeating. Changed the code to show appropriate message 
                                                              and disabled the delete button when edit button for any document clicked.
    0.3            Feb-03-2009           Kalyani Pallagani    Defect Id : 9756; Added code to clear the note and noteid when note has been deleted
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Text;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

namespace TPS360.Web.UI
{
    public partial class ControlNotesAndActivitiesEditor : BaseControl
    {
        #region Member Variables

        private int _memberId 
        {
            
            
            get 
            {
                int mId = 0;

                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    mId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

                }

                return mId;
            }
        }
        CommonNote _comonNotes = null;

        //public int memberId
        //{
        //    set { _memberId = value; }
        //}

        #endregion

        #region Properties

        public int CommonNoteId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_CommonNoteId"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_CommonNoteId"] = value;
            }
        }
        private CommonNote CurrentNote
        {
            get
            {
                if (_comonNotes == null)
                {
                    if (CommonNoteId > 0)
                    {
                        _comonNotes = Facade.GetCommonNoteById(CommonNoteId);
                    }
                    
                    if (_comonNotes == null)
                    {
                        _comonNotes = new CommonNote();
                    }
                }

                return _comonNotes;
            }
        }
        #endregion

        #region Methods    

        private MemberNote BuildMemberNot()
        {
            MemberNote memberNote = new MemberNote();

            if (_memberId > 0)
            {
                memberNote.MemberId = _memberId;
            }
            else
            {
                memberNote.MemberId = base.CurrentMember.Id;
            }
            memberNote.CreatorId = base.CurrentMember.Id;
            return memberNote;
        }
        #endregion

        #region Events

        #region Page event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {             
                txtSortColumn.Text = "btnDateTime";
                txtSortOrder.Text = "Desc";              
                PrepareView1();
                LoadLink();
               
            }
            else
            {
                PrepareView1();
                LoadLink();
            }

            if (Request.Url.ToString().ToLower().Contains("internalnotesandactivities.aspx"))  
            {
                btnAddNote.Text = "Add Note"; 
            }
            else
            {
               btnAddNote.Text = "Refresh";
            }

        }
        public void PrepareView1()
        {
            divnotes.Controls.Clear();

            StringBuilder strBUildResult = new StringBuilder();
                      

            Response.Clear();

            string interview = string.Empty;
            IFacade facade = new Facade();
           // IList<JobPosting> JobPosting = new List<JobPosting>();
            IList<CommonNote> CommonNotes = new List<CommonNote>();
            //JobPosting = facade.MemberCareerAllOpportunities();
            string sortExpression = "";
            CommonNotes = Facade.GetAllCommonNoteByMemberId(Convert.ToInt32(_memberId), sortExpression);


            HtmlTable TB_List = new HtmlTable();


            TB_List.Attributes.Add("onscroll", "SetScrollPosition()");


            TB_List.Attributes.Add("style", "font-size:16px; FONT-FAMILY: 'Arial';width:100%;height:50%; overflow: auto; zoom: 1; color:#1154af; position: left; margin: auto; padding:5px; ");
            TB_List.Width = "100%";
            TB_List.Style.Add("margin", "0");
            TB_List.Align = "center";
            bigDiv.Attributes.Add("style", "width:100%;height:100%; overflow: auto; margin: auto;");
         
            if (CommonNotes != null)
            {
                # region

                foreach (CommonNote jobpost in CommonNotes)
                {

                    //Unit width = new Unit(30, UnitType.Pixel);
                    HtmlTableRow Tr = new HtmlTableRow();
                    // Tr.Attributes["style"] = "width: 1200px";               
                    Tr.Attributes.CssStyle.Add("float", "center");
                    //Tr.Attributes["style"] = "font-size='5px'";
                    Tr.Style.Add("font-size", "'5px'");

                    HtmlTableCell td_blank = new HtmlTableCell();
                    td_blank.Style.Add("width", "400px");
                    td_blank.Style.Add("float", "center");

                    HtmlTableCell td_L = new HtmlTableCell();
                    //td_L.Attributes["style"]. = "align:center";
                    td_L.Style.Add("align", "right");
                    td_L.Style.Add("width", "50px");
                    td_L.Style.Add("float", "center");
                    td_L.Style.Add("font-size", "'5px'");
                    td_L.Attributes.Add("style", "text-align:left;");
                     td_L.Attributes.Add("style", "white-space:pre-wrap ; word-wrap:break-word;");
                                       

                    //td_L.Attributes["style"] = "width: 150px";
                    //td_L.Attributes.CssStyle.Add("float", "center");

                    HtmlTableCell td_blank1 = new HtmlTableCell();
                    td_blank1.Style.Add("width", "400px");
                    td_blank1.Style.Add("float", "center");


                    HtmlTableCell td_R = new HtmlTableCell();
                    td_R.Attributes["style"] = "align:center";
                    td_R.Attributes["style"] = "width: 300px";
                    HtmlTableCell td_Bview = new HtmlTableCell();


                    HtmlTableRow Tr_new = new HtmlTableRow();
                    HtmlTableCell td_L_new = new HtmlTableCell();
                    td_L_new.Style.Add("width", "50px");
                    td_L_new.Style.Add("float", "center");
                    td_L_new.Attributes.Add("style", "text-align:left;");
                    td_L_new.Attributes.Add("style", "color:black;font-size:10px");
                    HtmlTableCell td_R_new = new HtmlTableCell();

                    HtmlTableCell td_blank2 = new HtmlTableCell();
                    td_blank2.Style.Add("width", "400px");
                    td_blank2.Style.Add("float", "center");

                    HtmlTableRow Tr_loc = new HtmlTableRow();
                    HtmlTableCell td_L_loc = new HtmlTableCell();
                    td_L_loc.Style.Add("width", "50px");
                    td_L_loc.Style.Add("float", "center");
                    td_L_loc.Attributes.Add("style", "text-align:left;");
                    td_L_loc.Attributes.Add("style", "color:black;font-size:10px");
                    HtmlTableCell td_R_loc = new HtmlTableCell();

                    HtmlTableCell td_blank3 = new HtmlTableCell();
                    td_blank3.Style.Add("width", "400px");
                    td_blank3.Style.Add("float", "center");

                    HtmlTableRow Tr_exp = new HtmlTableRow();
                    HtmlTableCell td_L_exp = new HtmlTableCell();
                    td_L_exp.Style.Add("width", "10px");
                    td_L_exp.Style.Add("float", "center");
                    td_L_exp.Style.Add("font-size", "'5px'");
                    td_L_exp.Attributes.Add("style", "text-align:left;");
                    td_L_exp.Attributes.Add("style", "color:black;font-size:10px");

                    HtmlTableCell td_R_exp = new HtmlTableCell();

                    HtmlTableCell td_Bapply = new HtmlTableCell();

                    HtmlTableCell td_blank4 = new HtmlTableCell();
                    td_blank4.Style.Add("width", "400px");
                    td_blank4.Style.Add("float", "center");

                    HtmlTableRow Tr_empty = new HtmlTableRow();
                    HtmlTableCell td_L_empty = new HtmlTableCell();
                    td_L_empty.Style.Add("width", "50px");
                    td_L_empty.Style.Add("float", "center");
                    HtmlTableCell td_R_empty = new HtmlTableCell();
                    HtmlTableCell td_line = new HtmlTableCell();
                    td_L_empty.Attributes["style"] = "color: black";


                    // td_R_empty.Attributes["style"] = "width: 1200px";



                    Tr.ID = "Tr_" + jobpost.Id ;
                    td_L.ID = "td_L" + jobpost.Id;
                    td_R.ID = "td_R" + jobpost.Id;

                    Button BtnApply = new Button();
                    // Button BtnView = new Button();
                    HyperLink BtnView = new HyperLink();

                    BtnView.Attributes.Add("style", "color:#1154af;text-decoration:underline;font-size:15px");
                    BtnView.Font.Name = "arial";

                    BtnApply.Attributes["style"] = "color: white";
                    //BtnApply.Attributes["style"] = "width:1200px";
                    BtnApply.Attributes["style"] = "height: 35px";
                    // BtnApply.Attributes["style"] = "background-color:#088A85";
                    //BtnApply.Attributes.Add("style", "width:160px;background-color:#088A85");
                    BtnApply.Attributes.Add("style", "width:80px;background-color:#1154af");

                    // BtnApply.BorderColor  = System.Drawing.Color.DarkGreen ;   

                    BtnApply.ForeColor = System.Drawing.Color.White;
                    //BtnApply.Font.Bold = true;
                    BtnApply.Font.Name = "arial";
                    //BtnApply.Font.Size = FontUnit.Larger;

                    BtnView.ID = "btnView_" + jobpost.Id;
                    BtnView.Text = "View Details";
                    BtnView.ToolTip = "Click here,View Details post of " + jobpost.JobTitle;

                    BtnView.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/CareerJobDetail.aspx?JID=" + jobpost.Id + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    //td_L.Controls.Add(BtnApply);

                    //td_Bview.Controls.Add(BtnView);


                    td_R.InnerText = jobpost.NoteDetail;
                    td_R.Attributes.Add("style", "width:160px,font-weight:bold");
                    //td_L.InnerText = jobpost.NoteDetail;  //"Job Title :";
                    td_L.InnerText = jobpost.NoteDetail.Replace("\r\n", "</br>");

                    //Tr.Cells.Add(td_blank);
                    Tr.Cells.Add(td_L);
                    //Tr.Cells.Add(td_R);
                    Tr.Cells.Add(td_Bview);

                    TB_List.Controls.Add(Tr);
                    divnotes.Controls.Add(TB_List);

                    //***************************REQ. CODE************************************

                     Member member = Facade.GetMemberById(jobpost.CreatorId);
                     string test = "";
                     string categary = "";
                    if (member != null)
                    {
                         test = member.FirstName + " " + member.MiddleName + " " + member.LastName;
                    }

                    if (jobpost.NoteCategoryLookupId > 0)
                    {
                        GenericLookup g = Facade.GetGenericLookupById(jobpost.NoteCategoryLookupId);
                        if (g != null)
                            //td_L_exp.InnerText = g.Name;
                            categary = g.Name;
                    }

                    string space = " " + "||" + " ";

                    Tr_new.ID = "Tr_new" + jobpost.Id;

                    td_L_new.ID = "td_L_new" + jobpost.Id;
                    td_R_new.ID = "td_R_new" + jobpost.Id;

                    td_R_new.InnerText = jobpost.JobPostingCode;
                   // td_L_new.InnerText = jobpost.CreateDate.ToShortDateString() + " " + jobpost.CreateDate.ToShortTimeString() + " " + space + " " + test + " " + categary; //"Req. Code :";
                    //td_L_new.Attributes.Add("style", "word-spacing: 30px;");

                    //Tr_new.Cells.Add(td_blank1);
                    Tr_new.Cells.Add(td_L_new);
                    //Tr_new.Cells.Add(td_R_new);

                    TB_List.Controls.Add(Tr_new);
                    divnotes.Controls.Add(TB_List);

                    //***************************LOCATION************************************


                    Tr_loc.ID = "Tr_loc" + jobpost.Id;

                    td_L_loc.ID = "td_L_loc" + jobpost.Id;
                    td_R_loc.ID = "td_R_loc" + jobpost.Id;

                    td_R_loc.InnerText = jobpost.JobTitle;
                    //td_L_loc.InnerText = "Location :";
                    td_L_loc.InnerText = jobpost.CreateDate.ToShortDateString() + " " + jobpost.CreateDate.ToShortTimeString() + " " + space + " " + test + " " + space + " " + categary; //"Req. Code :";
                                       

                    //Tr_loc.Cells.Add(td_blank2);
                    Tr_loc.Cells.Add(td_L_loc);
                   // Tr_loc.Cells.Add(td_R_loc);

                    TB_List.Controls.Add(Tr_loc);
                    divnotes.Controls.Add(TB_List);


                    //***************************EXPERIENCE************************************

                    BtnApply.ID = "btnApply_" + jobpost.Id;
                    BtnApply.Text = "Apply";
                    BtnApply.ToolTip = "Click here,Apply the post " + jobpost.JobTitle;
                    //BtnApply.OnClientClick = "Redirect('" + url1 + "');";
                    //BtnApply.Attributes.Add("onclick", "EditModal('" + url1.ToString() + "','900px','620px'); return false;");//  use popup windows

                    //td_Bapply.Controls.Add(BtnApply);

                    Tr_exp.ID = "Tr_exp" + jobpost.Id;

                    td_L_exp.ID = "Td_L_exp" + jobpost.Id;
                    td_R_exp.ID = "Td_R_exp" + jobpost.Id;

                    string year = "";
                    //year = (jobpost.MinExpRequired + ((jobpost.MinExpRequired.Trim() != string.Empty && jobpost.MaxExpRequired.Trim() != string.Empty) ? " - " : "") + jobpost.MaxExpRequired.Trim());

                    if (year == "")
                    {
                        year = "";
                    }
                    else
                    {
                        year = year + " years";
                    }
                    td_R_exp.InnerText = year;
                    //td_L_exp.InnerText = "Experience :";

                   

                    //Tr_exp.Cells.Add(td_blank3);
                    Tr_exp.Cells.Add(td_L_exp);
                    //Tr_exp.Cells.Add(td_R_exp);
                    Tr_exp.Cells.Add(td_Bapply);

                    TB_List.Controls.Add(Tr_exp);
                    divnotes.Controls.Add(TB_List);


                    if (Request.Url.ToString().ToLower().Contains("internalnotesandactivities.aspx"))             
                    {
                        td_L_empty.InnerText = "_________________________________________________________________________________________________________";                                     
                    }
                    else
                    {
                        td_L_empty.InnerText = "____________________________________________________________________________________________";
                      
                    }
                                      
                    td_L_empty.Attributes.Add("style", "text-align:left;");            

             
                    Tr_empty.Cells.Add(td_L_empty);               

                    TB_List.Controls.Add(Tr_empty);
                    divnotes.Controls.Add(TB_List);

                    //***************************************




                }
                # endregion
            }
        }  
        private void LoadLink()
        {
            btnAddNote.Visible = true;
            SecureUrl urlavailability = UrlHelper.BuildSecureUrl("../" + UrlConstants.Candidate.CANDIDATE_INTERNALNOTESSHARING, string.Empty, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(_memberId));
            btnAddNote.Attributes.Add("onclick", "EditModal('" + urlavailability.ToString() + "','500px','400px'); return false;");

             //SecureUrl urlavailability = UrlHelper.BuildSecureUrl("../" + UrlConstants.Candidate.CANDIDATE_INTERNALNOTESSHARING, string.Empty, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(_memberId));
            // btnAddNote.Attributes.Add("onclick", "EditModal('" + urlavailability.ToString() + "','700px','400px'); return false;");

             System.Web.UI.HtmlControls.HtmlGenericControl divButtons = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Page.Master.FindControl("divButtons");
             string buttons = "";
             //buttons += "<input type=\"button\"   class=\"btn\" value=\"Email\"  style='margin-right:2px' onclick=\"window.open('" + urlavailability.ToString() + "','EmailView', 'height=500,width=400, toolbar=0, scrollbars=1, menubar=0, location=0');\"/>";

             buttons += "<input type=\"button\"   class=\"btn btn-primary\" value=\"Add Note\"  style='margin-right:52px ,margin-top:52px ' onclick=\"window.open('" + urlavailability.ToString() + "','EmailView11', 'height=400,width=500,left=400');\"/>";
             if (divButtons != null) divButtons.InnerHtml = buttons;

             //btnAddNote.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/CandidateNotes.aspx?mid=" + candidatInfo.MemberId + "','880px','570px'); return false;";

            // btnAddNote.OnClientClick = "EditModal('" + urlavailability.ToString() + "','400px','300px'); return false;";
                     
                         
             //btnAddNote.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/EmailEditorPop.aspx?meid=" + PrimaryEmail + "&mid=" + MemberId + "','730px','650px'); return false;";
            
            //btnAddNote.OnClientClick = "EditModal('" + urlavailability.ToString() + "','200px','100px'); return false;"; 

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
         
        }

    

        #endregion
   
        #endregion
         protected void btnAddNote_Click(object sender, EventArgs e)
         {
             //LoadLink();

             //SecureUrl urlavailability = UrlHelper.BuildSecureUrl("../" + UrlConstants.Candidate.CANDIDATE_INTERNALNOTESSHARING, string.Empty, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(_memberId));
             //btnAddNote.Attributes.Add("onclick", "EditModal('" + urlavailability.ToString() + "','700px','400px'); return false;");

             //System.Web.UI.HtmlControls.HtmlGenericControl divButtons = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Page.Master.FindControl("divButtons");
             //SecureUrl urlEmail = UrlHelper.BuildSecureUrl("../Dashboard/" + UrlConstants.Dashboard.NEWMAIL, "", UrlConstants.PARAM_JOB_ID, "201", UrlConstants.PARAM_PAGE_FROM, "JobDetail");
             //string buttons = "";
             ////buttons += "<input type=\"button\"   class=\"btn\" value=\"Email\"  style='margin-right:2px' onclick=\"window.open('" + urlavailability.ToString() + "','EmailView', 'height=500,width=400, toolbar=0, scrollbars=1, menubar=0, location=0');\"/>";

             //buttons += "<input type=\"button\"   class=\"btn\" value=\"Email1\"  style='margin-right:2px' onclick=\"window.open('" + urlavailability.ToString() + "','EmailView1', 'height=500,width=400, toolbar=0, scrollbars=1, menubar=0, location=0');\"/>";
             //if (divButtons != null) divButtons.InnerHtml = buttons;
         }
}
}
