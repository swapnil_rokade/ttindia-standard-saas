﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Company.ascx.cs
    Description: This is the user control page used for company registration
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author                  MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sep-15-2008           Yogeesh Bhat           Defect ID: 8660,8685; Changes made in SaveCompany() method.
                                                                (changed CurrentMember.Id to company.CreatorId)
    0.2            Jan-30-2009           Kalyani Pallagani      Defect ID: 8932,9203; Changes made in SaveCompany() method
                                                                to redirect to my company list page.Added RedirectToList method.
    0.3            Jan-30-2009           Kalyani Pallagani      Defect id: 9045; Commented company logo image dispaly code
 *  0.4            Apr-27-2009           Rajendra Prasad        Defect id: 10397;Added new method PopulateSiteSettings() to display
 *                                                              ddlcountry selected value according to sitesettings.
    0.5            Jun-18-2009               Veda               Defect id: 10647; when the user saves Companyinfo it should redirect to ContactsTab/FinancialData /DocumentTab  
 *  0.6            Apr-30-2010           Ganapati Bhat          Defect id: 12733; Commented lines to disable Toll free number extension
 *  0.7            1/June/2016           pravin khot            comment witch statement
 *  0.8            6/June/2016           pravin khot            added - cmpStartDate.ValueToCompare
 *  0.9            8/June/2016           pravin khot            added- cmpStartDate.Visible  = false;          
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Collections;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
namespace TPS360.Web.UI
{
    public partial class cltCompany : CompanyBaseControl
    {
        #region Member Variables

        public event Action<int> OnSaved;

        #endregion

        #region Properties

        public CompanyStatus Status
        {
            set;
            get;
        }

        #endregion

        #region Methods

        private void BuildDropDownList()
        {  
            //if (ddlCountryId.SelectedItem.Text == "United States")
            //    divEINNumber.Style.Add("display", "");
            //else divEINNumber.Style.Add("display", "none");
            MiscUtil.PopulateIndustryType(ddlIndustryType, Facade);
            MiscUtil.PopulateCompanySize(ddlCompanySize, Facade);
            MiscUtil.PopulateOwnerType(ddlOwnerType, Facade);
            //MiscUtil.PopulateState(ddlStateId, Convert.ToInt32(ddlCountryId.SelectedValue), Facade);
        }

        private void PopulateSiteSettings(DropDownList ddlCountry)
        {
            SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            if (siteSetting != null)
            {
                Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                ddlCountry.SelectedValue = (siteSettingTable[DefaultSiteSetting.Country.ToString()].ToString() == "0") ? "All" : siteSettingTable[DefaultSiteSetting.Country.ToString()].ToString(); //0.2
            }
        }
        private void ApplyDefaultsFromSiteSetting()
        {
            if (SiteSetting != null) uclCountryState.SelectedCountryId = Convert.ToInt16(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString());
            else uclCountryState.SelectedCountryName="Unites States";
               
            //if (ddlCountryId.SelectedItem.Text == "United States")
            //    divEINNumber.Style.Add("display", "");
            //else divEINNumber.Style.Add("display", "none");
        }
        private void PrepareView()
        {
            BuildDropDownList();
            Company company = base.CurrentCompany;
            txtCompanyName.Focus();
            if (!company.IsNew)
            {
                txtCompanyName.Text = MiscUtil.RemoveScript(company.CompanyName, string.Empty);
                txtAddress1.Text = MiscUtil.RemoveScript(company.Address1, string.Empty);
                txtAddress2.Text = MiscUtil.RemoveScript(company.Address2, string.Empty);
                //code commented by pravin khot on 1/June/2016***********
                //switch (CurrentCompanyStatus)
                //{
                //    case CompanyStatus.Vendor:

                txtServiceFee.Text = MiscUtil.RemoveScript(company.ServiceFee == 0 ? "" : company.ServiceFee.ToString(), string.Empty);
                wdcStartDate.Value = Convert.ToDateTime(company.ContractStartDate.ToShortDateString());
                wdcEndDate.Value = Convert.ToDateTime(company.ContractEndDate.ToShortDateString());

                //        break;
                //}
                if (company.ContractStartDate.ToShortDateString() != "")
                {
                    if (company.ContractStartDate > DateTime.Now)
                    {
                        cmpStartDate.ValueToCompare = DateTime.Now.ToShortDateString();
                        cmpEndDate.ValueToCompare = DateTime.Now.ToShortDateString();
                    }
                    else
                    {
                        cmpStartDate.ValueToCompare = company.ContractStartDate.ToShortDateString();
                        cmpEndDate.ValueToCompare = company.ContractStartDate.ToShortDateString();
                    }
                }
                else
                {
                    //cmpStartDate.ValueToCompare = DateTime.Now.ToShortDateString();
                    //cmpEndDate.ValueToCompare = DateTime.Now.ToShortDateString();
                }

                //********************END************************************

                uclCountryState.SelectedCountryId = company.CountryId;
                uclCountryState.SelectedStateId = company.StateId;
                txtCity.Text = MiscUtil.RemoveScript(company.City, string.Empty);
                txtZip.Text = MiscUtil.RemoveScript(company.Zip, string.Empty);

                txtWebAddress.Text = MiscUtil.RemoveScript(company.WebAddress, string.Empty);
                txtCompanyInformation.Text = MiscUtil.RemoveScript(company.CompanyInformation, string.Empty);
                ControlHelper.SelectListByValue(ddlIndustryType, StringHelper.Convert(company.IndustryType));
                ControlHelper.SelectListByValue(ddlCompanySize, StringHelper.Convert(company.CompanySize));
                ControlHelper.SelectListByValue(ddlOwnerType, StringHelper.Convert(company.OwnerType));
                //txtTickerSymbol.Text = company.TickerSymbol;
                txtOfficePhone.Text = MiscUtil.RemoveScript(company.OfficePhone, string.Empty);
                txtOfficePhoneExtension.Text = MiscUtil.RemoveScript(company.OfficePhoneExtension, string.Empty);
                txtTollFreePhone.Text = MiscUtil.RemoveScript(company.TollFreePhone, string.Empty);
                txtFaxNumber.Text = MiscUtil.RemoveScript(company.FaxNumber, string.Empty);
                txtPrimaryEmail.Text = MiscUtil.RemoveScript(company.PrimaryEmail, string.Empty);
                //txtEINNumber.Text = company.EINNumber;
                //txtAnnualRevenue.Text = company.AnnualRevenue;
                //txtNumberOfEmployee.Text = company.NumberOfEmployee.ToString() == "0" ? "" : company.NumberOfEmployee.ToString();

            }
            else
            {
                ApplyDefaultsFromSiteSetting();
                //added by pravin khot on 6/June/2016*******************
                cmpStartDate.ValueToCompare = DateTime.Now.ToShortDateString();
                cmpEndDate.ValueToCompare = DateTime.Now.ToShortDateString();
                //***************END****************************
            }
              


            //if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            //{
            //    lblCompanyName.Text = "Department Name";
            //    lblCompanySize.Text = "Department Size";
            //    lblCompanyInformation.Text = "About the Department";
            //    lblEmail.Text = "Department Email";
            //}
        }
        void ClearControls()
        {
            txtCompanyName.Text = string.Empty;
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtZip.Text = string.Empty;
            txtWebAddress.Text = string.Empty;
            txtCompanyInformation.Text = string.Empty;
            txtOfficePhone.Text = string.Empty;
            txtTollFreePhone.Text = string.Empty;
            txtFaxNumber.Text = string.Empty;
            txtPrimaryEmail.Text = string.Empty;
            uclCountryState.SelectedCountryId = 0;
            txtServiceFee.Text = string.Empty;
            wdcStartDate.Value = string.Empty;
            wdcEndDate.Value = string.Empty;            
            //hdnSelectedStateId.Value = "0";
            ddlCompanySize.SelectedIndex = 0;
            ddlIndustryType.SelectedIndex = 0;
            ddlOwnerType.SelectedIndex = 0;
          
        }
        
        private Company BuildCompany()
        {
            Company company = CurrentCompany;

            company.CompanyName = txtCompanyName.Text = MiscUtil .RemoveScript (txtCompanyName.Text.Trim());
           // company.CompanyStatus = CompanyStatus.Client ;
            string  currentSiteMapId = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
            int targetsitemap = 0;
            if (currentSiteMapId == "11")
                company.CompanyStatus = CompanyStatus.Client;
            else if (currentSiteMapId == "638") company.CompanyStatus = CompanyStatus.Vendor ;
            else company.CompanyStatus = CompanyStatus.Department;


            txtCompanyName.Text = MiscUtil.RemoveScript(txtCompanyName.Text, string.Empty);
            company.Address1 = txtAddress1.Text = MiscUtil .RemoveScript (txtAddress1.Text.Trim());
            txtAddress1.Text = MiscUtil.RemoveScript(txtAddress1.Text, string.Empty);
            company.Address2 = txtAddress2.Text = MiscUtil .RemoveScript (txtAddress2.Text.Trim());
            txtAddress2.Text = MiscUtil.RemoveScript(txtAddress2.Text, string.Empty);
            //company.CountryId = Convert.ToInt32(ddlCountryId.SelectedValue);
            //company.StateId = Convert.ToInt32(hdnSelectedStateId.Value == "" ? "0" : hdnSelectedStateId.Value);
            company.CountryId = uclCountryState.SelectedCountryId;
            company.StateId = uclCountryState.SelectedStateId;
            company.City = txtCity.Text = MiscUtil .RemoveScript (txtCity.Text);
            txtCity.Text = MiscUtil.RemoveScript(txtCity.Text, string.Empty);
            company.Zip = txtZip.Text = MiscUtil .RemoveScript (txtZip.Text);
            txtZip.Text = MiscUtil.RemoveScript(txtZip.Text, string.Empty);
            company.WebAddress = txtWebAddress.Text = MiscUtil .RemoveScript (txtWebAddress.Text);
            txtWebAddress.Text = MiscUtil.RemoveScript(txtWebAddress.Text, string.Empty);
            company.CompanyInformation = txtCompanyInformation.Text = MiscUtil .RemoveScript (txtCompanyInformation.Text);
            txtCompanyInformation.Text = MiscUtil.RemoveScript(txtCompanyInformation.Text, string.Empty);

            company.IndustryType = Convert.ToInt32(ddlIndustryType.SelectedValue);
            company.CompanySize = Convert.ToInt32(ddlCompanySize.SelectedValue);
            company.OwnerType = Convert.ToInt32(ddlOwnerType.SelectedValue);
            company.TickerSymbol = "";// txtTickerSymbol.Text = MiscUtil.RemoveScript(txtTickerSymbol.Text);
            company.OfficePhone = txtOfficePhone.Text = MiscUtil .RemoveScript (txtOfficePhone.Text);
            txtOfficePhone.Text = MiscUtil.RemoveScript(txtOfficePhone.Text, string.Empty);
            company.OfficePhoneExtension = txtOfficePhoneExtension.Text = MiscUtil .RemoveScript (txtOfficePhoneExtension.Text);
            company.TollFreePhone = txtTollFreePhone.Text = MiscUtil .RemoveScript (txtTollFreePhone.Text);
            txtTollFreePhone.Text = MiscUtil.RemoveScript(txtTollFreePhone.Text, string.Empty);
            company.FaxNumber = txtFaxNumber.Text = MiscUtil .RemoveScript (txtFaxNumber.Text);
            txtFaxNumber.Text = MiscUtil.RemoveScript(txtFaxNumber.Text, string.Empty);
            company.PrimaryEmail = txtPrimaryEmail.Text = MiscUtil .RemoveScript (txtPrimaryEmail.Text);
            txtPrimaryEmail.Text = MiscUtil.RemoveScript(txtPrimaryEmail.Text, string.Empty);
            //company.EINNumber = txtEINNumber.Text = MiscUtil .RemoveScript (txtEINNumber.Text);
            //company.AnnualRevenue = txtAnnualRevenue.Text = MiscUtil .RemoveScript (txtAnnualRevenue.Text);
            //company.ContractStartDate=

            //  int numberOfEmployee = 0;
            // int.TryParse(MiscUtil .RemoveScript (txtNumberOfEmployee.Text.Trim()), out numberOfEmployee);
            // company.NumberOfEmployee = numberOfEmployee;
            if (dvStartDate.Visible)
            {

                company.ContractStartDate = wdcStartDate.Value == "" ? DateTime.MinValue : Convert.ToDateTime(wdcStartDate.Value);
                company.ContractEndDate =wdcEndDate.Value==""?DateTime.MinValue: Convert.ToDateTime(wdcEndDate.Value);
                company.ServiceFee = txtServiceFee.Text==""?0:Convert.ToDecimal(txtServiceFee.Text);

            }
            
           

           /* if (company.ContractStartDate != DateTime.MinValue)
            {
                 wdcStartDate. = company.ContractStartDate;
            }
            else { wdcStartDate.SelectedDate = null; }
            if (company.ContractEndDate != DateTime.MinValue)
            {
                 wdcEndDate.SelectedDate = company.ContractEndDate;
            }
            else
            {
                wdcEndDate.SelectedDate = null;
            }*/

            if (!base.IsExternalRegistration)
            {
                if (CurrentMember != null)     
                {
                    company.CreatorId = CurrentMember.Id;
                    company.UpdatorId = CurrentMember.Id;
                }
            }

            company.IsExternalRegistration = base.IsExternalRegistration;

            return company;
        }

        private void SaveCompany()
        {
            if (IsValid)
            {
                try
                {
                    Company company = BuildCompany();
                    if (company.IsNew)
                    {
                        company = CurrentCompany = Facade.AddCompany(company, RequestedCompanyStatus, IsExternalRegistration);
                        AddCompanyManager(company.Id);
                        bool _IsAccessForCompanyOverview = true;
                        CustomSiteMap CustomMap = new CustomSiteMap();
                        string currentSiteMapId = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
                        int targetsitemap = 0;
                        if (currentSiteMapId == "11")
                        {
                            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(235, CurrentMember.Id);
                            targetsitemap = 11;
                        }
                        else if (currentSiteMapId == "638")
                        {
                            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(642, CurrentMember.Id);
                            targetsitemap = 638;
                        }
                        else
                        {
                            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(626, CurrentMember.Id);
                            targetsitemap = 622;
                        }                      
                        int IdForSitemap = 0;
                        string UrlForCompany = "";
                        if (CustomMap == null) _IsAccessForCompanyOverview = false;
                        else
                        {
                            IdForSitemap = CustomMap.Id;
                            UrlForCompany = "~/" + CustomMap.Url.ToString();
                        } 
                        //  CurrentCompanyId = company.Id;
                        ClearControls();
                        //UploadCompanyLogo();
                        MiscUtil.AddActivityOnObject(MemberType.Company, company.Id, company.CreatorId, ActivityType.AddCompany, Facade);   //0.1

                        if (RequestedCompanyStatus != CompanyStatus.Company)
                        {
                            CompanyStatusChangeRequest request = BuildCompanyStatusChangeRequest();
                            Facade.AddCompanyStatusChangeRequest(request);
                            ActivityType activityType = ActivityType.Unknown;
                            if (base.RequestedCompanyStatus == CompanyStatus.Partner)
                            {
                                activityType = ActivityType.RequestForConvertCompanyToPartner;
                            }
                            else if (base.RequestedCompanyStatus == CompanyStatus.Client)
                            {
                                activityType = ActivityType.RequestForConvertCompanyToClient;
                            }
                            else if (base.RequestedCompanyStatus == CompanyStatus.Vendor)
                            {
                                activityType = ActivityType.RequestForConvertCompanyToVendor;
                            }
                           // MiscUtil.AddActivityOnObject(MemberType.Company, company.Id, company.CreatorId, activityType, Facade);  //0.1
                        }

                        if (OnSaved != null)
                        {
                            OnSaved(company.Id);
                        }
                        string acctype = "";
                        if (CurrentCompanyStatus == CompanyStatus.Client) acctype = "Account";
                        else if (CurrentCompanyStatus == CompanyStatus.Department) acctype = "BU";
                        else acctype = "Vendor";

                        //MiscUtil.ShowMessage(lblMessage, acctype + " has been successfully added.", false);
                        MiscUtil.AddStatusMessageToSession(Session, acctype + " has been successfully added.");
                        if (_IsAccessForCompanyOverview)
                        {
                            SecureUrl url = UrlHelper.BuildSecureUrl(UrlForCompany.Substring(6), string.Empty, UrlConstants.PARAM_COMPANY_ID, company.Id.ToString(), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, targetsitemap.ToString());
                            Response.Redirect(url.ToString());
                           // System .Web .UI. ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "open", "<script language='javaScript'>window.open('" + url + "');</script>", false);
                           // this.Page.ClientScript.RegisterClientScriptBlock (typeof(MiscUtil), "OpenCompany", "<script>var w=window.open('" + url + "');</script>");
                        }
                    }
                    else
                    {
                        CurrentCompany = Facade.UpdateCompany(company);                        
                        //UploadCompanyLogo();
                        string acctype = "";
                        if (CurrentCompanyStatus == CompanyStatus.Client) acctype = "Account";
                        else if (CurrentCompanyStatus == CompanyStatus.Department) acctype = "BU";
                        else acctype = "Vendor";

                        MiscUtil.ShowMessage(lblMessage, acctype + " has been successfully Updated.", false);
                        MiscUtil.AddActivityOnObject(MemberType.Company, company.Id, company.CreatorId, ActivityType.UpdateCompany, Facade);    //0.1
                    }
                }
                catch (ArgumentException ex)
                {     string acctype = "";
                    if (CurrentCompanyStatus == CompanyStatus.Client) acctype = "Account";
                        else if (CurrentCompanyStatus == CompanyStatus.Department) acctype = "BU";
                        else acctype = "Vendor";
                    MiscUtil.ShowMessage(lblMessage, ex.Message.Replace ("Company",acctype ).Replace ("company",acctype .ToLower ()), true);
                }
            }
        }

        //private void UploadCompanyLogo()
        //{
        //    Company company = CurrentCompany;

        //    if (company != null)
        //    {
        //        if ((companyLogo.PostedFile != null) && (companyLogo.PostedFile.ContentLength > 0))
        //        {
        //            if (MiscUtil.IsValidImageFile(companyLogo))
        //            {
        //                string fileName = MiscUtil.ReplaceCharsFromFileName(company.CompanyName, null).Replace(" ", string.Empty) + Path.GetExtension(companyLogo.PostedFile.FileName);

        //                if (!Directory.Exists(UrlConstants.CompanyLogoDirectory))
        //                {
        //                    Directory.CreateDirectory(UrlConstants.CompanyLogoDirectory);
        //                }

        //                string path = Path.Combine(UrlConstants.CompanyLogoDirectory, fileName);

        //                companyLogo.SaveAs(path);

        //                company.CompanyLogo = fileName;
        //                CurrentCompany = Facade.UpdateCompany(company);
        //            }
        //        }
        //    }
        //}

        private void AddCompanyManager(int companyid)
        {
            CompanyAssignedManager companyTeam = new CompanyAssignedManager();
            companyTeam.CompanyId = companyid ;
            companyTeam.MemberId = base.CurrentMember.Id;
            companyTeam.IsPrimaryManager = true;
            companyTeam.CreatorId = base.CurrentMember.Id;
            try
            {
                Facade.AddCompanyAssignedManager(companyTeam);
            }
            catch (ArgumentException ex)
            {
                //MiscUtil.ShowMessage(lblMessage, ex.Message, true);
            }
        }
        private CompanyStatusChangeRequest BuildCompanyStatusChangeRequest()
        {
            CompanyStatusChangeRequest request = new CompanyStatusChangeRequest();

            request.CompanyId = base.CurrentCompanyId;

            if (!CurrentCompany.IsNew)
            {
                request.LastStatus = (int)CurrentCompany.CompanyStatus;
            }

            request.RequestedStatus = (int)base.RequestedCompanyStatus;

            request.Notes = string.Empty;
            request.CheckList.Clear();
            return request;
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {

            //cmpStartDate.ValueToCompare = DateTime.Now.ToShortDateString();
            //cmpEndDate.ValueToCompare = DateTime.Now.ToShortDateString();

            //ddlStateId.Attributes.Add("onChange", "return State_OnChange('" + ddlStateId.ClientID + "','" + hdnSelectedStateId.ClientID + "')");
            //ddlCountryId.Attributes.Add("OnChange", "return Country_OnChange('" + ddlCountryId.ClientID + "','" + ddlStateId.ClientID + "','" + hdnSelectedStateId.ClientID + "')");
            if (!IsPostBack)
            {
                switch (CurrentCompanyStatus)
                {

                    case CompanyStatus.Vendor:
                        dvStartDate.Visible = true;
                        dvendDate.Visible = true;
                        dvservicefee.Visible = true;
                        wdcStartDate.Value = string.Empty;
                        wdcEndDate.Value = string.Empty;
                        break;
                    case CompanyStatus.Department:
                        dvStartDate.Visible = false;
                        dvendDate.Visible = false;
                        dvservicefee.Visible = false;
                        break;
                }
                //cmbstartwithendcontract.Enabled = false;
                PrepareView();

                btnSave.Visible = !IsInformationReadOnly;
                if ( ApplicationSource == ApplicationSource.SelectigenceApplication)
                {                    
                    cmpStartDate.Enabled = false;
                    cmpEndDate.Enabled = false; 
                }
            }
            //code added by pravin khot on 8/Aug/2016***********
            cmpStartDate.Enabled = false;
            cmpStartDate.Visible  = false;
            cmpEndDate.Enabled = false;
            cmpEndDate.Visible = false;
            //*****************END******************************

            if (!IsPostBack)
            {
                if (Request.Url.ToString().ToLower().Contains("createnewclient"))
                {
                    divCompanyHeader.Visible = false;
                }
                Country country = Facade.GetCountryById(Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString()));
                if (country != null)
                {
                    if (country.Name == "India")
                    {
                        lblZip.Text = "PIN Code"; revZIPCode.ErrorMessage = "Please enter a valid PIN code";
                    }
                }
                string errorMsg = "";
                if (CurrentCompanyStatus == CompanyStatus.Client)


                    errorMsg = "Account";

                else if (CurrentCompanyStatus == CompanyStatus.Department)

                    errorMsg = "BU";
                else

                    errorMsg = "Vendor";

                rfvCompanyName.ErrorMessage = "Please enter " + errorMsg + " name.";
            }
            string name = "";
            if (!IsPostBack)
            {
                switch (CurrentCompanyStatus)
                {
                    case CompanyStatus.Client:
                        name = "Account";
                        break;
                    case CompanyStatus.Department:
                        name = "BU";
                        break;
                    case CompanyStatus.Vendor:
                        name = "Vendor";
                        break;
                }
                lblCompanyName.Text = name + " Name";
                lblCompanySize.Text = name + " Size";
                lblEmail.Text = name + " Email";
                lblCompanyInformation.Text = "About the " + name;

            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveCompany();
            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
            txtCompanyName.EnableViewState = true;
            txtCompanyName.Focus();

        }

        #endregion
    }
}