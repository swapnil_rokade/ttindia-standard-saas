﻿/* 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberHotList.ascx.cs
    Description: This is the control which is used to add candidate to hotlist 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Oct-21-2008          Gopala Swamy          Defect id: 9022;Put one codition with AND operator in If Statement
    0.2             Nov-11-2008          Jagadish              Defect id: 9110; Changed the visibility of listview 'lsvCandidateHotList' when 
                                                               'memberGroupList' is null.
    0.3             Jan-22-2009          Jagadish              Defect id: 9787; Assigned text to Lable 'lblCandidateOrConsultant'.
    0.4             Apr-27-2009          Gopala Swamy          Defect id: 10364; Else block has been added and a line is commented
    0.5             July-2-2009          Gopala Swamy          Defect id: 10821; Made some changes to Page_Load() event
-------------------------------------------------------------------------------------------------------------------------------------------       
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;

namespace TPS360.Web.UI
{
    public partial class ControlMemberHotList : BaseControl
    {
        #region Member Variables

        //private static int _memberId = 0;
        MemberGroupMap memberGroup = null;
        //private static string _memberrole = string.Empty;

        public int _memberId
        {
            set { hdnMemberId.Value = value.ToString(); }
            get { return Convert.ToInt32(hdnMemberId.Value == string.Empty ? "0" : hdnMemberId.Value); }
        }
        public string _memberrole
        {
            set { hdnMemberRole.Value = value; }
            get { return hdnMemberRole.Value; }
        }

        #endregion

        #region Properties

        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        int groupType = 0;
        public int GroupType
        {
            set { groupType = value; }
        }

        #endregion

        #region Methods

        private void PrepareView()
        {
            if (memberGroup == null)
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                }
            }
            //if (_memberId > 0)
            //{
            //    lblMemberName.Text = MiscUtil.GetMemberNameById(_memberId, Facade);
            //}
            PopulateDdlHotlist();
            BindListView();
        }

        private void PopulateDdlHotlist()
        {
            MiscUtil.PopulateMemberGroupByMemberId(ddlHotList, Facade, base.CurrentMember.Id, groupType);
        }

        private void BindListView()
        {

            odsHotList.SelectParameters["MemberID"].DefaultValue = _memberId.ToString();
            lsvCandidateHotList.DataBind();
            //IList<MemberGroupMap> memberGroupList = Facade.GetAllMemberGroupMapByMemberId(_memberId);

           // if (memberGroupList != null)
            {
               // lsvCandidateHotList.DataSource = memberGroupList;
               // lsvCandidateHotList.DataBind();
            }
           // else
            {
              //  lsvCandidateHotList.DataSource =string.Empty;
               // lsvCandidateHotList.DataBind();

            }
        }

        #endregion

        #region Events

        #region Page event

        protected void Page_Load(object sender, EventArgs e)
        {
            ddlHotList.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            if (!IsPostBack)
            {
                Response.Buffer = true;
                Response.CacheControl = "no-cache";
                Response.AddHeader("Pragma", "no-cache");
                Response.Expires  = -1;
                PrepareView();
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (ddlHotList.SelectedIndex == 0)
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one Hot List.", false);                
                return;
            }

            MemberGroupMap memberGroupMap = new MemberGroupMap();

            if (memberGroupMap != null && ddlHotList.SelectedIndex!=0)
            {
                memberGroupMap.MemberId = _memberId;
                memberGroupMap.MemberGroupId = Convert.ToInt32(ddlHotList.SelectedValue);
                memberGroupMap.CreatorId = base.CurrentMember.Id;
                memberGroupMap.UpdatorId = base.CurrentMember.Id;

                try
                {
                    if (memberGroupMap.IsNew)
                    {
                        Facade.AddMemberGroupMap(memberGroupMap);
                        MiscUtil.ShowMessage(lblMessage, "Successfully added candidate to Hot List.", false);
                        BindListView();
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }

        #endregion

        #region Listview Event

        protected void lsvCandidateHotList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            string link = string.Empty;
            SecureUrl url=null;

            bool _IsAdmin = base.IsUserAdmin;

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberGroupMap memberGroupMap = ((ListViewDataItem)e.Item).DataItem as MemberGroupMap;

                if (memberGroupMap != null)
                {
                    if (groupType == Convert.ToInt32(MemberGroupType.Candidate))
                    {
                        url = UrlHelper.BuildSecureUrl(UrlConstants.Candidate.CANDIDATE_MANAGEHOTLIST, "", UrlConstants.PARAM_ID, memberGroupMap.MemberGroupId.ToString());
                    }
                    
                    Label lblHotList = (Label)e.Item.FindControl("lblHotList");
                    Label lblDateAdded = (Label)e.Item.FindControl("lblDateAdded");

                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnRemove");

                    MemberGroup mg = Facade.GetMemberGroupById(memberGroupMap.MemberGroupId);
                    if (mg != null)
                    {
                        link = "<a href='" + url + "'>" + mg.Name + "</a>";

                        lblHotList.Text = link;
                    }
                    lblDateAdded.Text = memberGroupMap.CreateDate.ToShortDateString();
                    IList<MemberManager> memManager = new List<MemberManager>();
                    memManager = Facade.GetAllMemberManagerByMemberId(_memberId);
                    System.Web.UI.HtmlControls.HtmlTableCell thhotlist = (System.Web.UI.HtmlControls.HtmlTableCell)lsvCandidateHotList.FindControl("thhotlist");
                    System.Web.UI.HtmlControls.HtmlTableCell tdhotlist = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdhotlist");
                    
                    if (memManager != null)
                    {
                        foreach (MemberManager manager in memManager)
                        {

                            if ((manager.ManagerId == base.CurrentMember.Id) || (base.IsUserAdmin))
                            {
                                btnDelete.Visible = true;
                                thhotlist.Visible = true ;
                                tdhotlist.Visible = true ;
                                break;
                            }
                            else
                            {
                                thhotlist.Visible = false;
                                tdhotlist.Visible = false;
                                btnDelete.Visible = _IsAdmin;
                            }
                        }
                    }
                    else
                        btnDelete.Visible = _IsAdmin;
                    btnDelete.OnClientClick = "return ConfirmDelete('hot list')";
                    btnDelete.CommandArgument = StringHelper.Convert(memberGroupMap.Id);
                }
            }
        }

        protected void lsvCandidateHotList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteMemberGroupMapById(id))
                        {
                            BindListView();
                            MiscUtil.ShowMessage(lblMessage, "Successfully removed candidate from Hot List.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }

        #endregion

        #endregion
    }
}

