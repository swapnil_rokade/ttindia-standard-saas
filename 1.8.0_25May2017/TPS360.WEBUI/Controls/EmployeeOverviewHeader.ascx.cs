﻿/*<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateOverviewHeader.ascx.cs
    Description: This is the user control page used to display the ooption like ToDo,PDF,Print and many more
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date              Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Oct-2-2008        Gopala Swamy J      Defect id: 8914; supllied correct Path to  UrlPdf Variable
    0.2             Oct-20-2008       Gopala Swamy J      Defect id: 8991; changed the From "Hour to salary " to " Yearls" salary 
    0.3             Dec-10-2008       Shivanand           Defect id: 9494; 
                                                                In the method LoadCandidateBasicData(), label "lblSSN" is popuplated 
                                                                from currentCandidateDetail object.
                                                                In the method LoadConsultantBasicData(), label "lblSSN" is popuplated 
                                                                from currentConsultantDetail object.
    0.4             Dec-19-2008       Yogeesh Bhat        Defect Id: 9534: Changes made in LoadConsultantBasicData() method
    0.5             Dec-24-2008       Yogeesh Bhat        Defect Id: 9602: Changes made in Page_Load()method; Made "ResumeShare" link visible false.
    0.6             Jan-13-2009       Jagadish            Defect Id: 9680; Commented the code that use hyperlink 'linkLastActivities'.
    0.7             Jan-15-2009       Jagadish            Defect ID: 9690,9729: Changed the hyperlink 'lnkJobs' to lable.
    0.8             Jan-19-2009       Jagadish            Defect ID: 9682: Made the visibility os the button 'btnSendHRMS' false if login user is not a 'Super admin'
    0.9             Feb-16-2009       Kalyani pallagani   Defect ID: 9885: Added LoadMemberVideo() method to load video resume for both candidate and consultant
    1.0             Mar-03-2009       Jagadish            Defect id: 10038; Changes made in the method 'Page_Load()'.
 *  1.1             Apr-16-2009       Rajendra            Defect id: 10308; Changes made in the Show error Message().
    1.2             Jul-08-2009       Veda                Defect id: 10845: Primary Manager" link can only be accessed by the Superuser and primary manager of that candidate. 
    1.3             Aug-07-2009       Gopala Swamy J      Defect id: 11209: Changed "LoadConsultantBasicData()" and "LoadCandidateBasicData()"
 *  1.4             Nov-19-2009       Sandeesh            Enhancement#11814 :AM Notification - When a recruiter adds a candidate to a req the app will send a notification email to the Account Manager
    1.5             Dec-23-2009       Rajendra            Defect id: 12036: Added code in Page_Load(), btnSaveNotes_Click();
    1.6             apr-16-2010        Nagarathna V.B     enhance:12140: removed "btnSendHRMS" button.
 *  1.7             Apr-22-2010       Ganapati Bhat       Defect id: 12668; Code Added in LoadCandidateBasicData()
 *  1.8             Apr-22-2010       Basavaraj A         Defect 11923 , Added code to btnSaveNotes_Click , to Restrict User who is already added to same requisition
 *  1.9             May-04-2010       Ganapati Bhat       Defect id:12750 ; Added Code in LoadCandidateBasicData to avoid overlapping Firstname & Lastname 
    2.0             10/Jun/2016       Prasanth Kumar G    Introduced UserName and LDAP
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>*/
using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI;
using TPS360.Common.Shared;
using TPS360.Common;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Net.Mail;
using System.Net;
using System.Threading;
using System.Globalization;
using System.Web.Security;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Web;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntity;


namespace TPS360.Web.UI
{
    public partial class EmployeeOverviewHeader: BaseControl
    {
        #region Member Variables

        private  int _memberId = 0;
        #endregion

        #region Property
        #endregion

        #region Methods
        public bool IsRefreshed
        {
            get
            {
                if (Session["REFRESH_CHECK_GUID"] == null
                || ViewState["REFRESH_CHECK_GUID"] == null
                || Session["REFRESH_CHECK_GUID"].ToString().Equals(ViewState["REFRESH_CHECK_GUID"].ToString()))
                {
                    return false;
                }
                else
                    return true;
            }
        }
        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Int32.Parse(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

            }
            //From Member Login
            else
            {
                Member currentCandidate = Facade.GetMemberById(_memberId);
                _memberId = currentCandidate.Id;
            }
            hiddenMemberId.Value = _memberId.ToString();
        }

        
        
        
        private void LoadCandidateBasicData()
        {

            EmployeeOverviewDetails employee = new EmployeeOverviewDetails();
            employee = Facade.GetEmployeeOverviewDetails(_memberId);

            lblCandidateName.ToolTip =lblCandidateName.Text = employee.Name;
            lblMobile.ToolTip=lblMobile.Text = employee.Mobile;
            lblPrimaryEmail.ToolTip=lblPrimaryEmail.Text = employee.EmailId;
            lblLocation.ToolTip=lblLocation.Text = employee.Location;
            lblAccess.ToolTip=lblAccess.Text = employee.AccessRole;
            lblRequisitionCount.ToolTip=lblRequisitionCount.Text = employee.RequisitionCount.ToString();
            lblCandidateCount.ToolTip=lblCandidateCount.Text = employee.AssignedCandidateCount.ToString();
            lblInterviewCount.ToolTip=lblInterviewCount.Text = employee.InterviewCount.ToString();
            lblDocumentsCount.ToolTip=lblDocumentsCount.Text = employee.DocumentsCount.ToString();
            lblUserName.ToolTip = lblUserName.Text = employee.UserName.ToString();  //Line introduced by Prasanth on 10/Jun/2016
            string currentparentid = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID];
            ArrayList AccessArray = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
          
            if (AccessArray.Contains(381))
            {
                SecureUrl url = UrlHelper.BuildSecureUrl("../Employee/InternalNotesAndActivities.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, "381", UrlConstants.PARAM_MEMBER_ID, employee.Id.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentparentid);
                lnkAddNote.Attributes.Add("href", url.ToString());
            }
            else lnkAddNote.Visible = false;
            if (AccessArray.Contains(384))
            {
                SecureUrl url = UrlHelper.BuildSecureUrl("../Employee/InternalDocumentUpload.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, "384", UrlConstants.PARAM_MEMBER_ID, employee.Id.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentparentid);
                lnkAddDocument.Attributes.Add("href", url.ToString());
            }
            else lnkAddDocument.Visible = false;
            if (AccessArray.Contains(380))
            {

                SecureUrl url = UrlHelper.BuildSecureUrl("../Employee/TPS360Access.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, "380", UrlConstants.PARAM_MEMBER_ID, employee.Id.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentparentid);
                lnkEditAccess.Attributes.Add("href", url.ToString());
                lnkAccessRole.Attributes.Add("href", url.ToString());
            }
            else
            {
                lnkEditAccess.Visible = false;
                lnkAccessRole.Enabled = false;

            }


        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            GetMemberId();
            if (!IsPostBack)
            {
                LoadCandidateBasicData();            
            }
        }

        #endregion
    }
}