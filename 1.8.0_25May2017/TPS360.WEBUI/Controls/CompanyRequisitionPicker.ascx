﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompanyRequisitionPicker.ascx.cs"
    Inherits="TPS360.Web.UI.CompanyRequisitionPicker" %>
<script src="../js/CompanyRequisition.js" type ="text/javascript" ></script>
<link href ="../assets/css/chosen.css" rel ="Stylesheet" />

<div style =" padding-bottom :2px;">
  <asp:DropDownList ID="ddlClientList" runat="server" 
                                    onmouseover="ShowDiv('divReqs')" Width="150px"
                                    CssClass="chzn-select">
                                </asp:DropDownList>
                                </div>
                                <div style =" float :left ; padding-right : 5px;" >
                                <asp:DropDownList ID="ddlRequisition" EnableViewState="true" runat="server" CssClass="chzn-select"
                                    onmouseover="ShowDiv('divReqs')" Width="150px"
                                    onchange="DropdownChange('divReqs')">
                                </asp:DropDownList>
                                  <asp:CustomValidator ID="cvRequisition" runat="server" ClientValidationFunction="ValidateReqControl"
                                    Display="Dynamic" ErrorMessage="Please select a requisition." ValidationGroup="RequisitionList"
                                    Font-Bold="false"></asp:CustomValidator>
                                </div>
                              <asp:HiddenField ID="hdnSelectedRequisitionID" runat ="server" Value ="0" />
                               
                              

<script src="../assets/js/chosen.jquery.js" type ="text/javascript" ></script>
   <script type="text/javascript"> 
       function ValidateReqControl(source, args) {
        validator = document.getElementById(source.id);
        hdnSelectedRequisition = document.getElementById("<%=hdnSelectedRequisitionID.ClientID %>");
        ddlRequisition = document.getElementById("<%=ddlRequisition.ClientID %>");
        if (hdnSelectedRequisition.value == '' || hdnSelectedRequisition.value == '0' || ddlRequisition.options[ddlRequisition.selectedIndex].value == "0") 
        {
        
            validator.innerHTML = "<br/>Please select Requisition.";
            ShowDiv('divReqs'); n = 0;
            args.IsValid = false;
        }

    }
    
   Sys.Application.add_load(function() { 
   $(".chzn-select").chosen();
  $(".chzn-select-deselect").chosen({allow_single_deselect:true});
  });
    </script>

