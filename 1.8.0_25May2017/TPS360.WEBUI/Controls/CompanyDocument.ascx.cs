/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CompanyDocument.ascx.cs
    Description: This is the user control page used to upload company document.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Sep-15-2008          Shivanand           Defect id: 8680: In the method SaveCompanyDocument() the code to display message "Your document has been submited" is removed.
                                                             In the method SaveFiles() if block is added to compare CurrentCompanyId and to check if the base.CurrentMember is null.
                                                             In the method Page_Load() text on label "lblMessage" is cleared.
   0.2              Oct-01-2008          Jagadish            Defect id: 8748: Alert message was repeating, corrected that issue.
   0.3              20Jan2017           Prasanth Kumar G    id : 1153 checking extensions
   0.4              17Feb2017           Prasanth Kumar G    introduced function Check_FileMIMEType issue id 1153
-------------------------------------------------------------------------------------------------------------------------------------------       

*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Common;
using TPSTool.Parser;
using ParserLibrary;
using System.Web;
using System.Text;
using System.Net;

namespace TPS360.Web.UI
{
    public partial class cltCompanyDocument : CompanyBaseControl
    {
        #region Member Variables
        private static int _CompanyId = 0;
        #endregion

        #region Properties
        public MemberDocumentType DocumentType
        {
            set
            {
                ViewState["DocumentTypeId"] = value;
                hfDocumentType.Value = EnumHelper.GetDescription(value);
            }
            get { return (MemberDocumentType)ViewState["DocumentTypeId"]; }
        }

        #endregion

        #region Methods

        private void LoadQueryStringData()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID ]))
            {
                _CompanyId  = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]);
            }
            else
            {
                _CompanyId  = CurrentCompanyId;
            }
            if (_CompanyId  > 0)
            {
                hfMemberId.Value = _CompanyId.ToString();
            }
        }

        private void LoadUploadedDocument()
        {
            if (_CompanyId > 0)
            {
                    IList<CompanyDocument> lstAddedPosition = Facade.GetAllCompanyDocumentByCompanyId(_CompanyId);
                    lsvCompanyDocument.DataSource = lstAddedPosition;
                    lsvCompanyDocument.DataBind();
            }
        }
        private void LoadInitialData()
        {
            MiscUtil.PopulateDocumentType(ddlDocumentType, Facade);
            if (!string.IsNullOrEmpty(hfDocumentType.Value))
            {
                ddlDocumentType.SelectedValue = ((int)((MemberDocumentType)ViewState["DocumentTypeId"])).ToString();
                EnableDisableValidator(hfDocumentType.Value);
                rowDocumentType.Visible = false;
                lblDocumentTitle.Text = hfDocumentType.Value + " Title";
                lblDocumentPath.Text = hfDocumentType.Value + " Path";

                revPhoto.ValidationGroup = hfDocumentType.Value;
                revVideo.ValidationGroup = hfDocumentType.Value;
                revDocument.ValidationGroup = hfDocumentType.Value;
                rfvDocumentUpload.ValidationGroup = hfDocumentType.Value;
                rfvDocumentTitle.ValidationGroup = hfDocumentType.Value;
                btnUpload.ValidationGroup = hfDocumentType.Value;
            }
            else
            {
                rowDocumentType.Visible = true;
            }
            //LoadUploadedDocument();
        }

        private void EnableDisableValidator(string strDocumentType)
        {
            if (strDocumentType.ToLower().IndexOf("photo") >= 0)
            {
                revPhoto.Visible = true;
                revVideo.Visible = false;
                revDocument.Visible = false;
            }
            else if (strDocumentType.ToLower().IndexOf("video") >= 0)
            {
                revPhoto.Visible = false;
                revVideo.Visible = true;
                revDocument.Visible = false;
            }
            else
            {
                revPhoto.Visible = false;
                revVideo.Visible = false;
                revDocument.Visible = true;
            }
        }


        private void UploadDocument()
        {
            if (fuDocument.HasFile)
            {
                string strMessage = String.Empty;
                string _fileName = string.Empty;
                lblMessage.Text = string.Empty;
                bool boolError = false;
                string UploadedFilename = Convert.ToString(fuDocument.FileName);
                //Code introduced by Prasanth on 20Jan2017 Start id 1153
                string extension = Path.GetExtension(fuDocument.FileName);
                string ContentType = fuDocument.PostedFile.ContentType;
                //Code introduced by Prasanth on 17Feb2017 Start id 1153
                if (MiscUtil.Check_FileMIMEType(fuDocument) == false)
                {
                    strMessage = "Such File not allowed to upload";
                }
                 else
                {
                if (ddlDocumentType.SelectedItem.Text == "Word Resume")
                {
                    string[] FileName_Split = UploadedFilename.Split('.');
                    string ResumeName = CurrentCompany .CompanyName  + " - Resume." + FileName_Split[FileName_Split.Length - 1];
                    UploadedFilename = ResumeName;
                }
                string strFilePath = MiscUtil.GetCompanyDocumentPath (this.Page, _CompanyId , UploadedFilename, ddlDocumentType.SelectedItem.Text, false);//1.6
                if (CheckFileSize())
                {
                    fuDocument.SaveAs(strFilePath);

                    if (File.Exists(strFilePath))
                    {
                        CompanyDocument  companyDocument =Facade .GetCompanyDocumentByCompanyIdTypeAndFileName (_CompanyId ,ddlDocumentType.SelectedItem.Text.Trim(), UploadedFilename);//Facade.GetMemberDocumentByMemberIdAndFileName(_memberId, UploadedFilename);//1.6
                        if (companyDocument == null)
                        {
                            CompanyDocument newDoc = new CompanyDocument();
                            newDoc.FileName = UploadedFilename;
                            newDoc.DocumentType  = Int32.Parse(ddlDocumentType.SelectedValue);
                            newDoc.Description = MiscUtil.RemoveScript(txtDocumentDescription.Text);
                            newDoc.CompanyId = _CompanyId;
                            newDoc.CreatorId = CurrentMember.Id;
                            newDoc.Title = MiscUtil.RemoveScript(txtDocumentTitle.Text);
                            if (newDoc.Title.ToString() != string.Empty)
                            {
                                Facade.AddCompanyDocument(newDoc);
                                if (string.IsNullOrEmpty(strMessage))
                                {
                                    strMessage = "Successfully uploaded the file";
                                }
                            }
                        }
                        else
                        {
                            boolError = true;
                            strMessage = "Already this document available";
                        }
                    }
                }
                else
                {
                    boolError = true;
                    strMessage = "File size should be less than 10 MB";
                }
                //Code commented by Prasanth on 02Feb2017
                    //if (MiscUtil.RemoveScript(txtDocumentTitle.Text.Trim().ToString()) != string.Empty)
                    //    MiscUtil.ShowMessage(lblMessage, strMessage + lblMessage.Text, boolError);
            }
                 //Code introduced by Prasanth On 02Feb2017 start
                if (MiscUtil.RemoveScript(txtDocumentTitle.Text.Trim().ToString()) != string.Empty)
                    MiscUtil.ShowMessage(lblMessage, strMessage + lblMessage.Text, boolError);
                //**********************END*********************
        }

        }

        private void UpdateDocument()
        {
            CompanyDocument  newDoc = new CompanyDocument ();
            newDoc.Description =MiscUtil .RemoveScript ( txtDocumentDescription.Text);
            newDoc.Title =MiscUtil .RemoveScript ( txtDocumentTitle.Text);
            newDoc.FileName = hfMemberDocumentName.Value;
            newDoc.DocumentType  = Int32.Parse(ddlDocumentType.SelectedValue);
            newDoc.CompanyId  = _CompanyId ;
            newDoc.Id = Int32.Parse(hfMemberDocumentId.Value);
            Facade.UpdateCompanyDocument(newDoc);
            MiscUtil.ShowMessage(lblMessage, "Successfully updated the file", false);
        }

        private bool CheckFileSize()
        {
            int fileSize = Convert.ToInt32(fuDocument.FileContent.Length / (1024 * 1024));
            if (fileSize > ContextConstants.MAXIMUM_UPLOAD_SIZE)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private string GetDocumentLink(string strFileName, string strDocumenType)
        {

            string strFilePath = MiscUtil.GetCompanyDocumentPath (this.Page, _CompanyId , strFileName, strDocumenType, true);

            if (strFilePath != strFileName)
            {
                return "<a href='" + strFilePath + "' target='_blank'>" + strFileName + "</a>"; // 0.1
            }
            else
            {
                return strFileName + "(File not found)";
            }
        }

        private bool DeleteUploadedDocement(int intId, string strFileName, string strDocumentType)
        {
            if (Facade.DeleteCompanyDocumentById (intId ))
            {
                string strPath = MiscUtil.GetCompanyDocumentPath (this.Page, _CompanyId , strFileName, strDocumentType, false);
                File.Delete(strPath);
                return true;
            }
            else
            {
                return false;
            }
        }



        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadQueryStringData();
            _CompanyId  = Int32.Parse(hfMemberId.Value);
            if (!IsPostBack)
            {
                odsDoc.SelectParameters["companyId"].DefaultValue = _CompanyId.ToString();
              
                LoadInitialData();
            }
            lblMessage.Text = "";
            lblMessage.CssClass = "";
        }

        protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableDisableValidator(ddlDocumentType.SelectedItem.Text);
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string strMessage = string.Empty;  

            if (hfMemberDocumentId.Value == String.Empty)
            {
                UploadDocument();
                strMessage = "Successfully uploaded the file."; 
            }
            else
            {
                ddlDocumentType.Enabled = true;
                fuDocument.Enabled = true;
                rfvDocumentUpload.Enabled = true;
                cvDocumentType.Enabled = true;
                if (btnUpload.Text == "Update")
                {
                    UpdateDocument();
                }
            }
            Refresh(ddlDocumentType.SelectedItem.Text.ToLower(), strMessage); 
            if (ddlDocumentType.Visible) 
                ddlDocumentType.SelectedValue = "0";
           // LoadUploadedDocument();
            lsvCompanyDocument.DataBind();
            txtDocumentDescription.Text = String.Empty;
            txtDocumentTitle.Text = String.Empty;
            hfMemberDocumentId.Value = String.Empty;
        }
        protected void lsvDocuments_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCompanyDocument .FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) TPS360.Web.UI .Helper . ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
             
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null)
                {
                     hdnRowPerPageName.Value = "CompanyDocumentListRowPerPage";
                }
            }
        }
        protected  void lsvCompanyDocument_OnItemCommand(object sender, ListViewCommandEventArgs e) 
        {
              if (StringHelper.IsEqual(e.CommandName, "DeleteItem"))
              {
                  string[] strKeyWord = e.CommandArgument.ToString().Split(':');
                  if (strKeyWord.Length == 3 && DeleteUploadedDocement(Int32.Parse(strKeyWord[0]), strKeyWord[1], strKeyWord[2]))
                  {
                      MiscUtil.ShowMessage(lblMessage, "Successfully deleted the file.", false);
                      //LoadUploadedDocument();
                      lsvCompanyDocument.DataBind();
                      Refresh(strKeyWord[2].ToLower(), "Successfully deleted the file.");
                  }
                  else
                  {
                      MiscUtil.ShowMessage(lblMessage, "Could not delete the file.", true);
                  }
                  if (ddlDocumentType.Enabled)
                      ddlDocumentType.SelectedValue = "0";
                  int intId = Convert.ToInt32(strKeyWord[0].ToString());
                  if (string.Compare(intId.ToString(), hfMemberDocumentId.Value) == 0)
                  {

                      ddlDocumentType.Enabled = true;
                      fuDocument.Enabled = true;
                      rfvDocumentUpload.Enabled = true;
                      cvDocumentType.Enabled = true;
                      txtDocumentDescription.Text = "";
                      txtDocumentTitle.Text = "";
                      hfMemberDocumentId.Value = string.Empty;
                      ddlDocumentType.SelectedIndex = 0;
                  }
              }
              else if (StringHelper.IsEqual(e.CommandName, "EditItem"))
              {
                  int intId = Convert.ToInt32(e.CommandArgument);
                  CompanyDocument curDoc = Facade.GetCompanyDocumentById(intId);
                  txtDocumentDescription.Text = MiscUtil.RemoveScript(curDoc.Description, string.Empty);
                  txtDocumentTitle.Text = MiscUtil.RemoveScript(curDoc.Title, string.Empty);
                  ddlDocumentType.SelectedValue = curDoc.DocumentType.ToString();
                  hfMemberDocumentId.Value = intId.ToString();
                  hfMemberDocumentName.Value = curDoc.FileName;
                  ddlDocumentType.Enabled = false;
                  fuDocument.Enabled = false;
                  rfvDocumentUpload.Enabled = false;
                  cvDocumentType.Enabled = false;
                  btnUpload.Text = "Update";
              }
        
        }

        protected void lsvDocuments_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (TPS360.Web.UI.Helper.ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                CompanyDocument companyDocument = ((ListViewDataItem )e.Item ).DataItem as CompanyDocument;
                if (companyDocument != null)
                {
                    GenericLookup docType = Facade.GetGenericLookupById(companyDocument.DocumentType);
                    Label lblDocumentLink = e.Item.FindControl("lblDocumentLink") as Label;
                    Label lblDocumentType = e.Item.FindControl("lblDocumentType") as Label;
                    Label lblDocumentTitle = e.Item.FindControl("lblComDocumentTitle") as Label;
                    if (docType != null)
                    {
                        lblDocumentLink.Text = GetDocumentLink(companyDocument.FileName, docType.Name);
                        lblDocumentType.Text = docType.Name;
                        lblDocumentTitle.Text = companyDocument.Title;
                        ((ImageButton)e.Item.FindControl("btnDelete")).CommandArgument = companyDocument.Id.ToString() + ":" + companyDocument.FileName + ":" + docType.Name;// DefectID #11426
                        ((ImageButton)e.Item.FindControl("btnDelete")).OnClientClick = "return ConfirmDelete('uploaded file')"; //0.5
                        ((ImageButton)e.Item.FindControl("btnEdit")).CommandArgument = companyDocument.Id.ToString();
                    }
                }
            }
        }


        private void Refresh(string strSelected, string strMessage)
        {
            if (base.CurrentUserRole == ContextConstants.ROLE_CANDIDATE || base.CurrentUserRole == ContextConstants.ROLE_CONSULTANT)
            {

                if (strSelected.IndexOf("photo") >= 0 || strSelected.IndexOf("video") >= 0)
                {
                    string strurl = Request.Url.ToString();
                    if (!StringHelper.IsBlank(strurl) && strurl.IndexOf('?') > 0)
                    {
                        int intTab = 0;
                        if (revPhoto.Visible)
                            intTab = 2;
                        else if (revVideo.Visible)
                            intTab = 1;

                        Helper.Session.Set("AlreadyMemberDocumentLoaded", strMessage + "+" + intTab.ToString());
                        string[] urlAndQueryString = strurl.Split('?');
                        string _pageUrl = urlAndQueryString[0];
                        Helper.Url.Redirect(_pageUrl, string.Empty, UrlConstants.PARAM_COMPANY_ID , Convert.ToString(_CompanyId ));
                    }
                }
            }
        }

        #endregion
    }
}