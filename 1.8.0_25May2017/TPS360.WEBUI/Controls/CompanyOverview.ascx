﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:CompanyOverview.ascx
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
   0.1             May-13-2009          Gopala Swamy         Defect id:10442 ; Replaced Hyperlink with Label control
   0.2             May-15-2009          Sandeesh             Defect id:10440 : Changes made to get the Requisition status from database instead of reading from the  Enum 
   0.3             April-02-2010        Basavaraj Angadi     Defect Id:12483 , Changed from comapny to company and Fill to File
   0.4             Apr-19-2010          Ganapati Bhat        Defect id:12630;  Added Code in lsvCompanyInformation to display Message as "No Company Description found" 
   0.5             Apr-20-2010          Sudarshan.R.         Defect id:12484;  Changed SortCommand for city, from City to [J].[City] in lsvJobPosting Listview 
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompanyOverview.ascx.cs"
    Inherits="TPS360.Web.UI.cltCompanyOverview" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<link href="../Style/TabStyle.css" type="text/css" rel="Stylesheet" />
<style type="text/css">
    .TableFormLeble
    {
        font-weight: bold;
    }
</style>

<script type="text/javascript">
  function pageLoad()
 {
  var background = $find("MPE")._backgroundElement;
            background.onclick = function() {  $find("MPE").hide();}
            
 }

</script>

<div class="tabbable">
    <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1" data-toggle="tab" id="tabone">Profile</a></li>
        <li id="liRequisition" runat ="server" ><a href="#tab2" data-toggle="tab" id="tabtwo">Requisitions</a></li>
        <li><a href="#tab4" data-toggle="tab" id="tabfour">Contacts</a></li>
        <li><a href="#tab5" data-toggle="tab" id="tabfive">Documents</a></li>
    </ul>
    <div class="tab-content">
    
        <div class="tab-pane active" id="tab1">
            <asp:UpdatePanel ID="upProfile" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblCompanyProfile" runat="server" EnableViewState="false"></asp:Label>
                    
                    <div class="FormLeftColumn">
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblCompanyNameLabel" runat="server" Text="Company Name" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent" style="margin-left: 43%;">
                                <asp:Label ID="lblCompanyName" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblAddress1Label" runat="server" Text="Address 1" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblAddress1" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblAddress2Label" runat="server" Text="Address 2" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblAddress2" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblCityLabel" runat="server" Text="City" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblCity" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblStateLabel" runat="server" Text="State" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblStateId" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblCountryLabel" runat="server" Text="Country" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblCountryId" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblZipLabel" runat="server" Text="Zip/Postal Code" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblZip" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblIndustryTypeLabel" runat="server" Text="Industry Type" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblIndustryType" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow"  id ="divOwnerType" runat ="server" >
                            <div class="TableFormLeble">
                                <asp:Label ID="lblOwnerTypeLabel" runat="server" Text="Owner Type" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblOwnerType" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="FormRightColumn">
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblOfficeNumberLabel" runat="server" Text="Office Number" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblOfficeNumber" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblTollFreeNumberLabel" runat="server" Text="Toll Free Number" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblTollFreeNumber" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblFaxNumberLabel" runat="server" Text="Fax Number" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblFaxNumber" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblWebSiteURLLabel" runat="server" Text="Web Site URL" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblWebSiteURL" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblCompanyEmailLabel" runat="server" Text="Company Email" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblCompanyEmail" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow" runat="server" id="divEINNUmber">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblEINNumberLabel" runat="server" Text="EIN Number" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblEINNumber" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label ID="lblCompanySizeLabel" runat="server" Text="Company Size" EnableViewState="true"></asp:Label>:
                            </div>
                            <div class="TableFormContent">
                                <asp:Label ID="lblCompanySize" runat="server" EnableViewState="true"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class ="TableRow">
                    
                    <div class="TableFormLeble" style =" width : 21%;"> <asp:Label ID ="lblDescription" runat ="server" Text ="About the Account"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:Panel ID="pnlAboutCompanyContent" runat="server" Style="overflow: hidden;">
                    
                        <div class="TableFormContent" style="text-align: left">
                        
                            <asp:Label ID="lblCompanyInformation" runat="server" EnableViewState="true"></asp:Label>
                            <div class="GridContainer">
                                <asp:ListView ID="lsvCompanyInformation" runat="server" DataKeyNames="">
                                    <LayoutTemplate>
                                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                            <tr>
                                                <td>
                                                    No Description.
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                        </div>
                    </asp:Panel>
                    </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="tab-pane" id="tab2" >
            <asp:UpdatePanel ID="upRequisions" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlJobsContent" runat="server" Style="overflow: hidden;">
                        <asp:ObjectDataSource ID="odsJobPostingList" runat="server" SelectMethod="GetPaged"
                            TypeName="TPS360.Web.UI.JobPostingDataSource" SelectCountMethod="GetListCount"
                            OnSelecting="odsJobPostingList_Selecting" EnablePaging="True" SortParameterName="sortExpression"
                            OnInit="odsJobPostingList_Init">
                            <SelectParameters>
                                <asp:Parameter Name="companyId" DefaultValue="0" Type="Int32" Direction="Input" />
                                <asp:Parameter Name="IsTemplate" DefaultValue="False" Type="Boolean" Direction="Input" />
                                <asp:Parameter Name="JobStatus" DefaultValue="0" Type="Int32" Direction="Input" />
                                <asp:Parameter Name="IsJobActive" DefaultValue="true" Type="Boolean" Direction="Input" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <div class="TableFormContent" style="text-align: left">
                            <asp:Label ID="lblJobs" runat="server" EnableViewState="false"></asp:Label>
                            <div class="GridContainer" style="overflow: hidden;">
                                <asp:ListView ID="lsvJobPosting" runat="server" DataKeyNames="Id" OnItemDataBound="lsvJobPosting_ItemDataBound"
                                    OnPreRender="lsvJobPosting_PreRender">
                                    <LayoutTemplate>
                                          <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                            <tr>
                                                <th style="width: 5%; white-space: nowrap;">
                                                    Date Published
                                                </th>
                                                <th style="width: 10%; white-space: nowrap;">
                                                    Job Title
                                                </th>
                                                <th style="width: 8%; white-space: nowrap;">
                                                    Location
                                                </th>
                                            </tr>
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>
                                            <tr class="Pager">
                                                <td colspan="3">
                                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                            <tr>
                                                <td>
                                                    No Requisitions.
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                            <td style="text-align: left;">
                                                <asp:Label ID="lblPostedDate" runat="server" />
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:HyperLink ID="lnkJobTitle" runat="server" Target="_blank"></asp:HyperLink>
                                                <asp:Label ID="lblJobTitle" runat="server" />
                                            </td>
                                            <td style="text-align: left;">
                                                <asp:Label ID="lblCity" runat="server" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
       
        <div class="tab-pane" id="tab4">
        
        
            <asp:UpdatePanel ID="upContacts" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlCompanyContactContent" runat="server" Style="overflow: hidden;">
                        <div class="TableFormContent" style="text-align: left">
                            <asp:Label ID="lblCompanyContact" runat="server" EnableViewState="false"></asp:Label>
                            <div class="GridContainer">
                                <asp:ListView ID="lsvCompanyContact" runat="server" DataKeyNames="Id" OnItemDataBound="lsvCompanyContact_ItemDataBound"
                                    OnItemCommand="lsvCompanyContact_ItemCommand">
                                    <LayoutTemplate>
                                     <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                            <tr>
                                                <th >
                                                    Name
                                                </th>
                                                <th >
                                                    Title
                                                </th>
                                                <th >
                                                    Email
                                                </th>
                                                <th >
                                                    Office Phone
                                                </th>
                                                <th >
                                                    Mobile
                                                </th>
                                                <th >
                                                    City
                                                </th>
                                            </tr>
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                            <tr>
                                                <td>
                                                    No Contacts.
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                            <td align="left">
                                                <%-- <asp:Label ID="lblName" runat="server" EnableViewState="false" />--%>
                                                <asp:LinkButton ID="lblName" runat="server" CommandName="OpenModal"></asp:LinkButton>
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblTitle" runat="server" />
                                            </td>
                                            <td align="left">
                                                <asp:LinkButton ID="lnkEmail" runat="server"></asp:LinkButton>
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblDirectNumber" runat="server" />
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblMobile" runat="server" />
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblCity" runat="server" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="tab-pane" id="tab5">
            <asp:UpdatePanel ID="upDocuments" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblDocument" runat="server" EnableViewState="false"></asp:Label>
                    <div class="GridContainer">
                        <asp:ListView ID="lsvFileName" runat="server" DataKeyNames="Id" OnItemDataBound="lsvFillName_ItemDataBound">
                            <LayoutTemplate>
                                   <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                    <tr>
                                        <th>
                                            Document Title
                                        </th>
                                        <th>
                                            <%--<asp:LinkButton ID="btnFillName" CausesValidation="false" runat="server" ToolTip="Sort By Title"
                                        CommandArgument="FileName" Text="File Name" />--%>
                                            File Name
                                        </th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <EmptyDataTemplate>
                                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                    <tr>
                                        <td>
                                            No Documents.
                                        </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                    <td style="text-align: left; width: 25%">
                                        <asp:Label ID="lblDocumentTitle" runat="server"></asp:Label>
                                    </td>
                                    <td style="text-align: left;">
                                        <asp:Label ID="lblFileName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
