﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: AllEmailList.ascx.cs
    Description: This is the user control page used for sending e-mails
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sep-26-2008           Yogeesh Bhat        Defect ID: 8706; Changes made in Page_Load() method.
                                                             (Checked for Session["ReferenceLink"] if it is from referenceLink, 
                                                              updated _tomemberId to reference ID from session)
    0.2            Sep-30-2008           Yogeesh Bhat        Defect ID: 8856; Changes made in Page_Load(), btnReply_Click(), and
                                                             btnForward_Click() methods (Checked for listview items count)
    0.3            Oct-17-2008           Jagadish            Defect ID: 8969; Changed the parameter "Reply" to "Forward" in btnForward_Click() method.
    0.4            Nov-26-2008           Jagadish            Defect id: 8813; When a 'Delete icon' is clicked at 'Action' column header then multiple 'Radio buttons' were selected
                                                             Changed the code to fix that.
    0.5            Dec-05-2008           Jagadish            Defect id: 9358; changed case of variable id's from 'ReceiverId' and
                                                             'SenderId' to 'receiverId' and 'senderId' and '_tomemberId' to '_frommemberId'.
    0.6            Jan-02-2008           Jagadish            Defect id: 9518; changes made in btnForward_Click() method and btnReply_Click() method.
 *  0.7            28/March/2016         pravin khot        condition added -if (ddlCountry.SelectedValue == 0)
    0.8            29/March/2016         pravin khot         set - DefaultSiteSetting.Country
 *  0.9            7/April/2016          pravin khot         added code for country defoult setting- SiteSetting siteSetting = Facade.GetSiteSettingBySettingType
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Linq;
using System.Web.UI;
using System.Collections;//ADDED BY PRAVIN ON 7/APRIL/2016
namespace TPS360.Web.UI
{
   
    
    public partial class CountryStatePicker : BaseControl
    {
        public short CountryTabIndex
        {
            set
            {
                ddlCountry.TabIndex = value;
            }
        }
        public short StateTabIndex
        {
            set
            {
                ddlState.TabIndex = value;
            }
        }
        public bool IsCountryRequired
        {
            set
            {
                spanCountryRequired.Visible = value;
            }
        }
        private bool _Enabled = true;
        public bool Enabled
        {
            set
            {
                _Enabled = value;
                ddlCountry.Enabled = value;
                ddlState.Enabled = value;
            }
        }

        private bool _ApplyDefaultSiteSetting=false ;
        public bool ApplyDefaultSiteSetting
        {
            set
            {
                _ApplyDefaultSiteSetting = value;
            }
            get
            {
                return _ApplyDefaultSiteSetting;
            }
           
        }

        public string TableFormLabel_Width
        {
            set
            {
                divStateLable.Style.Add("Width", value);
                divCountryLabel.Style.Add("Width", value);
            }
        }


        private string _FirstOption;
        public string FirstOption
        {
            set
            {
                _FirstOption = value;

            }
            get { return _FirstOption; }
        }

        public string SelectedCountryName
        {
            get
            {
                return  ddlCountry .SelectedIndex ==0?"": ddlCountry.SelectedItem.Text;
            }
            set
            {
                ControlHelper.SelectListByText(ddlCountry, value);
            }
        }
               
        public int SelectedStateId
        {
            get
            {
                return Convert.ToInt32(hdnSelectedState.Value);
            }
            set
            {
                hdnSelectedState.Value = value.ToString();
                ControlHelper .SelectListByValue (ddlState ,value .ToString ());
            }
        }

        public int SelectedCountryId
        {
            get
            {
                FillControls();
                return Convert.ToInt32(ddlCountry.SelectedValue);
            }
            set
            {
                FillControls();
                if (value == 0)
                {
                    if (ApplyDefaultSiteSetting) GetDefaultsFromSiteSetting();
                    else ControlHelper.SelectListByValue(ddlCountry, value.ToString());


                }
                else ControlHelper.SelectListByValue(ddlCountry, value.ToString());
                
                MiscUtil.PopulateState(ddlState, Convert.ToInt32(ddlCountry.SelectedValue), Facade);
                ddlState.Items[0].Text = FirstOption;

            }
            
        }
        public string ShowAndHideState
        {
            
            set 
            {
                if (value.ToLower () == "false") 
                {
                    divState.Attributes.CssStyle.Add("display", "none");
                }
            
            }

        }

        protected void Page_Load(object sender,EventArgs e)
        {

            
            ddlCountry.Attributes.Add("onChange", "Country_OnChange('" + ddlCountry.ClientID + "','" + ddlState.ClientID + "','" + hdnSelectedState.ClientID + "');");
            ddlState.Attributes.Add("onChange", "State_OnChange('" + ddlState.ClientID + "','" + hdnSelectedState.ClientID + "')");

            FillControls();
            
        }
        
        public void  FillControls()
        {
            if (ddlCountry.Items.Count == 0)
            {
                if (!IsPostBack)
                {
                    MiscUtil.PopulateCountry(ddlCountry, Facade);
                    ListItem item = ddlCountry.Items .FindByText ("India");
                    ddlCountry.Items.Remove(item);
                    ddlCountry.Items.Insert (1, item);

                    item = ddlCountry.Items.FindByText("United States");
                    ddlCountry.Items.Remove(item);
                    ddlCountry.Items.Insert (2, item);


                    //code added by pravin khot on 28/March/2016***********
                    if (Request.Url.ToString().ToLower().Contains("candidateregistration.aspx"))
                    {
                        //***********Code commented by pravin khot on 29/March/2016*******
                        //if (Convert.ToInt32(ddlCountry.SelectedValue) == 0)
                        //{
                        //    ddlCountry.SelectedValue = "1";
                        //}
                        //******************END*********************************
                        //code added by pravin khot on 29/March/2016***********COMMENT BY PRAVIN ON 7/April/2016
                        //if (SiteSetting != null)
                        //{
                        //    SelectedCountryId = Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString());
                        //}
                        //ddlCountry.SelectedValue = SelectedCountryId.ToString();
                        //******************END*********************************

                        //********************Code added by pravin khot on 7/April/2016***************
                        SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                        if (siteSetting != null)
                        {
                            Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                            ddlCountry.SelectedValue = siteSettingTable[DefaultSiteSetting.Country.ToString()].ToString();
                        }
                        else
                        {
                            ddlCountry.SelectedValue = "2";
                        }
                        MiscUtil.PopulateState(ddlState, Convert.ToInt32(ddlCountry.SelectedValue), Facade);
                        ddlCountry.Items[0].Text = FirstOption;
                        ddlState.Items[0].Text = FirstOption;

                        //****************************END************************************************
                    }
                    //**********************END****************************
                    else
                    {

                        if (ApplyDefaultSiteSetting) GetDefaultsFromSiteSetting();
                        MiscUtil.PopulateState(ddlState, Convert.ToInt32(ddlCountry.SelectedValue), Facade);
                        ddlCountry.Items[0].Text = FirstOption;
                        ddlState.Items[0].Text = FirstOption;
                    }
                }
            }
        }
        private void GetDefaultsFromSiteSetting()
        {

            if (SiteSetting != null)
            {
                SelectedCountryId = Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString());
            }
            else
            {
                SelectedCountryName = "United States";
            }
        }
     

        
    }
    
}