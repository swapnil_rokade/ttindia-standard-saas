﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReferrerInfo.ascx.cs"
    Inherits="TPS360.Web.UI.ReferrerInfo" EnableViewState="true" %>
<link href="../Style/Common.css" type="text/css" rel="Stylesheet" />
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>

<script>
function hilightRow()
{
 $('.row,.altrow').each(function (){
            $(this).css({'cursor':'pointer'});
             $(this).removeClass('SelectedRow');
             $(this).find('input[type=image]:first').attr('src','../Images/Expand-plus.png');
          });
          
          
            $('.row,.altrow').each(function (){
             if($('#<%= hdnSelectedJobPostings.ClientID %>').val()==$(this).find('input[id$=hdnJobtitle]:first').val())
             {
                   $(this).find('input[type=image]:first').attr('src','../Images/Collapse-minus.png');
                    $(this).addClass('SelectedRow');
             }
    
  });
}
Sys.Application.add_load(function() { 

hilightRow ();
    $('.row,.altrow').click(function (){
    
     $('.row,.altrow').each(function (){
            $(this).css({'cursor':'pointer'});
             $(this).removeClass('SelectedRow');
             $(this).find('input[type=image]:first').attr('src','../Images/Expand-plus.png');
          });
          
          $('#<%= hdnSelectedJobPostings.ClientID %>').val($(this).find('input[id$=hdnJobtitle]:first').val());
          $(this).find('input[type=image]:first').attr('src','../Images/Collapse-minus.png');
          $(this).addClass('SelectedRow');
          
     
    });
    });
</script>

<style>
    .imageSelect
    {
        cursor: default;
    }
    .SelectedRow
    {
        background-color: orange !important;
        border-radius: 5px;
    }
</style>
<div>
    <asp:HiddenField ID="hdnSelectedJobPostings" runat="server" />
    <div class="TabPanelHeader">
        Referrer Info <span class="TabPanelHeadersmall">Fill in your name and email.</span></div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblFirstName" runat="server" Text="First Name"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox EnableViewState="false" ID="txtFirstName" runat="server" Width="200"
                CssClass="CommonTextBox" OnKeyPress="return alphanumeric_only(this)"> 
            </asp:TextBox>
            <span class="RequiredField">*</span>
        </div>
        <div class="TableFormValidatorContent" style="margin-left: 42%">
            <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                ErrorMessage="Please enter first name." ValidationGroup="RegInternal" EnableViewState="False"
                Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblLastName" runat="server" Text="Last Name"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox EnableViewState="false" ID="txtLastName" runat="server" Width="200"
                CssClass="CommonTextBox" OnKeyPress="return alphanumeric_only(this)"></asp:TextBox>
            <span class="RequiredField">*</span>
        </div>
        <div class="TableFormValidatorContent" style="margin-left: 42%">
            <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                ErrorMessage="Please enter last name." ValidationGroup="RegInternal" EnableViewState="False"
                Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            Email :
        </div>
        <div class="TableFormContent">
            <asp:TextBox EnableViewState="false" ID="txtUserName" runat="server" Width="200px"
                OnChange='CheckUserAvailability()' AutoComplete="OFF" CssClass="CommonTextBox"
                ValidationGroup="RegInternal"></asp:TextBox>
            <span class="RequiredField">*</span>
            <div id="divAvailableUser" style="color: Green; display: none" enableviewstate="true">
                User name available</div>
            <div id="divNotAvailable" style="color: Red; display: none" enableviewstate="true">
                User name not available</div>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
            <asp:Label ID="lblCheckUser" EnableViewState="False" runat="server" />
            <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName"
                ErrorMessage="Please enter email address." ValidationGroup="RegInternal" EnableViewState="False"
                Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revUserName" runat="server" ControlToValidate="txtUserName"
                Display="Dynamic" ErrorMessage="Please enter valid email address." ValidationExpression="\w+([-+.!#$%&*+-/=?^_`{|}~'\\\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                SetFocusOnError="true" ValidationGroup="RegInternal"></asp:RegularExpressionValidator>
            <%--\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*--%>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblEmployeeID" runat="server" Text="Employee ID"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox ID="txtployeeID" runat="server" Width="200" CssClass="CommonTextBox"></asp:TextBox>
            <span class="RequiredField">*</span>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
            <asp:RequiredFieldValidator ID="rfvPSNo" runat="server" ControlToValidate="txtployeeID"
                ErrorMessage="Please enter employee PS #." ValidationGroup="RegInternal" EnableViewState="False"
                Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div id="divJobList" runat="server">
        <div class="TabPanelHeader">
            Select Job Openings<span class="TabPanelHeadersmall"> Choose one or more jobs to refer
                the candidate for</span></div>
        <div style="padding-left: 20%; padding-right: 20%;">
            <asp:TextBox ID="txtSortColumn" runat="server" Visible="false" EnableViewState="true"></asp:TextBox>
            <asp:TextBox ID="txtSortOrder" runat="server" Visible="false" EnableViewState="true"></asp:TextBox>
            <div style="width: 100%;">
                <asp:ObjectDataSource ID="odsJobList" runat="server" SelectMethod="GetPagedRequisitionListForEmployeeReferal"
                    TypeName="TPS360.Web.UI.JobPostingDataSource" SelectCountMethod="GetListCountForEmployeeReferal"
                    EnablePaging="True" SortParameterName="sortExpression" />
                <asp:ListView ID="lsvJobPostingList" runat="server" DataKeyNames="Id" OnItemDataBound="lsvJobPostingList_ItemDataBound"
                    EnableViewState="true" OnItemCommand="lsvJobPostingList_ItemCommand" OnPreRender="lsvJobPostingList_PreRender">
                    <LayoutTemplate>
                        <table id="tlbTemplate" class="Grid" cellspacing="0" border="0">
                            <tr runat="server" id="trJobList">
                                <th>
                                    <asp:LinkButton ID="btnDatePostded" runat="server" ToolTip="Sort By Date Posted"
                                        CommandName="Sort" CommandArgument="[J].[PostedDate]" Text="Date Posted" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnJobPostingCode" runat="server" ToolTip="Sort By Job Posting Code"
                                        CommandName="Sort" CommandArgument="[J].[JobPostingCode]" Text="Code" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnJobTitle" runat="server" ToolTip="Sort By Job Title" CommandName="Sort"
                                        CommandArgument="[J].[JobTitle]" Text="Job Title" />
                                </th>
                                <th style="width: 45px !important;">
                                    Select
                                </th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="4" id="tdPager" runat="server">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No job openings available for referral
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'
                            style="cursor: pointer;">
                            <td>
                                <asp:Label ID="lblDatePosted" runat="server"></asp:Label>
                                <asp:HiddenField ID="hdnJobtitle" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblJobPostingCode" runat="server"></asp:Label>
                            </td>
                            <td>
							<%--Job Description View in Employee Referral Portal--%>
                                <asp:HyperLink ID="lblJobTitle" runat="server" />
                            </td>
                            <td style="text-align: center;">
                                <asp:ImageButton ID="imgSelect" runat="server" ImageUrl="~/Images/expand-plus.png"
                                    CssClass="imageSelect" OnClientClick="return false;" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                <div class="TableRow" ">
                    <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>
