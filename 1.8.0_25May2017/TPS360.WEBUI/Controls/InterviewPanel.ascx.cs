﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewPanel.cs
    Description         :   This page is used to fill up Interviewer Panel .
    Created By          :   Pravin
    Created On          :   20/Nov/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
      0.1               13/Jan/2016         pravin khot          using remove comma from skill
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Text;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Text.RegularExpressions;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.UI; 

namespace TPS360.Web.UI
{
    public partial class ControlsInterviewPanel : BaseControl
    {

        #region Member Variables

        private int _memberId
        {


            get
            {
                int mId = 0;

                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    mId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

                }

                return mId;
            }
        }
        InterviewPanel _InterviewPanel = null;

 
        #endregion

        #region Properties

        public int _InterviewPanelId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_InterviewPanel"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_InterviewPanel"] = value;
            }
        }
        private InterviewPanel CurrentInterviewPanel
        {
            get
            {
                if (_InterviewPanel == null)
                {
                    if (_InterviewPanelId > 0)
                    {
                       _InterviewPanel = Facade.InterviewPanel_GetById(_InterviewPanelId);
                    }

                    if (_InterviewPanel == null)
                    {
                        _InterviewPanel = new InterviewPanel();
                    }
                }

                return _InterviewPanel;
            }
        }
        #endregion

        #region Methods

        private void BindList()
        {
          this.lsvInterviewPanelList.DataBind();
        }

        private void SaveInterviewDetails()
        {
            MiscUtil.PopulateMemberListWithEmailByRole(chkInternalInterviewer, ContextConstants.ROLE_EMPLOYEE, Facade);
            chkInternalInterviewer = (CheckBoxList)MiscUtil.RemoveScriptForCheckBoxList(chkInternalInterviewer);
            chkInternalInterviewer.Items.RemoveAt(0);         
            PlaceUpDownArrow();
                    
        }
        private void populateClientInterviewers(int CompanyId)
        {                 
               
                IList<CompanyContact> contact = Facade.GetAllCompanyContactByComapnyId_IP(CompanyId);
                if (contact != null)
                    chkClientInterviewer.DataSource = (from a in contact  select new { Firstname = a.FirstName + " [" + a.Email + "]", MemberId = a.MemberId }).ToList();
                else chkClientInterviewer.DataSource = null;
                chkClientInterviewer.DataTextField = "FirstName";
                chkClientInterviewer.DataValueField = "MemberId";
                chkClientInterviewer.DataBind();
              
        }
        private void SaveInterviewPanel()
        {
            if (IsValid)
            {
                try
                {
                    InterviewPanel InterviewPanel = BuildInterviewPanel();
                                      
                    if (lblid.Text=="")
                    {
                            MiscUtil.ShowMessage(lblMessage, "Interview Panel has been added succesfully.", false);
                            ClearText();                                            
                    }
                    else
                    {                       
                            MiscUtil.ShowMessage(lblMessage, "Interview Panel has been updated succesfully.", false);
                            lblid.Text = string.Empty;
                            lblid.Text = "";
                            ClearText();  
                        
                    }     
                   
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }
      
        private InterviewPanel BuildInterviewPanel()
        {                      

            InterviewPanel InterviewPanel = null;        
                      
            if (!CurrentInterviewPanel.IsNew)
            {
                InterviewPanel = CurrentInterviewPanel;
            }
            else
            {
                InterviewPanel = new InterviewPanel();
            }
            #region
            InterviewPanelMaster InterviewPanelMaster = new InterviewPanelMaster();
            InterviewPanelMaster.InterviewPanel_Name = txtpanelname.Text.ToString();
            string InterviewPanel_Name = InterviewPanelMaster.InterviewPanel_Name;

            int InterviewPanel_id;
            int InterviewPanel_EditId;
                    
            if (lblid.Text == "")
            {
                InterviewPanel_id = Facade.InterviewPanelMaster_Id(InterviewPanel_Name);
            }
            else
            {
                InterviewPanel_EditId = Convert.ToInt32(lblid.Text.Trim());
                InterviewPanel_id = Facade.InterviewPanelMaster_Id(InterviewPanel_Name, InterviewPanel_EditId);
            }

            if (InterviewPanel_id > 0)
            {
                throw new ArgumentException("Interview Panel Name Form already exists. Please specify another Interview Panel Name.");
                                           
            }
            else
            {     
                               
            string skilllistdy = hfSkillSetNames.Value.Replace("amp;", "").Replace("~", "\"");
            string mn = Facade.Skill_GetandAddSkillIds("<SkillList>" + skilllistdy.Replace("&", "&amp;") + "</SkillList>", base.CurrentMember.Id);

            if (mn.Trim().EndsWith("!")) mn = mn.Substring(0, mn.Length - 1);
           
            InterviewPanelMaster.InterviewSkill = mn;
            InterviewPanelMaster.CreatorId = base.CurrentMember.Id;
            InterviewPanelMaster.UpdatorId = base.CurrentMember.Id;
            if (lblid.Text == "")
            {
                Facade.InterviewPanelMaster_Add(InterviewPanelMaster);
            }
            else
            {
                InterviewPanelMaster.InterviewPanel_Id = Convert.ToInt32(lblid.Text);
                Facade.InterviewPanelMaster_Update(InterviewPanelMaster);
            }
            #endregion

            int InterviewPanelids = Facade.InterviewPanelMaster_Id(InterviewPanel_Name);
            InterviewPanel.InterviewPanelType_Id = InterviewPanelids;
            if (lblid.Text != "")
            {
                Facade.InterviewPanel_UpdateDeleteById(InterviewPanelids);
            }

           
            foreach (ListItem lstItm in chkInternalInterviewer.Items)
            {
                if (lstItm.Selected )
                {
                    int InterviewerId = 0;
                    InterviewerId = Convert.ToInt32(lstItm.Value);
                    InterviewPanel.Interviewer_Id = InterviewerId;
                    InterviewPanel.Interview_Mode = "USER";

                    InterviewPanel.CreatorId = base.CurrentMember.Id;
                    InterviewPanel.UpdatorId = base.CurrentMember.Id;
                    if (lblid.Text == "")
                    {
                        InterviewPanel = Facade.InterviewPanel_Add(InterviewPanel);
                    }
                    else 
                    {
                        InterviewPanel=Facade.InterviewPanel_Update(InterviewPanel);
                    }                 
                              
                }
            }
            InterviewPanel.InterviewPanelType_Id = InterviewPanelids;
                foreach (ListItem item in chkClientInterviewer.Items)
                {
                    if (item.Selected)
                    {
                        int InterviewerId = 0;
                        InterviewerId = Convert.ToInt32(item.Value);

                        InterviewPanel.Interviewer_Id = InterviewerId;
                        InterviewPanel.Interview_Mode = "BUI";

                        InterviewPanel.CreatorId = base.CurrentMember.Id;
                        InterviewPanel.UpdatorId = base.CurrentMember.Id;
                        if (lblid.Text == "")
                        {
                            InterviewPanel = Facade.InterviewPanel_Add(InterviewPanel);
                        }
                        else
                        {
                            InterviewPanel = Facade.InterviewPanel_Update(InterviewPanel);
                        }
                    }                                      
                }
                InterviewPanel.InterviewPanelType_Id = InterviewPanelids;
                String oldString = MiscUtil.RemoveScript(hfOtherInterviewers.Value);

                if (oldString != string.Empty && oldString.StartsWith(","))
                {
                    oldString = oldString.Substring(1, oldString.Length - 1);
                    InterviewPanel.OtherInterviewers = oldString.Trim().TrimStart(',');

                    InterviewPanel.CreatorId = base.CurrentMember.Id;
                    InterviewPanel.UpdatorId = base.CurrentMember.Id;
                    if (lblid.Text == "")
                    {
                        InterviewPanel = Facade.InterviewPanel_AddOtherInterviewer(InterviewPanel);
                    }
                    else
                    {
                        InterviewPanel = Facade.InterviewPanel_UpdateOtherInterviewer(InterviewPanel);
                    }
                }
                else if (oldString != string.Empty) 
                {

                    InterviewPanel.OtherInterviewers = oldString.Trim().TrimStart(',');
                    InterviewPanel.CreatorId = base.CurrentMember.Id;
                    InterviewPanel.UpdatorId = base.CurrentMember.Id;
                    if (lblid.Text == "")
                    {
                        InterviewPanel = Facade.InterviewPanel_AddOtherInterviewer(InterviewPanel);
                    }
                    else
                    {
                        InterviewPanel = Facade.InterviewPanel_UpdateOtherInterviewer(InterviewPanel);
                    }
                }
            }

           return InterviewPanel;   
        }

      

        private void PrepareEditView()
        {
           // InterviewPanel InterviewPanelActivities = CurrentInterviewPanel;
           // ddlpanel.SelectedValue = Convert.ToString(InterviewPanelActivities.InterviewPanelType_Id);
           //// InterviewPanelMaster InterviewPanelMasterActivities = new InterviewPanelMaster();
            // ddlpanel.SelectedValue = Convert.ToString(InterviewPanelActivities.InterviewPanelType_Id);
            //PopulateSkill(InterviewPanelMasterActivities.InterviewSkill); 
            //PopulateSkill(InterviewPanelActivities.InterviewPanel_Skill); 
            //txtSkillName.Text = InterviewPanelActivities.InterviewPanel_Skill;
            //txtname.Text = InterviewPanelActivities.Interviewer_Name ;
            //txtemail.Text = InterviewPanelActivities.Interview_MailId;              
           
        }
        private IList<Skill> SplitSkillValues(string value, char delim)
        {
            IList<Skill> result = new List<Skill>();
            char[] delimiters = new char[] { delim, '\n' };
            IList<string> splitarray = value.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in splitarray)
            {
                if (Convert.ToInt32(s) > 0)
                {
                    Skill sk = new Skill();
                    sk.Id = Convert.ToInt32(s);
                    sk.Name = Facade.GetSkillById(Convert.ToInt32(s)).Name;
                    result.Add(sk);
                }
            }
            return result;
        }
        public void PopulateSkill(string skillids)
        {
            hfSkillSetNames.Value = "";
            IList<Skill> sk = SplitSkillValues(skillids, '!');
            foreach (Skill s in sk)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl div = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                div.InnerHtml = "<span>" + s.Name + "</span><button type='button' class='close' data-dismiss='alert'  onclick='javascript:Clicked(\"" + s.Name.Replace("&", "&amp;").Replace("\"", "~") + "\")'>\u00D7</button>";
                div.Attributes.Add("class", "EmailDiv");
                divContent.Controls.AddAt(0, div);
                hfSkillSetNames.Value += "<item><skill>" + s.Name.Replace("&", "&amp;").Replace("\"", "~") + "</skill></item>";

                
            }
        }

        private void PlaceUpDownArrow()
        {

            try
            {
                LinkButton lnk = (LinkButton)lsvInterviewPanelList.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }
        }


        private void ClearText()
        {
           
            txtpanelname.Text = string.Empty;
            txtSkillName.Text = string.Empty;
            txtOtherInterviewers.Text = string.Empty;
            lblid.Text = string.Empty;
            lblid.Text = "";
            hfOtherInterviewers.Value = String.Empty;
            hfSkillSetNames.Value = String.Empty;
            hfOtherInterviewers.Value = "";
            hfSkillSetNames.Value = "";
            foreach (ListItem item in chkInternalInterviewer.Items)
            {
                item.Selected = false;
            }
            foreach (ListItem item in chkClientInterviewer.Items)
            {
                item.Selected = false;
            }
            _InterviewPanelId = 0;
            
        }


        #endregion

        #region Events

        #region Page event
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtpanelname.Text = string.Empty;
                txtSkillName.Text = string.Empty;
                txtOtherInterviewers.Text = string.Empty;
                lblid.Text = string.Empty;
                txtpanelname.Focus();
                BindList();
                txtSortColumn.Text = "btnDateTime";
                txtSortOrder.Text = "Desc";
                PlaceUpDownArrow();
                populateClientInterviewers(7);
                SaveInterviewDetails();
            }        

          
            lblMessage.Text = "";
            lblMessage.CssClass = "";

            string pagesize = "";
            pagesize = (Request.Cookies["InterviewPanelRowPerPage"] == null ? "" : Request.Cookies["InterviewPanelRowPerPage"].Value);
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvInterviewPanelList.FindControl("pagerControl_Requisition");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveInterviewPanel();
            PlaceUpDownArrow();
        }

        #endregion

        #region ListView Events
        protected void lsvInterviewPanelList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _isAdmin = base.IsUserAdmin;

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                InterviewPanel InterviewPanel = ((ListViewDataItem)e.Item).DataItem as InterviewPanel;

                if (InterviewPanel != null)
                {
                    Label lblpanelname = (Label)e.Item.FindControl("lblpanelname");
                    Label lblskill = (Label)e.Item.FindControl("lblskill");
                    Label lblName = (Label)e.Item.FindControl("lblName");
                  
                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
                       
                  
                                      
                    lblpanelname.Text = InterviewPanel.InterviewPanel_Name;
                    //code change by pravin khot using remove comma from skill on 13/Jan/2016
                    string interviewskills = InterviewPanel.InterviewSkill;

                    lblskill.Text = interviewskills.Substring(0, interviewskills.Length - 2);

                    //************************End*******************************
                    lblName.Text = InterviewPanel.Interviewer_Name ;
                                      
                    btnDelete.OnClientClick = "return ConfirmDelete('Interview Panel')";   //Code added by pravin khot add DELETE confirmation message on 13/Jan/2016
                    btnEdit.CommandArgument = StringHelper.Convert(InterviewPanel.InterviewPanel_Id);
                    btnDelete.CommandArgument = StringHelper.Convert(InterviewPanel.InterviewPanel_Id);
                }
            }
        }
              
        protected void lsvInterviewPanelList_PreRender(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlTableRow row = (System.Web.UI.HtmlControls.HtmlTableRow)lsvInterviewPanelList.FindControl("row");
            if (row == null)
            {
                lsvInterviewPanelList.DataSource = null;
                lsvInterviewPanelList.DataBind();
            }

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvInterviewPanelList.FindControl("pagerControl_Requisition");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null)
                {
                    hdnRowPerPageName.Value = "InterviewPanelRowPerPage";
                }
            }
            PlaceUpDownArrow();

        }
        protected void lsvInterviewPanelList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }

            int InterviewPanelMaster_ById;

            int.TryParse(e.CommandArgument.ToString(), out InterviewPanelMaster_ById);

            if (InterviewPanelMaster_ById > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    InterviewPanel InterviewPanel = ((ListViewDataItem)e.Item).DataItem as InterviewPanel;

                    var lblpanelname = (Label)e.Item.FindControl("lblpanelname");
                    string panelname = lblpanelname.Text.ToString();
                    var lblskill = (Label)e.Item.FindControl("lblskill");
                    string skills = lblskill.Text.ToString();
                    txtpanelname.Text = panelname;

                    InterviewPanelMaster_ById = Facade.InterviewPanelMaster_Id(panelname);
                    lblid.Text = InterviewPanelMaster_ById.ToString();
                    string result = Facade.GetSkills(InterviewPanelMaster_ById);
                    if (result != "")
                    {
                        PopulateSkill(result);
                    }
                    int panelid = Convert.ToInt32 (lblid.Text);

                    foreach (ListItem item in chkInternalInterviewer.Items)
                    {
                        item.Selected = false;
                        int InterviewerId = 0;
                        InterviewerId = Convert.ToInt32(item.Value);
                                            

                        if (Facade.Interview_ChkEmailId(InterviewerId, panelid))
                        {
                            item.Selected = true;
                           
                        }                           
                    }
                            
                    foreach (ListItem item in chkClientInterviewer.Items)
                    {
                        item.Selected = false;
                        int InterviewerId = 0;
                        InterviewerId = Convert.ToInt32(item.Value);

                        if (Facade.Interview_ClientChkEmailId(InterviewerId, panelid))
                        {
                            item.Selected = true;
                        }
                    }
                    txtOtherInterviewers.Text = string.Empty;
                    hfOtherInterviewers.Value = "";                   


                    IList<InterviewPanel> InterviewPanels = Facade.InterviewPanel_OtherChkEmailId(panelid);
                    string[] interviewers = null;
                    char[] delim = { ',' };
                    if (InterviewPanels != null)
                    {
                        foreach (var a in InterviewPanels)
                        {
                            interviewers = a.Interview_Mode.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                            foreach (var s in interviewers)
                            {
                                System.Web.UI.HtmlControls.HtmlGenericControl div = new System.Web.UI.HtmlControls.HtmlGenericControl();
                                div.InnerHtml = "<span>" + s + "</span><button type='button' class='close' data-dismiss='alert'  onclick=javascript:Clicked('" + s + "')>\u00D7</button>";
                                div.Attributes.Add("class", "EmailDiv");
                                divContentip.Controls.AddAt(0, div);
                                if (hfOtherInterviewers.Value.Trim() != string.Empty) hfOtherInterviewers.Value += ",";
                                hfOtherInterviewers.Value += s;
                            }
                         }
                    }
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                   InterviewPanel InterviewPanel = ((ListViewDataItem)e.Item).DataItem as InterviewPanel;
                    try
                    {
                        InterviewPanelMaster_ById = 0;   
                        
                            var lblpanelname = (Label)e.Item.FindControl("lblpanelname");
                            string panelname = lblpanelname.Text.ToString();
                            InterviewPanelMaster_ById = Facade.InterviewPanelMaster_Id(panelname);
                            if (Facade.InterviewPanel_DeleteById(InterviewPanelMaster_ById))
                            {
                                BindList();
                                MiscUtil.ShowMessage(lblMessage, "Interview Panel has been successfully deleted.", false);
                                ClearText();
                            }
                            if (_InterviewPanelId == InterviewPanelMaster_ById)
                                ClearText();                  
                       

                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
            BindList();

        }
        #endregion

        #endregion          
                
       
}
}


