﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: OverviewMenu.ascx.cs
    Description: This is the user control page used to display overview menu.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 May-11-2009      Jagadish            Defect id: 10143; Changes made in method BuildMenu();
 *  0.2                 Feb-03-2016      Sakthi              Changes mad in methods AddToHiringMatrix() and SendNotificationEmailToReferrer()
 *  0.3                 11/Feb/2016      pravin khot         Change jobtitle onclick url- (Modals/CareerJobDetail.aspx)
 *  0.4                  12/May/2016     pravin khot         modify function SendNotificationEmailToRecruiters
 *  0.5                 11/Nov/2016      Sumit Sonawane      Modified func SendNotificationEmailToRecruiters to prevent crash if no team member/Req available.

-------------------------------------------------------------------------------------------------------------------------------------------       
*/
using System;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;

using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Web.UI.HtmlControls;
using TPS360.Common.BusinessEntities;
using TPS360.Providers;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.IO;
using TPS360.Common.Shared;

namespace TPS360.Web.UI
{
    public partial class ReferrerInfo : BaseControl
    {
        #region Private variable

        public bool isJobPostingSelected
        {
            get
            {
                return hdnSelectedJobPostings.Value.Trim() != string.Empty;
            }
        } 
      
        public string CandidateName
        {
            get;
            set;
        }

        private int JobId
        {
            get
            {

                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]);
                }
                else return 0;

            }
        }
        #endregion

        #region Methods

        public void AddToHiringMatrix(int memberid)
        {

            char[] delin = { ',' };
            string[] jid = hdnSelectedJobPostings.Value.Split(delin, StringSplitOptions.RemoveEmptyEntries);
            HiringMatrixLevels lev = null;// Facade.HiringMatrixLevel_GetInitialLevel();

            var webConfig = System.Web.Configuration. WebConfigurationManager.OpenWebConfiguration("~");
            var Referral = webConfig.AppSettings.Settings["Referral"];
            if (Referral != null && Referral .Value.ToString ()!=string .Empty ) 
            {
                 lev = Facade.HiringMatrixLevel_GetLevelByName(Referral.Value.ToString());
            }
            if (lev == null) lev = Facade.HiringMatrixLevel_GetInitialLevel();
            if (jid != null && jid.Length > 0)
            {
                foreach (string j in jid)
                {
                    Facade.MemberJobCart_AddCandidateToRequisition(0, memberid.ToString(), Convert.ToInt32(j));
                    Facade.MemberJobCart_MoveToNextLevel(0, 2, memberid.ToString(), Convert.ToInt32(j), lev.Id.ToString());
                    MiscUtil.EventLogForCandidateStatus(TPS360.Common.Shared.EventLogForCandidate.CandidateStatusChange, Convert.ToInt32(j), memberid.ToString(), 2, lev.Name, Facade);
                    Facade.UpdateCandidateRequisitionStatus(Convert.ToInt32(j), memberid.ToString(), 2, lev.Id);
                   //**************code modify by pravin khot on 12/May/2016*********
                   // SendNotificationEmailToRecruiters(Convert .ToInt32 ( j));//OLD CODE
                    SendNotificationEmailToRecruiters(Convert.ToInt32(j), memberid);
                    //**************************END***********************************
                }
            }
            SendNotificationEmailToReferrer(memberid);// Code modified by Sakthi on Feb-03-2016 - Parameter Added
        }
        private void SendNotificationEmailToReferrer(int memberid)// Code modified by Sakthi on Feb-03-2016 - Argument Added
        {
            string sentStatus = string.Empty;
            EmailHelper emailManager = new EmailHelper();
            StringBuilder body = new StringBuilder();
            //body.Append("<table border='0' cellspacing='0' cellpadding='0' style='background:#004B88;border-collapse:collapse'>");
            //body.Append("<tr><td colspan=3 width='1000' height='20'/></tr>");
            //body.Append("<tr>");
            //body.Append("<td width='200'/><td>");
            body.Append("<body style='background:#004B88'>");
            body.Append("<div >");
            body.Append("   <div>");
            body.Append("       <p><a name='0.1__MailOriginal'><span lang='EN-US' style='color:black'> </span></a></p>");
            body.Append("       <div align='center'>");
            body.Append("           <table border='0' cellspacing='0' cellpadding='0' style='background:white;border-collapse:collapse'>");
            body.Append("               <tbody>");
            body.Append("               <tr>");
            body.Append("                   <td width='808' valign='top' style='width:606.0pt;padding:0cm 0cm 0cm 0cm'>");
            body.Append("                       <p align='right' style='text-align:right'><span><span><img width='399' height='97' src=\"cid:COMPANY_LOGO\" style='border-width:0px;'/></span></span></p>");
            body.Append("                   </td>");
            body.Append("                   <td><span></span>");
            body.Append("                   </td></tr>");
            body.Append("               <tr>");
            body.Append("                   <td width='808' valign='top' style='width:606.0pt;padding:0cm 0cm 0cm 0cm'>");
            body.Append("                       <p><span><span><img width='854' height='174'src=\"cid:THANKS_LOGO\"></span></span></p>");
            body.Append("                   </td>");
            body.Append("                   <td><span></span>");
            body.Append("                   </td></tr>");
            body.Append("               <tr>");
            body.Append("                   <td width='808' valign='top' style='width:606.0pt;padding:0cm 0cm 0cm 0cm'>");
            body.Append("                       <p><span> </span></p>");
            body.Append("                   </td>");
            body.Append("                   <td><span></span>");
            body.Append("                   </td></tr>");
            body.Append("               <tr>");
            body.Append("                   <td width='808' valign='top' style='width:606.0pt;padding:0cm 0cm 0cm 0cm'>");
            body.Append("                       <table border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100.0%;border-collapse:collapse'>");
            body.Append("                           <tbody>");
            body.Append("                               <tr>");
            body.Append("                                   <td width='809' valign='top' style='width:606.75pt;padding:0cm 25.5pt 0cm 25.5pt'>");
            body.Append("                                       <p><span><span style='font-size:10.0pt;font-family:&quot;Trebuchet MS&quot;,&quot;sans-serif&quot;'>Dear " + txtFirstName.Text + ",");
            body.Append("                                       </span></span></p>");
            body.Append("                                       <p><span><span style='font-size:10.0pt;font-family:&quot;Trebuchet MS&quot;,&quot;sans-serif&quot;'> </span></span></p>");
            body.Append("                                       <p><span><span style='font-size:10.0pt;font-family:&quot;Trebuchet MS&quot;,&quot;sans-serif&quot;'>We appreciate your extra effort in participating in our Employee Referral Program! Thank you for referring " + CandidateName + "[ID: A" + memberid + "]."); // Code modified by Sakthi on Feb-03-2016 - Added memberid to display is ThankYou mail
            body.Append("                                       </span></span></p>");
            body.Append("                                       <p><span><span style='font-size:10.0pt;font-family:&quot;Trebuchet MS&quot;,&quot;sans-serif&quot;'> </span></span></p>");
            body.Append("                                       <p><span><span style='font-size:10.0pt;font-family:&quot;Trebuchet MS&quot;,&quot;sans-serif&quot;'>Our team will review this profile and assess its suitability against our current requirements.</span></span></p>");
            body.Append("                                       <p><span><span style='font-size:10.0pt;font-family:&quot;Trebuchet MS&quot;,&quot;sans-serif&quot;'> </span></span></p>");
            body.Append("                                       <p><span><span style='font-size:10.0pt;font-family:&quot;Trebuchet MS&quot;,&quot;sans-serif&quot;'>We look forward to your continued support in making the Employee Referral Program a success.</span></span></p>");
            body.Append("                                       <span></span>");
            body.Append("                                       <p><span><span style='font-size:10.0pt;font-family:&quot;Trebuchet MS&quot;,&quot;sans-serif&quot;'> </span></span></p>");
            body.Append("                                   </td>");
            body.Append("                                   <td><span></span>");
            body.Append("                                   </td></tr>");
            body.Append("                               <tr>");
            body.Append("                                   <td width='809' valign='top' style='width:606.75pt;padding:0cm 25.5pt 0cm 25.5pt'>");
            body.Append("                                       <span></span></td>");
            body.Append("                                   <td><span></span>");
            body.Append("                                   </td></tr>");
            body.Append("                               <tr>");
            body.Append("                                   <td width='809' valign='top' style='width:606.75pt;padding:0cm 25.5pt 0cm 25.5pt'>");
            body.Append("                                       <p><span> </span></p>");
            body.Append("                                   </td>");
            body.Append("                                   <td><span></span>");
            body.Append("                                   </td></tr>");
            body.Append("                           </tbody>");
            body.Append("                       </table>");
            body.Append("                       <span></span></td>");
            body.Append("                   <td><span></span>");
            body.Append("                   </td></tr>");
            body.Append("               <tr>");
            body.Append("                   <td width='808' valign='top' style='width:606.0pt;padding:0cm 0cm 0cm 0cm'>");
            body.Append("                       <p style='margin-left:26.45pt'><span><span style='font-size:10.0pt;font-family:&quot;Trebuchet MS&quot;,&quot;sans-serif&quot;'>Warm Regards,</span></span><span><span style='font-size:12.0pt'></span></span><br>");
            body.Append("                       <span><span style='font-size:10.0pt;font-family:&quot;Trebuchet MS&quot;,&quot;sans-serif&quot;'>Recruitment Team</span></span><span><span style='font-size:12.0pt'></span></span></p>");
            body.Append("                       <p style='margin-left:26.45pt'><span> </span></p>");
            body.Append("                   </td>");
            body.Append("                   <td><span></span>");
            body.Append("                   </td></tr>");
            body.Append("               </tbody>");
            body.Append("           </table>");
            body.Append("       </div>");
            body.Append("       <p><span><span lang='EN-US' style='color:black'> </span></span><span lang='EN-US' style='color:black'></span></p>");
            body.Append("   </div>");
            body.Append("</div>");
            body.Append("</body>");
            string subject = "Thank you for your referral!";
            if (SiteSetting == null)
            {
                SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)TPS360.Common.Shared.SettingType.SiteSetting);
                Hashtable config = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                emailManager.From = config[TPS360.Common.Shared.DefaultSiteSetting.SMTPUser.ToString()].ToString();// CurrentMember.PrimaryEmail;
            }
            else emailManager.From = SiteSetting[TPS360.Common.Shared.DefaultSiteSetting.SMTPUser.ToString()].ToString();// CurrentMember.PrimaryEmail;
            var stream = new MemoryStream();
            //image.Save(stream,System.Drawing.Imaging.ImageFormat.Png);
            stream.Position=0;
            //emailManager.Attachments.Add(new System.Net.Mail.Attachment(stream, "image/png"));
            emailManager.LinkedResources.Add("COMPANY_LOGO", Server.MapPath(UIConstants.EMAIL_COMPANY_THANKS));
            emailManager.LinkedResources.Add("THANKS_LOGO", Server.MapPath(UIConstants.EMAIL_THANKS_LOGO));
            emailManager.Subject = subject;
            emailManager.To.Add(txtUserName.Text);
            emailManager.Body = body.ToString();

            //***********Added by pravin khot on 8/March/2017***********
            //sentStatus = emailManager.Send();//comment by pravin khot on 17/June/2016
            //sentStatus = emailManager.SendMailFromSystemEmailId();//added by pravin khot on 17/June/2016
            int senderid = 0;
            string AdminEmailId = string.Empty;
            Hashtable siteSettingTable = null;
            SiteSetting siteSetting1 = Facade.GetSiteSettingBySettingType((int)TPS360.Common.Shared.SettingType.SiteSetting);
            siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting1.SettingConfiguration);
            if (siteSetting1 != null)
            {
                AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                senderid = Facade.GetMemberIdByEmail(AdminEmailId);
            }
            MailQueueData.AddMailToMailQueue(senderid, txtUserName.Text, subject, body.ToString(), "", "", null, Facade);
            sentStatus = "1";     
            //*****************END**********************

            if (sentStatus == "1")
            {
                SaveMemberEmail(subject, body.ToString(), emailManager.From, txtUserName.Text);
                sentStatus = "Sent";
            }
        }
        public static System.Drawing.Image GetImageFromUrl(string url)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);

            using (HttpWebResponse httpWebReponse = (HttpWebResponse)httpWebRequest.GetResponse())
            {
                using (Stream stream = httpWebReponse.GetResponseStream())
                {
                    return System.Drawing.Image.FromStream(stream);
                }
            }
        }
        private void SendNotificationEmailToRecruiters(int jobid, int memberid)    //// Sumit Sonawane 11/Nov/2016 
        {
            string sentStatus = string.Empty;
            EmailHelper emailManager = new EmailHelper();
            JobPosting job = Facade.GetJobPostingById(jobid);
            Member Candidate = Facade.GetMemberById(Convert.ToInt32(memberid));
            string CName = Candidate.FirstName + " " + Candidate.LastName;
            MemberExtendedInformation memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(Convert.ToInt32(memberid));
            string subject = "[" + job.JobTitle + " - " + job.JobPostingCode + " - Employee Referral Submission";
          

            IList<JobPostingHiringTeam> team = Facade.GetAllJobPostingHiringTeamByJobPostingId(jobid);
            if (SiteSetting == null)
            {
                SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)TPS360.Common.Shared.SettingType.SiteSetting);
                Hashtable config = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                emailManager.From = config[TPS360.Common.Shared.DefaultSiteSetting.SMTPUser.ToString()].ToString();// CurrentMember.PrimaryEmail;
            }
            else emailManager.From = SiteSetting[TPS360.Common.Shared.DefaultSiteSetting.SMTPUser.ToString()].ToString();// CurrentMember.PrimaryEmail;
            string to = "";
            StringBuilder body = new StringBuilder();
           
            try
            {
                foreach (JobPostingHiringTeam t in team)
                {
                    //emailManager.To.Add(t.PrimaryEmail);
                    //********Added by pravin khot on 12/May/2016***************
                    string RecutierName = "";

                    //***************Modify code by pravin khot on 12/May/2016****************
                    
                    body.Append("<html>");
                    body.Append("<head><title></title>");
                    body.Append("<style>body, td, p{font-family: Arial;font-size: 12px;} </style></head>");
                    body.Append("<body><P><STRONG>Dear [RecuriterName]</STRONG>,</P>");
                    body.Append("<P>A candidate has been submitted for your requisition in the Employee Referral Portal.</P>");
                    body.Append("<table><tr>");
                    body.Append("<td width=\"100\"><STRONG>Requisition</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500><STRONG> [JobTitle] | [JobCode]</STRONG></td></tr>");
                    //body.Append("<tr/>");
                    body.Append("<tr><td width=\"100\">Candidate<td width=\"10\" align=\"center\">:</td> <td width=500>[Candidatename] | [ID: A[CandidateID]]</td></tr>");
                    body.Append("<tr><td width=\"100\">Candidate Email<td width=\"10\" align=\"center\">:</td> <td width=500>[CandidateEmail]</td></tr>");
                    body.Append("<tr/><tr>");
                    body.Append("<td width=\"100\"><STRONG>Referrer Details</STRONG></td></tr>");
                    body.Append("<tr><td width=\"100\">Referrer Name<td width=\"10\" align=\"center\">:</td> <td width=500>[ReferrerName]</td></tr>");
                    body.Append("<tr><td width=\"100\">Ref. Emp.Code<td width=\"10\" align=\"center\">:</td> <td width=500>[EmpCode]</td></tr>");
                    body.Append("<tr><td width=\"100\">Email ID<td width=\"10\" align=\"center\">:</td> <td width=500>[EmailID]</td></tr>");
                    body.Append("<tr/></table>");
                    body.Append("</body></html>");
                    body.Replace("[JobTitle]", job.JobTitle);
                    body.Replace("[JobCode]", job.JobPostingCode);
                    body.Replace("[Candidatename]", CName);
                    body.Replace("[CandidateID]", memberid.ToString());
                    body.Replace("[CandidateEmail]", Candidate.PrimaryEmail);
                    body.Replace("[ReferrerName]", txtFirstName.Text + " " + txtLastName.Text);
                    body.Replace("[EmpCode]", txtployeeID.Text);
                    body.Replace("[EmailID]", txtUserName.Text);
                    emailManager.Subject = subject;
                    emailManager.To.Clear();
                    //*********************END********************************



                    Member recutier = Facade.GetMemberByMemberEmail(t.PrimaryEmail);
                    if (recutier != null)
                    {
                        RecutierName = recutier.FirstName + " " + recutier.LastName;
                    }
                    else
                    {
                        RecutierName = "";
                    }
                    body = body.Replace("[RecuriterName]", RecutierName);
                    //*******************END***************************

                   // to += t.PrimaryEmail + ";";

                    //***********Added by pravin khot on 8/March/2017***********
                    //sentStatus = emailManager.Send();//comment by pravin khot on 17/June/2016
                    //sentStatus = emailManager.SendMailFromSystemEmailId();//added by pravin khot on 17/June/2016
                    int senderid = 0;
                    string AdminEmailId = string.Empty;
                    Hashtable siteSettingTable = null;
                    SiteSetting siteSetting1 = Facade.GetSiteSettingBySettingType((int)TPS360.Common.Shared.SettingType.SiteSetting);
                    siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting1.SettingConfiguration);
                    if (siteSetting1 != null)
                    {
                        AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                        senderid = Facade.GetMemberIdByEmail(AdminEmailId);
                    }
                    MailQueueData.AddMailToMailQueue(senderid, t.PrimaryEmail, subject, body.ToString(), "", "", null, Facade);
                    sentStatus = "1";                 
                    if (sentStatus == "1")
                    {
                        SaveMemberEmail(subject, body.ToString(), emailManager.From, to);
                        sentStatus = "Sent";
                    }
                    body = new StringBuilder();
                    //*****************END**********************
                }
                //emailManager.Body = body.ToString();               

               
            }
            catch { MiscUtil.ShowMessage(lblMessage, "Requisition or Team member details not found , Candidate not submitted..", false); }
        }

        private void SaveMemberEmail( string subject, string body, string From,string To)
        {
            if (true) // Isvalid
            {
                try
                {
                    MemberEmail memberEmail = BuildMemberEmail(subject, body, From,To);
                     Facade.AddMemberEmail(memberEmail);
                }
                catch (ArgumentException ex)
                {

                }

            }
        }
        private MemberEmail BuildMemberEmail(string subject, string body, string strFrom,string To)
        {
            MemberEmail memberEmail = new MemberEmail();
            memberEmail.SenderId = 0;
            memberEmail.SenderEmail = strFrom;
            memberEmail.ReceiverEmail = To;
            memberEmail.Subject = subject;
            memberEmail.EmailBody = body;
            memberEmail.Status = 0;
            memberEmail.IsRemoved = false;
            memberEmail.CreatorId = 0;
            memberEmail.SentDate = DateTime.Now.ToString();
            memberEmail.EmailTypeLookupId = (int)TPS360 .Common .Shared . EmailType.Sent;
            return memberEmail;
        }
        public int  AddEmployeeReferalDetails(int MemberID)
        {
            if (hdnSelectedJobPostings.Value != null && hdnSelectedJobPostings.Value != "")
            {
                AddToHiringMatrix(MemberID);
            }
            
            int ReferrerID=Facade.AddReferral(BuildEmployeeReferralDetails(MemberID));
            txtUserName.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtployeeID.Text = "";
            if (divJobList.Visible) hdnSelectedJobPostings.Value = "";
            return ReferrerID;
            
        }
        private TPS360.Common .BusinessEntities . EmployeeReferral BuildEmployeeReferralDetails(int MemberID)
        {
            TPS360.Common.BusinessEntities.EmployeeReferral newreferal = new TPS360.Common.BusinessEntities.EmployeeReferral();
            newreferal.RefererEmail = MiscUtil.RemoveScript(txtUserName.Text);
            newreferal.RefererFirstName = MiscUtil.RemoveScript(txtFirstName.Text);
            newreferal.RefererLastName = MiscUtil.RemoveScript(txtLastName.Text);
            newreferal.JobpostingId = hdnSelectedJobPostings.Value;
            newreferal.EmployeeId = MiscUtil.RemoveScript(txtployeeID.Text);
            newreferal.MemberId = MemberID;
           // hdnSelectedJobPostings.Value = "";
            return newreferal;
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {

            switch (ApplicationSource)
            {
                case ApplicationSource.LandTApplication:
                    revUserName.Enabled = true;
                    break;
                case ApplicationSource.MainApplication:
                    revUserName.Enabled = false;
                    break;
                case ApplicationSource.SelectigenceApplication:
                    revUserName.Enabled = false;
                    break;
            }


            //if (isMainApplication) 
            //{
            //    revUserName.Enabled = false;
            //}

            if (!IsPostBack)
            {
                txtFirstName.Focus();
                if (JobId > 0)
                {
                    hdnSelectedJobPostings.Value = JobId.ToString();
                    divJobList.Visible = false;
                }
                else
                {
                    lsvJobPostingList.DataSourceID = "odsJobList";
                    lsvJobPostingList.DataBind();
                }
            }
        }
        private void PlaceUpDownArrow()
        {
            try
            {
                if (txtSortColumn.Text  == string.Empty)
                {
                    txtSortColumn.Text = "btnDatePostded";
                    txtSortOrder.Text = "ASC";
                }
                LinkButton lnk = (LinkButton)lsvJobPostingList .FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }

        protected void lsvJobPostingList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPosting  jobposting = ((ListViewDataItem)e.Item).DataItem as JobPosting ;

                if (jobposting != null)
                {
                    string s = hdnSelectedJobPostings.Value;

                    Label lblDatePosted = (Label)e.Item.FindControl("lblDatePosted");
                    Label lblJobPostingCode = (Label)e.Item.FindControl("lblJobPostingCode");
					//Job Description View in Employee Referral Portal
                    HyperLink lblJobTitle = (HyperLink)e.Item.FindControl("lblJobTitle");
                    HiddenField hdnJobtitle = (HiddenField)e.Item.FindControl("hdnJobtitle");
                    hdnJobtitle.Value = jobposting.Id.ToString();
                    lblDatePosted.Text = jobposting.PostedDate.ToShortDateString();
                    lblJobPostingCode.Text = jobposting.JobPostingCode;
                    lblJobTitle.Text = jobposting.JobTitle;

                    //*******Code comment by pravin khot on 11/Feb/2016*******************
					//Job Description View in Employee Referral Portal
                    //lblJobTitle.Attributes.Add("onclick", "ShowModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobposting.Id + "&FromPage=Referrer" + "&isEditButtonVisible=False','700px','570px'); return false;");
                    lblJobTitle.Attributes.Add("onclick", "ShowModal('" + UrlConstants.ApplicationBaseUrl + "Modals/CareerJobDetail.aspx?JID=" + jobposting.Id + "&FromPage=Referrer" + "&isEditButtonVisible=False','700px','570px'); return false;");
                    //*********************END*******************************
                    if (jobposting.Id.ToString() == hdnSelectedJobPostings.Value)
                    {
                        HtmlTableRow row = (HtmlTableRow)lblJobTitle.Parent.Parent;
                        string m=row.Attributes["class"];
                        row.Attributes.Add("class", m + " SelectedRow");
                        ImageButton imgSelect = (ImageButton)e.Item.FindControl("imgSelect");
                            imgSelect .ImageUrl ="../Images/Collapse-minus.png";
                    }
                }
            }
        }
        protected void lsvJobPostingList_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPostingList.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null)
                {
                    hdnRowPerPageName.Value = "EmployeeReferalJobsRowPerPage";
                    
                }
            }
            PlaceUpDownArrow();

          //System .Web .UI .  ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "whatever1", "Recaptcha.reload();", true);
        }
        protected void lsvJobPostingList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }
           

           
        }


        #endregion
    }
}