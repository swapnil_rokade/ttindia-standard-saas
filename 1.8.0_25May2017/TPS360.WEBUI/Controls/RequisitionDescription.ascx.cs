﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RequisitionDescription.ascx.cs
    Description: This is the user control page used for adding requisition description.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Nov-07-2008           Jagadish            Defect ID: 9184 and 9592; (Kaizen) Added a method 'BindRequisitionSkillSetList' to 
                                                               automatically extract skills from job description and add these skills to 'Requisition Skillset'.
    0.2              Nov-11-2008           Yogeesh Bhat        Defect ID: 9182; Changes made in btnSaveJobDescription_Click(), btnSaveClientJobDescription_Click() methods
                                                               (Redirected page to skillset page on save click)
    0.3              Apr-17-2009           Jagadish            Defect id: 10221; Changes made in method 'btnSaveJobDescription_Click'.
 
    0.4              Nov-18-2009           Rajendra            Enhanc id:11811; Changes made in method 'btnSaveJobDescription_Click'.
 *  0.5              Apr-27-2010           Basavaraj A         Defect # 12603 ; Commented the lines for JOb Descrition 
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Web;
using System.Collections.Generic; 
using System.Text.RegularExpressions;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;


namespace TPS360.Web.UI
{
    public partial class cltRequisitionDescription : RequisitionBaseControl
    {
        #region Member Variables

        private void Disablevalidationcontrols()
        {
            cvDescription.Enabled = false;
        }

        #endregion

        #region Methods
        public JobPosting BuildJobDescription(JobPosting jobPosting)
        {
            jobPosting.RawDescription = WHEJobDescription.InnerHtml;
            string s = WHEJobDescription.InnerHtml;
            s = Regex.Replace(s, "<script.*?</script>", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            jobPosting.JobDescription = s.ToString();
            string rawDes = Regex.Replace(MiscUtil.RemoveScript(WHEJobDescription.InnerHtml.Trim(), string.Empty), @"<(.|\n)*?>", string.Empty);
            jobPosting.RawDescription = HttpUtility.HtmlDecode(rawDes);
            jobPosting.ClientBrief = MiscUtil.RemoveScript(txtClientBrief.Text);
            return jobPosting;
        }        

        private void PrepareView(JobPosting jobPosting)
        {
                if (jobPosting != null)
                {
                    WHEJobDescription.InnerHtml = jobPosting.JobDescription.ToString();
                    txtClientBrief.Text = MiscUtil.RemoveScript(jobPosting.ClientBrief, string.Empty);
                }

             
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (fuDescription.HasFile)
            {
                String strFilePath = string.Empty;
                strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Temp/" + CurrentMember.UserId.ToString() + "/JobDescription/");
                bool IsExists = System.IO.Directory.Exists(strFilePath );

                if (!IsExists)
                    System.IO.Directory.CreateDirectory(strFilePath);
                strFilePath +=  fuDescription .FileName ;
                fuDescription.SaveAs(strFilePath);

                            Parser.DocumentConverter document = new Parser.DocumentConverter();
                Parser.DocumentConverter.OutputTypes doctype = Parser.DocumentConverter.OutputTypes.HtmlFormatted;
                if (System.IO.Path.GetExtension(fuDescription.FileName).ToLower() == ".pdf")
                {
                    doctype = Parser.DocumentConverter.OutputTypes.HtmlFormatted    ;
                }
                byte[] data = document.ConvertTo(strFilePath, doctype);
               if(data !=null) WHEJobDescription .InnerHtml = System.Text.ASCIIEncoding.ASCII.GetString(data);
                try
                {
                    System.IO.File.Delete(strFilePath);
                }
                catch
                {
                }
            }
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ApplicationSource == ApplicationSource.MainApplication || ApplicationSource == ApplicationSource.SelectigenceApplication || ApplicationSource == ApplicationSource.GenisysApplication)
            {
                Disablevalidationcontrols();
            }

            if (!IsPostBack)
            {
                if(CurrentJobPostingId >0)
                    PrepareView(CurrentJobPosting );
                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                {
                    divClientBrief.Visible = false;
                }
            }
        }
        #endregion
       
}
}
