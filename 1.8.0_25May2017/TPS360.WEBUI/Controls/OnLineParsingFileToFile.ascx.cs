﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewPanel.cs
    Description         :   This page is used to fill up Interviewer Panel .
    Created By          :   Pravin
    Created On          :   20/Nov/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
 * 0.1                10Apr2017         Prasanth Kumar G        Issue id 724 Fine tune
 * 0.2                22/May/2017       Sumit Sonawane          Issue Id 1326
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System.Web.UI.WebControls;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Data;
using System.Web.Security;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using ExpertPdf.HtmlToPdf;
using TPS360.Common;
using WebChart;
using System.Configuration;
using AjaxControlToolkit;
using System.Data.SqlClient;
using TPS360.BusinessFacade;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.Net;
using System.Xml;
using iTextSharp.text.pdf;
using iTextSharp.text;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using System.Globalization;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO.Compression;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Threading;
using System.Collections;
using System.Web.Services.Description;
using System;
using System.Web.Services;
using System.Web.Services.Protocols;
namespace TPS360.Web.UI
{
    public partial class ControlsOnLineParsingFileToFile : BaseControl
    {
        #region Member Variables
        string strFilePath = string.Empty;
        private string _role = "";
        private static int IdForSitemap = 0;
        private string currentSiteMapId = "0";
        private static string UrlForEmployee = string.Empty;
        int countnewcandidate = 0;
        int countupdatecandidate = 0;
        int totalcandidate = 0;
        int allreadyparse = 0;
        int totalparse = 0;
        bool cvstatus = false;
        OnLineParser _OnLineParser = null;
        private int _memberId
        {
            get
            {
                int mId = 0;

                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    mId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

                }

                return mId;
            }
        }

      
        #endregion

        #region Properties

        public int _OnLineParserId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_OnLineParser"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_OnLineParser"] = value;
            }
        }
        private OnLineParser CurrentOnLineParser
        {
            get
            {
                if (_OnLineParser == null)
                {
                    if (_OnLineParserId > 0)
                    {
                        _OnLineParser = Facade.OnLineParser_GetById(_OnLineParserId);
                    }

                    if (_OnLineParser == null)
                    {
                        _OnLineParser = new OnLineParser();
                    }
                }

                return _OnLineParser;
            }
        }
        #endregion

        public string PreviewDir
        {
            get;
            set;
        }
           
        public string PreviewXMLFile
        {
            get;
            set;
        }
        public string PreviewDOCFile
        {
            get;
            set;
        }
        public string CopyDir
        {
            get;
            set;
        }

        #region Methods
        private void BindList()
        {
            odsOnLineParserList.SelectParameters["memberId"].DefaultValue = base.CurrentMember.Id.ToString();
            odsOnLineParserList.SelectParameters["SortOrder"].DefaultValue = "";
           // odsOnLineParserList.SelectParameters["SortColumn"].DefaultValue = "";
            string pagesize = "";

            pagesize = (Request.Cookies["OnLineParserRowPerPage"] == null ? "" : Request.Cookies["OnLineParserRowPerPage"].Value); ;
           
           // odsOnLineParserList.DataSourceID = "odsOnLineParserList";
            this.lsvOnLineParserList.DataBind();
        }
        private void OnLineParserDetails()
        {
            PlaceUpDownArrow();
        }
        private void SaveOnLineParser(string  fileName78, string physicalPath1)
        {
            if (IsValid)
            {
                try
                {
                    OnLineParser OnLineParser = BuildOnLineParser(fileName78, physicalPath1);
                }
                catch (ArgumentException ex)
                {

                }
            }
        }
        private OnLineParser BuildOnLineParser(string fileName78, string physicalPath1)
        {
            OnLineParser OnLineParser = null;         
            OnLineParser = new OnLineParser();          
            OnLineParser.Title = fileName78;
            OnLineParser.Path = physicalPath1;
            OnLineParser.XMLFilePath = "";
            OnLineParser.Status = 0;
            OnLineParser.ParseStatus = 0;
            OnLineParser.DatabaseStatus = 0;
            OnLineParser.MemebrId = 0;
            OnLineParser.CreatorId = base.CurrentMember.Id;
            OnLineParser.UpdatorId = base.CurrentMember.Id;
            OnLineParser = Facade.OnLineParser_Add(OnLineParser);
            return OnLineParser;
        }        
        private void PlaceUpDownArrow()
        {

            try
            {
                LinkButton lnk = (LinkButton)lsvOnLineParserList.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }
        }
        private void ClearText()
        {

        }
        #endregion
        #region Events       
        public string TempDirectory
        {
            get
            {
                return (string)Helper.Session.Get("tempDir");
            }
            set
            {
                Helper.Session.Set("tempDir", value);
            }
        }
        //protected void btnClose_Click(object sender, EventArgs e)
        //{
        //    showresult.Visible = false;
        //}
        protected void btnExport_Click(object sender, EventArgs e)
        {
            string fileName = Path.GetFileName(fuSelectCV.PostedFile.FileName);
            strFilePath = HttpContext.Current.Server.MapPath("../" + UrlConstants.ONLINE_CV_DIR); 
            if (fileName != "")
            {
                string physicalPath = strFilePath + "\\" + fileName;
                OnLineParser olp = Facade.OnLineParser_GetByPath(physicalPath, base.CurrentMember.Id);
                if (olp == null)
                {
                    if (!Directory.Exists(strFilePath))
                    {
                        Directory.CreateDirectory(strFilePath);
                    }
                    fuSelectCV.PostedFile.SaveAs(strFilePath + "/" + fileName);
                    SaveOnLineParser(fileName, physicalPath);
                    MiscUtil.ShowMessage(lblMessage, "File uploaded succesfully", false);
                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "Already added this file", true);
                }                           
                                            
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "First select file", true);
            }
            PlaceUpDownArrow();
        }
        static byte[] Gzip(byte[] bytes)
        {
            if (bytes == null)
            {
                return new byte[0];
            }
            if (bytes.Length == 0)
            {
                return bytes;
            }
            using (MemoryStream memStream = new MemoryStream(bytes.Length / 2))
            {
                using (GZipStream gzipStream = new GZipStream(memStream, CompressionMode.Compress))
                {
                    gzipStream.Write(bytes, 0, bytes.Length);
                }
                return memStream.ToArray();
            }
        }   
        public void ParseResumeandsave(string physicalPath)
        {
            // PreviewDir = GetTempFolder();
            PreviewDir = "D:\\cv\\DOC";           
            //Service.Parsing.ParsingService ps = new Service.Parsing.ParsingService();
            ParserService.ParsingService ps = new ParserService.ParsingService();
            ps.Url = "http://services.resumeparsing.com/ParsingService.asmx";
            //Service.Parsing.ParseResumeRequest request = new Service.Parsing.ParseResumeRequest();
            ParserService.ParseResumeRequest request = new ParserService.ParseResumeRequest();


            request.AccountId = "26410689";
            request.ServiceKey = "ZBNA0ueiKEB+IEBEJyeNnF9YS1oFAS+KTR03xIXI";
            request.OutputHtml = true;
            string[] replaceThese = { ".docx", ".doc", ".pdf", ".rtf", ".txt" };

            foreach (var chkItem in Directory.GetFiles(PreviewDir))
            {
                string strFilePath = "";
                try
                {
                    strFilePath = chkItem;
                    byte[] fileBytes = File.ReadAllBytes(strFilePath);
                    fileBytes = Gzip(fileBytes);

                    request.FileBytes = fileBytes;
                    request.Configuration = "XX00000X0X00000001X1101010110X01101X1X000111111111111111110200000000001000010000000000000000000010000003";
                    //Service.Parsing.ParseResumeResponse response = ps.ParseResume(request);
                    ParserService.ParseResumeResponse response = ps.ParseResume(request);
                    if (response.Code.ToString() == "Success")
                    {
                        try
                        {
                            XmlDocument xmldatadoc = new XmlDocument();
                            xmldatadoc.LoadXml(response.Xml);
                            CopyDir = physicalPath + "\\Resources\\Parsing\\";
                            CopyDir = chkItem.Replace(PreviewDir, CopyDir);

                            foreach (string curr in replaceThese)
                            {
                                CopyDir = CopyDir.Replace(curr, string.Empty);
                            }

                            CopyDir = CopyDir + ".xml";
                            if (CopyDir.Contains("xml"))
                            {
                                xmldatadoc.Save(CopyDir);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    continue;
                }
            }
        }
        #endregion

        #region Page event
        protected void Page_Load(object sender, EventArgs e)
        {
            showresult.Visible = false;
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            //ChkUpdateDuplicate.Checked = false;
            string applicantIdsw = hdnSelectedIDS.Value;
            if (!IsPostBack)
            {
                BindList();
                txtSortColumn.Text = "btnDateTime";
                txtSortOrder.Text = "Desc";
                PlaceUpDownArrow();
                OnLineParserDetails();
            }
            currentSiteMapId = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap != null)             
            {
                IdForSitemap = CustomMap.Id;
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }

            lblMessage.Text = "";
            lblMessage.CssClass = "";

            string pagesize = "";
            pagesize = (Request.Cookies["OnLineParserRowPerPage"] == null ? "" : Request.Cookies["OnLineParserRowPerPage"].Value);
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvOnLineParserList.FindControl("pagerControl_Requisition");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
            }           
        }     
        #endregion
        private bool CheckSelectedList(int ID)
        {
            char[] del = { ',' };
            string[] arrID = hdnSelectedIDS.Value.Split(del, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in arrID)
            {
                if (Convert.ToInt32(s) == ID)
                {
                    return true;
                }
            }
            return false;
        }

        #region ListView Events
        protected void lsvOnLineParserList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _isAdmin = base.IsUserAdmin;

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                OnLineParser OnLineParser = ((ListViewDataItem)e.Item).DataItem as OnLineParser;

                if (OnLineParser != null)
                {
                    Label lblfilename = (Label)e.Item.FindControl("lblfilename");
                    Label lblparsestatus = (Label)e.Item.FindControl("lblparsestatus");
                    Label lbldbstatus = (Label)e.Item.FindControl("lbldbstatus");
                     HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
                                       
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
                    HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");
                    hdnID.Value = OnLineParser.OnLineParser_Id.ToString();
                   
                    CheckBox chkItemCandidate = (CheckBox)e.Item.FindControl("chkItemCandidate");
                    chkItemCandidate.Checked = CheckSelectedList(OnLineParser.OnLineParser_Id);

                    //lblfilename.Text = OnLineParser.Title ;
                    if (OnLineParser.MemebrId > 0)
                    {
                        lblfilename.Text = GetDocumentLink(OnLineParser.Title, "Word Resume", OnLineParser.MemebrId);
                    }
                    else
                    {
                        lblfilename.Text = OnLineParser.Title;
                    }

                    if (OnLineParser.ParseStatus > 0)
                    {
                        lblparsestatus.Text = "Successful";
                        lblparsestatus.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        lblparsestatus.Text = "Pending";
                        lblparsestatus.ForeColor = System.Drawing.Color.Red;
                    }

                    if (OnLineParser.DatabaseStatus > 0)
                    {
                        lbldbstatus.Text = "Successfully Added";
                        lbldbstatus.ForeColor = System.Drawing.Color.Green;
                        //CheckBox.Enabled = false;
                        //CheckBox.Checked= false;
                    }
                    else
                    {
                        lbldbstatus.Text = "Pending";
                        lbldbstatus.ForeColor = System.Drawing.Color.Red ;
                    }

                    if (OnLineParser.MemebrId > 0)    // code modified by Sumit Sonawane on 22/May/2017 for Issue Id 1326
                    {
                        Member member = Facade.GetMemberById(OnLineParser.MemebrId);
                        if (member != null)
                        {
                            string strFullName = member.FirstName + " " + member.LastName + " [A" + member.Id + "]";
                            ControlHelper.SetHyperLink(lnkCandidateName, UrlForEmployee, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(OnLineParser.MemebrId), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentSiteMapId);
                        }
                    }
                    else
                    {
                        lnkCandidateName.Text = "";
                    }

                    btnDelete.OnClientClick = "return ConfirmDelete('File')";
                    btnDelete.ToolTip = "Delete from List";
                    btnDelete.CommandArgument = StringHelper.Convert(OnLineParser.OnLineParser_Id);
                }
            }
        }
        protected void lsvOnLineParserList_PreRender(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlTableRow row = (System.Web.UI.HtmlControls.HtmlTableRow)lsvOnLineParserList.FindControl("row");
            if (row == null)
            {
                lsvOnLineParserList.DataSource = null;
                lsvOnLineParserList.DataBind();
            }
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvOnLineParserList.FindControl("pagerControl_Requisition");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null)
                {
                    hdnRowPerPageName.Value = "OnLineParserRowPerPage";
                }
            }
            PlaceUpDownArrow();

        }
        protected void lsvOnLineParserList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }

            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    OnLineParser OnLineParser = ((ListViewDataItem)e.Item).DataItem as OnLineParser;
                    try
                    {
                        OnLineParser OnLineParsing = Facade.OnLineParser_GetById(id);                     
                        PreviewXMLFile = OnLineParsing.XMLFilePath;
                        PreviewDOCFile = OnLineParsing.Path;
                        if (Facade.OnLineParser_DeleteById(id))
                        {
                            if (File.Exists(PreviewDOCFile))
                            {
                                File.Delete(PreviewDOCFile);
                            }
                            if (File.Exists(PreviewXMLFile))
                            {
                                File.Delete(PreviewXMLFile);
                            }                       

                            BindList();
                            MiscUtil.ShowMessage(lblMessage, "File has been succesfully removed.", false);
                            ClearText();
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
            BindList();
        }
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            string applicantIds = hdnSelectedIDS.Value;
            if (applicantIds != string.Empty)
            {
                uclConfirm.AddMessage("Are you sure that you want to delete this file(s)?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one file to remove.", true);
            }         
        }
        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                string[] CVIDs = hdnSelectedIDS.Value.Split(',');               
                int noofCandidates = 0;
                foreach (string s in CVIDs)
                {
                    int id = Convert.ToInt32(s);
                    if (id > 0)
                    {
                        Facade.OnLineParser_DeleteById(id);
                        noofCandidates++;
                    }    
                }
                BindList();
                hdnSelectedIDS.Value = "";
                MiscUtil.ShowMessage(lblMessage, "Successfully removed " + noofCandidates + " file(s).", false);
            }
        }       
        protected void btnParseToDatabase_Click(object sender, EventArgs e)
        {
            string applicantIds = hdnSelectedIDS.Value;
            if (applicantIds != string.Empty)
            {
                string[] CVIDs = hdnSelectedIDS.Value.Split(',');
                foreach (string s in CVIDs)
                {
                    totalcandidate = totalcandidate + 1;
                    int id = Convert.ToInt32(s);
                    GetxmlData(id);                   
                }
                countupdatecandidate = totalcandidate - countnewcandidate;
                if (ChkUpdateDuplicate.Checked == false)
                {
                    showresult.Visible = true;
                    Label2.Text = "Successfully Added - " + countnewcandidate.ToString();
                    Label3.Text = "Failed - " + countupdatecandidate.ToString();
                    Label4.Text = "  ";
                    //MiscUtil.ShowMessage(lblMessage, "Successfully Added - " + countnewcandidate.ToString() + " , Failed - " + countupdatecandidate.ToString() + " of Total" + totalcandidate.ToString() + " selected candidate(s) to database.", false);
                }
                else
                {
                    showresult.Visible = true;
                    Label2.Text = "Successfully Added - " + countnewcandidate.ToString();
                    Label3.Text = "Successfully Updated - " + countupdatecandidate.ToString();
                    Label4.Text = "  ";
                    //MiscUtil.ShowMessage(lblMessage, "Successfully Added - " + countnewcandidate.ToString() + " , Updated - " + countupdatecandidate.ToString() + " of Total" + totalcandidate.ToString() + " selected candidate(s) to database.", false);
                }                
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one file.", true);
            }
            hdnSelectedIDS.Value = "";
            foreach (ListViewDataItem item in lsvOnLineParserList.Items)
            {
                CheckBox chkItemCandidate = (CheckBox)item.FindControl("chkItemCandidate");
                chkItemCandidate.Checked = false;
            }
            ChkUpdateDuplicate.Checked = false;
            
        }
        protected void btnParse_Click(object sender, EventArgs e)
        {         
            string applicantIds = hdnSelectedIDS.Value;
            //Service.Parsing.ParsingService ps = new Service.Parsing.ParsingService();
            ParserService.ParsingService ps = new ParserService.ParsingService();

            ps.Url = "http://services.resumeparsing.com/ParsingService.asmx";
            //Service.Parsing.ParseResumeRequest request = new Service.Parsing.ParseResumeRequest();
            ParserService.ParseResumeRequest request = new ParserService.ParseResumeRequest();
            request.AccountId = "26410689";
            request.ServiceKey = "ZBNA0ueiKEB+IEBEJyeNnF9YS1oFAS+KTR03xIXI";
            request.OutputHtml = true;
            string[] replaceThese = { ".docx", ".doc", ".pdf", ".rtf", ".txt" };
            int praselimit=0;
            if (WebConfigurationManager.AppSettings["ParseLimit"].ToString() != string.Empty)
            {
                praselimit = Convert.ToInt32(WebConfigurationManager.AppSettings["ParseLimit"]);
            }
            int endlimit = 0;
            int parsedfailed = 0;
            int totalparse = 0;
            if (applicantIds != string.Empty)
            {
                string[] CVIDs = hdnSelectedIDS.Value.Split(',');
                #region
                foreach (string s in CVIDs)
                {
                    int id = Convert.ToInt32(s);
                    OnLineParser OP = Facade.OnLineParser_GetById(id);
                    if (OP != null)
                    {
                        int totalparsecount = Facade.OnLineParserCount_GetByUserId(0);

                        if (totalparsecount < praselimit)
                        {
                            string physicalPath = HttpContext.Current.Request.PhysicalApplicationPath;
                            string physicalPath1 = OP.Path;
                            string fileName78 = OP.Title;
                            try
                            {
                                byte[] fileBytes = File.ReadAllBytes(physicalPath1);
                                fileBytes = Gzip(fileBytes);

                                request.FileBytes = fileBytes;
                                request.Configuration = "XX00000X0X00000001X1101010110X01101X1X000111111111111111110200000000001000010000000000000000000010000003";
                                //Service.Parsing.ParseResumeResponse response = ps.ParseResume(request);
                                ParserService.ParseResumeResponse response = ps.ParseResume(request);
                                if (response.Code.ToString() == "Success")
                                {
                                    try
                                    {
                                        XmlDocument xmldatadoc = new XmlDocument();
                                        xmldatadoc.LoadXml(response.Xml);

                                        strFilePath = HttpContext.Current.Server.MapPath("../" + UrlConstants.ONLINE_PARSE_DIR);

                                        if (!Directory.Exists(strFilePath))
                                        {
                                            Directory.CreateDirectory(strFilePath);
                                        }
                                        CopyDir = strFilePath + "\\" + fileName78;
                                        //CopyDir = physicalPath + "Resources\\ParseCV\\" + fileName78;
                                        foreach (string curr in replaceThese)
                                        {
                                            CopyDir = CopyDir.Replace(curr, string.Empty);
                                        }
                                        CopyDir = CopyDir + ".xml";
                                        if (CopyDir.Contains("xml"))
                                        {
                                            xmldatadoc.Save(CopyDir);

                                            OP.XMLFilePath = CopyDir;
                                            OP.ParseStatus = 1;
                                            OP.DatabaseStatus = 0;
                                            OP.MemebrId = 0;
                                            Facade.OnLineParser_Update(OP);
                                            totalparse = totalparse + 1;

                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine(ex);

                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                parsedfailed = parsedfailed + 1;
                                continue;
                            }
                        }
                        else
                        {
                            endlimit = 1;                                           
                        }                        
                    }
                    else 
                    {
                        allreadyparse=allreadyparse+1;
                    }
                  
                    //BindList();
                }
                #endregion

               //int  alltotalparse=allreadyparse+totalparse;
                parsedfailed = allreadyparse + parsedfailed;
               if (endlimit == 1)
               {
                   MiscUtil.ShowMessage(lblMessage, "You have exceeded the Resume Parsing limit, Please contact administrator to extend the limit.", true);
               }
               else
               {
                   showresult.Visible = true;

                   Label2.Text = "    Parsed - " + totalparse.ToString();
                   Label3.Text = "";//"    already Parsed - " + allreadyparse.ToString();
                   Label4.Text = "    Failed  - " + parsedfailed.ToString();

                  // System.Threading.Thread.Sleep(10000);                   
                   //if (allreadyparse == 0)
                   //{
                   //    Label2.Text = "Successfully Parse selected files.";
                   //    MiscUtil.ShowMessage(lblMessage, "Successfully Parse selected files.", false);
                   //}
                   //else if (totalparse == 0)
                   //{
                   //    MiscUtil.ShowMessage(lblMessage, "Selected all files already Parse.", true);
                   //}
                   //else
                   //{
                   //    MiscUtil.ShowMessage(lblMessage, "Successfully Parse files " + totalparse.ToString() + " of " + alltotalparse.ToString() + " selected.", false);
                   //}
               }                
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one files .", true);
            }
            hdnSelectedIDS.Value = "";
            foreach (ListViewDataItem item in lsvOnLineParserList.Items)
            {
                CheckBox chkItemCandidate = (CheckBox)item.FindControl("chkItemCandidate");
                chkItemCandidate.Checked = false;
            }
            
        }
        //***********XML TO DATABASE *********************

        public void GetxmlData(int id)
        {
            MembershipUser newUser;
            Member member = new Member();
            MemberExperience memberExperience = new MemberExperience();
            MemberDetail memberdetail = new MemberDetail();
            MemberExtendedInformation memberExtendedInfo = new MemberExtendedInformation();
            MemberObjectiveAndSummary memberObjectiveAndSummary = new MemberObjectiveAndSummary();

            string password = "";
            string password1 = "";           

            OnLineParser OP = Facade.OnLineParser_GetById(id);
          
            PreviewXMLFile = OP.XMLFilePath;
            PreviewDOCFile = OP.Path;
          
            XmlDocument xmldoc = new XmlDocument();
            FileStream fs = null;
            try
            {
                fs = new FileStream(PreviewXMLFile, FileMode.Open, FileAccess.Read);
                xmldoc.Load(fs);

                //XDocument doc = XDocument.Load("E:\\Resumes\\AMITCHOUDHARY_CV.xml");    
                string UserEmail = "";
                UserEmail = ResumeValue_str(xmldoc, "sov:EmailAddresses", "sov:EmailAddress");
                if (UserEmail != "")
                {
                    Member membe = Facade.GetMemberByMemberEmail(UserEmail.Trim());                 
                    #region
                        if (membe == null)
                        {
                            cvstatus = true;
                            countnewcandidate = countnewcandidate + 1;
                            newUser = Membership.GetUser(UserEmail.Trim());
                            if (newUser != null)
                            {
                                Member mem1 = Facade.GetMemberByMemberEmail(UserEmail.Trim());
                                if (mem1 != null)
                                {
                                    newUser.IsApproved = true;
                                }
                                else
                                {
                                    newUser.IsApproved = true;
                                }
                            }
                            else
                            {
                                password = "changeme";
                                newUser = Membership.CreateUser(UserEmail.Trim(), password, UserEmail.Trim());
                            }
                            UserEmail = UserEmail.Trim();
                            System.Web.Security.MembershipUser user = System.Web.Security.Membership.GetUser(UserEmail);
                            try
                            {
                                password1 = user.ResetPassword();
                                user.ChangePassword(password, password1);
                                Membership.UpdateUser(user);
                            }
                            catch (Exception ex)
                            {
                            }

                            _role = ContextConstants.ROLE_CANDIDATE.ToLower();

                            if (newUser.IsApproved == true)
                            {
                                if (!Roles.IsUserInRole(newUser.UserName, _role))
                                {
                                    Roles.AddUserToRole(newUser.UserName, _role);
                                }
                                member.UserId = (Guid)newUser.ProviderUserKey;
                            }

                        }
                        else
                        {
                            if (ChkUpdateDuplicate.Checked == true)
                            {
                                cvstatus = true ;
                                member.Id = membe.Id;
                                member.UserId = membe.UserId;
                                if (!Roles.IsUserInRole(UserEmail, ContextConstants.ROLE_CANDIDATE.ToLower()))
                                {
                                    Roles.AddUserToRole(UserEmail, ContextConstants.ROLE_CANDIDATE.ToLower());
                                }
                            }
                            else
                            {
                                cvstatus = false ;
                            }
                        }
                        OP.DatabaseStatus = 0;
                        if (cvstatus == true)
                        {
                            BuildMember_Online(xmldoc, member);

                            Member memb = Facade.GetMemberByMemberEmail(UserEmail.Trim());
                            BuildMemberManager(memb.Id, CurrentMember.Id);
                            member.Id = memb.Id;
                            DeleteDetails(member.Id);

                            BuildMemberExperience_Online(xmldoc, memberExperience, "", member);
                            BuildMemberDetails_Online(xmldoc, memberdetail, member);
                            BuildMemberExtentedInformation_Online(xmldoc, memberExtendedInfo, member);

                            BuildMemberObjectiveAndSummary_Online(xmldoc, memberObjectiveAndSummary, member, PreviewDOCFile);

                            BuildMemberEducation_Online(xmldoc, member);
                            AddSkillMap_Online(xmldoc, member);
                            AddCertificationMap_Online(xmldoc, member);
                            UploadDocument(PreviewDOCFile, member, OP.Title);

                            OP.DatabaseStatus = 1;
                            OP.MemebrId = member.Id;
                            Facade.OnLineParser_Update(OP);
                        }
                        #endregion                    
                }
            }
            catch
            {
            }
            finally
            {
                if (fs != null)
                    fs.Close();
                if (cvstatus == true)
                {
                    if (OP.DatabaseStatus == 1)
                    {
                        //if (File.Exists(PreviewXMLFile))
                        //{
                        //    File.Delete(PreviewXMLFile);
                        //}
                        //if (File.Exists(PreviewDOCFile))
                        //{
                        //    File.Delete(PreviewDOCFile);
                        //}
                    }
                }
               // BindList();               
             }
        }    

        private string ResumeValue_str(XmlDocument xmldatadoc, string Parent_Node, string Child_Node)
        {
            string rtn = "";
            XmlNodeList xmlNodeList = xmldatadoc.GetElementsByTagName(Parent_Node);
            foreach (XmlNode childNode in xmlNodeList)
            {
                foreach (XmlNode xmlnode in childNode)
                {
                    if (xmlnode.Name == Child_Node)
                    {
                        rtn = xmlnode.InnerText;
                    }
                }
            }
            return rtn;
        }
        private int ResumeValue_int(XmlDocument xmldatadoc, string Parent_Node, string Child_Node)
        {
            int rtn = 0;
            XmlNodeList xmlNodeList = xmldatadoc.GetElementsByTagName(Parent_Node);
            foreach (XmlNode childNode in xmlNodeList)
            {
                foreach (XmlNode xmlnode in childNode)
                {
                    if (xmlnode.Name == Child_Node)
                    {
                        rtn = Convert.ToInt32(xmlnode.InnerText);
                    }
                    else
                    {
                        rtn = 0;
                    }
                }
            }
            return rtn;
        }
        private DateTime ResumeValue_datetime(XmlDocument xmldatadoc, string Parent_Node, string Child_Node)
        {
            DateTime rtn = DateTime.MinValue;

            XmlNodeList xmlNodeList = xmldatadoc.GetElementsByTagName(Parent_Node);
            foreach (XmlNode childNode in xmlNodeList)
            {
                foreach (XmlNode xmlnode in childNode)
                {
                    if (xmlnode.Name == Child_Node)
                    {
                        if ((xmlnode.InnerText) != null || (xmlnode.InnerText != ""))
                        {
                            rtn = Convert.ToDateTime(xmlnode.InnerText);
                        }
                    }

                }
            }
            return rtn;
        }
        public void BuildMemberManager(int memberid, int CurrentMember)
        {

            Facade.BuildMemberManager_ByMemberId(memberid, CurrentMember);
        }

        public void DeleteDetails(int memberid)
        {
            //Change MemberDetail
            Facade.DeleteMemberDetailByMemberId(memberid);

            //Delete MemberExperience
            Facade.DeleteMemberExperienceByMemberId(memberid);

            //DeleteMemberEducation
            Facade.DeleteMemberEducationByMemberId(memberid);

            //Delete MemberObjective and Summary
            //Facade.DeleteMemberObjectiveAndSummaryByMemberId(_memberId);

            //Delete MemberSkillmap
            Facade.DeleteMemberSkillMapByMemberId(memberid);

            //Delete MemberCertificationMap
            Facade.DeleteMemberCertificationMapByMemberId(memberid);

            //Delete MemberExtended information
            Facade.DeleteMemberExtendedInformationByMemberId(memberid);
        }
        public void BuildMember_Online(XmlDocument xmldatadoc, Member member)
        {
            string OutValue = string.Empty;
            string country;

            member.PrimaryEmail = ResumeValue_str(xmldatadoc, "sov:EmailAddresses", "sov:EmailAddress");
            member.FirstName = ResumeValue_str(xmldatadoc, "PersonName", "GivenName");
            member.MiddleName = ResumeValue_str(xmldatadoc, "PersonName", "MiddleName");
            member.LastName = ResumeValue_str(xmldatadoc, "PersonName", "FamilyName");
            member.NickName = OutValue;
            member.PermanentAddressLine1 = ResumeValue_str(xmldatadoc, "DeliveryAddress", "AddressLine");
            member.PermanentAddressLine2 = ResumeValue_str(xmldatadoc, "PostalAddress", "Municipality");
            member.PermanentCity = ResumeValue_str(xmldatadoc, "PostalAddress", "Municipality");
            //baseparser.GetState(out OutValue);
            //member.PermanentStateId = Facade.GetStateIdByStateCode(OutValue);  
            country = ResumeValue_str(xmldatadoc, "sov:Culture", "sov:Country");

            string countrycode = ResumeValue_str(xmldatadoc, "PostalAddress", "CountryCode");
            member.PermanentCountryId = Facade.GetCountryIdByCountryCode(countrycode);
            member.PermanentZip = ResumeValue_str(xmldatadoc, "PostalAddress", "PostalCode");

            member.PrimaryPhone = ResumeValue_str(xmldatadoc, "Telephone", "FormattedNumber");
            member.CellPhone = ResumeValue_str(xmldatadoc, "Mobile", "FormattedNumber");
            member.DateOfBirth = ResumeValue_datetime(xmldatadoc, "sov:PersonalInformation", "sov:DateOfBirth");

            //baseparser.GetState(out OutValue);
            member.PermanentStateId = Facade.GetStateIdByStateCode(OutValue);
            member.PermanentCountryId = Facade.GetCountryIdByCountryCode(OutValue);

            member.CreatorId = CurrentMember.Id;
            member.UpdatorId = CurrentMember.Id;
            EmployeeReferral reff = Facade.EmployeeReferral_GetByMemberId(member.Id);
            if (reff == null)
            {
                member.Status = 1;
            }
            if (member.Id > 0)
            {
                Facade.UpdateMember(member);
            }
            else
            {
                Facade.AddMember(member);
            }
        }
        public void BuildMemberExperience_Online(XmlDocument xmldatadoc, MemberExperience m_memberExperience, string CountryName, Member member)
        {
            XmlNodeList EmploymentHistory_xmlNodeList = xmldatadoc.GetElementsByTagName("EmploymentHistory");
            m_memberExperience.MemberId = member.Id;
            DateTime TempDate;
            if (EmploymentHistory_xmlNodeList != null)
            {
                if (m_memberExperience.MemberId > 0)
                {
                    Facade.DeleteMemberExperienceByMemberId(m_memberExperience.MemberId);
                }
            }
            foreach (XmlNode EmploymentHistory_childNode in EmploymentHistory_xmlNodeList)
            {
                foreach (XmlNode xmlnode in EmploymentHistory_childNode)
                {
                    if (xmlnode.Name == "EmployerOrg")
                    {
                        //foreach (XmlNode node in xmlnode)
                        //{
                        //if (node.Name == "EmployerOrg")
                        //{
                        foreach (XmlNode EmploymentHistory_node in xmlnode)
                        {
                            //MemberExperience m_memberExperience = new MemberExperience();
                            //m_memberExperience.MemberId = _memberId;
                            if (EmploymentHistory_node.Name == "EmployerOrgName")
                            {
                                m_memberExperience.CompanyName = EmploymentHistory_node.InnerText;
                            }
                            if (EmploymentHistory_node.Name == "Description") //roles and Responsibilities
                            {
                                m_memberExperience.Responsibilities = EmploymentHistory_node.InnerText;
                            }
                            if (EmploymentHistory_node.Name == "PositionHistory")
                            {
                                //m_memberExperience.PositionName = EmploymentHistory_node.ChildNodes.Item(0).InnerText;
                                //m_memberExperience.DateFrom = EmploymentHistory_node.ChildNodes.Item(3).InnerText.ToString() == "" ? Convert.ToDateTime("01/01/1970") : Convert.ToDateTime(EmploymentHistory_node.ChildNodes.Item(3).InnerText.ToString());
                                //m_memberExperience.DateTo = EmploymentHistory_node.ChildNodes.Item(4).InnerText.ToString() == "" ? Convert.ToDateTime("01/01/1970") : Convert.ToDateTime(EmploymentHistory_node.ChildNodes.Item(4).InnerText.ToString());
                                //m_memberExperience.Responsibilities = EmploymentHistory_node.ChildNodes.Item(2).InnerText;

                                //Code introduced by Prasanth on 10Apr2017 Issue id 724 Start
                                                              //m_memberExperience.PositionName = EmploymentHistory_node.ChildNodes.Item(0).InnerText;
                                //m_memberExperience.DateFrom = EmploymentHistory_node.ChildNodes.Item(3).InnerText.ToString() == "" ? Convert.ToDateTime("01/01/1970") : Convert.ToDateTime(EmploymentHistory_node.ChildNodes.Item(3).InnerText.ToString());
                                //m_memberExperience.DateTo = EmploymentHistory_node.ChildNodes.Item(4).InnerText.ToString() == "" ? Convert.ToDateTime("01/01/1970") : Convert.ToDateTime(EmploymentHistory_node.ChildNodes.Item(4).InnerText.ToString());
                                //m_memberExperience.Responsibilities = EmploymentHistory_node.ChildNodes.Item(2).InnerText;
                                //***************************End**************************


                                foreach (XmlNode EmploymentHistory_Chilednode in EmploymentHistory_node)
                                {
                                    if (EmploymentHistory_Chilednode.Name == "StartDate")
                                    {
                                        foreach (XmlNode AnyDate_node in EmploymentHistory_Chilednode)
                                        {
                                            if (AnyDate_node.Name == "AnyDate" && DateTime.TryParse(AnyDate_node.InnerText, out TempDate))
                                            {
                                                //Line Uncommented by Prasanth on 10Apr2017 Issue id 724
                                                m_memberExperience.DateFrom = TempDate; //Convert.ToDateTime(AnyDate_node.InnerText);
                                            }
                                        }
                                    }
                                    else if (EmploymentHistory_Chilednode.Name == "EndDate" )
                                    {
                                        foreach (XmlNode AnyDate_node in EmploymentHistory_Chilednode)
                                        {
                                            if (AnyDate_node.Name == "AnyDate" && DateTime.TryParse(AnyDate_node.InnerText, out TempDate))
                                            {
                                                //Line Uncommented by Prasanth on 10Apr2017 Issue id 724
                                                m_memberExperience.DateTo = TempDate; //Convert.ToDateTime(AnyDate_node.InnerText);
                                            }
                                        }
                                    }
                                    else if (EmploymentHistory_Chilednode.Name == "Description")
                                    {

                                        m_memberExperience.Responsibilities = EmploymentHistory_Chilednode.InnerText;

                                    }
                                    else if (EmploymentHistory_Chilednode.Name == "Title")
                                    {

                                        m_memberExperience.PositionName = EmploymentHistory_Chilednode.InnerText;

                                    }
                                    else if (EmploymentHistory_Chilednode.Name == "OrgInfo")
                                    {
                                        foreach (XmlNode OrgInfo_node in EmploymentHistory_Chilednode)
                                        {
                                            if (OrgInfo_node.Name == "PositionLocation")
                                            {
                                                foreach (XmlNode PositionLocation_node in OrgInfo_node)
                                                {
                                                    if (PositionLocation_node.Name == "CountryCode")
                                                    {
                                                        //Code uncommented by Prasanth on 10Apr2017 Issue id 724
                                                        m_memberExperience.CountryLookupId = Facade.GetCountryIdByCountryCode(PositionLocation_node.InnerText);
                                                    }
                                                    else if (PositionLocation_node.Name == "Region")
                                                    {
                                                        //Code uncommented by Prasanth on 10Apr2017 Issue id 724
                                                        m_memberExperience.StateId = Facade.GetCountryIdByCountryCode(PositionLocation_node.InnerText);
                                                    }
                                                    else if (PositionLocation_node.Name == "Municipality")
                                                    {
                                                        m_memberExperience.City = PositionLocation_node.InnerText;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }

                            if (EmploymentHistory_node.Name == "OrgInfo")
                            {
                                foreach (XmlNode OrgInfo_node in xmlnode)
                                {
                                    if (OrgInfo_node.Name == "PositionLocation")
                                    {
                                        foreach (XmlNode OrgInfo1_node in xmlnode)
                                        {
                                            if (OrgInfo1_node.Name == "CountryCode")
                                            {
                                                m_memberExperience.CountryLookupId = Facade.GetCountryIdByCountryCode(OrgInfo_node.InnerText);
                                            }
                                            if (OrgInfo1_node.Name == "Region")
                                            {
                                                //m_memberExperience.StateId = Facade.GetCountryIdByCountryCode(Region_node.InnerText);
                                            }
                                            if (OrgInfo1_node.Name == "Municipality")
                                            {
                                                m_memberExperience.City = OrgInfo1_node.InnerText;
                                            }
                                        }
                                    }
                                }
                            }
                            //Write code here for saving values into database
                            //}
                        }

                        m_memberExperience.CreatorId = m_memberExperience.UpdatorId = CurrentMember.Id;
                        Facade.AddMemberExperience(m_memberExperience);
                    }
                }
            }
        }
        public void BuildMemberDetails_Online(XmlDocument xmldatadoc, MemberDetail memberDetail, Member member)
        {
            memberDetail.MemberId = member.Id;
            //if (SelectedState != "") if (memberDetail.CurrentStateId == 0) memberDetail.CurrentStateId = Convert.ToInt32(SelectedState);
            //if (SelectedCountry != "") if (memberDetail.CurrentCountryId == 0) memberDetail.CurrentCountryId = Convert.ToInt32(SelectedCountry); ;
            //memberDetail.OfficePhone = OfficeNumber;
            //memberDetail.HomePhone = HomeNumber;
            string strGend = string.Empty;

            XmlNodeList PersonalInfo_xmlNodeList = xmldatadoc.GetElementsByTagName("sov:PersonalInformation");
            if (PersonalInfo_xmlNodeList.Count > 0)
            {
                foreach (XmlNode PersonalInfo_childNode in PersonalInfo_xmlNodeList)
                {
                    foreach (XmlNode xmlnode in PersonalInfo_childNode)
                    {
                        if (xmlnode.Name == "sov:Gender")
                        {
                            if (xmlnode.InnerText.Trim() != null) strGend = xmlnode.InnerText.Trim();
                            //else if (xmlnode.InnerText.Contains(" Female ")) strGend = "Female";

                            if (strGend == "Male" || strGend == "Female")
                            {
                                IList<GenericLookup> lookup = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.Gender, strGend.Trim());
                                foreach (GenericLookup gen in lookup)
                                {
                                    if (gen.Name == strGend)
                                        memberDetail.GenderLookupId = gen.Id;
                                }
                            }
                            else
                                memberDetail.GenderLookupId = 0;

                        }
                        if (xmlnode.Name == "sov:Gender")
                        {
                            string strMrStatus = string.Empty;
                            if (xmlnode.InnerText.Contains("Married")) strMrStatus = "Married";
                            else if (xmlnode.InnerText.Contains("Single") || xmlnode.InnerText.Contains(" Unmarried ") || xmlnode.InnerText.Contains(" UnMarried ") || xmlnode.InnerText.Contains(" Un Married ")) strMrStatus = "Single";
                            else if (xmlnode.InnerText.Contains("Divorced")) strMrStatus = "Divorced";
                            else if (xmlnode.InnerText.Contains("Widow")) strMrStatus = "Widow";
                            else if (xmlnode.InnerText.Contains("Separated ")) strMrStatus = "Separated";
                            if (strMrStatus == "Married" || strMrStatus == "Single" || strMrStatus == "Divorced" || strMrStatus == "Widow" || strMrStatus == "Separated")
                            {
                                IList<GenericLookup> lookup = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.MaritalStatus, strMrStatus.Trim());
                                foreach (GenericLookup gen in lookup)
                                {
                                    if (gen.Name == strMrStatus)
                                        memberDetail.MaritalStatusLookupId = gen.Id;
                                }
                            }
                            else
                                memberDetail.MaritalStatusLookupId = 0;
                        }
                    }
                    memberDetail.NumberOfChildren = 0;
                    memberDetail.CreatorId = memberDetail.UpdatorId = CurrentMember.Id;

                    if (memberDetail.Id > 0) Facade.UpdateMemberDetail(memberDetail);
                    else Facade.AddMemberDetail(memberDetail);
                }
            }
            else
            {
                if (memberDetail.Id > 0) Facade.UpdateMemberDetail(memberDetail);
                else Facade.AddMemberDetail(memberDetail);
            }

        }
        public void BuildMemberExtentedInformation_Online(XmlDocument xmldatadoc, MemberExtendedInformation memberExtendedInfo, Member member)
        {
            memberExtendedInfo.MemberId = member.Id;
            string OutValue = string.Empty;
            float dblValue = 0;
            int intValue = 0;
            //baseparser.GetWorkPermit(out intValue); memberExtendedInfo.WorkAuthorizationLookupId = intValue;
            //baseparser.GetCurrentPosition(out OutValue); memberExtendedInfo.CurrentPosition = OutValue;
            memberExtendedInfo.WorkAuthorizationLookupId = intValue;
            memberExtendedInfo.CurrentPosition = OutValue;
            memberExtendedInfo.LastEmployer = OutValue;
            

            XmlNode xn;
            XmlNodeList EmploymentHistory_xmlNodeList = xmldatadoc.GetElementsByTagName("EmploymentHistory");
            foreach (XmlNode EmploymentHistory_childNode in EmploymentHistory_xmlNodeList)
            {
                foreach (XmlNode xmlnode in EmploymentHistory_childNode)
                {
                    foreach (XmlNode xmlchildnode in xmlnode)
                    {

                        if (xmlchildnode.Name == "PositionHistory")
                        {

                            if (xmlchildnode.Attributes != null && xmlchildnode.Attributes["currentEmployer"] != null && xmlchildnode.Attributes["currentEmployer"].Value == "true")
                            {
                                memberExtendedInfo.CurrentPosition = xmlchildnode.ChildNodes.Item(0).InnerText;
                                memberExtendedInfo.LastEmployer = xmlchildnode.ChildNodes.Item(1).InnerText;
                                break;
                            }

                            //xn = xmlchildnode.Attributes.GetNamedItem("currentEmployer");
                            //if (xn.Value == "true")
                            //{
                            //    memberExtendedInfo.CurrentPosition = xmlchildnode.ChildNodes.Item(0).InnerText;
                            //    memberExtendedInfo.LastEmployer = xmlchildnode.ChildNodes.Item(1).InnerText;
                            //    break;
                            //}
                        }
                        
                    }
                    //break;
                }
                //break;
            }

            memberExtendedInfo.TotalExperienceYears = ResumeValue_str(xmldatadoc, "sov:ExperienceSummary", "sov:YearsOfWorkExperience");
            //memberExtendedInfo.PassportStatus = ResumeValue_str(xmldatadoc, "sov:PersonalInformation", "sov:PassportStatus");
            //memberExtendedInfo.SecurityClearance = ResumeValue_str(xmldatadoc, "sov:PersonalInformation", "sov:SecurityClearance");
            memberExtendedInfo.PassportNumber = ResumeValue_str(xmldatadoc, "sov:PersonalInformation", "sov:PassportNumber");
            memberExtendedInfo.SourceLookupId = 22;
            memberExtendedInfo.SourceDescription = "";
            memberExtendedInfo.IdCardLookUpId = 22;
            memberExtendedInfo.IdCardDetail = "";
            memberExtendedInfo.LinkedinProfile = "";

            memberExtendedInfo.CreatorId = memberExtendedInfo.UpdatorId = CurrentMember.Id;
            if (memberExtendedInfo.Id > 0) Facade.UpdateMemberExtendedInformation(memberExtendedInfo);
            else Facade.AddMemberExtendedInformation(memberExtendedInfo);
        }
        public void BuildMemberObjectiveAndSummary_Online(XmlDocument xmldatadoc, MemberObjectiveAndSummary memberObjectiveAndSummary, Member member, string PreviewDOCFile)
        {
            string RawResumeText = string.Empty;
            string HTMLCopyPasteResume = string.Empty;
            Parser.DocumentConverter document = new Parser.DocumentConverter();

            memberObjectiveAndSummary.MemberId = member.Id;
            string OutValue = string.Empty;
            memberObjectiveAndSummary.ResumeHighlight = ResumeValue_str(xmldatadoc, "sov:ExperienceSummary", "sov:Description");
            memberObjectiveAndSummary.Summary = ResumeValue_str(xmldatadoc, "StructuredXMLResume", "ExecutiveSummary");       
       

               XmlNodeList ObjectiveNodeList = xmldatadoc.GetElementsByTagName("Objective");

               foreach (XmlNode ObjectiveNodeList_childNode in ObjectiveNodeList)
               {
                   foreach (XmlNode xmlnode in ObjectiveNodeList_childNode)
                   {
                       if (xmlnode.Name == "#text")
                       {
                           memberObjectiveAndSummary.Objective = xmlnode.InnerText;
                       }
                   }
               }


            //baseparser.GetObjective(out OutValue); memberObjectiveAndSummary.Objective = OutValue;
            //baseparser.GetSummary(out OutValue); memberObjectiveAndSummary.Summary = OutValue;


            MemberObjectiveAndSummary objectiveandSummary = Facade.GetMemberObjectiveAndSummaryByMemberId(member.Id);

            if (File.Exists(PreviewDOCFile))
            {
                //RawResumeText = Parser.DocumentConverter.GetResumeText(PreviewDOCFile);
                //RawResumeText = TransformXMLToHTML(xmldatadoc, RawResumeText);
                try
                {
                    byte[] data = document.ConvertTo(PreviewDOCFile, Parser.DocumentConverter.OutputTypes.HtmlFormatted);
                    HTMLCopyPasteResume = System.Text.ASCIIEncoding.ASCII.GetString(data);
                }
                catch (System.Exception ex)
                {
                }
                memberObjectiveAndSummary.RawCopyPasteResume = HttpUtility.HtmlDecode(RawResumeText);
                memberObjectiveAndSummary.CopyPasteResume = HTMLCopyPasteResume;
            }
           

            if (objectiveandSummary != null)
            {
                objectiveandSummary.MemberId = member.Id;
                objectiveandSummary.CreatorId = objectiveandSummary.UpdatorId = CurrentMember.Id;
                objectiveandSummary.Id = objectiveandSummary.Id;
                objectiveandSummary.Objective = memberObjectiveAndSummary.Objective;
                objectiveandSummary.Summary = memberObjectiveAndSummary.Summary;
                objectiveandSummary.RawCopyPasteResume = memberObjectiveAndSummary.RawCopyPasteResume;
                  objectiveandSummary.CopyPasteResume =memberObjectiveAndSummary.CopyPasteResume ;
                  objectiveandSummary.ResumeHighlight = memberObjectiveAndSummary.ResumeHighlight;
                Facade.UpdateMemberObjectiveAndSummary(objectiveandSummary);
            }
            else
            {              
                memberObjectiveAndSummary.MemberId = member.Id;
                memberObjectiveAndSummary.CreatorId = memberObjectiveAndSummary.UpdatorId = CurrentMember.Id;
                Facade.AddMemberObjectiveAndSummary(memberObjectiveAndSummary);
            }

        }
     
     
        public void BuildMemberEducation_Online(XmlDocument xmldatadoc, Member member)
        {
            MemberEducation m_memberEducation = new MemberEducation();
            XmlNodeList EducationHistory_xmlNodeList = xmldatadoc.GetElementsByTagName("EducationHistory");
            foreach (XmlNode EducationHistory_childNode in EducationHistory_xmlNodeList)
            {
                if (EducationHistory_childNode.Name == "EducationHistory")
                {
                    foreach (XmlNode SchoolOrInstitutionMain_xmlnode in EducationHistory_childNode)
                    {
                        if (SchoolOrInstitutionMain_xmlnode.Name == "SchoolOrInstitution")
                        {
                            foreach (XmlNode SchoolOrInstitution_xmlnode in SchoolOrInstitutionMain_xmlnode)
                            {
                                if (SchoolOrInstitution_xmlnode.Name == "School")
                                {
                                    foreach (XmlNode SchoolOrInstitution_childnode in SchoolOrInstitution_xmlnode)
                                    {
                                        if (SchoolOrInstitution_childnode.Name == "SchoolName")
                                        {
                                            m_memberEducation.InstituteName = SchoolOrInstitution_childnode.InnerText;
                                        }
                                    }
                                }
                                else if (SchoolOrInstitution_xmlnode.Name == "Degree")
                                {
                                    //foreach (XmlNode SchoolOrInstitution_childnode in SchoolOrInstitution_xmlnode)
                                    //{
                                    //    if (SchoolOrInstitution_childnode.Name == "Degree")
                                    //    {

                                    DateTime dateTime;
                                    foreach (XmlNode Degree_xmlnode in SchoolOrInstitution_xmlnode)
                                    {
                                        if (Degree_xmlnode.Name == "DegreeName")
                                        {
                                            m_memberEducation.DegreeTitle = Degree_xmlnode.InnerText;
                                        }
                                        else if (Degree_xmlnode.Name == "DegreeDate")
                                        {

                                            foreach (XmlNode DegreeDate_xmlnode in Degree_xmlnode)
                                            {
                                                if ((DegreeDate_xmlnode.Name == "AnyDate") && DateTime.TryParse(Degree_xmlnode.InnerText, out dateTime))
                                                {
                                                    m_memberEducation.DegreeObtainedYear = Degree_xmlnode.InnerText;

                                                }
                                            }

                                        }
                                        else if (Degree_xmlnode.Name == "DegreeMeasure" && Degree_xmlnode.InnerXml.Contains("EducationalMeasure"))
                                        {
                                            if (Degree_xmlnode.FirstChild.ChildNodes[1].FirstChild != null)
                                                m_memberEducation.GPA = Degree_xmlnode.FirstChild.ChildNodes[1].FirstChild.InnerText;
                                        }

                                        else if (Degree_xmlnode.Name == "DegreeMajor")
                                        {

                                            foreach (XmlNode DegreeMajor_xmlnode in Degree_xmlnode)
                                            {
                                                if (DegreeMajor_xmlnode.Name == "Name")
                                                {
                                                    m_memberEducation.MajorSubjects = DegreeMajor_xmlnode.InnerText;

                                                }
                                            }
                                        }
                                        else if (Degree_xmlnode.Name == "DatesOfAttendance")
                                        {
                                            foreach (XmlNode DatesOfAttendance_xmlnode in Degree_xmlnode)
                                            {
                                                if (DatesOfAttendance_xmlnode.Name == "StartDate")
                                                {
                                                    foreach (XmlNode StartDate_xmlnode in DatesOfAttendance_xmlnode)
                                                    {
                                                        if ((StartDate_xmlnode.Name == "AnyDate") && DateTime.TryParse(StartDate_xmlnode.InnerText, out dateTime))
                                                        {
                                                            m_memberEducation.AttendedFrom = dateTime;

                                                        }
                                                    }
                                                }
                                                else if (DatesOfAttendance_xmlnode.Name == "EndDate")
                                                {
                                                    foreach (XmlNode EndDate_xmlnode in DatesOfAttendance_xmlnode)
                                                    {
                                                        if ((EndDate_xmlnode.Name == "AnyDate") && DateTime.TryParse(EndDate_xmlnode.InnerText, out dateTime))
                                                        {
                                                            m_memberEducation.AttendedTo = dateTime;

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else if (Degree_xmlnode.Name == "Comments")
                                        {
                                            m_memberEducation.BriefDescription = Degree_xmlnode.InnerText;
                                        }

                                       
                                    }

                                }
                                //Write code here to save values into database
                                //m_memberEducation.MemberId = member.Id;                              
                                //Facade.AddMemberEducation(m_memberEducation);
                            }
                            m_memberEducation.MemberId = member.Id;
                            Facade.AddMemberEducation(m_memberEducation);
                        }
                    }
                }
            }
            //DataTable EducatationInfo = new DataTable();
            //baseparser.GetEducation(out EducatationInfo);
            //IList<MemberEducation> membereducation = new List<MemberEducation>();
            //if (membereducation != null && EducatationInfo != null)
            //{
            //    for (int i = 0; i < EducatationInfo.Rows.Count; i++)
            //    {
            //        MemberEducation m_memberEducation = new MemberEducation();
            //        m_memberEducation.MemberId = _memberId;
            //        m_memberEducation.DegreeTitle = EducatationInfo.Rows[i][1].ToString();
            //        m_memberEducation.InstituteName = EducatationInfo.Rows[i][8].ToString();
            //        m_memberEducation.DegreeObtainedYear = EducatationInfo.Rows[i][7].ToString();
            //        m_memberEducation.MajorSubjects = EducatationInfo.Rows[i][2].ToString();
            //        m_memberEducation.LevelOfEducationLookupId = Convert.ToInt32(EducatationInfo.Rows[i][14]);
            //        m_memberEducation.GPA = EducatationInfo.Rows[i][3].ToString();
            //        m_memberEducation.GpaOutOf = EducatationInfo.Rows[i][4].ToString();
            //        m_memberEducation.AttendedFrom = EducatationInfo.Rows[i][5].ToString() == "" ? Convert.ToDateTime("01/01/1753") : Convert.ToDateTime(EducatationInfo.Rows[i][5]);
            //        m_memberEducation.AttendedTo = EducatationInfo.Rows[i][6].ToString() == "" ? Convert.ToDateTime("01/01/1753") : Convert.ToDateTime(EducatationInfo.Rows[i][6]);
            //        m_memberEducation.InstituteCountryId = Convert.ToInt32(EducatationInfo.Rows[i][15]);
            //        m_memberEducation.InstituteCity = EducatationInfo.Rows[i][11].ToString();
            //        m_memberEducation.BriefDescription = EducatationInfo.Rows[i][12].ToString();
            //        m_memberEducation.IsHighestEducation = Convert.ToBoolean(EducatationInfo.Rows[i][13]);
            //        m_memberEducation.StateId = Convert.ToInt32(EducatationInfo.Rows[i][16]);
            //        Facade.AddMemberEducation(m_memberEducation);
            //    }
            //}
        }



       


        public void AddSkillMap_Online(XmlDocument xmldatadoc, Member member)
        {

            MemberSkillMap memberskill = new MemberSkillMap();
            string skillname;
            XmlNodeList Qualifications_xmlNodeList = xmldatadoc.GetElementsByTagName("Qualifications");
            foreach (XmlNode Qualifications_childNode in Qualifications_xmlNodeList)
            {
                if (Qualifications_childNode.Name == "Qualifications")
                {
                    foreach (XmlNode QualificationsMain_childNode in Qualifications_childNode)
                    {

                        if (QualificationsMain_childNode.Name == "Competency")
                        {
                            foreach (XmlNode Competency_xmlnode in QualificationsMain_childNode)
                            {
                                if (Competency_xmlnode.Name == "CompetencyEvidence")
                                {
                                    foreach (XmlNode Competency_childnode in Competency_xmlnode)
                                    {
                                        if (Competency_childnode.Name == "NumericValue")
                                        {
                                            memberskill.YearsOfExperience = Convert.ToInt32(Competency_childnode.InnerText) / 12;
                                            skillname = Competency_xmlnode.Attributes.Item(0).Value;
                                            memberskill.SkillId = Facade.GetSkillIdBySkillName(skillname);
                                            if (Competency_xmlnode.Attributes.Count > 3)
                                            {
                                                memberskill.LastUsed = Convert.ToDateTime(Competency_xmlnode.Attributes.Item(3).Value);
                                            }
                                            else
                                            {
                                                memberskill.LastUsed = DateTime.MinValue;
                                            }

                                            if (memberskill.SkillId > 0)
                                            {
                                                memberskill.MemberId = member.Id;
                                                Facade.AddMemberSkillMap(memberskill);
                                            }
                                        }                                        
                                    
                                    }
                                }
                            }
                        }
                    }
                }
            }          
        }
        public void AddCertificationMap_Online(XmlDocument xmldatadoc, Member member)
        {
            DataTable certicationmap = new DataTable();          
            //XmlNodeList Qualifications_xmlNodeList = xmldatadoc.GetElementsByTagName("Qualifications");
            //foreach (XmlNode Qualifications_childNode in Qualifications_xmlNodeList)
            //{
            //    if (Qualifications_childNode.Name == "Qualifications")
            //    {
            //        foreach (XmlNode QualificationsMain_childNode in Qualifications_childNode)
            //        {
            //        }
            //    }
            //}
            //for (int i = 0; i < certicationmap.Rows.Count; i++)
            //{
            //    MemberCertificationMap memberCertificationMap = new MemberCertificationMap();
            //    memberCertificationMap.MemberId = _memberId;
            //    memberCertificationMap.CerttificationName = certicationmap.Rows[i][0].ToString();
            //    if (certicationmap.Rows[i][1] != System.DBNull.Value && certicationmap.Rows[i][1].ToString().Trim() != "") memberCertificationMap.ValidFrom = Convert.ToDateTime(certicationmap.Rows[i][1].ToString());
            //    if (certicationmap.Rows[i][2] != System.DBNull.Value && certicationmap.Rows[i][2].ToString().Trim() != "") memberCertificationMap.ValidTo = Convert.ToDateTime(certicationmap.Rows[i][2].ToString());
            //    memberCertificationMap.IssuingAthority = certicationmap.Rows[i][3].ToString();
            //    Facade.AddMemberCertificationMap(memberCertificationMap);
            //}

        }
        private void UploadDocument(string PreviewDOCFile, Member newMember, string Title)
        {
            if (PreviewDOCFile!="")
            {
                Member member = Facade.GetMemberById(newMember.Id);
                string strMessage = String.Empty;
                string _fileName = string.Empty;
                lblMessage.Text = string.Empty;
                bool boolError = false;
                string UploadedFilename = Convert.ToString(PreviewDOCFile);
                string[] FileName_Split = UploadedFilename.Split('.');
                string ResumeName = member.FirstName + member.LastName + " - Resume." + FileName_Split[FileName_Split.Length - 1];
                //UploadedFilename = ResumeName;
                UploadedFilename = Title;
                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, newMember.Id, UploadedFilename, "Word Resume", false);//1.6
                if (CheckFileSize(PreviewDOCFile))
                {                    
                    //fuDocument.SaveAs(strFilePath);                   
                       if (File.Exists(strFilePath))
                      {
                        File.Delete(strFilePath);
                      }
                       //System.IO.File.Move (PreviewDOCFile, strFilePath);
                        System.IO.File.Copy(PreviewDOCFile, strFilePath);
                        MemberDocument memberDocument = Facade.GetMemberDocumentByMemberIdAndFileName(newMember.Id, UploadedFilename);//1.6
                        if (memberDocument == null)
                        {
                            MemberDocument newDoc = new MemberDocument();
                            newDoc.FileName = UploadedFilename;
                            newDoc.MemberId = newMember.Id;
                            newDoc.FileTypeLookupId = 55;
                            newDoc.Title = "Primary Resume";
                            Facade.AddMemberDocument(newDoc);
                            MiscUtil.AddActivity(_role, newMember.Id, 0, ActivityType.UploadDocument, Facade);
                            //if (string.IsNullOrEmpty(strMessage))
                            //{
                            //    strMessage = "Successfully uploaded the file";
                            //}
                        }
                        else
                        {
                            memberDocument.FileName = UploadedFilename;
                            memberDocument.MemberId = newMember.Id;
                            Facade.UpdateMemberDocument(memberDocument);
                            MiscUtil.AddActivity(_role, newMember.Id, base.CurrentMember != null ? base.CurrentMember.Id : 0, ActivityType.UploadDocument, Facade);
                           // strMessage = "Successfully updated the file";

                        }
                   
                }
                else
                {
                    boolError = true;
                    strMessage = "File size should be less than 10 MB";
                }
               // MiscUtil.ShowMessage(lblMessage, strMessage + lblMessage.Text, boolError);
            }

        }
        private bool CheckFileSize(string PreviewDOCFile)
        {
            //PreviewDOCFile
            //int fileSize = Convert.ToInt32(fuDocument.FileContent.Length / (1024 * 1024));
            //if (fileSize > ContextConstants.MAXIMUM_UPLOAD_SIZE)
            //{
            //    return false;
            //}
            //else
            //{
            //    return true;
            //}
            return true;
        }
        private string GetDocumentLink(string strFileName, string strDocumenType,int memberid)
        {           
            string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, memberid, strFileName, strDocumenType, true);

            if (strFilePath != strFileName)
            {
                return "<a href='" + strFilePath + "' target='_blank'>" + strFileName + "</a>"; // 0.1
            }
            else
            {
                return strFileName + "(File not found)";
            }
        }
        //******************END*******************************
       
        #endregion       

    }        

}


