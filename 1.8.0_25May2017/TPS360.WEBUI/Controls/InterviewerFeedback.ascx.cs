﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewerFeedbackBuilder.cs
    Description         :   This page is used to fill up Interviewer Feed back .
    Created By          :   Prasanth
    Created On          :   20/Jul/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  0.1                 27/Dec/2015         Prasanth Kumar G    Introduced Checkexists
 *  0.2                 28/Dec/2015         Prasanth Kumar G    Introduced Overallfeedback
 *  0.3                 21/June/2016        pravin khot         modify function - SendEmailToRecruiter
    ------------------------------------------------------------------------------------------------------------------------------
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using System.IO;
using TPS360.Common;
using TPS360.Common.Shared;
using System.Text;
using System.Collections.Specialized;
using TPS360.BusinessFacade;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using TPS360.BusinessFacade;
using TPS360.Common;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Configuration;
using AjaxControlToolkit;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Linq;
namespace TPS360.Web.UI
{
    public partial class ControlsInterviewerFeedback : BaseControl
    {

        #region Veriables
        private static int _memberId = 0;
        private bool _IsAccessForOverview = true;
        private string UrlForAccess = string.Empty;
        private int IdForSitemap = 0;
        private bool IsNew = false;

        private static string _InterviewerEmail;
        private static int _interviewId;
        private static string _RecruiterEmail;
        private static int _CandidateId;
        private static string _InterviewDate;
        private static string _CandidateName;
        private string _sessionKey;
        #endregion


        #region Properties
        public int MemberId
        {
            set { _memberId = value; }
        }

        public string InterviewerEmail
        {
            set { _InterviewerEmail = value;}
        }

        public int InterviewId
        {
            set{_interviewId = value;}
        }
         

        public string InterviewTitle
        {
            get
            {
                string _interviewTitle = "";
                _interviewTitle = Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWTITLE].ToString();
                return _interviewTitle;
            }
        }

        public string RecruiterEmail
        {
            set { _RecruiterEmail = value; }
        }

        public int CandidateId
        {
            set { _CandidateId = value; }
        }

        public string InterviewDate
        {
            set { _InterviewDate = value; }
        }

        public string CandidateName
        {
            set { _CandidateName = value; }
            
        }


        public StringDictionary Files
        {
            get
            {
                if (Helper.Session.Get(SessionKey) == null)
                {
                    Helper.Session.Set(SessionKey, new StringDictionary());
                }

                return (StringDictionary)Helper.Session.Get(SessionKey);
            }
        }

        public string SessionKey
        {
            get
            {
                return _sessionKey;
            }
            set
            {
                _sessionKey = value;
            }
        }

        #endregion


        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {

            fuDocument.Attributes["onchange"] = "UploadFile(this)";
            
            if (!IsPostBack) 
            {
                PrepareView();
                
            }

            if (IsPostBack)
            {
                _sessionKey = "InterviewEmailFileUpload";
            }

        }


        protected void btnUpload_Click(object sender, EventArgs e)
        {
            
            string str = ValidateDocument();
            if (str != "")
            {
                MiscUtil.ShowMessage(lblMessage, str, false);
            
            }
            else
            {
                UploadDocument_Temp();
            }
        }
        #endregion



        #region Methods
        public void SaveInterviewerfeedback()
        {
            
                InterviewFeedback feedback = BuildInterviewfeedback();
                
                Facade.InterviewerFeedback_Add(feedback);
                
                SendEmailToRecruiter();
                
                
                ClearControls();
            
        }

        public string ValidateDocument()
        {
            string valstr = "";

            if (ddlDocumentType.SelectedIndex == 0)
            {
                valstr = "Please Select Document Type";
                return valstr;
            }
           

            if (fuDocument.HasFile)
            {

                //Code introduced by Prasanth on 4/Jan/2015 Start

                string[] validFileTypes = { "doc", "docx", "pdf","rtf","txt", "xls" };
                string ext = System.IO.Path.GetExtension(fuDocument.PostedFile.FileName);  
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                    }
                }

                if (isValidFile == false)
                {
                    valstr = "Invalid File. Please upload a File with extension " + string.Join(",", validFileTypes);
                    return valstr;
                }
                //******************END************************************


                if (!CheckFileSize())
                {
                    valstr = "File size should be less than 3 MB";
                    return valstr;
                }
                string UploadedFilename = Convert.ToString(fuDocument.FileName);
                string strFilePath = MiscUtil.GetMemberInterviewFeedbackDocumentPath(this.Page, _memberId, _interviewId, _InterviewerEmail, UploadedFilename, ddlDocumentType.SelectedItem.Text, false);
                if (File.Exists(strFilePath))
                {
                    MemberDocument memberDocument = Facade.GetAllByMemberIDInterviewIDInterviewerEmailDocumentTypeAndFileName(_memberId, _interviewId, _InterviewerEmail, ddlDocumentType.SelectedItem.Text.Trim(), UploadedFilename);
                    if (memberDocument != null)
                    {
                        valstr = "Already this document available";
                        return valstr;
                    }
                }
            }
            return valstr;
        }

        public InterviewFeedback BuildInterviewfeedback() 
        {
            InterviewFeedback feedback = new InterviewFeedback();

            feedback.Title = txtTitle.Text.Trim();
            feedback.Feedback = txtNotes.Text.ToString().Trim();
            feedback.InterviewRound = Convert.ToInt32(ddlInterviewRounds.SelectedValue);
            
            feedback.InterviewId = _interviewId;
            feedback.FirstName = txtFirstName.Text;
            feedback.LastName = txtLastName.Text;
            feedback.Email = _InterviewerEmail; 
            feedback.EmployeeID = txtEmployeeid.Text;
            feedback.DocumentName = fuDocument.FileName;
            feedback.OverallFeedback = DdlOverallFeedback.SelectedItem.ToString(); //Line introduced by Prasanth on 28/Dec/2015
            return feedback;
        }
        public void ClearControls() 
        {
            txtNotes.Text = "";
            txtTitle.Text = "";
            hdnInterviewFeebackId.Value = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtEmployeeid.Text = "";
           
            MiscUtil.PopulateInterviewRounds(ddlInterviewRounds, Facade);
            
           
        }

        

        private void PrepareView()
        {
             
            MiscUtil.PopulateInterviewerDocumentType(ddlDocumentType, Facade);
            txtEmail.Text = _InterviewerEmail;
           
            
            MiscUtil.PopulateInterviewRounds(ddlInterviewRounds, Facade);

           

        }

        private void UploadDocument_Temp()
        {



           

                hfFileName.Value = Convert.ToString(fuDocument.FileName);
                hfDocumentName.Value = ddlDocumentType.SelectedItem.Text;
                hfDocumentType.Value = ddlDocumentType.SelectedValue;
                hfFilePath.Value = MiscUtil.GetMemberInterviewFeedbackDocumentPath(this.Page, _memberId, _interviewId, _InterviewerEmail, hfFileName.Value, hfDocumentName.Value, false);
                fuDocument.SaveAs(hfFilePath.Value);
                LblFileName.Text = hfFileName.Value;
          
            
        }
        protected void SendEmailToRecruiter()
        {
            //string mailBody = "<head><title></title><style>body, td, p{	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;	}</style></head><body><P><STRONG>Dear [InterviewerName]</STRONG>,</P><P>This is an invitation for conducting an Interview</P><table><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>From Time:</STRONG> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td></tr><tr><td><STRONG>Interview Type:</STRONG> [InterviewType]</td></tr><tr><td><STRONG>Interview Location:</STRONG> [Location]</td></tr><tr><td><STRONG>Interviewers:</STRONG> [NameOfInterviewers]</td></tr><tr><td><STRONG>Candidate Name:</STRONG> [CandidateName]</td></tr><tr><td><STRONG>Candidate Contact:</STRONG> [CandidateContact]</td></tr></table><table><tr><td>Please provide your feedback/Assessment :<STRONG> <a href=[FeedbackLink]>Click here for Feedback /Assessment form</a> </STRONG> </td></tr></table><P>Thank you for your time</P><table><tr><td>Regards,</td></tr><tr><td>[RecruiterName]</td></tr></table><P><STRONG>[Signature]</STRONG></P></body></html>";
           // string mailBody = "<head><title></title><style>body, td, p{	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;	}</style></head><body><P><STRONG>Hello</STRONG>,</P><P>This is to Notify that you have received a Feedback from the Interviewer</P><table><tr><td><STRONG>Candidate Name:</STRONG> [CandidateName]</td></tr><tr><td><STRONG>Interview Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>Interviewer Name:</STRONG> [InterviewerName]</td></tr></table><table><tr><td>Please refer to the Interview Feedback section/ Report. : </td></tr></table><table><tr><td></td></tr><tr><td>Best Regards,</td></tr><tr><td>[AdminMailId]</td></tr></table></body></html>";
            string mailBody = "<head><title></title><style>body, td, p{	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;	}</style></head><body><P>This is to Notify that you have received a Feedback from [AdminMailId]</P><table><tr><td><STRONG>Candidate Name:</STRONG> [CandidateName]</td></tr><tr><td><STRONG>Interview Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>Interviewer Name:</STRONG> [InterviewerName]</td></tr></table><table><tr><td>Please refer to the Interview Feedback section/ Report. : </td></tr></table><table><tr><td></td></tr></table></body></html>";
            string Subject = "Notification : Interview feedback Received";
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(mailBody);
            
            
            stringBuilder.Replace("[CandidateName]", "A"+ _CandidateId.ToString() + " - " + _CandidateName.ToString());
            stringBuilder.Replace("[InterviewDate]", _InterviewDate.ToString());
            stringBuilder.Replace("[InterviewerName]", _InterviewerEmail.ToString());

            //*************Code modify by pravin khot on 21/June/2016*************
            //stringBuilder.Replace("[AdminMailId]", SiteSetting[DefaultSiteSetting.AdminEmail.ToString()].ToString());
            stringBuilder.Replace("[AdminMailId]", txtFirstName.Text + " " + txtLastName.Text + " (" + txtEmail.Text + " [" + txtEmployeeid.Text + "] )");

            //Member member = Facade.GetMemberByMemberEmail(_InterviewerEmail.ToString());
            //if (member == null)
            //{
            //    stringBuilder.Replace("[AdminMailId]", _InterviewerEmail.ToString());
            //}
            //else
            //{
            //    stringBuilder.Replace("[AdminMailId]", member.FirstName + " " + member.LastName);
            //}
            //****************************END*****************************************

            MailQueueData.AddMailToMailQueue(0, _RecruiterEmail, Subject, stringBuilder.ToString(), "", "", Files, Facade);
        }


        private void UploadDocument()
        {
            MemberDocument newDoc = new MemberDocument();
            newDoc.FileName = hfFileName.Value;
            newDoc.FileTypeLookupId = Int32.Parse(hfDocumentType.Value);

            newDoc.MemberId = _memberId;
            newDoc.InterviewId = _interviewId;
            newDoc.InterviewerEmail = _InterviewerEmail;
            Facade.AddMemberInterviewFeedbackDocument(newDoc);
        }





        private void UploadDocument_Old()
        {
            if (fuDocument.HasFile)
            {

                string strMessage = String.Empty;
                string _fileName = string.Empty;
                lblMessage.Text = string.Empty;
                bool boolError = false;
                string UploadedFilename = Convert.ToString(fuDocument.FileName);
                string strFilePath = MiscUtil.GetMemberInterviewFeedbackDocumentPath(this.Page, _memberId, _interviewId, _InterviewerEmail, UploadedFilename, ddlDocumentType.SelectedItem.Text, false);
                
                if (CheckFileSize())
                {
                    fuDocument.SaveAs(strFilePath);

                    if (File.Exists(strFilePath))
                    {
                        MemberDocument memberDocument = Facade.GetAllByMemberIDInterviewIDInterviewerEmailDocumentTypeAndFileName(_memberId,_interviewId,_InterviewerEmail, ddlDocumentType.SelectedItem.Text.Trim(), UploadedFilename);
                        if (memberDocument == null)
                        {
                            MemberDocument newDoc = new MemberDocument();
                            newDoc.FileName = UploadedFilename;
                            newDoc.FileTypeLookupId = Int32.Parse(ddlDocumentType.SelectedValue);
                         
                            newDoc.MemberId = _memberId;
                            newDoc.InterviewId = _interviewId;
                            newDoc.InterviewerEmail = _InterviewerEmail;
                            Facade.AddMemberInterviewFeedbackDocument(newDoc);
                            
                            
                        
                        }
                        
                    }
                }
             
            }

        }


        private bool CheckFileSize()
        {
            decimal fileSize = Convert.ToDecimal(Convert.ToDecimal(fuDocument.FileContent.Length) / (1024 * 1024));
            if (fileSize > Convert.ToDecimal(3.0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

           //Function introduced by Prasanth on 24/Dec/2015
        private string GetDocumentLink(string strFileName, string strDocumenType)
        {

            string strFilePath = MiscUtil.GetMemberInterviewFeedbackDocumentPath(this.Page, _memberId,_interviewId,_InterviewerEmail, strFileName, strDocumenType, true);

            if (strFilePath != strFileName)
            {
                return "<a href='" + strFilePath + "' target='_blank'>" + strFileName + "</a>"; // 0.1
            }
            else
            {
                return strFileName + "(File not found)";
            }
        }


        //Function introduced by Prasanth on 27/Dec/2015
         public void Checkexists(int InterviewId, string InterviewerEmail)
        {
            InterviewFeedback interview = Facade.InterviewFeedback_GetByInterviewId_InterviewerEmail(InterviewId, InterviewerEmail);
            if (interview != null)
            {
                Response.Redirect(UrlConstants.ApplicationBaseUrl + "Login.aspx?SeOut=SessionOut");
            }
        }

        #endregion


      
}
}


