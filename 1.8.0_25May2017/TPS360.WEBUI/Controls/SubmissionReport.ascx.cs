﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:Control/SubmissionReport.ascx.cs
    Description :This is user control common for SubmissionReport.aspx and MySubmissionReport.aspx 
    Created By: Sumit Sonawane
    Created On: 25/May/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
  
-------------------------------------------------------------------------------------------------------------------------------------------       

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using TPS360.Common.Helper;
using System.Configuration;
using System.Drawing;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.Security;

namespace TPS360.Web.UI
{
    public partial class ControlSubmissionReport : BaseControl
    {
        #region Member Variables
        bool _IsAccessToCompany = true;
        bool _IsAccessToCandidate = true;
        bool _isMySubmissionReport = false;
        bool _isTeamReport = false;
        private static string UrlForCompany = string.Empty;
        private static int SitemapIdForCompany = 0;
        private static string UrlForCandidate = string.Empty;
        private static int SitemapIdForCandidate = 0;
        #endregion

        #region Properties
        public bool IsMySubmissionReport 
        {
            set { _isMySubmissionReport = value; }
        }
        public bool IsTeamSubmissionReport
        {
            set { _isTeamReport = value; }
        }
        string _cookiename;
        string CookieName
        {
            get { return _cookiename; }
            set { _cookiename = value; }
        }
        #endregion
        private string AssociateRequisitionsWithEntitiesFromModule
        {
            get
            {
                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                {
                    return "BU";
                }
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                {
                    return "Account";
                }
                else
                {
                    return "Vendor";
                }
            }

        }
        #region Methods

        private void GenerateRequisitionReport(string format)
        {
            if (string.Equals(format, "word"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=SubmissionReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";
                Response.Output.Write(GetRequisitionReportTable("word"));
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "excel"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=SubmissionReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetRequisitionReportTable("excel"));
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PageWidth = 0;
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;

                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetRequisitionReportTable("pdf"));

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=SubmissionReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }

        private string GetRequisitionReportTable(string format)
        {
            bool InternalHiring = false;
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                InternalHiring = true;
            }
            StringBuilder reqReport = new StringBuilder();
            JobPostingDataSource jobPostingDataSource = new JobPostingDataSource();

            bool isTeamSubmissionReport = _isTeamReport ? true : false;

            string base64Data = "";
            string path = "";

            IList<Submission> jobSubmissionList = jobPostingDataSource.GetPaged(wcdJobPostedDate.StartDate.ToString(),
                wcdJobPostedDate.EndDate.ToString(), wdcJobSubmitedDate.StartDate.ToString(), wdcJobSubmitedDate.EndDate.ToString(), 
                wdcReqDate.StartDate.ToString(),
                wdcReqDate.EndDate.ToString(), ddlApplicantTpye.SelectedValue, ddlReqOwner.SelectedValue, ddlSubmittedBy.SelectedValue,
                ddlAccount.SelectedValue, (hdnSelectedContactID.Value != "" ? hdnSelectedContactID.Value : ddlSubmittedto.SelectedValue),
                isTeamSubmissionReport,CurrentMember.Id,
                Convert.ToInt32(ddlTeamList.SelectedValue==""?"0":ddlTeamList.SelectedValue)
                , null, -1, -1);


            if (jobSubmissionList != null)
            {
                path = AppDomain.CurrentDomain.BaseDirectory + "Images/logo-left-75px.png";

                if (System.IO.File.Exists(path))
                {
                    base64Data = Convert.ToBase64String(System.IO.File.ReadAllBytes(path));
                }
               
                int ColumnCount = 0;
                foreach (ListItem item in chkColumnList.Items)
                    if (item.Selected) ColumnCount++;
                reqReport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");

                reqReport.Append("    <tr style='text-align:center'>");
                //reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + LogoPath + "/Images/logo-left-75px.png' style= style='height:56px;width:56px'/></td>"); 
                if (format == "pdf")
                {
                    reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='data:image/png;base64," + base64Data + "' style= style='height:56px;width:56px'/></td>"); //0.7
                }
                else
                {
                    reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png'; style= style='height:56px;width:56px'/></td>"); //0.7
                } 
                
                reqReport.Append("<td style='font-weight:bold; font-size:22' colspan=" + (ColumnCount - 1) + ">Submission Report</td>");
                reqReport.Append("    </tr>");

                reqReport.Append(" <tr>");
                if (chkColumnList.Items.FindByValue("DateSubmitted").Selected)
                {
                    reqReport.Append("     <th>Date Submitted</th>");
                }
                if (chkColumnList.Items.FindByValue("JobTitle").Selected)
                {
                    reqReport.Append("     <th>Req. Title</th>");
                }

                if (chkColumnList.Items.FindByValue("JobPostingCode").Selected)
                {
                    reqReport.Append("     <th>Req. Code</th>");
                }
                if (chkColumnList.Items.FindByValue("Id").Selected)
                {
                    reqReport.Append("     <th>Candidate ID #</th>");
                }
                if (chkColumnList.Items.FindByValue("ApplicantName").Selected)
                {
                    reqReport.Append("     <th>Applicant Name</th>");
                }
                if (chkColumnList.Items.FindByValue("BU").Selected)
                {
                    reqReport.Append("     <th>" + AssociateRequisitionsWithEntitiesFromModule + "</th>");
                }

                if (chkColumnList.Items.FindByValue("BUContact").Selected)
                {
                    if (InternalHiring) reqReport.Append("     <th>" + AssociateRequisitionsWithEntitiesFromModule + " Contact</th>");
                    else reqReport.Append("     <th>Account Contact</th>");
                }

                if (chkColumnList.Items.FindByValue("SubmittedBy").Selected)
                {
                    reqReport.Append("     <th>Submitted By</th>");

                }
                if (chkColumnList.Items.FindByValue("SubmittedTo").Selected)
                {
                    reqReport.Append("     <th>Submitted To</th>");

                }
                if (chkColumnList.Items.FindByValue("SubmissionEmail").Selected)
                {
                    reqReport.Append("     <th>Submission Email</th>");
                }

                if (chkColumnList.Items.FindByValue("ApplicantType").Selected)
                {
                    reqReport.Append("     <th>Candidate Type</th>");
                }

                //if (chkColumnList.Items.FindByValue("AccountStatus").Selected)
                //{
                //    if (InternalHiring) reqReport.Append("     <th>Department Status</th>");
                //    else reqReport.Append("     <th>Account Status</th>");
                //}

                //if (chkColumnList.Items.FindByValue("JobDurationMonth").Selected)
                //{
                //    reqReport.Append("     <td>Req.Duration</td>");
                //}
                if (chkColumnList.Items.FindByValue("JobDurationLookupId").Selected)
                {
                    reqReport.Append("     <th>Req.Employment Type </th>");
                }
                //if (chkColumnList.Items.FindByValue("JobLocation").Selected)
                //{
                //    reqReport.Append("     <th>Req. Location</th>");
                //}
                if (chkColumnList.Items.FindByValue("JobStatus").Selected)
                {
                    reqReport.Append("     <th>Req. Status</th>");
                }

                reqReport.Append(" </tr>");

                foreach (Submission submission in jobSubmissionList)
                {
                    if (submission != null)
                    {
                        reqReport.Append(" <tr>");
                        if (chkColumnList.Items.FindByValue("DateSubmitted").Selected)
                        {
                            reqReport.Append("     <td>" + submission.SubmissionDate + "&nbsp;</td>");
                        }
                        if (chkColumnList.Items.FindByValue("JobTitle").Selected)
                        {
                            reqReport.Append("     <td>" + submission.JobTitle + "&nbsp;</td>");
                        }
                        if (chkColumnList.Items.FindByValue("JobPostingCode").Selected)
                        {
                            reqReport.Append("     <td>" + submission.JobPostingCode + "&nbsp;</td>");
                        }
                        if (chkColumnList.Items.FindByValue("Id").Selected)
                        {
                            reqReport.Append("     <td>" + "A" + submission.MemberId.ToString() + "&nbsp;</td>");
                        }
                        if (chkColumnList.Items.FindByValue("ApplicantName").Selected)
                        {
                            reqReport.Append("     <td>" + submission.ApplicantName + "&nbsp;</td>");
                        }
                        if (chkColumnList.Items.FindByValue("BU").Selected)
                        {
                            reqReport.Append("     <td>" + submission.CompanyName + "&nbsp;</td>");
                        }

                        if (chkColumnList.Items.FindByValue("BUContact").Selected)
                        {
                            reqReport.Append("     <td>" + submission.CompanyPrimaryEmail + "&nbsp;</td>");
                        }

                        if (chkColumnList.Items.FindByValue("SubmittedBy").Selected)
                        {
                            reqReport.Append("     <td>" + submission.SubmittedBy + "&nbsp;</td>");

                        }
                        if (chkColumnList.Items.FindByValue("SubmittedTo").Selected)
                        {
                            reqReport.Append("     <td>" + submission.SubmittedTo + "&nbsp;</td>");

                        }
                        if (chkColumnList.Items.FindByValue("SubmissionEmail").Selected)
                        {
                            reqReport.Append("     <td>" + submission.SubmissionPrimaryEmail + "&nbsp;</td>");
                        }

                        if (chkColumnList.Items.FindByValue("ApplicantType").Selected)
                        {
                            GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(submission.MemberType);
                            if (_RequisitionStatusLookup != null)
                            {
                                reqReport.Append("     <td>" + _RequisitionStatusLookup.Name + "&nbsp;</td>");
                            }
                            else
                            {
                                reqReport.Append("     <td> &nbsp;</td>");
                            }
                        }

                        //if (chkColumnList.Items.FindByValue("AccountStatus").Selected)
                        //{
                        //    reqReport.Append("     <td>"+EnumHelper.GetDescription(submission.CompanyStatus)+ "&nbsp;</td>");
                        //}

                        // if (chkColumnList.Items.FindByValue("JobDurationMonth").Selected)
                        //{ 
                        //    reqReport.Append("     <td>" + submission.JobDurationMonth + "&nbsp;</td>");
                        //}
                        if (chkColumnList.Items.FindByValue("JobDurationLookupId").Selected)
                        {
                            //reqReport.Append("     <td>" + MiscUtil.SplitValues(submission.JobDurationLookupId, ',', Facade) + "&nbsp;</td>");
                            GenericLookup gen = Facade.GetGenericLookupById(submission.newEmploymentlookUpId);

                            if (gen != null)
                            {
                                //lblJobDurationLookupId.Text = gen.Name;//MiscUtil.SplitValues(submission.EmployementTypeLookUpID, ',', Facade);

                                reqReport.Append("     <td>" + gen.Name + "&nbsp;</td>");
                            }
                            else 
                            {
                                reqReport.Append("     <td>" + string.Empty + "&nbsp;</td>");
                            }
                        }
                        //if (chkColumnList.Items.FindByValue("JobLocation").Selected)
                        //{
                        //    reqReport.Append("     <td>" + submission.City + "&nbsp;</td>");
                        //}
                        if (chkColumnList.Items.FindByValue("JobStatus").Selected)
                        {
                            GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(submission.JobStatus);
                            if (_RequisitionStatusLookup != null)
                            {
                                reqReport.Append("     <td>" + _RequisitionStatusLookup.Name + "&nbsp;</td>");
                            }

                        }

                        reqReport.Append(" </tr>");
                    }
                }

                reqReport.Append("    <tr>");
                //reqReport.Append("        <td align=left  width='300' height='75'><img src='" + LogoPath + "/Images/logo-left-75px.png' style= style='height:56px;width:56px;'/></td>");
                if (format == "pdf")
                {
                    reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='data:image/png;base64," + base64Data + "' style= style='height:56px;width:56px'/></td>"); //0.7
                }
                else
                {
                    reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png'; style= style='height:56px;width:56px'/></td>"); //0.7
                } 
                reqReport.Append("    </tr>");

                reqReport.Append(" </table>");
            }
            return reqReport.ToString();
        }

        private void BindList()
        {
            bool checkedAtleastOne = false;
            foreach (ListItem chkItem in chkColumnList.Items)
            {
                if (chkItem.Selected == true)
                {
                    checkedAtleastOne = true;
                    break;
                }
            }
            this.lsvSubmission.DataSourceID = "odsJobSubmissionList";
            if (checkedAtleastOne)
            {

                this.divlsvSubmission.Visible = true;
                this.lsvSubmission.DataBind();
            }
            else
            {
                this.divlsvSubmission.Visible = false;
                MiscUtil.ShowMessage(lblMessage, "Please select atleast one column", false);
            }
        }

        private void PrepareView()
        {

            MiscUtil.PopulateMemberListByRole(ddlReqOwner, ContextConstants.ROLE_EMPLOYEE, Facade);
            MiscUtil.PopulateClients(ddlAccount, (int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting), Facade);
            ddlAccount.Items[0].Text = "Select BU";
            lblAccount.Text = "BU";
            ////ddlAccount.Items.RemoveAt(1);
            //if (!_isTeamReport) 
            //{
            //    MiscUtil.PopulateMemberListByRole(ddlSubmittedBy, ContextConstants.ROLE_EMPLOYEE, Facade);
            //}
            
            PopulateClientContactList();
            MiscUtil.PopulateCandidateType(ddlApplicantTpye, Facade);
            int teamId = 0;
            int.TryParse(ddlTeamList.SelectedValue, out teamId);
            //if (chkColumnList.Items.FindByValue("BU") != null) chkColumnList.Items.FindByValue("BU").Text = AssociateRequisitionsWithEntitiesFromModule ;
            //if (chkColumnList.Items.FindByValue("BUContact") != null) chkColumnList.Items.FindByValue(" ").Text = AssociateRequisitionsWithEntitiesFromModule + " Contact";
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                //if (chkColumnList.Items.FindByValue("AccountStatus") != null) chkColumnList.Items.FindByValue("AccountStatus").Text = "Department Status";
            }
        }
        private void PopulateTeamList(int TeamLeaderId) 
        { 
            ddlTeamList.DataSource = Facade.GetAllTeamByTeamLeaderId(TeamLeaderId);
            ddlTeamList.DataTextField = "Title";
            ddlTeamList.DataValueField = "Id";
            ddlTeamList.DataBind();
        }
        private void PopulateClientContactList()
        {
            ddlSubmittedto.DataSource = Facade.GetAllCompanyContactsByCompanyId(Convert.ToInt32(ddlAccount.SelectedValue == "" ? "0" : ddlAccount.SelectedValue));
            ddlSubmittedto.DataTextField = "FirstName";
            ddlSubmittedto.DataValueField = "Email";
            ddlSubmittedto.DataBind();
            ListItem item = new ListItem();
            item.Text = "Please Select";
            item.Value = "0";
            ddlSubmittedto.Items.Insert(0, item);
            if (ddlSubmittedto.Items.Count == 1) ddlSubmittedto.Enabled = false;
            else ddlSubmittedto.Enabled = true;
        }
        private void ClearControls()
        {
            wdcReqDate.ClearRange();
            wdcJobSubmitedDate.ClearRange();
            wcdJobPostedDate.ClearRange();
            ddlApplicantTpye.SelectedIndex = ddlReqOwner.SelectedIndex =  ddlAccount.SelectedIndex = 0;
            ddlAccount.SelectedIndex = 0;
            PopulateClientContactList();
            //dlSubmittedto.Enabled = false;
            hdnSelectedContactID.Value = "0";
            lsvSubmission.Visible = false;
            hdnSubmittedBy.Value = "0";

            if (_isMySubmissionReport)
            {
                ddlSubmittedBy.SelectedValue = CurrentMember.Id.ToString();
            }
            else 
            {
                ddlSubmittedBy.SelectedIndex = 0;
            }
        }

        private void HideUnHideListViewHeaderItem(string headerTitle, string chkItemName)
        {
            HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvSubmission.FindControl(headerTitle);

            if (thHeaderTitle != null)
            {
                if (!chkColumnList.Items.FindByValue(chkItemName).Selected)
                {
                    thHeaderTitle.Visible = false;
                }
                else
                {
                    thHeaderTitle.Visible = true;
                }
            }
        }
        protected void ddlTeamList_SelectedIndexChanged(object sender, EventArgs e) 
        {
            PopulateTeamUser(ddlTeamList.SelectedValue);
        }
        string[] ColumnValues(string val, string split)
        {
            string[] separator = new string[] { split };
            string[] splitvalues = val.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            return splitvalues;
        }
        private void RenameListHeader(string headerTitle, string LinkName)
        {

            HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvSubmission.FindControl(headerTitle);
            if (thHeaderTitle != null)
            {
                LinkButton link = (LinkButton)thHeaderTitle.FindControl(LinkName);
                link.Text = link.Text.Replace("Account", AssociateRequisitionsWithEntitiesFromModule);
                link.ToolTip = link.ToolTip.Replace("Account", AssociateRequisitionsWithEntitiesFromModule);


            }

        }
        private void HideUnhideListViewHeader()
        {
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                RenameListHeader("thAccount", "btnAccount");
                RenameListHeader("thAccountContact", "btnAccountContact");
                //RenameListHeader("thAccountStatus", "btnAccountStatus");

            }

            HideUnHideListViewHeaderItem("thId", "Id");
            HideUnHideListViewHeaderItem("thDateSubmitted", "DateSubmitted");
            HideUnHideListViewHeaderItem("thReqTitle", "JobTitle");
            HideUnHideListViewHeaderItem("thJobPostingCode", "JobPostingCode");
            HideUnHideListViewHeaderItem("thApplicantName", "ApplicantName");
            HideUnHideListViewHeaderItem("thAccount", "BU");
            HideUnHideListViewHeaderItem("thAccountContact", "BUContact");
            HideUnHideListViewHeaderItem("thSubmittedBy", "SubmittedBy");
            HideUnHideListViewHeaderItem("thSubmittedTo", "SubmittedTo");
            HideUnHideListViewHeaderItem("thSubmissionEmail", "SubmissionEmail");
            HideUnHideListViewHeaderItem("thApplicantType", "ApplicantType");
            //HideUnHideListViewHeaderItem("thAccountStatus", "AccountStatus");
            //HideUnHideListViewHeaderItem("thJobDurationMonth", "JobDurationMonth");
            HideUnHideListViewHeaderItem("thJobDurationLookupId", "JobDurationLookupId");
            HideUnHideListViewHeaderItem("thJobLocation", "JobLocation");
            HideUnHideListViewHeaderItem("thReqStatus", "JobStatus");

        }

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvSubmission.FindControl(hdnSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.Attributes.Add("class", (hdnSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        public void repor(string rep)
        {
            int j = 0;
            int k = 0;
            foreach (ListItem items in chkColumnList.Items)
            {
                if (items.Selected)
                {
                    j = 1;

                }
                else
                    k = 1;
            }
            if ((j == 1 && k == 1) || (k == 0 && j == 1))
                GenerateRequisitionReport(rep);
            else
            {
                MiscUtil.ShowMessage(lbMessage, "Please select columns before generating the report.", true);
            }
        }
        #endregion

        #region Page Events
        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {

            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                repor("pdf");
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (_isMySubmissionReport) 
            {
                divSubmittedBy.Visible = false;
                //ddlSubmittedBy.SelectedValue = CurrentMember.Id.ToString();
                odsJobSubmissionList.SelectParameters["SubmittedBy"].DefaultValue = CurrentMember.Id.ToString();
            }
            
            ddlAccount.Attributes.Add("onChange", "return Company_OnChangeFillContactEmail('" + ddlAccount.ClientID + "','" + ddlSubmittedto.ClientID + "','" + hdnSelectedContactID.ClientID + "')");
            ddlSubmittedto.Attributes.Add("OnChange", "return Contact_OnChange('" + ddlSubmittedto.ClientID + "','" + hdnSelectedContactID.ClientID + "')");
            ddlReqOwner.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlSubmittedBy.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlAccount.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlSubmittedto.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlTeamList.Attributes.Add("onchange", "TeamOnChange('" + ddlTeamList.ClientID + "','" + ddlSubmittedBy.ClientID + "','" + hdnSubmittedBy.ClientID + "','" + CurrentMember.Id + "')");
            ddlSubmittedBy.Attributes.Add("onchange", "EmployeeOnChange('" + ddlSubmittedBy.ClientID + "','" + hdnSubmittedBy.ClientID + "')");
            if (_isTeamReport) 
            {
                divTeamList.Visible = true;
          
                odsJobSubmissionList.SelectParameters["IsTeamReport"].DefaultValue = "true";
                odsJobSubmissionList.SelectParameters["TeamLeaderId"].DefaultValue = CurrentMember.Id.ToString();
            }
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(235, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToCompany = false;
            else
            {
                SitemapIdForCompany = CustomMap.Id;
                UrlForCompany = "~/" + CustomMap.Url.ToString();
            }
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToCandidate = false;
            else
            {
                SitemapIdForCandidate = CustomMap.Id;
                UrlForCandidate = "~/" + CustomMap.Url.ToString();
            }

            CookieName = "UserColumnOption_Submission" + base.CurrentMember.PrimaryEmail;
            if (!IsPostBack)
            {

                PrepareView();
                PopulateTeamList(CurrentMember.Id);
                if (_isTeamReport)
                {
                    if (ddlTeamList.Items.Count > 0)
                        ddlTeamList.Items.Insert(0, new ListItem("All", "0"));

                    else
                        divTeamList.Visible = false;
                    PopulateTeamUser("0");
                    //ddlSubmittedBy.SelectedValue = hdnSubmittedBy.Value;
                }
                else 
                {
                    MiscUtil.PopulateMemberListByRole(ddlSubmittedBy, ContextConstants.ROLE_EMPLOYEE, Facade);
                    if (_isMySubmissionReport)
                    {
                        ddlSubmittedBy.SelectedValue = CurrentMember.Id.ToString();
                    }
                }
                
                if (Request.Cookies[CookieName] != null)
                {
                    string selectedColumnValue = Request.Cookies[CookieName].Value;
                    string[] Columns = ColumnValues(selectedColumnValue, "#");

                    foreach (ListItem item in chkColumnList.Items)
                    {
                        item.Selected = false;
                    }
                    foreach (string item in Columns)
                    {
                        try
                        {
                            chkColumnList.Items.FindByValue(item).Selected = true;
                        }
                        catch
                        {
                        }
                    }
                }
            }
            if (!IsPostBack)
            {

                btnSearch_Click(sender, e);
            }
        }
        #endregion

        #region Button Events
        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
            divExportButtons.Visible = false;
            if (divTeamList.Visible)
            {
                ddlTeamList.SelectedIndex = 0;
                PopulateTeamUser(ddlTeamList.SelectedValue);
            }
            else PopulateTeamUser("0");
            ddlSubmittedBy.SelectedIndex = 0;

            if (_isTeamReport)
            {
                odsJobSubmissionList.SelectParameters["IsTeamReport"].DefaultValue = "true";
                odsJobSubmissionList.SelectParameters["TeamLeaderId"].DefaultValue = CurrentMember.Id.ToString();
            }
        }
        private void PopulateTeamUser(string TeamId)
        {
            if (_isTeamReport)
            {
                if (TeamId == "0" || TeamId == "")
                {

                    ddlSubmittedBy.DataSource = Facade.GetAllMemberNameByTeamMemberId(ContextConstants.ROLE_EMPLOYEE, CurrentMember.Id);
                    ddlSubmittedBy.DataValueField = "Id";
                    ddlSubmittedBy.DataTextField = "FirstName";
                }
                else
                {
                    ddlSubmittedBy.DataSource = Facade.GetAllEmployeeByTeamId(TeamId);
                    ddlSubmittedBy.DataValueField = "Id";
                    ddlSubmittedBy.DataTextField = "FirstName";

                }
                ddlSubmittedBy.DataBind();
                ddlSubmittedBy.Items.Insert(0, new ListItem("All", "0"));
            }
            else 
            {
                
            }
            
       
             
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdnScrollPos.Value = "0";
            lsvSubmission.Items.Clear();
            ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvSubmission.FindControl("pagerControl");
            PopulateTeamUser(ddlTeamList.SelectedValue);
            if (hdnSubmittedBy.Value != null && hdnSubmittedBy.Value != "0" && hdnSubmittedBy.Value != "")
                ddlSubmittedBy.SelectedValue = hdnSubmittedBy.Value;
            if (PagerControls != null)
            {
                DataPager pager = (DataPager)PagerControls.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            //PopulateTeamUser(ddlTeamList.SelectedValue);
            BindList();
            PopulateClientContactList();

            //   ControlHelper.SelectListByValue(ddlSubmittedto, hdnSelectedContactID.Value);
            if (lsvSubmission != null && lsvSubmission.Items.Count > 0)
            {
                divExportButtons.Visible = true;
                HideUnhideListViewHeader();
                lsvSubmission.Visible = true;

                if (hdnSortColumn.Text == "") hdnSortColumn.Text = "btnDateSubmitted";
                if (hdnSortOrder.Text == "") hdnSortOrder.Text = "DESC";
                PlaceUpDownArrow();
            }
            else divExportButtons.Visible = false;

            string pagesize = "";
            pagesize = (Request.Cookies["SubmissionReportRowPerPage"] == null ? "" : Request.Cookies["SubmissionReportRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvSubmission.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }

            System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvSubmission.FindControl("tdpager");
            int count = 0;
            foreach (ListItem item in chkColumnList.Items)
            {
                if (item.Selected)
                    count = count + 1;
            }
            if (tdpager != null) tdpager.ColSpan = count;
            ControlHelper.SelectListByValue(ddlSubmittedto, hdnSelectedContactID.Value);

            if (_isTeamReport)
            {
                odsJobSubmissionList.SelectParameters["IsTeamReport"].DefaultValue = "true";
                odsJobSubmissionList.SelectParameters["TeamLeaderId"].DefaultValue = CurrentMember.Id.ToString();
            }
            

        }
        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {
            uclConfirm.AddMessage("Note: Some columns may not appear in report if too many are selected.", ConfirmationWindow.enmMessageType.Attention, true, true, "");
        }
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            repor("excel");
        }
        protected void btnExportToWord_Click(object sender, EventArgs e)
        {
            repor("word");
        }
        #endregion

        #region ListView
        protected void lsvSubmission_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            StringBuilder ColumnOption = new StringBuilder();
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {

                Submission submission = ((ListViewDataItem)e.Item).DataItem as Submission;

                if (submission != null)
                {

                    HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                    HtmlTableCell tdReqTitle = (HtmlTableCell)e.Item.FindControl("tdReqTitle");

                    if (chkColumnList.Items.FindByValue("JobTitle").Selected)
                    {
                        lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + submission.Id + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                        lnkJobTitle.Text = submission.JobTitle;
                        lnkJobTitle.Visible = true;
                        tdReqTitle.Visible = true;
                        ColumnOption.Append("JobTitle").Append("#");
                    }
                    else
                    {
                        tdReqTitle.Visible = false;
                        lnkJobTitle.Visible = false;
                    }

                    if (chkColumnList.Items.FindByValue("Id").Selected)
                    {
                        Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
                        lblCandidateID.Text = "A" + submission.MemberId.ToString();
                        HtmlTableCell tdId = (HtmlTableCell)e.Item.FindControl("tdId");
                        tdId.Visible = true;
                    }
                    else
                    {
                        HtmlTableCell tdId = (HtmlTableCell)e.Item.FindControl("tdId");
                        tdId.Visible = false;

                    }

                    Label lblJobPostingCode = (Label)e.Item.FindControl("lblJobPostingCode");

                    HtmlTableCell tdJobPostingCode = (HtmlTableCell)e.Item.FindControl("tdJobPostingCode");

                    if (chkColumnList.Items.FindByValue("JobPostingCode").Selected)
                    {
                        lblJobPostingCode.Text = submission.JobPostingCode;
                        tdJobPostingCode.Visible = true;
                        lblJobPostingCode.Visible = true;
                        ColumnOption.Append("JobPostingCode").Append("#");
                    }
                    else
                    {
                        tdJobPostingCode.Visible = false;
                        lblJobPostingCode.Visible = false;
                    }

                    Label lblJobDurationLookupId = (Label)e.Item.FindControl("lblJobDurationLookupId");

                    HtmlTableCell tdJobDurationLookupId = (HtmlTableCell)e.Item.FindControl("tdJobDurationLookupId");

                    if (chkColumnList.Items.FindByValue("JobDurationLookupId").Selected)
                    {
                        GenericLookup gen = Facade.GetGenericLookupById(submission.newEmploymentlookUpId);

                        if (gen != null) 
                        {
                            lblJobDurationLookupId.Text = gen.Name;//MiscUtil.SplitValues(submission.EmployementTypeLookUpID, ',', Facade);
                        }


                        tdJobDurationLookupId.Visible = true;
                        lblJobDurationLookupId.Visible = true;
                        ColumnOption.Append("JobDurationLookupId");
                    }
                    else
                    {
                        lblJobDurationLookupId.Visible = false;
                        tdJobDurationLookupId.Visible = false;
                    }

                    Label lblCity = (Label)e.Item.FindControl("lblLocation");

                    HtmlTableCell tdCity = (HtmlTableCell)e.Item.FindControl("tdLocation");

                    //if (chkColumnList.Items.FindByValue("JobLocation").Selected)
                    //{
                    //    lblCity.Text = submission.City;
                    //    tdCity.Visible = true;
                    //    lblCity.Visible = true;
                    //    ColumnOption.Append("JobLocation").Append("#");
                    //}
                    //else
                    //{
                    //    lblCity.Visible = false;
                    //    tdCity.Visible = false;
                    //}

                    Label lblStatus = (Label)e.Item.FindControl("lblStatus");

                    HtmlTableCell tdStatus = (HtmlTableCell)e.Item.FindControl("tdStatus");

                    if (chkColumnList.Items.FindByValue("JobStatus").Selected)
                    {

                        GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(submission.JobStatus);
                        if (_RequisitionStatusLookup != null)
                        {
                            lblStatus.Text = _RequisitionStatusLookup.Name;
                        }
                        tdStatus.Visible = true;
                        lblStatus.Visible = true;
                        ColumnOption.Append("JobStatus").Append("#");
                    }
                    else
                    {
                        lblStatus.Visible = false;
                        tdStatus.Visible = false;
                    }
                    HyperLink lnkAccount = (HyperLink)e.Item.FindControl("lnkAccount");
                    HtmlTableCell tdAccount = (HtmlTableCell)e.Item.FindControl("tdAccount");

                    if (chkColumnList.Items.FindByValue("BU").Selected)
                    {
                        if (_IsAccessToCompany)
                        {
                            if (SitemapIdForCompany > 0 && UrlForCompany != string.Empty)
                                ControlHelper.SetHyperLink(lnkAccount, UrlForCompany, string.Empty, submission.CompanyName, UrlConstants.PARAM_COMPANY_ID, StringHelper.Convert(submission.ClientId), UrlConstants.PARAM_SITEMAP_ID, SitemapIdForCompany.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, "622");
                        }
                        else
                        {
                            lnkAccount.Text = submission.CompanyName;
                        }
                        tdAccount.Visible = true;
                        lnkAccount.Visible = true;
                        ColumnOption.Append("Account").Append("#");
                    }
                    else
                    {
                        lnkAccount.Visible = false;
                        tdAccount.Visible = false;
                    }


                    Label lblAccountContact = (Label)e.Item.FindControl("lblAccountContact");

                    HtmlTableCell tdAccountContact = (HtmlTableCell)e.Item.FindControl("tdAccountContact");

                    if (chkColumnList.Items.FindByValue("BUContact").Selected)
                    {
                        lblAccountContact.Text = submission.CompanyPrimaryEmail;
                        tdAccountContact.Visible = true;
                        lblAccountContact.Visible = true;
                        ColumnOption.Append("BUContact").Append("#");
                    }
                    else
                    {
                        lblAccountContact.Visible = false;
                        tdAccountContact.Visible = false;
                    }

                    //Label lblAccountStatus = (Label)e.Item.FindControl("lblAccountStatus");

                    //HtmlTableCell tdAccountStatus = (HtmlTableCell)e.Item.FindControl("tdAccountStatus");

                    //if (chkColumnList.Items.FindByValue("AccountStatus").Selected)
                    //{
                    //    lblAccountStatus.Text = EnumHelper.GetDescription(submission.CompanyStatus);
                    //    tdAccountStatus.Visible = true;
                    //    lblAccountStatus.Visible = true;
                    //    ColumnOption.Append("AccountStatus").Append("#");
                    //}
                    //else
                    //{
                    //    lblAccountStatus.Visible = false;
                    //    tdAccountStatus.Visible = false;
                    //}

                    Label lblDateSubmitted = (Label)e.Item.FindControl("DateSubmitted");

                    HtmlTableCell tdDtSubmitted = (HtmlTableCell)e.Item.FindControl("tdDtSubmitted");

                    if (chkColumnList.Items.FindByValue("DateSubmitted").Selected)
                    {
                        lblDateSubmitted.Text = submission.SubmissionDate.ToShortDateString() + " " + submission.SubmissionDate.ToShortTimeString();
                        tdDtSubmitted.Visible = true;
                        lblDateSubmitted.Visible = true;
                        ColumnOption.Append("DateSubmitted").Append("#");
                    }
                    else
                    {
                        tdDtSubmitted.Visible = false;
                        lblDateSubmitted.Visible = false;
                    }
                    HyperLink lnkApplicantName = (HyperLink)e.Item.FindControl("lnkApplicantName");
                    HtmlTableCell tdApplicantName = (HtmlTableCell)e.Item.FindControl("tdApplicantname");
                    string _applicantName = submission.ApplicantName;
                    if (_applicantName.Trim() == "")
                        _applicantName = "No Candidate Name";
                    if (chkColumnList.Items.FindByValue("ApplicantName").Selected)
                    {
                        if (_IsAccessToCandidate)
                        {
                            if (SitemapIdForCandidate > 0 && UrlForCandidate != string.Empty)
                                ControlHelper.SetHyperLink(lnkApplicantName, UrlForCandidate, string.Empty, _applicantName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(submission.MemberId), UrlConstants.PARAM_SITEMAP_ID, SitemapIdForCandidate.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.ATS_OVERVIEW_SITEMAP_PARENTID);
                        }
                        else
                        {
                            lnkApplicantName.Text = _applicantName;// submission.ApplicantName;
                        }

                        tdApplicantName.Visible = true;
                        lnkApplicantName.Visible = true;
                        ColumnOption.Append("ApplicantName").Append("#");
                    }
                    else
                    {
                        lnkApplicantName.Visible = false;
                        tdApplicantName.Visible = false;
                    }


                    Label lblSubmittedBy = (Label)e.Item.FindControl("lblSubmittedBy");

                    HtmlTableCell tdSubmittedBy = (HtmlTableCell)e.Item.FindControl("tdSubmittedBy");

                    if (chkColumnList.Items.FindByValue("SubmittedBy").Selected)
                    {
                        lblSubmittedBy.Text = submission.SubmittedBy;
                        tdSubmittedBy.Visible = true;
                        lblSubmittedBy.Visible = true;
                        ColumnOption.Append("SubmittedBy").Append("#");
                    }
                    else
                    {
                        lblSubmittedBy.Visible = false;
                        tdSubmittedBy.Visible = false;
                    }
                    Label lblSubmittedTo = (Label)e.Item.FindControl("lblSubmittedTo");

                    HtmlTableCell tdSubmittedTo = (HtmlTableCell)e.Item.FindControl("tdSubmittedTo");

                    if (chkColumnList.Items.FindByValue("SubmittedTo").Selected)
                    {
                        lblSubmittedTo.Text = submission.SubmittedTo;
                        tdSubmittedTo.Visible = true;
                        lblSubmittedTo.Visible = true;
                        ColumnOption.Append("SubmittedTo").Append("#");
                    }
                    else
                    {
                        lblSubmittedTo.Visible = false;
                        tdSubmittedTo.Visible = false;
                    }
                    LinkButton lnkSubmissionEmail = (LinkButton)e.Item.FindControl("lnkSubmissionEmail");
                    HtmlTableCell tdSubmissionEmail = (HtmlTableCell)e.Item.FindControl("tdSubmissionEmail");

                    if (chkColumnList.Items.FindByValue("SubmissionEmail").Selected)
                    {
                        if (submission.MemberEmailId > 0)
                        {
                            lnkSubmissionEmail.CommandArgument = submission.MemberEmailId + "$" + submission.ClientId + "$" + submission.Id + "$" + submission.MemberId;
                            tdSubmissionEmail.Visible = true;
                            lnkSubmissionEmail.Visible = true;

                        }
                        else
                        {
                            //lnkSubmissionEmail.Visible = false;
                            lnkSubmissionEmail.Text = submission.SubmissionPrimaryEmail;
                            lnkSubmissionEmail.Enabled = false;
                            //tdSubmissionEmail.Visible = false;
                        }
                        ColumnOption.Append("SubmissionEmail").Append("#");
                    }
                    else
                    {
                        lnkSubmissionEmail.Visible = false;
                        tdSubmissionEmail.Visible = false;
                    }

                    Label lblApplicantType = (Label)e.Item.FindControl("lblApplicantType");

                    HtmlTableCell tdApplicantType = (HtmlTableCell)e.Item.FindControl("tdApplicantType");

                    if (chkColumnList.Items.FindByValue("ApplicantType").Selected)
                    {
                        GenericLookup _CandidateLookup = Facade.GetGenericLookupById(submission.MemberType);
                        if (_CandidateLookup != null)
                        {
                            lblApplicantType.Text = _CandidateLookup.Name;
                        }
                        tdApplicantType.Visible = true;
                        lblApplicantType.Visible = true;
                        ColumnOption.Append("ApplicantType").Append("#");
                    }
                    else
                    {
                        lblApplicantType.Visible = false;
                        tdApplicantType.Visible = false;
                    }

                    if (Request.Cookies[CookieName] != null)
                    {
                        Response.Cookies[CookieName].Value = ColumnOption.ToString();
                    }
                    else
                    {
                        HttpCookie aCookie = new HttpCookie(CookieName);
                        aCookie.Expires = DateTime.Now.AddDays(1);
                        aCookie.Value = ColumnOption.ToString();
                        Response.Cookies.Add(aCookie);
                    }
                }

            }
        }

        protected void lsvSubmission_PreRender(object sender, EventArgs e)
        {
            divExportButtons.Visible = lsvSubmission.Items.Count > 0;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvSubmission.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "SubmissionReportRowPerPage";
            }
            PlaceUpDownArrow();
            HideUnhideListViewHeader();
            if (IsPostBack)
            {
                if (PagerControl == null)
                {
                    lsvSubmission.Items.Clear();
                    lsvSubmission.DataSource = null;
                    lsvSubmission.DataBind();
                    divExportButtons.Visible = lsvSubmission.Items.Count > 0;
                }
            }

        }

        protected void lsvSubmission_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                int id;
                if (string.Equals(e.CommandName, "Email"))
                {
                    string[] ParamID = e.CommandArgument.ToString().Split('$');

                    string sub_MemberEmailId = ParamID[0];
                    string sub_ClientID = ParamID[1];
                    string sub_SubmissionId = ParamID[2];
                    string sub_MemberId = ParamID[3];
                    SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.CommonPages.EMAIL_PREVIEW.Substring(3), string.Empty, UrlConstants.PARAM_MemberEmail_ID, sub_MemberEmailId, UrlConstants.PARAM_COMPANY_ID, sub_ClientID, UrlConstants.PARAM_JOB_ID, sub_SubmissionId, UrlConstants.PARAM_APPLICANT_ID, sub_MemberId);
                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Email", "<script>window.open('" + url + "');</script>", false);
                }

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Text == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Text == "ASC") hdnSortOrder.Text = "DESC";
                        else hdnSortOrder.Text = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Text = lnkbutton.ID;
                        hdnSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            { }

        }
        #endregion
}
}
