﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:ObjectiveSummaryEditor.ascx
    Description:It is used for Displaying ObjectSummary for "Object & Summary" under Resume Builder
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Jan-13-2009          Gopala Swamy          Defect id:9249; Changed Label text  from "Objective" to "Objective:"                                                                     
    0.2             July-6-2009          Gopala Swamy          Defect id:10848; Changed Infragestic control to Asp server control    
    0.3             July-17-2009         Gopala Swamy          Defect Id:10938; Put update panel.
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ObjectiveSummaryEditor.ascx.cs" EnableViewState ="true" 
    Inherits="TPS360.Web.UI.ControlObjectiveSummaryEditor" %>
<%@ Register Assembly="Infragistics2.WebUI.WebHtmlEditor.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.WebHtmlEditor" TagPrefix="ighedit" %>   
    <script type ="text/javascript" >
    function check()
    {
    var isPost=document .getElementById ('<%=PostBack.ClientID%>');
     isPost .value="PostBack";
    }
    </script>
<div>
 <asp:UpdatePanel ID="updObjectiveSummary" runat="server" UpdateMode="Conditional"> <%--0.3--%>
    <ContentTemplate> 
    <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <asp:HiddenField ID ="PostBack" runat ="server" Value ="" />
    </div>
    <div class="TableRow">
        
        <div class="TableFormLeble" style="text-align:left">
            <asp:Label runat="server" Text="Objective:"></asp:Label>
        </div>
        
        <div class="TableFormContent">
                <asp:TextBox ID="txtObjective" runat="server" TextMode="MultiLine" CssClass="CommonTextBox" TabIndex ="1"
                Width="98%" Height="160px"></asp:TextBox>
        </div>
    </div>
  
    <div class="TableRow">
        <div class="TableFormLeble" style="text-align:left">
            <asp:Label ID="Label1" runat="server" Text="Summary:"></asp:Label>
        </div>
        <div class="TableFormContent">
             <asp:TextBox ID="txtSummary" runat="server" TextMode="MultiLine" CssClass="CommonTextBox"
               Width="98%" Height="160px"  TabIndex ="2"></asp:TextBox> 
        </div>
    </div>
    <div class="TableRow" style="text-align: center;">
        <asp:Button ID="btnSave" CssClass="CommonButton" ValidationGroup="ObjectiveSummary"
            runat="server" Text="Save" OnClick="btnSaveObjectiveSummary_Click" TabIndex ="3" OnClientClick ="check()" />
    </div>
  
    </ContentTemplate> 
    </asp:UpdatePanel> 
    
</div>
