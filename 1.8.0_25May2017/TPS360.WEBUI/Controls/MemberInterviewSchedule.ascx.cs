﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberInterviewSchedule.ascx.cs
    Description: This is the user control page used for member interview functionalities
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sep-29-2008           Yogeesh Bhat        Defect ID: 8734; Added PagerCommand() method for pager events
    0.2            Oct-01-2008           Yogeesh Bhat        Defect ID: 8733; Added delete functionlity in lsvInterviewSchedule_ItemCommand()
    0.3            Oct-06-2008           Yogeesh Bhat        Defect ID: 8731; Added Update, Cancel functionality for external interviewers
                                                             in btnAdd_Click() method;
    0.4            Oct-14-2008           Yogeesh Bhat        Defect ID: 8893; Added new method, ddlJobPosting_SelectedIndexChanged()
    0.5            Nov-04-2008           Jagadish            Defect ID: 9029; Added code to make First name, Last name, Email id text fields empty and cancel button invisible
                                                             when user selects edit mode in 'Schedule list & reports' tab.
    0.6            Nov-28-2008           Jagadish            Defect ID: 9199; Added the message when email id not exist.
    0.7            Dec-02-2008           Anand Dixit         Defect ID: 8955; Included Conversion from UTC to Local time.
    0.8            Jan-05-2009           Jagadish            Defect ID: 9522; Included Conversion from UTC to Local time.
    0.9            Jan-15-2009           Yogeesh Bhat        Defect ID: 9705: Changes made in PrepareView() method
    1.0            Jan-16-2009           Gopala Swamy        Defect ID: 9728: Changes made in lsvInterviewSchedule_ItemDataBound() method
    1.1            Jan-16-2009           Jagadish            Defect Id: 9704: Changed the control Dropdown to WebDateTimeEdit.
    1.2            Jan-21-2009           Jagadish            Defect Id: 9717: Modification to #9704. Added editable dropdown.
    1.3            Feb-12-2009           Yogeesh Bhat        Defect Id: 9246: Changes made in btnSave_Click(); Added new method GetInterviewerSchedule()
    1.4            Feb-13-2009           Nagarathna          Defect ID:9697:changes made in lsvExternalInterviewers_ItemCommand(),lsvInterviewSchedule_ItemCommand; 
 *  1.5            Sept-24-2009         Nagarathna V.B      Enha#11473;passing current user for send email method call
 *  1.6            Jan-19-2010          Gopala Swamy J        Defect Id:12901;Changed Style attribute and put events called "OnTextChanged"
 *  1.7            Apr-19-2010          Sudarshan.R.          Defect Id:12651; changes made to stop btnAdd click event firing on page refresh.
 *  1.8            Apr-22-2010          Sudarshan.R.          Defect Id:12712 ; Changes made in Page_Load.
 *  1.9            16/Oct/2015          Prasanth Kumar G      Introduced Interview QuestionBank (Not working commented), New Email Format
 *  0.2            9/Dec/2015           Pravin khot           Introduced PopulateSuggestedInterviewerWithEmail
 *  0.3            17/Dec/2015          Pravin khot           Introduced by mail sendind code by Suggested panel ,LoadSuggestedPanel,Loadotherinterviews
    0.4            21/Dec/2015          Prasanth Kumar G      Individual Email representing .
 *  0.5            22/Dec/2015          Prasanth Kumar G      Commented unnecessary Code
 *  0.6            4/March/2016         pravin khot           modify code using caltitle = "Interview - " remove jobtitle
 *  0.7            14/March/2016        pravin khot           modify code [MAIL SUBJECT CHANGE] - SendInterviewEmail,SendInterviewEmailrecruiter,SendInterviewEmailinterviwer
 *  0.8            4/May/2016           pravin khot           added  interview.QuestionBankTypeLookupId,IcsFileUIDCode,IsCancel ,modify UID CODE USE-IcsFileUIDCode
 *  0.9            5/May/2016           pravin khot           added-lblStatus
 *  0.10           9/May/2016           pravin khot           added new region - #region CancelInterveiwSchedule
 *  1.0            24/May/2016          pravin khot           added new - ddllocation , divlocation,ddlTimezone, GetTimeZoneIdBy_MemberId
 *  1.1            06/June/2016         Sumit Sonawane        Removed html code for emails
 *  1.2             8/June/2016         pravin khot            modify function -  PopulateClientDropdowns(); LoadJobPosting(); MessageAnswered ,BtnSave_click event
 *  1.3             13/June/2016        pravin khot           modify code on page load when interview model open. , EVENT Modify-btnAtach_Click
 *  1.4             14/June/2016        pravin khot           modify - function - prepareeditview 
 *  1.5             11/July/2016        pravin khot           commented - foreach (ListItem item in chkInternalInterviewer.Items)
 *  1.6             28/Feb/2017         Sumit Sonawane        added  No-Show and Completed on Action Button
 *  1.7             1/Mar/2017         Sumit Sonawane        modify - restrict to rescheduled/cancel Interview feedback.
 *  1.8             1/Mar/2017         Sumit Sonawane        modify - for Interview reschedule matrix capture.
 * * -------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common;
using System.Web;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web.UI;
using Microsoft.Office.Interop.Outlook;
using System.Timers;
namespace TPS360.Web.UI
{
    public partial class ControlMemberInterviewSchedule : BaseControl
    {
        #region Member Variables

        private static int _memberId = 0;
        private static int _interviewId = 0;
        private static string _memberrole = string.Empty;
        private static bool isNew = true;
        private static bool IsAccessToDelete = false;
        private bool _mailsetting = false;
        private string _sessionKey;
        private int interviewidforapp = 0;
        private static Guid uid;
        #endregion

        #region Properties

        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }
        public Guid Appuid
        {
            set { uid = value; }
            get { return uid; }
        }
        public int MemberId
        {
            set { _memberId = value; }
        }
        public string TempDirectory
        {
            get
            {
                return (string)Helper.Session.Get("tempDir");
            }
            set
            {
                Helper.Session.Set("tempDir", value);
            }
        }
        public string PreviewDir
        {
            get;
            set;
        }
        string CandidateId
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    return Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];

                }
                ////From Member Login
                else
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl["CanId"]))
                    {
                        return Helper.Url.SecureUrl["CanId"];

                    }
                    else return "0";
                }

            }
        }

        int InterId
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWSCHEDULE_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWSCHEDULE_ID]);

                }
                ////From Member Login
                else
                {
                    return 0;
                }

            }
        }

        int JobPostingId
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]);

                }
                ////From Member Login
                else
                {
                    return 0;
                }

            }
        }

        public bool isMainApplication
        {
            get
            {
                switch (ApplicationSource)
                {
                    case ApplicationSource.LandTApplication:
                        return false;
                        break;
                    case ApplicationSource.MainApplication:
                        return true;
                        break;
                    default:
                        return true;
                        break;
                }
                return true;
            }
        }


        #endregion

        #region Methods

        private void BindInterviewList()
        {
            if (!CandidateId.Contains(","))
            {
                odsInterviewSchedule.SelectParameters["CandidateId"].DefaultValue = CandidateId.ToString();
                this.lsvInterviewSchedule.DataBind();
            }
        }
        private void PrepareView()
        {
            BindInterviewList();
        }
        private void LoadTimeDropDowns()
        {
            int intStartTime = 12;
            string strMin = "00";
            string strAMPM = "AM";
            string strTime = "";
            ArrayList arrTime = new ArrayList();
            for (int i = 1; i <= 48; i++)
            {
                //if (i % 2 == 0)
                //{
                //    strMin = "30";
                //}
                //else
                //{
                //    strMin = "00";
                //}
                for (int j = 0; j < 60; j += 15)
                {
                    switch (j)
                    {
                        case 0:
                            strTime = intStartTime.ToString() + ":00 " + strAMPM;
                            arrTime.Add(strTime); //1.2
                            break;
                        case 15:
                            strTime = intStartTime.ToString() + ":15 " + strAMPM;
                            arrTime.Add(strTime); //1.2
                            break;
                        case 30:
                            strTime = intStartTime.ToString() + ":30 " + strAMPM;
                            arrTime.Add(strTime); //1.2
                            break;
                        default:
                            strTime = intStartTime.ToString() + ":45 " + strAMPM;
                            arrTime.Add(strTime); //1.2
                            break;
                    }
                }
                if (intStartTime == 12)
                {
                    intStartTime = 1;
                }
                else
                {
                    intStartTime++;
                }
                if (i == 24)
                {
                    strAMPM = "PM";
                }
            }
            ddlStartTime.DataSource = ddlEndTime.DataSource = arrTime; //1.2
            ddlStartTime.DataBind();
            ddlEndTime.DataBind();
            GetCurrentTime();
          
            //PopulateTimezone(ddlTimezone);//added by pravin khot on 25/May/2016

        }
        private void GetCurrentTime()
        {
            int min = DateTime.Now.Minute;
            string time = "";
            string TOtime = "";
            if (min > 45)
            {
                time = DateTime.Now.AddHours(1).AddMinutes(-min).ToString("h:mm tt");
                TOtime = DateTime.Now.AddHours(1).AddMinutes(-min).AddMinutes(15).ToString("h:mm tt");
            }
            else if (min > 30)
            {
                time = DateTime.Now.AddMinutes(-min).AddMinutes(45).ToString("h:mm tt");
                TOtime = DateTime.Now.AddHours(1).AddMinutes(-min).ToString("h:mm tt");
            }
            else if (min > 15)
            {
                time = DateTime.Now.AddMinutes(-min).AddMinutes(30).ToString("h:mm tt");
                TOtime = DateTime.Now.AddMinutes(-min).AddMinutes(45).ToString("h:mm tt");
            }
            else
            {
                time = DateTime.Now.AddMinutes(-min).AddMinutes(15).ToString("h:mm tt");
                TOtime = DateTime.Now.AddMinutes(-min).AddMinutes(30).ToString("h:mm tt");
            }
            ControlHelper.SelectListByText(ddlStartTime, time);
            ControlHelper.SelectListByText(ddlEndTime, TOtime);

            //int min = DateTime.Now.Minute;
            //string time = "";
            //string TOtime = "";
            //if (min > 30)
            //{
            //    time = DateTime.Now.AddHours(1).AddMinutes(-min).ToString("h:mm tt");
            //    TOtime = DateTime.Now.AddHours(1).AddMinutes(-min).AddMinutes(30).ToString("h:mm tt");
            //}
            //else
            //{
            //    time = DateTime.Now.AddMinutes(-min).AddMinutes(30).ToString("h:mm tt");
            //    TOtime = DateTime.Now.AddHours(1).AddMinutes(-min).ToString("h:mm tt");
            //}
            //ControlHelper.SelectListByText(ddlStartTime, time);
            //ControlHelper.SelectListByText(ddlEndTime, TOtime);
        }
        private void LoadCandidateResume()
        {
            if (CandidateId.Contains(','))
            {
                string[] hir1 = CandidateId.Split(',');
                foreach (string hir2 in hir1)
                {
                    IList<MemberDocument> documen = Facade.GetLatestResumeByMemberID(Convert.ToInt32(hir2));
                    if (documen != null)
                    {
                        foreach (MemberDocument doc in documen)
                        {
                            if (doc != null)
                            {
                                string stpath = MiscUtil.GetMemberDocumentPath(this.Page, Convert.ToInt32(hir2), doc.FileName, "Word Resume", false);
                                if (File.Exists(stpath))
                                {
                                    GenericLookup look = Facade.GetGenericLookupById(doc.FileTypeLookupId);
                                    string Link = GetDocumentLink(doc.FileName, look.Name, Convert.ToInt32(hir2));
                                    PreviewDir = GetTempFolder();
                                    string filePath = Path.Combine(PreviewDir, doc.FileName);
                                    //FileUpload1.PostedFile.SaveAs(filePath);
                                    lstAttachments.Items.Add(new ListItem(doc.FileName, filePath));
                                    string DocumentType = "Interview attachments";
                                    string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, Convert.ToInt32(hir2), doc.FileName, DocumentType, false);//1.
                                    if (File.Exists(stpath))
                                    {
                                        try
                                        {
                                            if (!File.Exists(stpath))
                                            File.Copy(stpath, strFilePath);
                                        }
                                        catch { }
                                    }

                                    hdnFileTypeId.Value = look.Name;
                                    hdnDocumentiD.Value = doc.Id.ToString();
                                    //hdnSelectedRes.Value = doc.Id.ToString();
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (chkSendEmailAlerttoInterviewers.Checked)
                            MiscUtil.ShowMessage(lblMessage, "No Resumes attached", false);
                    }
                }
            }

            else
            {

                IList<MemberDocument> documen = Facade.GetLatestResumeByMemberID(Convert.ToInt32(CandidateId));
                if (documen != null)
                {
                    foreach (MemberDocument doc in documen)
                    {
                        if (doc != null)
                        {
                            string stpath = MiscUtil.GetMemberDocumentPath(this.Page, Convert.ToInt32(CandidateId), doc.FileName, "Word Resume", false);
                            if (File.Exists(stpath))
                            {
                                                             
                                GenericLookup look = Facade.GetGenericLookupById(doc.FileTypeLookupId);
                                string Link = GetDocumentLink(doc.FileName, look.Name, Convert.ToInt32(CandidateId));
                                PreviewDir = GetTempFolder();
                                string filePath = Path.Combine(PreviewDir, doc.FileName);
                                //FileUpload1.PostedFile.SaveAs(filePath);
                                lstAttachments.Items.Add(new ListItem(doc.FileName, filePath));
                                string DocumentType = "Interview attachments";
                                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, Convert.ToInt32(CandidateId), doc.FileName, DocumentType, false);//1.
                                if (File.Exists(stpath))
                                {
                                    try
                                    {
                                        File.Copy(stpath, strFilePath);
                                    }
                                    catch { }
                                }

                                hdnFileTypeId.Value = look.Name;
                                hdnDocumentiD.Value = doc.Id.ToString();
                                //hdnSelectedRes.Value = doc.Id.ToString();
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (chkSendEmailAlerttoInterviewers.Checked)
                        MiscUtil.ShowMessage(lblMessage, "No Resumes attached", false);
                }
            }
        }
        private void LoadJobPosting()
        {
            ddlJobPosting.Items.Clear();           
                MiscUtil.GetAllByCleintIdByCandidatesId(ddlJobPosting, Facade, Convert.ToInt32(ddlClientInterviewer.SelectedValue), Convert.ToInt32(CandidateId));
                //MiscUtil.PopulateJobPostingByClientIdAndManagerId(ddlJobPosting, Facade, Convert.ToInt32(ddlClientInterviewer.SelectedValue), 0);
            
        }
        private void LoadAssociatedClient()
        {
            int _companyId = 0;
            if (ddlJobPosting.SelectedIndex > 0 || JobPostingId > 0)
            {
                if (ddlJobPosting.SelectedIndex > 0)
                {
                    _companyId = Facade.GetCompanyByJobPostingId(Convert.ToInt32(ddlJobPosting.SelectedValue));
                }               
                if (_companyId == 0)
                {
                    _companyId = Facade.GetCompanyByJobPostingId(Convert.ToInt32(JobPostingId));
                }
                if (ddlClientInterviewer.Visible)
                {
                    if (_companyId > 0)
                        ddlClientInterviewer.SelectedValue = _companyId.ToString();
                    else
                        ddlClientInterviewer.SelectedIndex = 0;
                }
                else
                {
                    if (_companyId > 0)
                    {
                        txtClientName.Text = Facade.GetCompanyNameById(_companyId);
                        hdnSelectedClientText.Value = txtClientName.Text.Trim();
                        hdnSelectedClientValue.Value = _companyId.ToString();
                    }
                }
                populateClientInterviers(_companyId);
            }
        }

        private void LoadOtherInterviewers()  // code added by pravin khot on 17/Dec/2015
        {
            int RequisitionId = 0;
            //string RequisitionName = ddlJobPosting.SelectedItem.Text;
            if (ddlJobPosting.SelectedIndex > 0 )
            {
                 RequisitionId = Convert.ToInt32(ddlJobPosting.SelectedValue);
            }
            else
            {
                RequisitionId = Convert.ToInt32(JobPostingId);
            }
           
            if (RequisitionId > 0)
            {
                IList<InterviewPanel> InterviewPanels = Facade.InterviewSchedule_OtherChkEmailId(RequisitionId);
                txtOtherInterviewers.Text = string.Empty;
                hfOtherInterviewers.Value = "";
                hfOtherInterviewers.Value = string.Empty;
                string[] interviewers = null;
                 bool   alreadyavailablee;
                 string[] emailarray = null;
                string alreadyadded="";
                string ss = "";
                string email1 = "";
                
                char[] delim = { ',' };
                if (InterviewPanels != null)
                {
                    if (hfOtherInterviewers.Value == "")
                    {
                        foreach (var a in InterviewPanels)
                        {
                            interviewers = a.Interview_Mode.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                            foreach (var s in interviewers)
                            {
                               
                               int lens = 0;
                               lens = s.Length;
                               if (lens > 1)
                                {
                                   
                                    if (hfOtherInterviewers.Value.Trim() != string.Empty)
                                    {                            
                                       
                                        alreadyadded = MiscUtil.RemoveScript(hfOtherInterviewers.Value.Trim().TrimStart(',').TrimEnd(','));

                                        alreadyavailablee = false;

                                        emailarray = alreadyadded.Split(delim, StringSplitOptions.RemoveEmptyEntries);

                                        foreach (var email in emailarray)
                                        {
                                            if (email != "")
                                            {
                                                ss = Convert.ToString(s.Trim());
                                                ss = Regex.Replace(ss, @"\s", "");
                                                email1 = Convert.ToString(email.Trim());
                                                email1 = Regex.Replace(email1, @"\s", "");
                                                if (email1.ToLower() == ss.ToLower())
                                                {
                                                    alreadyavailablee = true;
                                                    break;
                                                }
                                            }

                                        }
                                        if (alreadyavailablee == false)
                                        {
                                            hfOtherInterviewers.Value += ",";
                                            hfOtherInterviewers.Value += s;
                                            System.Web.UI.HtmlControls.HtmlGenericControl div = new System.Web.UI.HtmlControls.HtmlGenericControl();
                                            div.InnerHtml = "<span>" + s + "</span><button type='button' class='close' data-dismiss='alert'  onclick=javascript:Clicked('" + s + "')>\u00D7</button>";
                                            div.Attributes.Add("class", "EmailDiv");
                                            divContent.Controls.AddAt(0, div);
                                        }
                                    }
                                    else
                                    {
                                        hfOtherInterviewers.Value += s;
                                        System.Web.UI.HtmlControls.HtmlGenericControl div = new System.Web.UI.HtmlControls.HtmlGenericControl();
                                        div.InnerHtml = "<span>" + s + "</span><button type='button' class='close' data-dismiss='alert'  onclick=javascript:Clicked('" + s + "')>\u00D7</button>";
                                        div.Attributes.Add("class", "EmailDiv");
                                        divContent.Controls.AddAt(0, div);
                                    }
                                  
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                txtOtherInterviewers.Text = string.Empty;
                hfOtherInterviewers.Value = "";
                hfOtherInterviewers.Value = string.Empty;
            }
        }
               
        private void LoadSuggestedPanel()    // code added by pravin khot on 17/Dec/2015 using suggested panel load
        {
            int RequisitionId = 0;
            if (ddlJobPosting.SelectedIndex > 0 || JobPostingId > 0)
            {
                chkSuggestedInterviewer.Items.Clear();

                //string RequisitionName = ddlJobPosting.SelectedItem.Text; // commented by pravin khot on 19/Aug/2016
                if (ddlJobPosting.SelectedIndex > 0)
                {
                     RequisitionId = Convert.ToInt32(ddlJobPosting.SelectedValue);
                }
                else
                {
                     RequisitionId = Convert.ToInt32(JobPostingId);
                }
                if (RequisitionId > 0)
                {
                    MiscUtil.PopulateSuggestedInterviewerWithEmail(chkSuggestedInterviewer, RequisitionId, Facade);
                    chkSuggestedInterviewer = (CheckBoxList)MiscUtil.RemoveScriptForCheckBoxList(chkSuggestedInterviewer);
                    chkSuggestedInterviewer.Items.RemoveAt(0);

                    if (chkSuggestedInterviewer.Items.Count > 0)
                        dvSuugestedPanel.Visible = true;
                    else
                        dvSuugestedPanel.Visible = false;

                }
            }
        }
        private string path(string strpath)
        {
            string DocumentType = "Interview attachments";
            string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, _memberId, strpath, DocumentType, false);//1.
            if (strpath == "deletefile.doc")
                return strFilePath.Replace("\\" + strpath + "", "");
            else
                return strFilePath;
        }
        private void DeleteAttachedFile()
        {
            System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(path("deletefile.doc"));

            foreach (FileInfo file in downloadedMessageInfo.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in downloadedMessageInfo.GetDirectories())
            {
                dir.Delete(true);
            }
        }
        private void UploadFile()
        {
            if (!FileUpload1.HasFile)
            {
                MiscUtil.ShowMessage(lblMessage, "Please select a file to attach.", true);
                return;
            }
            if (true)
            {
                PreviewDir = GetTempFolder();
                string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                string filePath = Path.Combine(PreviewDir, fileName);
                FileUpload1.PostedFile.SaveAs(filePath);
                lstAttachments.Items.Add(new ListItem(FileUpload1.FileName, filePath));
                string fileUrl = "../Temp/" + CurrentUserId.ToString() + "/" + System.Uri.EscapeDataString(Path.GetFileName(filePath));

                string DocumentType = "Interview attachments";
                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, _memberId, fileName, DocumentType, false);//1.
                if (File.Exists(filePath))
                {
                    try
                    {
                        File.Move(filePath, strFilePath);
                    }
                    catch { }
                }
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                //Helper.Session.Set(SessionConstants.UserId, CurrentUserId.ToString());
            }
        }
        private string GetDocumentLink(string strFileName, string strDocumenType, int memberId)
        {
            string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, memberId, strFileName, strDocumenType, true);

            if (strFilePath != strFileName)
            {
                return "<a href='" + strFilePath + "' TARGET='_blank' >" + strFileName + "</a>";
            }
            else
            {
                return strFileName + "(File not found)";
            }
        }
        private void ClearForm()
        {
            try
            {
                lstAttachments.Items.Clear();
                //if (!chkSendEmailAlerttoInterviewers.Checked )
                //{
                divAttachments.Attributes.CssStyle.Add("display", "none");
                //}
                txtAppointmentTitle.Text = String.Empty;
                txtLocation.Text = String.Empty;
                txtRemarks.Text = String.Empty;
                txtOtherInterviewers.Text = String.Empty;
                _interviewId = 0;
                ddlJobPosting.SelectedIndex = 0;
                //chkInternalInterviewer.Items.Clear();
                chkSuggestedInterviewer.Items.Clear();//code added by pravin khot 
                ddlInterviewType.SelectedIndex = 0;
                ddllocation.SelectedIndex = 0;
                ddlTimezone.SelectedIndex = 0;
                DdlInterviewQuestionBankType.SelectedIndex = 0; //Code introduced by Prasanth on 16/Oct/2015
                // wdcEndDate.Value = DateTime.Now.ToShortDateString();
                wdcStartDate.Text = DateTime.Now.ToShortDateString();
                ddlStartTime.SelectedValue = "";
                ddlEndTime.SelectedValue = "";
                chkAllDayEvent.Checked = false;
                divET.Style.Add("display", "inline");
                divST.Style.Add("display", "inline");
                if (ddlClientInterviewer.Visible)
                    ddlClientInterviewer.SelectedIndex = 0;
                else
                    txtClientName.Text = string.Empty;
                dvClient.Visible = false;
                dvSuugestedPanel.Visible = false;//code added by pravin khot
                GetCurrentTime();
                chkAddAppointment.Checked = true;
                chkSendEmailAlerttoInterviewers.Checked = false;
                chkSendEmailAlerttoCandidates.Checked = false;
                hfOtherInterviewers.Value = String.Empty; //code added by pravin khot
                hfOtherInterviewers.Value = "";//code added by pravin khot
                for (int i = 0; i < divContent.Controls.Count; i++)
                {
                    if (divContent.Controls[i] is System.Web.UI.HtmlControls.HtmlGenericControl)
                        divContent.Controls.RemoveAt(i--);
                }

                //ddlDocument.SelectedIndex = 0;
            }
            catch { }
        }
        private void InsertInterview()
        {           
            char[] delim = { ',' };
            int[] IDs = new int[20];
            int i = 0;
            string[] candidateIds = CandidateId.Split(delim, StringSplitOptions.RemoveEmptyEntries);
            Interview newInterview = new Interview();
            foreach (string cid in candidateIds)
            {
                int activityId = 0;
                if (chkAddAppointment.Checked)
                    activityId = InsertActivity();
                int Duration = 0;
                DateTime StartDateTime = GetDateAndDuration(out Duration);

                //newInterview.ActivityId = activityId;
                newInterview.Id = activityId;
                newInterview.CreateDate = DateTime.Now;
                newInterview.CreatorId = CurrentMember.Id;
                newInterview.UpdateDate = DateTime.Now;
                newInterview.UpdatorId = CurrentMember.Id;
                //********************Code modify by pravin khot on 24/May/2016********
                newInterview.Location = MiscUtil.RemoveScript(txtLocation.Text);
                //newInterview.Location = ddllocation.SelectedValue.ToString() ; ;
                //**************************End*******************
                if (ddlClientInterviewer.Visible)
                    newInterview.ClientId = Int32.Parse(ddlClientInterviewer.SelectedValue);
                else if (txtClientName.Visible && txtClientName.Text != string.Empty)
                    newInterview.ClientId = Int32.Parse(hdnSelectedClientValue.Value);
                else
                    newInterview.ClientId = 0;
                newInterview.MemberId = Convert.ToInt32(cid); ;
                newInterview.JobPostingId = Int32.Parse(ddlJobPosting.SelectedValue);
                newInterview.Remark = MiscUtil.RemoveScript(txtRemarks.Text);
                newInterview.Feedback = String.Empty;
                newInterview.Status = 0;
                newInterview.Title = MiscUtil.RemoveScript(txtAppointmentTitle.Text);
                newInterview.TypeLookupId = Int32.Parse(ddlInterviewType.SelectedValue);
                newInterview.QuestionBankTypeLookupId = Int32.Parse(DdlInterviewQuestionBankType.SelectedValue); //Code introduced by Prasanth on 16/Oct/2015
                newInterview.StartDateTime = StartDateTime;
                newInterview.Duration = Duration;
                newInterview.AllDayEvent = chkAllDayEvent.Checked ? true : false;
                newInterview.EnableReminder = chkReminder.Checked ? true : false;
                           
                if (ddlTimezone.SelectedIndex != 0)
                {
                    newInterview.TimezoneId = Convert.ToInt32(ddlTimezone.SelectedValue);
                }
                newInterview.IsCancel = 0; //Code introduced by pravin on 4/May/2016
                //newInterview.IcsFileUIDCode = "";//Code introduced by pravin on 4/May/2016
                //*******************code added by pravin khot on 23/Dec/2015***************************
                newInterview.OtherInterviewers = "";
                newInterview.OtherInterviewers = MiscUtil.RemoveScript(hfOtherInterviewers.Value.Trim().TrimStart(',').TrimEnd(','));

                String oldString = MiscUtil.RemoveScript(hfOtherInterviewers.Value);
                if (oldString != string.Empty && oldString.StartsWith(","))
                {
                    oldString = oldString.Substring(1, oldString.Length - 1);
                    newInterview.OtherInterviewers = oldString.Trim().TrimStart(',');
                }
                else if (oldString != string.Empty)
                {
                    newInterview.OtherInterviewers = oldString.Trim().TrimStart(',');
                }
                //******************************End********************

                if (chkReminder.Checked)
                    newInterview.ReminderInterval = Int32.Parse(ddlReminder.SelectedValue);
                //if (ddlDocument.SelectedValue != "0")
                //newInterview.InterviewDocumentId = Convert.ToInt32(ddlDocument.SelectedValue);

                newInterview = Facade.AddInterview(newInterview);
                if (newInterview.JobPostingId > 0)
                    MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateInterviewScheduled, newInterview.JobPostingId, cid, CurrentMember.Id, Facade);
                if (newInterview.Id != 0)
                {
                    IDs[i] = newInterview.Id;
                    i++;
                    SaveInterviewers(newInterview.Id);
                    BindInterviewList();

                    if (Request.Url.ToString().ToLower().Contains("modals/scheduleinterview"))
                    {
                        ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Successfully saved interview');", true);
                        if (lblMessage.Text == "")
                        {
                            MiscUtil.ShowMessage(lblMessage, "Successfully saved interview", false);
                        }
                    }
                    else
                        MiscUtil.ShowMessage(lblMessage, "Successfully saved interview", false);
                }
                interviewidforapp = newInterview.Id;
            }
            foreach (int ID in IDs)
            {
                if (ID != 0)
                {
                    if (chkSendEmailAlerttoCandidates.Checked)
                        SendInterviewNotificationToCandidate(ID);
                    if (chkSendEmailAlerttoInterviewers.Checked)
                    {
                        SendInterviewNotificationToInterviewers(ID);
                    }
                }
            }
            txtEmailCandidateTemplate.Text = "";
            txtEmailInterviewerTemplate.Text = "";
            //******************code added by pravin  khot *********************
            txtOtherInterviewers.Text = string.Empty;
            hfOtherInterviewers.Value = "";
            hfOtherInterviewers.Value = string.Empty;
            //****************************End***********************************
            ClearForm();
        }
        private void UpdateInterview()
        {
            Interview interview = Facade.GetInterviewById(_interviewId);
            if (interview != null)
            {
                if (interview.IsCancel == 0 || interview.IsCancel == 2 || interview.IsCancel == 5) //IF CONDITION added by pravin khot on 6/May/2016***************
                {
                    if (interview.ActivityId > 0)
                    {
                        Activity activity = new Activity();
                        activity = Facade.GetActivityById(interview.ActivityId);
                        if (activity != null)
                        {
                            if (chkAddAppointment.Checked)
                            {
                                UpdateActivity(interview.ActivityId);
                                interview.ActivityId = interview.ActivityId;
                            }
                            else
                            {
                                Facade.DeleteActivityResourceByActivityID(interview.ActivityId);
                                Facade.DeleteActivityById(interview.ActivityId);
                                interview.ActivityId = 0;
                            }
                        }
                        else
                        {
                            if (chkAddAppointment.Checked)
                                interview.ActivityId = InsertActivity();
                        }
                    }
                    else
                    {
                        if (chkAddAppointment.Checked)
                            interview.ActivityId = InsertActivity();
                    }
                    int Duration = 0;
                    DateTime StartDateTime = GetDateAndDuration(out Duration);
                    interview.UpdateDate = DateTime.Now;
                    interview.UpdatorId = CurrentMember.Id;
                    //***********Code modify by pravin khot on 24/May/2016***********
                    interview.Location = MiscUtil.RemoveScript(txtLocation.Text);
                    //interview.Location = ddllocation.SelectedValue.ToString();
                    //***************************End******************************
                    interview.MemberId = Convert.ToInt32(CandidateId);
                    interview.Remark = MiscUtil.RemoveScript(txtRemarks.Text);
                    interview.Feedback = String.Empty;
                    interview.Status = 0;
                    interview.TypeLookupId = Int32.Parse(ddlInterviewType.SelectedValue);
                    interview.Title = MiscUtil.RemoveScript(txtAppointmentTitle.Text);
                    interview.JobPostingId = Int32.Parse(ddlJobPosting.SelectedValue);
                    //******************code added by Sumit Sonawane 1/Mar/2017 *********************
                    try
                    {
                        int CandidateHiringMatrixlevelid = 0;
                        HiringMatrix HM = Facade.getHiringMatrixLevelByJobPostingIDAndCandidateId(interview.JobPostingId, Convert.ToInt32(CandidateId));
                        if (HM != null)
                        {
                            CandidateHiringMatrixlevelid = HM.Id;
                        }

                        interview.CandidateHiringMatrixlevelid = CandidateHiringMatrixlevelid;
                    }
                    catch { }
                    //*****************************************End*************************************
                    //interview.InterviewDocumentId = ddlDocument.SelectedValue == "0" ? 0 : Convert.ToInt32(ddlDocument.SelectedValue);
                    if (ddlClientInterviewer.Visible)
                        interview.ClientId = Int32.Parse(ddlClientInterviewer.SelectedValue);
                    else if (txtClientName.Text != string.Empty && txtClientName.Visible)
                        interview.ClientId = Int32.Parse(hdnSelectedClientValue.Value);
                    else
                        interview.ClientId = 0;
                    interview.StartDateTime = StartDateTime;
                    interview.Duration = Duration;
                    interview.AllDayEvent = chkAllDayEvent.Checked ? true : false;
                    interview.EnableReminder = chkReminder.Checked ? true : false;
                    interview.TimezoneId = Int32.Parse(ddlTimezone.SelectedValue); //added by pravin khot on 25/May/2016
                    interview.QuestionBankTypeLookupId = Int32.Parse(DdlInterviewQuestionBankType.SelectedValue); //Code introduced by pravin on 4/May/2016
                    interview.IsCancel = 2; //Code introduced by pravin on 4/May/2016
                    //interview.IcsFileUIDCode  = ""; //Code introduced by pravin on 4/May/2016

                    interview.OtherInterviewers = MiscUtil.RemoveScript(hfOtherInterviewers.Value.Trim().TrimStart(',').TrimEnd(','));
                    if (chkReminder.Checked)
                        interview.ReminderInterval = Int32.Parse(ddlReminder.SelectedValue);
                    // if(MiscUtil .RemoveScript(txtAppointmentTitle .Text .ToString ())!=string .Empty )
                    {
                        interview = Facade.UpdateInterview(interview);
                        if (interview.JobPostingId > 0)
                            MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateInterviewScheduledEdited, interview.JobPostingId, CandidateId.ToString(), CurrentMember.Id, Facade);
                        if (interview.Id != 0)
                        {                            
                            Facade.DeleteInterviewInterviewerMapByInterviewId(interview.Id);
                            SaveInterviewers(interview.Id);
                            BindInterviewList();
                            //Code added by Sumit Sonawane on 1/Mar/2017 ***********
                            Interview InterviewStatusDetails = Facade.GetInterviewById(_interviewId);
                            InterviewStatusDetails.Id = interview.Id;
                            InterviewStatusDetails.IsCancel = interview.IsCancel;
                            InterviewStatusDetails.CreatorId = interview.CreatorId;
                            InterviewStatusDetails.UpdatorId = CurrentMember.Id;
                            InterviewStatusDetails.CreateDate = interview.CreateDate;
                            InterviewStatusDetails.UpdateDate = DateTime.Now;

                            InterviewStatusDetails = Facade.AddInterviewStatusDetails(InterviewStatusDetails);
                            //**************END*****************
                            MiscUtil.ShowMessage(lblMessage, "Successfully updated interview", false);
                        }
                        else
                        {
                            MiscUtil.ShowMessage(lblMessage, "Problem while saving interview.", true);
                        }
                    }
                }
                    //code added by pravin khot on 6/May/2016***************
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "This interview allready cancelled,Can not be edited.", true);
                    ClearForm(); 
                }
                //**********************END*********************************
            }
            interviewidforapp = interview.Id;
            if (chkSendEmailAlerttoInterviewers.Checked)
                SendInterviewNotificationToInterviewers(interview.Id);
            if (chkSendEmailAlerttoCandidates.Checked)
                SendInterviewNotificationToCandidate(interview.Id);

            txtEmailCandidateTemplate.Text = "";
            txtEmailInterviewerTemplate.Text = "";

        }
        private DateTime GetDateAndDuration(out int intDuration)
        {
            DateTime dtStartDateTime = DateTime.MinValue;
            DateTime dtEndDateTime = DateTime.MinValue;
            intDuration = 0;
            string strStartDate = wdcStartDate.Text;
            string strEndDate = wdcStartDate.Text;
            string strStartTime = ddlStartTime.SelectedValue;
            string strEndTime = ddlEndTime.SelectedValue;
            string strStartDateTime = strStartDate;
            string strEndDateTime = strEndDate;
            if (!chkAllDayEvent.Checked)
            {
                strStartDateTime = strStartDateTime + " " + strStartTime;
                strEndDateTime = strEndDateTime + " " + strEndTime;
                dtStartDateTime = DateTime.Parse(strStartDateTime);
                dtEndDateTime = DateTime.Parse(strEndDateTime);
                TimeSpan tsDuration = dtEndDateTime - dtStartDateTime;
                intDuration = Convert.ToInt32(tsDuration.TotalSeconds);
            }
            else
            {
                dtStartDateTime = DateTime.Parse(strStartDateTime);
                TimeSpan span = DateTime.Parse(strEndDateTime).AddDays(1).Subtract(DateTime.Parse(strStartDateTime));
                intDuration = Convert.ToInt32(span.TotalSeconds);
            }
            return dtStartDateTime;
        }
        private string BuildAppointmentDetail()
        {
            StringBuilder app = new StringBuilder();
            if (ddlClientInterviewer.SelectedIndex > 0)
            {
                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                    app.Append("BU:" + ddlClientInterviewer.SelectedItem.Text + "\n");
                else app.Append("Account:" + ddlClientInterviewer.SelectedItem.Text + "\n");
            }
            if (ddlJobPosting.SelectedIndex > 0)
            {
                app.Append("Requisition:" + ddlJobPosting.SelectedItem.Text + "\n");
            }
            app.Append("Title:" + txtAppointmentTitle.Text + "\n");
            if (txtLocation.Text != string.Empty)
                app.Append("Location:" + txtLocation.Text + "\n");

            if (ddlInterviewType.SelectedIndex > 0)
            {
                app.Append("Interview Type:" + ddlInterviewType.SelectedItem.Text + "\n");
            }
            StringBuilder intenalinterviewers = new StringBuilder();
            foreach (ListItem list in chkInternalInterviewer.Items)
            {

                if (list.Selected)
                {
                    if (intenalinterviewers.ToString() != "") intenalinterviewers.Append(", ");
                    intenalinterviewers.Append(list.Text);
                }
            }
            if (intenalinterviewers.ToString() != "")
                app.Append("Internal Interviewers:" + intenalinterviewers.ToString() + "\n");


            StringBuilder Clientinterviewers = new StringBuilder();
            foreach (ListItem list in chkClientInterviers.Items)
            {

                if (list.Selected)
                {
                    if (Clientinterviewers.ToString() != "") Clientinterviewers.Append(", ");
                    Clientinterviewers.Append(list.Text);
                }
            }
            if (Clientinterviewers.ToString() != "")
                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                    app.Append("BU Interviewers:" + Clientinterviewers.ToString() + "\n");
                else app.Append("Client Interviewers:" + Clientinterviewers.ToString() + "\n");

            if (txtRemarks.Text != "")
                app.Append("Notes:" + MiscUtil.RemoveScript(txtRemarks.Text) + "\n");


            return app.ToString();
        }
        private int InsertActivity()
        {
            int intDuration = 0;
            DateTime dtStartDateTime = GetDateAndDuration(out intDuration);
            Activity newActivity = new Activity();
            newActivity.StartDateTimeUtc = TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime); //dtStartDateTime;
            newActivity.Duration = intDuration;
            newActivity.Subject = MiscUtil.RemoveScript(txtAppointmentTitle.Text);
            // newActivity.ActivityDescription = MiscUtil .RemoveScript (txtRemarks.Text);
            newActivity.AllDayEvent = chkAllDayEvent.Checked;
            newActivity.Location = MiscUtil.RemoveScript(txtLocation.Text);
            newActivity.Status = 0;
            newActivity.Importance = 1;
            newActivity.RecurrenceID = -999;
            newActivity.ResourceName = CurrentMember.Id.ToString();
            newActivity.ActivityDescription = BuildAppointmentDetail();
            if (chkReminder.Checked)
            {
                newActivity.EnableReminder = true;
                newActivity.ReminderInterval = Int32.Parse(ddlReminder.SelectedValue);
            }
            else
                newActivity.EnableReminder = false;
            if (newActivity.Subject.ToString() != string.Empty)
            {
                Facade.AddActivity(newActivity);
            }
            return newActivity.ActivityID;
        }
        private bool UpdateActivity(int activityId)
        {
            Activity activity = Facade.GetActivityById(activityId);
            if (activity != null)
            {
                Int32 intDuration = 0;
                DateTime dtStartDateTime = GetDateAndDuration(out intDuration);
                activity.StartDateTimeUtc = TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime); //dtStartDateTime;
                activity.Duration = intDuration;
                activity.Subject = MiscUtil.RemoveScript(txtAppointmentTitle.Text);
                activity.ActivityDescription = BuildAppointmentDetail(); //MiscUtil .RemoveScript (txtRemarks.Text);
                activity.AllDayEvent = chkAllDayEvent.Checked;
                activity.Location = MiscUtil.RemoveScript(txtLocation.Text);
                activity.Status = 0;
                activity.Importance = 1;
                activity.RecurrenceID = -999;
                activity.ResourceName = CurrentMember.Id.ToString();
                if (chkReminder.Checked)
                {
                    activity.EnableReminder = true;
                    activity.ReminderInterval = Int32.Parse(ddlReminder.SelectedValue);
                }
                else
                    activity.EnableReminder = false;
                if (activity.Subject.ToString() != string.Empty)
                {
                    try
                    {
                        Facade.UpdateActivity(activity);
                    }
                    catch { }	
                }
                return true;
            }
            return false;
        }
        private void SaveInterviewers(int interviewId)
        {
            foreach (ListItem lstItm in chkInternalInterviewer.Items)
            {
                if (lstItm.Selected)
                {
                    InterviewInterviewerMap Map = new InterviewInterviewerMap();
                    Map.InterviewId = interviewId;
                    Map.InterviewerId = Int32.Parse(lstItm.Value);
                    Map.CreatorId = CurrentMember.Id;
                    Map.UpdatorId = CurrentMember.Id;
                    Map.IsClient = false;
                    Facade.AddInterviewInterviewerMap(Map);
                }
            }
            //***********************code added by pravin khot on 23/Dec/2015 for suggested panel interviewer save in DB**********
            foreach (ListItem item in chkSuggestedInterviewer.Items)
            {
                if (item.Selected)
                {
                    string InterviewrMode = string.Empty;
                    InterviewInterviewerMap Map = new InterviewInterviewerMap();
                    Map.InterviewId = interviewId;
                    Map.InterviewerId = Int32.Parse(item.Value);
                    InterviewrMode = Facade.SuggestedInterviewer_Id(Map.InterviewerId);
                    if (InterviewrMode == "USER")
                    {
                        Map.IsClient = false;
                    }
                    else
                    {
                        Map.IsClient = true;
                    }
                    Map.CreatorId = CurrentMember.Id;
                    Map.UpdatorId = CurrentMember.Id;

                    Facade.AddSuggestedInterviewInterviewerMap(Map);

                }
            }
            //************************************End*****************************************
            if (chkClientInterviers.Visible)
            {
                foreach (ListItem item in chkClientInterviers.Items)
                {
                    if (item.Selected)
                    {
                        InterviewInterviewerMap Map = new InterviewInterviewerMap();
                        Map.InterviewId = interviewId;
                        Map.InterviewerId = Int32.Parse(item.Value);
                        Map.IsClient = true;
                        Map.CreatorId = CurrentMember.Id;
                        Map.UpdatorId = CurrentMember.Id;
                        Facade.AddInterviewInterviewerMap(Map);
                    }
                }
            }
        }
        //private string getTimeFormat(DateTime Date)
        //{
        //    string[] dd = Date.ToShortTimeString().Split(':');
        //    string Time = string.Empty;
        //    if (Int32.Parse(dd[0].ToString()) > 12)
        //    {
        //        Time = (Int32.Parse(dd[0].ToString()) - 12).ToString() + ":" + dd[1].ToString() + " PM";
        //    }
        //    else
        //    {
        //        Time = dd[0].ToString() + ":" + dd[1].ToString() + " AM";
        //    }
        //    return Time;
        //}
        private string getTimeFormat(DateTime Date)
        {
            string[] dd = Date.ToShortTimeString().Split(':');
            string Time = string.Empty;
            if (Int32.Parse(dd[0].ToString()) > 11)
            {
                if (Int32.Parse(dd[0].ToString()) == 12)
                    Time = dd[0].ToString() + ":" + dd[1].ToString() + " PM";
                else
                    Time = (Int32.Parse(dd[0].ToString()) - 12).ToString() + ":" + dd[1].ToString() + " PM";
            }
            else
            {
                if (Int32.Parse(dd[0].ToString()) == 00)
                    Time = 12 + ":" + dd[1].ToString() + " AM";
                else
                    Time = (Int32.Parse(dd[0].ToString())).ToString() + ":" + dd[1].ToString() + " AM";
                //Time = dd[0].ToString() + ":" + dd[1].ToString() + " AM";
            }
            return Time;
        }

        private void PrepareEditView(int interviewId)
        {
            ClearForm();
            LoadCandidateResume();
            if (interviewId != 0)
            {
                MemberInterview memberInterview = Facade.GetMemberInterviewById(interviewId);
                //ddlJobPosting.SelectedValue = memberInterview.JobPostingId.ToString();
                if (memberInterview != null)
                {
                    ControlHelper.SelectListByValue(ddlJobPosting, memberInterview.JobPostingId.ToString());
                    ControlHelper.SelectListByValue(ddlDocument, memberInterview.InterviewDocumentId.ToString());
                    txtAppointmentTitle.Text = MiscUtil.RemoveScript(memberInterview.Title, string.Empty);
                    //************Code modify by pravin khot on 24/May/2016*************
                    txtLocation.Text = MiscUtil.RemoveScript(memberInterview.Location, string.Empty);
                    if (memberInterview.Location == "")
                    {
                        ddllocation.SelectedValue = "0";
                        divlocation.Visible = false;
                    }
                    else
                    {                       
                        GenericLookup genericlookup = new GenericLookup();
                        //genericlookup = Facade.GetGenericLookupById(Convert.ToInt32(memberInterview.Location));
                        //genericlookup = Facade.GetGenericLookupByDescription(memberInterview.Location);
                        //if (genericlookup.Id  <= 0)
                        //{
                        //    divlocation.Visible = false;
                        //}
                        //else
                        //{
                            divlocation.Visible = true;
                            txtLocation.Text = MiscUtil.RemoveScript(memberInterview.Location, string.Empty);
                            //ddllocation.SelectedValue = genericlookup.Id.ToString();
                            ddllocation.SelectedValue = "0";
                        //}
                    }
                   
                    //***************************END*****************************
                    txtRemarks.Text = MiscUtil.RemoveScript(memberInterview.Remark, string.Empty);
                    ddlInterviewType.SelectedValue = memberInterview.TypeLookupId.ToString();
                    ddlTimezone.SelectedValue = memberInterview.TimezoneId.ToString();//added by pravin khot on 25/May/2016
                    DdlInterviewQuestionBankType.SelectedValue = memberInterview.QuestionBankTypeLookupId.ToString(); //Code introduced by Prasanth on 16/Oct/2015
                    // txtOtherInterviewers.Text = MiscUtil.RemoveScript(memberInterview.OtherInterviewers, string.Empty);
                    if (memberInterview.StartDateTime != DateTime.MinValue)
                    {
                        wdcStartDate.Value = memberInterview.StartDateTime.ToShortDateString();
                      
                        //ddlStartTime.SelectedItem.Text= getTimeFormat(memberInterview.StartDateTime);//memberInterview.StartDateTime.ToShortTimeString();
                        string time = getTimeFormat(memberInterview.StartDateTime);
                        ControlHelper.SelectListByText(ddlStartTime, time);
                        DateTime dtEnd = DateTime.MinValue;
                        if (memberInterview.AllDayEvent)
                            dtEnd = memberInterview.StartDateTime.AddDays(-1).AddSeconds(Convert.ToDouble(memberInterview.Duration));
                        else
                            dtEnd = memberInterview.StartDateTime.AddSeconds(Convert.ToDouble(memberInterview.Duration));
                        //wdcEndDate.Value = dtEnd.ToShortDateString();
                        string[] dd = dtEnd.ToShortTimeString().Split(':');
                        string Time = string.Empty;
                        if (Int32.Parse(dd[0].ToString()) > 11)
                        {
                            if (Int32.Parse(dd[0].ToString()) == 12)
                                Time = dd[0].ToString() + ":" + dd[1].ToString() + " PM";
                            else
                                Time = (Int32.Parse(dd[0].ToString()) - 12).ToString() + ":" + dd[1].ToString() + " PM";
                        }
                        else
                        {
                            if (Int32.Parse(dd[0].ToString()) == 00)
                                Time = 12 + ":" + dd[1].ToString() + " AM";
                            else
                                Time = (Int32.Parse(dd[0].ToString())).ToString() + ":" + dd[1].ToString() + " AM";
                            //Time = dd[0].ToString() + ":" + dd[1].ToString() + " AM";
                        }
                        //if (Int32.Parse(dd[0].ToString()) > 12)
                        //{
                        //    Time = (Int32.Parse(dd[0].ToString()) - 12).ToString() + ":" + dd[1].ToString() + " PM";
                        //}
                        //else
                        //{
                        //    Time = dd[0].ToString() + ":" + dd[1].ToString() + " AM";
                        //}
                        try
                        {
                            //ddlEndTime.SelectedItem.Text = getTimeFormat(dtEnd);
                            ControlHelper.SelectListByText(ddlEndTime, Time);
                        }
                        catch
                        {
                            ddlEndTime.SelectedValue = "";
                        }
                    }
                    chkReminder.Checked = memberInterview.EnableReminder;
                    ddlReminder.SelectedValue = memberInterview.ReminderInterval.ToString();
                    chkAllDayEvent.Checked = memberInterview.AllDayEvent;
                    MiscUtil.PopulateMemberListWithEmailByRole(chkInternalInterviewer, ContextConstants.ROLE_EMPLOYEE, Facade);
                    chkInternalInterviewer = (CheckBoxList)MiscUtil.RemoveScriptForCheckBoxList(chkInternalInterviewer);
                    chkInternalInterviewer.Items.RemoveAt(0);


                    if (ddlClientInterviewer.Visible)
                        ddlClientInterviewer.SelectedValue = memberInterview.CompanyId.ToString();
                    else
                    {
                        txtClientName.Text = MiscUtil.RemoveScript(memberInterview.CompanyName, string.Empty);
                        hdnSelectedClientText.Value = MiscUtil.RemoveScript(memberInterview.CompanyName, string.Empty);
                        hdnSelectedClientValue.Value = memberInterview.CompanyId.ToString();
                    }
                    //added by pravin khot on 14/June/2016************
                    ddlJobPosting.Items.Clear();
                    MiscUtil.GetAllByCleintIdByCandidatesId(ddlJobPosting, Facade, Convert.ToInt32(ddlClientInterviewer.SelectedValue), Convert.ToInt32(CandidateId));
                    ControlHelper.SelectListByValue(ddlJobPosting, memberInterview.JobPostingId.ToString());
                    //**************END*******************************
                    populateClientInterviers(memberInterview.CompanyId);

                    IList<InterviewInterviewerMap> interviewermap = new List<InterviewInterviewerMap>();
                    interviewermap = Facade.GetAllInterviewersByInterviewId(interviewId);
                    if (interviewermap != null)
                    {
                        foreach (ListItem item in chkInternalInterviewer.Items)
                        {
                            foreach (InterviewInterviewerMap map in interviewermap)
                            {
                                if (item.Value == map.InterviewerId.ToString() && !map.IsClient)
                                {
                                    item.Selected = true;
                                }
                            }
                        }
                        foreach (ListItem item in chkClientInterviers.Items)
                        {
                            foreach (InterviewInterviewerMap map in interviewermap)
                            {
                                if (item.Value == map.InterviewerId.ToString() && map.IsClient)
                                {
                                    item.Selected = true;
                                }
                            }
                        }
                    }
                    //////////////////////////////code add pravin khot on 4/Jan/2016 //////////////////////////////////

                    chkSuggestedInterviewer.Items.Clear();
                    string RequisitionName = ddlJobPosting.SelectedItem.Text;
                    int RequisitionId = Convert.ToInt32(ddlJobPosting.SelectedValue);

                    if (RequisitionId > 0)
                    {
                        MiscUtil.PopulateSuggestedInterviewerWithEmail(chkSuggestedInterviewer, RequisitionId, Facade);
                        chkSuggestedInterviewer = (CheckBoxList)MiscUtil.RemoveScriptForCheckBoxList(chkSuggestedInterviewer);
                        chkSuggestedInterviewer.Items.RemoveAt(0);

                        if (chkSuggestedInterviewer.Items.Count > 0)
                        {
                            dvSuugestedPanel.Visible = true;

                            foreach (ListItem item in chkSuggestedInterviewer.Items)
                            {
                                item.Selected = false;
                                int InterviewerId = 0;
                                InterviewerId = Convert.ToInt32(item.Value);

                                if (Facade.MemberInterview_SuggestedInterviewerId(interviewId, InterviewerId))
                                {
                                    item.Selected = true;
                                }
                            }
                        }

                        else
                        {
                            dvSuugestedPanel.Visible = false;
                        }

                        txtOtherInterviewers.Text = string.Empty;
                        hfOtherInterviewers.Value = "";

                        //************************CODE COMMENT PRAVIN KHOT*************************
                        //IList<InterviewPanel> InterviewPanels = Facade.InterviewSchedule_OtherChkEmailId(RequisitionId);
                        //string[] interviewers = null;
                        //char[] delim = { ',' };
                        //if (InterviewPanels != null)
                        //{
                        //    foreach (var a in InterviewPanels)
                        //    {
                        //        interviewers = a.Interview_Mode.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                        //        foreach (var s in interviewers)
                        //        {
                        //            System.Web.UI.HtmlControls.HtmlGenericControl div = new System.Web.UI.HtmlControls.HtmlGenericControl();
                        //            div.InnerHtml = "<span>" + s + "</span><button type='button' class='close' data-dismiss='alert'  onclick=javascript:Clicked('" + s + "')>\u00D7</button>";
                        //            div.Attributes.Add("class", "EmailDiv");
                        //            divContent.Controls.AddAt(0, div);
                        //            if (hfOtherInterviewers.Value.Trim() != string.Empty) hfOtherInterviewers.Value += ",";
                        //            hfOtherInterviewers.Value += s;
                        //        }
                        //    }
                        //}
                    }

                    //////////////////////////////////// END ///////////////////////////////////
                    if (memberInterview.ActivityId > 0)
                    {
                        Activity activity = new Activity();
                        activity = Facade.GetActivityById(memberInterview.ActivityId);
                        if (activity != null)
                        {
                            chkAddAppointment.Checked = true;
                        }
                        else
                            chkAddAppointment.Checked = false;
                    }
                    else
                        chkAddAppointment.Checked = false;
                    if (chkAllDayEvent.Checked)
                    {
                        divST.Style.Add("display", "none");
                        divET.Style.Add("display", "none");
                    }
                    else
                    {
                        divET.Style.Add("display", "inline");
                        divST.Style.Add("display", "inline");
                    }
                    _interviewId = interviewId;
                    chkSendEmailAlerttoInterviewers.Checked = false;
                    string[] interviewers = null;
                    char[] delim = { ',' };
                    if (memberInterview.OtherInterviewers != null)
                    {
                        interviewers = memberInterview.OtherInterviewers.Split(delim, StringSplitOptions.RemoveEmptyEntries);


                        foreach (string s in interviewers)
                        {
                            System.Web.UI.HtmlControls.HtmlGenericControl div = new System.Web.UI.HtmlControls.HtmlGenericControl();
                            div.InnerHtml = "<span>" + s + "</span><button type='button' class='close' data-dismiss='alert'  onclick=javascript:Clicked('" + s + "')>\u00D7</button>";
                            div.Attributes.Add("class", "EmailDiv");
                            divContent.Controls.AddAt(0, div);
                            if (hfOtherInterviewers.Value.Trim() != string.Empty) hfOtherInterviewers.Value += ",";
                            hfOtherInterviewers.Value += s;
                        }
                    }
                }
            }
        }
        public static string GetAppFileContentFromUrl(string url)
        {
            string fileContent = string.Empty;
            string test = "<html><head><title></title><style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head><body><P>The appointment details are:</P><table><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>Start Time:</STRONG> [InterviewStartTime]</td></tr><tr><td><STRONG>End Time:</STRONG> [InterviewEndTime]</td></tr><tr><td><STRONG>Location:</STRONG> [Location]</td></tr><tr><td><STRONG>Interviewers:</STRONG> [NameOfInterviewers]</td></tr></table></body></html>";
            try
            {
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                fileContent = StringHelper.Convert(reader.ReadToEnd());
                reader.Close();
                response.Close();
            }
            catch (System.Exception)
            {

            }
            if (fileContent == "" || fileContent == null)
            {
                fileContent = test;
            }
            return fileContent;
        }
        public static string GetAppFileContentFromUrlrecruiter(string url)
        {
            string fileContent = string.Empty;
            // string test = "<html><head><title></title><style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head><body><P>The appointment details are:</P><table><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>Start Time:</STRONG> [InterviewStartTime]</td></tr><tr><td><STRONG>End Time:</STRONG> [InterviewEndTime]</td></tr><tr><td><STRONG>Location:</STRONG> [Location]</td></tr><tr><td><STRONG>Interviewers:</STRONG> [NameOfInterviewers]</td></tr></table></body></html>";

            string test = "<html><head><title></title><style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head><body><P>Appointment Details:</P><table><tr><td><STRONG>Interview / Meeting Date and Time :</STRONG>[InterviewDate]   [InterviewStartTime] to  [InterviewEndTime]</td></tr><tr><td><STRONG>Interview Type:</STRONG> [InterviewType]</td></tr><tr><td><STRONG>Interview Location:</STRONG> [Location]</td></tr><tr><td><STRONG>Interviewers:</STRONG> [NameOfInterviewers]</td></tr><tr><td><STRONG>Other Interviewers :</STRONG> [NameOfOtherInterviewers]</td></tr><tr><td><STRONG>Candidate Name:</STRONG> [NameOfCandidates] - [CandidateEmail]</td></tr><tr><td><STRONG>Candidate Contact:</STRONG> [ContactOfCandidates]</td></tr></table></body></html>";
            try
            {
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                fileContent = StringHelper.Convert(reader.ReadToEnd());
                reader.Close();
                response.Close();
            }
            catch (System.Exception)
            {

            }
            if (fileContent == "" || fileContent == null)
            {
                fileContent = test;
            }
            return fileContent;
        }

        public static string GetAppFileContentFromUrlinterviewer(string url)
        {
            string fileContent = string.Empty;
            // string test = "<html><head><title></title><style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head><body><P>The appointment details are:</P><table><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>Start Time:</STRONG> [InterviewStartTime]</td></tr><tr><td><STRONG>End Time:</STRONG> [InterviewEndTime]</td></tr><tr><td><STRONG>Location:</STRONG> [Location]</td></tr><tr><td><STRONG>Interviewers:</STRONG> [NameOfInterviewers]</td></tr></table></body></html>";

            string test = "<html><head><title></title><style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head><body><P>Appointment Details:</P><table><tr><td><STRONG>Interview / Meeting Date and Time :</STRONG>[InterviewDate]   [InterviewStartTime] to  [InterviewEndTime]</td></tr><tr><td><STRONG>Interview Type:</STRONG> [InterviewType]</td></tr><tr><td><STRONG>Interview Location:</STRONG> [Location]</td></tr><tr><td><STRONG>Interviewers:</STRONG> [NameOfInterviewers]</td></tr><tr><td><STRONG>Other Interviewers :</STRONG> [NameOfOtherInterviewers]</td></tr><tr><td><STRONG>Candidate Name:</STRONG> [NameOfCandidates] - [CandidateEmail]</td></tr><tr><td><STRONG>Candidate Contact:</STRONG> [ContactOfCandidates]</td></tr></table></body></html>";
            try
            {
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                fileContent = StringHelper.Convert(reader.ReadToEnd());
                reader.Close();
                response.Close();
            }
            catch (System.Exception)
            {

            }
            if (fileContent == "" || fileContent == null)
            {
                fileContent = test;
            }
            return fileContent;
        }
        private string GetFileContentFromUrl(string url)//modify by Sumit Sonawane on 07/June/2016
        {
            string fileContent = string.Empty;
           // test = "<html><head><title></title><style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head><body><P><STRONG>Dear [InterviewerName]</STRONG>,</P><P> Thank you for showing interest in <STRONG> [STANDARDAPPLICATIONSETTINGSCOMPANYNAME]</STRONG>.As per our discussion, we are pleased to <br> schedule your interview. Please find below the details for your interview.</P><table><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>Time:</STRONG> [InterviewStartTime]</td></tr><tr><td><STRONG>Venue:</STRONG> [Location]</td></tr><tr><td><STRONG>Contact:</STRONG>[ContactMember]<br><br></td></tr></table><P>If you are not able to come for the interview on above date kindly let us know in advance to <br> reschedule it.</P><P>Please reach out to me if you have any queries.<br><br></P><P>Warm Regards,</P><P>[Signature]</P></body></html>";

            //code modify added new if condition by pravin khot on 9/May/2016
            StringBuilder test = new StringBuilder();
            if (isNew)
            {
                using (StreamReader reader = new StreamReader(Server.MapPath("~/MailTemplate/InterviewFileContentForCandidate.htm")))
                {
                    fileContent = reader.ReadToEnd();
                }

                fileContent = fileContent.Replace("{InterviewerName}", "[InterviewerName]");
                fileContent = fileContent.Replace("{InterviewDate}", "[InterviewDate]");
                fileContent = fileContent.Replace("{InterviewStartTime}", "[InterviewStartTime]");


                fileContent = fileContent.Replace("{InterviewType}", "[InterviewType]");
                fileContent = fileContent.Replace("{Location}", "[Location]");

                fileContent = fileContent.Replace("{ContactMember}", "[ContactMember]");
                fileContent = fileContent.Replace("{ContactMemberMailID}", "[ContactMemberMailID]");

                fileContent = fileContent.Replace("{Signature}", "[Signature]");
            }
            else
            {
                using (StreamReader reader = new StreamReader(Server.MapPath("~/MailTemplate/InterviewFileContentForCandidateReschedule.htm")))
                {
                    fileContent = reader.ReadToEnd();
                }
                fileContent = fileContent.Replace("{InterviewerName}", "[InterviewerName]");
                fileContent = fileContent.Replace("{InterviewDate}", "[InterviewDate]");
                fileContent = fileContent.Replace("{InterviewStartTime}", "[InterviewStartTime]");

                fileContent = fileContent.Replace("{InterviewType}", "[InterviewType]");
                fileContent = fileContent.Replace("{Location}", "[Location]");

                fileContent = fileContent.Replace("{ContactMember}", "[ContactMember]");
                fileContent = fileContent.Replace("{ContactMemberMailID}", "[ContactMemberMailID]");

                fileContent = fileContent.Replace("{Signature}", "[Signature]");
            }
                 //test = "<html><head><title></title><style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head><body><P><STRONG>Dear [InterviewerName]</STRONG>,</P><P> Thank you for showing interest in <STRONG> [STANDARDAPPLICATIONSETTINGSCOMPANYNAME]</STRONG>.As per our discussion, we are pleased to <br> schedule your interview. Please find below the details for your interview.</P><table><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>Time:</STRONG> [InterviewStartTime]</td></tr><tr><td><STRONG>Venue:</STRONG> [Location]</td></tr><tr><td><STRONG>Contact:</STRONG>[ContactMember]<br><br></td></tr></table><P>If you are not able to come for the interview on above date kindly let us know in advance to <br> reschedule it.</P><P>Please reach out to me if you have any queries.<br><br></P><P>Warm Regards,</P><P>[Signature]</P></body></html>";
               
            //    test.Append("<html><head><title></title>");
            //    test.Append("<style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head>");
            //    test.Append("<body><P><STRONG>Dear [InterviewerName]</STRONG>,</P>");
            //    test.Append("<P> Thank you for showing interest in <STRONG> [STANDARDAPPLICATIONSETTINGSCOMPANYNAME]</STRONG>. As per our discussion, we are pleased to schedule your interview. Please find below the details for your interview.</P>");
            //    test.Append("<table><tr>");
            //    test.Append("<td width=\"100\"><STRONG>Date</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewDate]</td>");
            //    test.Append("</tr><tr>");
            //    test.Append("<td width=\"100\"><STRONG>Time</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewStartTime]</td>");
            //    test.Append("</tr><tr>");
            //    test.Append("<td width=\"100\"><STRONG>Interview Mode</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewType]</td>");
            //    test.Append("</tr><tr>");
            //    test.Append("<td width=\"100\"><STRONG>Venue</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [Location]</td>");
            //    test.Append("</tr><tr>");
            //    test.Append("<td width=\"100\"><STRONG>Contact</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500>[ContactMember] - [ContactMemberMailID]</td>");
            //    test.Append("</tr></table>");
            //    test.Append("<P>If you are not able to attend the interview on above date kindly let us know in advance to reschedule it.</P>");
            //    test.Append("<P>Please reach out to me if you have any queries.</P>");
            //    test.Append("<P><STRONG>[Signature]</STRONG></P>");
            //    test.Append("</body></html>");
            //}
            //else
            //{
            //    //test = "<html><head><title></title><style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head><body><P><STRONG>Dear [InterviewerName]</STRONG>,</P><P> The Interview has been rescheduled. Please find below the details for your interview. <br> </P>
            //    //<table><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr>
            //    //<tr><td><STRONG>Time:</STRONG> [InterviewStartTime]</td></tr><tr><td><STRONG>Venue:</STRONG> [Location]</td></tr>
            //    //<tr><td><STRONG>Contact:</STRONG>[ContactMember]<br><br></td></tr></table><P>If you are not able to come for the interview on above date kindly let us know in advance to <br> reschedule it.</P><P>Please reach out to me if you have any queries.<br><br></P><P>Warm Regards,</P><P>[Signature]</P></body></html>";
            //    test.Append("<html><head><title></title>");
            //    test.Append("<style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head>");
            //    test.Append("<body><P><STRONG>Dear [InterviewerName]</STRONG>,</P>");
            //    test.Append("<P> The Interview has been rescheduled. Please find below the details for your interview.</P>");
            //    test.Append("<table><tr>");
            //    test.Append("<td width=\"100\"><STRONG>Date</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewDate]</td>");
            //    test.Append("</tr><tr>");
            //    test.Append("<td width=\"100\"><STRONG>Time</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewStartTime]</td>");
            //    test.Append("</tr><tr>");
            //    test.Append("<td width=\"100\"><STRONG>Venue</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [Location]</td>");
            //    test.Append("</tr><tr>");
            //    test.Append("<td width=\"100\"><STRONG>Contact</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500>[ContactMember] - [ContactMemberMailID]</td>");
            //    test.Append("</tr></table>");
            //    test.Append("<P>If you are not able to attend the interview on above date kindly let us know in advance to reschedule it.</P>");
            //    test.Append("<P>Please reach out to me if you have any queries.</P>");
            //    test.Append("<P><STRONG>[Signature]</STRONG></P>");
            //    test.Append("</body></html>");
            //}
            //***********END*********************************S
            try
            {
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                fileContent = StringHelper.Convert(reader.ReadToEnd());
                reader.Close();
                response.Close();
            }
            catch (System.Exception)
            {

            }
            if (fileContent == "" || fileContent == null)
            {
                fileContent = test.ToString();//*********Code Modified By pravin On 09/May/2016***********
            }
            return fileContent;
        }

        //public static string GetFileContentFromUrlForInterviewers(string url)//commented by pravin khot on 9/May/2016
        private string GetFileContentFromUrlForInterviewers(string url)//modify by Sumit Sonawane on 06/June/2016
        {
            string fileContent = string.Empty;
            StringBuilder test = new StringBuilder();
            //Code introduced by Prasanth on 16/Dec/2015 Start
            //line commented by pravin khot on 9/May/2016***************
            //string test = "<head><title></title><style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head><body><P><STRONG>Dear [InterviewerName],</STRONG></P><P>This is an invitation for conducting an Interview</P><table><tr><td><STRONG>[InterviewTitle] | [ReqCode]-[JobTitle]</STRONG></td></tr><tr><td></td><tr><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>From Time:</STRONG> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td></tr><tr><td></td><tr><tr><td><STRONG>Interview Type:</STRONG> [InterviewType]</td></tr><tr><td><STRONG>Interview Location:</STRONG> [Location]</td></tr><tr><td></td><tr><tr><td><STRONG>Candidate Name:</STRONG> [NameOfCandidates] - [CandidateEmail]</td></tr><tr><td><STRONG>Candidate Contact:</STRONG> [ContactOfCandidates]</td></tr></table><P>Thank you for your time</P><table><tr><td>Regards,</td></tr><tr><td>[RecruiterName]</td></tr></table></body></html>";
            //*******************END******************
            //*******Code added by pravin khot on 9/May/2016**************
            if (isNew)
            {
                //test = "<head><title></title><style>body, td, p{	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;	}</style></head><body><P><STRONG>Dear [InterviewerName]</STRONG>,</P><P>This is an invitation for conducting an Interview</P><table><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>From Time:</STRONG> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td></tr><tr><td><STRONG>Interview Type:</STRONG> [InterviewType]</td></tr><tr><td><STRONG>Interview Location:</STRONG> [Location]</td></tr><tr><td><STRONG>Interviewers:</STRONG> [NameOfInterviewers]</td></tr><tr><td><STRONG>Candidate Name:</STRONG> [CandidateName]</td></tr><tr><td><STRONG>Candidate Contact:</STRONG> [CandidateContact]</td></tr></table><table><tr><td>Please provide your feedback/Assessment :<STRONG> <a href=[FeedbackLink]>Click here for Feedback /Assessment form</a> </STRONG> </td></tr></table><P>Thank you for your time</P><table><tr><td>Regards,</td></tr><tr><td>[RecruiterName]</td></tr></table><P><STRONG>[Signature]</STRONG></P></body></html>";

                using (StreamReader reader = new StreamReader(Server.MapPath("~/MailTemplate/InterviewFileContentForInterviewer.htm")))
                {
                    fileContent = reader.ReadToEnd();
                }

                fileContent = fileContent.Replace("{InterviewerName}", "[InterviewerName]");
                fileContent = fileContent.Replace("{InterviewDate}", "[InterviewDate]");
                fileContent = fileContent.Replace("{InterviewStartTime}", "[InterviewStartTime]");
                fileContent = fileContent.Replace("{InterviewEndTime}", "[InterviewEndTime]");
                fileContent = fileContent.Replace("{InterviewType}", "[InterviewType]");
                fileContent = fileContent.Replace("{Location}", "[Location]");
                fileContent = fileContent.Replace("{NameOfInterviewers}", "[NameOfInterviewers]");
                fileContent = fileContent.Replace("{NameOfOtherInterviewers}", "[NameOfOtherInterviewers]");
                fileContent = fileContent.Replace("{CandidateName}", "[CandidateName]");
                fileContent = fileContent.Replace("{CandidateContact}", "[CandidateContact]");
                fileContent = fileContent.Replace("{FeedbackLink}", "[FeedbackLink]");
                fileContent = fileContent.Replace("{RecruiterName}", "[RecruiterName]");
                fileContent = fileContent.Replace("{CandidateEmail}", "[CandidateEmail]");
                fileContent = fileContent.Replace("{NOTE}", "[NOTE]");
                fileContent = fileContent.Replace("{Signature}", "[Signature]");
            }
            else
            {
                using (StreamReader reader = new StreamReader(Server.MapPath("~/MailTemplate/InterviewFileContentForInterviewerReschedule.htm")))
                {
                    fileContent = reader.ReadToEnd();
                }

                fileContent = fileContent.Replace("{InterviewerName}", "[InterviewerName]");
                fileContent = fileContent.Replace("{InterviewDate}", "[InterviewDate]");
                fileContent = fileContent.Replace("{InterviewStartTime}", "[InterviewStartTime]");
                fileContent = fileContent.Replace("{InterviewEndTime}", "[InterviewEndTime]");
                fileContent = fileContent.Replace("{InterviewType}", "[InterviewType]");
                fileContent = fileContent.Replace("{Location}", "[Location]");
                fileContent = fileContent.Replace("{NameOfInterviewers}", "[NameOfInterviewers]");
                fileContent = fileContent.Replace("{NameOfOtherInterviewers}", "[NameOfOtherInterviewers]");
                fileContent = fileContent.Replace("{CandidateName}", "[CandidateName]");
                fileContent = fileContent.Replace("{CandidateContact}", "[CandidateContact]");
                fileContent = fileContent.Replace("{FeedbackLink}", "[FeedbackLink]");
                fileContent = fileContent.Replace("{RecruiterName}", "[RecruiterName]");
                fileContent = fileContent.Replace("{CandidateEmail}", "[CandidateEmail]");
                fileContent = fileContent.Replace("{NOTE}", "[NOTE]");
                fileContent = fileContent.Replace("{Signature}", "[Signature]");
            }
            try
            {
                // string smt = "~/" + url; M:\SP2_31May\1.7.0_SP2\TPS360.WEBUI\MailTemplate\InterviewMailTemplate.htm
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                fileContent = StringHelper.Convert(reader.ReadToEnd());
                reader.Close();

                response.Close();
            }
            catch (System.Exception)
            {

            }
            if (fileContent == "" || fileContent == null)
            {
                fileContent = test.ToString();//*********Code Modified By pravin On 09/May/2016***********
            }
            return fileContent;
        }
         
        //        test.Append("<head><title></title>");
        //        test.Append("<style>body, td, p{	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;	}</style></head>");
        //        test.Append("<body><P><STRONG>Dear [InterviewerName]</STRONG>,</P>");
        //        test.Append("<P>This is an invitation for conducting an Interview.</P>");
        //        test.Append("<table><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Date</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewDate]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>From Time</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Interview Mode</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewType]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Interview Location</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [Location]</td>");
        //        test.Append("</tr><tr/><tr/><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Interviewers</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NameOfInterviewers]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Candidate Name</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [CandidateName]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Candidate Contact</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [CandidateContact]</td>");
              
        //        if (txtRemarks.Text.Length > 0)
        //        {
        //            test.Append("</tr><tr>");
        //            test.Append("<td width=\"150\"><STRONG>Notes</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NOTE]</td>");
        //        }
        //        test.Append("</tr></table>");
        //        test.Append("<P>Please provide your feedback/Assessment :<STRONG> <a href=[FeedbackLink]>Click here for Feedback /Assessment form</a>. </STRONG> Thank you for your time.</P>");
        //        test.Append("<P><STRONG>[Signature]</STRONG></P>");
        //        test.Append("</body></html>");
        //    }
        //    else
        //    {
        //         //test = "<head><title></title><style>body, td, p{	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;	}</style></head><body><P><STRONG>Dear [InterviewerName]</STRONG>,</P><P>This is a rescheduled invitation for conducting an Interview</P>
        //        //<table><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>From Time:</STRONG> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td></tr><tr><td><STRONG>Interview Type:</STRONG> [InterviewType]</td></tr><tr><td>
        //        //<STRONG>Interview Location:</STRONG> [Location]</td></tr><tr><td><STRONG>Interviewers:</STRONG> [NameOfInterviewers]</td></tr><tr><td>
        //        //<STRONG>Candidate Name:</STRONG> [CandidateName]</td></tr><tr><td><STRONG>Candidate Contact:</STRONG> [CandidateContact]</td></tr></table><table><tr><td><STRONG>Notes :</STRONG></td></tr><tr><td>[NOTE]</td></tr></table><table><tr><td>Please provide your feedback/Assessment :<STRONG> <a href=[FeedbackLink]>Click here for Feedback /Assessment form</a> </STRONG> </td></tr></table><P>Thank you for your time</P><table><tr><td>Regards,</td></tr><tr><td>[RecruiterName]</td></tr></table><P><STRONG>[Signature]</STRONG></P></body></html>";
        //        test.Append("<head><title></title>");
        //        test.Append("<style>body, td, p{	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;	}</style></head>");
        //        test.Append("<body><P><STRONG>Dear [InterviewerName]</STRONG>,</P>");
        //        test.Append("<P>This is a rescheduled invitation for conducting an Interview.</P>");
        //        test.Append("<table><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Date</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewDate]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>From Time</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Interview Type</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewType]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Interview Location</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [Location]</td>");
        //        test.Append("</tr><tr/><tr/><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Interviewers</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NameOfInterviewers]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Candidate Name</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [CandidateName]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Candidate Contact</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [CandidateContact]</td>");
        //        //Interview intervw = Facade.GetInterviewById(interviewId);
        //        if (txtRemarks.Text.Length >0)
        //        {
        //            test.Append("</tr><tr>");
        //            test.Append("<td width=\"150\"><STRONG>Notes</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NOTE]</td>");
        //        }
        //        test.Append("</tr></table>");
        //        test.Append("<P>Please provide your feedback/Assessment :<STRONG> <a href=[FeedbackLink]>Click here for Feedback /Assessment form</a>. </STRONG> Thank you for your time.</P>");
        //        test.Append("<P><STRONG>[Signature]</STRONG></P>");
        //        test.Append("</body></html>");
        //    }
              
        //    //*****************END 09/May/2016***********************
        //    try
        //    {
        //        WebRequest request = WebRequest.Create(url);
        //        WebResponse response = request.GetResponse();
        //        Stream responseStream = response.GetResponseStream();
        //        StreamReader reader = new StreamReader(responseStream); 

        //        fileContent = StringHelper.Convert(reader.ReadToEnd());
        //        reader.Close();
        //        response.Close();
        //    }
        //    catch (System.Exception)
        //    {

        //    }
        //    if (fileContent == "" || fileContent == null)
        //    {
        //        fileContent = test.ToString();//*********Code Modified By pravin On 09/May/2016***********
        //    }
        //    return fileContent;
        //}

         
             
        //public static string GetFileContentFromUrlForRecruiter(string url)//commented by pravin khot on 9/May/2016
        private string GetFileContentFromUrlForRecruiter(string url)   //modify by Sumit Sonawane on 06/June/2016
        {
            string fileContent = string.Empty;
            StringBuilder test = new StringBuilder();
            //old code commented by pravin khot on 9/May/2016****
            //test = "<head><title></title><style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head><body><P><STRONG>Dear [InterviewerName],</STRONG></P><P>An Interview has been scheduled and following are the details.</P><table><tr><td><STRONG>[InterviewTitle] | [ReqCode]-[JobTitle]</STRONG></td></tr><tr><td></td><tr><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>From Time:</STRONG> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td></tr><tr><td></td><tr><tr><td><STRONG>Interview Mode:</STRONG> [InterviewType]</td></tr><tr><td><STRONG>Interview Location:</STRONG> [Location]</td></tr><tr><td></td><tr><tr><td><STRONG>Candidate Name:</STRONG> [NameOfCandidates] - [CandidateEmail]</td></tr><tr><td><STRONG>Candidate Contact:</STRONG> [ContactOfCandidates]</td></tr><tr><td></td></tr></table><table><tr><td>Regards,</td></tr><tr><td>[RecruiterName]</td></tr></table></body></html>";
            //****************END*******************
            //**************** Code Modofied By PRAVIN KHOT on 09/May/2016 ****************
            if (isNew)
            {
                using (StreamReader reader = new StreamReader(Server.MapPath("~/MailTemplate/InterviewFileContentForRecruiter.htm")))
                {
                    fileContent = reader.ReadToEnd();
                }

                fileContent = fileContent.Replace("{InterviewerName}", "[InterviewerName]");
                fileContent = fileContent.Replace("{JobTitle}", "[JobTitle]");
                fileContent = fileContent.Replace("{ReqCode}", "[ReqCode]");
                fileContent = fileContent.Replace("{InterviewDate}", "[InterviewDate]");
                fileContent = fileContent.Replace("{InterviewStartTime}", "[InterviewStartTime]");
                fileContent = fileContent.Replace("{InterviewEndTime}", "[InterviewEndTime]");
                fileContent = fileContent.Replace("{InterviewType}", "[InterviewType]");
                fileContent = fileContent.Replace("{Location}", "[Location]");
                fileContent = fileContent.Replace("{NameOfInterviewers}", "[NameOfInterviewers]");
                fileContent = fileContent.Replace("{NameOfOtherInterviewers}", "[NameOfOtherInterviewers]");
                fileContent = fileContent.Replace("{NameOfCandidates}", "[NameOfCandidates]");
                fileContent = fileContent.Replace("{ContactOfCandidates}", "[ContactOfCandidates]");

                //   fileContent = fileContent.Replace("{CandidateEmail}", "[CandidateEmail]");
                fileContent = fileContent.Replace("{NOTE}", "[NOTE]");
                fileContent = fileContent.Replace("{Signature}", "[Signature]");
            }

            else
            {
                using (StreamReader reader = new StreamReader(Server.MapPath("~/MailTemplate/InterviewFileContentForRecruiterReschedule.htm")))
                {
                    fileContent = reader.ReadToEnd();
                }

                fileContent = fileContent.Replace("{InterviewerName}", "[InterviewerName]");
                fileContent = fileContent.Replace("{JobTitle}", "[JobTitle]");
                fileContent = fileContent.Replace("{ReqCode}", "[ReqCode]");
                fileContent = fileContent.Replace("{InterviewDate}", "[InterviewDate]");
                fileContent = fileContent.Replace("{InterviewStartTime}", "[InterviewStartTime]");
                fileContent = fileContent.Replace("{InterviewEndTime}", "[InterviewEndTime]");
                fileContent = fileContent.Replace("{InterviewType}", "[InterviewType]");
                fileContent = fileContent.Replace("{Location}", "[Location]");
                fileContent = fileContent.Replace("{NameOfInterviewers}", "[NameOfInterviewers]");
                fileContent = fileContent.Replace("{NameOfOtherInterviewers}", "[NameOfOtherInterviewers]");
                fileContent = fileContent.Replace("{NameOfCandidates}", "[NameOfCandidates]");
                fileContent = fileContent.Replace("{ContactOfCandidates}", "[ContactOfCandidates]");

                //   fileContent = fileContent.Replace("{CandidateEmail}", "[CandidateEmail]");
                fileContent = fileContent.Replace("{NOTE}", "[NOTE]");
                fileContent = fileContent.Replace("{Signature}", "[Signature]");
            }
            try
            {
                // string smt = "~/" + url; M:\SP2_31May\1.7.0_SP2\TPS360.WEBUI\MailTemplate\InterviewMailTemplate.htm
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                fileContent = StringHelper.Convert(reader.ReadToEnd());
                reader.Close();

                response.Close();
            }
            catch (System.Exception)
            {

            }
            if (fileContent == "" || fileContent == null)
            {
                fileContent = test.ToString();//*********Code Modified By pravin On 09/May/2016***********
            }

            return fileContent;
        }
        //        //test = "<head><title></title><style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head><body><P><STRONG>Dear [InterviewerName],</STRONG></P><P>An Interview has been scheduled and following are the details.</P><table><tr><td><STRONG>[InterviewTitle] | [ReqCode]-[JobTitle]</STRONG></td></tr><tr><td></td><tr><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>From Time:</STRONG> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td></tr><tr><td></td><tr><tr><td><STRONG>Interview Mode:</STRONG> [InterviewType]</td></tr><tr><td><STRONG>Interview Location:</STRONG> [Location]</td></tr><tr><td></td><tr><tr><td><STRONG>Candidate Name:</STRONG> [NameOfCandidates] - [CandidateEmail]</td></tr><tr><td><STRONG>Candidate Contact:</STRONG> [ContactOfCandidates]</td></tr><tr><td></td></tr></table><table><tr><td>Regards,</td></tr><tr><td>[RecruiterName]</td></tr></table></body></html>";
        //        test.Append("<head><title></title>");
        //        test.Append("<style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head>");
        //        test.Append("<body><P><STRONG>Dear [InterviewerName],</STRONG></P>");
        //        test.Append("<P>An Interview has been scheduled and following are the details.</P>");
        //        test.Append("<table><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Requisition</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [JobTitle] | [ReqCode]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Date</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewDate]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>From Time</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Interview Mode</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewType]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Interview Location</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [Location]</td>");
        //        test.Append("</tr><tr/><tr/><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Interviewers</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NameOfInterviewers]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Candidate Name</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NameOfCandidates] - [CandidateEmail]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Candidate Contact</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [ContactOfCandidates]</td>");
        //        if (txtRemarks.Text.Length > 0)
        //        {
        //            test.Append("</tr><tr>");
        //            test.Append("<td width=\"150\"><STRONG>Notes</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NOTE]</td>");
        //        }
        //        test.Append("</tr></table>");
        //        test.Append("<br><br><STRONG>[Signature]</STRONG>");
        //        test.Append("</body></html>");
        //    }
        //    else
        //    {
        //        test.Append("<head><title></title>");
        //        test.Append("<style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head>");
        //        test.Append("<body><P><STRONG>Dear [InterviewerName],</STRONG></P>");
        //        test.Append("<P>An Interview has been rescheduled and following are the details.</P>");
        //        test.Append("<table><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Requisition</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [JobTitle] | [ReqCode]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Date</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewDate]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>From Time</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Interview Mode</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewType]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Interview Location</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [Location]</td>");
        //        test.Append("</tr><tr/><tr/><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Interviewers</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NameOfInterviewers]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Candidate Name</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NameOfCandidates] - [CandidateEmail]</td>");
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Candidate Contact</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [ContactOfCandidates]</td>");

        //        if (txtRemarks.Text.Length > 0)
        //        {
        //            test.Append("</tr><tr>");
        //            test.Append("<td width=\"150\"><STRONG>Notes</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NOTE]</td>");
        //        }
        //        test.Append("</tr></table>");
        //        test.Append("<br><br><STRONG>[Signature]</STRONG>");
        //        test.Append("</body></html>");

        //        //test = "<head><title></title><style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head><body><P><STRONG>Dear [InterviewerName],</STRONG></P><P>An Interview has been rescheduled and following are the details.</P><table><tr><td><STRONG>Requisition :</STRONG>[InterviewTitle] | [ReqCode]-[JobTitle]</td></tr><tr><td></td><tr><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>From Time:</STRONG> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td></tr><tr><td></td><tr><tr><td><STRONG>Interview Mode:</STRONG> [InterviewType]</td></tr><tr><td><STRONG>Interview Location:</STRONG> [Location]</td></tr><tr><td></td><tr><tr><td><STRONG>Candidate Name:</STRONG> [NameOfCandidates] - [CandidateEmail]</td></tr><tr><td><STRONG>Candidate Contact:</STRONG> [ContactOfCandidates]</td></tr><tr><td></td></tr></table><table><tr><td>Regards,</td></tr><tr><td>[RecruiterName]</td></tr></table></body></html>";
        //    }
        //    //********** Code END*****************
        //    try
        //    {
        //        WebRequest request = WebRequest.Create(url);
        //        WebResponse response = request.GetResponse();
        //        Stream responseStream = response.GetResponseStream();
        //        StreamReader reader = new StreamReader(responseStream);

        //        fileContent = StringHelper.Convert(reader.ReadToEnd());
        //        reader.Close();
        //        response.Close();
        //    }
        //    catch (System.Exception)
        //    {

        //    }
        //    if (fileContent == "" || fileContent == null)
        //    {
        //        fileContent = test.ToString();// Line Modified By pravin  On 09/May/2016
        //    }
        //    return fileContent;
        //}



        //    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     
        private void SentInterviewNotificationToRecruiter(int interviewId)
        {
            char[] delim = { ',' };
            string[] candidateIds = CandidateId.Split(delim, StringSplitOptions.RemoveEmptyEntries);
            foreach (string cid in candidateIds)
            {


                if (interviewId != 0)
                {
                    string TemplateUrl = AppSettings.InterviewMailTemplateUrl;
                    string mailBody = "";
                    //if (isMainApplication)
                    //    mailBody = GetFileContentFromUrl(TemplateUrl);
                    //else
                    //    mailBody = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(cid), interviewId, ddlInterviewType.SelectedItem.Text, 0);


                    switch (ApplicationSource)
                    {
                        case ApplicationSource.LandTApplication:
                            mailBody = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(cid), interviewId, ddlInterviewType.SelectedItem.Text, 0);
                            break;
                        case ApplicationSource.MainApplication:
                            mailBody = GetFileContentFromUrl(TemplateUrl);
                            break;
                        default:
                            mailBody = GetFileContentFromUrl(TemplateUrl);
                            break;
                    }

                    StringBuilder stringBuilder = new StringBuilder();
                    string mailFrom = string.Empty;
                    string mailTo = string.Empty;
                    string Sign = string.Empty;
                    stringBuilder.Append(mailBody);
                    string InterviewsName = Facade.GetInterviersNameByInterviewId(interviewId);
                    IList<Member> member = new List<Member>();
                    member = Facade.GetAllMemberDetailsByInterviewId(interviewId, Convert.ToInt32(cid));
                    MemberSignature signature = new MemberSignature();
                    signature = Facade.GetActiveMemberSignatureByMemberId(CurrentMember.Id);
                    if (signature != null)
                        Sign = signature.Signature;
                    else
                        Sign = CurrentMember.FirstName + "  " + CurrentMember.LastName;
                    mailFrom = CurrentMember.PrimaryEmail;
                    if (chkAllDayEvent.Checked)
                    {
                        stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString() + " All Day");
                        stringBuilder.Replace("[InterviewStartTime]", "");
                        stringBuilder.Replace("[InterviewEndTime]", "");
                        //stringBuilder.Replace("<tr><td><STRONG>Start Time:</STRONG> [InterviewStartTime]</td></tr><tr><td><STRONG>End Time:</STRONG> [InterviewEndTime]</td></tr>", "");//LINE COMMENTED BY PRAVIN KHOT ON 9/May/2016
                    }
                    else
                    {
                        stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString());
                        stringBuilder.Replace("[InterviewStartTime]", ddlStartTime.SelectedItem.Text.ToString());
                        string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                        if (TimeZoneLocation != "Select Timezone")
                        {
                            string TimeWithTimeZone = ddlEndTime.SelectedItem.Text.ToString() + " [ TimeZone - " + ddlTimezone.SelectedItem.Text.ToString() + " ]";
                            stringBuilder.Replace("[InterviewEndTime]", TimeWithTimeZone.ToString());
                        }
                        else
                        {
                            stringBuilder.Replace("[InterviewEndTime]", ddlEndTime.SelectedItem.Text.ToString());
                        }
                       
                    }
                    stringBuilder.Replace("[Location]", txtLocation.Text.ToString());
                    //stringBuilder.Replace("[NameOfInterviewers]", (InterviewsName != string.Empty ? InterviewsName.Trim() : string.Empty));
                    ///////////////////////////Sumit Sonawane 21/06/2016/////////////////////////////////////////////////////////////////////////////

                    StringBuilder NameOfInterviewersAll = new StringBuilder();
                    foreach (ListItem item in chkClientInterviers.Items)
                    {
                        if (item.Selected)
                        {
                            NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                        }
                        // else { NameOfInterviewersAll = null; }

                    }
                    //foreach (ListItem item in chkInternalInterviewer.Items)
                    //{
                    //    if (item.Selected)
                    //    {
                    //        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                    //    }
                    //    //  else { NameOfInterviewersAll = null; }

                    //}
                    foreach (ListItem item in chkSuggestedInterviewer.Items)
                    {
                        if (item.Selected)
                        {
                            NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                        }
                        //  else { NameOfInterviewersAll = null; }

                    }
                    //newInterview.OtherInterviewers = MiscUtil.RemoveScript(hfOtherInterviewers.Value.Trim().TrimStart(',').TrimEnd(','));
                    //NameOfInterviewersAll = NameOfInterviewersAll.Append("Other  Interviewers : ");
                    string str = hfOtherInterviewers.Value.ToString();
                    string Substr = string.Empty;
                    if (str.StartsWith(","))
                    {
                        Substr = str.Substring(1, str.Length - 1);
                    }
                    else { Substr = hfOtherInterviewers.Value.ToString(); }
                    //NameOfInterviewersAll = NameOfInterviewersAll.Append(Substr);
                    Substr = Substr.Replace(",", "<br/>" + "&nbsp;&nbsp;");
                    stringBuilder.Replace("[NameOfOtherInterviewers]", Substr.ToString());
                    stringBuilder.Replace("[NameOfInterviewers]", NameOfInterviewersAll.ToString());

                    /////////////////////////////////////////////////////////////////////////////////////////
                       
                    stringBuilder.Replace("[Signature]", Sign);
                    if (chkSendEmailAlerttoInterviewers.Checked)
                    {

                    }
                    foreach (Member mem in member)
                    {
                        string membr = mem.FirstName + " " + mem.LastName;
                        stringBuilder.Replace("[InterviewerName]", mem.FirstName + " " + mem.LastName);
                        mailTo = mem.PrimaryEmail;
                        SendInterviewEmail(mailFrom, mailTo, stringBuilder.ToString(), membr);
                        stringBuilder.Replace(mem.FirstName + " " + mem.LastName, "[InterviewerName]");
                    }
                }
            }
        }

        private void SendInterviewNotificationToInterviewers(int interviewId)
        {
            string mailBody = "";
            string mailbody1 = "";
            string OtherinterviewersMailbody = ""; //Code introduced by Prasanth on 21/Dec/2015
            char[] delim = { ',' };
            string[] candidateIds = CandidateId.Split(delim, StringSplitOptions.RemoveEmptyEntries);

            if (isMainApplication)
                mailBody = getInterviewScheduleBodyForInterviewers(interviewId);
            Interview inter = Facade.GetInterviewById(interviewId);
            if (interviewId != 0)
            {
                foreach (string cid in candidateIds)
                {
                    if (inter.MemberId == Convert.ToInt32(cid))
                    {
                        if (!isMainApplication)
                        {
                            mailBody = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(cid), interviewId, ddlInterviewType.SelectedItem.Text, 0);
                        }

                        string bodystring = txtEmailInterviewerTemplate.Text;
                        if (bodystring != "")
                        {
                            mailBody = bodystring;
                        }
                        Member candid = Facade.GetMemberById(Convert.ToInt32(cid));
                        mailBody = mailBody.Replace("[NameOfCandidates]", candid.FirstName + " " + candid.LastName);
                        mailBody = mailBody.Replace("[ContactOfCandidates]", candid.CellPhone);
                        foreach (ListItem item in chkInternalInterviewer.Items)
                        {
                            if (item.Selected)
                            {
                               
                                string[] name = item.Text.Split('[');
                                //for recruiter

                                mailbody1 = getInterviewScheduleBodyForRecruiter(interviewId);
                                //SendInterviewEmail(CurrentMember.PrimaryEmail, Facade.GetMemberById(Convert.ToInt32(item.Value)).PrimaryEmail, mailbody1.Replace("[InterviewerName]", name[0].Trim()), name[0].Trim());
                                SendInterviewEmailrecruiter(CurrentMember.PrimaryEmail, Facade.GetMemberById(Convert.ToInt32(item.Value)).PrimaryEmail, mailbody1.Replace("[InterviewerName]", name[0].Trim()), name[0].Trim(), cid);
                                
                            }
                        }
                        //--------------code added by pravin khot on 17/Dec/2015--------------------
                        foreach (ListItem item in chkSuggestedInterviewer.Items)
                        {
                            if (item.Selected)
                            {
                                string[] name = item.Text.Split('[');
                                string[] email = item.Text.TrimEnd(']', ' ').Split('[');

                                //**************** code comment by pravin khot on 4/Jan/2016*********************
                                ////SendInterviewEmail(CurrentMember.PrimaryEmail, Facade.GetMemberById(Convert.ToInt32(item.Value)).PrimaryEmail, mailBody.Replace("[InterviewerName]", name[0].Trim()));
                                //SendInterviewEmailinterviwer(CurrentMember.PrimaryEmail, email[1].ToString(), mailBody.Replace("[InterviewerName]", name[0].Trim()), name[0].Trim(), cid);
                                //******************************End***************************************************

                                SendOtherInterviewEmailinterviwer(interviewId, email[1].ToString(), name[0].Trim(), cid);  //Write call Function by pravin khot on 4/Jan/2016
                             }
                        }
                        //-------------------end---------------------------------------
                        foreach (ListItem item in chkClientInterviers.Items)
                        {
                            if (item.Selected)
                            {
                               
                                string[] name = item.Text.Split('[');
                                string[] email = item.Text.TrimEnd(']', ' ').Split('[');
                                //**************** code comment by pravin khot on 4/Jan/2016*********************
                                ////SendInterviewEmail(CurrentMember.PrimaryEmail, Facade.GetMemberById(Convert.ToInt32(item.Value)).PrimaryEmail, mailBody.Replace("[InterviewerName]", name[0].Trim()));
                                //SendInterviewEmailinterviwer(CurrentMember.PrimaryEmail, email[1].ToString(), mailBody.Replace("[InterviewerName]", name[0].Trim()), name[0].Trim(),cid);
                                //******************************End***************************************************

                                SendOtherInterviewEmailinterviwer(interviewId, email[1].ToString(), name[0].Trim(), cid);  //Write call Function by pravin khot on 4/Jan/2016
                            }
                        }

                        string[] otherinterviewers = hfOtherInterviewers.Value.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string s in otherinterviewers)
                        {
                            //************code add by pravin khot 4/Jan/2016 for chk email are empty or not
                            int lens = 0;
                            lens = s.Length;

                            if (lens > 1)
                            {
                                SendOtherInterviewEmailinterviwer(interviewId, s,s, cid);  //Write call Function by pravin khot on 4/Jan/2016
                            }
                           //************************************End**********************************************
                        }
                        Appuid = System.Guid.NewGuid();

                    }
                }
            }
        }
        //--------------code added by pravin khot on 4/Jan/2016  for sending the email--------------------
        private void SendOtherInterviewEmailinterviwer(int interviewId, string InterviewerEmailId, string InterviewerName, string candidateId)
        {
            string OtherinterviewersMailbody = "";//////////////////////////////////////////////////Sumit///////////////////////
            Member candid = Facade.GetMemberById(Convert.ToInt32(candidateId));
            //SendInterviewEmail(CurrentMember.PrimaryEmail, s, mailBody.Replace("[InterviewerName]", s));
            /*Line introduced by Prasanth on 16/Oct/2015 start*/
            //Line commented by Prasanth on 22/Dec/2015
            //SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Interview/InterviewerFeedback.aspx", string.Empty, UrlConstants.PARAM_INTERVIEWID, interviewId + "~" + s);
            //Code introduced by Prasanth on 22/Dec/2015 Start
            string JobTitlle = "";
            if (ddlJobPosting.SelectedIndex > 0)
            {
                JobTitlle = ddlJobPosting.SelectedItem.ToString();
            }
            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Interview/InterviewerFeedback.aspx", string.Empty, UrlConstants.PARAM_INTERVIEWID, interviewId + "~" + InterviewerEmailId + "~" + candid.FirstName + " " + candid.LastName + "~" + JobTitlle + "~" + wdcStartDate.Text + " " + ddlStartTime.Text + "~" + CandidateId + "~" + CurrentMember.PrimaryEmail);
            //******************END********************
            //Code introduced by Prasanth on 21/Dec/2015 Start

            if (!isMainApplication)
            {
                OtherinterviewersMailbody = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(candidateId), interviewId, ddlInterviewType.SelectedItem.Text, 0);
            }
            else
            {
                OtherinterviewersMailbody = getInterviewScheduleBodyForInterviewers(interviewId);
            }
            
            OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[CandidateName]", candid.FirstName + " " + candid.LastName);
            OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[CandidateContact]", candid.CellPhone);
            
            //Code commented by Prasanth on 22/Dec/2015 Start
            //if (DdlInterviewQuestionBankType.SelectedIndex == 0)
            //{
            //    OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[FeedbackLink]", "");
            //}
            //else
            //{
            //    OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[FeedbackLink]", url.ToString());
            //}
            //*****************END***********************
            /**************END*********************/

            //Line introduced by Prasanth on 22/Dec/2015
            OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[FeedbackLink]", url.ToString());
            //OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[InterviewerName]", InterviewerName.ToString());/////////////Sumit Sonawane
            /////////Done//////////////////Sumit Sonawane 21/06/2016/////////////////////////////////////////////////////////////////////////////

            StringBuilder NameOfInterviewersAll = new StringBuilder();
            foreach (ListItem item in chkClientInterviers.Items)
            {
                if (item.Selected)
                {
                    NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                }
                // else { NameOfInterviewersAll = null; }

            }
            //foreach (ListItem item in chkInternalInterviewer.Items)
            //{
            //    if (item.Selected)
            //    {
            //        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
            //    }
            //    //  else { NameOfInterviewersAll = null; }

            //}
            foreach (ListItem item in chkSuggestedInterviewer.Items)
            {
                if (item.Selected)
                {
                    NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                }
                //  else { NameOfInterviewersAll = null; }

            }
            //stringBuilder.Replace("[NameOfInterviewers]", NameOfInterviewersAll.ToString());
            StringBuilder stringBuilder = new StringBuilder();
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //NameOfInterviewersAll = NameOfInterviewersAll.Append(txtOtherInterviewers.Text);
            //NameOfInterviewersAll = NameOfInterviewersAll.Append("Other  Interviewers : ");
            string str = hfOtherInterviewers.Value.ToString();
            string Substr = string.Empty;
            if (str.StartsWith(","))
            {
                Substr = str.Substring(1, str.Length - 1);
            }
            else { Substr = hfOtherInterviewers.Value.ToString(); }
            //NameOfInterviewersAll = NameOfInterviewersAll.Append(Substr);
            Substr = Substr.Replace(",", "<br/>" + "&nbsp;&nbsp;");
            stringBuilder.Replace("[NameOfOtherInterviewers]", Substr.ToString());
            OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[InterviewerName]", InterviewerName.ToString());/////////////Sumit Sonawane

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


            //line modified by Prasanth on 21/Dec/2015
            SendInterviewEmailinterviwer(CurrentMember.PrimaryEmail, InterviewerEmailId, OtherinterviewersMailbody.Replace("[NameOfInterviewers]", InterviewerName), InterviewerEmailId, candidateId);

            /*Line introduced by Prasanth on 16/Oct/2015 */
            InsertInterviewResponse(interviewId, InterviewerEmailId, Convert.ToInt32(DdlInterviewQuestionBankType.SelectedValue));
        }
        //***************************************End*********************************************************




        //Code introduced by Prasanth on 16/Oct/2015 Start
        private void InsertInterviewResponse(int interviewId, string InterviewerEmail, int AssessmentId)
        {
            InterviewResponse newInterviewresponse = new InterviewResponse();
            newInterviewresponse.InterviewId = interviewId;
            newInterviewresponse.InterviewerEmail = InterviewerEmail;
            newInterviewresponse.QuestionBank_id = AssessmentId;
            Facade.InterviewResponse_Add(newInterviewresponse);
        }

        //******************END*************************


        private void SendInterviewNotificationToCandidate(int interviewId)
        {
            string body = "";
            if (isMainApplication)
                body = getInterviewScheduleBody(interviewId);
            char[] delim = { ',' };
            string[] candidateIds = CandidateId.Split(delim, StringSplitOptions.RemoveEmptyEntries);
            foreach (string cid in candidateIds)
            {
                Interview inter = Facade.GetInterviewById(interviewId);
                if (inter.MemberId == Convert.ToInt32(cid))
                {
                    if (interviewId != 0)
                    {
                        if (!isMainApplication)
                        {
                            body = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(cid), interviewId, ddlInterviewType.SelectedItem.Text, 0);
                        }
                        string bodystring = txtEmailCandidateTemplate.Text;
                        if (bodystring != "")
                        {
                            body = bodystring;
                        }
                        Member member = Facade.GetMemberById(Convert.ToInt32(cid));
                        //SendInterviewEmail(CurrentMember.PrimaryEmail, member.PrimaryEmail, body.Replace("[InterviewerName]", member.FirstName + " " + member.LastName));
                        SendInterviewEmail(CurrentMember.PrimaryEmail, member.PrimaryEmail, body.Replace("[InterviewerName]", member.FirstName + " " + member.LastName), member.FirstName + " " + member.LastName);
                    }
                }
            }
        }
        private string getappoinmentbody(int interviewId)
        {
            if (interviewId != 0)
            {
                //string InterviewsName = Facade.GetInterviersNameByInterviewId(interviewId);
                string InterviewsName = string.Empty;
                string[] InterviewsNames;
                foreach (ListItem item in chkClientInterviers.Items)
                {
                    if (item.Selected)
                    {
                        InterviewsNames = item.Text.Split('[');
                        //InterviewsName = InterviewsName + ",Mr/Ms " + InterviewsNames[0].ToString();
                        InterviewsName = InterviewsName + "," + InterviewsNames[0].ToString();
                    }

                }
                InterviewsName = InterviewsName.Trim().TrimStart(',');
                string TemplateUrl = AppSettings.InterviewMailTemplateUrl;
                string mailBody = GetAppFileContentFromUrl(TemplateUrl);
                StringBuilder stringBuilder = new StringBuilder();
                string Sign = string.Empty;
                stringBuilder.Append(mailBody);
                MemberSignature signature = new MemberSignature();
                signature = Facade.GetActiveMemberSignatureByMemberId(CurrentMember.Id);
                if (signature != null)
                    Sign = signature.Signature;
                else
                    Sign = CurrentMember.FirstName + "  " + CurrentMember.LastName;
                if (chkAllDayEvent.Checked)
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString() + " All Day");
                    stringBuilder.Replace("[InterviewStartTime]", "");
                    stringBuilder.Replace("[InterviewEndTime]", "");
                }
                else
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString());
                    stringBuilder.Replace("[InterviewStartTime]", ddlStartTime.SelectedItem.Text.ToString());
                    string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                    if (TimeZoneLocation != "Select Timezone")
                    {
                        string TimeWithTimeZone = ddlEndTime.SelectedItem.Text.ToString() + " [ TimeZone - " + ddlTimezone.SelectedItem.Text.ToString() + " ]";
                        stringBuilder.Replace("[InterviewEndTime]", TimeWithTimeZone.ToString());
                    }
                    else
                    {
                        stringBuilder.Replace("[InterviewEndTime]", ddlEndTime.SelectedItem.Text.ToString());
                    }

                }
                stringBuilder.Replace("[Location]", txtLocation.Text.ToString());
                //stringBuilder.Replace("[NameOfInterviewers]", (InterviewsName != string.Empty ? InterviewsName.Trim() : string.Empty));
                // stringBuilder.Replace("[Signature]", Sign);

                /////////Done//////////////////Sumit Sonawane 21/06/2016/////////////////////////////////////////////////////////////////////////////

                StringBuilder NameOfInterviewersAll = new StringBuilder();
                foreach (ListItem item in chkClientInterviers.Items)
                {
                    if (item.Selected)
                    {
                        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                    }
                    // else { NameOfInterviewersAll = null; }

                }
                //foreach (ListItem item in chkInternalInterviewer.Items)
                //{
                //    if (item.Selected)
                //    {
                //        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                //    }
                //    //  else { NameOfInterviewersAll = null; }

                //}
                foreach (ListItem item in chkSuggestedInterviewer.Items)
                {
                    if (item.Selected)
                    {
                        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                    }
                    //  else { NameOfInterviewersAll = null; }

                }
                //NameOfInterviewersAll = NameOfInterviewersAll.Append(txtOtherInterviewers.Text);
                //NameOfInterviewersAll = NameOfInterviewersAll.Append("Other  Interviewers : ");
                string str = hfOtherInterviewers.Value.ToString();
                string Substr = string.Empty;
                if (str.StartsWith(","))
                {
                    Substr = str.Substring(1, str.Length - 1);
                }
                else { Substr = hfOtherInterviewers.Value.ToString(); }
                //NameOfInterviewersAll = NameOfInterviewersAll.Append(Substr);
                Substr = Substr.Replace(",", "<br/>" + "&nbsp;&nbsp;");
                stringBuilder.Replace("[NameOfOtherInterviewers]", Substr.ToString());
                stringBuilder.Replace("[NameOfInterviewers]", NameOfInterviewersAll.ToString());

                /////////////////////////////////////////////////////////////////////////////////////////
         
         
                return stringBuilder.ToString();
            }

            return "";
        }


        private string getappoinmentbodyrecruiter(int interviewId,string CID)
        {
            if (interviewId != 0)
            {
                //string InterviewsName = Facade.GetInterviersNameByInterviewId(interviewId);
                string otherinterviewername = string.Empty;
                string InterviewsName = string.Empty;
                string[] InterviewsNames;
                foreach (ListItem item in chkClientInterviers.Items)
                {
                    if (item.Selected)
                    {
                        InterviewsNames = item.Text.Split('[');
                        InterviewsName = InterviewsName + "," + InterviewsNames[0].ToString();
                    }

                }
                InterviewsName = InterviewsName.Trim().TrimStart(',');

                Interview otherinterviewer = Facade.GetInterviewById(interviewId);
                otherinterviewername = otherinterviewer.OtherInterviewers.ToString();


                string TemplateUrl = AppSettings.InterviewMailTemplateUrl;
                string mailBody = GetAppFileContentFromUrlrecruiter(TemplateUrl);
                StringBuilder stringBuilder = new StringBuilder();
                string Sign = string.Empty;
                stringBuilder.Append(mailBody);
                MemberSignature signature = new MemberSignature();
                signature = Facade.GetActiveMemberSignatureByMemberId(CurrentMember.Id);
                if (signature != null)
                    Sign = signature.Signature;
                else
                    Sign = CurrentMember.FirstName + "  " + CurrentMember.LastName;
                if (chkAllDayEvent.Checked)
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString() + " All Day");
                    stringBuilder.Replace("[InterviewStartTime]", "");
                    stringBuilder.Replace("[InterviewEndTime]", "");
                }
                else
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString());
                    stringBuilder.Replace("[InterviewStartTime]", ddlStartTime.SelectedItem.Text.ToString());
                    string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                    if (TimeZoneLocation != "Select Timezone")
                    {
                        string TimeWithTimeZone = ddlEndTime.SelectedItem.Text.ToString() + " [ TimeZone - " + ddlTimezone.SelectedItem.Text.ToString() + " ]";
                        stringBuilder.Replace("[InterviewEndTime]", TimeWithTimeZone.ToString());
                    }
                    else
                    {
                        stringBuilder.Replace("[InterviewEndTime]", ddlEndTime.SelectedItem.Text.ToString());
                    }

                }


                Interview iDetail = Facade.GetInterviewById(interviewId);
                int canid = iDetail.MemberId;
                Member Cannameandaddress = Facade.GetMemberById(Convert.ToInt32(CID));

                if (ddlInterviewType.SelectedIndex > 0)
                {
                    stringBuilder.Replace("[InterviewType]", ddlInterviewType.SelectedItem.Text);
                }
                else
                {
                    stringBuilder.Replace("[InterviewType]", "");
                }

                stringBuilder.Replace("[Location]", txtLocation.Text.ToString());
                stringBuilder.Replace("[NameOfInterviewers]", (InterviewsName != string.Empty ? InterviewsName.Trim() : string.Empty));
                //stringBuilder.Replace("[NameOfOtherInterviewers]", (otherinterviewername != string.Empty ? otherinterviewername.Trim() : string.Empty));
                /////////Done//////////////////Sumit Sonawane 21/06/2016/////////////////////////////////////////////////////////////////////////////

                StringBuilder NameOfInterviewersAll = new StringBuilder();
                foreach (ListItem item in chkClientInterviers.Items)
                {
                    if (item.Selected)
                    {
                        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                    }
                    // else { NameOfInterviewersAll = null; }
                }
                //foreach (ListItem item in chkInternalInterviewer.Items)
                //{
                //    if (item.Selected)
                //    {
                //        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                //    }
                //    //  else { NameOfInterviewersAll = null; }

                //}
                foreach (ListItem item in chkSuggestedInterviewer.Items)
                {
                    if (item.Selected)
                    {
                        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                    }
                    //  else { NameOfInterviewersAll = null; }

                }
                //NameOfInterviewersAll = NameOfInterviewersAll.Append(txtOtherInterviewers.Text);
                //NameOfInterviewersAll = NameOfInterviewersAll.Append("Other  Interviewers : ");
                string str = hfOtherInterviewers.Value.ToString();
                string Substr = string.Empty;
                if (str.StartsWith(","))
                {
                    Substr = str.Substring(1, str.Length - 1);
                }
                else { Substr = hfOtherInterviewers.Value.ToString(); }
                //NameOfInterviewersAll = NameOfInterviewersAll.Append(Substr);
                Substr = Substr.Replace(",", "<br/>" + "&nbsp;&nbsp;");
                stringBuilder.Replace("[NameOfOtherInterviewers]", Substr.ToString());
                stringBuilder.Replace("[NameOfInterviewers]", NameOfInterviewersAll.ToString());

                /////////////////////////////////////////////////////////////////////////////////////////
         
         
                stringBuilder.Replace("[NameOfCandidates]", Cannameandaddress.FirstName + " " + Cannameandaddress.LastName);
                stringBuilder.Replace("[ContactOfCandidates]", Cannameandaddress.CellPhone);
                stringBuilder.Replace("[CandidateEmail]", Cannameandaddress.PrimaryEmail);


                // stringBuilder.Replace("[Signature]", Sign);
                return stringBuilder.ToString();
            }

            return "";
        }


        private string getappoinmentbodyinterviewer(int interviewId,string CID)
        {
            if (interviewId != 0)
            {
                //string InterviewsName = Facade.GetInterviersNameByInterviewId(interviewId);
                string otherinterviewername = string.Empty;
                string InterviewsName = string.Empty;
                string[] InterviewsNames;
                foreach (ListItem item in chkClientInterviers.Items)
                {
                    if (item.Selected)
                    {
                        InterviewsNames = item.Text.Split('[');
                        //InterviewsName = InterviewsName + ",Mr/Ms " + InterviewsNames[0].ToString();
                        InterviewsName = InterviewsName + "," + InterviewsNames[0].ToString();
                    }

                }
                InterviewsName = InterviewsName.Trim().TrimStart(',');

                Interview otherinterviewer = Facade.GetInterviewById(interviewId);
                otherinterviewername = otherinterviewer.OtherInterviewers.ToString();


                string TemplateUrl = AppSettings.InterviewMailTemplateUrl;
                string mailBody = GetAppFileContentFromUrlinterviewer(TemplateUrl);
                StringBuilder stringBuilder = new StringBuilder();
                string Sign = string.Empty;
                stringBuilder.Append(mailBody);
                MemberSignature signature = new MemberSignature();
                signature = Facade.GetActiveMemberSignatureByMemberId(CurrentMember.Id);
                if (signature != null)
                    Sign = signature.Signature;
                else
                    Sign = CurrentMember.FirstName + "  " + CurrentMember.LastName;
                if (chkAllDayEvent.Checked)
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString() + " All Day");
                    stringBuilder.Replace("[InterviewStartTime]", "");
                    stringBuilder.Replace("[InterviewEndTime]", "");

                }
                else
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString());
                    stringBuilder.Replace("[InterviewStartTime]", ddlStartTime.SelectedItem.Text.ToString());
                    string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                    if (TimeZoneLocation != "Select Timezone")
                    {
                        string TimeWithTimeZone = ddlEndTime.SelectedItem.Text.ToString() + " [ TimeZone - " + ddlTimezone.SelectedItem.Text.ToString() + " ]";
                        stringBuilder.Replace("[InterviewEndTime]", TimeWithTimeZone.ToString());
                    }
                    else
                    {
                        stringBuilder.Replace("[InterviewEndTime]", ddlEndTime.SelectedItem.Text.ToString());
                    }

                }


                Interview iDetail = Facade.GetInterviewById(interviewId);
                int canid = iDetail.MemberId;
                Member Cannameandaddress = Facade.GetMemberById(Convert.ToInt32(CID));

                if (ddlInterviewType.SelectedIndex > 0)
                {
                    stringBuilder.Replace("[InterviewType]", ddlInterviewType.SelectedItem.Text);
                }
                else
                {
                    stringBuilder.Replace("[InterviewType]", "");
                }

                stringBuilder.Replace("[Location]", txtLocation.Text.ToString());
                stringBuilder.Replace("[NameOfInterviewers]", (InterviewsName != string.Empty ? InterviewsName.Trim() : string.Empty));
                //stringBuilder.Replace("[NameOfOtherInterviewers]", (otherinterviewername != string.Empty ? otherinterviewername.Trim() : string.Empty));
                /////////Done//////////////////Sumit Sonawane 21/06/2016/////////////////////////////////////////////////////////////////////////////

                StringBuilder NameOfInterviewersAll = new StringBuilder();
                foreach (ListItem item in chkClientInterviers.Items)
                {
                    if (item.Selected)
                    {
                        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                    }
                    // else { NameOfInterviewersAll = null; }

                }
                //foreach (ListItem item in chkInternalInterviewer.Items)
                //{
                //    if (item.Selected)
                //    {
                //        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                //    }
                //    //  else { NameOfInterviewersAll = null; }

                //}
                foreach (ListItem item in chkSuggestedInterviewer.Items)
                {
                    if (item.Selected)
                    {
                        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                    }
                    //  else { NameOfInterviewersAll = null; }

                }
                //NameOfInterviewersAll = NameOfInterviewersAll.Append(txtOtherInterviewers.Text);
                //NameOfInterviewersAll = NameOfInterviewersAll.Append("Other  Interviewers : ");
                string str = hfOtherInterviewers.Value.ToString();
                string Substr = string.Empty;
                if (str.StartsWith(","))
                {
                    Substr = str.Substring(1, str.Length - 1);
                }
                else { Substr = hfOtherInterviewers.Value.ToString(); }
                //NameOfInterviewersAll = NameOfInterviewersAll.Append(Substr);
                Substr = Substr.Replace(",", "<br/>" + "&nbsp;&nbsp;");
                stringBuilder.Replace("[NameOfOtherInterviewers]", Substr.ToString());
                stringBuilder.Replace("[NameOfInterviewers]", NameOfInterviewersAll.ToString());

                /////////////////////////////////////////////////////////////////////////////////////////
                stringBuilder.Replace("[NameOfCandidates]", Cannameandaddress.FirstName + " " + Cannameandaddress.LastName);
                stringBuilder.Replace("[ContactOfCandidates]", Cannameandaddress.CellPhone);
                stringBuilder.Replace("[CandidateEmail]", Cannameandaddress.PrimaryEmail);


                // stringBuilder.Replace("[Signature]", Sign);
                return stringBuilder.ToString();
            }

            return "";
        }
        private string getInterviewScheduleBody(int interviewId)
        {
            if (interviewId != 0)
            {
                //string InterviewsName = Facade.GetInterviersNameByInterviewId(interviewId);
                string InterviewsName = string.Empty;
                string[] InterviewsNames;
                foreach (ListItem item in chkClientInterviers.Items)
                {
                    if (item.Selected)
                    {
                        InterviewsNames = item.Text.Split('[');
                        //InterviewsName = InterviewsName + ",Mr/Ms " + InterviewsNames[0].ToString();
                        InterviewsName = InterviewsName + "," + InterviewsNames[0].ToString();
                    }

                }
                InterviewsName = InterviewsName.Trim().TrimStart(',');
                string TemplateUrl = AppSettings.InterviewMailTemplateUrl;
                string mailBody = GetFileContentFromUrl(TemplateUrl);
                StringBuilder stringBuilder = new StringBuilder();
                string Sign = string.Empty;
                stringBuilder.Append(mailBody);
                MemberSignature signature = new MemberSignature();
                signature = Facade.GetActiveMemberSignatureByMemberId(CurrentMember.Id);
                if (signature != null)
                    Sign = signature.Signature;
                else
                    Sign = CurrentMember.FirstName + "  " + CurrentMember.LastName;

                    Hashtable siteSettingTable = null;

                    TPS360.BusinessFacade.Facade f = new TPS360.BusinessFacade.Facade();
                    MemberExtendedInformation MailSetting = f.GetMemberExtendedInformationByMemberId(CurrentMember.Id);

                    TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
                    SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                    siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                    stringBuilder.Replace("[STANDARDAPPLICATIONSETTINGSCOMPANYNAME]", siteSettingTable[DefaultSiteSetting.CompanyName.ToString()].ToString());		              
				
                if (chkAllDayEvent.Checked)
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString() + " All Day");
                    stringBuilder.Replace("[InterviewStartTime]", "");
                    stringBuilder.Replace("[InterviewEndTime]", "");
                }
                else
                {
                    stringBuilder.Replace("[InterviewStartTime]", ddlStartTime.SelectedItem.Text.ToString());
                    string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                    if (TimeZoneLocation != "Select Timezone")
                    {
                        string TimeWithTimeZone = ddlEndTime.SelectedItem.Text.ToString() + " [ TimeZone - " + ddlTimezone.SelectedItem.Text.ToString() + " ]";
                        stringBuilder.Replace("[InterviewEndTime]", TimeWithTimeZone.ToString());
                    }
                    else
                    {
                        stringBuilder.Replace("[InterviewEndTime]", ddlEndTime.SelectedItem.Text.ToString());
                    }

                }
                stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString());
                stringBuilder.Replace("[Location]", txtLocation.Text.ToString());
                if (ddlClientInterviewer.SelectedIndex > 0)
                {
                    stringBuilder.Replace("[CompanyName]", ddlClientInterviewer.SelectedItem.ToString());
                }
                else
                {
                    stringBuilder.Replace("[CompanyName]", "   ");
                }
                if (ddlInterviewType.SelectedIndex > 0)
                {
                    stringBuilder.Replace("[InterviewType]", ddlInterviewType.SelectedItem.Text);
                }
                else
                {
                    stringBuilder.Replace("[InterviewType]", "");
                }
                stringBuilder.Replace("[ContactMember]", CurrentMember.FirstName + "  " + CurrentMember.LastName);
                stringBuilder.Replace("[ContactMemberMailID]",CurrentMember.PrimaryEmail);
                //stringBuilder.Replace("[NameOfInterviewers]", (InterviewsName != string.Empty ? InterviewsName.Trim() : string.Empty));
                /////////Done//////////////////Sumit Sonawane 21/06/2016/////////////////////////////////////////////////////////////////////////////

                StringBuilder NameOfInterviewersAll = new StringBuilder();
                foreach (ListItem item in chkClientInterviers.Items)
                {
                    if (item.Selected)
                    {
                        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                    }
                    // else { NameOfInterviewersAll = null; }

                }
                //foreach (ListItem item in chkInternalInterviewer.Items)
                //{
                //    if (item.Selected)
                //    {
                //        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                //    }
                //    //  else { NameOfInterviewersAll = null; }

                //}
                foreach (ListItem item in chkSuggestedInterviewer.Items)
                {
                    if (item.Selected)
                    {
                        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                    }
                    //  else { NameOfInterviewersAll = null; }

                }
                //NameOfInterviewersAll = NameOfInterviewersAll.Append(txtOtherInterviewers.Text);
                //NameOfInterviewersAll = NameOfInterviewersAll.Append("Other  Interviewers : ");
                string str = hfOtherInterviewers.Value.ToString();
                string Substr = string.Empty;
                if (str.StartsWith(","))
                {
                    Substr = str.Substring(1, str.Length - 1);
                }
                else { Substr = hfOtherInterviewers.Value.ToString(); }
                //NameOfInterviewersAll = NameOfInterviewersAll.Append(Substr);
                Substr = Substr.Replace(",", "<br/>" + "&nbsp;&nbsp;");
                stringBuilder.Replace("[NameOfOtherInterviewers]", Substr.ToString());
                stringBuilder.Replace("[NameOfInterviewers]", NameOfInterviewersAll.ToString());

                /////////////////////////////////////////////////////////////////////////////////////////
         
                
                stringBuilder.Replace("[Signature]", Sign);
                return stringBuilder.ToString();
            }

            return "";

        }

        private string getInterviewScheduleBodyForInterviewers(int interviewId)
        {
            if (interviewId != 0)
            {
                string InterviewsName = Facade.GetInterviersNameByInterviewId(interviewId);
                string TemplateUrl = AppSettings.InterviewMailTemplateUrl;
                string mailBody = GetFileContentFromUrlForInterviewers(TemplateUrl);
                StringBuilder stringBuilder = new StringBuilder();
                string Sign = string.Empty;
                stringBuilder.Append(mailBody);
                MemberSignature signature = new MemberSignature();
                signature = Facade.GetActiveMemberSignatureByMemberId(CurrentMember.Id);
                if (signature != null)
                    Sign = signature.Signature;
                else
                    Sign = CurrentMember.FirstName + "  " + CurrentMember.LastName;
                if (chkAllDayEvent.Checked)
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString() + " All Day");
                    stringBuilder.Replace("[InterviewStartTime]", "");
                    stringBuilder.Replace("[InterviewEndTime]", "");
                }
                else
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString());
                    stringBuilder.Replace("[InterviewStartTime]", ddlStartTime.SelectedItem.Text.ToString());
                    string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                    if (TimeZoneLocation != "Select Timezone")
                    {
                        string TimeWithTimeZone = ddlEndTime.SelectedItem.Text.ToString() + " [ TimeZone - " + ddlTimezone.SelectedItem.Text.ToString() + " ]";
                        stringBuilder.Replace("[InterviewEndTime]", TimeWithTimeZone.ToString());
                    }
                    else
                    {
                        stringBuilder.Replace("[InterviewEndTime]", ddlEndTime.SelectedItem.Text.ToString());
                    }

                }
                Interview iDetail = Facade.GetInterviewById(interviewId);
                int canid = iDetail.MemberId;
                Member Cannameandaddress = Facade.GetMemberById(canid);
                if (ddlJobPosting.SelectedIndex > 0)
                {
                    JobPosting jDetail = Facade.GetJobPostingById(iDetail.JobPostingId);
                    stringBuilder.Replace("[ReqCode]", jDetail.JobPostingCode);
                    stringBuilder.Replace("[JobTitle]", jDetail.JobTitle);
                }
                else
                {
                    stringBuilder.Replace("[ReqCode]", "");
                    stringBuilder.Replace("[JobTitle]", "");
                }

                /////////Done//////////////////Sumit Sonawane 21/06/2016/////////////////////////////////////////////////////////////////////////////

                StringBuilder NameOfInterviewersAll = new StringBuilder();
                foreach (ListItem item in chkClientInterviers.Items)
                {
                    if (item.Selected)
                    {
                        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                    }
                    // else { NameOfInterviewersAll = null; }

                }
                //**********Code commented by pravin khot on 11/July/2016*********
                //foreach (ListItem item in chkInternalInterviewer.Items)
                //{
                //    if (item.Selected)
                //    {
                //        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                //    }
                //    //  else { NameOfInterviewersAll = null; }

                //}
                //********************END********************
                foreach (ListItem item in chkSuggestedInterviewer.Items)
                {
                    if (item.Selected)
                    {
                        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                    }
                    //  else { NameOfInterviewersAll = null; }

                }
                //NameOfInterviewersAll = NameOfInterviewersAll.Append(txtOtherInterviewers.Text);
                //NameOfInterviewersAll = NameOfInterviewersAll.Append("Other  Interviewers : ");
                string str = hfOtherInterviewers.Value.ToString();
                string Substr = string.Empty;
                if (str.StartsWith(","))
                {
                    Substr = str.Substring(1, str.Length - 1);
                }
                else { Substr = hfOtherInterviewers.Value.ToString(); }
                //NameOfInterviewersAll = NameOfInterviewersAll.Append(Substr);
                Substr = Substr.Replace(",", "<br/>" + "&nbsp;&nbsp;");
                stringBuilder.Replace("[NameOfOtherInterviewers]", Substr.ToString());
                stringBuilder.Replace("[NameOfInterviewers]", NameOfInterviewersAll.ToString());

                /////////////////////////////////////////////////////////////////////////////////////////
                //stringBuilder.Replace("[RecruiterName]", CurrentMember.FirstName + "  " + CurrentMember.LastName);
                stringBuilder.Replace("[InterviewTitle]", txtAppointmentTitle.Text);
                if (ddlInterviewType.SelectedIndex > 0)
                {
                    stringBuilder.Replace("[InterviewType]", ddlInterviewType.SelectedItem.Text);
                }
                else
                {
                    stringBuilder.Replace("[InterviewType]", "");
                }

                stringBuilder.Replace("[Location]", txtLocation.Text.ToString());
                stringBuilder.Replace("[CandidateName]", Cannameandaddress.FirstName + " " + Cannameandaddress.LastName);
                stringBuilder.Replace("[CandidateContact]", Cannameandaddress.CellPhone);
                stringBuilder.Replace("[CandidateEmail]", Cannameandaddress.PrimaryEmail);
                stringBuilder.Replace("[NOTE]", txtRemarks.Text.Trim());//ADDED BY PRAVIN KHOT ON 6/May/2016

                //stringBuilder.Replace("[Location]", txtLocation.Text.ToString());
                // stringBuilder.Replace("[NameOfInterviewers]", (InterviewsName != string.Empty ? InterviewsName.Trim() : string.Empty));
                stringBuilder.Replace("[Signature]", Sign);
                return stringBuilder.ToString();
            }
            return "";

        }

        private string getInterviewScheduleBodyForRecruiter(int interviewId)
        {
            if (interviewId != 0)
            {
                string InterviewsName = Facade.GetInterviersNameByInterviewId(interviewId);
                string TemplateUrl = AppSettings.InterviewMailTemplateUrl;
                string mailBody = GetFileContentFromUrlForRecruiter(TemplateUrl);
                StringBuilder stringBuilder = new StringBuilder();
                string Sign = string.Empty;
                stringBuilder.Append(mailBody);
                MemberSignature signature = new MemberSignature();
                signature = Facade.GetActiveMemberSignatureByMemberId(CurrentMember.Id);
                if (signature != null)
                    Sign = signature.Signature;
                else
                    Sign = CurrentMember.FirstName + "  " + CurrentMember.LastName;
                if (chkAllDayEvent.Checked)
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString() + " All Day");
                    stringBuilder.Replace("[InterviewStartTime]", "");
                    stringBuilder.Replace("[InterviewEndTime]", "");
                }
                else
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString());
                    stringBuilder.Replace("[InterviewStartTime]", ddlStartTime.SelectedItem.Text.ToString());
                    string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                    if (TimeZoneLocation != "Select Timezone")
                    {
                        string TimeWithTimeZone = ddlEndTime.SelectedItem.Text.ToString() + " [ TimeZone - " + ddlTimezone.SelectedItem.Text.ToString() + " ]";
                        stringBuilder.Replace("[InterviewEndTime]", TimeWithTimeZone.ToString());
                    }
                    else
                    {
                        stringBuilder.Replace("[InterviewEndTime]", ddlEndTime.SelectedItem.Text.ToString());
                    }

                }

                Interview iDetail = Facade.GetInterviewById(interviewId);
                int canid = iDetail.MemberId;
                Member Cannameandaddress = Facade.GetMemberById(canid);
                if (ddlJobPosting.SelectedIndex > 0)
                {
                    JobPosting jDetail = Facade.GetJobPostingById(iDetail.JobPostingId);
                    stringBuilder.Replace("[ReqCode]", jDetail.JobPostingCode);
                    stringBuilder.Replace("[JobTitle]", jDetail.JobTitle);
                }
                else
                {
                    stringBuilder.Replace("[ReqCode]", "");
                    stringBuilder.Replace("[JobTitle]", "");
                }
                /////////Done//////////////////Sumit Sonawane 21/06/2016/////////////////////////////////////////////////////////////////////////////

                StringBuilder NameOfInterviewersAll = new StringBuilder();
                foreach (ListItem item in chkClientInterviers.Items)
                {
                    if (item.Selected)
                    {
                        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                    }
                    // else { NameOfInterviewersAll = null; }

                }
                //foreach (ListItem item in chkInternalInterviewer.Items)
                //{
                //    if (item.Selected)
                //    {
                //        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                //    }
                //    //  else { NameOfInterviewersAll = null; }

                //}
                foreach (ListItem item in chkSuggestedInterviewer.Items)
                {
                    if (item.Selected)
                    {
                        NameOfInterviewersAll.Append(item + "<br/>" + "&nbsp;&nbsp;");
                    }
                    //  else { NameOfInterviewersAll = null; }

                }
                //NameOfInterviewersAll = NameOfInterviewersAll.Append(txtOtherInterviewers.Text);
                //NameOfInterviewersAll = NameOfInterviewersAll.Append("Other  Interviewers : ");
                string str = hfOtherInterviewers.Value.ToString();
                string Substr = string.Empty;
                if (str.StartsWith(","))
                {
                    Substr = str.Substring(1, str.Length - 1);
                }
                else { Substr = hfOtherInterviewers.Value.ToString(); }
                //NameOfInterviewersAll = NameOfInterviewersAll.Append(Substr);
                Substr = Substr.Replace(",", "<br/>" + "&nbsp;&nbsp;");
                stringBuilder.Replace("[NameOfOtherInterviewers]", Substr.ToString());
                stringBuilder.Replace("[NameOfInterviewers]", NameOfInterviewersAll.ToString());

                /////////////////////////////////////////////////////////////////////////////////////////
                //stringBuilder.Replace("[RecruiterName]", CurrentMember.FirstName + "  " + CurrentMember.LastName);
                stringBuilder.Replace("[InterviewTitle]", txtAppointmentTitle.Text);
                if (ddlInterviewType.SelectedIndex > 0)
                {
                    stringBuilder.Replace("[InterviewType]", ddlInterviewType.SelectedItem.Text);
                }
                else
                {
                    stringBuilder.Replace("[InterviewType]", "");
                }

                stringBuilder.Replace("[Location]", txtLocation.Text.ToString());
                stringBuilder.Replace("[NameOfCandidates]", Cannameandaddress.FirstName + "  " + Cannameandaddress.LastName);
                stringBuilder.Replace("[ContactOfCandidates]", Cannameandaddress.CellPhone);
                stringBuilder.Replace("[CandidateEmail]", Cannameandaddress.PrimaryEmail);
                stringBuilder.Replace("[NOTE]", txtRemarks.Text.Trim());

                 stringBuilder.Replace("[NameOfInterviewers]", (InterviewsName != string.Empty ? InterviewsName.Trim() : string.Empty));
                stringBuilder.Replace("[Signature]", Sign);
                return stringBuilder.ToString();
            }

            return "";

        }
        public void lnkEditInterviewerNotify_Click(object sender, EventArgs e)
        {
            InterviewModalPopupExtender.Show();
            string mailBody = null;
            if (txtEmailInterviewerTemplate.Text == "")
            {
                mailBody = getEditInterviewScheduleBody();
                txtEmailInterviewerTemplate.Text = mailBody;
            }
            mailBody = null;
        }

        public void lnkEditCandiateNotify_Click(object sender, EventArgs e)
        {
            CandidateModalPopupExtender.Show();
            string mailBody = null;
            if (txtEmailCandidateTemplate.Text == "")
            {
                mailBody = getEditInterviewScheduleBody();
                txtEmailCandidateTemplate.Text = mailBody;
            }
            mailBody = null;
        }


        private string getEditInterviewScheduleBody()
        {
            string body = "";
            char[] delim = { ',' };
            StringBuilder stringBuilder = new StringBuilder();
            string[] candidateIds = CandidateId.Split(delim, StringSplitOptions.RemoveEmptyEntries);

            if (!isMainApplication)
            {
                switch (ddlInterviewType.SelectedItem.Text)
                {
                    case "Face to Face":
                        body = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(candidateIds.GetValue(0)), 0, ddlInterviewType.SelectedItem.Text, 0);
                        break;
                    case "Phone":
                        body = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(candidateIds.GetValue(0)), 0, ddlInterviewType.SelectedItem.Text, 0);
                        break;
                    case "ProfileReject":
                        body = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(candidateIds.GetValue(0)), 0, ddlInterviewType.SelectedItem.Text, 0);
                        break;
                    case "Reject":
                        body = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(candidateIds.GetValue(0)), 0, ddlInterviewType.SelectedItem.Text, 0);
                        break;
                    case "Initial Screening":
                        body = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(candidateIds.GetValue(0)), 0, ddlInterviewType.SelectedItem.Text, 0);
                        break;
                    case "Please Select":
                        body = "";
                        break;
                    case "Informal":
                        body = "";
                        break;
                }
            }
            stringBuilder.Append(body);
            return stringBuilder.ToString();
        }
        private string GetTempFolder()
        {
            if (StringHelper.IsBlank(TempDirectory))
            {
                string dir = CurrentUserId.ToString();
                string path = Path.Combine(UrlConstants.TempDirectory, dir);

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                TempDirectory = path;
            }
            if (hdnTmpFolder.Value == string.Empty)
            {
                DateTime dt = DateTime.Now;
                string s = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString() + "";
                if (!Directory.Exists(TempDirectory + "\\" + s))
                {
                    Directory.CreateDirectory(TempDirectory + "\\" + s);
                    hdnTmpFolder.Value = s;
                    PreviewDir = TempDirectory + "\\" + s;
                }
            }
            else
            {
                PreviewDir = TempDirectory + "\\" + hdnTmpFolder.Value;
            }
            return PreviewDir;
        }
        public string GenerateFileName(string context)
        {
            return context + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_";
        }

        //"STATUS:CANCELLED"
        private void SendAppoinmentforOrganizer(string RecipientEmail, string memberName, string CID)
        {
            string calJobPostingCode = string.Empty;
            string calJobTitle = string.Empty;
            string calcanid = string.Empty;
            string calcanname = string.Empty;
            string caltitle = string.Empty;


            string InterviewsName = string.Empty;
            string[] InterviewsNames;
            foreach (ListItem item in chkClientInterviers.Items)
            {
                if (item.Selected)
                {
                    InterviewsNames = item.Text.Split('[');
                    //InterviewsName = InterviewsName + ",Mr/Ms " + InterviewsNames[0].ToString();
                    InterviewsName = InterviewsName + "," + InterviewsNames[0].ToString();
                }

            }
            InterviewsName = InterviewsName.Trim().TrimStart(',');
            //bool IsOrganizer = false;
            //if (CurrentMember.PrimaryEmail == RecipientEmail)
            //{
            //    IsOrganizer = true;
            //}
            //string stpath = "";
            //IList<MemberDocument> documen = Facade.GetLatestResumeByMemberID(Convert.ToInt32(CandidateId));
            //if (documen != null)
            //{
            //    foreach (MemberDocument doc in documen)
            //    {
            //        if (doc != null)
            //        {
            //            stpath = MiscUtil.GetMemberDocumentPath(this.Page, Convert.ToInt32(CandidateId), doc.FileName, "Word Resume", false);
            //            break;
            //        }
            //    }
            //}
            Interview interview1 = Facade.GetInterviewById(interviewidforapp);
            string Remainder1 = getappoinmentbodyrecruiter(interview1.Id, CID);

            Interview iDetail = Facade.GetInterviewById(interview1.Id);
            int canid = iDetail.MemberId;
            Member Cannameandaddress1 = Facade.GetMemberById(canid);
            Member Cannameandaddress = Facade.GetMemberById(Convert.ToInt32(CID));
            if (ddlJobPosting.SelectedIndex > 0)
            {
                JobPosting jDetail = Facade.GetJobPostingById(iDetail.JobPostingId);
                calJobPostingCode = jDetail.JobPostingCode;
                calJobTitle = jDetail.JobTitle;

            }
            else
            {
                calJobPostingCode = string.Empty;
                calJobTitle = string.Empty;

            }



            calcanid = Cannameandaddress.Id.ToString();
            calcanname = Cannameandaddress.FirstName + Cannameandaddress.LastName;


            //*********Code modify by pravin khot 4/March/2016**************START CODE**************
            //caltitle = "Interview Meeting - " + txtAppointmentTitle.Text + " _ " + calJobPostingCode + " - " + calJobTitle + " _ " + calcanid + " - " + calcanname;
            caltitle = "Interview - " + txtAppointmentTitle.Text + " _ " + calJobPostingCode + " _ " + calcanid + " - " + calcanname;
            //************************END CODE******************************************************


            try //current id
            {
                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, CurrentMember.Id, "Appt.ics", "Other", false);
                //string fileName = "Appt1.vcs";
                //string fileName = "Appt-";
                string fileName = "";
                //fileName=GenerateFileName(fileName);
                //fileName += txtAppointmentTitle.Text;
                //after change 
                fileName += caltitle;
                //fileName += System.Guid.NewGuid().ToString();
                fileName += ".ics";

                string dir = GetTempFolder();

                string filePath = Path.Combine(dir, fileName);

                StringBuilder writer = new StringBuilder();
                string startDate = Convert.ToDateTime(wdcStartDate.Text).ToShortDateString();
                string startTime = ddlStartTime.SelectedItem.Text;
                string endDateTime = ddlEndTime.SelectedItem.Text;
                //string attendee = getAttendies();
                int intDuration = 0;
                DateTime dtStartDateTime = GetDateAndDuration(out intDuration); //Convert.ToDateTime(startDate + " " + startTime);
                DateTime dtEndDateTime = Convert.ToDateTime(startDate + " " + endDateTime);
				writer.AppendLine("BEGIN:VCALENDAR");
                writer.AppendLine("VERSION:2.0");
                writer.AppendLine("PRODID:-//Microsoft Corporation//Outlook MIMEDIR//EN");

                //writer.AppendLine("METHOD:REQUEST");  
              
                //writer.AppendLine("PRODID:-//Google Inc//Google Calendar 70.9054//EN");
                //writer.AppendLine("CALSCALE:GREGORIAN");
                
             

                writer.AppendLine("BEGIN:VEVENT");
                writer.AppendLine("X-ALT-DESC;FMTTYPE=text/html:" + Remainder1 + "");
                //*********Code added by pravin khot on 4/May/2016****************
                if (iDetail.IcsFileUIDCode == "" || iDetail.IcsFileUIDCode == null)
                {
                    Facade.UpdateInterviewIcsCodeById(interview1.Id, Appuid.ToString());
                    writer.AppendLine("UID:" + Appuid.ToString());

                }
                else
                {
                    writer.AppendLine("UID:" + iDetail.IcsFileUIDCode.ToString());
                }
                //******************************END*************************************

                // writer.AppendLine("UID:" + Appuid.ToString());//comment by pravin khot on 4/May/2016

                string tt = string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime));
                string ttt = string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime));
                int ix = tt.IndexOf(":");
                int ixx = ttt.IndexOf(":");

                tt = tt.Substring(ix + 1);
                ttt = ttt.Substring(ixx + 1);
                string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                if (TimeZoneLocation != "Select Timezone")
                {
                    writer.AppendLine(string.Format("DTSTART;TZID=" + TimeZoneLocation + ":" + tt + ""));
                    writer.AppendLine(string.Format("DTEND;TZID=" + TimeZoneLocation + ":" + ttt + ""));
                }
                else
                {
                    writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime)));
                    writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                    writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                }
                //string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", "TZID:'" + TimeZoneLocation + "'", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime))); //TZID=US-Pacific:20140606T180000
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", "TZID:'" + TimeZoneLocation + "'", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));

                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime)));
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                //writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));

                //***********Added by pravin khot on 14/July/2016****************
                //writer.AppendLine("ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:" + RecipientEmail);
                //writer.AppendLine("ORGANIZER;CN= " + CurrentUserName + ":mailto:" + CurrentMember.PrimaryEmail);
                //**************************END*********************************


                writer.AppendLine("LOCATION:" + MiscUtil.RemoveScript(txtLocation.Text));
               
                //if (stpath != "" || stpath != string.Empty)
                //    writer.AppendLine("ATTACH;FMTTYPE=application/msword:" + stpath);
                writer.AppendLine("BEGIN:VALARM");
                writer.AppendLine("TRIGGER:-PT15M");
                writer.AppendLine("ACTION:DISPLAY");
                writer.AppendLine("STATUS:CONFIRMED"); // writer.AppendLine("STATUS:CANCELLED");
                //writer.AppendLine("DESCRIPTION:Remainder1");
                //writer.AppendLine("DESCRIPTION:The appointment details are:\n\nDate:"+wdcStartDate.Text.ToString()+"\nStart Time:"+ddlStartTime.SelectedItem.Text.ToString()+"\nEnd Time:"+ddlEndTime.SelectedItem.Text.ToString()+"\nLocation:"+txtLocation.Text.ToString()+"\nInterviewers:"+(InterviewsName != string.Empty ? InterviewsName.Trim() : string.Empty));

                writer.AppendLine("END:VALARM");
                //writer.AppendLine("SUMMARY:" + MiscUtil.RemoveScript(txtAppointmentTitle.Text));
                writer.AppendLine("SUMMARY:" + MiscUtil.RemoveScript(caltitle));

                writer.AppendLine("END:VEVENT");
                writer.AppendLine("END:VCALENDAR");
                //writer.AppendLine("PRODID:-//Microsoft Corporation//Outlook MIMEDIR//EN");
                //writer.AppendLine("BEGIN:VCALENDAR");
                //writer.AppendLine("VERSION:2.0");
                //writer.AppendLine("METHOD:REQUEST");
                //writer.AppendLine("BEGIN:VEVENT");
                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime)));
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                //writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //if (!IsOrganizer)
                //{
                //    writer.AppendLine("ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:" + RecipientEmail);
                //    writer.AppendLine("ORGANIZER;CN= " + CurrentUserName + ":mailto:" + CurrentMember.PrimaryEmail);
                //}
                //writer.AppendLine("UID:" + System.Guid.NewGuid().ToString());
                ////writer.AppendLine(string.Format("CREATED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                ////writer.AppendLine("DESCRIPTION:\n\n"+Remainder1+"");
                //writer.AppendLine("X-ALT-DESC;FMTTYPE=text/html:" + Remainder1 + "");
                ////writer.AppendLine(string.Format("LAST-MODIFIED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                ////writer.AppendLine("SEQUENCE:0");
                ////writer.AppendLine("STATUS:CONFIRMED");
                //writer.AppendLine("SUMMARY:" + MiscUtil.RemoveScript(txtAppointmentTitle.Text));
                //writer.AppendLine("LOCATION:" + MiscUtil.RemoveScript(txtLocation.Text));

                ////writer.AppendLine("TRANSP:OPAQUE");
                ////writer.AppendLine(string.Format("CREATED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                ////writer.AppendLine(string.Format("LAST-MODIFIED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                ////writer.AppendLine("PRIORITY:5");
                ////writer.AppendLine("X-MICROSOFT-CDO-IMPORTANCE:1");
                ////writer.AppendLine("CLASS:PUBLIC");
                ////writer.AppendLine("STATUS:CONFIRMED");
                ////writer.AppendLine("STATUS:CANCELLED");
                //writer.AppendLine("BEGIN:VALARM");
                //writer.AppendLine("TRIGGER:-PT15M");
                //writer.AppendLine("ACTION:DISPLAY");
                //writer.AppendLine("STATUS:CONFIRMED");

                //writer.AppendLine("END:VALARM");
                //writer.AppendLine("END:VEVENT");
                //writer.AppendLine("END:VCALENDAR");

                //System.IO.FileInfo file = new System.IO.FileInfo(filePath);
                UTF8Encoding enc = new UTF8Encoding();
                byte[] arrBytData = enc.GetBytes(writer.ToString());

                //Response.Clear();
                ////Response.ContentType = "text/plain"; // clear the current output content from the buffer

                //Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                //Response.AppendHeader("Content-Length", arrBytData.Length.ToString());

                //Response.ContentType = "application/octet-stream";
                //Response.BinaryWrite(arrBytData);
                //Response.Flush();
                //Response.End();

                File.WriteAllBytes(strFilePath, arrBytData);

                bool isFileExist = File.Exists(strFilePath);
                if (isFileExist)
                {
                    try
                    {
                        if (!File.Exists(filePath))
                        {
                            File.Copy(strFilePath, filePath);
                        }
                    }
                    catch (FileNotFoundException ex)
                    {

                    }
                    if (strFilePath != fileName)
                    {
                        if (Files.ContainsKey(fileName))
                            Files.Remove(fileName);
                        Files.Add(fileName, filePath);
                    }
                    else
                    {
                        //return   "(File not found)";
                    }
                }

            }
            catch (System.Exception ex)
            {
                string test = ex.Message;
            }

        }
        //STATUS:CONFIRMED
        private void SendAppoinment(string RecipientEmail, string memberName)
        {
            string calJobPostingCode = string.Empty;
            string calJobTitle = string.Empty;
            string calcanid = string.Empty;
            string calcanname = string.Empty;
            string caltitle = string.Empty;

            string receipentmail1 = string.Empty;

           
            //bool IsOrganizer = false;
            //if (CurrentMember.PrimaryEmail == RecipientEmail)
            //{
            //    IsOrganizer = true;
            //}
            //string stpath = "";
            //IList<MemberDocument> documen = Facade.GetLatestResumeByMemberID(Convert.ToInt32(CandidateId));
            //if (documen != null)
            //{
            //    foreach (MemberDocument doc in documen)
            //    {
            //        if (doc != null)
            //        {
            //            stpath = MiscUtil.GetMemberDocumentPath(this.Page, Convert.ToInt32(CandidateId), doc.FileName, "Word Resume", false);
            //            break;
            //        }
            //    }
            //}
            Interview interview1 = Facade.GetInterviewById(interviewidforapp);
            string Remainder1 = getappoinmentbody(interview1.Id);


            Interview iDetail = Facade.GetInterviewById(interview1.Id);
            int canid = iDetail.MemberId;
            Member Cannameandaddress = Facade.GetMemberById(canid);
            if (ddlJobPosting.SelectedIndex > 0)
            {
                JobPosting jDetail = Facade.GetJobPostingById(iDetail.JobPostingId);
                calJobPostingCode = jDetail.JobPostingCode;
                calJobTitle = jDetail.JobTitle;

            }
            else
            {
                calJobPostingCode = string.Empty;
                calJobTitle = string.Empty;

            }

            calcanid = Cannameandaddress.Id.ToString();
            calcanname = Cannameandaddress.FirstName + Cannameandaddress.LastName;

            //*********Code modify by pravin khot 4/March/2016**************START CODE**************
            //caltitle = "Interview Meeting - " + txtAppointmentTitle.Text + " _ " + calJobPostingCode + " - " + calJobTitle + " _ " + calcanid + " - " + calcanname;
            caltitle = "Interview - " + txtAppointmentTitle.Text + " _ " + calJobPostingCode + " _ " + calcanid + " - " + calcanname;
            //************************END CODE******************************************************    
            string InterviewsName = string.Empty;
            string internalrecruiter = string.Empty;
            string[] InterviewsNames;
            string[] internalrecruiters;

            foreach (ListItem lstItm in chkInternalInterviewer.Items)
            {
                if (lstItm.Selected)
                {

                    internalrecruiters = lstItm.Text.Split('[');
                    internalrecruiters[1] = internalrecruiters[1].Replace(']', ' ').Trim();
                    if (internalrecruiters[1].ToString() != CurrentMember.PrimaryEmail.ToString())
                    {
                        internalrecruiter = internalrecruiter + "," + internalrecruiters[1].ToString();
                    }

                }
            }
            internalrecruiter = internalrecruiter.Trim().TrimStart(',');


            internalrecruiter = internalrecruiter.Replace(']', ' ');

            foreach (ListItem item in chkClientInterviers.Items)
            {
                if (item.Selected)
                {

                    InterviewsNames = item.Text.Split('[');
                    //InterviewsName = InterviewsName + ",Mr/Ms " + InterviewsNames[0].ToString();
                    InterviewsName = InterviewsName + "," + InterviewsNames[1].ToString();
                }

            }
            InterviewsName = InterviewsName.Trim().TrimStart(',');

            InterviewsName = InterviewsName.Replace(']', ' ');
            receipentmail1 = internalrecruiter + "," + InterviewsName;
            receipentmail1 = receipentmail1.Replace(",,", ",");

            receipentmail1 = receipentmail1.TrimStart(',');
            receipentmail1 = receipentmail1.Trim();
            receipentmail1 = receipentmail1.TrimEnd(',');
            try
            {
                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, CurrentMember.Id, "Appt.ics", "Other", false);
                //string fileName = "Appt1.vcs";
                //string fileName = "Appt-";
                string fileName = "";
                //fileName=GenerateFileName(fileName);
                fileName += txtAppointmentTitle.Text;
                ////after change 
                //fileName += caltitle;
                //fileName += System.Guid.NewGuid().ToString();
                fileName += ".ics";

                string dir = GetTempFolder();

                string filePath = Path.Combine(dir, fileName);

                StringBuilder writer = new StringBuilder();
                string startDate = Convert.ToDateTime(wdcStartDate.Text).ToShortDateString();
                string startTime = ddlStartTime.SelectedItem.Text;
                string endDateTime = ddlEndTime.SelectedItem.Text;
                //string attendee = getAttendies();
                int intDuration = 0;
                DateTime dtStartDateTime = GetDateAndDuration(out intDuration); //Convert.ToDateTime(startDate + " " + startTime);
                DateTime dtEndDateTime = Convert.ToDateTime(startDate + " " + endDateTime);                
                writer.AppendLine("BEGIN:VCALENDAR");
                writer.AppendLine("PRODID:-//Microsoft Corporation//Outlook MIMEDIR//EN");
                //writer.AppendLine("PRODID:-//Google Inc//Google Calendar 70.9054//EN");
                //writer.AppendLine("CALSCALE:GREGORIAN");
                writer.AppendLine("VERSION:2.0");
                writer.AppendLine("METHOD:REQUEST");
                writer.AppendLine("BEGIN:VEVENT");

                string tt = string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime));
                string ttt = string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime));
                int ix = tt.IndexOf(":");
                int ixx = ttt.IndexOf(":");

                tt = tt.Substring(ix + 1);
                ttt = ttt.Substring(ixx + 1);
                string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                if (TimeZoneLocation != "Select Timezone")
                {
                    writer.AppendLine(string.Format("DTSTART;TZID=" + TimeZoneLocation + ":" + tt + ""));
                    writer.AppendLine(string.Format("DTEND;TZID=" + TimeZoneLocation + ":" + ttt + ""));
                }
                else
                {
                    writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime)));
                    writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                    writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                }

                //string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", "TZID:'" + TimeZoneLocation + "'", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime))); //TZID=US-Pacific:20140606T180000
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", "TZID:'" + TimeZoneLocation + "'", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
             
                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime)));
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                //writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //if (!IsOrganizer)
                //{

                writer.AppendLine("ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:" + RecipientEmail);
                writer.AppendLine("ORGANIZER;CN= " + CurrentUserName + ":mailto:" + CurrentMember.PrimaryEmail);
                //}
                //*********Code added by pravin khot on 4/May/2016****************
                if (iDetail.IcsFileUIDCode == "" || iDetail.IcsFileUIDCode == null)
                {
                    Facade.UpdateInterviewIcsCodeById(interview1.Id, Appuid.ToString());
                    writer.AppendLine("UID:" + Appuid.ToString());

                }
                else
                {
                    writer.AppendLine("UID:" + iDetail.IcsFileUIDCode.ToString());
                }
                //******************************END*************************************

                // writer.AppendLine("UID:" + Appuid.ToString());//comment by pravin khot on 4/May/2016
               
                //writer.AppendLine(string.Format("CREATED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //writer.AppendLine("DESCRIPTION:\n\n"+Remainder1+"");
                writer.AppendLine("X-ALT-DESC;FMTTYPE=text/html:" + Remainder1 + "");
                //writer.AppendLine(string.Format("LAST-MODIFIED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //writer.AppendLine("SEQUENCE:0");
                //writer.AppendLine("STATUS:CONFIRMED");
                //writer.AppendLine("SUMMARY:" + MiscUtil.RemoveScript(txtAppointmentTitle.Text));
                writer.AppendLine("SUMMARY:" + MiscUtil.RemoveScript(caltitle));
              
                writer.AppendLine("LOCATION:" + MiscUtil.RemoveScript(txtLocation.Text));
              
                //if (stpath != "" || stpath != string.Empty)
                //    writer.AppendLine("ATTACH;FMTTYPE=application/msword:" + stpath);
                //writer.AppendLine("TRANSP:OPAQUE");
                //writer.AppendLine(string.Format("CREATED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //writer.AppendLine(string.Format("LAST-MODIFIED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //writer.AppendLine("PRIORITY:5");
                //writer.AppendLine("X-MICROSOFT-CDO-IMPORTANCE:1");
                //writer.AppendLine("CLASS:PUBLIC");
                //writer.AppendLine("STATUS:CONFIRMED");
                //writer.AppendLine("STATUS:CANCELLED");
                writer.AppendLine("BEGIN:VALARM");
                writer.AppendLine("TRIGGER:-PT15M");
                writer.AppendLine("ACTION:DISPLAY");
                writer.AppendLine("STATUS:CONFIRMED");

                writer.AppendLine("END:VALARM");
                writer.AppendLine("END:VEVENT");
                writer.AppendLine("END:VCALENDAR");

                //System.IO.FileInfo file = new System.IO.FileInfo(filePath);
                UTF8Encoding enc = new UTF8Encoding();
                byte[] arrBytData = enc.GetBytes(writer.ToString());

                //Response.Clear();
                ////Response.ContentType = "text/plain"; // clear the current output content from the buffer

                //Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                //Response.AppendHeader("Content-Length", arrBytData.Length.ToString());

                //Response.ContentType = "application/octet-stream";
                //Response.BinaryWrite(arrBytData);
                //Response.Flush();
                //Response.End();

                File.WriteAllBytes(strFilePath, arrBytData);

                bool isFileExist = File.Exists(strFilePath);
                if (isFileExist)
                {
                    try
                    {
                        if (!File.Exists(filePath))
                        {
                            File.Copy(strFilePath, filePath);
                        }
                    }
                    catch (FileNotFoundException ex)
                    {

                    }
                    if (strFilePath != fileName)
                    {
                        if (Files.ContainsKey(fileName))
                            Files.Remove(fileName);
                        Files.Add(fileName, filePath);
                    }
                    else
                    {
                        //return   "(File not found)";
                    }
                }

            }
            catch (System.Exception ex)
            {
                string test = ex.Message;
            }

        }
       // STATUS:CONFIRMED
        private void SendAppoinmentbodywithacceptrejectrecruiter(string RecipientEmail, string memberName, string CID)
        {
            string calJobPostingCode = string.Empty;
            string calJobTitle = string.Empty;
            string calcanid = string.Empty;
            string calcanname = string.Empty;
            string caltitle = string.Empty;
            string receipentmail1 = string.Empty;



            //bool IsOrganizer = false;
            //if (CurrentMember.PrimaryEmail == RecipientEmail)
            //{
            //    IsOrganizer = true;
            //}
            //string stpath = "";
            //IList<MemberDocument> documen = Facade.GetLatestResumeByMemberID(Convert.ToInt32(CandidateId));
            //if (documen != null)
            //{
            //    foreach (MemberDocument doc in documen)
            //    {
            //        if (doc != null)
            //        {
            //            stpath = MiscUtil.GetMemberDocumentPath(this.Page, Convert.ToInt32(CandidateId), doc.FileName, "Word Resume", false);
            //            break;
            //        }
            //    }
            //}
            Interview interview1 = Facade.GetInterviewById(interviewidforapp);
            //string Remainder1 = getappoinmentbody(interview1.Id);
            string Remainder1 = getappoinmentbodyrecruiter(interview1.Id, CID);


            Interview iDetail = Facade.GetInterviewById(interview1.Id);
            int canid = iDetail.MemberId;
            Member Cannameandaddress = Facade.GetMemberById(Convert.ToInt32(CID));
            if (ddlJobPosting.SelectedIndex > 0)
            {
                JobPosting jDetail = Facade.GetJobPostingById(iDetail.JobPostingId);
                calJobPostingCode = jDetail.JobPostingCode;
                calJobTitle = jDetail.JobTitle;

            }
            else
            {
                calJobPostingCode = string.Empty;
                calJobTitle = string.Empty;

            }

            calcanid = Cannameandaddress.Id.ToString();
            calcanname = Cannameandaddress.FirstName + Cannameandaddress.LastName;

            //*********Code modify by pravin khot 4/March/2016**************START CODE**************
            //caltitle = "Interview Meeting - " + txtAppointmentTitle.Text + " _ " + calJobPostingCode + " - " + calJobTitle + " _ " + calcanid + " - " + calcanname;
            caltitle = "Interview - " + txtAppointmentTitle.Text + " _ " + calJobPostingCode + " _ " + calcanid + " - " + calcanname;
            //************************END CODE******************************************************

            string otherinterviewername = string.Empty;
            string InterviewsName = string.Empty;
            string internalrecruiter = string.Empty;
            string[] InterviewsNames;
            string[] internalrecruiters;

            foreach (ListItem lstItm in chkInternalInterviewer.Items)
            {
                if (lstItm.Selected)
                {

                    internalrecruiters = lstItm.Text.Split('[');
                    internalrecruiters[1] = internalrecruiters[1].Replace(']', ' ').Trim();
                    if (internalrecruiters[1].ToString() != CurrentMember.PrimaryEmail.ToString())
                    {
                        internalrecruiter = internalrecruiter + "," + internalrecruiters[1].ToString();
                    }

                }
            }
            internalrecruiter = internalrecruiter.Trim().TrimStart(',');


            internalrecruiter = internalrecruiter.Replace(']', ' ');

            foreach (ListItem item in chkClientInterviers.Items)
            {
                if (item.Selected)
                {

                    InterviewsNames = item.Text.Split('[');
                    //InterviewsName = InterviewsName + ",Mr/Ms " + InterviewsNames[0].ToString();
                    InterviewsName = InterviewsName + "," + InterviewsNames[1].ToString();
                }

            }
            InterviewsName = InterviewsName.Trim().TrimStart(',');

            InterviewsName = InterviewsName.Replace(']', ' ');

            Interview otherinterviewer = Facade.GetInterviewById(interview1.Id);
            if (otherinterviewer.OtherInterviewers != "")
            {
                otherinterviewername = otherinterviewer.OtherInterviewers.ToString();
                otherinterviewername += ",";
            }

            string candidatenameprimaryemail = Cannameandaddress.PrimaryEmail;

            if (chkSendEmailAlerttoCandidates.Checked)
            {
                candidatenameprimaryemail = Cannameandaddress.PrimaryEmail;
            }
            else
            {
                candidatenameprimaryemail = "";
            }

            receipentmail1 = internalrecruiter + "," + InterviewsName + "," + otherinterviewername + candidatenameprimaryemail;

            receipentmail1 = receipentmail1.Replace(",,", ",");

            receipentmail1 = receipentmail1.TrimStart(',');
            receipentmail1 = receipentmail1.Trim();
            receipentmail1 = receipentmail1.TrimEnd(',');

            try
            {
                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, CurrentMember.Id, "Appt.ics", "Other", false);
                //string fileName = "Appt1.vcs";
                //string fileName = "Appt-";
                string fileName = "";
                //fileName=GenerateFileName(fileName);
                fileName += caltitle;
                ////after change 
                //fileName += caltitle;
                //fileName += System.Guid.NewGuid().ToString();
                fileName += ".ics";
                //Appuid = System.Guid.NewGuid();

                string dir = GetTempFolder();


                string filePath = Path.Combine(dir, fileName);


                StringBuilder writer = new StringBuilder();
                string startDate = Convert.ToDateTime(wdcStartDate.Text).ToShortDateString();
                string startTime = ddlStartTime.SelectedItem.Text;
                string endDateTime = ddlEndTime.SelectedItem.Text;
                //string attendee = getAttendies();
                int intDuration = 0;
                DateTime dtStartDateTime = GetDateAndDuration(out intDuration); //Convert.ToDateTime(startDate + " " + startTime);
                DateTime dtEndDateTime = Convert.ToDateTime(startDate + " " + endDateTime);               
                writer.AppendLine("BEGIN:VCALENDAR");
                writer.AppendLine("PRODID:-//Microsoft Corporation//Outlook MIMEDIR//EN");
                //writer.AppendLine("PRODID:-//Google Inc//Google Calendar 70.9054//EN");
                //writer.AppendLine("CALSCALE:GREGORIAN");
                writer.AppendLine("VERSION:2.0");
                writer.AppendLine("METHOD:REQUEST");
                writer.AppendLine("BEGIN:VEVENT");

                string tt = string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime));
                string ttt = string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime));
                int ix = tt.IndexOf(":");
                int ixx = ttt.IndexOf(":");

                tt = tt.Substring(ix + 1);
                ttt = ttt.Substring(ixx + 1);
                string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                if (TimeZoneLocation != "Select Timezone")
                {
                    writer.AppendLine(string.Format("DTSTART;TZID=" + TimeZoneLocation + ":" + tt + ""));
                    writer.AppendLine(string.Format("DTEND;TZID=" + TimeZoneLocation + ":" + ttt + ""));
                }
                else
                {
                    writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime)));
                    writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                    writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                }

                //string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", "TZID:'" + TimeZoneLocation + "'", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime))); //TZID=US-Pacific:20140606T180000
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", "TZID:'" + TimeZoneLocation + "'", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                
                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime)));
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                //writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //if (!IsOrganizer)
                //{
                writer.AppendLine("ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:" + RecipientEmail);
                writer.AppendLine("ORGANIZER;CN= " + CurrentUserName + ":mailto:" + CurrentMember.PrimaryEmail);
                //}

              

                //*********Code added by pravin khot on 4/May/2016****************
                if (otherinterviewer.IcsFileUIDCode == "" || otherinterviewer.IcsFileUIDCode == null)
                {
                    Facade.UpdateInterviewIcsCodeById(interview1.Id, Appuid.ToString());
                    writer.AppendLine("UID:" + Appuid.ToString());

                }
                else
                {
                    writer.AppendLine("UID:" + otherinterviewer.IcsFileUIDCode.ToString());
                }
                //******************************END*************************************

                // writer.AppendLine("UID:" + Appuid.ToString());//comment by pravin khot on 4/May/2016
            
                //writer.AppendLine(string.Format("CREATED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //writer.AppendLine("DESCRIPTION:\n\n"+Remainder1+"");
                writer.AppendLine("X-ALT-DESC;FMTTYPE=text/html:" + Remainder1 + "");
                //writer.AppendLine(string.Format("LAST-MODIFIED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //writer.AppendLine("SEQUENCE:0");
                //writer.AppendLine("STATUS:CONFIRMED");
                //writer.AppendLine("SUMMARY:" + MiscUtil.RemoveScript(txtAppointmentTitle.Text));
                writer.AppendLine("SUMMARY:" + MiscUtil.RemoveScript(caltitle));

             
                writer.AppendLine("LOCATION:" + MiscUtil.RemoveScript(txtLocation.Text));
               
                //if (stpath != "" || stpath != string.Empty)
                //    writer.AppendLine("ATTACH;FMTTYPE=application/msword:" + stpath);
                //writer.AppendLine("TRANSP:OPAQUE");
                //writer.AppendLine(string.Format("CREATED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //writer.AppendLine(string.Format("LAST-MODIFIED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //writer.AppendLine("PRIORITY:5");
                //writer.AppendLine("X-MICROSOFT-CDO-IMPORTANCE:1");
                //writer.AppendLine("CLASS:PUBLIC");
                //writer.AppendLine("STATUS:CONFIRMED");
                //writer.AppendLine("STATUS:CANCELLED");
                writer.AppendLine("BEGIN:VALARM");
                writer.AppendLine("TRIGGER:-PT15M");
                writer.AppendLine("ACTION:DISPLAY");
                writer.AppendLine("STATUS:CONFIRMED");

                writer.AppendLine("END:VALARM");
                writer.AppendLine("END:VEVENT");
                writer.AppendLine("END:VCALENDAR");

                //System.IO.FileInfo file = new System.IO.FileInfo(filePath);
                UTF8Encoding enc = new UTF8Encoding();
                byte[] arrBytData = enc.GetBytes(writer.ToString());

                //Response.Clear();
                ////Response.ContentType = "text/plain"; // clear the current output content from the buffer

                //Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                //Response.AppendHeader("Content-Length", arrBytData.Length.ToString());

                //Response.ContentType = "application/octet-stream";
                //Response.BinaryWrite(arrBytData);
                //Response.Flush();
                //Response.End();

                File.WriteAllBytes(strFilePath, arrBytData);

                bool isFileExist = File.Exists(strFilePath);
                if (isFileExist)
                {
                    try
                    {
                        if (!File.Exists(filePath))
                        {
                            File.Copy(strFilePath, filePath);
                        }
                    }
                    catch (FileNotFoundException ex)
                    {

                    }
                    if (strFilePath != fileName)
                    {
                        if (Files.ContainsKey(fileName))
                            Files.Remove(fileName);
                        Files.Add(fileName, filePath);
                    }
                    else
                    {
                        //return   "(File not found)";
                    }
                }

            }
            catch (System.Exception ex)
            {
                string test = ex.Message;
            }

        }
        //STATUS:CONFIRMED"
        private void SendAppoinmentbodywithacceptrejectinterviewer(string RecipientEmail, string memberName, string CID)
        {
            string calJobPostingCode = string.Empty;
            string calJobTitle = string.Empty;
            string calcanid = string.Empty;
            string calcanname = string.Empty;
            string caltitle = string.Empty;
            string receipentmail1 = string.Empty;



            //bool IsOrganizer = false;
            //if (CurrentMember.PrimaryEmail == RecipientEmail)
            //{
            //    IsOrganizer = true;
            //}
            //string stpath = "";
            //IList<MemberDocument> documen = Facade.GetLatestResumeByMemberID(Convert.ToInt32(CandidateId));
            //if (documen != null)
            //{
            //    foreach (MemberDocument doc in documen)
            //    {
            //        if (doc != null)
            //        {
            //            stpath = MiscUtil.GetMemberDocumentPath(this.Page, Convert.ToInt32(CandidateId), doc.FileName, "Word Resume", false);
            //            break;
            //        }
            //    }
            //}
            Interview interview1 = Facade.GetInterviewById(interviewidforapp);
            //string Remainder1 = getappoinmentbody(interview1.Id);
            //string Remainder1 = getappoinmentbodyrecruiter(interview1.Id);
            string Remainder1 = getappoinmentbodyinterviewer(interview1.Id, CID);


            Interview iDetail = Facade.GetInterviewById(interview1.Id);
            int canid = iDetail.MemberId;
            Member Cannameandaddress = Facade.GetMemberById(Convert.ToInt32(CID));
            if (ddlJobPosting.SelectedIndex > 0)
            {
                JobPosting jDetail = Facade.GetJobPostingById(iDetail.JobPostingId);
                calJobPostingCode = jDetail.JobPostingCode;
                calJobTitle = jDetail.JobTitle;

            }
            else
            {
                calJobPostingCode = string.Empty;
                calJobTitle = string.Empty;

            }

            calcanid = Cannameandaddress.Id.ToString();
            calcanname = Cannameandaddress.FirstName + Cannameandaddress.LastName;

            //*********Code modify by pravin khot 4/March/2016**************START CODE**************
            //caltitle = "Interview Meeting - " + txtAppointmentTitle.Text + " _ " + calJobPostingCode + " - " + calJobTitle + " _ " + calcanid + " - " + calcanname;
            caltitle = "Interview - " + txtAppointmentTitle.Text + " _ " + calJobPostingCode + " _ " + calcanid + " - " + calcanname;
            //************************END CODE******************************************************

            string otherinterviewername = string.Empty;
            string InterviewsName = string.Empty;
            string internalrecruiter = string.Empty;
            string[] InterviewsNames;
            string[] internalrecruiters;

            foreach (ListItem lstItm in chkInternalInterviewer.Items)
            {
                if (lstItm.Selected)
                {

                    internalrecruiters = lstItm.Text.Split('[');
                    internalrecruiters[1] = internalrecruiters[1].Replace(']', ' ').Trim();
                    if (internalrecruiters[1].ToString() != CurrentMember.PrimaryEmail.ToString())
                    {
                        internalrecruiter = internalrecruiter + "," + internalrecruiters[1].ToString();
                    }

                }
            }
            internalrecruiter = internalrecruiter.Trim().TrimStart(',');


            internalrecruiter = internalrecruiter.Replace(']', ' ');

            foreach (ListItem item in chkClientInterviers.Items)
            {
                if (item.Selected)
                {

                    InterviewsNames = item.Text.Split('[');
                    //InterviewsName = InterviewsName + ",Mr/Ms " + InterviewsNames[0].ToString();
                    InterviewsName = InterviewsName + "," + InterviewsNames[1].ToString();
                }

            }
            InterviewsName = InterviewsName.Trim().TrimStart(',');

            InterviewsName = InterviewsName.Replace(']', ' ');

            Interview otherinterviewer = Facade.GetInterviewById(interview1.Id);
            if (otherinterviewer.OtherInterviewers != "")
            {
                otherinterviewername = otherinterviewer.OtherInterviewers.ToString();
                otherinterviewername += ",";
            }

            string candidatenameprimaryemail = Cannameandaddress.PrimaryEmail;

            if (chkSendEmailAlerttoCandidates.Checked)
            {
                candidatenameprimaryemail = Cannameandaddress.PrimaryEmail;
            }
            else
            {
                candidatenameprimaryemail = "";
            }

            receipentmail1 = internalrecruiter + "," + InterviewsName + "," + otherinterviewername + candidatenameprimaryemail;

            receipentmail1 = receipentmail1.Replace(",,", ",");

            receipentmail1 = receipentmail1.TrimStart(',');
            receipentmail1 = receipentmail1.Trim();
            receipentmail1 = receipentmail1.TrimEnd(',');

            try  //other int
            {
                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, CurrentMember.Id, "Appt.ics", "Other", false);
                //string fileName = "Appt1.vcs";
                //string fileName = "Appt-";
                string fileName = "";
                //fileName=GenerateFileName(fileName);
                fileName += caltitle;
                ////after change 
                //fileName += caltitle;
                //fileName += System.Guid.NewGuid().ToString();
                fileName += ".ics";
                 //Appuid=System.Guid.NewGuid();
                string dir = GetTempFolder();
                string filePath = Path.Combine(dir, fileName);

                StringBuilder writer = new StringBuilder();
                string startDate = Convert.ToDateTime(wdcStartDate.Text).ToShortDateString();
                string startTime = ddlStartTime.SelectedItem.Text;
                string endDateTime = ddlEndTime.SelectedItem.Text;
                //string attendee = getAttendies();
                int intDuration = 0;
                DateTime dtStartDateTime = GetDateAndDuration(out intDuration); //Convert.ToDateTime(startDate + " " + startTime);
                DateTime dtEndDateTime = Convert.ToDateTime(startDate + " " + endDateTime);               
                writer.AppendLine("BEGIN:VCALENDAR");
                writer.AppendLine("PRODID:-//Microsoft Corporation//Outlook MIMEDIR//EN");
                //writer.AppendLine("PRODID:-//Google Inc//Google Calendar 70.9054//EN");
                //writer.AppendLine("CALSCALE:GREGORIAN");              


                
                writer.AppendLine("VERSION:2.0");
                writer.AppendLine("METHOD:REQUEST");
                writer.AppendLine("BEGIN:VEVENT");                      
	



                string tt = string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime));
                string ttt = string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime));
                int ix = tt.IndexOf(":");
                int ixx = ttt.IndexOf(":");

                tt = tt.Substring(ix+1);
                ttt = ttt.Substring(ixx + 1);
                string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                if (TimeZoneLocation != "Select Timezone")
                {
                    writer.AppendLine(string.Format("DTSTART;TZID=" + TimeZoneLocation + ":" + tt + ""));
                    writer.AppendLine(string.Format("DTEND;TZID=" + TimeZoneLocation + ":" + ttt + ""));
                }
                else
                {
                    writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime)));
                    writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                    writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                }

                //writer.AppendLine(string.Format("DTSTART;TZID=Asia/Dubai:20160616T113000DTEND")); //TZID=US-Pacific:20140606T180000

                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", "TZID=" + TimeZoneLocation + ""));

                
                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", "TZID=" + TimeZoneLocation + "")); //TZID=US-Pacific:20140606T180000
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", "TZID=" + TimeZoneLocation + ""));
                //writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));

                //string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", "TZID:'" + TimeZoneLocation + "'", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime))); //TZID=US-Pacific:20140606T180000
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", "TZID:'" + TimeZoneLocation + "'", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));


                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", "TZID:Europe/London", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime))); //TZID=US-Pacific:20140606T180000
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", "TZID:Europe/London", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                //writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));

                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", "TZID:Asia/Shanghai", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime))); //TZID=US-Pacific:20140606T180000
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", "TZID:Asia/Shanghai", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                //writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));

                //writer.AppendLine(string.Format(TimeZoneInfo.ConvertTimeBySystemTimeZoneId(currentTime, TimeZoneInfo.Local.Id, "China Standard Time")));
                              
                //if (!IsOrganizer)
                //{
                writer.AppendLine("ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:" + RecipientEmail);
                writer.AppendLine("ORGANIZER;CN= " + CurrentUserName + ":mailto:" + CurrentMember.PrimaryEmail);
                //}
                //*********Code added by pravin khot on 4/May/2016****************
                if (otherinterviewer.IcsFileUIDCode == "" || otherinterviewer.IcsFileUIDCode == null)
                {
                    Facade.UpdateInterviewIcsCodeById(interview1.Id, Appuid.ToString());
                    writer.AppendLine("UID:" + Appuid.ToString());

                }
                else
                {
                    writer.AppendLine("UID:" + otherinterviewer.IcsFileUIDCode.ToString());
                }
                //******************************END*************************************

               // writer.AppendLine("UID:" + Appuid.ToString());//comment by pravin khot on 4/May/2016
                //writer.AppendLine(string.Format("CREATED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //writer.AppendLine("DESCRIPTION:\n\n"+Remainder1+"");
                writer.AppendLine("X-ALT-DESC;FMTTYPE=text/html:" + Remainder1 + "");
                //writer.AppendLine(string.Format("LAST-MODIFIED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //writer.AppendLine("SEQUENCE:0");
                //writer.AppendLine("STATUS:CONFIRMED");
                //writer.AppendLine("SUMMARY:" + MiscUtil.RemoveScript(txtAppointmentTitle.Text));
                writer.AppendLine("SUMMARY:" + MiscUtil.RemoveScript(caltitle));
             
                writer.AppendLine("LOCATION:" + MiscUtil.RemoveScript(txtLocation.Text));
              

                //if (stpath != "" || stpath != string.Empty)
                //    writer.AppendLine("ATTACH;FMTTYPE=application/msword:" + stpath);
                //writer.AppendLine("TRANSP:OPAQUE");
                //writer.AppendLine(string.Format("CREATED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //writer.AppendLine(string.Format("LAST-MODIFIED:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //writer.AppendLine("PRIORITY:5");
                //writer.AppendLine("X-MICROSOFT-CDO-IMPORTANCE:1");
                //writer.AppendLine("CLASS:PUBLIC");
                //writer.AppendLine("STATUS:CONFIRMED");
                //writer.AppendLine("STATUS:CANCELLED");

                writer.AppendLine("BEGIN:VALARM");
                writer.AppendLine("TRIGGER:-PT15M");
                writer.AppendLine("ACTION:DISPLAY");
                writer.AppendLine("STATUS:CONFIRMED");

                writer.AppendLine("END:VALARM");
                writer.AppendLine("END:VEVENT");
                writer.AppendLine("END:VCALENDAR");

                //System.IO.FileInfo file = new System.IO.FileInfo(filePath);
                UTF8Encoding enc = new UTF8Encoding();
                byte[] arrBytData = enc.GetBytes(writer.ToString());

                //Response.Clear();
                ////Response.ContentType = "text/plain"; // clear the current output content from the buffer

                //Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

                //Response.AppendHeader("Content-Length", arrBytData.Length.ToString());

                //Response.ContentType = "application/octet-stream";
                //Response.BinaryWrite(arrBytData);
                //Response.Flush();
                //Response.End();

                File.WriteAllBytes(strFilePath, arrBytData);

                bool isFileExist = File.Exists(strFilePath);
                if (isFileExist)
                {
                    try
                    {
                        if (!File.Exists(filePath))
                        {
                            File.Copy(strFilePath, filePath);
                        }
                    }
                    catch (FileNotFoundException ex)
                    {

                    }
                    if (strFilePath != fileName)
                    {
                        if (Files.ContainsKey(fileName))
                            Files.Remove(fileName);
                        Files.Add(fileName, filePath);
                    }
                    else
                    {
                        //return   "(File not found)";
                    }
                }

            }
            catch (System.Exception ex)
            {
                string test = ex.Message;
            }

        }




        //private void SendAppoinment(string RecipientEmail)
        //{
        //    try
        //    {
        //        string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, CurrentMember.Id, "Appt.vcs", "Other", false);
        //        //string fileName = "Appt1.vcs";
        //        string fileName = "Appt";
        //        fileName=GenerateFileName(fileName);
        //        fileName += txtAppointmentTitle.Text;
        //        fileName += System.Guid.NewGuid().ToString();
        //        fileName += ".vcs";

        //        string dir = GetTempFolder();

        //        string filePath = Path.Combine(dir, fileName);

        //        StringBuilder writer = new StringBuilder();
        //        string startDate = Convert.ToDateTime(wdcStartDate.Text).ToShortDateString();
        //        string startTime = ddlStartTime.SelectedItem.Text;
        //        string endDateTime = ddlEndTime.SelectedItem.Text;
        //        //string attendee = getAttendies();
        //        int intDuration = 0;
        //        DateTime dtStartDateTime = GetDateAndDuration(out intDuration); //Convert.ToDateTime(startDate + " " + startTime);
        //        DateTime dtEndDateTime = Convert.ToDateTime(startDate + " " + endDateTime);
        //        writer.AppendLine("PRODID:-//Microsoft Corporation//Outlook MIMEDIR//EN");
        //        writer.AppendLine("BEGIN:VCALENDAR");
        //        writer.AppendLine("METHOD:REQUEST");
        //        writer.AppendLine("BEGIN:VEVENT");

        //        //Int32 intDuration = 0;

        //        //DateTime dtStartDateTime = GetDateAndDuration(out intDuration);
        //        //DateTime startDate = TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime);

        //        writer.AppendLine("SUMMARY:" + MiscUtil.RemoveScript(txtAppointmentTitle.Text));

        //        writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime)));
        //        writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
        //        writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));

        //        writer.AppendLine("LOCATION:" + MiscUtil.RemoveScript(txtLocation.Text));

        //        writer.AppendLine("BEGIN:VALARM");
        //        writer.AppendLine("TRIGGER:-PT15M");
        //        writer.AppendLine("ACTION:DISPLAY");
        //        writer.AppendLine("STATUS:CANCELLED");
        //        writer.AppendLine("DESCRIPTION:Reminder");
        //        writer.AppendLine("END:VALARM");
        //        writer.AppendLine("END:VEVENT");
        //        writer.AppendLine("END:VCALENDAR");

        //        //System.IO.FileInfo file = new System.IO.FileInfo(filePath);
        //        UTF8Encoding enc = new UTF8Encoding();
        //        byte[] arrBytData = enc.GetBytes(writer.ToString());

        //        //Response.Clear();
        //        ////Response.ContentType = "text/plain"; // clear the current output content from the buffer

        //        //Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);

        //        //Response.AppendHeader("Content-Length", arrBytData.Length.ToString());

        //        //Response.ContentType = "application/octet-stream";
        //        //Response.BinaryWrite(arrBytData);
        //        //Response.Flush();
        //        //Response.End();

        //        File.WriteAllBytes(strFilePath, arrBytData);

        //        bool isFileExist = File.Exists(strFilePath);
        //        if (isFileExist)
        //        {
        //            try
        //            {
        //                if (!File.Exists(filePath))
        //                {
        //                    File.Copy(strFilePath, filePath);
        //                }
        //            }
        //            catch (FileNotFoundException ex)
        //            {

        //            }
        //            if (strFilePath != fileName)
        //            {
        //                if (Files.ContainsKey(fileName))
        //                    Files.Remove(fileName);
        //                Files.Add(fileName, filePath);
        //            }
        //            else
        //            {
        //                //return   "(File not found)";
        //            }
        //        }

        //    }
        //    catch (System.Exception ex)
        //    {
        //        string test = ex.Message;
        //    }

        //}


        public string SessionKey
        {
            get
            {
                return _sessionKey;
            }
            set
            {
                _sessionKey = value;
            }
        }

        public StringDictionary UploadedFiles
        {
            get
            {
                if (Helper.Session.Get("InterviewEmailFileUpload") == null)
                {
                    Helper.Session.Set("InterviewEmailFileUpload", new StringDictionary());
                }
                return (StringDictionary)Helper.Session.Get("InterviewEmailFileUpload");
            }
            set
            {
                Helper.Session.Set("InterviewEmailFileUpload", value);
            }
        }

        public StringDictionary Files
        {
            get
            {
                if (Helper.Session.Get(SessionKey) == null)
                {
                    Helper.Session.Set(SessionKey, new StringDictionary());
                }

                return (StringDictionary)Helper.Session.Get(SessionKey);
            }
        }
        private void SendInterviewEmail(string mailFrom, string mailTo, string mailBody, string memberName)
        {

            string Subject = "";

            //***************Code added by pravin khot on 14/March/2016***************
            Hashtable siteSettingTable = null;
            TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
            SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
            string companyname= siteSettingTable[DefaultSiteSetting.CompanyName.ToString()].ToString();
            //***************************END***********************************
            if (isNew)//if condition added by pravin khot on 6/May/2016
            {
                if (ddlJobPosting.SelectedIndex > 0)
                {
                    string[] subject = ddlJobPosting.SelectedItem.Text.Split('-');
                    Subject = "Notification: Interview Scheduled at " + companyname + " for " + subject[1].Trim(); //code modify by pravin khot ADD - companyname 
                }
                else
                {
                    Subject = "Notification: Interview Scheduled at " + companyname + " for " + (ddlJobPosting.SelectedIndex > 0 ? " - " + ddlJobPosting.SelectedItem.Text : string.Empty);//code modify by pravin khot ADD - companyname 
                }
            }
            else
            {
                if (ddlJobPosting.SelectedIndex > 0)
                {
                    string[] subject = ddlJobPosting.SelectedItem.Text.Split('-');
                    Subject = "Notification: Interview Rescheduled at " + companyname + " for " + subject[1].Trim(); //code modify by pravin khot ADD - companyname 
                }
                else
                {
                    Subject = "Notification: Interview Rescheduled at " + companyname + " for " + (ddlJobPosting.SelectedIndex > 0 ? " - " + ddlJobPosting.SelectedItem.Text : string.Empty);//code modify by pravin khot ADD - companyname 
                }

            }
            int senderid = 0;
            string senderEmailid = (mailFrom != "") ? mailFrom : SiteSetting[DefaultSiteSetting.AdminEmail.ToString()].ToString();

            senderid = Facade.GetMemberByMemberEmail(senderEmailid).Id;

            Files.Clear();
            int canId = Facade.getCandidateIdbyPrimaryEmail(mailTo.ToString());
            if (chkSendEmailAlerttoInterviewers.Checked)
            {
                if (canId == 0)
                {
                    SendAttachment(mailTo,"");
                }
            }
            //if (CurrentMember.PrimaryEmail == mailTo)
            //{
            //    SendAppoinmentforOrganizer(mailTo, memberName);
            //}
            //else
            SendAppoinment(mailTo, memberName);

            //MailQueueData.AddMailToMailQueue(senderid, mailTo, Subject, mailBody, "", "", null, Facade);
            MailQueueData.AddMailToMailQueue(senderid, mailTo, Subject, mailBody, " ", " ", Files, Facade);

        }


        private void SendInterviewEmailrecruiter(string mailFrom, string mailTo, string mailBody, string memberName, string cid)
        {

            string Subject = "";
            Member candid = Facade.GetMemberById(Convert.ToInt32(cid));//NEW LINE ADDED BY PRAVIN 14/March/2016
            string candidateid="A"+ cid;
            if (isNew)
            {
                if (ddlJobPosting.SelectedIndex > 0)
                {
                    string[] subject = ddlJobPosting.SelectedItem.Text.Split('-');
                    //***********************Code modify by pravin khot on 14/March/2016************
                    //Subject = "Notification: Interview Scheduled" + " - " + subject[1].Trim(); //OLD CODE COMMENTED BY PRAVIN 14/March/2016
                    Subject = "Notification: Interview Scheduled for" + " " + candid.FirstName + " " + candid.LastName + " [ " + candidateid + " ]- " + subject[1].Trim();//NEW LINE ADDED BY PRAVIN 14/March/2016
                    //***************************END********************************************
                }
                else
                {
                    //Subject = "Notification: Interview Scheduled" + (ddlJobPosting.SelectedIndex > 0 ? " - " + ddlJobPosting.SelectedItem.Text : string.Empty);//OLD CODE COMMENTED BY PRAVIN 14/March/2016
                    Subject = "Notification: Interview Scheduled for" + " " + candid.FirstName + " " + candid.LastName + " [ " + candidateid + " ]- " + (ddlJobPosting.SelectedIndex > 0 ? " - " + ddlJobPosting.SelectedItem.Text : string.Empty);//NEW LINE ADDED BY PRAVIN 14/March/2016
                }
            }
            else
            {
                if (ddlJobPosting.SelectedIndex > 0)
                {
                    string[] subject = ddlJobPosting.SelectedItem.Text.Split('-');
                    //***********************Code modify by pravin khot on 14/March/2016************
                    //Subject = "Notification: Interview Scheduled" + " - " + subject[1].Trim(); //OLD CODE COMMENTED BY PRAVIN 14/March/2016
                    Subject = "Notification: Interview Rescheduled for" + " " + candid.FirstName + " " + candid.LastName + " [ " + candidateid + " ]- " + subject[1].Trim();//NEW LINE ADDED BY PRAVIN 14/March/2016
                    //***************************END********************************************
                }
                else
                {
                    //Subject = "Notification: Interview Scheduled" + (ddlJobPosting.SelectedIndex > 0 ? " - " + ddlJobPosting.SelectedItem.Text : string.Empty);//OLD CODE COMMENTED BY PRAVIN 14/March/2016
                    Subject = "Notification: Interview Rescheduled for" + " " + candid.FirstName + " " + candid.LastName + " [ " + candidateid + " ]- " + (ddlJobPosting.SelectedIndex > 0 ? " - " + ddlJobPosting.SelectedItem.Text : string.Empty);//NEW LINE ADDED BY PRAVIN 14/March/2016
                }
            }
            int senderid = 0;
            string senderEmailid = (mailFrom != "") ? mailFrom : SiteSetting[DefaultSiteSetting.AdminEmail.ToString()].ToString();

            senderid = Facade.GetMemberByMemberEmail(senderEmailid).Id;

            Files.Clear();
            int canId = Facade.getCandidateIdbyPrimaryEmail(mailTo.ToString());
            if (chkSendEmailAlerttoInterviewers.Checked)
            {
                if (canId == 0)
                {
                    SendAttachment(mailTo,cid);
                }
            }
            if (CurrentMember.PrimaryEmail == mailTo)
            {
                SendAppoinmentforOrganizer(mailTo, memberName, cid);
                //SendAppoinmentforrecruiter(mailTo, memberName);
            }
            else
                SendAppoinmentbodywithacceptrejectrecruiter(mailTo, memberName, cid);

            //MailQueueData.AddMailToMailQueue(senderid, mailTo, Subject, mailBody, "", "", null, Facade);
            MailQueueData.AddMailToMailQueue(senderid, mailTo, Subject, mailBody, " ", " ", Files, Facade);

        }
        private void SendInterviewEmailinterviwer(string mailFrom, string mailTo, string mailBody, string memberName, string cid)
        {

            string Subject = "";
            Member candid = Facade.GetMemberById(Convert.ToInt32(cid));//NEW LINE ADDED BY PRAVIN 14/March/2016
            string candidateid = "A" + cid;
            if (isNew)//if condition added by pravin khot 6/May/2016
            {
                if (ddlJobPosting.SelectedIndex > 0)
                {
                    string[] subject = ddlJobPosting.SelectedItem.Text.Split('-');
                    //***********************Code modify by pravin khot on 14/March/2016************
                    //Subject = "Notification: Interview Scheduled" + " - " + subject[1].Trim(); //OLD CODE COMMENTED BY PRAVIN 14/March/2016
                    Subject = "Notification: Interview Scheduled for" + " " + candid.FirstName + " " + candid.LastName + " [ " + candidateid + " ]- " + subject[1].Trim();//NEW LINE ADDED BY PRAVIN 14/March/2016
                    //***************************END********************************************
                }
                else
                {
                    //Subject = "Notification: Interview Scheduled" + (ddlJobPosting.SelectedIndex > 0 ? " - " + ddlJobPosting.SelectedItem.Text : string.Empty);//OLD CODE COMMENTED BY PRAVIN 14/March/2016
                    Subject = "Notification: Interview Scheduled for" + " " + candid.FirstName + " " + candid.LastName + " [ " + candidateid + " ] - " + (ddlJobPosting.SelectedIndex > 0 ? " - " + ddlJobPosting.SelectedItem.Text : string.Empty);//NEW LINE ADDED BY PRAVIN 14/March/2016
                }
            }
            else
            {
                if (ddlJobPosting.SelectedIndex > 0)
                {
                    string[] subject = ddlJobPosting.SelectedItem.Text.Split('-');
                    //***********************Code modify by pravin khot on 14/March/2016************
                    //Subject = "Notification: Interview Scheduled" + " - " + subject[1].Trim(); //OLD CODE COMMENTED BY PRAVIN 14/March/2016
                    Subject = "Notification: Interview Rescheduled for" + " " + candid.FirstName + " " + candid.LastName + " [ " + candidateid + " ]- " + subject[1].Trim();//NEW LINE ADDED BY PRAVIN 14/March/2016
                    //***************************END********************************************
                }
                else
                {
                    //Subject = "Notification: Interview Scheduled" + (ddlJobPosting.SelectedIndex > 0 ? " - " + ddlJobPosting.SelectedItem.Text : string.Empty);//OLD CODE COMMENTED BY PRAVIN 14/March/2016
                    Subject = "Notification: Interview Rescheduled for" + " " + candid.FirstName + " " + candid.LastName + " [ " + candidateid + " ] - " + (ddlJobPosting.SelectedIndex > 0 ? " - " + ddlJobPosting.SelectedItem.Text : string.Empty);//NEW LINE ADDED BY PRAVIN 14/March/2016
                }
            }

            int senderid = 0;
            string senderEmailid = (mailFrom != "") ? mailFrom : SiteSetting[DefaultSiteSetting.AdminEmail.ToString()].ToString();

            senderid = Facade.GetMemberByMemberEmail(senderEmailid).Id;

           // Files.Clear(); //Commented by pravin on date 11/Feb/2016
            int canId = Facade.getCandidateIdbyPrimaryEmail(mailTo.ToString());
            if (chkSendEmailAlerttoInterviewers.Checked)
            {
                if (canId == 0)
                {
                    SendAttachment(mailTo, cid);
                }
            }
            ////if (CurrentMember.PrimaryEmail == mailTo)
            ////{
            //SendAppoinmentforinterviewer(mailTo, memberName);
            //}
            //else
            SendAppoinmentbodywithacceptrejectinterviewer(mailTo, memberName, cid);


            //MailQueueData.AddMailToMailQueue(senderid, mailTo, Subject, mailBody, "", "", null, Facade);
            MailQueueData.AddMailToMailQueue(senderid, mailTo, Subject, mailBody, " ", " ", Files, Facade);

        }

        //add private void SendInterviewEmailinterviewerfunction(string mailFrom, string mailTo, string mailBody)
        // private void SendInterviewEmail(string mailFrom, string mailTo, string mailBody)
        //{

        //    string Subject = "";
        //    if (ddlJobPosting.SelectedIndex > 0)
        //    {
        //        string[] subject = ddlJobPosting.SelectedItem.Text.Split('-');
        //        Subject = "Notification: Interview Scheduled" + " - " + subject[1].Trim();
        //    }
        //    else
        //    {
        //        Subject = "Notification: Interview Scheduled" + (ddlJobPosting.SelectedIndex > 0 ? " - " + ddlJobPosting.SelectedItem.Text : string.Empty);
        //    }
        //    int senderid = 0;
        //    string senderEmailid = (mailFrom != "") ? mailFrom : SiteSetting[DefaultSiteSetting.AdminEmail.ToString()].ToString();

        //    senderid = Facade.GetMemberByMemberEmail(senderEmailid).Id;
        //    SendAppoinment(mailTo);

        //    //MailQueueData.AddMailToMailQueue(senderid, mailTo, Subject, mailBody, "", "", null, Facade);
        //    MailQueueData.AddMailToMailQueue(senderid, mailTo, Subject, mailBody, "", "", Files, Facade);

        //}
        private void SendAttachment(string RecipientEmail,string cid)
        {

            if (lstAttachments.Items.Count != 0)
            {
                PreviewDir = GetTempFolder();

                foreach (ListItem chkItem in lstAttachments.Items)
                {
                    chkItem.Selected = true;
                    if (chkItem.Selected)
                    {
                        string strFilePath;
                        string filePath = Path.Combine(PreviewDir, chkItem.Text);

                        
                            string DocumentType = "Interview attachments";
                            strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, _memberId, chkItem.Text, DocumentType, false);//1.
                            //_memberId = 0;
                            //strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, Convert.ToInt32(cid), chkItem.Text, DocumentType, false);//1.

                            if (File.Exists(strFilePath))
                            {
                                try
                                {
                                    if (!File.Exists(filePath))
                                        File.Copy(strFilePath, filePath);
                                }
                                catch { }
                                if (Files.ContainsKey(lstAttachments.SelectedItem.Text))
                                    Files.Remove(lstAttachments.SelectedItem.Text);
                                int canId = Facade.getCandidateIdbyPrimaryEmail(RecipientEmail.ToString());
                                if (chkSendEmailAlerttoInterviewers.Checked)
                                {
                                    if (canId == 0)
                                    {
                                        Files.Add(lstAttachments.SelectedItem.Text, filePath);
                                    }
                                }
                            }

                        if (cid != "" || cid!=string.Empty)
                        {
                            DocumentType = "Word Resume";
                            strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, Convert.ToInt32(cid), chkItem.Text, DocumentType, false);//1.
                        }


                        if (File.Exists(strFilePath))
                        {
                            try
                            {
                                if (!File.Exists(filePath))
                                    File.Copy(strFilePath, filePath);
                            }
                            catch { }
                            if (Files.ContainsKey(lstAttachments.SelectedItem.Text))
                                Files.Remove(lstAttachments.SelectedItem.Text);
                            int canId = Facade.getCandidateIdbyPrimaryEmail(RecipientEmail.ToString());
                            if (chkSendEmailAlerttoInterviewers.Checked)
                            {
                                if (canId == 0)
                                {
                                    Files.Add(lstAttachments.SelectedItem.Text, filePath);
                                }
                            }
                        }
                        //else
                        //{

                        //        try
                        //        {

                        //            File.Copy(filePath, strFilePath);
                        //        }
                        //        catch { }
                        //        if (Files.ContainsKey(lstAttachments.SelectedItem.Text))
                        //            Files.Remove(lstAttachments.SelectedItem.Text);
                        //        int canId = Facade.getCandidateIdbyPrimaryEmail(RecipientEmail.ToString());
                        //        if (chkSendEmailAlerttoInterviewers.Checked)
                        //        {
                        //            if (canId == 0)
                        //            {
                        //                Files.Add(lstAttachments.SelectedItem.Text, filePath);
                        //            }
                        //        }


                        //}

                    }
                }
            }
        }

        private void PopulateDocument(int CandidateId)
        {
            IList<MemberDocument> doc = Facade.GetAllMemberDocumentByMemberId(CandidateId);
            ddlDocument.DataSource = doc;
            ddlDocument.DataTextField = "Title";
            ddlDocument.DataValueField = "Id";
            ddlDocument.DataBind();
            if (doc != null)
            {
                if (doc.Count > 1)
                {
                    ddlDocument.Items.Insert(0, new ListItem("Select Document", "0"));
                }
            }
            else
            {
                ddlDocument.Enabled = false;
                chkAttachCandidateResume.Checked = false;
                chkAttachCandidateResume.Enabled = false;
            }


        }
        private void PopulateClientDropdowns()
        {
            int CompanyCount = Facade.Company_GetCompanyCount();
            //if (CompanyCount <= 500)  //condition commented by pravin khot on 19/Aug/2016
            //{
                ddlClientInterviewer.Visible = true;
                txtClientName.Visible = false;
                ddlClientInterviewer.Items.Clear();


                int companyStatus = (int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting);
                //**********Code commented and added by pravin khot on 8/June/2016*********
                //ddlClientInterviewer.DataSource = Facade.GetAllClientsByStatus(companyStatus);  
                ddlClientInterviewer.DataSource = Facade.GetAllClientsByStatusByCandidateId(companyStatus, Convert.ToInt32(CandidateId));
                //*************************END*************************************

                ddlClientInterviewer.DataTextField = "CompanyName";       //0.7
                ddlClientInterviewer.DataValueField = "Id";        //0.7
                ddlClientInterviewer.DataBind();

                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                {
                    ddlClientInterviewer = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientInterviewer);
                    ddlClientInterviewer.Items.Insert(0, new ListItem("Select BU", "0"));
                    lblClient.Text = "BU";
                }
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                {
                    ddlClientInterviewer = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientInterviewer);
                    ddlClientInterviewer.Items.Insert(0, new ListItem("Select Account", "0"));
                    lblClient.Text = "Account";
                }
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Vendor)
                {
                    ddlClientInterviewer = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientInterviewer);
                    ddlClientInterviewer.Items.Insert(0, new ListItem("Select Vendor", "0"));
                    lblClient.Text = "Vendor";
                }

            //}
            //else
            //{
            //    ddlClientInterviewer.Visible = false;
            //    txtClientName.Visible = true;
            //}
        }
        private void populateClientInterviers(int CompanyId)
        {
            if (ddlClientInterviewer.SelectedIndex > 0 || txtClientName.Text != string.Empty)
            {
                IList<CompanyContact> contact = Facade.GetAllCompanyContactByComapnyId(CompanyId);

                // ArrayList  contact = Facade.GetAllCompanyContactsByCompanyId(CompanyId);
                if (contact != null)
                    chkClientInterviers.DataSource = (from a in contact select new { Firstname = a.FirstName + " [" + a.Email + "]", MemberId = a.MemberId }).ToList();
                else chkClientInterviers.DataSource = null;
                chkClientInterviers.DataTextField = "FirstName";
                chkClientInterviers.DataValueField = "MemberId";
                chkClientInterviers.DataBind();
                //chkClientInterviers = (CheckBoxList)MiscUtil.RemoveScriptForDropDown(chkClientInterviers);
                if (chkClientInterviers.Items.Count > 0)
                    dvClient.Visible = true;
                else
                    dvClient.Visible = false;
            }
            else
                dvClient.Visible = false;
        }
        private DateTime getTime(DateTime date, int duration)
        {
            if (duration > 0)
            {
                duration = duration / 60;
            }
            return date.AddMinutes(Convert.ToDouble(duration));
        }
        private void PlaceUpDownArrow()
        {

            try
            {
                LinkButton lnk = (LinkButton)lsvInterviewSchedule.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        //*********Commented by pravin khot on 8/June/2016************
        //public void MessageAnswered1(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        //{
        //    if (e.Answer == ConfirmationWindow.enmAnswer.OK)
        //    {
        //        SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Employee/MailSetup.aspx", string.Empty);
        //        //Page .ClientScript .RegisterStartupScript (typeof (Page),"Open","<script>window.open('"+ url + "');</script>");
        //        ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Open", "<script>window.open('" + url + "');</script>", false);
        //    }

        //}
        //****************************END***********************************
        
        private void SaveInterviewDetails()
        {
            string ss = ddlStartTime.SelectedValue;
            if (isNew)
                InsertInterview();
            else
                UpdateInterview();
            isNew = true;
            ClearForm();
            MiscUtil.PopulateMemberListWithEmailByRole(chkInternalInterviewer, ContextConstants.ROLE_EMPLOYEE, Facade);
            chkInternalInterviewer = (CheckBoxList)MiscUtil.RemoveScriptForCheckBoxList(chkInternalInterviewer);
            chkInternalInterviewer.Items.RemoveAt(0);
            foreach (ListItem item in chkInternalInterviewer.Items)
            {
                if (item.Value == CurrentMember.Id.ToString())
                    item.Selected = true;
            }
            PlaceUpDownArrow();

        }

        private void PasteOtherInterviewers()
        {
            char[] delim = { ',' };
            string[] otherinterviewers = hfOtherInterviewers.Value.Split(delim, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in otherinterviewers)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl div = new System.Web.UI.HtmlControls.HtmlGenericControl();
                div.InnerHtml = "<span>" + s + "</span><button type='button' class='close' data-dismiss='alert'  onclick=javascript:Clicked('" + s + "')>\u00D7</button>";
                div.Attributes.Add("class", "EmailDiv");
                divContent.Controls.AddAt(0, div);
            }
        }
        //public void PopulateTimezone(ListControl ddltimezone)
        //{
        //    ddltimezone.Items.Clear();
        //    BusinessFacade.IFacade facade = new BusinessFacade.Facade();
        //    ddltimezone.DataSource = facade.GetAllTimeZone();
        //    ddltimezone.DataTextField = "Timezone";
        //    ddltimezone.DataValueField = "Timezoneid";
        //    try
        //    {
        //        ddltimezone.DataBind();
        //    }
        //    catch { }
        //    //list = RemoveScriptForDropDown(list);
        //    ddltimezone.Items.Insert(0, new ListItem("Please Select", "0"));   // Integer conversion error handled.

        //    //added by pravin khot on 25/May/2016******************
        //    int id = facade.GetTimeZoneIdBy_MemberId(CurrentMember.Id);
        //    //ddlTimezone.SelectedValue = id.ToString();
        //    //******************END*******************

        //}
        #endregion

        #region Events

        #region Page event

        protected void Page_Load(object sender, EventArgs e)
        {           
            Appuid = Guid.NewGuid();
            //*****************code add by pravin khot on 15/Dec/2015*****************
            hfOtherInterviewers.Value = "";
            hfOtherInterviewers.Value = string.Empty;        
            //*******************************End************************************

            ddlJobPosting.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlClientInterviewer.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            _mailsetting = MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id);

            if (chkSendEmailAlerttoInterviewers.Checked && IsPostBack)
            {
                divAttachments.Attributes.CssStyle.Add("display", "inline");
            }
            if (Helper.Url.SecureUrl[UrlConstants.PARAM_MSG].ToString() != "")
            {
                if (Helper.Url.SecureUrl[UrlConstants.PARAM_MSG].ToString() == "Interview Feedback")
                    upInterview.Visible = false;
            }
            if (!IsPostBack)
            {              
                LoadCandidateResume();
                if (hdnClear.Value == string.Empty)
                    UploadedFiles.Clear();
                _sessionKey = "InterviewEmailFileUpload";
            }
            if (!Page.IsPostBack)
            {
                PopulateTimezoneList();
            }
            //cmpStartDate.ValueToCompare = DateTime.Now.ToShortDateString();
            divDocument.Visible = false;

            chkAttachCandidateResume.Checked = true;
            LoadTimeDropDowns();
            MiscUtil.PopulateInterviewType(ddlInterviewType, Facade);
            MiscUtil.PopulateQuestionBankType(DdlInterviewQuestionBankType, Facade);//Code introduced by Prasanth on 16/Oct/2015
            MiscUtil.PopulateVenueMaster(ddllocation, Facade);//Code introduced by Prasanth on 16/Oct/2015

            PopulateClientDropdowns();
            if (CandidateId.Contains(','))
            {
                string[] hir1 = CandidateId.Split(',');
                foreach (string hir2 in hir1)
                {
                    PopulateDocument(Convert.ToInt32(hir2));
                }
            }
            else
            {
                PopulateDocument(Convert.ToInt32(CandidateId));
            }
            if (!Request.RawUrl.ToLower().ToString().Contains("modals/scheduleinterview"))
            {
                MemberManager manager = Facade.GetMemberManagerByMemberIdAndManagerId(Convert.ToInt32(CandidateId), CurrentMember.Id);
                if (manager != null)
                    IsAccessToDelete = true;
                else
                    IsAccessToDelete = false;
            }
            uclConfirm.MsgBoxAnswered += MessageAnswered;

            if (!IsPostBack)
            {
                divlocation.Visible = false;//ADDED BY PRAVIN KHOT ON 24/May/2016
                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                {
                    lblClient.Text = "BU";
                    lblClientInterviewer.Text = "BU Interviewers";
                }
                          
                isNew = true;

                //  MiscUtil.PopulateClients(ddlClientInterviewer , Facade);
                PopulateClientDropdowns();
                //LoadJobPosting();

                MiscUtil.PopulateMemberListWithEmailByRole(chkInternalInterviewer, ContextConstants.ROLE_EMPLOYEE, Facade);
                chkInternalInterviewer = (CheckBoxList)MiscUtil.RemoveScriptForCheckBoxList(chkInternalInterviewer);
                chkInternalInterviewer.Items.RemoveAt(0);
                foreach (ListItem item in chkInternalInterviewer.Items)
                {
                    if (item.Value == CurrentMember.Id.ToString())
                        item.Selected = true;
                }
                if (JobPostingId > 0)
                {
                    ddlJobPosting.SelectedValue = JobPostingId.ToString();
                    LoadAssociatedClient();
                    //**************code added by pravin khot on 22/Dec/2015 use for suggested panel on loading time*****************
                    LoadSuggestedPanel();
                    if (hfOtherInterviewers.Value == string.Empty && hfOtherInterviewers.Value == "")
                    {
                        LoadOtherInterviewers();
                    }
                    //***************************End********************************************************
                    int ContId = Facade.GetContactIdByJobPostingId(JobPostingId);
                    if (ContId > 0)
                    {
                        foreach (ListItem item in chkClientInterviers.Items)
                        {
                            if (item.Value == ContId.ToString())
                                item.Selected = true;
                        }
                    }
                    foreach (ListItem item in chkInternalInterviewer.Items)
                    {
                        if (item.Value == CurrentMember.Id.ToString())
                            item.Selected = true;
                    }
                }
                if (InterId > 0)
                {
                    PrepareEditView(InterId);
                    isNew = false;
                }
                PrepareView();
                txtSortColumn.Text = "btnDateTime";
                txtSortOrder.Text = "Desc";
                wdcStartDate.Text = DateTime.Now.ToShortDateString();
                PlaceUpDownArrow();
                if (Request.RawUrl.ToLower().ToString().Contains("modals/scheduleinterview"))
                {
                    divScheduleHeader.Visible = false;
                    ddlJobPosting.Enabled = false;
                    ddlClientInterviewer.Enabled = false;
                    dvListView.Visible = false;
                    Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
                    if (lbModalTitle != null)
                    {
                        lbModalTitle.Text = "Schedule Interview";
                    }

                    //*********added by pravin khot on 13/June/2016*************
                        int _companyId = Facade.GetCompanyByJobPostingId(Convert.ToInt32(JobPostingId.ToString()));
                        if (ddlClientInterviewer.Visible)
                        {
                            if (_companyId > 0)
                                ddlClientInterviewer.SelectedValue = _companyId.ToString();
                            else
                                ddlClientInterviewer.SelectedIndex = 0;
                        }                 
                        MiscUtil.PopulateJobPostingByClientIdAndManagerId(ddlJobPosting, Facade, Convert.ToInt32(ddlClientInterviewer.SelectedValue), 0);
                        ControlHelper.SelectListByValue(ddlJobPosting, JobPostingId.ToString());
                        ddlJobPosting.Enabled = false;
                    //***************************END************************************
                }
            }
            ddlJobPosting.Focus();
            if (IsPostBack)
            {
                MiscUtil.LoadAllControlState(Request, this);
                // postBack handler for AJAX AutoComplete Extender - JavaScript call: AutoCompletedClientItemSelected
                if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"] == "AutoCompleteExtender" && Request.Form["__EVENTARGUMENT"] != null)
                {
                    populateClientInterviers(Int32.Parse(hdnSelectedClientValue.Value));
                }

            }
            if (chkAllDayEvent.Checked)
            {
                divST.Style.Add("display", "none");
                divET.Style.Add("display", "none");
            }
            else
            {
                divET.Style.Add("display", "inline");
                divST.Style.Add("display", "inline");
            }
            System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvInterviewSchedule.FindControl("tdPager");

            if (Request.RawUrl.Contains("TPS360Overview.aspx"))
            {
                dvDetails.Visible = false;
                if (tdPager != null) tdPager.ColSpan = 9;
            }
            else
            {
                dvDetails.Visible = true;
                if (tdPager != null) tdPager.ColSpan = 11; // modify by pravin khot if (tdPager != null) tdPager.ColSpan = 10;
            }
            if (tdPager != null)
            {
                IsAccessToDelete = true;//ADDED BY PRAVIN KHOT ON 15/June/2016
                if (IsAccessToDelete || IsUserAdmin)
                    tdPager.ColSpan = 11; // modify by pravin khot if (tdPager != null) tdPager.ColSpan = 10;
                else
                    tdPager.ColSpan = 9;
            }
            string pagesize = "";
            pagesize = (Request.Cookies["InterviewRowPerPage"] == null ? "" : Request.Cookies["InterviewRowPerPage"].Value);
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvInterviewSchedule.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            }
            if (isMainApplication)
            {
                btnEditNotificationEmailInterviewers.Visible = false;
                btnEditNotificationEmailCandidates.Visible = false;

            }

            if (IsPostBack)
            {

                if (hfOtherInterviewers.Value != null && hfOtherInterviewers.Value != "")
                {
                    PasteOtherInterviewers();
                }
                _sessionKey = "InterviewEmailFileUpload";
            }
        }

        #endregion

        #region Button Event
        //**********************added and modify by pravin khot on 8/June/2016*********
        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                if (!_mailsetting)
                {
                    if (e.Answer == ConfirmationWindow.enmAnswer.OK)
                    {
                        SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Employee/MailSetup.aspx", string.Empty);
                        //Page .ClientScript .RegisterStartupScript (typeof (Page),"Open","<script>window.open('"+ url + "');</script>");
                        ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Open", "<script>window.open('" + url + "');</script>", false);
                    }
                }
                else
                {
                    Save();
                }
            }
        }
        //****************************END************************************
        private void Save()
        {
            try
            {
                if (chkSendEmailAlerttoInterviewers.Checked)
                {
                    if (_mailsetting)
                    {
                        SaveInterviewDetails();
                        try
                        {
                            DeleteAttachedFile();
                        }
                        catch { }
                        //MiscUtil.ShowMessage(lblMessage, "Interview has been successfully Saved.", false);
                    }
                    else
                        uclConfirm.AddMessage("Mail Server SMTP details have not been entered. Would you like to enter them now?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
                }
                else
                {
                    SaveInterviewDetails();
                    //MiscUtil.ShowMessage(lblMessage, "Interview has been successfully Saved.", false);
                }

                //TPS360.Web.UI.Helper.SecureUrl url = TPS360.Web.UI.Helper.UrlHelper.BuildSecureUrl("../ATS/InternalInterviewSchedule.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, "403", UrlConstants.PARAM_MEMBER_ID, _memberId.ToString(), UrlConstants.PARAM_MSG, "Interview has been successfully Saved.");
                //Response.Flush();
                //Helper.Url.Redirect(url);
            }
            catch
            {

            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            //added condition by pravin khot on 8/June/2016*********
            if (ddlJobPosting.SelectedIndex <= 0)
            {
                uclConfirm.AddMessage("Candidate has not been added the selected requisition. Do you want to continue ?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
            }
            else
            {
                Save();
            }
            //**********************END*********************
        }
        protected void btnAtach_Click(object sender, EventArgs e)
        {
            btnRemove.Enabled = true;
            UploadFile();
            if (ddlJobPosting.SelectedIndex > 0)
            {
                int _companyId = Facade.GetCompanyByJobPostingId(Convert.ToInt32(ddlJobPosting.SelectedValue));
                if (ddlClientInterviewer.Visible)
                {
                    if (_companyId > 0)
                        ddlClientInterviewer.SelectedValue = _companyId.ToString();
                    else
                        ddlClientInterviewer.SelectedIndex = 0;
                }
            }
            //**********Code added by pravin khot on 13/June/2016********
            int lactionid = Convert.ToInt32(ddllocation.SelectedValue);
            if (lactionid > 0)
            {
                GenericLookup genericlookup = new GenericLookup();
                genericlookup = Facade.GetGenericLookupById(lactionid);
                if (genericlookup.Description == "")
                {
                    divlocation.Visible = false;
                }
                else
                {
                    divlocation.Visible = true;
                    txtLocation.Text = genericlookup.Description.ToString();
                }

            }
            else
            {
                divlocation.Visible = false;
            }
            //*******************END**************************

            //if (JobPostingId > 0)
            //{
            //    ddlJobPosting.SelectedValue = JobPostingId.ToString();
            //    LoadAssociatedClient();
            //    int ContId = Facade.GetContactIdByJobPostingId(JobPostingId);
            //    if (ContId > 0)
            //    {
            //        foreach (ListItem item in chkClientInterviers.Items)
            //        {
            //            if (item.Value == ContId.ToString())
            //                item.Selected = true;
            //        }
            //    }
            //    foreach (ListItem item in chkInternalInterviewer.Items)
            //    {
            //        if (item.Value == CurrentMember.Id.ToString())
            //            item.Selected = true;
            //    }
            //}

        }
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            if (lstAttachments.Items.Count > 0 && lstAttachments.SelectedIndex >= 0)
            {
                PreviewDir = GetTempFolder();
                //string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                string filePath = Path.Combine(PreviewDir, lstAttachments.SelectedItem.Text);
                //string dir = base.CurrentJobPostingId.ToString();
                //string path = Path.Combine(UrlConstants.RequisitionDocumentDirectory, dir);
                //string filePath = path + "\\" + lstAttachments.SelectedItem.Text;
                try
                {
                    if (!File.Exists(filePath))
                    {

                        if (File.Exists(path(lstAttachments.SelectedItem.Text)))
                        {
                            File.Delete(path(lstAttachments.SelectedItem.Text));
                        }
                    }
                    else
                        File.Delete(filePath);
                }
                catch { }
                lstAttachments.Items.RemoveAt(lstAttachments.SelectedIndex);
                if (lstAttachments.Items.Count == 0)
                {
                    btnRemove.Enabled = false;
                }
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select file to remove.", true);
                return;
            }
        }
        //protected void wdcStartdate_valuechanged(object sender, EventArgs e)
        //{
        //    if((DateTime) wdcStartDate .Value >=DateTime .Now )
        //    wdcEndDate.Value = wdcStartDate.Value;
        //}
        #endregion

        #region ListView Events

        protected void lsvInterviewSchedule_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _IsAdmin = base.IsUserAdmin;
            try
            {
                if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
                {
                    MemberInterview memberInterview = ((ListViewDataItem)e.Item).DataItem as MemberInterview;

                    if (memberInterview != null)
                    {
                        Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
                        Label lblLocation = (Label)e.Item.FindControl("lblLocation");
                        LinkButton lblJobTitle = (LinkButton)e.Item.FindControl("lblJobTitle");
                        Label lblType = (Label)e.Item.FindControl("lblType");
                        Label lblTitle = (Label)e.Item.FindControl("lblTitle");
                        Label lblInterviewers = (Label)e.Item.FindControl("lblInterviewers");
                        Label lblNotes = (Label)e.Item.FindControl("lblNotes");
                        Label lblClient = (Label)e.Item.FindControl("lblClient");
                        HiddenField hfActivityId = (HiddenField)e.Item.FindControl("hfActivityId");
                        LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");
                        LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete");

                        LinkButton lnkbtnNoShow = (LinkButton)e.Item.FindControl("lnkbtnNoShow");
                        LinkButton lnkbtnCompleted = (LinkButton)e.Item.FindControl("lnkbtnCompleted");

                        LinkButton btnFeedback = (LinkButton)e.Item.FindControl("btnFeedback");
                        LinkButton btnInterviewerfeedbackDetail = (LinkButton)e.Item.FindControl("InterviewerfeedbackDetail");  //add by pravin khot on 22/Dec/2015 using direct view interview feedback
                        Label lblStatus = (Label)e.Item.FindControl("lblStatus");//add by pravin khot on 5/May/2016
                        Label lnlStatusNew = (Label)e.Item.FindControl("lnlStatusNew");//add by Sumit Sonawane on 1/Mar/2017

                        lblJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + memberInterview.JobPostingId.ToString() + "&FromPage=JobDetail" + "','700px','570px'); return false;");

                        if (memberInterview.AllDayEvent)
                        {
                            lblDateTime.Text = memberInterview.StartDateTime.ToShortDateString() + " All Day";
                        }
                        else
                        {
                            DateTime date = new DateTime();
                            date = getTime(memberInterview.StartDateTime, memberInterview.Duration);
                            if (date.Hour > 12 || memberInterview.StartDateTime.Hour > 12)
                                lblDateTime.Text = memberInterview.StartDateTime.ToShortDateString() + " " + memberInterview.StartDateTime.ToString("hh:mm tt") + " - " + getTime(memberInterview.StartDateTime, memberInterview.Duration).ToString("hh:mm tt");
                            else
                                lblDateTime.Text = memberInterview.StartDateTime.ToShortDateString() + " " + memberInterview.StartDateTime.ToString("hh:mm") + " - " + getTime(memberInterview.StartDateTime, memberInterview.Duration).ToString("hh:mm tt");
                        }

                        if (memberInterview.Location.Length > 13)
                        {
                            lblLocation.Text = memberInterview.Location.Substring(0, 13) + "...";
                            lblLocation.ToolTip = memberInterview.Location;
                        }
                        else
                            lblLocation.Text = memberInterview.Location;


                        SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Requisition.DIR + UrlConstants.Requisition.JOB_POSTING_PREVIEW, string.Empty, UrlConstants.PARAM_JOB_ID, memberInterview.JobPostingId.ToString());
                        if (memberInterview.JobTitle.Length > 16)
                        {
                            lblJobTitle.Text = memberInterview.JobTitle.Substring(0, 16);
                            lblJobTitle.ToolTip = memberInterview.JobTitle;
                        }
                        else
                            lblJobTitle.Text = memberInterview.JobTitle;
                        //lblJobTitle.OnClientClick = "window.open('" + url + "')";
                        lblType.Text = memberInterview.InterviewTypeName;

                        Label lblOtherInterviewers = (Label)e.Item.FindControl("lblOtherInterviewers");
                        lblOtherInterviewers.Text = memberInterview.OtherInterviewers;

                        lblInterviewers.Text = memberInterview.InterviewerName;

                        if (memberInterview.Remark.Length > 13)
                        {
                            lblNotes.Text = memberInterview.Remark.Substring(0, 13) + "...";
                            lblNotes.ToolTip = memberInterview.Remark;
                        }
                        else
                            lblNotes.Text = memberInterview.Remark;
                        lblTitle.Text = memberInterview.Title;
                        if (lblTitle.Text.Length > 18)
                        {
                            lblTitle.Text = lblTitle.Text.Substring(0, 18) + "...";
                            lblTitle.ToolTip = memberInterview.Title;
                        }
                        if (memberInterview.CompanyName.Length > 15)
                        {
                            lblClient.Text = memberInterview.CompanyName.Substring(0, 15);
                            lblClient.ToolTip = memberInterview.CompanyName;
                        }
                        else
                            lblClient.Text = memberInterview.CompanyName;

                        //if (!memberInterview.IsCancel)
                        if (memberInterview.IsCancel != 1)
                        {
                            IsAccessToDelete = true;//ADDED BY PRAVIN KHOT ON 15/June/2016

                            if (memberInterview.IsCancel == 0)
                            {
                                lblStatus.Text = "Schedule";
                            }
                            else if (memberInterview.IsCancel == 2)
                            {
                                lblStatus.Text = "Reschedule";
                            }
                            else if (memberInterview.IsCancel == 5)
                            {
                                lblStatus.Text = "No-Show";
                            }
                            else if (memberInterview.IsCancel == 6)
                            {
                                lblStatus.Text = "Completed";
                                btnEdit.Text = "Show Details";
                                btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(memberInterview.Id);
                                btnDelete.Visible = false;
                                btnFeedback.Visible = false;
                                lblStatus.Text = "Completed";
                                lnkbtnNoShow.Visible = false;
                                lnkbtnCompleted.Visible = false;
                                return;
                            }
                            //else
                            //{
                            //    lblStatus.Text = "Reschedule";
                            //}

                            btnDelete.OnClientClick = "return ConfirmDeleteSchedule('Interview Schedule')"; //modify function ConfirmDelete to ConfirmDeleteSchedule by pravin khot on 5/May/2016

                            System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvInterviewSchedule.FindControl("thAction");
                            System.Web.UI.HtmlControls.HtmlTableCell tdAction = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAction");
                            if (IsAccessToDelete || _IsAdmin)
                            {
                                thAction.Visible = true;
                                tdAction.Visible = true;
                                if (Helper.Url.SecureUrl[UrlConstants.PARAM_MSG].ToString() == "Interview Feedback")
                                {
                                    btnEdit.Visible = false;
                                    btnDelete.Visible = false;
                                    lnkbtnNoShow.Visible = false;
                                    lnkbtnCompleted.Visible = false;
                                }
                            }
                            else
                            {
                                btnEdit.Visible = false;
                                btnDelete.Visible = false;
                                lnkbtnNoShow.Visible = false;
                                lnkbtnCompleted.Visible = false;
                                //thAction.Visible = false;
                                //tdAction.Visible = false;
                            }
                            if (Request.RawUrl.Contains("TPS360Overview.aspx"))
                            {
                                thAction.Visible = false;
                                tdAction.Visible = false;
                            }
                            hfActivityId.Value = memberInterview.ActivityId.ToString();
                            btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(memberInterview.Id);
                            lnkbtnNoShow.CommandArgument = lnkbtnCompleted.CommandArgument = StringHelper.Convert(memberInterview.Id);
                            SecureUrl Url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Modals/Interviewfeedback.aspx", "", UrlConstants.PARAM_INTERVIEWID, memberInterview.Id.ToString(), UrlConstants.PARAM_INTERVIEWTITLE, memberInterview.Title);

                            btnFeedback.Attributes.Add("onclick", "EditModal('" + Url.ToString() + "','700px','570px'); return false;");

                            //**************add by pravin khot on 22/Dec/2015 using direct view interview feedback

                            SecureUrl Url1 = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Interview/InterviewerfeedbackDetail.aspx", "", UrlConstants.PARAM_INTERVIEWID, memberInterview.Id.ToString(), UrlConstants.PARAM_INTERVIEWTITLE, memberInterview.Title);

                            btnInterviewerfeedbackDetail.Attributes.Add("onclick", "EditModal('" + Url1.ToString() + "','700px','570px'); return false;");

                        }
                        else
                        {
                            btnEdit.Text = "Show Details";
                            btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(memberInterview.Id);
                            btnDelete.Visible = false;
                            btnFeedback.Visible = false;
                            lblStatus.Text = "Cancelled";
                            lnkbtnNoShow.Visible = false;
                            lnkbtnCompleted.Visible = false;
                        }

                      
                        //*****************************End***************************************************

                    }
                }
            }
            catch
            {

            }

        }
        protected void lsvInterviewSchedule_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvInterviewSchedule.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                else
                {
                    lsvInterviewSchedule.DataSource = null;
                    lsvInterviewSchedule.DataBind();
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "InterviewRowPerPage";
            }
            else
            {
                lsvInterviewSchedule.DataSource = null;
                lsvInterviewSchedule.DataBind();
            }
            PlaceUpDownArrow();
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                LinkButton btnClient = (LinkButton)lsvInterviewSchedule.FindControl("btnClient");
                if (btnClient != null) btnClient.Text = "BU";
            }
        }
        protected void lsvInterviewSchedule_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);
            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }
            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))//****Code changed by Sumit Sonawane on 1/Mar/2017 to check interview feedback for reshedule*****                    
                {
                    IList<InterviewResponse> chkIntrvwFeed = Facade.InterviewResponse_GetByInterviewId(id);
                    try
                    {
                        int chkUpdator = 0;
                        for (int i = 0; chkIntrvwFeed.Count > i; i++)
                        {
                            if (chkIntrvwFeed[i].UpdatorId == 1)
                            {
                                chkUpdator = 1;
                            }
                        }

                        if (chkUpdator == 0)
                        {
                            ClearForm();
                            PrepareEditView(id);
                            //************Code added by pravin khot on 5/May/2016*************                    
                            chkSendEmailAlerttoInterviewers.Checked = true;
                            //***********************END*****************************
                            hfInterviewId.Value = id.ToString();
                            isNew = false;
                        }
                        else
                        {
                            MiscUtil.ShowMessage(lblMessage, "Interview Feedback exists, cannot be rescheduled / cancelled.", true);
                        }
                    }
                    catch
                    {
                        if (chkIntrvwFeed == null)
                        {
                            ClearForm();
                            PrepareEditView(id);
                            //************Code added by pravin khot on 5/May/2016*************                    
                            chkSendEmailAlerttoInterviewers.Checked = true;
                            //***********************END*****************************
                            hfInterviewId.Value = id.ToString();
                            isNew = false;
                        }
                    }

                }

                else if (string.Equals(e.CommandName, "DeleteItem"))//****Code changed by Sumit Sonawane on 1/Mar/2017 to check interview feedback for reshedule*****                    
                {
                    IList<InterviewResponse> chkIntrvwFeed = Facade.InterviewResponse_GetByInterviewId(id);
                    try
                    {
                        int chkUpdator = 0;
                        for (int i = 0; chkIntrvwFeed.Count > i; i++)
                        {
                            if (chkIntrvwFeed[i].UpdatorId == 1)
                            {
                                chkUpdator = 1;
                            }
                        }

                        if (chkUpdator == 0)
                        {
                            HiddenField hfActivityId = (HiddenField)e.Item.FindControl("hfActivityId");
                            //************Code Commented by pravin khot on 4/May/2016*****************
                            //if (Facade.DeleteInterviewById(id))
                            //{
                            //    Facade.DeleteInterviewInterviewerMapByInterviewId(id);
                            //    if (Int32.Parse(hfActivityId.Value) > 0)
                            //    {
                            //        Facade.DeleteActivityResourceByActivityID(Int32.Parse(hfActivityId.Value));
                            //        Facade.DeleteActivityById(Int32.Parse(hfActivityId.Value));
                            //    }
                            //    BindInterviewList();
                            //    if (string.Compare(id.ToString(), hfInterviewId.Value) == 0)
                            //    {
                            //        ClearForm();
                            //        isNew = true;
                            //        hfInterviewId.Value = string.Empty;
                            //    }
                            //    MiscUtil.ShowMessage(lblMessage, "Interview has been successfully deleted.", false);
                            //}
                            //*********************************END***************************

                            //************Code Added by pravin khot on 4/May/2016*****************

                            Facade.CancelInterviewById(id);
                            interviewidforapp = id;
                            SendCancelInterviewNotificationToInterviewers(id);
                            //Code added by Sumit Sonawane on 1/Mar/2017 ***********
                            Interview InterviewStatusDetails = Facade.GetInterviewById(id);
                            InterviewStatusDetails.Id = id;
                            InterviewStatusDetails.IsCancel = InterviewStatusDetails.IsCancel;
                            InterviewStatusDetails.CreatorId = InterviewStatusDetails.CreatorId;
                            InterviewStatusDetails.UpdatorId = CurrentMember.Id;
                            InterviewStatusDetails.CreateDate = InterviewStatusDetails.CreateDate;
                            InterviewStatusDetails.UpdateDate = DateTime.Now;

                            InterviewStatusDetails = Facade.AddInterviewStatusDetails(InterviewStatusDetails);
                            //**************END*****************
                            BindInterviewList();
                            //if (string.Compare(id.ToString(), hfInterviewId.Value) == 0)
                            //{
                            ClearForm();
                            isNew = true;
                            hfInterviewId.Value = string.Empty;
                            //}
                            MiscUtil.ShowMessage(lblMessage, "Interview has been successfully Cancelled.", false);
                            //************************************END*****************************

                        }
                        else
                        {
                            MiscUtil.ShowMessage(lblMessage, "Interview Feedback exists, cannot be rescheduled / cancelled.", true);
                        }
                    }
                    catch
                    {
                        if (chkIntrvwFeed == null)
                        {
                            Facade.CancelInterviewById(id);
                            interviewidforapp = id;
                            SendCancelInterviewNotificationToInterviewers(id);

                            BindInterviewList();
                            //if (string.Compare(id.ToString(), hfInterviewId.Value) == 0)
                            //{
                            ClearForm();
                            isNew = true;
                            hfInterviewId.Value = string.Empty;
                            //}
                            MiscUtil.ShowMessage(lblMessage, "Interview has been successfully Cancelled.", false);
                        }
                    }
                }
                else if (string.Equals(e.CommandName, "IntrvNoShow"))//****Code changed by Sumit Sonawane on 1/Mar/2017 to check interview feedback for reshedule*****                    
                {
                    IList<InterviewResponse> chkIntrvwFeed = Facade.InterviewResponse_GetByInterviewId(id);
                    try
                    {
                        int chkUpdator = 0;
                        for (int i = 0; chkIntrvwFeed.Count > i; i++)
                        {
                            if (chkIntrvwFeed[i].UpdatorId == 1)
                            {
                                chkUpdator = 1;
                            }
                        }

                        if (chkUpdator == 0)
                        {
                            Facade.UpdateInterviewNoShow_ById(id);
                            interviewidforapp = id;
                            BindInterviewList();
                            ClearForm();
                            isNew = true;
                            hfInterviewId.Value = string.Empty;
                            //Code added by Sumit Sonawane on 1/Mar/2017 ***********
                            Interview InterviewStatusDetails = Facade.GetInterviewById(id);
                            InterviewStatusDetails.Id = id;
                            InterviewStatusDetails.IsCancel = InterviewStatusDetails.IsCancel;
                            InterviewStatusDetails.CreatorId = InterviewStatusDetails.CreatorId;
                            InterviewStatusDetails.UpdatorId = CurrentMember.Id;
                            InterviewStatusDetails.CreateDate = InterviewStatusDetails.CreateDate;
                            InterviewStatusDetails.UpdateDate = DateTime.Now;

                            InterviewStatusDetails = Facade.AddInterviewStatusDetails(InterviewStatusDetails);
                            //**************END*****************
                            MiscUtil.ShowMessage(lblMessage, "Interview has been successfully Moved to No-Show.", false);
                        }
                        else
                        {
                            MiscUtil.ShowMessage(lblMessage, "Interview Feedback exists, cannot be rescheduled / cancelled / No-Show.", true);
                        }

                    }
                    catch
                    {
                        if (chkIntrvwFeed == null)
                        {
                            Facade.UpdateInterviewNoShow_ById(id);
                            interviewidforapp = id;
                            BindInterviewList();
                            ClearForm();
                            isNew = true;
                            hfInterviewId.Value = string.Empty;
                            //Code added by Sumit Sonawane on 1/Mar/2017 ***********
                            Interview InterviewStatusDetails = Facade.GetInterviewById(id);
                            InterviewStatusDetails.Id = id;
                            InterviewStatusDetails.IsCancel = InterviewStatusDetails.IsCancel;
                            InterviewStatusDetails.CreatorId = InterviewStatusDetails.CreatorId;
                            InterviewStatusDetails.UpdatorId = CurrentMember.Id;
                            InterviewStatusDetails.CreateDate = InterviewStatusDetails.CreateDate;
                            InterviewStatusDetails.UpdateDate = DateTime.Now;

                            InterviewStatusDetails = Facade.AddInterviewStatusDetails(InterviewStatusDetails);
                            //**************END*****************
                            MiscUtil.ShowMessage(lblMessage, "Interview has been successfully Moved to No-Show.", false);
                        }
                    }
                }

                else if (string.Equals(e.CommandName, "IntrvCompleted"))//****Code changed by Sumit Sonawane on 1/Mar/2017 to check interview feedback for reshedule*****                    
                {
                    Facade.UpdateInterviewCompleted_ById(id);
                    interviewidforapp = id;
                    BindInterviewList();
                    ClearForm();
                    isNew = true;
                    hfInterviewId.Value = string.Empty;
                    //Code added by Sumit Sonawane on 1/Mar/2017 ***********
                    Interview InterviewStatusDetails = Facade.GetInterviewById(id);
                    InterviewStatusDetails.Id = id;
                    InterviewStatusDetails.IsCancel = InterviewStatusDetails.IsCancel;
                    InterviewStatusDetails.CreatorId = InterviewStatusDetails.CreatorId;
                    InterviewStatusDetails.UpdatorId = CurrentMember.Id;
                    InterviewStatusDetails.CreateDate = InterviewStatusDetails.CreateDate;
                    InterviewStatusDetails.UpdateDate = DateTime.Now;

                    InterviewStatusDetails = Facade.AddInterviewStatusDetails(InterviewStatusDetails);
                    //**************END*****************
                    MiscUtil.ShowMessage(lblMessage, "Interview has been successfully Moved to Completed.", false);
                }
            }
        }

        #endregion

        #region CancelInterveiwSchedule
        //******************Code added by pravin khot 9/May/2016*******************
        private void SendCancelInterviewNotificationToInterviewers(int interviewId)
        {
            string mailBody = "";
            string mailbody1 = "";           
            char[] delim = { ',' };
            string[] candidateIds = CandidateId.Split(delim, StringSplitOptions.RemoveEmptyEntries);

            if (isMainApplication)
                mailBody = getCancelInterviewScheduleBodyForInterviewers(interviewId);
            Interview inter = Facade.GetInterviewById(interviewId);
            if (interviewId != 0)
            {
                foreach (string cid in candidateIds)
                {
                    if (inter.MemberId == Convert.ToInt32(cid))
                    {
                        if (!isMainApplication)
                        {
                            mailBody = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(cid), interviewId, ddlInterviewType.SelectedItem.Text, 0);
                        }
                        string bodystring = txtEmailInterviewerTemplate.Text;
                        if (bodystring != "")
                        {
                            mailBody = bodystring;
                        }
                        SendCancelInterviewNotificationToCandidate(interviewId);

                        IList<InterviewInterviewerMap> interviewermapsuggested = new List<InterviewInterviewerMap>();
                        interviewermapsuggested = Facade.GetAllsuggestedInterviewersByInterviewId(interviewId);
                        if (interviewermapsuggested != null)
                        {
                            foreach (InterviewInterviewerMap map in interviewermapsuggested)
                            {
                                Member member = Facade.GetMemberById(Convert.ToInt32(map.InterviewerId));
                                string name = member.FirstName + "  " + member.LastName;

                                SendOtherInterviewEmailinterviwerCancel(interviewId, member.PrimaryEmail.ToString(), name, cid);

                            }
                        }

                        IList<InterviewInterviewerMap> interviewermap = new List<InterviewInterviewerMap>();
                        interviewermap = Facade.GetAllInterviewersByInterviewId(interviewId);
                        if (interviewermap != null)
                        {
                            foreach (InterviewInterviewerMap map in interviewermap)
                            {
                                Member member = Facade.GetMemberById(Convert.ToInt32(map.InterviewerId));

                                if (member != null)
                                {
                                    string name = member.FirstName + "  " + member.LastName;

                                    if (map.IsClient == true)
                                    {
                                        SendOtherInterviewEmailinterviwerCancel(interviewId, member.PrimaryEmail.ToString(), name, cid);
                                    }
                                    else
                                    {
                                        SendOtherInterviewEmailRecruiterCancel(interviewId, member.PrimaryEmail.ToString(), name, cid);
                                    }
                                }
                            }
                        }

                        MemberInterview memberInterview = Facade.GetMemberInterviewById(interviewId);
                        string[] otherinterviewers = memberInterview.OtherInterviewers.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string s in otherinterviewers)
                        {
                            int lens = 0;
                            lens = s.Length;
                            if (lens > 1)
                            {
                                SendOtherInterviewEmailinterviwerCancel(interviewId, s, s, cid);
                            }
                        }
                        
                        Appuid = System.Guid.NewGuid();
                    }
                }
            }
        }
        private void SendCancelInterviewNotificationToCandidate(int interviewId)
        {
            string body = "";
            if (isMainApplication)
                body = getCancelInterviewScheduleBody(interviewId);
            char[] delim = { ',' };
            string[] candidateIds = CandidateId.Split(delim, StringSplitOptions.RemoveEmptyEntries);
            foreach (string cid in candidateIds)
            {
                Interview inter = Facade.GetInterviewById(interviewId);
                txtAppointmentTitle.Text = inter.Title;
                if (inter.MemberId == Convert.ToInt32(cid))
                {
                    if (interviewId != 0)
                    {
                        if (!isMainApplication)
                        {
                            body = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(cid), interviewId, ddlInterviewType.SelectedItem.Text, 0);
                        }
                        string bodystring = txtEmailCandidateTemplate.Text;
                        if (bodystring != "")
                        {
                            body = bodystring;
                        }
                        Member member = Facade.GetMemberById(Convert.ToInt32(cid));
                       SendCancelInterviewEmail(CurrentMember.PrimaryEmail, member.PrimaryEmail, body.Replace("[InterviewerName]", member.FirstName + " " + member.LastName), member.FirstName + " " + member.LastName);
                    }
                }
            }
        }
        private string getCancelInterviewScheduleBody(int interviewId)//modify by Sumit Sonawane on 07/June/2016
        {
            if (interviewId != 0)
            {
                string InterviewsName = string.Empty;
                string[] InterviewsNames;
                foreach (ListItem item in chkClientInterviers.Items)
                {
                    if (item.Selected)
                    {
                        InterviewsNames = item.Text.Split('[');
                        InterviewsName = InterviewsName + "," + InterviewsNames[0].ToString();
                    }

                }
                InterviewsName = InterviewsName.Trim().TrimStart(',');
                string TemplateUrl = AppSettings.InterviewMailTemplateUrl;
                string mailBody = GetFileContentFromUrlCancelInterview(TemplateUrl);
                StringBuilder stringBuilder = new StringBuilder();
                string Sign = string.Empty;
                stringBuilder.Append(mailBody);
                MemberSignature signature = new MemberSignature();
                signature = Facade.GetActiveMemberSignatureByMemberId(CurrentMember.Id);
                if (signature != null)
                    Sign = signature.Signature;
                else
                    Sign = CurrentMember.FirstName + "  " + CurrentMember.LastName;

                Hashtable siteSettingTable = null;

                TPS360.BusinessFacade.Facade f = new TPS360.BusinessFacade.Facade();
                MemberExtendedInformation MailSetting = f.GetMemberExtendedInformationByMemberId(CurrentMember.Id);
                TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
                SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                stringBuilder.Replace("[STANDARDAPPLICATIONSETTINGSCOMPANYNAME]", siteSettingTable[DefaultSiteSetting.CompanyName.ToString()].ToString());
                Interview iDetail = Facade.GetInterviewById(interviewId);
                try
                {
                    stringBuilder.Replace("[Location]", iDetail.Location);
                }
                catch { }
                if (ddlClientInterviewer.SelectedIndex > 0)
                {
                    stringBuilder.Replace("[CompanyName]", ddlClientInterviewer.SelectedItem.ToString());
                }
                else
                {
                    stringBuilder.Replace("[CompanyName]", "   ");

                }
                stringBuilder.Replace("[ContactMember]", CurrentMember.FirstName + "  " + CurrentMember.LastName);
                stringBuilder.Replace("[ContactMemberMailID]", CurrentMember.PrimaryEmail);
                
                stringBuilder.Replace("[NameOfInterviewers]", (InterviewsName != string.Empty ? InterviewsName.Trim() : string.Empty));
                stringBuilder.Replace("[Signature]", Sign);

                if (iDetail.StartDateTime != DateTime.MinValue)
                {
                    wdcStartDate.Value = iDetail.StartDateTime.ToShortDateString();

                    string time = getTimeFormat(iDetail.StartDateTime);
                    ControlHelper.SelectListByText(ddlStartTime, time);
                    DateTime dtEnd = DateTime.MinValue;
                    if (iDetail.AllDayEvent)
                        dtEnd = iDetail.StartDateTime.AddDays(-1).AddSeconds(Convert.ToDouble(iDetail.Duration));
                    else
                        dtEnd = iDetail.StartDateTime.AddSeconds(Convert.ToDouble(iDetail.Duration));
                    string[] dd = dtEnd.ToShortTimeString().Split(':');
                    string Time = string.Empty;
                    if (Int32.Parse(dd[0].ToString()) > 11)
                    {
                        if (Int32.Parse(dd[0].ToString()) == 12)
                            Time = dd[0].ToString() + ":" + dd[1].ToString() + " PM";
                        else
                            Time = (Int32.Parse(dd[0].ToString()) - 12).ToString() + ":" + dd[1].ToString() + " PM";
                    }
                    else
                    {
                        if (Int32.Parse(dd[0].ToString()) == 00)
                            Time = 12 + ":" + dd[1].ToString() + " AM";
                        else
                            Time = (Int32.Parse(dd[0].ToString())).ToString() + ":" + dd[1].ToString() + " AM";
                    }
                    try
                    {
                        ControlHelper.SelectListByText(ddlEndTime, Time);
                    }
                    catch
                    {
                        ddlEndTime.SelectedValue = "";
                    }
                }
                if (iDetail.AllDayEvent == true)
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString() + " All Day");
                    stringBuilder.Replace("<tr><td><STRONG>From Time:</STRONG> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td></tr>", "");
                }
                else
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString());
                    stringBuilder.Replace("[InterviewStartTime]", ddlStartTime.SelectedItem.Text.ToString());
                    stringBuilder.Replace("[InterviewEndTime]", ddlEndTime.SelectedItem.Text.ToString());
                    string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                    if (TimeZoneLocation != "Select Timezone")
                    {
                        string TimeWithTimeZone = ddlEndTime.SelectedItem.Text.ToString() + " [ TimeZone - " + ddlTimezone.SelectedItem.Text.ToString() + " ]";
                        stringBuilder.Replace("[InterviewEndTime]", TimeWithTimeZone.ToString());
                    }
                    else
                    {
                        stringBuilder.Replace("[InterviewEndTime]", ddlEndTime.SelectedItem.Text.ToString());
                    }

                }            

                return stringBuilder.ToString();
            }

            return "";

        }
        private string GetFileContentFromUrlCancelInterview(string url)//send to candidate//modify by Sumit Sonawane on 07/June/2016
        {
            string fileContent = string.Empty;
            StringBuilder test = new StringBuilder();
            using (StreamReader reader = new StreamReader(Server.MapPath("~/MailTemplate/InterviewCancelForCandidate.htm")))
            {
                fileContent = reader.ReadToEnd();
            }

            fileContent = fileContent.Replace("{InterviewerName}", "[InterviewerName]");
            fileContent = fileContent.Replace("{InterviewDate}", "[InterviewDate]");
            fileContent = fileContent.Replace("{InterviewStartTime}", "[InterviewStartTime]");

            fileContent = fileContent.Replace("{Location}", "[Location]");

            fileContent = fileContent.Replace("{ContactMember}", "[ContactMember]");
            fileContent = fileContent.Replace("{ContactMemberMailID}", "[ContactMemberMailID]");

            fileContent = fileContent.Replace("{Signature}", "[Signature]");
            //string test = "<html><head><title></title><style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head>
            //<body><P><STRONG>Dear [InterviewerName]</STRONG>,</P><P> The below Interview has been cancelled.</P><table><tr><td>
            //<STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>Time:</STRONG> [InterviewStartTime]</td></tr><tr><td>
            //<STRONG>Venue:</STRONG> [Location]</td></tr><tr><td><STRONG>Contact:</STRONG>[ContactMember]<br><br></td></tr></table>
            //<P>Please reach out to me if you have any queries.</P></P><P>Warm Regards,</P><P>[Signature]</P></body></html>";

            //test.Append("<html><head><title></title>");
            //test.Append("<style>body, td, p{	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;	}</style></head>");
            //test.Append("<body><P><STRONG>Dear [InterviewerName]</STRONG>,</P>");
            //test.Append("<P>The below Interview has been cancelled.</P>");
            //test.Append("<table><tr>");
            //test.Append("<td width=\"150\"><STRONG>Date</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewDate]</td>");
            //test.Append("</tr><tr>");
            //test.Append("<td width=\"150\"><STRONG>Time</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewStartTime]</td>");
            //test.Append("</tr><tr>");
            //test.Append("<td width=\"150\"><STRONG>Venue</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [Location]</td>");
            //test.Append("</tr><tr/><tr/><tr>");
            //test.Append("<td width=\"150\"><STRONG>Contact</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [ContactMember]</td>");
            //test.Append("</tr><tr>");
            //test.Append("<P>Please reach out to me if you have any queries.</P></tr></table>");
            //test.Append("<P><STRONG>[Signature]</STRONG></P>");
            //test.Append("</body></html>");
    
            try
            {
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                fileContent = StringHelper.Convert(reader.ReadToEnd());
                reader.Close();
                response.Close();
            }
            catch (System.Exception)
            {

            }
            if (fileContent == "" || fileContent == null)
            {
                fileContent = test.ToString();
            }
            return fileContent;
        }
        private string getCancelInterviewScheduleBodyForInterviewers(int interviewId)//modify by Sumit Sonawane on 07/June/2016
        {
            if (interviewId != 0)
            {
                string InterviewsName = Facade.GetInterviersNameByInterviewId(interviewId);
                string TemplateUrl = AppSettings.InterviewMailTemplateUrl;
                string mailBody = GetFileContentFromUrlForCancelInterviewers(TemplateUrl);
                StringBuilder stringBuilder = new StringBuilder();
                string Sign = string.Empty;
                stringBuilder.Append(mailBody);
                MemberSignature signature = new MemberSignature();
                signature = Facade.GetActiveMemberSignatureByMemberId(CurrentMember.Id);
                if (signature != null)
                    Sign = signature.Signature;
                else
                    Sign = CurrentMember.FirstName + "  " + CurrentMember.LastName;
             
                Interview iDetail = Facade.GetInterviewById(interviewId);
                int canid = iDetail.MemberId;
                Member Cannameandaddress = Facade.GetMemberById(canid);

                JobPosting jDetail = null;
                if (iDetail.JobPostingId > 0)
                {
                    jDetail = Facade.GetJobPostingById(iDetail.JobPostingId);
                    stringBuilder.Replace("[ReqCode]", jDetail.JobPostingCode);
                    stringBuilder.Replace("[JobTitle]", jDetail.JobTitle);
                    stringBuilder.Replace("[JOBTITLE]", jDetail.JobTitle);
                    stringBuilder.Replace("[JOBCODE]", jDetail.JobPostingCode);
                }
                else
                {
                    stringBuilder.Replace("[ReqCode]", "");
                    stringBuilder.Replace("[JobTitle]", "");
                    stringBuilder.Replace("[JOBTITLE]", "");
                    stringBuilder.Replace("[JOBCODE]", "");
                }                      
              
                stringBuilder.Replace("[RecruiterName]", CurrentMember.FirstName + "  " + CurrentMember.LastName);
                stringBuilder.Replace("[InterviewTitle]", iDetail.Title);

                ddlInterviewType.SelectedValue = iDetail.TypeLookupId.ToString();
                if (ddlInterviewType.SelectedIndex > 0)
                {
                    stringBuilder.Replace("[InterviewType]", ddlInterviewType.SelectedItem.Text);
                }
                else
                {
                    stringBuilder.Replace("[InterviewType]", "");
                }
                //*************Code added by pravin khot on 24/May/2016********
                //stringBuilder.Replace("[Location]", iDetail.Location);
                try
                {
                    stringBuilder.Replace("[Location]", iDetail.Location);
                }
                catch { stringBuilder.Replace("[InterviewType]", ""); }
                //*********************END********************************
                stringBuilder.Replace("[NameOfCandidates]", Cannameandaddress.FirstName + " " + Cannameandaddress.LastName);
                stringBuilder.Replace("[ContactOfCandidates]", Cannameandaddress.CellPhone);
                stringBuilder.Replace("[CandidateEmail]", Cannameandaddress.PrimaryEmail);
                stringBuilder.Replace("[NOTE]", iDetail.Remark);
                stringBuilder.Replace("[Signature]", Sign);

                if (iDetail.StartDateTime != DateTime.MinValue)
                {
                    wdcStartDate.Value = iDetail.StartDateTime.ToShortDateString();

                    string time = getTimeFormat(iDetail.StartDateTime);
                    ControlHelper.SelectListByText(ddlStartTime, time);
                    DateTime dtEnd = DateTime.MinValue;
                    if (iDetail.AllDayEvent)
                        dtEnd = iDetail.StartDateTime.AddDays(-1).AddSeconds(Convert.ToDouble(iDetail.Duration));
                    else
                        dtEnd = iDetail.StartDateTime.AddSeconds(Convert.ToDouble(iDetail.Duration));
                    string[] dd = dtEnd.ToShortTimeString().Split(':');
                    string Time = string.Empty;
                    if (Int32.Parse(dd[0].ToString()) > 11)
                    {
                        if (Int32.Parse(dd[0].ToString()) == 12)
                            Time = dd[0].ToString() + ":" + dd[1].ToString() + " PM";
                        else
                            Time = (Int32.Parse(dd[0].ToString()) - 12).ToString() + ":" + dd[1].ToString() + " PM";
                    }
                    else
                    {
                        if (Int32.Parse(dd[0].ToString()) == 00)
                            Time = 12 + ":" + dd[1].ToString() + " AM";
                        else
                            Time = (Int32.Parse(dd[0].ToString())).ToString() + ":" + dd[1].ToString() + " AM";
                    }
                    try
                    {
                        ControlHelper.SelectListByText(ddlEndTime, Time);
                    }
                    catch
                    {
                        ddlEndTime.SelectedValue = "";
                    }
                }
                if (iDetail.AllDayEvent == true)
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString() + " All Day");
                    stringBuilder.Replace("<tr><td><STRONG>From Time:</STRONG> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td></tr>", "");
                }
                else
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString());
                    stringBuilder.Replace("[InterviewStartTime]", ddlStartTime.SelectedItem.Text.ToString());
                    string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                    if (TimeZoneLocation != "Select Timezone")
                    {
                        string TimeWithTimeZone = ddlEndTime.SelectedItem.Text.ToString() + " [ TimeZone - " + ddlTimezone.SelectedItem.Text.ToString() + " ]";
                        stringBuilder.Replace("[InterviewEndTime]", TimeWithTimeZone.ToString());
                    }
                    else
                    {
                        stringBuilder.Replace("[InterviewEndTime]", ddlEndTime.SelectedItem.Text.ToString());
                    }

                }            

                return stringBuilder.ToString();
            }
            return "";
        }
        private string getCancelInterviewScheduleBodyForRecruiter(int interviewId)//modify by Sumit Sonawane on 07/June/2016
        {
            if (interviewId != 0)
            {
                string InterviewsName = Facade.GetInterviersNameByInterviewId(interviewId);
                string TemplateUrl = AppSettings.InterviewMailTemplateUrl;
                string mailBody = GetFileContentFromUrlForCancelRecruiter(TemplateUrl);
                StringBuilder stringBuilder = new StringBuilder();
                string Sign = string.Empty;
                stringBuilder.Append(mailBody);
                MemberSignature signature = new MemberSignature();
                signature = Facade.GetActiveMemberSignatureByMemberId(CurrentMember.Id);
                if (signature != null)
                    Sign = signature.Signature;
                else
                    Sign = CurrentMember.FirstName + "  " + CurrentMember.LastName;
               
                Interview iDetail = Facade.GetInterviewById(interviewId);
                int canid = iDetail.MemberId;
                Member Cannameandaddress = Facade.GetMemberById(canid);


                JobPosting jDetail = null;
                if (iDetail.JobPostingId > 0)
                {
                    jDetail = Facade.GetJobPostingById(iDetail.JobPostingId);
                    stringBuilder.Replace("[ReqCode]", jDetail.JobPostingCode);
                    stringBuilder.Replace("[JobTitle]", jDetail.JobTitle);
                    stringBuilder.Replace("[JOBTITLE]", jDetail.JobTitle);
                    stringBuilder.Replace("[JOBCODE]", jDetail.JobPostingCode);
                }
                else
                {
                    stringBuilder.Replace("[ReqCode]", "");
                    stringBuilder.Replace("[JobTitle]", "");
                    stringBuilder.Replace("[JOBTITLE]", "");
                    stringBuilder.Replace("[JOBCODE]", "");
                }        

                stringBuilder.Replace("[RecruiterName]", CurrentMember.FirstName + "  " + CurrentMember.LastName);
                stringBuilder.Replace("[InterviewTitle]", iDetail.Title);
                ddlInterviewType.SelectedValue = iDetail.TypeLookupId.ToString();
                if (ddlInterviewType.SelectedIndex > 0)
                {
                    stringBuilder.Replace("[InterviewType]", ddlInterviewType.SelectedItem.Text);
                }
                else
                {
                    stringBuilder.Replace("[InterviewType]", "");
                }
                //*************Code added by pravin khot on 24/May/2016********
                //stringBuilder.Replace("[Location]", iDetail.Location);

                try
                {
                    stringBuilder.Replace("[Location]", iDetail.Location);
                }
                catch { stringBuilder.Replace("[InterviewType]", ""); }
                //*********************END********************************
                stringBuilder.Replace("[NameOfCandidates]", Cannameandaddress.FirstName + " " + Cannameandaddress.LastName);
                stringBuilder.Replace("[ContactOfCandidates]", Cannameandaddress.CellPhone);
                stringBuilder.Replace("[CandidateEmail]", Cannameandaddress.PrimaryEmail);
                stringBuilder.Replace("[NOTE]", iDetail.Remark);
                stringBuilder.Replace("[Signature]", Sign);

                if (iDetail.StartDateTime != DateTime.MinValue)
                {
                    wdcStartDate.Value = iDetail.StartDateTime.ToShortDateString();

                    string time = getTimeFormat(iDetail.StartDateTime);
                    ControlHelper.SelectListByText(ddlStartTime, time);
                    DateTime dtEnd = DateTime.MinValue;
                    if (iDetail.AllDayEvent)
                        dtEnd = iDetail.StartDateTime.AddDays(-1).AddSeconds(Convert.ToDouble(iDetail.Duration));
                    else
                        dtEnd = iDetail.StartDateTime.AddSeconds(Convert.ToDouble(iDetail.Duration));
                    string[] dd = dtEnd.ToShortTimeString().Split(':');
                    string Time = string.Empty;
                    if (Int32.Parse(dd[0].ToString()) > 11)
                    {
                        if (Int32.Parse(dd[0].ToString()) == 12)
                            Time = dd[0].ToString() + ":" + dd[1].ToString() + " PM";
                        else
                            Time = (Int32.Parse(dd[0].ToString()) - 12).ToString() + ":" + dd[1].ToString() + " PM";
                    }
                    else
                    {
                        if (Int32.Parse(dd[0].ToString()) == 00)
                            Time = 12 + ":" + dd[1].ToString() + " AM";
                        else
                            Time = (Int32.Parse(dd[0].ToString())).ToString() + ":" + dd[1].ToString() + " AM";
                    }
                    try
                    {
                        ControlHelper.SelectListByText(ddlEndTime, Time);
                    }
                    catch
                    {
                        ddlEndTime.SelectedValue = "";
                    }
                }
                if (iDetail.AllDayEvent == true)
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString() + " All Day");
                    stringBuilder.Replace("<tr><td><STRONG>From Time:</STRONG> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td></tr>", "");
                }
                else
                {
                    stringBuilder.Replace("[InterviewDate]", wdcStartDate.Text.ToString());
                    stringBuilder.Replace("[InterviewStartTime]", ddlStartTime.SelectedItem.Text.ToString());
                    string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                    if (TimeZoneLocation != "Select Timezone")
                    {
                        string TimeWithTimeZone = ddlEndTime.SelectedItem.Text.ToString() + " [ TimeZone - " + ddlTimezone.SelectedItem.Text.ToString() + " ]";
                        stringBuilder.Replace("[InterviewEndTime]", TimeWithTimeZone.ToString());
                    }
                    else
                    {
                        stringBuilder.Replace("[InterviewEndTime]", ddlEndTime.SelectedItem.Text.ToString());
                    }

                }            
                return stringBuilder.ToString();
            }
            return "";
        }
        private string GetFileContentFromUrlForCancelInterviewers(string url)//send to Interviewers//modify by Sumit Sonawane on 07/June/2016
        {
           
        //    //string test = "<head><title></title><style>body, td, p{	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;	
        ////}</style></head><body><P><STRONG>Dear [InterviewerName]</STRONG>,</P><P>The Interview has been cancelled</P><table><tr><td>
        //    //<STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>From Time:</STRONG> [InterviewStartTime]<STRONG> to
        //    //</STRONG> [InterviewEndTime]</td></tr><tr><td><STRONG>Interview Mode:</STRONG> [InterviewType]</td></tr><tr><td><STRONG>Interview Location:</STRONG> [Location]</td>
        //    //</tr><tr><td><STRONG>Interviewers:</STRONG> [NameOfInterviewers]</td></tr><tr><td><STRONG>Candidate Name:</STRONG> [CandidateName]
        //    //</td></tr><tr><td><STRONG>Candidate Contact:</STRONG> [CandidateContact]</td></tr></table><table><tr><td>
        //    //<STRONG>Notes :</STRONG></td></tr><tr><td>[NOTE]</td></tr></table><P>Thank you for your time</P>
        //    //<table><tr><td>Regards,</td></tr><tr><td>[RecruiterName]</td></tr></table>
        //    //<P><STRONG>[Signature]</STRONG></P></body></html>";

        //    StringBuilder test = new StringBuilder();
        //    test.Append("<head><title></title>");
        //    test.Append("<style>body, td, p{	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;	}</style></head>");
        //    test.Append("<body><P><STRONG>Dear [InterviewerName]</STRONG>,</P>");
        //    test.Append("<P>The Interview has been cancelled.</P>");
        //    test.Append("<table><tr>");
        //    test.Append("<td width=\"150\"><STRONG>Date</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewDate]</td>");
        //    test.Append("</tr><tr>");
        //    test.Append("<td width=\"150\"><STRONG>From Time</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td>");
        //    test.Append("</tr><tr>");
        //    test.Append("<td width=\"150\"><STRONG>Interview Mode</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewType]</td>");
        //    test.Append("</tr><tr>");
        //    test.Append("<td width=\"150\"><STRONG>Interview Location</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [Location]</td>");
        //    test.Append("</tr><tr/><tr/><tr>");
        //    test.Append("<td width=\"150\"><STRONG>Interviewers</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NameOfInterviewers]</td>");
        //    test.Append("</tr><tr>");
        //    test.Append("<td width=\"150\"><STRONG>Candidate Name</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [CandidateName]</td>");
        //    test.Append("</tr><tr>");
        //    test.Append("<td width=\"150\"><STRONG>Candidate Contact</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [CandidateContact]</td>");
          
        //    //if (intervw.Remark != "" || intervw.Remark != string.Empty)
        //    //{
        //        test.Append("</tr><tr>");
        //        test.Append("<td width=\"150\"><STRONG>Notes</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NOTE]</td>");
        //    //}

        //    test.Append("</tr></table>");
        //    test.Append("<P>Thank you for your time.</P>");
        //   // test.Append("<table><tr><td>Regards,</td></tr><tr><td>[RecruiterName]</td></tr></table>");
        //    test.Append("<P><STRONG>[Signature]</STRONG></P>");
        //    test.Append("</body></html>");
            StringBuilder test = new StringBuilder();
            string fileContent = string.Empty;

            using (StreamReader reader = new StreamReader(Server.MapPath("~/MailTemplate/InterviewCancelForInterviewer.htm")))
            {
                fileContent = reader.ReadToEnd();
            }

            fileContent = fileContent.Replace("{InterviewerName}", "[InterviewerName]");

            fileContent = fileContent.Replace("{InterviewDate}", "[InterviewDate]");
            fileContent = fileContent.Replace("{InterviewStartTime}", "[InterviewStartTime]");
            fileContent = fileContent.Replace("{InterviewEndTime}", "[InterviewEndTime]");
            fileContent = fileContent.Replace("{InterviewType}", "[InterviewType]");
            fileContent = fileContent.Replace("{Location}", "[Location]");
            fileContent = fileContent.Replace("{NameOfInterviewers}", "[NameOfInterviewers]");
            fileContent = fileContent.Replace("{NameOfOtherInterviewers}", "[NameOfOtherInterviewers]");
            fileContent = fileContent.Replace("{NameOfCandidates}", "[NameOfCandidates]");
            fileContent = fileContent.Replace("{ContactOfCandidates}", "[ContactOfCandidates]");


            fileContent = fileContent.Replace("{Signature}", "[Signature]");
          

       
            try
            {
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                fileContent = StringHelper.Convert(reader.ReadToEnd());
                reader.Close();
                response.Close();
            }
            catch (System.Exception)
            {

            }
            if (fileContent == "" || fileContent == null)
            {
                fileContent = test.ToString();
            }
            return fileContent;
           
            //string test = "<head><title></title><style>body, td, p{	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;	}</style></head><body><P><STRONG>Dear [InterviewerName]</STRONG>,</P><P>The Interview has been cancelled</P><table><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr><tr><td><STRONG>From Time:</STRONG> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td></tr><tr><td><STRONG>Interview Mode:</STRONG> [InterviewType]</td></tr><tr><td><STRONG>Interview Location:</STRONG> [Location]</td></tr><tr><td><STRONG>Interviewers:</STRONG> [NameOfInterviewers]</td></tr><tr><td><STRONG>Candidate Name:</STRONG> [CandidateName]</td></tr><tr><td><STRONG>Candidate Contact:</STRONG> [CandidateContact]</td></tr></table><table><tr><td><STRONG>Notes :</STRONG></td></tr><tr><td>[NOTE]</td></tr></table><P>Thank you for your time</P><table><tr><td>Regards,</td></tr><tr><td>[RecruiterName]</td></tr></table><P><STRONG>[Signature]</STRONG></P></body></html>";
           
        }
        private string GetFileContentFromUrlForCancelRecruiter(string url)//send to Recruiter//modify by Sumit Sonawane on 07/June/2016
        {
           //string test = "<head><title></title><style>body, td, p{	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;	
        //}</style></head><body><P><STRONG>Dear [InterviewerName]</STRONG>,</P><P>An Interview has been cancelled and following are the details.
            //</P><table><tr><td><STRONG>Requisition :</STRONG> [JOBTITLE] | [JOBCODE]</td></tr><tr><td><STRONG>Date:</STRONG> [InterviewDate]</td></tr>
            //<tr><td><STRONG>From Time:</STRONG> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td></tr>
            //<tr><td><STRONG>Interview Mode:</STRONG> [InterviewType]</td></tr>
            //<tr><td><STRONG>Interview Location:</STRONG> [Location]</td></tr><tr><td><STRONG>Interviewers:</STRONG> [NameOfInterviewers]</td></tr>
            //<tr><td><STRONG>Candidate Name:</STRONG> [CandidateName]</td></tr><tr><td><STRONG>Candidate Contact:</STRONG> [CandidateContact]</td></tr></table><br><br><table><tr><td>Regards,</td></tr><
            //tr><td>[RecruiterName]</td></tr></table><P><STRONG>[Signature]</STRONG></P></body></html>";
 
            string fileContent = string.Empty;
            StringBuilder test = new StringBuilder();

            using (StreamReader reader = new StreamReader(Server.MapPath("~/MailTemplate/InterviewCancelForRecruiter.htm")))
                {
                    fileContent = reader.ReadToEnd();
                }

                fileContent = fileContent.Replace("{InterviewerName}", "[InterviewerName]");
                fileContent = fileContent.Replace("{JobTitle}", "[JobTitle]");
                fileContent = fileContent.Replace("{ReqCode}", "[ReqCode]");
                fileContent = fileContent.Replace("{InterviewDate}", "[InterviewDate]");
                fileContent = fileContent.Replace("{InterviewStartTime}", "[InterviewStartTime]");
                fileContent = fileContent.Replace("{InterviewEndTime}", "[InterviewEndTime]");
                fileContent = fileContent.Replace("{InterviewType}", "[InterviewType]");
                fileContent = fileContent.Replace("{Location}", "[Location]");
                fileContent = fileContent.Replace("{NameOfInterviewers}", "[NameOfInterviewers]");
                fileContent = fileContent.Replace("{NameOfOtherInterviewers}", "[NameOfOtherInterviewers]");
                fileContent = fileContent.Replace("{NameOfCandidates}", "[NameOfCandidates]");
                fileContent = fileContent.Replace("{ContactOfCandidates}", "[ContactOfCandidates]");

               
                fileContent = fileContent.Replace("{Signature}", "[Signature]");
            //test.Append("<head><title></title>");
            //test.Append("<style>body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}</style></head>");
            //test.Append("<body><P><STRONG>Dear [InterviewerName],</STRONG></P>");
            //test.Append("<P>An Interview has been cancelled and following are the details.</P>");
            //test.Append("<table><tr>");
            //test.Append("<td width=\"150\"><STRONG>Requisition</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [JOBTITLE] | [JOBCODE]</td>");
            //test.Append("</tr><tr>");
            //test.Append("<td width=\"150\"><STRONG>Date</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewDate]</td>");
            //test.Append("</tr><tr>");
            //test.Append("<td width=\"150\"><STRONG>From Time</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewStartTime]<STRONG> to </STRONG> [InterviewEndTime]</td>");
            //test.Append("</tr><tr>");
            //test.Append("<td width=\"150\"><STRONG>Interview Mode</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [InterviewType]</td>");
            //test.Append("</tr><tr>");
            //test.Append("<td width=\"150\"><STRONG>Interview Location</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [Location]</td>");
            //test.Append("</tr><tr/><tr/><tr>");
            //test.Append("<td width=\"150\"><STRONG>Interviewers</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NameOfInterviewers]</td>");
            //test.Append("</tr><tr>");
            //test.Append("<td width=\"150\"><STRONG>Candidate Name</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [NameOfCandidates] - [CandidateEmail]</td>");
            //test.Append("</tr><tr>");
            //test.Append("<td width=\"150\"><STRONG>Candidate Contact</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> [ContactOfCandidates]</td>");
          
            //test.Append("</tr></table>");
            //test.Append("<br><br><STRONG>[Signature]</STRONG>");
            //test.Append("</body></html>");
            try
            {
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                fileContent = StringHelper.Convert(reader.ReadToEnd());
                reader.Close();
                response.Close();
            }
            catch (System.Exception)
            {

            }
            if (fileContent == "" || fileContent == null)
            {
                fileContent = test.ToString();
            }
            return fileContent;
        }
        private void SendOtherInterviewEmailinterviwerCancel(int interviewId, string InterviewerEmailId, string InterviewerName, string candidateId)
        {
            string OtherinterviewersMailbody = "";/////////////////////////////////////////////////////Sumit////////////////
            Member candid = Facade.GetMemberById(Convert.ToInt32(candidateId));
            string JobTitlle = "";
            MemberInterview memberInterview = Facade.GetMemberInterviewById(interviewId);
            if (memberInterview != null)
            {
                ControlHelper.SelectListByValue(ddlJobPosting, memberInterview.JobPostingId.ToString());
                ControlHelper.SelectListByValue(ddlDocument, memberInterview.InterviewDocumentId.ToString());
                txtAppointmentTitle.Text = MiscUtil.RemoveScript(memberInterview.Title, string.Empty);
                ddlInterviewType.SelectedValue = memberInterview.TypeLookupId.ToString();
                DdlInterviewQuestionBankType.SelectedValue = memberInterview.QuestionBankTypeLookupId.ToString();
            }
            if (ddlJobPosting.SelectedIndex > 0)
            {
                JobTitlle = ddlJobPosting.SelectedItem.ToString();
            }
            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Interview/InterviewerFeedback.aspx", string.Empty, UrlConstants.PARAM_INTERVIEWID, interviewId + "~" + InterviewerEmailId + "~" + candid.FirstName + " " + candid.LastName + "~" + JobTitlle + "~" + wdcStartDate.Text + " " + ddlStartTime.Text + "~" + CandidateId + "~" + CurrentMember.PrimaryEmail);

            if (!isMainApplication)
            {
                OtherinterviewersMailbody = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(candidateId), interviewId, ddlInterviewType.SelectedItem.Text, 0);
            }
            else
            {
                OtherinterviewersMailbody = getCancelInterviewScheduleBodyForInterviewers(interviewId);
            }

            OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[CandidateName]", candid.FirstName + " " + candid.LastName);
            OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[CandidateContact]", candid.CellPhone);

            OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[FeedbackLink]", url.ToString());
            OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[InterviewerName]", InterviewerName.ToString());/////////////Sumit Sonawane

            SendInterviewEmailinterviwerCancel(CurrentMember.PrimaryEmail, InterviewerEmailId, OtherinterviewersMailbody.Replace("[NameOfInterviewers]", InterviewerName), InterviewerEmailId, candidateId);

            InsertInterviewResponse(interviewId, InterviewerEmailId, Convert.ToInt32(DdlInterviewQuestionBankType.SelectedValue));
        }
        private void SendOtherInterviewEmailRecruiterCancel(int interviewId, string InterviewerEmailId, string InterviewerName, string candidateId)
        {
            string OtherinterviewersMailbody = "";
            Member candid = Facade.GetMemberById(Convert.ToInt32(candidateId));

              MemberInterview memberInterview = Facade.GetMemberInterviewById(interviewId);
              if (memberInterview != null)
              {
                  ControlHelper.SelectListByValue(ddlJobPosting, memberInterview.JobPostingId.ToString());
                  ControlHelper.SelectListByValue(ddlDocument, memberInterview.InterviewDocumentId.ToString());
                  txtAppointmentTitle.Text = MiscUtil.RemoveScript(memberInterview.Title, string.Empty);
                  ddlInterviewType.SelectedValue = memberInterview.TypeLookupId.ToString();
                  DdlInterviewQuestionBankType.SelectedValue = memberInterview.QuestionBankTypeLookupId.ToString();
              }

            string JobTitlle = "";
            if (ddlJobPosting.SelectedIndex > 0)
            {
                JobTitlle = ddlJobPosting.SelectedItem.ToString();
            }
            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Interview/InterviewerFeedback.aspx", string.Empty, UrlConstants.PARAM_INTERVIEWID, interviewId + "~" + InterviewerEmailId + "~" + candid.FirstName + " " + candid.LastName + "~" + JobTitlle + "~" + wdcStartDate.Text + " " + ddlStartTime.Text + "~" + CandidateId + "~" + CurrentMember.PrimaryEmail);

            if (!isMainApplication)
            {
                OtherinterviewersMailbody = Facade.Interview_GetInterviewTemplate(Convert.ToInt32(candidateId), interviewId, ddlInterviewType.SelectedItem.Text, 0);
            }
            else
            {
                OtherinterviewersMailbody = getCancelInterviewScheduleBodyForRecruiter(interviewId);
            }

            OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[CandidateName]", candid.FirstName + " " + candid.LastName);
            OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[CandidateContact]", candid.CellPhone);

            OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[FeedbackLink]", url.ToString());
            OtherinterviewersMailbody = OtherinterviewersMailbody.Replace("[InterviewerName]", InterviewerName.ToString());

            SendInterviewEmailinterviwerCancel(CurrentMember.PrimaryEmail, InterviewerEmailId, OtherinterviewersMailbody.Replace("[NameOfInterviewers]", InterviewerName), InterviewerEmailId, candidateId);

            InsertInterviewResponse(interviewId, InterviewerEmailId, Convert.ToInt32(DdlInterviewQuestionBankType.SelectedValue));
        }
        private void SendInterviewEmailinterviwerCancel(string mailFrom, string mailTo, string mailBody, string memberName, string cid)
        {
            string Subject = "";
            Member candid = Facade.GetMemberById(Convert.ToInt32(cid));
            string candidateid = "A" + cid;

            if (ddlJobPosting.SelectedIndex > 0)
            {
                string[] subject = ddlJobPosting.SelectedItem.Text.Split('-');
                Subject = "Notification: Interview Cancel for" + " " + candid.FirstName + " " + candid.LastName + " [ " + candidateid + " ]- " + subject[1].Trim();//NEW LINE ADDED BY PRAVIN 14/March/2016
            }
            else
            {
                Subject = "Notification: Interview Cancel for" + " " + candid.FirstName + " " + candid.LastName + " [ " + candidateid + " ] - " + (ddlJobPosting.SelectedIndex > 0 ? " - " + ddlJobPosting.SelectedItem.Text : string.Empty);//NEW LINE ADDED BY PRAVIN 14/March/2016
            }

            int senderid = 0;
            string senderEmailid = (mailFrom != "") ? mailFrom : SiteSetting[DefaultSiteSetting.AdminEmail.ToString()].ToString();

            senderid = Facade.GetMemberByMemberEmail(senderEmailid).Id;
            int canId = Facade.getCandidateIdbyPrimaryEmail(mailTo.ToString());
            if (chkSendEmailAlerttoInterviewers.Checked)
            {
                if (canId == 0)
                {
                    SendAttachment(mailTo, cid);
                }
            }
            SendCancelAppoinmentforOrganizer(mailTo, memberName, cid);
            MailQueueData.AddMailToMailQueue(senderid, mailTo, Subject, mailBody, " ", " ", Files, Facade);
        }
        private void SendCancelInterviewEmail(string mailFrom, string mailTo, string mailBody, string memberName)
        {
            string Subject = "";
           
            Hashtable siteSettingTable = null;
            TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
            SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
            string companyname = siteSettingTable[DefaultSiteSetting.CompanyName.ToString()].ToString();         

            Subject = "Notification: Interview Cancelled at " + companyname + "";         

            int senderid = 0;
            string senderEmailid = (mailFrom != "") ? mailFrom : SiteSetting[DefaultSiteSetting.AdminEmail.ToString()].ToString();

            senderid = Facade.GetMemberByMemberEmail(senderEmailid).Id;

            Files.Clear();
            int canId = Facade.getCandidateIdbyPrimaryEmail(mailTo.ToString());
            if (chkSendEmailAlerttoInterviewers.Checked)
            {
                if (canId == 0)
                {
                    SendAttachment(mailTo, "");
                }
            }
            SendCancelAppoinmentToCandidate(mailTo, memberName);
            MailQueueData.AddMailToMailQueue(senderid, mailTo, Subject, mailBody, " ", " ", Files, Facade);

        }
        private void SendCancelAppoinmentToCandidate(string RecipientEmail, string memberName)
        {
            string calJobPostingCode = string.Empty;
            string calJobTitle = string.Empty;
            string calcanid = string.Empty;
            string calcanname = string.Empty;
            string caltitle = string.Empty;

            string receipentmail1 = string.Empty;

            Interview interview1 = Facade.GetInterviewById(interviewidforapp);
            string Remainder1 = getappoinmentbody(interview1.Id);

            Interview iDetail = Facade.GetInterviewById(interview1.Id);
            int canid = iDetail.MemberId;
            Member Cannameandaddress = Facade.GetMemberById(canid);
          
                //JobPosting jDetail = Facade.GetJobPostingById(iDetail.JobPostingId);
                //calJobPostingCode = jDetail.JobPostingCode;
                //calJobTitle = jDetail.JobTitle;
            
            calcanid = Cannameandaddress.Id.ToString();
            calcanname = Cannameandaddress.FirstName + Cannameandaddress.LastName;

            caltitle = "Interview Cancel- " + interview1.Title + " _ " + iDetail.JobPostingId + " _ " + calcanid + " - " + calcanname;
            string InterviewsName = string.Empty;
            string internalrecruiter = string.Empty;
                    
            internalrecruiter = internalrecruiter.Trim().TrimStart(',');
            internalrecruiter = internalrecruiter.Replace(']', ' ');
          
            InterviewsName = InterviewsName.Trim().TrimStart(',');

            InterviewsName = InterviewsName.Replace(']', ' ');
            receipentmail1 = internalrecruiter + "," + InterviewsName;
            receipentmail1 = receipentmail1.Replace(",,", ",");

            receipentmail1 = receipentmail1.TrimStart(',');
            receipentmail1 = receipentmail1.Trim();
            receipentmail1 = receipentmail1.TrimEnd(',');
            try
            {
                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, CurrentMember.Id, "Appt.ics", "Other", false);
                string fileName = "";
                fileName += txtAppointmentTitle.Text;
                fileName += ".ics";

                string dir = GetTempFolder();

                string filePath = Path.Combine(dir, fileName);

                StringBuilder writer = new StringBuilder();
                string startDate = Convert.ToDateTime(wdcStartDate.Text).ToShortDateString();
                string startTime = ddlStartTime.SelectedItem.Text;
                string endDateTime = ddlEndTime.SelectedItem.Text;
                //string attendee = getAttendies();
                int intDuration = 0;
                DateTime dtStartDateTime = GetDateAndDuration(out intDuration); //Convert.ToDateTime(startDate + " " + startTime);
                DateTime dtEndDateTime = Convert.ToDateTime(startDate + " " + endDateTime);
                writer.AppendLine("BEGIN:VCALENDAR");
                writer.AppendLine("PRODID:-//Microsoft Corporation//Outlook MIMEDIR//EN");
                //writer.AppendLine("PRODID:-//Google Inc//Google Calendar 70.9054//EN");
                //writer.AppendLine("CALSCALE:GREGORIAN");

                writer.AppendLine("VERSION:2.0");
                writer.AppendLine("METHOD:CANCEL");
                writer.AppendLine("BEGIN:VEVENT");
                //writer.AppendLine(string.Format("X-WR-TIMEZONE:" + ddlTimezone.SelectedItem.Text));//added by pravin khot on 25/May/2016

                string tt = string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime));
                string ttt = string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime));
                int ix = tt.IndexOf(":");
                int ixx = ttt.IndexOf(":");

                tt = tt.Substring(ix + 1);
                ttt = ttt.Substring(ixx + 1);
                string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                if (TimeZoneLocation != "Select Timezone")
                {
                    writer.AppendLine(string.Format("DTSTART;TZID=" + TimeZoneLocation + ":" + tt + ""));
                    writer.AppendLine(string.Format("DTEND;TZID=" + TimeZoneLocation + ":" + ttt + ""));
                }
                else
                {
                    writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime)));
                    writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                    writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                }

                //string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", "TZID:'" + TimeZoneLocation + "'", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime))); //TZID=US-Pacific:20140606T180000
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", "TZID:'" + TimeZoneLocation + "'", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));

                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime)));
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                //writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //if (!IsOrganizer)
                //{
                writer.AppendLine("ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:" + RecipientEmail);
                writer.AppendLine("ORGANIZER;CN= " + CurrentUserName + ":mailto:" + CurrentMember.PrimaryEmail);
                //}
                //*********Code added by pravin khot on 4/May/2016****************
                if (iDetail.IcsFileUIDCode == "" || iDetail.IcsFileUIDCode == null)
                {
                    Facade.UpdateInterviewIcsCodeById(interview1.Id, Appuid.ToString());
                    writer.AppendLine("UID:" + Appuid.ToString());

                }
                else
                {
                    writer.AppendLine("UID:" + iDetail.IcsFileUIDCode.ToString());
                }
                //******************************END*************************************

                writer.AppendLine("X-ALT-DESC;FMTTYPE=text/html:" + Remainder1 + "");
                writer.AppendLine("SUMMARY:" + MiscUtil.RemoveScript(caltitle));

              
                writer.AppendLine("LOCATION:" + MiscUtil.RemoveScript(txtLocation.Text));
             
               
                writer.AppendLine("STATUS:CANCELLED");
                writer.AppendLine("DESCRIPTION:This Interview has been canceled." + MiscUtil.RemoveScript(caltitle));
                writer.AppendLine("SUMMARY:" + MiscUtil.RemoveScript(caltitle));
                writer.AppendLine("CLASS:PUBLIC");
                writer.AppendLine("END:VEVENT");
                writer.AppendLine("END:VCALENDAR");

                UTF8Encoding enc = new UTF8Encoding();
                byte[] arrBytData = enc.GetBytes(writer.ToString());
                File.WriteAllBytes(strFilePath, arrBytData);

                bool isFileExist = File.Exists(strFilePath);
                if (isFileExist)
                {
                    try
                    {
                        if (!File.Exists(filePath))
                        {
                            File.Copy(strFilePath, filePath);
                        }
                    }
                    catch (FileNotFoundException ex)
                    {

                    }
                    if (strFilePath != fileName)
                    {
                        if (Files.ContainsKey(fileName))
                            Files.Remove(fileName);
                        Files.Add(fileName, filePath);
                    }
                    else
                    {
                        //return   "(File not found)";
                    }
                }

            }
            catch (System.Exception ex)
            {
                string test = ex.Message;
            }

        }
        private void SendCancelAppoinmentforOrganizer(string RecipientEmail, string memberName, string CID)
        {
            string calJobPostingCode = string.Empty;
            string calJobTitle = string.Empty;
            string calcanid = string.Empty;
            string calcanname = string.Empty;
            string caltitle = string.Empty;

            string InterviewsName = string.Empty;
            string[] InterviewsNames;
            foreach (ListItem item in chkClientInterviers.Items)
            {
                if (item.Selected)
                {
                    InterviewsNames = item.Text.Split('[');
                    InterviewsName = InterviewsName + "," + InterviewsNames[0].ToString();
                }
            }
            InterviewsName = InterviewsName.Trim().TrimStart(',');
            Interview interview1 = Facade.GetInterviewById(interviewidforapp);
            string Remainder1 = getappoinmentbodyrecruiter(interview1.Id, CID);

            Interview iDetail = Facade.GetInterviewById(interview1.Id);
            int canid = iDetail.MemberId;
            Member Cannameandaddress1 = Facade.GetMemberById(canid);
            Member Cannameandaddress = Facade.GetMemberById(Convert.ToInt32(CID));
            if (ddlJobPosting.SelectedIndex > 0)
            {
                JobPosting jDetail = Facade.GetJobPostingById(iDetail.JobPostingId);
                calJobPostingCode = jDetail.JobPostingCode;
                calJobTitle = jDetail.JobTitle;
            }
            else
            {
                calJobPostingCode = string.Empty;
                calJobTitle = string.Empty;
            }

            calcanid = Cannameandaddress.Id.ToString();
            calcanname = Cannameandaddress.FirstName + Cannameandaddress.LastName;
            caltitle = "Cancel Interview - " + iDetail.Title  + " _ " + calJobPostingCode + " _ " + calcanid + " - " + calcanname;

            try 
            {
                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, CurrentMember.Id, "Appt.ics", "Other", false);
                string fileName = "";
                fileName += caltitle;
                fileName += ".ics";
                string dir = GetTempFolder();
                string filePath = Path.Combine(dir, fileName);

                StringBuilder writer = new StringBuilder();
                string startDate = Convert.ToDateTime(wdcStartDate.Text).ToShortDateString();
                string startTime = ddlStartTime.SelectedItem.Text;
                string endDateTime = ddlEndTime.SelectedItem.Text;
                int intDuration = 0;
                DateTime dtStartDateTime = GetDateAndDuration(out intDuration); 
                DateTime dtEndDateTime = Convert.ToDateTime(startDate + " " + endDateTime);
                writer.AppendLine("BEGIN:VCALENDAR");
                writer.AppendLine("VERSION:2.0");
                writer.AppendLine("PRODID:-//Microsoft Corporation//Outlook MIMEDIR//EN");
                //writer.AppendLine("PRODID:-//Google Inc//Google Calendar 70.9054//EN");
                //writer.AppendLine("CALSCALE:GREGORIAN");
                writer.AppendLine("METHOD:CANCEL");
                writer.AppendLine("BEGIN:VEVENT");
                writer.AppendLine("X-ALT-DESC;FMTTYPE=text/html:" + Remainder1 + "");
                if (iDetail.IcsFileUIDCode == "" || iDetail.IcsFileUIDCode == null)
                {
                    Facade.UpdateInterviewIcsCodeById(interview1.Id, Appuid.ToString());
                    writer.AppendLine("UID:" + Appuid.ToString());
                }
                else
                {
                    writer.AppendLine("UID:" + iDetail.IcsFileUIDCode.ToString());
                }

                string tt = string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime));
                string ttt = string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime));
                int ix = tt.IndexOf(":");
                int ixx = ttt.IndexOf(":");

                tt = tt.Substring(ix + 1);
                ttt = ttt.Substring(ixx + 1);
                string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                if (TimeZoneLocation != "Select Timezone")
                {
                    writer.AppendLine(string.Format("DTSTART;TZID=" + TimeZoneLocation + ":" + tt + ""));
                    writer.AppendLine(string.Format("DTEND;TZID=" + TimeZoneLocation + ":" + ttt + ""));
                }
                else
                {
                    writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime)));
                    writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                    writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                }

                //string TimeZoneLocation = ddlTimezone.SelectedItem.Text;
                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", "TZID:'" + TimeZoneLocation + "'", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime))); //TZID=US-Pacific:20140606T180000
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", "TZID:'" + TimeZoneLocation + "'", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));

                //writer.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtStartDateTime)));
                //writer.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", TimeZoneInfo.ConvertTimeToUtc(dtEndDateTime)));
                //writer.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));

                //ADD CODE BY PRAVIN KHOT ON 14/July/2016******************
                writer.AppendLine("ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:" + RecipientEmail);
                writer.AppendLine("ORGANIZER;CN= " + CurrentUserName + ":mailto:" + CurrentMember.PrimaryEmail);
                //********************END**************************

                writer.AppendLine("LOCATION:" + MiscUtil.RemoveScript(txtLocation.Text));
               
                //writer.AppendLine("BEGIN:VALARM");
                //writer.AppendLine("TRIGGER:-PT15M");
                //writer.AppendLine("ACTION:DISPLAY");
                writer.AppendLine("STATUS:CANCELLED");
                //writer.AppendLine("END:VALARM");
                writer.AppendLine("DESCRIPTION:This Interview has been canceled." + MiscUtil.RemoveScript(caltitle));  
                writer.AppendLine("SUMMARY:" + MiscUtil.RemoveScript(caltitle));  
                writer.AppendLine("CLASS:PUBLIC");
                writer.AppendLine("END:VEVENT");
                writer.AppendLine("END:VCALENDAR");

                UTF8Encoding enc = new UTF8Encoding();
                byte[] arrBytData = enc.GetBytes(writer.ToString());
                File.WriteAllBytes(strFilePath, arrBytData);

                bool isFileExist = File.Exists(strFilePath);
                if (isFileExist)
                {
                    try
                    {
                        if (!File.Exists(filePath))
                        {
                            File.Copy(strFilePath, filePath);
                        }
                    }
                    catch (FileNotFoundException ex)
                    {

                    }
                    if (strFilePath != fileName)
                    {
                        if (Files.ContainsKey(fileName))
                            Files.Remove(fileName);
                        Files.Add(fileName, filePath);
                    }
                    else
                    {
                        //return   "(File not found)";
                    }
                }

            }
            catch (System.Exception ex)
            {
                string test = ex.Message;
            }

        }
        //**************************END pravin khot 9/May/2016****************************************
        #endregion  CancelInterveiwSchedule

        #region  IndexChanged Events

        protected void ddlClientInterviewer_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlJobPosting.SelectedIndex = -1;
            //*****************COMMENT AND MODIFY BY PRAVIN KHOT ON 8/June/2016********
            //MiscUtil.PopulateJobPostingByClientIdAndManagerId(ddlJobPosting, Facade, Convert.ToInt32(ddlClientInterviewer.SelectedValue), 0);
            ddlJobPosting.Items.Clear();
            MiscUtil.GetAllByCleintIdByCandidatesId(ddlJobPosting, Facade, Convert.ToInt32(ddlClientInterviewer.SelectedValue), Convert.ToInt32(CandidateId));
            //***************************END**********************************
            populateClientInterviers(Int32.Parse(ddlClientInterviewer.SelectedValue));
            chkSuggestedInterviewer.Items.Clear();
            txtOtherInterviewers.Text = string.Empty;
            hfOtherInterviewers.Value = "";
            hfOtherInterviewers.Value = string.Empty;
            dvSuugestedPanel.Visible = false;

            //LoadOtherInterviewers();
        }
        protected void ddlJobPosting_SelectedIndexChanged(object sender, EventArgs e)
        {
            //*****************************Code comment by pravin khot**********************************
            txtOtherInterviewers.Text = string.Empty;
            hfOtherInterviewers.Value = "";
            hfOtherInterviewers.Value = string.Empty;
            //****************************************End***********************************************
            if (hfSourceId.Value != string.Empty)
            {
                foreach (ListItem item in chkInternalInterviewer.Items)
                {
                    if (item.Value == hfSourceId.Value)
                        item.Selected = false;
                }
            }
            hfSourceId.Value = string.Empty;
            if (ddlJobPosting.SelectedIndex != 0)
            {
                MemberJobCart cart = Facade.GetMemberJobCartByMemberIdAndJobPostringId(_memberId, Convert.ToInt32(ddlJobPosting.SelectedValue));
                if (cart != null)
                {
                    foreach (ListItem item in chkInternalInterviewer.Items)
                    {
                        if (item.Value == cart.SourceId.ToString())
                        {
                            item.Selected = true;
                            hfSourceId.Value = item.Value;
                        }
                    }
                }

                //----------------Code Introduced by pravin khot using fill Suggested Interviewer Name 9/Dec/2015-------------------
                chkSuggestedInterviewer.Items.Clear();
                string RequisitionName = ddlJobPosting.SelectedItem.Text;
                int RequisitionId = Convert.ToInt32(ddlJobPosting.SelectedValue);
                MiscUtil.PopulateSuggestedInterviewerWithEmail(chkSuggestedInterviewer, RequisitionId, Facade);
                chkSuggestedInterviewer = (CheckBoxList)MiscUtil.RemoveScriptForCheckBoxList(chkSuggestedInterviewer);
                chkSuggestedInterviewer.Items.RemoveAt(0);

                if (chkSuggestedInterviewer.Items.Count > 0)
                    dvSuugestedPanel.Visible = true;
                else
                    dvSuugestedPanel.Visible = false;

                txtOtherInterviewers.Text = string.Empty;
                hfOtherInterviewers.Value = "";
                hfOtherInterviewers.Value = string.Empty;
                LoadOtherInterviewers();
                //*****************************************End********************************************************************

            }
            //LoadAssociatedClient();
            ddlJobPosting.SelectedItem.Attributes.Add("title", ddlJobPosting.SelectedItem.Text);
        }
        protected void ddlInterviewType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtEmailInterviewerTemplate.Text = "";
            txtEmailCandidateTemplate.Text = "";
        }
        //******************Code added by pravin khot on 24/May/2016***********
        protected void ddllocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            int lactionid = Convert.ToInt32(ddllocation.SelectedValue);
            if (lactionid > 0)
            {
                GenericLookup genericlookup = new GenericLookup();
                genericlookup = Facade.GetGenericLookupById(lactionid);
                if (genericlookup.Description == "")
                {
                    divlocation.Visible = false ;
                }
                else
                {
                    divlocation.Visible = true;
                    txtLocation.Text = genericlookup.Description.ToString() ;
                }
               
            }
            else
            {
                divlocation.Visible = false ;
            }
        }
        //************************END***************************
        #endregion

        //*********Added by pravin khot on 15/July/2016************
        public void PopulateTimezoneList()
        {
            ddlTimezone.Visible = true;
            ddlTimezone.Items.Clear();
            ddlTimezone.DataSource = Facade.GetAllTimeZone();
            ddlTimezone.DataTextField = "TimeZone";
            ddlTimezone.DataValueField = "Timezoneid";
            ddlTimezone.DataBind();
            ddlTimezone = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlTimezone);
            ddlTimezone.Items.Insert(0, new ListItem("Select Timezone", "0"));

            int id = Facade.GetTimeZoneIdBy_MemberId(CurrentMember.Id);
            if (id > 0)
            {
                ddlTimezone.SelectedValue = id.ToString();
            }
            else
            {
                  SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                  if (siteSetting != null)
                  {
                      Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                      if (siteSettingTable[DefaultSiteSetting.TimeZone.ToString()].ToString() == "0" || siteSettingTable[DefaultSiteSetting.TimeZone.ToString()] == null || siteSettingTable[DefaultSiteSetting.TimeZone.ToString()] == "")
                      {
                          
                      }
                      else
                      {
                          ddlTimezone.SelectedValue = siteSettingTable[DefaultSiteSetting.TimeZone.ToString()].ToString();
                      }
                  }
            }
        }
        //*********************END********************************

       

        #endregion



    }
}
