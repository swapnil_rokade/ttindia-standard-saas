﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: NotesAndActivitiesEditor.ascx
    Description: This is the user control page used to insert notes and activities.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Jan-09-2008          Jagadish N            Defect id: 9073; Implemented sorting.
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotesAndActivitiesEditor.ascx.cs"
    Inherits="TPS360.Web.UI.ControlNotesAndActivitiesEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>

 
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
<div>
     
    <div class="TableRow" style="text-align:  right">
        <h6 class="field-header">
            <asp:Button ID="btnAddNote" runat="server" Text="Add Note" CssClass="btn btn-primary"
                onclick="btnAddNote_Click" /></h6>                   
  
    </div>   
    
    <br />
    <div class="TabPanelHeader">
        List of Notes
    </div>
    
     <div class="GridContainer">
       <div  id="bigDiv" runat="server" >    
       <%-- <div id="divnotes" runat="server" style="width:100%; height:100%; overflow: inherit; " >  --%>    
     <div id="divnotes" runat="server" style="width: 100%; height: 350px; overflow-y: scroll;" >
        </div>
       </div>
     </div>  
    
    
    
    
    <div class="GridContainer" style="text-align: left;">
        <asp:ObjectDataSource ID="odsNotesList" runat="server" EnablePaging="false" SelectMethod="GetAllByMemberID"
            TypeName="TPS360.Web.UI.CommonNoteDataSource" SortParameterName="sortExpression">
            <SelectParameters>
                <asp:Parameter Name="memberId"/>
            </SelectParameters>
        </asp:ObjectDataSource>
      
    </div>  

    
</div>
