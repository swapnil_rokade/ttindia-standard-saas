﻿<%------------------------------------------------------------------------------------------------------------------------------------
    FileName: Controls/RequisitionPreview.ascx
    Description: This is the user control page used to display details of selected requisition.
    Created By: 
    Created On:
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date          Author            Modification
    -----------------------------------------------------------------------------------------------------------------------------------
    0.1            Oct-20-2008   Shivanand         Defect Id 9063; The text on labels is changed to Level I, Level II, Level III.  
    0.2            Apr-07-2009   Sandeesh          Defect id:9457: changed the alignment of the tables
    0.3            Jan-21-2010   Nagarathna V.B     Defect Id:12078 decreased JobDesc & additional Notes div width 100% to 99.8%
---------------------------------------------------------------------------------------------------------------------------------------
--%>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionPreview.ascx.cs"
    Inherits="TPS360.Web.UI.cltRequisitionPreview" %>
    
<div>
    <div class="TableRow" style="text-align:left">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="CommonHeaderForDashboard">
        <div style="float:left;text-align:left;">
            <asp:Label EnableViewState="false" ID="lblJobTitle" runat="server" Text="" Font-Size="Medium" Font-Bold="true"></asp:Label>
        </div>
        <div style="float:right;text-align:right;">
            <asp:Label EnableViewState="false" ID="lblCityState" runat="server" Text="" ForeColor="Black"></asp:Label>
        </div>                
    </div> 
    <div style="width:99.6%;border-style:solid;border-width:1px;">
        <div style="float:left;width:43%;text-align:left;">
            <div id="divSalary" runat="server" visible="false">
                <asp:Label EnableViewState="false" ID="lblSalaryHeader" runat="server" style="font-weight:bold;" Text="Salary"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblSalary" runat="server" Text=""></asp:Label>
            </div>
            
            <div id="divEmpType" runat="server">
                <asp:Label EnableViewState="false" ID="lblEmpTypeHeader" runat="server" style="font-weight:bold;" Text="Employment Type"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblEmpType" runat="server" Text=""></asp:Label>
            </div>
            <div id="divDuration" runat="server">
                <asp:Label EnableViewState="false" ID="lblDurationHeader" runat="server" style="font-weight:bold;" Text="Duration"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblDuration" runat="server" Text=""></asp:Label>
            </div>
            <div id="divPayRate" runat="server">
                <asp:Label EnableViewState="false" ID="lblPayRateHeader" runat="server" style="font-weight:bold;" Text="Pay Rate"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblPayRate" runat="server" Text=""></asp:Label>
            </div>
            <div id="divPaymentType" runat="server">
                <asp:Label EnableViewState="false" ID="lblPaymentTypeHeader" runat="server" style="font-weight:bold;" Text="Payment Type"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblPaymentType" runat="server" Text=""></asp:Label>
            </div>
            <div id="divExpPaid" runat="server">
                <asp:Label EnableViewState="false" ID="lblExpPaidHeader" runat="server" style="font-weight:bold;" Text="Expenses Paid"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblExpPaid" runat="server" Text=""></asp:Label>
            </div>
            
            <div>
                <asp:Label EnableViewState="false" ID="lblExperienceRequiredHeader" runat="server" style="font-weight:bold;" Text="Years of Exp"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblMinExperienceRequiredHeader" runat="server" style="font-weight:bold;" Text="Min"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblMinExperienceRequired" runat="server" Text=""></asp:Label>
                &nbsp;&nbsp;&nbsp;
                <asp:Label EnableViewState="false" ID="lblMaxExperienceRequiredHeader" runat="server" style="font-weight:bold;" Text="Max"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblMaxExperienceRequired" runat="server" Text=""></asp:Label>
            </div>
            
           <div>
                <asp:Label EnableViewState="false" ID="lblNoofOpeningHeader" runat="server" style="font-weight:bold;" Text="No. of Openings"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblNoofOpening" runat="server" Text=""></asp:Label>
            </div>
            
            
            <div id="divProjectName" runat="server" visible="false">
                <asp:Label EnableViewState="false" ID="lblProjectNameHeader" runat="server" style="font-weight:bold;" Text="Project"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblProjectName" runat="server" Text=""></asp:Label>
            </div>
            <div id="divClientName" runat="server" visible="false">
                <asp:Label EnableViewState="false" ID="lblClientNameHeader" runat="server" style="font-weight:bold;" Text="Account"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblAllClientName" runat="server" Text=""></asp:Label>                
            </div>
            
        </div>
        <div style="float:left;width:33%;">
            <div>
                <asp:Label EnableViewState="false" ID="lblEducationQualificationHeader" runat="server" style="font-weight:bold;" Text="Required Education"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblEducationQualification" runat="server" Text=""></asp:Label>    
            </div>
            <div>
                <asp:Label EnableViewState="false" ID="lblWorkAuthorizationHeader" runat="server" style="font-weight:bold;" Text="Work Authorization"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblWorkAuthorization" runat="server" Text=""></asp:Label>
            </div>
            
            <div id="divPreSelected" runat="server" visible="false">
                <asp:Label EnableViewState="false" ID="lblPreselectedHeader" runat="server" style="font-weight:bold;" Text="Pre-Selected"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblPreselected" runat="server" Text=""></asp:Label>
            </div>
            <div id="divLevel1" runat="server" visible="false">
                <asp:Label EnableViewState="false" ID="LavelIHeader" runat="server" style="font-weight:bold;" Text="Level I"></asp:Label>:   <!-- 0.1 -->
                <asp:Label EnableViewState="false" ID="lblLavelI" runat="server" Text=""></asp:Label>
            </div>
            <div id="divLevel2" runat="server" visible="false">
                <asp:Label EnableViewState="false" ID="LavelIIHeader" runat="server" style="font-weight:bold;" Text="Level II"></asp:Label>:  <!-- 0.1 -->
                <asp:Label EnableViewState="false" ID="lblLavelII" runat="server" Text=""></asp:Label>
            </div>
            <div id="divLevel3" runat="server" visible="false">
                <asp:Label EnableViewState="false" ID="LavelIIIHeader" runat="server" style="font-weight:bold;" Text="Level III"></asp:Label>:  <!-- 0.1 -->
                <asp:Label EnableViewState="false" ID="lblLavelIII" runat="server" Text=""></asp:Label>
            </div>
            <div id="divFinalHired" runat="server" visible="false">
                <asp:Label EnableViewState="false" ID="FinalHiredHeader" runat="server" style="font-weight:bold;" Text="Final Hired"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblFinalHired" runat="server" Text=""></asp:Label>
            </div>
            <div id="divBenefits" runat="server">
                <asp:Label EnableViewState="false" ID="lblBenefitsHeader" runat="server" style="font-weight:bold;" Text="Benefits"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblBenefits" runat="server" Text=""></asp:Label>
            </div>
            <div id="divTravelRequired" runat="server">
                <asp:Label EnableViewState="false" ID="lblTravelRequiredHeader" runat="server" style="font-weight:bold;" Text="Travel Required"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblTravelRequired" runat="server" Text=""></asp:Label>
            </div>
            <div id="divTeleCommuting" runat="server">
                <asp:Label EnableViewState="false" ID="lblTeleCommutingHeader" runat="server" style="font-weight:bold;" Text="Telecommuting"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblTeleCommuting" runat="server" Text=""></asp:Label>
            </div>
            <div id="divClientJobID" runat="server">
                <asp:Label EnableViewState="false" ID="lblClientJobIDHeader" runat="server" style="font-weight:bold;" Text="Account Job ID"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblClientJobId" runat="server" Text=""></asp:Label>
            </div>
            <div id="divClientRate" runat="server">
                <asp:Label EnableViewState="false" ID="lblClientRateHeader" runat="server" style="font-weight:bold;" Text="Bill Rate"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblClientRate" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div style="float:right;width:23%;text-align:right;">
            <div>
                <asp:Label EnableViewState="false" ID="lblJobStatusHeader" runat="server" style="font-weight:bold;" Text="Current Status"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblJobStatus" runat="server" Text=""></asp:Label>
            </div>
            <div>
                <asp:Label EnableViewState="false" ID="lblPublishingDateHeader" runat="server" style="font-weight:bold;" Text="Date Published"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblPublishingDate" runat="server" Text=""></asp:Label>
            </div>
            <div>
                <asp:Label EnableViewState="false" ID="lblHireDateHeader" runat="server" style="font-weight:bold;" Text="Closing Date"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblHireDate" runat="server" Text=""></asp:Label>
            </div>
            <div>
                <asp:Label EnableViewState="false" ID="lblPostingDateHeader" runat="server" style="font-weight:bold;" Text="Date Created"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblPostingDate" runat="server" Text=""></asp:Label>    
            </div>
            <div>
                <asp:Label EnableViewState="false" ID="lblTimeToFillHeader" runat="server" style="font-weight:bold;" Text="Time to Fill"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblTimeToFill" runat="server" Text=""></asp:Label>
            </div>
            <div>
                <asp:Label EnableViewState="false" ID="lblStartDateHeader" runat="server" style="font-weight:bold;" Text="Start Date"></asp:Label>:
                <asp:Label EnableViewState="false" ID="lblStartDate" runat="server" Text=""></asp:Label>    
            </div>
        </div>
    </div>
        
    <div class="CommonHeaderForDashboard" style="clear:both;">
        <asp:Label EnableViewState="false" ID="lblJobDescHeader" runat="server" style="font-weight:bold;" Text="Job Description"></asp:Label>
    </div>
    <div id="divJobDesc" style="width:99.8%;" runat="server">
     <table id="tblJobDesc"  style="width:100%" class="EmptyDataTable alert alert-warning" runat="server">
        <tr>
            <td>
                No Description added.
            </td>
        </tr>
     </table>
     <asp:Label EnableViewState="false" ID="lblJobDesc" runat="server" Text=""></asp:Label>
    </div>
    <div class="CommonHeaderForDashboard" style="clear: both;">
        <asp:Label EnableViewState="false" ID="lblHiringTeamHeader" runat="server" style="font-weight:bold;" Text="Requisition Hiring Team Members"></asp:Label>
    </div>
    <div class="GridContainer">
        <asp:ListView ID="lsvAssignedTeam" runat="server" DataKeyNames="Id" OnItemDataBound="lsvAssignedTeam_ItemDataBound">
            <LayoutTemplate>
                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                    <tr>
                        <th style="width: 40%; white-space: nowrap;">
                            Name
                        </th>
                        <th style="width: 40%; white-space: nowrap;">
                            Email
                        </th>                            
                        <th style="width: 10%; white-space: nowrap;">
                            Role
                        </th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                </table>
            </LayoutTemplate>
            <EmptyDataTemplate>
                <table id="tblEmptyData" style="width:100%" class="EmptyDataTable alert alert-warning" runat="server">
                    <tr>
                        <td>
                            No recruiters assigned.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <ItemTemplate>
                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                    <td style="text-align:left">
                        <asp:Label ID="lblMemberName" runat="server" />
                    </td>
                    <td style="text-align:left">
                        <asp:Label ID="lblMemberEmail" runat="server" />
                    </td>
                    <td style="text-align:left">
                        <asp:Label ID="lblEmployeeType" runat="server" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
    
    <div class="CommonHeaderForDashboard" style="clear:both;">
        <asp:Label EnableViewState="false" ID="lblAdditionalNotesHeader" runat="server" style="font-weight:bold;" Text="Additional Notes"></asp:Label>
    </div>
    <div id="divAddiNotes" style="width:99.8%;" runat="server">
     <table id="tblAdditionalNotes"  style="width:100%" class="EmptyDataTable alert alert-warning" runat="server">
        <tr>
            <td>
                No Additional Notes added.
            </td>
        </tr>
     </table>
     <table style="width:99%">
        <tr>
            <td>
             <asp:Label EnableViewState="false" ID="lblAdditionaNotes" runat="server" Text=""></asp:Label>
            </td>
        </tr>
     </table>
   </div>
    
</div>

