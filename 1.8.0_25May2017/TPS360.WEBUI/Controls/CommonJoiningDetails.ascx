﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CommonJoiningDetails.ascx.cs" Inherits="TPS360.Web.UI.CommonJoiningDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.EditorControls" tagprefix="ig" %>

<style type="text/css">  
        .CalendarCSS  
        {  
            background-color:Gray  ;  
            color:Blue ;  
            border-color : Maroon ;
            }  
            .custom-calendar .ajax__calendar_container

{

 background-color:#ffc; /* pale yellow */

 border:solid 1px #666;

}

.custom-calendar .ajax__calendar_title

{

 background-color:#cf9; /* pale green */

 height:20px;

 color:#333;

}

.custom-calendar .ajax__calendar_prev,

.custom-calendar .ajax__calendar_next

{

 background-color:#aaa; /* darker gray */

 height:20px;

 width:20px;

}

.custom-calendar .ajax__calendar_today

{

 background-color:#cff;  /* pale blue */

 height:20px;

}

.custom-calendar .ajax__calendar_days table thead tr td

{

 background-color:#ff9; /* dark yellow */

 color:#333;

}

.custom-calendar .ajax__calendar_day

{

 color:#333; /* normal day - darker gray color */

}

.custom-calendar .ajax__calendar_other .ajax__calendar_day

{

 display :none ;
 color:#666; /* day not actually in this month - lighter gray color */

}
.calendarDropdown
{
	margin-left : -18px;
 margin-bottom :-3px;
}
    </style>  
    <script language ="javascript" type ="text/javascript" >
        function WebDateChooser1_InitializeDateChooser(oDateChooser)
{
 // used to set "drop-down block" variable if calendar was already opened
 oDateChooser._myMouseDown = function()
 {
  var me = igdrp_getComboById('<%=wdcJoiningDate.ClientID%>');
  if(me && me.isDropDownVisible())
   me._calOpenedTime = new Date().getTime();
 }
 // used to open 
 oDateChooser._myClick = function()
 {
  var me = igdrp_getComboById('<%=wdcJoiningDate.ClientID%>');
  if(me && !me.isDropDownVisible() && (!me._calOpenedTime || me._calOpenedTime + 500 < new Date().getTime()))
   me.setDropDownVisible(true);
 }
 oDateChooser.inputBox.onmousedown = oDateChooser._myMouseDown;
 oDateChooser.inputBox.onclick = oDateChooser._myClick;
//alert('ok');
}
function CalculateRevenue()
{
var BillingSal=document .getElementById ('<%=txtBillableSalary.ClientID %>');
var BillingRate=document .getElementById ('<%=txtBillingRate.ClientID %>');
var Revenue=document .getElementById ('<%=txtRevenue.ClientID %>');
var rev='';
if(BillingSal.value !='' && BillingRate.value !='')
{
     rev=(BillingSal .value * BillingRate .value / 100);
}
Revenue .value=rev;
}
  
    </script>
     <style type="text/css">
.front
{

 position : relative ;
}

</style>
<asp:UpdatePanel ID="upJoin" runat ="server" >
<ContentTemplate >



<asp:HiddenField ID ="hdnBulkAction" runat ="server" Value ="" />
<asp:HiddenField ID="hfMemberId" runat ="server" />
<asp:HiddenField ID ="hfJobPostingId" runat ="server" />
<asp:HiddenField ID ="hfMemberJoiningDetailsId" runat ="server" Value ="0" />
<asp:HiddenField ID ="hfStatusId" runat ="server" />
<asp:HiddenField ID ="hfCurrentMemberId" runat ="server" />

<div class ="TableRow">
    <div class ="TableFormLeble">Joining Date:
    </div>
    <div class ="TableFormContent">
    <%--<asp:TextBox   
            ID="txtJoiningDate"  
            runat="server"  Width ="100px" TabIndex ="1"
            >  
        </asp:TextBox>  
           <ajaxToolkit:CalendarExtender   
            ID="wdcJoiningDate"  
            runat="server"  
            TargetControlID="txtJoiningDate"    
            CssClass="custom-calendar"   
             PopupButtonID ="imgShows" EnableViewState ="true"  
             
            >  
        </ajaxToolkit:CalendarExtender>  
      
       
        <asp:ImageButton ID="imgShows" runat ="server"  CssClass ="calendarDropdown"  ImageUrl ="~/Images/downarrow.gif" /> --%>
<%--  <asp:TextBox ID="txtJoiningDate" AutoComplete="OFF" runat="server" Width ="100px" ></asp:TextBox>--%>  
        <ig:WebDatePicker ID="wdcJoiningDate" runat="server" Width="100px" DropDownCalendarID="ddcJoiningDate"></ig:WebDatePicker>
        <ig:WebMonthCalendar ID="ddcJoiningDate" runat="server"></ig:WebMonthCalendar>
 
        </div>
</div>


<div class ="TableRow">
    <div class ="TableFormLeble">Offered Salary:
    </div>
    <div class ="TableFormContent">
          <asp:TextBox ID="txtSalary" runat="server" CssClass="CommonTextBox"   TabIndex ="2"
                Width="70px" ></asp:TextBox>
                
            <asp:DropDownList ID="ddlSalary" runat="server" CssClass="CommonDropDownList"  Width="65px"  TabIndex ="3"
                >
                <asp:ListItem Value="4">Yearly</asp:ListItem>
                <asp:ListItem Value="3">Monthly</asp:ListItem>
                <asp:ListItem Value="2">Daily</asp:ListItem>
                <asp:ListItem Value="1" Selected="True">Hourly</asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="ddlSalaryCurrency" runat="server"  Width ="50px"
                CssClass="CommonDropDownList"  TabIndex ="4">
            </asp:DropDownList>
            <asp:Label ID="lblPayrateCurrency" runat ="server" ></asp:Label>
    </div>
</div>
 <div class="TableRow" > 
     <div class ="TableFormValidatorContent" style =" margin-left : 42%">
       <asp:RegularExpressionValidator ID ="rgvSalary" runat ="server" ControlToValidate ="txtSalary" Display ="Dynamic" ErrorMessage ="Please enter Numeric values" ValidationExpression ="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)" ValidationGroup ="ValGrpJobTitle"></asp:RegularExpressionValidator>
    </div>
        </div> 
  
<div class ="TableRow">
    <div class ="TableFormLeble">Billable Salary:
    </div>
    <div class ="TableFormContent">
          <asp:TextBox ID="txtBillableSalary" runat="server" CssClass="CommonTextBox" 
                 TabIndex ="5" onblur="CalculateRevenue()"  Width="70px" ></asp:TextBox>
          <asp:DropDownList ID="ddlBillableSalaryCurrency" runat ="server"  TabIndex ="6" Width="50px" CssClass ="CommonDropDownList"  ></asp:DropDownList>
             <asp:Label ID="lblBillablePayRateCurrency" runat ="server" ></asp:Label>
    </div>
</div>
 <div class="TableRow" > 
     <div class ="TableFormValidatorContent" style =" margin-left : 42%">
       <asp:RegularExpressionValidator ID ="rgvBillSalary" runat ="server" ControlToValidate ="txtBillableSalary" Display ="Dynamic" ErrorMessage ="Please enter Numeric values" ValidationExpression ="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)" ValidationGroup ="ValGrpJobTitle"></asp:RegularExpressionValidator>
    </div>
        </div>
<div class ="TableRow">
    <div class ="TableFormLeble">Billing Rate:
    </div>
    <div class ="TableFormContent">
          <asp:TextBox ID="txtBillingRate" runat="server" CssClass="CommonTextBox" 
                Width ="23px"  onblur="CalculateRevenue()" MaxLength="4" TabIndex ="7"></asp:TextBox> 
            <span> %</span>
            
    </div>
</div>
 <div class="TableRow" > 
     <div class ="TableFormValidatorContent" style =" margin-left : 42%">
       <asp:RegularExpressionValidator ID ="rgvBillRate" runat ="server" ControlToValidate ="txtBillingRate" Display ="Dynamic" ErrorMessage ="Please enter Numeric values" ValidationExpression ="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)" ValidationGroup ="ValGrpJobTitle"></asp:RegularExpressionValidator>
    </div>
        </div>
        <div class ="TableRow">
    <div class ="TableFormValidatorContent" style =" margin-left : 42%">
        <asp:RangeValidator ID="gvRate" runat="server" Display ="Dynamic"  ErrorMessage="Enter 0 to 100 only." ControlToValidate="txtBillingRate" Type="Double" ValidationGroup ="ValGrpJobTitle"   ></asp:RangeValidator>
    </div>
</div>
<div class ="TableRow">
    <div class ="TableFormLeble">Revenue:
    </div>
    <div class ="TableFormContent">
          <asp:TextBox ID="txtRevenue" runat="server" CssClass="CommonTextBox" TabIndex ="8"  Width="70px" ></asp:TextBox>
          <asp:DropDownList ID="ddlRevenueCurrency" runat ="server"  TabIndex ="9" Width="50px" CssClass ="CommonDropDownList"   ></asp:DropDownList>
           <asp:Label ID="lblRevenueCurrency" runat ="server" ></asp:Label>
    </div>
</div>
<div class ="TableRow">
    <div class ="TableFormLeble">Location:
    </div>
    <div class ="TableFormContent">
          <asp:TextBox ID="txtLocation" runat="server" CssClass="CommonTextBox" 
                 TabIndex ="9"  Width="70px" ></asp:TextBox>
    </div>
</div>
 <div class="TableRow" > 
     <div class ="TableFormValidatorContent" style =" margin-left : 42%">
       <asp:RegularExpressionValidator ID ="rgvRevenue" runat ="server" ControlToValidate ="txtRevenue" Display ="Dynamic" ErrorMessage ="Please enter Numeric values" ValidationExpression ="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)" ValidationGroup ="ValGrpJobTitle"></asp:RegularExpressionValidator>
    </div>
        </div>

  

<div class ="TableRow"  style =" padding-top :30px; margin-left : 42%" >

<asp:Button ID ="btnSave" runat ="server" Text ="Save" OnClick ="btnSave_Click" CssClass ="CommonButton" ValidationGroup ="ValGrpJobTitle" TabIndex ="10"/>
<asp:Button ID ="btnRemove" runat ="server" Text ="Remove Joining Details" OnClick ="btnRemove_Click" CssClass ="CommonButton" TabIndex ="11"/>
     
</div>

</ContentTemplate>
</asp:UpdatePanel>