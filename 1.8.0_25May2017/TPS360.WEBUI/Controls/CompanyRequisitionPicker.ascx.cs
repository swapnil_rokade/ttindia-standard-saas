﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: AllEmailList.ascx.cs
    Description: This is the user control page used for sending e-mails
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sep-26-2008           Yogeesh Bhat        Defect ID: 8706; Changes made in Page_Load() method.
                                                             (Checked for Session["ReferenceLink"] if it is from referenceLink, 
                                                              updated _tomemberId to reference ID from session)
    0.2            Sep-30-2008           Yogeesh Bhat        Defect ID: 8856; Changes made in Page_Load(), btnReply_Click(), and
                                                             btnForward_Click() methods (Checked for listview items count)
    0.3            Oct-17-2008           Jagadish            Defect ID: 8969; Changed the parameter "Reply" to "Forward" in btnForward_Click() method.
    0.4            Nov-26-2008           Jagadish            Defect id: 8813; When a 'Delete icon' is clicked at 'Action' column header then multiple 'Radio buttons' were selected
                                                             Changed the code to fix that.
    0.5            Dec-05-2008           Jagadish            Defect id: 9358; changed case of variable id's from 'ReceiverId' and
                                                             'SenderId' to 'receiverId' and 'senderId' and '_tomemberId' to '_frommemberId'.
    0.6            Jan-02-2008           Jagadish            Defect id: 9518; changes made in btnForward_Click() method and btnReply_Click() method.
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Linq;
using System.Web.UI;
namespace TPS360.Web.UI
{
   
    
    public partial class CompanyRequisitionPicker : BaseControl
    {

        private bool _isMyList = true;
        public bool ISMyList
        {
            get
            {
                return _isMyList;
            }
            set
            {
                _isMyList = value;
            }
        }
       
                          
        public int SelectedRequisitionId
        {
            get
            {
                return Convert.ToInt32(hdnSelectedRequisitionID.Value);
            }
            set
            {
                hdnSelectedRequisitionID.Value = value.ToString();
                LoadRequsitions();
                ControlHelper .SelectListByValue (ddlRequisition   ,value .ToString ());
            }
        }

        public string FirstText
        {
            get
            {
                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                {
                    return  "Select BU";
                }
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                {
                    return  "Select Account";

                }
                else 
                {
                   return  "Select Vendor";
                }

            }
        }

        public int SelectedClientId
        {
            get
            {
                FillControls();
                return Convert.ToInt32(ddlClientList .SelectedValue );
            }
            set
            {
                FillControls();
                ControlHelper.SelectListByValue(ddlClientList , value.ToString());
                LoadRequsitions();
            }
            
        }
        protected void Page_Load(object sender,EventArgs e)
        {
            ddlClientList.Attributes.Add("onchange", "javascript:ClientForRequisitionOnChange('" + ddlClientList.ClientID + "','" + ddlRequisition.ClientID + "','" + (ISMyList  ? base.CurrentMember.Id.ToString() : "0") + "','');");
            ddlRequisition.Attributes.Add("onchange", "javascript:RequisitionOnChange('" + ddlRequisition.ClientID + "','" + hdnSelectedRequisitionID.ClientID + "')");
            FillControls();
        }
        
        public void  FillControls()
        {
            if (ddlClientList.Items.Count == 0)
            {
                if (!IsPostBack)
                {
                    MiscUtil.PopulateClients(ddlClientList, ((int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting)), Facade);
                    MiscUtil.RemoveScriptForDropDown(ddlClientList);
                    ddlClientList.Items[0].Text = FirstText;
                    LoadRequsitions();
                }
            }

           
        }

        private void LoadRequsitions()
        {

            MiscUtil.PopulateJobPostingByClientIdAndManagerId(ddlRequisition, Facade, Convert.ToInt32(ddlClientList.SelectedValue == "" ? "0" : ddlClientList.SelectedValue), (ISMyList  ? base.CurrentMember.Id : 0));
        }
       
        
    }
    
}