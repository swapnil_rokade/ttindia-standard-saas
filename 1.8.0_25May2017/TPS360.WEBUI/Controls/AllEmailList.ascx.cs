﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: AllEmailList.ascx.cs
    Description: This is the user control page used for sending e-mails
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sep-26-2008           Yogeesh Bhat        Defect ID: 8706; Changes made in Page_Load() method.
                                                             (Checked for Session["ReferenceLink"] if it is from referenceLink, 
                                                              updated _tomemberId to reference ID from session)
    0.2            Sep-30-2008           Yogeesh Bhat        Defect ID: 8856; Changes made in Page_Load(), btnReply_Click(), and
                                                             btnForward_Click() methods (Checked for listview items count)
    0.3            Oct-17-2008           Jagadish            Defect ID: 8969; Changed the parameter "Reply" to "Forward" in btnForward_Click() method.
    0.4            Nov-26-2008           Jagadish            Defect id: 8813; When a 'Delete icon' is clicked at 'Action' column header then multiple 'Radio buttons' were selected
                                                             Changed the code to fix that.
    0.5            Dec-05-2008           Jagadish            Defect id: 9358; changed case of variable id's from 'ReceiverId' and
                                                             'SenderId' to 'receiverId' and 'senderId' and '_tomemberId' to '_frommemberId'.
    0.6            Jan-02-2008           Jagadish            Defect id: 9518; changes made in btnForward_Click() method and btnReply_Click() method.
 * 0.7             21/June/2016          pravin khot         modify- hlnkSubject,hlnkEmailBody
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Linq;
using System.Web.UI;
namespace TPS360.Web.UI
{
    public partial class ControlAllEmailList : BaseControl
    {
        #region Member Variables

        public int _memberId = 0;
        public int _emailTypeLookupId = 0;
        public  int _frommemberId = 0;
        public  int _tomemberId = 0;
        public  int _contactId = 0;
        public  bool status = false;
        #endregion

        #region Properties


        public int EmailTypeLookupId
        {
            get
            {
                return _emailTypeLookupId;
            }

            set
            {
                _emailTypeLookupId = value;
            }

        }
        int companyID
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]);

                }
                ////From Member Login
                else
                {
                    return 0;
                }

            }
        }
        public EmailFromType EmailFromType
        {
            get
            {
                return (EmailFromType)(ViewState[this.ClientID + "_EmailFromType"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_EmailFromType"] = value;
            }
        }
        public SelectionType SelectionType
        {
            get
            {
                return (SelectionType)(ViewState[this.ClientID + "_SelectionMode"] ?? SelectionType.CheckBox);
            }
            set
            {
                ViewState[this.ClientID + "_SelectionMode"] = value;
            }
        }
        #endregion

        #region Methods
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvEmail.FindControl(hdnSortColumn.Value);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", ( hdnSortOrder .Value == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }

        private void BindList()
        {
            lsvEmail.DataSourceID = "odsEmailList";
            lsvEmail.DataBind();
        }


        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Int32.Parse(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                _tomemberId = _memberId;
                _frommemberId = base.CurrentMember.Id; //jaga
            }
            //From Member Login
            else
            {
                _memberId = base.CurrentMember.Id;
                _frommemberId = _memberId;
            }
           
        }

        private void PrePareView()
        {
                status = false;
                lsvEmail.DataSourceID = "odsEmailList";
                lsvEmail.DataBind();
            
        }

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ddlcontact.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            if (!IsPostBack)
            {
                string[] arr;
                if (Session["ReferenceLink"] != null)
                {
                    arr = Session["ReferenceLink"].ToString().Split((char)Convert.ToChar(" "));
                    _tomemberId = Convert.ToInt32(arr[1]);
                    Session.Abandon();
                }
                GetMemberId();
                PrePareView();
                if (EmailFromType == (EmailFromType)EmailFromType.CompanyEmail)
                {
                    lblcontent.Visible = true;
                    ddlcontact.Visible = true;
                    lblcontent.Text = "Filter by Contact: ";
                    checking.Value = "Company";
                    IList<CompanyContact> contact = new List<CompanyContact>();
                    contact = Facade.GetAllCompanyContactByComapnyId(companyID);
                    if (contact != null)
                    {
                        contact = contact.OrderBy(f => f.Email).ToList();
                        int p_contact = 0;
                        ddlcontact.Items.Add("Please Select");
                        foreach (CompanyContact con in contact)
                        {
                            ListItem list = new ListItem();
                            if (con.Email != string.Empty)
                            {
                                list.Text =MiscUtil .RemoveScript ( con.FirstName,string .Empty ) + " " +MiscUtil .RemoveScript ( con.LastName ,string .Empty )+ " [" + con.Email + "]";
                                list.Value = con.MemberId.ToString();
                                list.Attributes.Add("title", list.Text);
                                ddlcontact.Items.Add(list);

                            }
                        }
                    }
                }
                this.EmailTypeLookupId = 0;
                hdnSortColumn.Value = "btnDateTime";
                hdnSortOrder.Value = "DESC";
                PlaceUpDownArrow();
            }
            string pagesize = "";
           pagesize = (Request.Cookies["AllEmailRowPerPage"] == null ? "" : Request.Cookies["AllEmailRowPerPage"].Value); ;
           ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvEmail.FindControl("pagerControl");
           if (PagerControl != null)
           {
               DataPager pager = (DataPager)PagerControl.FindControl("pager");
               if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
           }
        }

        protected void btnReply_Click(object sender, EventArgs e)
        {
            if (lsvEmail.DataKeys.Count > 0)       
            {
                //int memberEmailId = Request.Form["rdoMemberEmailId"] != null ? Int32.Parse(Request.Form["rdoMemberEmailId"]) : 0;
                string mberEmail = Request.Form["rdoMemberEmailId"] != null ? Request.Form["rdoMemberEmailId"].ToString() : string.Empty;
                string[] memberEmail = mberEmail.Split(',');
                int memberEmailId=0;
                if (mberEmail != "")
                    memberEmailId = Convert.ToInt32(memberEmail[0]);

                    if (Request.Form["rdoMemberEmailId"] != null)
                    {
                        string type = Facade.GetMemberEmailTypeById(memberEmailId);
                        type = memberEmail[1];
                        if (type == "Received")
                        {
                        string UrlEml;
                        SecureUrl url;
                        if (EmailFromType ==(EmailFromType ) EmailFromType .CompanyEmail )
                        {
                            UrlEml = UrlConstants.SFA.COMPANY_EMAIL_PAGE;
                            url = UrlHelper.BuildSecureUrl(UrlEml, string.Empty, UrlConstants.PARAM_ID, memberEmailId.ToString(), UrlConstants.PARAM_COMPANY_ID, Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID], UrlConstants.PARAM_FLAG_ID, "Reply");
                            Helper.Url.Redirect(url);
                        }
                        else if (EmailFromType ==(EmailFromType ) EmailFromType.EmailHistory  )
                        {
                            url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_ID, memberEmailId.ToString(), UrlConstants.PARAM_FLAG_ID, "Reply", UrlConstants.PARAM_PAGE_FROM, "History");
                            string scriptRun = "<script> var w= window.open('" + url.ToString() + "','NewReplyEmailWindow', 'width=600, height=600, toolbar=0, scrollbars=1, menubar=0, location=0');</script>";
                            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(System.Web.UI.Page), "NewReplyEmailWindow", scriptRun, false);
                        }
                        else if (EmailFromType == (EmailFromType)EmailFromType.EmployeeEmail)
                        {
                            UrlEml = UrlConstants.Employee.EMPLOYEE_EMAILPAGE;
                            url = UrlHelper.BuildSecureUrl(UrlEml, string.Empty, UrlConstants.PARAM_ID, memberEmailId.ToString(), UrlConstants.PARAM_MEMBER_ID, Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID], UrlConstants.PARAM_FLAG_ID, "Reply");
                            Helper.Url.Redirect(url);
                        }
                        else
                        {
                            UrlEml =UrlConstants.ATS.ATS_INTERNALEMAIL_EDITOR;
                            url = UrlHelper.BuildSecureUrl(UrlEml, string.Empty, UrlConstants.PARAM_ID, memberEmailId.ToString(), UrlConstants.PARAM_MEMBER_ID, Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID], UrlConstants.PARAM_FLAG_ID, "Reply");
                            Helper.Url.Redirect(url);
                        }
                        }
                        else
                            MiscUtil.ShowMessage(lblMessage, "Reply option not available for "+type +" mails.", false);
            
                        
                    }
                    else
                        MiscUtil.ShowMessage(lblMessage, "Please select email to Reply.", true);
                }


            else
            {
                BindList();
                return;
            }
        }
        protected void btnForward_Click(object sender, EventArgs e)
        {
            if (lsvEmail.DataKeys.Count > 0)        
            {
                //int memberEmailId = Request.Form["rdoMemberEmailId"] != null ? Int32.Parse(Request.Form["rdoMemberEmailId"]) : 0;
                string mberEmail = Request.Form["rdoMemberEmailId"] != null ? Request.Form["rdoMemberEmailId"].ToString() : string.Empty;
                string[] memberEmail = mberEmail.Split(',');
                int memberEmailId=0;
                if (mberEmail != "")
                    memberEmailId = Convert.ToInt32(memberEmail[0]);

                if (Request.Form["rdoMemberEmailId"] != null)
                {
                    string UrlEml;
                    SecureUrl url;
                    if (EmailFromType == (EmailFromType)EmailFromType.CompanyEmail )
                    {
                        UrlEml = UrlConstants.SFA.COMPANY_EMAIL_PAGE;
                        url = UrlHelper.BuildSecureUrl(UrlEml, string.Empty, UrlConstants.PARAM_ID, memberEmailId.ToString(), UrlConstants.PARAM_COMPANY_ID, Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID], UrlConstants.PARAM_FLAG_ID, "Forward");
                        Helper.Url.Redirect(url);
                    }
                    else if (EmailFromType == (EmailFromType)EmailFromType.EmailHistory )
                    {
                        url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_ID, memberEmailId.ToString(), UrlConstants.PARAM_FLAG_ID, "Forward",UrlConstants .PARAM_PAGE_FROM ,"History");
                        string scriptRun = "<script> var w= window.open('" + url.ToString() + "','NewEmailWindow', 'width=600, height=600, toolbar=0, scrollbars=1, menubar=0, location=0');</script>";
                         System .Web .UI . ScriptManager.RegisterClientScriptBlock(Page, typeof(System .Web .UI .Page ), "NewEmailWindow", scriptRun , false);
                    }
                    else if (EmailFromType == (EmailFromType)EmailFromType.EmployeeEmail)
                    {
                        UrlEml = UrlConstants.Employee.EMPLOYEE_EMAILPAGE;
                        url = UrlHelper.BuildSecureUrl(UrlEml, string.Empty, UrlConstants.PARAM_ID, memberEmailId.ToString(), UrlConstants.PARAM_MEMBER_ID, Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID], UrlConstants.PARAM_FLAG_ID, "Forward");
                        Helper.Url.Redirect(url);
                    }
                    else
                    {
                        UrlEml = UrlConstants.ATS.ATS_INTERNALEMAIL_EDITOR;
                        url = UrlHelper.BuildSecureUrl(UrlEml, string.Empty, UrlConstants.PARAM_ID, memberEmailId.ToString(), UrlConstants.PARAM_MEMBER_ID, Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID], UrlConstants.PARAM_FLAG_ID, "Forward");
                        Helper.Url.Redirect(url);
                    }

                }
                else
                    MiscUtil.ShowMessage(lblMessage, "Please select email to Forward.", true);
            }

            else
            {
                BindList();
                return;
            }
        }

        protected void btnSearchEmail_Click(object sender, EventArgs e)
        
            {
                if (ddlcontact.Items.Count > 0) ddlcontact.SelectedIndex = 0;
            status = false;
            lsvEmail.DataSourceID = "odsEmailList";
            lsvEmail.DataBind();
            hdnSortColumn.Value = "btnDateTime";
            hdnSortOrder.Value = "DESC";
            PlaceUpDownArrow();
        }

        #endregion
        private string StripHTML(string source)
        {
            try
            {
                string result;
                result = source.Replace("\r", " ");
                result = result.Replace("\n", " ");
                result = result.Replace("\t", string.Empty);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = result.Replace("\n", "\r");
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\t)", "\t\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\r)", "\t\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\t)", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                string breaks = "\r\r\r";
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }
                return result;
            }
            catch
            {
                return source;
            }
        }
        #region ListView Events

        protected void lsvEmail_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _isAdmin = base.IsUserAdmin; 
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberEmail memberEmail = ((ListViewDataItem)e.Item).DataItem as MemberEmail;
                   string body=string .Empty ;
                   if (memberEmail.EmailBody.ToString() != string.Empty)
                   {
                       string rawDes = Regex.Replace(memberEmail.EmailBody.ToString().Trim(), @"<(.|\n)*?>", string.Empty);
                       rawDes = rawDes.Replace("body, td, p{font-family: Arial, Helvetica, sans-serif;font-size: 12px;}", string.Empty);
                       body = rawDes;
                   }
              
                if (memberEmail != null)
                {
                    int size = 0;

                    Label lblEmailType = (Label)e.Item.FindControl("lblEmailType");
                    Label lblMemberEmailId = (Label)e.Item.FindControl("lblMemberEmailId");
                    Label lblDataTime = (Label)e.Item.FindControl("lblDataTime");
                    Label lblSent = (Label)e.Item.FindControl("lblSent");
                    Label lblReceived = (Label)e.Item.FindControl("lblReceived");
                    HyperLink hlnkSubject = (HyperLink)e.Item.FindControl("hlnkSubject");
                    HyperLink hlnkEmailBody = (HyperLink)e.Item.FindControl("hlnkEmailBody");
                    HiddenField hdfMemberEmailId = (HiddenField)e.Item.FindControl("hdfMemberEmailId");
                    if (EmailFromType ==(EmailFromType ) EmailFromType .CompanyEmail )
                    {
                        if (companyID  > 0)
                        {
                            //*******Code modify by pravin khot on 21/June/2016************
                                    hlnkSubject.Text = memberEmail.Subject;
                                    hlnkSubject.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                                    hlnkSubject.Target = "_blank";

                                    hlnkEmailBody.Text = body;
                                    hlnkEmailBody.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                                    hlnkEmailBody.Target = "_blank";
                                    //ControlHelper.SetHyperLink(hlnkSubject, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, memberEmail.Subject, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                                    //ControlHelper.SetHyperLink(hlnkEmailBody, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, body, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
        
                            //*********************END***********************************       
                        }
                    }
                    else if ((EmailFromType == (EmailFromType)EmailFromType.EmailHistory) || (EmailFromType ==(EmailFromType )EmailFromType .EmployeeEmail ))
                    {
                        GetMemberId();
                        if (_memberId > 0)
                        {
                            //*******Code modify by pravin khot on 21/June/2016************
                            hlnkSubject.Text = memberEmail.Subject;
                            hlnkSubject.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                            hlnkSubject.Target = "_blank";

                            hlnkEmailBody.Text = body;
                            hlnkEmailBody.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                            hlnkEmailBody.Target = "_blank";
                            //ControlHelper.SetHyperLink(hlnkSubject, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, memberEmail.Subject, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                            //ControlHelper.SetHyperLink(hlnkEmailBody, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, body, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                          //*********************END***********************************
                            //ControlHelper.SetHyperLink(hlnkSubject, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, memberEmail.Subject, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                           // ControlHelper.SetHyperLink(hlnkEmailBody, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, body, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                        }
                    }
                    else
                    {
                        if (_tomemberId > 0)
                        {
                            string PrimaryEmail = Facade.GetMemberUserNameById(_tomemberId);

                            if (PrimaryEmail != string .Empty )
                            {
                            MembershipUser user = Membership.GetUser(PrimaryEmail);

                            if (user != null)
                            {
                                if (Roles.IsUserInRole(user.UserName, MemberType.Candidate.ToString()))
                                {
                                    //*******Code modify by pravin khot on 21/June/2016************
                                    hlnkSubject.Text = memberEmail.Subject;
                                    hlnkSubject.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                                    hlnkSubject.Target = "_blank";
                                   // ControlHelper.SetHyperLink(hlnkSubject, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, memberEmail.Subject, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                                   //*********************END***********************************
                                }
                                else if (Roles.IsUserInRole(user.UserName, MemberType.Employee.ToString()))
                                {
                                    //*******Code modify by pravin khot on 21/June/2016************
                                    hlnkSubject.Text = memberEmail.Subject;
                                    hlnkSubject.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                                    hlnkSubject.Target = "_blank";
                                    //ControlHelper.SetHyperLink(hlnkSubject, UrlConstants.Employee.EMPLOYEE_EMAIL_BODY_PREVIEW, string.Empty, memberEmail.Subject, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                                    //*********************END***********************************
                                }


                            }
                            if (user != null)
                            {
                                if (Roles.IsUserInRole(user.UserName, MemberType.Candidate.ToString()))
                                {
                                    hlnkEmailBody.Text = body;
                                    hlnkEmailBody.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                                    hlnkEmailBody.Target = "_blank";
                                    //ControlHelper.SetHyperLink(hlnkEmailBody, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, body, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                                }
                                else if (Roles.IsUserInRole(user.UserName, MemberType.Employee.ToString()))
                                {
                                    hlnkEmailBody.Text = body;
                                    hlnkEmailBody.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                                    hlnkEmailBody.Target = "_blank";
                                    //ControlHelper.SetHyperLink(hlnkEmailBody, UrlConstants.Employee.EMPLOYEE_EMAIL_BODY_PREVIEW, string.Empty, body, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                                }

                            }
                        }
                      }
                    }
                    hdfMemberEmailId.Value = memberEmail.Id.ToString();
                    if ((int)EmailType.Sent == memberEmail.EmailTypeLookupId && base.CurrentMember.Id == memberEmail.SenderId)
                        lblEmailType.Text =EmailType.Sent.ToString ();
                    else if ((int)EmailType.Sent == memberEmail.EmailTypeLookupId && base.CurrentMember.Id == memberEmail.ReceiverId)
                        lblEmailType.Text =EmailType.Received.ToString ();
                    else
                        lblEmailType.Text = MiscUtil.GetLookupNameById(memberEmail.EmailTypeLookupId, Facade);
                    string stus = "";
                    if (EmailFromType == (EmailFromType)EmailFromType.EmployeeEmail)
                    {
                        GetMemberId();
                        
                        if (_memberId > 0)
                        {
                            if ((int)EmailType.Received == memberEmail.EmailTypeLookupId && _memberId == memberEmail.SenderId)
                            {
                                lblEmailType.Text = EmailType.Sent.ToString();
                                stus = "Sent";
                            }
                            else if ((int)EmailType.Sent == memberEmail.EmailTypeLookupId && _memberId != memberEmail.SenderId)
                            {
                                lblEmailType.Text = EmailType.Received.ToString();
                                stus = "Received";
                            }
                            else
                                lblEmailType.Text = MiscUtil.GetLookupNameById(memberEmail.EmailTypeLookupId, Facade);
                        }
                    }
                    lblDataTime.Text = memberEmail.SentDate.ToString();
                    lblSent.Text = memberEmail.SenderEmail;
                    lblReceived.Text = memberEmail.ReceiverEmail;
                    lblMemberEmailId.Text = "<input id='rdoMemberEmailId' name='rdoMemberEmailId' type='radio' value='" + memberEmail.Id + "," + stus+ "' style='width:10px;'/>"; 


                }
            }
        }
     
        protected void lsvEmail_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvEmail.FindControl("pagerControl");
            if (PagerControl != null)
            {

                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "AllEmailRowPerPage";
            }
            PlaceUpDownArrow();
        }
        protected void lsvEmail_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC")  hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteMemberEmailById(id))
                        {
                            BindList();
                            MiscUtil.ShowMessage(lblMessage, "Email has been successfully deleted.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }

        #endregion

        #endregion

        #region ObjectDataSource Event

        protected void odsEmailList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (EmailFromType ==(EmailFromType ) EmailFromType .CompanyEmail )
            {
                if (status ==true )
                {
                    e.InputParameters["emailTypeLookupId"] = EmailTypeLookupId.ToString();
                    e.InputParameters["senId"] = _contactId.ToString(); 
                    e.InputParameters["AnyKey"] =MiscUtil .RemoveScript ( txtSearchEmail.Text);
                }
                else
                {
                    e.InputParameters["emailTypeLookupId"] = EmailTypeLookupId.ToString();
                    e.InputParameters["companyId"] = companyID.ToString();
                    e.InputParameters["AnyKey"] =MiscUtil .RemoveScript ( txtSearchEmail.Text);
                }
            }
            else if (EmailFromType ==(EmailFromType ) EmailFromType .EmailHistory )
            {
                e.InputParameters["emailTypeLookupId"] = EmailTypeLookupId.ToString();
                GetMemberId();
                e.InputParameters["senId"] = _memberId.ToString();
                e.InputParameters["AnyKey"] =MiscUtil .RemoveScript ( txtSearchEmail.Text);
            }
            else
            {
                e.InputParameters["emailTypeLookupId"] = EmailTypeLookupId.ToString();
                GetMemberId();
                e.InputParameters["senId"] = _tomemberId.ToString();
                e.InputParameters["AnyKey"] =MiscUtil .RemoveScript ( txtSearchEmail.Text);
            }
            
        }
        #endregion
        protected void ddlcontact_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlcontact.SelectedIndex !=0)
            {
                string selMailAddress = ddlcontact.SelectedItem.Text.Substring((ddlcontact.SelectedItem.Text.IndexOf('[') + 1), ((ddlcontact.SelectedItem.Text.LastIndexOf(']') - ddlcontact.SelectedItem.Text.IndexOf('[')) - 1));
                ddlcontact.SelectedItem.Attributes.Add("title", ddlcontact.SelectedItem.Text);
                Member mem = new Member();
                mem  = Facade.GetMemberByMemberEmail(selMailAddress);
                _contactId = mem.Id;
                status = true;
                lsvEmail.DataSourceID = "odsEmailList";
                lsvEmail.DataBind();
            }
            else
            {
                status = false;
                lsvEmail.DataSourceID = "odsEmailList";
                lsvEmail.DataBind();
            }
        }
       
}
}