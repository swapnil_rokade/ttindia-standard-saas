﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CompanyContact.ascx.cs
    Description: This is the user control page used for company contact information
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                27/May/2016         pravin khot          date validation true on page load
 *  0.2                15/May/2017         Sumit Sonawane     Issue Id 1309

 * -------------------------------------------------------------------------------------------------------------------------------------------        
  */

using System;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections.Generic;
using System.Collections;
using TPS360.BusinessFacade;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using TPS360.Web.UI.Helper ;
using System.Linq;
namespace TPS360.Web.UI
{
    public partial class cltRequisitionEditor : RequisitionBaseControl
    {
        #region Member Variables
       
        private string _requisitionType;
        private string _ApplicationEdition
        {
            get
            {
                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString ()] != null)
                    return SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString();
                else
                    return string.Empty;
            }
        }
        public string RequisitionType
        {
            set
            {
                this._requisitionType = value;
            }
        }

        private string DefaultCountryCode
        {
            get
            {
                return SiteSetting[DefaultSiteSetting.Country.ToString()].ToString();
            }           
        }
        #endregion
        
        #region Methods
        private void PostbackCalls()
        {           
            PopulateBUContactList();          
          //  ControlHelper.SelectListByValue(ddlBUContact, hdnSelectedContactID.Value);
        }
        private void PopulateFormData(JobPosting jobPosting)
        {
            string date;
            if (!jobPosting.IsNew)
            {
                if (jobPosting.StartDate != "")
                {
                    if (Convert.ToDateTime(jobPosting.StartDate) > DateTime.Now.Date)
                    {
                        cmpStartDate.ValueToCompare = DateTime.Now.ToShortDateString();
                        cmpOpenDate.ValueToCompare = DateTime.Now.ToShortDateString();
                        cmbopenwithExpectedFullfillment.ValueToCompare = DateTime.Now.ToShortDateString();
                    }
                    else
                    {
                        cmpStartDate.ValueToCompare = jobPosting.StartDate;
                        cmpOpenDate.ValueToCompare = jobPosting.StartDate;
                        cmbopenwithExpectedFullfillment.ValueToCompare = jobPosting.StartDate; 
                    }
                }
                else
                {
                    cmpStartDate.ValueToCompare = DateTime.Now.ToShortDateString();
                    cmpOpenDate.ValueToCompare = DateTime.Now.ToShortDateString();
                    cmbopenwithExpectedFullfillment.ValueToCompare = DateTime.Now.ToShortDateString();
                }
               

                cmpStartDate.Enabled = false;
                txtJobTitle.Text =MiscUtil .RemoveScript ( jobPosting.JobTitle,string .Empty );
                if (jobPosting.NoOfOpenings == 0)
                {
                    txtNoOfOpenings.Text = "1";
                    Session["txtNoOfOpeningsVal"] = 1;
                }
                else
                {
                    Session["txtNoOfOpeningsVal"] = jobPosting.NoOfOpenings;
                    txtNoOfOpenings.Text = MiscUtil .RemoveScript (jobPosting.NoOfOpenings.ToString(),string .Empty );
                }
                if (string.IsNullOrEmpty(jobPosting.StartDate) || jobPosting.StartDate == "ASAP") //1.3
                {
                    chkStartDate.Checked = true;
                    //wdcStartDate.Enabled = false;
                }
                else
                {
                    chkStartDate.Checked = false;
                     Country coun=null;
                    int countryid= Convert .ToInt32 (SiteSetting [ DefaultSiteSetting .Country.ToString () ].ToString ());
                    coun=Facade .GetCountryById (countryid );
                    if (coun != null)
                    {
                        if (coun.Name == "India")
                        {
                            if (jobPosting.StartDate.ToString() == "" || jobPosting.StartDate.ToString().Trim() == "ASAP")
                                wdcStartDate.Value = jobPosting.StartDate.ToString();
                            else
                            {
                                date = jobPosting.StartDate.ToString().Split('/')[1] + "/" + jobPosting.StartDate.ToString().Split('/')[0] + "/" + jobPosting.StartDate.ToString().Split('/')[2];
                                wdcStartDate.Value = Convert.ToDateTime(jobPosting.StartDate);
                            }

                        }
                        else
                        {
                            wdcStartDate.Value = jobPosting.StartDate;                           
                        }
                    }
                    else wdcStartDate.Value = jobPosting.StartDate;
                }

                if (jobPosting.IsNew)
                {
                    SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                    if (siteSetting != null)
                    {
                        Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                        jobPosting.FinalHiredDate = DateTime.Now.AddDays(Convert.ToInt32(siteSettingTable[DefaultSiteSetting.FinalHireCompletionTimeframe.ToString()]));
                    }
                }
                else
                {
                    wdcClosingDate.Value = jobPosting.FinalHiredDate;
                }

                txtExpRequiredMin.Text =MiscUtil .RemoveScript ( jobPosting.MinExpRequired,string .Empty );
                txtExpRequiredMax.Text = MiscUtil .RemoveScript (jobPosting.MaxExpRequired,string .Empty );
                uclEducationList.SelectedItems = jobPosting.RequiredDegreeLookupId;
                //if (!string.IsNullOrEmpty(jobPosting.RequiredDegreeLookupId ))
                //{
                //    string RequiredDegree = jobPosting.RequiredDegreeLookupId .ToString();
                //    string[] DegreeRequired = new string[20];
                //    char[] splitter = { ',' };
                //    DegreeRequired = RequiredDegree.Split(splitter);

                //    for (int i = 0; i < ddlQualification .Items .Count ; i++)
                //    {
                //        for (int x = 0; x < DegreeRequired.Length; x++)
                //        {
                //            if (ddlQualification.Items[i].Value == DegreeRequired[x].ToString())
                //            {
                //                ddlQualification.Items[i].Selected = true;
                //            }
                //        }
                //    }
                //}
                uclCountryState.SelectedCountryId = jobPosting.CountryId;
                uclCountryState.SelectedStateId = jobPosting.StateId;
                //txtCity.Text = MiscUtil .RemoveScript (jobPosting.City,string .Empty );
                txtZipCode.Text = MiscUtil .RemoveScript (jobPosting.ZipCode,string .Empty );
                txtSalary.Text = MiscUtil.RemoveScript(jobPosting.PayRate.ToString(), string.Empty);
                ddlSalaryCurrency.SelectedValue = jobPosting.PayRateCurrencyLookupId.ToString();
                ddlRequisitionType.SelectedValue = jobPosting.RequisitionType;

                //ddlbranch.SelectedValue = jobPosting.RequestionBranch;
                //ddlgrade.SelectedValue = jobPosting.RequestionGrade;
            }
            else
            {
                txtNoOfOpenings.Text = "1";
            }
          
            txtClientJobId.Text  =  jobPosting.ClientJobId==null?"" :MiscUtil .RemoveScript ( jobPosting .ClientJobId ,string .Empty );

            PopulateSkill(jobPosting.JobSkillLookUpId);
            uclCountryState.SelectedStateId = jobPosting.StateId;
            if (!string.IsNullOrEmpty(Convert.ToString(jobPosting.OpenDate)))
            {
                wdcOpenDate.Value = jobPosting.OpenDate;
            }
            if (jobPosting.SalesGroupLookUpId != 0)
                ControlHelper.SelectListByValue(ddlSalesGroup, jobPosting.SalesGroupLookUpId.ToString());
            if (jobPosting.SalesRegionLookUpId != 0)
                ControlHelper.SelectListByValue(ddlSalesRegion, jobPosting.SalesRegionLookUpId.ToString());
            if (jobPosting.JobLocationLookUpID != 0)
                ControlHelper.SelectListByValue(ddlJobLocation, jobPosting.JobLocationLookUpID.ToString());
            if (jobPosting.ClientId != 0)
                ControlHelper.SelectListByValue(ddlBU, jobPosting.ClientId.ToString());
            if (jobPosting.JobCategoryLookupId != 0)
                ControlHelper.SelectListByValue(ddlJbCategory, jobPosting.JobCategoryLookupId.ToString());
            if (jobPosting.City != "")
            {
                //if (isMainApplication)
                //{
                //    txtCity.Text = MiscUtil.RemoveScript(jobPosting.City, string.Empty);
                //}
                //else {
                //    ControlHelper.SelectListByText(ddlCity, jobPosting.City);
                //}

                switch (ApplicationSource)
                {
                    case ApplicationSource.LandTApplication:
                        ControlHelper.SelectListByText(ddlCity, jobPosting.City);
                        break;
                    case ApplicationSource.MainApplication:
                        txtCity.Text = MiscUtil.RemoveScript(jobPosting.City, string.Empty);
                        break;
                    case ApplicationSource.SelectigenceApplication:
                        txtCity.Text = MiscUtil.RemoveScript(jobPosting.City, string.Empty);
                        break;
                    default:
                        txtCity.Text = MiscUtil.RemoveScript(jobPosting.City, string.Empty);
                        break;
                }
            }
                
            PopulateBUContactList();
            //ControlHelper.SelectListByValue(ddlBUContact, jobPosting.ClientContactId.ToString());
           // hdnSelectedContactID.Value = ddlBUContact.SelectedValue;

            if (jobPosting.ClientContactId > 0)
            {
                CompanyContact contact = Facade.GetCompanyContactById(jobPosting.ClientContactId);
                if (contact != null)
                {
                    hdnSelectedContactText.Value = txtBUcontact.Text = contact.FirstName + " " + contact.LastName;
                    hdnSelectedContactValue.Value = jobPosting.ClientContactId.ToString();

                }
            }

            ddlBUContacts.Visible = true;
            ddlBUContacts.Items.Clear();
            ddlBUContacts.DataSource = Facade.GetAllBUContactIdByBUId(Convert.ToInt32(ddlBU.SelectedValue));
            ddlBUContacts.DataTextField = "Name";
            ddlBUContacts.DataValueField = "Id";
            ddlBUContacts.DataBind();
            ddlBUContacts = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlBUContacts);
            ddlBUContacts.Items.Insert(0, new ListItem("Select " + lblBUContacts.Text, "0"));                      

            if (jobPosting.ClientContactId != 0)
                ControlHelper.SelectListByValue(ddlBUContacts, jobPosting.ClientContactId.ToString());
      
        }

        public void PopulateSkill(string skillids)
        {
            hfSkillSetNames.Value = "";
            IList<Skill> sk = SplitSkillValues(skillids, '!');
            foreach (Skill s in sk)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl div = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                //div.InnerHtml = "<span>" + s.Name + "</span><button type='button' class='close' data-dismiss='alert'  onclick='javascript:Clicked(\"" + s.Name + "\")'>\u00D7</button>";
				div.InnerHtml = "<span>" + s.Name + "</span><button type='button' class='close' data-dismiss='alert'  onclick='javascript:Clicked(\"" + s.Name.Replace("&", "&amp;").Replace("\"","~") + "\")'>\u00D7</button>";
                div.Attributes.Add("class", "EmailDiv");
                divContent.Controls.AddAt(0, div);
                //hfSkillSetNames.Value += "<item><skill>" + s.Name.Replace("&", "&amp;") + "</skill></item>";
				hfSkillSetNames.Value += "<item><skill>" + s.Name.Replace("&", "&amp;").Replace("\"" ,"~")+ "</skill></item>";
            }
        }
        public void postbackCall()
        {

            //string skilllistdy = hfSkillSetNames.Value;
			string skilllistdy = hfSkillSetNames.Value.Replace("amp;", "").Replace("~","\"");
            string mn = Facade.Skill_GetandAddSkillIds("<SkillList>" + skilllistdy.Replace("&", "&amp;") + "</SkillList>", base.CurrentMember.Id);
            if (mn.Trim().EndsWith("!")) mn = mn.Substring(0, mn.Length - 1);
            PopulateSkill(mn);

           // rfvcontactemail.Enabled = false;
           // rfvBUcontact.Visible  = false;
            //if (chkSalary .Checked )
            //{
            //    this.chkSalary.Checked = true;
            //    txtSalary.Enabled = false;
            //    txtMaxPayRate.Enabled = false;
            //    ddlSalary.Enabled = false;
            //    ddlSalaryCurrency.Enabled = false;
            //}
            //else
            //{
            //    txtSalary.Enabled = true;
            //    txtMaxPayRate.Enabled = true;
            //    ddlSalary.Enabled = true;
            //    ddlSalaryCurrency.Enabled = true;
            //    this.chkSalary.Checked = false;
            //    if (ddlSalaryCurrency.SelectedItem.Text == "INR")
            //    {
            //        lblPayrateCurrency.Visible = true;
            //        lblPayrateCurrency.Text = " Lacs";
            //    }
            //    else
            //        lblPayrateCurrency.Text = "";
            //}
            PopulateBUContactList();
            //ControlHelper.SelectListByValue(ddlBUContact, hdnSelectedContactID.Value);

            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;

        }
        private IList<Skill >  SplitSkillValues(string value, char delim)
        {
            IList<Skill> result = new List<Skill>();
            char[] delimiters = new char[] { delim, '\n' };
            IList<string> splitarray = value.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in splitarray)
            {
                if (Convert.ToInt32(s) > 0)
                {
                    Skill sk = new Skill();
                    sk.Id = Convert.ToInt32(s);
                    sk.Name = Facade.GetSkillById(Convert.ToInt32(s)).Name;
                    result.Add(sk);
                }
            }
            return result;
        }
        public JobPosting BuildJobPosting(JobPosting jobPosting)
        {
            jobPosting.JobTitle = MiscUtil.RemoveScript(txtJobTitle.Text.Trim());
            //jobPosting.JobCategorySubId = 3;
            //jobPosting.JobCategoryLookupId = 0; 
            // jobPosting.JobIndustryLookupId = 0;
            jobPosting.NoOfOpenings = Int32.Parse(string.IsNullOrEmpty(txtNoOfOpenings.Text.Trim()) ? "0" : txtNoOfOpenings.Text.Trim());                //1.1
            //wdcStartDate.Enabled = !chkStartDate.Checked;
            jobPosting.StartDate = (chkStartDate.Checked) ? "ASAP" : wdcStartDate.Text.Trim() == "" ? "" : Convert.ToDateTime(wdcStartDate.Value).ToString("MM/dd/yyyy");
            //jobPosting.OpenDate = Convert.ToDateTime(wdcOpenDate.Value).ToString("MM/dd/yyyy");

            if (!string.IsNullOrEmpty(wdcClosingDate.Text) && wdcClosingDate.Text != "Null")
            {
                jobPosting.FinalHiredDate = DateTime.Parse(wdcClosingDate.Text);
            }
            else
                jobPosting.FinalHiredDate = DateTime.MinValue;

            jobPosting.MinExpRequired = MiscUtil.RemoveScript(txtExpRequiredMin.Text.Trim());
            jobPosting.MaxExpRequired = MiscUtil.RemoveScript(txtExpRequiredMax.Text.Trim());
            string Qualification = uclEducationList.SelectedItems;
            //for (int i = 0; i < ddlQualification .Items .Count ; i++)
            //{
            //    if (ddlQualification.Items[i].Selected)
            //    {
            //        Qualification += "," + ddlQualification.Items[i].Value;
            //    }
            //}
            jobPosting.RequiredDegreeLookupId = Qualification == string.Empty ? string.Empty : Qualification + ",";
            string Duration = string.Empty;
            jobPosting.JobDurationLookupId = Duration == string.Empty ? string.Empty : Duration + ",";
            jobPosting.JobDurationMonth = "";

            jobPosting.JobAddress1 = "";
            jobPosting.JobAddress2 = "";
            jobPosting.CountryId = uclCountryState.SelectedCountryId;
            jobPosting.StateId = uclCountryState.SelectedStateId;
            jobPosting.ZipCode = MiscUtil.RemoveScript(txtZipCode.Text.Trim());
            jobPosting.PayRate = MiscUtil.RemoveScript(txtSalary.Text.Trim());
            jobPosting.PayRateCurrencyLookupId = Int32.Parse(ddlSalaryCurrency.SelectedValue);
            //Work Authorization
            jobPosting.AuthorizationTypeLookupId = "";
            jobPosting.TravelRequired = false;
            jobPosting.TravelRequiredPercent = "0";
            jobPosting.ClientJobId = MiscUtil.RemoveScript(txtClientJobId.Text);
            jobPosting.OtherBenefits = ""; ;
            jobPosting.TeleCommunication = false;
            jobPosting.BUContactText = MiscUtil.RemoveScript(txtContactEmail.Text);
            if (jobPosting.IsNew)
            {
                IList<GenericLookup> RequisitionStatusList = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.RequisitionStatus, JobStatus.Draft.ToString());
                if (RequisitionStatusList != null)
                    jobPosting.JobStatus = RequisitionStatusList[0].Id;
            }
            jobPosting.PostedDate = DateTime.Now;
            jobPosting.IsJobActive = false;
            jobPosting.IsTemplate = false;

            if (_requisitionType == UIConstants.TEMPLATE_REQUISITION)
            {
                jobPosting.IsTemplate = true;
            }
            if(jobPosting.CreatorId==0)
                jobPosting.CreatorId = base.CurrentMember.Id;
            jobPosting.UpdatorId = base.CurrentMember.Id;
            jobPosting.IsExpensesPaid = false;
            jobPosting.RequisitionSource = string.Empty;
            jobPosting.EmailSubject = string.Empty;
            jobPosting.RawDescription = string.Empty;


            if (!string.IsNullOrEmpty(wdcOpenDate.Text) && wdcOpenDate.Text != "Null")
            {
                jobPosting.OpenDate = DateTime.Parse(wdcOpenDate.Text);
            }
            if (ddlSalesRegion.SelectedIndex != 0)
            {
                jobPosting.SalesRegionLookUpId = Convert.ToInt32(ddlSalesRegion.SelectedValue);
            }
            if (ddlSalesGroup.SelectedIndex != 0)
            {
                jobPosting.SalesGroupLookUpId = Convert.ToInt32(ddlSalesGroup.SelectedValue);
            }
            if (ddlJbCategory.SelectedIndex != 0)
            {
                jobPosting.JobCategoryLookupId = Convert.ToInt32(ddlJbCategory.SelectedValue);
            }
            if (ddlBU.SelectedIndex != 0)
            {
                jobPosting.ClientId = Convert.ToInt32(ddlBU.SelectedValue);

            }
            if (ddlJobLocation.SelectedIndex != 0)
            {
                jobPosting.JobLocationLookUpID = Convert.ToInt32(ddlJobLocation.SelectedValue);
            }
            //if (isMainApplication)
            //{
            //    jobPosting.City = MiscUtil.RemoveScript(txtCity.Text.Trim());

            //}
            //else
            //{
            //    if (ddlCity.SelectedIndex != 0)
            //    {
            //        jobPosting.City = ddlCity.SelectedItem.ToString();
            //    }
            //}


            switch (ApplicationSource)
            {
                case ApplicationSource.LandTApplication:
                    if (ddlCity.SelectedIndex != 0)
                    {
                        jobPosting.City = ddlCity.SelectedItem.ToString();
                    }
                    break;
                case ApplicationSource.MainApplication:
                    jobPosting.City = MiscUtil.RemoveScript(txtCity.Text.Trim());
                    break;
                default:
                    jobPosting.City = MiscUtil.RemoveScript(txtCity.Text.Trim());
                    break;
            }

            //********Code modify by pravin khot on 8/Aug/2016**************
            //jobPosting.ClientContactId = getContactID();// Convert.ToInt32(hdnSelectedContactID.Value == "" ? "0" : hdnSelectedContactID.Value);
            jobPosting.ClientContactId = Convert.ToInt32(ddlBUContacts.SelectedValue);
            //***********************END**********************************

            //string skilllistdy = hfSkillSetNames.Value;
			string skilllistdy = hfSkillSetNames.Value.Replace("~","\"");
            string mn = Facade.Skill_GetandAddSkillIds("<SkillList>" + skilllistdy.Replace("&", "&amp;") + "</SkillList>", base.CurrentMember.Id);

            if (mn.Trim().EndsWith("!")) mn = mn.Substring(0, mn.Length - 1);
            jobPosting.JobSkillLookUpId = mn;
            jobPosting.RequisitionType = ddlRequisitionType.SelectedValue;

            //jobPosting.RequestionBranch = ddlbranch.SelectedValue;
            //jobPosting.RequestionGrade = ddlgrade.SelectedValue;

            return jobPosting;
        }

        public int getContactID()
        {
            if (hdnSelectedContactText.Value.Trim() == txtBUcontact.Text.Trim()) return Convert.ToInt32(hdnSelectedContactValue.Value==string .Empty ?"0":  hdnSelectedContactValue .Value );
            else
            {
                Random randno=new Random ();
               
                string placeholderemail=randno .Next () + "@placeholder.com";
                if (txtContactEmail.Text != string.Empty) placeholderemail = txtContactEmail.Text;
                System.Web.Security.MembershipUser  user = System.Web.Security.Membership.GetUser(placeholderemail);
                while (user != null)
                {
                    placeholderemail = randno.Next() + "@placeholder.com";
                    user = System.Web.Security.Membership.GetUser(placeholderemail);
                }
               // user = System.Web.Security.Membership.CreateUser(placeholderemail, "changeme");
                char [] delim={' '};
                string[] cname = txtBUcontact.Text.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                string firstname = "";
                string lastname = "";
                if (cname.Length == 1)
                {
                    firstname = cname[0];
                }
                else
                {
                    firstname = cname[0];
                    lastname = txtBUcontact.Text.Trim().Substring(txtBUcontact.Text.Trim().IndexOf(' '));
                }
                
               int newmemberid= RegisterUser(placeholderemail,firstname ,lastname );
              int companyContactId= BuildContact(firstname ,lastname , Convert.ToInt32(ddlBU.SelectedValue), Convert.ToInt32(newmemberid));
              hdnSelectedContactText.Value = txtBUcontact.Text;
              hdnSelectedContactValue.Value = companyContactId.ToString();
              txtContactEmail.Text = "";
              return companyContactId;
            }
        }
        private int BuildContact(string firstname,string lastname,int companyid,int memberid)
        {
            CompanyContact companyContact = new CompanyContact();
            companyContact.FirstName = MiscUtil.RemoveScript(firstname);
            companyContact.LastName = MiscUtil.RemoveScript(lastname );
            companyContact.Title = "";
            companyContact.Email = txtContactEmail.Text == string.Empty ? "" : txtContactEmail.Text;
            companyContact.Address1 = "";
            companyContact.Address2 = "";
            companyContact.CountryId = 0;
            companyContact.StateId = 0;
            companyContact.City = "";
            companyContact.ZipCode = "";
            companyContact.OfficePhone = "";
            companyContact.OfficePhoneExtension = "";
            companyContact.MobilePhone = "";
            companyContact.Fax = "";
            companyContact.IsPrimaryContact = false;
            companyContact.NoBulkEmail = false;
            companyContact.ContactRemarks = "";
            companyContact.CreatorId = base.CurrentMember.Id;
            companyContact.UpdatorId = base.CurrentMember.Id;
            companyContact.IsOwner = true;
            companyContact.OwnershipPercentage = 0;
            companyContact.EthnicGroupLookupId = 0;
            companyContact.CompanyId = companyid;
            companyContact.MemberId = memberid;
            companyContact = Facade.AddCompanyContact(companyContact);
            return companyContact.Id;
        }

        private int RegisterUser(string email,string firstname,string lastname)
        {
            string strMessage = String.Empty;
            bool IsCompany = false;
            SecureUrl url;
            string SecureURL;
            string _role = "Department Contact";
            if (IsValid)
            {
                if (_role != "")
                {
                    try
                    {
                        System.Web.Security.MembershipUser newUser;
                        newUser = System.Web.Security.Membership.CreateUser(email , "vendoraccess",email .ToLower ());
                        if (newUser.IsApproved == true)
                        {
                            if (!System.Web .Security .Roles.RoleExists(_role))
                            {
                                System.Web.Security.Roles.CreateRole(_role);
                                CustomRole ro = Facade.GetCustomRoleByName(_role);
                                if (ro == null)
                                {
                                    CustomRole rol = new CustomRole();
                                    rol.Name = _role;
                                    rol.CreatorId = base.CurrentMember.Id;
                                    Facade.AddCustomRole(rol);
                                }
                            }
                            try
                            {
                                System.Web.Security.Roles.AddUserToRole(newUser.UserName, _role);

                            }
                            catch
                            {
                            }
                            Member newMember = new Member();
                            newMember.UserId = (Guid)newUser.ProviderUserKey;
                            newMember.ResumeSource = Convert.ToInt32(ResumeSource.SelfRegistration);
                            newMember.PermanentCountryId = 0;
                            newMember.PermanentStateId = 0;
                            newMember.IsRemoved = false;
                            newMember.AutomatedEmailStatus = true;
                            newMember.FirstName = firstname ;
                            newMember.LastName = lastname ;
                            newMember.DateOfBirth = DateTime.MinValue;
                            newMember.PrimaryEmail = email;
                            newMember.Status = (int)MemberStatus.Active;
                         
                                IsCompany = true;
                                newMember.ResumeSource = (int)ResumeSource.Client;
                           
                            newMember.CreatorId = base.CurrentMember.Id;
                            newMember.CreateDate = DateTime.Now;
                            newMember.UpdatorId = base.CurrentMember.Id;
                            newMember.UpdateDate = DateTime.Now;
                            newMember = Facade.AddMember(newMember);
                            
                          
                          
                                MemberDetail newMemberDetail = new MemberDetail();
                                newMemberDetail.MemberId = newMember.Id;
                                newMemberDetail.CurrentStateId = 0;
                                newMemberDetail.CurrentCountryId = 0;
                                newMemberDetail.GenderLookupId = 0;
                                newMemberDetail.EthnicGroupLookupId = 0;
                                newMemberDetail.MaritalStatusLookupId = 0;
                                newMemberDetail.NumberOfChildren = 0;
                                newMemberDetail.BloodGroupLookupId = 0;
                                newMemberDetail.AnniversaryDate = DateTime.MinValue;

                                Facade.AddMemberDetail(newMemberDetail);

                                //--Trace
                                MemberExtendedInformation memberExtended = new MemberExtendedInformation();
                                memberExtended.MemberId = newMember.Id;
                                memberExtended.Availability = ContextConstants.MEMBER_DEFAULT_AVAILABILITY;
                                Facade.AddMemberExtendedInformation(memberExtended);

                           
                            //  Trace Tech
                            MemberObjectiveAndSummary m_memberObjectiveAndSummary = new MemberObjectiveAndSummary();
                            m_memberObjectiveAndSummary.CreatorId = base.CurrentMember.Id;
                            m_memberObjectiveAndSummary.UpdatorId = base.CurrentMember.Id;
                            m_memberObjectiveAndSummary.CopyPasteResume = "";
                            m_memberObjectiveAndSummary.CreateDate = DateTime.Today;
                            m_memberObjectiveAndSummary.UpdateDate = DateTime.Today;
                            m_memberObjectiveAndSummary.RawCopyPasteResume = "";// StripTagsCharArray(m_memberObjectiveAndSummary.CopyPasteResume);
                            m_memberObjectiveAndSummary.MemberId = newMember.Id;
                            Facade.AddMemberObjectiveAndSummary(m_memberObjectiveAndSummary);


                         

                         
                            MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(newMember.Id);
                            CustomRole role = Facade.GetCustomRoleByName(_role.ToString());
                            int roleid = 0;
                            if (role != null)
                                roleid = role.Id;

                            if (map == null)
                            {
                                map = new MemberCustomRoleMap();
                                map.CustomRoleId = roleid;
                                map.MemberId = newMember.Id;
                                map.CreatorId = base.CurrentMember.Id;

                                Facade.AddMemberCustomRoleMap(map);
                                AddDefaultMenuAccess(newMember.Id, roleid);
                            }
                            else
                            {
                                if (map.CustomRoleId == roleid)
                                {
                                    map = new MemberCustomRoleMap();
                                    map.CustomRoleId = roleid;
                                    map.UpdatorId = base.CurrentMember.Id;
                                    Facade.UpdateMemberCustomRoleMap(map);
                                    AddDefaultMenuAccess(newMember.Id, roleid);
                                }
                            }
                            return newMember.Id;
                        }
                        newUser.IsApproved = false;
                        System.Web .Security .Membership.UpdateUser(newUser);
                        
                    }
                    catch (System.Web.Security.MembershipCreateUserException ex)
                    {

                    }

                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "No role specified.", true);
                }
            }
            return 0;
        }
        private void AddDefaultMenuAccess(int memId, int role)
        {

            ArrayList previlegeList = Facade.GetAllCustomRolePrivilegeIdsByRoleId(role);

            if (previlegeList != null && previlegeList.Count > 0)
            {
                Facade.DeleteMemberPrivilegeByMemberId(memId);

                for (int i = 0; i <= previlegeList.Count - 1; i++)
                {
                    MemberPrivilege previlege = new MemberPrivilege();

                    previlege.CustomSiteMapId = Convert.ToInt32(previlegeList[i]);
                    previlege.MemberId = memId;
                    Facade.AddMemberPrivilege(previlege);
                }
            }
        }
        public void PopulateBUList()
        {

            int CompanyCount = Facade.Company_GetCompanyCount();
            {
                //if (isMainApplication) 
                //{

                //    if (CompanyCount == 0) 
                //    {
                //        ddlBU.Enabled = false;
                //        txtBUcontact.Enabled = false;
                //    }
                //}
                int companyStatus = (int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting);

                //if (CompanyCount <= 500) //condidtion commented by pravin khot on 19/Aug/2016
                //{
                    ddlBU.Visible = true;
                    //txtClientName.Visible = false;
                    ddlBU.Items.Clear();
                    //ddlBU.DataSource = Facade.GetAllClientsByStatusByCandidateId(companyStatus,CurrentMember.Id);
                    ddlBU.DataSource = Facade.GetAllClientsByStatus(companyStatus);
                    ddlBU.DataTextField = "CompanyName";
                    ddlBU.DataValueField = "Id";
                    ddlBU.DataBind();
                    ddlBU = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlBU);
                        ddlBU.Items.Insert(0, new ListItem("Select " + lblBU.Text,"0"));


                        int buid = Convert.ToInt32(ddlBU.SelectedValue);

                        ddlBUContacts.Visible = true;
                        ddlBUContacts.Items.Clear();
                        ddlBUContacts.DataSource = Facade.GetAllBUContactIdByBUId(buid);
                        ddlBUContacts.DataTextField = "Name";
                        ddlBUContacts.DataValueField = "Id";
                        ddlBUContacts.DataBind();
                        ddlBUContacts = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlBUContacts);
                        ddlBUContacts.Items.Insert(0, new ListItem("Select " + lblBUContacts.Text, "0"));                      
                    
                //}
                //else
                //{
                //    ddlBU.Visible = false;
                //    //txtClientName.Visible = true;
                //}
            }
            PopulateBUContactList();
        }

        private void PopulateBUContactList()
        {
            //ddlBUContact.SelectedIndex = 0;


            ArrayList contacts = Facade.GetAllCompanyContactsByCompanyId(ddlBU.Visible ? Convert.ToInt32(ddlBU.SelectedValue == "" ? "0" : ddlBU.SelectedValue) : Convert.ToInt32(hdnSelectedClient.Value == "" ? "0" : hdnSelectedClient.Value));
            CompanyContact con = new CompanyContact();
            con.Id = 0;
            con.FirstName = "Please Select";
            contacts.Insert(0, con);
            //ddlBUContact.DataSource = contacts;
            //ddlBUContact.DataValueField = "Id";
            //ddlBUContact.DataTextField = "FirstName";

            //ddlBUContact.DataBind();
            //ddlBUContact = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlBUContact);
            //if (ddlBUContact.Items.Count == 1) ddlBUContact.Enabled = false;
           // else ddlBUContact.Enabled = true;


        }
        public void PopulateCurrencyLookUp() 
        {
            ddlCurrency.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.Currency);
            ddlCurrency.DataTextField = "Name";
            ddlCurrency.DataValueField = "Id";
            ddlCurrency.DataBind();
        }

        

        private void SaveJobPostingData(JobPosting jobPosting)
        {
            //IList<JobPostingDocument> documents = getAtachedDocumentList(CurrentJobPostingId );
            if (IsValid)
            {
                try
                {
                   BuildJobPosting(jobPosting);
                    
                    if (jobPosting.IsNew)
                    {
                        jobPosting = Facade.AddJobPosting(jobPosting);
                       
                        MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionCreated, jobPosting.Id, CurrentMember.Id, Facade);
                        if (_requisitionType != UIConstants.TEMPLATE_REQUISITION)
                            Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_DESCRIPTION_PAGE, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id), UrlConstants.PARAM_MSG, "Requisition has been added successfully.");     //0.1
                        else
                            Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_TEMPLATE_EDITOR_PAGE, string.Empty, UrlConstants.PARAM_ID, StringHelper.Convert(jobPosting.Id), UrlConstants.PARAM_MSG, "Requisition has been added successfully.");
                    }
                    else
                    {
                        
                        Facade.UpdateJobPosting(jobPosting);
                        MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionEdited, jobPosting.Id, CurrentMember.Id, Facade);
                        MiscUtil.ShowMessage(lblMessage, "Requisition has been updated successfully.", false);
                    }

                }
                catch (ArgumentException ax)
                {
                    MiscUtil.ShowMessage(lblMessage, ax.Message, true);
                }
            }            
        }
        private void PrepareView()
        {            
            MiscUtil.PopulateEducationQualification(uclEducationList .ListItem , Facade);
            uclEducationList.ListItem.Items.RemoveAt(0);
            //ddlQualification.Items.RemoveAt(0);
            MiscUtil.PopulateCurrency(ddlSalaryCurrency, Facade);
            MiscUtil.PopulateSalesRegion(ddlSalesRegion, Facade);
            MiscUtil.PopulateSalesGroup(ddlSalesGroup, Facade);
            MiscUtil.PopulateRequisitionType(ddlRequisitionType, Facade);
            
            //MiscUtil.PopulateRequestionBranch(ddlbranch, Facade);
            //MiscUtil.PopulateRequestionGrade(ddlgrade, Facade);

            //if (isMainApplication)
            //{ MiscUtil.PopulateCategory(ddlJbCategory, Facade); }
            //else { MiscUtil.PopulateJobCategory(ddlJbCategory, Facade); }

            switch (ApplicationSource)
            {
                case ApplicationSource.LandTApplication:
                    MiscUtil.PopulateJobCategory(ddlJbCategory, Facade);
                    break;
                case ApplicationSource.MainApplication:
                    MiscUtil.PopulateCategory(ddlJbCategory, Facade);
                    break;
                default:
                    MiscUtil.PopulateCategory(ddlJbCategory, Facade);
                    break;
            }

            MiscUtil.PopulateJobLocation(ddlJobLocation, Facade);
            MiscUtil.PopulateCity(ddlCity, Facade);           
            GetDefaultsFromSiteSetting();
            PopulateBUList();
            if (DefaultCountryCode != "")
            {
                Country country = Facade.GetCountryById(Convert .ToInt32 ( DefaultCountryCode));
                if (country != null)
                {
                    if (country.Name == "India")
                    {
                        lblZipCode.Text = "PIN Code"; revZIPCode.ErrorMessage = "Please enter a valid PIN code";
                        divZip.Visible = false;
                        divPinCode.Visible = true;
                    }
                }
            }          
        }

        private void GetDefaultsFromSiteSetting()       
        {

            if (SiteSetting != null)
            {
                ddlSalaryCurrency.SelectedValue = SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString();
                //if (ddlSalaryCurrency.SelectedItem.Text == "INR")
                //    lblPayrateCurrency.Text = "(Lacs)";
                ddlSalary.SelectedValue = SiteSetting[DefaultSiteSetting.PaymentType.ToString()].ToString();
            }
            else
            {
                ddlSalaryCurrency.SelectedValue = ddlSalaryCurrency.Items.FindByText("$ USD").Value;
            }
        }
      
        private void DisableValidationControls()
        {
            rfvBU.Enabled = false;
            rfvcontactemail.Enabled = false;
            //if (txtBUcontact.Text == "")
            //{
            //   // rfvBUcontact.Enabled = true;
            //    rfvcontactemail.Enabled = false;
            //}
            //else
            //{
            //    //rfvcontactemail.Enabled = true;
            //   // rfvBUcontact.Enabled = true;
            //}
            rfvBUcontact.Enabled = false;
            RFVBUContacts.Enabled = true;
            rfvnoofopenings.Enabled = true;
            rfvStartDate.Enabled = true;
            rfVOpendDate.Enabled = true;
            rfvClosingDate.Enabled = true;
            rfvCity.Enabled = false;
            uclCountryState.Enabled = true ; //code condition TRUE by pravin khot on 11/Feb/2016
            cvSkill.Enabled = false;
            rfvJobCategory.Enabled = false;
            rfvJobLocation.Enabled = false;
            rfvBillRate.Enabled = false;
            rfvSalesRegion.Enabled = false;
            rfvSalesGroup.Enabled = false;
            cvExperience.Enabled = true;
            trsalesregion.Visible = false;
            trsalesgroup.Visible = false;
            trjoblocation.Visible = false;
                                    
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ////*************added by pravin khot on 27/May/2016*******
            //JobPosting jobPosting = new JobPosting();
            //if (jobPosting.IsNew)
            //{
            //    cmpStartDate.ValueToCompare = DateTime.Now.ToShortDateString();   // 1.2 start
            //    cmpOpenDate.ValueToCompare = DateTime.Now.ToShortDateString();
            //    cmpOpenDate.ValueToCompare = DateTime.Now.ToShortDateString();
            //}
            ////***********************END****************************
            if (ApplicationSource == ApplicationSource.MainApplication || ApplicationSource == ApplicationSource.SelectigenceApplication || ApplicationSource == ApplicationSource.GenisysApplication)
            {
                if (!IsPostBack)
                {
                    Session["txtNoOfOpeningsVal"] = "";
                    if (ddlBU.SelectedIndex == 0 || ddlBU.SelectedIndex == -1)
                    {
                        //divBUContact.Disabled = true;
                        txtBUcontact.Enabled = false;
                    }

                }

                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.STAFFING_FIRM)
                {
                    lblBU.Text = "Account";
                    lblBUContact.Text = "Account Contact";
                    //divClientBudget.Visible = true;
                    lblBUContacts.Text = "Account Contact";
                    PopulateCurrencyLookUp();
                }

                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                {
                    lblBU.Text = "Account";
                    lblBUContact.Text = "Account Contact";
                    lblBUContacts.Text = "Account Contact";
                }


                DisableValidationControls();
                hdnApplicationType.Value = "";
                dvBillrate.Visible = false;
                ddlCity.Visible = false;
                txtCity.Visible = true;

                //CODE MODIFY BY PRAVIN KHOT ON 27/May/2016 FALSE TO TRUE
                //cmpStartDate.Enabled = false;
                //cmpOpenDate.Enabled = false;
                cmpStartDate.Enabled = true ;
                cmpOpenDate.Enabled = true;
                //***************END********************

            }
            else if (ApplicationSource == ApplicationSource.MainApplication || ApplicationSource == ApplicationSource.SelectigenceApplication || ApplicationSource == ApplicationSource.GenisysApplication)
            {
                ddlCity.Visible = true;
                txtCity.Visible = false;
                spnTxtCity.Visible = false;
                spnddlCity.Visible = true;
            }
            else if (ApplicationSource == ApplicationSource.SelectigenceApplication)
            {
                if (!IsPostBack)
                {
                    if (ddlBU.SelectedIndex == 0 || ddlBU.SelectedIndex == -1)
                    {
                        //divBUContact.Disabled = true;
                        txtBUcontact.Enabled = false;
                    }

                }

                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.STAFFING_FIRM)
                {
                    lblBU.Text = "Account";
                    lblBUContact.Text = "Account Contact";

                }

                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                {
                    lblBU.Text = "Account";
                    lblBUContact.Text = "Account Contact";

                }


                DisableValidationControls();
                hdnApplicationType.Value = "";
                dvBillrate.Visible = false;
                ddlCity.Visible = false;
                txtCity.Visible = true;
            }
            txtZipCode.Attributes.Add("onblur", "javascript:FilterNumeric('" + txtZipCode.ClientID + "')");

            //code commented by pravin khot on 11/Aug/2016 
            //cmpStartDate.ValueToCompare = DateTime.Now.ToShortDateString ();  
            //cmpOpenDate.ValueToCompare = DateTime.Now.ToShortDateString();
            //*********END************************
            
            ddlSalaryCurrency.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('" + ddlSalaryCurrency.ClientID + "','" + lblPayrateCurrency.ClientID + "')");
            if (!Page.IsPostBack)
            {
                cmpStartDate.ValueToCompare = DateTime.Now.ToShortDateString(); 
                cmpOpenDate.ValueToCompare = DateTime.Now.ToShortDateString();

                wdcStartDate.Value  = DateTime.Now;
                //wdcOpenDate.Value = DateTime.Now;
                //wdcStartDate.Value = string.Empty;
                wdcOpenDate.Value = string.Empty;
                wdcClosingDate.Value = string.Empty;              

                PrepareView();
                if (CurrentJobPostingId > 0) PopulateFormData(CurrentJobPosting);
                string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];
                if (!StringHelper.IsBlank(message))
                {
                    MiscUtil.ShowMessage(lblMessage, message, false);
                }
            }
            if (chkStartDate.Checked)                          
            {
                wdcStartDate.BorderColor = System.Drawing.Color.LightGray;
                //wdcStartDate.DropButton.ImageUrlDisabled = "../Images/ddlGrayOut.jpg";
            }
            else
                wdcStartDate.BorderColor = System.Drawing.Color.Gray;
            if (!string.IsNullOrEmpty(wdcClosingDate.Text) && wdcClosingDate.Text != "Null")
            {
                if (DateTime.Parse(wdcClosingDate.Text ) == DateTime.MinValue)
                    wdcClosingDate.Value = null;
            }

            if (ApplicationSource == ApplicationSource.GenisysApplication||ApplicationSource==ApplicationSource.SelectigenceApplication)
            {
                //*****OLD commented by pravin khot on 27/May/2016
                //cmpOpenDate.Enabled = false;
                //cmbopenwithExpectedFullfillment.Enabled = false;
                ////rfVOpendDate.Enabled = false;
                //cmpStartDate.Enabled = false;
                //cmpDate.Enabled = false;     
                //**************END******************

                //*****ADD by pravin khot on 27/May/2016
                cmpOpenDate.Enabled = true ;
                cmbopenwithExpectedFullfillment.Enabled = true;
                //rfVOpendDate.Enabled = false;
                cmpStartDate.Enabled = true;
                cmpDate.Enabled = true;     
                //**************END******************                     
            
            }          

           // AjaxControlToolkit.Utility.SetFocusOnLoad(ddlBU);

            if (IsPostBack) postbackCall();         
         
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            //if (MiscUtil .RemoveScript ( txtSkillName.Text.Trim()) != string.Empty)
            //{
            //    IList<Skill> AddedSkillList = getSkillList();
            //    var list = from a in AddedSkillList where a.Name.ToString().ToUpper() ==MiscUtil .RemoveScript ( txtSkillName.Text.ToUpper()) select a;
            //    if (list.ToList().Count == 0) addSkill(AddedSkillList);
            //}
            //txtJobTitle.Text = MiscUtil.RemoveScript(txtJobTitle.Text,string .Empty );
            //if (txtJobTitle.Text.ToString() != string.Empty)
            //{
            //    SaveJobPostingData();
                
            //    PopulateFormData();
            //    if (IsValid)       
            //    {
            //        Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_DESCRIPTION_PAGE, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(base.CurrentJobPostingId));
            //    }
            //}
          
        }
        // code modified by Sumit Sonawane on 15/May/2017 for Issue Id 1309
        protected void txtNoOfOpenings_TextChanged(object sender, EventArgs e)
        {
            Session["txtNoOfOpeningsVal"] = txtNoOfOpenings.Text;
        }

        protected void chkStartDate_CheckedChanged(object sender, EventArgs e)
        {
        //    wdcStartDate.Value = "";        
        //    if (!(wdcStartDate.Enabled = !chkStartDate.Checked))
        //    {
        //        wdcStartDate.BorderColor = System.Drawing.Color.LightGray;
        //        //wdcStartDate.DropButton.ImageUrlDisabled = "../Images/ddlGrayOut.jpg";
        //        chkStartDate.Focus();
        //    }
        //    else
        //    {
        //        wdcStartDate.Value = DateTime.Now;
        //        wdcStartDate.BorderColor = System.Drawing.Color.Gray; chkStartDate.Focus();
        //    }
        }
        
        #endregion

        
        private string GetTempFolder(bool App)
        {
                string dir = CurrentUserId.ToString();
                string path = "";
                if (!App)
                {
                    path = System.IO.Path.Combine(UrlConstants.TempDirectory, dir)+ "\\" + "Requisition";
                   
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path) ;
                    }
                }
                else path = UrlConstants.ApplicationBaseUrl + "Temp/" + dir +"/Requisition" ;

               
                return  path ;
        }
        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlBUContacts.SelectedIndex = -1;
            MiscUtil.PopulateBUContactIdByBUId(ddlBUContacts, Facade, Convert.ToInt32(ddlBU.SelectedValue));

        }         

    }
}
