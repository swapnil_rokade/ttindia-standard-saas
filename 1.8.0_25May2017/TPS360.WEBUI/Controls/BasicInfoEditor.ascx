﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: BasicInfoEditor.ascx
    Description: I removed required field validators for addres and city and zipcode and recorrecr the zip formate
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Oct-06-2008          Gopala Swamy J         Defect id: 8956; I removed required field validators for addres and city and zipcode and recorrect the zip formate.
    0.2             Nov-13-2008          Jagadish               Defect id: 8701; Changed the validation expression from "^\d+$" to "[A-Z 0-9 ]*" and added validation group.
    0.3             Apr-06-2009          Jagadish               Defect id: 10191; Restricted zip code text field to 6 characters.
    0.4             10/Jun/2016          Prasanth Kumar G       Introduced LDAP and UserName
---------------------------------------------------------------------------------------------------------------------------------------       
--%>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BasicInfoEditor.ascx.cs"
    Inherits="TPS360.Web.UI.ControlsBasicInfoEditor" %>
    <%@ Register Src ="~/Controls/CountryStatePicker.ascx" TagName ="CountryState" TagPrefix ="ucl" %>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
<script type ="text/javascript" language ="javascript"  src="../js/AjaxVariables.js"></script>
		<script type ="text/javascript" language ="javascript" src="../js/AjaxScript.js" ></script>
		
		<script language="javascript" type="text/javascript">
  function ValidateSourceDescription(sender,args)
    {
    var ddlSource= $("#<%=ddlSource.ClientID %>");
        args .IsValid=true;
//        if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")==2)
//        {
//            if( $('#<%=ddlVendorContact.ClientID %>').prop("selectedIndex")<=0) args .IsValid=false ;
//        }
        if ($(ddlSource).find('option:selected').text().trim() == "Job Portals")
        {
           if( $('#<%=ddlSourceDescription.ClientID %>').prop("selectedIndex")<0) args .IsValid=false ;
           
        }
        else  if ($(ddlSource).find('option:selected').text().trim() == "Employee Referral")
        {
            if( $('#<%=ddlEmployeeReferrer.ClientID %>').prop("selectedIndex")<=0) args .IsValid=false ;
        }
        else 
        {
            if( $('#<%= txtsrcdesc.ClientID %>').val().trim()=="") args .IsValid=false ;
        }
//        if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")>0 && $('#<%=ddlEmployeeReferrer.ClientID %>').prop("selectedIndex")>0 && $('#<%=ddlSourceDescription.ClientID %>').prop("selectedIndex")>0 && $('#<%= txtsrcdesc.ClientID %>').val().trim()=="")
//        {
//           args.IsValid=false;
//        }
        
    }
    
function showHideSource(i)
{
var ddlSource= $("#<%=ddlSource.ClientID %>");
    $("#<%=divEmpReferral.ClientID %>").css("display", "none");     
                   $("#<%=divVendor.ClientID %>").css("display", "none");     
                    $("#<%=txtsrcdesc.ClientID %>").css("display", "none");                   
                    $("#<%=ddlSourceDescription.ClientID %>").css("display", "none");    
                    
                    
                if ($(ddlSource).find('option:selected').text().trim() == "Job Portals") {
                    $("#<%=ddlSourceDescription.ClientID %>").css("display", "");                     
                }
                
                else  if ($(ddlSource).find('option:selected').text().trim() == "Employee Referral")
                {
                $("#<%=divEmpReferral.ClientID %>").css("display", "");     
                }
//                else  if ($(ddlSource).find('option:selected').text().trim() == "Vendor")
//                {
//                   $("#<%=divVendor.ClientID %>").css("display", "");     
//                }
                else {
                     $("#<%=txtsrcdesc.ClientID %>").css("display",""); 
                     if(i==0)
                        $("#<%=txtsrcdesc.ClientID %>").val('');  
                }
}
       
Sys.Application.add_load(function() {
            $("#<%=ddlSource.ClientID %>").change(function() {
           showHideSource(0);
            });
            showHideSource(1);
        });
        
</script>

		<script type ="text/javascript" >
		
		function ValidatePrimaryEmail(source, arguments) 
		{
		    var ch=document .getElementById ("ctl00_cphCandidateMaster_uwtResumeBuilder__ctl0_ucntrlBasicInfo_hdnCheckValidity");
		   
		    if(ch!=null)
    		{
                arguments.IsValid = ch.value=="false" ? false  : true  ; 
            }
        }

		</script>
		<asp:UpdatePanel ID="upBasic" runat ="server" >
		<ContentTemplate >
		
<div style="width:100%">
<asp:HiddenField ID ="hfOldEmail" runat ="server" />
    <div class="TableRow" style="text-align: center">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="FormLeftColumn">
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblFirstName" runat="server" Text="First Name"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtFirstName" runat="server" CssClass="CommonTextBox"
                    ValidationGroup="BasicInfo" onmouseover  ="this.title=this.value;"></asp:TextBox>
                <span class="RequiredField">*</span>
            </div>
        </div>
        <div class="TableRow"  style =" width :100%" >
            <div class="TableFormValidatorContent" style=" margin-left :42%">
                <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                    ErrorMessage="Please enter first name." EnableViewState="False" Display="Dynamic"
                    ValidationGroup="BasicInfo">
                </asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblMiddleName" runat="server" Text="Middle Name"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtMiddleName" runat="server" CssClass="CommonTextBox" onmouseover  ="this.title=this.value;"></asp:TextBox>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblLastName" runat="server" Text="Last Name"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtLastName" runat="server" CssClass="CommonTextBox"
                    ValidationGroup="BasicInfo" onmouseover  ="this.title=this.value;"></asp:TextBox>
                <span class="RequiredField">*</span>
            </div>
        </div>
        <div class="TableRow"  style =" width :100%" >
            <div class="TableFormValidatorContent" style=" margin-left :42%">
                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                    ErrorMessage="Please enter last name." EnableViewState="False" Display="Dynamic"
                    ValidationGroup="BasicInfo">
                </asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblNickName" runat="server" Text="Nick Name"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtNickName" runat="server" CssClass="CommonTextBox" onmouseover  ="this.title=this.value;"></asp:TextBox>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblAddress1" runat="server" Text="Address 1"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtAddress1" runat="server" CssClass="CommonTextBox"
                    ValidationGroup="BasicInfo" onmouseover  ="this.title=this.value;"></asp:TextBox>
            </div>
        </div>
       
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblAddress2" runat="server" Text="Address 2"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtAddress2" onmouseover  ="this.title=this.value;" runat="server" CssClass="CommonTextBox"></asp:TextBox>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblCity" runat="server" Text="City"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtCityB" runat="server" onmouseover  ="this.title=this.value;" CssClass="CommonTextBox"></asp:TextBox>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="true" ID="lblZip" runat="server" Text="Zip Code"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtZipCode" runat="server" CssClass="CommonTextBox"
                    ValidationGroup="BasicInfo" MaxLength="10" onmouseover  ="this.title=this.value;"></asp:TextBox> <%--0.3--%>
            </div>
        </div>
     <div class="TableRow"  style =" width :100%" >
            <div class="TableFormValidatorContent" style=" margin-left :42%">
                <asp:RegularExpressionValidator ID="revZIPCode" runat="server" ControlToValidate="txtZipCode"
                    Display="Dynamic" ErrorMessage="Please enter a valid zip code" ValidationExpression="^[0-9-]*$" ValidationGroup="BasicInfo"></asp:RegularExpressionValidator><%--(\d{6}(-\d{4})?)|(\d{5}(-\d{4})?)--%>
            </div>
        </div>
        
       <%-- <ucl:CountryState ID="uclCountryState" runat="server" FirstOption="Please Select" />--%>
       <ucl:CountryState ID="uclCountryState" runat ="server" FirstOption="Any" ApplyDefaultSiteSetting ="False"  TableFormLabel_Width="40%" />
                   
    </div>
    <div class="FormRightColumn">
     <!-- Code introduced by Prasanth on 10/Jun/2016 Start-->
            <div  id="divUserName" runat="server" visible="false">
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblUsrName" runat="server" Text="User Name"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtUserName" runat="server" 
                            AutoComplete="OFF" CssClass="CommonTextBox"
                            TabIndex="1" ValidationGroup="BasicInfo"></asp:TextBox>
                             <span class="RequiredField">*</span>
                    </div>
                </div>
            </div>
            <div class="TableRow" style =" width :100%" visible="false"></div>
            <div class="TableFormValidatorContent" style=" margin-left :42%" visible="false">
            <asp:CustomValidator ID="cvUserName" runat ="server" EnableViewState ="true" ControlToValidate ="txtUserName" 
            ClientValidationFunction ="ValidatePrimaryEmail" ValidationGroup ="BasicInfo" Display ="Dynamic"
            ErrorMessage ="User Name already exists, please enter a unique User Name" Visible="false"></asp:CustomValidator>
            
            <%-- <div id="divNotAvailableUserName" style =" color :Red ; display : none  " enableviewstate ="true" >User Name already exists, please enter a unique User Name</div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserName"
                    ValidationGroup="BasicInfo" ErrorMessage="Please enter User Name." EnableViewState="False"
                    Display="Dynamic"></asp:RequiredFieldValidator>
               
            </div>--%>
        </div>
            
            <!-- ******************END***************************-->
<!-- Code introduced by Prasanth on 10/Jun/2016-->
             <div  id="divLDAP" runat="server" visible="false">
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblAdLogin" runat="server" Text="LDAP Authentication"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                       <asp:CheckBox ID="ChkLDAP" runat="server" />
                    </div>
                </div>
             </div>
            <!-- ********************END*********************-->
    
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblPrimaryEmail" runat="server" Text="Primary Email"></asp:Label>:
            </div>
            <div class="TableFormContent">
            <asp:HiddenField ID="hdnCheckValidity"  runat ="server" />
                <asp:TextBox EnableViewState="false" ID="txtPrimaryEmail" runat="server" CssClass="CommonTextBox" 
                    ValidationGroup="BasicInfo" onmouseover  ="this.title=this.value;"></asp:TextBox>
                <span class="RequiredField">*</span>
                    
            </div>
        </div>
        <div class="TableRow" style =" width :100%" >
            <div class="TableFormValidatorContent" style=" margin-left :42%">
            <asp:CustomValidator ID="cvPrimaryEmail" runat ="server" EnableViewState ="true" ControlToValidate ="txtPrimaryEmail" ClientValidationFunction ="ValidatePrimaryEmail" ValidationGroup ="BasicInfo" Display ="Dynamic" ErrorMessage ="Email address already exists, please enter a unique email" ></asp:CustomValidator>
             <div id="divNotAvailable" style =" color :Red ; display : none  " enableviewstate ="true" >Email address already exists, please enter a unique email</div>
                <asp:RequiredFieldValidator ID="rfvPrimaryEmail" runat="server" ControlToValidate="txtPrimaryEmail"
                    ValidationGroup="BasicInfo" ErrorMessage="Please enter primary email." EnableViewState="False"
                    Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revPrimaryEmail" runat="server" ControlToValidate="txtPrimaryEmail"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Please enter valid email address."
                    ValidationGroup="BasicInfo" Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblAlternateEmail" runat="server" Text="Alternate Email"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtAlternateEmail" runat="server" CssClass="CommonTextBox"
                    ValidationGroup="BasicInfo" onmouseover  ="this.title=this.value;"></asp:TextBox>
            </div>
        </div>
        <div class="TableRow"  style =" width :100%" >
            <div class="TableFormValidatorContent" style=" margin-left :42%">
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAlternateEmail"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Please enter valid email address."
                    ValidationGroup="BasicInfo" Display="Dynamic">
                </asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblPrimaryPhone" runat="server" Text="Home Phone"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtPrimaryPhone" runat="server" CssClass="CommonTextBox"
                    Width="96px" ValidationGroup="BasicInfo" onmouseover  ="this.title=this.value;"  MaxLength ="15" rel="Integer"></asp:TextBox>
                <asp:Label EnableViewState="false" ID="lblPrimaryPhoneEx" runat="server" Text="Ext."></asp:Label>
                <asp:TextBox EnableViewState="false" ID="txtPrimaryPhoneExt" onmouseover  ="this.title=this.value;" runat="server" CssClass="CommonTextBox"
                    Width="20px"  MaxLength ="4" rel="Integer"></asp:TextBox>
            </div>
        </div>
        <div class="TableRow"  style =" width :100%" >
            <div class="TableFormValidatorContent" style=" margin-left :42%">
                <asp:RegularExpressionValidator ID="revHomePhone" runat="server" Display="Dynamic"
                    ControlToValidate="txtPrimaryPhone" EnableViewState="false" ErrorMessage="Please enter 0-9,(),+- only."
                    ValidationGroup="BasicInfo" ValidationExpression="^([\+][0-9]{1,10}([ \-])?)?([\(]{1}[0-9]{1,5}[\)])?([0-9 \-]{1,32})((x|X)?[0-9]{2,4}?)$"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormValidatorContent" style=" margin-left :42%">
                <asp:RegularExpressionValidator ID="revHomePhoneEx" runat="server" Display="Dynamic"
                    ControlToValidate="txtPrimaryPhoneExt" EnableViewState="false" ErrorMessage="Please enter extension number in 2-4 digits."
                    ValidationGroup="BasicInfo" ValidationExpression="^[0-9]{2,4}$"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblCellPhone" runat="server" Text="Cell Phone"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtCellPhone" onmouseover  ="this.title=this.value;" runat="server" CssClass="CommonTextBox"  MaxLength ="15" rel="Integer"></asp:TextBox>
            </div>
        </div>
        <div class="TableRow"  style =" width :100%" >
            <div class="TableFormValidatorContent" style=" margin-left :42%">
                <asp:RegularExpressionValidator ID="revCellPhone" runat="server" Display="Dynamic"
                    ControlToValidate="txtCellPhone" EnableViewState="false" ErrorMessage="Please enter 0-9,(),+- only."
                    ValidationGroup="BasicInfo" ValidationExpression="^([\+][0-9]{1,10}([ \-])?)?([\(]{1}[0-9]{1,5}[\)])?([0-9 \-]{1,32})((x|X)?[0-9]{2,4}?)$"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblOfficePhone" runat="server" Text="Office Phone" ></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtOfficePhone" runat="server" CssClass="CommonTextBox"
                    Width="96px" onmouseover  ="this.title=this.value;"  MaxLength ="15" rel="Integer"></asp:TextBox>
                <asp:Label EnableViewState="false" ID="lblOfficePhoneEx" runat="server" Text="Ext."></asp:Label>
                <asp:TextBox EnableViewState="false" ID="txtOfficePhoneEx" runat="server" CssClass="CommonTextBox"
                    Width="20px" onmouseover  ="this.title=this.value;"  MaxLength ="4" rel="Integer"></asp:TextBox>
            </div>
        </div>
        <div class="TableRow"  style =" width :100%" >
            <div class="TableFormValidatorContent" style=" margin-left :42%">
                <asp:RegularExpressionValidator ID="revOfficePhone" runat="server" Display="Dynamic"
                    ControlToValidate="txtOfficePhone" EnableViewState="false" ErrorMessage="Please enter 0-9,(),+- only."
                    ValidationGroup="BasicInfo" ValidationExpression="^([\+][0-9]{1,10}([ \-])?)?([\(]{1}[0-9]{1,5}[\)])?([0-9 \-]{1,32})((x|X)?[0-9]{2,4}?)$"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormValidatorContent" style=" margin-left :42%">
                <asp:RegularExpressionValidator ID="revOfficePhoneEx" runat="server" Display="Dynamic"
                    ControlToValidate="txtOfficePhoneEx" EnableViewState="false" ErrorMessage="Please enter extension number in 2-4 digits."
                    ValidationGroup="BasicInfo" ValidationExpression="^[0-9]{2,4}$"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="TableRow" id="divCandidateType" runat ="server" visible ="false"  >
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblApplicantType" runat="server" Text="Candidate Type"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:DropDownList ID="ddlApplicantType" runat="server" CssClass="CommonDropDownList" EnableViewState="true">
                </asp:DropDownList>
            </div>
        </div>
        <div id="divCandidateSource" runat ="server" >
         <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label ID="lblSource" runat="server" Text="Source"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:DropDownList ID="ddlSource" runat="server" CssClass="CommonDropDownList" TabIndex="3">
                </asp:DropDownList>
               <asp:Label ID="lblSourceName" runat ="server" ></asp:Label>
                 <span class="RequiredField">*</span> 
            </div> 
           
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label ID="lblSourceDescription" runat="server" Text="Source Description"></asp:Label>:
            </div>
            <div class="TableFormContent">
            <asp:Label ID="lblSourceDescriptionDetail" runat ="server" ></asp:Label>
            <div id="divVendor"  runat ="server" style =" display : none; float:left ">
                
                <asp:DropDownList ID="ddlVendor" runat ="server" CssClass ="chzn-select" Width ="90px"  ></asp:DropDownList>
                <asp:DropDownList ID="ddlVendorContact" runat ="server"  CssClass ="chzn-select" Width ="90px"  ></asp:DropDownList>
                <asp:HiddenField ID="hdnSelectedVendorContact" runat ="server"  Value ="0" />
                <asp:HiddenField runat="server" ID="hdntemp" Value="0" />
            </div>
            <div id="divEmpReferral" runat ="server" style =" display : none; float:left " >
                <asp:DropDownList ID="ddlEmployeeReferrer"  runat ="server"  CssClass ="chzn-select" Width ="150px" ></asp:DropDownList>
            </div>
                <asp:DropDownList ID="ddlSourceDescription" runat="server"  CssClass="CommonDropDownList" style=" display :none "
                    TabIndex="3">
                </asp:DropDownList>
                 <asp:TextBox ID="txtsrcdesc" runat="server" CssClass="CommonTextBox" TabIndex="1"  style="display:none"></asp:TextBox>
                     <span class="RequiredField">*</span>       
                             </div>
         
        </div>
        <div class ="TableRow">
            <div class="TableFormValidatorContent" style="margin-left: 42%">
                <asp:CustomValidator runat="server" Display="Dynamic" ValidationGroup="BasicInfo" ID="CVSourceDescription" ErrorMessage="Please Select Source Description" ClientValidationFunction="ValidateSourceDescription"></asp:CustomValidator>
            </div>
        </div>
        </div>
    </div>
    <div class="TableRow" style="text-align: center; padding-bottom : 20px;">
        <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" OnClick="btnSaveMember_Click"
            ValidationGroup="BasicInfo" />
    </div>
   
</div>
</ContentTemplate>
		</asp:UpdatePanel>
		
