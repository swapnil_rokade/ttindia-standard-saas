﻿ <%--
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateHireStatusUpdate.ascx
    Description: This page is used to display the Candidate Hiring Status Update - all controls are defined here.
    Created By: Kanchan Yeware
    Created On: 7-25-2016
    Modification Log:    
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>


<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CandidateHireStatusUpdate.ascx.cs"
    Inherits="Controls_CandidateHireStatusUpdtae" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<link href="../assets/css/chosen.css" rel="Stylesheet" />
<script type="text/javascript" src="../assets/js/chosen.jquery.js"></script>

<style type="text/css">
    .dropdown-menu:after
    {
        border-bottom: 7px solid #FFFFFF;
        border-left: 7px solid transparent;
        border-right: 7px solid transparent;
        content: "";
        display: inline-block;
        left: 80%;
        position: absolute;
        top: -6px;
    }
    .dropdown-menu::before
    {
        position: absolute;
        top: -7px;
        left: 80%;
        display: inline-block;
        border-right: 7px solid transparent;
        border-bottom: 7px solid #CCC;
        border-left: 7px solid transparent;
        border-bottom-color: rgba(0, 0, 0, 0.2);
        content: '';
    }
    #GridContainer
    {
        margin-left: 25px;
        margin-right: -25px;
        margin: auto;
        width: auto;
        padding: 10px;
    }
    .Grid
    {
        font-size: 13px;
        font-family: 'Helvetica Neue' , Helvetica, Arial, sans-serif;
        border: 1px solid #DDD;
        border-collapse: separate;
        border-left: 0;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .Grid th, .Grid td
    {
        border-left: 1px solid #DDDDDD !important;
        border-top: 1px solid #DDDDDD !important;
        border-bottom: none !important;
        border-right: none !important; /*padding: 8px !important;*/
        line-height: 18px !important;
        text-align: left !important;
        display: table-cell !important;
        background: none;
    }
    .Grid .altrow
    {
        background-color: #F9F9F9;
    }
    .Grid .altrow:hover
    {
        background-color: #F9F9F9 !important;
    }
    .Grid .row:hover
    {
        background-color: #F9F9F9 !important;
    }
    #Footer
    {
        clear: both;
        height: 25px;
        width: 100%;
        background-color: #e4effd;
        margin: 15px auto 15px auto;
        border-top: 1px solid #bfdbff;
        border-bottom: 1px solid #bfdbff;
        float: left;
        background: url( "../Images/bgnoise.png" ) repeat scroll 0 0 transparent !important;
        border: 0;
        border-top: 1px solid #EEEEEE;
        background: transparent;
    }
    .center
    {
        margin: auto;
        width: 30%;
        padding: 10px;
    }
    .TableFormLeble
    {
        font-size: 12px;
        color: #444444;
        text-align: left;
        font-weight: bold;
        white-space: nowrap;
        vertical-align: middle;
        width: 28%;
        height: 20px;
        padding: 5px 5px 2px 3px;
        float: left;
    }
    .TableFormContent
    {
        padding: 5px 0px 2px 5px !important;
    }
    .TableFormValidatorContent
    {
        text-align: left;
        vertical-align: top;
        padding: 0px 0px 0px 0px;
        margin-left: 31%;
        font-size: 13px;
        font-weight: 500;
    }
    .form-actions
    {
        padding: 20px 440px 18px !important;
    }
    .chzn-container
    {
        width: 150px !important;
    }
    .chzn-drop
    {
        width: 200px !important;
    }
    .Grid tr td
    {
        overflow: visible !important;
    }
    .Grid tr
    {
    background-color:#E6E6E6;
    color:#333333;
    text-decoration: none;    
    padding-right: 10px;
    	}
</style>
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
     <div class="TableRow" style="text-align: center">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div id="GridContainer">
        <div class="TableFormContent" style="vertical-align: top">        
            <asp:UpdatePanel ID="upGrade" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:GridView ID="grdCandidateList" runat="server" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="ID" HeaderText="ID" Visible="false"/>
                            <asp:BoundField DataField="ManagerId" HeaderText="ManagerId" Visible="false"/>
                            <asp:BoundField DataField="MemberID" HeaderText="Candidate ID" />
                             <asp:TemplateField HeaderText="Candidate Name">
                                <ItemTemplate>                                  
                                    <asp:HyperLink ID="lnkCandidateName"  runat="server"></asp:HyperLink>
                               </ItemTemplate>
                            </asp:TemplateField> 
                           <%-- <asp:BoundField DataField="CandidateName" HeaderText="Candidate Name" /> --%>
                            <%-- <asp:BoundField DataField="RequisitionJobTitle" HeaderText="Requisition Job Title" />   --%>                          
                             <asp:TemplateField HeaderText="Requisition Job Title">
                                <ItemTemplate>                                  
                                    <asp:HyperLink ID="lnkJobTitle" runat="server" Target="_blank" Style="min-width: 90px" Width="300px" AutoPostBack="true" EnableViewState="True"></asp:HyperLink>
                               </ItemTemplate>
                            </asp:TemplateField> 
                             <asp:BoundField DataField="RequisitionCode" HeaderText="Requisition Code" />                   
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblMemberID" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblManagerId" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblJobPostingID" runat="server" Visible="false"></asp:Label>
                                    <asp:DropDownList ClientIDMode="Static" ID="ddlHiringStatus" runat="server" CssClass="chzn-select"
                                        Style="min-width: 300px" Width="500px" AutoPostBack="true" EnableViewState="True"
                                        OnSelectedIndexChanged="ddlHiringStatus_OnSelectedIndexChanged">
                                    </asp:DropDownList>
                              </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div id="divNoRecord" style="text-align: center">
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                            visible="false">
                            <tr>
                                <td>
                                    <asp:Label ID="lblEmptyMsg" runat="server"> </asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divButton" style="width: 10%; margin: 0 auto; text-align: center; align-content: center;
                    padding-top: 20px;">
                        <table id="tblButton" runat="server">
                            <tr>
                                <td>
                                    <asp:Button ID="btnSaveStatus" runat="server" Text="Save" OnClick="btnSaveStatus_OnClick"
                                    class="CommonButton" Visible="false"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <script type="text/javascript">
        Sys.Application.add_load(function() {
            $(".chzn-select").chosen();
        });        
    </script>

</body>
</html>
