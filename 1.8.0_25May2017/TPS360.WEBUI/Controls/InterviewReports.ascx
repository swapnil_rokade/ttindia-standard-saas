﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:InterviewReports.ascx
    Description By:This form is common for InterviewReports.aspx and MyInterviewReports.aspx 
    Created By:Sumit Sonawane
    Created On:25/May/2016
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
      0.1               12/May/2016           pravin khot         ADD NEW FIELD-InterviewFeedback.
    ---------------------------------------------------------------------------------------------------------------------------------------     

--%>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterviewReports.ascx.cs"
    Inherits="TPS360.Web.UI.ControlInterviewReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>
<script src="../js/EmployeeAndTeamOnChange.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
        var ModalProgress = '<%= Modal.ClientID %>';
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
        function PageLoadedReq(sender, args) {
            try {
                var hdnScroll = document.getElementById('<%=hdnScrollPos.ClientID%>');
                var bigDiv = document.getElementById('ctl00_cphHomeMaster_divlsvInterview');
                bigDiv.scrollLeft = hdnScroll.value;
            }
            catch (e) {
            }
        }
        function SetScrollPosition() {
            var hdnScroll = document.getElementById('<%=hdnScrollPos.ClientID%>');
            var bigDiv = document.getElementById('ctl00_cphHomeMaster_divlsvInterview');
            hdnScroll.value = bigDiv.scrollLeft;
        }
        function SelectUnselectColumnOption(chkColumnOption) {
            var chkBoxAll = $get('<%= chkColumns.ClientID%>');
            var chkBoxList = $get('<%= chkColumnList.ClientID%>');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            if (chkBoxAll.checked) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = true;
                }
            }
            if (!chkBoxAll.checked) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = false;
                }
            }
        }
        function SelectUnselectAllColumnOption(chkColumnOptionHeader) {
            var checkedCount = 0;
            var chkBoxAll = $get('<%= chkColumns.ClientID%>');
            var chkBoxList = $get('<%= chkColumnList.ClientID%>');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            for (var i = 0; i < chkBoxCount.length; i++) {
                if (chkBoxCount[i].checked) {
                    checkedCount++;
                }
            }
            if (checkedCount == chkBoxCount.length) {
                chkBoxAll.checked = true;
            }
            else {
                chkBoxAll.checked = false;
            }
        }
</script>

<asp:TextBox ID="hdnSortColumn" runat="server" Visible="false" />
<asp:TextBox ID="hdnSortOrder" runat="server" Visible="false" />
<ucl:confirm id="uclConfirm" runat="server"></ucl:confirm>
<asp:UpdatePanel ID="pnlInterviewReports" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div style="text-align: left">
            <asp:Label ID="lbMessage" runat="server" EnableViewState="false"></asp:Label>
        </div>
        <asp:HiddenField ID="hdnScrollPos" runat="server" />
        <asp:HiddenField ID="hdnIsTeamReport" runat="server" />
        <asp:ObjectDataSource ID="odsInterviewList" runat="server" SelectMethod="GetPaged"
            TypeName="TPS360.Web.UI.MemberInterviewDataSource" SelectCountMethod="GetListCount"
            EnablePaging="True" SortParameterName="sortExpression">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlInterviewType" Name="InterviewType" PropertyName="SelectedValue"
                    Type="String" />
                <asp:ControlParameter ControlID="dtPicker" Name="StartDate" PropertyName="StartDate"
                    Type="String" />
                <asp:ControlParameter ControlID="dtPicker" Name="EndDate" PropertyName="EndDate"
                    Type="String" />
                <asp:ControlParameter ControlID="ddlStartStartTime" Name="StartStartTime" PropertyName="SelectedValue"
                    Type="String" />
                <asp:ControlParameter ControlID="ddlStartEndTime" Name="StartEndTime" PropertyName="SelectedValue"
                    Type="String" />
                <asp:ControlParameter ControlID="ddlEndStartTime" Name="EndStartTime" PropertyName="SelectedValue"
                    Type="String" />
                <asp:ControlParameter ControlID="ddlEndEndTime" Name="EndEndTime" PropertyName="SelectedValue"
                    Type="String" />
                <asp:ControlParameter ControlID="ddlRequisition" Name="Requisition" PropertyName="SelectedValue"
                    Type="String" />
                <asp:ControlParameter ControlID="ddlClient" Name="Client" PropertyName="SelectedValue"
                    Type="String" />
                <asp:ControlParameter ControlID="hdnSelectedContactID" Name="ClientInterviewers" PropertyName="Value" 
                    Type="String" />
                <asp:ControlParameter ControlID="ddlInternalInterviewers" Name="InternalInterviewers" PropertyName="SelectedValue"
                    Type="String" />
                <asp:ControlParameter ControlID="txtLocation" Name="Location" PropertyName="Text"
                    Type="String" />
                <asp:Parameter Name="TeamLeaderId" 
                    Type="Int32" DefaultValue="0" />
                <asp:ControlParameter ControlID="ddlTeam" PropertyName="SelectedValue" Name="TeamId"
                    Type="Int32" DefaultValue="0" />
                    <asp:ControlParameter ControlID="hdnIsTeamReport" Name="isTeamReport" Type="Boolean" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:Panel ID="pnlSearchRegion" runat="server" DefaultButton="btnSearch">
            <div style="width: 100%;">
                <div>
                    <div class="TableRow">
                        <div class="TabPanelHeader nomargin" style="border-style: none none none;">
                            Filter Options</div>
                    </div>
                    <div class="TableRow" style="text-align: center">
                        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                    </div>
                    <asp:CollapsiblePanelExtender ID="cpeSeafvfrchBody" runat="server" TargetControlID="pnlSearchContent"
                        ExpandControlID="pnlSearchHeader" CollapseControlID="pnlSearchHeader" ImageControlID="ImgSearch"
                        CollapsedImage="~/Images/expand-plus.png" ExpandedImage="~/Images/collapse-minus.png"
                        SuppressPostBack="false" EnableViewState="true">
                    </asp:CollapsiblePanelExtender>
                    <asp:Panel ID="pnlSearchHeader" runat="server">
                        <div class="" style="clear: both; display: none;">
                            <asp:Image ID="ImgSearch" ImageUrl="~/Images/expand-plus.png" runat="server" />
                            Search Filters
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlSearchContent" runat="server" Visible="true" Style="overflow: hidden;"
                        Height="0">
                        <div class="FormLeftColumn" style="width: 48%">
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label ID="lblInterviewType" runat="server" EnableViewState="false" Text="Interview Type"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlInterviewType" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label ID="lblDate" runat="server" EnableViewState="false" Text="Date"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <ucl:daterangepicker id="dtPicker" runat="server" />
                                </div>
                            </div>
                            <div class="TableRow" style="white-space: nowrap">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="False" ID="lblDateHeader" runat="server" Text="Start Time"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <div style="float: left;">
                                        <asp:DropDownList ID="ddlStartStartTime" runat="server" class="CommonDropDownList"
                                            Width="90px">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="float: left;">
                                        &nbsp;To&nbsp;
                                    </div>
                                    <div style="float: left;">
                                        <asp:DropDownList ID="ddlStartEndTime" runat="server" class="CommonDropDownList"
                                            Width="90px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="TableRow" style="white-space: nowrap">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="False" ID="Label1" runat="server" Text="End Time"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <div style="float: left;">
                                        <asp:DropDownList ID="ddlEndStartTime" runat="server" class="CommonDropDownList"
                                            Width="90px">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="float: left;">
                                        &nbsp;To&nbsp;
                                    </div>
                                    <div style="float: left;">
                                        <asp:DropDownList ID="ddlEndEndTime" runat="server" class="CommonDropDownList" Width="90px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="FormRightColumn" style="width: 48%">
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="false" ID="lblRequisition" runat="server" Text="Requisition"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlRequisition" runat="server" CssClass="CommonDropDownList">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="true" ID="lblClient" runat="server"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlClient" CssClass="CommonDropDownList" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label ID="lblClientInterviewers" runat="server"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlClientInterviewers" runat="server" CssClass="CommonDropDownList"
                                        Enabled="false">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnSelectedContactID" runat="server" Value="0" />
                                </div>
                            </div>
                            <div class="TableRow" id="divTeam" runat="server" visible="false">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="false" ID="lblTeam" runat="server" Text="Team"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlTeam" CssClass="CommonDropDownList" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="TableRow" id="divRecruiter" runat="server">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="false" ID="lblInternalInterviewers" runat="server" Text="Recruiters"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlInternalInterviewers" CssClass="CommonDropDownList" runat="server">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnInternalInterviewers" runat="server" />
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label ID="lblLocation" runat="server" EnableViewState="false" Text="Location"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox ID="txtLocation" runat="server" CssClass="SearchTextbox"></asp:TextBox>
                                </div>
                            </div>
                            <%-- <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblOtherInterviewers" runat="server" Text="Other Interviewers"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlOtherInterviewers" CssClass="CommonDropDownList" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>--%>
                        </div>
                    </asp:Panel>
                    <div class="HeaderDevider">
                    </div>
                    <div class="TableRow well well-small nomargin">
                        <asp:CollapsiblePanelExtender ID="cpnlCandidateTopBar" runat="server" TargetControlID="pnlContent"
                            ExpandControlID="pnlHeader" CollapseControlID="pnlHeader" Collapsed="true" ImageControlID="imgShowHide"
                            CollapsedImage="~/Images/expand-plus.png" ExpandedImage="~/Images/collapse-minus.png"
                            SuppressPostBack="true">
                        </asp:CollapsiblePanelExtender>
                        <asp:Panel ID="pnlHeader" runat="server">
                            <div class="" style="clear: both; cursor: pointer;">
                                <asp:Image ID="imgShowHide" ImageUrl="~/Images/expand-plus.png" runat="server" />
                                Included Columns
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlContent" runat="server" Style="overflow: hidden;" Height="0">
                            <div class="TableRow" style="text-align: left; padding-top: 5px;">
                                <asp:CheckBox runat="server" Text="Select All" ID="chkColumns" onclick="javascript:SelectUnselectColumnOption(this);"
                                    Checked="true" CssClass="ColumnSelection" />
                                <asp:CheckBoxList ID="chkColumnList" runat="server" RepeatColumns="2" CssClass="ColumnSelection"
                                    onclick="javascript:SelectUnselectAllColumnOption('chkColumns')">
                                    <asp:ListItem Value="Date" Selected="True">Date</asp:ListItem>
                                    <asp:ListItem Value="StartTime" Selected="True">Start Time</asp:ListItem>
                                    <asp:ListItem Value="EndTime" Selected="True">End Time</asp:ListItem>
                                    <asp:ListItem Value="Id" Selected="True">Candidate ID #</asp:ListItem>
                                    <asp:ListItem Value="CandidateName" Selected="True">Candidate Name</asp:ListItem>
                                    <asp:ListItem Value="JobTitle" Selected="True">Job Title</asp:ListItem>
                                    <asp:ListItem Value="ReqCode" Selected="True">Req. Code</asp:ListItem>
                                    <asp:ListItem Value="ApptTitle" Selected="True">Appt. Title</asp:ListItem>
                                    <asp:ListItem Value="Location" Selected="True">Location</asp:ListItem>
                                    <asp:ListItem Value="Type" Selected="True">Type</asp:ListItem>
                                    <asp:ListItem Value="BU" Selected="True">BU</asp:ListItem>
                                    <asp:ListItem Value="BUInterviewers" Selected="True">BU Interviewers</asp:ListItem>
                                    <asp:ListItem Value="Recruiters" Selected="True">Recruiters</asp:ListItem>
                                    <asp:ListItem Value="Notes" Selected="True">Notes</asp:ListItem>
                                    <asp:ListItem Value="OtherInterviewers" Selected="True">Other Interviewers</asp:ListItem>
                                    <asp:ListItem Value="InterviewFeedback" Selected="True">Interview Feedback</asp:ListItem> <%--Added by pravin khot on 12/May/2016--%>

                                </asp:CheckBoxList>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="TableRow" style="text-align: center; padding-top: 5px">
                        <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search"
                            CssClass="btn btn-primary" EnableViewState="false" OnClick="btnSearch_Click"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                        <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                            EnableViewState="false" CausesValidation="false" OnClick="btnClear_Click" />
                    </div>
                    <div class="TableRow" style="padding-bottom: 5px" id="divExportButtons" runat="server"
                        visible="false">
                        <div class="TabPanelHeader nomargin" style="margin-bottom: 5px; border-style: none none none;">
                            Report Results</div>
                        <asp:ImageButton ID="btnExportToExcel" CssClass="btn" SkinID="sknExportToExcel" runat="server"
                            ToolTip="Export To Excel" OnClick="btnExportToExcel_Click" />
                        <asp:ImageButton ID="btnExportToPDF" CssClass="btn" SkinID="sknExportToPDF" runat="server"
                            ToolTip="Export To PDF" OnClick="btnExportToPDF_Click" />
                        <asp:ImageButton ID="btnExportToWord" CssClass="btn" SkinID="sknExportToWord" runat="server"
                            ToolTip="Export To Word" OnClick="btnExportToWord_Click" />
                        <%-- 
                              <asp:Button ID="btnExportToPDF" runat="server" Text="Export To PDF" AlternateText="Export To PDF"
                                CssClass="btn" EnableViewState="false" OnClick="btnExportToPDF_Click" />
                            <asp:Button ID="btnExportToExcel" runat="server" Text="Export To Excel" AlternateText="Export To Excel"
                                CssClass="btn" EnableViewState="false" CausesValidation="false" OnClick="btnExportToExcel_Click" />
                            <asp:Button ID="btnExportToWord" runat="server" Text="Export To Word" AlternateText="Export To Word"
                                CssClass="btn" EnableViewState="false" CausesValidation="false" OnClick="btnExportToWord_Click" />
                                --%>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlGridRegion" runat="server">
            <div id="divlsvInterview" runat="server" class="GridContainer" style="overflow: auto;
                overflow-y: hidden; width: 100%; text-align: left;" onscroll='SetScrollPosition()'>
                <asp:ListView ID="lsvInterview" runat="server" DataKeyNames="Id" OnItemDataBound="lsvInterview_ItemDataBound"
                    OnItemCommand="lsvInterview_ItemCommand" OnPreRender="lsvInterview_PreRender">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid ReportGrid" cellspacing="0" border="0">
                            <tr>
                                <th runat="server" id="thDate" style="min-width: 80px" enableviewstate="false">
                                    <asp:LinkButton runat="server" ID="btnDate" CommandName="Sort" CommandArgument="[I].[StartDateTime]"
                                        Text="Date" ToolTip="Sort By Date" />
                                </th>
                                <th runat="server" id="thStartTime" style="min-width: 80px" enableviewstate="false">
                                    <asp:LinkButton runat="server" ID="btnStartTime" CommandName="Sort" CommandArgument="StartTime"
                                        Text="Start Time" ToolTip="Sort By Start Time" />
                                </th>
                                <th runat="server" id="thEndTime" style="min-width: 80px" enableviewstate="false">
                                    <asp:LinkButton runat="server" ID="btnEndTime" CommandName="Sort" CommandArgument="EndTime"
                                        Text="End Time" ToolTip="Sort By End Time" />
                                </th>
                                <th runat="server" id="thId">
                                    <asp:LinkButton runat="server" ID="lnkID" Style="min-width: 130px" CommandName="Sort"
                                        CommandArgument="[I].[MemberId]" Text="Candidate ID #" ToolTip="Sort By Candidate ID #" />
                                </th>
                                <th runat="server" id="thCandidateName" style="min-width: 120px" enableviewstate="false">
                                    <asp:LinkButton runat="server" ID="btnCandidateName" CommandName="Sort" CommandArgument="[M].[FirstName]"
                                        Text="Candidate Name" ToolTip="Sort By Candidate Name" />
                                </th>
                                <th runat="server" id="thJobTitle" style="min-width: 80px" enableviewstate="false">
                                    <asp:LinkButton runat="server" ID="btnJobTitle" CommandName="Sort" CommandArgument="[JP].[JobTitle]"
                                        Text="Job Title" ToolTip="Sort By Job Title" />
                                </th>
                                <th runat="server" id="thReqCode" style="min-width: 80px" enableviewstate="false">
                                    <asp:LinkButton runat="server" ID="btnReqCode" CommandName="Sort" CommandArgument="[JP].[JobPostingCode]"
                                        Text="Req. Code" ToolTip="Sort By Req Code" />
                                </th>
                                <th runat="server" id="thApptTitle" style="min-width: 80px" enableviewstate="false">
                                    <asp:LinkButton runat="server" ID="btnApptTitle" CommandName="Sort" CommandArgument="[I].[Title]"
                                        Text="Appt. Title" ToolTip="Sort By Appt Title" />
                                </th>
                                <th runat="server" id="thLocation" style="min-width: 80px" enableviewstate="false">
                                    <asp:LinkButton runat="server" ID="btnLocation" CommandName="Sort" CommandArgument="[I].[Location]"
                                        Text="Location" ToolTip="Sort By Location" />
                                </th>
                                <th runat="server" id="thType" style="min-width: 80px" enableviewstate="false">
                                    <asp:LinkButton runat="server" ID="btnType" CommandName="Sort" CommandArgument="[GL].[Name]"
                                        Text="Type" ToolTip="Sort By Type" />
                                </th>
                                <th runat="server" id="thClient" style="min-width: 80px" enableviewstate="false">
                                    <asp:LinkButton runat="server" ID="btnClient" CommandName="Sort" CommandArgument="[C].[CompanyName]"
                                        Text="BU " ToolTip="Sort By Account" />
                                </th>
                                <th runat="server" id="thClientInterviewer" style="min-width: 150px" enableviewstate="false">
                                    <asp:LinkButton runat="server" ID="btnClientInterviewer" CommandName="Sort" CommandArgument="[GIVN].[InterviewerName]"
                                        Text="BU Interviewer" ToolTip="Sort By Account Interviewer" />
                                </th>
                                <th runat="server" id="thInternalInterviewers" style="min-width: 150px" enableviewstate="false">
                                    <asp:LinkButton runat="server" ID="btnInternalInterviewers" CommandName="Sort" CommandArgument="[GIV].[InterviewerName]"
                                        Text="Recruiters" ToolTip="Sort By Recruiters" />
                                </th>
                                <th runat="server" id="thNotes" style="min-width: 120px" enableviewstate="false">
                                    <asp:Label runat="server" ID="btnNotes" Text="Notes" ToolTip="Notes"></asp:Label>
                                </th>
                                <th runat="server" id="thOtherInterviewers" style="min-width: 150px" enableviewstate="false">
                                    <asp:LinkButton runat="server" ID="btnOtherInterviewers" CommandName="Sort" CommandArgument="[I].[OtherInterviewers]"
                                        Text="Other Interviewers" ToolTip="Sort By Other Interviewers" />
                                </th>
                                 <%-- *****************added by pravin khot on 12/May/2016******************** --%>
                                      <th runat="server" id="thInterviewFeedback" style="min-width: 120px"  enableviewstate ="false" >
                                        <asp:Label runat="server" ID="btnInterviewFeedback" Text="InterviewFeedback" ToolTip="InterviewFeedback"></asp:Label>
                                    </th>
                                   <%-- ******************************END******************************************--%>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td id="tdpager" runat="server">
                                    <ucl:pager id="pagerControl" runat="server" enableviewstate="true" />
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No data was returned.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td runat="server" id="tdDate">
                                <asp:Label ID="lblDate" runat="server"></asp:Label>
                            </td>
                            <td runat="server" id="tdStartTime">
                                <asp:Label runat="server" ID="lblStartTime" />
                            </td>
                            <td runat="server" id="tdEndTime">
                                <asp:Label runat="server" ID="lblEndTime" />
                            </td>
                            <td runat="server" id="tdId">
                                <asp:Label ID="lblCandidateID" runat="server"></asp:Label>
                            </td>
                            <td runat="server" id="tdCandidateName">
                                <asp:HyperLink ID="lnkCandidateName" runat="server"></asp:HyperLink>
                            </td>
                            <td runat="server" id="tdJobTitle">
                                <asp:HyperLink ID="lnkJobTitle" runat="server" Target="_blank"></asp:HyperLink>
                            </td>
                            <td runat="server" id="tdReqCode">
                                <asp:Label runat="server" ID="lblReqCode" />
                            </td>
                            <td runat="server" id="tdApptTitle">
                                <asp:Label runat="server" ID="lblApptTitle" />
                            </td>
                            <td runat="server" id="tdLocation">
                                <asp:Label runat="server" ID="lblLocation" />
                            </td>
                            <td runat="server" id="tdType">
                                <asp:Label runat="server" ID="lblType" />
                            </td>
                            <td runat="server" id="tdClient">
                                <asp:Label runat="server" ID="lblClient" />
                            </td>
                            <td runat="server" id="tdClientInterviewers">
                                <asp:Label runat="server" ID="lblClientInterviewers" />
                            </td>
                            <td runat="server" id="tdInternalInterviewers">
                                <asp:Label runat="server" ID="lblInternalInterviewers" />
                            </td>
                            <td runat="server" id="tdNotes">
                                <asp:Label runat="server" ID="lblNotes" />
                            </td>
                            <td runat="server" id="tdOtherInterviewers">
                                <asp:Label runat="server" ID="lblOtherInterviewers" />
                            </td>
                             <%-- **************Added by pravin khot on 12/May/2016****************--%>
                                 <td runat="server" id="tdInterviewFeedback">
                                    <asp:Label runat="server" ID="lblInterviewFeedback"  />
                                </td>
                                <%--*****************************END******************************--%>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlmodal" runat="server" Style="display: none">
            <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                <ContentTemplate>
                    <img src="../Images/AjaxLoading.gif" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <ajaxtoolkit:modalpopupextender id="Modal" runat="server" targetcontrolid="pnlmodal"
            popupcontrolid="pnlmodal" backgroundcssclass="divModalBackground">
            </ajaxtoolkit:modalpopupextender>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExportToPDF" />
        <asp:PostBackTrigger ControlID="btnExportToExcel" />
        <asp:PostBackTrigger ControlID="btnExportToWord" />
    </Triggers>
</asp:UpdatePanel>
