﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :  CareerRequisitionList.ascx
    Description         :  career page
    Created By          :   Pravin
    Created On          :   18/Jan/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using AjaxControlToolkit;
using TPS360.BusinessFacade;
using TPS360.Common;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Globalization;
using System.Threading;
using System.Net.Mail;
using System.Net;
namespace TPS360.Web.UI
{
    public partial class CareerRequisitionList : BaseControl
    {
        #region Member Variables
        IList<HiringMatrixLevels> hiringLevels = null;
        bool ShowAction = false;
        public int HeaderAdd = 0;
        ArrayList _permittedMenuIdList;
        public string RequisitionType = "Master";
        public static bool IsAccss = false;

        public DropDownList ddlJobStatusCommon
        {
            set;
            get;
        }
        #endregion

        #region Methods
        private void BindList()
        {
            //this.lsvJobPosting.DataBind();

        }
        private void MemberPrivilege()
        {
            
        }
        private bool IsInPermitedMenuList(int menuId)
        {
            if (_permittedMenuIdList == null)
            {
                if (CurrentMember != null) _permittedMenuIdList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            }

            if ((_permittedMenuIdList != null) && (_permittedMenuIdList.Count > 0))
            {
                if (_permittedMenuIdList.Contains(menuId))
                {
                    return true;
                }
            }

            return false;
        }

        private void SelectJobStatusList()
        {
            if (Request.Cookies["RequisitionStatus"] != null)
            {
                ddlJobStatus.SelectedItems = Request.Cookies["RequisitionStatus"].Value;
            }
            else
            {
                foreach (ListItem item in ddlJobStatus.ListItem.Items)
                {
                    item.Selected = false;
                }
            }
        }

        private void PrepareView()
        {


            MiscUtil.PopulateRequistionStatus(ddlJobStatus.ListItem, Facade);
            SelectJobStatusList();
        
            MiscUtil.PopulateMemberListByRole(ddlReqCreator, ContextConstants.ROLE_EMPLOYEE, Facade);
            MiscUtil.PopulateMemberListByRole(ddlEmployee, ContextConstants.ROLE_EMPLOYEE, Facade);
            int companyStatus = 6;//(int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting);
            MiscUtil.PopulateClients(ddlEndClient.ListItem, companyStatus, Facade);
   
            //if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
            //{
            //    lblByEndClientsHeader.Text = "BU";
                
            //}
            //else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
            //{
            //    lblByEndClientsHeader.Text = "Account";
                
            //}
            //else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Vendor)
            //{
            //    lblByEndClientsHeader.Text = "Vendor";
             
            //}

        }


        #endregion

        #region Events

        #region Page Events
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //string pagesize = "";
            //if (RequisitionType == "My")
            //{
            //    pagesize = (Request.Cookies["MyRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MyRequisitionListRowPerPage"].Value); ;
            //}
            //else
            //{
            //    pagesize = (Request.Cookies["MasterRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MasterRequisitionListRowPerPage"].Value); ;
            //}
            //ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            //if (PagerControl != null)
            //{
            //    DataPager pager = (DataPager)PagerControl.FindControl("pager");
            //    if (pager != null)
            //    {
            //        pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            //        if (pager.Page.IsPostBack)
            //            pager.SetPageProperties(0, pager.MaximumRows, true);
            //    }
            //}

            //this.lsvJobPosting.DataBind();
            //uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            //uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
            //AddHiringMatrixColumns();

            Response.Cookies["RequisitionStatus"].Value = ddlJobStatus.SelectedItems;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtJobTitle.Text = "";
            txtReqCode.Text = "";
            foreach (ListItem item in ddlJobStatus.ListItem.Items)
            {
                item.Selected = false;
            }
            Response.Cookies["RequisitionStatus"].Value = ddlJobStatus.SelectedItems;
            dtPstedDate.ClearRange();
            ddlReqCreator.SelectedIndex = 0;
            ddlEmployee.SelectedIndex = 0;
            txtCity.Text = "";
            uclCountryState.SelectedCountryId = 0;
            uclCountryState.SelectedStateId = 0;
            //ddlEndClient.SelectedIndex = 0;
            foreach (ListItem item in ddlEndClient.ListItem.Items)
            {
                item.Selected = false;
            }
           // lsvJobPosting.DataBind();
            string pagesize = "";
            if (RequisitionType == "My")
            {
                pagesize = (Request.Cookies["MyRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MyRequisitionListRowPerPage"].Value); ;
            }
            else
            {
                pagesize = (Request.Cookies["MasterRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MasterRequisitionListRowPerPage"].Value); ;
            }
            //ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            //if (PagerControl != null)
            //{
            //    DataPager pager = (DataPager)PagerControl.FindControl("pager");
            //    if (pager != null)
            //    {
            //        pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            //    }
            //}
            //AddHiringMatrixColumns();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Image lblLogin = (Image)Page.Master.FindControl("imgTpsLogo");
            //lblLogin.ImageUrl = "ImagesApprove.jpg";
           
            //ddlJobStatusCommon = new DropDownList();
            //MiscUtil.PopulateRequistionStatus(ddlJobStatusCommon, Facade);
            //if (hdnDoPostPack.Value == "1")
            //{
            //    lsvJobPosting.DataBind();
            //    hdnDoPostPack.Value = "0";
            //    return;
            //}
           
            //string testUrl = HttpContext.Current.Request.Url.Authority;
            //ddlReqCreator.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            //ddlEndClient.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            //ddlEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            //this.Page.MaintainScrollPositionOnPostBack = true;
            //if (Page.Request.Url.AbsoluteUri.ToString().Contains("MasterRequisitionList.aspx")) { RequisitionType = "Master"; }
            //else { RequisitionType = "My"; }
           
            //if (CurrentMember != null)
            //{
            //    ArrayList Privilege = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            //    if (Privilege.Contains(56))
            //        IsAccss = true;
            //    else IsAccss = false;
            //}

            //if (!IsPostBack)
            //{
            //    //PrepareView();

            //    string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];


            //    if (!StringHelper.IsBlank(message))
            //    {
            //        MiscUtil.ShowMessage(lblMessage, message, false);
            //    }
            //    hdnSortColumn.Value = "btnPostedDate";
            //    hdnSortOrder.Value = "DESC";
            //    PlaceUpDownArrow();
            //}
            PrepareView1();
            //string pagesize = "";
            //if (RequisitionType == "My")
            //{
            //    pagesize = (Request.Cookies["MyRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MyRequisitionListRowPerPage"].Value); ;
            //}
            //else
            //{
            //    pagesize = (Request.Cookies["MasterRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MasterRequisitionListRowPerPage"].Value); ;
            //}
            //ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            //if (PagerControl != null)
            //{
            //    DataPager pager = (DataPager)PagerControl.FindControl("pager");
            //    if (pager != null)
            //    {
            //        pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            //    }
            //}
            //if (Facade.GetCustomRoleNameByMemberId(CurrentMember.Id) == ContextConstants.ROLE_DEPARTMENT_CONTACT)
            //{
            //    divBU.Visible = false;
            //}
            //AddHiringMatrixColumns();
            //MemberPrivilege();
        }

        private void PlaceUpDownArrow()
        {
            //try
            //{
            //    LinkButton lnk = (LinkButton)lsvJobPosting.FindControl(hdnSortColumn.Value);
            //    System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
            //    im.EnableViewState = false;
            //    if (lnk.CommandArgument.Contains("[1]") || lnk.CommandArgument.Contains("[JMC].[ManagersCount]"))
            //    {
            //        im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Descending" : "Ascending"));
            //    }
            //    else
            //        im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
            //}
            //catch
            //{
            //}

        }
        #endregion

        #region ListView Events

        private void AddHiringMatrixColumns()
        {
            //System.Web.UI.HtmlControls.HtmlTableCell thLevels = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("thLevels");
            //if (thLevels != null)
            //{
                //thLevels.Controls.Clear();
                //IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
                //hiringLevels = hiringMatrixLevels;
                //int ColLocation = 0;

                //if (hiringMatrixLevels != null)
                //{
                //    if (hiringMatrixLevels.Count > 0)
                //    {
                //        foreach (HiringMatrixLevels levels in hiringMatrixLevels)
                //        {
                //            System.Web.UI.HtmlControls.HtmlTableCell col = new System.Web.UI.HtmlControls.HtmlTableCell("TH");
                //            LinkButton lnkLevel = new LinkButton();
                //            lnkLevel.Text = levels.Name.Trim();
                //            lnkLevel.CommandName = "Sort";
                //            lnkLevel.ID = "thLevel" + ColLocation;
                //            LinkButton lnktemp = (LinkButton)lsvJobPosting.FindControl(lnkLevel.ID);
                //            if (lnktemp != null) lsvJobPosting.Controls.Remove(lnktemp);
                //            lnkLevel.EnableViewState = false;
                //            lnkLevel.CommandArgument = "Level[" + levels.Id + "]";
                //            lnkLevel.EnableViewState = false;
                //            if (levels.SortingOrder == 0)
                //                thLevels.Controls.AddAt(ColLocation, lnkLevel);
                //            else
                //            {
                //                col.Controls.Add(lnkLevel);
                //                thLevels.Controls.AddAt(ColLocation, col);
                //            }
                //            ColLocation++;
                //        }
                //    }
                //}
                //System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("tdPager");
                //tdPager.ColSpan = Convert.ToInt32(tdPager.ColSpan) + hiringMatrixLevels.Count;
            //}
        }
        public bool isMemberinHinringTeam(int memberid, int jobpostingid)
        {
            bool returnValue = false;

            JobPostingHiringTeam team = Facade.GetJobPostingHiringTeamByMemberId(memberid, jobpostingid);

            if (team != null)
            {
                returnValue = true;
            }
            else
            {
                returnValue = false;
            }

            return returnValue;
        }
        protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _isAdmin = base.IsUserAdmin;
            bool _isHiringManager = base.IsHiringManager;
            bool isAssingned = false;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
                int ColLocation = 0;
                if (jobPosting != null)
                {

                    System.Web.UI.HtmlControls.HtmlTableRow row = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("row");
                    System.Web.UI.HtmlControls.HtmlTableRow trCandidateDetailss = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trCandidateDetails");
                    Image imgShowHide = (Image)e.Item.FindControl("imgShowHide");
                    row.Attributes.Add("onclick", "javascript:ShowOrHideRow('" + trCandidateDetailss.ClientID + "','" + jobPosting.Id + "','" + ("1") + "');");

                    
                    //row.Attributes.Add("onclick", "javascript:ShowOrHideRow('" + trCandidateDetailss.ClientID + "','" + imgShowHide.ClientID + "','" + jobPosting.Id + "','" + ("1") + "');");

                    //JobPostingHiringTeam team = Facade.GetJobPostingHiringTeamByMemberId(base.CurrentMember.Id, jobPosting.Id);
                    //if (team != null) isAssingned = true;
                    //ColLocation = 0;
                    //System.Web.UI.HtmlControls.HtmlTableCell tdLevels = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdLevels");
                    //int i = 0;
                    //LinkButton lnkTeamCount = (LinkButton)e.Item.FindControl("lnkTeamCount");
                    //lnkTeamCount.Text = jobPosting.NoOfAssignedManagers.ToString();
                    //lnkTeamCount.CommandArgument = "View and apply";

                    //lnkTeamCount.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "','700px','570px'); return false;";
                    //System.Web.UI.HtmlControls.HtmlTableCell thReqCode = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thReqCode");
                    //System.Web.UI.HtmlControls.HtmlTableCell tdJobCode = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdJobCode");

                    //HyperLink tdLevel = (HyperLink)e.Item.FindControl("tdLevel");
                    //Hiring_Levels lev = jobPosting.HiringMatrixLevels[0];
                    //tdLevel.Text = lev.CandidateCount.ToString();
                    //// tdLevel.Target = "_blank";
                    //HiringMatrixLevels l = Facade.HiringMatrixLevel_GetInitialLevel();// (lev.HiringMatrixLevelID);
                    //HyperLink hlkOpenHiringMatrix = (HyperLink)e.Item.FindControl("hlkOpenHiringMatrix");

                    //if (_isAdmin || jobPosting.CreatorId == base.CurrentMember.Id || isAssingned || _isHiringManager)
                    //{
                    //    ControlHelper.SetHyperLink(tdLevel, UrlConstants.ATS.ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX, string.Empty, lev.CandidateCount.ToString(), UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id), UrlConstants.PARAM_TAB, l.Id.ToString());
                    //    SecureUrl urlh = UrlHelper.BuildSecureUrl("../" + UrlConstants.ATS.ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX.Replace("~/", ""), string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id), UrlConstants.PARAM_TAB, l.Id.ToString());
                    //    hlkOpenHiringMatrix.Attributes.Add("href", urlh.ToString());

                    //}
                    //else tdLevel.Enabled = false;// lnkLevel.Enabled = false;


                    //DropDownList ddlJobStatus = (DropDownList)e.Item.FindControl("ddlJobStatus");
                    //Label lblJobStatus = (Label)e.Item.FindControl("lblJobStatus");
                    //if ((jobPosting.AllowRecruitersToChangeStatus == true && isAssingned) || jobPosting.CreatorId == base.CurrentMember.Id)
                    //{

                    //    ddlJobStatus.DataValueField = ddlJobStatusCommon.DataValueField;
                    //    ddlJobStatus.DataTextField = ddlJobStatusCommon.DataTextField;
                    //    ddlJobStatus.DataSource = ddlJobStatusCommon.DataSource;
                    //    ddlJobStatus.DataBind();
                    //    ControlHelper.SelectListByValue(ddlJobStatus, jobPosting.JobStatus.ToString());

                    //    ddlJobStatus.Attributes.Add("onchange", "javascript:RequisitionJobStatusChanged('" + ddlJobStatus.ClientID + "','" + jobPosting.Id + "','" + base.CurrentMember.Id + "','" + lblMessage.ClientID + "','" + jobPosting.JobTitle + "')");
                    //    lblJobStatus.Visible = false;
                    //    ddlJobStatus.Visible = true;
                    //}
                    //else
                    //{
                    //    ddlJobStatus.Visible = false;
                    //    lblJobStatus.Visible = true;
                    //}
                   // Label lblPostedDate = (Label)e.Item.FindControl("lblPostedDate");
                    Label lnkJobTitle = (Label)e.Item.FindControl("lnkJobTitle");

                    Label lblopening = (Label)e.Item.FindControl("lblopening");
                    Label lblopendate = (Label)e.Item.FindControl("lblopendate");
                    Label lblexperience = (Label)e.Item.FindControl("lblexperience");
                   
                    HiddenField hfCandidateId = (HiddenField)e.Item.FindControl("hfCandidateId");

                    HyperLink lblApply = (HyperLink)e.Item.FindControl("lblApply"); 

                    //HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                    //Label lblCity = (Label)e.Item.FindControl("lblCity");
                    //Label lblClient = (Label)e.Item.FindControl("lblClient");       //0.4
                    //Label lblNoOfOpenings = (Label)e.Item.FindControl("lblNoOfOpenings");
                    //Label lblClientJobId = (Label)e.Item.FindControl("lblClientJobId");
                    //LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");
                    //// LinkButton btnCreateNewFromExisting = (LinkButton)e.Item.FindControl("btnCreateNewFromExisting");
                    //LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete");
                    //LinkButton lnkEmployeeReferralLink = (LinkButton)e.Item.FindControl("lnkEmployeeReferralLink");

                    hfCandidateId.Value = jobPosting.JobPostingCode;
                    lnkJobTitle.Text = jobPosting.JobTitle;

                    lblApply.Text = "View and Apply";
                    lblApply.ToolTip = "Click here ,to view the requirment details and Apply the Post";
                    lblApply.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/CareerJobDetail.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                                       
                    System.Web.UI.HtmlControls.HtmlTableRow trCandidateDetails = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trCandidateDetails");
                    //txtAllKeywords.Text = txtAllKeywords.Text != string.Empty ? System.Text.RegularExpressions.Regex.Replace(txtAllKeywords.Text, @"\s+", " ") : string.Empty;

                    if (jobPosting.Id > 0)
                    {
                        lblopening.Text = Convert.ToString(jobPosting.NoOfOpenings);
                        lblopendate.Text = jobPosting.MinExpRequired;
                        lblexperience.Text = jobPosting.MaxExpRequired;
                       
                    }
                  
                  
                    //lblPostedDate.Text = jobPosting.PostedDate.ToShortDateString();
                    ////ControlHelper.SetHyperLink(lnkJobTitle, UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE, string.Empty, MiscUtil.RemoveScript(jobPosting.JobTitle), UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id));
                    //lnkJobTitle.Text = jobPosting.JobTitle;
                    //lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "','700px','570px'); return false;");


                    //lblCity.Text = jobPosting.City + ((!string.IsNullOrEmpty(jobPosting.City) && jobPosting.StateId > 0) ? ", " : string.Empty) + MiscUtil.GetStateNameById(jobPosting.StateId, Facade);
                    //GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(jobPosting.JobStatus);
                    //if (_RequisitionStatusLookup != null) lblJobStatus.Text = _RequisitionStatusLookup.Name;
                    ////lblClient.Text = "";


                    //{
                    //    if (jobPosting.ClientId > 0)
                    //    {
                    //        Company Client = Facade.GetCompanyById(jobPosting.ClientId);
                    //       // if (Client != null) lblClient.Text = Client.CompanyName;
                    //    }
                    //}
                    //System.Web.UI.HtmlControls.HtmlTableCell tdAction = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAction");
                    //System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thAction");

                   // if (!ShowAction) ShowAction = _isAdmin || jobPosting.CreatorId == CurrentMember.Id || _isHiringManager || isMemberinHinringTeam(CurrentMember.Id, jobPosting.Id);
                    //else
                    //{
                    //    thAction.Visible = true;
                    //    tdAction.Visible = true;
                    //}
                    //if (!_isAdmin && jobPosting.CreatorId != CurrentMember.Id && !_isHiringManager)
                    //{
                    //    //btnDelete.Visible = false;
                    //    //btnEdit.Visible = false;
                    //    //                        btnCreateNewFromExisting.Visible = false;

                    //}
                    //if (_isHiringManager)
                    //{
                    //    ShowAction = !_isHiringManager;
                    //    thAction.Visible = false;
                    //    tdAction.Visible = false;
                    //}

                    //Label lblAssignedManager = (Label)e.Item.FindControl("lblAssignedManager");
                    //Member MemberCreated = null;
                    //if (jobPosting.CreatorId != 0) MemberCreated = Facade.GetMemberById(jobPosting.CreatorId);                  //0.5
                    //if (MemberCreated != null)
                    //    lblAssignedManager.Text = MemberCreated.FirstName + " " + MemberCreated.LastName;   //0.5
                    //try
                    //{
                    //    System.Web.UI.HtmlControls.HtmlTableCell thAssigningManager = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("thAssigningManager");
                    //    System.Web.UI.HtmlControls.HtmlTableCell tdAssigningManager = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAssigningManager");
                    //    System.Web.UI.HtmlControls.HtmlTableCell thClient = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("thClient");
                    //    System.Web.UI.HtmlControls.HtmlTableCell tdClient = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdClient");
                    //    if (RequisitionType == "Master")
                    //    {
                    //        thAssigningManager.Visible = false;
                    //        tdAssigningManager.Visible = false;
                    //        thClient.Visible = true;
                    //        tdClient.Visible = true;
                    //    }
                    //    else
                    //    {
                    //        thAssigningManager.Visible = true;
                    //        tdAssigningManager.Visible = true;
                    //        thClient.Visible = false;
                    //        tdClient.Visible = false;
                    //    }
                    //}
                    //catch
                    //{
                    //}
                    //btnDelete.OnClientClick = "return ConfirmDelete('Requisition')";
                    //btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(jobPosting.Id);

                    //if (jobPosting.ShowInEmployeeReferralPortal)
                    //    lnkEmployeeReferralLink.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/ReferralLink.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "','680px','360px'); return false;");
                    //else
                    //{
                    //    System.Web.UI.HtmlControls.HtmlContainerControl liDivider = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("liDivider");
                    //    if (liDivider != null) liDivider.Visible = false;
                    //    lnkEmployeeReferralLink.Visible = false;
                    //}

                    //if (!IsAccss)
                    //{
                    //    btnEdit.Visible = false;
                    //    //                        btnCreateNewFromExisting.Visible = false;
                    //}
                    //lblNoOfOpenings.Text = jobPosting.NoOfOpenings.ToString();

                    //lblClientJobId.Text = jobPosting.ClientJobId;
                    //if (ApplicationSource == ApplicationSource.GenisysApplication)
                    //{

                    //    thReqCode.Visible = false;
                    //    tdJobCode.Visible = false;

                    //}


                }
            }
        }



        private void PrepareView1()
        {
            div1.Controls.Clear(); 
            //System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Page.Master.FindControl("section-header");
            //currdiv.Visible = false;

          

            StringBuilder strBUildResult = new StringBuilder();

            //int JobId =174;
            //JobPosting JobPosting;
            //JobPosting = Facade.GetJobPostingById(JobId);

            Response.Clear();

            string interview = string.Empty;
            IFacade facade = new Facade();
            IList<JobPosting> JobPosting = new List<JobPosting>();
            JobPosting = facade.MemberCareerAllOpportunities();

           
            //strBUildResult.Append("<td></td>");             

            //if (interviewlist != null)

            //    strBUildResult.Append("<div id = 'd1'>");
            //strBUildResult.Append("<table id='Hearder' style='font-family : Arial;' align=left  width='1800' height='75'>");

            //strBUildResult.Append(" <tr>");
            //strBUildResult.Append("     <td style='color:#3AC0BB' align=left > <font size=5> <u> <b>Opportunities </b></u> </font> </td>");
            //strBUildResult.Append(" </tr>");
            //strBUildResult.Append(" </table>");

            HtmlTable TB_List = new HtmlTable();
          

            TB_List.Attributes.Add("onscroll", "SetScrollPosition()");


            TB_List.Attributes.Add("style", "font-size:16px; FONT-FAMILY: 'Arial';width:100%;height:50%; overflow: auto; zoom: 1; color:#1154af; position: left; margin: auto; padding:5px; ");
            //TB_List.Attributes.Add("style", "background-color:#83BABA");
            TB_List.Width = "100%";
            TB_List.Style.Add("margin", "0");
            TB_List.Align = "center";
            //div1.Attributes.Add("style", "font-size:18px;FONT-FAMILY: 'Arial';width:100%;height:100%; overflow: auto; margin: auto;");
            bigDiv.Attributes.Add("style", "width:100%;height:100%; overflow: auto; margin: auto;");  
      
           // TB_List.Style="width='2400px' align='center'";

            if (JobPosting != null)
            {

                foreach (JobPosting jobpost in JobPosting)
                {
                    //strBUildResult.Append("<td></td>");

                    //for (int i = 0; i < 5; i++)
                    //{

                    SecureUrl url1 = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "CandidatePortal/CandidateRegistration.aspx?JID=" + jobpost.Id + "&FromPage=JobDetail" + "','700px','570px'", string.Empty, jobpost.JobPostingCode);


                    //JobPosting.JobPostingCode = ((i) + Convert.ToInt32(JobPosting.JobPostingCode)).ToString() ;


                    //strBUildResult.Append("<Table Id = 'grid' style=width='2400px' align=center>");

                    //strBUildResult.Append("<tr>");

                    //strBUildResult.Append("<td style='color:white ; width: 1000px; height: 30px; background-color:#088A85;' >");
                    //strBUildResult.Append("<b>Job Title : </b>" + jobpost.City + "<br />");
                    //strBUildResult.Append("</td>");

                    //strBUildResult.Append("</tr>");






                    //****************New code **********************************

                    //Unit width = new Unit(30, UnitType.Pixel);
                    HtmlTableRow Tr = new HtmlTableRow();
                    // Tr.Attributes["style"] = "width: 1200px";               
                    Tr.Attributes.CssStyle.Add("float", "center");
                    //Tr.Attributes["style"] = "font-size='5px'";
                    Tr.Style.Add("font-size", "'5px'");

                    HtmlTableCell td_blank = new HtmlTableCell();
                    td_blank.Style.Add("width", "400px");
                    td_blank.Style.Add("float", "center");

                    HtmlTableCell td_L = new HtmlTableCell();
                    //td_L.Attributes["style"]. = "align:center";
                    td_L.Style.Add("align", "right");
                    td_L.Style.Add("width", "50px");
                    td_L.Style.Add("float", "center");
                    td_L.Style.Add("font-size", "'5px'");
                    td_L.Attributes.Add("style", "text-align:right;");

                    //td_L.Attributes["style"] = "width: 150px";
                    //td_L.Attributes.CssStyle.Add("float", "center");

                    HtmlTableCell td_blank1 = new HtmlTableCell();
                    td_blank1.Style.Add("width", "400px");
                    td_blank1.Style.Add("float", "center");


                    HtmlTableCell td_R = new HtmlTableCell();
                    td_R.Attributes["style"] = "align:center";
                    td_R.Attributes["style"] = "width: 300px";
                    HtmlTableCell td_Bview = new HtmlTableCell();


                    HtmlTableRow Tr_new = new HtmlTableRow();
                    HtmlTableCell td_L_new = new HtmlTableCell();
                    td_L_new.Style.Add("width", "50px");
                    td_L_new.Style.Add("float", "center");
                    td_L_new.Attributes.Add("style", "text-align:right;");
                    HtmlTableCell td_R_new = new HtmlTableCell();

                    HtmlTableCell td_blank2 = new HtmlTableCell();
                    td_blank2.Style.Add("width", "400px");
                    td_blank2.Style.Add("float", "center");

                    HtmlTableRow Tr_loc = new HtmlTableRow();
                    HtmlTableCell td_L_loc = new HtmlTableCell();
                    td_L_loc.Style.Add("width", "50px");
                    td_L_loc.Style.Add("float", "center");
                    td_L_loc.Attributes.Add("style", "text-align:right;");
                    HtmlTableCell td_R_loc = new HtmlTableCell();

                    HtmlTableCell td_blank3 = new HtmlTableCell();
                    td_blank3.Style.Add("width", "400px");
                    td_blank3.Style.Add("float", "center");

                    HtmlTableRow Tr_exp = new HtmlTableRow();
                    HtmlTableCell td_L_exp = new HtmlTableCell();
                    td_L_exp.Style.Add("width", "50px");
                    td_L_exp.Style.Add("float", "center");
                    td_L_exp.Attributes.Add("style", "text-align:right;");
                    HtmlTableCell td_R_exp = new HtmlTableCell();
                    HtmlTableCell td_Bapply = new HtmlTableCell();

                    HtmlTableCell td_blank4 = new HtmlTableCell();
                    td_blank4.Style.Add("width", "400px");
                    td_blank4.Style.Add("float", "center");

                    HtmlTableRow Tr_empty = new HtmlTableRow();
                    HtmlTableCell td_L_empty = new HtmlTableCell();
                    td_L_empty.Style.Add("width", "50px");
                    td_L_empty.Style.Add("float", "center");
                    HtmlTableCell td_R_empty = new HtmlTableCell();
                    HtmlTableCell td_line = new HtmlTableCell();
                    td_L_empty.Attributes["style"] = "color: black";


                    // td_R_empty.Attributes["style"] = "width: 1200px";



                    Tr.ID = "Tr_" + jobpost.JobPostingCode;
                    td_L.ID = "td_L" + jobpost.JobPostingCode;
                    td_R.ID = "td_R" + jobpost.JobPostingCode;

                    Button BtnApply = new Button();
                   // Button BtnView = new Button();
                    HyperLink BtnView = new HyperLink();


                    //BtnView.Attributes["style"] = "color: #1154af";
                    //BtnView.Attributes["style"] = "color: white";
                    //BtnView.Attributes["style"] = "width: 1200px";
                   // BtnView.Attributes["style"] = "height: 10px";
                    // BtnView.Attributes["style"] = "background-color:#088A85";
                    //BtnView.Attributes.Add("style", "width:160px;background-color:#088A85"); 
                    // BtnView.Attributes.Add("style", "width:160px;background-color:#1154af"); 

                    //BtnView.ForeColor = System.Drawing.Color.White;
                    //BtnView.Font.Bold = true;
                    //BtnView.Font.Name = "arial";
                    //BtnView.Font.Size = FontUnit.Larger;

                    BtnView.Attributes.Add("style", "color:#1154af;text-decoration:underline;font-size:15px");
                    BtnView.Font.Name = "arial";
                   
                    BtnApply.Attributes["style"] = "color: white";
                    //BtnApply.Attributes["style"] = "width:1200px";
                    BtnApply.Attributes["style"] = "height: 35px";
                    // BtnApply.Attributes["style"] = "background-color:#088A85";
                    //BtnApply.Attributes.Add("style", "width:160px;background-color:#088A85");
                    BtnApply.Attributes.Add("style", "width:80px;background-color:#1154af"); 
                   
                    // BtnApply.BorderColor  = System.Drawing.Color.DarkGreen ;   

                    BtnApply.ForeColor = System.Drawing.Color.White;
                    //BtnApply.Font.Bold = true;
                    BtnApply.Font.Name = "arial";
                    //BtnApply.Font.Size = FontUnit.Larger;

                    BtnView.ID = "btnView_" + jobpost.JobPostingCode;
                    BtnView.Text = "View Details";
                    BtnView.ToolTip = "Click here,View Details post of " + jobpost.JobTitle;

                    BtnView.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/CareerJobDetail.aspx?JID=" + jobpost.Id + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    //td_L.Controls.Add(BtnApply);

                    td_Bview.Controls.Add(BtnView);


                    td_R.InnerText = jobpost.JobTitle;
                    td_R.Attributes.Add("style", "width:160px,font-weight:bold");
                    td_L.InnerText = "Job Title :";


                    //Tr.Cells.Add(td_blank);
                    Tr.Cells.Add(td_L);
                    Tr.Cells.Add(td_R);
                    Tr.Cells.Add(td_Bview);

                    TB_List.Controls.Add(Tr);
                    div1.Controls.Add(TB_List);

                    //***************************REQ. CODE************************************

                    Tr_new.ID = "Tr_new" + jobpost.JobPostingCode;

                    td_L_new.ID = "td_L_new" + jobpost.JobPostingCode;
                    td_R_new.ID = "td_R_new" + jobpost.JobPostingCode;

                    td_R_new.InnerText = jobpost.JobPostingCode;
                    td_L_new.InnerText = "Req. Code :";

                    //Tr_new.Cells.Add(td_blank1);
                    Tr_new.Cells.Add(td_L_new);
                    Tr_new.Cells.Add(td_R_new);

                    TB_List.Controls.Add(Tr_new);
                    div1.Controls.Add(TB_List);

                    //***************************LOCATION************************************


                    Tr_loc.ID = "Tr_loc" + jobpost.JobPostingCode;

                    td_L_loc.ID = "td_L_loc" + jobpost.JobPostingCode;
                    td_R_loc.ID = "td_R_loc" + jobpost.JobPostingCode;

                    td_R_loc.InnerText = jobpost.City;
                    td_L_loc.InnerText = "Location :";

                    //Tr_loc.Cells.Add(td_blank2);
                    Tr_loc.Cells.Add(td_L_loc);
                    Tr_loc.Cells.Add(td_R_loc);

                    TB_List.Controls.Add(Tr_loc);
                    div1.Controls.Add(TB_List);


                    //***************************EXPERIENCE************************************

                    BtnApply.ID = "btnApply_" + jobpost.JobPostingCode;
                    BtnApply.Text = "Apply";
                    BtnApply.ToolTip = "Click here,Apply the post " + jobpost.JobTitle;
                    BtnApply.OnClientClick = "Redirect('" + url1 + "');";
                    //BtnApply.Attributes.Add("onclick", "EditModal('" + url1.ToString() + "','900px','620px'); return false;");//  use popup windows

                    td_Bapply.Controls.Add(BtnApply);

                    Tr_exp.ID = "Tr_exp" + jobpost.JobPostingCode;

                    td_L_exp.ID = "Td_L_exp" + jobpost.JobPostingCode;
                    td_R_exp.ID = "Td_R_exp" + jobpost.JobPostingCode;

                    string year = "";
                    year = (jobpost.MinExpRequired + ((jobpost.MinExpRequired.Trim() != string.Empty && jobpost.MaxExpRequired.Trim() != string.Empty) ? " - " : "") + jobpost.MaxExpRequired.Trim());

                    if (year == "")
                    {
                        year = "";
                    }
                    else
                    {
                        year = year + " years";
                    }
                    td_R_exp.InnerText = year;
                    td_L_exp.InnerText = "Experience :";

                    //Tr_exp.Cells.Add(td_blank3);
                    Tr_exp.Cells.Add(td_L_exp);
                    Tr_exp.Cells.Add(td_R_exp);
                    Tr_exp.Cells.Add(td_Bapply);

                    TB_List.Controls.Add(Tr_exp);
                    div1.Controls.Add(TB_List);


                    //*****************************Gap between*****************
                    // border-right:2px solid red;
                    td_L_empty.InnerText = "_______________________________________";
                    td_R_empty.InnerText = "__________________________________________________________";
                    td_line.InnerText = "_____________________________________________";
                    td_L_empty.Attributes.Add("style", "text-align:right;");
                    //td_L_empty.Attributes.Add("style", "width:2px;background-color:black");

                    //Tr_empty.Cells.Add(td_blank4);
                    Tr_empty.Cells.Add(td_L_empty);
                    Tr_empty.Cells.Add(td_R_empty);
                    //Tr_empty.Cells.Add(td_R_empty);
                    Tr_empty.Cells.Add(td_line);
                    //Tr_empty.Cells.Add("kk");

                    TB_List.Controls.Add(Tr_empty);
                    div1.Controls.Add(TB_List);

                }
            }




                //strBUildResult.Append("<tr>");

                //strBUildResult.Append("<td>");
                //strBUildResult.Append("<b>Req. Code:</b> " + JobPosting.City + " <br />");
                //strBUildResult.Append("</td>");

                //strBUildResult.Append("<td align=left>");
                //SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "CandidatePortal/CandidateRegistration.aspx?JID=" + JobPosting.JobPostingCode + "&FromPage=JobDetail" + "','700px','570px'", string.Empty, UrlConstants.PARAM_MEMBER_ID, JobPosting.JobPostingCode);
                ////SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "CandidatePortal/CandidateRegistration.aspx",);
                ////strBUildResult.Append(" <a href=\"#\" Target='' onclick=\"window.open('" + url.ToString() + "', '_blank')\">Apply</a>");

                ////strBUildResult.Append(" <input type='button' value='Redirect Me' onclick='Redirect('"+ url.ToString()+"'); />");
                ////strBUildResult.Append(" <input type='button' value='Redirect Me' onclick='Redirect();' />");
                //string btnid1 = "btn" + JobPosting.JobPostingCode;


                //strBUildResult.Append("<input type='button' ID='" + btnid1 + "' runat='server' Text='Button' OnClientClick ='EditModal(('" + UrlConstants.ApplicationBaseUrl + "Modals/CareerJobDetail.aspx?JID=714&FromPage=JobDetail" + "','700px','570px'); return false;')' />");
                // strBUildResult.Append("<asp:LinkButton ID='lnkTeamCount'  text='Apply'  runat='server'  OnClick='EditModal(('" + UrlConstants.ApplicationBaseUrl + "Modals/CareerJobDetail.aspx?JID=714&FromPage=JobDetail" + "','700px','570px'); return false;')' ></asp:LinkButton>");
                

                ////strBUildResult.Append("<asp:Button ID='TestButton' Text='Test Me' OnClick= 'Redirect();' runat='server'/>");
                ////strBUildResult.Append("<a    class=\"~\btn\"  href='" + url.ToString() + "' Target='_blank' style='margin-right:2px' >Edit</a>");
                ////strBUildResult.Append(" <a href=\"#\"  onclick=\"window.location=" + url.ToString()+">Apply</a>");
                ////strBUildResult.Append(" <a href=\"#\" Target='' onclick=\"Response.Redirect(CandidatePortal/CandidateRegistration.aspx)\">Apply</a>");
                //strBUildResult.Append("</td>");

                //strBUildResult.Append("</tr>");

                //strBUildResult.Append("<tr>");
                //strBUildResult.Append("<td>");
                //strBUildResult.Append("<b>Location: </b> " + JobPosting.City + " <br />");
                //strBUildResult.Append("</td>");
                //strBUildResult.Append("</tr>");

                //strBUildResult.Append("<tr>");
                //strBUildResult.Append("<td>");
                //strBUildResult.Append("<b>Experience (yrs):</b> " + JobPosting.MinExpRequired + " - " + JobPosting.MaxExpRequired + "<br />");
                //strBUildResult.Append("</td>");
                //strBUildResult.Append("</tr>");


                //string interviewskills = JobPosting.JobSkillLookUpId;
                //if (interviewskills != "")
                //{
                //    JobPosting.JobSkillLookUpId = interviewskills.Substring(0, interviewskills.Length - 2);
                //}

                //strBUildResult.Append("<tr>");
                //strBUildResult.Append("<td>");
                //strBUildResult.Append("<b>Required Skills:</b> " + JobPosting.JobSkillLookUpId + " - " + JobPosting.MaxExpRequired + "<br />");
                //strBUildResult.Append("</td>");
                //strBUildResult.Append("</tr>");

                //strBUildResult.Append("<tr>");
                //strBUildResult.Append("<td>");
                //strBUildResult.Append("<b>Job Description:</b> " + JobPosting.JobDescription + " - " + JobPosting.MaxExpRequired + "<br />");
                //strBUildResult.Append("</td>");
                //strBUildResult.Append("</tr>");

                //string btnid = "btn" + JobPosting.JobPostingCode;

                //strBUildResult.Append("<tr style=width='1400' align=center>");
                //strBUildResult.Append(" <td  <b>--------------------------------------------------------------------------------------------------<br /> </td>");
                //strBUildResult.Append("</tr>");

                //strBUildResult.Append("</Table>");
                //strBUildResult.Append("</div>");
                ////strBUildResult.Append("    <tr>");
                ////strBUildResult.Append("   <td height='30'></td>");
                ////strBUildResult.Append("    </tr>");


            //}
            //Response.Write(strBUildResult.ToString());
            //Response.End();
            //    div1.Controls.Add(strBUildResult);
            //div1.InnerHtml =

            //    div1.Controls.Add(<asp:LinkButton ID='lnkTeamCount'  text='Apply'  runat='server'  OnClick='EditModal(('" + UrlConstants.ApplicationBaseUrl + "Modals/CareerJobDetail.aspx?JID=714&FromPage=JobDetail" + "','700px','570px'); return false;')' ></asp:LinkButton>);
                
            
        }
    






        protected void lsvJobPosting_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    Session.Add("List", 3);
                    Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_EDITOR_PAGE, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(id));
                }
                else if (string.Equals(e.CommandName, "Managers"))
                {
                    ////uclTemplateManagers.ModalTitle = uclManagers .setCurrentJobPosting (Convert .ToInt32 (e.CommandArgument )) + " - Assigned Team";
                    //uclManagers.JobPostingId = Convert.ToInt32(e.CommandArgument);
                    //uclTemplateManagers.ModalTitle = uclManagers.Job_Posting.JobTitle + " - Assigned Team";
                    //mpeManagers.Show();
                    //System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thAction");
                    //ShowAction = thAction.Visible;

                }
                else if (string.Equals(e.CommandName, "CreateNewFromExisting"))
                {
                    try
                    {
                        JobPosting jobPosting = new JobPosting();
                        int intReqCount = Facade.GetLastRequisitionCodeByMemberId(CurrentMember.Id);
                        string strMemberJobCount = "";
                        if (strMemberJobCount.Length < 4)
                            strMemberJobCount = (intReqCount + 1).ToString("#0000");
                        else strMemberJobCount = ((intReqCount + 1)).ToString();

                        string jobPostingCode = (base.CurrentMember.LastName.IsNullOrEmpty() ? (Convert.ToString(base.CurrentMember.FirstName.Substring(0, 2))) : ((Convert.ToString(base.CurrentMember.FirstName.Substring(0, 1)) + Convert.ToString(base.CurrentMember.LastName.Substring(0, 1))))).ToUpper() + strMemberJobCount;
                        jobPosting = Facade.CreateJobPostingFromExistingJob(id, jobPostingCode, base.CurrentMember.Id);//0.1 and 1.2
                        IList<JobPostingHiringTeam> JPHTT = Facade.GetAllJobPostingHiringTeamByJobPostingId(id);
                        bool IsCurrentMember = false;
                        if (JPHTT != null)
                        {
                            for (int i = 0; i < JPHTT.Count; i++)
                            {
                                if (JPHTT[i].MemberId == base.CurrentMember.Id)
                                {
                                    IsCurrentMember = true;
                                }
                            }
                            for (int i = 0; i < JPHTT.Count; i++)
                            {
                                JobPostingHiringTeam JPHT = new JobPostingHiringTeam();
                                JPHT.MemberId = JPHTT[i].MemberId;
                                JPHT.JobPostingId = jobPosting.Id;
                                JPHT.UpdateDate = System.DateTime.Now;
                                JPHT.UpdatorId = base.CurrentMember.Id;
                                JPHT.CreateDate = System.DateTime.Now;
                                JPHT.EmployeeType = "Internal";
                                JPHT.CreatorId = base.CurrentMember.Id;
                                Facade.AddJobPostingHiringTeam(JPHT);
                                MemberJobCart memberJobCart = new MemberJobCart();
                                memberJobCart.MemberId = base.CurrentMember.Id;
                                memberJobCart.JobPostingId = jobPosting.Id;
                                memberJobCart.ApplyDate = DateTime.Now;
                                Facade.AddMemberJobCart(memberJobCart);
                                Facade.UpdateMemberRequisitionCount(base.CurrentMember.Id, intReqCount + 1);
                                if (!IsCurrentMember)
                                {
                                    JobPostingHiringTeam JPHT1 = new JobPostingHiringTeam();
                                    JPHT1.MemberId = base.CurrentMember.Id;
                                    JPHT1.JobPostingId = jobPosting.Id;
                                    JPHT1.UpdateDate = System.DateTime.Now;
                                    JPHT1.UpdatorId = base.CurrentMember.Id;
                                    JPHT1.CreateDate = System.DateTime.Now;
                                    JPHT1.EmployeeType = "Internal";
                                    JPHT1.CreatorId = base.CurrentMember.Id;
                                    Facade.AddJobPostingHiringTeam(JPHT1);

                                    MemberJobCart memberJobCart1 = new MemberJobCart();
                                    memberJobCart1.MemberId = base.CurrentMember.Id;
                                    memberJobCart1.JobPostingId = jobPosting.Id;
                                    memberJobCart1.ApplyDate = DateTime.Now;
                                    Facade.AddMemberJobCart(memberJobCart1);
                                    Facade.UpdateMemberRequisitionCount(base.CurrentMember.Id, intReqCount + 1);
                                }
                            }
                        }
                        if (jobPosting != null)
                            Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_EDITOR_PAGE, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id));
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteJobPostingById(id, base.CurrentMember.Id))
                        {
                           // BindList();
                            MiscUtil.ShowMessage(lblMessage, "Requisition has been successfully deleted.", false);
                        }
                        Facade.DeleteMemberJobCartById(base.CurrentMember.Id);
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }

        protected void lsvJobPosting_PreRender(object sender, EventArgs e)
        {
            //try
            //{
            //    ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            //    if (PagerControl != null)
            //    {
            //        DataPager pager = (DataPager)PagerControl.FindControl("pager");
            //        if (pager != null)
            //        {
            //            DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
            //            pager.PageSize = 10000;
            //            if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
            //        }

            //        HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
            //        if (hdnRowPerPageName != null)
            //        {
            //            if (RequisitionType == "My") hdnRowPerPageName.Value = "MyRequisitionListRowPerPage";
            //            else hdnRowPerPageName.Value = "MasterRequisitionListRowPerPage";
            //        }
            //        //if (!ShowAction)
            //        //{
            //        //    foreach (ListViewItem item in lsvJobPosting.Items)
            //        //    {
            //        //        System.Web.UI.HtmlControls.HtmlTableCell tdAction = (System.Web.UI.HtmlControls.HtmlTableCell)item.FindControl("tdAction");
            //        //        tdAction.Visible = false;

            //        //    }
            //        //    System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("tdPager");
            //        //    tdpager.ColSpan = 10;
            //        //}
            //        //else
            //        //{
            //        //    System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thAction");
            //        //    thAction.Visible = true;
            //        //    System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("tdPager");
            //        //    tdpager.ColSpan = 11;
            //        //}
            //    }
            //    if (ApplicationSource == ApplicationSource.GenisysApplication)
            //    {
            //        //System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("tdPager");
            //        //tdpager.ColSpan = 10;
                              
            //    }




            //}
            //catch
            //{
            //}
           // PlaceUpDownArrow();
            //if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            //{
            //    LinkButton lnkClient = (LinkButton)lsvJobPosting.FindControl("lnkClient");
            //    if (lnkClient != null)
            //    {
            //        if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
            //            lnkClient.Text = "Account";
            //        else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
            //            lnkClient.Text = "BU";
            //        else lnkClient.Text = "Vendor";
            //    }
            //}
        }
        protected void odsRequisitionList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //if (RequisitionType == "Master")
            //{
            //    ShowAction = false;
            //    System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thAction");
            //    //thAction.Visible = false;
            //}
        }

        #endregion

        #endregion
    }
}