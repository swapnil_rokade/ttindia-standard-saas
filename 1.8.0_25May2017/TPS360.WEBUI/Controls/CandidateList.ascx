﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:CandidateList.ascx
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Jan-8-2008          Gopala Swamy          Defect id:9679(Kaizen) ; Commented "Send bulk mail" button  and corresponding label
    0.2             Dec-08-2009         Sandeesh              Defect id:11511 - For dispalying current state and current city instead of permanent state and city.     
    0.3             23/June/2016        pravin khot           added new Source,SourceDescription
    0.4             19/Sep/2017         Sumit Sonawane        copy paste disabled for txtCandidateID
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CandidateList.ascx.cs"
    Inherits="TPS360.Web.UI.ControlCandidateList" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src ="~/Controls/CountryStatePicker.ascx" TagName ="CountryState" TagPrefix ="ucl" %>
<%@ Register Src ="~/Controls/CompanyRequisitionPicker.ascx" TagName ="CompReq" TagPrefix ="ucl" %>
<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<style>
  
    .BulkActionContext
    {
        cursor: pointer;
        position: absolute;
    }
    .BulkActionContext_List
    {
        display: block;
        top: 0px;
        clear: both;
        font-weight: bold;
        margin: 0;
        margin-left: 113px;
        padding: 0;
        border: 1px solid #4291df;
        background-color: #4291df;
        position: relative;
        width: 122px;
    }
    .BulkActionContext_List li
    {
        margin-left: -114px;
        position: relative;
        clear: both;
        padding-left: 8px;
        padding-bottom: 20px;
        padding-top: 7px;
        color: #555555;
        border-bottom: 1px solid #FCFCFC;
        border-top-style: none;
        border-top-width: 0px;
        border-bottom-color: #4291df;
        border-left-color: #4291df;
        border-right-color: #4291df;
        margin-top: 0px;
        z-index: 9999;
        background-color: #EFF6FF;
    }
    .BulkActionContext_List li:hover
    {
        border-top: none;
        border-color: #4291df;
    }
    .BulkActionContext #icon
    {
        padding: 0 7px;
        position: relative;
        z-index: 9999;
        float: left;
        border-width: 1px;
        border-style: solid;
        border-color: #4291df #4291df #4291df;
        top: 1px;
        clear: both;
        background-color: #EFF6FF;
        border-bottom-style: none;
        display: block;
        height: 20px;
        line-height: 23px;
        text-align: center;
        margin: 2px 2px 0 0;
        padding-bottom: 6px;
    }
    .BulkActionContext #icon:hover
    {
        position: relative;
        border-top: none;
        border-color: #4291df;
        margin-top: 3px;
    }
    .BulkActionContext a
    {
        color: #1F1FFF;
        text-decoration: none;
    }
    .BulkActionContext a:hover
    {
        color: #0000ff;
        text-decoration: none;
    }
</style>

<script src="../js/CompanyRequisition.js"></script>

<script language="javascript" type="text/javascript">

    /////////////////////////////////////
document.body.onclick = function(e)
  {
  $('#<%= pnlSearchBoxContent.ClientID %>').css({'overflow-y':'','overflow-x':''});
  $('#<%= pnlSearchBoxContent.ClientID %> div:first').css({'overflow-y':'','overflow-x':''});
   $('input[rel=Integer]').keypress(function (event){return allowInteger(event,$(this));});
  }

  var n = 10;
  function ValidateReq(source, args) {
      validator = document.getElementById(source.id);
      var hdnField = document.getElementById('ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS')
      if (hdnField.value != '') args.IsValid = true;
      else {
          validator.innerHTML = "<br/>Please select candidate.";

          ShowDiv('divReqs'); n = 0;
          args.IsValid = false;
      }




  }

    function ValidateHotList(source, args) {

        validator = document.getElementById(source.id);
        var hdnField = document.getElementById('ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS')
        hdnSelectedHotList = document.getElementById("ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedHotList");
        ddlHotlist = document.getElementById("ctl00_cphHomeMaster_ucntrlCandidateList_ddlHotlist");
        if (hdnSelectedHotList.value != '' || ddlHotlist.options[ddlHotlist.selectedIndex].value != "0") {
           
            if (hdnField.value != '') args.IsValid = true;
            else {
                validator.innerHTML = "<div>Please select candidate.<div>";
                ShowDiv('divHotListdiv'); n = 0;
                args.IsValid = false;
            }
        }
        else {
            validator.innerHTML = "Please select a hot list.";            
            ShowDiv('divHotListdiv'); n = 0;
            args.IsValid = false;
        }

    }

    function monitorClick(e) {
        try {
        
        
            var evt = (e) ? e : event;

            var theElem = (evt.srcElement) ? evt.srcElement : evt.target;
           
            while (theElem != null) {
                 if( $('#' + theElem .id).parents('.btn-group') ==null || theElem .id=='' || theElem .id.indexOf('updPanelCandidateList')>=0){

                    n = 10;
                    var divReqs = document.getElementById('divReqs');
                    if (divReqs != null) {
                        $(divReqs).closest('.btn-group').removeClass('open');
                        divReqs.style.display = "none";
                    }
                    var divHotListdiv = document.getElementById('divHotListdiv');
                    if (divHotListdiv != null) {
                        $(divHotListdiv).closest('.btn-group').removeClass('open');
                        divHotListdiv.style.display = "none";
                    }
                    return true;
                }
                else {
                    n = 10;
                    return true;
                }

                theElem = theElem.offsetParent;
            }
        }
        catch (e) {
        } n = 10;
        return true;
    }
    document.onclick = monitorClick;

    function ShowDiv(divId) {

        n = 10;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function DropdownClicked(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }

    function DropdownChange(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function ShowReqDiv(divId) {
        $('.btn-group').removeClass('open');
        var a = document.getElementById("divReqs");
        var b = document.getElementById("divHotListdiv");
        if (a != null) a.style.display = "none";
        if (b != null) b.style.display = "none";

        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) {
            $(reqDiv).closest('.btn-group').addClass('open');
            reqDiv.style.display = "";
            reqDiv.parentElement.className = "btn-group open";
        } return false;
    }

    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
    function PageLoadedReq(sender, args) {
        
        try {
            var chkBox = document.getElementById('chkAllItem');
            if (chkBox != null) chkBox.disabled = false;
            CheckUnCheckGridViewHeaderCheckbox('tlbTemplate', 'chkAllItem', 0, 1);
        }
        catch (e) {
        }
    }

    function RequisitionSelected(source, eventArgs) {
        hdnSelectedRequisition = document.getElementById("ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedRequisition");
        hdnSelectedRequisitionText = document.getElementById("ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedRequisitionText");
        hdnSelectedRequisitionText.value = eventArgs.get_text();
        hdnSelectedRequisition.value = eventArgs.get_value();
    }

    function CLCheckUnCheckAllRowLevelCheckboxes(cntrlHeaderCheckbox, posCheckbox) {


        var cntrlGridView = document.getElementById("tlbTemplate");
        var chkBoxAll = document.getElementById(cntrlHeaderCheckbox);
        var hndID = "";
        var chkBox = "";
        var checked = 0;
        var rowNum = 0;
     
        if (cntrlHeaderCheckbox != null && cntrlHeaderCheckbox.checked != null) {
           
            var rowLength = 0;
            rowLength = cntrlGridView.rows.length;
          
            for (var i = 1; i < rowLength; i++) {
                var myrow = cntrlGridView.rows[i];
             
                var mycel = myrow.getElementsByTagName("td")[posCheckbox];
                           
                if (mycel.childNodes[0].value == "on") {
                    chkBox = mycel.childNodes[0];
                }
                else {
                    chkBox = mycel.childNodes[1];
                }
              
                try {
                    if (mycel.childNodes[2].value == undefined) {
                        hndID = mycel.childNodes[3];
                    }
                    else {
                        hndID = mycel.childNodes[2];
                    }
                }
                catch (e) {
                }
               
                if (chkBox != null) {
                    if (chkBox.checked == null) {
                        chkBox = chkBox.nextSibling;
                    }
                    if (chkBox != null) {
                        if (chkBox.checked != null) {
                            chkBox.checked = cntrlHeaderCheckbox.checked;
                            rowNum++;
                        }
                    }
                }
                if (chkBox != null) {
                    if (chkBox.checked != null && chkBox.checked == true) {
                        if (chkBox.id.indexOf('chkItemCandidate') > -1) {
                            if (hndID != null) ADDID(hndID.value, 'ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS');
                            checked++;

                        }
                    }
                    else {
                        if (chkBox.id.indexOf('chkItemCandidate') > -1) {
                            if (hndID != null) RemoveID(hndID.value, 'ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS');
                        }
                    }
                }
            }
        }

        if (checked == rowNum) {
            if (chkBoxAll != null) {
                chkBoxAll.checked = true;
            }
        }
        else {
            if (chkBoxAll != null) {
                chkBoxAll.checked = false;
            }
        }
    }

    function AssignValue(optionvalue) {
        var selectedvalue = document.getElementById('<%=txtSelectValue.ClientID %>');
        selectedvalue.value = optionvalue
    }

    function ReAssign() {
        var selectedvalue = document.getElementById('<%=txtSelectValue.ClientID %>');
        selectedvalue.value = "NoSelect";
    }

    function checkEnter(e) {
        var kC = window.event ? event.keyCode :
            e && e.keyCode ? e.keyCode :
            e && e.which ? e.which : null;
        if (kC == 13) {
            var selectedvalue = document.getElementById('<%=txtSelectValue.ClientID %>');
            if (selectedvalue.value == "Hotlist") {
                var btnclick = document.getElementById('ctl00_cphHomeMaster_ucntrlCandidateList_btnAddToHotList');
                btnclick.focus();
            }
            else if (selectedvalue.value == "Requisition") {
                var btnclick = document.getElementById('ctl00_cphHomeMaster_ucntrlCandidateList_btnAddToPSList');
                btnclick.focus();
            }
        }
    }
              
</script>

 <script type="text/javascript">
     $(document).ready(function() {
         $('#txtCandidateID').bind('copy paste cut', function(e) {
             e.preventDefault(); //disable cut,copy,paste
         });
     });
    </script>

<div>
    <asp:TextBox ID="txtSortColumn" runat="server" Visible="false" Text ="btnName"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Visible="false" Text ="ASC"></asp:TextBox>
    <div style="display: none">
        <asp:TextBox ID="txtSelectValue" Text="NoSelect" runat="server" />
    </div>
    <asp:HiddenField ID="hdnSelectedRequisitionText" runat="server" />
    <asp:HiddenField ID="hdnSelectedRequisition" runat="server" />
    <asp:HiddenField ID="hdnCurrentMemberId" runat="server" />
    <asp:HiddenField ID="hdnSelectedHotList" runat="server" />
    <asp:HiddenField ID="hdnSelectedHotListText" runat="server" />
    <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
    <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
    <asp:ObjectDataSource ID="odsCandidateList" runat="server" SelectMethod="GetPagedCandidateList"
        TypeName="TPS360.Web.UI.CandidateDataSource" SelectCountMethod="GetListCountCandidateList"
        EnablePaging="True" SortParameterName="sortExpression">
        <SelectParameters>
            <asp:Parameter Name="memberId" DefaultValue="0" />
            <asp:ControlParameter ControlID="txtCandidateName" PropertyName="Text" Name="CandidateName" />
            <asp:ControlParameter ControlID="txtCandidateEmail" PropertyName="Text" Name="CandidateEmail" />
            <asp:ControlParameter ControlID="txtCurrentPosition" PropertyName="Text" Name="Position" />
            <asp:ControlParameter ControlID="ddlAssignedManager" PropertyName="SelectedValue"
                Name="AssignedManager" />
            <asp:ControlParameter ControlID="ddlHiringStatus" PropertyName="SelectedValue" Name="HiringLevel" />
            <asp:ControlParameter ControlID="txtMobilePhone" PropertyName="Text" Name="MobilePhone" />
            <asp:ControlParameter ControlID="txtCity" PropertyName="Text" Name="City" />
            <asp:ControlParameter ControlID="uclCountryState" PropertyName="SelectedStateId" Name="StateId" Type ="String"  />
            <asp:ControlParameter ControlID="uclCountryState" PropertyName="SelectedCountryId" Name="CountryId" Type ="String"  />
            <asp:ControlParameter ControlID="ddlSource" PropertyName="SelectedValue" Name="SourceID" />
             <asp:ControlParameter ControlID ="txtCandidateID" PropertyName ="Text" Name ="CandidateID" />
            <asp:Parameter Name ="CandidateType"  DefaultValue ="0" />
            <asp:Parameter Name="SortOrder" DefaultValue="asc" />
            <asp:Parameter Name="SortColumn" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:UpdatePanel ID="updPanelCandidateList" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnSelectedIDS" runat="server" />
            <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
            <div class="TableRow" style="text-align: left;">
                <asp:Label ID="lblMessage" runat="server" EnableViewState="false" style="z-index:9999"></asp:Label>
                <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
            </div>
            <asp:Panel ID="pnlSearchBoxBody" runat="server" CssClass="well well-small nomargin"
                DefaultButton="btnSearch">
                <asp:Panel ID="pnlSearchRegion" runat="server">
                    <asp:CollapsiblePanelExtender ID="cpeSearch" runat="server" TargetControlID="pnlSearchBoxContent"
                        ExpandControlID="pnlSearchBoxHeader" CollapseControlID="pnlSearchBoxHeader" CollapsedImage="~/Images/expand-plus.png"
                        ExpandedImage="~/Images/collapse-minus.png" Collapsed="true" ImageControlID="imgSearchBoxToggle"
                        EnableViewState="true" SuppressPostBack="True">
                    </asp:CollapsiblePanelExtender>
                    <asp:Panel ID="pnlSearchBoxHeader" runat="server">
                        <div class="SearchBoxContainer">
                            <div class="SearchTitleContainer">
                                <div class="ArrowContainer">
                                    <asp:ImageButton ID="imgSearchBoxToggle" runat="server" ImageUrl="~/Images/expand-plus.png"
                                        AlternateText="collapse" />
                                </div>
                                <div class="TitleContainer">
                                    Filter Options
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlSearchBoxContent" runat="server" CssClass ="filter-section" Height="0">
                        <%--  <div class="BoxContainer" >--%>
                        <div class="TableRow spacer">
                            <div class="FormLeftColumn" style="width: 50%;">
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblCandidateName" runat="server" Text="Candidate Name"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:TextBox ID="txtCandidateName" runat="server" CssClass="SearchTextbox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblCandidateEmail" runat="server" Text="Candidate Email"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:TextBox ID="txtCandidateEmail" runat="server" CssClass="SearchTextbox"></asp:TextBox>
                                    </div>
                                </div>
                                
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblCandidateID" runat="server" Text="Candidate ID"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:TextBox ID="txtCandidateID" runat="server" oncopy="return false" onpaste="return false" oncut="return false" CssClass="ApplicantIDTextbox"  rel="Integer"></asp:TextBox>
                                    </div>
                                </div>
                                
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblCurrentPosition" runat="server" Text="Current Position"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:TextBox ID="txtCurrentPosition" runat="server" CssClass="SearchTextbox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblAssignedManagers" runat="server" Text="Assigned Manager"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlAssignedManager" runat="server" CssClass="CommonDropDownList">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblHiringStatus" runat="server" Text="Hiring Status"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlHiringStatus" CssClass="CommonDropDownList" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="FormRightColumn" style="width: 49%">
                                <div class="TableRow" style="white-space: nowrap;">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblMobilePhone" runat="server" Text="Mobile"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:TextBox EnableViewState="false" ID="txtMobilePhone" CssClass="SearchTextbox"
                                            runat="server" rel="Integer" MaxLength="12"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="TableRow" style="white-space: nowrap;">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblByLocation" runat="server" Text="City"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:TextBox EnableViewState="false" ID="txtCity" CssClass="SearchTextbox" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <ucl:CountryState ID="uclCountryState" runat ="server" FirstOption="Any" ApplyDefaultSiteSetting ="False"  TableFormLabel_Width="40%" />
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblCandidateType" runat="server" Text="Source"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlSource" CssClass="CommonDropDownList" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px">
                            <asp:LinkButton ID="btnSearch" runat="server" AlternateText="Search" CssClass="btn btn-primary"
                                ValidationGroup="RequisitionList" EnableViewState="false" OnClick="btnSearch_Click"
                                CausesValidation="false"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                            <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                                EnableViewState="false" OnClick="btnClear_Click" />
                        </div>
                        <%--            </div>--%>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <div id="divBulkActions" class="btn-toolbar" runat="server">
                <div class="pull-left">
                    <div class="btn-group">
                        <a href="Javascript:void(0)" onclick="ShowReqDiv('divReqs')" id="refReq" class="btn btn-medium dropdown-toggle">
                            Add to Requisition <span class="caret"></span></a>
                        <ul class="dropdown-menu-custom" id="divReqs" style="display: none">
                            <li style="width: 220px; text-align: left" id="list1">
                               <ucl:CompReq id="uclComReq" runat="server"></ucl:CompReq>
                                <asp:Button ID="btnAddToPSList" ValidationGroup="RequisitionList" CssClass="btn btn-small"
                                    runat="server" Text="Add" OnClick="btnAddToPSList_Click" onmouseover="ShowDiv('divReqs')" />
                                <asp:CustomValidator ID="cvRequisition" runat="server" ClientValidationFunction="ValidateReq"
                                    Display="Dynamic" ErrorMessage="Please select a requisition." ValidationGroup="RequisitionList"
                                    Font-Bold="false"></asp:CustomValidator>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <a href="Javascript:void(0)" onclick="ShowReqDiv('divHotListdiv')" class="btn dropdown-toggle"
                            id="refHot")">Add to Hot List <span class="caret"></span></a>
                        <ul class="dropdown-menu-custom" id="divHotListdiv" style="display: none">
                            <li style="width: 220px; text-align: left" id="list">
                                <asp:DropDownList EnableViewState="true" ID="ddlHotlist" runat="server" CssClass="chzn-select"
                                    onmouseover="ShowDiv('divHotListdiv')" onclick="DropdownClicked('divHotListdiv')"
                                    Width="150px" onchange="DropdownChange('divHotListdiv')">
                                </asp:DropDownList>
                                <asp:TextBox ID="TxtHotList" runat="server" Visible="true" onfocus="AssignValue('Hotlist')"
                                    onblur="ReAssign()" onkeypress="checkEnter(event)" Width="150px" EnableViewState="true"
                                    onmouseover="ShowDiv('divHotListdiv')"></asp:TextBox>
                                <asp:Button ID="btnAddToHotList" CssClass="btn btn-small" runat="server" Text="Add" style=" margin-bottom : 10px"
                                    ValidationGroup="HotList" OnClick="btnAddToHotList_Click" onmouseover="ShowDiv('divHotListdiv')" />
                                <div id="divHot">
                                </div>
                                <ajaxToolkit:AutoCompleteExtender ID="hot_ListAutoComplete" runat="server" ServicePath="~/AjaxExtender.asmx"
                                    TargetControlID="TxtHotList" MinimumPrefixLength="1" ServiceMethod="GetHotListItems"
                                    EnableCaching="true" CompletionListElementID="divHot" CompletionListCssClass="AutoCompleteBox"
                                    CompletionInterval="0" FirstRowSelected="True" OnClientItemSelected="ValidateHotList" />
                                <%--OnClientPopulated="BoldChildrenHotList" BehaviorID="AutoCompleteExHotList"--%>
                                <%-- <asp:CompareValidator ID="cvHotlist" runat="server" ControlToValidate="ddlHotlist" Type="Integer" Operator="NotEqual" ValueToCompare="0" ErrorMessage="Please select a hot list." EnableViewState="False" Display="Dynamic" ValidationGroup="HotList" Font-Size ="Small" Font-Bold ="false"     ></asp:CompareValidator>--%>
                                <asp:CustomValidator ID="cvHotList" runat="server" ErrorMessage="Please select a hot list."
                                    EnableViewState="False" Display="Dynamic" ValidationGroup="HotList" Font-Size="Small"
                                    Font-Bold="false" ClientValidationFunction="ValidateHotList"></asp:CustomValidator>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <asp:LinkButton ID="btnEmail" runat="server" Text="Email Candidates" OnClick="btnEmail_Click"
                            CssClass="btn btn-medium" />
                    </div>
                    <div class="btn-group" runat="server" id="liRemove">
                        <asp:LinkButton ID="btnRemove" runat="server" Text="Delete" OnClick="btnRemove_Click"
                            CssClass="btn btn-danger" />
                    </div>
                </div>
                    <div style="float: right; margin-top: 10px; margin-right: 4px; margin-bottom: 3px;">
                    Name Format:
                    <asp:DropDownList ID="ddlNameFormat" runat="server" CssClass="CommonDropDownList"
                        Width="100px" OnSelectedIndexChanged="ddlNameFormat_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Text="First Last" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Last, First" Value="1"></asp:ListItem>
                    </asp:DropDownList>
                </div>                                          
            </div>
            <div class="GridContainer" style="overflow: auto">
                <asp:ListView ID="lsvCandidateList" runat="server" DataKeyNames="Id" OnItemDataBound="lsvCandidateList_ItemDataBound"
                    EnableViewState="true" OnItemCommand="lsvCandidateList_ItemCommand" OnPreRender="lsvCandidateList_PreRender">
                    <LayoutTemplate>
                        <table id="tlbTemplate" class="Grid" cellspacing="0" border="0">
                            <tr runat="server" id="trPrecise">
                                <th style="width: 25px !important; white-space: nowrap;">
                                    <%--<asp:CheckBox runat="server" ID="chkAllItem" />--%>
                                    <%--<input id="chkAllItem" name="chkAllItem" type="checkbox" onclick="CheckUnCheckAllRowLevelCheckboxes(this,tlbTemplate,0)" />--%>
                                    <input id="chkAllItem" name="chkAllItem" type="checkbox" onclick="CLCheckUnCheckAllRowLevelCheckboxes(this,0)"
                                        disabled="disabled" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnid" runat="server" ToolTip="Sort By ID"  CommandName="Sort"
                                        CommandArgument="[C].[ID]" Text="ID #" />
                                </th>
                                <th >
                                    <asp:LinkButton ID="btnName" runat="server" ToolTip="Sort By Name"  CommandName="Sort"
                                        CommandArgument="[C].[CandidateName]" Text="Name" />
                                </th>
                                <th >
                                    <asp:LinkButton ID="btnPosition" runat="server" ToolTip="Sort By Position" CommandName="Sort"
                                        CommandArgument="[C].[CurrentPosition]" Text="Position" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnLocation" runat="server" ToolTip="Sort By Location" CommandName="Sort"
                                        CommandArgument="[C].[PermanentCity]" Text="Location"  />
                                    <%--0.2--%>
                                </th>
                                <th>
                                    <asp:Label ID="btnMobilePhone" runat="server" Text="Mobile" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnEmail" runat="server" ToolTip="Sort By Email" CommandName="Sort"
                                        CommandArgument="[C].[PrimaryEmail]" Text="Email"  />
                                </th>
                                   <%-- *******************ADDED BY PRAVIN KHOT ON 23/June/2016************--%>
                                  <th>
                                        <asp:Label ID="btnSource" runat="server" Text="Source" />
                                      <%-- <asp:LinkButton ID="btnSource" runat="server" ToolTip="Sort By Source" CommandName="Sort"
                                        CommandArgument="[GS].[Name]" Text="Source"  />       --%>                             
                                </th>
                                
                                 <th>
                                      <asp:Label ID="btnSourceDescription" runat="server" Text="Source Description" />
                                     <%-- <asp:LinkButton ID="btnSourceDescription" runat="server" ToolTip="Sort By Source Description" CommandName="Sort"
                                        CommandArgument="[ME].[SourceDescription]" Text="Source Description"  />--%>
                                </th>
                                  <%--  *******************************END********************************--%>
                                <th>
                                    <asp:Label ID="lblRemarksHeader" runat="server" Text="Remarks" />
                                </th>
                                
                               
                                
                                <th id="thAction" runat="server" style="text-align: center; width: 55px !important; white-space: nowrap;"
                                    visible="true">
                                    Action
                                </th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="10" id="tdPager" runat="server">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No data was returned.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td>
                                <asp:CheckBox runat="server" ID="chkItemCandidate" OnClick="CheckUnCheckGridViewHeaderCheckbox('tlbTemplate','chkAllItem',0,1)" />
                                <asp:HiddenField ID="hdnID" runat="server" />
                            </td>
                            <td>
                            <asp:Label ID="lblID" runat="server" />
                           
                            </td>
                            
                            <td>
                                <%--<asp:Label ID="lblName" runat="server" />--%>
                                <asp:HyperLink ID="lnkCandidateName"  runat="server"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:Label ID="lblPosition" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblLocation" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblMobilePhone" runat="server" />
                            </td>
                            <td>
                                <%--<asp:Label ID="lblEmail" runat="server" />--%>
                                <asp:LinkButton ID="lblEmail" runat="server" />
                            </td>
                              <%-- *******************ADDED BY PRAVIN KHOT ON 23/June/2016************--%>
                             <td>
                                <asp:Label ID="lblSource" runat="server" />
                            </td>
                             <td>
                                <asp:Label ID="lblSourceDescription" runat="server" />
                            </td>
                              <%--  *******************************END********************************--%>
                            <td>
                                <asp:Label ID="lblRemarks" runat="server" />
                            </td>
                            <td style="text-align: center; white-space: nowrap;" visible="true" id="tdAction">
                                <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" CommandName="EditItem">
                                </asp:ImageButton>
                                <asp:ImageButton ID="btnDelete" Visible="true" SkinID="sknDeleteButton" runat="server"
                                    CommandName="DeleteItem"></asp:ImageButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
