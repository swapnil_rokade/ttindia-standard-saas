﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: SearchCompany.ascx.cs
    Description: This is the user control page used to search for company .
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Sep-11-2008           Jagadish             Applied site setting to country dropdown.
    0.2              Feb-19-2009           Gopala Swamy J       Defect Id:9973;put ternory operation
    0.3              Mar-18-2009           Nagarathna V.B       Defect Id:10137;avoid search operation on clear button click.
 *  0.4              Mar-19-2009           Rajendra A.S         Defect Id:10122;Avoid search operation on clear button click.
    0.5              Juny-23-2009          Gopala Swamy J       Defect Id:10935; Added javascript code to uncheck all after rendering the page to browser
 *  0.6              Feb-15-2010           Nagarathna V.B       Defect Id:12122 commented mysearch in lsvCompany_ItemCommand();
 *  0.7              May-06-2010           Ganapati Bhat        Defect Id:12764; Added Code in PopulateSiteSettings(),ddlCountry_SelectedIndexChanged(), Page_Load()
    0.8              26/July/2016          pravin khot          added- ArrayList AList = Facade.GetAllJobPostingByClientId(id);
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Linq;
using TPS360.Common;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.Security;
namespace TPS360.Web.UI
{
    public partial class SearchCompany : CompanyBaseControl
    {
        #region Member Variables
        private bool _IsAccessToEmployee = true;
        private  string UrlForEmployee = string.Empty;
        private string URLForEditor = string.Empty;
        private  int IdForSitemap = 0;
        private int IdForEditor = 0;
        private static bool _mailsetting = false;
        private bool _IsAccessToCompanyEdit = false;
        private string currentSiteMapId = "0";


      
        #endregion

        #region Properties
        public string AccountType
        {
            get
            {

                switch (CurrentCompanyStatus)
                {
                    case CompanyStatus.Client:
                        return "Company";
                    case CompanyStatus.Department:
                        return "BU";
                    case CompanyStatus.Vendor:
                        return "Vendor";
                    default:
                        return "";
                }

            }

        }
        public ListView CompanyListView
        {
            get
            {
                return lsvCompany;
            }
        }

        public int? CurrentEmployeeId
        {
            get
            {
                return (int?)(ViewState[this.ClientID + "_CurrentEmployeeId"] ?? base.CurrentMember.Id);
            }
            set
            {
                ViewState[this.ClientID + "_CurrentEmployeeId"] = value;
            }
        }

        public int? CurrentCampaignId
        {
            get
            {
                return (int?)(ViewState[this.ClientID + "_CurrentCampaignId"] ?? null);
            }
            set
            {
                ViewState[this.ClientID + "_CurrentCampaignId"] = value;
            }
        }

        public SearchType SearchType
        {
            get
            {
                return (SearchType)(ViewState[this.ClientID + "_SearchType"] ?? SearchType.MySearch);
            }
            set
            {
                ViewState[this.ClientID + "_SearchType"] = value;
            }
        }

        public int SelectedCampaignId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_SelectedCampaignId"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_SelectedCampaignId"] = value;
            }
        }

        public bool ShowTaskRegion
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_ShowTaskRegion"] ?? true);
            }
            set
            {
                ViewState[this.ClientID + "_ShowTaskRegion"] = value;
            }
        }

        public bool ShowSearchRegion
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_ShowSearchRegion"] ?? true);
            }
            set
            {
                ViewState[this.ClientID + "_ShowSearchRegion"] = value;
            }
        }

        public bool ShowSearchByEmployee
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_ShowSearchByEmployee"] ?? true);
            }
            set
            {
                ViewState[this.ClientID + "_ShowSearchByEmployee"] = value;
            }
        }

        public SelectionType SelectionType
        {
            get
            {
                return (SelectionType)(ViewState[this.ClientID + "_SelectionMode"] ?? SelectionType.CheckBox);
            }
            set
            {
                ViewState[this.ClientID + "_SelectionMode"] = value;
            }
        }

        public CompanyStatus Status
        {
            get;
            set;
        }

        public bool DefaultBindList
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_DefaultBindList"] ?? true);
            }
            set
            {
                ViewState[this.ClientID + "_DefaultBindList"] = value;
            }
        }

        public bool ShowActionPanel
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_ShowActionPanel"] ?? true);
            }
            set
            {
                ViewState[this.ClientID + "_ShowActionPanel"] = value;
            }
        }

        public bool EnableSelectOption
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_EnableSelectOption"] ?? true);
            }
            set
            {
                ViewState[this.ClientID + "_EnableSelectOption"] = value;
            }
        }

        #endregion

        #region Methods
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvCompany.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", ( txtSortOrder.Text  == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        private void PopulateIndustryType()
        {
            MiscUtil.PopulateIndustryType(ddlIndustryType, Facade);
            ddlIndustryType.Items.RemoveAt(0);
            ddlIndustryType.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_ALL);
        }

        private void Clear()
        {
            this.txtName.Text = string.Empty;
            this.txtContactName.Text = string.Empty;
            uclCountryState.SelectedCountryId = 0;
            uclCountryState.SelectedStateId = 0;
            this.ddlIndustryType.SelectedIndex = 0;
            this.ddlEmployee.SelectedIndex = 0;
            this.txtCity.Text = string.Empty;
            dtCreateDate.ClearRange();
            dtUpdateDate.ClearRange();
            lsvCompany.Visible = false;
            BindList();
        }

        public void BindList()
        {
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "[C].[CompanyName]";
            }
            odsCompanyList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            lsvCompany.DataSourceID = "odsCompanyList";
            this.lsvCompany.DataBind();
            lsvCompany.Visible = true;
        }

        public void PrepareView()
        {
            PopulateIndustryType();
            MiscUtil.PopulateMemberListByRole(ddlEmployee, ContextConstants.ROLE_EMPLOYEE, Facade);
            if (ddlEmployee.Items.Count > 0) ddlEmployee.Items.RemoveAt(0);
            ddlEmployee.Items.Insert(0, UIConstants.DROP_DOWNL_ITEM_ALL);
            pnlHideSearchByEmployee.Visible = ShowSearchByEmployee;
            pnlSearchRegion.Visible = ShowSearchRegion;
        }

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            currentSiteMapId = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
            ddlEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            CustomSiteMap CustomMap = new CustomSiteMap();
            int targetsitemap = 0;
            if (currentSiteMapId == "11")
            {
                targetsitemap = 235;
                IdForEditor = 237;
            }
            else if (currentSiteMapId == "638")
            {
                targetsitemap = 642;
                IdForEditor = 644;
            }
            else
            {
                targetsitemap = 626;
                IdForEditor = 627;
            }
           
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(targetsitemap , CurrentMember.Id);
            if (CustomMap == null) _IsAccessToEmployee = false;
            else
            {
                IdForSitemap = CustomMap.Id;
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }
                       

            _mailsetting = MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id);
            ArrayList AList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            if (AList.Contains(237))
            {
                _IsAccessToCompanyEdit = true;   
            }
            if (!IsPostBack)
            {
                lblcompanyName.Text = AccountType + " Name";
                string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];
                if (!StringHelper.IsBlank(message))
                    MiscUtil.ShowMessage(lblMessage, message, false);
                PrepareView();
                BindList();
                txtSortColumn.Text = "btnCompanyName";
                txtSortOrder.Text = "ASC";
                PlaceUpDownArrow();
                
            }

            string pagesize = "";
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("MyCompanyList.aspx"))
            {
                pnlHideSearchByEmployee.Visible = false;
                pagesize = (Request.Cookies["MyCompanyListRowPerPage"] == null ? "" : Request.Cookies["MyCompanyListRowPerPage"].Value);
            }
            else
            {
                pnlHideSearchByEmployee.Visible = true;
                pagesize = (Request.Cookies["MasterCompanyListRowPerPage"] == null ? "" : Request.Cookies["MasterCompanyListRowPerPage"].Value); ;
            }
             ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCompany.FindControl("pagerControl");
             if (PagerControl != null)
             {
                 DataPager pager = (DataPager)PagerControl.FindControl("pager");
                 if (pager != null)
                 {
                     pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                 }
             }
          

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCompany.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            BindList();
            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void odsCompanyList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["employeeId"] = null;

            if (SearchType == SearchType.MasterSearch)
            {
                e.InputParameters["employeeId"] = ddlEmployee.SelectedItem.Value;
            }
            else if (SearchType == SearchType.MySearch)
            {
                e.InputParameters["employeeId"] = base.CurrentMember.Id.ToString();
            }
            else if (SearchType == SearchType.SearchByEmployee)
            {
                e.InputParameters["employeeId"] = CurrentEmployeeId.ToString();
            }
            else if (SearchType == SearchType.Other)
            {
                if (ShowSearchByEmployee)
                {
                    e.InputParameters["employeeId"] = ddlEmployee.SelectedItem.Value;
                }
            }
            e.InputParameters["companystatus"]= ((int)Status ).ToString ();
            if (SortOrder.Text  == string.Empty && SortColumn .Text  ==string .Empty )
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "CompanyName";
            }
            e.InputParameters["SortOrder"] = SortOrder.Text.ToString();
        }

        #endregion

        #region ListView Events

        protected void lsvCompany_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            HtmlTable header = ((HtmlTable)lsvCompany.FindControl("tlbTemplate"));
            if (!ShowActionPanel)
            {
                HtmlTableCell thAction = header.FindControl("thAction") as HtmlTableCell;
                thAction.Visible = false;
            }

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Company company = ((ListViewDataItem)e.Item).DataItem as Company;
                bool _IsAdmin = base.IsUserAdmin;
                if (company != null)
                {
                    HyperLink lnkCompanyName = (HyperLink)e.Item.FindControl("lnkCompanyName");
                    Label lblLocation = (Label)e.Item.FindControl("lblLocation");
                    Label lblPrimaryContact = (Label)e.Item.FindControl("lblPrimaryContact");
                    LinkButton lnkPrimaryEmail = (LinkButton)e.Item.FindControl("lnkPrimaryEmail");
                    Label lblOfficePhone = (Label)e.Item.FindControl("lblOfficePhone");
                    HtmlInputCheckBox chkSelect = (HtmlInputCheckBox)e.Item.FindControl("chkSelect");
                    Label lblSelected = (Label)e.Item.FindControl("lblSelected");
                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    string  location = string.Empty;

                    location = company.City.Trim();
                    if (company.StateId > 0)
                    {
                        State state = Facade.GetStateById(company.StateId);
                        if (location != string.Empty) location += ", ";
                        location += state.Name;

                    }
                    lblLocation.Text = location;
                    string officePhone = string.Empty;
                    if (!string.IsNullOrEmpty(company.OfficePhone))
                    {
                        officePhone += company.OfficePhone;

                        if (!string.IsNullOrEmpty(company.OfficePhoneExtension))
                        {
                            officePhone += " " + "x" + company.OfficePhoneExtension;
                        }
                    }

                    lblOfficePhone.Text = string.IsNullOrEmpty(officePhone) ? "" : officePhone;

                    if (_IsAccessToEmployee)
                    {
                        if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                            ControlHelper.SetHyperLink(lnkCompanyName, UrlForEmployee, string.Empty, company.CompanyName, UrlConstants.PARAM_COMPANY_ID, StringHelper.Convert(company.Id), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentSiteMapId);
                    }
                    else
                    {
                        lnkCompanyName.Text = company.CompanyName;
                        lnkCompanyName.Enabled = false;
                    }
                    lblPrimaryContact.Text = (!StringHelper.IsBlank(company.PrimaryContact.FirstName + " " + company.PrimaryContact.LastName)) ? (company.PrimaryContact.FirstName + " " + company.PrimaryContact.LastName) : "-";
                    if (!string.IsNullOrEmpty(company.PrimaryContact.Email))
                    {
                        if (!_mailsetting)
                            lnkPrimaryEmail.Text = "<a href=MailTo:" + company .PrimaryContact .Email  + ">" + company .PrimaryContact .Email  + "</a>";
                        else
                        {
                            lnkPrimaryEmail.Text = company.PrimaryContact.Email;
                            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_MEMBER_ID, company .PrimaryContact .MemberId .ToString (),UrlConstants .PARAM_COMPANY_ID ,company .Id .ToString (),UrlConstants .PARAM_EMAIL_TYPE ,"Company");
                            //lnkPrimaryEmail.Attributes.Add("onclick", "window.open('" + url + "','NewMail','height=626,width=770,toolbar=no,directories=no,status=no,linemenubar=no,scrollbars=no,resizable=no,modal=no'); return false;");
                            lnkPrimaryEmail.Attributes.Add("onclick", "btnMewMail_Click('" + url + "')"); 
                            
                        }
                    }
                  
                    btnDelete.CommandArgument = StringHelper.Convert(company.Id);

                  
                  
                    if (!ShowActionPanel)
                    {
                        HtmlTableCell tdAction = e.Item.FindControl("tdAction") as HtmlTableCell;
                        tdAction.Visible = false;
                    }

                    btnDelete.Visible = _IsAdmin;

                    btnDelete.OnClientClick = "return ConfirmDelete('"+AccountType +"')";

                    HtmlTableCell tdPager = lsvCompany.FindControl("tdPager") as HtmlTableCell;
                    
                    if (_IsAdmin  || _IsAccessToCompanyEdit)
                    {
                        SecureUrl UR = UrlHelper.BuildSecureUrl( UrlConstants .SFA .COMPANY_EDITOR   , string.Empty, UrlConstants.PARAM_COMPANY_ID, StringHelper.Convert(company.Id), UrlConstants.PARAM_SITEMAP_ID, IdForEditor .ToString (), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentSiteMapId);

                        //ControlHelper.SetHyperLink(lnkCompanyName, UrlForEmployee, string.Empty, company.CompanyName, UrlConstants.PARAM_COMPANY_ID, StringHelper.Convert(company.Id), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentSiteMapId);


                        btnEdit.OnClientClick = "window.location.replace('" + UR + "'); return false;";
                        tdPager.ColSpan = 6;
                    }
                    else
                    {
                        if (!_IsAdmin)
                        {
                            HtmlTableCell tdAction = e.Item.FindControl("tdAction") as HtmlTableCell;
                            HtmlTableCell thAction = header.FindControl("thAction") as HtmlTableCell;
                            tdAction.Visible = false;
                            thAction.Visible = false;
                            tdPager.ColSpan = 5;
                        }
                    }
                }
            }
        }
        protected void lsvCompany_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCompany.FindControl("pagerControl");
              if (PagerControl != null)
              {
                  DataPager pager = (DataPager)PagerControl.FindControl("pager");
                  if (pager != null)
                  {
                      DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                      if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                  }
                  HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                  if (hdnRowPerPageName != null)
                  {
                       if (Page.Request.Url.AbsoluteUri.ToString().Contains("MyCompanyList.aspx")) hdnRowPerPageName.Value = "MyCompanyListRowPerPage";
                      else hdnRowPerPageName.Value = "MasterCompanyListRowPerPage";
                  }
              }
              PlaceUpDownArrow();

            
                  LinkButton btnCompanyName = (LinkButton)lsvCompany.FindControl("btnCompanyName");
                  if (btnCompanyName != null) 
                  {
                      btnCompanyName.Text = lblcompanyName.Text;
                      btnCompanyName.ToolTip ="Sort By "+ lblcompanyName.Text;            
                   
                  }
                  
             
        }
        protected void lsvCompany_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
                    uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else
                        SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsCompanyList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);
            
            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {     
                        //***********Added by pravin khot on 26/July/2016*******
                        ArrayList AList = Facade.GetAllJobPostingByClientId(id);                        
                        if (AList.Count > 0 && AccountType == "BU")
                        {
                            MiscUtil.ShowMessage(lblMessage, "Can't Delete BU ,Already Mapped to Requisition.", true );
                        }
                            //******************END**********************
                        else 
                        {

                            IList<CompanyContact> con = new List<CompanyContact>();
                            con = Facade.GetAllCompanyContactByComapnyId(id);
                            if (con != null)
                            {
                                foreach (CompanyContact contact in con)
                                {
                                    if (contact.MemberId > 0)//Added by pravin khot on 26/July/2016*******
                                    {
                                        Facade.DeleteMemberById(contact.MemberId);
                                        Membership.DeleteUser(contact.Email.Trim());
                                    }
                                }
                            }
                            if (Facade.DeleteCompanyById(id))
                            {
                                MiscUtil.ShowMessage(lblMessage, AccountType + " deleted successfully.", false);
                                BindList();

                            }
                        }

                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
              
            }
        }
        #endregion

        #endregion



       
    }
}