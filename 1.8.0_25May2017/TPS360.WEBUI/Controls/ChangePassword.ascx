﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChangePassword.ascx.cs"
    Inherits="TPS360.Web.UI.ctlChangePassword" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:UpdatePanel ID="upChangePassword" runat="server">
    <ContentTemplate>
        <div style="padding: 15px;">
            <div class="TableRow" style="text-align: center;">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblNewPassword" runat="server" Text="New Password"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtNewPassword" MaxLength="15" runat="server" CssClass="CommonTextBox"
                        TextMode="Password"></asp:TextBox>
                    <span class="RequiredField">*</span>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="padding-left: 43%;">
                    <asp:RequiredFieldValidator ValidationGroup="changePassword" ID="rfvNewPassword"
                        runat="server" ControlToValidate="txtNewPassword" ErrorMessage="Please enter new password."
                        EnableViewState="False" Display="Dynamic">
                    </asp:RequiredFieldValidator>
                    <%--<asp:RegularExpressionValidator ValidationGroup="changePassword" ID="revNewPassword"
                        runat="server" ControlToValidate="txtNewPassword" Display="Dynamic" ErrorMessage="Please enter at least 8 characters."
                        ValidationExpression="^[\s\S]{8,}$"></asp:RegularExpressionValidator>--%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtNewPassword"
                    Display="Dynamic" ErrorMessage="Password must be greater than 8 characters long with at least one numeric,</br> one alphabet and one special character." EnableViewState="false"
                    ValidationGroup="RegInternal" ValidationExpression="(?=^.{8,30}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&*()_+}{:;'?/>.<,])(?!.*\s).*$"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblConfirmPassword" runat="server" Text="Confirm Password"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtConfirmPassword" MaxLength="15" runat="server" CssClass="CommonTextBox"
                        TextMode="Password"></asp:TextBox>
                    <span class="RequiredField">*</span>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="padding-left: 43%;">
                    <asp:RequiredFieldValidator ValidationGroup="changePassword" ID="rfvConfirmPassword"
                        runat="server" ControlToValidate="txtConfirmPassword" ErrorMessage="Please retype new password."
                        EnableViewState="False" Display="Dynamic">
                    </asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvNewPassword" ValidationGroup="changePassword" runat="server"
                        ControlToValidate="txtConfirmPassword" Type="String" Operator="Equal" ControlToCompare="txtNewPassword"
                        ErrorMessage="Passwords must match." EnableViewState="False" Display="Dynamic">
                    </asp:CompareValidator>
                </div>
            </div>
            <div class="TableRow" style="text-align: center;">
                <div class="TableFormLeble">
                    &nbsp;</div>
                <div class="TableFormContent">
                    <asp:Button ID="btnSubmit" ValidationGroup="changePassword" CssClass="CommonButton"
                        runat="server" Text="Save" OnClick="btnSubmit_Click" />
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

