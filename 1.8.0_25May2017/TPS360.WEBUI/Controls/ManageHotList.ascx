﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManageHotList.ascx.cs"
    Inherits="TPS360.Web.UI.ctlManageHotList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/HotListManagerList.ascx" TagName="Manager" TagPrefix="ucl" %>
<%@ Register Src ="~/Controls/CompanyRequisitionPicker.ascx" TagName ="CompReq" TagPrefix ="ucl" %>
<link href="../Style/TabStyle.css" type="text/css" rel="Stylesheet" />
<script type ="text/javascript" language ="javascript" src ="../js/CompanyRequisition.js"></script>
<script>
var n=10;
   function monitorClick(e) {
        try {
            var evt = (e) ? e : event;

            var theElem = (evt.srcElement) ? evt.srcElement : evt.target;

            while (theElem != null) { 
                if( $('#' + theElem .id).parents('.btn-group') ==null || theElem .id=='' || theElem .id.indexOf('upCanList')>=0){
                    n = 10;
                    var divReqs = document.getElementById('divReqs');
                    if (divReqs != null) {
                        $(divReqs).closest('.btn-group').removeClass('open');
                        divReqs.style.display = "none";
                    }
                    var divHotListdiv = document.getElementById('divHotListdiv');
                    if (divHotListdiv != null) {
                        $(divHotListdiv).closest('.btn-group').removeClass('open');
                        divHotListdiv.style.display = "none";
                    }
                    return true;
                }
                else {
                    n = 10;
                    return true;
                }

                theElem = theElem.offsetParent;
            }
        }
        catch (e) {
        } n = 10;
        return true;
    }
    document.onclick = monitorClick;
  function ShowReqDiv(divId) {
        $('.btn-group').removeClass('open');
        var a = document.getElementById("divReqs");
        var b = document.getElementById("divHotListdiv");
        if (a != null) a.style.display = "none";
        if (b != null) b.style.display = "none";

        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) {
            $(reqDiv).closest('.btn-group').addClass('open');
            reqDiv.style.display = "";
            reqDiv.parentElement.className = "btn-group open";
        } return false;
    }
    
    
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq); 
function PageLoadedReq(sender,args)
{
try
{
var chkBox=document .getElementById ('chkAllItem');
if(chkBox!=null)chkBox.disabled=false ;


}
catch (e)
{
}
}
 function IsNumeric(input) {
        return (input - 0) == input && input.length > 0;
    }
    
    function SelectRowCheckboxRejected(param1, param2) {
 
 
  
        // debugger;
        var ischeckall = true;
        var HeaderCheckbox = document.getElementById('chkAllItem');
        var elements = HeaderCheckbox.form.elements;

        for (var i = 0; i < elements.length; i++) {
            if ((elements[i].type == 'checkbox') && (elements[i].id != HeaderCheckbox.id)) {
                if (elements[i].checked == true) {
                    var hndId = elements[i + 1];
                   
                    if (IsNumeric(hndId.value)) {
                        ADDID(hndId.value, '<%= hdnSelectedIDS.ClientID %>');
                    }

                }
                else {
                    var hndId = elements[i + 1];
                    if (IsNumeric(hndId.value)) {
                        RemoveID(hndId.value, '<%= hdnSelectedIDS.ClientID %>');
                    }
                }
            }
        }
        HeaderCheckbox.checked = ischeckall;
    }
    
function CLCheckUnCheckAllRowLevelCheckboxes(cntrlHeaderCheckbox,posCheckbox)
{
  $("table tbody tr").each(function(){
            if( IsNumeric($(this).find(":hidden").val()))
            {
                $(this).find(":checkbox").attr('checked',cntrlHeaderCheckbox .checked);
               
                if(cntrlHeaderCheckbox .checked) ADDID( $(this).find(":hidden").val(),'<%= hdnSelectedIDS.ClientID %>');
                else             
                RemoveID($(this).find(":hidden").val(),'<%= hdnSelectedIDS.ClientID %>');
                
                
            }
        });
        
        
}
</script>


<div class="TableRow">
    <asp:Label ID="lblMessageMember" runat="server"></asp:Label>
</div>
<asp:UpdatePanel ID="updHotlist" runat="server">
    <ContentTemplate>
        <div class="TableRow" style="text-align: left">
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="TableRow">

            <div class="tabbable">
                <!-- Only required for left/right tabs -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab">Candidates in Hot List</a></li>
                    <li><a href="#tab2" data-toggle="tab">Hot List Managers</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                    <asp:UpdatePanel ID="upCanList" runat ="server" >
                    <ContentTemplate >
                    
                        <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
    <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />

            <asp:HiddenField ID="hdnSortColumn" runat="server" />
            <asp:HiddenField ID="hdnSortOrder" runat="server" />
                    
                        <asp:HiddenField ID="hdnSelectedIDS" runat="server" />
                        <div id="divBulkActions" class="btn-toolbar" runat="server">
                            <div class="pull-left">
                                <div class="btn-group">
                                    <a href="Javascript:void(0)" onclick="ShowReqDiv('divReqs')" id="refReq" class="btn btn-medium dropdown-toggle">
                                        Add to Requisition <span class="caret"></span></a>
                                    <ul class="dropdown-menu-custom" id="divReqs" style="display: none">
                                        <li style="width: 220px; text-align: left" id="list">
                                              <ucl:CompReq id="uclComReq" runat="server"></ucl:CompReq>
                                            <asp:Button ID="btnAddToPreSelectList" Text="Add" runat="server" OnClick="btnAddToPreSelectList_Click"
                                                ValidationGroup="Requisition" CssClass="btn btn-small" />
                      
                                        </li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <a href="Javascript:void(0)" onclick="ShowReqDiv('divHotListdiv')" class="btn dropdown-toggle"
                                        id="refHot")">Add to Hot List <span class="caret"></span></a>
                                    <ul class="dropdown-menu-custom" id="divHotListdiv" style="display: none">
                                        <li style="width: 220px; text-align: left" id="list">
                                            <asp:DropDownList ID="ddlSelectHotList" runat="server" CssClass="chzn-select"  Width ="150px"
                                                ValidationGroup="AddtoHotlist">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="TxtHotList" runat="server" Visible="true"></asp:TextBox>
                                            <div id="divHot">
                                            </div>
                                            <ajaxToolkit:AutoCompleteExtender ID="hot_ListAutoComplete" runat="server" ServicePath="~/AjaxExtender.asmx"
                                                TargetControlID="TxtHotList" MinimumPrefixLength="1" ServiceMethod="GetHotList"
                                                EnableCaching="true" CompletionListElementID="divHot" CompletionListCssClass="AutoCompleteBox"
                                                CompletionInterval="0" FirstRowSelected="True" />
                                            <asp:Button ID="btnAddToHotList" Text="Add" runat="server" CssClass="btn btn-small"
                                                OnClick="btnAddToHotList_Click" ValidationGroup="AddtoHotlist" style=" margin-bottom : 15px " />
                                            <asp:CompareValidator ID="cvLevelOfEducation" runat="server" ControlToValidate="ddlSelectHotList"
                                                Type="Integer" Operator="NotEqual" ValueToCompare="0" ErrorMessage="Please select hot list."
                                                EnableViewState="False" Display="Dynamic" ValidationGroup="AddtoHotlist"></asp:CompareValidator>
                                        </li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Email Candidates" OnClick="btnEmail_Click"
                                        CssClass="btn btn-medium" />
                                </div>
                                <div class="btn-group" runat="server" id="liRemove">
                                    <asp:LinkButton ID="btnRemove" runat="server" Text="Remove" OnClick="btnRemove_Click"
                                        CssClass="btn btn-danger" OnClientClick="return ConfirmAction('Are you sure that you want to remove candidate(s) from Hot List?')" />
                                </div>
                            </div>
                        </div>
                        <div class="TableRow" style="text-align: left;">
                            <asp:Label ID="lblMessageCandidate" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <div class="GridContainer">
                            <asp:ObjectDataSource ID="odsCandidateList" runat="server" OnInit="odsCandidateList_OnInit"
                                SelectCountMethod="GetListCount" EnablePaging="true" SelectMethod="GetPagedByMemberGroupId"
                                TypeName="TPS360.Web.UI.MemberDataSource" SortParameterName="sortExpression"
                                OnSelected="odsCandidateList_Selected"></asp:ObjectDataSource>
                            <asp:ListView ID="lsvlistOfCandidates" runat="server" DataSourceID="odsCandidateList"
                                OnItemDataBound="lsvlistOfCandidates_ItemDataBound" DataKeyNames="Id" OnPreRender="lsvlistOfCandidates_PreRender"
                                OnItemCommand="lsvlistOfCandidates_ItemCommand">
                                <LayoutTemplate>
                                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                        <tr>
                                            <th style="width: 20px !important;">
                                                <input id="chkAllItem" name="chkAllItem" type="checkbox" onclick="CLCheckUnCheckAllRowLevelCheckboxes(this,0)"
                                                    enableviewstate="true" disabled="disabled" />
                                            </th>
                                            <th>
                                                <asp:LinkButton ID="btnlsvName" runat="server" ToolTip="Name" CommandName="Sort"
                                                    CommandArgument="FirstName" Text="Name" />
                                            </th>
                                            <th>
                                                <asp:LinkButton ID="btnlsvPosition" runat="server" CommandName="Sort" CommandArgument="CurrentPosition"
                                                    Text="Position" />
                                            </th>
                                         
                                             <th>
                                                <asp:LinkButton ID="btnlsvCity" runat="server" CommandName="Sort" CommandArgument="PermanentCity"
                                                    Text="Location" />
                                            </th>
                                            <th>
                                                <asp:LinkButton ID="btnlsvMobile" runat="server" CommandName="Sort" CommandArgument="CellPhone"
                                                    Text="Mobile" />
                                            </th>
                                           
                                            <th>
                                                <asp:LinkButton ID="btnlsvEmail" runat="server" CommandName="Sort" CommandArgument="PrimaryEmail"
                                                    Text="Email" />
                                            </th>
                                            <th style="width: 45px !important;">
                                               Action
                                            </th>
                                        </tr>
                                        <tr id="itemPlaceholder" runat="server">
                                        </tr>
                                        <tr class="Pager">
                                            <td colspan="7">
                                                <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                            </td>
                                        </tr>
                                    </table>
                                </LayoutTemplate>
                                <EmptyDataTemplate>
                                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                        <tr>
                                            <td>
                                                No candidates in Hot List.
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                                <ItemTemplate>
                                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                        <td style="text-align: left;">
                                            <asp:CheckBox ID="chkBox" runat="server" onclick="SelectRowCheckboxRejected();" />
                                            <asp:HiddenField ID="hdnID" runat="server" />
                                        </td>
                                        <td style="text-align: left;">
                                            <asp:HyperLink ID="lnkCandidateName"  runat="server"></asp:HyperLink>
                                        </td>
                                        <td style="text-align: left;">
                                            <asp:Label ID="lblPosition" runat="server" />
                                        </td>
                                       <td style="text-align: left;">
                                            <asp:Label ID="lblCity" runat="server" />
                                        </td>
                                        <td style="text-align: left;">
                                            <asp:Label ID="lblMobile" runat="server" />
                                        </td>
                                       
                                        <td style="text-align: left;">
                                            <asp:LinkButton ID="lblEmail" runat="server" />
                                        </td>
                                        <td style =" text-align : center ">
                                             <asp:ImageButton ID="btnDelete" Visible="true" SkinID="sknDeleteButton" runat="server"
                                    CommandName="DeleteItem"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                        
                    </div>
                    <div class="tab-pane" id="tab2" style="width: 100%; overflow: auto;">
                     <ucl:Manager ID="uclManager" runat ="server" />
                    </div>
                </div>
            </div>
    

</div>
