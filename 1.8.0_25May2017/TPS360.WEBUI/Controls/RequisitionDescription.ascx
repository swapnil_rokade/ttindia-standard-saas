<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionDescription.ascx.cs" 
    Inherits="TPS360.Web.UI.cltRequisitionDescription" %>
<%@ Register Src ="~/Controls/HtmlEditor.ascx" TagName ="HtmlEditor" TagPrefix ="ucl" %>
<script>
function validateDescription(sender,args)
{
   if($('#ctl00_cphHomeMasterTitle_reqDescription_WHEJobDescription_txtEditor').val()=='')
   {
   args .IsValid=false ;
   }
}
    </script>
<div>
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
        <div class ="TableRow">
        <div class ="TableFormLeble">Extract JD from Document:</div>
            <div class ="TableFormContent" > 
                <asp:UpdatePanel ID="upDescriptionupload" runat ="server" >
                <ContentTemplate >
                    <asp:FileUpload ID="fuDescription" runat ="server" CssClass ="CommonButton" TabIndex ="28" />  
                    <asp:Button ID ="btnExport" Text ="Upload & Extract" runat ="server" CssClass ="CommonButton" OnClick ="btnExport_Click" ValidationGroup ="UploadDocumentDescriotion" TabIndex ="29" />
                </ContentTemplate>
                <Triggers >
                    <asp:PostBackTrigger ControlID ="btnExport" />
                </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
  <div class="TableRow">
  <div class ="TableFormValidatorContent" style =" margin-left : 42%">
   <asp:RequiredFieldValidator ID="rfvDocumentUpload" runat="server" ErrorMessage="Please select a file to upload."
                            ControlToValidate="fuDescription" ValidationGroup="UploadDocumentDescriotion" Display ="Dynamic" ></asp:RequiredFieldValidator>
                            
                        <asp:RegularExpressionValidator Display ="Dynamic"  ID="revDocument" runat="server" ErrorMessage="Please UploadDOCs/DOCXs,RTFs,TXTs and PDFs only."
                            ValidationExpression="(.*\.[dD][oO][cC][xX])|(.*\.[dD][oO][cC])|(.*\.[rR][tT][fF])|(.*\.[tT][xX][tT])|(.*\.[pP][dD][fF])"
                            ControlToValidate="fuDescription" ValidationGroup="UploadDocumentDescriotion"></asp:RegularExpressionValidator>
                    </div>
                    </div>
        <div  class ="TableRow" align="left"    style =" width:98.4%" >Job Description:</div>
        <div class="TableFormContent"  >
            <ucl:HtmlEditor ID="WHEJobDescription" runat ="server" EnableViewState ="true"  /><div style ="  float :right ;" ><span class="RequiredField">*</span></div>
        </div>
       <asp:CustomValidator ID="cvDescription" runat ="server" ErrorMessage ="Please enter description" ValidationGroup ="ValGrpJobTitle" ClientValidationFunction ="validateDescription" EnableClientScript ="true"  ></asp:CustomValidator>
        
        <div  class="TableFormContent" id= "divClientBrief" runat ="server"  >
         <div class ="CommonHeader"  style =" width:98.4%">Client Brief</div>
         <asp:TextBox ID ="txtClientBrief" runat ="server" TextMode ="MultiLine" CssClass ="CommonTextBox" Width ="98.4%" Height ="200px" ></asp:TextBox>
        </div> 
    
             <%--tabindex=30   --%>     
</div>
