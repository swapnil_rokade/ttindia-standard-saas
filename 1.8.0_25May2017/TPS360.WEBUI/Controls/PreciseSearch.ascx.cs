﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:PreciseSearch.ascx.cs
    Description: This is the user control page used to disply the list of candidate in grid
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
 *    0.1             28/April/2016        pravin khot           Introduced by use for setting- AllowCandidatetobeaddedonMultipleRequisition
 *    0.2             23/June/2016        pravin khot           added new Source,SourceDescription

-------------------------------------------------------------------------------------------------------------------------------------------       

>*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using AjaxControlToolkit;
using TPS360.BusinessFacade;
using TPS360.Common;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Globalization;
using System.Threading;
using System.Net.Mail;
using System.Net;

namespace TPS360.Web.UI
{
    public partial class ControlsPreciseSearch : BaseControl
    {
        #region Member Variables

        private bool _IsMemberMailAccountAvailable = false;
        private bool _isMyList = false;
        private bool _getCandidateCount = false;
        Stopwatch _watch;
        public int RecordCount = 0;
        public static string strSearchKeyword;
        private bool _IsAccessToCandidate = true;
        private static string UrlForCandidate = string.Empty;
        private static int IdForSitemap = 0;

        #endregion

        #region Properties

        public bool IsMyList
        {
            get { return _isMyList; }
            set { _isMyList = value; }
        }

        private int JobPostingID
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID].ToString()))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]);
                }
                else
                {
                    return 0;
                }
            }
        }
        #endregion

        #region Methods

        private void PlaceUpDownArrow()
        {
            try
            {

                LinkButton lnk = (LinkButton)lsvSearchResult.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                if (lnk.CommandArgument.Contains("Experience"))
                {
                    im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Descending" : "Ascending"));
                }
                else
                    im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));


            }
            catch
            {
            }

        }
        private void LoadHotList()
        {
            int memberGroupCount = Facade.GetMemberGroupCountByGroupType((int)MemberGroupType.Candidate);
            if (memberGroupCount < 500)
            {
                txtSearch_HotList.Visible = false;
                ddlHotList.Visible = true;
                ddlHotList.Items.Clear();
                ddlHotList.DataSource = Facade.GetAllMemberGroup((int)MemberGroupType.Candidate); //0.7
                ddlHotList.DataTextField = "Name";
                ddlHotList.DataValueField = "Id";
                ddlHotList.DataBind();
                ddlHotList =(DropDownList ) MiscUtil.RemoveScriptForDropDown(ddlHotList);
                MiscUtil.InsertPleaseSelect(ddlHotList);
            }
            else
            {
                ddlHotList.Visible = false;
                txtSearch_HotList.Visible = true;

            }
        }
        private bool CheckSelectedList(int ID)
        {
            char[] del = { ',' };
            string[] arrID = txtSelectedIds.Text.Split(del, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in arrID)
            {
                if (Convert.ToInt32(s) == ID)
                {
                    return true;
                }
            }
            return false;
        }
        private void LoadRequisionList()
        {
            IList<HiringMatrixLevels> level = Facade.GetAllHiringMatrixLevels();
            //chkHiringStatus.DataSource = level;
            //chkHiringStatus.DataTextField = "Name";
            //chkHiringStatus.DataValueField = "Id";


            uclHiringLevel.DataSource = level;
            uclHiringLevel.DataTextField = "Name";
            uclHiringLevel.DataValueField = "Id";
            uclHiringLevel.DataBind();
        }
        private void LoadApplicantType()
        {
            //ddlApplicantType.Items.Clear();
            //MiscUtil.PopulateCandidateType(ddlApplicantType, Facade);
        }
        private void BuildPaymentTypeDropDownList()
        {
            ddlPaymentType.Items.Clear();
            ddlPaymentType.Items.Add(new ListItem("Yearly", "4"));
            ddlPaymentType.Items.Add(new ListItem("Monthly", "3"));
            ddlPaymentType.Items.Add(new ListItem("Daily", "2"));
            ddlPaymentType.Items.Add(new ListItem("Hourly", "1"));

        }
        private void BuildResumeLastUpdateDropDownList()
        {
            ddlResumeLastUpdate.Items.Clear();

            ddlResumeLastUpdate.Items.Add(new ListItem("Does not matter", ""));
            ddlResumeLastUpdate.Items.Add(new ListItem("1 Days", "1"));
            ddlResumeLastUpdate.Items.Add(new ListItem("2 Days", "2"));
            ddlResumeLastUpdate.Items.Add(new ListItem("7 Days", "7"));
            ddlResumeLastUpdate.Items.Add(new ListItem("15 Days", "15"));
            ddlResumeLastUpdate.Items.Add(new ListItem("30 Days", "30"));
            ddlResumeLastUpdate.Items.Add(new ListItem("60 Days", "60"));
            ddlResumeLastUpdate.Items.Add(new ListItem("90 Days", "90"));
            ddlResumeLastUpdate.Items.Add(new ListItem("6 Months", "182"));
            ddlResumeLastUpdate.Items.Add(new ListItem("1 Year", "365"));
        }
        private void BuildPerPageDropDownList()
        {
           /* ddlPerPage.Items.Clear();

            ddlPerPage.Items.Add(new ListItem("10", "10"));
            ddlPerPage.Items.Add(new ListItem("20", "20"));
            ddlPerPage.Items.Add(new ListItem("50", "50"));
            ddlPerPage.Items.Add(new ListItem("75", "75"));
            ddlPerPage.Items.Add(new ListItem("100", "100"));
            ddlPerPage.SelectedIndex = 1;*/
        }

        private void BuildExpandCollapseDetailsDropDownList()
        {
           /* ddlExpandColapseDetails.Items.Clear();

            ddlExpandColapseDetails.Items.Add(new ListItem("Collapse", "Collapse"));
            ddlExpandColapseDetails.Items.Add(new ListItem("Expand", "Expand"));*/
        }

        private void BuildSecurityClearanceRadioButton()
        {
            optSecurityClearance.Items.Clear();
            optSecurityClearance.Items.Add(new ListItem("Yes", "1"));
            optSecurityClearance.Items.Add(new ListItem("No", "0"));
            optSecurityClearance.SelectedIndex = 1;
        }

        private void PrepareView()
        {

            ddlSourceId.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.SourceType);
            ddlSourceId.DataValueField = "Id";
            ddlSourceId.DataTextField = "Name";
            ddlSourceId.DataBind();
            ddlSourceId.Items.Insert(0, new ListItem("Any", "0"));


            MiscUtil.PopulateWorkAuthorization(uclWorkStatus.ListItem , Facade);
            MiscUtil.PopulateCurrency(ddlCurrency, Facade);
            MiscUtil.PopulateEmploymentDuration(ddlJobType, Facade);
            MiscUtil.PopulateEducationQualification(uclEducation.ListItem , Facade);
            uclEducation.ListItem.Items.RemoveAt(0);
           // chkEducationList.Items.RemoveAt(0);
            MiscUtil.PopulateWorkSchedule(ddlWorkSchedule, Facade);
            MiscUtil.PopulateInternalRating(ddlinternalratingID, Facade);
            ddlinternalratingID.Items[0].Text = "Any";

            MiscUtil.PopulateGenderType(ddlGender, Facade);//Gender
            ddlGender.Items[0].Text = "Any";

            MiscUtil.PopulateIndustry(ddlIndustry, Facade);
            ddlIndustry.Items[0].Text = "Any";

            MiscUtil.PopulateJobFunction(ddlJobFunction, Facade);
            ddlJobFunction.Items[0].Text = "Any";

            MiscUtil.PopulateCountry(ddlCountryOfBirth, Facade);

            


            ControlHelper .SelectListByValue (ddlCurrency ,(SiteSetting[DefaultSiteSetting.Currency .ToString ()] == null ? "0" : SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString ()));
            MiscUtil.PopulateMemberListByRole(ddlAssignedManager , ContextConstants.ROLE_EMPLOYEE, Facade);
            if (ddlCurrency.SelectedItem.Text == "INR")
            {
                lblSalaryRate.Text = "(Lacs)";
                lblSalaryRate.Visible = true;
            }
            else
            {
                lblSalaryRate.Text = "";
            }
            ArrayList membergroup = Facade.GetAllMemberGroupNameByManagerId(base.CurrentMember.Id, (int)MemberGroupType.Candidate);
            if (membergroup.Count < 500)
            {
                pnlhotListCompletionItem.Visible = false ;
                TxtHotList.Visible = false;
                ddlJobSeekerGroup.Visible = true;
                ddlJobSeekerGroup.DataSource = membergroup;
                ddlJobSeekerGroup.DataTextField = "Name";
                ddlJobSeekerGroup.DataValueField = "Id";
                ddlJobSeekerGroup.DataBind();
                ListItem item = new ListItem();
                item .Text =UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT_HOTLIST;
                item .Value ="0";
                ddlJobSeekerGroup.Items.Insert(0, item);
            }
            else
            {
                ddlJobSeekerGroup.Visible = false;
                TxtHotList.Visible = true;
                pnlhotListCompletionItem.Visible = true;

            }
            BuildPaymentTypeDropDownList();
            BuildResumeLastUpdateDropDownList();
            //BuildPerPageDropDownList();
            BuildExpandCollapseDetailsDropDownList();
            BuildSecurityClearanceRadioButton();
            uclCountryState.SelectedCountryId = 0;
        }

    
        public bool AddToHotList(int hotlistid, int memberid)
        {
            MemberGroupMap memberGroupMap = Facade.GetMemberGroupMapByMemberIdandMemberGroupId(memberid, hotlistid);
            if (memberGroupMap == null)
            {
                MemberGroupMap newMemberGroupMap = new MemberGroupMap();
                newMemberGroupMap.CreateDate = DateTime.Now;
                newMemberGroupMap.CreatorId = base.CurrentMember.Id;
                newMemberGroupMap.UpdateDate = DateTime.Now;
                newMemberGroupMap.UpdatorId = base.CurrentMember.Id;
                newMemberGroupMap.MemberGroupId = hotlistid;
                newMemberGroupMap.MemberId = memberid;
                Facade.AddMemberGroupMap(newMemberGroupMap);
                return true;
            }
            else if (memberGroupMap.IsRemoved)
            {
                memberGroupMap.IsRemoved = false;
                memberGroupMap.UpdateDate = DateTime.Now;
                memberGroupMap.UpdatorId = base.CurrentMember.Id;
                Facade.UpdateMemberGroupMap(memberGroupMap);
                return true;
            }
            return false;
        }

        private string GetSelectedCandidates()
        {
            string selectedCandidates = string.Empty;
            for (int cnt = 0; cnt < lsvSearchResult.Items.Count; cnt++)
            {
                CheckBox chkCandidate = (CheckBox)lsvSearchResult.Items[cnt].FindControl("chkCandidate");
                HiddenField hfCandidateId = (HiddenField)lsvSearchResult.Items[cnt].FindControl("hfCandidateId");
                if (chkCandidate != null && chkCandidate.Checked)
                {
                    selectedCandidates += "," + hfCandidateId.Value.Trim();
                }
            }

            return (selectedCandidates == string.Empty) ? (string.Empty) : (selectedCandidates.Substring(1));
        }


        private void ShowResultSummary()
        {
            if (!string.IsNullOrEmpty(hfTotalResult.Value))
            {
                DataPager pager = this.lsvSearchResult.FindControl("pager") as DataPager;

                if (pager != null)
                {
                    int rows = (Request.Cookies["PreciseSearchRowPerPage"] != null ? (Convert.ToInt32(Request.Cookies["PreciseSearchRowPerPage"].Value == "" ? Convert.ToString(20) : Request.Cookies["PreciseSearchRowPerPage"].Value)) : Convert.ToInt32(20));

                    pager.PageSize = rows;
                    DropDownList ddlRowsPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    ControlHelper.SelectListByValue(ddlRowsPerPage, pager.PageSize.ToString());
                    int startRowIndex = int.Parse(hfStartRowIndex.Value);
                    int maxRow = int.Parse(hfMaximumRowIndex.Value);
                    double totalResult = double.Parse(hfTotalResult.Value);
                    double page = totalResult > 0 ? Math.Ceiling(((double)(startRowIndex + maxRow) / maxRow)) : 0;
                    if (page.ToString() != "NaN")
                        page = page - 1;
                    double from = page > 0 ? page * pager.PageSize + 1 : 1;
                    double to = from + pager.PageSize - 1;
                    if (to > totalResult)
                        to = totalResult;

                    double elapsed = double.Parse(_watch.Elapsed.Ticks.ToString()) / (1000 * TimeSpan.TicksPerMillisecond);
                }
                else
                {
                    divShowSearchStatus.Visible = false;
                }

            }
        }
     
        private string ReplaceUnWantedCharacters(string srtInput)
        {

            if (!string.IsNullOrEmpty(srtInput))
            {

                srtInput = srtInput.Replace(",", " and ");
                srtInput = srtInput.Replace("(", " ");
                srtInput = srtInput.Replace(")", " ");
                while ((srtInput.ToUpper().Trim().StartsWith("AND ")) || (srtInput.ToUpper().Trim().StartsWith("OR ")))
                {
                    if (srtInput.ToUpper().Trim().StartsWith("AND "))
                    {
                        srtInput = srtInput.Trim().Substring(4); 
                    }
                    if (srtInput.ToUpper().Trim().StartsWith("OR "))
                    {
                        srtInput = srtInput.Trim().Substring(3);  
                    }

                }
                srtInput = srtInput.Trim();
                while ((srtInput.ToUpper().Trim().EndsWith(" AND")) || (srtInput.ToUpper().Trim().EndsWith(" OR")))
                {
                    if (srtInput.ToUpper().Trim().EndsWith(" OR"))
                    {
                        srtInput = srtInput.Substring(0, srtInput.Trim().Length - 2);
                    }
                    if (srtInput.ToUpper().Trim().EndsWith(" AND"))
                    {
                        srtInput = srtInput.Substring(0, srtInput.Trim().Length - 3);
                    }
                }
                if (srtInput.Trim().ToUpper() == "AND" || srtInput.Trim().ToUpper() == "OR")
                {
                    srtInput = string.Empty;
                }
            }
            return srtInput;

        }

        private string HighlightString(string keyword, string oritxt)
        {
            string retText = string.Empty;
            if (keyword.Contains("*"))
            {
                string[] list = oritxt.Split(new string[] { " ", "/" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string li in list)
                {
                    if (li.ToLower ().StartsWith(keyword.ToLower ().Replace ("*","").Trim ()))
                    {
                        Regex exx = new Regex(li, RegexOptions.IgnoreCase);
                        retText = exx.Replace(oritxt, new MatchEvaluator(ReplaceKeyWord));
                    }
                }
            }
            else
            {
                Regex exx = new Regex(keyword.Trim().Replace(" ", "|"), RegexOptions.IgnoreCase);
                retText = exx.Replace(oritxt, new MatchEvaluator(ReplaceKeyWord));
            }
            return retText;
        }
        public string ReplaceKeyWord(Match m)
        {
            if (m.Value.ToLower() != "not" && m.Value.ToLower() != "and" && m.Value.ToLower() != "or" && m.Value != "&" && m.Value != "," && m.Value != "!" && m.Value !="")
                return "<span class=highlight>" + m.Value + "</span>";
            else
                return m.Value;
        }

        public string HighlightProcess(string keywords, string lblproperty)
        {
            string ReturnText = string.Empty;
            string[] list = keywords.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            bool el = false;
            if (keywords.StartsWith("\"") && keywords.EndsWith("\""))
            {
                if (lblproperty.ToLower().Contains(keywords.ToLower().Replace("\"", " ").Trim()))
                {
                    ReturnText = HighlightString(keywords.Replace("\"", " "), lblproperty);
                }
                else
                    ReturnText = lblproperty;
            }
            else if (keywords.Contains(","))
            {
                ReturnText = HighlightString(keywords.Replace(",", " "), lblproperty);
            }
            else
            {
                if (list.Contains("not") || list.Contains("Not") || list.Contains("-") || list.Contains("NOT"))
                {
                    string[] split = keywords.Split(new string[] { "not", "NOT", "Not", "-" }, StringSplitOptions.RemoveEmptyEntries);
                    if (split.Length > 0)
                    {
                        if (lblproperty.ToLower().Contains(split[0].Trim ()))// txtJobTitle.Text.ToLower().Trim() == candidate.CurrentPosition.ToLower ().Trim ())
                        {
                            ReturnText = HighlightString(split[0].Trim (), lblproperty);// "<span class=highlight>" + candidate.CurrentPosition + "</span>";
                            el = true;
                        }
                    }
                }
                else
                {
                    foreach (string li in list)
                    {


                        if (lblproperty.ToLower().Contains(li.ToLower().Replace("*", "")))// txtJobTitle.Text.ToLower().Trim() == candidate.CurrentPosition.ToLower ().Trim ())
                        {
                            ReturnText = HighlightString(keywords, lblproperty);// "<span class=highlight>" + candidate.CurrentPosition + "</span>";
                            el = true;
                        }
                        else if (lblproperty.ToLower().Contains(li.ToLower().Replace("'", "")))
                        {
                            ReturnText = HighlightString(keywords.Replace("'", ""), lblproperty);
                            el = true;
                        }
                    }
                }
                if (!el)
                    ReturnText = lblproperty;
            }
            return ReturnText;
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            uclComReq.ISMyList = true;
            liRemove.Visible = base.IsUserAdmin;
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            ddlCurrency.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('" + ddlCurrency.ClientID + "','" + lblSalaryRate .ClientID + "')");
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToCandidate = false;
            else
            {
                IdForSitemap = CustomMap.Id;
                UrlForCandidate = "~/" + CustomMap.Url.ToString();
            }

           AutoCompleteExtender1.ContextKey = base.CurrentMember.Id.ToString();
            autoSeachHotList.ContextKey = "0";
            _IsMemberMailAccountAvailable = MiscUtil.IsValidMailSetting(Facade, base.CurrentMember.Id);
            if (!IsPostBack)
            {
                if (SiteSetting[DefaultSiteSetting.Country.ToString()] != null)
                {
                    Country country = Facade.GetCountryById(Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()]));
                    if (country != null)
                    {
                        hdnSiteSettingCountry.Value = country.Name;
                        if (country.Name == "India")
                        {
                            txtZipCode.ToolTip = "PIN Code";
                            revZIPCode.ErrorMessage = "Please enter a valid PIN code";
                            foreach (ListItem item in ddlRadius.Items)
                            {
                                item.Text = item.Text.Replace("miles", "km");
                            }
                        }
                    }
                }

                if (Request.Cookies["PreciseSearchNameFormat"] != null) 
                {
                    ControlHelper.SelectListByValue(ddlNameFormat, Request.Cookies["PreciseSearchNameFormat"].Value);
                    LinkButton lnkHeaderName = (LinkButton)lsvSearchResult.FindControl("lnkHeaderName");

                    if (lnkHeaderName != null) {
                        if (ddlNameFormat.SelectedValue == "1")
                        {

                           // SortColumn.Text = 
                                lnkHeaderName.CommandArgument = "[C].[LastName]";
                            SortOrder.Text = "ASC";
                            odsCandidateSearch.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                            odsCandidateSearch.SelectParameters["SortColumn"].DefaultValue = "[C].[LastName]";
                        }
                        else {

                            //SortColumn.Text = 
                                lnkHeaderName.CommandArgument = "[C].[CandidateName]";
                            SortOrder.Text = "ASC";
                            odsCandidateSearch.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                            odsCandidateSearch.SelectParameters["SortColumn"].DefaultValue = "[C].[CandidateName]";
                        
                        }
                    
                    }
                
                }


                txtSearch_HotList.Visible = true;
                PrepareView();
                LoadRequisionList();
                LoadHotList();
                Session["PreciseSearchBaseModule"] = "ATS";
                lsvSearchResult.Visible = false; 
                //custom
                LoadApplicantType();
                if (JobPostingID > 0)
                {
                    JobPosting job = Facade.GetJobPostingById(JobPostingID);
                    string Skills = MiscUtil.SplitSkillValues(job.JobSkillLookUpId, '!', Facade);
                    txtAllKeywords.Text = "'" + job.JobTitle + "'" + (Skills != string.Empty ? "," + Skills : string.Empty);
                    txtWorkCity.Text = job.City;
                    uclCountryState.SelectedCountryId = job.CountryId;
                    uclCountryState.SelectedStateId = job.StateId;
                    txtMinExp.Text = job.MinExpRequired != string.Empty ? job.MinExpRequired : string.Empty;
                    txtMaxExp.Text = job.MaxExpRequired != string.Empty ? job.MaxExpRequired : string.Empty;
                    optSecurityClearance.SelectedIndex = job.SecurityClearance ? 0 : 1;
                    if (job.RequiredDegreeLookupId != string.Empty)
                    {
                        uclEducation.SelectedItems = job.RequiredDegreeLookupId;
                        //IList<string> EducationId = new List<string>();
                        //EducationId = job.RequiredDegreeLookupId.Split(',');
                        //foreach (string id in EducationId)
                        //{
                        //    if (id != string.Empty)
                        //    {
                        //        foreach (ListItem item in chkEducationList.Items)
                        //        {
                        //            if (item.Value == id)
                        //                item.Selected = true;
                        //        }
                        //    }
                        //}
                    }
                    btnSearch_Click(sender, e);
                }
            }

            Session["RequisitionSkillSet"] = null; 
        }

        private string getOrKeyword(string keyword)
        {
            char[] delimiters = new char[] { ',', '\n' };
            string[] key = keyword.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            string result = string.Empty;
            if (keyword.StartsWith(",")) result = "|";
            string lastword = string.Empty;
            for (int i = 0; i < key.Length; i++)
            {
                if (key[i].ToLower() != "or")
                {
                    if (key[i].ToLower() == ("not") || key[i].Contains("-"))
                    {
                        string s = string.Empty; s = getNotKeyword(key[i]);
                        if (s.StartsWith("&!"))
                        {
                            result = (result.EndsWith("&")) ? result.Substring(0, result.Length - 1) : result;
                            result = (result.EndsWith("|")) ? result.Substring(0, result.Length - 1) : result;
                            result = (result.EndsWith("&!")) ? result.Substring(0, result.Length - 2) : result;
                            result += s;
                        }
                        else result += s;
                    }
                    else result += key[i];
                    if (!result.EndsWith("&") && !result.EndsWith("|") && !result.EndsWith("&!") && result != string.Empty)
                        result += "|";
                }
            }
            return result == string.Empty ? "|" : result;
        }
        private string getNotKeyword(string keyword)
        {
            char[] delimiters = new char[] { '-', '\n' };
            string[] key = keyword.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            string result = string.Empty;
            if (keyword.StartsWith("&!")) result = "&!";
            string lastword = string.Empty;
            for (int i = 0; i < key.Length; i++)
            {
                if (key[i].ToLower() != "not")
                {
                    result += key[i];
                    if (!result.EndsWith("&") && !result.EndsWith("|") && !result.EndsWith("&!"))
                        result += "&!";
                }
            }
            return result == string.Empty ? "&!" : result;
        }
        private string BuildKeyWords(string keyword)
        {
            char[] delimiters = new char[] { ' ', '\n' };
            string[] key = keyword.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            string result = string.Empty;
            string lastword = string.Empty;
            for (int i = 0; i < key.Length; i++)
            {
                if (key[i].EndsWith("*")) key[i] = "\"" + key[i] + "\"";
                if (key[i].ToLower() != "and")
                {
                    if (key[i].ToLower() == ("not") || key[i].Contains("-") || key[i].ToLower() == ("or") || key[i].Contains(","))
                    {
                        string s = string.Empty; s = getOrKeyword(key[i]);
                        if (s.StartsWith("|") || s.StartsWith("&!"))
                        {
                            result = (result.EndsWith("&")) ? result.Substring(0, result.Length - 1) : result;
                            result = (result.EndsWith("|")) ? result.Substring(0, result.Length - 1) : result;
                            result = (result.EndsWith("&!")) ? result.Substring(0, result.Length - 2) : result;
                            result += s;
                        }
                        else result += s;
                    }
                    else result += key[i];
                    if (!result.EndsWith("&") && !result.EndsWith("|") && !result.EndsWith("&!"))
                        result += "&";
                }


            }
            result = (result.EndsWith("&")) ? result.Substring(0, result.Length - 1) : result;
            result = (result.EndsWith("|")) ? result.Substring(0, result.Length - 1) : result;
            result = (result.EndsWith("&!")) ? result.Substring(0, result.Length - 2) : result;
            result = (result.StartsWith("&")) ? result.Substring(1, result.Length - 1) : result;
            result = (result.StartsWith("|")) ? result.Substring(1, result.Length - 1) : result;
            result = (result.StartsWith("!")) ? result.Substring(1, result.Length - 1) : result;
            return result;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdnDetailRowIds.Value = "";
            txtSelectedIds.Text = "";
            odsCandidateSearch.SelectMethod = "GetPaged_DefaultSorting";
            odsCandidateSearch.SelectParameters["DisplayOption"].DefaultValue = "";
            /*if (ddlExpandColapseDetails.SelectedItem.Text == "Expand")
            {
                odsCandidateSearch.SelectMethod = "GetPaged_ExpandSorting";
                odsCandidateSearch.SelectParameters["DisplayOption"].DefaultValue = "Expand";
                //odsCandidateSearch
            }*/
            lblRangeError.Text = "";
            if (txtSalaryRangeFrom.Text.Trim() != "" || txtSalaryRangeTo.Text.Trim() != "")
            {
                if (txtSalaryRangeFrom.Text.IsNullOrEmpty() || txtSalaryRangeTo.Text.IsNullOrEmpty())
                {
                    lblRangeError.Visible = true;
                    lblRangeError.Text = "Please enter valid salary range";
                    return;
                }
            }
            if (txtAllKeywords.Text == "")
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "[C].[CandidateName]";
                odsCandidateSearch.SelectParameters["SortColumn"].DefaultValue = ddlNameFormat.SelectedIndex == 1 ? "[C].[LastName]" : "[C].[CandidateName]";
            }
            else
            {
                SortOrder.Text = "desc";
                SortColumn.Text = "[CO].[RANK]";
                odsCandidateSearch.SelectParameters["SortColumn"].DefaultValue = "[CO].[RANK]";
            }
            odsCandidateSearch.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            lsvSearchResult.Visible = true; 
            lsvSearchResult.Items.Clear();
            _watch = new Stopwatch();
            _watch.Start();
            hfStartRowIndex.Value = "0";
            string allkeys = string.Empty;
            if (!string.IsNullOrEmpty(txtAllKeywords.Text))
                allkeys = BuildKeyWords(txtAllKeywords.Text.Replace("&", "").Replace("|", "").Replace("!", "")).Replace("'", "\"");
            odsCandidateSearch.SelectParameters["allKeys"].DefaultValue = allkeys; //1.2
            odsCandidateSearch.SelectParameters["anyKey"].DefaultValue = string.Empty; ; //1.2
            odsCandidateSearch.SelectParameters["currentPosition"].DefaultValue =MiscUtil .RemoveScript ( txtJobTitle.Text.Trim());
            odsCandidateSearch.SelectParameters["currentCity"].DefaultValue =txtWorkCity .Text .Trim ()=="City" ? string .Empty : MiscUtil .RemoveScript ( txtWorkCity.Text.Trim());
            odsCandidateSearch.SelectParameters["jobType"].DefaultValue = ddlJobType.SelectedValue == "0" ? null : ddlJobType.SelectedValue;
            odsCandidateSearch.SelectParameters["CurrentCompany"].DefaultValue = MiscUtil.RemoveScript(txtEmpOrCom.Text.Trim());
            odsCandidateSearch.SelectParameters["CompanyStatus"].DefaultValue = ddlEmpOrCom.SelectedValue;
            odsCandidateSearch.SelectParameters["CandidateEmail"].DefaultValue = MiscUtil.RemoveScript(txtCandidateEmail.Text.Trim());
            odsCandidateSearch .SelectParameters ["MobilePhone"].DefaultValue =MiscUtil .RemoveScript (txtMobilePhone .Text .Trim ());
            odsCandidateSearch.SelectParameters["ZipCode"].DefaultValue = MiscUtil.RemoveScript(txtZipCode.Text.Trim());
            odsCandidateSearch.SelectParameters["Radius"].DefaultValue = (txtWorkCity.Text.Trim() != string .Empty  &&  uclCountryState .SelectedStateId != 0) ? ddlRadius.SelectedValue : string.Empty;
            odsCandidateSearch.SelectParameters["AddinWhereClaues"].DefaultValue = (txtWorkCity.Text.Trim() != string .Empty && uclCountryState .SelectedStateId  != 0) ? "false" : "true";
            odsCandidateSearch.SelectParameters["IsMile"].DefaultValue = ddlRadius.SelectedItem.Text.Contains("miles") ? "true" : "false";
            odsCandidateSearch.SelectParameters["AssignedManagerId"].DefaultValue = ddlAssignedManager.SelectedValue;// ddlSararySelection.SelectedValue;
            odsCandidateSearch.SelectParameters["SalarySelector"].DefaultValue = ddlSararySelection.SelectedValue;
            
            string Qualification = null;
            string WorkSchedule = null;

            string Gender = null;
            string Nationality = null;
            string Industry = null;
            string JobFunction = null;
           
            //foreach (ListItem li in chkEducationList.Items)
            //{
            //    if (li.Selected)
            //    {
            //        Qualification += li.Value.ToString() + ",";
            //    }
            //}

            //if (!(string.IsNullOrEmpty(Qualification)))
            //    Qualification = Qualification.Remove(Qualification.Length - 1);
            Qualification = uclEducation.SelectedItems;
            if (ddlWorkSchedule.SelectedIndex != -1 && ddlWorkSchedule.SelectedIndex != 0)
                WorkSchedule = ddlWorkSchedule.SelectedValue;


            if (ddlGender.SelectedIndex != -1 && ddlGender.SelectedIndex != 0)
                Gender = ddlGender.SelectedValue;
            if (ddlCountryOfBirth.SelectedIndex != -1 && ddlCountryOfBirth.SelectedIndex != 0)
                Nationality = ddlCountryOfBirth.SelectedValue;
            if (ddlIndustry.SelectedIndex != -1 && ddlIndustry.SelectedIndex != 0)
                Industry = ddlIndustry.SelectedValue;
            if (ddlJobFunction.SelectedIndex != -1 && ddlJobFunction.SelectedIndex != 0)
                JobFunction = ddlJobFunction.SelectedValue;

            odsCandidateSearch.SelectParameters["workStatus"].DefaultValue = uclWorkStatus .SelectedItems ;

            string hiringstatus = uclHiringLevel.SelectedItems;
            //foreach (ListItem item in uclHiringLevel .ListItem .Items)
            //{
            //    if (item.Selected)
            //    {
            //        if (hiringstatus != "") hiringstatus += ",";
            //        hiringstatus += item.Value;
            //    }

            //}
            odsCandidateSearch.SelectParameters["HiringStatus"].DefaultValue = hiringstatus;
            odsCandidateSearch.SelectParameters["securityClearence"].DefaultValue = optSecurityClearance.SelectedValue == "1" ? optSecurityClearance.SelectedValue : null;

            if (!string.IsNullOrEmpty(wdcAvailability.Text.Trim().ToString()))
            {
                odsCandidateSearch.SelectParameters["availability"].DefaultValue = wdcAvailability.Text.Trim() == "" ? wdcAvailability.Text.Trim().ToString() : (Convert.ToDateTime(wdcAvailability.Text.Trim()).ToString("MM/dd/yyyy"));
            }
            else odsCandidateSearch.SelectParameters["availability"].DefaultValue = null;
            odsCandidateSearch.SelectParameters["industryType"].DefaultValue =  null ;
            odsCandidateSearch.SelectParameters["lastUpdateDate"].DefaultValue = ddlResumeLastUpdate.SelectedValue == "0" ? null : ddlResumeLastUpdate.SelectedValue;
            odsCandidateSearch.SelectParameters["applicantType"].DefaultValue = null;
            odsCandidateSearch.SelectParameters["salaryType"].DefaultValue = (txtSalaryRangeFrom.Text.Trim() == "" && txtSalaryRangeTo.Text.Trim() == "" ? null : ddlPaymentType.SelectedValue);  // ddlPaymentType.SelectedValue == "0" ? null : ddlPaymentType.SelectedValue;
            odsCandidateSearch.SelectParameters["salaryFrom"].DefaultValue =MiscUtil .RemoveScript ( txtSalaryRangeFrom.Text.Trim());
            odsCandidateSearch.SelectParameters["salaryTo"].DefaultValue =MiscUtil .RemoveScript ( txtSalaryRangeTo.Text.Trim());
            odsCandidateSearch.SelectParameters["currencyType"].DefaultValue = ddlCurrency.SelectedValue == "0" ? null : ddlCurrency.SelectedValue;
            odsCandidateSearch.SelectParameters["functionalCategory"].DefaultValue = null ;
            odsCandidateSearch.SelectParameters["hiringMatrixId"].DefaultValue = null;
            odsCandidateSearch.SelectParameters["hotListId"].DefaultValue = ddlHotList.Visible == true ? (ddlHotList.SelectedValue == "0" ? null : ddlHotList.SelectedValue) : (hdnSelectedHotListText.Value == txtSearch_HotList.Text && txtSearch_HotList.Text != string.Empty ? hdnSelectedHotList.Value : null);
            odsCandidateSearch.SelectParameters["memberId"].DefaultValue = CurrentMember == null ? null : base.CurrentMember.Id.ToString();

            int rows = (Request.Cookies["PreciseSearchRowPerPage"] != null ? (Convert.ToInt32(Request.Cookies["PreciseSearchRowPerPage"].Value == "" ? Convert.ToString(20) : Request.Cookies["PreciseSearchRowPerPage"].Value)) : Convert.ToInt32(20));

            odsCandidateSearch.SelectParameters["rowPerPage"].DefaultValue = rows.ToString(); 
            odsCandidateSearch.SelectParameters["memberPositionId"].DefaultValue = null; 
            odsCandidateSearch.SelectParameters["isMyList"].DefaultValue = (IsMyList) ? "1" : "0";
            odsCandidateSearch.SelectParameters["minExpYr"].DefaultValue =MiscUtil .RemoveScript ( txtMinExp.Text.Trim());
            odsCandidateSearch.SelectParameters["maxExpYr"].DefaultValue =MiscUtil .RemoveScript ( txtMaxExp.Text.Trim());
            odsCandidateSearch.SelectParameters["maxResults"].DefaultValue = null;       
            odsCandidateSearch.SelectParameters["Education"].DefaultValue = Qualification;
            odsCandidateSearch.SelectParameters["WorkSchedule"].DefaultValue = WorkSchedule;
            odsCandidateSearch.SelectParameters["NoticePeriod"].DefaultValue = MiscUtil.RemoveScript(txtNoticePeriod.Text.Trim());

            odsCandidateSearch.SelectParameters["Gender"].DefaultValue = Gender;//Gender
            odsCandidateSearch.SelectParameters["Nationality"].DefaultValue = Nationality;//Gender
            odsCandidateSearch.SelectParameters["Industry"].DefaultValue = Industry;//Gender
            odsCandidateSearch.SelectParameters["JobFunction"].DefaultValue = JobFunction;//Gender

            string allKeyWords = txtAllKeywords.Text.Trim();
            string anyKeyWord = string.Empty; //1.2
            string currentPosition =MiscUtil .RemoveScript ( txtJobTitle.Text.Trim());
            string currentCity =MiscUtil .RemoveScript ( txtWorkCity.Text.Trim());
            string currentCountryId = uclCountryState.SelectedCountryId.ToString();
            string applicantType =  null;
            string jobType = ddlJobType.SelectedValue == "0" ? null : ddlJobType.SelectedValue;
            string currentcompany = MiscUtil.RemoveScript(txtEmpOrCom.Text.Trim());
            string companystatus = ddlEmpOrCom.SelectedValue;
            string CandidateEmail = MiscUtil.RemoveScript(txtCandidateEmail.Text.Trim());
            string MobilePhone = MiscUtil.RemoveScript(txtMobilePhone.Text.Trim());
            string ZipCode =  MiscUtil.RemoveScript(txtZipCode.Text.Trim());
            string Radius = (txtWorkCity .Text .Trim ()!=string .Empty  && uclCountryState .SelectedStateId  !=0) ? ddlRadius.SelectedValue : string.Empty;
            bool AddinWhereClause = (txtWorkCity.Text.Trim() != string .Empty  && uclCountryState .SelectedStateId  != 0) ? false : true;
            bool IsMile = ddlRadius.SelectedItem.Text.Contains("miles") ? true : false;
           
            //Adding Education Data-----------------------------------------------
            //foreach (ListItem li in chkEducationList.Items)
            //    if (li.Selected) Qualification += li.Value.ToString() + ",";
            //if (!(string.IsNullOrEmpty(Qualification)))
            //    Qualification = Qualification.Remove(Qualification.Length - 1);
            Qualification = uclEducation.SelectedItems;

            //--------------------------------------------------------------------

          
            //Adding Work Schedule
            if (ddlWorkSchedule.SelectedIndex != -1 && ddlWorkSchedule.SelectedIndex != 0)
                WorkSchedule = ddlWorkSchedule.SelectedValue;



            if (ddlGender.SelectedIndex != -1 && ddlGender.SelectedIndex != 0)
                Gender = ddlGender.SelectedValue;
            if (ddlCountryOfBirth.SelectedIndex != -1 && ddlCountryOfBirth.SelectedIndex != 0)
                Nationality = ddlCountryOfBirth.SelectedValue;
            if (ddlIndustry.SelectedIndex != -1 && ddlIndustry.SelectedIndex != 0)
                Industry = ddlIndustry.SelectedValue;
            if (ddlJobFunction.SelectedIndex != -1 && ddlJobFunction.SelectedIndex != 0)
                JobFunction = ddlGender.SelectedValue;


            string securityClearence = optSecurityClearance.SelectedIndex > -1 ? optSecurityClearance.SelectedValue : null;

            string availability = string.Empty;
            if (!string.IsNullOrEmpty(wdcAvailability.Text.Trim().ToString()))
            {
                availability = wdcAvailability.Text.Trim().ToString();
            }
            string industryType = null;
            string lastUpdateDate = ddlResumeLastUpdate.SelectedValue == "0" ? null : ddlResumeLastUpdate.SelectedValue;
            string salaryType = ddlPaymentType.SelectedValue == "0" ? null : ddlPaymentType.SelectedValue;
            string salaryFrom =MiscUtil .RemoveScript ( txtSalaryRangeFrom.Text.Trim());
            string salaryTo =MiscUtil .RemoveScript ( txtSalaryRangeTo.Text.Trim());
            string currencyType = ddlCurrency.SelectedValue == "0" ? null : ddlCurrency.SelectedValue;
            string functionalCategory = null;

            string hiringMatrixId = null;
            string hotListId = ddlHotList.Visible == true ? (ddlHotList.SelectedValue == "0" ? null : ddlHotList.SelectedValue) : (hdnSelectedHotListText.Value == txtSearch_HotList.Text && txtSearch_HotList.Text != string.Empty ? hdnSelectedHotList.Value : null);
            string memberId = CurrentMember == null ? null : base.CurrentMember.Id.ToString();

            string memberPositionId = null; 

            string strIsMyList = IsMyList ? "1" : "0";
            string minExpYr = txtMinExp.Text.Trim();
            string maxExpYr = txtMaxExp.Text.Trim();

            string strState = uclCountryState.SelectedStateId.ToString();
            string NoticePeriod = MiscUtil.RemoveScript(txtNoticePeriod.Text.Trim());
            allKeyWords = BuildKeyWords(allKeyWords.Replace("&", "").Replace("|", "")).Replace("'", "\"");
            string whereClause = "";// Facade.GetCandidateSearchQueryForPreciseSearch(allKeyWords, anyKeyWord, currentPosition, currentCity, currentCountryId, jobType, uclWorkStatus.SelectedItems, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, memberPositionId, strIsMyList, minExpYr, maxExpYr, strState, applicantType, Qualification, WorkSchedule, hiringstatus, currentcompany, companystatus, CandidateEmail, MobilePhone, ZipCode, Radius, AddinWhereClause, IsMile, ddlAssignedManager.SelectedValue, ddlSararySelection.SelectedValue, ddlinternalratingID.SelectedValue);
            Session["SearchQuery"] = whereClause;

            lsvSearchResult.DataSourceID = "odsCandidateSearch";

            if (whereClause.Length < 8000) 
            {
                try
                {
                    ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvSearchResult.FindControl("pagerControl");
                    if (PagerControl != null)
                    {
                        string pagesize = (Request.Cookies["PreciseSearchRowPerPage"] == null ? "" : Request.Cookies["PreciseSearchRowPerPage"].Value); ;
                        DataPager pager = PagerControl.FindControl("pager") as DataPager;//2.6
                        if (pager != null) pager.PageSize = (pagesize == "" ? 20 : Convert.ToInt32(pagesize));
                        if (pager.Page.IsPostBack)
                            pager.SetPageProperties(0, pager.MaximumRows, true);
                    }
                    
                    lsvSearchResult.DataBind();
                    _watch.Stop();
                    ShowResultSummary();
                }
                catch (Exception ef)
                {
                }
            }

            else
            {
                MiscUtil.ShowMessage(lblMessage, "Too many keywords. Please remove some keywords.", true, true);

            }

            System.Web.UI.HtmlControls.HtmlTableCell thRelevance = (System.Web.UI.HtmlControls.HtmlTableCell)lsvSearchResult.FindControl("thRelevance");
            if (allKeyWords == "")
            {

                if (thRelevance != null)
                    thRelevance.Visible = false;
                txtSortColumn.Text = "lnkHeaderName";
                txtSortOrder.Text = "ASC";
                PlaceUpDownArrow();
            }
            else
            {
                if (thRelevance != null)
                    thRelevance.Visible = true;
                txtSortColumn.Text = "lnkHeaderRelevance";
                txtSortOrder.Text = "DESC";
                PlaceUpDownArrow();
            }

            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
            ForHighlightString();   
           
        }

        private void setPinZipText()
        {
            if (SiteSetting[DefaultSiteSetting.Country.ToString()] != null)
            {
                {
                    if (uclCountryState .SelectedCountryName == "India")
                    {
                        txtZipCode.ToolTip = "PIN Code";
                        revZIPCode.ErrorMessage = "Please enter a valid PIN code";
                        foreach (ListItem item in ddlRadius.Items)
                        {
                            item.Text = item.Text.Replace("miles", "km");
                        }
                    }
                    else
                    {
                        txtZipCode.ToolTip = "Zip Code";
                        revZIPCode.ErrorMessage = "Please enter a valid Zip code";
                        foreach (ListItem item in ddlRadius.Items)
                        {
                            item.Text = item.Text.Replace("km", "miles");
                        }
                    }
                }
            }
        }
        protected void btnAddToHotList_Click(object sender, EventArgs e)
        {
            if (getSelectedHotListID ()>0)
            {
                int successCount = 0;
                int totalCount = 0;
                char[] delim = { ','};
                string[] candidateIds = txtSelectedIds.Text.Split(delim,StringSplitOptions.RemoveEmptyEntries );
                if (candidateIds.Length > 0)
                {
                    foreach (string id in candidateIds)
                    {
                        if (!string.IsNullOrEmpty(id))
                        {
                            if (AddToHotList(getSelectedHotListID(), int.Parse(id)))
                            {
                                successCount++;
                            }
                            totalCount++;
                        }
                    }
                    MiscUtil.ShowMessage(lblMessage, successCount.ToString() + " of " + totalCount.ToString() + " candidate(s) added.", false);
                    if (lsvSearchResult.Items.Count > 0)
                    {
                        for (int i = 0; i < lsvSearchResult.Items.Count; i++)
                        {
                            CheckBox chkSinglecandidate = (CheckBox)lsvSearchResult.Items[i].FindControl("chkCandidate");
                            chkSinglecandidate.Checked = false;
                        }
                    }
                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate to add.", true);
                }
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select hotlist.", true);
            }
            txtSelectedIds.Text = "";
        }

        string[] candidateIds;
        private void SendEmailToAccountManager()
        {
            int hireCount = 0;
            string strMemberEmails = string.Empty;

            bool IsError = false;
            string strSMTP = string.Empty;
            string strUser = string.Empty;
            string strPwd = string.Empty;
            string strEmailid = string.Empty; 
            int intPort = 25;
            bool boolsslEnable = false;
            Hashtable siteSettingTable = null;

            SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            if (siteSetting != null)
            {
                siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
            }
            JobPosting jobPosting = Facade.GetJobPostingById(getSelectedRequisitionID ());
            if (jobPosting != null)
            {
                Member member = Facade.GetMemberById(jobPosting.CreatorId);
                strEmailid = member.FirstName + " " + member.LastName + "<" + member.PrimaryEmail + ">";

            }
            if (siteSetting != null)
            {
                strUser = siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                strPwd = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                strSMTP = siteSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();
                if (siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != null && siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != "")
                {
                    try
                    {
                        intPort = Convert.ToInt32(siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString());
                    }
                    catch (FormatException)
                    {
                        MiscUtil.ShowMessage(lblMessage, "SMTP Port data is incorrect in site settings.", true);
                        return;
                    }
                }

                if (siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString() != null)
                    boolsslEnable = Convert.ToBoolean(siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString());
            }

            if (strUser != "" && strPwd != "" && strSMTP != "")
            {
                if (strEmailid.Trim() != "")
                {
                    IsError = SendEmail(strEmailid, strSMTP, strUser, strPwd, intPort, boolsslEnable);
                }
            }
            else
            {
                IsError = true;
            }
            if (IsError)
            {
                MiscUtil.ShowMessage(lblMessage, "Please set SMTP credentials in site settings.", true);
                return;
            }
        }

        private bool SendEmail(string strEmailId, string strSMTP, string strUser, string strPwd, int intPort, bool boolsslEnable)
        {
            bool strSendStatus = true;

            CultureInfo cultInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo txtInfo = cultInfo.TextInfo;

            string strSenderName = txtInfo.ToTitleCase(base.CurrentMember.FirstName + " " + base.CurrentMember.LastName);
            string[] strUserName = strEmailId.Split('<');

            MailMessage message = new MailMessage();
            MailAddress mailAddressFrom = new MailAddress(base.CurrentMember.PrimaryEmail);//1.8
            MailAddress mailAddressTo = new MailAddress(strEmailId);
            message.From = mailAddressFrom;
            message.To.Add(mailAddressTo);
            message.IsBodyHtml = true;
            message.Subject = "NOTIFICATION: New candidates are added to the requisition - " ;
            message.Body = "Dear " + txtInfo.ToTitleCase(strUserName[0]) + ", <br/><br/> New candidates are added to the requisition - "  + ". Please go to your <b>My Requisition List</b> to view the requisition details." + "<br/><br/>Regards,<br/>" + strSenderName;
            SmtpClient emailClient = new SmtpClient(strSMTP, intPort);
            NetworkCredential SMTPUserInfo = new NetworkCredential(strUser, strPwd);
            emailClient.EnableSsl = boolsslEnable;
            emailClient.UseDefaultCredentials = false;
            emailClient.Credentials = SMTPUserInfo;
            object userState = message;
            try
            {
                emailClient.SendAsync(message, userState);

                strSendStatus = false;
            }
            catch (Exception ex)
            {
                MiscUtil.ShowMessage(lblMessage, ex.Message.ToString(), true);
                strSendStatus = true;
            }

            return strSendStatus;
        }

        MemberHiringProcess memberHiringProcess;
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            string applicantIds = txtSelectedIds.Text; 
            if (applicantIds != string.Empty)
            {
                uclConfirm.AddMessage("Are you sure that you want to delete this candidate(s)?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate to remove.", true);
            }
        }
        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                string[] candidateIDs = txtSelectedIds.Text.Split(',');
                int noofCandidates = 0;
                foreach (string s in candidateIDs)
                {

                    int id = Convert.ToInt32(s);
                    Member member = Facade.GetMemberById(id);
                    if (member != null)
                    {
                        System.Web.Security.MembershipUser membershipUser = System.Web.Security.Membership.GetUser(member.UserId);

                        if (Facade.DeleteMemberById(id))
                        {
                            System.Web.Security.Roles.RemoveUserFromRole(membershipUser.UserName, ContextConstants.ROLE_CANDIDATE);
                            System.Web.Security.Membership.DeleteUser(membershipUser.UserName, true);
                            noofCandidates++;
                        }
                    }
                }

                DataBind();
                txtSelectedIds.Text  = "";
                MiscUtil.ShowMessage(lblMessage, "Successfully removed " + noofCandidates + " candidate(s).", false);
            }

        }
        private int getSelectedRequisitionID()
        {
            return uclComReq.SelectedRequisitionId;
        }
        private int getSelectedHotListID()
        {
            if(ddlJobSeekerGroup .Visible ==true )
                  return Convert.ToInt32(ddlJobSeekerGroup .SelectedValue );
            else   return Convert.ToInt32(hdnSelectedHotListValue.Value  );

        }
        protected void btnAddToSelectionStep_Click(object sender, EventArgs e)
        {


            string s = hdnSelectedRequisitionValue.Value;
            string[] IDs = txtSelectedIds.Text.Split(',');
            int intRowsSelected = 0;
            int requisitionId = getSelectedRequisitionID(); 
            int intCounter = 0;
            int intCounterChecked = 0;
            int intCounterAdded = 0;
            string strMessage = String.Empty;
            int intAlreadyInList = 0;//ADDED BY PRAVIN KHOT ON 28/April/2016
            bool boolError = false;
            if (requisitionId == 0)
            {
                MiscUtil.ShowMessage(lblMessage, "Please select Requisition.", true );
                uclComReq.SelectedRequisitionId = 0;
                return;
            }
            if (txtSelectedIds.Text.Trim() == "")
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate to add.", true);
                return;
            }
            string CanIds = txtSelectedIds.Text;

            //***********CODE COMMENT BY PRAVIN KHOT ON 28/April/2016*****************
            //int intAlreadyInList = Facade.MemberJobCart_AddCandidateToRequisition(base.CurrentMember.Id, CanIds, requisitionId 
            //*********************************END********************************

            //**************code added by pravin khot on 28/April/2016  use for setting- AllowCandidatetobeaddedonMultipleRequisition**************
            #region
            Hashtable siteSettingTable = null;

            TPS360.BusinessFacade.Facade f = new TPS360.BusinessFacade.Facade();
            MemberExtendedInformation MailSetting = f.GetMemberExtendedInformationByMemberId(CurrentMember.Id);
            TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
            SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
            string chkstatus;
            chkstatus = siteSettingTable[DefaultSiteSetting.AllowCandidatetobeaddedonMultipleRequisition.ToString()].ToString();
                      
            if (chkstatus == "False")
            {
                intAlreadyInList = Facade.MemberJobCart_AddCandidateToRequisition(base.CurrentMember.Id, CanIds, requisitionId);
            }
            else
            {
                intAlreadyInList = Facade.MemberJobCart_AddCandidateToMultipleRequisition(base.CurrentMember.Id, CanIds, requisitionId);
            }                       
          
            #endregion
            //*******************************END******************************************

            string[] Ids = CanIds.Split(',');
            intRowsSelected = Ids.Length;
            intCounterChecked = Ids.Length;
            intCounterAdded = Ids.Length - intAlreadyInList;            
            foreach (ListViewItem lsvItm in lsvSearchResult.Items)
            {
                if (ControlHelper.IsListItemDataRow(lsvItm.ItemType))
                {
                    CheckBox chkItemCandidate = (CheckBox)lsvItm.FindControl("chkCandidate");

                    if (chkItemCandidate != null && chkItemCandidate.Checked)
                    {
                        chkItemCandidate.Checked = false;
                    }
                }
            }
            if (intRowsSelected > 0)
            {
                if (intCounterChecked > 0)
                {
                    if (intCounterAdded > 0)
                    {
                       // SendEmailToAccountManager();

                        if (intCounterAdded == intCounterChecked)
                        {
                            strMessage = "Successfully added selected candidate(s).";
                        }
                        else
                        {
                            strMessage = "Successfully added " + intCounterAdded.ToString() + " of " + intCounterChecked.ToString() + " selected candidate(s).";
                        }
                    }
                    else
                    {
                        boolError = true;
                        //strMessage = "The Selected candidate(s) are already in the list.";//code comment by pravin khot on 2/May/2016
                        strMessage = "Candidate(s) selected from the list are already added to a Requisition and in the Hiring Process.";//new line added by pravin khot on 2/May/2016
                    }
                }
                else
                {
                    boolError = true;
                    strMessage = "Please select at least one candidate to add.";
                }

            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate to add.", true );
            }
            if (strMessage != string.Empty)
                MiscUtil.ShowMessage(lblMessage, strMessage, boolError);


            txtSelectedIds.Text = "";
            uclComReq.SelectedRequisitionId = 0;
        }
       
        #endregion

        #region ListViewEvent

      
        protected void lsvSearchResult_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                HyperLink hlnCandidateName = (HyperLink)e.Item.FindControl("hlnCandidateName");
                Candidate candidate = ((ListViewDataItem)e.Item).DataItem as Candidate;
                LinkButton lblCandidateEmail = (LinkButton)e.Item.FindControl("lblCandidateEmail");
                CheckBox chkCandidate = (CheckBox)e.Item.FindControl("chkCandidate");
                if (candidate != null)
                {
                    System.Web.UI.HtmlControls.HtmlTableRow row = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("row");
                    System.Web.UI.HtmlControls.HtmlTableRow trCandidateDetailss = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trCandidateDetails");
                    Image imgShowHide = (Image)e.Item.FindControl("imgShowHide");
                    row.Attributes.Add("onclick", "javascript:ShowOrHideRow('" + trCandidateDetailss.ClientID + "','" + imgShowHide.ClientID + "','" + candidate.Id + "','" + (txtAllKeywords .Text ==""? "0" : "1") + "');");
                    hlnCandidateName.Attributes.Add("onclick", "javascript:ShowOrHide('" + trCandidateDetailss.ClientID + "','" + imgShowHide.ClientID + "');");
                    lblCandidateEmail.Attributes.Add("onclick", "javascript:ShowOrHide('" + trCandidateDetailss.ClientID + "','" + imgShowHide.ClientID + "');");
                    chkCandidate.Attributes.Add("onclick", "javascript:ShowOrHideCheckBoxClicked('" + trCandidateDetailss.ClientID + "','" + imgShowHide.ClientID + "')");
                    hdnDetailRowIds.Value += trCandidateDetailss.ClientID + ";" + imgShowHide.ClientID + ",";
                    System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvSearchResult.FindControl("tdPager");
                    System.Web.UI.HtmlControls.HtmlTableCell tdDetail = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDetail");
                    /*if (ddlExpandColapseDetails.SelectedItem.Text == "Expand")
                    {
                        imgShowHide.ImageUrl = "../Images/collapse-minus.png";
                        trCandidateDetailss.Style.Add(HtmlTextWriterStyle.Display, "");
                    }*/
                    if (txtAllKeywords.Text.Trim() == string.Empty)
                    {
                        if (tdPager != null) tdPager.ColSpan = 10;// if (tdPager != null) tdPager.ColSpan = 8;
                        if (tdDetail != null) tdDetail.ColSpan = 5;
                        System.Web.UI.HtmlControls.HtmlTableCell thRelevance = (System.Web.UI.HtmlControls.HtmlTableCell)lsvSearchResult.FindControl("thRelevance");
                        if (thRelevance != null) thRelevance.Visible = false;
                        System.Web.UI.HtmlControls.HtmlTableCell tdRelevance = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdRelevance");
                        if (tdRelevance != null) tdRelevance.Visible = false;
                    }
                    else
                    {
                        if (tdPager != null) tdPager.ColSpan = 11;//if (tdPager != null) tdPager.ColSpan = 9;
                        if (tdDetail != null) tdDetail.ColSpan = 6;
                    }
                    //System.Web.UI.HtmlControls.HtmlGenericControl relevance_bar = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("relevance_bar");
                    //relevance_bar.Style.Add(HtmlTextWriterStyle.Width, Convert.ToDouble(candidate.Rank).ToString("#,#0") + "%");
                    System.Web.UI.HtmlControls.HtmlGenericControl relevance_text = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("relevance_text");
                    //relevance_text.InnerText = Convert.ToDouble(candidate.Rank).ToString("#,#0") + "%";
                    relevance_text.Style[HtmlTextWriterStyle.Width] = Convert.ToDouble(candidate.Rank).ToString("#,#0") + "%"; 

                    Label lblCandidateExperience = (Label)e.Item.FindControl("lblCandidateExperience");
                    Label lblCandidateCurrentPosition = (Label)e.Item.FindControl("lblCandidateCurrentPosition");
                    Label lblCandidateCurrentCompany = (Label)e.Item.FindControl("lblCandidateCurrentCompany");
                    Label lblCandidateWorkStatus = (Label)e.Item.FindControl("lblCandidateWorkStatus");
                    Label lblCandidateCurrentSalary = (Label)e.Item.FindControl("lblCandidateCurrentSalary");
                    Label lblCandidateExpectedSalary = (Label)e.Item.FindControl("lblCandidateExpectedSalary");
                    Label lblCandidateLastUpdate = (Label)e.Item.FindControl("lblCandidateLastUpdate");
                    Label lblCandidateAvailability = (Label)e.Item.FindControl("lblCandidateAvailability");
                    Label lblCandidateMobile = (Label)e.Item.FindControl("lblCandidateMobile");

                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    Label lblCandidateSkills = (Label)e.Item.FindControl("lblskills");
                    Label lblHiringstatus = (Label)e.Item.FindControl("lblHiringstatus");
                    Label lblPrimaryManager = (Label)e.Item.FindControl("lblPrimaryManager");
                    Label lblCandidateObjective = (Label)e.Item.FindControl("lblCandidateObjective");
                    Label lblRemarks = (Label)e.Item.FindControl("lblRemarks");
                    Label lblHighestDegree = (Label)e.Item.FindControl("lblHighestDegree");
                    Label lblCandidateType = (Label)e.Item.FindControl("lblCandidateType");
                    Label lblWorkSchedule = (Label)e.Item.FindControl("lblWorkSchedule");

                    //*************ADDED BY PRAVIN KHOT ON 23/June/2016 ********************
                    Label lblSource = (Label)e.Item.FindControl("lblSource");
                    Label lblSourceDescription = (Label)e.Item.FindControl("lblSourceDescription");
                    lblSource.Text = candidate.Source;
                    //lblSourceDescription.Text = candidate.SourceDescription;
                    lblSourceDescription.Text = candidate.SourceDescription.Substring(candidate.SourceDescription.LastIndexOf('>') + 1);
                    //***********************END**********************************

                    HiddenField hfCandidateId = (HiddenField)e.Item.FindControl("hfCandidateId");
                    System.Web.UI.HtmlControls.HtmlTableRow trCandidateDetails = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trCandidateDetails");
                    txtAllKeywords.Text = txtAllKeywords.Text != string.Empty ? System.Text.RegularExpressions.Regex.Replace(txtAllKeywords.Text, @"\s+", " ") : string.Empty;
                    if ((candidate.Id > 0) && (candidate.DisplayOption==null))
                    {
                        lblPrimaryManager.Text = candidate.PrimaryManagerName; //Facade.getMemberManager_PrimaryManagerName(candidate.Id);
                        //State st = null;
                        //if (candidate.PermanentStateId != null && candidate.PermanentStateId != 0)
                        //    st = Facade.GetStateById(candidate.PermanentStateId);
                        if (candidate.CurrentCity.ToLower().Trim() == txtWorkCity.Text.ToLower().Trim() && txtWorkCity.Text.Trim() != "")
                            lblCity.Text = "<span class=highlight>" + candidate.CurrentCity + "</span>";
                        else
                            lblCity.Text = candidate.CurrentCity;
                        //string state = candidate.PermanentStateName;
                        //if (lblCity.Text != "" && state.Trim () != "") lblCity.Text += ", ";
                        //if ( state.Trim() != "")
                        //{
                        //    if (candidate .PermanentStateId  ==Convert .ToInt32 ( hdnSelectedState.Value  ==string .Empty ? "0":hdnSelectedState .Value ))
                        //        lblCity.Text += "<span class=highlight>" + state  + "</span>";
                        //    else 
                        //    lblCity.Text += state;
                        //}
                        if (txtAllKeywords.Text != string.Empty)
                        {
                            //if (lblCity.Text.ToLower().Contains(txtAllKeywords.Text.ToLower().Trim()))
                            lblCity.Text = HighlightProcess(txtAllKeywords.Text, lblCity.Text);//HighlightString(txtAllKeywords.Text, lblCity.Text);//"<span class=highlight>" + lblCity.Text + "</span>";
                        }

                        hfCandidateId.Value = candidate.Id.ToString();

                        chkCandidate.Checked = CheckSelectedList(candidate.Id);
                        string _candidateName = "";

                        if (ddlNameFormat.SelectedValue == "0")
                            _candidateName = candidate.FirstName;
                        else
                        {
                            char[] delim = { ' ' };
                            string[] names= candidate .FirstName .Split (delim ,StringSplitOptions .RemoveEmptyEntries );
                            if (names.Length > 1)
                            {
                                _candidateName = names[1] + ", " + names[0];
                            }
                            else {
                                _candidateName = names[0];
                            }
                            
                        }

                        if (_candidateName.Trim() == "")
                            _candidateName = "No Candidate Name";
                        if (_IsAccessToCandidate)
                        {
                            if (IdForSitemap > 0 && UrlForCandidate != string.Empty)
                            {
                                string[] list = txtAllKeywords.Text.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                                if (list.Contains("not") || list.Contains("Not") || list.Contains("-") || list.Contains("NOT"))
                                {
                                    string[] split = txtAllKeywords.Text.Split(new string[] { "not", "NOT", "Not", "-" }, StringSplitOptions.RemoveEmptyEntries);
                                    if (split.Length > 0)
                                    {
                                        Session["PreciseSearch"] = "PreciseSearch";
                                        if (!string.IsNullOrEmpty(txtAllKeywords.Text))
                                        {
                                            Session["AllSearchKeywords"] = ReplaceUnWantedCharacters(split[0]);
                                        }
                                        ControlHelper.SetHyperLink(hlnCandidateName, UrlForCandidate, string.Empty, _candidateName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(candidate.Id), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.OVERVIEW_SITEMAP_ID, UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.OVERVIEW_SITEMAP_PARENTID, UrlConstants.PARAM_PAGE_FROM, "Precise", UrlConstants.PARAM_PRECISE_SEARCH, UrlConstants.PARAM_PRECISE_SEARCH, UrlConstants.PARAM_SEARCH_ALLKEYWORDS, split[0].Trim(), UrlConstants.PARAM_SEARCH_ANYKEYWORD, string.Empty,UrlConstants.PARAM_SITEMAP_PARENT_ID,"12");
                                    }
                                }

                                else
                                {
                                    Session["PreciseSearch"] = "PreciseSearch";
                                    if (!string.IsNullOrEmpty(txtAllKeywords.Text))
                                    {
                                        Session["AllSearchKeywords"] = ReplaceUnWantedCharacters(txtAllKeywords.Text);
                                    }

                                    ControlHelper.SetHyperLink(hlnCandidateName, UrlForCandidate, string.Empty, _candidateName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(candidate.Id), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.OVERVIEW_SITEMAP_ID, UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.OVERVIEW_SITEMAP_PARENTID, UrlConstants.PARAM_PAGE_FROM, "Precise", UrlConstants.PARAM_PRECISE_SEARCH, UrlConstants.PARAM_PRECISE_SEARCH, UrlConstants.PARAM_SEARCH_ALLKEYWORDS, txtAllKeywords.Text, UrlConstants.PARAM_SEARCH_ANYKEYWORD, string.Empty, UrlConstants.PARAM_SITEMAP_PARENT_ID, "12");
                                }
                            }
                        }
                        else
                            hlnCandidateName.Text = _candidateName;// candidate.FirstName + " " + candidate.LastName;
                        if (txtAllKeywords.Text != string.Empty)
                        {
                         
                            hlnCandidateName.Text = HighlightProcess(txtAllKeywords.Text, hlnCandidateName.Text);
                        }
                       
                        lblCandidateMobile.Text = candidate.CellPhone;//  member.CellPhone;
                        if (txtAllKeywords.Text != string.Empty)
                        {
                            lblCandidateMobile.Text = HighlightProcess(txtAllKeywords.Text, lblCandidateMobile.Text); 
                            
                        }
                        if (!string.IsNullOrEmpty(lblCandidateMobile.Text))
                        {
                            if (lblCandidateMobile.Text.Trim().ToLower() == txtMobilePhone.Text.Trim().ToLower())
                                lblCandidateMobile.Text = "<span class=highlight>" + candidate.CellPhone + "</span>";
                        }
                        //Binding PrimaryEmail
                        if (_IsMemberMailAccountAvailable)
                        {
                            lblCandidateEmail.Text = candidate.PrimaryEmail;
                            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_MEMBER_ID, candidate.Id.ToString());
                            lblCandidateEmail.OnClientClick = "btnMewMail_Click('" + url + "')";
                        }
                        else
                        {
                            lblCandidateEmail.Text = "<a href=mailto:" + candidate.PrimaryEmail + ">" + candidate.PrimaryEmail + "</a>";
                        }
                        if (txtAllKeywords.Text != string.Empty)
                        {
                            lblCandidateEmail.Text = HighlightProcess(txtAllKeywords.Text,candidate.PrimaryEmail);
                        }

                        string keyWordCount = string.Empty;
                        if (!string.IsNullOrEmpty(lblCandidateEmail.Text))
                        {
                            if (lblCandidateEmail.Text.Trim().ToLower() == txtCandidateEmail.Text.Trim().ToLower())
                                lblCandidateEmail.Text = "<span class=highlight>" + candidate.PrimaryEmail + "</span>";
                        }


                        if (!string.IsNullOrEmpty(candidate.TotalExperienceYears))
                        {
                            if (txtMinExp.Text.Trim() != string.Empty || txtMaxExp.Text.Trim() != string.Empty)
                                lblCandidateExperience.Text = "<span class=highlight>" + candidate.TotalExperienceYears + "</span>";
                            else
                                lblCandidateExperience.Text = candidate.TotalExperienceYears;
                        }
                        lblHiringstatus.Text = candidate.HighestHiringStatus;
                        foreach (ListItem item in uclHiringLevel .ListItem .Items)
                        {
                            if (item.Selected && item.Text == candidate.HighestHiringStatus)
                                lblHiringstatus.Text = "<span class=highlight>" + lblHiringstatus.Text + "</span>";
                        }


                        if (candidate.CurrentPosition.ToLower().Contains(txtJobTitle.Text.ToLower()))// txtJobTitle.Text.ToLower().Trim() == candidate.CurrentPosition.ToLower ().Trim ())
                            lblCandidateCurrentPosition.Text = HighlightString(txtJobTitle.Text, candidate.CurrentPosition);// "<span class=highlight>" + candidate.CurrentPosition + "</span>";
                        else
                            lblCandidateCurrentPosition.Text = candidate.CurrentPosition;
                        if (txtJobTitle.Text == string.Empty && txtAllKeywords.Text != string.Empty)
                        {
                            lblCandidateCurrentPosition.Text = HighlightProcess(txtAllKeywords.Text, candidate.CurrentPosition);
                        }
                        
                    }
                    
                }
            }

        }
        protected void lsvSearchResult_PreRender(object sender, EventArgs e)
        {
            divSearchSummary.InnerHtml = "";
            divShowSearchStatus.Visible = true;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvSearchResult.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Controls.Count >= 1)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }
                }
                else
                {
                    divShowSearchStatus.Visible = false;
                }

                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "PreciseSearchRowPerPage";

                HiddenField hdnTotalRowCount = (HiddenField)PagerControl.FindControl("hdnTotalRowCount");
                HiddenField hdnStartRowIndex = (HiddenField)PagerControl.FindControl("hdnStartRowIndex");
                HiddenField hdnMaxRow = (HiddenField)PagerControl.FindControl("hdnMaxRow");
                if (hdnTotalRowCount != null && hdnStartRowIndex != null && hdnMaxRow != null)
                {
                    if (Convert.ToInt32(hdnTotalRowCount.Value != "" ? hdnTotalRowCount.Value : "0") > 0)
                    {
                        divShowSearchStatus.Visible = true;
                        int from = Convert.ToInt32(hdnStartRowIndex.Value) + 1;
                        int maxrow = Convert.ToInt32(hdnMaxRow.Value);
                        int to = 0;
                        if (from + maxrow > Convert.ToInt32(hdnTotalRowCount.Value)) to = Convert.ToInt32(hdnTotalRowCount.Value);
                        else to = from + maxrow - 1;
                       // divSearchSummary.InnerHtml = "<b>Results " + from + " - " + to + " of " + hdnTotalRowCount.Value + (txtAllKeywords.Text.Trim() != "" ? " for " + txtAllKeywords.Text : "") + "</b>"; //1.2
                    }
                }
            }
            else
            {
                divShowSearchStatus.Visible = false;
            }
            PlaceUpDownArrow();
            if (txtZipCode.Text.ToLower().Trim() == "zip code" || txtZipCode.Text.ToLower().Trim() == "pin code")
            {
                setPinZipText();
            }

        }
       

        protected void ddlNameFormat_SelectedIndexChanged(object sender, EventArgs e)
        {

            Response.Cookies["PreciseSearchNameFormat"].Value = ddlNameFormat.SelectedValue;
            LinkButton lnkHeaderName = (LinkButton)lsvSearchResult.FindControl("lnkHeaderName");
            if (ddlNameFormat.SelectedIndex  == 1)
            {
                //SortColumn.Text = 
                    lnkHeaderName.CommandArgument = "[C].[LastName]";
                SortOrder.Text = "ASC";
                odsCandidateSearch.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                odsCandidateSearch.SelectParameters["SortColumn"].DefaultValue = "[C].[LastName]";
            }
            else
            {
                //btnName.CommandArgument = "[C].[FirstName]";
               // SortColumn.Text = 
                    lnkHeaderName.CommandArgument = "[C].[CandidateName]";
                SortOrder.Text = "ASC";
                odsCandidateSearch.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                odsCandidateSearch.SelectParameters["SortColumn"].DefaultValue = "[C].[CandidateName]";
                //odsCandidateList.SelectParameters["sortExpression"].DefaultValue = "[C].[FirstName]";
            }
            btnSearch_Click(sender, e);

        }


        protected void lsvSearchResult_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC")
                            txtSortOrder.Text = "DESC";
                        else
                            txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }

                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsCandidateSearch.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                    odsCandidateSearch.SelectParameters["SortColumn"].DefaultValue = SortColumn.Text.ToString();
                    if (e.CommandArgument.ToString() == "[C].[CandidateName]")
                    {
                        odsCandidateSearch.SelectParameters["SortColumn"].DefaultValue = ddlNameFormat.SelectedIndex == 1 ? "[C].[LastName]" : "[C].[CandidateName]";
                    }
                }
            }
            catch
            {
            }
        }

        #endregion

        #region Events


        protected void ddlPerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Cookies["PreciseSearchRowPerPage"].Value = Convert .ToString (20);
        }

        protected void lnkBtnEmail_Click(object sender, EventArgs e) 
        {

            string applicantIds = GetSelectedCandidates(); 
            if (applicantIds != string.Empty)
            {
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Dashboard/" + UrlConstants.Dashboard.NEWMAIL, "",  UrlConstants.PARAM_PAGE_FROM, "CandidateList", UrlConstants.PARAM_SELECTED_IDS, applicantIds);
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "NewMail", "<script>btnMewMail_Click('" + url + "');</script>", false);
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate to email.", true);
            }
            txtSelectedIds .Text ="";
            foreach (ListViewDataItem item in lsvSearchResult.Items)
            {
                CheckBox chkCandidate = (CheckBox)item.FindControl("chkCandidate");
                chkCandidate.Checked = false;
                   
            }

        }

        #endregion

        #region ObjectDataSource
        protected void odsCandidateSearch_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            odsCandidateSearch.SortParameterName = "sortExpression";

            if (_getCandidateCount)
            {
                hfTotalResult.Value = e.ReturnValue.ToString();
                   if (_watch != null)
                    {
                        _watch.Stop();
                        ShowResultSummary();
                    }
            }
            else
            {
                hfTotalResult.Value = "0";
            }
        }

        protected void odsCandidateSearch_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            _getCandidateCount = e.ExecutingSelectCount;
            if (e.ExecutingSelectCount) divShowSearchStatus.Visible = true;                                         
        }
        #endregion

        public string ReplaceKeyWords(Match m)
        {
            return "<span class=highlight>" + m.Value + "</span>";
        }

        void ForHighlightString()
        {
            StringBuilder SearchString = new StringBuilder();
            //CurrentCompany
            if (txtEmpOrCom.Text.Trim().ToLower() != string.Empty)
                SearchString.Append("CurrentCompany - " + txtEmpOrCom.Text.Trim() + ",");

            //Work Status
            foreach (ListItem item in uclWorkStatus .ListItem .Items )
            {
                if (item.Selected)
                    SearchString.Append("WorkStatus - " + item.Value + ",");
            }
            //Binding Availability
            if (wdcAvailability.Text != string.Empty)
                SearchString.Append("Availability - True ,");


            //CurrentSalary
            if (txtSalaryRangeFrom.Text.Trim() != string.Empty)
                SearchString.Append("RangeFrom - " + txtSalaryRangeFrom.Text.Trim() + ",");

            //ExpectedSalary
            if (txtSalaryRangeTo.Text != string.Empty)
                SearchString.Append("RangeTo - " + txtSalaryRangeTo.Text.Trim() + ",");

            //BindingLast updated Date
            if (ddlResumeLastUpdate.SelectedIndex > 0)// && (Convert .ToDateTime ( DateTime .Now .AddDays (-Convert .ToDouble (ddlApplicantType .SelectedValue )).ToShortDateString ())<=Convert .ToDateTime ( candidate .UpdateDate .ToShortDateString ())) )
            {
                SearchString.Append("LastUpdate - True,");
            }

            //Remarks
            if (txtAllKeywords.Text != string.Empty)
            {
                SearchString.Append("Remarks - " + txtAllKeywords.Text.Trim() + ",");
            }

            //Candidate Type
            //if (ddlApplicantType.SelectedIndex > 0)
              //  SearchString.Append("CandidateType - " + ddlApplicantType.SelectedValue + ",");

            ////Binding Work Schedule
            if (ddlWorkSchedule.SelectedIndex > 0)// && Convert.ToInt32(ddlWorkSchedule.SelectedValue) == candidate.WorkScheduleLookupId)
                SearchString.Append("WorkSchedule - " + Convert.ToInt32(ddlWorkSchedule.SelectedValue )+ ",");


            //Binding Highest Degree
            foreach (ListItem item in uclEducation .ListItem .Items )
            {
                if (item.Selected)
                {
                    SearchString.Append("HighestDegree - " + item.Text + ",");
                }
            }
            string strSearch = Guid.NewGuid().ToString();
            Session[strSearch] = SearchString.ToString();
            hdnHighlightName.Text= strSearch;
        }
    }
}
