﻿<%--
----------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewPanel.aspx
    Description: Interview Panel
    Created By:  Pravin khot
    Created On:  19/nov/2015
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date            Author            Modification
    -------------------------------------------------------------------------------------------------------------------------------------------------
                    
   -------------------------------------------------------------------------------------------------------------------------------------------------------
--%> 
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OnLineParsingFileToFile.ascx.cs" Inherits="TPS360.Web.UI.ControlsOnLineParsingFileToFile" %>

<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<link href="../assets/css/chosen.css" rel="Stylesheet" />
<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.0.min.js"></script>
 <style>
    .TableFormLeble
    {
        width: 41%;
    }
    .EmailDiv
    {
        vertical-align: middle;
        text-align: right;
        border: solid 9px #CCC;
        padding-bottom: 2px;
        padding-top: 2px;
        margin-right: 50%;
         margin-left: 50%;
        margin-bottom: 4px;
        width: 300px;
        float: center;
        background-image: -moz-linear-gradient(center top , #FFFFFF 20%, #F6F6F6 50%, #EEEEEE 52%, #F4F4F4 100%);
        border-radius: 3px;
    }
</style>

<script language="javascript" type="text/javascript">

    /////////////////////////////////////
    function ValidateFileUpload(Source, args) {
        
        var fuData = document.getElementById('<%= fuSelectCV.ClientID %>');
        var FileUploadPath = fuData.value;

        if (FileUploadPath == '') {
            // There is no file selected
        }
        else {
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

            if (Extension == "pdf" || Extension == "docx" || Extension == "doc" || Extension == "rtf" || Extension == "txt" ) {
                args.IsValid = true; // Valid file type
            }
            else {
                args.IsValid = false; // Not valid file type
            }
        }
    }

    function ValidateHotList(source, args) {
        validator = document.getElementById(source.id);
        var hdnField = document.getElementById('ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS')      

            if (hdnField.value != '') args.IsValid = true;
            else {
                validator.innerHTML = "<div>Please select candidate.<div>";
                ShowDiv('divHotListdiv'); n = 0;
                args.IsValid = false;
            }    
    }

    function monitorClick(e) {
        try {
            var evt = (e) ? e : event;

            var theElem = (evt.srcElement) ? evt.srcElement : evt.target;

            while (theElem != null) {
                if ($('#' + theElem.id).parents('.btn-group') == null || theElem.id == '' || theElem.id.indexOf('updPanelCandidateList') >= 0) {

                    n = 10;
                    var divReqs = document.getElementById('divReqs');
                    if (divReqs != null) {
                        $(divReqs).closest('.btn-group').removeClass('open');
                        divReqs.style.display = "none";
                    }
                    var divHotListdiv = document.getElementById('divHotListdiv');
                    if (divHotListdiv != null) {
                        $(divHotListdiv).closest('.btn-group').removeClass('open');
                        divHotListdiv.style.display = "none";
                    }
                    return true;
                }
                else {
                    n = 10;
                    return true;
                }

                theElem = theElem.offsetParent;
            }
        }
        catch (e) {
        } n = 10;
        return true;
    }
    document.onclick = monitorClick;

    function ShowDiv(divId) {
        n = 10;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function DropdownClicked(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function DropdownChange(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function ShowReqDiv(divId) {
        $('.btn-group').removeClass('open');
        var a = document.getElementById("divReqs");
        var b = document.getElementById("divHotListdiv");
        if (a != null) a.style.display = "none";
        if (b != null) b.style.display = "none";

        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) {
            $(reqDiv).closest('.btn-group').addClass('open');
            reqDiv.style.display = "";
            reqDiv.parentElement.className = "btn-group open";
        } return false;
    }
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
    function PageLoadedReq(sender, args) {      
        try {
            var chkBox = document.getElementById('chkAllItem');
            if (chkBox != null) chkBox.disabled = false;
            CheckUnCheckGridViewHeaderCheckbox('tlbTemplate', 'chkAllItem', 0, 1);
        }
        catch (e) {
        }
    }
    function CLCheckUnCheckAllRowLevelCheckboxes(cntrlHeaderCheckbox, posCheckbox) {

        var cntrlGridView = document.getElementById("tlbTemplate");      
        var chkBoxAll = document.getElementById(cntrlHeaderCheckbox);     
        var hndID = "";
        var chkBox = "";
        var checked = 0;
        var rowNum = 0;

        if (cntrlHeaderCheckbox != null && cntrlHeaderCheckbox.checked != null) {
            var rowLength = 0;
            rowLength = cntrlGridView.rows.length;           
            for (var i = 1; i < rowLength; i++) {
                var myrow = cntrlGridView.rows[i];
                
                var mycel = myrow.getElementsByTagName("td")[posCheckbox];

                if (mycel.childNodes[0].value == "on") {
                    chkBox = mycel.childNodes[0];
                }
                else {
                    chkBox = mycel.childNodes[1];
                }
                
                try {
                    if (mycel.childNodes[2].value == undefined) {
                        hndID = mycel.childNodes[3];
                    }
                    else {
                        hndID = mycel.childNodes[2];
                    }
                }
                catch (e) {
                }

                if (chkBox != null) {
                    if (chkBox.checked == null) {
                        chkBox = chkBox.nextSibling;
                    }
                    if (chkBox != null) {
                        if (chkBox.checked != null) {
                            chkBox.checked = cntrlHeaderCheckbox.checked;
                            rowNum++;
                        }
                    }
                }
                if (chkBox != null) {
                    if (chkBox.checked != null && chkBox.checked == true) {
                        if (chkBox.id.indexOf('chkItemCandidate') > -1) {
                            if (hndID != null) ADDID(hndID.value, 'ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS');
                            checked++;                          
                        }
                    }  
                    else {
                        if (chkBox.id.indexOf('chkItemCandidate') > -1) {                           
                            if (hndID != null) RemoveID(hndID.value, 'ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS');
                        }
                    }
                   
                }
            }
        }

        if (checked == rowNum) {
            if (chkBoxAll != null) {
                chkBoxAll.checked = true;
            }
        }
        else {
            if (chkBoxAll != null) {
                chkBoxAll.checked = false;
            }
        }
    }
    function AssignValue(optionvalue) {
        var selectedvalue = document.getElementById('<%=txtSelectValue.ClientID %>');
        selectedvalue.value = optionvalue
    }

    function ReAssign() {
        var selectedvalue = document.getElementById('<%=txtSelectValue.ClientID %>');
        selectedvalue.value = "NoSelect";
    }

    function checkEnter(e) {
        var kC = window.event ? event.keyCode :
            e && e.keyCode ? e.keyCode :
            e && e.which ? e.which : null;
        if (kC == 13) {
            var selectedvalue = document.getElementById('<%=txtSelectValue.ClientID %>');
            if (selectedvalue.value == "Hotlist") {
                var btnclick = document.getElementById('ctl00_cphHomeMaster_ucntrlCandidateList_btnAddToHotList');
                btnclick.focus();
            }
            else if (selectedvalue.value == "Requisition") {
            var btnclick = document.getElementById('ctl00_cphHomeMaster_ucntrlCandidateList_btnAddToPSList');
                btnclick.focus();
            }
        }
    }

$(document).click(function(event) {
   $('#showresult:visible').hide();
});

</script>
 <div> 
 <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
  <div style="display: none">
        <asp:TextBox ID="txtSelectValue" Text="NoSelect" runat="server" />
  </div>
  
  <asp:ObjectDataSource ID="odsOnLineParserList" runat="server" 
           SelectMethod="GetAllOnLineParser"  EnablePaging="True"
            TypeName="TPS360.Web.UI.OnLineParsingDataSource" SelectCountMethod="GetCountAllOnLineParser"
            SortParameterName="sortExpression">                            
           <SelectParameters>    
                      <asp:Parameter Name="memberId" DefaultValue="0"/>
                      <asp:Parameter Name="SortOrder" DefaultValue="asc" />
                      <asp:Parameter Name="SortColumn" DefaultValue="" />
             </SelectParameters>             
  </asp:ObjectDataSource>   
        
<asp:UpdatePanel ID="updPanelCandidateList" runat="server">  
    <ContentTemplate>           
             <asp:HiddenField ID="hdnSelectedIDS" runat="server" />
              <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>             
     <asp:UpdatePanel ID="pnlBulkImport" runat="server">
      <ContentTemplate>
       <div class="TableRow" style="padding-top: 2em">
        <div class="TableFormLeble">
            Select File :</div>
         <div class="TableFormContent">
            <asp:UpdatePanel ID="upDescriptionupload" runat="server">             
                <ContentTemplate>
                <div style="text-align: left">
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </div>
                    <asp:FileUpload ID="fuSelectCV" runat="server" CssClass="CommonButton" AllowMultiple="True"  Width="260px" TabIndex="1" />
                    <asp:Button ID="btnExport" Text="Upload" runat="server" CssClass="CommonButton" OnClick="btnExport_Click" ValidationGroup="UploadDocumentDescriotion" TabIndex="2" />
                    
                  <asp:CustomValidator ID="cvfileUpload" runat="server" ControlToValidate="fuSelectCV" ClientValidationFunction="ValidateFileUpload"
                        ErrorMessage="Please select resume file." ValidationGroup="UploadDocumentDescriotion" ></asp:CustomValidator>
                        
                    <%-- <asp:CustomValidator ID="cvfileDimension" runat="server" ControlToValidate="fuSelectCV" ClientValidationFunction="ValidateFileSize"
                        ErrorMessage="Please select PNG image file of size 1305 x 216."></asp:CustomValidator>--%>
                           
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExport" />
                </Triggers> 
            </asp:UpdatePanel>        
        </div>
    </div>
      </ContentTemplate>
    </asp:UpdatePanel>

     
        <br />
     <div id="showresult" runat="server" class="EmailDiv" style="width:180px"  Visible="false" >
            <table>
              <tr>
                <td><asp:Label ID="Label2"  Text="Total Parse" runat="server"></asp:Label></td>
              </tr>
              <tr>
                <td> <asp:Label ID="Label3"  Text="Total Success" runat="server"></asp:Label></td>
              </tr>
               <tr>    
                <td> <asp:Label ID="Label4"  Text="Total Failed" runat="server"></asp:Label></td>         
               </tr>
               <tr>    
                   <td> 
                        <asp:Button ID="btnclose"  CssClass="btn btn-small"
                                    runat="server" Text="Close" />                   
                   </td>           
               </tr>
            </table>        
       
     </div>     
   
    <div class="TabPanelHeader">
        List of Files
    </div>    
        <div id="divBulkActions" class="btn-toolbar" runat="server">
                <div class="pull-left">  
                    <div class="btn-group">
                        <asp:LinkButton ID="btnParse" runat="server" Text="Parse" OnClick="btnParse_Click"
                            CssClass="btn btn-medium" />
                    </div>               
                    <div class="btn-group">
                        <asp:LinkButton ID="btnParseToDatabase" runat="server" Text="Add To Database" OnClick="btnParseToDatabase_Click"
                            CssClass="btn btn-medium" />
                    </div>
                    <div class="btn-group" runat="server" id="liRemove">
                        <asp:LinkButton ID="btnRemove" runat="server" Text="Delete" OnClick="btnRemove_Click"
                            CssClass="btn btn-danger" />
                    </div>
                    <div >
                       <asp:CheckBox ID="ChkUpdateDuplicate"   Text="Update Duplicates" runat="server">
                        </asp:CheckBox>
                    </div>
            </div>                                                        
       </div>
    <br />
      <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>  
    
      <asp:HiddenField ID="hdnTmpFolder" runat="server" />
   
         <div class="GridContainer" style="text-align: left;"> 
            
          <asp:ListView ID="lsvOnLineParserList" runat="server" 
            DataSourceID="odsOnLineParserList" DataKeyNames="Id" OnItemDataBound="lsvOnLineParserList_ItemDataBound" 
                onPreRender="lsvOnLineParserList_PreRender" EnableViewState="true" 
            onitemcommand="lsvOnLineParserList_ItemCommand" >
            <LayoutTemplate>
                <table id="tlbTemplate"  class="Grid" cellspacing="0" border="0">
                    <tr>
                         <th style="width: 10px !important; white-space: nowrap;">                                 
                               <input id="chkAllItem" name="chkAllItem" type="checkbox" onclick="CLCheckUnCheckAllRowLevelCheckboxes(this,0)"
                                 disabled="disabled" />
                           </th>                    
                        <th style="white-space: nowrap;width :205px !important  ">
                               File Name                         
                        </th>                 
                        <th style="white-space: nowrap;width :80px !important  ">
                               Parse Status                        
                        </th>
                        <th style="white-space: nowrap;width :80px !important  ">
                               Database Status                        
                        </th>
                         <th style="white-space: nowrap;width :205px !important  ">
                               Candidate Name                       
                        </th>
                        <th style="text-align: center;  " runat ="server" id="thAction"  width="30px" visible="True">Action</th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                    <tr class="Pager">
                        <td id="tdpager" runat="server" colspan="6">
                            <ucl:Pager ID="pagerControl_Requisition" runat="server" EnableViewState="true" />
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
             <EmptyDataTemplate>
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning"  enableviewstate ="false" >
                    <tr>
                        <td>
                            No data was returned.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
                <itemtemplate>
                <tr id="row"  runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                       <td>
                           <asp:CheckBox runat="server" ID="chkItemCandidate" OnClick="CheckUnCheckGridViewHeaderCheckbox('tlbTemplate','chkAllItem',0,1)" />
                           <asp:HiddenField ID="hdnID" runat="server" />
                       </td>
                      <td>
                        <asp:Label ID="lblfilename" runat="server"  />                        
                    </td>                   
                    <td>
                        <asp:Label ID="lblparsestatus" runat="server"  />
                    </td>   
                     <td>
                        <asp:Label ID="lbldbstatus" runat="server"  />                        
                    </td>                                     
                    <td>
                       <asp:HyperLink ID="lnkCandidateName"  runat="server"></asp:HyperLink>
                    </td>                                  
                    <td style="text-align: center;" runat ="server" id ="tdAction">                    
                        <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" Visible="True"
                            CommandName="DeleteItem" ></asp:ImageButton>
                    </td>
                </tr>
            </itemtemplate>
            </asp:ListView>
            </div>
        </ContentTemplate>  
      </asp:UpdatePanel>              
 </div> 


