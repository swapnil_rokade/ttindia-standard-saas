﻿using System;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using TPS360.Common;
using System.Web.UI;
using System.Linq;

namespace TPS360.Web.UI
{
    public partial class ControlLookUpList : BaseControl
    {
        #region Member Variables

        #endregion

        #region Methods

        private void BindList()
        {
            Type enumType = typeof(LookupType);            

            string[] names = Enum.GetNames(enumType);

            IList<Pair> arrayList = new List<Pair>();

            if ((names != null) && (names.Length > 0))
            {
                foreach (string name in names)
                {
                    int value = (int)Enum.Parse(enumType, name);
                    string strDescription = EnumHelper.GetDescription((Enum)Enum.Parse(enumType, name));

                    if (value > 0)
                    {
                        arrayList.Add(new Pair { First = value.ToString(), Second = strDescription });
                    }
                }
            }

            arrayList = arrayList.OrderBy(t => t.Second).ToList();

            rptLookUpList.DataSource = arrayList;
            rptLookUpList.DataBind();
        }

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];

                ControlHelper.SetHyperLink(lnkAddLookup, UrlConstants.Admin.LOOKUP_EDITOR_PAGE, string.Empty, "[ Add Lookup ]");

                if (!StringHelper.IsBlank(message))
                {
                    MiscUtil.ShowMessage(lblMessage, message, false);
                }

                BindList();
            }
        }

        #endregion

        #region Repeater Events

        protected void rptSubLookUpList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            bool _IsAdmin = base.IsUserAdmin;
            
            if (ControlHelper.IsItemDataRow(e.Item.ItemType))
            {
                GenericLookup genericLookup = e.Item.DataItem as GenericLookup;

                if (genericLookup != null)
                {
                    Label lblName = (Label)e.Item.FindControl("lblName");
                    Label lblCreateDate = (Label)e.Item.FindControl("lblCreateDate");
                    Label lblUpdateDate = (Label)e.Item.FindControl("lblUpdateDate");

                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    lblName.Text = genericLookup.Name;
                    //lblCreateDate.Text = genericLookup.CreateDate.ToString();
                    //lblUpdateDate.Text = genericLookup.UpdateDate.ToString();

                    btnDelete.OnClientClick = "return ConfirmDelete('lookup')";

                    btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(genericLookup.Id);
                    //btnDelete.Visible = _IsAdmin;
                }
            }
        }

        protected void rptLookUpList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (ControlHelper.IsItemDataRow(e.Item.ItemType))
            {
                Pair pair = e.Item.DataItem as Pair;
                int typeId = Convert.ToInt32(pair.First);
                string strDescription = pair.Second.ToString();

                HyperLink lnkTableName = ((HyperLink)e.Item.FindControl("lnkTableName"));
                Label lblSubListTotal = (Label)e.Item.FindControl("lblSubListTotal");
                
                lnkTableName.Text = strDescription;

                Repeater rptSubLookUpList = ((Repeater)e.Item.FindControl("rptSubLookUpList"));

                IList<GenericLookup> gnericLookupList = Facade.GetAllGenericLookupByLookupType((LookupType)typeId);
                
                int total = 0;

                if(gnericLookupList != null && gnericLookupList.Count > 0)
                {
                    total = gnericLookupList.Count;
                }

                if (total == 0)
                    e.Item.Visible = false;
                lblSubListTotal.Text = "(" + total.ToString() + ")";

                rptSubLookUpList.DataSource = gnericLookupList;
                rptSubLookUpList.DataBind();

            }
        }

        protected void rptSubLookUpList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    Helper.Url.Redirect(UrlConstants.Admin.LOOKUP_EDITOR_PAGE, string.Empty, UrlConstants.PARAM_ID, StringHelper.Convert(id));
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteGenericLookupById(id))
                        {
                            BindList();
                            MiscUtil.ShowMessage(lblMessage, "Lookup has been successfully deleted.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }

        #endregion

        #endregion
    }
}