﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmployeeList.ascx
    Description: This is the user control page used to display Employee list.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Oct-13-2008          Jagadish N            Defect id: 8987; "Edit" icon in "Action" column is removed.
    0.2              Jan-21-2009          Gopala Swamy          Enhancement Id(Kaizon):9242  Put Link button to "Position,Access Role,Primary Phone,Mobile,Location,Status"
    0.3             Jan -20-2010         Basavaraj Angadi    Defect id:12901 ; Changed the style Attribute
    0.4              23/Feb/2016         pravin khot           added access condition by - <asp:ListItem Text ="In-Active" Value ="3">
    0.5              10/March/2016       pravin khot            DropDownList ddlStatus added two property.
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmployeeList.ascx.cs"
    Inherits="TPS360.Web.UI.cltEmployeeList" %>

   <%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>    
        <script language="javascript" src="../js/AjaxScript.js" type="text/javascript">
        
function ELCheckUnCheckAllRowLevelCheckboxes(cntrlHeaderCheckbox,posCheckbox) {
   
var cntrlGridView = document.getElementById("tlbTemplate");
if(cntrlHeaderCheckbox!=null&&cntrlHeaderCheckbox.checked!=null)
{
var rowLength=cntrlGridView.rows.length;
for(var i=1;i<rowLength;i++)
{
var myrow=cntrlGridView.rows[i];
var mycel=myrow.getElementsByTagName("td")[posCheckbox];
var chkBox=mycel.childNodes[0];
if(chkBox!=null)
{
if(chkBox.checked==null)
{
chkBox=chkBox.nextSibling;}
if(chkBox!=null)
{
if(chkBox.checked!=null)
{
    chkBox.checked = cntrlHeaderCheckbox.checked;
} 
} 
} 
} 
} 
}
    </script>
    <asp:TextBox ID="txtSortColumn" runat ="server" Visible ="false"  ></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat ="server" Visible ="false"  ></asp:TextBox>
<asp:TextBox ID ="SortColumn" runat ="server"  EnableViewState ="true"  Visible ="false" />
<asp:TextBox ID ="SortOrder" runat ="server"  EnableViewState ="true" Visible ="false"  />
<div>
    <asp:ObjectDataSource ID="odsEmployeeList" runat="server" SelectMethod="GetPaged"
        TypeName="TPS360.Web.UI.EmployeeDataSource" SelectCountMethod="GetListCount"
        EnablePaging="True" SortParameterName="sortExpression">
        <SelectParameters>
            <asp:Parameter Name="managerId" DefaultValue="0" />
             <asp:Parameter Name="organizationFunctionalityCategoryMapId" DefaultValue="0" />
             <asp:Parameter Name="tierLookupId" DefaultValue="0" />
             <asp:Parameter Name="branchOfficeId" DefaultValue="0" />
            <asp:Parameter Name="status" DefaultValue="0" />
            <asp:Parameter Name ="SortOrder" DefaultValue ="asc" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:UpdatePanel ID="updPanelEmployeeList" runat="server">
        <ContentTemplate>
            <div style="text-align: center;">
                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
            </div>
            <div class="CommonHeader" style="display:none;">
                <asp:Label ID="lblHeader" runat="server" EnableViewState="false" Text="List of Employees"></asp:Label>
            </div>
          <%-- <div class="TableRow" style="text-align: right;margin-bottom:3px;">
                <asp:button ID="btnUpdate" CssClass="btn" runat="server"  Text="Update Status" onclick="btnUpdate_Click"/>
            </div> --%>
            <div class="GridContainer">
                <asp:ListView ID="lsvEmployeeList" runat="server"
                    DataKeyNames="Id" OnItemDataBound="lsvEmployeeList_ItemDataBound" OnItemCommand="lsvEmployeeList_ItemCommand" OnPreRender ="lsvEmployeeList_PreRender">
                    <LayoutTemplate>
                        <table id="tlbTemplate" class="Grid" cellspacing="0" border="0">
                            <tr runat ="server" id ="trEmployee">
                              <%--  <th style="width: 25px !important; white-space: nowrap;">
                                    <input id="chkAllItem" name="chkAllItem" type="checkbox" onclick="ELCheckUnCheckAllRowLevelCheckboxes(this,0)" />
                                </th> --%>
                              
                                <th style=" white-space: nowrap;">
                                    <asp:LinkButton ID="btnLookupName" runat="server" ToolTip="Sort By Name" CommandName="Sort"
                                        CommandArgument="[E].[FirstName]" Text="Name" />
                                </th>
                              
                                <th style=" white-space: nowrap;">
                                      <asp:LinkButton ID="btnSystemAccess" runat="server" CommandName="Sort"  ToolTip="Sort By Access Role" CommandArgument="[CR].[Name]"
                                        Text="Access Role" />                              
                                </th>
                                <th style="white-space: nowrap;">
                                    <asp:LinkButton ID="btnUpdateDate" runat="server" CommandName="Sort" ToolTip="Sort By Email" CommandArgument="[E].[PrimaryEmail]"
                                        Text="Email" />
                                </th>
                                <th style=" white-space: nowrap;">
                                    <asp:LinkButton ID="lnkReportingTo" runat="server" CommandName="Sort"  ToolTip="Sort By Reporting To" CommandArgument="[EE].[FirstName]" 
                                        Text="Reporting To" />                               
                                </th>
                                <th style="white-space: nowrap; width : 90px !important">
                                  
                                    <asp:LinkButton ID="btnStatus" runat="server" CommandName="Sort"  ToolTip="Sort By Status" CommandArgument="[E].[Status]"
                                        Text="Access" />                             
                                </th>
                                <th style="text-align: center; width: 55px !important;" runat="server" id="hdrAction">
                                    Action 
                                </th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="6" runat ="server" id ="tdPager">
                                    <ucl:Pager Id="pagerControl" runat ="server" EnableViewState ="true"  />
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No User available.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate >
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                          <%--   <td>
                                <asp:CheckBox runat="server" ID="chkItem" onclick="CheckUnCheckGridViewHeaderCheckbox('tlbTemplate','chkAllItem',0)" />
                            </td> --%>
                          
                            <td>
                                <asp:HyperLink ID="lnkEmployeeName"  runat="server"></asp:HyperLink>
                            </td>
                            
                            <td>
                                <asp:Label ID="lblSystemAccess"  runat="server" Width ="90px" />
                            </td>
                            <td style="white-space: nowrap;">
                                <asp:LinkButton ID="lblEmail" runat="server" />
                            </td>
                           <td>
                                <asp:Label ID="lblReportingTo"  runat="server" Width ="80px" />
                            </td>
                            <td style="white-space: nowrap;">
                               <%--********************Code modify and property added by pravin khot on 10/March/2016***************--%>
                                  <%--<asp:DropDownList CssClass="DropDownList" runat="server" ID="ddlStatus">--%>
                                  <asp:DropDownList CssClass="DropDownList" runat="server" ID="ddlStatus" AutoPostBack="True" onselectedindexchanged="ddlStatus_SelectedIndexChanged" >
                                <%--**********************************END************************************    --%>
                                    <asp:ListItem Text ="Enabled" Value ="1"></asp:ListItem>
                                    <asp:ListItem Text ="Blocked" Value ="2"></asp:ListItem>
                                    <asp:ListItem Text ="In-Active" Value ="3"></asp:ListItem> <%--this line added by pravin khot on 23/Feb/2016--%>
                                </asp:DropDownList>
                            </td>
                            <td style="text-align: center; white-space:nowrap;" runat="server" id="CntAction">
                                <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton"  runat="server" CommandName="DeleteItem"
                                    Visible="true"></asp:ImageButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
          
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
