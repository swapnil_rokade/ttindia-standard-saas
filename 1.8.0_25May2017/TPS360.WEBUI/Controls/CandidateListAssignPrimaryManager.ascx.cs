﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:ControlCandidateListAssignPrimaryManager.ascx.cs
    Description: This is the user control page used to disply the list of candidate in grid
    Created By: pravin khot
    Created On: 26/May/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------       

>*/


using System;
using System.Web.UI.WebControls;
using System.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Collections.Generic;
using System.Web.UI;
using System.Collections;
using System.Globalization;
using System.Net.Mail;
using System.Net;
using System.Threading;
using System.Text;

namespace TPS360.Web.UI
{
    public partial class ControlCandidateListAssignPrimaryManager : BaseControl
    {
        #region Member Variables

        private static bool _mailsetting = false;
        private static bool _isMyList = false;
        private bool _IsAccessToEditCandidate = false;
        private static bool _isAddToMyList = false;
        private bool _IsAccessToEmployee = true;
        private static string UrlForEmployee = string.Empty;
        private static int IdForSitemap = 0;
        private string  currentSiteMapId = "0";
        #endregion

        #region Properties
        public String HeaderTitle
        {
            set { }
        }

        public bool IsMyList
        {
            set { _isMyList = value; }
        }

        public int WorkFlowId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_WorkFlowId"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_WorkFlowId"] = value;
            }
        }

        public bool IsSendAlertByEmail
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_IsSendAlertByEmail"] ?? false);
            }
            set
            {
                ViewState[this.ClientID + "_IsSendAlertByEmail"] = value;
            }
        }       

        #endregion

        #region Methods


        private void LoadWorkFlowSetting()
        {
            ApplicationWorkflow applicationWorkflow = Facade.GetApplicationWorkflowByTitle(WorkFlowTitle.ATSCandidatesAppearHotListToMyCandidateList.ToString());
            if (applicationWorkflow != null && applicationWorkflow.RequireApproval)
            {
                _isAddToMyList = true;
            }
            else
            {
                _isAddToMyList = false;
            }

            applicationWorkflow = Facade.GetApplicationWorkflowByTitle(WorkFlowTitle.ATSShouldAssignedManagersForCandidateAlertedWhenCandidateAddedHotList.ToString());
            if (applicationWorkflow != null && applicationWorkflow.RequireApproval && applicationWorkflow.IsEmailAlert)
            {
                IsSendAlertByEmail = true;
                WorkFlowId = applicationWorkflow.Id;
            }
            else
            {
                IsSendAlertByEmail = false;
            }
        } 

        private void BindList()
        {
            if (_isMyList)
            {
                odsCandidateList.SelectParameters["memberId"].DefaultValue = base.CurrentMember.Id.ToString();
                if (_isAddToMyList)
                {
                    odsCandidateList.SelectParameters["hotListManagerId"].DefaultValue = base.CurrentMember.Id.ToString();
                }
             
            }
            else
            {
                odsCandidateList.SelectParameters["memberId"].DefaultValue = "0";
            }
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "[C].[CandidateName]";
            }
            odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            odsCandidateList.SelectParameters["SortColumn"].DefaultValue = SortColumn.Text.ToString();
            string pagesize = "";
            if (_isMyList)
            {
                pagesize = (Request.Cookies["MyCandidateListRowPerPage"] == null ? "" : Request.Cookies["MyCandidateListRowPerPage"].Value); ;
            }
            else
            {
                pagesize = (Request.Cookies["MasterCandidateListRowPerPage"] == null ? "" : Request.Cookies["MasterCandidateListRowPerPage"].Value); ;
            }
            lsvCandidateList.DataSourceID = "odsCandidateList";
            this.lsvCandidateList.DataBind();
             ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
             if (PagerControl != null)
             {
                 DataPager pager = (DataPager)PagerControl.FindControl("pager");
                 if (pager != null)
                     pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
             }
        }

        private void LoadddlAssignManagerList()
        {
            TxtAssignPrimaryManager.Visible = false;
            ddlAssignManager.Visible = true;
            MiscUtil.PopulateMemberListByRole(ddlAssignManager, ContextConstants.ROLE_EMPLOYEE, Facade);
            ddlAssignManager.Items.RemoveAt(0);
            ListItem item = new ListItem();
            item.Value = "0";
            item.Text = "Select primary manager";
            ddlAssignManager.Items.Insert(0, item);
        }      

        #endregion

        #region Events

        #region Page Events
        private void PopulateControls()
        {
           // MiscUtil.PopulateCandidateType(ddlCandidateType, Facade);

            ddlSource.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.SourceType);
            ddlSource.DataValueField = "Id";
            ddlSource.DataTextField = "Name";
            ddlSource.DataBind();
            ddlSource.Items.Insert(0, new ListItem("Any", "0"));

            IList<HiringMatrixLevels> level = Facade.GetAllHiringMatrixLevels();
            ddlHiringStatus.DataSource = level;
            ddlHiringStatus.DataTextField = "Name";
            ddlHiringStatus.DataValueField = "Id";
            ListItem item = new ListItem();
            item.Value = "0";
            item.Text = "Please Select";
            ddlHiringStatus.DataBind();
            ddlHiringStatus.Items.Insert(0, item);
            ddlHiringStatus = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlHiringStatus);
            MiscUtil.PopulateMemberListByRole(ddlAssignedManager , ContextConstants.ROLE_EMPLOYEE, Facade);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            currentSiteMapId = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
            //liRemove.Visible = base.IsUserAdmin;
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            ddlAssignedManager.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlHiringStatus.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            //ddlHotlist.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToEmployee = false;
            else
            {
                
                IdForSitemap = CustomMap.Id; 
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }
            if (_isMyList)
            {
                //uclComReq.ISMyList = _isMyList;
                hdnCurrentMemberId.Value = base.CurrentMember.Id.ToString();
                //hot_ListAutoComplete.ContextKey = base.CurrentMember.Id.ToString();
            }
            else
            {
                hdnCurrentMemberId.Value = "0";
                //hot_ListAutoComplete.ContextKey = "0";
            }
            
            _mailsetting = MiscUtil.IsValidMailSetting(Facade ,CurrentMember .Id );
            ArrayList AList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            if (AList.Contains(362)) _IsAccessToEditCandidate = true;
            if (!IsPostBack)
            {

                PopulateControls();     
                LoadWorkFlowSetting();
                BindList();
                if (Request.Cookies["NameFormat"] != null)
                    ControlHelper.SelectListByValue(ddlNameFormat, Request.Cookies["NameFormat"].Value);
                LinkButton btnName = (LinkButton)lsvCandidateList.FindControl("btnName");
                if (btnName != null)
                {
                    if (ddlNameFormat.SelectedValue == "1")
                    {
                        btnName.CommandArgument = "[C].[LastName]";
                        SortOrder.Text = "ASC";
                        odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                        odsCandidateList.SelectParameters["SortColumn"].DefaultValue = "[C].[LastName]";
                    }
                    else
                    {
                        btnName.CommandArgument = "[C].[CandidateName]";
                        SortOrder.Text = "ASC";
                        odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                        odsCandidateList.SelectParameters["SortColumn"].DefaultValue = "[C].[CandidateName]";
                    }

                }
                LoadddlAssignManagerList();
            }
           
            if (Session["msg"] != null)
            {
                MiscUtil.ShowMessage(lblMessage, Session["msg"].ToString(), false  );
                Session.Remove("msg");
            }
            if (Session["msgs"] != null)
            {
                MiscUtil.ShowMessage(lblMessage, Session["msgs"].ToString(), true );
                Session.Remove("msgs");
            }
            string prk = hdnSelectedIDS.Value;
        }

        //private int GetSelectedRequisitionID()
        //{
        //    //return uclComReq.SelectedRequisitionId;
        //}
        #endregion      

        private bool SendEmail(string strEmailId, string strSMTP, string strUser, string strPwd, int intPort, bool boolsslEnable)
        {
            bool strSendStatus = true;

            CultureInfo cultInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo txtInfo = cultInfo.TextInfo;

            string strSenderName = txtInfo.ToTitleCase(base.CurrentMember.FirstName + " " + base.CurrentMember.LastName);
            string[] strUserName = strEmailId.Split('<');

            MailMessage message = new MailMessage();
            MailAddress mailAddressFrom = new MailAddress(base.CurrentMember.PrimaryEmail);//1.8
            MailAddress mailAddressTo = new MailAddress(strEmailId);
            message.From = mailAddressFrom;
            message.To.Add(mailAddressTo);
            message.IsBodyHtml = true;

            string SelectedRequisitionName="";
            
            message.Subject = "NOTIFICATION: New candidates added to the requisition - " + SelectedRequisitionName;
            message.Body = "Dear " + txtInfo.ToTitleCase(strUserName[0]) + ", <br/><br/> New candidates are added to the requisition - " + SelectedRequisitionName + ". Please go to your <b>My Requisition List</b> to view the requisition details." + "<br/><br/>Regards,<br/>" + strSenderName;
            SmtpClient emailClient = new SmtpClient(strSMTP, intPort);
            NetworkCredential SMTPUserInfo = new NetworkCredential(strUser, strPwd);
            emailClient.EnableSsl = boolsslEnable;
            emailClient.UseDefaultCredentials = false;
            emailClient.Credentials = SMTPUserInfo;

            object userState = message;
            try
            {
                emailClient.SendAsync(message, userState);
                strSendStatus = false;
            }
            catch (Exception ex)
            {
                MiscUtil.ShowMessage(lblMessage, ex.Message.ToString(), true);
                strSendStatus = true;
            }

            return strSendStatus;
        }

        MemberHiringProcess memberHiringProcess;
        #region ButtonEvents

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            divBulkActions.Visible = true;
           
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            BindList();
            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            divBulkActions.Visible = true;            
            txtCandidateName.Text = "";
            txtCurrentPosition.Text = "";
            ddlAssignedManager.SelectedIndex = 0;
            ddlHiringStatus.SelectedIndex = 0;
            txtCity.Text = "";
            //ddlCandidateType.SelectedIndex = 0;
            ddlSource.SelectedIndex = 0;
            txtCandidateEmail.Text = "";
            txtMobilePhone .Text ="";
            BindList();
            uclCountryState.SelectedCountryId = 0;
            uclCountryState.SelectedStateId  = 0;
            txtCandidateID.Text = "";
        }
        protected void btnRemove_Click(object sender, EventArgs e)
        { 
            string applicantIds = hdnSelectedIDS.Value; 
            if (applicantIds != string.Empty)
            {
                uclConfirm.AddMessage("Are you sure that you want to delete this candidate(s)?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate to remove.", true  );
            }
        }
        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                string[] candidateIDs = hdnSelectedIDS.Value.Split(',');
                int noofCandidates = 0;
                foreach (string s in candidateIDs)
                {

                    int id = Convert.ToInt32(s);
                    Member member = Facade.GetMemberById(id);
                    if (member != null)
                    {
                        MembershipUser membershipUser = Membership.GetUser(member.UserId);

                        if (Facade.DeleteMemberById(id))
                        {
                            Roles.RemoveUserFromRole(membershipUser.UserName, ContextConstants.ROLE_CANDIDATE);
                            Membership.DeleteUser(membershipUser.UserName, true);
                            noofCandidates++;
                        }
                    }
                }
             
                BindList();
                hdnSelectedIDS.Value = "";
                MiscUtil.ShowMessage(lblMessage, "Successfully removed "+ noofCandidates  +" candidate(s).", false );
            }

        }
        protected void btnEmail_Click(object sender, EventArgs e)
        {
            string applicantIds = hdnSelectedIDS.Value; 
            if (applicantIds != string.Empty)
            {
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Dashboard/" + UrlConstants.Dashboard.NEWMAIL, "",  UrlConstants.PARAM_PAGE_FROM, "CandidateList", UrlConstants.PARAM_SELECTED_IDS, applicantIds);
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "NewMail", "<script>btnMewMail_Click('" + url + "');</script>", false);
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate to email.", true);
            }
            hdnSelectedIDS.Value = "";
            foreach (ListViewDataItem item in lsvCandidateList.Items)
            {
                CheckBox chkItemCandidate = (CheckBox)item.FindControl("chkItemCandidate");
                chkItemCandidate.Checked = false;
            }
        }
      

        protected void btnAssignPrimaryManager_Click(object sender, EventArgs e)
        {
            bool iscollapsed = cpeSearch.Collapsed;
            string strMessage = String.Empty;
            if (base.CurrentMember.Id != 0)
            {
                int AssignManagerId = 0;
                if (ddlAssignManager.Visible == true)
                {
                    AssignManagerId = Convert.ToInt32(ddlAssignManager.SelectedValue);
                }
                else
                {
                    if (TxtAssignPrimaryManager.Text != "")
                        AssignManagerId = Facade.GetMemberGroupByName(TxtAssignPrimaryManager.Text).Id;
                }
                string hotlist = string.Empty;
                hotlist = hdnSelectedIDS.Value;
                IList<string> ids = new List<string>();
                if (hotlist.Trim() != String.Empty)
                {
                    ids = hotlist.Split(',');
                }
                foreach (string id in ids)
                {
                    int candidateid = Int32.Parse(id);
                    MemberManager Manager = Facade.GetMemberManagerByMemberIdAndManagerId(candidateid, AssignManagerId);
                    if (Manager == null)
                    {
                        MemberManager newManager = new MemberManager();
                        newManager.CreatorId = base.CurrentMember.Id;
                        newManager.UpdatorId = base.CurrentMember.Id;
                        newManager.ManagerId = AssignManagerId;
                        newManager.MemberId = candidateid;
                        newManager.IsPrimaryManager = true;
                        Facade.AddMemberManager(newManager);
                       
                    }
                    else
                    {
                        //MemberManager currentManager = null;
                        //currentManager.IsPrimaryManager = true;
                        //currentManager.UpdatorId = base.CurrentMember.Id;
                        //Facade.UpdateMemberManager(currentManager);
                        //Facade.UpdateUpdateDateByMemberId(candidateid);
                    }
                    Facade.UpdateUpdateDateByMemberId(candidateid);
                    Facade.UpdateMemberManagerIsPrimary(candidateid, AssignManagerId);
                }
                foreach (ListViewItem lsvItm in lsvCandidateList.Items)
                {
                    if (ControlHelper.IsListItemDataRow(lsvItm.ItemType))
                    {
                        CheckBox chkItemCandidate = (CheckBox)lsvItm.FindControl("chkItemCandidate");
                        if (chkItemCandidate != null && chkItemCandidate.Checked)
                        {
                            chkItemCandidate.Checked = false;
                        }
                    }
                }
                MiscUtil.ShowMessage(lblMessage, "Assigned primary manager successfully for Selected candidate(s)", false);
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Member is not found.", false);
            }
        }

       
        #endregion

        private bool CheckSelectedList(int ID)
        {
            char[] del = { ',' };
            string[] arrID = hdnSelectedIDS.Value.Split(del, StringSplitOptions.RemoveEmptyEntries  );
            foreach (string s in arrID)
            {
                if (Convert.ToInt32(s) == ID)
                {
                    return true;
                }
            }
            return false;
        }

        #region ListView Events
        protected void ddlNameFormat_SelectedIndexChanged(object sender, EventArgs e)
        {

            Response.Cookies["NameFormat"].Value = ddlNameFormat .SelectedValue;
            LinkButton btnName = (LinkButton)lsvCandidateList.FindControl("btnName");
            if (ddlNameFormat.SelectedValue == "1")
            {
               SortColumn .Text = btnName.CommandArgument = "[C].[LastName]";
               SortOrder.Text = "ASC";
                odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                odsCandidateList.SelectParameters["SortColumn"].DefaultValue = SortColumn.Text.ToString();
            }
            else
            {
                //btnName.CommandArgument = "[C].[FirstName]";
                SortColumn.Text =  btnName.CommandArgument = "[C].[FirstName]";
                SortOrder.Text = "ASC";
                odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                odsCandidateList.SelectParameters["SortColumn"].DefaultValue = SortColumn.Text.ToString();
               // odsCandidateList.SelectParameters["sortExpression"].DefaultValue = "[C].[FirstName]";
            }
            btnSearch_Click(sender, e);
           
        }
        protected void lsvCandidateList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

            bool _IsAdmin = base.IsUserAdmin;
            
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Candidate candidatInfo = ((ListViewDataItem)e.Item).DataItem as Candidate;

                if (candidatInfo != null)
                {
                    Label lblID = (Label)e.Item.FindControl("lblID");
                    lblID.Text = "A" +(candidatInfo.Id).ToString();
                    HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
                    Label lblPosition = (Label)e.Item.FindControl("lblPosition");
                    Label lblLocation = (Label)e.Item.FindControl("lblLocation");
                    Label lblHomePhone = (Label)e.Item.FindControl("lblHomePhone");
                    Label lblMobilePhone = (Label)e.Item.FindControl("lblMobilePhone");
                    LinkButton lblEmail = (LinkButton)e.Item.FindControl("lblEmail");
                    Label lblRemarks = (Label)e.Item.FindControl("lblRemarks");

                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
                    HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");
                    hdnID.Value = candidatInfo.Id.ToString ();
                    CheckBox chkItemCandidate = (CheckBox)e.Item.FindControl("chkItemCandidate");
                    chkItemCandidate.Checked = CheckSelectedList(candidatInfo.Id);
                     string strFullName="";
                    if(ddlNameFormat .SelectedValue =="0")
                         strFullName = candidatInfo.FirstName + " " + candidatInfo.LastName;
                    else 
                        strFullName = candidatInfo.LastName  + ((candidatInfo .FirstName.Trim  ()!="" && candidatInfo .LastName.Trim () !="") ? ", ": " " ) + candidatInfo.FirstName ;
                    if (strFullName.Trim()=="")
                    {
                        strFullName = "No Candidate Name";
                    }
                    if (_IsAccessToEmployee)
                    {
                        if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                            ControlHelper.SetHyperLink(lnkCandidateName, UrlForEmployee, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(candidatInfo.Id), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentSiteMapId );
                    }
                    else lnkCandidateName.Text = strFullName;
                    lblPosition.Text = candidatInfo.CurrentPosition;

                    string location = candidatInfo.CurrentCity + ((!string.IsNullOrEmpty(candidatInfo.CurrentCity) && candidatInfo.StateName.Trim() != string.Empty) ? ", " : string.Empty) + candidatInfo.StateName.Trim();

                    lblLocation.Text = location;
                    //lblHomePhone.Text = candidatInfo.HomePhone;         
                    lblMobilePhone.Text = candidatInfo.CellPhone;
                    if (!_mailsetting)
                        lblEmail.Text = "<a href=MailTo:" + candidatInfo.PrimaryEmail + ">" + candidatInfo.PrimaryEmail + "</a>";
                    else
                    {
                        lblEmail.Text = candidatInfo.PrimaryEmail;
                        SecureUrl  url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_MEMBER_ID , candidatInfo .Id .ToString ());
                        //lblEmail.Attributes.Add("onclick", "window.open('" + url + "','NewMail','height=626,width=770,toolbar=no,directories=no,status=no,linemenubar=no,scrollbars=no,resizable=no,modal=no'); return false;");
                        lblEmail.Attributes.Add("onclick", "btnMewMail_Click('" + url + "')");
                    }
                    lblRemarks.Text = candidatInfo.Remarks;
                                                     

                    btnDelete.Visible = _IsAdmin;
                    btnDelete.OnClientClick = "return ConfirmDelete('candidate');";
                    btnDelete.CommandArgument = StringHelper.Convert(candidatInfo.Id);
                    System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvCandidateList.FindControl("tdPager");
                    if (_IsAdmin || _IsAccessToEditCandidate)
                    {
                        SecureUrl ur = UrlHelper.BuildSecureUrl(UrlConstants.ATS.ATS_INTERNALRESUMEBUILDER, string.Empty, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(candidatInfo.Id), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentSiteMapId, UrlConstants.PARAM_SITEMAP_ID, (IdForSitemap + 1).ToString());
                        btnEdit.OnClientClick = "window.location.replace('" + ur + "'); return false;";
                        //Response.Redirect(ur.ToString());
                        tdPager.ColSpan = 9;
                    }
                    else
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvCandidateList.FindControl("thAction");
                        System.Web.UI.HtmlControls.HtmlTableCell tdAction = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAction");
                        thAction.Visible = false;
                        tdAction.Visible = false;
                        tdPager.ColSpan = 8;
                    }
                }
            }
        }
        protected void lsvCandidateList_PreRender(object sender, EventArgs e)
        {
            divBulkActions.Visible = true;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                else
                {
                    
                    divBulkActions.Visible = false;
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null)
                {
                    if (_isMyList) hdnRowPerPageName.Value = "MyCandidateListRowPerPage";
                    else hdnRowPerPageName.Value = "MasterCandidateListRowPerPage";
                }
            }
            else
            {
                divBulkActions.Visible = false;
            }
            PlaceUpDownArrow();
        }
        protected void  lsvCandidateList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text  == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text  == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                    
                     if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                         SortOrder.Text = "asc";
                     else  SortOrder.Text = SortOrder.Text.ToLower () == "asc" ? "desc" : "asc";
                     SortColumn.Text = e.CommandArgument.ToString();

                   
                     odsCandidateList.SelectParameters["SortOrder"].DefaultValue  = SortOrder.Text.ToString();
                     odsCandidateList.SelectParameters["SortColumn"].DefaultValue = SortColumn.Text.ToString();
                     if (e.CommandArgument.ToString() == "[C].[CandidateName]")
                     {
                         odsCandidateList.SelectParameters["SortColumn"].DefaultValue = ddlNameFormat.SelectedIndex == 1 ? "[C].[LastName]" : "[C].[CandidateName]";
                     }
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                       
                            Member member = Facade.GetMemberById(id);
                            MembershipUser membershipUser = Membership.GetUser(member.UserId);
                        
                            if (Facade.DeleteMemberById(id))
                            {
                                Roles.RemoveUserFromRole(membershipUser.UserName, ContextConstants.ROLE_CANDIDATE);
                                Membership.DeleteUser(membershipUser.UserName, true);
                                
                                BindList();
                                MiscUtil.ShowMessage(lblMessage, "Candidate has been deleted successfully.", false);
                            }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }

        #endregion

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvCandidateList.FindControl(txtSortColumn .Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));


            }
            catch
            {
            }

        }

        #endregion
}
}
