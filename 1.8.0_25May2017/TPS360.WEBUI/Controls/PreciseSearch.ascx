﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:PreciseSearch.ascx
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             23/June/2016        pravin khot           added new Source,SourceDescription
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PreciseSearch.ascx.cs"
    Inherits="TPS360.Web.UI.ControlsPreciseSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
  <%@ Register Src ="~/Controls/CountryStatePicker.ascx" TagName ="CountryState" TagPrefix ="ucl" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>
<%@ Register Src ="~/Controls/CompanyRequisitionPicker.ascx" TagName ="CompReq" TagPrefix ="ucl" %>
<script src="../js/SearchCandidateSub.js" type="text/javascript"></script>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src ="~/Controls/MultipleItemPicker.ascx" TagName ="MultipleSelection" TagPrefix="ucl" %>
<script src="../js/CompanyRequisition.js" type ="text/javascript" ></script>

<style type="text/css">
    .text-label
    {
        color: #cdcdcd;
        font-weight: bold;
        font-size: small;
    }
</style>

<script type="text/javascript">

     

document.body.onclick = function(e)
  {
  $('#<%= pnlAdditionalInfoContent.ClientID %>').css({'overflow-y':'','overflow-x':''});
  $('#<%= pnlAdditionalInfoContent.ClientID %> div:first').css({'overflow-y':'','overflow-x':'','display':'inline-block','width':'100%'});
   $('input[rel=Integer]').keypress(function (event){return allowInteger(event,$(this));});
  
  }
  function ValidateRequisition(source, args)
      {
        alert ('d');
       
      }    
      
    function HotList_Selected(source, eventArgs) {
        hdnSelectedHotListValue = document.getElementById("ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnSelectedHotListValue");
        hdnSelectedHotListText = document.getElementById("ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnSelectedHot_ListText");
        hdnSelectedHotListText.value = eventArgs.get_text();
        hdnSelectedHotListValue.value = eventArgs.get_value();

    }
   
    var n = 10;
    function monitorClick(e) {
        try {
            var evt = (e) ? e : event;

            var theElem = (evt.srcElement) ? evt.srcElement : evt.target;

            while (theElem != null) {
                if (!$('#'+theElem .id).hasClass('active-result') && theElem.id.indexOf('spanTitle') &&  theElem.id.indexOf('txtSearch')&& theElem.id.indexOf('refReq') < 0 && theElem.id.indexOf('refHot') < 0 && theElem.id.indexOf('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_ddlRequisition') && theElem.id.indexOf('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtRequisition') && theElem.id.indexOf('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_ddlJobSeekerGroup') && theElem.id.indexOf('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_TxtHotList') && theElem.id.indexOf('btnAddToHotList') && theElem.id.indexOf('list') && theElem.id.indexOf('icon') && n != 0) {
                    n = 10;
                    var divReqs = document.getElementById('divReqs');
                    if (divReqs != null) {
                        $(divReqs).closest('.btn-group').removeClass('open');
                        divReqs.style.display = "none";
                    }
                    var divHotListdiv = document.getElementById('divHotListdiv');
                    if (divHotListdiv != null) {
                        $(divHotListdiv).closest('.btn-group').removeClass('open');
                        divHotListdiv.style.display = "none";
                    }
                    return true;
                }
                else {
                    n = 10;
                    return true;
                }
                theElem = theElem.offsetParent;
            }
        }
        catch (e) {
        } n = 10;
        return true;
    }
    document.onclick = monitorClick;
    function DropdownChange(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function HideContextMenu(divId) {
        if (n == 0) {
            var reqDiv = document.getElementById(divId);
        }
        if (reqDiv != null) reqDiv.style.display = "none";
    }
    function DropdownClicked(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }

    function ShowDiv(divId) {
        n = 10;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function ShowReqDiv(divId) {

$('.btn-group').removeClass('open');

        var a = document.getElementById("divReqs");
        var b = document.getElementById("divHotListdiv");
        if (a != null) a.style.display = "none";
        if (b != null) b.style.display = "none";

        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) {
            $(reqDiv).closest('.btn-group').addClass('open');
            reqDiv.style.display = "";
            reqDiv.parentElement.className = "btn-group open";
        }
        return false;
    }
    function ShowOrHide(v, img) {
        var tdDetail = document.getElementById(v);
        var image = document.getElementById(img);
        if (tdDetail.style.display == "none") {
            tdDetail.style.display = "";
            image.src = "../Images/collapse-minus.png";
        }
        else {
            tdDetail.style.display = "none"; image.src = "../Images/expand-plus.png";
        }

    }

    function ShowOrHideRow(v, img, id,isKeywordPresent) {
        var tdDetail = document.getElementById(v);
        var image = document.getElementById(img);
        var country = document.getElementById("ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnSiteSettingCountry").value;
        var SearchStringName = document.getElementById("ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnHighlightName").value;

        if (tdDetail.style.display == "none") {
            // alert(SearchStringName);
            GetCandidateSearchSub(id, v, country, SearchStringName,isKeywordPresent);
            tdDetail.style.display = "";
            image.src = "../Images/collapse-minus.png";
        }
        else {
            tdDetail.style.display = "none"; image.src = "../Images/expand-plus.png";
        }

    }
    function ShowOrHideCheckBoxClicked(v, img) {
        var tdDetail = document.getElementById(v);
        var image = document.getElementById(img);
        if (tdDetail.style.display == "none") {
            tdDetail.style.display = "";
            image.src = "../Images/collapse-minus.png";
        }
        else {
            tdDetail.style.display = "none"; image.src = "../Images/expand-plus.png";
        }
        SelectRowCheckbox();
        CheckAllRow();
    }
    function CheckAllRow() {
        var DetailArray = document.getElementById('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnDetailRowIds').value.split(',');
        var allExpand = true;
        var AllCollapsed = true;
        for (var i = 0; i < DetailArray.length; i++) {
            var arr = DetailArray[i].split(';');
            var row = document.getElementById(arr[0]);
            if (row != null) {
                if (row.style.display == "none") {
                    allExpand = false;
                }
                else {
                    AllCollapsed = false;
                }
            }

        }

        var headerimg = document.getElementById('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_lsvSearchResult_imgShowHide');
        if (allExpand) headerimg.src = "../Images/collapse-minus.png";
        if (AllCollapsed) headerimg.src = "../Images/expand-plus.png";
    }
    function HotListSelected(source, eventArgs) {
        hdnSelectedHotList = document.getElementById("ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnSelectedHotList");
        hdnSelectedHotListText = document.getElementById("ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnSelectedHotListText");
        hdnSelectedHotListText.value = eventArgs.get_text();
        hdnSelectedHotList.value = eventArgs.get_value();
    }



    function clickSearchButton(e) {
        // debugger;

        //event = fix(event);
        if (e.keyCode == 13) {
            var btn = $get('<%= btnSearch.ClientID%>');
            if (typeof btn == 'object') {
                btn.click();
            }
        }
    }
    function fix(event) {
        if (!event) event = window.event;
        if (!event) return null;
        if (!event.stopPropagation) { event.stopPropagation = Func('this.cancelBubble = true') }
        if (!event.preventDefault) { event.preventDefault = Func('this.returnValue = true') }
        if (typeof event.layerX == 'undefined' && typeof event.offsetX == 'number') {
            event.layerX = event.offsetX; event.layerY = event.offsetY;
        }
        if (event.target) {
            if (event.target.nodeType == 3) event.target = event.target.parentNode;
        }
        if (!event.target && event.srcElement) {
            event.target = event.srcElement;
            if (event.type == 'onmouseout') {
                event.relatedTarget = event.toElement;
            }
            else if (event.type == 'onmouseover') {
                event.relatedTarget = event.fromElement;
            }
        }
        event.fixed = true;
        return event;
    }


    function IsNumeric(sText) {
        var ValidChars = "0123456789.";
        var IsNumber = true;
        var Char;


        for (i = 0; i < sText.length && IsNumber == true; i++) {
            Char = sText.charAt(i);
            if (ValidChars.indexOf(Char) == -1) {
                IsNumber = false;
            }
        }
        return IsNumber;

    }
    function ShowHideAllDetail(HeaderImage) {
        var image = HeaderImage.src;
        var expand;
        if (image.indexOf("expand-plus.png") >= 0) {
            expand = true;
            HeaderImage.src = "../Images/collapse-minus.png";
        }
        else {
            expand = false;
            HeaderImage.src = "../Images/expand-plus.png";
        }
        var DetailArray = document.getElementById('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_hdnDetailRowIds').value.split(',');
        for (var i = 0; i < DetailArray.length; i++) {
            var arr = DetailArray[i].split(';');
            var row = document.getElementById(arr[0]);
            var img = document.getElementById(arr[1]);
            if (row != null && img != null) {
                img.src = HeaderImage.src;
                if (expand) row.style.display = "";
                else row.style.display = "none";
            }
        }
    }


    function SelectRowCheckbox() {
        var ischeckall = true;
        var HeaderCheckbox = document.getElementById('chkAllItem');
        var elements = HeaderCheckbox.form.elements;

        for (var i = 0; i < elements.length; i++) {
            if ((elements[i].type == 'checkbox') && (elements[i].id != HeaderCheckbox.id)) {


                if (elements[i].checked == true) {
                    var hndId = elements[i + 1];
                    ADDID(hndId.value, 'ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSelectedIds');

                }
                else {
                    var hndId = elements[i + 1];
                    RemoveID(hndId.value, 'ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSelectedIds');

                }


            }

        }
        HeaderCheckbox.checked = ischeckall;
    }






    function SelectAllCheckbox(HeaderCheckbox) {

        // debugger;
        var isChecked = HeaderCheckbox.checked;
        var elements = HeaderCheckbox.form.elements;

        for (var i = 0; i < elements.length; i++) {
            if ((elements[i].type == 'checkbox') && (elements[i].id != HeaderCheckbox.id)) {
                if (elements[i].id.indexOf('chkCandidate') > -1) {
                    if (elements[i].checked != isChecked) {
                        elements[i].checked = isChecked;
                        if (isChecked == true) {
                            var hndId = elements[i + 1];
                            if (IsNumeric(hndId.value) && hndId.value != "0")
                                ADDID(hndId.value, 'ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSelectedIds');

                        }
                        else {
                            var hndId = elements[i + 1];
                            if (IsNumeric(hndId.value) && hndId.value != "0")
                                RemoveID(hndId.value, 'ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSelectedIds');
                        }


                    }
                }

            }
        }
    }

    function IsReset() {
        var txtField = document.getElementById('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSelectedIds');
        txtField.value = "";
    }


    function CheckUnCheckRowLevelCheckboxes(cntrlHeaderCheckbox, cntrlGridView, posCheckbox) {
        //debugger
        var ctrlHeaderChkBox = $get(cntrlHeaderCheckbox);
        var ctrlTable = $get(cntrlGridView);
        var rowLength = ctrlTable.rows.length;

        for (var i = 1; i < rowLength; i++) {
            var myrow = ctrlTable.rows[i];
            var mycel = myrow.getElementsByTagName("td")[posCheckbox];

            for (var j = 0; j < mycel.childNodes.length; j++) {
                if (mycel.childNodes[j].type == "checkbox") {
                    mycel.childNodes[j].checked = ctrlHeaderChkBox.checked;
                }
            }
        }
    }

    function CheckUnCheckHeaderCheckbox(triggerControl, cntrlGridView, cntrlHeaderCheckbox, posCheckbox) {
        // debugger
        var ctrlHeaderChkBox = $get(cntrlHeaderCheckbox);

        if (!triggerControl.checked) {
            ctrlHeaderChkBox.checked = triggerControl.checked;
        }
        else {
            var checked = true;
            var ctrlTable = $get(cntrlGridView);
            var rowLength = ctrlTable.rows.length;

            for (var i = 1; i < rowLength; i++) {
                var myrow = ctrlTable.rows[i];
                var mycel = myrow.getElementsByTagName("td")[posCheckbox];

                for (var j = 0; j < mycel.childNodes.length; j++) {
                    if (mycel.childNodes[j].type == "checkbox") {
                        if (!checked || !mycel.childNodes[j].checked) {
                            checked = false;
                        }
                    }
                }
            }

            ctrlHeaderChkBox.checked = checked;
        }
    }



    function fnValidateSalary(source, args) {
        // debugger;        

        var txtSalaryRangeFromValue = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSalaryRangeFrom').value.trim();
        var ddlPaymentTypeValue = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_ddlPaymentType').value;

        if (txtSalaryRangeFromValue != "" && !isNaN(txtSalaryRangeFromValue) && ddlPaymentTypeValue < 1 && txtSalaryRangeFromValue > 0) {
            if (args.Value == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }

    function fnValidateSalaryTo(source, args) {
        // debugger;            

        var txtSalaryRangeToValue = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSalaryRangeTo').value.trim();
        var ddlPaymentTypeValue = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_ddlPaymentType').value;

        if (txtSalaryRangeToValue != "" && !isNaN(txtSalaryRangeToValue) && ddlPaymentTypeValue < 1 && txtSalaryRangeToValue > 0) {
            if (args.Value == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }

    function fnValidateCurrency(source, args) {
        // debugger;            

        var txtSalaryRangeFromValue = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSalaryRangeFrom').value.trim();
        var ddlCurrency = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_ddlCurrency').value;

        if (txtSalaryRangeFromValue != "" && !isNaN(txtSalaryRangeFromValue) && ddlCurrency < 1 && txtSalaryRangeFromValue > 0) {
            if (args.Value == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }



    }

    function fnValidateCurrencyTo(source, args) {
        // debugger;            

        var txtSalaryRangeToValue = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_txtSalaryRangeTo').value.trim();
        var ddlCurrency = $get('ctl00_cphHomeMaster_ctrlEntireInternalDatabase_ddlCurrency').value;

        if (txtSalaryRangeToValue != "" && !isNaN(txtSalaryRangeToValue) && ddlCurrency < 1 && txtSalaryRangeToValue > 0) {
            if (args.Value == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }

    function CheckboxUncheck() {
        var frm = document.forms[0];
        for (i = 0; i < frm.length; i++) {
            e = frm.elements[i];
            if (e.type == 'checkbox' && e.name.indexOf(chkCandidate) != -1) {
                e.checked = false;
            }
        }
    }
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReqs);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReqs);
    function PageLoadedReqs(sender, args) {
       
    }
    function endReqs(sender, args) {       
    }
</script>

<asp:Panel ID="pnlRoot" runat="server" DefaultButton="btnSearch">
    <asp:HiddenField ID="hdnSelectedRequisitionText" runat="server" />
    <asp:HiddenField ID="hdnSelectedRequisitionValue" runat="server" />
    <asp:HiddenField ID="hdnSelectedHot_ListText" runat="server" />
    <asp:HiddenField ID="hdnSelectedHotListValue" runat="server" />
    <asp:UpdatePanel ID="upnlSearchBox" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="hdnSiteSettingCountry" runat="server" />
            <asp:HiddenField ID="hdnSelectedHotListText" runat="server" />
            <asp:HiddenField ID="hdnSelectedHotList" runat="server" />
            <div style="display: none;">
                <asp:TextBox ID="hdnHighlightName" runat="server" />
            </div>
            <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
            <div style="width: 100%;">
                <div class="TabPanelHeader" style="text-align:left">Search Options</div>
                <asp:HiddenField ID="hdnDetailRowIds" runat="server" />
                <div class="TableRow" style="display: none;">
                    <div style="text-align: left; float: left;">
                        <asp:HiddenField ID="hfStartRowIndex" runat="server" Value="0" />
                        <asp:HiddenField ID="hfMaximumRowIndex" runat="server" Value="0" />
                        <asp:HiddenField ID="hfTotalResult" runat="server" Value="0" />
                    </div>
                </div>
                <div>
                    <div class="TableRow" style="height: 40px;">
                        <div class="TableFormLeble" style="padding-left: 20px; text-align: left;">
                            <asp:Label ID="lblAllKeywords" runat="server" Text="Keywords"></asp:Label>:
                            <asp:TextBox ID="txtAllKeywords" runat="server" CssClass="SearchTextbox" Width="450px"
                                EnableViewState="true"></asp:TextBox>
                            <%-- 10637 --%>
                            &nbsp;&nbsp;<asp:LinkButton ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary"
                                OnClick="btnSearch_Click" ValidationGroup="ATSPreciseSearch"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                            <%-- 8669 --%>
                        </div>
                        <a id="lnkHelp" runat="server" style="color: Blue; cursor: pointer; float: right;">Search
                            Tips</a>
                    <div>
                        <asp:Panel ID="pnlSearchHelp" runat="server" CssClass="modalPopup" Style="display: none;
                            width: 560px;">
                            <div style="margin: 10px">
                                <div class="TableRow" style="font-family: Verdana; font-size: 11px;">
                                    <center>
                                        <b>Search Tips:</b></center>
                                    <br />
                                    <b>Boolean AND:</b> To search for all keywords, separate search terms with spaces
                                    or 'AND'.
                                    <br />
                                    Ex : Asp.net SQL Server AND Java
                                    <br />
                                    <br />
                                    <b>Boolean OR:</b> To search for any of the keywords, separate search terms with
                                    'OR' or commas.
                                    <br />
                                    Ex : Asp.net OR SQL Server, Java
                                    <br />
                                    <br />
                                    <b>Boolean NOT:</b> To exclude a keyword, prepend the search terms with 'NOT' or
                                    '-'.
                                    <br />
                                    Ex : Asp.net NOT SQL Server - Java
                                    <br />
                                </div>
                                <div style="text-align: center; padding-top: 7px;">
                                    <asp:Button ID="btnOkHelp" runat="server" CssClass="CommonButton" Text="Return" Width="50px" />
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:ModalPopupExtender ID="helpModalPopupExt" runat="server" TargetControlID="lnkHelp"
                            PopupControlID="pnlSearchHelp" BackgroundCssClass="modalBackground" DropShadow="false"
                            OkControlID="btnOkHelp" BehaviorID="MPE">
                        </asp:ModalPopupExtender>
                    </div>
                </div>
            </div>
            <div style="clear: both">
            </div>
            <div class="BoxContainer"  style =" margin : 24px 0px 0px 0px ">
                <div style="width: 100%; text-align: center;">
                    <asp:Panel ID="pnlAdditionalInfoBody" runat="Server" CssClass="well well-small nomargin">
                        <asp:Panel ID="pnlAdditionalInfoHeader" runat="server" Style="cursor: pointer;">
                            <div class="SearchTitleContainer">
                                <div class="ArrowContainer">
                                    <asp:ImageButton ID="imgAdditionalInfoToggle" runat="server" ImageUrl="~/Images/expand-plus.png"
                                        AlternateText="collapse" />
                                </div>
                                <div class="TitleContainer">
                                    Advanced Search
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlAdditionalInfoContent" runat="server" 
                             Style=" overflow:inherit   " Height="0">
                            <div class="TableRow spacer">
                                <div class="FormLeftColumn" style="width: 55% ;">
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblJobTitle" runat="server" Text="Current Job Title"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:TextBox ID="txtJobTitle" Width="180px" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblWorkCity" runat="server" Text="Location" ></asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:TextBox ID="txtWorkCity" Width="130px" runat="server" CssClass="CommonTextBox"
                                                ToolTip="City" placeholder="City"></asp:TextBox>
                                            <asp:TextBox ID="txtZipCode" runat="server" CssClass="CommonTextBox" Width="100px" placeholder = "PIN/ZIP Code"
                                                ToolTip="Zip Code"></asp:TextBox>
                                            <div class="TableFormValidatorContent" style="margin-left: 61%">
                                                <asp:RegularExpressionValidator ID="revZIPCode" runat="server" ControlToValidate="txtZipCode"
                                                    Display="Dynamic" ErrorMessage="Please enter a valid zip code" ValidationExpression="^(PIN Code|Zip Code|[0-9-]*)$"
                                                    ValidationGroup="ATSPreciseSearch"></asp:RegularExpressionValidator><%--(\d{6}(-\d{4})?)|(\d{5}(-\d{4})?)--%>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblRadius" runat="server" Text="Radius"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:DropDownList ID="ddlRadius" runat="server" CssClass="CommonDropDownList" Width="130px"
                                                EnableViewState="true">
                                                <asp:ListItem Value="5">Within 5 miles</asp:ListItem>
                                                <asp:ListItem Value="10">Within 10 miles</asp:ListItem>
                                                <asp:ListItem Value="20" Selected="True">Within 20 miles</asp:ListItem>
                                                <asp:ListItem Value="30">Within 30 miles</asp:ListItem>
                                                <asp:ListItem Value="40">Within 40 miles</asp:ListItem>
                                                <asp:ListItem Value="50">Within 50 miles</asp:ListItem>
                                                <asp:ListItem Value="60">Within 60 miles</asp:ListItem>
                                                <asp:ListItem Value="75">Within 75 miles</asp:ListItem>
                                                <asp:ListItem Value="100">Within 100 miles</asp:ListItem>
                                                <asp:ListItem Value="150">Within 150 miles</asp:ListItem>
                                                <asp:ListItem Value="200">Within 200 miles</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                 <ucl:CountryState ID="uclCountryState" runat ="server" FirstOption="Any" ApplyDefaultSiteSetting ="False"  />
    
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblYearsofExp" runat="server" Text="Years of Experience"></asp:Label>:
                                        </div>
                                        <div style="vertical-align: text-top; float: left;">
                                            <asp:Label ID="Label3" runat="server" Text="MIN"></asp:Label><span>:</span>
                                            <asp:TextBox ID="txtMinExp" Width="20px" runat="server" CssClass="CommonTextBox"
                                                MaxLength="2"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label4" runat="server" Text="MAX"></asp:Label><span>:</span>
                                            <asp:TextBox ID="txtMaxExp" Width="20px" runat="server" CssClass="CommonTextBox"
                                                MaxLength="2"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                                            <asp:RegularExpressionValidator ID="revMinExp" runat="server" ErrorMessage="MIN field should be integer."
                                                ControlToValidate="txtMinExp" Display="Dynamic" SetFocusOnError="True" ValidationExpression="^\d*[0-9]$"
                                                ValidationGroup="ATSPreciseSearch">
                                            </asp:RegularExpressionValidator>
                                        </div>
                                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                                            <asp:RegularExpressionValidator ID="revMaxExp" runat="server" ErrorMessage="MAX field should be integer."
                                                ControlToValidate="txtMaxExp" Display="Dynamic" SetFocusOnError="True" ValidationExpression="^\d*[0-9]$"
                                                ValidationGroup="ATSPreciseSearch">
                                            </asp:RegularExpressionValidator>
                                        </div>
                                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                                            <asp:CompareValidator ID="cmpValidExp" ControlToValidate="txtMaxExp" ControlToCompare="txtMinExp"
                                                runat="server" SetFocusOnError="true" Display="Dynamic" EnableViewState="false"
                                                ErrorMessage="Max experience should be greater than min experience" ValidationGroup="ATSPreciseSearch"
                                                Operator="GreaterThan" Type="Integer" />
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble" ">
                                            <asp:Label ID="lblSalaryRange" runat="server" Text="Salary Range"></asp:Label>:
                                        </div>
                                        <div style="vertical-align: text-top;" width="400px;" align="left">
                                            <asp:Label ID="lblSalaryRangeFrom" runat="server" Text="From"></asp:Label><span>:</span>
                                            <asp:TextBox ID="txtSalaryRangeFrom" Width="50px" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                                            <asp:Label ID="lblSalaryRangeTo" runat="server" Text="To"></asp:Label><span>:</span>
                                            <asp:TextBox ID="txtSalaryRangeTo" Width="50px" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                                            <asp:DropDownList ID="ddlSararySelection" runat="server" CssClass="CommonDropDownList"
                                                Width="80px">
                                                <asp:ListItem Text="Expected" Selected="True" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Current" Value="1"></asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="TableRow">
                                                <div class="TableFormLeble" ">
                                                    <asp:Label ID="lbl" runat="server"></asp:Label>
                                                </div>
                                                <div style="float: left;">
                                                    <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="CommonDropDownList" Width="102px">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="CommonDropDownList" Width="80px">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="lblSalaryRate" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="TableRow">
                                            <asp:Label ID="lblRangeError" runat="server" EnableViewState="false" ForeColor="Red"
                                                Font-Names="Verdana" Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                                            <asp:RegularExpressionValidator runat="server" ID="revSalaryRangeFrom" ControlToValidate="txtSalaryRangeFrom"
                                                ErrorMessage="Please enter valid Min Salary" ValidationExpression="^\d*[0-9]\d*(\.\d+)?$"
                                                EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ValidationGroup="ATSPreciseSearch">
                                            </asp:RegularExpressionValidator>
                                        </div>
                                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                                            <asp:RegularExpressionValidator runat="server" ID="revSalaryRangeTo" ControlToValidate="txtSalaryRangeTo"
                                                ErrorMessage="Please enter valid Max Salary" ValidationExpression="^\d*[0-9]\d*(\.\d+)?$"
                                                EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ValidationGroup="ATSPreciseSearch">
                                            </asp:RegularExpressionValidator>
                                        </div>
                                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                                            <asp:CompareValidator ID="cmpValYearlySalary" ControlToValidate="txtSalaryRangeTo"
                                                ControlToCompare="txtSalaryRangeFrom" runat="server" SetFocusOnError="true" Display="Dynamic"
                                                EnableViewState="false" ErrorMessage="Max salary should be greater than min salary"
                                                ValidationGroup="ATSPreciseSearch" Operator="GreaterThanEqual" Type="Double" />
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                                            <asp:CustomValidator ID="cvCurrency" runat="server" ControlToValidate="ddlCurrency"
                                                EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please select Currency type."
                                                ClientValidationFunction="fnValidateCurrency" ValidationGroup="ATSPreciseSearch" />
                                        </div>
                                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                                            <asp:CustomValidator ID="cvPaymentType" runat="server" ControlToValidate="ddlPaymentType"
                                                EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please select Salary Type."
                                                ClientValidationFunction="fnValidateSalary" ValidationGroup="ATSPreciseSearch" />
                                        </div>
                                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                                            <asp:CustomValidator ID="cvCurrencyTO" runat="server" ControlToValidate="ddlCurrency"
                                                EnableViewState="false" SetFocusOnError="true" Display="Dynamic" Visible="false"
                                                ErrorMessage="Please select Currency type." ClientValidationFunction="fnValidateCurrencyTo"
                                                ValidationGroup="ATSPreciseSearch" />
                                        </div>
                                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                                            <asp:CustomValidator ID="cvPaymentTypeTo" runat="server" ControlToValidate="ddlPaymentType"
                                                EnableViewState="false" SetFocusOnError="true" Display="Dynamic" Visible="false"
                                                ErrorMessage="Please select Salary Type." ClientValidationFunction="fnValidateSalaryTo"
                                                ValidationGroup="ATSPreciseSearch" />
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblCandidateEmail" runat="server" Text="Candidate Email"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:TextBox ID="txtCandidateEmail" runat="server" CssClass="SearchTextbox"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblCandidateID" runat="server" Text="Candidate ID"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:TextBox ID="txtCandidateID" runat="server" CssClass="ApplicantIDTextbox" rel="Integer"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblMobilePhone" runat="server" Text="Mobile"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:TextBox ID="txtMobilePhone" runat="server" CssClass="SearchTextbox"  rel="Integer" MaxLength="15"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblAssignedManager" runat="server" Text="Assigned Manager"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:DropDownList ID="ddlAssignedManager" runat="server" CssClass="CommonDropDownList">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblSearchInHotList" runat="server" Text="Hot List"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:DropDownList ID="ddlHotList" runat="server" CssClass="CommonDropDownList" Width="187px">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSearch_HotList" runat="server" CssClass="CommonTextBox" AutoComplete="OFF"
                                                Visible="true" Width="200px"></asp:TextBox>
                                            <div id="divSearch_HotList">
                                            </div>
                                            <ajaxToolkit:AutoCompleteExtender ID="autoSeachHotList" runat="server" ServicePath="~/AjaxExtender.asmx"
                                                TargetControlID="txtSearch_HotList" MinimumPrefixLength="1" ServiceMethod="GetHotListItems"
                                                EnableCaching="True" CompletionInterval="0" CompletionListCssClass="AutoCompleteBox"
                                                FirstRowSelected="True" OnClientItemSelected="HotListSelected">
                                            </ajaxToolkit:AutoCompleteExtender>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblEducation" runat="server" Text="Education"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                        <ucl:MultipleSelection ID="uclEducation" runat ="server" />
                                            <%--<div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                                width: 205px; height: 100px; overflow: auto; display : none ">
                                                <asp:CheckBoxList ID="chkEducationList" runat="server" AutoPostBack="false">
                                                </asp:CheckBoxList>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="FormRightColumn" style="width: 44%">
                                 <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="Label2" runat="server" Text="Source">Source</asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:DropDownList ID="ddlSourceId" runat="server" CssClass="CommonDropDownList">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblempCom" runat="server" Text="Employer/Company"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:TextBox ID="txtEmpOrCom" runat="server" CssClass="CommonTextBox" Width="120px"></asp:TextBox>
                                            <asp:DropDownList ID="ddlEmpOrCom" runat="server" CssClass="CommonDropDownList" Width="80px">
                                                <asp:ListItem Value="Current" Selected="True">Current</asp:ListItem>
                                                <asp:ListItem Value="Previous">Previous</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblJobType" runat="server" Text="Employment Type"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:DropDownList ID="ddlJobType" runat="server" CssClass="CommonDropDownList" Width="200px">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblResumeLastUpdate" runat="server" Text="Resume Last Updated"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:DropDownList ID="ddlResumeLastUpdate" runat="server" CssClass="CommonDropDownList">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                   
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label EnableViewState="false" ID="lblWorkSchedule" runat="server" Text="Work Schedule"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent">
                                            <asp:DropDownList EnableViewState="true" ID="ddlWorkSchedule" runat="server" CssClass="CommonDropDownList">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="TableRow">
                                        <div class="TableFormLeble">
                                            <asp:Label ID="lblWorkStatus" runat="server" Text="Work Status"></asp:Label>:
                                        </div>
                                        <div class="TableFormContent" style="float: left; width: 150px;">
                                         <ucl:MultipleSelection ID="uclWorkStatus" runat ="server" />
                                            <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                                width: 205px; height: 100px; overflow: auto ; display : none ">
                                                <asp:CheckBoxList ID="lstWorkStatus" runat="server">
                                                </asp:CheckBoxList>
                                            </div>
                                        </div>
                                        <div class="TableRow">
                                            <div class="TableFormLeble">
                                                <asp:Label ID="lblHiringStatus" runat="server" Text="Hiring Status"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent" style="float: left; width: 150px;">
                                                <ucl:MultipleSelection ID="uclHiringLevel" runat ="server" />
                                                <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                                    width: 205px; height: 100px; overflow: auto; display : none ;">
                                                    <asp:CheckBoxList ID="chkHiringStatus" runat="server">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="TableRow">
                                            <div class="TableFormLeble">
                                                <asp:Label ID="lblAvailability" runat="server" Text="Available After"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                              <%--  <igsch:WebDateChooser ID="wdcAvailability" MinDate="01/01/1753" runat="server" Editable="true"
                                                    NullDateLabel="" CalendarLayout-HideOtherMonthDays="true">
                                                </igsch:WebDateChooser>--%>
                                                <ig:WebDatePicker ID="wdcAvailability"  DropDownCalendarID="ddcAvailability" runat="server"></ig:WebDatePicker>
                                                <ig:WebMonthCalendar  ID="ddcAvailability" AllowNull="true"  runat="server"></ig:WebMonthCalendar>
                                            </div>
                                        </div>
                                        <div class="TableRow">
                                            <div class="TableFormLeble">
                                                <asp:Label ID="lblSecurityClearance" runat="server" Text="Security Clearance"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:RadioButtonList ID="optSecurityClearance" runat="server" RepeatDirection="Horizontal">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="TableRow">
                                            <div class="TableFormLeble">
                                                <asp:Label ID="lblinternalrating" runat="server" Text="Internal Rating"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:DropDownList EnableViewState="true" ID="ddlinternalratingID" runat="server" CssClass="CommonDropDownList">
                                                
                                            </asp:DropDownList>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        
                                        
                                        <div class="TableRow">
                                            <div class="TableFormLeble">
                                             <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:DropDownList EnableViewState="true" ID="ddlGender" runat="server" CssClass="CommonDropDownList">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        
                                        <div class="TableRow">
                                            <div class="TableFormLeble">
                                             <asp:Label ID="lblNationality" runat="server" Text="Nationality"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:DropDownList EnableViewState="true" ID="ddlCountryOfBirth" runat="server" CssClass="CommonDropDownList">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="TableRow">
                                            <div class="TableFormLeble">
                                             <asp:Label ID="lblIndustry" runat="server" Text="Industry"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:DropDownList EnableViewState="true" ID="ddlIndustry" runat="server" CssClass="CommonDropDownList">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="TableRow">
                                            <div class="TableFormLeble">
                                             <asp:Label ID="lblJobFunction" runat="server" Text="Job Function"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:DropDownList EnableViewState="true" ID="ddlJobFunction" runat="server" CssClass="CommonDropDownList">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        
                                        
                                        <div class="TableRow">
                                            <div class="TableFormLeble">
                                                <asp:Label ID="lblNoticePeriod" runat="server" Text="Notice Period"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:TextBox ID="txtNoticePeriod" runat="server" CssClass="CommonTextBox" AutoComplete="OFF"
                                                Visible="true" Width="200px"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                    <div style="height: 10px;">
                    </div>
                    <asp:CollapsiblePanelExtender ID="cpeAdvancedSearch" runat="Server" TargetControlID="pnlAdditionalInfoContent"
                        ExpandControlID="pnlAdditionalInfoHeader" CollapseControlID="pnlAdditionalInfoHeader"
                        Collapsed="True" ImageControlID="imgAdditionalInfoToggle" SuppressPostBack="true"
                        CollapsedImage="~/Images/expand-plus.png" ExpandedImage="~/Images/collapse-minus.png"
                        SkinID="CollapsiblePanel" />
                  
                    </asp:Panel>
                    <div style="height: 10px;">
                    </div>
                    <div class="TableRow">
                        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                    </div>
                    <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
                    <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" ServicePath="~/AjaxExtender.asmx"
                        TargetControlID="TxtHotList" MinimumPrefixLength="1" ServiceMethod="GetHotListItems"
                        EnableCaching="True" CompletionListElementID="pnlhotListCompletionItem" CompletionInterval="0"
                        CompletionListCssClass="AutoCompleteBox" FirstRowSelected="True" OnClientItemSelected="HotList_Selected" />
                    <div id="divShowSearchStatus" runat="server" visible="false" style="margin-left: 0px;"
                        align="left">
                        <div class="TabPanelHeader" style="margin-bottom:0px;">Results</div>
                        <div class="btn-toolbar" id="divBulkActions" runat="server">
                            <div class="pull-left">
                                <div class="btn-group">
                                    <a href="Javascript:void(0)" onclick="ShowReqDiv('divReqs')" id="refReq" class="btn dropdown-toggle">
                                        Add to Requisition <span class="caret"></span></a>
                                    <ul class="dropdown-menu-custom" id="divReqs" style="display: none">
                                        <li style="width: 220px; text-align: left" id="list">
                                              <ucl:CompReq id="uclComReq" runat="server"></ucl:CompReq>
                                                <asp:HiddenField ID="hndCurrentMember" runat="server" Value="2" />
                                                <asp:Button ID="btnAddToSelectionStep" runat="server" CssClass="btn btn-small" Text="Add"
                                                    OnClick="btnAddToSelectionStep_Click" ValidationGroup ="RequisitionList" />
                                                      <asp:CustomValidator ID="cvRequisition" runat="server" ClientValidationFunction="ValidateRequisition"
                                    Display="Dynamic" ErrorMessage="Please select a Candidate." ValidationGroup="RequisitionList"
                                    Font-Bold="false"></asp:CustomValidator>
                                            </asp:Panel>
                                        </li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <a href="Javascript:void(0)" onclick="ShowReqDiv('divHotListdiv')" class="btn btn-medium dropdown-toggle"
                                        id="refHot")">Add to Hot List <span class="caret"></span></a>
                                    <ul class="dropdown-menu-custom" id="divHotListdiv" style="display: none">
                                        <li style="width: 220px; text-align: left" id="li1">
                                            <asp:Panel ID="pnlHotList" runat="server" DefaultButton="btnAddToHotList">
                                                <asp:DropDownList ID="ddlJobSeekerGroup" runat="server" CssClass="chzn-select" Width = "150px"
                                                    onclick="DropdownClicked('divHotListdiv')" onchange="DropdownChange('divHotListdiv')">
                                                </asp:DropDownList>
                                                <asp:TextBox ID="TxtHotList" runat="server" Visible="true" Width="130px"></asp:TextBox>
                                                <asp:Panel ID="pnlhotListCompletionItem" runat="server" CssClass="AutoCompleteCompletionItem"
                                                    Visible="false">
                                                </asp:Panel>
                                                <asp:Button ID="btnAddToHotList" runat="server" CssClass="btn btn-small" Text="Add"
                                                    OnClick="btnAddToHotList_Click" style=" margin-bottom : 15px;" />
                                            </asp:Panel>
                                        </li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <asp:LinkButton ID="btnEmail" runat="server" Text="Email Candidates" OnClick="lnkBtnEmail_Click"
                                        CssClass="btn" />
                                </div>
                                <div class="btn-group" runat="server" id="liRemove">
                                    <asp:LinkButton ID="btnRemove" runat="server" Text="Delete" OnClick="btnRemove_Click"
                                        CssClass="btn btn-danger" />
                                </div>
                            </div>
                            
                            <div style="float: right; margin-top: 10px; margin-right: 4px; margin-bottom: 3px;">
                    Name Format:
                    <asp:DropDownList ID="ddlNameFormat" runat="server" CssClass="CommonDropDownList"
                        Width="100px" OnSelectedIndexChanged="ddlNameFormat_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Text="First Last" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Last, First" Value="1"></asp:ListItem>
                    </asp:DropDownList>
                        </div>
                    </div>
                    <div id="divSearchSummary" runat="server" align="right" style="vertical-align: baseline;
                        height: 30px; font: 9pt Arial,Verdana;">
                        
                        
                </div>
                    </div>
                    
                    
                    
                    
                    <div class="GridContainer" id="divListViewExample" style="width: 100%; overflow: auto;
                        overflow-y: hidden;">
                        <asp:Label ID="Label1" runat="server" EnableViewState="false"></asp:Label>
                        <asp:ObjectDataSource ID="odsCandidateSearch" runat="server" SelectMethod="GetPaged"
                            TypeName="TPS360.Web.UI.CandidateSearchDataSource" SelectCountMethod="GetListCount"
                            EnablePaging="True" SortParameterName="sortExpression" EnableViewState="true"
                            OnSelected="odsCandidateSearch_Selected" OnSelecting="odsCandidateSearch_Selecting">
                            <SelectParameters>
                            <asp:ControlParameter ControlID ="uclCountryState" Name ="currentCountryId" PropertyName ="SelectedCountryId" Type ="String"  />
                            <asp:ControlParameter ControlID ="uclCountryState" Name ="state" PropertyName ="SelectedStateId" Type ="String" />
                                <asp:Parameter Name="allKeys" DefaultValue="" />
                                <asp:Parameter Name="anyKey" DefaultValue="" />
                                <asp:Parameter Name="currentPosition" DefaultValue="" />
                                <asp:Parameter Name="currentCity" DefaultValue="" />
                              
                                <asp:Parameter Name="jobType" DefaultValue="" />
                                <asp:Parameter Name="workStatus" DefaultValue="" />
                                <asp:Parameter Name="securityClearence" DefaultValue="" />
                                <asp:Parameter Name="availability" DefaultValue="" />
                                <asp:Parameter Name="industryType" DefaultValue="" />
                                <asp:Parameter Name="lastUpdateDate" DefaultValue="" />
                                <asp:Parameter Name="salaryType" DefaultValue="" />
                                <asp:Parameter Name="salaryFrom" DefaultValue="" />
                                <asp:Parameter Name="salaryTo" DefaultValue="" />
                                <asp:Parameter Name="currencyType" DefaultValue="" />
                                <asp:Parameter Name="functionalCategory" DefaultValue="" />
                                <asp:Parameter Name="hiringMatrixId" DefaultValue="" />
                                <asp:Parameter Name="hotListId" DefaultValue="" />
                                <asp:Parameter Name="memberId" DefaultValue="" />
                                <asp:Parameter Name="rowPerPage" DefaultValue="" />
                                <asp:Parameter Name="memberPositionId" DefaultValue="" />
                                <asp:Parameter Name="isVolumeHire" DefaultValue="0" />
                                <asp:Parameter Name="isMyList" DefaultValue="0" />
                                <asp:Parameter Name="minExpYr" DefaultValue="" />
                                <asp:Parameter Name="maxExpYr" DefaultValue="" />
                                <asp:Parameter Name="applicantType" DefaultValue="" />
                               
                                <asp:Parameter Name="maxResults" DbType="Int32" DefaultValue="" />
                                <asp:Parameter Name="Education" DefaultValue="" />
                                <asp:Parameter Name="WorkSchedule" DefaultValue="" />
                                
                                <asp:Parameter Name="Gender" DefaultValue="" />
                                <asp:Parameter Name="Nationality" DefaultValue="" />
                                <asp:Parameter Name="Industry" DefaultValue="" />
                                <asp:Parameter Name="JobFunction" DefaultValue="" />
                                
                                <asp:Parameter Name="HiringStatus" DefaultValue="" />
                                <asp:Parameter Name="CurrentCompany" DefaultValue="" />
                                <asp:Parameter Name="CompanyStatus" DefaultValue="" />
                                <asp:Parameter Name="CandidateEmail" DefaultValue="" />
                                <asp:Parameter Name="MobilePhone" DefaultValue="" />
                                <asp:Parameter Name="ZipCode" DefaultValue="" />
                                <asp:Parameter Name="Radius" DefaultValue="" />
                                <asp:Parameter Name="AddinWhereClaues" DbType="Boolean" DefaultValue="true" />
                                <asp:Parameter Name="IsMile" DbType="Boolean" DefaultValue="false" />
                                <asp:Parameter Name="AssignedManagerId" DefaultValue="0" />
                                <asp:Parameter Name="SalarySelector" DefaultValue="0" />
                                <asp:Parameter Name="SortOrder" DefaultValue="asc" />
                                <asp:Parameter Name="SortColumn" DefaultValue="[C].[CandidateName]" />
                                <asp:Parameter Name="DisplayOption" DefaultValue="" />
                                <asp:Parameter Name="NoticePeriod" DefaultValue="" />
                                <asp:ControlParameter ControlID ="ddlSourceId" PropertyName ="SelectedValue" Name ="SourceId" />
                                <asp:ControlParameter ControlID ="ddlinternalratingID" PropertyName ="SelectedValue" Name ="internalRating" />
                                <asp:ControlParameter ControlID ="txtCandidateID" PropertyName ="Text" Name="CandidateID" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
                        <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
                        <div style="display: none">
                            <asp:TextBox ID="txtSelectedIds" runat="server"></asp:TextBox></div>
                        <asp:ListView ID="lsvSearchResult" runat="server" DataKeyNames="Id" EnableViewState="true"
                            OnItemDataBound="lsvSearchResult_ItemDataBound" OnItemCommand="lsvSearchResult_ItemCommand"
                            OnPreRender="lsvSearchResult_PreRender">
                            <LayoutTemplate>
                                <table id="tlbTemplate" class="Grid" cellspacing="0" border="0" style="width: 100%">
                                    <tr style="width: 100%;" runat="server">
                                        <th style="width: 40px !important;">
                                          
                                                <asp:Image ID="imgShowHide" ImageUrl="~/Images/expand-plus.png" Visible="false" runat="server"
                                                    onClick="ShowHideAllDetail(this)" />
                                                <input id="chkAllItem" name="chkAllItem" type="checkbox" onclick="SelectAllCheckbox(this)" />
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="lnkHeaderName" runat="server" Text="Name" CommandName="Sort"
                                                CommandArgument="[C].[CandidateName]"></asp:LinkButton>
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="lnkHeaderPosition" runat="server" Text="Position" CommandName="Sort"
                                                CommandArgument="[C].[CurrentPosition]"></asp:LinkButton>
                                        </th>
                                        <th>
                                            Location
                                        </th>
                                        <th>
                                            Mobile Phone
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="lnkHeaderEmail" runat="server" Text="Email" CommandName="Sort"
                                                CommandArgument="[C].[PrimaryEmail]"></asp:LinkButton>
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="lnkHeaderExperience" runat="server" Text="Exp (Yrs)" CommandName="Sort"
                                                CommandArgument="C.TotalExperienceYears" Width="60%"></asp:LinkButton>
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="lnkHeaderHiringStatus" runat="server" Text="Hiring Status" CommandName="Sort"
                                                CommandArgument="SortOrder" Width="60%"></asp:LinkButton>
                                        </th>
                                           <%-- *******************ADDED BY PRAVIN KHOT ON 23/June/2016************--%>
                                       <th>
                                          <asp:Label ID="btnSource" runat="server" Text="Source" />
                                          <%-- <asp:LinkButton ID="btnSource" runat="server" ToolTip="Sort By Source" CommandName="Sort"
                                          CommandArgument="[GS].[Name]" Text="Source"  />       --%>                             
                                       </th>
                                
                                       <th>
                                      <asp:Label ID="btnSourceDescription" runat="server" Text="Source Description" />
                                         <%-- <asp:LinkButton ID="btnSourceDescription" runat="server" ToolTip="Sort By Source Description" CommandName="Sort"
                                         CommandArgument="[ME].[SourceDescription]" Text="Source Description"  />--%>
                                      </th>
                                          <%--  *******************************END********************************--%>
                                        <th id="thRelevance" runat="server" style="width: 180px">
                                            <asp:LinkButton ID="lnkHeaderRelevance" runat="server" Text="Relevance" CommandName="Sort"
                                                CommandArgument="[CO].[RANK]"></asp:LinkButton>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="padding: 0px 0px 0px 0px; border-style: none none none none">
                                        </td>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Pager">
                                        <td colspan="11" id="tdPager" runat="server">
                                            <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                        </td>
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <EmptyDataTemplate>
                                <div class="alert alert-warning">
                                    No candidates found. Please try another search.
                                </div>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'
                                    align="left">
                                    <td style="border-bottom-style: none;" id="td1" align="left">
                                                <asp:Image ID="imgShowHide" ImageUrl="~/Images/expand-plus.png" runat="server" CssClass="imgShowHide" />
                                                <asp:CheckBox ID="chkCandidate" runat="server" />
                                                <asp:HiddenField ID="hfCandidateId" runat="server" />
                                           
                                    </td>
                                    <td style="border-style: none" id="td2">
                                        <asp:HyperLink ID="hlnCandidateName" runat="server" Text=""  EnableViewState="true"></asp:HyperLink>
                                    </td>
                                    <td style="border-style: none" id="td3">
                                        <asp:Label ID="lblCandidateCurrentPosition" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td style="border-style: none" id="td4">
                                        <asp:Label ID="lblCity" runat="server"></asp:Label>
                                    </td>
                                    <td style="border-style: none" id="td5">
                                        <asp:Label ID="lblCandidateMobile" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td style="border-style: none" id="td9">
                                        <asp:LinkButton ID="lblCandidateEmail" runat="server" Text=""></asp:LinkButton>
                                    </td>
                                    <td style="border-style: none" id="td6">
                                        <asp:Label ID="lblCandidateExperience" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td style="border-style: none" id="td7">
                                        <asp:Label ID="lblHiringstatus" runat="server" Text=""></asp:Label>
                                    </td>
                                   <%-- *******************ADDED BY PRAVIN KHOT ON 23/June/2016************--%>
                                     <td>
                                       <asp:Label ID="lblSource" runat="server" />
                                    </td>
                                    <td>
                                      <asp:Label ID="lblSourceDescription" runat="server" />
                                    </td>
                                  <%--  *******************************END********************************--%>
                                    <td style="border-style: none;" id="tdRelevance" runat="server">
                                        <div id="divRelevance" runat="server">
                                            <div id="relevance_bar" class="progress" runat="server">
                                                <div id="relevance_text" class="bar" runat="server">
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="trCandidateDetails" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "Expandrow" : "Expandaltrow" %>'
                                    style="display: none">
                                    <td>
                                    </td>
                                    <td colspan="2" align="left" style="font-weight: normal;">
                                        <asp:Panel ID="pnlContent1" runat="server">
                                            <b>Primary Manager:</b>
                                            <asp:Label ID="lblPrimaryManager" runat="server"></asp:Label>
                                            <br />
                                            <b>Skills:</b><asp:Label ID="lblskills" runat="server"></asp:Label>
                                            <br />
                                            <b>Availability:</b>
                                            <asp:Label ID="lblCandidateAvailability" runat="server" Text=""></asp:Label><br />
                                            <b>Work Schedule:</b>
                                            <asp:Label ID="lblWorkSchedule" runat="server" Text=""></asp:Label><br />
                                            <b>Work Status:</b>
                                            <asp:Label ID="lblCandidateWorkStatus" runat="server" Text=""></asp:Label><br />
                                            <b>Objective:</b>
                                            <asp:Label ID="lblCandidateObjective" runat="server" Text=""></asp:Label><br />
                                            <b>Remarks:</b>
                                            <asp:Label ID="lblRemarks" runat="server" Text=""></asp:Label>
                                        </asp:Panel>
                                    </td>
                                    <td colspan="6" align="left" style="font-weight: normal;" id="tdDetail" runat="server">
                                        <asp:Panel ID="pnlContent2" runat="server">
                                            <b>Current Salary:</b>
                                            <asp:Label ID="lblCandidateCurrentSalary" runat="server" Text=""></asp:Label><br />
                                            <b>Expected Salary:</b>
                                            <asp:Label ID="lblCandidateExpectedSalary" runat="server" Text=""></asp:Label><br />
                                            <b>Current Company:</b>
                                            <asp:Label ID="lblCandidateCurrentCompany" runat="server" Text=""></asp:Label><br />
                                            <b>Highest Degree:</b>
                                            <asp:Label ID="lblHighestDegree" runat="server" Text=""></asp:Label><br />
                                            <b>Candidate Type:</b>
                                            <asp:Label ID="lblCandidateType" runat="server" Text=""></asp:Label><br />
                                            <b>Last Updated:</b>
                                            <asp:Label ID="lblCandidateLastUpdate" runat="server" Text=""></asp:Label>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
