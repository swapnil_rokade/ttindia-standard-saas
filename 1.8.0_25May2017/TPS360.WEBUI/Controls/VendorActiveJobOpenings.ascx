<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VendorActiveJobOpenings.ascx.cs"
    Inherits="VendorActiveJobOpenings" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
<asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
<asp:Panel ID="widgetBody" runat="server">
   <asp:ObjectDataSource ID="odsRequisitionList" runat="server" SelectMethod="GetPagedRequisitionListForActiveJobOpenings"
                        OnSelecting="odsRequisitionList_Selecting" TypeName="TPS360.Web.UI.JobPostingDataSource"
                        SelectCountMethod="GetListCountForActiveJobOpenings" EnablePaging="True" SortParameterName="sortExpression">
                        <SelectParameters>
                            <asp:Parameter Name="JobTitle" DefaultValue="" />
                            <asp:Parameter Name="ReqCode" DefaultValue="" />
                            <asp:Parameter Name="City" DefaultValue="" />
                            <asp:Parameter Name="StateID" DefaultValue="0" />
                            <asp:Parameter Name="CountryID" DefaultValue="0" />
                            <asp:ControlParameter ControlID="hdnVendorId" PropertyName="Value" Name="VendorId" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                <asp:UpdatePanel ID="upcandidateList" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="hdnSortColumn" runat="server" Value ="btnPostedDate" />
                            <asp:HiddenField ID="hdnSortOrder" runat="server" Value ="DESC" />
                            <asp:HiddenField ID="hdnEmployeeDashboard" runat="server" />
                            <asp:HiddenField ID="hdnVendorId" runat="server" />
                            <div class="GridContainer" style="padding-top: 5px;">
                                <div style="overflow: auto; overflow-y: hidden" id="bigDiv" onscroll='SetScrollPosition()'>
                                    <asp:UpdatePanel ID="upJobPostings" runat="server">
                                        <ContentTemplate>
                                            <asp:ListView ID="lsvJobPosting" runat="server" EnableViewState="true" DataKeyNames="Id"
                                                OnItemDataBound="lsvJobPosting_ItemDataBound" OnItemCommand="lsvJobPosting_ItemCommand"
                                                OnPreRender="lsvJobPosting_PreRender" DataSourceID="odsRequisitionList">
                                                <LayoutTemplate>
                                                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                                        <tr id="trHeadLevel" runat="server">
                                                            <th style="white-space: nowrap; width: 95px !important;">
                                                                <asp:LinkButton ID="btnPostedDate" runat="server" ToolTip="Sort By Posted Date" CommandName="Sort" CommandArgument="[J].[CreateDate]"
                                                                    Width="60%" Text="Date Posted" TabIndex="2" />
                                                            </th>                                                           
                                                            <th style="white-space: nowrap; min-width: 120px">
                                                                <asp:LinkButton ID="btnJobTitle" runat="server" ToolTip="Sort By Job Title" CommandName="Sort" CommandArgument="[J].[JobTitle]"
                                                                    Width="120%" Text="Job Title" TabIndex="5" />
                                                            </th>
                                                             <th>
                                                                 Experience
                                                            </th>
                                                            <th>
                                                               Location
                                                            </th>
                                                              <th>
                                                                <asp:LinkButton ID="lnkDueDate" runat="server" ToolTip="Sort By Due Date" CommandName="Sort" CommandArgument="[J].[FinalHiredDate]"
                                                                    Width="80%" Text="Due Date" TabIndex="6" />
                                                            </th>
                                                              <th>
                                                                <asp:LinkButton ID="lnkDueinDays" runat="server" ToolTip="Sort By Due in Days" CommandName="Sort" CommandArgument="[J].[FinalHiredDate]"
                                                                    Width="90%" Text="Due in Days" TabIndex="6" />
                                                            </th>
                                                        </tr>
                                                        <tr id="itemPlaceholder" runat="server">
                                                        </tr>
                                                        <tr class="Pager">
                                                            <td colspan="6" runat="server" id="tdPager">
                                                                <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </LayoutTemplate>
                                                <EmptyDataTemplate>
                                                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                                        <tr>
                                                            <td>
                                                                No data was returned.
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EmptyDataTemplate>
                                                <ItemTemplate>
                                                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                                        <td>
                                                            <asp:Label ID="lblPostedDate" runat="server" Width="80px" />
                                                        </td>
                                                        <td>
                                                            <asp:HyperLink ID="lnkJobTitle" runat="server"  TabIndex="8" Width="120px"></asp:HyperLink>
                                                        </td>
                                                          <td>
                                                            <asp:Label ID="lblExperience" runat="server" Width="80px" />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblLocation" runat="server" Width="80px" />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblDueDate" runat="server" Width="80px" />
                                                        </td>
                                                           <td id="tdDueinDays" runat="server" style="text-align:center">
                                                            <asp:Label ID="lblDueinDays" runat="server" Width="80px" />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
</asp:Panel>
