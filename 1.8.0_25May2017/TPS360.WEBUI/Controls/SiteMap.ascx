﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteMap.ascx.cs" Inherits="TPS360.Web.UI.cltSiteMap" %>
<asp:SiteMapDataSource ID="SqlSiteMap" runat="server" SiteMapProvider="SqlSiteMap"
    ShowStartingNode="false" />
<div style="padding: 15px;">
    <div class="MainBox" style="width: 100%;">
        <div class="BoxHeader">
            <div class="BoxHeaderContainer">
                <div class="TitleBoxLeft">
                    <asp:Image ID="imgArrow" runat="server" SkinID="sknArrow" AlternateText="" /></div>
                <div class="TitleBoxMid">
                    Site Map Editor</div>
            </div>
        </div>
        <div class="HeaderDevider">
        </div>
        <div class="MasterBoxContainer">
            <div class="ChildBoxContainer">
                <div style="clear: both">
                </div>
                <div class="BoxContainer">
                    <asp:UpdatePanel ID="pnlDynamic" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="TableRow" style="text-align: center">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblMenuType" runat="server" Text="Menu Type"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlMenuType" CssClass="CommonDropDownList" runat="server" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlMenuType_SelectedIndexChanged" Width="200" />
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblParentSiteMap" runat="server" Text="Parent Menu"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlParentSiteMap" CssClass="CommonDropDownList" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox ID="txtTitle" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                                    <span class="RequiredField">*</span>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormValidatorContent" style =" margin-left :42%">
                                    <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle"
                                        ErrorMessage="Please enter title." EnableViewState="False" Display="Dynamic" ValidationGroup ="Sitemap"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblDescription" runat="server" Text="Description"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="CommonTextBox" TextMode="MultiLine"
                                        Width="300" Rows="4" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblUrl" runat="server" Text="Url"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlUrl" CssClass="CommonDropDownList" runat="server" />
                                </div>
                            </div>                            
                            <div class="TableRow" style="text-align: center">
                                <div class="TableFormLeble">
                                </div>
                                <div class="TableFormContent">
                                    <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup ="Sitemap" />
                                    <br />
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:Image ID="imgRightTopImage" runat="server" SkinID="sknRightTopImage" AlternateText="" />
                <asp:Image ID="imgBoxLeftBottom" runat="server" SkinID="sknBoxLeftBottom" AlternateText="" />
                <asp:Image ID="imgBoxRightBottom" runat="server" SkinID="sknBoxRightBottom" AlternateText="" />
            </div>
        </div>
    </div>
</div>