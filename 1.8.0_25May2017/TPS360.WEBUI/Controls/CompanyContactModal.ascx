﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompanyContactModal.ascx.cs" Inherits="TPS360.Web.UI.CompanyContactModal"  %>
<style type="text/css">
.TableFormLeble
{
font-weight:bold;
}
.TableFormContent
{
	overflow :hidden ;
	text-overflow:ellipsis;
}
</style>
<div class="GridContainer" style="width: 100%;" >
   <asp:Panel ID="pnlContact" runat="server">

        <asp:HiddenField ID="hdnTitle" runat ="server" />
           <div class="FormLeftColumn" style="width: 50%;">
             
          <div class="TableRow">
            <div class="TableFormLeble">
                First Name:
            </div>
            <div class="TableFormContent">
                <asp:Label ID="lblFirstName" runat ="server" ></asp:Label>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                Last Name:
            </div>
            <div class="TableFormContent">
                <asp:Label ID="lblLastName" runat ="server" ></asp:Label>
            </div>
        </div>
           <div class="TableRow">
            <div class="TableFormLeble">
                Email:
            </div>
            <div class="TableFormContent">
                <asp:Label ID="lblEmail" runat ="server" ></asp:Label>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                Title:
            </div>
            <div class="TableFormContent">
                <asp:Label ID="lblTitlevalue" runat ="server" ></asp:Label>
            </div>
        </div>
             
        <div class="TableRow">
            <div class="TableFormLeble">Office Phone:
            </div>
            <div class="TableFormContent">
                <asp:Label ID="lblOfficePhoneValue" runat ="server" ></asp:Label>
            </div>
          
        </div>
    
        <div class="TableRow">
            <div class="TableFormLeble">Mobile Phone:
            </div>
            <div class="TableFormContent">
                <asp:Label ID="lblMobilePhoneValue" runat ="server" ></asp:Label>
            </div>
        </div>
     
        <div class="TableRow">
            <div class="TableFormLeble">Fax:
            </div>
            <div class="TableFormContent">
                <asp:Label ID="lblFaxValue" runat ="server" ></asp:Label>
            </div>
        </div>
     
        <div class="TableRow">
            <div class="TableFormLeble">
                Is Primary Contact:
            </div>
            <div class="TableFormContent" style="vertical-align: top;">
                <asp:Label ID="lblIsPrimary" runat ="server" ></asp:Label>
            </div>
        </div>
           </div>
           <div class="FormRightColumn" style="width: 50%;">
<%--           <div class="TableRow">
            <div class="TableFormLeble">
                No Bulk Email:
            </div>
            <div class="TableFormContent">
                <asp:Label ID="lblNoBulkEmail" runat ="server" ></asp:Label>
            </div>
        </div>--%>
           <div class="TableRow">
                    <div class="TableFormLeble">Address 1:
                    </div>
            <div class="TableFormContent">
                <asp:Label ID="lblAddress1value" runat ="server" ></asp:Label>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">Address 2:
            </div>
            <div class="TableFormContent">
                <asp:Label ID="lblAddress2Value" runat ="server" ></asp:Label>
            </div>
        </div>
        <div class="TableRow"> 
          <div class="TableFormLeble">City:
            </div>
            <div class="TableFormContent">
                <asp:Label ID="lblCityValue" runat ="server" ></asp:Label>
            </div>
        </div>
       
              <div class="TableRow">
                    <div class="TableFormLeble">State:
                    </div>
                    <div class="TableFormContent">
                        <asp:Label ID="lblStateValue" runat ="server" ></asp:Label>
                </div>
                </div> 
        <div class="TableRow">
            <div class="TableFormLeble">Country:
            </div>
            <div class="TableFormContent">
                <asp:Label ID="lblCountryValue" runat ="server" ></asp:Label>
            </div>
        </div>
              
         
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label ID="lblZipCode" runat="server" Text="Zip/Postal Code"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:Label ID="lblZipValue" runat ="server" ></asp:Label>
            </div>
        </div>
           </div>
        
           
        
      
      
        
      
       
        <div class="TableRow">
            <div class="TableFormLeble" style =" width : 21%;">
               Contact Remarks:
            </div>
            <div class="TableFormContent">
              <asp:Label ID="lblContactRemarks" runat ="server" Width ="75%"   ></asp:Label>
              
            </div>
        </div>
        
    </asp:Panel>
</div>

                 