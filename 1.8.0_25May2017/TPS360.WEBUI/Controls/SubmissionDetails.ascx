﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubmissionDetails.ascx.cs" Inherits="TPS360.Web.UI.SubmissionDetails"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

  
  <style type="text/css">  
        .CalendarCSS  
        {  
            background-color:Gray  ;  
            color:Blue ;  
            border-color : Maroon ;
            }  
            .custom-calendar .ajax__calendar_container

{

 background-color:#ffc; /* pale yellow */

 border:solid 1px #666;

}

.custom-calendar .ajax__calendar_title

{

 background-color:#cf9; /* pale green */

 height:20px;

 color:#333;

}

.custom-calendar .ajax__calendar_prev,

.custom-calendar .ajax__calendar_next

{

 background-color:#aaa; /* darker gray */

 height:20px;

 width:20px;

}

.custom-calendar .ajax__calendar_today

{

 background-color:#cff;  /* pale blue */

 height:20px;

}

.custom-calendar .ajax__calendar_days table thead tr td

{

 background-color:#ff9; /* dark yellow */

 color:#333;

}

.custom-calendar .ajax__calendar_day

{

 color:#333; /* normal day - darker gray color */

}

.custom-calendar .ajax__calendar_other .ajax__calendar_day

{

 display :none ;
 color:#666; /* day not actually in this month - lighter gray color */

}
.calendarDropdown
{
	margin-left : -18px;
 margin-bottom :-3px;
}
    </style> 
    
    <script type ="text/javascript" >
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);
function pageLoaded(sender, args) {
try{
AccountsChanged();
}
catch (e)
{

}
}
    function AccountsChanged()
{

   var CompanyList=document .getElementById ('<%=ddlAccount.ClientID %>');
    var selectedCompany ='';
    if(CompanyList !=null)selectedCompany =  CompanyList.options[CompanyList .selectedIndex].value;
        var hdnAccountNo=document .getElementById ('<%=hdnAccountNo.ClientID %>');
     if(selectedCompany !='')
        hdnAccountNo .value=selectedCompany ;
    
   BringContacts();
   
}
        function WebDateChooser1_InitializeDateChooser(oDateChooser)
{
 // used to set "drop-down block" variable if calendar was already opened
 oDateChooser._myMouseDown = function()
 {
  var me = igdrp_getComboById('<%=wdcSubmissionDate.ClientID%>');
  if(me && me.isDropDownVisible())
   me._calOpenedTime = new Date().getTime();
 }
 // used to open 
 oDateChooser._myClick = function()
 {
  var me = igdrp_getComboById('<%=wdcSubmissionDate.ClientID%>');
  if(me && !me.isDropDownVisible() && (!me._calOpenedTime || me._calOpenedTime + 500 < new Date().getTime()))
   me.setDropDownVisible(true);
 }
 oDateChooser.inputBox.onmousedown = oDateChooser._myMouseDown;
 oDateChooser.inputBox.onclick = oDateChooser._myClick;
//alert('ok');
}


function BringContacts()
{

 var hdnAccountNo=document .getElementById ('<%=hdnAccountNo.ClientID %>');

selectedCompany=hdnAccountNo .value;

 var requestUrl = "../AJAXServers/CompanyContactAjaxServer.aspx" + "?SelectedCompanyId=" + encodeURIComponent(selectedCompany) + "&FillEmail=Email";

	CreateXmlHttp();
	if(XmlHttp)
	{
		XmlHttp.onreadystatechange = HandleAccountResponse;
		XmlHttp.open("GET", requestUrl,  true);
		XmlHttp.send(null);		
	}
	
}
var XmlHttp;
function CreateXmlHttp()
{
	//Creating object of XMLHTTP in IE
	try
	{
		XmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		catch(oc)
		{
			XmlHttp = null;
		}
	}
	//Creating object of XMLHTTP in Mozilla and Safari 
	if(!XmlHttp && typeof XMLHttpRequest != "undefined") 
	{
		XmlHttp = new XMLHttpRequest();
	}
}
function HandleAccountResponse()
{
	// To make sure receiving response data from server is completed
	if(XmlHttp.readyState == 4)
	{
		// To make sure valid response is received from the server, 200 means response received is OK
		if(XmlHttp.status == 200)
		{			
		
			ClearAndSetAccountContacts(XmlHttp.responseText);
			
		}
		else
		{
			//alert("There was a problem retrieving data from the server." );
		}
	}
}

function CheckEmailExistanceinSubmission(text)
{
  var hdnReceiver=document .getElementById ('<%=hdnReceiver.ClientID %>');
  var arry=hdnReceiver .value.split(";");
  if(arry.length>0)
  {
    for(var i=0;i<arry.length;i++)
    {
      if(text ==arry[i])return true ;
    }
    return false ;
  }
}
//Clears the contents of state combo box and adds the states of currently selected country
function ClearAndSetAccountContacts(CompanyNode)
{
  $('#'+'<%=chkListSubmittedTo.ClientID %>').html('');
  var xml=$.parseXML(CompanyNode);
  $(xml ).find('contact').each(function (){
    var Id= $(this).find('Id').text();
	var Name= $(this).find('Name').text();
	    var tableRef = document.getElementById('<%=chkListSubmittedTo.ClientID %>');
        if(Name!="Please Select")
        {
                var checked=''
                if(CheckEmailExistanceinSubmission(Id))
                    checked ='checked=checked';
                if(Name !='')
                {
                    var newrow='<tr><td><input type =checkbox value ='+ Id +' '+checked +'>' +  Name + '</input>';
                   $('#'+'<%=chkListSubmittedTo.ClientID %>').append(newrow );
                }
          }  
 });
	
}
function GetContactsEmail()
{
    var selectedValues='';
    $("#"+'<%=chkListSubmittedTo.ClientID %>').find('input').each(function (){
        if($(this).is(':checked'))
        {
            if(selectedValues!='')selectedValues +=';';
               selectedValues =  selectedValues + $(this).val();
        }
    });
     $("#"+'<%=hdnReceiver.ClientID %>').val(selectedValues );
}
function ValidateSelection(sender,args)
{
var account=document .getElementById ('<%=ddlAccount.ClientID %>');

if(account !=null)
{
if( account.options[account .selectedIndex].value=='0')
{
   args .IsValid=false  ;
}
else 
{
    args .IsValid=true ;
}
}
else args .IsValid=true;
}
    </script>
     <style type="text/css">
        .front{ position : relative ;}
        </style>
<asp:UpdatePanel ID="upSubmit" runat ="server" >
<ContentTemplate >

<asp:HiddenField ID="hfMemberId" runat ="server" />
<asp:HiddenField ID="hfJobPostingId" runat ="server" />
<asp:HiddenField ID ="hfStatusId" runat ="server" />
<asp:HiddenField ID ="hfCurrentMemberId" runat ="server" />
<asp:HiddenField ID="hdnReceiver" runat ="server" />
<asp:HiddenField ID ="hfMemberHiringDetailsId" runat ="server" Value ="0" />
<asp:HiddenField ID ="hdnBulkAction" runat ="server" Value ="" />
<div class ="TableRow">
    <div class ="TableFormLeble">Date Submitted:</div>
    <div class ="TableFormContent">
<asp:TextBox   
            ID="txtSubmissionDate"  
            runat="server"  Width ="100px"
            >  
        </asp:TextBox>  
           <ajaxToolkit:CalendarExtender   
            ID="wdcSubmissionDate"  
            runat="server"  
            TargetControlID="txtSubmissionDate"    
            CssClass="custom-calendar"   
             PopupButtonID ="imgShow"    EnableViewState ="true"  
               
            >  
        </ajaxToolkit:CalendarExtender>  
      
       
        <asp:ImageButton ID="imgShow" runat ="server"  CssClass ="calendarDropdown"  ImageUrl ="~/Images/downarrow.gif" /> 

     <%--<asp:TextBox   ID="txtDateOffered"  runat="server"  Width ="100px" >  </asp:TextBox>  --%>
    <span class ="RequiredField">*</span>
    </div>
</div>

<div class ="TableRow">
 <div class="TableFormValidatorContent" style=" margin-left :42%">
                <asp:RequiredFieldValidator ID="rfvDateSubmitted" runat="server" ControlToValidate="txtSubmissionDate"
                    ErrorMessage="Please select Date." EnableViewState="False" Display="Dynamic" EnableClientScript ="true" 
                    ValidationGroup="SubmisstionDetails">
                </asp:RequiredFieldValidator>
            </div>
</div>
<div class ="TableRow">
    <div class ="TableFormLeble">Submitted By:
    </div>
    <div class ="TableFormContent">
        <asp:DropDownList ID="ddlSubmittedBy" runat ="server" CssClass ="CommonDropDownList" ValidationGroup="SubmisstionDetails"></asp:DropDownList>
        <span class ="RequiredField">*</span>
    </div>
</div>
<div class ="TableRow">
 <div class="TableFormValidatorContent" style=" margin-left :42%">
                <asp:RequiredFieldValidator ID="rfvSubmittedBy" runat="server" ControlToValidate="ddlSubmittedBy"
                    ErrorMessage="Please select Submitted By." EnableViewState="False" Display="Dynamic"
                    ValidationGroup="SubmisstionDetails" >
                </asp:RequiredFieldValidator>
            </div>
</div>
<div class ="TableRow">
    <div class ="TableFormLeble"><asp:Label ID="lblClient" runat ="server" Text ="Account"></asp:Label>:
    </div>
    <div class ="TableFormContent">
        <asp:Label ID="lblAccount" runat ="server" EnableViewState ="true"  ></asp:Label>
        <asp:HiddenField ID ="hdnAccountNo" runat ="server" EnableViewState ="true"  />
        <asp:DropDownList ID="ddlAccount" runat ="server" 
            CssClass ="CommonDropDownList" onchange="javascript:AccountsChanged()" 

            ></asp:DropDownList>

         
             <span class ="RequiredField">*</span>

    </div>
</div>
 <div class ="TableRow">
 <div class="TableFormValidatorContent" style=" margin-left :42%">
               
                <asp:CustomValidator ID ="csvAccount" runat ="server" Display ="Dynamic" ControlToValidate ="ddlAccount" ValidationGroup ="SubmisstionDetails" ClientValidationFunction ="ValidateSelection"
                 ErrorMessage ="Please Select Account."></asp:CustomValidator>
            </div>
</div>
<div class ="TableRow" id ="dvSubmittedTo" runat ="server" >
    <div class ="TableFormLeble">Submitted To:
    </div>
    <div class ="TableFormContent"> 
        <div style="border: solid 1px #7f7f7f;border-collapse:collapse;color: #333333; width:40%; height :100px; overflow :auto ">
        <asp:CheckBoxList ID="chkListSubmittedTo" runat ="server">
        <asp:listitem Value="0">No Contacts</asp:listitem>
        </asp:CheckBoxList>
        <asp:HiddenField ID ="hdnSelectedContactID" runat ="server" />
        </div> 
    </div>
</div>
<div class ="TableRow" align="center" >
    <asp:Button ID ="btnSave" runat ="server" Text ="Save" CssClass ="CommonButton"  OnClientClick ="javascript:GetContactsEmail()"
        OnClick="btnSave_Click" ValidationGroup ="SubmisstionDetails" />
</div>
</ContentTemplate>
</asp:UpdatePanel>