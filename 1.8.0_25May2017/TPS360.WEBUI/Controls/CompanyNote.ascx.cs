﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CompanyNote.ascx.cs
    Description: This is the ascx.cs page used to dispaly,add,edit and delete the company notes.
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              Jan-22-2008          Kalyani P          Defect ID: 9046; Added code to delete the company note 
    0.2              Nov-16-2009          Rajendra           Defect ID: 11579; Added code in BindList();
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Web.UI.HtmlControls;
using System.Globalization;
using TPS360.Common.Shared;
using System.Collections.Generic;
using System.Text.RegularExpressions;
namespace TPS360.Web.UI
{
    public partial class Controls_CompanyNote : CompanyBaseControl
    {
        #region Member Variables

        CommonNote  _companyNote;
        
        #endregion

        #region Properties
        public string AccountType
        {
            get
            {

                switch (CurrentCompanyStatus)
                {
                    case CompanyStatus.Client:
                        return "Company";
                    case CompanyStatus.Department:
                        return "BU";
                    case CompanyStatus.Vendor:
                        return "Vendor";
                    default:
                        return "";
                }

            }

        }
        public int CompanyNoteId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_CompanyNoteId"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_CompanyNoteId"] = value;
            }
        }

        private CommonNote  CurrentCompanyNote
        {
            get
            {
                if (_companyNote == null)
                {
                    if (CompanyNoteId > 0)
                    {
                        _companyNote = Facade.GetCommonNoteById(CompanyNoteId);
                    }

                    if (_companyNote == null)
                    {
                        _companyNote = new CommonNote ();
                    }
                }

                return _companyNote;
            }
        }

        #endregion

        #region Event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtNotes.Focus();
                BindList();
                MiscUtil.PopulateNotesType(ddlNoteType, Facade);
                txtSortColumn.Text = "btnDateTime";
                txtSortOrder.Text = "Desc";
                PlaceUpDownArrow();
            }
        }

        protected void btnSubmitNotes_Click(object sender, EventArgs e)
        {
            SaveNote();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        #endregion

        #region Method

        public void BindList()
        {
            odsNotesList.SelectParameters["companyId"].DefaultValue = CurrentCompanyId.ToString();
            this.lsvCompanyNotes.DataBind();
        }

        private void SaveNote()
        {
            if (IsValid)
            {
                try
                {
                    CommonNote commonNote = BuildCommonNote();
                    
                    if (commonNote.IsNew)
                    {
                        commonNote.CreatorId = CurrentMember.Id;
                        commonNote = Facade.AddCommonNote(commonNote);

                        if (commonNote != null)
                        {
                            CompanyNote note = BuildCompanyNote();
                            note.CommonNoteId = commonNote.Id;                            
                           
                            Facade.AddCompanyNote(note);
                        }


                        MiscUtil.ShowMessage(lblMessage, AccountType +  " Note has been added successfully.", false);

                    }
                    else
                    {
                        commonNote.UpdatorId = CurrentMember.Id;
                        Facade.UpdateCommonNote(commonNote);
                        
                        MiscUtil.ShowMessage(lblMessage, AccountType + " Note has been updated successfully.", false);
                    }
                    Clear();
                    BindList();
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }
        
        private CommonNote BuildCommonNote()
        {
            CommonNote commonNote = null;

            if (!CurrentCompanyNote.IsNew)
            {
                commonNote = CurrentCompanyNote;
            }
            else
            {
                commonNote = new CommonNote();
            }

            if (!StringHelper.IsEqual(ddlNoteType.SelectedItem.Text, UIConstants.DROP_DOWNL_ITEM_PLEASE_SELECT))
            {
                commonNote.NoteCategoryLookupId = int.Parse(ddlNoteType.SelectedValue);
            }

            commonNote.NoteDetail = txtNotes.Text=MiscUtil .RemoveScript(txtNotes.Text.Trim());
            commonNote.CreatorId = base.CurrentMember.Id;
            commonNote.UpdatorId = base.CurrentMember.Id;

            return commonNote;
        }

        private CompanyNote BuildCompanyNote()
        {
            CompanyNote companyNote = new CompanyNote();
            companyNote.CompanyId = base.CurrentCompanyId;
            companyNote.CreatorId = CurrentMember.Id;
            return companyNote;
        }

        private void PrepareView()
        {
            CommonNote  companyNote = CurrentCompanyNote;
            txtNotes.Text =MiscUtil .RemoveScript ( companyNote.NoteDetail,string .Empty );
            ControlHelper.SelectListByValue(ddlNoteType, StringHelper.Convert(companyNote.NoteCategoryLookupId ));
        }

        private void Clear()
        {
            txtNotes.Text = string.Empty;
            ddlNoteType.SelectedIndex = 0;
            CompanyNoteId = 0;
        }
        private void PlaceUpDownArrow()
        {

            try
            {
                LinkButton lnk = (LinkButton)lsvCompanyNotes.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", ( txtSortOrder .Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        #endregion

        #region ListView Events

        protected void lsvCompanyNotes_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _IsAdmin = base.IsUserAdmin; 
            
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                CommonNote  companyNote = ((ListViewDataItem)e.Item).DataItem as CommonNote ;

                if (companyNote != null)
                {
                    Label lblNote = (Label)e.Item.FindControl("lblNote");
                    Label lblNoteType = (Label)e.Item.FindControl("lblNoteType");
                    Label lblCreatedDate = (Label)e.Item.FindControl("lblCreatedDate");
                    Label lblCreatedBy = (Label)e.Item.FindControl("lblCreatedBy");

                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    GenericLookup lookup = null;

                    if (!companyNote.IsNew )
                    {
                        lookup = Facade.GetGenericLookupById(companyNote.NoteCategoryLookupId );
                    }

                    if (lookup != null)
                    {
                        lblNoteType.Text = lookup.Name;
                    }

                    if (companyNote.CreatorId  > 0)
                    {
                        string  memberName = Facade.GetMemberNameById(companyNote.CreatorId );
                        lblCreatedBy.Text = memberName;
                    }

                    lblNote.Text = companyNote.NoteDetail ;
                    lblCreatedDate.Text = companyNote.CreateDate.ToShortDateString() + " " + companyNote.CreateDate.ToShortTimeString();// StringHelper.Convert(UIConstants.SHORT_DATE_FORMAT, companyNote.CreateDate);
                    btnDelete.Visible = _IsAdmin;

                    btnDelete.OnClientClick = "return ConfirmDelete('"+AccountType +" Note')";
                    btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(companyNote.Id );
                }
            }
        }

        protected void lsvCompanyNotes_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    CompanyNoteId = id;

                    PrepareView();
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                         if (Facade.DeleteCommonNoteById (id ))
                         {
                             BindList();
                             if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                             {
                                 MiscUtil.ShowMessage(lblMessage, "Department note has been successfully deleted.", false);
                             }
                             else
                                 MiscUtil.ShowMessage(lblMessage, "Company note has been successfully deleted.", false);
                         }
                         if (CompanyNoteId == id)
                             Clear();
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }

        protected void lsvCompanyNotes_PreRender(object sender, EventArgs e)
        {
            PlaceUpDownArrow();

            if (lsvCompanyNotes != null)
            {
                HtmlTableCell tdpager = (HtmlTableCell)lsvCompanyNotes.FindControl("tdPager");
                ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCompanyNotes.FindControl("pagerControl_Requisition");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }
                    HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                    if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "MemberCompanyNoteRowPerPage";
                }
            }         
        }
        #endregion
    }
}
