<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VendorCandidatePerformance.ascx.cs"
    Inherits="VendorCandidatePerformance" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
<asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
<asp:Panel ID="widgetBody" runat="server">
    <asp:ObjectDataSource ID="odsCandidateListCandidatePerformance" runat="server" SelectMethod="GetPagedCandidatePerformance"
        TypeName="TPS360.Web.UI.CandidateDataSource" SelectCountMethod="GetListCountCandidatePerformance"
        EnablePaging="True" SortParameterName="sortExpression">
        <SelectParameters>
            <asp:Parameter Name="memberId" Type="Int32" />
              <asp:Parameter Name="IsVendor" Type="Boolean"  DefaultValue ="false" />
                <asp:Parameter Name="IsVendorContact" Type="Boolean"  DefaultValue ="true" />
                <asp:Parameter Name ="DateFrom"  Type ="DateTime" />
                <asp:Parameter Name ="DateTo"  Type ="DateTime" />
            <asp:Parameter Name="SortOrder" DefaultValue="asc" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ListView ID="lsvCandidateListCandidatePerformance" runat="server" DataSourceID="odsCandidateListCandidatePerformance"
        EnableViewState="true" DataKeyNames="Id" OnItemDataBound="lsvCandidateListCandidatePerformance_ItemDataBound"
        OnItemCommand="lsvCandidateListCandidatePerformance_ItemCommand" OnPreRender="lsvCandidateListCandidatePerformance_PreRender">
        <LayoutTemplate>
            <table id="tlbTemplate" class="Grid" cellspacing="0" border="0">
                <tr runat="server" id="trRecentApplicant">
                
                    <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnID" runat="server" ToolTip="Sort By Candidate ID" CommandName="Sort"
                            CommandArgument="[C].[ID]" Text="Candidate ID" />
                    </th>      
                    <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnName" runat="server" ToolTip="Sort By Candidate Name" CommandName="Sort"
                            CommandArgument="[C].[FirstName]" Text="Candidate Name" />
                    </th>
                    <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnEmail" runat="server" ToolTip="Sort By Email ID" CommandName="Sort"
                            CommandArgument="[C].[PrimaryEmail]" Text="Email ID" />
                    </th>                    
                     <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnJobTitle" runat="server" ToolTip="Sort By Job Title" CommandName="Sort"
                            CommandArgument="[C].[CellPhone]" Text="Job Title" />
                    </th>      
                    <th style="min-width: 100px; white-space: nowrap; text-align:center">
                     <asp:Label ID="lblProcessDays" runat="server" ToolTip="Process Days" Text="Process Days" />
                       <%-- <asp:LinkButton ID="btnProcessDays" runat="server" Text="Process Days" />--%>
                    </th>
                    <th style="min-width: 100px; white-space: nowrap;">
                        <asp:Label ID="lblStatus" runat="server" ToolTip="Status" Text="Status" />
                    </th>
                  
                </tr>
                <tr id="itemPlaceholder" runat="server">
                </tr>
                <tr class="Pager">
                    <td colspan="6">
                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <EmptyDataTemplate>
            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                style="width: 100%; margin: 0px 0px;">
                <tr>
                    <td>
                        No candidates available.
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                 <td>
                    <asp:Label ID="lblID" runat="server" />
                </td>
                <td>
                    <asp:HyperLink ID="lnkCandidateName"  runat="server"></asp:HyperLink>
                </td>
                <td>
                    <asp:Label ID="lblEmail" runat="server" />
                </td>
                 <td>
                    <asp:Label ID="lblJobTitle" runat="server" />
                </td>
                 <td runat="server" style="text-align:center">
                    <asp:Label ID="lblProcessDays" runat="server" />
                </td>
                 <td>
                    <asp:Label ID="lblStatuss" runat="server" />
                </td>
               
            </tr>
        </ItemTemplate>
    </asp:ListView>
</asp:Panel>
