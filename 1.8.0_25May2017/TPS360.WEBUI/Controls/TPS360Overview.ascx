﻿<%--
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: TPS360Overview.ascx
    Description: This is the control which used to display the overview for candidate/consultant
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author               MOdification
    -------------------------------------------------------------------------------------------------------------------------------------------------------
    0.1             Dec-04-2008            Jagadish              Defect ID: 9350; Added ObjectDataSource for Listview "lsvExperience".
    0.2             Jan-22-2009            Gopala Swamy          Enhancement Id:9750: modified "lsvJobCart" control according to the specification
    0.3             May-5-2009             Gopalas Swamy         Enhancement Id:10426; Made visibility false
    0.4             Aug-27-2009            Gopala Swamy J        Enhancement Id:11280; Made visibility false
-------------------------------------------------------------------------------------------------------------------------------------------------------------       
--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TPS360Overview.ascx.cs"
    Inherits="TPS360.Web.UI.ControlTPS360Overview" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/MemberSkillsEditor.ascx" TagName="Skills" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/MemberCertificationEditor.ascx" TagName="Certification"
    TagPrefix="License" %>
<%@ Register Src="~/Controls/MemberEducationEditor.ascx" TagName="Education" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MemberExperienceEditor.ascx" TagName="Experience" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MemberInterviewSchedule.ascx" TagName="Interview" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MemberJobCarts.ascx" TagName="JobCart" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/MemberInterviewRejection.ascx" TagName="Rejection" TagPrefix="ucl" %>

<script src="../js/AjaxScript.js" type="text/javascript"></script>

<script src="../js/AjaxModal/Main.js" type="text/javascript"></script>

<script type="text/javascript">
Sys.Application.add_load(function() {

activateResumeTab();
});
 function Status_OnChange(s,p)
     {
          var status=document .getElementById (s);
          var hf=document .getElementById (p);
          hf.value=status .options[status .selectedIndex].value;
     }
     function CallBackStatusChanges(ddl,hdn)
     {
return ;
     try{
       var status=document .getElementById (ddl );
       
      for(var i=0;i<status .length;i++)
      {
      if(status .options[i].value==hdn)
      {
      status.selectedIndex = i;
      }
      }
      }
      catch (e)
      {
      }
     }
     
       function activateResumeTab()
     
     {
     if($('#<%= hdnisPostback.ClientID %>').val()=="1")
     {
     $('#tab1').removeClass('active');$('#tab7').addClass('active');
     $('#lijobcart').removeClass('active');
     $('#liresume').addClass('active');
     $('#<%= hdnisPostback.ClientID %>').val("0")
     }
     }
</script>

<asp:HiddenField ID="hdnisPostback" runat="server" Value="0" />
<div class="tabbable">
    <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
        <li class="active" id="lijobcart"><a href="#tab1" data-toggle="tab">Job Cart</a></li>
        <li><a href="#tab2" data-toggle="tab">Interviews</a></li>
        <li><a href="#tab3" data-toggle="tab">Objective & Summary</a></li>
        <li><a href="#tab4" data-toggle="tab">Skills</a></li>
        <li><a href="#tab5" data-toggle="tab">Experience</a></li>
        <li><a href="#tab6" data-toggle="tab">Education</a></li>
        <li id="liresume"><a href="#tab7" data-toggle="tab">Resumes</a></li>
        <li><a href="#tab8" data-toggle="tab">Reason for Rejection</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <ucl:JobCart runat="server" ID="uclJobCart"></ucl:JobCart>
        </div>
        <div class="tab-pane" id="tab2" style="width: 100%; overflow: auto;">
            <asp:UpdatePanel ID="upInterview" runat="server">
                <ContentTemplate>
                    <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
                    <%-- <ucl:Interview ID ="uclInterview" runat ="server" />--%>
                    <asp:ObjectDataSource ID="odsInterviewSchedule" runat="server" SelectMethod="GetPaged"
                        TypeName="TPS360.Web.UI.MemberInterviewDataSource" SelectCountMethod="GetListCount"
                        EnablePaging="True" SortParameterName="sortExpression">
                        <SelectParameters>
                            <asp:Parameter Name="CandidateId" DefaultValue="0" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ListView ID="lsvInterviewSchedule" runat="server" DataKeyNames="Id" DataSourceID="odsInterviewSchedule"
                        EnableViewState="true" OnItemDataBound="lsvInterviewSchedule_ItemDataBound" OnItemCommand="lsvInterviewSchedule_ItemCommand"
                        OnPreRender="lsvInterviewSchedule_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                <tr>
                                    <th style="width: 90px !important;">
                                        <asp:LinkButton ID="btnDateTime" runat="server" ToolTip="Sort By Date & Time" CommandName="Sort"
                                            CommandArgument="[I].[StartDateTime]" Text="Date & Time" />
                                    </th>
                                    <th style="white-space: nowrap;">
                                        <asp:LinkButton ID="btnJobTitle" runat="server" CommandName="Sort" CommandArgument="[JP].[JobTitle]"
                                            Text="Requisition" />
                                    </th>
                                    <th style="white-space: nowrap;">
                                        <asp:LinkButton ID="btnClient" runat="server" CommandName="Sort" CommandArgument="[C].[CompanyName]"
                                            Text="Account" />
                                    </th>
                                    <th style="white-space: nowrap;">
                                        <asp:LinkButton ID="btnTitle" runat="server" CommandName="Sort" CommandArgument="[I].[Title]"
                                            Text="Title" />
                                    </th>
                                    <th style="white-space: nowrap;">
                                        <asp:LinkButton ID="btnLocation" runat="server" CommandName="Sort" CommandArgument="[I].[Location]"
                                            Text="Location" />
                                    </th>
                                    <th style="white-space: nowrap;">
                                        <asp:LinkButton ID="btnType" runat="server" CommandName="Sort" CommandArgument="[GL].[Name]"
                                            Text="Type" />
                                    </th>
                                    <th style="white-space: nowrap;">
                                        <asp:LinkButton ID="btnHeaderInterviewers" runat="server" CommandName="Sort" CommandArgument="[INV].[InterviewersName]"
                                            Text="Interviewers"></asp:LinkButton>
                                    </th>
                                    <th style="white-space: normal;">
                                        <asp:Label ID="lblNotes" runat="server" Text="Notes"></asp:Label>
                                    </th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td id="tdPager" runat="server" colspan="8">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No interview data available.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td>
                                    <asp:Label ID="lblDateTime" runat="server" Width="100px" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lblJobTitle" runat="server" Width="90px"></asp:LinkButton>
                                </td>
                                <td>
                                    <asp:Label ID="lblClient" runat="server" Width="80px" />
                                </td>
                                <td>
                                    <asp:Label ID="lblTitle" runat="server" Width="100px" />
                                </td>
                                <td>
                                    <asp:Label ID="lblLocation" runat="server" Width="70px" />
                                </td>
                                <td>
                                    <asp:Label ID="lblType" runat="server" Width="70px" />
                                </td>
                                <td>
                                    <asp:Label ID="lblInterviewers" runat="server" Width="120px" />
                                </td>
                                <td>
                                    <asp:Label ID="lblNotes" runat="server" Width="70px" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="tab-pane" id="tab3">
            <div>
                <div style="text-align: left; clear: both;">
                    <asp:Label EnableViewState="false" ID="lblObjective" runat="server" Text=""></asp:Label>
                </div>
                <div style="text-align: left; clear: both;">
                    <asp:Label EnableViewState="false" ID="lblSummery" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div id="divObjectiveandSummary" runat="server" visible="false">
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                    <tr>
                        <td runat="server" id="tdObjectiveSummary">
                            No Objective & Summary available.
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="tab-pane" id="tab4">
            <uc1:Skills ID="uclSkills" runat="server" />
        </div>
        <div class="tab-pane" id="tab5">
            <ucl:Experience runat="server" ID="uclExperience" />
        </div>
        <div class="tab-pane" id="tab6">
            <ucl:Education ID="uclEducataion" runat="server" />
            <hr class="divider" style="margin-top: 10px; margin-bottom: 10px;"></hr>
            <License:Certification ID="uclCertification" runat="server" />
        </div>
        <div class="tab-pane" id="tab7">
            <div style="text-align: left;">
                <asp:Label EnableViewState="false" ID="lblCopyPasteResume" runat="server" Text=""></asp:Label>
            </div>
            <div id="divCopyPasteResume" runat="server" visible="false">
                <table id="Table1" class="EmptyDataTable alert alert-warning" runat="server">
                    <tr>
                        <td>
                            No Copy/Paste Resume available.
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="tab-pane" id="tab8">
            <ucl:Rejection runat="server" ID="uclRejection" />
        </div>
    </div>
</div>
