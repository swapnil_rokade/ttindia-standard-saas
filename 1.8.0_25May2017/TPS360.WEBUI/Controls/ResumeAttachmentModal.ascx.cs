﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.IO;
using Sovren;
using System.Text;
namespace TPS360.Web.UI
{
    public partial class ResumeAttachmentModal :BaseControl 
    {
        #region Member Variables
        MemberObjectiveAndSummary _memberObjectiveAndSummary;
        #endregion

        #region Properties

        public int _MemberId = 0;
        public int MemberID
        {
            get
            {
                return _MemberId;
            }
            set
            {
                _MemberId = value;
               
            }
        }

        MemberObjectiveAndSummary CurrentMemberObjectiveAndSummary
        {
            get
            {
                if (_memberObjectiveAndSummary == null)
                {
                    if (MemberID == 0)
                        this.MemberID = Int32.Parse(hfMemberId.Value);
                    if (MemberID > 0)
                    {
                        _memberObjectiveAndSummary = Facade.GetMemberObjectiveAndSummaryByMemberId(MemberID);
                    }

                    if (_memberObjectiveAndSummary == null)
                    {
                        _memberObjectiveAndSummary = new MemberObjectiveAndSummary();
                    }
                }

                return _memberObjectiveAndSummary;
            }
        }

        public delegate void ResumeAttEventHandler();
        public event ResumeAttEventHandler ResumeAttachment;

        #endregion

        #region Methods
        private  void BindList()
        {
                lsvExistingResume.DataSourceID  = "odsDocumentList";
                lsvExistingResume.DataBind();
        }

        private void PrepareView()
        {
            MiscUtil.PopulateDocumentType(ddlDocType, Facade);
           // ddlDocType.SelectedIndex = 26;  //commented by pravin khot on 30/June/2016
            if(CurrentMemberObjectiveAndSummary !=null )
                txtCopyPasteResume.Text = CurrentMemberObjectiveAndSummary.CopyPasteResume;
        }

        public void clear()
        {
            txtDocTitle.Text = string.Empty;
            txtDocmentTitle.Text = string.Empty;
        }

        private void EnableDisableValidator(string strDocumentType)
        {
            if (strDocumentType.ToLower().IndexOf("photo") >= 0)
            {
                revPhoto.Visible = true;
                revVideo.Visible = false;
                revDocument.Visible = false;
            }
            else if (strDocumentType.ToLower().IndexOf("video") >= 0)
            {
                revPhoto.Visible = false;
                revVideo.Visible = true;
                revDocument.Visible = false;
            }
            else
            {
                revPhoto.Visible = false;
                revVideo.Visible = false;
                revDocument.Visible = true;
            }
        }

        private bool CheckFileSize()
        {
            int fileSize = Convert.ToInt32(fuDocPath.FileContent.Length / (1024 * 1024));
            if (fileSize > ContextConstants.MAXIMUM_UPLOAD_SIZE)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private string GetDocumentLink(string strFileName, string strDocumenType, int memberId)
        {
            string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, memberId, strFileName, strDocumenType, true);

            if (strFilePath != strFileName)
            {
                return "<a href='" + strFilePath + "' TARGET='_blank' >" + strFileName + "</a>";
            }
            else
            {
                return strFileName + "(File not found)";
            }
        }   

        private void UploadDocument()
        {
            if (fuDocPath .HasFile )
            {
                string _fileName = string.Empty;
                bool boolError = false;
                string UploadedFilename = Convert.ToString(fuDocPath.FileName);
                if (ddlDocType.SelectedItem.Text == "Word Resume")  
                {
                    string[] FileName_Split = UploadedFilename.Split('.');
                    string ResumeName = FileName_Split [0] + " - Resume." + FileName_Split[FileName_Split.Length - 1];
                    UploadedFilename = ResumeName;
                }
                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, MemberID, UploadedFilename, ddlDocType.SelectedItem.Text, false);
                if (CheckFileSize())
                {
                    fuDocPath.SaveAs(strFilePath);

                    if (File.Exists(strFilePath))
                    {
                        MemberDocument memberDocument = Facade.GetMemberDocumentByMemberIdTypeAndFileName(MemberID, ddlDocType.SelectedItem.Text.Trim(), UploadedFilename);
                        if (memberDocument == null)
                        {
                            MemberDocument newDoc = new MemberDocument();
                            newDoc.FileName = UploadedFilename;
                            newDoc.FileTypeLookupId = Int32.Parse(ddlDocType.SelectedValue);
                            newDoc.Description = string.Empty;
                            newDoc.MemberId = MemberID ;
                            newDoc.Title = MiscUtil.RemoveScript(txtDocTitle.Text);
                            if (newDoc.Title.ToString() != string.Empty)
                            {
                                newDoc = Facade.AddMemberDocument(newDoc);
                                if (hdnSelRes.Value == string.Empty)
                                    hdnSelRes.Value = newDoc.Id.ToString();
                                else
                                    hdnSelRes.Value = hdnSelRes.Value + "," + newDoc.Id.ToString();
                                
                                MiscUtil.ShowMessage(lblMessage, "Document Added Sucessfully.", false );
                            }
                        }
                        else
                        {
                            MiscUtil.ShowMessage(lblMessage, "Already this document available.", true);
                        }
                    }
                }
                else
                {
                    boolError = true;
                   MiscUtil.ShowMessage(lblMessage, "File size should be less than 10 MB.", true);
                }
                
            }

        }

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvExistingResume.FindControl(hdnSortColumn.Value);

                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));


            }
            catch
            {
            }

        }

        private void RemoveContactInfor()
        {
            string EmailPattern = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            string areaCodeRegExp = @"(?<areaCodeGroup>\(\d\d\d\))";
            string phoneRegExp = @"(?<phoneGroup>\d\d\d\-\d\d\d\d)";
            string Zippattern = @"(\d{5}-\d{4}|\d{5}|\d{9}|\d{6})";
            string usPhoneRegExp = @"^\(?[\d]{3}\)?[\s-]?[\d]{3}[\s-]?[\d]{4}$";
            if (txtCopyPasteResume.Text != string.Empty)
            {

                MatchCollection usPhoneNumbers = Regex.Matches(txtCopyPasteResume.Text, usPhoneRegExp);
                foreach (Match mat in usPhoneNumbers) 
                {
                    if (mat.Value != string.Empty) 
                    {
                        RemoveContact(mat.Value);
                    }
                }
                MatchCollection Emailcollection = Regex.Matches(txtCopyPasteResume.Text, EmailPattern);
                foreach (Match mat in Emailcollection)
                {
                    if (mat.Value != string.Empty)
                    {
                        RemoveContact(mat.Value);
                    }
                }
                MatchCollection PhoneCollection = Regex.Matches(txtCopyPasteResume.Text, areaCodeRegExp + " " + phoneRegExp);
                foreach (Match mat in PhoneCollection)
                {
                    if (mat.Value != string.Empty)
                        RemoveContact(mat.Value);
                }
                MatchCollection AddressCollection = Regex.Matches(txtCopyPasteResume.Text, Zippattern);
                foreach (Match mat in AddressCollection)
                {
                    if (mat.Value != string.Empty)
                        RemoveContact(mat.Value);
                }
                Member Details = new Member();
                Details = Facade.GetMemberById(Int32.Parse(hfMemberId.Value));
                if (Details != null)
                {
                    if (Details.PermanentAddressLine1 != string.Empty) RemoveContact(Details.PermanentAddressLine1);
                    if (Details.PermanentAddressLine2 != string.Empty) RemoveContact(Details.PermanentAddressLine2);
                    if (Details.PermanentCity != string.Empty) RemoveContact(Details.PermanentCity);
                }
            }
            
        }

        private void RemoveContact(string value)
        {
            if (value != string.Empty)
            {
                IList<string> ss = txtCopyPasteResume.Text.Split(new string[] { "</p>" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in ss)
                {
                    if (s.Contains(value))
                        txtCopyPasteResume.Text = txtCopyPasteResume.Text.Replace(s, string.Empty);
                }
            }
        }

        private void AddMemberDocument(string fileName)
        {
            MemberDocument newDoc = new MemberDocument();
            newDoc.FileName = fileName;
            newDoc.FileTypeLookupId = 55;
            newDoc.Description = string.Empty;
            newDoc.MemberId = MemberID;
            newDoc.Title = MiscUtil.RemoveScript(txtDocmentTitle.Text);
            if (newDoc.Title.ToString() != string.Empty)
            {
                newDoc = Facade.AddMemberDocument(newDoc);

               
                if (hdnSelRes.Value == string.Empty)
                    hdnSelRes.Value = newDoc.Id.ToString();
                else
                    hdnSelRes.Value = hdnSelRes.Value + "," + newDoc.Id.ToString();
            }
        }

        private void StoreResumeintoLocal(string fileName, string fileType)
        {
            String strFilePath = string.Empty;
            strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Resources/Member");

            strFilePath = strFilePath + "\\" + hfMemberId.Value;

            if (!Directory.Exists(strFilePath))
            {
                Directory.CreateDirectory(strFilePath);
            }
            strFilePath = strFilePath + "\\Word Resume";

            if (!Directory.Exists(strFilePath))
            {
                Directory.CreateDirectory(strFilePath);
            }
            if (fileType == ".pdf")
            {
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                pdfConverter.SavePdfFromHtmlStringToFile(txtCopyPasteResume.Text  , strFilePath+"\\"+fileName + fileType );
            }
            else if (fileType == ".doc")
            {
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                Byte[] bytes = encoding.GetBytes("<body>"+txtCopyPasteResume.Text+"</body>");
                MemoryStream ms = new MemoryStream(bytes);

                FileStream fs = new FileStream(strFilePath + "\\" + fileName + fileType, FileMode.Create);
                ms.WriteTo(fs);
                ms.Close();
                fs.Close();
                fs.Dispose();
            }
            else
            {
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
               
               
                string s = StripHTML(txtCopyPasteResume.Text).Replace("\r\n", "<br/>");
                s = HttpUtility.HtmlEncode(s);
                Byte[] bytes = encoding.GetBytes(s );
                MemoryStream ms = new MemoryStream(bytes);
                
                FileStream fs = new FileStream(strFilePath + "\\" + fileName + fileType, FileMode.Create);
                ms.WriteTo(fs);
                ms.Close();
                fs.Close();
                fs.Dispose();
            }

        }

        private string StripHTML(string source)
        {
            try
            {
                string result;
                result = source.Replace("\r", " ");
                result = result.Replace("\n", " ");
                result = result.Replace("\t", string.Empty);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = result.Replace("\n", "\r");
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\t)", "\t\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\r)", "\t\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\t)", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                string breaks = "\r\r\r";
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }
                return result;
            }
            catch
            {
                return source;
            }
        }

        private void CreateDocument(string fileName, string fileType)
        {
            try
            {
                MemberDocument memberDocument = Facade.GetMemberDocumentByMemberIdTypeAndFileName(MemberID, "Word Resume", fileName + fileType);
                if (memberDocument == null)
                {
                    AddMemberDocument(fileName + fileType);
                }
                else
                {
                    try
                    {
                        AddMemberDocument(fileName + "(1)" + fileType);
                        fileName = fileName + "(1)";
                    }
                    catch
                    {
                        string exit = "success";
                        int i = 2;
                        while (exit == string.Empty)
                        {
                            try
                            {
                                AddMemberDocument(fileName + "(" + i + ")" + fileType);
                                exit = string.Empty;
                                fileName = fileName + "(" + i + ")";
                            }
                            catch
                            {
                                i = i + 1;
                            }
                        }
                    }
                }

                StoreResumeintoLocal(fileName, fileType);
            }
            catch
            {
            }
        }

        #endregion

        #region PageEvents
        protected void Page_Load(object sender, EventArgs e)
        {
           
            ddlDocType.Attributes.Add("onchange", "javascript:ValidatorChange()");
            HiddenField hdncheck = (HiddenField)this.Page.Master.FindControl("cphHomeMaster").FindControl("hdncheck");
            string ApplicationSource = ConfigurationManager.AppSettings["ApplicationSource"].ToString();
            if (ApplicationSource.ToLower() != "landt") 
            {
                lnkRemoveContact.Visible = true;
            }
            if (IsPostBack)
            {
                hdncheck.Value = "s";
            }
            else
            {
                hdncheck.Value = "";
            }
            
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
           
            if (MemberID > 0)
            {

            
               // lblCandidateName.Text =pnlResume Facade.GetMemberNameById(MemberID) + " - Attach Resume";
                BindList();
                PrepareView();
                hfMemberId.Value = MemberID.ToString();
                hdnSortColumn.Value = "btnDocumentType";
                hdnSortOrder.Value = "ASC";
                PlaceUpDownArrow();
                
            }
            else
            {
                
            }
        }
        #endregion

        #region ListView Events
        protected void lsvExistingResume_PreRender(object sender, EventArgs e)
        {
            PlaceUpDownArrow();
            if (hdnSelRes .Value != string.Empty)
            {
                IList<string> ids = hdnSelRes.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string id in ids)
                {
                    foreach (ListViewDataItem item in lsvExistingResume.Items)
                    {
                        CheckBox chkitem = (CheckBox)item.FindControl("chkItemCandidate");
                        HiddenField hdnID = (HiddenField)item.FindControl("hdnID");
                        if (hdnID.Value == id)
                        {
                            chkitem.Checked = true;
                        }
                    }
                }
            }
        }

        protected void lsvExistingResume_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsDocumentList.SelectParameters["SortColumn"].DefaultValue = SortColumn.Text;
                    odsDocumentList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text;
                    AjaxControlToolkit.ModalPopupExtender ex = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ModalPopupExtender");
                    //ScriptManager.RegisterClientScriptBlock(this.Page, typeof(MiscUtil), "TabChanged", "<script>minimixeModel();</script>", false);
                    ex.Show();
                } 
            }
            catch
            {
            }
        }
      
        protected void lsvExistingResume_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberDocument document = ((ListViewDataItem)e.Item).DataItem as MemberDocument;
                if (document != null)
                {
                    Label lblFileName = (Label)e.Item.FindControl("lblFileName");
                    Label lblTitle = (Label)e.Item.FindControl("lblTitle");
                    Label lblType = (Label)e.Item.FindControl("lblType");
                    CheckBox chkitem = (CheckBox)e.Item.FindControl("chkItemCandidate");
                    HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");
                    hdnID.Value = document.Id.ToString();
                    lblTitle.Text = document.Title;
                    GenericLookup loolup = Facade.GetGenericLookupById(document.FileTypeLookupId);
                    lblType.Text = loolup != null ? loolup.Name : string.Empty;
                    lblFileName.Text = GetDocumentLink(document.FileName, loolup.Name, document.MemberId);
                }
            }
        }

        protected void odsDocumentList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["MemberID"] = MemberID > 0 ? MemberID.ToString() : hfMemberId.Value;
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "[G].[Name]";
            }
            odsDocumentList.SelectParameters["SortColumn"].DefaultValue = SortColumn.Text;
            odsDocumentList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text;
        }
        #endregion

        #region Button Events
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (hfMemberId.Value != string.Empty)
            {
                this.MemberID = Int32.Parse(hfMemberId.Value);
                UploadDocument();

                BindList();
                txtDocTitle.Text = string.Empty;
                AjaxControlToolkit.ModalPopupExtender ex = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ModalPopupExtender");
                ex.Show();
            }
        }

        protected void btnAttach_Click(object sender, EventArgs e)
        {
             
            HiddenField hdnDocumentID = (HiddenField)this.Page.Master.FindControl("cphHomeMaster").FindControl("hdnResumeDocumentID");
            hdnDocumentID.Value = hdnSelRes.Value; //hdnSelectedIDS.Value;
            AjaxControlToolkit.ModalPopupExtender ex = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ModalPopupExtender");
            ex.Hide();
            hdnSelRes.Value   = string.Empty;
            if (ResumeAttachment != null)
            {
                ResumeAttachment();
            }
            HiddenField hdncheck = (HiddenField)this.Page.Master.FindControl("cphHomeMaster").FindControl("hdncheck");
            hdncheck.Value = "";
        }
       
        protected void lnkRemoveContact_Click(object sender, EventArgs e)
        {
            RemoveContactInfor();
            AjaxControlToolkit.ModalPopupExtender ex = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ModalPopupExtender");
            ex.Show();
        }

        protected void btnAttachAsDoc_Click(object sender, EventArgs e)
        {
            if (hfMemberId .Value !=string .Empty )
            {
                this.MemberID = Int32.Parse(hfMemberId.Value);
                CurrentMemberObjectiveAndSummary.CopyPasteResume = txtCopyPasteResume.Text;
                CurrentMemberObjectiveAndSummary.UpdatorId = CurrentMember.Id;
                Facade.UpdateMemberObjectiveAndSummary(CurrentMemberObjectiveAndSummary);
                string strFilePath = string.Empty;
                if (ddlOutputType.SelectedValue == "1")
                    CreateDocument(txtDocmentTitle.Text, ".doc");
                else if (ddlOutputType.SelectedValue == "2")
                    CreateDocument(txtDocmentTitle.Text, ".pdf");
                else if (ddlOutputType.SelectedValue == "3")
                    CreateDocument(txtDocmentTitle.Text, ".txt");
                BindList();
                ddlOutputType.SelectedIndex = 0;
                txtDocmentTitle.Text = string.Empty;
                //uwtAttachedResume.SelectedTabIndex = 0;
                AjaxControlToolkit.ModalPopupExtender ex = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMaster").FindControl("ModalPopupExtender");
                //ScriptManager.RegisterClientScriptBlock(this.Page, typeof(MiscUtil), "TabChanged", "<script>minimixeModel();</script>", false);
                ex.Show();
                MiscUtil.ShowMessage(lblMessage, "Document Added Successfully", false);  
            }
        }
        #endregion
    }
}