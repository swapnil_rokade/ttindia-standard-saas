﻿<%-- 
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewerAssessmentForm.ascx
    Description         :   
    Created By          :   Prasanth
    Created On          :   16/Oct/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
   0.1              27/Dec/2015         Prasanth Kumar G        Introduced RowNumber
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
--%>
<%@ Control Language="C#" AutoEventWireup="true"  CodeFile="InterviewerAssessmentForm.ascx.cs" Inherits="TPS360.Web.UI.ControlsInterviewerAssessmentForm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
 <link href="../assets/css/chosen.css" rel="Stylesheet" />
<script type="text/javascript">


    function SetScrollPosition() {
        var hdnScroll = document.getElementById('ctl00_cphHomeMaster_uclIA_hdnScrollPos');
        var bigDiv = document.getElementById('bigDiv');
        hdnScroll.value = bigDiv.scrollLeft;
    }
    
    
    
    

  </script>
 <asp:HiddenField ID="hdnScrollPos" runat="server" />
 <asp:HiddenField ID="hfInterviewId" runat="server" Value="" />
 <asp:HiddenField ID="hfInterviewerEmail_Id" runat="server" Value="" />
 
 
<%--  0123456789 Start--%> 
   
    <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>

     <asp:ObjectDataSource ID="odsInterviewResponseList" runat="server" SelectMethod="InterviewResponseGetPaged"
      TypeName="TPS360.Web.UI.InterviewerFeedbackDataSource" SelectCountMethod="InterviewResponseGetListCount"
        EnablePaging="True" SortParameterName="sortExpression">
     <SelectParameters>
     <asp:Parameter Name="InterviewId" DefaultValue="0" />
     <asp:Parameter Name="InterviewerEmail" DefaultValue="" />
     <asp:Parameter Name = "AssessmentId" DefaultValue = "0" />
     </SelectParameters>
     </asp:ObjectDataSource>
                                                   
    <asp:UpdatePanel ID="upInterviewerAssessmentList" runat="server">
    <ContentTemplate>
     <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
    
    <div style="display:none">
    
     <div class="TabPanelHeader" >
     List of Assessments
          
    </div>
    <div class="TableRow">
    <div class="TableFormLeble">
            <asp:Label ID="Label1" runat="server" Text="Select An Assessment"></asp:Label>:
    </div>
    <div class="TableFormContent">
        <asp:DropDownList ID="DdlAssessment" runat="server" CssClass="CommonDropDownList" 
            onselectedindexchanged="DdlAssessment_SelectedIndexChanged" AutoPostBack = "true">
        </asp:DropDownList>
           
    </div>
    </div>
        <br>
    </br>
    <%-- 0123456789 End --%>
 
 
    <div class="TabPanelHeader" >
         List of Questions
          
        </div>
    
    </div>
    <div class="GridContainer">
    <div style="overflow: inherit" id="bigDiv" onscroll='SetScrollPosition()'>
        <asp:ListView ID="lsvQuestion" runat="server" DataKeyNames="Id" DataSourceID="odsInterviewResponseList"
            OnItemDataBound="lsvQuestion_ItemDataBound"  OnItemCommand="lsvQuestion_ItemCommand" OnPreRender ="lsvQuestion_PreRender">
      
       <%-- <asp:ListView ID="lsvQuestion" runat="server" DataKeyNames="Id" >  --%>

            <LayoutTemplate>
                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                    <tr>
                      
                      
                        <th style="text-align: center; white-space: nowrap; display:none;">
                           AnswerType
                        </th>
                        <th style="text-align: center; white-space: nowrap; display:none;">
                           ID
                        </th>
                        
                        <th style="text-align: center; white-space: nowrap; width:40px">
                           Row No.
                        </th>
                        <th style="text-align: center; white-space: nowrap;">
                            Question
                        </th>
                        <th style="text-align: center; white-space: nowrap;">
                            Response
                        </th>
                        <th style="text-align: center; white-space: nowrap;">
                            Rationale
                        </th>
                       <%--  <th style="width: 16%; text-align: center; white-space: nowrap;">
                            Action
                        </th>--%>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                    <tr class="Pager">
                    <td colspan="4">
                        <%--<ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />--%>
                    </td>
                    </tr>
                </table>
            </LayoutTemplate>
            <EmptyDataTemplate>
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                    <tr>
                        <td style="color: Red">
                           No Questions Available.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <ItemTemplate>
                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                     <td id = "tdAnswertype" style="display:none;">
                        <asp:Label  ID="lblAnswertype" runat="server" />
                    </td>
                     <td style="display:none;">
                        <asp:Label  ID="lblId" runat="server" Width ="40px" Font-Bold = "true" ></asp:Label>
                    </td> 
                    <td>
                        <asp:Label  ID="lblRowNo" runat="server" Width ="40px" Font-Bold = "true"></asp:Label>
                    </td> 
                    <td style="text-align: left;">
                        
                        <asp:TextBox  ID="TxtQuestion" runat="server" TextMode="MultiLine" Width="100%" Enabled="false" BackColor="#d2d2d2" Font-Bold="true"/>
                    </td>
                     <td style="text-align: left;">
                        
                        
                        <asp:TextBox  ID="TxtAnswer" runat="server" TextMode="MultiLine" Visible="false" Width="100%"/>
                        <asp:DropDownList ID="DdlAnswer" runat="server" Visible="false" CommandName = "XYZ" />
 
                    </td>
                    <td style="text-align: left;">
                        
                        <asp:TextBox  ID="TxtRationale" runat="server" TextMode="MultiLine" Width="100%" />
                    </td>
                    
                </tr>
            </ItemTemplate>
        </asp:ListView>
          </div>
    </div>
    

    </ContentTemplate>
    
    </asp:UpdatePanel>