﻿/*-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ControlMemberNotesAvailability.ascx.cs
    Description: This is the user control page used to display the availabilty of the member.
    Created By: pravin khot
    Created On: 13/July/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------       
*/
using System;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Xml.Linq;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;

namespace TPS360.Web.UI
{
    public partial class ControlMemberNotesAvailability : BaseControl
    {
        #region Member Variables

        private int _memberId = 0;
        private static string _memberrole = string.Empty;

        #endregion

        #region Properties

        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        public Int32 MemberId
        {
            set { _memberId = value; }
        }

        public int WorkFlowId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_WorkFlowId"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_WorkFlowId"] = value;
            }
        }

        public bool IsSendAlertByEmail
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_IsSendAlertByEmail"] ?? false);
            }
            set
            {
                ViewState[this.ClientID + "_IsSendAlertByEmail"] = value;
            }
        }

        #endregion

        #region Methods

        private void LoadRadioButtonList()
        {

            List<GenericLookup> lst = (List<GenericLookup>)Facade.GetAllGenericLookupByLookupType(LookupType.ResumeAvailability);

            var sortById = from o in lst  
                           orderby o.Id
                           select new
                           {
                               o.Id,
                               o.Name
                           };
            //rdbtnlstAvailability.Items.Clear();
            //rdbtnlstAvailability.DataSource = sortById;
            //rdbtnlstAvailability.DataTextField = "Name";
            //rdbtnlstAvailability.DataValueField = "Id";
            //rdbtnlstAvailability.DataBind();
        }

        private void SaveResumePrivacy()
        {
            //try
            //{
            //    lblMessage.Text = String.Empty;
            //    bool allowSave = true;
            //    string msg;
            //    DateTime dtAvailable = DateTime.MinValue;
            //    if (rdbtnlstAvailability.SelectedIndex == 0)
            //    {
            //        dtAvailable = System.DateTime.Now;
            //    }
            //    if (rdbtnlstAvailability.SelectedIndex == 1)
            //    {
            //        if (wdcDateOfBirth.Text.Trim().ToString() != "" && wdcDateOfBirth.Text.Trim().ToString() != null)
            //        {
            //            dtAvailable = Convert.ToDateTime(wdcDateOfBirth.Text.Trim());
            //            if (dtAvailable < DateTime.Today)
            //            {
            //                allowSave = false;
            //            }
            //        }
            //        else
            //        {
            //            lblErrorDate.Text = "Please select the date of availabilty.";
            //            return;
            //        }
            //    }

            //    if (allowSave)
            //    {
            //        MemberExtendedInformation memberExtended = Facade.GetMemberExtendedInformationByMemberId(_memberId);
            //        if (memberExtended == null)
            //        {
            //            memberExtended = new MemberExtendedInformation();
            //            memberExtended.MemberId = _memberId;
            //            memberExtended.Availability = Int32.Parse(rdbtnlstAvailability.SelectedValue);
            //            memberExtended.AvailableDate = dtAvailable;
            //            Facade.AddMemberExtendedInformation(memberExtended);
            //            SendAlert();
            //            MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.ChangeResumePrivacy, Facade);
            //            msg = "Successfully Updated Availability.";
            //           // MiscUtil.ShowMessage(lblMessage, msg, false);
            //        }
            //        else if (rdbtnlstAvailability.SelectedIndex == 1 && memberExtended.AvailableDate != dtAvailable)
            //        {
            //            memberExtended.Availability = Int32.Parse(rdbtnlstAvailability.SelectedValue);
            //            memberExtended.AvailableDate = dtAvailable;
            //            Facade.UpdateMemberExtendedInformation(memberExtended);
            //            SendAlert();
            //            MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.ChangeResumePrivacy, Facade);
            //            msg = "Successfully Updated Availability.";
            //           // MiscUtil.ShowMessage(lblMessage, msg, false);
            //        }
            //        else if (memberExtended.Availability != Int32.Parse(rdbtnlstAvailability.SelectedValue))
            //        {
            //            memberExtended.Availability = Int32.Parse(rdbtnlstAvailability.SelectedValue);
            //            memberExtended.AvailableDate = dtAvailable;
            //            Facade.UpdateMemberExtendedInformation(memberExtended);
            //            SendAlert();
            //            MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.ChangeResumePrivacy, Facade);
            //            msg = "Successfully Updated Availability.";
            //           // MiscUtil.ShowMessage(lblMessage, msg, false);
            //        }
            //        else
            //        {
            //            msg = "No changes to save.";
            //            MiscUtil.ShowMessage(lblMessage, msg, true);
            //            return;
            //        }
            //        string availabilitytext= MiscUtil.GetMemberAvailability(memberExtended.Availability, memberExtended.AvailableDate, Facade);
            //        ScriptManager.RegisterClientScriptBlock(this, typeof(MiscUtil), "Availabilty", ";parent.changeAvailability('" + availabilitytext + "');parent.ShowStatusMessage('" + msg + "',false);", true);
            //        //ScriptManager.RegisterClientScriptBlock(this, typeof(MiscUtil), "Availabilty", ";parent.ShowStatusMessage('" + msg + "',false);", true);
            //    }
            //    else
            //    {
                    
            //         msg = "Available date should not be less than current date.";  //0.9
            //         MiscUtil.ShowMessage(lblMessage, msg, true);

            //    }
               
            //}
            //catch (ArgumentException ex)
            //{
            //    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
            //}
        }

        private void LoadResumePrivacy()
        {
            try
            {
                MemberExtendedInformation memberExtended = Facade.GetMemberExtendedInformationByMemberId(_memberId);
                //if (memberExtended != null && memberExtended.Availability > 1)
                //{
                //    rdbtnlstAvailability.SelectedValue = memberExtended.Availability.ToString();
                //    if (rdbtnlstAvailability.SelectedIndex == 1)
                //    {
                //        wdcDateOfBirth.Value = memberExtended.AvailableDate;
                //    }
                //}
                //else
                //{
                //    rdbtnlstAvailability.SelectedIndex = 0;
                //}
            }
            catch (ArgumentException ex)
            {
                MiscUtil.ShowMessage(lblMessage, ex.Message, true);
            }
        }

        private void LoadWorkFlowSetting()
        {
            ApplicationWorkflow applicationWorkflow = Facade.GetApplicationWorkflowByTitle(WorkFlowTitle.ATSShouldAssignedManagersForCandidateAlertedWhenCandidateChangesTheirAvailability.ToString());
            if (applicationWorkflow != null && applicationWorkflow.RequireApproval && applicationWorkflow.IsEmailAlert && (base.CurrentUserRole == ContextConstants.ROLE_CANDIDATE || base.CurrentUserRole == ContextConstants.ROLE_CONSULTANT))
            {
                IsSendAlertByEmail = true;
                WorkFlowId = applicationWorkflow.Id;
            }
            else
            {
                IsSendAlertByEmail = false;
            }
        }

        private void SendAlert()
        {
            if (IsSendAlertByEmail)
            {
                try
                {
                    string strMemberIds = String.Empty;
                    EmailHelper newMailManager = new EmailHelper();
                    newMailManager.From = MiscUtil.GetAdministratorEmail();
                    IList<MemberManager> memberManagerList = Facade.GetAllMemberManagerByMemberId(base.CurrentMember.Id);
                    foreach (MemberManager memberManager in memberManagerList)
                    {
                        Member member = Facade.GetMemberById(memberManager.ManagerId);
                        if (member != null && !string.IsNullOrEmpty(member.PrimaryEmail))
                        {
                            newMailManager.To.Add(member.PrimaryEmail);
                            if (string.IsNullOrEmpty(strMemberIds))
                            {
                                strMemberIds = member.Id.ToString();
                            }
                            else
                            {
                                strMemberIds = strMemberIds + "," + member.Id.ToString();
                            }
                        }
                    }

                    IList<ApplicationWorkflowMap> appWorkFlowMapList = Facade.GetAllApplicationWorkflowMapByApplicationWorkflowId(WorkFlowId);
                    foreach (ApplicationWorkflowMap appWorkFlowMap in appWorkFlowMapList)
                    {
                        Member member = Facade.GetMemberById(appWorkFlowMap.MemberId);
                        if (member != null && strMemberIds.IndexOf(member.Id.ToString()) == -1 && !string.IsNullOrEmpty(member.PrimaryEmail))
                        {
                            newMailManager.To.Add(member.PrimaryEmail);
                        }
                    }

                    newMailManager.Subject = UIConstants.SUBJECT_CHANGEAVAILABILITY_ALERT;
                    EmailHelper newMailHelper = new EmailHelper();

                   // newMailManager.Body = newMailHelper.GetAlertChangeAvailability(MiscUtil.GetFirstAndLastName(base.CurrentMember.FirstName, base.CurrentMember.LastName), base.CurrentMember.PrimaryEmail, rdbtnlstAvailability.SelectedItem.Text);
                    newMailManager.Send(base.CurrentMember.Id);

                }
                catch { }
            }
        }
        #endregion

        #region Events

        private void SaveNotsAndActivities()
        {
            if (IsValid)
            {
                try
                {
                    CommonNote notsAndActivities = BuildNotsAndActivities();
                    MemberNote memberNote = BuildMemberNot();
                    txtNotes.Text = MiscUtil.RemoveScript(txtNotes.Text);
                    if (notsAndActivities.IsNew)
                    {
                        if (txtNotes.Text.ToString() != string.Empty)
                        {
                            notsAndActivities = Facade.AddCommonNote(notsAndActivities);
                            memberNote.CommonNoteId = notsAndActivities.Id;
                            Facade.AddMemberNote(memberNote);
                            MiscUtil.ShowMessage(lblMessage, "Note has been added succesfully.", false);
                            
                            //Thread.Sleep(3000);
                            //string script = "<script type=\"text/javascript\"> parent.CloseModal(); </script>";
                            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "myscript", script);                            
                            //Response.Redirect(Request.RawUrl);  
                        }
                    }
                    else
                    {
                        if (txtNotes.Text.ToString() != string.Empty)
                        {
                            Facade.UpdateCommonNote(notsAndActivities);
                            MiscUtil.ShowMessage(lblMessage, "Note has been updated succesfully.", false);
                        }
                    } 
                    ClearText();
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            } 


        }


        private void ClearText()
        {
            ddlNoteCategory.SelectedIndex = 0;
            txtNotes.Text = string.Empty;
            //CommonNoteId = 0;
        }
        private CommonNote BuildNotsAndActivities()
        {
            CommonNote commonNote = null;
            //if (!CurrentNote.IsNew)
            //{
            //    commonNote = CurrentNote;
            //}
            //else
            //{
                commonNote = new CommonNote();
            //}
            commonNote.NoteCategoryLookupId = Convert.ToInt32(ddlNoteCategory.SelectedValue);
            commonNote.NoteDetail = MiscUtil.RemoveScript(txtNotes.Text.Trim());

            commonNote.CreatorId = base.CurrentMember.Id;
            commonNote.UpdatorId = base.CurrentMember.Id;
            return commonNote;
        }

        private MemberNote BuildMemberNot()
        {
            MemberNote memberNote = new MemberNote();

            if (_memberId > 0)
            {
                memberNote.MemberId = _memberId;
            }
            else
            {
                memberNote.MemberId = base.CurrentMember.Id;
            }
            memberNote.CreatorId = base.CurrentMember.Id;
            return memberNote;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveNotsAndActivities();
           
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MiscUtil.PopulateNotesType(ddlNoteCategory, Facade);
               
            }
            lblMessage.Text = "";
            lblMessage.CssClass = "";
            //lblErrorDate.Text = "";
            if (Session["msg"] != null)
            {
                MiscUtil.ShowMessage(lblMessage, Session["msg"].ToString(), false);
                Session["msg"] = null;
            }
            if (Session["msgs"] != null)
            {
                MiscUtil.ShowMessage(lblMessage, Session["msgs"].ToString(), true );
                Session["msgs"] = null;
            }
        } 
        #endregion
    }
}
