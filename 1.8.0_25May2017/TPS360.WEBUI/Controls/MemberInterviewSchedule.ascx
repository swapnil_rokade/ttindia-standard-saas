﻿ <%-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberInterviewSchedule.ascx
    Description:  
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              16/Oct/2015        Prasanth Kumar G     Introduced Interview QuestionBank
    0.2              9/Dec/2015         Pravin khot          Add code of Interview suggested panel
    0.3              12/Feb/2016        pravin khot          Commented link button InterviewerfeedbackDetail
    0.4               5/March/2016       pravin khot         new added - lblStatus
    0.5              24/May/2016        pravin khot          new added modify - ddllocation
    0.6              25/May/2016        pravin khot          added-ddlTimezone and visible="false"
    0.7              24/June/2016       pravin khot          added-lblotheremailids
    0.8              28/Feb/2017        Sumit Sonawane       added  No-Show and Completed on Action Button
    0.9             1/Mar/2017         Sumit Sonawane        modify - restrict to rescheduled/cancel Interview feedback.
    1.0             1/Mar/2017         Sumit Sonawane        modify - for Interview reschedule matrix capture.
    ------------------------------------------------------------------------------------------------------------------------------          
--%>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.LayoutControls" TagPrefix="ig" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberInterviewSchedule.ascx.cs"
    Inherits="TPS360.Web.UI.ControlMemberInterviewSchedule" %>
<%--<%@ Register Assembly="Infragistics2.WebUI.WebDateChooser.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.WebSchedule" TagPrefix="igsch" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/HtmlEditor.ascx" TagName="HtmlEditor" TagPrefix="ucl" %>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script src="../Scripts/fixWebkit.js" type="text/javascript"></script>
<script src="../assets/js/chosen.jquery.js" type="text/javascript"></script>
<link href="../assets/css/chosen.css" rel="Stylesheet" />

<script type="text/javascript" language="javascript">

  window.onload = function() {
 
    document.getElementById("<%= hfOtherInterviewers.ClientID %>").value = "";  
);
</script>
<style type="text/css">
    .TableFormLeble
    {
        width: 33%;
    }
    .EmailDiv
    {
        vertical-align: middle;
        text-align: center;
        border: solid 1px #CCC;
        padding-bottom: 2px;
        padding-top: 2px;
        margin-right: 4px;
        margin-bottom: 4px;
        width: auto;
        float: left;
        background-image: -moz-linear-gradient(center top , #FFFFFF 20%, #F6F6F6 50%, #EEEEEE 52%, #F4F4F4 100%);
        border-radius: 3px;
    }
</style>

<script language="javascript" type="text/javascript">





    //*********************** code introduced by Pravin khot on 4/Jan/2016 START Using validation of duplicate interviewers****************************

    function CheckExistanceOfOtherEmail(curretEmail) {
       
        var alreadyavailablee = false;
        var alreadyadded=document.getElementById("<%= hfOtherInterviewers.ClientID %>").value
       
        var emailarray = alreadyadded.split(",");
       
        var str = curretEmail.trim().toLowerCase();
        str = str.replace(/[,;]$/, '');
       
        for (var i = 0; i < emailarray.length; i++) {
            if (emailarray[i].trim() != '') {
                if (emailarray[i].trim().toLowerCase() == str) {
                    alreadyavailablee = true;
                    break;
                }
            }           
        }
        return alreadyavailablee;
    }
    
   
    function checkcheckedSuggestedInterviewer() {   //ref to SuggestedInterviewer
        
        var CHK = document.getElementById("<%=chkSuggestedInterviewer.ClientID%>");
        var checkbox = CHK.getElementsByTagName("input");
        var label = CHK.getElementsByTagName("label");
     
        for (var i = 0; i < checkbox.length; i++) {
            if (checkbox[i].checked) {              
                var prk = label[i].innerHTML;
                var last = prk.lastIndexOf(']');
                var first = prk.lastIndexOf('[');
                first = first + 1;
                var email = prk.substring(first, last);
                
                if (CheckExistanceOfOtherEmail(email)) {
                    alert(label[i].innerHTML + " This Interviewer Allready Added in Other Interviewers");
                    checkbox[i].checked = false;
                    return;
                    break;
                }           
                
                if (CheckemailClientInterviersSI(email)) {
                    alert(label[i].innerHTML + " This Interviewer Allready Selected In BU Interviewers Panel");
                    checkbox[i].checked = false;
                    return;
                    break;
                }
                if (CheckemailInternalInterviewerSI(email)) {
                    alert(label[i].innerHTML + " This Interviewer Allready Selected In Recruiters Panel");
                    checkbox[i].checked = false ;
                    return;
                    break;
                }

                

            } 
        }
    }
    function CheckemailInternalInterviewerSI(email) {
        var already = false;
        var CHK1 = document.getElementById("<%=chkInternalInterviewer.ClientID%>");
        var checkbox1 = CHK1.getElementsByTagName("input");
        var label1 = CHK1.getElementsByTagName("label");

        for (var i = 0; checkbox1.length; i++) {
             if (checkbox1[i].checked) {
                 var prk1 = label1[i].innerHTML;
                 var last1 = prk1.lastIndexOf(']');
                 var first1 = prk1.lastIndexOf('[');
                 first1 = first1 + 1;
                 var email1 = prk1.substring(first1, last1);
                 if (email1.trim().toLowerCase() == email.trim().toLowerCase()) {
                     already = true;                   
                     break;
                 }
                 
             }
            
        }
        return already;
    }
    function CheckemailClientInterviersSI(email) {
        var already = false;
        var CHK2 = document.getElementById("<%=chkClientInterviers.ClientID%>");
        var checkbox2 = CHK2.getElementsByTagName("input");
        var label2 = CHK2.getElementsByTagName("label");
        for (var i = 0; i < checkbox2.length; i++) {
            if (checkbox2[i].checked) {
                var prk2 = label2[i].innerHTML;
                var last2 = prk2.lastIndexOf(']');
                var first2 = prk2.lastIndexOf('[');
                first2 = first2 + 1;
                var email2 = prk2.substring(first2, last2);
//                alert(email2); 
                if (email2.trim().toLowerCase() == email.trim().toLowerCase()) {
                    already = true;
                    break;
                }

            }

        }
        return already;
    }



    function checkcheckedInternalInterviewer() {  //ref to InternalInterviewer
        var CHKII = document.getElementById("<%=chkInternalInterviewer.ClientID%>");
        var checkboxII = CHKII.getElementsByTagName("input");
        var labelII = CHKII.getElementsByTagName("label");

        for (var i = 0; i < checkboxII.length; i++) {
            if (checkboxII[i].checked) {
                var prkII = labelII[i].innerHTML;
                var lastII = prkII.lastIndexOf(']');
                var firstII = prkII.lastIndexOf('[');
                firstII = firstII + 1;
                var emailII = prkII.substring(firstII, lastII);
                if (CheckExistanceOfOtherEmail(emailII)) {
                    alert(labelII[i].innerHTML + " This Interviewer Allready Added in Other Interviewers");
                    checkboxII[i].checked = false;
                    return;
                    break;
                }      
              
                if (CheckemailInternalInterviewerS(emailII)) {
                    alert(labelII[i].innerHTML + " This Interviewer Allready Selected In Suggested Panel");
                    checkboxII[i].checked = false;
                    return;
                    break;
                }
                if (CheckemailClientInterviersS(emailII)) {
                    alert(labelII[i].innerHTML + " This Interviewer Allready Selected In BU Interviewers Panel");
                    checkboxII[i].checked = false;
                    return;
                    break;
                }

            }
        }
    }
    function CheckemailInternalInterviewerS(emailII) {
        var prkS = "";
        var lastS ="";
        var firstS ="";
        var emailS ="";
        
        var already = false;
        var CHKS = document.getElementById("<%=chkSuggestedInterviewer.ClientID%>");
        var checkboxS = CHKS.getElementsByTagName("input");
        var labelS = CHKS.getElementsByTagName("label");

        for (var i = 0; checkboxS.length; i++) {
            if (checkboxS[i].checked) {
                 prkS = labelS[i].innerHTML;
                 lastS = prkS.lastIndexOf(']');
                firstS = prkS.lastIndexOf('[');
                firstS = firstS + 1;
                emailS = prkS.substring(firstS, lastS);
              
                if (emailS.trim().toLowerCase() == emailII.trim().toLowerCase()) {
                    already = true;
                    break;
                }

            }

        }
        return already;
    }

    function CheckemailClientInterviersS(emailII) {

        var prkC = "";
        var lastC = "";
        var firstC = "";
        var emailC = "";
        
        var already = false;
        var CHKC = document.getElementById("<%=chkClientInterviers.ClientID%>");
        var checkboxC = CHKC.getElementsByTagName("input");
        var labelC = CHKC.getElementsByTagName("label");

        for (var i = 0; i < checkboxC.length; i++) {
            if (checkboxC[i].checked) {
                prkC = labelC[i].innerHTML;
                lastC = prkC.lastIndexOf(']');
                firstC = prkC.lastIndexOf('[');
                firstC = firstC + 1;
                emailC = prkC.substring(firstC, lastC);
                if (emailC.trim().toLowerCase() == emailII.trim().toLowerCase()) {
                    already = true;
                    break;
                }

            }

        }
        return already;
    }

    function checkcheckedClientInterviers() { //ref to ClientInterviers
        var prkCI = "";
        var lastCI = "";
        var firstCI = "";
        var emailCI = "";

        var CHKCI = document.getElementById("<%=chkClientInterviers.ClientID%>");
        var checkboxCI = CHKCI.getElementsByTagName("input");
        var labelCI = CHKCI.getElementsByTagName("label");

        for (var i = 0; i < checkboxCI.length; i++) {
            if (checkboxCI[i].checked) {
                prkCI = labelCI[i].innerHTML;
                lastCI = prkCI.lastIndexOf(']');
                firstCI = prkCI.lastIndexOf('[');
                firstCI = firstCI + 1;
                emailCI = prkCI.substring(firstCI, lastCI);

                if (CheckExistanceOfOtherEmail(emailCI)) {
                     alert(labelCI[i].innerHTML + " This Interviewer Allready Added in Other Interviewers");
                     checkboxCI[i].checked = false;
                    return;
                    break;
                }    
                
                if (CheckemailClientInterviersCI2(emailCI)) {
                    alert(labelCI[i].innerHTML + " This Interviewer Allready Selected In Suggested Panel");
                    checkboxCI[i].checked = false;
                    return;
                    break;
                }
                if (CheckemailInternalInterviewerCI2(emailCI)) {
                    alert(labelCI[i].innerHTML + " This Interviewer Allready Selected In Recruiters Panel");
                    checkboxCI[i].checked = false;
                    return;
                    break;
                }              

            }
        }
    }
    function CheckemailInternalInterviewerCI2(emailCI) {
        var already = false;
        var CHKCI1 = document.getElementById("<%=chkInternalInterviewer.ClientID%>");
        var checkboxCI1 = CHKCI1.getElementsByTagName("input");
        var labelCI1 = CHKCI1.getElementsByTagName("label");

        for (var i = 0; checkboxCI1.length; i++) {
            if (checkboxCI1[i].checked) {
                var prkCI1 = labelCI1[i].innerHTML;
                var lastCI1 = prkCI1.lastIndexOf(']');
                var firstCI1 = prkCI1.lastIndexOf('[');
                firstCI1 = firstCI1 + 1;
                var emailCI1 = prkCI1.substring(firstCI1, lastCI1);
                if (emailCI1.trim().toLowerCase() == emailCI.trim().toLowerCase()) {
                    already = true;
                    break;
                }

            }

        }
        return already;
    }

    function CheckemailClientInterviersCI2(emailCI) {
        var already = false;
        var CHKCI2 = document.getElementById("<%=chkSuggestedInterviewer.ClientID%>");
        var checkboxCI2 = CHKCI2.getElementsByTagName("input");
        var labelCI2 = CHKCI2.getElementsByTagName("label");

        for (var i = 0; i < checkboxCI2.length; i++) {
            if (checkboxCI2[i].checked) {
                var prkCI2 = labelCI2[i].innerHTML;
                var lastCI2 = prkCI2.lastIndexOf(']');
                var firstCI2 = prkCI2.lastIndexOf('[');
                firstCI2 = firstCI2 + 1;
                var emailCI2 = prkCI2.substring(firstCI2, lastCI2);
                if (emailCI2.trim().toLowerCase() == emailCI.trim().toLowerCase()) {
                    already = true;
                    break;
                }

            }

        }
        return already;
    }
     

    function Check(str) {
        var already = false;
        var chkListaTipoModificaciones = document.getElementById('<%= chkInternalInterviewer.ClientID %>');
         if(chkListaTipoModificaciones != null){
        var chkLista = chkListaTipoModificaciones.getElementsByTagName("input");
        var label = chkListaTipoModificaciones.getElementsByTagName("label");
        for (var i = 0; i < chkLista.length; i++) {
            if (chkLista[i].checked) {
                var prk = label[i].innerHTML;
                var last = prk.lastIndexOf(']');
                var first = prk.lastIndexOf('[');
                first = first + 1;
                var email = prk.substring(first, last);
                if (email.trim().toLowerCase() == str) {
                    already = true;
                    break;
                }
            }
        }
        }
        return already;
    }
    function Check1(str) {
        var already = false;
        var chkListaTipoModificaciones = document.getElementById('<%= chkClientInterviers.ClientID %>');
         if(chkListaTipoModificaciones != null){
        var chkLista = chkListaTipoModificaciones.getElementsByTagName("input");
        var label = chkListaTipoModificaciones.getElementsByTagName("label");
        for (var i = 0; i < chkLista.length; i++) {
            if (chkLista[i].checked) {
                var prk = label[i].innerHTML;
                var last = prk.lastIndexOf(']');
                var first = prk.lastIndexOf('[');
                first = first + 1;
                var email = prk.substring(first, last);
                if (email.trim().toLowerCase() == str) {
                    already = true;
                    break;
                }
            }
        }
        }
        return already;
    }
    function Check2(str) {
        var already = false;
        var chkListaTipoModificaciones = document.getElementById('<%= chkSuggestedInterviewer.ClientID %>');
        if(chkListaTipoModificaciones != null){
        var chkLista = chkListaTipoModificaciones.getElementsByTagName("input");
        var label = chkListaTipoModificaciones.getElementsByTagName("label");
        for (var i = 0; i < chkLista.length; i++) {
            if (chkLista[i].checked) {
                var prk = label[i].innerHTML;
                var last = prk.lastIndexOf(']');
                var first = prk.lastIndexOf('[');
                first = first + 1;
                var email = prk.substring(first, last);
                if (email.trim().toLowerCase() == str) {
                    already = true;
                    break;
                }
            }
            }
            }
           
        return already;
    }
    function CheckExistanceOfEmail(curretEmail, alreadyadded) { //ref to Other interviewers
        var alreadyavailablee = false;
        var emailarray = alreadyadded.split(",");
        var str = curretEmail.trim().toLowerCase();
        str = str.replace(/[,;]$/, '');
        for (var i = 0; i < emailarray.length; i++) {
            if (emailarray[i].trim() != '') {
                if (emailarray[i].trim().toLowerCase() == str) {
                    alreadyavailablee = true;
                    break;
                }
            }
            if (Check(str)) {
                alreadyavailablee = true;
                break;
            }
            if (Check1(str)) {
                alreadyavailablee = true;
                break;
            }
            if (Check2(str)) {
                alreadyavailablee = true;
                break;
            }
        }
        return alreadyavailablee;
    }

    function Clicked(email, parent) {
      
        $('#<%= hfOtherInterviewers.ClientID %>').val($('#<%= hfOtherInterviewers.ClientID %>').val().replace(email, '').replace(',,', ','));
    }

    function EnableDisableDocumentDropdown() {
        //alert('sdasdasd');

        var ddlDoc = document.getElementById('<%= ddlDocument.ClientID %>');
        var chkDoc = document.getElementById('<%= chkAttachCandidateResume.ClientID %>');

        if (chkDoc.checked) {
            ddlDoc.disabled = false;
        }
        else {
            ddlDoc.disabled = true;
        }

    }


    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    Sys.Application.add_load(function() {


        $('#<%=ddlStartTime.ClientID %>').change(function() {
            var EndTime = document.getElementById('<%= ddlEndTime.ClientID %>');
            var StartTime = document.getElementById('<%= ddlStartTime.ClientID %>');
            EndTime.selectedIndex = StartTime.selectedIndex + 1;
            var LblDatevalidate = document.getElementById('<%= LblDatevalidate.ClientID %>');
            LblDatevalidate.innerHTML = "";
            args.IsValid = true;
        });

        $('.close').click(function() {
        });

        $('#<%= txtOtherInterviewers.ClientID %>').keyup(function(event) {
            var key = window.event ? event.keyCode : event.which;
            var index = 0;
            if ($(this).val().toLowerCase().indexOf(";") > 0) index = $(this).val().toLowerCase().indexOf(";");
            else if ($(this).val().toLowerCase().indexOf(",") > 0) index = $(this).val().toLowerCase().indexOf(",");
            else if (key == 13) index = $(this).val().trim().length;

            if (index > 0) {
                if (validateEmail($(this).val().substring(0, index))) {
                    var text = $(this).val();
                    if (!CheckExistanceOfEmail($(this).val(), $(this).next().val())) {
                        $(this).parent().prepend("<div class='EmailDiv'><span>" + text.substring(0, index) + "</span><button type='button' class='close' data-dismiss='alert' onclick=javascript:Clicked('" + text.substring(0, index) + "')>\u00D7</button></div>");
                        $(this).val(text.substring(index + 1));
                        $(this).next().val($(this).next().val() + ',' + text.substring(0, index));
                    }
                    else
                        ShowMessage('<%= lblMessage.ClientID %>', 'This Email already added', true);  //code added by pravin
                    $(this).val('');
                }
                // ShowMessage('<%= lblMessage.ClientID %>', 'Please Enter Valid Email-Id', true);              
                return false;
            }
        });

    
        $('#<%= txtOtherInterviewers.ClientID %>').focusout(function() {

            var index = $(this).val().length;
            if (index > 0) {
                if (validateEmail($(this).val().substring(0, index))) {
                    var text = $(this).val();
                    if (!CheckExistanceOfEmail($(this).val(), $(this).next().val())) {
                        //alert(text);
                        $(this).parent().prepend("<div class='EmailDiv'><span>" + text.substring(0, index) + "</span><button type='button' class='close' data-dismiss='alert'>\u00D7</button></div>");
                        $(this).val(text.substring(index + 1));
                        $(this).next().val($(this).next().val() + ',' + text.substring(0, index));
                    }
                    else
                        ShowMessage('<%= lblMessage.ClientID %>', 'This Email already added', true); //code added by pravin
                    $(this).val('');
                }
            }
        });
    });


    function ClientSelected(source, eventArgs) {

        hdnSelectedClient = document.getElementById('<%= hdnSelectedClient.ClientID %>');
        hdnSelectedClientText = document.getElementById('<%=hdnSelectedClientText.ClientID %>');
        hdnSelectedClientText.value = eventArgs.get_text();
        hdnSelectedClient.value = eventArgs.get_value();

    }
    function checkEnter(e) {
        try {
            var keycode;
            if (window.event) keycode = window.event.keyCode;
            else if (event) keycode = event.keyCode;
            else if (e) keycode = e.which;
            else return true;

            if ((keycode == 13)) {
                var s;
                s = document.getElementById(btnSave);
                s.UseSubmitBehaviour = true;

                //__doPostBack('btnSave','OnClick');
                return true;
            }
            else {
                return false;
            }
            return true;
        }
        catch (e1) {
        }
    }
    
    function ClientSelected(source, eventArgs)
    {  
        hdnSelectedClientValue=document .getElementById ('<%=hdnSelectedClientValue.ClientID%>');
        hdnSelectedClientText=document .getElementById ('<%=hdnSelectedClientText.ClientID%>');

        hdnSelectedClientText.value=eventArgs.get_text();
        hdnSelectedClientValue.value=eventArgs.get_value(); 
        if(hdnSelectedClientText .value !='')
        {
            __doPostBack("AutoCompleteExtender", source._element.value);
        }
    }
    function EnableDisableReminderDropDown(chkControl)
    {
        
        var ddlReminder = $get('<%= ddlReminder.ClientID %>');                
        if (chkControl.checked == true)
        {
            ddlReminder.disabled = false;
        }
        else
        {
            ddlReminder.disabled = true;
        }
    }
     function VisibleInvisibleDivattch(chkControl) {
        var divattach = $get('<%= divAttachments.ClientID %>');

        if (chkControl.checked) {
            divattach.style.display = "inline"
        }
        else {
            divattach.style.display = "none"
        }
    }
    function VisibleInvisibleAllDayEvent(chkControl)
    {        
        var dttmStartTime = $get('<%= divST.ClientID %>');
        var dttmEndTime = $get('<%= divET.ClientID %>');
        var LblDatevalidate = document .getElementById('<%= LblDatevalidate.ClientID %>');
        var lblEndTime=document .getElementById ('<%=lblEndTime.ClientID %>');
               
        if (chkControl.checked)
        {
            dttmStartTime.style.display ="none";            
            dttmEndTime.style.display = "none";
            lblEndTime.style.display="none";
            LblDatevalidate .innerHTML='';
        }
        else
        {
            dttmStartTime.style.display = "inline";
            dttmEndTime.style.display = "inline";
            lblEndTime.style.display="inline";
        }
    }
    function Validatedate(source, args)
    {
        var LblDatevalidate = document .getElementById('<%= LblDatevalidate.ClientID %>');
        var hdSDate = document .getElementById('<%=hdnIntrvdate.ClientID %>');
        var dateTime = new Date;
        LblDatevalidate.innerHTML = "";
        var sDate = args.Value.split('/');
        hdSDate.value=sDate;
        
        var DateCompare = new Date(dateTime.getFullYear(),dateTime.getMonth(),dateTime.getDate()); 
        var StartDate = new Date(hdSDate.value.split(',')[2], hdSDate.value.split(',')[0] - 1, hdSDate.value.split(',')[1]);
         
        if (StartDate < DateCompare) 
        {
            LblDatevalidate.innerHTML = "Start Date should not be less than current date.";
            LblDatevalidate.style.display = "block";
            args.IsValid = false;
            return;
        }
        
    }
    function DateValueChanged(sender, eventArgs) 
    {
        var AllDay =document .getElementById ('<%= chkAllDayEvent.ClientID %>');
        if(!AllDay .checked)
        {
            validateTime();
        }
    }
    function ValidateDateAndTime(sender, eventArgs)
    {
        var LblDatevalidate = document .getElementById('<%= LblDatevalidate.ClientID %>');
        if(LblDatevalidate .innerHTML == "")
        {
            eventArgs .IsValid=true ;
        }
        else 
        {
            eventArgs .IsValid=false ;
        }
    }
    function validateTime()
    {
        var strStartDate,strEndDate,StartDateTime,EndDateTime;
        var dateTime = new Date();
        var DateCompare;  
//        alert($('<%= wdcStartDate.ClientID %>').val());
//        strStartDate =document .getElementById ('<%=wdcStartDate.ClientID%>').value.split('/');
//        strEndDate =document .getElementById ('<%=wdcStartDate.ClientID%>').value.split('/');
        var LblDatevalidate = document .getElementById('<%= LblDatevalidate.ClientID %>');
        LblDatevalidate.innerHTML = ""; 

        var e =document .getElementById ('<%= ddlStartTime.ClientID %>');
        var ee =document .getElementById ('<%= ddlEndTime.ClientID %>');
        StartDateTime = e.options[e.selectedIndex].value;
        EndDateTime=ee.options[ee.selectedIndex].value;
        var sTime = e.selectedIndex;
        var etime=ee.selectedIndex ;
        if(sTime>=etime)
        {

                LblDatevalidate.innerHTML = "Start Time must be less than End Time.";
                LblDatevalidate.style.display = "block";
                args.IsValid=false;
            }
            else
            {
                LblDatevalidate.innerHTML = "";
                args.IsValid=true;
            }
        var sTime = StartDateTime.split(" ");
        var eTime = EndDateTime.split(" ");
        if (sTime[1].toUpperCase() == "AM") 
        {
            var stimeHM = sTime[0].split(':');
            if (stimeHM[0] == "12") 
            {
                stimeHM[0] = parseInt(stimeHM[0]) - 12;
            }
        }
        else 
        {
            var stimeHM = sTime[0].split(':');
            if (stimeHM[0] != "12")
            {
                stimeHM[0] = parseInt(stimeHM[0]) + 12;
            }
        }

        if (eTime[1].toUpperCase() == "AM") 
        {
            var etimeHM = eTime[0].split(':');
            if (etimeHM[0] == "12") 
            {
                etimeHM[0] = parseInt(etimeHM[0]) - 12;
            }
        }
        else 
        {
            var etimeHM = eTime[0].split(':');
            if (etimeHM[0] != "12") 
            {
                etimeHM[0] = parseInt(etimeHM[0]) + 12;
            }
        }
        DateCompare = new Date(dateTime.getFullYear(),dateTime.getMonth(),dateTime.getDate(),dateTime.getHours(),dateTime.getMinutes());
        StartDateTime = new Date(strStartDate[2], strStartDate[0] - 1, strStartDate[1], stimeHM[0], stimeHM[1]);
        EndDateTime = new Date(strEndDate[2], strEndDate[0] - 1, strEndDate[1], etimeHM[0], etimeHM[1]);

        if(strStartDate[0].length==1)
        {
            strStartDate[0] ="0"+strStartDate[0];
        }
        if(strStartDate[1].length==1)
        {
            strStartDate[1] ="0"+strStartDate[1];
        }
        var startDate = strStartDate[2]+ strStartDate[0] + strStartDate[1];

        if(strEndDate[0].length==1)
        {
            strEndDate[0] ="0"+strEndDate[0];
        }
        if(strEndDate[1].length==1)
        {
            strEndDate[1] ="0"+strEndDate[1];
        }
        var endDate = strEndDate[2] + strEndDate[0] + strEndDate[1] ;
        var year = dateTime.getFullYear().toString();
        var month= (dateTime.getMonth()+1).toString();
        var day=  dateTime.getDate().toString();
        if(month.length==1)
        {
            month ="0"+month.toString();
        }
        if(day.length==1)
        {
            day ="0"+day.toString();
        }
        var tmpTodayDate =year.toString()+month.toString()+day.toString();
        if( startDate== endDate)
        { 
            if (StartDateTime < DateCompare) 
            {

                LblDatevalidate.innerHTML = "Start Time should not be less than current time.";
                LblDatevalidate.style.display = "block";
                args.IsValid=false;
            }
            else if (StartDateTime >= EndDateTime) 
            {

                LblDatevalidate.innerHTML = "Start Time must be less than End Time.";
                LblDatevalidate.style.display = "block";
                args.IsValid=false;
            }
            else
            {
                LblDatevalidate.innerHTML = "";
                args.IsValid=true;
            }
        }
        else
        {
            if (StartDateTime < DateCompare) 
            {
                LblDatevalidate.innerHTML = "Start Time should not be less than current time.";
                LblDatevalidate.style.display = "block";
                args.IsValid=false;
            }
            else if (StartDateTime >= EndDateTime) 
            {
                LblDatevalidate.innerHTML = "Start Time must be less than End Time.";
                LblDatevalidate.style.display = "block";
                args.IsValid=false;
            }
            else
            {
                LblDatevalidate.innerHTML = "";
                args.IsValid=true;
            }
        }
    }    

    function ValidateChkList(source, args)
    {
        var chkListaTipoModificaciones= document.getElementById ('<%= chkInternalInterviewer.ClientID %>');
        var chkLista= chkListaTipoModificaciones.getElementsByTagName("input");
        for(var i=0;i<chkLista.length;i++)
        {  
            if(chkLista[i].checked)
            {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;
    }    
   
    
</script>

<div>
    <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
    <asp:UpdatePanel ID="upPanel" runat="server">
        <ContentTemplate>
            <div>
                <ajaxToolkit:ModalPopupExtender ID="InterviewModalPopupExtender" runat="server" BackgroundCssClass="modalBackground"
                    DropShadow="false" PopupControlID="lgnPanelInterviewer" TargetControlID="lblInterviewer"
                    BehaviorID="SIIE">
                </ajaxToolkit:ModalPopupExtender>
                <ajaxToolkit:ModalPopupExtender ID="CandidateModalPopupExtender" runat="server" BackgroundCssClass="modalBackground"
                    DropShadow="false" PopupControlID="lgnPanelCandidate" TargetControlID="lblCandidate"
                    BehaviorID="SICE">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Label ID="lblInterviewer" runat="server"></asp:Label>
                <asp:Label ID="lblCandidate" runat="server"></asp:Label>
                <asp:Panel ID="lgnPanelInterviewer" runat="server" CssClass="modalPopup" Style="display: none;"
                    Width="700px" Height="400px">
                    <ucl:HtmlEditor ID="txtEmailInterviewerTemplate" runat="server" />
                    <br />
                    <asp:HiddenField ID="hdnInterviewTemp" runat="server" />
                    <div align="center">
                        <%--<asp:Button ID="Button1" runat="server" Text="Save" CssClass="CommonButton" Width="44px" />
                    --%><asp:Button ID="btnClose" runat="server" Text="Close" CssClass="CommonButton" />
                    </div>
                </asp:Panel>
            </div>
            <div>
                <asp:Panel ID="lgnPanelCandidate" runat="server" CssClass="modalPopup" Style="display: none;"
                    Width="700px" Height="400px">
                    <ucl:HtmlEditor ID="txtEmailCandidateTemplate" runat="server" />
                    <br />
                    <asp:HiddenField ID="hdnCandidateTemp" runat="server" />
                    <div align="center">
                        <%--<asp:Button ID="Button2" runat="server" Text="Save" CssClass="CommonButton" Width="44px" />
                    --%><asp:Button ID="Button3" runat="server" Text="Close" CssClass="CommonButton" />
                    </div>
                </asp:Panel>
            </div>
            <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
            <asp:HiddenField ID="hdnSelectedClientText" runat="server" />
            <asp:HiddenField ID="hdnSelectedClientValue" runat="server" />
            <asp:HiddenField ID="hfInterviewId" runat="server" />
            <asp:HiddenField ID="hfSourceId" runat="server" />
            <asp:HiddenField ID="hdnTmpFolder" runat="server" />
            <asp:HiddenField ID="hdnIntrvdate" runat="server" />
            <asp:HiddenField ID="hdnClear" runat="server" />
            <asp:HiddenField ID="hdnDocumentiD" runat="server" EnableViewState="true" />
            <asp:HiddenField ID="hdnFileTypeId" runat="server" EnableViewState="true" />
       
            <asp:HiddenField ID="hdnSelectedClient" runat="server" Value="0" />
            <div class="TableRow" style="text-align: center">
                <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
            </div>
            <asp:UpdatePanel ID="upInterview" runat="server">
                <ContentTemplate>
                    <div id="dvDetails" runat="server">
                        <div class="TabPanelHeader" id="divScheduleHeader" runat="server">
                            Schedule Interview
                        </div>
                        <div class="FormLeftColumn" style="width: 50%;">
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblClient" runat="server"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlClientInterviewer" runat="server" CssClass="CommonDropDownList"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlClientInterviewer_SelectedIndexChanged"
                                        TabIndex="6">
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtClientName" runat="server" Visible="true" Width="200" TabIndex="7"
                                        AutoComplete="Off"></asp:TextBox>
                                    <div id="divClient">
                                    </div>
                                    <ajaxToolkit:AutoCompleteExtender ID="Client_AutoComplete" runat="server" ServicePath="~/AjaxExtender.asmx"
                                        TargetControlID="txtClientName" ServiceMethod="GetClients" MinimumPrefixLength="1"
                                        EnableCaching="true" CompletionListElementID="divClient" CompletionInterval="0"
                                        FirstRowSelected="True" OnClientItemSelected="ClientSelected">
                                    </ajaxToolkit:AutoCompleteExtender>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="lblJobPosting" runat="server" Text="Requisition"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlJobPosting" runat="server" CssClass="CommonDropDownList"
                                        ValidationGroup="InterviewSchedule" AutoPostBack="true" OnSelectedIndexChanged="ddlJobPosting_SelectedIndexChanged"
                                        TabIndex="1">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="lblAppointmentTitle" runat="server" Height="20px"
                                        Text="Appointment Title"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox EnableViewState="false" ID="txtAppointmentTitle" runat="server" CssClass="CommonTextBox"
                                        TabIndex="2">
                                    </asp:TextBox>
                                    <span class="RequiredField">*</span>
                                </div>
                            </div>
                            <div class="TableRow"  style =" width :100%" >
                                <div class="TableFormValidatorContent" style=" margin-left :33%">
                                    <asp:RequiredFieldValidator ID="rfvAppointmentTitle" runat="server" ControlToValidate="txtAppointmentTitle"
                                        ErrorMessage="Please enter Appointment Title." EnableViewState="False" Display="Dynamic"
                                        ValidationGroup="InterviewSchedule">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="lblStartTime" runat="server" Text="Start Time"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <div style="float: left;">
                                        <%-- <igsch:WebDateChooser ID="wdcStartDate" runat="server" AllowNull="false" NullDateLabel=""
                                            EnableKeyboardNavigation="True" AutoCloseUp="true" TabIndex="9" Editable="True"
                                            Width="90px">
                                            <CalendarLayout HideOtherMonthDays="True">
                                            </CalendarLayout>
                                        </igsch:WebDateChooser>--%>
                                        <ig:WebDatePicker ID="wdcStartDate" Width="100px" DropDownCalendarID="ddcStartDate"
                                            runat="server">
                                        </ig:WebDatePicker>
                                        <ig:WebMonthCalendar ID="ddcStartDate" AllowNull="true" runat="server">
                                        </ig:WebMonthCalendar>
                                    </div>
                                    <div id="divST" runat="server" style="display: inline-table;">
                                        <asp:DropDownList EnableViewState="false" ID="ddlStartTime" runat="server" class="CommonDropDownList"
                                            TabIndex="10" Width="80px">
                                        </asp:DropDownList>
                                     <%--   <asp:DropDownList EnableViewState="false" ID="ddlStartTime" runat="server" class="CommonDropDownList"
                                            TabIndex="10" OnChange="validateTime()" Width="80px">
                                        </asp:DropDownList>--%>
                                    </div>
                                    <asp:CheckBox ID="chkAllDayEvent" Text="All Day" runat="server" onclick="VisibleInvisibleAllDayEvent(this)"
                                        Width="10px" Style="white-space: nowrap;" TabIndex="11" />
                                </div>
                            </div>
                            <%--<div class="TableRow">
                                <div class="TableFormValidatorContent" style="margin-left: 42%">
                                    <%--<asp:CompareValidator ID="cmpStartDate" runat="server" Operator="GreaterThanEqual"
                                        Type="Date" ControlToValidate="wdcStartDate" ErrorMessage="Start date should not be before the current date"
                                        Display="Dynamic" ValidationGroup="InterviewSchedule" />
                                    </div>
                            </div>--%>
                            <div class="TableRow" id="divET" runat="server">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblEndTime" runat="server" Text="End Time:"></asp:Label>
                                </div>
                                <div class="TableFormContent">
                                    <%--<asp:DropDownList EnableViewState="false" ID="ddlEndTime" runat="server" class="CommonDropDownList"
                                        Width="70px" TabIndex="13">
                                    </asp:DropDownList>--%>
                                    <asp:DropDownList EnableViewState="false" ID="ddlEndTime" runat="server" class="CommonDropDownList"
                                        Width="70px" TabIndex="13" OnChange="validateTime()">
                                    </asp:DropDownList>
                                </div>
                                
                            </div>
                            <div class="TableRow">
                                <div class="TableFormValidatorContent" style="margin-left: 32%">
                                    <asp:Label ID="LblDatevalidate" ForeColor="Red" runat="server" Text=""></asp:Label>
                                    <asp:CustomValidator runat="server" ID="CVDateTime" ClientValidationFunction="ValidateDateAndTime"
                                        ErrorMessage="" ValidationGroup="InterviewSchedule" Display="Dynamic" />
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                </div>
                                <div class="TableFormContent">
                                    <asp:CheckBox ID="chkReminder" Text="Reminder" runat="server" onclick="EnableDisableReminderDropDown(this)"
                                        Checked="true" TabIndex="14" />
                                    <asp:DropDownList EnableViewState="false" ID="ddlReminder" runat="server" class="CommonDropDownList"
                                        Width="100px" TabIndex="15">
                                        <asp:ListItem Value="0" Selected="True">0 minutes</asp:ListItem>
                                        <asp:ListItem Value="300">5 minutes</asp:ListItem>
                                        <asp:ListItem Value="600">10 minutes</asp:ListItem>
                                        <asp:ListItem Value="900">15 minutes</asp:ListItem>
                                        <asp:ListItem Value="1800">30 minutes</asp:ListItem>
                                        <asp:ListItem Value="3600">1 hour</asp:ListItem>
                                        <asp:ListItem Value="7200">2 hours</asp:ListItem>
                                        <asp:ListItem Value="14400">4 hours</asp:ListItem>
                                        <asp:ListItem Value="28800">8 hours</asp:ListItem>
                                        <asp:ListItem Value="43200">0.5 day</asp:ListItem>
                                        <asp:ListItem Value="86400">1 day</asp:ListItem>
                                        <asp:ListItem Value="172800">2 days</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="lblShowTimeAs" runat="server" Text="before"></asp:Label>
                                </div>
                            </div>
                            
                           <!-- Code introduced by pravin on 25/May/2016 start-->
                             <%-- <div class="TableRow" id="divtimezone" runat="server">
                                 <div class="TableFormLeble">
                                      <asp:Label EnableViewState="false" ID="lblchangetimezone" runat="server" Text="Timezone"></asp:Label>:
                                 </div>
                                 <div class="TableFormContent">
                                  <asp:DropDownList ID="ddlTimezone" runat="server" CssClass="CommonDropDown" Width="200px">
                                                                                   </asp:DropDownList>
                                   
                                 </div>
                           </div>--%>
                           
                            <div class="TableRow">
                             <div class="TableFormLeble">
                               <asp:Label ID="lblchangetimezone" runat="server" Text="Timezone"></asp:Label>:
                             </div>
                             <div class="TableFormContent" style="vertical-align: top">
                                <asp:DropDownList ID="ddlTimezone" runat="server" style="width:165px" CssClass="chzn-select" TabIndex="1">
                               </asp:DropDownList>
                              <%-- <span class="RequiredField">*</span>--%>
                            </div>
                          </div>
                       <%-- <div class="TableRow">
                         <div class="TableFormValidatorContent" style="margin-left: 32%">
                               <asp:RequiredFieldValidator ID="rfvTimezone" runat="server" ControlToValidate="ddlTimezone" ErrorMessage="Please Select Time zone."
                                InitialValue="0" Display="Dynamic" ValidationGroup="InterviewSchedule"></asp:RequiredFieldValidator>
                            </div>
                           </div>--%>
                            <!-- **********************END***********************-->
                             
                            
                             <!-- Code introduced by Prasanth on 16/Oct/2015 start-->
                             <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="Label1" runat="server" Text="Assessment Form"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList EnableViewState="true" ID="DdlInterviewQuestionBankType" runat="server" CssClass="CommonDropDownList"
                                        TabIndex="15" AutoPostBack="false">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="HdnInterviewQuestionBank" runat="server" />
                                </div>
                            </div>
                            <!-- **********************END***********************-->
                            
                            
                        <!-- Code introduced by Pravin on 9/Dec/2015 start-->
                        
                            <%--********Code modify buy pravin khot on 24/May/2016  *********--%>
                            
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="Label2" runat="server"  Text="Location"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList EnableViewState="true" ID="ddllocation" runat="server" CssClass="CommonDropDownList"
                                        TabIndex="4" AutoPostBack="true" OnSelectedIndexChanged="ddllocation_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="HiddenField3" runat="server" />
                                    <span class="RequiredField" id="splocation">*</span>
                                </div>
                            </div>
                            
                       
                            
                      <div class="TableRow">
                             <div class="TableFormContent" style="margin-left: 33%">
                              <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="ddllocation"
                               SetFocusOnError="true" InitialValue="0" ErrorMessage="Please Select Location." EnableViewState="False"
                               Display="Dynamic" ValidationGroup="InterviewSchedule"></asp:RequiredFieldValidator>
                              </div>
                      </div>
                
                            <div class="TableRow" id="divlocation" runat="server">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="lblLocation" runat="server"   Text="Address"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox EnableViewState="false" ID="txtLocation" runat="server" TextMode="MultiLine"  CssClass="CommonTextBox" Rows="3" Width="31%" TabIndex="3">
                                    </asp:TextBox>
                                </div>
                            </div>
                            
                           <%-- ***********************END*******************************--%>
                                                       
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="lblInterviewType" runat="server" Text="Interview Mode"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList EnableViewState="true" ID="ddlInterviewType" runat="server" CssClass="CommonDropDownList"
                                        TabIndex="4" AutoPostBack="false" OnSelectedIndexChanged="ddlInterviewType_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                     <span class="RequiredField" id="Span1">*</span>
                                </div>
                            </div>
                            
                             <div class="TableRow">
                            <div class="TableFormContent" style="margin-left: 33%">
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlInterviewType"
                            SetFocusOnError="true" InitialValue="0" ErrorMessage="Please Select Interview Type."
                            Display="Dynamic" ValidationGroup="InterviewSchedule"></asp:RequiredFieldValidator>
                           </div>
                           </div>
                            <!-- **********************END***********************-->
                            
                        </div>
                        <div class="FormRightColumn" style="width: 50%;">
                        
                       <%-- change by pravin khot on date 9/Dec/2015  (comment below code)--%>
                       
                           <%-- <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="lblLocation" runat="server" Text="Location"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox EnableViewState="false" ID="txtLocation" runat="server" TabIndex="3">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="lblInterviewType" runat="server" Text="Interview Mode"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList EnableViewState="true" ID="ddlInterviewType" runat="server" CssClass="CommonDropDownList"
                                        TabIndex="4" AutoPostBack="true" OnSelectedIndexChanged="ddlInterviewType_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                </div>
                            </div>--%>
                            
                              <!-- **********************END***********************-->
                              
                              
                           <!-- Code introduced by Pravin on 9/Dec/2015 start ADD-->
                           
                            <div class="TableRow" id="dvSuugestedPanel" runat="server" visible="false">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="lblSuggestedInterviewer" runat="server" Text="Suggested Panel"
                                        Style="white-space: normal;"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <div style="width: 63%; float: left;">
                                        <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                            width: 100%; height: 100px; overflow: auto">
                                            <asp:CheckBoxList ID="chkSuggestedInterviewer" runat="server" AutoPostBack="false" OnClick="javascript:checkcheckedSuggestedInterviewer(this.id)"
                                                ValidationGroup="InterviewSchedule" TabIndex="5">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                    <div style="vertical-align: top; float: left">
                                        <span class="RequiredField"></span></div>
                                </div>
                            </div>
                             <div class="TableRow">
                                <div class="TableFormValidatorContent" style="text-align: center;">
                                    <asp:CustomValidator ID="cvSuggestedInterviewer" runat="server" 
                                        Display="Dynamic" EnableClientScript="true" ErrorMessage="Select at least one Interviewer"
                                        ValidationGroup="InterviewSchedule"></asp:CustomValidator>
                                </div>
                            </div>
                            
                            <!-- **********************END***********************-->
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="lblInternalInterviewer" runat="server" Text="Recruiters"
                                        Style="white-space: normal;"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <div style="width: 63%; float: left;">
                                        <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                            width: 100%; height: 100px; overflow: auto">
                                            <asp:CheckBoxList ID="chkInternalInterviewer" runat="server" AutoPostBack="false" OnClick="javascript:checkcheckedInternalInterviewer(this.id)"
                                                ValidationGroup="InterviewSchedule" TabIndex="5">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                    <div style="vertical-align: top; float: left">
                                        <span class="RequiredField">*</span></div>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormValidatorContent" style="text-align: center;">
                                    <asp:CustomValidator ID="cvInternalInterviewer" runat="server" ClientValidationFunction="ValidateChkList"
                                        Display="Dynamic" EnableClientScript="true" ErrorMessage="Select at least one Interviewer"
                                        ValidationGroup="InterviewSchedule"></asp:CustomValidator>
                                </div>
                            </div>
                            <div class="TableRow" id="dvClient" runat="server" visible="false">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="true" ID="lblClientInterviewer" runat="server" Text="Account Interviewers"
                                        Style="white-space: normal;"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                        width: 63%; height: 100px; overflow: auto">
                                        <asp:CheckBoxList ID="chkClientInterviers" runat="server" AutoPostBack="false" ValidationGroup="InterviewSchedule" OnClick="javascript:checkcheckedClientInterviers(this.id)"
                                            TabIndex="8">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label EnableViewState="false" ID="lbOtherInterviewers" runat="server" Text="Other Interviewers"
                                        Style="white-space: normal;">
                                    </asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <div style="width: 61%; height: auto; border: solid 1px #CCC; margin-left: 34%; padding: 4px;"
                                        id="divContent" runat="server" enableviewstate="true">
                                        <asp:TextBox EnableViewState="false" ID="txtOtherInterviewers" runat="server" CssClass="CommonTextBox"
                                            TabIndex="2" placeholder="Separate emails by commas" Width="95%">   </asp:TextBox>
                                        <asp:HiddenField ID="hfOtherInterviewers" runat="server" Value="" />
                                    </div>                                    
                                </div>
                               <%-- **************ADDED BY PRAVIN KHOT ON 24/June/2016***********--%>
                                <div class="TableFormContent">
                                    <div style="width: 100%; height: auto; margin-left: 34%; padding: 4px;"
                                        id="div1" runat="server" enableviewstate="true">
                                       <asp:Label EnableViewState="false" ID="lblotheremailids" runat="server" Text="( Email ID's separated by commas(,) or Semicolon(;) or tab )"
                                        Style="white-space: normal;  font-size: smaller"> </asp:Label>                                
                                    </div>                                   
                                </div>
                                <%--****************************END*****************************--%>
                            </div>
                        </div>
                        <div style="text-align: left;">
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 16.5%">
                                    <asp:Label ID="lblRemarks" runat="server" Text="Notes"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:TextBox EnableViewState="false" ID="txtRemarks" runat="server" TextMode="MultiLine"
                                        CssClass="CommonTextBox" Rows="3" Width="81%" TabIndex="16">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 16.5%">
                                </div>
                                <div class="TableFormContent">
                                    <asp:CheckBox ID="chkAddAppointment" runat="server" Text="Add appointment to my calendar"
                                        Checked="true" TabIndex="17" />
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 16.5%">
                                </div>
                                <div class="TableFormContent">
                                    <asp:CheckBox ID="chkSendEmailAlerttoInterviewers" runat="server" Text="Send notification email to Interviewers"
                                        TabIndex="18" onclick="VisibleInvisibleDivattch(this)"/>
                                    <asp:LinkButton runat="server" ID="btnEditNotificationEmailInterviewers" CommandName="editTemplate"
                                        Text="Edit Notification Email" ToolTip="Edit Notification Email to interviewers"
                                        EnableViewState="false" OnClick="lnkEditInterviewerNotify_Click" CommandArgument="editInterviewers" />
                                </div> 
                           </div>
                            <div id="divAttachments" runat="server" style="display:none">
                            <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 20%">
                                        <asp:Label ID="lblAttachFileHeader" runat="server" Text="Attach File"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                    <asp:FileUpload ID="FileUpload1" runat="server" Width="350px" />
                                        <%--<asp:FileUpload ID="FileUpload1" runat="server" Width="350px" />--%>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormValidatorContent" style="margin-left: 20%">
                                        <asp:RequiredFieldValidator ID="rfvDocumentUpload" runat="server" ErrorMessage="Please select a file to upload."
                                            ControlToValidate="FileUpload1" ValidationGroup="UploadDocumentValid" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revfuDocument" runat="server" ErrorMessage="Invalid File Format"
                                            ControlToValidate="FileUpload1" ValidationExpression="^(?!.*\.exe$).*$" Display="Dynamic"
                                            ValidationGroup="UploadDocumentValid"></asp:RegularExpressionValidator>
                                    </div>
                                </div>



                           <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 20%">
                                        <asp:Label ID="lblAttachmentsHeader" runat="server" Text="Attachments"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <table>
                                            <tr>
                                                <td style="width: 300px;">
                                                    <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                                        width: 98%; height: 100px; overflow: auto;">
                                                        <asp:CheckBoxList ID="lstAttachments" runat="server">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </td>
                                                <td style="vertical-align: bottom">
                                                    <div style="display: inline">
                                                        <asp:Button ID="btnAtach" runat="server" CssClass="CommonButton" Text="Attach"  OnClick="btnAtach_Click"
                                                            ValidationGroup="UploadDocumentValid" />
                                                           
                                                        <asp:Button ID="btnRemove" runat="server" CssClass="CommonButton" Text="Remove" OnClick="btnRemove_Click"
                                                            ValidationGroup="RemoveDocumentValid" /></div>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:HiddenField ID="HiddenField2" runat="server" />
                                    </div>
                                </div>


                       
                         </div>  
                            <div class="TableRow">
						 
                                <div class="TableFormLeble" style="width: 16.5%">
                                </div>
                                <div class="TableFormContent">
                                    <asp:CheckBox ID="chkSendEmailAlerttoCandidates" runat="server" Text="Send notification email to Candidate"
                                        TabIndex="18" />
                                    <asp:LinkButton runat="server" ID="btnEditNotificationEmailCandidates" CommandName="editTemplate"
                                        Text="Edit Notification Email" ToolTip="Edit Notification Email to candidates"
                                        EnableViewState="false" OnClick="lnkEditCandiateNotify_Click" CommandArgument="editCandidates" />
                                </div>
                            </div>
                            <div runat="server" id="divDocument">
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 16.5%">
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:CheckBox ID="chkAttachCandidateResume" onchange="EnableDisableDocumentDropdown();"
                                            runat="server" Text="Attach candidate resume to Interviewer email" TabIndex="18" />
                                        <asp:DropDownList ID="ddlDocument" class="CommonDropDownList" runat="server" Width="145px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="TableRow" style="text-align: center">
                                <asp:Button ID="btnSave" CssClass="CommonButton" ValidationGroup="InterviewSchedule"
                                    runat="server" Text="Save" OnClick="btnSave_Click" TabIndex="18" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div>
                <asp:ObjectDataSource ID="odsInterviewSchedule" runat="server" SelectMethod="GetPaged"
                    TypeName="TPS360.Web.UI.MemberInterviewDataSource" SelectCountMethod="GetListCount"
                    EnablePaging="True" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:Parameter Name="CandidateId" DefaultValue="0" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <div class="TableRow" id="dvListView" runat="server">
                    <div class="TabPanelHeader">
                        List of Interviews
                    </div>
                    <asp:ListView ID="lsvInterviewSchedule" runat="server" DataKeyNames="Id" DataSourceID="odsInterviewSchedule"
                        EnableViewState="true" OnItemDataBound="lsvInterviewSchedule_ItemDataBound" OnItemCommand="lsvInterviewSchedule_ItemCommand"
                        OnPreRender="lsvInterviewSchedule_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                <tr>
                                    <th style="width: 90px !important;">
                                        <asp:LinkButton ID="btnDateTime" runat="server" ToolTip="Sort By Date & Time" CommandName="Sort"
                                            CommandArgument="[I].[StartDateTime]" Text="Date & Time" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnJobTitle" runat="server" CommandName="Sort" CommandArgument="[JP].[JobTitle]"
                                            Text="Requisition" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnClient" runat="server" CommandName="Sort" CommandArgument="[C].[CompanyName]"
                                            Text="Account" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnTitle" runat="server" CommandName="Sort" CommandArgument="[I].[Title]"
                                            Text="Title" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnLocation" runat="server" CommandName="Sort" CommandArgument="[I].[Location]"
                                            Text="Location" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnType" runat="server" CommandName="Sort" CommandArgument="[GL].[Name]"
                                            Text="Type" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnHeaderInterviewers" runat="server" CommandName="Sort" CommandArgument="[INV].[InterviewersName]"
                                            Text="Interviewers"></asp:LinkButton>
                                    </th>
                                    <th>
                                        Other Interviewers
                                    </th>
                                    <th>
                                        <asp:Label ID="lblNotes" runat="server" Text="Notes"></asp:Label>
                                    </th>
                                    
                                <%-- ************** added by pravin khot on 5/May/2016******--%>
                                <th>
                                      Interview Status
                                 </th>                                     
                                  <%-- ************** added by Sumit Sonawane 28/Feb/2017  ******--%>
                                  <%-- <th>
                                      Status
                                 </th>--%>
                                 <%-- ************** END******--%>
                                 
                                    <th style="text-align: center;" id="thAction" runat="server" width="50px">
                                        Actions
                                    </th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td id="tdPager" runat="server" colspan="11">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No interview data available.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td>
                                    <asp:Label ID="lblDateTime" runat="server" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lblJobTitle" runat="server"></asp:LinkButton>
                                </td>
                                <td>
                                    <asp:Label ID="lblClient" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblTitle" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblLocation" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblType" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblInterviewers" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblOtherInterviewers" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblNotes" runat="server" />
                                </td>
                              <%-- ************** added by pravin khot on 5/May/2016******--%>
                                 <td>
                                    <asp:Label ID="lblStatus" runat="server" />
                                </td>
                                <%-- <td>
                                    <asp:Label ID="lnlStatusNew" runat="server" />
                                </td>--%>
                                 <%-- ************** END******--%>
                                <td style="overflow: inherit;" runat="server" id="tdAction" >
                                    <%--<asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" CommandName="EditItem"
                                        CausesValidation="false"></asp:ImageButton>
                                    <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem"
                                        CausesValidation="false"></asp:ImageButton>
                                    <asp:HiddenField ID="hfActivityId" runat="server" />--%>
                                    <ul class="nav" style="margin-bottom: 0px; padding-left: 0px;">
                                    <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle" style="width: 30px;
                                        margin-top: -15px;" href="#">
                                            <button class="btn btn-mini" type="button">
                                                <i class="icon icon-cog"></i><span class="caret"></span>
                                            </button>
                                    </a>
                                        <ul class="dropdown-menu" style="margin-left: -120px; margin-top: -49px;">
                                            <li>
                                                <asp:LinkButton ID="btnEdit" Text="Reschedule" CausesValidation="false" CommandName="EditItem" runat="server"></asp:LinkButton>
                                            </li>
                                            <li>
                                                <asp:LinkButton ID="btnDelete" Text="Cancel" CausesValidation="false" CommandName="DeleteItem" runat="server"></asp:LinkButton>
                                                <asp:HiddenField ID="hfActivityId" runat="server" />
                                            </li>
                                             <%-- ************** added by Sumit Sonawane 28/Feb/2017  ******--%>
                                             <li>
                                                <asp:LinkButton ID="lnkbtnNoShow" Text="No-Show" CausesValidation="false" CommandName="IntrvNoShow" runat="server"></asp:LinkButton>
                                            </li>
                                             <li>
                                                <asp:LinkButton ID="lnkbtnCompleted" Text="Completed" CausesValidation="false" CommandName="IntrvCompleted" runat="server"></asp:LinkButton>
                                            </li>
                                             <%-- ********************************END***********************--%>
                                            <li>
                                                <asp:LinkButton ID="btnFeedback" Text="Recruiter Feedback" CausesValidation="false" CommandName="FeedbackItem" runat="server"></asp:LinkButton>
                                            </li>
                                            <%--**********Commented by pravin khot on 12/Feb/2016***************--%>
                                             <%-- <li>
                                                <asp:LinkButton ID="InterviewerfeedbackDetail" Text="Interviewer Feedback" CausesValidation="false" CommandName="InterviewerfeedbackDetail" runat="server"></asp:LinkButton>
                                            </li>--%>
                                           <%-- ********************************END***********************--%>
                                        </ul>
                                    </li>
                                </ul>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
        <asp:PostBackTrigger ControlID="btnAtach" />
       </Triggers>
    </asp:UpdatePanel>
</div>

<script type="text/javascript">
    Sys.Application.add_load(function() {
        $(".chzn-select").chosen();
        $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
    });
</script>

 