﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RequisitionStatusReport.ascx.cs
    Description: This is the user control page used for requisition status report.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author               Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Jan-22-2009           Yogeesh Bhat         Defect ID: 9780; Changes made in lsvInterviewSchedule_ItemDataBound()
    0.2            Feb-13-2009           Kalyani Pallagani    Defect ID: 9907; Added code in AddSelectedMemberToJobPostingHiringTeam method
 *  0.3            May-12-2009           Sandeesh             Defect id:10440 :Populated the Requisition status dropdown list from the database
    0.4            Jul-01-2009           Nagarathna V.B       Enhancement 10778 :Populated "Please Select" in Requisition status drop down.
 *  0.5            Apr-08-2010           Sudarshan.R.         Defect Id:12527 Added Timetohire label text in save button click event
 *  0.6            Apr-21-2010           Sudarshan.R.         Defect Id:12666 ; Added validation for ddlJobStatus in btnChangeJobStatus_Click event
 *  0.7            Apr-23-2010           Sudarshan.R.         Defect Id:12722; Added validation for txtAdditionalNote in btnSaveNote_Click event
 *  0.8            May-03-2010           Ganapati Bhat        Defect Id:12745; Code Modified in btnChangeJobStatus_Click event
 *  0.9            May-21-2010           Prashant Tripathi    Defect Id:12818; Replaced ToShortDateString() to ToString()
 * -------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections.Generic;
using TPS360.Web.UI.Helper;

namespace TPS360.Web.UI
{
    public partial class cltRequisitionStatusReport : ATSBaseControl
    {
        #region Properties
        private int SelectedLevelId
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_TAB]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_TAB]);
                }
                return 0;
            }
        }
        #endregion

        #region Methods

        private void SaveNote()
        {
            CommonNote commonNote = new CommonNote();
            commonNote.NoteDetail =MiscUtil .RemoveScript ( txtAdditionalNote.Text.Trim());
            commonNote.IsInternal = false;
            commonNote.NoteCategoryLookupId = 0;
            commonNote.CreatorId = base.CurrentMember.Id;
            commonNote.UpdatorId = base.CurrentMember.Id;
            commonNote = Facade.AddCommonNote(commonNote);

            if (commonNote != null)
            {
                JobPostingNote jobPostingNote = new JobPostingNote();
                jobPostingNote.JobPostingId = base.CurrentJobPostingId;
                jobPostingNote.CommonNoteId = commonNote.Id;

                jobPostingNote = Facade.AddJobPostingNote(jobPostingNote);
            }
            txtAdditionalNote.Text = string.Empty;
        }

        private void PopulateAddedNoteList()
        {
            lsvAddedNote.DataSource = Facade.GetAllJobPostingNoteByJobPostingId(base.CurrentJobPostingId);
            lsvAddedNote.DataBind();
        }

        private void PopulateHiringTeamMembers()
        {
            
            lsvAssignedTeam.DataSource = Facade.GetAllJobPostingHiringTeamByJobPostingId(base.CurrentJobPostingId);
            lsvAssignedTeam.DataBind();
        }

        private void AddSelectedMemberToJobPostingHiringTeam(ListBox listBox, EmployeeType employeeType)
        {
            int countSelected = 0;
            int countAdded = 0;
            int memberId = 0;
            string message = string.Empty;
            //if (listBox.SelectedIndex > 0) 
            {
                for (int i = 0; i <= listBox.Items.Count - 1; i++)
                {
                    if (listBox.Items[i].Selected)
                    {
                        countSelected++;
                        Int32.TryParse(listBox.Items[i].Value, out memberId);
                        if (memberId > 0)
                        {
                            JobPostingHiringTeam jobPostingHiringTeam = Facade.GetJobPostingHiringTeamByMemberId(memberId, base.CurrentJobPostingId);

                            if (jobPostingHiringTeam == null)
                            {
                                jobPostingHiringTeam = new JobPostingHiringTeam();
                                jobPostingHiringTeam.EmployeeType = employeeType.ToString();
                                jobPostingHiringTeam.JobPostingId = base.CurrentJobPostingId;
                                jobPostingHiringTeam.MemberId = memberId;
                                jobPostingHiringTeam.CreatorId = base.CurrentMember.Id;
                                Facade.AddJobPostingHiringTeam(jobPostingHiringTeam);
                                countAdded++;
                            }
                        }
                    }
                }
                if (countSelected > 0)
                {
                    if (countAdded > 0)
                    {
                        int countNotAdded = countSelected - countAdded;
                        if (countNotAdded == 0)
                        {
                            message = "Successfully added all the selected members.";
                        }
                        else
                        {
                            message = "Successfully added " + countAdded + " of " + countSelected + " selected members.</br>The remaining members are already in the list</b></font>";
                        }
                    }
                    else
                    {
                        message = "The selected member(s) are already in the list";
                    }
                }
                else 
                {
                    message = "Select at least one member to add.";
                }
            }
           // else
           // {
           //     message = "Please Select a member to add";
          //  }

            MiscUtil.ShowMessage(lblMessage, message, false);
        }

        private void PrepareView()
        {
            MiscUtil.PopulateRequistionStatus(ddlJobStatus, Facade);
            ddlJobStatus.Items.Insert(0, new ListItem("Please Select", "0"));
            ddlJobStatus.SelectedValue = base.CurrentJobPosting.JobStatus.ToString();
            MiscUtil.PopulateMemberListByRole(lstRecruiters, ContextConstants.ROLE_EMPLOYEE, Facade);
            lstRecruiters.Items.RemoveAt(0);
            PopulateHiringTeamMembers();
            lblTimeToHire.Text = "<strong>Date Published:</strong> " + (MiscUtil.IsValidDate(base.CurrentJobPosting.ActivationDate) ? base.CurrentJobPosting.ActivationDate.ToShortDateString() : "<strong>Not Set</strong>") + "<br/><strong>Closing Date: </strong>" + (MiscUtil.IsValidDate(base.CurrentJobPosting.FinalHiredDate) ? base.CurrentJobPosting.FinalHiredDate.ToShortDateString() : "<strong>Not Set</strong>") + "<br/><strong>Total: </strong>" + MiscUtil.FindDateDiffFrmEndtoStart(base.CurrentJobPosting.FinalHiredDate, base.CurrentJobPosting.ActivationDate);

            if ((CurrentJobPosting.AllowRecruitersToChangeStatus == true && Facade.GetJobPostingHiringTeamByMemberId(CurrentMember.Id, CurrentJobPosting.Id) != null) || CurrentJobPosting.CreatorId == base.CurrentMember.Id)
            {
            }
            else
            {
                ddlJobStatus.Enabled =  btnChangeJobStatus.Enabled = false;
            }
        }

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
           
           // odsEventLog.SelectParameters["JobPostingId"].DefaultValue = CurrentJobPosting.Id.ToString();
          //  odsEventLog.SelectParameters["CandidateId"].DefaultValue = "0";
           if(SelectedLevelId==0) 
           this.Page.Title = "Requisition Status & Notes";
           
            if (!IsPostBack)
            {
                uclHiringLog.Bind();
               // hdnSortColumn.Value = "btnDate";
                //hdnSortOrder.Value = "desc";
                //lsvHiringLog.DataBind();
                PrepareView();
                ddlJobStatus.Focus();
            }
            PopulateAddedNoteList();
        }
        #endregion

        #region Button Events
        protected void btnChangeJobStatus_Click(object sender, EventArgs e)
        {
            
            bool isStatusChanged = false;
            string message = string.Empty;
            if (Convert.ToInt32(ddlJobStatus.SelectedValue) > 0)
            {
                //if (IsValid)
                {
                    try
                    {
                        JobPosting jobPosting = base.CurrentJobPosting;
                        jobPosting.JobStatus = Int32.Parse(ddlJobStatus.SelectedValue);
                        jobPosting.UpdatorId = base.CurrentMember.Id;
                        ApplicationWorkflow applicationWorkflow = Facade.GetApplicationWorkflowByTitle(WorkFlowTitle.RequisitionManagementShouldSelectedApprovedUsersAbilityToChangeStats.ToString());
                        if (applicationWorkflow != null && applicationWorkflow.RequireApproval)
                        {
                            IList<ApplicationWorkflowMap> applicationWorkflowMapList = Facade.GetAllApplicationWorkflowMapByApplicationWorkflowId(applicationWorkflow.Id);
                            if (applicationWorkflowMapList != null && applicationWorkflowMapList.Count > 0)
                            {
                                foreach (ApplicationWorkflowMap applicationWorkflowMap in applicationWorkflowMapList)
                                {
                                    if (applicationWorkflowMap.MemberId == base.CurrentMember.Id)
                                    {
                                        Facade.UpdateJobPosting(jobPosting);
                                        isStatusChanged = true;
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            Facade.UpdateJobPosting(jobPosting);
                            isStatusChanged = true;
                        }
                        MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionStatusChange, CurrentJobPosting.Id, CurrentMember.Id, Facade);
                    }
                    catch (ArgumentException ax)
                    {
                        MiscUtil.ShowMessage(lblMessage, ax.Message, true);
                    }
                }
                if (isStatusChanged)
                {
                    MiscUtil.ShowMessage(lblMessage, "Successfully Updated Status", false);
                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "Approval require to change the status.", true);
                }
            }
            else
            {
                message = "Please select Job Status.";
                MiscUtil.ShowMessage(lblMessage, message, false); 
            }
        }

        protected void btnAddRecruiters_Click(object sender, EventArgs e)
        {
            AddSelectedMemberToJobPostingHiringTeam(lstRecruiters, EmployeeType.Internal);
            MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionAssigned, CurrentJobPosting.Id, CurrentMember.Id, Facade);
            PopulateHiringTeamMembers();
        }

        protected void btnSaveNote_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            if (!String.IsNullOrEmpty(MiscUtil .RemoveScript ( Convert.ToString(txtAdditionalNote.Text))))
            {
                SaveNote();
                PopulateAddedNoteList();
                PopulateHiringTeamMembers();
            }
            else
            {
                message = "Please Add Notes.";
                PopulateAddedNoteList();
                MiscUtil.ShowMessage(lblMessage, message, false);
            }
        }
        #endregion

        #region List View Events
        protected void lsvAssignedTeam_ItemDataBound(object sender, ListViewItemEventArgs e)
        {            
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPostingHiringTeam jobPostingHiringTeam = ((ListViewDataItem)e.Item).DataItem as JobPostingHiringTeam;

                if (jobPostingHiringTeam != null)
                {
                    Label lblAssignedDate = (Label)e.Item.FindControl("lblAssignedDate");
                    Label lblMemberName = (Label)e.Item.FindControl("lblMemberName");
                    Label lblMemberEmail = (Label)e.Item.FindControl("lblMemberEmail");

                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    lblAssignedDate.Text = jobPostingHiringTeam.CreateDate.ToShortDateString ();
                    if (jobPostingHiringTeam.MemberId > 0)
                    {
                        if (jobPostingHiringTeam.EmployeeType == EmployeeType.Internal.ToString())
                        {
                            Member member = Facade.GetMemberById(jobPostingHiringTeam.MemberId);
                            if (member != null)
                            {
                                lblMemberName.Text = member.FirstName + " " + member.LastName;
                                lblMemberEmail.Text = member.PrimaryEmail;
                            }
                        }
                        else if (jobPostingHiringTeam.EmployeeType == EmployeeType.External.ToString())
                        {
                            CompanyContact companyContact = Facade.GetCompanyContactById(jobPostingHiringTeam.MemberId);
                            if (companyContact != null)
                            {
                                lblMemberName.Text = companyContact.FirstName + " " + companyContact.LastName;
                                lblMemberEmail.Text = companyContact.Email;
                            }
                        }
                    }
                    if (jobPostingHiringTeam.MemberId == CurrentJobPosting.CreatorId)
                        btnDelete.Visible = false;
                    else
                        btnDelete.Visible = true;
                    btnDelete.OnClientClick = "return ConfirmDelete('Hiring Team Member')";
                    btnDelete.CommandArgument = StringHelper.Convert(jobPostingHiringTeam.Id);
                }
            }
        }

        protected void lsvAssignedTeam_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteJobPostingHiringTeamById(id))
                        {
                            PopulateHiringTeamMembers();
                            MiscUtil.ShowMessage(lblMessage, "Hiring team member has been successfully deleted.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }

        protected void lsvAddedNote_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPostingNote jobPostingNote = ((ListViewDataItem)e.Item).DataItem as JobPostingNote;

                if (jobPostingNote != null)
                {
                    Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
                    Label lblAddedBy = (Label)e.Item.FindControl("lblAddedBy");
                    Label lblNote = (Label)e.Item.FindControl("lblNote");
                    CommonNote commonNote = Facade.GetCommonNoteById(jobPostingNote.CommonNoteId);
                    lblDateTime.Text = commonNote.CreateDate.ToString();
                    lblAddedBy.Text = MiscUtil.GetMemberNameById(commonNote.CreatorId, Facade);
                    lblNote.Text = commonNote.NoteDetail;
                }
            }
        }


        //protected void lsvHiringLog_ItemDataBound(object sender, ListViewItemEventArgs e)
        //{
        //    if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
        //    {
        //        EventLogForRequisitionAndCandidate log = ((ListViewDataItem)e.Item).DataItem as EventLogForRequisitionAndCandidate;
        //        if (log != null)
        //        {
        //            Label lblDate = (Label)e.Item.FindControl("lblDate");
        //            Label lblUser = (Label)e.Item.FindControl("lblUser");
        //            Label lblAction = (Label)e.Item.FindControl("lblAction");
        //            lblDate.Text = log.ActionDate.ToString();
        //            lblUser.Text = log.UserName;
        //            lblAction.Text = log.ActionType;
        //        }
        //    }
        //}
        //protected void lsvHiringLog_PreRender(object sender, EventArgs e)
        //{
        //    PlaceUpDownArrow();
        //}
        //private void PlaceUpDownArrow()
        //{
        //    try
        //    {
        //        LinkButton lnk = (LinkButton)lsvHiringLog.FindControl(hdnSortColumn.Value);
        //        ImageButton imgUp = new ImageButton();
        //        imgUp.Style.Add("disploay", "inline");
        //        imgUp.Style.Add("float", "right");
        //        lnk.Style.Add("float", "left");
        //        lnk.Style.Add("disploay", "inline");
        //        imgUp.Enabled = false;
        //        imgUp.ID = "imgArrow";
        //        if (hdnSortOrder.Value == "ASC") imgUp.ImageUrl = "../Images/uparrow-header.gif";
        //        else imgUp.ImageUrl = "../Images/downarrow-header.gif";

        //        ImageButton im = (ImageButton)lnk.Parent.FindControl("imgArrow");
        //        if (im == null)
        //        {
        //            if (lnk.Parent.ID != "thLevels") lnk.Parent.Controls.Add(imgUp);
        //            else lnk.Parent.Controls.AddAt(1, imgUp);
        //        }
        //    }
        //    catch
        //    {
        //    }

        //}
       
        //protected void lsvHiringLog_ItemCommand(object sender, ListViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName == "Sort")
        //        {
        //            LinkButton lnkbutton = (LinkButton)e.CommandSource;
        //            if (hdnSortColumn.Value == lnkbutton.ID)
        //            {
        //                if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
        //                else hdnSortOrder.Value = "ASC";
        //            }
        //            else
        //            {
        //                hdnSortColumn.Value = lnkbutton.ID;
        //                hdnSortOrder.Value = "ASC";
        //            }
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}
        //protected void btnExport_Click(object sender, EventArgs args)
        //{
        //    string filename = "ReqHiringLog-" + CurrentJobPosting.JobPostingCode + "-" +  DateTime.Now.ToShortDateString() + ".xls";
        //    Response.AddHeader("content-disposition", "attachment;filename=" + filename);
        //    Response.ContentEncoding = System.Text.Encoding.ASCII;
        //    Response.ContentType = "application/msexcel";
        //    Response.Output.Write(MiscUtil.getEventLogReport(CurrentJobPostingId, 0,0, Facade));
        //    Response.Flush();
        //    Response.End();
        //}
        #endregion

       
    }
}
