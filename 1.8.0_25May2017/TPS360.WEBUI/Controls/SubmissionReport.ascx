﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:SubmissionReport.ascx
    Description By:This form is common for SubmissionReport.aspx and MySubmissionReport.aspx 
    Created By:Sumit Sonawane
    Created On:25/May/2016
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------

    ---------------------------------------------------------------------------------------------------------------------------------------     

--%>


<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubmissionReport.ascx.cs"
    Inherits="TPS360.Web.UI.ControlSubmissionReport" %>

<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName = "DateRangePicker" TagPrefix ="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
    <script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
    <script src="../js/EmployeeAndTeamOnChange.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
     
    
        
        //To Keep the scroll bar in the Exact Place
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
        function PageLoadedReq(sender, args) {
            try {
                var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
                var bigDiv = document.getElementById('ctl00_cphHomeMaster_divlsvSubmission');
                var ddlSubmittedBy=document .getElementById ('<%= hdnSubmittedBy.ClientID %>');
                bigDiv.scrollLeft = hdnScroll.value;
            }
            catch (e) {
            }
        }
        function SetScrollPosition() {
            var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
            var bigDiv = document.getElementById('ctl00_cphHomeMaster_divlsvSubmission');
            hdnScroll.value = bigDiv.scrollLeft;
        }
        function SelectUnselectColumnOption(chkColumnOption) {
            var chkBoxAll = $get('<%= chkColumns.ClientID%>');
            var chkBoxList = $get('<%= chkColumnList.ClientID%>');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            if (chkBoxAll.checked) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = true;
                }
            }
            if (!chkBoxAll.checked) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = false;
                }
            }
        }
        function SelectUnselectAllColumnOption(chkColumnOptionHeader) {
            var checkedCount = 0;
            //debugger;
            var chkBoxAll = $get('<%= chkColumns.ClientID%>');
            var chkBoxList = $get('<%= chkColumnList.ClientID%>');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            for (var i = 0; i < chkBoxCount.length; i++) {
                if (chkBoxCount[i].checked) {
                    checkedCount++;
                }
            }
            if (checkedCount == chkBoxCount.length) {
                chkBoxAll.checked = true;
            }
            else {
                chkBoxAll.checked = false;
            }
        }
    </script>

    <asp:TextBox ID="hdnSortColumn" runat="server" Visible="false" />
    <asp:TextBox ID="hdnSortOrder" runat="server" Visible="false" />
    <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
    <asp:UpdatePanel ID="pnlReceivedtEmailHistory" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="text-align: left">
                <asp:Label ID="lbMessage" runat="server" EnableViewState="false"></asp:Label>
            </div>
            <asp:HiddenField ID="hdnScrollPos" runat="server" />
            <asp:ObjectDataSource ID="odsJobSubmissionList" runat="server" SelectMethod="GetPaged"
                TypeName="TPS360.Web.UI.JobPostingDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:ControlParameter ControlID="wcdJobPostedDate" Name="JobPostStartDate" PropertyName="StartDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="wcdJobPostedDate" Name="JobPostEndDate" PropertyName="EndDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="wdcJobSubmitedDate" Name="JobSubmitStartDate"
                        PropertyName="StartDate" Type="String" />
                    <asp:ControlParameter ControlID="wdcJobSubmitedDate" Name="JobSubmitEndDate" PropertyName="EndDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="wdcReqDate" Name="ReqStartDate" PropertyName="StartDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="wdcReqDate" Name="ReqEndDate" PropertyName="EndDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlApplicantTpye" Name="ApplicantTpye" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlReqOwner" Name="ReqOwner" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlSubmittedBy" Name="SubmittedBy" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlAccount" Name="Account" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="hdnSelectedContactID" Name="Submittedto" PropertyName="Value"
                        Type="String" />
                        <asp:Parameter Name="IsTeamReport" Type="Boolean" DefaultValue="false" />
                        <asp:Parameter Name="TeamLeaderId" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter ControlID="ddlTeamList" Name="TeamId" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Panel ID="pnlSearchRegion" runat="server" DefaultButton="btnSearch">
                <div style="width: 100%;">
                    <div>
                        <div class="TableRow">
                            <div class="TabPanelHeader nomargin" style="border-style: none none none;">Filter Options</div>
                        </div>
                        <div class="TableRow" style="text-align: center">
                            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <div class="FormLeftColumn" style="width: 48%">
                            <div class="TableRow" style="white-space: nowrap">
                                <div class="TableFormLeble" style="width: 40%;">
                                    Date Posted:
                                </div>
                                <div class="TableFormContent">
                                    <ucl:DateRangePicker ID="wcdJobPostedDate" runat ="server"  />
                                </div>
                            </div>
                            <div class="TableRow" style="white-space: nowrap">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="False" ID="lblDateHeader" runat="server" Text="Date Submitted"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                     <ucl:DateRangePicker ID="wdcJobSubmitedDate" runat ="server"  />
                                   
                                </div>
                            </div>
                            <div class="TableRow" style="white-space: nowrap">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="False" ID="Label1" runat="server" Text="Req. Start Date"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                     <ucl:DateRangePicker ID="wdcReqDate" runat ="server"  />
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="false" ID="Label2" runat="server" Text="Candidate Type"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlApplicantTpye" runat="server" CssClass="CommonDropDownList">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="FormRightColumn" style="width: 48%">
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="false" ID="lblByReqOwner" runat="server" Text="Req. Owner"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlReqOwner" runat="server" CssClass="CommonDropDownList">
                                    </asp:DropDownList>
                                </div>
                            </div>
                  
                            <div id="divTeamList" visible="false" runat="server">
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="false" ID="lblTeamList" runat="server" Text="Team"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlTeamList" CssClass="CommonDropDownList" AutoPostBack="false" runat="server">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnTeamList" runat="server" />
                                </div>
                            </div>
                            </div>
                            
                            <div id="divSubmittedBy" runat="server" class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="false" ID="lblSubmittedBy" runat="server" Text="Submitted By"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlSubmittedBy" CssClass="CommonDropDownList" runat="server">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnSubmittedBy" Value="0" runat="server" />
                                </div>
                            </div>
                 
                             
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label ID="lblAccount" runat="server"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlAccount" runat="server" CssClass="CommonDropDownList">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="TableRow">
                                <div class="TableFormLeble" style="width: 40%;">
                                    <asp:Label EnableViewState="false" ID="lblSubmittedTo" runat="server" Text="Submitted To"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <asp:DropDownList ID="ddlSubmittedto" CssClass="CommonDropDownList" runat="server">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnSelectedContactID" runat="server" />
                                </div>
                            </div>
                           
                            
                        </div>
                        <div class="TableRow well well-small nomargin">
                            <asp:CollapsiblePanelExtender ID="cpnlCandidateTopBar" runat="server" TargetControlID="pnlContent"
                                ExpandControlID="pnlHeader" CollapseControlID="pnlHeader" Collapsed="true" ImageControlID="imgShowHide"
                                CollapsedImage="~/Images/expand-plus.png" ExpandedImage="~/Images/collapse-minus.png"
                                SuppressPostBack="true">
                            </asp:CollapsiblePanelExtender>
                            <asp:Panel ID="pnlHeader" runat="server">
                                <div class="" style="clear: both;  cursor :pointer ;">
                                    <asp:Image ID="imgShowHide" ImageUrl="~/Images/expand-plus.png" runat="server" />
                                    Included Columns
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlContent" runat="server" Style="overflow: hidden;" Height="0">
                                <div class="TableRow" style="text-align: left; padding-top: 5px">
                                    <asp:CheckBox runat="server" Text="Select All" ID="chkColumns" CssClass ="ColumnSelection" onclick="javascript:SelectUnselectColumnOption(this);" />
                                    <asp:CheckBoxList ID="chkColumnList" runat="server" RepeatColumns="2" CssClass ="ColumnSelection" onclick="javascript:SelectUnselectAllColumnOption('chkColumns')">
                                    <asp:ListItem Value ="Id" Selected ="True" >Candidate ID #</asp:ListItem>
                                        <asp:ListItem Value="DateSubmitted" Selected="True">Date Submitted</asp:ListItem>
                                        <asp:ListItem Value="JobTitle" Selected="True">Req. Title</asp:ListItem>
                                        <asp:ListItem Value="JobPostingCode" Selected="True">Req. Code</asp:ListItem>
                                        <asp:ListItem Value="ApplicantName" Selected="True">Applicant Name</asp:ListItem>
                                        <asp:ListItem Value="BU" Selected="True">BU</asp:ListItem>
                                        <asp:ListItem Value="BUContact" Selected="True">BU Contact</asp:ListItem>
                                        <asp:ListItem Value="SubmittedBy" Selected="True">Submitted By</asp:ListItem>
                                        <asp:ListItem Value="SubmittedTo" Selected="True">Submitted To</asp:ListItem>
                                        <asp:ListItem Value="SubmissionEmail" Selected="True">Submission Email</asp:ListItem>
                                        <asp:ListItem Value="ApplicantType">Candidate Type</asp:ListItem>
                                        
                                        <asp:ListItem Value="JobDurationLookupId">Req. Employment Type</asp:ListItem>
                                       <%-- <asp:ListItem Value="JobLocation">Req. Location</asp:ListItem>--%>
                                        <asp:ListItem Value="JobStatus">Req. Status</asp:ListItem>
                                    </asp:CheckBoxList>
                                </div>
                            </asp:Panel>
                        </div>
                        <asp:CollapsiblePanelExtender ID="cpnlHiringMatrix" runat="server" TargetControlID="pnlGroupHeader"
                            ExpandControlID="pnlGroupHeader" CollapseControlID="pnlGroupHeader" Collapsed="true"
                            ImageControlID="imgShowHideHirining" CollapsedImage="~/Images/expand_all.gif"
                            ExpandedImage="~/Images/collapse_all.gif" SuppressPostBack="false">
                        </asp:CollapsiblePanelExtender>
                        <asp:Panel ID="pnlGroupHeader" runat="server" Visible="false">
                            <div class="CommonHeader" style="clear: both;">
                                <asp:Image ID="imgShowHideHirining" ImageUrl="~/Images/expand_all.gif" runat="server" />
                                Grouping Options
                            </div>
                        </asp:Panel>
                        <div class="TableRow" style="text-align: center; padding-top: 5px">
                            <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search" CssClass="btn btn-primary"
                                EnableViewState="false" OnClick="btnSearch_Click" ><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                            <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                                EnableViewState="false" CausesValidation="false" OnClick="btnClear_Click" />
                        </div>
                        <div class="TableRow" style="padding-bottom: 5px" id="divExportButtons" runat="server"
                            visible="false">
                            <div class="TabPanelHeader nomargin" style="margin-bottom: 5px;border-style: none none none;">Report Results</div>
                             <asp:ImageButton ID="btnExportToExcel" CssClass ="btn"  SkinID ="sknExportToExcel" runat ="server" ToolTip ="Export To Excel" OnClick ="btnExportToExcel_Click" />
                            <asp:ImageButton ID="btnExportToPDF" CssClass ="btn"  SkinID ="sknExportToPDF" runat ="server" ToolTip ="Export To PDF" OnClick ="btnExportToPDF_Click" />
                            <asp:ImageButton ID="btnExportToWord" CssClass ="btn"  SkinID ="sknExportToWord" runat ="server" ToolTip ="Export To Word" OnClick ="btnExportToWord_Click" />
                            
                            <%-- 
                             <asp:Button ID="btnExportToPDF" runat="server" Text="Export To PDF" AlternateText="Export To PDF"
                                CssClass="btn" EnableViewState="false" OnClick="btnExportToPDF_Click" />
                            <asp:Button ID="btnExportToExcel" runat="server" Text="Export To Excel" AlternateText="Export To Excel"
                                CssClass="btn" EnableViewState="false" CausesValidation="false" OnClick="btnExportToExcel_Click" />
                            <asp:Button ID="btnExportToWord" runat="server" Text="Export To Word" AlternateText="Export To Word"
                                CssClass="btn" EnableViewState="false" CausesValidation="false" OnClick="btnExportToWord_Click" />
                                --%>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlGridRegion" runat="server">
                <div id="divlsvSubmission" runat="server" class="GridContainer" style="overflow: auto;
                    overflow-y: hidden; width: 100%; text-align: left;" onscroll='SetScrollPosition()'>
                    <asp:ListView ID="lsvSubmission" runat="server" DataKeyNames="Id" OnItemDataBound="lsvSubmission_ItemDataBound"
                        OnItemCommand="lsvSubmission_ItemCommand" OnPreRender="lsvSubmission_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid ReportGrid" cellspacing="0" border="0">
                                <tr>
                              
                                    <th  runat="server" id="thDateSubmitted" style="min-width: 120px" enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnDateSubmitted" CommandName="Sort" CommandArgument="[MS].[SubmittedDate]"
                                            Text="Date Submitted" ToolTip="Sort By Date Submitted" />
                                    </th>
                                    <th runat="server" id="thReqTitle" style="min-width: 80px" enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnReqTitle" CommandName="Sort" CommandArgument="JobTitle"
                                            Text="Req. Title" ToolTip="Sort By Req title" />
                                    </th>
                                    <th runat="server" id="thJobPostingCode" style="min-width: 100px" enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnJobPostingCode" CommandName="Sort" CommandArgument="JobPostingCode"
                                            Text="Req. Code" ToolTip="Sort By Req code" />
                                    </th>
                                      <th  runat="server" id="thId"  >
                                        <asp:LinkButton runat="server" ID="lnkId" CommandName="Sort" CommandArgument="[MN].[ID]"
                                            Text="Candidate ID #" ToolTip="Sort By Date Candidate ID #" />
                                    </th>
                                    <th runat="server" id="thApplicantName" style="min-width: 120px" enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnApplicantName" CommandName="Sort" CommandArgument="[MN].[FirstName]+[MN].[LastName]"
                                            Text="Applicant Name" ToolTip="Sort By Applicant Name" />
                                    </th>
                                    <th runat="server" id="thAccount" style="min-width: 120px" enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnAccount" CommandName="Sort" CommandArgument="[C].[CompanyName]"
                                            Text="Account" ToolTip="Sort By Account" />
                                    </th>
                                    <th runat="server" id="thAccountContact" style="min-width: 120px" enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnAccountContact" CommandName="Sort" CommandArgument="[CC].[FirstName]+[CC].[LastName]"
                                            Text="Account Contact" ToolTip="Sort By Account Contact" />
                                    </th>
                                    <th runat="server" id="thSubmittedBy" style="min-width: 120px" enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnSubmittedBy" CommandName="Sort" CommandArgument="[M].[FirstName]+[M].[LastName]"
                                            Text="Submitted By" ToolTip="Sort By Submitted By" />
                                    </th>
                                    <th runat="server" id="thSubmittedTo" style="min-width: 120px" enableviewstate ="false" >
                                        Submitted To
                                        <%--<asp:LinkButton runat="server" ID="btnSubmittedTo" CommandName="Sort" CommandArgument="[M].[FirstName]+[M].[LastName]" Text="Submitted By" ToolTip="Submitted By" />--%>
                                    </th>
                                    <th runat="server" id="thSubmissionEmail" style="min-width: 130px" enableviewstate ="false" >
                                        Submission Email
                                        <%--<asp:LinkButton runat="server" ID="btnSubmissionEmail" CommandName="Sort" CommandArgument="[M].[PrimaryEmail]" Text="Submission Email" ToolTip="Submission Email" />--%>
                                    </th>
                                    <th runat="server" id="thApplicantType" style="min-width: 120px" enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnApplicantType" CommandName="Sort" CommandArgument="[MN].[MemberType]"
                                            Text="Candidate Type" ToolTip="Sort By Candidate Type" />
                                    </th>
                                    
                                    <th runat="server" id="thJobDurationLookupId" style="min-width: 170px" enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnJobDurationLookupId" CommandName="Sort" CommandArgument="[GLET].Name"
                                            Text="Req. Employment Type" ToolTip="Sort By Req Employment Type" />
                                    </th>
                                <%--    <th runat="server" id="thJobLocation" style="min-width: 120px" enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnJobLocation" CommandName="Sort" CommandArgument="[J].[City]"
                                            Text="Req. Location" ToolTip="Sort By Req Location" />
                                    </th>--%>
                                    <th runat="server" id="thReqStatus" style="min-width: 120px" enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnReqStatus" CommandName="Sort" CommandArgument="[GL].[Name]"
                                            Text="Req. Status" ToolTip="Sort By Req Status" />
                                    </th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td id="tdpager" runat="server">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                              
                                <td runat="server" id="tdDtSubmitted">
                                    <asp:Label ID="DateSubmitted" runat="server" Width="120px"></asp:Label>
                                </td>
                                <td runat="server" id="tdReqTitle">
                                    <asp:HyperLink ID="lnkJobTitle" runat="server"  Width="80px"></asp:HyperLink>
                                </td>
                                <td runat="server" id="tdJobPostingCode">
                                    <asp:Label runat="server" ID="lblJobPostingCode" Width="80px" />
                                </td>
                                  <td runat="server" id="tdId">
                                    <asp:Label ID="lblCandidateID" runat="server" Width="120px"></asp:Label>
                                </td>
                                <td runat="server" id="tdApplicantname">
                                    <asp:HyperLink ID="lnkApplicantName" runat="server" ></asp:HyperLink>
                                </td>
                                <td runat="server" id="tdAccount">
                                    <asp:HyperLink ID="lnkAccount" runat="server"  ></asp:HyperLink>
                                </td>
                                <td runat="server" id="tdAccountContact">
                                    <asp:Label runat="server" ID="lblAccountContact"  />
                                </td>
                                <td runat="server" id="tdSubmittedBy">
                                    <asp:Label runat="server" ID="lblSubmittedBy"  />
                                </td>
                                <td runat="server" id="tdSubmittedTo">
                                    <asp:Label runat="server" ID="lblSubmittedTo"  />
                                </td>
                                <td runat="server" id="tdSubmissionEmail">
                                    <asp:LinkButton ID="lnkSubmissionEmail" runat="server" CommandName="Email" Text="View Email">
                                        </asp:LinkButton>
                                </td>
                                <td runat="server" id="tdApplicantType">
                                    <asp:Label runat="server" ID="lblApplicantType"  />
                                </td>
                               
                                <td runat="server" id="tdJobDurationLookupId">
                                    <asp:Label runat="server" ID="lblJobDurationLookupId"  />
                                </td>
                               <%-- <td runat="server" id="tdLocation">
                                    <asp:Label runat="server" ID="lblLocation" />
                                </td>--%>
                                <td runat="server" id="tdStatus">
                                    <asp:Label runat="server" ID="lblStatus"  />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlmodal" runat="server" Style="display: none">
                <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                    <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="Modal" runat="server" TargetControlID="pnlmodal"
                PopupControlID="pnlmodal" BackgroundCssClass="divModalBackground">
            </ajaxToolkit:ModalPopupExtender>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            <asp:PostBackTrigger ControlID="btnExportToWord" />
        </Triggers>
    </asp:UpdatePanel>