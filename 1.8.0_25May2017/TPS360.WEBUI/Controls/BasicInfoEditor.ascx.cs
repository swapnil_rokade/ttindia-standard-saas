﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: BasicInfoEditor.ascx.cs
    Description: This is the user control page used in resume builder to allow user to provide basic information.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              19-Feb-2009            Jagadish            Defect id: 9828; Changes made in PrepareEditView().
  * 0.2              14-Apr-2009            Sandeesh            Defect id 10333 :Changes made in BuildMemberDetail()
    0.3              Apr-22-2009         Shivanand              Defect #10218; Changes made in the methods SaveMember().
 * 0.4               Apr-21-2010            Sudarshan.R.        Defect ID:12683 Changes made to load default country from Admin Settings
 * 0.5               10/Feb/2016            pravin khot         addede by on page load divCandidateSource.Visible = false;
   0.6               10/Jun/2016            Prasanth Kumar G    Introduced LDAP and UserName
 * 0.7               11/Nov/2016            Sumit Sonawane      Changed Cursor style.
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using TPS360.Web.UI.Helper;
using System.Web.Security; 
using System.Text.RegularExpressions;
namespace TPS360.Web.UI
{

    public partial class ControlsBasicInfoEditor : BaseControl
    {

        #region Private Variables

        private int _memberId = 0;
        private static string _memberrole = string.Empty;

        MemberDetail _memberDetail;
        Member _member;

        private static Member memberArchiveInfo = null;
        private static MemberDetail memberDetailArchiveInfo = null;
        private static Hashtable hashTableArchiveInfo = null;

        #endregion

        #region Properties

        private Member CurrentMemberInfo
        {
            get
            {
                if (_member == null)
                {

                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                    {
                        _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                    }
                    else
                    {
                        _memberId = base.CurrentMember.Id;
                    }

                    if (_memberId > 0)
                    {
                        _member = Facade.GetMemberById(_memberId);
                        if(_member !=null)
                        memberArchiveInfo = (Member)_member.Clone();
                    }

                    if (_member == null)
                    {
                        _member = new Member();
                    }
                }
                return _member;
            }
        }

        private MemberDetail CurrentMemberDetails
        {
            get
            {
                if (_memberDetail == null)
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                    {
                        _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                    }

                    if (_memberId > 0)
                    {
                        _memberDetail = Facade.GetMemberDetailByMemberId(_memberId);
                    }
                    else
                    {
                        _memberDetail = Facade.GetMemberDetailByMemberId(base.CurrentMember.Id);
                    }

                    if (_memberDetail == null)
                    {
                        _memberDetail = new MemberDetail();
                    }
                    else
                    {
                        memberDetailArchiveInfo = (MemberDetail)_memberDetail.Clone();
                    }
                }
                return _memberDetail;
            }
        }

        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        public int WorkFlowId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_WorkFlowId"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_WorkFlowId"] = value;
            }
        }

        public bool IsInformationArchived
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_IsInformationArchived"] ?? false);
            }
            set
            {
                ViewState[this.ClientID + "_IsInformationArchived"] = value;
            }
        }

        #endregion

        #region Methods

        private void SaveMember()
        {
            if (IsValid)
            {
                try
                {
                    Member member = BuildMember();
                    MemberDetail memberDetail = BuildMemberDetail();
                    if (member.FirstName.Trim().ToString() != string.Empty && member.LastName.Trim().ToString() != string.Empty)
                    {
                        if (member.IsNew)
                        {
                            Facade.AddMember(member);
                            Facade.AddMemberDetail(memberDetail);
                            MiscUtil.ShowMessage(lblMessage, "Basic information has been added successfully.", false);
                        }
                        else
                        {
                            Facade.UpdateMember(member);
                            Facade.UpdateMemberDetail(memberDetail);
                            Facade.UpdateUpdateDateByMemberId(member.Id);

                            if (divCandidateSource .Visible && ddlSource.Visible )
                            {
                                MemberExtendedInformation exinfo = Facade.GetMemberExtendedInformationByMemberId(member.Id);
                                string source = exinfo.SourceLookupId.ToString();
                                string srcdesc=exinfo.SourceDescription;
                                exinfo.SourceLookupId = Convert.ToInt32(ddlSource.SelectedValue);
                                switch (ddlSource.SelectedItem.Text)
                                {
                                    //case 2:
                                    //    //exinfo.SourceDescription = hdnSelectedVendorContact.Value;
                                    //    if (hdntemp.Value != "0")
                                    //    {
                                    //        exinfo.SourceDescription = ddlVendor.SelectedValue + '-' + hdntemp.Value;
                                    //    }
                                    //    break;
                                    case "Job Portals":
                                        exinfo.SourceDescription = ddlSourceDescription.SelectedValue;
                                        break;
                                    case "Employee Referral":
                                        exinfo.SourceDescription = ddlEmployeeReferrer.SelectedValue;
                                        break;
                                    default:
                                        exinfo.SourceDescription = txtsrcdesc.Text;
                                        break;
                                }
                                Facade.UpdateMemberExtendedInformation(exinfo);
                                if (source != ddlSource.SelectedValue.ToString () || srcdesc != exinfo.SourceDescription)
                                {
                                    int src = Convert.ToInt32(ddlSource.SelectedValue);
                                    TPS360.Common.BusinessEntities.MemberSourceHistory history = BuildMemberSourceHistory(member.Id, src, exinfo.SourceDescription);                                
                                    Facade.AddMemberSourceHistory(history);
                                }

                            }
                            if (hfOldEmail.Value != string.Empty)
                                Membership.DeleteUser(hfOldEmail.Value.Trim());
                            MiscUtil.ShowMessage(lblMessage, "Successfully updated Basic Information.", false);
                            if (IsInformationArchived)
                            {
                                hashTableArchiveInfo = new Hashtable();
                                if (memberArchiveInfo != member)
                                {
                                    if (memberArchiveInfo.PrimaryEmail != member.PrimaryEmail)
                                    {
                                        hashTableArchiveInfo.Add("PrimaryEmail", memberArchiveInfo.PrimaryEmail);
                                    }
                                    //Code introduced by Prasanth on 10/Jun/2016 Start
                                    if (memberArchiveInfo.UserName != member.UserName)
                                    {
                                        hashTableArchiveInfo.Add("UserName", memberArchiveInfo.UserName);
                                    }
                                    if (memberArchiveInfo.IsLDAP != member.IsLDAP)
                                    {
                                        hashTableArchiveInfo.Add("IsLDAP", memberArchiveInfo.IsLDAP);
                                    }
                                    //*********************END************************
                                    if (memberArchiveInfo.AlternateEmail != member.AlternateEmail)
                                    {
                                        hashTableArchiveInfo.Add("AlternateEmail", memberArchiveInfo.AlternateEmail);
                                    }
                                    if (memberArchiveInfo.PrimaryPhone != member.PrimaryPhone)
                                    {
                                        hashTableArchiveInfo.Add("HomePhone", memberArchiveInfo.PrimaryPhone + " Ext. " + memberArchiveInfo.PrimaryPhoneExtension);
                                    }
                                    if (memberArchiveInfo.CellPhone != member.CellPhone)
                                    {
                                        hashTableArchiveInfo.Add("CellPhone", memberArchiveInfo.CellPhone);
                                    }
                                }
                                if (memberDetailArchiveInfo != memberDetail)
                                {
                                    if (memberDetailArchiveInfo.CurrentAddressLine1 != memberDetail.CurrentAddressLine1)
                                    {
                                        hashTableArchiveInfo.Add("CurrentAddressLine1", memberDetailArchiveInfo.CurrentAddressLine1);
                                    }
                                    if (memberDetailArchiveInfo.CurrentAddressLine2 != memberDetail.CurrentAddressLine2)
                                    {
                                        hashTableArchiveInfo.Add("CurrentAddressLine2", memberDetailArchiveInfo.CurrentAddressLine2);
                                    }
                                    if (memberDetailArchiveInfo.CurrentCountryId != memberDetail.CurrentCountryId)
                                    {
                                        hashTableArchiveInfo.Add("CurrentCountryId", memberDetailArchiveInfo.CurrentCountryId);
                                    }
                                    if (memberDetailArchiveInfo.CurrentStateId != memberDetail.CurrentStateId)
                                    {
                                        hashTableArchiveInfo.Add("CurrentStateId", memberDetailArchiveInfo.CurrentStateId);
                                    }
                                    if (memberDetailArchiveInfo.CurrentCity != memberDetail.CurrentCity)
                                    {
                                        hashTableArchiveInfo.Add("CurrentCity", memberDetailArchiveInfo.CurrentCity);
                                    }
                                    if (memberDetailArchiveInfo.CurrentZip != memberDetail.CurrentZip)
                                    {
                                        hashTableArchiveInfo.Add("CurrentZip", memberDetailArchiveInfo.CurrentZip);
                                    }
                                    if (memberDetailArchiveInfo.OfficePhone != memberDetail.OfficePhone)
                                    {
                                        hashTableArchiveInfo.Add("OfficePhone", memberDetailArchiveInfo.OfficePhone + " Ext. " + memberDetailArchiveInfo.OfficePhoneExtension);
                                    }
                                }
                                MemberChangeArchive memberChangeArchive = new MemberChangeArchive();

                                if (hashTableArchiveInfo != null)
                                {
                                    memberChangeArchive.ChangedObject = ObjectEncrypter.Encrypt<Hashtable>(hashTableArchiveInfo);
                                    memberChangeArchive.MemberId = member.Id;
                                    memberChangeArchive.CreatorId = base.CurrentMember.Id;

                                    Facade.AddMemberChangeArchive(memberChangeArchive);
                                }
                            }
                        }
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }

            }
        }
        private TPS360.Common.BusinessEntities.MemberSourceHistory BuildMemberSourceHistory(int memberid, int source, string srcdesc)
        {
            TPS360.Common.BusinessEntities.MemberSourceHistory sourcehistory = new TPS360.Common.BusinessEntities.MemberSourceHistory();
            sourcehistory.MemberId = memberid;
            sourcehistory.IsRemoved = false;
            sourcehistory.UpdatorId = sourcehistory.CreatorId = base.CurrentMember != null ? base.CurrentMember.Id : 0;
            sourcehistory.CreateDate = sourcehistory.UpdateDate = DateTime.Now;
            sourcehistory.Source = source;

            sourcehistory.SourceDescription = srcdesc;

            sourcehistory.UserId = base.CurrentMember == null ? 0 : base.CurrentMember.Id;

            return sourcehistory;
        }
        private void FillEmployeeReferrerList()
        {

            ddlEmployeeReferrer.DataSource = Facade.GetAllEmployeeReferrerList();
            ddlEmployeeReferrer.DataValueField = "Id";
            ddlEmployeeReferrer.DataTextField = "Name";
            ddlEmployeeReferrer.DataBind();
            ddlEmployeeReferrer.Items.Insert(0, new ListItem("Select Employee", "0"));
        }
        private void FillVendorContacts(int CompanyId)
        {
           // if (ddlVendor.SelectedIndex > 0)
            {
                ddlVendorContact.DataSource = Facade.GetAllCompanyContactsByCompanyId(CompanyId);
                ddlVendorContact.DataTextField = "FirstName";
                ddlVendorContact.DataValueField = "MemberId";
                ddlVendorContact.DataBind();
                ddlVendorContact = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlVendorContact);
                //if (ddlVendorContact.Items.Count > 0)
                //    ddlVendorContact.Enabled = true;
                //else
                //    ddlVendorContact.Enabled = false;
            }
            //else
             //   ddlVendorContact.Enabled = false;

            ddlVendorContact.Items.Insert(0, new ListItem("Please Select", "0"));
        }
        private void FillVendorDetails()
        {
            ddlVendor.DataSource = Facade.GetAllClientsByStatus((int)CompanyStatus .Vendor );
            ddlVendor.DataTextField = "CompanyName";
            ddlVendor.DataValueField = "Id";
            ddlVendor.DataBind();
            FillVendorContacts(Convert.ToInt32(ddlVendor.SelectedValue==""?"0": ddlVendor .SelectedValue ));
        }
        private void PrepareView()
        {
           // if (Request.Url.ToString().ToLower().Contains("/candidateportal/")) divCandidateType.Visible = false;
            ddlSource.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.SourceType);
            ddlSource.DataValueField = "Id";
            ddlSource.DataTextField = "Name";
            ddlSource.DataBind();
            if (!IsUserAdmin)
            {
                ddlSource.Items.RemoveAt(2);
                ddlSource.Items.RemoveAt(2);
            }


            ddlSourceDescription.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.SourceDesciption);
            ddlSourceDescription.DataValueField = "Id";
            ddlSourceDescription.DataTextField = "Name";
            ddlSourceDescription.DataBind();
            MiscUtil.PopulateCandidateType(ddlApplicantType, Facade);
            ApplyDefaultsFromSiteSetting();
            FillVendorDetails();
            FillEmployeeReferrerList();
        }

        private void ApplyDefaultsFromSiteSetting()
        {
      
        }

        private void PrepareEditView()
        {
            Member member = CurrentMemberInfo;
            MemberDetail memberDetail = CurrentMemberDetails;
            txtPrimaryEmail.Attributes.Remove("OnChange");
            txtPrimaryEmail.Attributes.Add("OnChange", "CheckPrimaryEmailAvailability('" + txtPrimaryEmail.ClientID + "','" + member.Id + "','" + cvPrimaryEmail .ClientID +"');");
            //Code introduced by Prasanth on 10/Jun/2016 Start
            txtUserName.Attributes.Remove("OnChange");
            txtUserName.Attributes.Add("OnChange", "CheckADUserAvailability('" + txtUserName.ClientID + "','" + member.Id + "','" + cvUserName.ClientID + "');");
            //if (member.MemberCode.Substring(0, 3) == "MEM")
            if (MemberRole != "Candidate")
            {
                divLDAP.Visible = true;
            }
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("CandidatePortal/CandidateProfile.aspx"))
            {
                divLDAP.Visible = false;
            }
            //*****************END**************************
            txtFirstName.Text =MiscUtil .RemoveScript ( member.FirstName,string .Empty );
            txtMiddleName.Text =MiscUtil .RemoveScript (  member.MiddleName,string .Empty );
            txtNickName.Text = MiscUtil .RemoveScript ( member.NickName,string .Empty );
            txtLastName.Text = MiscUtil .RemoveScript ( member.LastName,string .Empty );

            txtPrimaryEmail.Text = MiscUtil .RemoveScript ( member.PrimaryEmail,string .Empty );
            //Code introduced by Prasanth on 10/Jun/2016 Start
            txtUserName.Text = MiscUtil.RemoveScript(member.UserName, string.Empty);
            ChkLDAP.Checked = member.IsLDAP;
            //*********************END*******************
            txtAlternateEmail.Text = MiscUtil .RemoveScript ( member.AlternateEmail,string .Empty );
            txtPrimaryPhone.Text = MiscUtil .RemoveScript ( member.PrimaryPhone,string .Empty );
            txtPrimaryPhoneExt.Text = MiscUtil .RemoveScript ( member.PrimaryPhoneExtension,string .Empty );
            txtCellPhone.Text = MiscUtil .RemoveScript ( member.CellPhone,string .Empty );
            txtAddress1.Text = MiscUtil .RemoveScript ( member.PermanentAddressLine1,string .Empty );
            txtAddress2.Text = MiscUtil .RemoveScript ( member.PermanentAddressLine2,string .Empty );
            txtCityB.Text = MiscUtil .RemoveScript ( member.PermanentCity,string .Empty );
            txtZipCode.Text = MiscUtil .RemoveScript ( member.PermanentZip,string .Empty );



           
            if (member.PermanentCountryId > 0) uclCountryState.SelectedCountryId = member.PermanentCountryId;
            else ApplyDefaultsFromSiteSetting();
            if (member.PermanentStateId > 0) uclCountryState.SelectedStateId = member.PermanentStateId;
            
            
            
            
            txtOfficePhone.Text = MiscUtil .RemoveScript ( memberDetail.OfficePhone,string .Empty );
            txtOfficePhoneEx.Text = MiscUtil.RemoveScript(memberDetail.OfficePhoneExtension, string.Empty);
            ddlApplicantType.SelectedValue = member.MemberType.ToString();


            MemberExtendedInformation exinfo = Facade.GetMemberExtendedInformationByMemberId(member.Id);
            ControlHelper.SelectListByValue(ddlSource, exinfo.SourceLookupId.ToString ());

            if (ddlSource.Items.Count == 9 && (exinfo.SourceLookupId == 1014 || exinfo.SourceLookupId == 1013 || exinfo.SourceLookupId == 1150))
            {
                ddlSource.Visible = false;
                divVendor.Visible = false;
                divEmpReferral.Visible = false;
                txtsrcdesc.Visible = false;
                lblSourceName.Text = Facade.GetGenericLookupById(exinfo.SourceLookupId).Name;
                if (exinfo.SourceLookupId == 1014)
                {
                    CompanyContact contact = Facade.GetCompanyContactByMemberId(Convert.ToInt32(exinfo.SourceDescription));
                    if (contact != null)
                    {

                        ControlHelper.SelectListByValue(ddlVendor, contact.CompanyId.ToString());
                        lblSourceDescriptionDetail.Text = ddlVendor.SelectedItem.Text + " - " + contact.FirstName + " " + contact.LastName;
                    }
                }
                else if (exinfo.SourceLookupId == 1013)
                {
                    ControlHelper.SelectListByValue(ddlEmployeeReferrer, exinfo.SourceDescription);
                    lblSourceDescriptionDetail.Text = ddlEmployeeReferrer.SelectedItem.Text;
                }
                else
                {
                    lblSourceDescriptionDetail.Text = exinfo.SourceDescription;
                }
            }
            else
            {

                if (ddlSource.SelectedItem.Text =="Job Portals")
                {
                    ControlHelper.SelectListByValue(ddlSourceDescription, exinfo.SourceDescription);
                }
                else if (ddlSource.SelectedItem.Text =="Employee Referral")
                {
                    ControlHelper.SelectListByValue(ddlEmployeeReferrer, exinfo.SourceDescription);
                }
                //else if (ddlSource.SelectedIndex == 2)
                //{
                //    //CompanyContact contact = Facade.GetCompanyContactByMemberId(Convert.ToInt32(exinfo.SourceDescription));
                //    //if (contact != null)
                //    //{
                //    //    ControlHelper.SelectListByValue(ddlVendor, contact.CompanyId.ToString());
                //    //    FillVendorContacts(contact.CompanyId);
                //    //    ControlHelper.SelectListByValue(ddlVendorContact, contact.MemberId.ToString());
                //    //    hdnSelectedVendorContact.Value = contact.MemberId.ToString();
                //    //}
                //    if (exinfo.SourceDescription.Contains("-"))
                //    {
                //        string[] SrcDesc = exinfo.SourceDescription.Split('-');
                //        ControlHelper.SelectListByValue(ddlVendor, SrcDesc[0].ToString());
                //        FillVendorContacts(Convert.ToInt32(SrcDesc[0]));
                //        ControlHelper.SelectListByValue(ddlVendorContact, SrcDesc[1].ToString());
                        
                //    }
                //    else
                //    {
                //        hdnSelectedVendorContact.Value = exinfo.SourceDescription.ToString();
                //    }
                //}
                else
                {
                    txtsrcdesc.Text = exinfo.SourceDescription;
                }
            }

        }
       
        private Member BuildMember()
        {
            Member member = CurrentMemberInfo;

            member.MemberCode = member.Id.ToString();

            member.FirstName = txtFirstName.Text=MiscUtil .RemoveScript (txtFirstName.Text.Trim());
            txtFirstName.Text = MiscUtil.RemoveScript(txtFirstName.Text.Trim(),string .Empty );
            member.MiddleName = txtMiddleName.Text=MiscUtil .RemoveScript (txtMiddleName.Text.Trim());
            txtMiddleName.Text = MiscUtil.RemoveScript(txtMiddleName.Text.Trim(), string.Empty);
            member.LastName = txtLastName.Text=MiscUtil .RemoveScript (txtLastName.Text.Trim());
            txtLastName.Text = MiscUtil.RemoveScript(txtLastName.Text.Trim(), string.Empty);
            member.NickName = txtNickName.Text=MiscUtil .RemoveScript(txtNickName.Text.Trim());
            txtNickName.Text = MiscUtil.RemoveScript(txtNickName.Text.Trim(), string.Empty);
            hfOldEmail.Value = string.Empty;
            if (string.Compare(member.PrimaryEmail, txtPrimaryEmail.Text.Trim()) != 0 )
            {
                MembershipUser NewUser;
                
                NewUser = Membership.CreateUser(txtPrimaryEmail.Text.Trim(), "changeme", txtPrimaryEmail.Text.Trim());
                if (NewUser.IsApproved)
                {
                    if(MemberRole !=string .Empty )
                    Roles.AddUserToRole(NewUser.UserName, MemberRole);
                    member.UserId = (Guid)NewUser.ProviderUserKey;
                }
                hfOldEmail.Value = member.PrimaryEmail;
            }
            member.PrimaryEmail = txtPrimaryEmail.Text=MiscUtil .RemoveScript (txtPrimaryEmail.Text.Trim());
            txtPrimaryEmail.Text = MiscUtil.RemoveScript(txtPrimaryEmail.Text.Trim(), string.Empty);
            member.AlternateEmail = txtAlternateEmail.Text=MiscUtil .RemoveScript (txtAlternateEmail.Text.Trim());
            txtAlternateEmail.Text = MiscUtil.RemoveScript(txtAlternateEmail.Text.Trim(), string.Empty);
            member.PrimaryPhone  = txtPrimaryPhone.Text=MiscUtil .RemoveScript (txtPrimaryPhone.Text.Trim());
            txtPrimaryPhone.Text = MiscUtil.RemoveScript(txtPrimaryPhone.Text.Trim(), string.Empty);
            member.PrimaryPhoneExtension = txtPrimaryPhoneExt.Text =MiscUtil .RemoveScript (txtPrimaryPhoneExt.Text.Trim());
            txtPrimaryPhoneExt.Text = MiscUtil.RemoveScript(txtPrimaryPhoneExt.Text.Trim(), string.Empty);
            member.CellPhone = txtCellPhone.Text=MiscUtil .RemoveScript (txtCellPhone.Text.Trim());
            txtCellPhone.Text = MiscUtil.RemoveScript(txtCellPhone.Text.Trim(), string.Empty);

            member.PermanentAddressLine1 = txtAddress1.Text=MiscUtil .RemoveScript (txtAddress1.Text.Trim());
            txtAddress1.Text = MiscUtil.RemoveScript(txtAddress1.Text.Trim(), string.Empty);
            member.PermanentAddressLine2 = txtAddress2.Text =MiscUtil .RemoveScript (txtAddress2.Text.Trim());
            txtAddress2.Text = MiscUtil.RemoveScript(txtAddress2.Text.Trim(), string.Empty);
            member.PermanentCity = txtCityB.Text=MiscUtil .RemoveScript (txtCityB.Text.Trim());
            txtCityB.Text = MiscUtil.RemoveScript(txtCityB.Text.Trim(), string.Empty);
            member.PermanentZip = txtZipCode.Text=MiscUtil .RemoveScript (txtZipCode.Text.Trim());
            txtZipCode.Text = MiscUtil.RemoveScript(txtZipCode.Text.Trim(), string.Empty);
          
            member.PermanentStateId =uclCountryState.SelectedStateId;
            member.PermanentCountryId =uclCountryState.SelectedCountryId;
            member.MemberType = Convert.ToInt32(ddlApplicantType.SelectedValue==""?"0":ddlApplicantType.SelectedValue);
            member.CreatorId = base.CurrentMember.Id;
            member.UpdatorId = base.CurrentMember.Id;
            member.IsLDAP = ChkLDAP.Checked;//Line introduced by Prasanth on 10/Jun/2016
            return member;
        }

        private MemberDetail BuildMemberDetail()
        {
            MemberDetail memberDetail = CurrentMemberDetails;
            memberDetail.OfficePhone =txtOfficePhone.Text= MiscUtil .RemoveScript (txtOfficePhone.Text.Trim());
            txtOfficePhone.Text = MiscUtil.RemoveScript(txtOfficePhone.Text.Trim(), string.Empty);
            memberDetail.OfficePhoneExtension =txtOfficePhoneEx.Text= MiscUtil .RemoveScript (txtOfficePhoneEx.Text.Trim());
            txtOfficePhoneEx.Text = MiscUtil.RemoveScript(txtOfficePhoneEx.Text.Trim(), string.Empty);
            memberDetail.HomePhone = txtPrimaryPhone.Text=MiscUtil .RemoveScript (txtPrimaryPhone.Text.Trim());
            txtPrimaryPhone.Text = MiscUtil.RemoveScript(txtPrimaryPhone.Text.Trim(), string.Empty); 
            if (_memberId > 0)memberDetail.MemberId = _memberId;
            else memberDetail.MemberId = base.CurrentMember.Id;
            memberDetail.CreatorId = base.CurrentMember.Id;
            memberDetail.UpdatorId = base.CurrentMember.Id;
            return memberDetail;
        }

        private void LoadWorkFlowSetting()
        {
            ApplicationWorkflow applicationWorkflow = Facade.GetApplicationWorkflowByTitle(WorkFlowTitle.ATSCandidateInformationArchived.ToString());
            if (applicationWorkflow != null && applicationWorkflow.RequireApproval && _memberrole == ContextConstants.ROLE_CANDIDATE)
             IsInformationArchived = true;
            else  IsInformationArchived = false;
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //Code added by pravin khot on 10/Feb/2016***********
            if (IsUserCandidate || (Page.Request.Url.AbsoluteUri.ToString().Contains("CandidatePortal/CandidateProfile.aspx")))
            {
                divCandidateSource.Visible = false;
                divLDAP.Visible = false;
            }
            //************************End******************************

            ddlVendor.Attributes.Add("onChange", "return Company_OnChange('" + ddlVendor.ClientID + "','" + ddlVendorContact.ClientID + "','" + hdntemp.ClientID + "','MemberID')");
            ddlVendorContact.Attributes.Add("OnChange", "return Contact_OnChange('" + ddlVendorContact.ClientID + "','" + hdntemp.ClientID + "')");
            string countryId;
            if (!IsPostBack)
            {
                PrepareView();
                Country country = Facade.GetCountryById(Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString()));
                if (country != null)
                {
                    if (country.Name == "India") 
                    { 
                        lblZip.Text = "PIN Code";
                        revZIPCode.ErrorMessage = "Please enter a valid PIN code"; 
                    }
                }
                PrepareEditView();
                LoadWorkFlowSetting();
            }
            if (IsPostBack)
            {
               // PrepareView();
               // PrepareEditView();
                uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
                
            }
            MemberManager manage= Facade.GetMemberManagerByMemberIdAndManagerId(CurrentMemberInfo.Id, CurrentMember.Id);
            if (!IsUserAdmin && manage == null) txtPrimaryEmail.ReadOnly = true; txtPrimaryEmail.Style["cursor"] = "not-allowed";  /////////added by Sumit Sonawane 11/Nov/2016/////////////////////////
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("/Employee/InternalResumeBuilder.aspx"))
            {
                //divCandidateType.Visible = false;
                divCandidateSource.Visible = false;
            }
            else
            {
                this.Page.Title = CurrentMemberInfo.FirstName + " " + CurrentMemberInfo.MiddleName + " " + CurrentMemberInfo.LastName + " - " + "Resume Builder";
            }
        }

        protected void btnSaveMember_Click(object sender, EventArgs e)
        {
            if (CurrentMemberInfo .PrimaryEmail.Trim().ToLower() != txtPrimaryEmail.Text.Trim().ToLower())
            {
                Member m = Facade.GetMemberByMemberEmail(txtPrimaryEmail.Text);
                if (m != null)
                {
                    cvPrimaryEmail.IsValid = false;
                    return;
                }
            }

            SaveMember();
            PrepareEditView();

            try
            {
                if (Page.Request.Url.AbsoluteUri.ToString().Contains("ATS/InternalResumeBuilder.aspx"))
                {
                    HyperLink lnkCandidateName = (HyperLink)this.Page.Master.FindControl("ctlCandidateOverviewHeader").FindControl("lnkCandidateName");
                    lnkCandidateName.Text =MiscUtil .RemoveScript ( txtFirstName.Text )+ " " +MiscUtil .RemoveScript ( txtLastName.Text);
                    Label lblPhone = (Label)this.Page.Master.FindControl("ctlCandidateOverviewHeader").FindControl("lblPhone");
                    if (lblPhone != null) lblPhone.Text = txtPrimaryPhone.Text;
                    Label lblMobile = (Label)this.Page.Master.FindControl("ctlCandidateOverviewHeader").FindControl("lblMobile");
                    if (lblMobile != null) lblMobile.Text = txtCellPhone.Text;
                    Label lblofficePhone = (Label)this.Page.Master.FindControl("ctlCandidateOverviewHeader").FindControl("lblofficePhone");
                    if (lblofficePhone != null) lblofficePhone.Text = txtOfficePhone.Text;
                    Label lblLocation = (Label)this.Page.Master.FindControl("ctlCandidateOverviewHeader").FindControl("lblLocation");
                    if (lblLocation != null) lblLocation.Text = MiscUtil.GetLocation(MiscUtil .RemoveScript ( txtCityB.Text), Facade .GetStateNameById (uclCountryState .SelectedStateId ), txtZipCode.Text);
                }
                else if (Page.Request.Url.AbsoluteUri.ToString().Contains("Employee/InternalResumeBuilder.aspx"))
                {

                    HyperLink  lblEmployeeName = (HyperLink )this.Page.Master.FindControl("ctlEmployeLP").FindControl("lblEmployeeName");
                    lblEmployeeName.Text =MiscUtil .RemoveScript ( txtFirstName.Text) + " " +MiscUtil .RemoveScript ( txtLastName.Text);

                    Label lblPhone = (Label)this.Page.Master.FindControl("ctlEmployeLP").FindControl("lblPhone");
                    if (lblPhone != null) lblPhone.Text = txtPrimaryPhone.Text;
                    if (txtPrimaryPhone .Text  != string.Empty && txtPrimaryPhoneExt .Text  != string.Empty) lblPhone.Text += " x" +txtPrimaryPhoneExt .Text ;
                       

                    Label lblMobile = (Label)this.Page.Master.FindControl("ctlEmployeLP").FindControl("lblMobile");
                    if (lblMobile != null) lblMobile.Text = txtCellPhone.Text;
                    Label lbloffice = (Label)this.Page.Master.FindControl("ctlEmployeLP").FindControl("lbloffice");
                    if (lbloffice != null) lbloffice.Text = txtOfficePhone.Text;
                    if (txtOfficePhoneEx.Text != string.Empty && txtOfficePhone.Text != string.Empty) lbloffice.Text += " x" + txtOfficePhoneEx.Text;


                    System.Web.UI.HtmlControls.HtmlGenericControl divEmployeeAddress = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Page.Master.FindControl("ctlEmployeLP").FindControl("divEmployeeAddress");
                    string empAdd = string.Empty;
                    string CountryName = uclCountryState.SelectedCountryName ;
                    empAdd +=MiscUtil .RemoveScript ( txtAddress1.Text )+ " " +MiscUtil .RemoveScript ( txtAddress2.Text);
                    empAdd += "<br/>";
                    empAdd += (!string.IsNullOrEmpty(txtCityB.Text) ?MiscUtil .RemoveScript ( txtCityB.Text) + ", " : string.Empty) +  Facade .GetStateNameById (uclCountryState.SelectedStateId);
                    empAdd += "<br/>";
                    empAdd += CountryName.Trim();
                    divEmployeeAddress.InnerHtml = empAdd;
                }
            }
            catch
            {
            }

            txtPrimaryEmail.Attributes.Remove("OnChange");
            txtPrimaryEmail.Attributes.Add("OnChange", "CheckPrimaryEmailAvailability('" + txtPrimaryEmail.ClientID + "','" +  CurrentMemberInfo .Id  + "','" + cvPrimaryEmail.ClientID + "');");
            //Code introduced by Prasanth on 10/Jun/2016 Start
            txtPrimaryEmail.Attributes.Remove("OnChange");
            txtPrimaryEmail.Attributes.Add("OnChange", "CheckADUserAvailability('" + txtUserName.ClientID + "','" + CurrentMemberInfo.Id + "','" + cvUserName.ClientID + "');");
            //****************END********************
            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;

          
            
        }
      
 
        #endregion
    }
}
