﻿/*-----------------------------------------------------------------------------
//Ver  Date         Name                Description
//-----------------------------------------------------------------------------
//0.1               Anand               Defect # 8667 - Uncommented code from 239 - 254
//                                            - Inserted break statement
//      27-09-2008                            - Commented Code from 239 - 254
// 0.2  29-09-2009  Anand               Defect# 8667 
//                                          - Added Method AddDefautlMenuAccess(319 to 227)
//                                          - Execute Method AddDefautlMenuAccess (256) 
//0.3  11-Feb-2009 Kalyani              Defect #9809 Added code in RegisterUser()
//       
   0.4  13-Jul-2009     Nagarathna V.B  Enhaancement:10717 opening the new window on submitt button click.  
 * 0.5  07-Aug-2009    Veda             Defect Id:11215 space before and after the mail id has been trimmed
 * 0.6  11-Aug-2009    Gopala Swamy J   Defect Id:11204 Changed the label text
   0.7  11-Aug-2009    Gopala Swamy J   Defect Id:11208 Passing one parameter
 * 0.8  13-May-2010    Sudarshan.R.     Defect Id:12794 changed the error message from user name to email id.
 * 0.9  11/Jan/2015    Prasanth Kumar G Enable Captcha for CandidatePortal
 * 1.0  19/Jan/2016    pravin khot      Using Job apply by career page
 * 1.1   10/Feb/2016   pravin khot       Using candidate registration by defoult source selection when candidate registration
 * 1.2   15/March/2016 pravin khot       new function added - SendEmailToCandidateAfterApply - Using Mail to Candidate once Resume is Uploaded 
 * 1.3   17/March/2016 pravin khot      Change url on click email template.
 * 1.4   27/April/2016 pravin khot      New code added - Using for DefaultSiteSetting.AllowVendorstoUpdateCandidateProfiles
 * 1.5   11/May/2016   pravin khot       New field added- NoticePeriod 
 * 1.6   18/May/2016   pravin khot       added automatic password create - password1 = user.ResetPassword();
 * 1.7   19/May/2016   pravin khot       added - btnCandidateLogin 
   1.8   10/Jun/2016   Prasanth Kumar G     introduced LDAP
 * 1.9   13/June/2016  pravin khot       added/modify code in condition of - if (Request.Url.ToString().ToLower().Contains("employee/registrationinfoeditor.aspx") 
   1.10  14/June/2016    Prasanth Kumar G introduced function GetMemberADUserNameById()
 * 2.1   11/July/2016  pravin khot       added- Contains("referral/employeereferral.aspx"))
   2.2     20Jan2017      Prasanth Kumar G issue id 1153
-----------------------------------------------------------------------------*/

using System;
using System.Collections;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;
using System.Collections.Specialized;
using TPS360.Common.Helper;
using TPS360.BusinessFacade;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace TPS360.Web.UI
{
    public partial class ControlRegistrationInternal : BaseControl
    {
        #region Veriables

        private string _role = "";
        private string _message = "";
        private int _memberId = 0;
        private bool _IsAccessForOverview = true;
        private string UrlForAccess = string.Empty;
        private int IdForSitemap = 0;
        SecureUrl redirectUrl = null;
        private bool _mailsetting = false;
        #endregion

        #region Properties
        private UserPageSetup _Setup
        {
            get { return Context.Items[typeof(UserPageSetup)] as UserPageSetup; }
            set { Context.Items[typeof(UserPageSetup)] = value; }
        }
        public int JobId
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]);
                }
                else return 0;

            }
        }
      
        public string Role
        {
            get
            {
                return _role;
            }
            set
            {
                _role = value;
            }
        }

        #endregion

        #region Methods

        private void CommonLoad()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }

            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MSG]))
            {
                _message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];
            }
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ROLE_TYPE]))
            {
                _role = Helper.Url.SecureUrl[UrlConstants.PARAM_ROLE_TYPE];
            }
        }

        private void clear()
        {

            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtUserEmail.Text = "";
            txtUserName.Text = ""; //Line added by Prasanth on 10/Jun/2016
            txtCopyPasteResume.Text = "";
            txtPassword.Text = "";
            txtConformPassword.Text = "";
            txtMobile.Text = "";
            txtPincode.Text = "";
            txtCity.Text = "";
            uclCountryState.SelectedCountryId = 0;
            uclCountryState.SelectedStateId = 0;
            txtEmployeePassword.Text = "";
            txtEmployeeConfirmPassword.Text = "";

            txtCurrentCompany.Text = "";
            ddlSource.SelectedIndex = 0;
            txtCurrentYearlyCTC.Text = "";
            txtExpectedYearlyCTC.Text = "";
            ddlExpectedYearlyCurrency.SelectedIndex = 0;
            ddlExpectedSalaryCycle.SelectedIndex = 0;
            ddlCurrentYearlyCurrency.SelectedIndex = 0;
            ddlCurrentSalaryCycle.SelectedIndex = 0;
            uclExperience.Clear();
            txtsrcdesc.Text = "";
            ddlSourceDescription.SelectedIndex = 0;

            ddlSourceDescription.Style.Add("display", "none");
            txtsrcdesc.Style.Add("display", "");

            ddlIDProof.SelectedIndex = 0;
            txtIDProof.Text = "";

            txtLinkedIn.Text = "";
            txtNoticePeriod.Text = ""; //added by pravin khot on 11/May/2016 
            ChkLDAP.Checked = false; //Line introduced by Prasanth on 10/Jun/2016
        }



        private void CheckMemberAvailability(string Email)
        {
            MembershipUser user = Membership.GetUser(Email);
            if (user != null)
            {
                Member mem = Facade.GetMemberByMemberEmail(Email);
                if (mem == null)
                {
                    Membership.DeleteUser(Email);
                }
            }
        }
        private void UpdateUserOwnership()
        {
            int source = 0;
            string sourcedesc = "";
            getSourceandSrcdesc(out source, out sourcedesc);
            Member newMember = Facade.GetMemberByMemberEmail(txtUserEmail.Text.Trim());
            if (_role.ToLower() == ContextConstants.ROLE_CANDIDATE.ToLower())
            {
                string countryName = string.Empty;
                if (SiteSetting != null)
                {
                    Country coun = Facade.GetCountryById(Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString()));
                    if (coun != null)
                        countryName = coun.Name;
                }

                ParseAndUpdate parse = new ParseAndUpdate(newMember.Id, "", "", "", "", "", "");
                //decimal months //= Int32.Parse(txtExpRequiredMonths.Text == string.Empty ? "0" : txtExpRequiredMonths.Text);

                //months = months / 12 * 10;
                //months = decimal.Round(months, 0);
                string months = uclExperience.Experience;
                if (rdoUploadResume.Checked)
                {

                    UploadDocument(newMember);

                    parse.GetParsedResume(newMember, "", "Upload", newMember.FirstName + newMember.LastName + " - " + "Resume" + Path.GetExtension(fuDocument.FileName), uclCountryState.SelectedCountryName.ToString(), txtMobile.Text, txtCity.Text, txtPincode.Text, uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(), txtCurrentCompany.Text, source, sourcedesc, months, txtCurrentYearlyCTC.Text, Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue), Convert.ToInt32(ddlCurrentSalaryCycle.SelectedValue), txtExpectedYearlyCTC.Text, Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue), Convert.ToInt32(ddlExpectedSalaryCycle.SelectedValue), Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text, MiscUtil.RemoveScript(txtLinkedIn.Text));
                }
                else if (rdoCopyPaste.Checked)
                {

                    SaveCopyPasteResume(newMember);
                    parse.GetParsedResume(newMember, txtCopyPasteResume.Text, "CopyPaste", "", countryName, txtMobile.Text, txtCity.Text, txtPincode.Text, uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(), txtCurrentCompany.Text, source, sourcedesc, months, txtCurrentYearlyCTC.Text, Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue), Convert.ToInt32(ddlCurrentSalaryCycle.SelectedValue), txtExpectedYearlyCTC.Text, Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue), Convert.ToInt32(ddlExpectedSalaryCycle.SelectedValue), Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text, MiscUtil.RemoveScript(txtLinkedIn.Text));
                }
                else if (rdoStepByStep.Checked)
                {
                    parse.GetParsedResume(newMember, "", "CopyPaste", "", countryName, txtMobile.Text, txtCity.Text, txtPincode.Text, uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(), txtCurrentCompany.Text, source, sourcedesc, months, txtCurrentYearlyCTC.Text, Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue), Convert.ToInt32(ddlCurrentSalaryCycle.SelectedValue), txtExpectedYearlyCTC.Text, Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue), Convert.ToInt32(ddlExpectedSalaryCycle.SelectedValue), Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text, MiscUtil.RemoveScript(txtLinkedIn.Text));

                }
                TPS360.Common.BusinessEntities.MemberSourceHistory history = BuildMemberSourceHistory(newMember.Id, source, sourcedesc);
                history.Source = newMember.ResumeSource;
                Facade.AddMemberSourceHistory(history);
                //Facade.DeleteAllMemberManagerByMemberId(newMember.Id);
                Facade.getMemberUpdateCreatorId(newMember.Id, (base.CurrentMember != null ? base.CurrentMember.Id : 0));
                MiscUtil.ShowMessage(lblMessage, "Candidate updated successfully.", false);
                clear();
            }
        }

        private TPS360.Common.BusinessEntities.MemberSourceHistory BuildMemberSourceHistory(int memberid, int source, string srcdesc)
        {
            TPS360.Common.BusinessEntities.MemberSourceHistory sourcehistory = new TPS360.Common.BusinessEntities.MemberSourceHistory();
            sourcehistory.MemberId = memberid;
            sourcehistory.IsRemoved = false;
            sourcehistory.UpdatorId = sourcehistory.CreatorId = base.CurrentMember != null ? base.CurrentMember.Id : 0;
            sourcehistory.CreateDate = sourcehistory.UpdateDate = DateTime.Now;
            sourcehistory.Source = source;

            sourcehistory.SourceDescription = srcdesc;

            sourcehistory.UserId = base.CurrentMember == null ? 0 : base.CurrentMember.Id;

            return sourcehistory;
        }
        private void getSourceandSrcdesc(out int source, out string sourcedesc)
        {
            sourcedesc = "";
            source = 0;
            if (IsUserVendor)
            {
                //source = 1014;
                source = 1150;
                sourcedesc = base.CurrentMember != null ? base.CurrentMember.Id.ToString() : "0";
                CompanyContact contact = Facade.GetCompanyContactByMemberId(Convert.ToInt32(sourcedesc));
                if (contact != null)
                {

                    ControlHelper.SelectListByValue(ddlVendor, contact.CompanyId.ToString());
                    sourcedesc = ddlVendor.SelectedItem.Text + " - " + contact.FirstName + " " + contact.LastName + "(" + contact.Email + ")";
                }
            }
            if (uclRefer.Visible)
            {
                source = 1013;
            }
            if (divSource.Visible)
            {
                source = Convert.ToInt32(ddlSource.SelectedValue);
                switch (ddlSource.SelectedIndex)
                {
                    case 7:
                        sourcedesc = ddlSourceDescription.SelectedValue;
                        break;
                    //case 1:
                    //    sourcedesc = txtsrcdesc.Text;
                    //    break;
                    //case 2:
                    //    sourcedesc = txtsrcdesc.Text;
                    //    break;
                    case 10:
                        sourcedesc = ddlEmployeeReferrer.SelectedValue;
                        break;
                    //case 4:
                    //    //sourcedesc = hdnSelectedVendorContact.Value;
                    //    sourcedesc = ddlVendor.SelectedValue +'-'+ hdntemp.Value;
                    //    break;
                    default:
                        sourcedesc = txtsrcdesc.Text;
                        break;
                }

            }

        }

        private void RegisterUser()
        {
            string strMessage = String.Empty;
            bool IsCompany = false;
            SecureUrl url;
            string SecureURL;

            // if (IsValid)
            {
                if (_role != "")
                {
                    try
                    {
                        MembershipUser newUser;
                        string password = "";
                        string password1 = ""; //added by pravin khot on 18/May/2016
                        string PrimaryEmail = "";//added by pravin khot on 18/May/2016
                        if (_role.ToLower() == ContextConstants.ROLE_CANDIDATE.ToLower())
                        {
                            password = divPassword.Visible ? txtPassword.Text : "changeme";                                            
                        }
                        if (_role.ToLower() == ContextConstants.ROLE_EMPLOYEE.ToLower())
                        {
                            password = ddlPassordOption.SelectedIndex == 0 ? txtEmployeePassword.Text : "Recruitment123";
                        }
                        if (Facade.CheckCandidateDuplication(txtFirstName.Text, txtLastName.Text, txtMobile.Text, DateTime.MinValue, txtUserEmail.Text, Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text))
                        {
                            throw new MembershipCreateUserException(MembershipCreateStatus.DuplicateUserName);
                        }
                        //newUser = Membership.CreateUser(txtUserEmail.Text.Trim(), password, txtUserEmail.Text.Trim()); Code commented by Prasanth on 10/Jun/2016

                        //Code introduced by Prasanth on 10/Jun/2016 Start

                        if (Request.Url.ToString().ToLower().Contains("employee/registrationinfoeditor.aspx"))
                        {
                            newUser = Membership.CreateUser(txtUserName.Text.Trim(), password, txtUserEmail.Text.Trim());
                        }
                        else
                        {
                            newUser = Membership.CreateUser(txtUserEmail.Text.Trim(), password, txtUserEmail.Text.Trim());
                        }
                        
                        //*********************END***********************
                       //*******************Code added by pravin khot on 16/May/2016******
                        if (Request.Url.ToString().ToLower().Contains("candidateregistration.aspx"))
                        {
                            PrimaryEmail = txtUserEmail.Text.Trim();
                            System.Web.Security.MembershipUser user = System.Web.Security.Membership.GetUser(PrimaryEmail);
                            try
                            {
                                password1 = user.ResetPassword();
                                user.ChangePassword(password, password1);
                                Membership.UpdateUser(user);
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        //***********************END*********************************                        


                           
                        if (newUser.IsApproved == true)
                        {

                            Roles.AddUserToRole(newUser.UserName, _role);

                            Member newMember = new Member();
                            MemberExtendedInformation memberExtendedInfo = new MemberExtendedInformation();

                            bool IsCreateMemberDetailandExtendedInfo = false;
                            bool IsCreateMemberManager = false;

                            newMember.UserId = (Guid)newUser.ProviderUserKey;
                            newMember.ResumeSource = Convert.ToInt32(ResumeSource.SelfRegistration);
                            if (IsUserVendor)
                            {
                                newMember.ResumeSource = Convert.ToInt32(ResumeSource.Vendor);
                            }
                            newMember.PermanentCountryId = 0;
                            newMember.PermanentStateId = 0;
                            newMember.IsRemoved = false;
                            newMember.AutomatedEmailStatus = true;
                            newMember.UserName = txtUserName.Text.Trim(); //Line intorduced by Prasanth on 10/Jun/2016
                            newMember.FirstName = txtFirstName.Text.Trim();
                            newMember.LastName = txtLastName.Text.Trim();
                            newMember.DateOfBirth = DateTime.MinValue;
                            newMember.PrimaryEmail = newUser.Email;
                            newMember.Status = (int)MemberStatus.Active;

                            newMember.PermanentCity = txtCity.Text;
                            newMember.PermanentStateId = uclCountryState.SelectedStateId;
                            newMember.PermanentCountryId = uclCountryState.SelectedCountryId;
                            newMember.PermanentZip = txtPincode.Text;
                            newMember.IsLDAP = ChkLDAP.Checked; //Line introduced by Prasanth on 10/Jun/2016
                            if (Request.Url.ToString().ToLower().Contains("candidateregistration.aspx"))
                            {
                                newMember.ResumeSource = (int)ResumeSource.SelfRegistration;
                            }
                            else if (uclRefer.Visible)
                            {
                                newMember.ResumeSource = (int)ResumeSource.ReferralProgram;
                            }
                            else
                            {

                                if (base.CurrentUserRole == ContextConstants.ROLE_EMPLOYEE)
                                {
                                    newMember.ResumeSource = (int)ResumeSource.Employee;
                                }
                                else if (base.CurrentUserRole == ContextConstants.ROLE_ADMIN)
                                {
                                    newMember.ResumeSource = (int)ResumeSource.Admin;
                                }
                                newMember.CreatorId = base.CurrentMember != null ? base.CurrentMember.Id : 0;

                                newMember.UpdatorId = base.CurrentMember != null ? base.CurrentMember.Id : 0;

                                //If not a candidate registratiion
                                if (_role.ToLower() != ContextConstants.ROLE_CANDIDATE.ToLower())
                                {
                                    IsCreateMemberDetailandExtendedInfo = true;
                                }

                                //Add to self as manager                            
                                if (base.CurrentUserRole == ContextConstants.ROLE_EMPLOYEE || base.CurrentUserRole == ContextConstants.ROLE_ADMIN)
                                {

                                    IsCreateMemberManager = true;
                                }
                            }

                            newMember = Facade.AddFullMemberInfo(newMember, IsCreateMemberDetailandExtendedInfo, ContextConstants.MEMBER_DEFAULT_AVAILABILITY, IsCreateMemberManager);
                            if (newMember.Id == 0)
                            {
                                //Membership.DeleteUser(txtUserEmail.Text.Trim(), true); Code introduced by Prasanth on 10/Jun/2016
                                //Code introduced by Prasanth on 10/Jun/2016 Start
                                if (Request.Url.ToString().ToLower().Contains("employee/registrationinfoeditor.aspx"))
                                {
                                    Membership.DeleteUser(txtUserName.Text.Trim(), true);
                                }
                                else
                                {
                                    Membership.DeleteUser(txtUserEmail.Text.Trim(), true);
                                }
                                //******************END**************************
                                MiscUtil.ShowMessage(lblMessage, _role + " Creation was not successful. Please try again.", true);
                                return;
                            }

                            // MiscUtil.AddActivity(_role, _memberId, base.CurrentMember.Id, ActivityType.AddMember, Facade);
                            string countryName = string.Empty;
                            if (SiteSetting != null)
                            {
                                Country coun = Facade.GetCountryById(Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString()));
                                if (coun != null)
                                    countryName = coun.Name;
                            }
                            int source = 0;
                            string sourcedesc = "";
                            if (_role.ToLower() == ContextConstants.ROLE_CANDIDATE.ToLower())
                            {
                                //****code added by pravin khot on 10/Feb/2016 using candidate registration by defoult source selection************
                                if (Request.Url.ToString().ToLower().Contains("candidateregistration.aspx"))
                                {
                                    source = 1149;                                    
                                    sourcedesc = "Submitted on date : " + newMember.CreateDate; 
                                }
                                //*****************END*****************************
                                else
                                {
                                    getSourceandSrcdesc(out source, out sourcedesc);                                
                                    if (uclRefer.Visible)
                                    {
                                        redirectUrl = null;
                                        uclRefer.CandidateName = newMember.FirstName + " " + newMember.LastName;
                                        sourcedesc = uclRefer.AddEmployeeReferalDetails(newMember.Id).ToString();
                                        newMember.Status = 0;
                                    }
                                }
                                // decimal months = Int32.Parse(txtExpRequiredMonths.Text==string .Empty ? "0": txtExpRequiredMonths .Text );

                                //months = months / 12 * 10;
                                //months = decimal.Round(months, 0);
                                string months = uclExperience.Experience;
                                ParseAndUpdate parse = new ParseAndUpdate(newMember.Id, "", "", "", "", "", "");
                                if (rdoUploadResume.Checked)
                                {
                                    UploadDocument(newMember);
                                    parse.GetParsedResume(newMember, "", "Upload", newMember.FirstName + newMember.LastName + " - " + "Resume" + Path.GetExtension(fuDocument.FileName), uclCountryState.SelectedCountryName.ToString(), txtMobile.Text, txtCity.Text, txtPincode.Text, uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(), txtCurrentCompany.Text, source, sourcedesc, months, txtCurrentYearlyCTC.Text, Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue), Convert.ToInt32(ddlCurrentSalaryCycle.SelectedValue), txtExpectedYearlyCTC.Text, Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue), Convert.ToInt32(ddlExpectedSalaryCycle.SelectedValue), Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text, MiscUtil.RemoveScript(txtLinkedIn.Text));
                                }
                                else if (rdoCopyPaste.Checked)
                                {

                                    SaveCopyPasteResume(newMember);
                                    parse.GetParsedResume(newMember, txtCopyPasteResume.Text, "CopyPaste", "", countryName, txtMobile.Text, txtCity.Text, txtPincode.Text, uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(), txtCurrentCompany.Text, source, sourcedesc, months, txtCurrentYearlyCTC.Text, Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue), Convert.ToInt32(ddlCurrentSalaryCycle.SelectedValue), txtExpectedYearlyCTC.Text, Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue), Convert.ToInt32(ddlExpectedSalaryCycle.SelectedValue), Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text, MiscUtil.RemoveScript(txtLinkedIn.Text));
                                }
                                else if (rdoStepByStep.Checked)
                                {
                                    parse.GetParsedResume(newMember, "", "CopyPaste", "", countryName, txtMobile.Text, txtCity.Text, txtPincode.Text, uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(), txtCurrentCompany.Text, source, sourcedesc, months, txtCurrentYearlyCTC.Text, Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue), Convert.ToInt32(ddlCurrentSalaryCycle.SelectedValue), txtExpectedYearlyCTC.Text, Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue), Convert.ToInt32(ddlExpectedSalaryCycle.SelectedValue), Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text, MiscUtil.RemoveScript(txtLinkedIn.Text));
                                    //parse.GetParsedResume(newMember, "", "CopyPaste", "", countryName, txtMobile.Text, txtCity.Text, txtPincode.Text, uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(), txtCurrentCompany.Text, source, sourcedesc, months, Convert.ToDecimal(txtCurrentYearlyCTC.Text), Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue), Convert.ToInt32(ddlCurrentSalaryCycle.SelectedValue), txtExpectedYearlyCTC.Text, Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue), Convert.ToInt32(ddlExpectedSalaryCycle.SelectedValue), Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text, MiscUtil.RemoveScript(txtLinkedIn.Text));

                                }

                                //*****************Code added by pravin khot on 11/May/2016 USING FOR ADD NoticePeriod********************* 
                                memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(newMember.Id);
                                memberExtendedInfo.NoticePeriod = MiscUtil.RemoveScript(txtNoticePeriod.Text.Trim(), string.Empty);
                                Facade.UpdateMemberExtendedInformation(memberExtendedInfo);
                                //********************************END*********************************
                            }


                            if (_role.ToLower() == ContextConstants.ROLE_CANDIDATE.ToLower())
                            {
                                if (!Request.Url.ToString().ToLower().Contains("candidateregistration.aspx"))
                                {
                                    if (!uclRefer.Visible)
                                    {
                                        newUser.IsApproved = false;
                                        Membership.UpdateUser(newUser);
                                        SitemapPermission(358);
                                        if (_IsAccessForOverview)
                                        {
                                            ArrayList AList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                                            if (AList.Contains(362) || rdoStepByStep.Checked)
                                            {
                                                redirectUrl = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Candidate.CANDIDATE_INTERNALRESUMEBUILDER.Substring(2), "", UrlConstants.PARAM_MEMBER_ID, newMember.Id.ToString(), UrlConstants.PARAM_MSG, "Candidate has been created successfully.", UrlConstants.PARAM_RESUMEBUILD_OPTION, ((int)ResumeBuilderOption.StepByStep).ToString(), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.CANDIDATE_RESUMEBUILDER_SITEMAP, UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.ATS_OVERVIEW_SITEMAP_PARENTID);
                                                // Page.ClientScript.RegisterStartupScript(typeof(Page), "OpenCand", "<script>var w=window.open('" + url + "');</script>");
                                            }
                                            else
                                            {
                                                redirectUrl = UrlHelper.BuildSecureUrl(UrlForAccess.Substring(6), "", UrlConstants.PARAM_MEMBER_ID, newMember.Id.ToString(), UrlConstants.PARAM_MSG, "Candidate has been created successfully.", UrlConstants.PARAM_RESUMEBUILD_OPTION, ((int)ResumeBuilderOption.StepByStep).ToString(), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.ATS_OVERVIEW_SITEMAP_PARENTID);
                                                // Page.ClientScript.RegisterStartupScript(typeof(Page), "OpenCand", "<script>var w=window.open('" + url + "');</script>");
                                            }
                                        }
                                        else
                                        {
                                            if (IsUserVendor)
                                            {
                                                if (rdoStepByStep.Checked)
                                                    redirectUrl = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Vendor.CANDIDATE_INTERNALRESUMEBUILDER.Substring(2), string.Empty, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(newMember.Id), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.VENDOR_CANDIDATE_RESUMEBUILDER_SITEMAP_ID, UrlConstants.PARAM_SITEMAP_PARENT_ID, "653");
                                                else
                                                    redirectUrl = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Vendor.OVERVIEW.Substring(2), string.Empty, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(newMember.Id), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.VENDOR_CANDIDATE_OVERVIEW_SITEMAP_ID, UrlConstants.PARAM_SITEMAP_PARENT_ID, "653");
                                                //Page.ClientScript.RegisterStartupScript(typeof(Page), "OpenCand", "<script>var w=window.open('" + url + "');</script>");
                                            }
                                        }
                                    }
                                }
                                else
                                {

                                    System.Web.Security.FormsAuthentication.SetAuthCookie(newMember.PrimaryEmail, true);
                                    redirectUrl = UrlHelper.BuildSecureUrl(UrlConstants.CandidatePortal.HOME, string.Empty, UrlConstants.PARAM_MEMBER_ID, newMember.Id.ToString());
                                }


                                TPS360.Common.BusinessEntities.MemberSourceHistory history = BuildMemberSourceHistory(newMember.Id, source, sourcedesc);
                                Facade.AddMemberSourceHistory(history);

                                //**************code added by pravin khot on 19/Jan/2016 Using Job apply by career page********************
                                if (JobId > 0)
                                {     
                                    
                                        JobPosting JobPostingCareerJob = new JobPosting();
                                        //JobPostingCareerJob = Facade.GetJobPostingById(JobId);
                                        //JobPostingCareerJob.CareerJobPostingCode = Convert.ToInt32(JobPostingCareerJob.JobPostingCode);
                                        //JobPostingCareerJob.CareerJobPostingCode = Convert.ToInt32(JobId);
                                        JobPostingCareerJob.CareerJobId = JobId;
                                        JobPostingCareerJob.MemberId = newMember.Id;
                                        JobPostingCareerJob.IsApplied = false;
                                        Facade.CareerJobId_Add(JobPostingCareerJob);                                                                   
                                }                               
                                //*******************************End**********************************

                                if (Request.Url.ToString().ToLower().Contains("employeereferral.aspx"))
                                {
                                    MiscUtil.ShowMessage(lblMessage, "Successfully Submitted Candidate.<br/>You will be able to re-submit this candidate after 6 months", false);
                                }
                                else
                                {
                                    if (JobId > 0)
                                    {                                        
                                        MiscUtil.AddStatusMessageToSession(Session, "Candidate has been apply job and created successfully.");
                                        //*************Code modify by pravin khot on 18/May/2016***********
                                        //SendEmailToCandidateAfterApply(JobId, password, newMember); //code line added by pravin khot on 15/March/2016 using Mail to Candidate once Resume is Uploaded .                                       
                                        SendEmailToCandidateAfterApply(JobId, password1, newMember);//NEW CODE
                                        //*********************************END********************************
                                    }
                                    else
                                    {
                                        MiscUtil.AddStatusMessageToSession(Session, "Candidate has been created successfully.");
                                        //MiscUtil.ShowMessage(lblMessage, "Candidate has been created successfully.", false);
                                    }
                                }
                                clear();
                            }
                            else if (_role.ToLower() == ContextConstants.ROLE_EMPLOYEE.ToLower())
                            {
                                MemberCustomRoleMap memberRolemap = new MemberCustomRoleMap();
                                IList<CustomRole> roleList = Facade.GetAllCustomRole();
                                int _intcustrole = 0;
                                foreach (CustomRole cr in roleList)
                                {
                                    if (cr.Name == "Employee")
                                    {
                                        _intcustrole = cr.Id;
                                        break;
                                    }
                                }
                                // AddDefaultMenuAccess(_intcustrole, newMember.Id);
                                AssignRole(newMember.Id);
                                LoadUserPageSetup(newMember.Id);
                                SitemapPermission(360);
                                if (_IsAccessForOverview)
                                {
                                    ArrayList AList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                                    if (AList.Contains(380))
                                    {
                                        redirectUrl = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Employee.EMPLOYEE_INTERNALACCESS.Substring(2), "", UrlConstants.PARAM_MEMBER_ID, newMember.Id.ToString(), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Employee.ACCESS_SITEMAP_ID);
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "open", "<script language='javaScript'>window.open('" + url + "');</script>", false);
                                        //redirectUrl = url;
                                    }
                                    else
                                    {
                                        redirectUrl = UrlHelper.BuildSecureUrl(UrlForAccess.Substring(11), "", UrlConstants.PARAM_MEMBER_ID, newMember.Id.ToString(), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString());
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "open", "<script language='javaScript'>window.open('" + url + "');</script>", false);
                                        // redirectUrl = url;
                                    }
                                }
                                if (ddlPassordOption.SelectedIndex == 0)
                                {

                                }
                                else SendEmail(newMember);

                                //MiscUtil.ShowMessage(lblMessage, "User created successfully.", false);
                                MiscUtil.AddStatusMessageToSession(Session, "User created successfully.");

                                ddlRole.SelectedIndex = 0;

                                _memberId = 0;

                                clear();
                            }
                        }
                        if (redirectUrl != null)
                        {
                            Helper.Url.Redirect(redirectUrl.ToString());
                        }
                    }
                    catch (MembershipCreateUserException ex)
                    {
                        switch (ex.StatusCode)
                        {
                            case MembershipCreateStatus.DuplicateUserName:
                                {
                                    MiscUtil.ShowMessage(lblMessage, "User already exists.", true);
                                    //txtUserEmail.Focus(); Code commented by Prasanth on 10/Jun/2016
                                    txtUserName.Focus(); //Code introduced by Prasanth on 10/Jun/2016
                                    break;
                                }
                            case MembershipCreateStatus.InvalidUserName:
                                {
                                    MiscUtil.ShowMessage(lblMessage, "Invalid user name.", true);
                                    //txtUserEmail.Focus(); Code commented by Prasanth on 10/Jun/2016
                                    txtUserName.Focus(); //Code introduced by Prasanth on 10/Jun/2016
                                    break;
                                }
                            case MembershipCreateStatus.InvalidPassword:
                                {
                                    MiscUtil.ShowMessage(lblMessage, "Invalid password. Please enter matching passwords.", true);
                                    //txtUserEmail.Focus(); Code commented by Prasanth on 10/Jun/2016
                                    txtUserName.Focus(); //Code introduced by Prasanth on 10/Jun/2016
                                    break;
                                }
                        }
                    }
                    //catch (Exception exp)
                    //{
                    //    DeleteMembershipUser();
                    //    MiscUtil.ShowMessage(lblMessage,exp.Message,true);
                    //}

                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "No role specified.", true);
                }
            }
        }
        private MemberEmail BuildMemberEmail(Member newmember, string subject, string body, string strFrom)
        {
            MemberEmail memberEmail = new MemberEmail();
            memberEmail.SenderId = 0;
            memberEmail.SenderEmail = strFrom;
            memberEmail.ReceiverId = newmember.Id;
            memberEmail.ReceiverEmail = newmember.PrimaryEmail;
            memberEmail.Subject = subject;
            memberEmail.EmailBody = body;
            memberEmail.Status = 0;

            memberEmail.IsRemoved = false;
            memberEmail.CreatorId = base.CurrentMember != null ? base.CurrentMember.Id : 0;
            memberEmail.UpdatorId = base.CurrentMember != null ? base.CurrentMember.Id : 0;
            memberEmail.SentDate = DateTime.Now.ToString();
            memberEmail.EmailTypeLookupId = (int)EmailType.Sent;
            return memberEmail;
        }
        private void SaveMemberEmail(Member newmember, string subject, string body, string From)
        {
            if (true) // Isvalid
            {
                try
                {
                    MemberEmail memberEmail = BuildMemberEmail(newmember, subject, body, From);
                    memberEmail = Facade.AddMemberEmail(memberEmail);
                    //SaveMemberEmailDetail(memberEmail);
                }
                catch (ArgumentException ex)
                {

                }

            }
        }

        private void SendEmail(Member NewMember)
        {
            string sentStatus = string.Empty;
            EmailHelper emailManager = new EmailHelper();
            string subject = "New Password For Talentrackr Access";
            //emailManager.Subject = subject;
            //    emailManager.To.Clear();
            //    emailManager.From = CurrentMember.PrimaryEmail;
            //    emailManager.To.Add(NewMember.PrimaryEmail );
            string body = emailManager.PrepareViewforTps360Access(NewMember);


            System.Web.Security.MembershipUser user = System.Web.Security.Membership.GetUser(NewMember.PrimaryEmail);
            try
            {
                body = body.Replace("**********", user.ResetPassword());
            }
            catch (Exception ex)
            {
            }

            MailQueueData.AddMailToMailQueue(CurrentMember.Id, NewMember.PrimaryEmail.ToString(), subject, body, "", "", null, Facade);
            sentStatus = "Sent";
            //emailManager.Body = body;
            //sentStatus = emailManager.Send(base.CurrentMember != null ? base.CurrentMember.Id : 0);

            //SaveMemberEmail(NewMember ,subject   ,body ,emailManager.From  );


        }

        void DeleteMembershipUser()
        {
            //Membership.DeleteUser(txtUserEmail.Text.Trim(), true); Code Commented by Prasanth on 10/Jun/2016
            Membership.DeleteUser(txtUserName.Text.Trim(), true); //Line introduced by Prasanth on 10/Jun/2016

            //Member member = Facade.GetMemberByMemberEmail(txtUserEmail.Text.Trim());
            //MembershipUser membershipUser = Membership.GetUser(member.UserId);

            //if (Facade.DeleteMemberById(member.Id))
            //{
            //    Roles.RemoveUserFromRole(membershipUser.UserName,_role);// ContextConstants.ROLE_CANDIDATE);
            //    Membership.DeleteUser(membershipUser.UserName, true);
            //}
        }

        private void LoadUserPageSetup(int NewMemberId)
        {
            UserPageDataAccess upda = new UserPageDataAccess();
            int pageCount = upda.GetUserPageCount(NewMemberId);

            if (pageCount == 0)
            {
                //new user. Create page for him.

                UserPage page = new UserPage();
                page.Title = "Dashboard";
                page.UserId = NewMemberId;
                upda.AddUserPage(page);

                //set the current page
                UserPageSetting userPageSetting = new UserPageSetting();
                userPageSetting.CurrentPageId = page.Id;
                userPageSetting.UserId = NewMemberId;
                new UserPageSettingDataAccess().AddUserPageSetting(userPageSetting);

                List<Widget> defaultWidgetList = new WidgetDataAccess().GetAllWidget(true);

                if (defaultWidgetList != null && defaultWidgetList.Count > 0)
                {
                    _Setup = new UserPageSetup(NewMemberId);
                    //add widget to instance in a page
                    foreach (Widget widget in defaultWidgetList)
                    {
                        WidgetInstance wi = new WidgetInstance();
                        wi.Title = widget.Name;
                        wi.PageId = _Setup.CurrentPage.Id;
                        wi.State = string.Empty;
                        wi.WidgetId = widget.Id;
                        wi.Expanded = true;
                        wi.State = widget.DefaultState;

                        WidgetInstanceDataAccess wida = new WidgetInstanceDataAccess();
                        wida.AddWidgetInstance(wi);
                        wida.UpdateWidgetInstancePosition(_Setup.CurrentPage.Id);
                    }
                }
            }

            _Setup = Cache[base.CurrentMember.Id.ToString()] as UserPageSetup;
            if (null == _Setup)
                _Setup = new UserPageSetup(base.CurrentMember.Id);
        }
        private void UploadDocument(Member newMember)
        {
            if (fuDocument.HasFile)
            {
                Member member = Facade.GetMemberById(newMember.Id);
                string strMessage = String.Empty;
                string _fileName = string.Empty;
                lblMessage.Text = string.Empty;
                bool boolError = false;
                string UploadedFilename = Convert.ToString(fuDocument.FileName);
                string[] FileName_Split = UploadedFilename.Split('.');
                string ResumeName = member.FirstName + member.LastName + " - Resume." + FileName_Split[FileName_Split.Length - 1]; 
                UploadedFilename = ResumeName;
                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, newMember.Id, UploadedFilename, "Word Resume", false);//1.6
                if (CheckFileSize())
                {
                    fuDocument.SaveAs(strFilePath);
                    if (File.Exists(strFilePath))
                    {

                        MemberDocument memberDocument = Facade.GetMemberDocumentByMemberIdAndFileName(newMember.Id, UploadedFilename);//1.6
                        if (memberDocument == null)
                        {
                            MemberDocument newDoc = new MemberDocument();
                            newDoc.FileName = UploadedFilename;
                            newDoc.MemberId = newMember.Id;
                            newDoc.FileTypeLookupId = 55;
                            newDoc.Title = "Primary Resume";
                            Facade.AddMemberDocument(newDoc);
                            MiscUtil.AddActivity(_role, newMember.Id, 0, ActivityType.UploadDocument, Facade);
                            if (string.IsNullOrEmpty(strMessage))
                            {
                                strMessage = "Successfully uploaded the file";
                            }
                        }
                        else
                        {
                            memberDocument.FileName = UploadedFilename;
                            memberDocument.MemberId = newMember.Id;
                            Facade.UpdateMemberDocument(memberDocument);
                            MiscUtil.AddActivity(_role, newMember.Id, base.CurrentMember != null ? base.CurrentMember.Id : 0, ActivityType.UploadDocument, Facade);
                            strMessage = "Successfully updated the file";

                        }
                    }
                }
                else
                {
                    boolError = true;
                    strMessage = "File size should be less than 10 MB";
                }
                MiscUtil.ShowMessage(lblMessage, strMessage + lblMessage.Text, boolError);
            }

        }

        void SaveCopyPasteResume(Member newmember)
        {
            string strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Resources/");
            string strFullfilePath = strFilePath + "Copy Paste Resume" + "\\" + newmember.Id.ToString();
            string filenameandfullpath = strFullfilePath + "\\" + newmember.FirstName + newmember.LastName + ".txt";
            if (!Directory.Exists(strFullfilePath))
            {
                Directory.CreateDirectory(strFullfilePath);
            }

            using (StreamWriter writer = new StreamWriter(filenameandfullpath, true))
            {
                writer.Write(txtCopyPasteResume.Text);
            }
        }

        private bool CheckFileSize()
        {
            int fileSize = Convert.ToInt32(fuDocument.FileContent.Length / (1024 * 1024));
            if (fileSize > ContextConstants.MAXIMUM_UPLOAD_SIZE)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private void AddDefaultMenuAccess(int roleId, int memberId)
        {

            ArrayList previlegeList = Facade.GetAllCustomRolePrivilegeIdsByRoleId(roleId);

            if (previlegeList != null && previlegeList.Count > 0)
            {
                Facade.DeleteMemberPrivilegeByMemberId(memberId);

                for (int i = 0; i <= previlegeList.Count - 1; i++)
                {
                    MemberPrivilege previlege = new MemberPrivilege();

                    previlege.CustomSiteMapId = Convert.ToInt32(previlegeList[i]);
                    previlege.MemberId = memberId;
                    Facade.AddMemberPrivilege(previlege);
                }
            }
        }
        private void UpdateUser()
        {
            Member member = Facade.GetMemberById(_memberId);
            MembershipUser user = Membership.GetUser(member.UserId);
            user.ChangePassword(user.GetPassword(), "");

            Membership.UpdateUser(user);
        }
        private void FillEmployeeReferrerList()
        {

            ddlEmployeeReferrer.DataSource = Facade.GetAllEmployeeReferrerList();
            ddlEmployeeReferrer.DataValueField = "Id";
            ddlEmployeeReferrer.DataTextField = "Name";
            ddlEmployeeReferrer.DataBind();
            ddlEmployeeReferrer.Items.Insert(0, new ListItem("Select Employee", "0"));
        }
        private void FillVendorContacts(int CompanyId)
        {
            // if (ddlVendor.SelectedIndex > 0)
            {
                ddlVendorContact.DataSource = Facade.GetAllCompanyContactsByCompanyId(CompanyId);
                ddlVendorContact.DataTextField = "FirstName";
                ddlVendorContact.DataValueField = "MemberId";
                ddlVendorContact.DataBind();
                ddlVendorContact = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlVendorContact);
                if (ddlVendorContact.Items.Count > 0)
                    ddlVendorContact.Enabled = true;
                else
                    ddlVendorContact.Enabled = false;
            }
            //else
            //   ddlVendorContact.Enabled = false;

            ddlVendorContact.Items.Insert(0, new ListItem("Please Select", "0"));
        }
        private void FillVendorDetails()
        {
            ddlVendor.DataSource = Facade.GetAllClientsByStatus((int)CompanyStatus.Vendor);
            ddlVendor.DataTextField = "CompanyName";
            ddlVendor.DataValueField = "Id";
            ddlVendor.DataBind();
            if (ddlVendor.Items.Count == 0) ddlVendorContact.Enabled = ddlVendor.Enabled = false;
            else FillVendorContacts(Convert.ToInt32(ddlVendor.SelectedValue));
        }
        private void PrepareView()
        {
            //******************Code added by pravin khot on 21/Jan/2016 by defoult selected item in the source**************
            if (JobId > 0)
            { 
                ddlSource.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.SourceType).Where(x => x.Id == 1149).ToList();
                ddlSource.DataValueField = "Id";
                ddlSource.DataTextField = "Name";
                ddlSource.DataBind();            
                ddlSource.Enabled = false;

                txtsrcdesc.Text = ddlSource.SelectedItem.Text ;
                txtsrcdesc.Enabled = false;              
            }
             //*********************************************End***********************************************************
            else
            {
                ddlSource.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.SourceType).OrderBy(x => x.SortOrder).ToList();
                ddlSource.DataValueField = "Id";
                ddlSource.DataTextField = "Name";
                ddlSource.DataBind();
                ddlSource.Enabled = true;
                if (!IsUserAdmin)
                {
                    ddlSource.Items.RemoveAt(3);
                    ddlSource.Items.RemoveAt(3);
                }
              
            }

            ddlSourceDescription.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.SourceDesciption).OrderBy(x => x.SortOrder).ToList();
            ddlSourceDescription.DataValueField = "Id";
            ddlSourceDescription.DataTextField = "Name";
            ddlSourceDescription.DataBind();

            ddlIDProof.DataSource = Facade.GetAllGenericLookupByLookupType(LookupType.IDProof).OrderBy(x => x.SortOrder).ToList();
            ddlIDProof.DataValueField = "Id";
            ddlIDProof.DataTextField = "Name";
            ddlIDProof.DataBind();

            MiscUtil.PopulateCurrency(ddlCurrentYearlyCurrency, Facade);
            MiscUtil.PopulateCurrency(ddlExpectedYearlyCurrency, Facade);
            GetDefaultsFromSiteSetting();
            FillVendorDetails();
            FillEmployeeReferrerList();
        }

        private void PrepareEditView()
        {

            Member member = Facade.GetMemberById(_memberId);
            MembershipUser user = Membership.GetUser(member.UserId);
            txtUserEmail.Text = user.UserName;
        }


        private void GetDefaultsFromSiteSetting()
        {
            if (SiteSetting != null)
            {
                ddlCurrentYearlyCurrency.SelectedValue = SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString();
                ddlExpectedYearlyCurrency.SelectedValue = SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString();
            }
        }

        private void DisableValidationControl()
        {
            rfvmobile.Enabled = false;
            rfvcity.Enabled = false;
            rfvcurrentcompany.Enabled = false;
            rfvSource.Enabled = false;
            CVSourceDescription.Enabled = false;
            rfvcurrentCTC.Enabled = false;
            rfvExpectedCTC.Enabled = false;
            revUserName.Enabled = true;
        }
        //*************NEW Function ADDED by pravin khot on 15/March/2016 (using Mail to Candidate once Resume is Uploaded )*****START NEW CODE************* 
        private void SendEmailToCandidateAfterApply(int JobId, string password, Member newMember)
        {
            string sentStatus = string.Empty;
            EmailHelper emailManager = new EmailHelper();

            string mailbody = string.Empty;
            JobPosting jobdetails = Facade.GetJobPostingById(JobId);

            string jobcode = string.Empty;
            string CandidateName = string.Empty;
            string jobPosition = string.Empty;
            jobcode = jobdetails.JobPostingCode.ToString();
            //EmailSenderId = Facade.DefaultSenderEmail_Id();

            CandidateName = newMember.FirstName + " " + newMember.LastName;
            jobPosition = jobdetails.JobTitle.ToString();
            //*******************Code line commented and added by pravin khot on 17/March/2016*********Change URL*********
            //SecureUrl CareerPageurl = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "CandidatePortal/CareerPage.aspx", CurrentMember.PrimaryEmail);

            //SecureUrl CareerPageurl = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "CandidatePortal/login.aspx", CurrentMember.PrimaryEmail);//line commented by pravin khot on 28/March/2016
            SecureUrl CareerPageurl = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "CandidatePortal/login.aspx", newMember.PrimaryEmail);//line added by pravin khot on 28/March/2016
            //**********************************END*****************************************************************

            System.Text.StringBuilder reqReport = new System.Text.StringBuilder();

            reqReport.Append("<html>");
            reqReport.Append("<head><title></title>");
            reqReport.Append("<style>body, td, P{font-family: Arial;font-size: 12px;} </style></head>");
            reqReport.Append("<body><P>Dear " + CandidateName + ",</P>");
            reqReport.Append("<P>We have received your application for the position of " + jobPosition + " - " + jobcode + " and are currently reviewing your experience and qualifications.</P>");
            reqReport.Append("<P>If you would like to review your candidate file, click here :<STRONG> <a href=[CAREERPAGELINK]>[COMPANYNAME]/CareerPage.aspx</a> </STRONG></P>");
            reqReport.Append("<P>Login Credentials that you created on our Career Page.</P>");
            reqReport.Append("<table><tr>");
            reqReport.Append("<td width=\"100\"><STRONG>Login ID</STRONG></td><td width=\"10\" align=\"center\">:</td><td width=500> " + newMember.PrimaryEmail + "</td></tr>");
            reqReport.Append("<tr><td width=\"100\"><STRONG>Password</STRONG><td width=\"10\" align=\"center\">:</td> <td width=500> " + password + "</td></tr>");
            reqReport.Append("<tr/></table>");
            reqReport.Append("<P>If your profile corresponds to our requirements, we will contact you soon. We thank you for your interest in [COMPANYNAME].</P>");
            reqReport.Append("<P>Thank you,<br>Talent Acquisition Team <br>[COMPANYNAME]</P>");
            reqReport.Append("</body></html>");
            /*reqReport.Append("<table style='font-family : Arial ;font-size:12pt; width:100%; border-collapse:collapse; border-spacing: 0px;' align='left' border = '0' bordercolor='#000000'  cellspacing='0' frame='value' cellpadding='5'  >");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Dear " + CandidateName + ",</td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            //************ Code Modified By Sakthi on 04/05/2016 *************
            reqReport.Append("        <td>We have received your application for the position of " + jobPosition + " - " + jobcode + " and are currently reviewing your experience and qualifications.</td>");
            //************ Code END By Sakthi on 04/05/2016 *************
            reqReport.Append("    </tr>");


            reqReport.Append("    <tr>");
            reqReport.Append("        <td>If you would like to review your candidate file, click here :<STRONG> <a href=[CAREERPAGELINK]>[COMPANYNAME]/CareerPage.aspx</a> </STRONG></td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Login Credentials that you created on our Career Page.</td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td><b>Login ID :</b> " + newMember.PrimaryEmail + "<br>");
            reqReport.Append("        <b>Password :</b> " + password + "</td>");
            reqReport.Append("    </tr>");

            reqReport.Append("    <tr>");
            reqReport.Append("        <td>If your profile corresponds to our requirements, we will contact you soon. We thank you for your interest in [COMPANYNAME].</td>");
            reqReport.Append("    </tr>");
            //************ Code Modified By Sakthi on 04/05/2016 *************
            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Thank you,<br>");
            reqReport.Append("    Talent Acquisition Team <br>[COMPANYNAME] </td>");
            reqReport.Append("    </tr>");
            //************ Code END By Sakthi on 04/05/2016 *************
            reqReport.Append("    <tr>");
            reqReport.Append("        <td>Replies to this message are undeliverable and will not reach the [COMPANYNAME]. Please do not reply.</td>");
            reqReport.Append("    </tr>");

            reqReport.Append(" </table>");*/

            Hashtable siteSettingTable = null;
            //**********line commented by pravin khot on 28/March/2016**************
            //TPS360.BusinessFacade.Facade f = new TPS360.BusinessFacade.Facade();
            //MemberExtendedInformation MailSetting = f.GetMemberExtendedInformationByMemberId(CurrentMember.Id);
            //TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
            //SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            //**********line commented END**************
            SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)TPS360.Common.Shared.SettingType.SiteSetting);
            siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);

            reqReport = reqReport.Replace("[COMPANYNAME]", siteSettingTable[DefaultSiteSetting.CompanyName.ToString()].ToString());
            reqReport = reqReport.Replace("[CAREERPAGELINK]", CareerPageurl.ToString());
            mailbody = reqReport.ToString();

            string mailsubject = string.Empty;
            mailsubject = "Thank You from " + siteSettingTable[DefaultSiteSetting.CompanyName.ToString()].ToString();

            ////MailQueueData.AddMailToMailQueue(CurrentMember.Id, newMember.PrimaryEmail, mailsubject, mailbody, "", "", null, Facade);//line commented by pravin khot on 28/March/2016
            //MailQueueData.AddMailToMailQueue(EmailSenderId, newMember.PrimaryEmail, mailsubject, mailbody, "", "", null, Facade);//line added/modify by pravin khot on 28/March/2016

            //*********Code added by pravin khot on 29/March/2016************************START CODE***********
            if (SiteSetting == null)
            {
                Hashtable config = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                emailManager.From = config[TPS360.Common.Shared.DefaultSiteSetting.AdminEmail.ToString()].ToString();// CurrentMember.PrimaryEmail;
            }
            else emailManager.From = SiteSetting[TPS360.Common.Shared.DefaultSiteSetting.AdminEmail.ToString()].ToString();// CurrentMember.PrimaryEmail;

            var stream = new MemoryStream();
            stream.Position = 0;
            reqReport = reqReport.Replace("[COMPANYNAME]", siteSettingTable[DefaultSiteSetting.CompanyName.ToString()].ToString());
            reqReport = reqReport.Replace("[CAREERPAGELINK]", CareerPageurl.ToString());
            emailManager.Subject = mailsubject;
            emailManager.To.Add(newMember.PrimaryEmail);
            emailManager.Body = reqReport.ToString();
 
            //***********Added by pravin khot on 8/March/2017***********
            //sentStatus = emailManager.Send();//comment by pravin khot on 17/June/2016
            //sentStatus = emailManager.SendMailFromSystemEmailId();//added by pravin khot on 17/June/2016
            int senderid = 0;
            string AdminEmailId = string.Empty;
            if (siteSetting != null)
            {
                AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                senderid = Facade.GetMemberIdByEmail(AdminEmailId);
            }
            MailQueueData.AddMailToMailQueue(senderid, newMember.PrimaryEmail, mailsubject, reqReport.ToString(), "", "", null, Facade);
            sentStatus = "1";
            //mailSender.Send(mailMessage);
            //*****************END**********************


            if (sentStatus == "1")
            {
                SaveMemberEmail(mailsubject, reqReport.ToString(), emailManager.From, newMember.PrimaryEmail);
                sentStatus = "Sent";
            }
            //**************END CODE*****************************************
        }

        //******New functions added by pravin khot on 29/March/2016 --1.SaveMemberEmail ,2.BuildMemberEmail*********************************************
        private void SaveMemberEmail(string subject, string body, string From, string To)
        {
            if (true) // Isvalid
            {
                try
                {
                    MemberEmail memberEmail = BuildMemberEmail(subject, body, From, To);
                    Facade.AddMemberEmail(memberEmail);
                }
                catch (ArgumentException ex)
                {

                }

            }
        }
        private MemberEmail BuildMemberEmail(string subject, string body, string strFrom, string To)
        {
            MemberEmail memberEmail = new MemberEmail();
            memberEmail.SenderId = 0;
            memberEmail.SenderEmail = strFrom;
            memberEmail.ReceiverEmail = To;
            memberEmail.Subject = subject;
            memberEmail.EmailBody = body;
            memberEmail.Status = 0;
            memberEmail.IsRemoved = false;
            memberEmail.CreatorId = 0;
            memberEmail.SentDate = DateTime.Now.ToString();
            memberEmail.EmailTypeLookupId = (int)TPS360.Common.Shared.EmailType.Sent;
            return memberEmail;
        }
        //**********************************END NEW CODE*********************************************

        #endregion

        #region Events
        private void SitemapPermission(int ParentId)
        {
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(ParentId, base.CurrentMember != null ? base.CurrentMember.Id : 0);
            if (CustomMap == null) _IsAccessForOverview = false;
            else
            {
                IdForSitemap = CustomMap.Id;
                UrlForAccess = "~/" + CustomMap.Url.ToString();
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {

        }

     
        protected void Page_Load(object sender, EventArgs e)
        {
            btnCandidateLogin.Visible = false;//code added by pravin khot on 19/May/2016
           //************ Code added by pravin khot on 28/Jan/2016********************
            divHide.Visible = false;
            if (JobId > 0)
            {
                btnbacktoCareer.Visible = true;
            }
            else
            {
                btnbacktoCareer.Visible = false;
            }
            //***************************End************************************

            if (ApplicationSource == ApplicationSource.MainApplication)
            {
                DisableValidationControl();
                hdnApplicationType.Value = "";
            }

            //if (IsUserVendor)
            //    spnRequiredField.Visible = rfvproof.Enabled = true;
            //else
            //    spnRequiredField.Visible = rfvproof.Enabled = false;
            ddlVendor.Attributes.Add("onChange", "return Company_OnChange('" + ddlVendor.ClientID + "','" + ddlVendorContact.ClientID + "','" + hdntemp.ClientID + "','MemberID')");
            ddlVendorContact.Attributes.Add("OnChange", "return Contact_OnChange('" + ddlVendorContact.ClientID + "','" + hdntemp.ClientID + "')");
            CommonLoad();
            if (!IsPostBack)
            {
                dvRecaptcha.Visible = false; //Code introduced by Prasanth on 11/Jan/2015
                //Code intorduced by Prasanth on 10/Jun/2016
                if (Request.Url.ToString().ToLower().Contains("employee/registrationinfoeditor.aspx"))
                {
                    divUserName.Visible = true;
                    divLDAP.Visible = true;
                }
                //***********************END******************
                if (Request.Url.ToString().ToLower().Contains("candidateregistration.aspx"))
                {

                    btnCandidateLogin.Visible = true ;//code added by pravin khot on 19/May/2016
                 
                    divPassword.Visible = false  ;  //Code modify by pravin khot on 17/May/2016 divPassword.Visible = true ; 
                    //divCaptcha.Visible = true;   //Code commented by Prasanth on 11/Jan/2015
                    dvRecaptcha.Visible = true;  //Code introduced by Prasanth on 11/Jan/2015
                }
                //********Code added by pravin khot on 11/May/2016********
                divIDProof.Visible = false;
                divCurrentCompany.Visible = false;
                divNoticePeriod.Visible = true;
                divCurrentCTC.Visible = true;
                divExpectedCTC.Visible = true;
                divTotalExp.Visible = true;
               
                //******************END**********************
                uclRefer.Visible = false;
                if (IsUserVendor)
                {
                    divSource.Visible = false;
                }
                if (Request.Url.ToString().ToLower().Contains("employeereferral.aspx"))
                {
                    divLinkedIn.Visible = true;
                    divSource.Visible = false;
                    uclRefer.Visible = true;
                    divprofileBuilder.Visible = divNotInEmployeeReferal.Visible = false;
                    lblCandidateInfoHeader.Text = "Candidate Information";
                    spanCandidateInfoHeader.InnerText = "Enter the candidate's contact information and upload their resume";
                    btnSave.Text = "Submit Candidate";
                    divCaptcha.Visible = true;
                    divCapthaHeader.Visible = true;
                    divCurrentCTC.Visible = false;
                    divExpectedCTC.Visible = false;
                    divCurrentCompany.Visible = false;
                    divTotalExp.Visible = false;
                    divIDProof.Visible = false;
                }

                if (_role.ToLower() != ContextConstants.ROLE_CANDIDATE.ToLower())
                {
                    divAdditionalInfo.Visible = false;
                    divResumeOption.Visible = false;
                    //divCopyPasteResume.Visible = false;
                    //  divUploadResume.Visible = false;
                    divApplicationAccess.Visible = true;
                    MiscUtil.PopulateCustomRole(ddlRole, Facade);
                    ddlRole.Items.Remove(ddlRole.Items.FindByText("Candidate"));
                    ddlRole.Items.Remove(ddlRole.Items.FindByText("Vendor"));
                    ddlRole.Items.Remove(ddlRole.Items.FindByText("Please Select"));
                }
                else
                {
                }
                txtUserEmail.Focus();


                if (_message != "")
                {
                    lblMessage.Text = _message;
                }
                PrepareView();
            
                //if (_memberId > 0)
                //{
                //    PrepareEditView();
                //}
            }

            lblCheckUser.Text = string.Empty;
            //dvRecaptcha.Visible = false;0123456789

          
        }

        protected void btnCheckUserName_Click(object sender, EventArgs e)
        {

            string txtEmail = txtUserEmail.Text;
            txtEmail = txtEmail.Trim();
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (!(re.IsMatch(txtEmail)))
            {
                lblCheckUser.Text = string.Empty;
                lblCheckUser.Text = "Please enter valid email address";
                lblCheckUser.ForeColor = System.Drawing.Color.Red;
                return;
            }
            MembershipUser user = Membership.GetUser(txtUserEmail.Text.Trim());
            System.Threading.Thread.Sleep(2000);
            if (user != null)
            {
                lblCheckUser.Text = string.Empty;
                lblCheckUser.Text = "Email Id not available. Please try with another email id.";
                lblCheckUser.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                lblCheckUser.Text = string.Empty;
                lblCheckUser.Text = "Email Id available.";
                lblCheckUser.ForeColor = System.Drawing.Color.Green;
            }
        }

       
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Helper.Url.Redirect(UrlConstants.CommonSite.HOME_PAGE);
        }
        //**********Code added by pravin khot on 19/May/2016*********
        protected void btnCandidateLogin_Click(object sender, EventArgs e)
        {
            Helper.Url.Redirect(UrlConstants.LOGIN_PAGE);
        }
        //************************END****************************
        private void AssignRole(int memberid)
        {
            if (ddlRole.SelectedItem.Text.ToLower() == ContextConstants.ROLE_ADMIN.ToLower())
            {
                string strUserName = string.Empty;
                _memberId = memberid;

                if (_memberId > 0)
                {
                    //strUserName = Facade.GetMemberUserNameById(_memberId);//Line commented by Prasanth on 14/Jun/2016
                    strUserName = Facade.GetMemberADUserNameById(_memberId);//Line Introduced by Prasanth on 14/Jun/2016
                     if (!Roles.IsUserInRole(strUserName, ContextConstants.ROLE_ADMIN))
                            Roles.AddUserToRole(strUserName, ContextConstants.ROLE_ADMIN);
                 

                }

            }
            else
            {
                _memberId = memberid;
                if (_memberId > 0)
                {
                    string strUserName = Facade.GetMemberUserNameById(_memberId);
                    if (Roles.IsUserInRole(strUserName, ContextConstants.ROLE_ADMIN))
                        Roles.RemoveUserFromRole(strUserName, ContextConstants.ROLE_ADMIN);

                }

            }

            MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(memberid);

            int roleId = Convert.ToInt32(ddlRole.SelectedItem.Value);

            if (map == null)
            {
                map = new MemberCustomRoleMap();
                map.CustomRoleId = roleId;
                map.MemberId = _memberId;
                map.CreatorId = base.CurrentMember != null ? base.CurrentMember.Id : 0;

                Facade.AddMemberCustomRoleMap(map);
                AddDefaultMenuAccess(_memberId);
            }
            else if (map.CustomRoleId != roleId)
            {
                map.CustomRoleId = roleId;
                map.UpdatorId = base.CurrentMember != null ? base.CurrentMember.Id : 0;
                Facade.UpdateMemberCustomRoleMap(map);
                AddDefaultMenuAccess(_memberId);
            }

            //PrepareView();
            //MiscUtil.ShowMessage(lblMessageAssignRole, "Role assigned successfully.", false);
        }

        private void AddDefaultMenuAccess(int memberid)
        {
            int roleId = Convert.ToInt32(ddlRole.SelectedItem.Value);
            ArrayList previlegeList = Facade.GetAllCustomRolePrivilegeIdsByRoleId(roleId);

            if (previlegeList != null && previlegeList.Count > 0)
            {
                Facade.DeleteMemberPrivilegeByMemberId(memberid);

                for (int i = 0; i <= previlegeList.Count - 1; i++)
                {
                    MemberPrivilege previlege = new MemberPrivilege();

                    previlege.CustomSiteMapId = Convert.ToInt32(previlegeList[i]);
                    previlege.MemberId = memberid;
                    Facade.AddMemberPrivilege(previlege);
                }
            }
        }
        //***************Code added by pravin khot on 28/Jan/2016********************
        protected void btnbacktoCareer_Click(object sender, EventArgs e)
        {
            Response.Redirect("CareerPage.aspx");
            //Helper.Url.Redirect(UrlConstants.CommonSite.HOME_PAGE);
        }
        //*************************End*************************************************

        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                Helper.Url.Redirect(UrlConstants.LOGIN_PAGE);
            }
        }
        protected void ChkExisting_Click(object sender, EventArgs e)
        {
            //uclConfirm.AddMessage("Mail Server SMTP details have not been entered. Would you like to enter them now?", ConfirmationWindow.enmMessageType.Attention, true, true, "");

            //if (ChkExistingUser.Checked)
            //{
            //    if (txtUserEmail.Text.Length > 0)
            //    {
            //        Member m = Facade.GetMemberByMemberEmail(txtUserEmail.Text);
            //        if (m != null)
            //        {
            //            string message = "Do you want to Submit?";

            //            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            //            sb.Append("return confirm('");

            //            sb.Append(message);

            //            sb.Append("');");

            //            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "alert", sb.ToString());

            //            //Helper.Url.Redirect(UrlConstants.LOGIN_PAGE);
            //           // uclConfirm.AddMessage("Mail Server SMTP details have?", ConfirmationWindow.enmMessageType.Attention, true, false, "");
            //           // MiscUtil.ShowMessage(lblMessage, "This email id allready applied another job. Please try with another email Id.", true);
                        
            //        }
            //    }
            //    else
            //    {
            //        txtUserEmail.Focus();
            //    }
            //    //MiscUtil.ShowMessage(lblMessage, "This email id allready applied another job. Please try with another email Id.", true);
            //    //Helper.Url.Redirect(UrlConstants.LOGIN_PAGE);
                
            //}
            //else            
            //{
               
            //}
           
        }
        protected void btnSaveRegistration_Click(object sender, EventArgs e)
        {
            //Code introduced by Prasanth on 20Jan2017 Start id 1153
            if (fuDocument.HasFile)
            {
                string extension = Path.GetExtension(fuDocument.FileName);
                string ContentType = fuDocument.PostedFile.ContentType;

                if (MiscUtil.Check_FileMIMEType(fuDocument) == false) return;
            }
            //if (!cvRecaptcha.IsValid || !rqfCheck.IsValid) return;
            if (Request.Url.ToString().ToLower().Contains("employeereferral.aspx"))
            {
                //if (!uclRefer.isJobPostingSelected)
                //{
                //    MiscUtil.ShowMessage(lblMessage, "Please select a requisition before submission.", true);
                //    return;
                //}
                //************Code added by pravin khot 11/May/2016********
                //if (!MiscUtil.IsValidMailSetting(Facade, 0))
                //{
                //    MiscUtil.ShowMessage(lblMessage, "Mail Setting is not configured. Please contact your administrator.", false);
                //    return;
                //}
                //*********************END*****************
            }
            string txtEmail = txtUserEmail.Text;
            txtEmail = txtEmail.Trim();
            txtFirstName.Text = MiscUtil.RemoveScript(txtFirstName.Text.Trim());
            txtLastName.Text = MiscUtil.RemoveScript(txtLastName.Text.Trim());
            if (txtLastName.Text.Trim() != string.Empty && txtFirstName.Text.Trim() != string.Empty)
            {
                string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                Regex re = new Regex(strRegex);
                if (!(re.IsMatch(txtEmail)))
                {
                    lblCheckUser.Text = string.Empty;
                    lblCheckUser.Text = "Please enter valid email address";
                    lblCheckUser.ForeColor = System.Drawing.Color.Red;
                    return;
                }
                CheckMemberAvailability(txtUserEmail.Text);
                MembershipUser user = Membership.GetUser(txtUserEmail.Text.Trim());
                bool sourceexpired = Facade.IsMemberCandidateSourceExpired(txtUserEmail.Text.Trim());

                if (user != null && !sourceexpired)
                {
                    //if (!uclRefer.Visible)
                    {
                        //************Code added by pravin khot on 13/June/2016**************
                        if (Request.Url.ToString().ToLower().Contains("employee/registrationinfoeditor.aspx"))
                        {
                            Member mem1 = Facade.GetMemberByMemberEmail(txtUserEmail.Text.Trim());
                            if (mem1 != null)
                            {
                                MiscUtil.ShowMessage(lblMessage, "User Email is allready exists", true);
                                txtUserEmail.Focus();
                            }
                        }
                        //**************************END************************

                        if (Request.Url.ToString().ToLower().Contains("candidateportal/candidateregistration.aspx"))
                        {
                            if (JobId > 0)
                            {
                                //uclConfirm.MsgBoxAnswered += MessageAnswered;
                                MiscUtil.ShowMessage(lblMessage, "This email id allready applied another job. Please try with another email Id.", true);
                                txtUserEmail.Focus();
                                //Helper.Url.Redirect(UrlConstants.LOGIN_PAGE);
                            }
                            else
                            {
                                lblCheckUser.Visible = true;
                                lblCheckUser.Text = string.Empty;
                                lblCheckUser.Text = "Email Id not available. Please try with another email Id.";
                                lblCheckUser.ForeColor = System.Drawing.Color.Red;                       
                            }
                        }

                        //*****************************Code added by pravin khot on 11/July/2016*******
                        if (Request.Url.ToString().ToLower().Contains("referral/employeereferral.aspx"))
                        {

                            Member mem1 = Facade.GetMemberByMemberEmail(txtUserEmail.Text.Trim());
                            if (mem1 == null)
                            {
                                RegisterUser();
                            }
                            else
                            {
                                MiscUtil.ShowMessage(lblMessage, "Candidate Profile Already Exists", true);
                                txtUserEmail.Focus();
                            }
                        }
                        //******************************END*********************************

                        //*****************************Code added by pravin khot on 11/July/2016*******
                        if (Request.Url.ToString().ToLower().Contains("ats/registrationinfoeditor.aspx"))
                        {

                            Member mem1 = Facade.GetMemberByMemberEmail(txtUserEmail.Text.Trim());
                            if (mem1 == null)
                            {
                                RegisterUser();
                            }
                            else
                            {
                                MiscUtil.ShowMessage(lblMessage, "Email is allready exists. Please try with another email Id.", true);
                                txtUserEmail.Focus();
                            }
                        }
                        //******************************END*********************************


                        //************Code added by pravin khot on 27/April/2016  Using for DefaultSiteSetting.AllowVendorstoUpdateCandidateProfiles**************
                       #region
                            Hashtable siteSettingTable = null;
                            TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
                            SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                            siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);

                            string chkstatus;
                            chkstatus = siteSettingTable[DefaultSiteSetting.AllowVendorstoUpdateCandidateProfiles.ToString()].ToString();

                            if (Request.Url.ToString().ToLower().Contains("vendor/createnewcandidate.aspx"))
                            {
                                if (chkstatus == "True")
                                {
                                    int canId = Facade.getCandidateIdbyPrimaryEmail(txtEmail.ToString());

                                    Member candidatecreatorid = Facade.GetMemberById(canId);
                                    //IList<Member> memberList = Facade.GetAllVendorCandidatesByCreatorId(CurrentMember.Id);
                                    //foreach (Member mem in memberList)
                                    //{
                                    //    if(mem.PrimaryEmail == txtEmail.ToString())
                                    //    {
                                    //        return;
                                    //    }
                                    //}
                                    MemberExtendedInformation souredes = Facade.GetMemberExtendedInformationByMemberId(canId);


                                    DateTime CreateDate = DateTime.Parse(candidatecreatorid.CreateDate.ToString("d"));
                                    DateTime UpdateDate = DateTime.Parse(candidatecreatorid.UpdateDate.ToString("d"));
                                    DateTime now = DateTime.Parse(DateTime.Now.ToString("d"));
                                    DateTime addsixmonthfromCREATEDATE = CreateDate.AddMonths(6);
                                    DateTime addsixmonthfromUPDATEDDATE = UpdateDate.AddMonths(6);

                                    if (now > addsixmonthfromUPDATEDDATE)
                                    {
                                        //if ((candidatecreatorid.CreatorId == CurrentMember.Id && souredes.SourceLookupId ==1150)||souredes.SourceLookupId !=1150)
                                        //{

                                        if (canId != 0)
                                        {
                                            Member _member = new Member();
                                            MemberDetail _memberDetail = new MemberDetail();
                                            MemberExtendedInformation _memberExtendedInfo = new MemberExtendedInformation();

                                            if (canId > 0)
                                            {
                                                _member = Facade.GetMemberById(canId);
                                            }

                                            if (canId > 0)
                                            {
                                                _memberDetail = Facade.GetMemberDetailByMemberId(canId);
                                            }

                                            if (canId > 0)
                                            {
                                                _memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(canId);
                                            }

                                            _member.Id = canId;
                                            _member.FirstName = txtFirstName.Text;
                                            _member.LastName = txtLastName.Text;
                                            _member.CellPhone = txtMobile.Text;
                                            _member.PermanentCity = txtCity.Text;
                                            _member.PermanentStateId = uclCountryState.SelectedStateId;
                                            _member.PermanentCountryId = uclCountryState.SelectedCountryId;
                                            _member.PermanentZip = txtPincode.Text;

                                            _member.CreatorId = base.CurrentMember.Id;

                                            Facade.UpdateMemberthroughvendor(_member);

                                            string countryName = string.Empty;
                                            if (SiteSetting != null)
                                            {
                                                Country coun = Facade.GetCountryById(Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString()));
                                                if (coun != null)
                                                    countryName = coun.Name;
                                            }
                                            int source = 0;
                                            string sourcedesc = "";

                                            getSourceandSrcdesc(out source, out sourcedesc);
                                            string months = uclExperience.Experience;
                                            ParseAndUpdate parse = new ParseAndUpdate(canId, "", "", "", "", "", "");
                                            if (rdoUploadResume.Checked)
                                            {

                                                UploadDocument(_member);
                                                parse.GetParsedResume(_member, "", "Upload", txtFirstName.Text + txtLastName.Text + " - " + "Resume" + Path.GetExtension(fuDocument.FileName), uclCountryState.SelectedCountryName.ToString(), txtMobile.Text, txtCity.Text, txtPincode.Text, uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(), txtCurrentCompany.Text, source, sourcedesc, months, txtCurrentYearlyCTC.Text, Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue), Convert.ToInt32(ddlCurrentSalaryCycle.SelectedValue), txtExpectedYearlyCTC.Text, Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue), Convert.ToInt32(ddlExpectedSalaryCycle.SelectedValue), Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text, MiscUtil.RemoveScript(txtLinkedIn.Text));
                                            }
                                            else if (rdoCopyPaste.Checked)
                                            {
                                                SaveCopyPasteResume(_member);
                                                parse.GetParsedResume(_member, txtCopyPasteResume.Text, "CopyPaste", "", countryName, txtMobile.Text, txtCity.Text, txtPincode.Text, uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(), txtCurrentCompany.Text, source, sourcedesc, months, txtCurrentYearlyCTC.Text, Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue), Convert.ToInt32(ddlCurrentSalaryCycle.SelectedValue), txtExpectedYearlyCTC.Text, Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue), Convert.ToInt32(ddlExpectedSalaryCycle.SelectedValue), Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text, MiscUtil.RemoveScript(txtLinkedIn.Text));
                                            }
                                            else if (rdoStepByStep.Checked)
                                            {
                                                parse.GetParsedResume(_member, "", "CopyPaste", "", countryName, txtMobile.Text, txtCity.Text, txtPincode.Text, uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(), txtCurrentCompany.Text, source, sourcedesc, months, txtCurrentYearlyCTC.Text, Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue), Convert.ToInt32(ddlCurrentSalaryCycle.SelectedValue), txtExpectedYearlyCTC.Text, Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue), Convert.ToInt32(ddlExpectedSalaryCycle.SelectedValue), Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text, MiscUtil.RemoveScript(txtLinkedIn.Text));

                                            }

                                            //*****************Code added by pravin khot on 11/May/2016 USING FOR ADD NoticePeriod********************* 
                                            _memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(_member.Id);
                                            _memberExtendedInfo.NoticePeriod = MiscUtil.RemoveScript(txtNoticePeriod.Text.Trim(), string.Empty);
                                            Facade.UpdateMemberExtendedInformation(_memberExtendedInfo);
                                            //********************************END*********************************

                                            //if (_IsAccessForOverview)
                                            //{
                                            //    ArrayList AList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                                            //    if (AList.Contains(362) || rdoStepByStep.Checked)
                                            //    {
                                            //        redirectUrl = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Candidate.CANDIDATE_INTERNALRESUMEBUILDER.Substring(2), "", UrlConstants.PARAM_MEMBER_ID, _member.Id.ToString(), UrlConstants.PARAM_MSG, "Candidate updated successfully.", UrlConstants.PARAM_RESUMEBUILD_OPTION, ((int)ResumeBuilderOption.StepByStep).ToString(), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.CANDIDATE_RESUMEBUILDER_SITEMAP, UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.ATS_OVERVIEW_SITEMAP_PARENTID);

                                            //        // Page.ClientScript.RegisterStartupScript(typeof(Page), "OpenCand", "<script>var w=window.open('" + url + "');</script>");
                                            //    }
                                            //    //clear();
                                            //}
                                            ////System.Web.Security.FormsAuthentication.SetAuthCookie(_member.PrimaryEmail, true);
                                            ////redirectUrl = UrlHelper.BuildSecureUrl(UrlConstants.CandidatePortal.HOME, string.Empty, UrlConstants.PARAM_MEMBER_ID, _member.Id.ToString());
                                            ////if (redirectUrl != null)
                                            ////{
                                            ////    Helper.Url.Redirect(redirectUrl.ToString());
                                            ////}
                                            clear();
                                            ddlExpectedYearlyCurrency.SelectedValue = "51";
                                            ddlCurrentYearlyCurrency.SelectedValue = "51";

                                            MiscUtil.ShowMessage(lblMessage, "Candidate updated successfully.", false);
                                        }
                                        //}
                                        //else
                                        //{
                                        //    lblMessage.ForeColor = System.Drawing.Color.Red;
                                        //    MiscUtil.ShowMessage(lblMessage, "Candidate Profile Already exists in our Database", true);

                                        //}
                                    }
                                    else if (candidatecreatorid.CreatorId == CurrentMember.Id)
                                    {
                                        if (canId != 0)
                                        {
                                            Member _member = new Member();
                                            MemberDetail _memberDetail = new MemberDetail();
                                            MemberExtendedInformation _memberExtendedInfo = new MemberExtendedInformation();

                                            if (canId > 0)
                                            {
                                                _member = Facade.GetMemberById(canId);
                                            }

                                            if (canId > 0)
                                            {
                                                _memberDetail = Facade.GetMemberDetailByMemberId(canId);
                                            }

                                            if (canId > 0)
                                            {
                                                _memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(canId);
                                            }

                                            _member.Id = canId;
                                            _member.FirstName = txtFirstName.Text;
                                            _member.LastName = txtLastName.Text;
                                            _member.CellPhone = txtMobile.Text;
                                            _member.PermanentCity = txtCity.Text;
                                            _member.PermanentStateId = uclCountryState.SelectedStateId;
                                            _member.PermanentCountryId = uclCountryState.SelectedCountryId;
                                            _member.PermanentZip = txtPincode.Text;

                                            Facade.UpdateMember(_member);

                                            string countryName = string.Empty;
                                            if (SiteSetting != null)
                                            {
                                                Country coun = Facade.GetCountryById(Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString()));
                                                if (coun != null)
                                                    countryName = coun.Name;
                                            }
                                            int source = 0;
                                            string sourcedesc = "";

                                            getSourceandSrcdesc(out source, out sourcedesc);
                                            string months = uclExperience.Experience;
                                            ParseAndUpdate parse = new ParseAndUpdate(canId, "", "", "", "", "", "");
                                            if (rdoUploadResume.Checked)
                                            {

                                                UploadDocument(_member);
                                                parse.GetParsedResume(_member, "", "Upload", txtFirstName.Text + txtLastName.Text + " - " + "Resume" + Path.GetExtension(fuDocument.FileName), uclCountryState.SelectedCountryName.ToString(), txtMobile.Text, txtCity.Text, txtPincode.Text, uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(), txtCurrentCompany.Text, source, sourcedesc, months, txtCurrentYearlyCTC.Text, Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue), Convert.ToInt32(ddlCurrentSalaryCycle.SelectedValue), txtExpectedYearlyCTC.Text, Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue), Convert.ToInt32(ddlExpectedSalaryCycle.SelectedValue), Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text, MiscUtil.RemoveScript(txtLinkedIn.Text));
                                            }
                                            else if (rdoCopyPaste.Checked)
                                            {
                                                SaveCopyPasteResume(_member);
                                                parse.GetParsedResume(_member, txtCopyPasteResume.Text, "CopyPaste", "", countryName, txtMobile.Text, txtCity.Text, txtPincode.Text, uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(), txtCurrentCompany.Text, source, sourcedesc, months, txtCurrentYearlyCTC.Text, Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue), Convert.ToInt32(ddlCurrentSalaryCycle.SelectedValue), txtExpectedYearlyCTC.Text, Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue), Convert.ToInt32(ddlExpectedSalaryCycle.SelectedValue), Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text, MiscUtil.RemoveScript(txtLinkedIn.Text));
                                            }
                                            else if (rdoStepByStep.Checked)
                                            {
                                                parse.GetParsedResume(_member, "", "CopyPaste", "", countryName, txtMobile.Text, txtCity.Text, txtPincode.Text, uclCountryState.SelectedStateId.ToString(), uclCountryState.SelectedCountryId.ToString(), txtCurrentCompany.Text, source, sourcedesc, months, txtCurrentYearlyCTC.Text, Convert.ToInt32(ddlCurrentYearlyCurrency.SelectedValue), Convert.ToInt32(ddlCurrentSalaryCycle.SelectedValue), txtExpectedYearlyCTC.Text, Convert.ToInt32(ddlExpectedYearlyCurrency.SelectedValue), Convert.ToInt32(ddlExpectedSalaryCycle.SelectedValue), Convert.ToInt32(ddlIDProof.SelectedValue), txtIDProof.Text, MiscUtil.RemoveScript(txtLinkedIn.Text));

                                            }

                                            //*****************Code added by pravin khot on 11/May/2016 USING FOR ADD NoticePeriod********************* 
                                            _memberExtendedInfo = Facade.GetMemberExtendedInformationByMemberId(_member.Id);
                                            _memberExtendedInfo.NoticePeriod = MiscUtil.RemoveScript(txtNoticePeriod.Text.Trim(), string.Empty);
                                            Facade.UpdateMemberExtendedInformation(_memberExtendedInfo);
                                            //********************************END*********************************
                                            //if (_IsAccessForOverview)
                                            //{
                                            //    ArrayList AList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                                            //    if (AList.Contains(362) || rdoStepByStep.Checked)
                                            //    {
                                            //        redirectUrl = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Candidate.CANDIDATE_INTERNALRESUMEBUILDER.Substring(2), "", UrlConstants.PARAM_MEMBER_ID, _member.Id.ToString(), UrlConstants.PARAM_MSG, "Candidate updated successfully.", UrlConstants.PARAM_RESUMEBUILD_OPTION, ((int)ResumeBuilderOption.StepByStep).ToString(), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.CANDIDATE_RESUMEBUILDER_SITEMAP, UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.ATS_OVERVIEW_SITEMAP_PARENTID);

                                            //        // Page.ClientScript.RegisterStartupScript(typeof(Page), "OpenCand", "<script>var w=window.open('" + url + "');</script>");
                                            //    }
                                            //    //clear();
                                            //}
                                            ////System.Web.Security.FormsAuthentication.SetAuthCookie(_member.PrimaryEmail, true);
                                            ////redirectUrl = UrlHelper.BuildSecureUrl(UrlConstants.CandidatePortal.HOME, string.Empty, UrlConstants.PARAM_MEMBER_ID, _member.Id.ToString());
                                            ////if (redirectUrl != null)
                                            ////{
                                            ////    Helper.Url.Redirect(redirectUrl.ToString());
                                            ////}
                                            clear();
                                            ddlExpectedYearlyCurrency.SelectedValue = "51";
                                            ddlCurrentYearlyCurrency.SelectedValue = "51";

                                            MiscUtil.ShowMessage(lblMessage, "Candidate updated successfully.", false);
                                        }

                                    }
                                    else
                                    {
                                        lblMessage.ForeColor = System.Drawing.Color.Red;
                                        MiscUtil.ShowMessage(lblMessage, "Candidate Profile Already exists in our Database", true);

                                    }
                                }
                                else
                                {
                                    lblMessage.ForeColor = System.Drawing.Color.Red;
                                    MiscUtil.ShowMessage(lblMessage, "Candidate Profile already Available,cannot update the profile", true);
                                }
                            }
                            #endregion
                        //****************************END 27/April/2016*****************************************
                                         
                        
                    }
                    //else
                    //{
                    //    Member newmember = Facade.GetMemberByMemberEmail(txtUserEmail.Text.Trim());
                    //    if (newmember != null)
                    //    {
                    //       // uclRefer.CandidateName = newmember.FirstName + " " + newmember.LastName;
                    //        uclRefer.AddEmployeeReferalDetails(newmember.Id);

                    //        clear();
                    //        lblCheckUser.Visible = true;
                    //        lblCheckUser.Text = string.Empty;
                    //        lblCheckUser.Text = "Email Id not available. Please try with another email Id.";
                    //        lblCheckUser.ForeColor = System.Drawing.Color.Red;
                    //    }
                    //}
                    

                }
                else
                {
                    if (_role != String.Empty)
                    {
                        if (_role.ToLower() == ContextConstants.ROLE_CANDIDATE.ToLower() && sourceexpired && user != null)
                        {
                            UpdateUserOwnership();
                            return;
                        }

                        if (_memberId == 0)
                        {
                              // _mailsetting = MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id);
                            if (Request.Url.ToString().ToLower().Contains("vendor/createnewcandidate.aspx") || Request.Url.ToString().ToLower().Contains("referral/employeereferral.aspx") || Request.Url.ToString().ToLower().Contains("ats/registrationinfoeditor.aspx"))
                            {
                                RegisterUser();
                            }
                            else if (Request.Url.ToString().ToLower().Contains("employee/registrationinfoeditor.aspx"))
                            {
                                //************Code added by pravin khot on 13/June/2016**************
                                Member mem = Facade.GetMemberByMemberUserName(txtUserName.Text.Trim());
                                if (mem == null)
                                {
                                    Member mem1 = Facade.GetMemberByMemberEmail(txtUserEmail.Text.Trim());
                                    if (mem1 == null)
                                    {
                                        if (ddlPassordOption.SelectedIndex == 1)
                                        {
                                            if (MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id))
                                                RegisterUser();
                                            else
                                            {
                                                dv.Visible = true;
                                                uclConfirm.AddMessage("Mail Server SMTP details have not been entered. Would you like to enter them now?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
                                                return;
                                            }
                                        }
                                        else
                                            RegisterUser();
                                    }
                                    else
                                    {
                                        MiscUtil.ShowMessage(lblMessage, "User Email is allready exists", true);
                                        txtUserEmail.Focus();
                                    }

                                }
                                else
                                {
                                    MiscUtil.ShowMessage(lblMessage, "User Name is allready exists", true);
                                    txtUserName.Focus(); 
                                }  
                                //********************END****************************
                              
                            }
                            else
                            {
                                //if (MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id)) 
                                    RegisterUser();
                                //else
                                //{
                                //    dv.Visible = true;
                                //    uclConfirm.AddMessage("Mail Server SMTP details have not been entered. Would you like to enter them now?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
                                //    return;
                                //}
                            }
                          //RegisterUser();
                        }
                    }
                    else
                    {
                        MiscUtil.ShowMessage(lblMessage, "Role is undefined", true);
                    }
                }
            }

        }

        #endregion

    }
}