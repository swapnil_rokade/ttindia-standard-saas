﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.IO;

namespace TPS360.Web.UI
{
    public partial class CommonJoiningDetails : ATSBaseControl   
    {
        #region Member Variables

        #endregion

        #region Properties
        public string bulk = string.Empty;
        public string BulkAction
        {
            get
            {
                return hdnBulkAction.Value == string.Empty ? "" : hdnBulkAction.Value;
            }
            set
            {
                hdnBulkAction.Value = value.ToString();
                bulk = value;
            }
        }

        public string _MemberId = string.Empty;
        public string MemberID
        {
            get
            {
                return hfMemberId.Value == string.Empty ? "0" : hfMemberId.Value;
            }
            set
            {
                StatusId = 0;
                hfMemberId.Value = value;
                _MemberId = value;
                if (!hfMemberId .Value .Contains (","))
                    PrepareEditView();
                else
                {
                    btnRemove.Visible = false;
                    //txtJoiningDate.Text = DateTime.Now.ToShortDateString();
                }
                hdnBulkAction.Value = string.Empty;

            }
        }
        public int _statusID = 0;
        public int StatusId
        {
            get
            {
                return Convert.ToInt32(hfStatusId.Value == string.Empty ? "0" : hfStatusId.Value);
            }
            set
            {
                hfStatusId.Value = value.ToString();
                _statusID = value;
            }
        }
        public int _JobPostingId = 0;
        public int JobPostingId
        {
            get
            {
                return Convert.ToInt32(hfJobPostingId.Value == string.Empty ? "0" : hfJobPostingId.Value);
            }
            set
            {
                hfJobPostingId.Value = value.ToString();
                _JobPostingId = value;
                // Prepareview();
            }
        }
        public delegate void JoiningDetailsEventHandler(string MemberId, int StatusId, bool IsAdded);
        public event JoiningDetailsEventHandler JoiningDetailsAdded;
        #endregion

        #region Methods
        private void Prepareview()
        {
            MiscUtil.PopulateCurrency(ddlSalaryCurrency, Facade);
            MiscUtil.PopulateCurrency(ddlBillableSalaryCurrency, Facade);
            MiscUtil.PopulateCurrency(ddlRevenueCurrency, Facade);
            GetDefaultsFromSiteSetting();
            if (ddlSalaryCurrency.SelectedItem != null)
            {
                if (ddlSalaryCurrency.SelectedItem.Text == "INR") lblPayrateCurrency.Text = "(Lacs)";
                else lblPayrateCurrency.Text = "";
            }
            if (ddlBillableSalaryCurrency.SelectedItem != null)
            {
                if (ddlBillableSalaryCurrency.SelectedItem.Text == "INR") lblBillablePayRateCurrency.Text = "(Lacs)";
                else lblBillablePayRateCurrency.Text = "";
            }
            if (ddlRevenueCurrency.SelectedItem != null)
            {
                if (ddlRevenueCurrency.SelectedItem.Text == "INR") lblRevenueCurrency.Text = "(Lacs)";
                else lblRevenueCurrency.Text = "";
            }
            if (wdcJoiningDate.Text == null || wdcJoiningDate.Text == "")
                wdcJoiningDate.Text = DateTime.Now.ToString();
        }
        private void PrepareEditView()
        {
            ClearControls();
            MemberJoiningDetail memberJoiningDetails = Facade.GetMemberJoiningDetailsByMemberIdAndJobPostingID(Convert.ToInt32(hfMemberId.Value), JobPostingId);
            if (memberJoiningDetails != null)
            {
                btnRemove.Visible = true;
                hfMemberJoiningDetailsId .Value  = memberJoiningDetails.Id.ToString();
                txtSalary.Text = memberJoiningDetails.OfferedSalary;
                txtBillableSalary.Text = memberJoiningDetails.BillableSalary;
                txtBillingRate.Text = memberJoiningDetails.BillingRate;
                txtRevenue.Text = memberJoiningDetails.Revenue;
                txtLocation.Text = memberJoiningDetails.OtherLocation;
                ControlHelper.SelectListByValue(ddlSalaryCurrency, memberJoiningDetails.OfferedSalaryCurrency.ToString());
                ControlHelper.SelectListByValue(ddlSalary, memberJoiningDetails.OfferedSalaryPayCycle.ToString());
                ControlHelper.SelectListByValue(ddlBillableSalaryCurrency, memberJoiningDetails.BillableSalaryCurrency.ToString());
                ControlHelper.SelectListByValue(ddlRevenueCurrency, memberJoiningDetails.RevenueCurrency.ToString());
                if (memberJoiningDetails.JoiningDate != DateTime.MinValue)
                {
                    //txtJoiningDate.Text = memberJoiningDetails.JoiningDate.ToShortDateString();
                    //wdcJoiningDate.SelectedDate = memberJoiningDetails.JoiningDate;
                    wdcJoiningDate.Text = memberJoiningDetails.JoiningDate.ToString();
                }
                else 
                { 
                    //txtJoiningDate.Text = ""; wdcJoiningDate.SelectedDate = null; 
                    wdcJoiningDate.Text = "";
                }
                if (ddlSalaryCurrency.SelectedItem != null)
                {
                    if (ddlSalaryCurrency.SelectedItem.Text == "INR") lblPayrateCurrency.Text = "(Lacs)";
                    else lblPayrateCurrency.Text = "";
                }
                if (ddlBillableSalaryCurrency.SelectedItem != null)
                {
                    if (ddlBillableSalaryCurrency.SelectedItem.Text == "INR") lblBillablePayRateCurrency.Text = "(Lacs)";
                    else lblBillablePayRateCurrency.Text = "";
                }
                if (ddlRevenueCurrency.SelectedItem != null)
                {
                    if (ddlRevenueCurrency.SelectedItem.Text == "INR") lblRevenueCurrency.Text = "(Lacs)";
                    else lblRevenueCurrency.Text = "";
                }
            }
            else
            {
                btnRemove.Visible = false;
                ClearControls();
            }
        }
        private MemberJoiningDetail BuildMemberJoiningDetails()
        {
            MemberJoiningDetail mem = new MemberJoiningDetail();
            mem.JobPostingId = JobPostingId;
            mem.OfferedSalary = txtSalary.Text.Trim();
            mem.BillableSalary = txtBillableSalary.Text.Trim();
            mem.BillingRate = txtBillingRate.Text.Trim();
            mem.Revenue = txtRevenue.Text.Trim();
            mem.OfferedSalaryPayCycle = Convert.ToInt32(ddlSalary.SelectedValue);
            mem.OfferedSalaryCurrency = Convert.ToInt32(ddlSalaryCurrency.SelectedValue);
            mem.BillableSalaryCurrency = Convert.ToInt32(ddlBillableSalaryCurrency.SelectedValue);
            mem.RevenueCurrency = Convert.ToInt32(ddlRevenueCurrency.SelectedValue);
            //DateTime value;
            //DateTime.TryParse(txtJoiningDate.Text, out value);
            //wdcJoiningDate.SelectedDate = value;
            //if (txtJoiningDate.Text.Trim() != string.Empty && wdcJoiningDate.SelectedDate != null)
            //    mem.JoiningDate = Convert.ToDateTime(wdcJoiningDate.SelectedDate);// txtJoiningDate.Text.Trim());
            if (wdcJoiningDate.Text != "")
                mem.JoiningDate = Convert.ToDateTime(wdcJoiningDate.Text);
            mem.IsRemoved = false;
            mem.OtherLocation = txtLocation.Text.Trim();
            mem.CreatorId = base.CurrentMember.Id;
            mem.UpdatorId = base.CurrentMember.Id;
            return mem;
        }
        private void GetDefaultsFromSiteSetting()
        {

            if (SiteSetting != null)
            {
                ddlBillableSalaryCurrency .SelectedValue = ddlRevenueCurrency .SelectedValue = ddlSalaryCurrency.SelectedValue = SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString();
            }
        }

        private void ClearControls()
        {
            txtSalary.Text = "";
            txtRevenue.Text = "";
            txtBillableSalary.Text = "";
            txtBillingRate.Text = "";
            txtLocation.Text = "";
            ControlHelper.SelectListByValue(ddlSalaryCurrency, "0");
            ControlHelper.SelectListByValue(ddlBillableSalaryCurrency, "0");
            ControlHelper.SelectListByValue(ddlRevenueCurrency, "0");
            GetDefaultsFromSiteSetting();
            //txtJoiningDate.Text =  DateTime.Now.ToShortDateString();
            wdcJoiningDate.Text = DateTime.Now.ToString();
            if (ddlSalaryCurrency.SelectedItem != null)
            {
                if (ddlSalaryCurrency.SelectedItem.Text == "INR") lblPayrateCurrency.Text = "(Lacs)";
                else lblPayrateCurrency.Text = "";
            }
            if (ddlBillableSalaryCurrency.SelectedItem != null)
            {

                if (ddlBillableSalaryCurrency.SelectedItem.Text == "INR") lblBillablePayRateCurrency.Text = "(Lacs)";
                else lblBillablePayRateCurrency.Text = "";
            }
            if (ddlRevenueCurrency.SelectedItem != null)
            {
                if (ddlRevenueCurrency.SelectedItem.Text == "INR") lblRevenueCurrency.Text = "(Lacs)";
                else lblRevenueCurrency.Text = "";
            }
          
        }
        #endregion

        #region PageEvents
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlSalaryCurrency.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('" + ddlSalaryCurrency.ClientID + "','" + lblPayrateCurrency.ClientID + "')");
            ddlBillableSalaryCurrency.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('" + ddlBillableSalaryCurrency.ClientID + "','" +  lblBillablePayRateCurrency.ClientID + "')");
            ddlRevenueCurrency.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('" +   ddlRevenueCurrency.ClientID + "','" + lblRevenueCurrency.ClientID + "')");
            //txtJoiningDate.Text = DateTime.Now.ToShortDateString();
            //wdcJoiningDate.Format = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern; //"dd/MM/yyyy";
            gvRate.MinimumValue = "0";
            gvRate.MaximumValue = "100";
            if (!IsPostBack)
            {
                Prepareview();
                Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
                if (lbModalTitle != null) lbModalTitle.Text = "Joining Details";
            }
        }
        #endregion

        #region ButtonEvents
        protected void btnSave_Click(object sender, EventArgs e)
        {
            MemberJoiningDetail memberJoiningDetails = BuildMemberJoiningDetails();
            //*******Code added by pravin khot on 28/July/2016**************
            RejectCandidate cand = Facade.GetRejectCandidateByMemberIdAndJobPostingID(Int32.Parse(hfMemberId.Value), JobPostingId);
            if (cand != null)
            {
                Facade.RejectToUnRejectCandidateStatusChange(hfMemberId.Value, JobPostingId);
            }
            //************************END****************************
            Facade.AddMemberJoiningDetails(memberJoiningDetails, hfMemberId.Value);
            if (hfStatusId.Value != "" && Convert.ToInt32(hfStatusId.Value) > 0)
            {
                Facade.MemberJobCart_MoveToNextLevel(0, base.CurrentMember.Id, hfMemberId .Value , JobPostingId , hfStatusId .Value );
                Facade.UpdateCandidateRequisitionStatus (JobPostingId, hfMemberId.Value, base.CurrentMember.Id,Convert .ToInt32 (hfStatusId .Value ));
                MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, JobPostingId , hfMemberId .Value , CurrentMember.Id, "", Facade);
                ScriptManager.RegisterClientScriptBlock(lblBillablePayRateCurrency , typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Selected candidate(s) moved Successfully.');", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(lblBillablePayRateCurrency, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Joining Details Updated Successfully.');", true);
            }
        }
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(hfMemberJoiningDetailsId.Value) > 0)
            {
                ClearControls();
                if (txtSalary.Text.Trim() == string.Empty )
                {
                    Facade.DeleteMemberJoiningDetailsByID(Convert.ToInt32(hfMemberJoiningDetailsId.Value));
                }
            }
            if (JoiningDetailsAdded != null) JoiningDetailsAdded(hfMemberId.Value, Convert.ToInt32(hfStatusId.Value != string.Empty ? hfStatusId.Value : "0"), false);
        }
        #endregion
    }
}
