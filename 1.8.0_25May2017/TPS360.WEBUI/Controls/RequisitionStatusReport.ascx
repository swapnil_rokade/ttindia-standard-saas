﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionStatusReport.ascx.cs"
    Inherits="TPS360.Web.UI.cltRequisitionStatusReport" %>

<%@ Register Src="~/Controls/CandidateActionLog.ascx" TagName ="HiringLog" TagPrefix ="ucl" %>
<%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>

    <script type ="text/javascript" >
		
		function Validate(source, arguments) 
		{
		
		        var sel=document .getElementById ('<%= lstRecruiters.ClientID %>');
                var listLength = sel.options.length;
                for(var i=0;i<listLength;i++)
                {
                    if(sel.options[i].selected && i!=0)
                    {
                        arguments.IsValid =true ;
                        return ; 
                    }
                    else  arguments.IsValid =false ;
                }
        }

		</script>
<div style =" min-width :950px;" id="divStatus">
    <div class="TableRow" style="text-align:left">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="CommonHeader">
        <asp:Label EnableViewState="false" ID="lblJobStatusHeader" runat="server" Text="Requisition Status"></asp:Label>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble" style="width:20%">
              <asp:Label EnableViewState="false" ID="lblJobStatus" runat="server" Text="Change Req Status"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:DropDownList ID="ddlJobStatus" runat="server" CssClass="CommonDropDownList" TabIndex ="1"
                >
            </asp:DropDownList>
            <asp:Button ID="btnChangeJobStatus" CssClass="CommonButton" runat="server" Text="Save" TabIndex ="2" OnClick="btnChangeJobStatus_Click"   ValidationGroup ="re" />
        </div>
    </div>
    <div class="CommonHeader">
        <asp:Label EnableViewState="false" ID="lblHiringTeamHeader" runat="server" Text="Assigned Recruiters and Hiring Team"></asp:Label>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble" style="width:20%">
            <asp:Label EnableViewState="false" ID="lblRecruiters" runat="server" Text="Select Recruiter"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:ListBox ID="lstRecruiters" runat="server" SelectionMode="multiple" TabIndex ="3" CssClass="CommonDropDownList" Width="160px">
            </asp:ListBox>
            <asp:Button ID="btnAddRecruiters" CssClass="CommonButton" runat="server" Text="Add" OnClick="btnAddRecruiters_Click" TabIndex ="4" ValidationGroup ="RecruiterValidation"/>
        </div>
    </div>
    
    <%-- <div class ="TableRow">
        <div  class ="TableFormValidatorContent" style=" margin-left : 19.7%;">
            <asp:CustomValidator ID="cvRecruiters" runat ="server" EnableViewState ="true"  ClientValidationFunction ="Validate" ValidationGroup ="RecruiterValidation" Display ="Dynamic" ErrorMessage ="Select at least one member to add." ></asp:CustomValidator>
           <%-- <asp:RequiredFieldValidator ID ="RequiredFieldValidator1" runat ="server" Display ="Dynamic"  ErrorMessage ="Please Add Notes." ControlToValidate ="txtAdditionalNote" ValidationGroup ="NoteValidation"></asp:RequiredFieldValidator> --%>
       <%--  </div>
    </div>--%>
    <div class="TableRow">
        <div class="TableFormLeble" style="width:19.6%;float:left">
            <asp:Label EnableViewState="false" ID="lblAssignedTeam" runat="server" Text="Assigned Team"></asp:Label>:
        </div>
        <div class="TableFormContent" style="float:left;vertical-align:top;width:50%">
            <asp:ListView ID="lsvAssignedTeam" runat="server" DataKeyNames="Id" OnItemDataBound="lsvAssignedTeam_ItemDataBound" 
                   OnItemCommand="lsvAssignedTeam_ItemCommand">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th style="width: 10%; white-space: nowrap;">
                                Date Assigned
                            </th>
                            <th style="width: 40%; white-space: nowrap;">
                                Name
                            </th>
                            <th style="width: 40%; white-space: nowrap;">
                                Email
                            </th>                            
                            <th style="text-align: center; white-space: nowrap; width: 10%;">
                                Action
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                        <tr>
                            <td>
                                No recruiters assigned.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:Label ID="lblAssignedDate" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblMemberName" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblMemberEmail" runat="server" />
                        </td>
                        <td style="text-align: center;">
                            <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" TabIndex ="5" CommandName="DeleteItem">
                            </asp:ImageButton>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>        
    </div>
    <div class="CommonHeader" style="clear:both;">
        <asp:Label EnableViewState="false" ID="lblNoteHeader" runat="server" Text="Notes"></asp:Label>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble" style="width:20%">
              <asp:Label EnableViewState="false" ID="lblAddNote" runat="server" Text="Add Note"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox ID="txtAdditionalNote" runat="server" CssClass="CommonTextBox" TabIndex ="6" TextMode="MultiLine" Rows="5" Width="350px" ValidationGroup ="NoteValidation" ></asp:TextBox>
            <asp:Button ID="btnSaveNote" CssClass="CommonButton" runat="server" Text="Save" TabIndex ="7" OnClick="btnSaveNote_Click" ValidationGroup ="NoteValidation" />
        </div>
    </div>
    <div class ="TableRow">
        <div  class ="TableFormValidatorContent" style=" margin-left : 19.7%;">
            <asp:RequiredFieldValidator ID ="rfvNotes" runat ="server" Display ="Dynamic"  ErrorMessage ="Please Add Notes." ControlToValidate ="txtAdditionalNote" ValidationGroup ="NoteValidation"></asp:RequiredFieldValidator> 
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble" style="width:19.6%;float:left">
            <asp:Label EnableViewState="false" ID="lblAddedNote" runat="server" Text="Notes"></asp:Label>:
        </div>
        <div class="TableFormContent" style="text-align:left;width:50%;float:left;">
            <asp:ListView ID="lsvAddedNote" runat="server" DataKeyNames="Id"  OnItemDataBound="lsvAddedNote_ItemDataBound" >
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th style="width: 20%; white-space: nowrap;">
                                Date & Time
                            </th>
                            <th style="width: 20%; white-space: nowrap;">
                                Added By
                            </th>                            
                            <th style="width: 40%; white-space: nowrap;">
                                Note
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                        <tr>
                            <td>
                                No notes added.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:Label ID="lblDateTime" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblAddedBy" runat="server"  />
                        </td>
                        <td>
                            <asp:Label ID="lblNote" runat="server" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
    
     <div class="CommonHeader"  style="clear:both;">
        <asp:Label EnableViewState="false" ID="lblHiringLog" runat="server" Text="Hiring Log"></asp:Label>
    </div>
    <div class="TableRow">
        <div style =" padding-top :5px ; padding-bottom : 10px">
<div align="left" ></div>
<ucl:HiringLog ID ="uclHiringLog" runat ="server" />
<%--<asp:ObjectDataSource ID="odsEventLog" runat="server" SelectMethod="GetPaged"
        TypeName="TPS360.Web.UI.EventLogDataSource" SelectCountMethod="GetListCount"
        EnablePaging="True" SortParameterName="sortExpression">
        <SelectParameters>
            <asp:Parameter Name="JobPostingId" DefaultValue="0" />
            <asp:Parameter Name ="CandidateId" DefaultValue ="0" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:UpdatePanel ID ="upHiringLog" runat ="server" UpdateMode ="Conditional" >
    <ContentTemplate >
    <asp:Button ID="btnExport" runat ="server" Text ="Export" CssClass ="CommonButton" OnClick ="btnExport_Click" />
   <asp:HiddenField ID ="hdnSortColumn" runat ="server" />
   <asp:HiddenField ID ="hdnSortOrder" runat ="server" />
<asp:ListView ID="lsvHiringLog" runat="server" DataKeyNames="Id" DataSourceID ="odsEventLog" 
                OnItemDataBound="lsvHiringLog_ItemDataBound" OnItemCommand="lsvHiringLog_ItemCommand" OnPreRender ="lsvHiringLog_PreRender">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th style="width: 20%; white-space: nowrap;">
                                <asp:LinkButton ID="btnDate" runat="server" Text="Date & Time" CommandName="Sort" CommandArgument="[EL].[ActionDate]" />
                            </th>
                           <th style="width: 20%; white-space: nowrap;">
                                <asp:LinkButton ID="btnUser" runat="server" Text="User" CommandName="Sort" CommandArgument="[M].[FirstName]" />
                            </th>
                            <th style="width: 20%; white-space: nowrap;">
                                <asp:LinkButton ID="btnAction" runat="server" Text="Action" CommandName="Sort" CommandArgument="[EL].[ActionType]" />
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="3">
                                <ucl:Pager Id="pagerControl" runat ="server" EnableViewState ="true"  />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                        <tr>
                            <td>
                                No data was returned.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td> <asp:Label ID="lblDate" runat="server" /> </td>
                        <td><asp:Label ID="lblUser" runat="server" /> </td>
                        <td> <asp:Label ID="lblAction" runat="server"/> </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
   </ContentTemplate>
   <Triggers >
   <asp:PostBackTrigger  ControlID ="btnExport" />
   </Triggers>
    </asp:UpdatePanel>--%>
</div>
    </div>
    
    
    
    <div class="CommonHeader" style="clear:both;">
        <asp:Label EnableViewState="false" ID="lblTimeToHireHeader" runat="server" Text="Time To Hire"></asp:Label>
    </div>
    <div class="TableRow" style="text-align:left;">                                        
        <asp:Label EnableViewState="true" ID="lblTimeToHire" runat="server" Text="" ></asp:Label>                                        
    </div>
</div>
