﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RequisitionHiringMatrix.ascx
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Dec-24-2008          Gopala Swamy          Defect id: 9603;  I made visibility changes to false.
    0.2             Mar-24-2009          Jagadish              Defect id: 10200; Changes made in javascript function 'SelectUnSelectAllApplicant'.
    0.3             Apr-02-2009          Jagadish              Defect id: 9614;  Added javascript function 'RemoveCandidate'.
    0.4             Apr-09-2009          Sandeesh              Defect id :10293 Removed the string 'btnSubmitToClient'.
    0.5             May-06-2009          Yogeesh Bhat          Defect Id: 10437, 10438 Changes made in SelectUnSelectAllApplicant(), SelectUnselectHeaderCheckbox().
    0.6             May-06-2009          N.Srilakshmi          Defect Id: 10430; Removed lblLevel1RemarksHeader,lblLevel2RemarksHeader,lblLevel3RemarksHeader.
    0.7             May-08-2009          Sandeesh              Defect Id :10424  Added javascript function 'SelectCandidate'.
    0.8             May-15-2009          Veda                  Defect Id: 10435, Instead of having a whole new window open when a "Remarks" link is clicked, a small window should appear with the remarks text box.
    0.9             May-18-2009          Jagadish              Defect Id: 10474; Changes made in javascript functions 'SelectCandidate()' and 'RemoveCandidate(strMsg)'.
    1.0             May-27-2009          Veda                  Defect Id: 10435, Instead of having a whole new window open when a "Remarks" link is clicked, a small (ajax) popup should appear with the remarks text box.
    1.1             Jun-08-2009          Veda                  Defect Id: 10535, Displyaing all three level remarks and added onclientclick for save button
    1.2             Jul-01-2009          Veda                  Defect Id: 10832, Displyaing remarks in table format.
    1.3             Aug-19-2009         Nagarathna V.B         Defect Id:10496 :added "SelectCandidateforSubmit()" it is called in submit to client button.
    1.4             March-03-2010       Basavaraj A            Defect Id:10954 :Changes h   ave been implemented As Data was not displayed consistently between the 'Level I/II/III' remarks.   
-------------------------------------------------------------------------------------------------------------------------------------------

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionHiringMatrix.ascx.cs"
    Inherits="TPS360.Web.UI.cltRequisitionHiringMatrix" EnableViewState="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="Web" Namespace="WebChart" Assembly="WebChart" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/HiringDetails.ascx" TagName="HiringDetails" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RejectDetails.ascx" TagName="RejectCandidate" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/SubmissionDetails.ascx" TagName="SubmissionDetails"
    TagPrefix="ucl" %>
<%@ Register Src="~/Controls/JoiningDetails.ascx" TagName="JoiningDetails" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/HiringNotes.ascx" TagName="HiringNotes" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/CandidateActionLog.ascx" TagName="ActionLog" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RejectCandidateList.ascx" TagName="Rejection" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ChangeReqStatus.ascx" TagName="ChangeStatus" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RequisitionAssignedManagers.ascx" TagName="Managers"
    TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RequisitionNotes.ascx" TagName="Notes" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/SubmitToClient.ascx" TagName="SubmitToClient" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ModalTemplate.ascx" TagName="Template" TagPrefix="ucl" %>

<script src="../Scripts/RequisitionHiringMatrix.js" type="text/javascript"></script>

<script src="../js/AjaxScript.js" type="text/javascript"></script>

<script src="../js/AjaxModal/Main.js" type="text/javascript"></script>

<style type="text/css">
    .ContextExpand
    {
        width: 27px;
        height: 19px;
    }
    .ExpandImg
    {
        width: 16px;
        height: 16px;
        background-image: url(../Images/expand-plus.png);
        src: url(../Images/expand-plus.png);
    }
    .CollapseImg
    {
        width: 16px;
        height: 16px;
        background-image: url(../Images/collapse-minus.png);
        src: url(../Images/collapse-minus.png);
    }
    .AutoCompleteCompletionItem
    {
        font-size: small;
    }
    .BulkActionContext
    {
        cursor: pointer;
        position: absolute;
    }
    .BulkActionContext_List
    {
        display: block;
        top: 0px;
        clear: both;
        font-weight: bold;
        margin: 0;
        margin-left: 113px;
        padding: 0;
        border: 1px solid #4291df;
        background-color: #4291df;
        position: relative;
        width: 51px;
    }
    .BulkActionContext_List li
    {
        margin-left: -114px;
        position: relative;
        clear: both;
        padding-left: 8px;
        padding-bottom: 20px;
        padding-top: 7px;
        color: #555555;
        border-bottom: 1px solid #FCFCFC;
        border-top-style: none;
        border-top-width: 0px;
        border-bottom-color: #4291df;
        border-left-color: #4291df;
        border-right-color: #4291df;
        margin-top: 0px;
        z-index: 9999;
        background-color: #EFF6FF;
    }
    .BulkActionContext_List li:hover
    {
        border-top: none;
        border-color: #4291df;
    }
    .BulkActionContext #icon
    {
        padding: 0 7px;
        position: relative;
        z-index: 9999;
        float: left;
        border-width: 1px;
        border-style: solid;
        border-color: #4291df #4291df #4291df;
        top: 1px;
        clear: both;
        background-color: #EFF6FF;
        border-bottom-style: none;
        display: block;
        height: 20px;
        line-height: 23px;
        text-align: center;
        margin: 2px 2px 0 0;
        padding-bottom: 6px;
    }
    .BulkActionContext #icon:hover
    {
        position: relative;
        border-top: none;
        border-color: #4291df;
        margin-top: 3px;
    }
    .BulkActionContext a
    {
        color: #1F1FFF;
        text-decoration: none;
    }
    .BulkActionContext a:hover
    {
        color: #0000ff;
        text-decoration: none;
    }
    img
    {
        border: none;
    }
</style>

<script type="text/javascript">
    function pageLoad() {

        var mpe = $find("MPEStatus");
        mpe.add_shown(onShownStatus);
        var MPENotes = $find("MPENotes");
        MPENotes.add_shown(onShownNotes);
        var MPManagers = $find("MPEManagers");
        MPManagers.add_shown(onShownManagers);
        var MPEAction = $find("MPEAction");
        MPEAction.add_shown(onShownMPEAction);

        var MPEReject = $find("MPEReject");
        MPEReject.add_shown(onShownMPEReject);
        var MPEHiring = $find("MPEHiring");
        MPEHiring.add_shown(onShownMPEHiring);
        var MPESubmission = $find("MPESubmission");
        MPESubmission.add_shown(onShownMPESubmission);
        var MPEJoin = $find("MPEJoin");
        MPEJoin.add_shown(onShownMPEJoin);
    }
    function onShownMPEReject() {
        var background = $find("MPEReject")._backgroundElement;
        background.onclick = function() { $find("MPEReject").hide(); }
    }
    function onShownMPEHiring() {
        var background = $find("MPEHiring")._backgroundElement;
        background.onclick = function() { $find("MPEHiring").hide(); }
    }
    function onShownMPESubmission() {
        var background = $find("MPESubmission")._backgroundElement;
        background.onclick = function() { $find("MPESubmission").hide(); }
    }
    function onShownMPEJoin() {
        var background = $find("MPEJoin")._backgroundElement;
        background.onclick = function() { $find("MPEJoin").hide(); }
    }



    function onShownMPEAction() {
        var background = $find("MPEAction")._backgroundElement;
        background.onclick = function() { $find("MPEAction").hide(); }

    }
    function onShownStatus() {

        var background = $find("MPEStatus")._backgroundElement;
        background.onclick = function() { $find("MPEStatus").hide(); }
    }
    function onShownNotes() {
        var background = $find("MPENotes")._backgroundElement;
        background.onclick = function() { $find("MPENotes").hide(); }
    }
    function onShownManagers() {
        var background = $find("MPEManagers")._backgroundElement;
        background.onclick = function() { $find("MPEManagers").hide(); }
    }

    function DropdownChange(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function HideContextMenu(divId) {
        if (n == 0) {
            var reqDiv = document.getElementById(divId);
        }
        if (reqDiv != null) reqDiv.style.display = "none";
    }
    function DropdownClicked(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }

    function ShowDiv(divId) {
        n = 10;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function ShowReqDiv(divId) {

        var a = document.getElementById("divReqs");
        var b = document.getElementById("divHotListdiv");
        if (a != null) a.style.display = "none";
        if (b != null) b.style.display = "none";

        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) {
            $(reqDiv).closest('.btn-group').addClass('open');
            reqDiv.style.display = "";
            reqDiv.parentElement.className = "btn-group open";
        }
        return false;
    }
    function ShowOrHide(v, img) {
        var tdDetail = document.getElementById(v);
        var image = document.getElementById(img);
        if (tdDetail.style.display == "none") {
            tdDetail.style.display = "";
            image.src = "../Images/collapse-minus.png";
        }
        else {
            tdDetail.style.display = "none"; image.src = "../Images/expand-plus.png";
        }

    }


    /*
    $(document).ready(
    function(){
    $('#lblSubmit').click(
    function(){
    $('#divOuter').hide({direction:'left'},500);
    $('#divSubmitToClient').show({direction:'right'},500);   
                
    });
        
        
    $('#btnbackToHiringMatrix').click(
    function(){
    $('#divSubmitToClient').hide({direction:'right'},500);
    $('#divOuter').show({direction:'left'},500);  
    });
    });
  
       */
    function CloseExtenderPopup(id) {
        var s = $find(id);
        s.hide();
    }
</script>

<ajaxToolkit:ModalPopupExtender ID="mpeChangeStatus" BehaviorID="MPEStatus" runat="server"
    BackgroundCssClass="DarkModalBackground" DropShadow="false" PopupControlID="pnlChangeStatus"
    TargetControlID="lblTargetChangeStatus" OnCancelScript="closepopups()">
</ajaxToolkit:ModalPopupExtender>
<ajaxToolkit:ModalPopupExtender ID="mpeManagers" BehaviorID="MPEManagers" runat="server"
    BackgroundCssClass="DarkModalBackground" DropShadow="false" PopupControlID="pnlManagers"
    TargetControlID="lblTargetManagers" OnCancelScript="closepopups()">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="pnlChangeStatus" runat="server" Style="display: none">
    <asp:Label ID="lblTargetChangeStatus" runat="server"></asp:Label>
    <ucl:Template ID="uclTemplate" runat="server" ContentDisplay="table-cell" ModalBehaviourId="MPEStatus"
        ContentWidth="500px" ContentHeight="200px">
        <contents>
            <ucl:ChangeStatus runat="server" ID="uclChangeStatus" />
        </contents>
    </ucl:Template>
</asp:Panel>
<asp:UpdatePanel ID="upreNotes" runat="server">
    <contenttemplate>
        <ajaxToolkit:ModalPopupExtender ID="mpeNotes" BehaviorID="MPENotes" runat="server"
            BackgroundCssClass="DarkModalBackground" DropShadow="false" PopupControlID="pnlNotes"
            TargetControlID="lblTargetNotes" OnCancelScript="closepopups()">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel ID="pnlNotes" runat="server" Style="display: none">
            <asp:Label ID="lblTargetNotes" runat="server"></asp:Label>
            <ucl:Template ID="uclTemplateNotes" runat="server" ContentDisplay="table-cell" ContentWidth="650px"
                ContentHeight="400px" ModalBehaviourId="MPENotes">
                <Contents>
                    <ucl:Notes runat="server" ID="uclNotes" />
                </Contents>
            </ucl:Template>
        </asp:Panel>
    </contenttemplate>
</asp:UpdatePanel>
<asp:Panel ID="pnlManagers" runat="server" Style="display: none">
    <asp:Label ID="lblTargetManagers" runat="server"></asp:Label>
    <ucl:Template ID="uclTemplateManagers" runat="server" ContentDisplay="table-cell"
        ContentWidth="600px" ContentHeight="200px" ModalBehaviourId="MPEManagers">
        <contents>
            <ucl:Managers runat="server" ID="uclManagers" />
        </contents>
    </ucl:Template>
</asp:Panel>
<style type="text/css">
    div.relevance-container
    {
        border: 1px solid #ccc;
        min-width: 60px;
        margin: 2px 5px 2px 0;
        padding: 1px;
        float: left;
        background: white;
    }
    div.relevance-container div
    {
        background-color: #D0E1FF;
        height: 20px;
    }
    div.relevance-container span
    {
        min-width: 60px;
        text-align: center;
        float: left;
        font: normal 12px Arial,sans-serif;
        margin-top: 3px;
    }
</style>

<script type="text/javascript">
    function ClosePopUP() {
        var mo = document.getElementById('<%=hdnmodal.ClientID %>');

        mo.value = "";
    }
    function closepopups() {
        var mo = document.getElementById('<%=hdnmodal.ClientID %>');

        mo.value = "";
    }

    function EmptyHiddenReject() {
        var hdnr = document.getElementById('<%=hdnmodal.ClientID %>');
        hdnr.value = "";


    }
    var n = 0;
    function monitorClick(e) {
        try {
            var evt = (e) ? e : event;

            var theElem = (evt.srcElement) ? evt.srcElement : evt.target;

            while (theElem != null) {
                if (theElem.id.indexOf('imgContextMenu') < 0) {

                    if (theElem.id.indexOf('divReqs') < 0 && theElem.id.indexOf('ctl00_cphHomeMaster_ucRequisitionHiringMatrixNavigationTopMenu_uclHiringMatrix_ddlLevels') < 0 && theElem.id.indexOf('refReq') < 0) {
                        var divReqs = document.getElementById('divReqs');
                        if (divReqs != null) {
                            $(divReqs).closest('.btn-group').removeClass('open');
                            divReqs.style.display = "none";
                        }
                    }

                    var alldivs = document.getElementById('ctl00_cphHomeMaster_ucRequisitionHiringMatrixNavigationTopMenu_uclHiringMatrix_hdnDivContextMenuids').value.split(';');
                    for (var m = 0; m < alldivs.length; m++) {
                        var cm = document.getElementById(alldivs[m]);
                        if (cm != null) cm.style.display = "none";
                    }
                    return true;
                }
                else {

                    n = 10;
                    return true;

                }




                theElem = theElem.offsetParent;




            }
        }
        catch (e) {
        }
        return true;
    }
    document.onclick = monitorClick;




    function ListItemClick(control) {
        document.getElementById(control).style.display = "none";
    }
    function ShowContextMenu(control) {
        n = 10;
        document.getElementById(control).style.display = "";
    }
    function ShowOrHideContext(param1, param2, divContext, imgContextMenu) {
        n = 10;

        var alldivs = document.getElementById('ctl00_cphHomeMaster_ucRequisitionHiringMatrixNavigationTopMenu_uclHiringMatrix_hdnDivContextMenuids').value.split(';');

        for (var m = 0; m < alldivs.length; m++) {
            var cm = document.getElementById(alldivs[m]);
            if (cm != null) cm.style.display = "none";
        }

        document.getElementById(imgContextMenu).style.visibility = "hidden";

        ShowOrHide(param1, param2);

        var divContext = document.getElementById(divContext);

        divContext.style.display = "";

    }
    function onLeaveContextMenu(control) {//HideContextMenu(control );

        n = 0;
        t = setTimeout('HideContextMenu ("' + control + '");', 1000);

    }
    function HideContextMenu(control) {
        var v = document.getElementById(control);
        if (n == 0) v.style.display = "none";

    }
    function ShowOrHideContextMenu(control, operation, divId) {
        var con = document.getElementById(control);
        if (document.getElementById(divId).style.display == "none") {
            con.style.visibility = operation;
        }
    }
    function SelectRowCheckbox(param1, param2) {
        // debugger;
        var ischeckall = true;
        var HeaderCheckbox = document.getElementById('chkAllItem');
        var elements = HeaderCheckbox.form.elements;

        for (var i = 0; i < elements.length; i++) {
            if ((elements[i].type == 'checkbox') && (elements[i].id != HeaderCheckbox.id)) {
                if (elements[i].checked == true) {
                    var hndId = elements[i + 1];
                    if (IsNumeric(hndId.value)) {
                        ADDID(hndId.value, 'ctl00_cphHomeMaster_ucRequisitionHiringMatrixNavigationTopMenu_uclHiringMatrix_txtSelectedIds');
                    }

                }
                else {
                    var hndId = elements[i + 1];
                    if (IsNumeric(hndId.value)) {
                        RemoveID(hndId.value, 'ctl00_cphHomeMaster_ucRequisitionHiringMatrixNavigationTopMenu_uclHiringMatrix_txtSelectedIds');
                    }
                }
            }
        }
        HeaderCheckbox.checked = ischeckall;
        // alert (document .getElementById ('ctl00_cphHomeMaster_ucRequisitionHiringMatrixNavigationTopMenu_uclHiringMatrix_txtSelectedIds').value);
    }


    function IsNumeric(input) {
        return (input - 0) == input && input.length > 0;
    }



    function SelectAllCheckbox(HeaderCheckbox) {

        // debugger;
        var isChecked = HeaderCheckbox.checked;
        var elements = HeaderCheckbox.form.elements;

        for (var i = 0; i < elements.length; i++) {
            if ((elements[i].type == 'checkbox') && (elements[i].id != HeaderCheckbox.id)) {
                if (!elements[i].disabled) {
                    if (elements[i].checked != isChecked) {
                        elements[i].checked = isChecked;
                        if (isChecked == true) {
                            var hndId = elements[i + 1];
                            if (IsNumeric(hndId.value) && hndId.value != "0")
                                ADDID(hndId.value, 'ctl00_cphHomeMaster_ucRequisitionHiringMatrixNavigationTopMenu_uclHiringMatrix_txtSelectedIds');

                        }
                        else {
                            var hndId = elements[i + 1];
                            if (IsNumeric(hndId.value) && hndId.value != "0")
                                RemoveID(hndId.value, 'ctl00_cphHomeMaster_ucRequisitionHiringMatrixNavigationTopMenu_uclHiringMatrix_txtSelectedIds');

                        }


                    }
                }
            }
        }
    }
    function IsNumeric(sText) {
        var ValidChars = "0123456789.";
        var IsNumber = true;
        var Char;


        for (i = 0; i < sText.length && IsNumber == true; i++) {
            Char = sText.charAt(i);
            if (ValidChars.indexOf(Char) == -1) {
                IsNumber = false;
            }
        }
        return IsNumber;

    }
    function ShowHideAllDetail(HeaderImage) {
        var image = HeaderImage.src;
        var expand;
        if (image.indexOf("expand-plus.png") >= 0) {
            expand = true;
            HeaderImage.src = "../Images/collapse-minus.png";
        }
        else {
            expand = false;
            HeaderImage.src = "../Images/expand-plus.png";
        }
        var DetailArray = document.getElementById('ctl00_cphHomeMaster_ucRequisitionHiringMatrixNavigationTopMenu_uclHiringMatrix_hdnDetailRowIds').value.split(',');
        for (var i = 0; i < DetailArray.length; i++) {
            var arr = DetailArray[i].split(';');
            var row = document.getElementById(arr[0]);
            var img = document.getElementById(arr[1]);
            if (row != null && img != null) {
                img.src = HeaderImage.src;
                if (expand) row.style.display = "";
                else row.style.display = "none";
            }
        }
    }

    function ShowOrHide(v, img) {



        var tdDetail = document.getElementById(v);

        var image = document.getElementById(img);

        if (tdDetail != null && tdDetail.style.display == "none") {
            tdDetail.style.display = "";

            image.src = "../Images/collapse-minus.png";

        }
        else {
            tdDetail.style.display = "none"; image.src = "../Images/expand-plus.png";
        }

        CheckAllRow();


    }
    function CheckAllRow() {
        var DetailArray = document.getElementById('ctl00_cphHomeMaster_ucRequisitionHiringMatrixNavigationTopMenu_uclHiringMatrix_hdnDetailRowIds').value.split(',');
        var allExpand = true;
        var AllCollapsed = true;
        for (var i = 0; i < DetailArray.length; i++) {
            var arr = DetailArray[i].split(';');
            var row = document.getElementById(arr[0]);
            if (row != null) {
                if (row.style.display == "none") {
                    allExpand = false;
                }
                else {
                    AllCollapsed = false;
                }
            }

        }

        var headerimg = document.getElementById('ctl00_cphHomeMaster_ucRequisitionHiringMatrixNavigationTopMenu_uclHiringMatrix_lsvCandidateList_imgShowHide');
        if (allExpand) headerimg.src = "../Images/collapse-minus.png";
        if (AllCollapsed) headerimg.src = "../Images/expand-plus.png";
    }
    function DisplayErrorMessage() {
        var SelecedId = document.getElementById('<%=txtSelectedIds.ClientID %>');

        if (SelecedId.value == "") {
            ShowRedMessage("ctl00_cphHomeMaster_ucRequisitionHiringMatrixNavigationTopMenu_uclHiringMatrix_lsvCandidateList_pagerControl_lblMessage", "Please select at least one candidate");
            return false;
        }
        else
        { return true; }
    }
    function Status_OnChange(s, p) {
        var status = document.getElementById(s);
        var hf = document.getElementById(p);
        hf.value = status.options[status.selectedIndex].value;
    }
    function HighligthselectedItem(item) {

        item.className = "active level";

    }
</script>

<asp:UpdatePanel ID="up" runat="server">
    <contenttemplate>
        <ajaxToolkit:AlwaysVisibleControlExtender ID="AjaxVisibleControl" TargetControlID="lblMessage"
            runat="server">
        </ajaxToolkit:AlwaysVisibleControlExtender>
        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
    </contenttemplate>
</asp:UpdatePanel>
<div id="divOuter">
    <asp:HiddenField ID="hdncurretn" runat="server" />
    <asp:HiddenField ID="hdnCurname" runat="server" />
    <asp:HiddenField ID="hdnDivContextMenuids" runat="server" />
    <asp:HiddenField ID="hdnmodal" runat="server" Value="" />
    <asp:HiddenField ID="hdnmodalreject" runat="server" Value="" />
    <asp:HiddenField ID="hdnMoving" runat="server" />
    <ucl:Confirm ID="uclConfirm" runat="server">
    </ucl:Confirm>
    <%--    <div class="CommonHeader" >
        <asp:Label EnableViewState="false" ID="lblBriefJobDetailsHeader" runat="server" Text="Brief Requisition Details"></asp:Label>
    </div>
    <div style =" text-align :left ">
        <div style="float: left; width: 32%">
            <asp:Label EnableViewState="false" ID="lblJobTitleHeader" runat="server" Text="Job Title"></asp:Label>:
            <asp:Label  ID="lblJobTitle" runat="server" Text="" EnableViewState ="true" ></asp:Label>
        </div>
        <div style="float: left; width: 32%">
          <asp:Label EnableViewState="false" ID="lblJobLocationHeader" runat="server" Text="Location"></asp:Label>:
          <asp:Label  ID="lblJobLocation" runat="server" Text="" EnableViewState ="true" ></asp:Label>
        </div>
        <div style="float: left; width: 32%">
            <asp:Label EnableViewState="false" ID="lblJobYearOfExpHeader" runat="server" Text="Years of Exp"></asp:Label>:
            <asp:Label  ID="lblJobYearOfExp" runat="server" Text="" EnableViewState ="true" ></asp:Label>
        </div>
    </div>
    <div style="clear: both; text-align :left ">
        <div style="float: left; width: 32%">
            <asp:Label EnableViewState="false" ID="lblJobEducationHeader" runat="server" Text="Education"></asp:Label>:
            <asp:Label ID="lblJobEducation" runat="server" Text="" EnableViewState ="true" ></asp:Label>
        </div>
        <div style="float: left; width: 32%">
            <asp:Label EnableViewState="false" ID="lblJobSalaryHeader" runat="server" Text="Salary"></asp:Label>:
            <asp:Label  ID="lblJobSalary" runat="server" Text="" EnableViewState ="true" ></asp:Label>
        </div>
        <div style="float: left; width: 32%">
            <asp:Label EnableViewState="false" ID="lblSkillsHeader" runat="server" Text="Skills"></asp:Label>:
            <asp:Label ID="lblSkills" runat="server" Text="" EnableViewState ="true" ></asp:Label>
        </div>
    </div>
    <div id="divButtons" runat="server" style="clear: both; padding-bottom: 10px; padding-top: 10px;" align="left" >
    
        <asp:DropDownList ID="ddlLevels" runat ="server" CssClass ="CommonDropDownList"></asp:DropDownList>
       <asp:Button ID="btnMoveToNextLevel" runat="server" Text ="Change Status" CssClass="CommonButton"  EnableViewState ="true" OnClick="btnMoveToNextLevel_Click"  CausesValidation="false" UseSubmitBehavior="False" OnClientClick ="if(!DisplayErrorMessage()) return false;"/>
       <%--  <div  style =" display :none "> <asp:Button ID="btnRemoveFromThisLevel" runat="server" CssClass="CommonButton" Text="Move to Previous Level" OnClick="btnRemoveFromThisLevel_Click" CausesValidation="false" Width="145px" UseSubmitBehavior="False"/></div>
        <asp:Button ID="btnRemove" runat="server" CssClass="CommonButton" Text="Reject" OnClick="btnRemove_Click" CausesValidation="false" Width="145px" UseSubmitBehavior="False" OnClientClick ="if(!DisplayErrorMessage()) return false;"/>&nbsp; &nbsp;
        <asp:Button ID="btnEmail" runat ="server" CssClass ="CommonButton" Text ="Email Requisition" CausesValidation ="true" OnClick ="btnEmail_Click" Width ="145px" OnClientClick ="if(!DisplayErrorMessage()) return false;"/>
        <asp:Button ID="btnSubmitToClient" runat="server" CssClass="CommonButton" Text="Submit to Client/Vendor" OnClick="btnSubmitToClient_Click"   CausesValidation="false" Width="150px" Visible="false" UseSubmitBehavior="False" OnClientClick ="if(!DisplayErrorMessage()) return false;" /> 
    </div>--%>
    <%--     Hidden Fields--%>
    <asp:HiddenField ID="hdnChkBox" runat="server" Value="" />
    <asp:HiddenField ID="hdnIsHide" runat="server" />
    <asp:HiddenField ID="hdnDetailRowIds" runat="server" />
    <asp:HiddenField ID="hdnRemove" runat="server" />
    <div style="display: none">
        <asp:TextBox ID="lblCurrentJobPostingID" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtSelectedIds" runat="server" EnableViewState="true"></asp:TextBox></div>
    <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
    <asp:HiddenField ID="hdnNextLevelName" runat="server" />
    <asp:HiddenField ID="hdnPreviousLevelName" runat="server" />
    <div style="text-align: left;">
        <asp:HiddenField ID="hdnPageTitle" runat="server" />
        <asp:HiddenField ID="hdnCount" runat="server" Value="0" />
        <asp:HiddenField ID="hdnRejectCount" runat="server" />
        <div id="Body">
            <%--<div id="BodyLeft">
				</div>
				<div id="BodyRight">
				</div>--%>
            <div id="BodyMiddle" style="width: 99%">
                <div id="HMatrixContainer" style="height: 100%;">
                    <div id="LeftSidebar" style="float: left; width: 13%;">
                        <div class="HMatrixLeft" style="width: 100%;">
                            <div class="header">
                                <span class="title">Status Levels</span>
                            </div>
                            <asp:ListView ID="lvNavigationItems" runat="server" OnItemDataBound="lvNavigationItems_ItemDataBound">
                                <layouttemplate>
                                    <div class="content">
                                        <div class="levels">
                                            <li id="itemPlaceholder" runat="server" />
                                        </div>
                                    </div>
                                </layouttemplate>
                                <itemtemplate>
                                    <asp:LinkButton ID="lnkLeftMenuItem" runat="server" CssClass="level" OnClick="lnkLeftMenuItem_Click"
                                        Height="100%" Width="100%">
                                        <div id="dvspanName" runat="server" style="padding: 0px  0px 0px 10px">
                                            <span class="name" id="spName" runat="server" style="width: 60%; float: left;"></span>
                                        </div>
                                        <div style="vertical-align: middle; float: right; width: 30%; padding-right: 5px">
                                            <span class="count" id="spCount" runat="server"></span>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:HiddenField ID="hnSelectedLevel" runat="server" />
                                </itemtemplate>
                            </asp:ListView>
                        </div>
                        <div id="HMatrixOptions" class="HMatrixLeft" style="margin-top: 15px; margin-bottom: 30px;
                            width: 100%;">
                            <div class="header">
                                <span class="title">Options</span>
                            </div>
                            <div class="content">
                                <div class="levels">
                                    <asp:LinkButton ID="lnkReqStatus" runat="server" CssClass="level" OnClick="lnkReqStatus_Click"
                                        CommandArgument="ChangeStatus" Height="100%" Width="100%">
                        <span class="name" style ="white-space :normal ; width :60%; padding-left : 10px;">Change Req Status</span>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkNotes" runat="server" CssClass="level" OnClick="lnkReqStatus_Click"
                                        CommandArgument="Notes" Height="100%" Width="100%">
                        <span class="name" style ="white-space :normal ; width :60%; padding-left : 10px;">Notes</span>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkAssigned" runat="server" CssClass="level" OnClick="lnkReqStatus_Click"
                                        CommandArgument="AssignedManagers" Height="100%" Width="100%">
                        <span class="name" style ="white-space :normal ; width :60%; padding-left : 10px;">Assigned Team</span>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkHiringLog" runat="server" CssClass="level" OnClick="lnkReqStatus_Click"
                                        CommandArgument="HiringLog" Height="100%" Width="100%">
                        <span class="name" style ="white-space :normal ; width :60%; padding-left : 10px;">Hiring Log</span>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="HMatrixContent" style="overflow: auto; vertical-align: top; float: left;
                        width: 87%; position: relative;">
                        <div class="header">
                            <asp:HyperLink ID="lnkReqTitle" runat="server" CssClass="title" Target="_blank"></asp:HyperLink>
                        </div>
                        <div class="content">
                            <div id="dvbulkAction" class="btn-toolbar" runat="server" style="padding-left: 6px;">
                                <div class="pull-left">
                                    <div class="btn-group">
                                        <a href="Javascript:void(0)" onclick="ShowReqDiv('divReqs')" id="refReq" class="btn btn-medium dropdown-toggle">
                                            Change Status <span class="caret"></span></a>
                                        <ul class="dropdown-menu-custom" id="divReqs" style="display: none">
                                            <li style="width: 150px; text-align: left; padding-top: 10;" id="list">
                                                <asp:DropDownList ID="ddlLevels" runat="server" CssClass="CommonDropDownList" Width="150px"
                                                    onclick="DropdownClicked('divReqs')" onchange="DropdownChange('divReqs')" OnSelectedIndexChanged="ddlLevels_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="btn-group" runat="server" id="liEmail">
                                        <asp:LinkButton ID="lnkEmailReq" runat="server" Text="Email Requisition" OnClick="btnEmail_Click"
                                            CssClass="btn"></asp:LinkButton>
                                    </div>
                                    <div class="btn-group" runat="server" id="liSubmit">
                                        <asp:LinkButton ID="lnkSubmit" runat="server" Text="Submit" OnClick="btnSubmitToClient_Click"
                                            CssClass="btn"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="CandidateGrid">
                                <div id="dvhirng" runat="server">
                                    <div style="clear: both; padding: 0px 5px 5px 5px" id="divList">
                                        <asp:ObjectDataSource ID="odsHiringMatrix" runat="server" SelectMethod="GetPaged"
                                            TypeName="TPS360.Web.UI.HiringMatrixDataSource" SelectCountMethod="GetListCount"
                                            EnablePaging="True" SortParameterName="sortExpression">
                                            <SelectParameters>
                                                <asp:Parameter Name="JobPostingId" DefaultValue="0" Type="String" Direction="Input" />
                                                <asp:Parameter Name="SelectionStepLookupId" DefaultValue="0" Type="String" Direction="Input" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                        <asp:ListView ID="lsvCandidateList" runat="server" DataKeyNames="Id" DataSourceID="odsHiringMatrix"
                                            OnItemDataBound="lsvCandidateList_ItemDataBound" OnItemCommand="lsvCandidateList_ItemCommand"
                                            OnPreRender="lsvCandidateList_PreRender" EnableViewState="true">
                                            <layouttemplate>
                                                <table id="tlbTemplate" class="Grid" cellspacing="0" border="0">
                                                    <tr id="Tr1" runat="server">
                                                        <th style="vertical-align: middle;">
                                                            <div style="width: 24px;">
                                                                <!--                                               <div style="float: left; width :12px;" id="divImg" ><asp:Image ID="imgShowHide" ImageUrl="~/Images/expand-plus.gif" runat="server" onClick="javascript:ShowHideAllDetail(this)"  /></div> -->
                                                                <div style="float: left; width: 12px;">
                                                                    <input id="chkAllItem" name="chkAllItem" type="checkbox" onclick="SelectAllCheckbox(this)"
                                                                        disabled="disabled" /></div>
                                                            </div>
                                                            <div style="margin-left: 35px; margin-top: 3px;">
                                                                <asp:LinkButton ID="lnkHeaderName" runat="server" Text="Name" CommandName="Sort"
                                                                    CommandArgument="[C].[FirstName]"></asp:LinkButton></div>
                                                        </th>
                                                        <th style="vertical-align: middle;">
                                                            <asp:LinkButton ID="lnkHeaderPosition" runat="server" Text="Position" CommandName="Sort"
                                                                CommandArgument="[C].[CurrentPosition]" Width="60px"></asp:LinkButton>
                                                        </th>
                                                        <th style="vertical-align: middle;">
                                                            Location
                                                        </th>
                                                        <%--<th style =" display : none ">Mobile Phone</th>--%>
                                                        <th style="vertical-align: middle;">
                                                            <asp:LinkButton ID="lnkHeaderEmail" runat="server" Text="Email" CommandName="Sort"
                                                                CommandArgument="[C].[PrimaryEmail]"></asp:LinkButton>
                                                        </th>
                                                        <th style="min-width: 60px; white-space: normal; vertical-align: middle;">
                                                            <asp:LinkButton ID="lnkHeaderExperience" runat="server" Text="Exp (Yrs)" CommandName="Sort"
                                                                CommandArgument="Experience"></asp:LinkButton>
                                                        </th>
                                                        <%--<th style =" min-width :60px; display :none ;" id ="thInterview" runat ="server" ><asp:LinkButton ID ="lnkInterviews" runat ="server" Text ="Interviews" CommandName ="Sort" CommandArgument ="[CRI].[InterviewTime]" ></asp:LinkButton></th>--%>
                                                        <th id="thRelevance" runat="server" style="min-width: 60px; white-space: nowrap;
                                                            vertical-align: middle;">
                                                            <asp:LinkButton ID="lnkHeaderRelevance" runat="server" Text="Matching" CommandName="Sort"
                                                                CommandArgument="MatchingPercentage"></asp:LinkButton>
                                                        </th>
                                                        <th style="vertical-align: middle;">
                                                            Status
                                                        </th>
                                                        <%--<th>Action</th>--%>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" style="padding: 0px 0px 0px 0px; border-style: none none none none">
                                                        </td>
                                                    </tr>
                                                    <tr id="itemPlaceholder" runat="server">
                                                    </tr>
                                                    <tr class="Pager">
                                                        <td colspan="7" id="tdPager" runat="server">
                                                            <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </layouttemplate>
                                            <emptydatatemplate>
                                                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                                                    enableviewstate="true">
                                                    <tr>
                                                        <td>
                                                            No candidates in status level.
                                                        </td>
                                                    </tr>
                                                </table>
                                            </emptydatatemplate>
                                            <itemtemplate>
                                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'
                                                    align="left">
                                                    <td style="border-bottom-style: none; white-space: normal; min-width: 90px;" id="td2">
                                                        <div style="width: 25px;">
                                                            <div style="float: left; width: 12px; display: none">
                                                                <img class="ExpandImg" />
                                                            </div>
                                                            <div style="float: left; width: 12px;">
                                                                <asp:CheckBox ID="chkCandidate" runat="server" onClick="javascript:SelectRowCheckbox()" />
                                                                <asp:HiddenField ID="hfCandidateId" runat="server" />
                                                                <asp:HiddenField ID="hfstatuschangeId" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div style="white-space: normal; margin-left: 35px; width: 60%; margin-top: 2px">
                                                                <asp:HyperLink ID="hlnCandidateName" runat="server" Text="" Target="_blank" EnableViewState="true"></asp:HyperLink>
                                                            </div>
                                                            <div style="float: right; margin-top: -15px;">
                                                                <span id="intervewImg" runat="server" visible="false" title="Upcoming Interview">
                                                                    <img src="../Images/CalendarIcon.png" runat="server" id="imgInterv" /></span>
                                                            </div>
                                                            <%--<td  style =" border-bottom-style :none ;">--%>
                                                            <div style="float: right; width: 23%; vertical-align: top; margin-top: -15px;">
                                                                <div style="padding-left: 5px;">
                                                                    <img src="../Images/contextmenuicon.png" class="ContextExpand" style="cursor: pointer;
                                                                        display: none" /></div>
                                                                <div class="ContextMenu" id="divContextMenu" style="display: none; z-index: 9999;
                                                                    margin-top: -26px;" top="0">
                                                                    <div id="icon" runat="server" class="ContextMenuicon">
                                                                        <img src="../Images/contextmenuicon.png" id="imgList" runat="server"></div>
                                                                    <ul class="ContextMenu_List" id="ulList" runat="server" style="float: left;">
                                                                        <li id="liInterview" runat="server">
                                                                            <asp:HyperLink ID="lnkSchedule" runat="server" Text="Schedule Interview" Target="_blank"
                                                                                Width="100%" /></li>
                                                                        <li id="liEmail" runat="server">
                                                                            <asp:LinkButton ID="lnkEmailrequisition" runat="server" Text="Email Requisition Details"
                                                                                CommandName="EmailRequisition" Width="100%"></asp:LinkButton></li>
                                                                        <li id="liSubmit" runat="server">
                                                                            <asp:LinkButton ID="lnkSubmit" runat="server" Text="Submit" CommandName="SubmitToClient"
                                                                                Width="100%"></asp:LinkButton></li>
                                                                        <li id="liEditSubmissionDetails" runat="server">
                                                                            <asp:LinkButton ID="lnkEditSubmissionDetails" runat="server" Text="Edit Submission Details"
                                                                                CommandName="SubmissionDetails"></asp:LinkButton></li>
                                                                        <li id="liHiringDetails" runat="server">
                                                                            <asp:LinkButton ID="lnkHiringDetails" runat="server" Text="Edit Offer Details" Width="100%"
                                                                                CommandName="HiringDetails"></asp:LinkButton></li>
                                                                        <li id="liJoiningDetails" runat="server">
                                                                            <asp:LinkButton ID="lnkJoiningDetails" runat="server" Text="Edit Joining Details"
                                                                                Width="100%" CommandName="JoiningDetails"></asp:LinkButton></li>
                                                                        <li id="liHiringLog" runat="server">
                                                                            <asp:LinkButton ID="lnkHiringLog" runat="server" Text="Hiring Log" Width="100%" CommandName="HiringLog"></asp:LinkButton></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%--</td>--%>
                                                    </td>
                                                    <td style="border-bottom-style: none; min-width: 60px; max-width: 130px" id="td3">
                                                        <asp:Label ID="lblCandidateCurrentPosition" runat="server" Text="" Style="white-space: normal"></asp:Label>
                                                    </td>
                                                    <td style="border-bottom-style: none; min-width: 60px;" id="td4">
                                                        <asp:Label ID="lblCity" runat="server" Width="60px"></asp:Label>
                                                    </td>
                                                    <td style="border-bottom-style: none; min-width: 70px; overflow: hidden;" id="td9">
                                                        <asp:LinkButton ID="lblCandidateEmail" runat="server" Text=""></asp:LinkButton>
                                                    </td>
                                                    <td style="border-bottom-style: none; min-width: 60px;" id="td6">
                                                        <asp:Label ID="lblCandidateExperience" runat="server" Text="" Width="75px"></asp:Label>
                                                    </td>
                                                    <td style="border-bottom-style: none; min-width: 60px;" id="tdRelevance" runat="server">
                                                        <div id="divRelevance" runat="server">
                                                            <div id="relevance_bar" class="progress" runat="server">
                                                                <div id="relevance_text" class="bar" runat="server">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="border-bottom-style: none; min-width: 70px; max-width: 90px" id="tdStatus"
                                                        runat="server">
                                                        <asp:HiddenField ID="hdnCurrentlevelId" runat="server" />
                                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="CommonDropDownList" AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" Width="90%" EnableViewState="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="trCandidateDetails" class='<%# Container.DataItemIndex % 2 == 0 ? "Expandrow" : "Expandaltrow" %>'
                                                    style="display: none" runat="server">
                                                </tr>
                                            </itemtemplate>
                                        </asp:ListView>
                                    </div>
                                </div>
                                <div id="dvReje" runat="server">
                                    <ucl:Rejection ID="uclRejection" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div id="divSubmitToClient" style="display: none">
    <button id="btnbackToHiringMatrix" class="CommonButton">
        Back To Hiring Matrix</button>
    <ucl:SubmitToClient ID="uclSubmit" runat="server" />
</div>
