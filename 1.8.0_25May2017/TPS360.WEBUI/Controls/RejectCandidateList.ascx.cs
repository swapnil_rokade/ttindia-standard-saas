﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   RejectCandidateList.cs
    Description         :  
    Created By          :   Pravin khot
    Created On          :   
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
      0.1            29/April/2016          pravin khot         code modify - e.CommandName, "UndoRejection" ,btnUndoRejection_Click modify
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Linq;
using TPS360.Common;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.Security;

namespace TPS360.Web.UI
{
    public partial class RejectCandidateList :ATSBaseControl 
    {
        #region MemberVariables

        private bool _IsAccessToEmployee = true;
        private static string UrlForEmployee = string.Empty;
        private static int IdForSitemap = 0;

        #endregion

        #region Properties
        public string _rejectLevel=string .Empty ;
        public string RejectLevel
        {
            get 
            { 
               return  _rejectLevel ;
            }
            set
            {
                _rejectLevel = value;
            }
        }
        public delegate void RejectCandidateEventHandler();
        public event RejectCandidateEventHandler UndoRejectDetails;
        #endregion

        #region Methods
        public void BindList()
        {
            lsvCandidateList.Items.Clear();
            odsRejectCandidateList.SelectParameters["JobPostingId"].DefaultValue =CurrentJobPostingId.ToString();
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "[M].[FirstName]";
            }
            odsRejectCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text;
            lsvCandidateList.DataSourceID = "odsRejectCandidateList";
            lsvCandidateList.DataBind();
        }


        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvCandidateList.FindControl(txtSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        //public void SetTabText()
        //{
        //    Infragistics.WebUI.UltraWebTab.UltraWebTab uwtRequisitionHiringMatrixNavigationTopMenu = (Infragistics.WebUI.UltraWebTab.UltraWebTab)this.Page.Master.FindControl("cphHomeMaster").FindControl("ucRequisitionHiringMatrixNavigationTopMenu").FindControl("uwtRequisitionHiringMatrixNavigationTopMenu");


        //    int[] hiringMatrixCount = Facade.GetHiringMatrixMemberCount(base.CurrentJobPostingId);
        //    IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
        //    for (int i = 0; i < hiringMatrixLevels.Count; i++)
        //    {
        //        try
        //        {
        //            uwtRequisitionHiringMatrixNavigationTopMenu.Tabs[i].Text = hiringMatrixLevels[i].Name + " (" + hiringMatrixCount[i].ToString() + ")";
        //        }
        //        catch
        //        {
        //        }
        //    }
        //    int RejectCandidateCount = Facade.GetRejectCandidateCount(base .CurrentJobPostingId );
        //    uwtRequisitionHiringMatrixNavigationTopMenu.Tabs[hiringMatrixLevels.Count].Text = "Rejected (" + RejectCandidateCount + ")";
        //    if (RejectCandidateCount == 0)
        //        btnUndoRejection.Visible = false;
        //}
        private void RejectDetailsAdded(int StatusId, string MemberId)
        {
            btnUndoRejection.Visible = false;
            BindList();
        }
        public void showList()
        {
            btnUndoRejection.Visible = false;
            dvrdetails.Visible = true;
            BindList();
        }
        public void hideList()
        {
            dvrdetails.Visible = false;
        }
        #endregion

        #region PageEvents
        protected void Page_Load(object sender, EventArgs e)
        {

           
            {
                RejectLevel = string.Empty;
                CustomSiteMap CustomMap = new CustomSiteMap();
                CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
                if (CustomMap == null) _IsAccessToEmployee = false;
                else
                {
                    IdForSitemap = CustomMap.Id;
                    UrlForEmployee = "~/" + CustomMap.Url.ToString();
                }
                if (!IsPostBack)
                {
                    txtSortColumn.Text = "btnName";
                    txtSortOrder.Text = "ASC";
                    PlaceUpDownArrow();
                }
                string pagesize = "";
                pagesize = (Request.Cookies["RejectCandidateListRowPerPage"] == null ? "" : Request.Cookies["RejectCandidateListRowPerPage"].Value); ;
                ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)lsvCandidateList.FindControl("pagerControl");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                }

            }
        }
        #endregion

        #region ListView Events
        protected void lsvRejectCandidateList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsRejectCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                }
                int id;
                int.TryParse(e.CommandArgument.ToString(), out id);
                if (id > 0)
                {
                    if (string.Equals(e.CommandName, "UndoRejection"))
                    {   
                        //**********code added by pravin khot on 29/April/2016********************
                        string requisitionname = Facade.CandidateAvailableInRequisition_Name(id, CurrentJobPostingId);
                        if (requisitionname == null || requisitionname == "")
                        //************************************END*******************************
                        {
                            RejectCandidate can = Facade.GetRejectCandidateByMemberIdAndJobPostingID(id, CurrentJobPostingId);
                            if (can != null)
                            {
                                Facade.MemberJobCart_MoveToNextLevel(can.HiringMatrixLevel, base.CurrentMember.Id, id.ToString(), CurrentJobPosting.Id, can.HiringMatrixLevel.ToString());
                                Facade.DeleteRejectCandidateByMemberIdAndJobPostingID(id, CurrentJobPostingId);
                                btnUndoRejection.Visible = false;
                                BindList();
                                ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(MiscUtil), "Show", "ModalOperationCompleted('Successfully moved candidate(s).');", true);
                            }
                        }
                        //**********code added by pravin khot on 29/April/2016********************
                        else
                        {
                            MiscUtil.ShowMessage(lblMessage, "Cannot undo Rejection.  Candidate added to another Requisition.", false);
                        }
                        //************************************END*******************************
                    }
                  
                }
            }
            catch
            {
            }
        }
        protected void lsvRejectCandidateList_PreRender(object sender, EventArgs e)
        {

            
            {

                ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }
                    HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                    if (hdnRowPerPageName != null)
                    {
                        hdnRowPerPageName.Value = "RejectCandidateListRowPerPage";
                    }
                }
                PlaceUpDownArrow();

                HtmlTable tblEmptyData = (HtmlTable)lsvCandidateList.FindControl("tblEmptyData");
                if (tblEmptyData != null)
                {
                    lsvCandidateList.DataSource  = null;
                    lsvCandidateList.DataBind();
                }
            }
        }
        protected void lsvRejectCandidateList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                RejectCandidate candidatInfo = ((ListViewDataItem)e.Item).DataItem as RejectCandidate;

                if (candidatInfo != null)
                {
                    btnUndoRejection.Visible = true;
                    System.Web.UI.HtmlControls.HtmlTableRow row = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("rows");
                    System.Web.UI.WebControls.Image imgContextMenu = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgContextMenus");
                    System.Web.UI.HtmlControls.HtmlGenericControl divContextMenu = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("divContextMenus");
                    System.Web.UI.HtmlControls.HtmlGenericControl ulList = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("ulLists");
                    System.Web.UI.HtmlControls.HtmlGenericControl icon = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("icons");
                    System.Web.UI.HtmlControls.HtmlImage imgList = (System.Web.UI.HtmlControls.HtmlImage)e.Item.FindControl("imgLists");
                   // imgContextMenu.Style.Add("visibility", "hidden");
                  //  row.Attributes.Add("onmouseover", "javascript:ShowOrHideContextMenus('" + imgContextMenu.ClientID + "','visible','" + divContextMenu.ClientID + "');");
                   // row.Attributes.Add("onmouseout", "javascript:ShowOrHideContextMenus('" + imgContextMenu.ClientID + "','hidden','" + divContextMenu.ClientID + "');");
                   // imgContextMenu.Attributes.Add("onclick", "javascript:ShowOrHideContexts('" + divContextMenu.ClientID + "','" + imgContextMenu.ClientID + "');");
                   // divContextMenu.Attributes.Add("onmouseout", "onLeaveContextMenus('" + divContextMenu.ClientID + "')");
                   // ulList.Attributes.Add("onmouseout", "onLeaveContextMenus('" + divContextMenu.ClientID + "')");
                   // ulList.Attributes.Add("onmouseover", "javascript:ShowContextMenus('" + divContextMenu.ClientID + "')");
                   // imgList.Attributes.Add("onmouseover", "javascript:ShowContextMenus('" + divContextMenu.ClientID + "')");
                   // icon.Attributes.Add("onmouseout", "onLeaveContextMenus('" + divContextMenu.ClientID + "')");
                   // icon.Attributes.Add("onmouseover", "javascript:ShowContextMenus('" + divContextMenu.ClientID + "')");
                  //  System.Web.UI.HtmlControls.HtmlGenericControl liUndoRejectCandidate = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liUndoRejectCandidate");
                   // System.Web.UI.HtmlControls.HtmlGenericControl liEditRejectCandidate = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liEditRejectCandidate");
                   // System.Web.UI.HtmlControls.HtmlGenericControl liHiringLog = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("liHiringLog");
                    //liUndoRejectCandidate.Attributes.Add("onmouseover", "javascript:ShowContextMenus('" + divContextMenu.ClientID + "')");
                   // liEditRejectCandidate.Attributes.Add("onmouseover", "javascript:ShowContextMenus('" + divContextMenu.ClientID + "')");
                    //liHiringLog.Attributes.Add("onmouseover", "javascript:ShowContextMenus('" + divContextMenu.ClientID + "')");
                   // hdnDivContextMenuids.Value += divContextMenu.ClientID + ";";
                    LinkButton lnkUndoRejectCandidate = (LinkButton)e.Item.FindControl("lnkUndoRejectCandidate");
                    LinkButton lnkEditRejectCandidate = (LinkButton)e.Item.FindControl("lnkEditRejectCandidate");
                    LinkButton lnkHiringLog = (LinkButton)e.Item.FindControl("lnkHiringLog");
                    HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
                    Label lblMobilePhone = (Label)e.Item.FindControl("lblMobilePhone");
                    LinkButton lblEmail = (LinkButton)e.Item.FindControl("lblEmail");
                    Label lblRejectedOn = (Label)e.Item.FindControl("lblRejectedOn");
                    Label lblRejectedBy = (Label)e.Item.FindControl("lblRejectedBy");
                    Label lblReasonforRejection = (Label)e.Item.FindControl("lblReasonforRejection");
                    HiddenField hfHiringId = (HiddenField)e.Item.FindControl("hfHiringId");
                    HiddenField hfCandidateId = (HiddenField)e.Item.FindControl("hfCandidateId");
                    lnkHiringLog.CommandArgument =lnkEditRejectCandidate.CommandArgument = lnkUndoRejectCandidate.CommandArgument = candidatInfo.Id.ToString();
                    string strFullName = candidatInfo.FirstName + " " + candidatInfo.LastName;
                    if (strFullName.Trim() == "")
                    {
                        strFullName = "No Candidate Name";
                    }
                    if (_IsAccessToEmployee)
                    {
                        if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                            ControlHelper.SetHyperLink(lnkCandidateName, UrlForEmployee, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(candidatInfo.Id), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(),UrlConstants .PARAM_SITEMAP_PARENT_ID ,UrlConstants .Candidate .ATS_OVERVIEW_SITEMAP_PARENTID );
                    }
                    else lnkCandidateName.Text = strFullName;
                    hfHiringId.Value = candidatInfo.HiringMatrixLevel.ToString();
                    hfCandidateId.Value = candidatInfo.Id.ToString();
                    lblMobilePhone.Text = candidatInfo.CellPhone;
                    lblEmail.Text = candidatInfo.PrimaryEmail;
                    lblEmail.ToolTip = candidatInfo.PrimaryEmail;
                    lblRejectedOn.Text = candidatInfo.CreateDate.ToString();
                    lblRejectedBy.Text = candidatInfo.RejectorName;
                    lblReasonforRejection.Text = candidatInfo.RejectDetails;

                    lnkEditRejectCandidate.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/RejectionDetails.aspx?StatusId=" + 0 + "&JID=" + CurrentJobPostingId + "&Canid=" + candidatInfo.Id + "&IsEdit=1','500','275'); return false;";
                    lnkHiringLog.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/HiringLog.aspx?StatusId=" + 0 + "&JID=" + CurrentJobPostingId + "&Canid=" +  candidatInfo.Id   + "','778','570'); return false;";



                }
            }
        }
        #endregion

        #region Button Events
        protected void btnUndoRejection_Click(object sender, EventArgs e)
        {
            bool IsChecked = false;
            //added by pravin on 29/April/2016
            int totalselected = 0;
            int NotUndoProcess = 0;
            int count = 0;
            string requisitionname;
            //********END*******************
            foreach (ListViewDataItem  item in lsvCandidateList.Items)
            {
                
                HiddenField hfHiringId = (HiddenField)item.FindControl("hfHiringId");
                HiddenField hfCandidateId = (HiddenField)item.FindControl("hfCandidateId");
                CheckBox chkall = (CheckBox)item.FindControl("chkItemCandidate");
                if (chkall.Checked)
                {
                    IsChecked = true;
                    //**********code added by pravin khot on 29/April/2016********************                   
                    requisitionname = Facade.CandidateAvailableInRequisition_Name(Int32.Parse(hfCandidateId.Value), CurrentJobPostingId);
                    if (requisitionname == null || requisitionname == "")
                    {
                  //************************************END*******************************
                     
                        Facade.MemberJobCart_MoveToNextLevel(Int32.Parse(hfHiringId.Value), base.CurrentMember.Id, hfCandidateId.Value, CurrentJobPosting.Id, hfHiringId.Value);
                        Facade.DeleteRejectCandidateByMemberIdAndJobPostingID(Int32.Parse(hfCandidateId.Value), CurrentJobPostingId);
                        count=count+1;
                    } 
                    totalselected = totalselected + 1;
                    requisitionname = ""; //added by pravin on 29/April/2016
                }
               
            }

            if (UndoRejectDetails != null)
                UndoRejectDetails();
            btnUndoRejection.Visible = false;
            BindList();
            //int RejectCandidateCount = Facade.GetRejectCandidateCount(base.CurrentJobPostingId);
            //if (RejectCandidateCount == 0)
            //    btnUndoRejection.Visible = false; 
            NotUndoProcess=totalselected-count ;
            if (IsChecked)
            {
                //*******************Code added and comment by pravin khot on 29/April/2016******************   
                if (NotUndoProcess > 0)
                {
                     MiscUtil.ShowMessage(lblMessage, "Candidate(s) " + count.ToString() + " of " + totalselected.ToString() + " reinstated. " + NotUndoProcess.ToString() + " Candidate(s) are in process on another requisition.", false);
                    // MiscUtil.ShowMessage(lblMessage, "Candidate(s) " + count.ToString() + " of " + totalselected.ToString() + " moved successfully. Other " + NotUndoProcess.ToString() + " Candidate(s) are added in other Requisition this not allow undo process", false);
                }
                else
                {                        
                    MiscUtil.ShowMessage(lblMessage, "Candidate(s) " + count.ToString() + " of " + totalselected.ToString() + " reinstated.", false);
                    // MiscUtil.ShowMessage(lblMessage, "Candidate(s) " + count.ToString() + " of " + totalselected.ToString() + " moved successfully.", false);
                }
                //ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(MiscUtil), "Show", "ModalOperationCompleted('Candidate(s) moved successfully.');", true);
                //********************************END pravin khot on 29/April/2016******************

                //MiscUtil.ShowMessage(lblMessage, "Candidate(s) moved successfully. ", false);
            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(MiscUtil), "Show", "ModalOperationCompleted('Please select atleast one candidate.');", true);
                MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate.", true);
            }


        }
        #endregion
    }
}