﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:Control/CandidateReports.ascx.cs
    Description :This is user control common for CandidateReports.aspx and MyCandidateReports.aspx 
    Created By: Sumit Sonawane
    Created On: 25/May/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
      0.1             28/July/2016         pravin khot          added- lblReqCode
-------------------------------------------------------------------------------------------------------------------------------------------       

>*/


using System;
using System.Web.Security;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using TPS360.Common;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Linq;

namespace TPS360.Web.UI
{

    public partial class ControlCandidateReports : BaseControl
    {
        #region Members
        private bool _IsAccessToEmployee = true;
        private static string UrlForEmployee = string.Empty;
        private static int IdForSitemap = 0;
        private static bool _isMyCandidateReport = false;
        private static bool _isTeamReport = false;


        string _cookiename;
        string CookieName
        {
            get { return _cookiename; }
            set { _cookiename = value; }
        }
        
        #endregion
        #region Properties
        public bool IsMyCandidateReport 
        {
            set { _isMyCandidateReport = value; }
        }
        public bool IsTeamReport 
        {
            set { _isTeamReport = value; }
        }

        private string TeamMembersId
        {
            get;
            set;
        }
        #endregion
        #region Methods
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvCandidateList.FindControl(txtSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }
        private void GenerateCandidateReport(string format)
        {
            if (string.Equals(format, "word"))
            {

                Response.AddHeader("content-disposition", "attachment;filename=CandidateReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";
                Response.Output.Write(GetCandidateReportTable("word"));
                Response.Flush();
                Response.End();

            }
            else if (string.Equals(format, "excel"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=CandidateReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetCandidateReportTable("excel"));
                Response.Flush();
                Response.End();

            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PageWidth = 0;
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;


                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetCandidateReportTable("pdf"));

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=CandidateReport-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }

        private string GetCandidateReportTable(string format)
        {
            StringBuilder reqReport = new StringBuilder();
            CandidateDataSource candidateDataSource = new CandidateDataSource();



            IList<string> checkedList = new List<string>();
            foreach (ListItem item in chkColumnList.Items)
            {
                if (item.Selected)
                    checkedList.Add(item.Value);
            }
            if (_isMyCandidateReport) 
            {
                ddlUpdatedByEmployee.SelectedValue = ddlAddedByEmployee.SelectedValue = CurrentMember.Id.ToString();
                
            }
            bool isTeamReportList = _isTeamReport ? true : false;
            string base64Data = "";
            string path = "";
            //if (ddlTeamList.SelectedValue == "") 
            //{
            //    ddlTeamList.SelectedValue = "0";
            //}
            string teamLeaderId="";
            if(_isTeamReport)
                teamLeaderId=CurrentMember.Id.ToString();
            else
                teamLeaderId="0";

            IList<DynamicDictionary> candidateList1 = candidateDataSource.GetPaged(wdcCandidatesAdded.StartDate, wdcCandidatesAdded.EndDate, Int32.Parse(ddlAddedByEmployee.SelectedValue.Trim() == "" ? "0" : ddlAddedByEmployee.SelectedValue), 0,
               wdcCandidatesUpdated.StartDate, wdcCandidatesUpdated.EndDate, Int32.Parse(ddlUpdatedByEmployee.SelectedValue.Trim() == "" ? "0" : ddlUpdatedByEmployee.SelectedValue), uclCountryState.SelectedCountryId, uclCountryState.SelectedStateId, MiscUtil.RemoveScript(txtCity.Text), DateTime.MinValue, DateTime.MinValue, 0, 0,
               0, 0, Int32.Parse(ddlCandidatesWorkPermit.SelectedValue), Int32.Parse(ddlWorlSchedule.SelectedValue), Int32.Parse(ddlCandidatesGender.SelectedValue),
               Int32.Parse(ddlCandidatesMaritalStatus.SelectedValue), Int32.Parse(ddlCandidatesEducation.SelectedValue), 0, false, false, false, Int32.Parse(ddlAssignedManager.SelectedValue), "", -1, -1, checkedList, isTeamReportList, teamLeaderId, Int32.Parse(ddlTeamList.SelectedValue.Trim() == "" ? "0" : ddlTeamList.SelectedValue));

            if (candidateList1 != null)
            {
                path = AppDomain.CurrentDomain.BaseDirectory + "Images/logo-left-75px.png";
                
                if (File.Exists(path))
                {
                    base64Data = Convert.ToBase64String(File.ReadAllBytes(path));
                }

                reqReport.Append("<table style='width:100%; border-collapse:collapse; border-spacing:1px;'  align='left' border = '1' bordercolor='#000000' cellspacing='0' cellpadding='0'>");
                reqReport.Append("    <tr>");
                if (format == "pdf")
                {
                    reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='data:image/png;base64," + base64Data + "' style= style='height:56px;width:56px'/></td>"); //0.7
                }
                else
                {
                    reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png'; style= style='height:56px;width:56px'/></td>"); //0.7
                }
                reqReport.Append("    </tr>");

                reqReport.Append(" <tr>");
                //0.1 starts
                if (lsvCandidateList.Items.Count > 0)
                {
                    foreach (ListItem chkItem in chkColumnList.Items)
                    {
                        HtmlTableCell thHeaderTitle = new HtmlTableCell();

                        if (chkItem.Value.Contains("Availability"))
                            thHeaderTitle = (HtmlTableCell)lsvCandidateList.FindControl("thAvailability");
                        else
                            thHeaderTitle = (HtmlTableCell)lsvCandidateList.FindControl("th" + chkItem.Value);

                        if (thHeaderTitle != null)
                        {
                            if (chkItem.Selected)
                            {
                                reqReport.Append("<th><b>" + chkItem.Text + "</b></th>");
                            }
                        }
                    }
                }
                else
                {
                    foreach (ListItem chkItem in chkColumnList.Items)
                    {
                        if (chkItem.Selected)
                        {
                            reqReport.Append("<th><b>" + chkItem.Text + "</b></th>");
                        }
                    }
                }
                //0.1 ends

                reqReport.Append(" </tr>");
                int i = 0;
                foreach (DynamicDictionary d in candidateList1)
                {
                    // if (i++ % 2 != 0)
                    //     reqReport.Append("<tr style='background-color:#EBF4FA;'>");
                    // else
                    reqReport.Append("<tr>");

                    foreach (var s in d.properties)
                    {
                        if (s.Key.ToString() == "Id")
                        {
                            reqReport.Append("     <td>" + "A" + Convert.ToString(s.Value) + "</td>");
                        }
                        else if (s.Key.ToString() == "UpdateDate" || s.Key.ToString() == "CreateDate" || s.Key.ToString() == "DateOfBirth")
                        {
                            if (s.Value != "")
                                reqReport.Append("     <td align=left>" + Convert.ToDateTime(s.Value).ToShortDateString() + "</td>");
                            else
                                reqReport.Append("     <td>" + string.Empty + "</td>");
                        }
                        else if (s.Key.ToString() == "PassportStatus")
                        {
                            if (Convert.ToBoolean(s.Value.ToString().ToLower()))
                            {
                                reqReport.Append("     <td>" + "Ready" + "</td>");
                            }
                            else
                            {
                                reqReport.Append("     <td>" + "None" + "</td>");
                            }

                            
                        }
                        else if (s.Key.ToString() == "Availability")
                        {
                            string[] availabilityArray = s.Value.ToString().Split(',');
                            string strValue = "";
                            if (availabilityArray.Count() >= 2) 
                            {
                                if (s.Value.ToString().Contains("Not Available"))
                                {
                                    strValue = "Not Available";
                                }
                                else if (Convert.ToDateTime(availabilityArray[1]).Year != 1900)
                                {
                                    strValue = availabilityArray[0] + " " + Convert.ToDateTime(availabilityArray[1]).ToShortDateString();
                                }
                                
                            }
                           

                            reqReport.Append("     <td>" + strValue + "</td>");
                            
                        }
                        else if (s.Value != string.Empty && s.Value != null)
                        {
                            reqReport.Append("     <td>" + Convert.ToString(s.Value) + "</td>");
                        }

                        else
                        {
                            reqReport.Append("     <td>" + string.Empty + "</td>");
                        }

                    }
                    reqReport.Append("    </tr>");
                }
            }

            reqReport.Append("    <tr>");

            if (format == "pdf")
            {
                reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='data:image/png;base64," + base64Data + "' style= style='height:56px;width:56px'/></td>"); //0.7
            }
            else
            {
                reqReport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png'; style= style='height:56px;width:56px'/></td>"); //0.7
            }            // reqReport.Append("        <td align=left width='300' height='75'><img src='" + Server.MapPath(UIConstants.EMAIL_COMPANY_LOGO) + "' style= style='height:56px;width:56px;'/></td>");                
            reqReport.Append("    </tr>");


            reqReport.Append(" </table>");
            return reqReport.ToString();
        }

        private void BindList()
        {
            bool chkColumnListcount = false;
            foreach (ListItem items in chkColumnList.Items)
            {
                if (items.Selected)
                {
                    chkColumnListcount = true;
                    break;
                }
            }
            if (chkColumnListcount)
            {

                this.lsvCandidateList.DataSourceID = "odsCandidateList";
                this.lsvCandidateList.DataBind();
                divCandidateList.Visible = true;

            }
            else
            {
                divCandidateList.Visible = false;
                MiscUtil.ShowMessage(lblMessage, "Please select atleast one column", false);
            }
        }

        private void Clear()
        {
            divCandidateList.Visible = false;
            ClearSearchCriteria();
        }
        private void PopulateTeamlistByTeamLeaderId(int TeamLeaderId) 
        {
            ddlTeamList.DataSource = Facade.GetAllTeamByTeamLeaderId(TeamLeaderId);
            ddlTeamList.DataTextField = "Title";
            ddlTeamList.DataValueField = "Id";
            ddlTeamList.DataBind();
            //ddlTeamList.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));
        }

        private void PopulateUserList(string teamId) 
        {
            if (_isTeamReport)
            {
                if (teamId == "0" || teamId == "")
                {

                    ddlUpdatedByEmployee.DataSource = ddlAddedByEmployee.DataSource = Facade.GetAllMemberNameByTeamMemberId(ContextConstants.ROLE_EMPLOYEE, CurrentMember.Id);
                    ddlUpdatedByEmployee.DataValueField = ddlAddedByEmployee.DataValueField = "Id";
                    ddlUpdatedByEmployee.DataTextField = ddlAddedByEmployee.DataTextField = "FirstName";
                }
                else
                {
                    //ddlSubmittedBy.DataSource = Facade.GetAllEmployeeByTeamId(TeamId);
                    ddlUpdatedByEmployee.DataSource = ddlAddedByEmployee.DataSource = Facade.GetAllEmployeeByTeamId(teamId);
                    ddlUpdatedByEmployee.DataValueField = ddlAddedByEmployee.DataValueField = "Id";
                    ddlUpdatedByEmployee.DataTextField = ddlAddedByEmployee.DataTextField = "FirstName";

                }
                ddlAddedByEmployee.DataBind();
                ddlAddedByEmployee.Items.Insert(0, new ListItem("All", "0"));
                ddlUpdatedByEmployee.DataBind();
                ddlUpdatedByEmployee.Items.Insert(0, new ListItem("All", "0"));
            }
        }
        private void ClearSearchCriteria()
        {
            lblMessage.Text = string.Empty;
            wdcCandidatesUpdated.ClearRange();
            wdcCandidatesAdded.ClearRange();
            hdnAddedBy.Value = "0";
            hdnUpdatedBy.Value = "0";
            txtCity.Text = string.Empty;

            ddlAddedByEmployee.SelectedIndex = 0;
            ddlCandidatesEducation.SelectedIndex = 0;
            ddlCandidatesGender.SelectedIndex = 0;
            ddlCandidatesMaritalStatus.SelectedIndex = 0;
            ddlCandidatesWorkPermit.SelectedIndex = 0;
            ddlAssignedManager.SelectedIndex = 0;
            uclCountryState.SelectedCountryId = 0;
            uclCountryState.SelectedStateId = 0;
            ddlUpdatedByEmployee.SelectedIndex = 0;
            ddlWorlSchedule.SelectedIndex = 0;
            ddlTeamList.SelectedIndex = -1;
        }

        private void PrepareView()
        {
            ArrayList lstEmployee = null;
            if (_isTeamReport)
            {
                lstEmployee = Facade.GetAllMemberNameByTeamMemberId(ContextConstants.ROLE_EMPLOYEE, CurrentMember.Id);
                ddlAddedByEmployee.DataTextField = ddlUpdatedByEmployee.DataTextField = ddlAssignedManager.DataTextField = "FirstName";
                ddlAssignedManager.DataSource = lstEmployee;
                ddlAssignedManager.DataTextField = "FirstName";
                ddlAssignedManager.DataValueField = "Id";
                ddlAssignedManager.DataBind();
            }
            else
            {
                lstEmployee = Facade.GetAllMemberNameByRoleName(ContextConstants.ROLE_EMPLOYEE);
                ddlAddedByEmployee.DataTextField = ddlUpdatedByEmployee.DataTextField = ddlAssignedManager.DataTextField = "Name";
                ddlAssignedManager.DataSource = lstEmployee;
                ddlAssignedManager.DataTextField = "Name";
                ddlAssignedManager.DataValueField = "Id";
                ddlAssignedManager.DataBind();
            }
            
            ddlAssignedManager = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlAssignedManager);
            ddlAssignedManager.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));
            MiscUtil.PopulateWorkSchedule(ddlWorlSchedule, Facade);
            MiscUtil.PopulateMaritalStatusType(ddlCandidatesMaritalStatus, Facade);
            ddlCandidatesMaritalStatus.Items.RemoveAt(0);
            ddlCandidatesMaritalStatus.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));

            MiscUtil.PopulateGenderType(ddlCandidatesGender, Facade);
            ddlCandidatesGender.Items.RemoveAt(0);
            ddlCandidatesGender.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));

            MiscUtil.PopulateWorkAuthorization(ddlCandidatesWorkPermit, Facade);
            ddlCandidatesWorkPermit.Items.RemoveAt(0);
            ddlCandidatesWorkPermit.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));

            MiscUtil.PopulateEducationQualification(ddlCandidatesEducation, Facade);
            ddlCandidatesEducation.Items.RemoveAt(0);
            ddlCandidatesEducation.Items.Insert(0, new ListItem(UIConstants.DROP_DOWNL_ITEM_ANY, "0"));
            
        }
        private void ResetDataSourceParameters()
        {
            odsCandidateList.SelectParameters["addedFrom"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["addedTo"].DefaultValue = DateTime.MinValue.ToString();
            if (_isMyCandidateReport)
            {
                odsCandidateList.SelectParameters["addedBy"].DefaultValue = CurrentMember.Id.ToString();
            }
            else
            {
                odsCandidateList.SelectParameters["addedBy"].DefaultValue = "0";
            }

            if (_isTeamReport) 
            {
                odsCandidateList.SelectParameters["IsTeamReport"].DefaultValue = "true";
                odsCandidateList.SelectParameters["TeamMembersId"].DefaultValue = CurrentMember.Id.ToString();
                odsCandidateList.SelectParameters["TeamId"].DefaultValue = "0";
            }

            odsCandidateList.SelectParameters["addedBySource"].DefaultValue = "0";
            odsCandidateList.SelectParameters["updatedFrom"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["updatedTo"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["updatedBy"].DefaultValue = "0";

            odsCandidateList.SelectParameters["country"].DefaultValue = "0";
            odsCandidateList.SelectParameters["state"].DefaultValue = "0";
            odsCandidateList.SelectParameters["city"].DefaultValue = String.Empty;

            odsCandidateList.SelectParameters["interviewFrom"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["interviewTo"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["interviewLevel"].DefaultValue = "0";
            odsCandidateList.SelectParameters["interviewStatus"].DefaultValue = "0";
            odsCandidateList.SelectParameters["industry"].DefaultValue = "0";
            odsCandidateList.SelectParameters["functionalCategory"].DefaultValue = "0";
            odsCandidateList.SelectParameters["workPermit"].DefaultValue = "0";
            odsCandidateList.SelectParameters["WorkSchedule"].DefaultValue = "0";
            odsCandidateList.SelectParameters["gender"].DefaultValue = "0";
            odsCandidateList.SelectParameters["maritalStatus"].DefaultValue = "0";
            odsCandidateList.SelectParameters["educationId"].DefaultValue = "0";
            odsCandidateList.SelectParameters["assessmentId"].DefaultValue = "0";
            odsCandidateList.SelectParameters["isBroadcastedResume"].DefaultValue = "false";
            odsCandidateList.SelectParameters["isReferredCandidates"].DefaultValue = "false";
            odsCandidateList.SelectParameters["hasJobAgent"].DefaultValue = "false";
            odsCandidateList.SelectParameters["assignedManager"].DefaultValue = "0";
        }

        private void SetDataSourceParameters()
        {
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "[C].[FirstName]";
            }
            odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            if (_isTeamReport)
            {
                odsCandidateList.SelectParameters["IsTeamReport"].DefaultValue = "true";
                odsCandidateList.SelectParameters["TeamMembersId"].DefaultValue = CurrentMember.Id.ToString();
                odsCandidateList.SelectParameters["TeamId"].DefaultValue = ddlTeamList.SelectedValue;
            }


            if (_isMyCandidateReport)
            {
                odsCandidateList.SelectParameters["addedBy"].DefaultValue = CurrentMember.Id.ToString();
            }
            else
            {
                odsCandidateList.SelectParameters["addedBy"].DefaultValue = ddlAddedByEmployee.SelectedValue;
            }
            
            odsCandidateList.SelectParameters["addedBySource"].DefaultValue = "0";

            odsCandidateList.SelectParameters["interviewFrom"].DefaultValue = DateTime.MinValue.ToString();

            odsCandidateList.SelectParameters["interviewTo"].DefaultValue = DateTime.MinValue.ToString();
            odsCandidateList.SelectParameters["interviewLevel"].DefaultValue = "0";
            odsCandidateList.SelectParameters["interviewStatus"].DefaultValue = "0";
            odsCandidateList.SelectParameters["functionalCategory"].DefaultValue = "0";
            odsCandidateList.SelectParameters["gender"].DefaultValue = ddlCandidatesGender.SelectedValue;
            odsCandidateList.SelectParameters["educationId"].DefaultValue = ddlCandidatesEducation.SelectedValue;
            odsCandidateList.SelectParameters["updatedBy"].DefaultValue = ddlUpdatedByEmployee.SelectedValue;
            odsCandidateList.SelectParameters["industry"].DefaultValue = "0";
            odsCandidateList.SelectParameters["workPermit"].DefaultValue = ddlCandidatesWorkPermit.SelectedValue;

            odsCandidateList.SelectParameters["WorkSchedule"].DefaultValue = ddlWorlSchedule.SelectedValue;


            odsCandidateList.SelectParameters["maritalStatus"].DefaultValue = ddlCandidatesMaritalStatus.SelectedValue;
            odsCandidateList.SelectParameters["assessmentId"].DefaultValue = "0";
            odsCandidateList.SelectParameters["city"].DefaultValue = MiscUtil.RemoveScript(txtCity.Text);
            odsCandidateList.SelectParameters["assignedManager"].DefaultValue = ddlAssignedManager.SelectedValue;


        }

        private void HideListViewHeaderItem()
        {
            foreach (ListItem chkItem in chkColumnList.Items)
            {
                HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvCandidateList.FindControl("th" + chkItem.Value);
                if (thHeaderTitle != null)
                {
                    if (chkItem.Selected)
                    {
                        thHeaderTitle.Visible = true;

                    }
                    else
                    {
                        thHeaderTitle.Visible = false;
                    }
                }
            }
        }

        private void HideListViewItem(ListViewItem itm)
        {
            foreach (ListItem chkItem in chkColumnList.Items)
            {
                HtmlTableCell thHeaderTitle = (HtmlTableCell)itm.FindControl("td" + chkItem.Value);
                if (thHeaderTitle != null)
                {
                    if (chkItem.Selected)
                    {
                        thHeaderTitle.Visible = true;

                    }
                    else
                    {
                        thHeaderTitle.Visible = false;
                    }
                }
            }
        }

        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                repor("pdf");
            }

        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
       
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            ddlTeamList.Attributes.Add("onchange", "TeamOnChange('" + ddlTeamList.ClientID + "','" + ddlAddedByEmployee.ClientID + "','" + ddlUpdatedByEmployee.ClientID + "','" + hdnAddedBy.ClientID + "','" + CurrentMember.Id + "')");
            ddlAddedByEmployee.Attributes.Add("onchange", "EmployeeOnChange('" + ddlAddedByEmployee.ClientID + "','" + hdnAddedBy.ClientID + "')");
            ddlUpdatedByEmployee.Attributes.Add("onchange", "UpdateByOnChange('" + ddlUpdatedByEmployee.ClientID + "','" + hdnUpdatedBy.ClientID + "')");


            ddlAddedByEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlAssignedManager.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlUpdatedByEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            CookieName = "CandidateReportUserColumnOption_" + base.CurrentMember.PrimaryEmail;
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
           
            if (CustomMap == null) _IsAccessToEmployee = false;
            else
            {
                IdForSitemap = CustomMap.Id;
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }
            if (_isMyCandidateReport) 
            {
                spnUpdatedByEmployee.Visible = false;
                ddlUpdatedByEmployee.Visible = false;
                spnAddedByEmployee.Visible = false;
                ddlAddedByEmployee.Visible = false;
                divAssignedManager.Visible = false;
            }
            if (_isTeamReport) 
            {
                divTeam.Visible = true;
            }
            if (!IsPostBack)
            {
                PrepareView();
                PopulateTeamlistByTeamLeaderId(CurrentMember.Id);
                if (_isTeamReport) 
                {
                    if (ddlTeamList.Items.Count > 0)
                        ddlTeamList.Items.Insert(0, new ListItem("All", "0"));

                    else
                        divTeam.Visible = false;

                    PopulateUserList("0");
                }
                else
                {
                    MiscUtil.PopulateMemberListByRole(ddlAddedByEmployee, ContextConstants.ROLE_EMPLOYEE, Facade);
                    MiscUtil.PopulateMemberListByRole(ddlUpdatedByEmployee, ContextConstants.ROLE_EMPLOYEE, Facade);
                    if (_isMyCandidateReport)
                    {
                        ddlAddedByEmployee.SelectedValue = CurrentMember.Id.ToString();
                        ddlUpdatedByEmployee.SelectedValue = CurrentMember.Id.ToString();
                    }
                }
                divCandidateList.Visible = false;
                if (Request.Cookies[CookieName] != null)
                {

                    //string ColumnOptions = Request.Cookies[CookieName].Value;
                    string ColumnOptions = "Id#Name#PrimaryEmail#CurrentCity#CurrentStateName#PrimaryPhone#CellPhone#HomePhone#CurrentPosition#LastEmployer#";
                    string[] Columns = ColumnOptions.Split('#');
                    foreach (ListItem item in chkColumnList.Items)
                    {
                        item.Selected = false;
                    }
                    foreach (string item in Columns)
                    {
                        if (chkColumnList.Items.FindByValue(item) != null) chkColumnList.Items.FindByValue(item).Selected = true;
                    }
                }
            }
            if (!IsPostBack)
            {

                btnSearch_Click(sender, e);
            }
        }
        protected void ddlTeamList_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateUserList(ddlTeamList.SelectedValue);
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdnScrollPos.Value = "0";
            string ColumnOption = "";
            PopulateUserList(ddlTeamList.SelectedValue);
            //ddlAddedByEmployee.SelectedValue = hdnAddedBy.Value;           
            //ddlUpdatedByEmployee.SelectedValue = hdnUpdatedBy.Value;

            foreach (ListItem item in chkColumnList.Items)
            {
                if (item.Selected == true) ColumnOption += item.Value + "#";

            }
            System.Web.HttpCookie aCookie = new System.Web.HttpCookie(CookieName);
            aCookie.Value = ColumnOption.ToString();
            Response.Cookies.Add(aCookie);
            lsvCandidateList.Items.Clear();
            SetDataSourceParameters();
            ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
            if (PagerControls != null)
            {
                DataPager pager = (DataPager)PagerControls.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            divExportButton.Visible = false;
            BindList();
            HideListViewHeaderItem();

            string pagesize = "";
            if (txtSortColumn.Text == "") txtSortColumn.Text = "lnkBtnName";
            if (txtSortOrder.Text == "") txtSortOrder.Text = "ASC";
            PlaceUpDownArrow();
            pagesize = (Request.Cookies["CandidateReportRowPerPage"] == null ? "" : Request.Cookies["CandidateReportRowPerPage"].Value); ;

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);



                }
            }
            System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvCandidateList.FindControl("tdpager");
            if (tdpager != null) tdpager.ColSpan = chkColumnList.Items.Cast<ListItem>().Where(x => x.Selected == true).Count();
            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            divExportButton.Visible = false;
            lsvCandidateList.DataBind();

            if (divTeam.Visible)
            {
                ddlTeamList.SelectedIndex = 0;
                PopulateUserList(ddlTeamList.SelectedValue);
            }
            else PopulateUserList("0");

            ddlUpdatedByEmployee.SelectedIndex = 0;
            ddlAddedByEmployee.SelectedIndex = 0;
        }

        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {
            uclConfirm.AddMessage("Note: Some columns may not appear in report if too many are selected.", ConfirmationWindow.enmMessageType.Attention, true, true, "");
            repor("pdf");
        }
        public void repor(string rep)
        {
            int j = 0;
            int k = 0;
            foreach (ListItem items in chkColumnList.Items)
            {
                if (items.Selected)
                {
                    j = 1;

                }
                else
                    k = 1;
            }
            if ((j == 1 && k == 1) || (k == 0 && j == 1))
                GenerateCandidateReport(rep);
            else
                MiscUtil.ShowMessage(lbMessage, "Please select columns before generating the report.", true);
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            repor("excel");
        }

        protected void btnExportToWord_Click(object sender, EventArgs e)
        {
            repor("word");
        }

        protected void lsvCandidateList_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCandidateList.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "CandidateReportRowPerPage";
            }

            PlaceUpDownArrow();
            HideListViewHeaderItem();
            if (IsPostBack)
            {
                if (lsvCandidateList.Items.Count == 0)
                {
                    lsvCandidateList.DataSource = null;
                    lsvCandidateList.DataBind();
                }
            }
        }
        protected void lsvCandidateList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                }
            }
            catch
            {
            }
        }
        protected void lsvCandidateList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            lsvCandidateList.Visible = true;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Candidate candidatInfo = ((ListViewDataItem)e.Item).DataItem as Candidate;

                if (candidatInfo != null)
                {
                    divExportButton.Visible = true;
                    HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName");
                    Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
                    Label lblNickName = (Label)e.Item.FindControl("lblNickName");
                    Label lblPrimaryEmail = (Label)e.Item.FindControl("lblPrimaryEmail");
                    Label lblAlternateEmail = (Label)e.Item.FindControl("lblAlternateEmail");
                    Label lblPermanentAddress = (Label)e.Item.FindControl("lblPermanentAddress");
                    Label lblPermanentCity = (Label)e.Item.FindControl("lblPermanentCity");
                    Label lblPermanentState = (Label)e.Item.FindControl("lblPermanentState");
                    Label lblPermanentZip = (Label)e.Item.FindControl("lblPermanentZip");
                    Label lblPermanentCountry = (Label)e.Item.FindControl("lblPermanentCountry");
                    Label lblPermanentPhone = (Label)e.Item.FindControl("lblPermanentPhone");
                    Label lblPermanentMobile = (Label)e.Item.FindControl("lblPermanentMobile");
                    Label lblCurrentAddress = (Label)e.Item.FindControl("lblCurrentAddress");
                    Label lblCurrentCity = (Label)e.Item.FindControl("lblCurrentCity");
                    Label lblCurrentState = (Label)e.Item.FindControl("lblCurrentState");
                    Label lblCurrentZip = (Label)e.Item.FindControl("lblCurrentZip");
                    Label lblCurrentCountry = (Label)e.Item.FindControl("lblCurrentCountry");
                    Label lblPrimaryPhone = (Label)e.Item.FindControl("lblPrimaryPhone");
                    Label lblCellPhone = (Label)e.Item.FindControl("lblCellPhone");
                    Label lblHomePhone = (Label)e.Item.FindControl("lblHomePhone");
                    Label lblOfficePhone = (Label)e.Item.FindControl("lblOfficePhone");
                    Label lblCurrentPosition = (Label)e.Item.FindControl("lblCurrentPosition");
                    Label lblCurrentCompany = (Label)e.Item.FindControl("lblCurrentCompany");
                    Label lblDateOfBirth = (Label)e.Item.FindControl("lblDateOfBirth");
                    Label lblCityOfBirth = (Label)e.Item.FindControl("lblCityOfBirth");
                    Label lblCountryOfBirth = (Label)e.Item.FindControl("lblCountryOfBirth");
                    Label lblCitizenship = (Label)e.Item.FindControl("lblCitizenship");
                    Label lblGender = (Label)e.Item.FindControl("lblGender");
                    Label lblMaritalStatus = (Label)e.Item.FindControl("lblMaritalStatus");
                    Label lblYearsOfExperience = (Label)e.Item.FindControl("lblYearsOfExperience");
                    Label lblAvailability = (Label)e.Item.FindControl("lblAvailability");
                    Label lblWillingToTravel = (Label)e.Item.FindControl("lblWillingToTravel");
                    Label lblCurrentYearlyRate = (Label)e.Item.FindControl("lblCurrentYearlyRate");
                    Label lblExpectedYearlyRate = (Label)e.Item.FindControl("lblExpectedYearlyRate");
                    Label lblCreator = (Label)e.Item.FindControl("lblCreator");
                    Label lblCreateDate = (Label)e.Item.FindControl("lblCreateDate");
                    Label lblLastUpdator = (Label)e.Item.FindControl("lblLastUpdator");
                    Label lblLastUpdateDate = (Label)e.Item.FindControl("lblLastUpdateDate");
                    Label lblWorkSchedule = (Label)e.Item.FindControl("lblWorkSchedule");
                    Label lblWebsite = (Label)e.Item.FindControl("lblWebsite");
                    Label lblLinkedInProfile = (Label)e.Item.FindControl("lblLinkedInProfile");
                    Label lblPassportStatus = (Label)e.Item.FindControl("lblPassportStatus");
                    Label lblIDProof = (Label)e.Item.FindControl("lblIDProof");
                    Label lblHighestEducation = (Label)e.Item.FindControl("lblHighestEducation");
                    Label lblSource = (Label)e.Item.FindControl("lblSource");
                    Label lblSourceDescription = (Label)e.Item.FindControl("lblSourceDescription");
                    Label lblReqCode = (Label)e.Item.FindControl("lblReqCode");
                    Label lblHiringStatus = (Label)e.Item.FindControl("lblHiringStatus");
                    lblCandidateID.Text = "A" + candidatInfo.Id.ToString();
                    string strPermanentAddress = candidatInfo.PermanentAddressLine1;
                    string strCurrentAddress = candidatInfo.CurrentAddressLine1;
                    string strFullName = MiscUtil.GetFirstAndLastName(candidatInfo.FirstName, candidatInfo.LastName);
                    if (strFullName.Trim() == "")
                    {
                        strFullName = "No Candidate Name";
                    }
                    if (_IsAccessToEmployee)
                    {
                        if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                            ControlHelper.SetHyperLink(lnkCandidateName, UrlForEmployee, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(candidatInfo.Id), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.ATS_OVERVIEW_SITEMAP_PARENTID);
                    }
                    else lnkCandidateName.Text = strFullName;
                    lblNickName.Text = candidatInfo.NickName;
                    lblPrimaryEmail.Text = candidatInfo.PrimaryEmail;
                    lblAlternateEmail.Text = candidatInfo.AlternateEmail;

                    if (!String.IsNullOrEmpty(strPermanentAddress) && !String.IsNullOrEmpty(candidatInfo.PermanentAddressLine2))
                    {
                        strPermanentAddress = strPermanentAddress + "</br>" + candidatInfo.PermanentAddressLine2;
                    }
                    else if (String.IsNullOrEmpty(strPermanentAddress))
                    {
                        strPermanentAddress = candidatInfo.PermanentAddressLine2;
                    }

                    if (!String.IsNullOrEmpty(strCurrentAddress) && !String.IsNullOrEmpty(candidatInfo.CurrentAddressLine2))
                    {
                        strCurrentAddress = strCurrentAddress + "</br>" + candidatInfo.CurrentAddressLine2;
                    }
                    else if (String.IsNullOrEmpty(strCurrentAddress))
                    {
                        strCurrentAddress = candidatInfo.CurrentAddressLine2;
                    }

                    lblPermanentAddress.Text = strPermanentAddress;
                    lblPermanentCity.Text = candidatInfo.PermanentCity;
                    lblPermanentState.Text = candidatInfo.CurrentStateName;
                    lblPermanentZip.Text = candidatInfo.CurrentZip; 
                    lblPermanentCountry.Text = candidatInfo.CurrentCountryName;
                    string strPermanentPhone = candidatInfo.PermanentPhone;
                    if (!String.IsNullOrEmpty(strPermanentPhone) && !String.IsNullOrEmpty(candidatInfo.PermanentPhoneExt))
                    {
                        strPermanentPhone = strPermanentPhone + " Ext. " + candidatInfo.PermanentPhoneExt;
                    }
                    lblPermanentPhone.Text = strPermanentPhone;
                    lblPermanentMobile.Text = candidatInfo.PermanentMobile;

                    lblCurrentAddress.Text = strCurrentAddress;
                    lblCurrentCity.Text = candidatInfo.CurrentCity;
                    lblCurrentState.Text = candidatInfo.PermanentStateName;
                    lblCurrentZip.Text = candidatInfo.PermanentZip;
                    lblCurrentCountry.Text = candidatInfo.PermanentCountryName;
                    string strPrimaryPhone = candidatInfo.PrimaryPhone;
                    if (!String.IsNullOrEmpty(strPrimaryPhone) && !String.IsNullOrEmpty(candidatInfo.PrimaryPhoneExtension))
                    {
                        strPrimaryPhone = strPrimaryPhone + " Ext. " + candidatInfo.PrimaryPhoneExtension;
                    }
                    lblPrimaryPhone.Text = strPrimaryPhone;
                    lblCellPhone.Text = candidatInfo.CellPhone;
                    lblHomePhone.Text = candidatInfo.HomePhone;
                    string strOfficePhone = candidatInfo.OfficePhone;
                    if (!String.IsNullOrEmpty(strOfficePhone) && !String.IsNullOrEmpty(candidatInfo.OfficePhoneExtension))
                    {
                        strOfficePhone = strOfficePhone + " Ext. " + candidatInfo.OfficePhoneExtension;
                    }
                    lblOfficePhone.Text = strOfficePhone;
                    lblCurrentPosition.Text = candidatInfo.CurrentPosition;
                    lblCurrentCompany.Text = candidatInfo.LastEmployer;
                    if (candidatInfo.DateOfBirth != null && candidatInfo.DateOfBirth != DateTime.MinValue)
                    {
                        lblDateOfBirth.Text = candidatInfo.DateOfBirth.ToShortDateString();
                    }
                    lblCityOfBirth.Text = candidatInfo.CityOfBirth;
                    lblCountryOfBirth.Text = candidatInfo.CitizenshipCountryName;
                    lblCitizenship.Text = candidatInfo.BirthCountryName;
                    lblGender.Text = candidatInfo.Gender;
                    lblMaritalStatus.Text = candidatInfo.MaritalStatus;
                    lblYearsOfExperience.Text = candidatInfo.TotalExperienceYears;
                    lblAvailability.Text = candidatInfo.AvailabilityText;
                    if (candidatInfo.AvailabilityText.IndexOf(" ") > 0 && candidatInfo.AvailableDate != null && candidatInfo.AvailableDate != DateTime.MinValue)
                    {
                        lblAvailability.Text = lblAvailability.Text + " " + candidatInfo.AvailableDate.ToShortDateString();
                    }
                    lblWillingToTravel.Text = candidatInfo.WillingToTravel ? "Yes" : "No";


                    lblWorkSchedule.Text = candidatInfo.WorkSchedule;


                    string strSalary = candidatInfo.CurrentYearlyRate.ToString();
                    if (!String.IsNullOrEmpty(strSalary) && !String.IsNullOrEmpty(candidatInfo.CurrentYearlyCurrency))
                    {
                        strSalary = strSalary + " " + candidatInfo.CurrentYearlyCurrency;
                    }
                    lblCurrentYearlyRate.Text = strSalary;
                    strSalary = candidatInfo.ExpectedYearlyRate.ToString();
                    if (!String.IsNullOrEmpty(strSalary) && !String.IsNullOrEmpty(candidatInfo.ExpectedYearlyCurrency))
                    {
                        strSalary = strSalary + " " + candidatInfo.ExpectedYearlyCurrency;
                    }
                    lblExpectedYearlyRate.Text = strSalary;

                    lblCreator.Text = candidatInfo.CreatorName;
                    lblCreateDate.Text = candidatInfo.CreateDate.ToShortDateString();
                    lblLastUpdator.Text = candidatInfo.LastUpdatorName;
                    lblLastUpdateDate.Text = candidatInfo.UpdateDate.ToShortDateString();

                    lblWebsite.Text = candidatInfo.Website.ToString();
                    lblLinkedInProfile.Text = candidatInfo.LinkedInProfile;
                    lblPassportStatus.Text = candidatInfo.PassportStatus ? "Ready" : "None";
                    lblIDProof.Text = candidatInfo.IDCard;
                    lblHighestEducation.Text = candidatInfo.HighestDegree;
                    lblSource.Text = candidatInfo.Source;
                    lblSourceDescription.Text = candidatInfo.SourceDescription;

                    lblReqCode.Text = candidatInfo.ReqCode; // added by pravin khot on 28/July/2016
                    lblHiringStatus.Text = candidatInfo.HiringStatus; // added by pravin khot on 28/July/2016


                    HideListViewItem(e.Item);
                }
            }
        }

        protected void lnkBtnResult_Click(object sender, EventArgs e)
        {
            LinkButton lnkBtnSender = (LinkButton)sender;
            if (lnkBtnSender.Text != "0")
            {
                HideListViewHeaderItem();
                ResetDataSourceParameters();
                switch (lnkBtnSender.ID)
                {
                    case "lnkBtnResultByNew":
                        odsCandidateList.SelectParameters["addedBy"].DefaultValue = ddlAddedByEmployee.SelectedValue;
                        odsCandidateList.SelectParameters["addedBySource"].DefaultValue = "0";
                        break;
                    case "lnkBtnResultByInterviewe":

                        odsCandidateList.SelectParameters["interviewFrom"].DefaultValue = DateTime.MinValue.ToString();

                        odsCandidateList.SelectParameters["interviewTo"].DefaultValue = DateTime.MinValue.ToString();
                        odsCandidateList.SelectParameters["interviewLevel"].DefaultValue = "0";
                        odsCandidateList.SelectParameters["interviewStatus"].DefaultValue = "0";
                        break;
                    case "lnkBtnResultByFunctionalCategory":
                        odsCandidateList.SelectParameters["functionalCategory"].DefaultValue = "0";
                        break;
                    case "lnkBtnResultByGender":
                        odsCandidateList.SelectParameters["gender"].DefaultValue = ddlCandidatesGender.SelectedValue;
                        break;
                    case "lnkBtnResultByEducation":
                        odsCandidateList.SelectParameters["educationId"].DefaultValue = ddlCandidatesEducation.SelectedValue;
                        break;
                    case "lnkBtnResultByUpdate":
                        odsCandidateList.SelectParameters["updatedBy"].DefaultValue = ddlUpdatedByEmployee.SelectedValue;
                        break;
                    case "lnkResultByIndustry":
                        odsCandidateList.SelectParameters["industry"].DefaultValue = "0";
                        break;
                    case "lnkBtnResultByWorkPermit":
                        odsCandidateList.SelectParameters["workPermit"].DefaultValue = ddlCandidatesWorkPermit.SelectedValue;
                        break;
                    case "lnkBtnResultByMaritalStatus":
                        odsCandidateList.SelectParameters["maritalStatus"].DefaultValue = ddlCandidatesMaritalStatus.SelectedValue;
                        break;
                    case "lnkBtnResultByTakenTests":
                        odsCandidateList.SelectParameters["assessmentId"].DefaultValue = "0";
                        break;
                    case "lnkBtnResultByManager":
                        odsCandidateList.SelectParameters["assignedManager"].DefaultValue = ddlAssignedManager.SelectedValue;
                        break;
                    case "lnkBtnResultByLocation":
                        odsCandidateList.SelectParameters["city"].DefaultValue = txtCity.Text;
                        break;
                    default:
                        break;
                }
                BindList();
            }
        }



        #endregion

    }
}
