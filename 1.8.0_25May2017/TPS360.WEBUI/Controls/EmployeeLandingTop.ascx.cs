﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmployeeLandingTop.ascx.cs
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             05-May-2009         Jagadish            Defect id: 10431; Changes made in method LoadEmployeeData().
 *  0.2             23-Nov-2009         Sandeesh            Enhancement id:11645 -Changes made  for adding internal rating for an employee 
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Collections;
namespace TPS360.Web.UI
{
    public partial class EmployeeLandingTop : BaseControl
    {
        private int memberId = 0;
        private static string UrlForEmployee = string.Empty;
        private static int IdForSitemap = 0;
        #region Methods

        private void LoadEmployeeData()
        {
            if (IsUserEmployee && memberId <= 0)
            {
                ControlHelper.SetHyperLink(lblEmployeeName, UrlForEmployee , string.Empty, MiscUtil.GetFullName(base.CurrentMember.FirstName, base.CurrentMember.MiddleName, base.CurrentMember.LastName), UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(memberId),UrlConstants .PARAM_SITEMAP_ID ,IdForSitemap .ToString ());
                string empAdd = string.Empty;
                string CountryName = MiscUtil.GetCountryNameById(base.CurrentMember.PermanentCountryId, Facade);
                empAdd += base.CurrentMember.PermanentAddressLine1 + " " + CurrentMember.PermanentAddressLine2;
                empAdd += "<br/>";
                empAdd += (!string.IsNullOrEmpty(base.CurrentMember.PermanentCity) ? base.CurrentMember.PermanentCity + ", " : string.Empty) + CountryName.Trim ();
                empAdd += "<br/>";
                empAdd += CurrentMember.PermanentCountryId > 0 ? CountryName.Trim () : "";
                divEmployeeAddress.InnerHtml = empAdd;
                lblPhone.Text = base.CurrentMember.PrimaryPhone;
                lblMobile.Text = base.CurrentMember.CellPhone;
                lblPrimaryEmail.Text = "<a href=mailto:'" + base.CurrentMember.PrimaryEmail + "'>" + base.CurrentMember.PrimaryEmail + "</a>"; //0.1
                MemberDetail memDetail = Facade.GetMemberDetailByMemberId(base.CurrentMember.Id );
                if (memDetail != null) lblOffice.Text = memDetail.OfficePhone;
            }
            else
            {
                if (memberId > 0)
                {
                    Member member = Facade.GetMemberById(memberId);
                    if (member != null)
                    {
                        ControlHelper.SetHyperLink(lblEmployeeName, UrlForEmployee, string.Empty, MiscUtil.GetFullName(member.FirstName, member.MiddleName, member.LastName), UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(memberId), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString());

                        if (memberId != null)
                        {
                            lblPhone.Text = member.PrimaryPhone;
                            if (member.PrimaryPhone.Trim() != string.Empty && member.PrimaryPhoneExtension.Trim() != string.Empty) lblPhone.Text += " x" + member.PrimaryPhoneExtension;

                            lblMobile.Text = member.CellPhone;
                            ArrayList AccessArray = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                            if (AccessArray.Contains(602))
                            {
                                SecureUrl url = UrlHelper.BuildSecureUrl("../Employee/InternalEmailEditor.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, "600", UrlConstants.PARAM_MEMBER_ID, member.Id.ToString());
                                lblPrimaryEmail.Text = "<a href=" + url + ">" + member.PrimaryEmail + "</a>";
                            }
                            else lblPrimaryEmail.Text = "<a href=MailTo:" + member.PrimaryEmail + ">" + member.PrimaryEmail + "</a>";
                        }

                        MemberDetail memDetail = Facade.GetMemberDetailByMemberId(member.Id);
                        if (memDetail != null) lblOffice.Text = memDetail.OfficePhone;
                        if (memDetail != null) if (memDetail.OfficePhoneExtension.Trim() != string.Empty && memDetail.OfficePhone.Trim() != string.Empty) lblOffice.Text += " x" + memDetail.OfficePhoneExtension;
                        string empAdd = string.Empty;
                        empAdd += member.PermanentAddressLine1 + " " + member.PermanentAddressLine2;
                        empAdd += "<br/>";
                        string stateName = string.Empty;
                        if (member.PermanentStateId > 0) stateName = MiscUtil.GetStateNameById(member.PermanentStateId, Facade);
                        empAdd += (!string.IsNullOrEmpty(member.PermanentCity) ? member.PermanentCity : string.Empty);// +MiscUtil.GetCountryNameById(member.PermanentCountryId, Facade);
                        if (member.PermanentCity.Trim() != string.Empty && stateName != string.Empty) empAdd += ", ";
                        empAdd += stateName;
                        empAdd += " " + member.PermanentZip;
                        empAdd += "<br/>";
                        empAdd += member.PermanentCountryId > 0 ? MiscUtil.GetCountryNameById(member.PermanentCountryId, Facade) : "";
                        divEmployeeAddress.InnerHtml = empAdd;
                        lblrole.Text = Facade.GetCustomRoleNameByMemberId(memberId);
                        lblRequisitions.Text = Facade.GetRequisitionCountByMemberId(member.Id).ToString();
                        lblCandidates.Text = Facade.GetCandidateCountByMemberId(member.Id).ToString();
                    }
                }
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(360, CurrentMember.Id);
            if (CustomMap != null)
            {
                IdForSitemap = CustomMap.Id;
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }
            if (!IsPostBack)
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                }
                if (memberId > 0)
                {
                    LoadEmployeeData();
                }
            }
        }

        #endregion
    }
}