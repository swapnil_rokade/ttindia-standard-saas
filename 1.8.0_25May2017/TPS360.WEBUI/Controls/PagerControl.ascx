﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PagerControl.ascx.cs"
    Inherits="TPS360.Web.UI.PagerControl" EnableViewState="true" %>
<asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
<asp:HiddenField ID="hdnRowPerPageName" runat="server" />
<asp:HiddenField ID="hdnTotalRowCount" runat="server" />
<asp:HiddenField ID="hdnStartRowIndex" runat="server" />
<asp:HiddenField ID="hdnMaxRow" runat="server" />

<script type="text/javascript">

(function ($) {

     $.fn.numeric = function (options) {
         return this.each(function () {
             var  $this = $(this);

             $this.keypress(options, function (e) {
                 // allow backspace and delete 
                 if  (e.which == 8 || e.which == 0)
                     return  true;

                 //if the letter is not digit 
                 if  (e.which < 48 || e.which > 57)
                     return  false;

                 // check max range 
                 var dest = e.which - 48;
                 var result = this.value + dest.toString();
                 if  (parseInt (result,0) >parseInt ($(this).next().val()) || parseInt (result ,0)<=0) {
                     return  false;
                 }
             });
         });
     };
 })(jQuery);

function pageLoad()
{
try{
 $( '.pagerSlider' ).numeric({max:3});
 }
 catch (e)
 {
 }
} 

    //onkeydown="if (event.keyCode==13) {event.keyCode=9; return event.keyCode }

    function validate(source, arguments) {

        var hdnOldPageNo = document.getElementById('<%= hdnOldPageNo.ClientID %>');

        var hdntxtSliderClientID = document.getElementById('<%= hdntxtSliderClientID.ClientID %>');
        var txtSlider = document.getElementById(hdntxtSliderClientID.value);
        if (txtSlider != null) {
            if (hdnOldPageNo.value == txtSlider.value) {
                arguments.IsValid = false; return;
            }

            arguments.IsValid = true;
        }

    }




    function entername() {
        var hdnOldPageNo = document.getElementById('<%= hdnOldPageNo.ClientID %>');
        var hdntxtSliderClientID = document.getElementById('<%= hdntxtSliderClientID.ClientID %>');
        var txtSlider = document.getElementById(hdntxtSliderClientID.value);
        if (txtSlider != null)
            hdnOldPageNo.value = txtSlider.value;
    }

</script>

<asp:Panel ID="pnlPager" runat="server" DefaultButton="btnNotVisible">
    <div style="display: none">
    
        <asp:Button ID="btnNotVisible" runat="server" ValidationGroup="vv" /></div>
    <asp:HiddenField ID="hdnNoOfRowsCookie" runat ="server" />
    <asp:HiddenField ID="hdnOldPageNo" runat="server" />
    <asp:HiddenField ID="hdntxtSliderClientID" runat="server" />
  
    <div class="row-fluid" style="margin-left: 0px; padding: 6px 4px;font-size:13px;">
        <asp:DataPager runat="server" ID="pager" PageSize="10" EnableViewState="true">
            <Fields>
                <asp:TemplatePagerField OnPagerCommand="PagerCommand">
                    <PagerTemplate>
                        <div style="float: left; width: 20%;text-align:left">
                            <div class="pull-left">
                                <asp:DropDownList ID="ddlRowPerPage" runat="server" OnSelectedIndexChanged="ddlRowPerPage_SelectedIndexChanged"
                                    EnableViewState="true" AutoPostBack="true" Width="60px">
                                    <asp:ListItem Value="10" Selected="True">10</asp:ListItem>
                                    <asp:ListItem Value="20">20</asp:ListItem>
                                    <asp:ListItem Value="50">50</asp:ListItem>
                                    <asp:ListItem Value="75">75</asp:ListItem>
                                    <asp:ListItem Value="100">100</asp:ListItem>
                                </asp:DropDownList>
                                rows per page
                            </div>
                        </div>
                        <div style="float:left;width:60%;text-align: center">
                            <div class="pagination" runat="server" id="divPager">
                                <ul>
                                    <li class="disabled">
                                        <asp:LinkButton ID="btnFirstDisabled"  runat="server" Visible='<%# Container.StartRowIndex<=0 %>'
                                            Enabled="false"><i class="customicon-firstpage-grey"></i></asp:LinkButton>
                                    </li>
                                    <li class="disabled">
                                        <asp:LinkButton ID="btnPreviousDisabled"  runat="server" Enabled="false" Visible='<%# Container.StartRowIndex<=0 %>'><i class="customicon-previouspage-grey"></i></asp:LinkButton>
                                    </li>
                                    <li>
                                        <asp:LinkButton ID="btnFirst"  CssClass="firstPagerLiBorder" runat="server" CommandName="First"
                                            ToolTip="First Page" CausesValidation="false" Visible='<%# Container.StartRowIndex>0 %>'><i class="customicon-firstpage-grey"></i></asp:LinkButton>
                                    </li>
                                    <li>
                                        <asp:LinkButton ID="btnPrevious"  runat="server" CommandName="Previous" ToolTip="Previous Page"
                                            CausesValidation="false" Visible='<%# Container.StartRowIndex>0 %>'><i class="customicon-previouspage-grey"></i></asp:LinkButton>
                                    </li>
                                    <li class="disabled"><a style="padding-top:2px;height:24px;background-color:#ffffff">Page
                                        <asp:TextBox ID="txtSlider"  runat="server"  Width="22" EnableViewState="true" Text='<%# 
                                                                                        Container.TotalRowCount > 0 
                                                                                            ?  Math.Ceiling(((double)(Container.StartRowIndex + Container.MaximumRows) / Container.MaximumRows)) 
                                                                                            : 0 
                                                                                        %>' AutoPostBack="true" onfocus='entername()'
                                            OnTextChanged="CurrentPageChanged" ValidationGroup="vv" ReadOnly='<%#  Container.StartRowIndex==0 && ( Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows) ) >= Container.TotalRowCount) %>'
                                            CssClass="pagerSlider" />
                                              <asp:HiddenField ID="hdnMaxPageNumber" runat ="server"  Value =  '<%# Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows)%>' />
                                        <asp:CustomValidator ID="cv" runat="server" ControlToValidate="txtSlider" ClientValidationFunction="validate"
                                            ValidationGroup="vv"></asp:CustomValidator>
                                        of
                                        <%# 
                                                                                    Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows)%>
                                    </a></li>
                                    <li class="disabled">
                                        <asp:LinkButton ID="btnNextDisabled" runat="server"  Enabled="false" Visible='<%#  Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows)  )>= Container.TotalRowCount%>'><i class="customicon-nextpage-grey"></i></asp:LinkButton></li>
                                    <li class="disabled">
                                        <asp:LinkButton ID="btnLastDisabled" runat="server" Enabled="false" Visible='<%# Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows) ) >= Container.TotalRowCount %>'><i class="customicon-lastpage-grey"></i></asp:LinkButton></li>
                                    <li>
                                        <asp:LinkButton ID="btnNext" runat="server"  CommandName="Next" ToolTip="Next Page"
                                            CausesValidation="false" Visible='<%#  Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows)  )< Container.TotalRowCount%>'><i class="customicon-nextpage-grey"></i></asp:LinkButton>
                                    </li>
                                    <li>
                                        <asp:LinkButton ID="btnLast" runat="server"   CommandName="Last" ToolTip="Last Page"
                                            CausesValidation="false" Visible='<%# Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows) ) < Container.TotalRowCount %>'><i class="customicon-lastpage-grey"></i></asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div style="float:right;width:20%;text-align:right;">
                            <div style="display: none;">
                                <%#  hdnTotalRowCount.Value  =Container.TotalRowCount.ToString () %>
                                <%#  hdnStartRowIndex.Value = Container.StartRowIndex.ToString ()%>
                                <%#  hdnMaxRow.Value = Container.MaximumRows.ToString ()%>
                              
                                
                            </div>
                            <div class="pull-right">
                                <%# Container.TotalRowCount > 0 ? Math.Ceiling(((double)(Container.StartRowIndex ) ))+1  : 1 %>
                                -
                                <%#Container.TotalRowCount > (Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows))) ? Math.Ceiling((double)(Container.StartRowIndex + Container.MaximumRows)) : Container.TotalRowCount%>
                                of
                                <%# Math.Ceiling((double)Container.TotalRowCount )%>
                            </div>
                        </div>
                    </PagerTemplate>
                </asp:TemplatePagerField>
            </Fields>
        </asp:DataPager>
    </div>
</asp:Panel>
