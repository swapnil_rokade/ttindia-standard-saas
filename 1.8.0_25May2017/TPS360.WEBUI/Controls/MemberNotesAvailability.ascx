﻿<!-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberNotesAvailability.ascx
    Description: This is the user control page used to display the availabilty of the member.
    Created By: pravin khot 
    Created On: 13/July/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------       

-->

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberNotesAvailability.ascx.cs"
    Inherits="TPS360.Web.UI.ControlMemberNotesAvailability" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>

<script type="text/javascript" language="javascript">
     
</script>

<div style="margin-left:100px">
    <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>  
   
 
    <div id="div11" class="TableRow">
        <div class="TableFormLeble" style="margin-left: -25%">
            <asp:Label ID="Label2" runat="server" Text="Category"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:DropDownList ID="ddlNoteCategory" runat="server" CssClass="CommonDropDownList"
                ValidationGroup="NoteInfo">
            </asp:DropDownList>
             <span class="RequiredField">*</span>
        </div>
   </div>
        
   <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 18%">
             <asp:CompareValidator ID="cvNoteCategory" ValidationGroup="NoteInfo" runat="server"
                        ControlToValidate="ddlNoteCategory" ErrorMessage="Please Select Note Category"
                        Operator="NotEqual" ValueToCompare="0" Display="Dynamic"></asp:CompareValidator>
        </div>
    </div>
    
   <%-- <div  id="div1" class="TableRow">
        <div class="TableFormLeble" style="margin-left: -52%">
            <asp:Label ID="lblNotes" runat="server" Text="Note"></asp:Label>:
        </div>
     </div>--%>
     
     <div class="TableRow">
        <div class="TableFormLeble" style="margin-left: -12%">
            <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="8" CssClass="CommonTextBox"
                ValidationGroup="NoteInfo" Width="350px" >
            </asp:TextBox>
            <span class="RequiredField">*</span>
        </div>       
    </div>
    

   
    <div  class="TableRow">
        <div class="TableFormValidatorContent" style="margin-top: 46% ;margin-left: 15%">
            <asp:RequiredFieldValidator ID="rfvDegree" runat="server" ControlToValidate="txtNotes"
                ErrorMessage="Please enter note." EnableViewState="False" Display="Dynamic" ValidationGroup="NoteInfo">
            </asp:RequiredFieldValidator>
        </div>
    </div>  
     </div> 
     
    <%--<div id="div2" class="TableRow" >--%>
   <div class="TableRow">
       <div class="TableFormLeble" style="margin-top: 1% ;margin-left: 15%" >
        <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" ValidationGroup="NoteInfo"
            OnClick="btnSave_Click" />
      </div>  
</div>
