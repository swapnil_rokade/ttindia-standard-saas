﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: AllEmailList.ascx.cs
    Description: This is the user control page used for sending e-mails
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sep-26-2008           Yogeesh Bhat        Defect ID: 8706; Changes made in Page_Load() method.
                                                             (Checked for Session["ReferenceLink"] if it is from referenceLink, 
                                                              updated _tomemberId to reference ID from session)
    0.2            Sep-30-2008           Yogeesh Bhat        Defect ID: 8856; Changes made in Page_Load(), btnReply_Click(), and
                                                             btnForward_Click() methods (Checked for listview items count)
    0.3            Oct-17-2008           Jagadish            Defect ID: 8969; Changed the parameter "Reply" to "Forward" in btnForward_Click() method.
    0.4            Nov-26-2008           Jagadish            Defect id: 8813; When a 'Delete icon' is clicked at 'Action' column header then multiple 'Radio buttons' were selected
                                                             Changed the code to fix that.
    0.5            Dec-05-2008           Jagadish            Defect id: 9358; changed case of variable id's from 'ReceiverId' and
                                                             'SenderId' to 'receiverId' and 'senderId' and '_tomemberId' to '_frommemberId'.
    0.6            Jan-02-2008           Jagadish            Defect id: 9518; changes made in btnForward_Click() method and btnReply_Click() method.
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Linq;
using System.Web.UI;
namespace TPS360.Web.UI
{
    public partial class DateRangePicker : BaseControl
    {

        public DateTime StartDate
        {
            get
            {
                if (hdnDateRange.Value == "Select Date Range")
                    return DateTime.MinValue;
                else
                {
                    string[] dates = hdnDateRange.Value.Split('-');
                    if (dates.Length > 1) return Convert.ToDateTime(dates[0]);
                    else return DateTime.MinValue;
                }
            }
        }


        public void setDateRange(DateTime StartDate, DateTime EndDate)
        {
            if (StartDate == DateTime.MinValue || EndDate == DateTime.MinValue)
                ClearRange();
            else hdnDateRange.Value = lblDateRange.Text = hdnDateRange.Value = StartDate.ToShortDateString() + " - " + EndDate.ToShortDateString();

        }

        public DateTime EndDate
        {
            get
            {

                if (hdnDateRange.Value == "Select Date Range")
                    return DateTime.MinValue;
                else
                {
                    string[] dates = hdnDateRange.Value.Split('-');
                    return Convert.ToDateTime(dates[1]);
                }
            }
        }

        public void ClearRange()
        {
            hdnDateRange.Value = lblDateRange.Text = "Select Date Range";

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            lblDateRange.Text = hdnDateRange.Value;
            System.Globalization.DateTimeFormatInfo info = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat;
            if (info.ShortDatePattern == "MM/dd/yyyy") hdnDateTimeFormat.Value = info.ShortDatePattern;
            else hdnDateTimeFormat.Value = hdnDateTimeFormat.Value = "MM/dd/yyyy";


            //hdnDateTimeFormat.Value = info .
        }
    }
}