﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ApplicationMenu.ascx.cs
    Description: This is the user control page used for main menu and sub menu functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Sep-11-2008          Yogeesh Bhat        Defect ID: 8642; changes made in function BuildSubMenu()
    0.2             Feb-11-2009          N.Srilakshmi        Defect ID; 9930; changes made in BuildSubMenu,rptTopMenu_ItemDataBound
    0.3             Feb-23-2009         N.Srilakshmi         Defect Id; 9976; changes made in PublishSecondaryMenu()
    0.4             Feb-24-2008         Kalyani Pallagani    Defect ID: 9755; changes made in function BuildSubMenu
 *  0.5             Mar-05-2009         Rajendra Prasad A.S  Defect ID: 9751; changes made in function BuildSubMenu
-------------------------------------------------------------------------------------------------------------------------------------------       
 
*/

using System;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Security;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Providers;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using System.Text.RegularExpressions;
namespace TPS360.Web.UI
{
    public partial class ApplicationMenu : BaseControl
    {
        #region Veriables

        ArrayList _permittedMenuIdList;
        int counter = 1;
        string script = "\n\t<script language='javascript' type='text/javascript'>";

        string innerHtml = string.Empty;

        string _currentParentNode = string.Empty;
        string _currentNode = string.Empty;

        StringBuilder sb = new StringBuilder();

        #endregion

        #region Methods

        private bool IsInPermitedMenuList(int menuId)
        {
            if (_permittedMenuIdList == null)
            {
                _permittedMenuIdList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            }

            if ((_permittedMenuIdList != null) && (_permittedMenuIdList.Count > 0))
            {
                if (_permittedMenuIdList.Contains(menuId))
                {
                    return true;
                }
            }

            return false;
        }

        private void BuildJavaScriptMethods()
        {
            //HtmlGenericControl placeHolder = (HtmlGenericControl)this.Page.Master.FindControl("divScriptHolder");

            //if (placeHolder == null)
            //{
            //    placeHolder = (HtmlGenericControl)this.Page.Master.Master.FindControl("divScriptHolder");
            //}

            //string currentParentNode = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
            //string currentNode = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_ID].ToString();

            //SqlSiteMapProvider provider = SiteMap.Providers["SqlSiteMap"] as SqlSiteMapProvider;
            //SiteMapNode node = provider.FindSiteMapNodeFromKey(currentNode);

            //if (node != null && node.Url.IsNotNullOrEmpty())
            //{
            //    placeHolder.InnerHtml = script + "initalizetab('" + maintab.ClientID + "', '{0}', '{1}', '{2}');\n\t</script>\n".Fill("tab" + currentParentNode, "subTab" + currentNode, "true");
            //}
            //else
            //{
            //    placeHolder.InnerHtml = script + "initalizetab('" + maintab.ClientID + "', '{0}', '{1}', '{2}');\n\t</script>\n".Fill("tab" + currentParentNode, "subTab" + currentNode, "false");
            //}            
        }

        //private void PublishSecondaryMenu()
        //{

        //    sb.Append("<div id=\"ChildMenuBar\">");

        //    sb.Append("<div id='scroller' style='overflow: hidden; white-space: nowrap;  '>" + innerHtml + "</div>");

        //    sb.Append("</div>");

        //    secondaryMenuHolder.InnerHtml = sb.ToString();
        //}

        private void BuildMenu()
        {
            if (CurrentMember == null || CurrentUser == null)
            {
                return;
            }

            SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();

            SqlSiteMapProvider provider = SiteMap.Providers["SqlSiteMap"] as SqlSiteMapProvider;
            int topmenuid=0;
            if (Request.Url.ToString().ToLower().Contains("/vendor/"))
                topmenuid = (int)SiteMapType.VendorPortalTopMenu;
            else topmenuid = (int)SiteMapType.ApplicationTopMenu;
            SiteMapNode root = provider.FindSiteMapNodeFromKey(StringHelper.Convert(topmenuid ));

            if (root != null)
            {
                SiteMapNodeCollection nodeList = root.ChildNodes;

                if (nodeList != null && nodeList.Count > 0)
                {
                    foreach (SiteMapNode node in nodeList)
                    {

                        if (IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
                        {
                            permittedNodeList.Add(node);
                        }
                    }
                }

                rptTopMenu.DataSource = permittedNodeList;
                rptTopMenu.DataBind();

                //BuildJavaScriptMethods();
                //PublishSecondaryMenu();
            }
        }

        protected string BuildUrl(string url)
        {
            bool isMainApplication = false;

            switch (ApplicationSource) 
            {
                case ApplicationSource.LandTApplication:
                    isMainApplication = false;
                    break;
                case ApplicationSource.MainApplication:
                    isMainApplication = true;
                    break;
                case ApplicationSource.SelectigenceApplication:
                    isMainApplication = true;
                    break;
                case ApplicationSource.GenisysApplication:
                    isMainApplication = true;
                    break;
                default:
                    isMainApplication = true;
                    break;
            }

            if (isMainApplication && url.Contains("sitemapid=666"))
            {
                //url.Replace("OfferandJoined.aspx", "CommonOfferandJoined.aspx");
                string newUrl = Regex.Replace(url, "(OfferandJoined.aspx)", "CommonOfferandJoined.aspx", RegexOptions.None);
                url = newUrl;
            }
            
            string pageUrl = url;
            pageUrl = (new SecureUrl(pageUrl)).ToString();

            return pageUrl;
        }

        private string BuildSubMenuDropDown(SiteMapNode activeNode, string secondaryManuName)
        {
            SiteMapNodeCollection nodeList = activeNode.ChildNodes;

            StringBuilder subMenuSb = new StringBuilder();

            if (nodeList != null && nodeList.Count > 0)
            {
                foreach (SiteMapNode node in nodeList)
                {
                    if (IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
                    {
                        string strDescription = "'" + node.Description.Replace(" ", " ") + "'";
                        string strActiveClass = node["Id"].Equals(_currentNode)? " class=\"active\"" : "";
                        subMenuSb.Append("<li EnableViewState=\"true\" " + strActiveClass + "><a href=\"../" + BuildUrl(node.Url) + "\" title=\"" + strDescription + "\">" + node.Title + "</a></li>");
                    }
                }
            }

            return subMenuSb.ToString();
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            _currentParentNode = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
            if (_currentParentNode == "658") _currentParentNode = "653";
            _currentNode = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_ID].ToString();
            BuildMenu();
        }

        protected void lgs_LogOut(object sender, EventArgs e)
        {
            string role = (Roles.GetRolesForUser(base.CurrentUserName)).GetValue(0).ToString();
            DateTime dtLogoutTime = DateTime.Now;
            MiscUtil.AddActivity(role, base.CurrentMember.Id, base.CurrentMember.Id, ActivityType.Logout, Facade);

            MembershipUser membershipUser = Membership.GetUser(base.CurrentUser.UserName, false);
            if (membershipUser != null)
            {
                Facade.UpdateAllMemberDailyReportByLoginTimeAndMemberId(membershipUser.LastLoginDate, base.CurrentMember.Id);
            }
        }

        protected void rptTopMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (ControlHelper.IsItemDataRow(e.Item.ItemType))
            {
                SiteMapNode siteMapNode = (SiteMapNode)e.Item.DataItem;

                string submenuName = "objMenuItem" + siteMapNode["Id"].ToString();
                string secondaryMenuName = "tab" + siteMapNode["Id"].ToString();

                Label labelMenuName = (Label)e.Item.FindControl("lblMenuName");

                HtmlGenericControl li = (HtmlGenericControl)e.Item.FindControl("liId");
                li.Attributes.Add("rel", secondaryMenuName);

                //HtmlAnchor linkMenuName = (HtmlAnchor)e.Item.FindControl("lnkMenuName");
                //if (siteMapNode.Url.IsNotNullOrEmpty())
                //{
                //    linkMenuName.HRef = BuildUrl("../" + siteMapNode.Url);
                //}
                //else
                //{
                //    linkMenuName.Attributes.Add("onClick", "javascript:return false;");
                //}
                labelMenuName.Text = siteMapNode.Title;
                labelMenuName.ToolTip = siteMapNode.Description;

                if (siteMapNode["Id"].Equals(_currentParentNode))
                {
                    li.Attributes["class"] += " active";
                }


                HtmlGenericControl subUl = (HtmlGenericControl)e.Item.FindControl("ulSubMenu");
                subUl.InnerHtml = this.BuildSubMenuDropDown(siteMapNode, secondaryMenuName);

                counter++;
            }
        }

        #endregion
    }
}