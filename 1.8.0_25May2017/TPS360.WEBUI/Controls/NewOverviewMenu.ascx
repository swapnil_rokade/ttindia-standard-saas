﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: OverviewMenu.ascx
    Description: This is the user control page used to display overview menu.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Mar-5-2009           Jagadish            Defect id: 10012; Applied scrolling effect for control 'tabList'.
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewOverviewMenu.ascx.cs" Inherits="TPS360.Web.UI.NewOverviewMenu" EnableViewState="true"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ul class="sidemenu" runat ="server"  id="MenuList">   
                    
                </ul>
    <div id="divScriptHolder" runat="server">
    </div>
    <asp:SiteMapDataSource runat="server" ID="MenuSiteMap" ShowStartingNode="false" />

    
    