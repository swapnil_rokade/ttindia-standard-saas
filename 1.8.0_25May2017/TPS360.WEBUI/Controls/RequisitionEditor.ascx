﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RequisitionEditor.ascx
    Description: This is the user control page used to create new requisition.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Sep-24-2008          Jagadish            Defect Id: 8674. Changed the label text form "Tele Communication" to 
                                                             "Telecommuting".    
    0.2             Oct-7-2008           Gopala Swamy J      Defect Id: 8962 I put one anchor tag which opens another window in separate browser
    0.3             Apr-02-2009          Rajendra Prasad     Defect ID: 10242; Added textbox in the place of dropdownlist to display No. of openings.                                                             
    0.4             Apr-02-2009          Sandeesh            Defect ID: 9226; Added validation control to validate the 'start date'          
    0.5             Apr-06-2009          Jagadish            Defect id: 10191; Restricted zip code text field to 6 characters.     
    0.6             Apr-13-2009          Rajendra Prasad     Defect ID: 10299; Added Range Validator for the Textbox "txtNoOfOpenings".                                                             
    0.7             Apr-20-2009             Nagarathna       DEfectId:9200 Added customvalidator. 
    0.8             May-13-2010          Sudarshan.R.        DefectID: 12809; changed min and max experience validator from integer to double
--------------------------------------------------------------------------------------------------------------------------------------------
--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionEditor.ascx.cs"
    Inherits="TPS360.Web.UI.cltRequisitionEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>
<%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/Controls/MultipleItemPicker.ascx" TagName="MultipleSelection"
    TagPrefix="ucl" %>
<link href="../assets/css/chosen.css" rel="Stylesheet" />
<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.0.min.js"></script>
<style>
    .text-label
    {
        color: #cdcdcd;
        font-weight: bold;
        font-size: small;
    }
</style>
<style>
    .EmailDiv
    {
        vertical-align: middle;
        text-align: left;
        border: solid 1px #CCC;
        padding-bottom: 2px;
        padding-top: 2px;
        margin-right: 4px;
        margin-bottom: 4px;
        width: 80%;
        background-image: -moz-linear-gradient(center top , #FFFFFF 20%, #F6F6F6 50%, #EEEEEE 52%, #F4F4F4 100%);
        border-radius: 3px;
    }
</style>
<%--<script runat ="server" >
    void ValidateEducationList(object sender, EventArgs e)
    {
        if (uclEducationList.ListItem.Items.Cast<ListItem>().Where(x => x.Selected == true).Count() == 0)
        {
            cvEduQualification.IsValid = false;
        }
    }
</script>--%>
<script language="javascript" type="text/javascript">
$(document).ready(
            function() {
            function thisFunction() {
            var Opendate = $find("<%=wdcOpenDate.ClientID%>").get_value();
            var Enddate = $find("<%=wdcClosingDate.ClientID%>").get_value();
            
            if(Enddate==null)
            {
                var numberOfDaysToAdd = 30;
                var TempDate = new Date();
                TempDate.setDate(Opendate.getDate() + numberOfDaysToAdd);
                var dd = TempDate.getDate();
                var mm = TempDate.getMonth() + 1;
                var y = TempDate.getFullYear();

                var FormattedDate = mm + '/'+ dd + '/'+ y;
                $find("<%=wdcClosingDate.ClientID%>").set_value(FormattedDate);
                }
            }
   $(window).load(thisFunction);
 }   
);
</script>
<script language="javascript" type="text/javascript">

    
function Clicked(email,parent)
{
  //$('#<%= hfSkillSetNames.ClientID %>').val($('#<%= hfSkillSetNames.ClientID %>').val().replace('<item><skill>' +email + '</skill></item>','').replace(',,',','));
  $('#<%= hfSkillSetNames.ClientID %>').val($('#<%= hfSkillSetNames.ClientID %>').val().replace('<item><skill>' +email + '</skill></item>','').replace(',,',','));

}
function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

function SkillAlreadyAdded(skill)
{
var result=false ;
  // var skilllist= $('#<%= hfSkillSetNames.ClientID %>').val();
  var skilllist= $('#<%= hfSkillSetNames.ClientID %>').val().replace(/&/g,"-");
     xmlDoc = $.parseXML("<SkillList>"+ skilllist+"</SkillList>"  );
    var $xml = $( xmlDoc );

      $xml.find( "item" ).each(function (){
      
           // if($(this).find('skill').text().trim().toLowerCase()==skill .trim().toLowerCase()) result =true ;
		   if($(this).find('skill').text().trim().toLowerCase()==skill .trim().toLowerCase().replace(/&/g,"-")) result =true ;
      });
      return result ;
}




   Sys.Application.add_load(function() { 
   $('.close').click(function (){
      alert ($(this).previous().val());
   });
    $('#<%= btnAddSkill.ClientID %>').click(function (){ 
   
    var txtSkillName=$('#<%= txtSkillName.ClientID %>');
    if(txtSkillName.val().trim()!='' )
    {

    if(!SkillAlreadyAdded(txtSkillName.val()))
     {
        var text=txtSkillName.val();
        $('#<%= divContent.ClientID %>').append("<div class='EmailDiv'><span>"+text+"</span><button type='button' class='close' data-dismiss='alert' onclick=javascript:Clicked('" +text+"')>\u00D7</button></div>");
        $('#<%= hfSkillSetNames.ClientID %>').val($('#<%= hfSkillSetNames.ClientID %>').val() + '<item><skill>'+  text + '</skill></item>' );
        txtSkillName.val('');
        }
        else ShowMessage('<%= lblMessage.ClientID %>','Skill already added',true );

    }
    return false ;
    });

      
   });
   
    
    function RemoveRow(row)
    {

        $(row).parent().parent().remove();
    }

Sys.Application.add_load(function() { 
  $('input[rel=Numeric]').keypress(function (event){return allownumeric(event,$(this));});
  $('input[rel=Integer]').keypress(function (event){return allowInteger(event,$(this));});
   // SetDefaultText (document .getElementById ('<%=txtSalary.ClientID %>'));
            //     SetDefaultText (document .getElementById ('<%=txtMaxPayRate.ClientID %>'));
});
function RemoveDefaultText(e)
{
    if(e.value == e.title){e.value = ''; e.className='CommonTextBox';}
}
function SetDefaultText(e)
{
    if (e.value == ''){ e.value = e.title;
     e.className='text-label';}
}
function ValidateSkill(souce , eventArgs)
{
    if( $('#<%= hfSkillSetNames.ClientID %>').val()=='')
        eventArgs .IsValid=false ;
}
  function CompanyContact_Selected(source, eventArgs) {
   $('#divContactEmaill').css({'display':'none'});
   
        hdnSelectedContactText = document.getElementById("<%= hdnSelectedContactText.ClientID%>");
        hdnSelectedContactValue = document.getElementById("<%= hdnSelectedContactValue.ClientID%>");
        hdnSelectedContactText.value = eventArgs.get_text();
        hdnSelectedContactValue.value = eventArgs.get_value();
        //txtContactEmail.Text = eventArgs.get_text();
    }
function ValidatePayRate()
{

}
          
          function MakeEnableDisable(option, control,clearSelectIndex)
          {
            var _Control = document.getElementById(control);
            _Control.disabled = option;
            if(option && clearSelectIndex)
            {
                _Control.selectedIndex=0;
            }
          }



        function checkEnter(e)
         {
         try{
                var keycode;                                        
                if (window.event) keycode = window.event.keyCode;
                else if (event) keycode = event.keyCode;
                else if (e) keycode = e.which;
                else return true;
                
                if( (keycode==13))
                {
                    var s;
                    s=document.getElementById(btnSave);
                    s.UseSubmitBehaviour=true ;

                    //__doPostBack('btnSave','OnClick');
                    return true;
                }
                else
                {
                    return false;
                }
                return true;
                }
                catch (e1)
                {
                }
             }
          
    //0.7 statrs
        
    function fnValidateSalary(source,args) 
    {
      // debugger;
         
        var txtValidCurrency = $get('<%= txtSalary.ClientID %>').value.trim();            
       if((txtValidCurrency != ""  && (!isNaN(txtValidCurrency) || txtValidCurrency==$get('<%= txtSalary.ClientID %>').title))) // || (txtValidMaxCurrency != "" && txtValidMaxCurrency > 0 && !isNaN(txtValidMaxCurrency)))   
       {  
            if(args.Value == 0)
            args.IsValid = false;
            else
            args.IsValid = true; 
       }      
       else  args.IsValid=false ;  
    }
      
    function fnValidateMaxPayRate(source,args) 
    {
      // debugger;
       var txtValidCurrency = $get('<%= txtMaxPayRate.ClientID %>').value.trim();  
       if((txtValidCurrency != "" && (!isNaN(txtValidCurrency) || txtValidCurrency==$get('<%= txtMaxPayRate.ClientID %>').title))) // || (txtValidMaxCurrency != "" && txtValidMaxCurrency > 0 && !isNaN(txtValidMaxCurrency)))   
       {  
            if(args.Value == 0)
            args.IsValid = false;
            else
            args.IsValid = true; 
       }      
       else  args.IsValid=false ;  
    }

    function FilterNumeric(txtID)
    {
    
      var txt=document .getElementById (txtID );
         var t=txt.value+'';
        for(var i=0;i<t.length;i++)
        {
          var st=t.substr(i,1);
          if(st >=0 && st<=9)  continue ;
          else {txt.value=""; break ;}
        }
       
    }
   
    function numeric_only(evt)
    {
	    var e = event || evt; // for trans-browser compatibility
	    var charCode = e.which || e.keyCode;

	    if (charCode > 31 && (charCode < 48 || charCode > 57))
		    return false;

	    return true;

    }
     function Decimal_only(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode;
         if ((charCode > 31 && (charCode < 48 || charCode > 57 ) && charCode !=46) )
            return false;

         return true;
      }
      
      
     function LockWheel()
     {
         document.onmousewheel = function(){ stopWheel(); } /* IE7, IE8 */
            if(document.addEventListener){ /* Chrome, Safari, Firefox */
            document.addEventListener('DOMMouseScroll', stopWheel, false);
                }
     }
     
     function stopWheel(e)
     {
        if(!e){ e = window.event; } /* IE7, IE8, Chrome, Safari */
        if(e.preventDefault) { e.preventDefault(); } /* Chrome, Safari, Firefox */
        e.returnValue = false; /* IE7, IE8 */
     }
    function ContactfocusLost()
    {

        if($('#<%=hdnSelectedContactText.ClientID %>').val()!=$('#<%=txtBUcontact.ClientID %>').val())
        {
           $('#<%=hdnSelectedContactValue.ClientID %>').val('0');
        }
        var hdnSelectedClientValue=document .getElementById ('<%=hdnSelectedContactValue.ClientID %>');
        if(hdnSelectedClientValue.value.trim()=='' || hdnSelectedClientValue.value=='0') {
           
//            $('#divContactEmaill').css({'display':''});
//            $('#<%=txtContactEmail.ClientID %>').focus();
            //$('#<%=txtBUcontact.ClientID %>').val('');  
        }
    }
     function ReleaseWheel()
     {
            document.onmousewheel = null;  /* IE7, IE8 */
            if(document.addEventListener)
            { /* Chrome, Safari, Firefox */
                document.removeEventListener('DOMMouseScroll', stopWheel, false);
            }
    }    
    
    function RemoveCalender() {    
    
        //var control = $find('<%= wdcStartDate.ClientID %>');  
        var control = document.getElementById('<%= wdcStartDate.ClientID %>');  
        control.visible=false;
        
    }
    //for LandT
    function ValidateChkList(source, args)
    {
       var chkListaTipoModificaciones= document.getElementById ('<%= ddlQualification.ClientID %>');
       var chkLista= chkListaTipoModificaciones.getElementsByTagName("input");
       for(var i=0;i<chkLista.length;i++)
        {  
            if(chkLista[i].checked)
            {
                args.IsValid = true;
                return;
            }
        }
      args.IsValid = false;
    } 
    //for LandT
    Sys.Application.add_load(function() {

        //  $('.TableRow').show();
        // $('#divContactEmail').css({'display':'none'});

        if ($('#<%=hdnApplicationType.ClientID %>').val() == '') {
            $('.RequiredField').css({ 'display': 'none' });
            $('#spjobtitle').css({ 'display': '' });
            $('#SpContactEmail').css({ 'display': '' });
            $('#SpBUcontact').css({ 'display': '' });
            $('#SpNoOfOpenings').css({ 'display': '' });
            $('#Spopendate').css({ 'display': '' });
            $('#Spclosedate').css({ 'display': '' });
            $('#Spexperience').css({ 'display': '' });
            $('#SpStartDate').css({ 'display': '' });
            $('#SpBUContacts').css({ 'display': '' });          
        }
    });
    //For BU contact
   Sys.Application.add_load(function() {
   var hdnSelectedClientValue=document .getElementById ('<%=hdnSelectedContactValue.ClientID %>');
   $('#<%=ddlBU.ClientID %>').change(function() {
   if($('#<%=ddlBU.ClientID %>').prop("selectedIndex")==0){
   $('#<%=txtBUcontact.ClientID %>').prop("disabled",true); 
   $('#<%=txtBUcontact.ClientID %>').val('');
   $('#<%=hdnSelectedContactText.ClientID %>').val('');
   if(hdnSelectedClientValue.value.trim()=='' || hdnSelectedClientValue.value=='0')
   {
            $('#divContactEmaill').css({'display':'none'});
            $('#<%=txtContactEmail.ClientID %>').val('');         
   }
   
   }else{
            $('#<%=txtBUcontact.ClientID %>').prop("disabled",false);
            $('#<%=txtContactEmail.ClientID %>').attr("disabled", false); 
   
   }
   });
   
   });
    
    
    function RemoveRow(row)
    {
    
      $(row).parent().parent().remove();
    }
    function ClientSelected(source, eventArgs) {
     
        hdnSelectedClient=document .getElementById ('<%= hdnSelectedClient.ClientID %>');
        hdnSelectedClientText=document .getElementById ('<%=hdnSelectedClientText.ClientID %>');
        hdnSelectedClientText.value=eventArgs.get_text();
        hdnSelectedClient.value=eventArgs.get_value(); 
       
    }
    function ValidateExperience(source, eventArgs)
    {
        var MinExp= document.getElementById ('<%= txtExpRequiredMin.ClientID %>');
        var MaxExp= document.getElementById ('<%= txtExpRequiredMax.ClientID %>');
        if(MinExp.value!=''||MaxExp.value!='')
        {
            eventArgs.IsValid=true;
        }else
        {
            eventArgs.IsValid=false;
        }

    }
    function ValidateBUemail(source, eventArgs) {
        var BUcontact = document.getElementById('<%= txtBUcontact.ClientID %>');
        var ContactEmail = document.getElementById('<%= txtContactEmail.ClientID %>');
        var hdnSelectedClientValue = document.getElementById('<%=hdnSelectedContactValue.ClientID %>');

        if (hdnSelectedClientValue.value.trim() == '' || hdnSelectedClientValue.value == '0') {
            if (ContactEmail.value == '') {
               
                eventArgs.IsValid = false;
            }
            else {

                if (BUcontact.value == '') {
                    eventArgs.IsValid = false;
                    $('#<%=txtBUcontact.ClientID %>').focus();                    
                }
                else {
                    eventArgs.IsValid = true;
                }
            }
        }
        else {
            
            eventArgs.IsValid = true;
        }
    }

    function ValidateContactemail(source, eventArgs) {
        var BUcontact = document.getElementById('<%= txtBUcontact.ClientID %>');
        var ContactEmail = document.getElementById('<%= txtContactEmail.ClientID %>');
        var hdnSelectedClientValue = document.getElementById('<%=hdnSelectedContactValue.ClientID %>');

        if (hdnSelectedClientValue.value.trim() == '' || hdnSelectedClientValue.value == '0') {
            if (ContactEmail.value == '') {

                eventArgs.IsValid = false;
            }
            else {

                eventArgs.IsValid = true;
            }
        }
        else {

            eventArgs.IsValid = true;
        }
    }

    
    
function wdcOpenDate_ValueChanged(sender, eventArgs) {
//var Opendate = $find("<%=wdcOpenDate.ClientID%>").get_value();
////    var TempDate= new Date(Opendate);
////    var numberOfDaysToAdd = 30;
////    TempDate.setDate(Opendate.getDate() + numberOfDaysToAdd);
////    var temp=TempDate.getDate();
////    if(temp>30)
////    {
////    //TempDate.setMonth(Opendate.getMonth());
////    var mm = TempDate.getMonth()+1;
////    }
////    else
////    {
////    TempDate.setMonth(Opendate.getMonth()+2);
////    var mm = TempDate.getMonth();
////    }
////    var dd = TempDate.getDate();
////   
////   // var mm = TempDate.getMonth();
////    var y = TempDate.getFullYear();

////    var FormattedDate = dd + '/'+ mm + '/'+ y;
////    alert(FormattedDate);
////    $find("<%=wdcClosingDate.ClientID%>").set_value(FormattedDate);
////    
    var Opendate = $find("<%=wdcOpenDate.ClientID%>").get_value();
    var TempDate= new Date(Opendate);
    var numberOfDaysToAdd = 30;
    TempDate.setDate(Opendate.getDate() + numberOfDaysToAdd);
    
    var dd = TempDate.getDate();
    var mm = TempDate.getMonth() + 1;
    var y = TempDate.getFullYear();

    var FormattedDate = dd + '/'+ mm + '/'+ y;
    
    $find("<%=wdcClosingDate.ClientID%>").set_value(FormattedDate);
//    
    
}

  //0.7  ends
</script>

<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
<meta name="CODE_LANGUAGE" content="C#">
<meta name="vs_defaultClientScript" content="JavaScript">
<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">


<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<asp:HiddenField ID="hdnApplicationType" runat="server" Value="LandT" />
<asp:HiddenField ID="hdnSelectedClientText" runat="server" />
<asp:HiddenField ID="hdnSelectedClient" runat="server" Value="0" />
<div style="margin: 0 auto;">
    <asp:Panel ID="pnlRequisition" runat="server">
        <div class="TableRow" style="text-align: left" id="div" runat="server">
            <asp:UpdatePanel ID="l" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="TableRow">
            <div class="FormLeftColumn">
                <%--Added for LandT--%>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblBU" runat="server" Text="BU"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="ddlBU" runat="server" CssClass="chzn-select" TabIndex="1" style="width:270px;" AutoPostBack="true" OnSelectedIndexChanged="ddlBU_SelectedIndexChanged">
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvBU" runat="server" ControlToValidate="ddlBU" ErrorMessage="Please Select BU."
                            InitialValue="0" Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--Added for LandT--%>
                
                  <%--*****************Added by pravin khot on 9/Aug/2016******************************--%>
                  <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblBUContacts" runat="server" Text="BU Contact"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="ddlBUContacts" runat="server" CssClass="chzn-select" style="width:170px;" TabIndex="1" >
                        </asp:DropDownList>
                        <span class="RequiredField" id="SpBUContacts">*</span>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="RFVBUContacts" runat="server" ControlToValidate="ddlBUContacts" ErrorMessage="Please Select BU Contact."
                            InitialValue="0" Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
              <%--  *************************end*********************--%>
              
                
                <div id="divBUContact" class="TableRow" runat="server" visible="true" >
                    <div class="TableFormLeble">
                        <asp:Label ID="lblBUContact" runat="server" Text="BU Contact"></asp:Label>:
                    </div>
                    <%--<div class="TableFormContent" style="vertical-align: top">
                <asp:DropDownList ID="ddlBUContact" TabIndex="2" runat="server" CssClass="chzn-select">
                </asp:DropDownList>
                <asp:HiddenField ID="hdnSelectedContactID" runat="server" />
                <span class="RequiredField">*</span>
            </div>--%>
                    <div class="TableFormContent">
                        <div id="divBUcontact">
                        </div>
                        <asp:HiddenField ID="hdnSelectedContactValue" runat="server" />
                        <asp:HiddenField ID="hdnSelectedContactText" runat="server" />
                        <asp:HiddenField ID="hfBUcontact" runat="server" Value="" />
            
                       <asp:TextBox ID="txtBUcontact" runat="server" AutoPostBack="false" CssClass="CommonTextBox"
                            TabIndex="2" AutoComplete="OFF" onclick="LockWheel();" onBlur="ReleaseWheel(); ContactfocusLost();" />
                        <ajaxToolkit:AutoCompleteExtender ID="BUAutoComplete" runat="server" ServicePath="~/AjaxExtender.asmx"
                            TargetControlID="txtBUcontact" MinimumPrefixLength="1" ServiceMethod="GetContact"
                            EnableCaching="False" CompletionInterval="0" CompletionListCssClass="AutoCompleteBox"
                            FirstRowSelected="True" CompletionListElementID="divBUcontact" OnClientItemSelected="CompanyContact_Selected" />
                        <span class="RequiredField" id="SpBUcontact">*</span>
                    </div>
                </div>
                <div class="TableRow">
                   <div class="TableFormValidatorContent" style="margin-left: 42%">   
                   
                   <asp:CustomValidator runat="server" Display="Dynamic" ValidationGroup="ValGrpJobTitle"
                            ID="rfvBUcontact" ErrorMessage="Please Enter BU Contact" ClientValidationFunction="ValidateBUemail">
                        </asp:CustomValidator>

                   
                                            
<%--                       <asp:RequiredFieldValidator ID="rfvBUcontact" runat="server" ControlToValidate="txtBUcontact"
                            ErrorMessage="Please Enter BU Contact." Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
--%>                    </div>                    
                </div>
              
               
              <%-- <div class="TableRow">
                   <div class="TableFormValidatorContent" style="margin-left: 42%">                           
                         <asp:CustomValidator id="rfvBUcontact" runat="server"
                         ErrorMessage="CompanyTagNo or EmployeeTagNo Required"
                         Display="Dynamic"  ValidationGroup="ValGrpJobTitle"
                         ClientValidationFunction="CompanyTagOrEmployeeTag()" />  
                   </div>                    
                </div>--%>
               
             <%--  --%>
              
                 
                <div id="divContactEmaill"  >
                    <div class="TableFormLeble">
                        <asp:Label ID="lblcontactemail" runat="server" Text="New Contact Email"></asp:Label>:
                    </div>
                    <div class="TableFormContent">

                        <asp:TextBox ID="txtContactEmail" runat="server" EnableViewState="false"
                         OnChange='CheckUserAvailability()' AutoComplete="OFF" CssClass="CommonTextBox" 
                         TabIndex="1" ValidationGroup="ValGrpJobTitle"></asp:TextBox>
                        <span class="RequiredField" id="SpContactEmail">*</span>
                    </div>                    
                        <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <div id="divAvailableUser" style="color: Green; display: none" enableviewstate="true">
                            User name available<br /></div>
                        <div id="divNotAvailable" style="color: Red; display: none" enableviewstate="true">
                            User name not available<br /></div>
                          <asp:RegularExpressionValidator ID="revUserName" runat="server" ControlToValidate="txtContactEmail"
                        Display="Dynamic" ErrorMessage="Please enter valid email address." ValidationExpression="\w+([-+.!#$%&*+-/=?^_`{|}~'\\\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        SetFocusOnError="true" ValidationGroup="ValGrpJobTitle"></asp:RegularExpressionValidator>
                        
                        <asp:CustomValidator runat="server" Display="Dynamic" ValidationGroup="ValGrpJobTitle"
                            ID="rfvcontactemail" ErrorMessage="Please Enter new Contact Email" ClientValidationFunction="ValidateContactemail">
                        </asp:CustomValidator>

                       
                        </div> 
                        </div>  
                        
                         <%--<div class="TableRow">
                   <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvcontactemail" runat="server" ControlToValidate="txtContactEmail"
                            ErrorMessage="Please Enter Contact Email." Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>                    
                </div>   --%>                     
              </div>                      
                  
                  
                  
                   
                        
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblNoOfOpenings" runat="server" Text="No. of Openings"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtNoOfOpenings" runat="server" CssClass="CommonTextBox" MaxLength="4" AutoPostBack="true" OnTextChanged="txtNoOfOpenings_TextChanged"
                            Width="50px" TabIndex="3" ValidationGroup="ValGrpJobTitle" rel="Integer"></asp:TextBox>
                            

                            <span class="RequiredField" id="SpNoOfOpenings">*</span>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableRow">
                        <div class="TableFormValidatorContent" style="margin-left: 42%">
                            <asp:RequiredFieldValidator ID="rfvnoofopenings" runat="server" ControlToValidate="txtNoOfOpenings"
                                SetFocusOnError="true" ErrorMessage="Please Enter No Of Openings." Display="Dynamic"
                                ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="TableFormValidatorContent" style="margin-left: 42%;">
                        <asp:RangeValidator ID="RVOpenings" runat="server" Display="Dynamic" ControlToValidate="txtNoOfOpenings"
                            ErrorMessage="Please enter valid integer Number more than '0'" MaximumValue="9999"
                            MinimumValue="1" Type="Integer" ValidationGroup="ValGrpJobTitle"></asp:RangeValidator>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <%--Added For LandT--%>
                        <asp:Label ID="lblStartDate" runat="server" Text="Date of Requisition"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="white-space: nowrap">
                        <div>
                            <div style="float: left" id="divDate" runat="server">
                                <%--<igsch:WebDateChooser ID="wdcStartDate" runat="server" NullDateLabel="" EnableKeyboardNavigation="True"
                                    AutoCloseUp="true" FocusOnInitialization="True" TabIndex="4" Editable="True">
                                    <CalendarLayout HideOtherMonthDays="True">
                                    </CalendarLayout>
                                </igsch:WebDateChooser>--%>
                                <ig:WebDatePicker ID="wdcStartDate"  DropDownCalendarID="ddcStartDate" runat="server"></ig:WebDatePicker>    
                                    <ig:WebMonthCalendar  ID="ddcStartDate" HideOtherMonthDays="true" AllowNull="true"  runat="server"></ig:WebMonthCalendar>
                                  
                            </div>
                            
                            <span class="RequiredField" id="SpStartDate" >*</span>
                            <%--for LandT--%>
                            <div id="divASAP" runat="server" style="display: none">
                                <div align="left" style="float: left">
                                    <asp:CheckBox ID="chkStartDate" runat="server" Text="ASAP" AutoPostBack="true" OnCheckedChanged="chkStartDate_CheckedChanged"
                                        TabIndex="5" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:CompareValidator ID="cmpStartDate" runat="server" Operator="GreaterThanEqual"
                            Type="Date" ControlToValidate="wdcStartDate" ErrorMessage="Start date should not be before the current date"
                            Display="Dynamic" />
                        <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="wdcStartDate"
                            SetFocusOnError="true" ErrorMessage="Please select date of requisition." Display="Dynamic"
                            ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--Added for LandT--%>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblOpenDate" runat="server" Text="Open Date"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="white-space: nowrap">
                        <div>
                            <div style="float: left" id="divOpenDate" runat="server">
                                <%--<igsch:WebDateChooser ID="wdcOpenDate" runat="server" NullDateLabel="" EnableKeyboardNavigation="True"
                                    FocusOnInitialization="True" TabIndex="6" Editable="true">
                                    <CalendarLayout HideOtherMonthDays="True">
                                    </CalendarLayout>
                                </igsch:WebDateChooser>--%>
                                <ig:WebDatePicker ID="wdcOpenDate" DropDownCalendarID="ddcOpenDate" runat="server">
                                <ClientSideEvents  ValueChanged="wdcOpenDate_ValueChanged"/>
                                </ig:WebDatePicker>
                                <ig:WebMonthCalendar  ID="ddcOpenDate"  AllowNull="true" HideOtherMonthDays="true"  runat="server"></ig:WebMonthCalendar>
                            </div>
                            <span class="RequiredField" id="Spopendate" >*</span> 
                        </div>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:CompareValidator ID="cmpOpenDate" runat="server" Operator="GreaterThanEqual" ControlToCompare="wdcStartDate"
                            Type="Date" ControlToValidate="wdcOpenDate" ErrorMessage="Open date should not be before the current date"
                            Display="Dynamic" ValidationGroup="ValGrpJobTitle" />
                        <asp:CompareValidator ID="cmbopenwithExpectedFullfillment" runat="server" Operator="LessThanEqual"
                            Type="Date" ControlToValidate="wdcOpenDate" ErrorMessage="Open date should not be after the expected fullfillment date"
                            Display="Dynamic" ControlToCompare="wdcClosingDate" ValidationGroup="ValGrpJobTitle" />
                        <asp:RequiredFieldValidator ID="rfVOpendDate" runat="server" ControlToValidate="wdcOpenDate"
                            SetFocusOnError="true" ErrorMessage="Please select open date." Display="Dynamic"
                            ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <%--for LandT--%>
                        <asp:Label ID="lblClosingDate" runat="server" Text="Expected Fulfillment Date"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="white-space: nowrap">
                        <div>
                            <div style="float: left" id="divExpFulfillmentDate" runat="server">
                               <%-- <igsch:WebDateChooser ID="wdcClosingDate" runat="server" NullDateLabel="" EnableKeyboardNavigation="True"
                                    FocusOnInitialization="True" TabIndex="7" Editable="true">
                                    <CalendarLayout HideOtherMonthDays="True">
                                    </CalendarLayout>
                                </igsch:WebDateChooser>--%>
                                <ig:WebDatePicker ID="wdcClosingDate"  DropDownCalendarID="ddcClosingDate" runat="server"></ig:WebDatePicker>
                                <ig:WebMonthCalendar  ID="ddcClosingDate" AllowNull="true" HideOtherMonthDays="true"   runat="server"></ig:WebMonthCalendar>
                            </div>
                            <span class="RequiredField" id="Spclosedate" >*</span>  
                        </div>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvClosingDate" runat="server" ControlToValidate="wdcClosingDate"
                            SetFocusOnError="true" ErrorMessage="Please Select Expected Fulfillment Date."
                            Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:CompareValidator ID="cmpDate" ControlToCompare="wdcStartDate" ControlToValidate="wdcClosingDate"
                            Type="Date" Operator="GreaterThanEqual" ErrorMessage="Expected Fulfillment Date Should not before Date of Requisition."
                            ValidationGroup="ValGrpJobTitle" Display="Dynamic" runat="server"></asp:CompareValidator>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlCity" CssClass="chzn-select" runat="server" TabIndex="8">
                        </asp:DropDownList><span runat="server" id="spnddlCity" class="RequiredField" >*</span>
                        <div id="dvcityfill">
                                    </div>
                                    <asp:HiddenField ID="hdcityfill" runat="server" Value="" />
                        <asp:TextBox EnableViewState="false" ID="txtCity" runat="server" placeholder="City" 
                        AutoPostBack="false" AutoComplete="OFF" 
                        TabIndex="8" CssClass="CommonTextBox">
                        </asp:TextBox>
                        <ajaxToolkit:AutoCompleteExtender ID="txtCityfill_AutoCompleteExtender" runat="server"
                                        ServicePath="~/AjaxExtender.asmx" TargetControlID="txtCity" MinimumPrefixLength="1"
                                        ServiceMethod="GetLocationName" EnableCaching="True" CompletionInterval="0" CompletionListCssClass="AutoCompleteBox"
                                        FirstRowSelected="True" CompletionListElementID="dvcityfill" />
                        <span runat="server" id="spnTxtCity" class="RequiredField" >*</span> &nbsp;&nbsp;                    
                       
                    </div>
                </div>
                <div>
                    <div id="divZipCode" style="display: none" runat="server">
                        <asp:Label ID="lblZipCode" runat="server" Text="Zip/Postal Code"></asp:Label>:
                        <asp:TextBox ID="txtZipCode" runat="server" CssClass="CommonTextBox" Width="80px"
                            MaxLength="6" TabIndex="9" OnKeyPress="return numeric_only(this)"></asp:TextBox>
                        <%--0.5--%>
                        &nbsp;&nbsp; <a id="divZip" runat="server" href="http://zip4.usps.com/zip4/welcome.jsp"
                            target="_blank" tabindex="24">ZIP code lookup</a> <a id="divPinCode" runat="server"
                                visible="false" href="http://www.indiapost.gov.in/Pin/Pinsearch.aspx " target="_blank"
                                tabindex="10">PIN Code Lookup</a>
                        <div class="TableFormValidatorContent" style="margin-left: 64.5%">
                            <asp:RegularExpressionValidator ID="revZIPCode" runat="server" ControlToValidate="txtZipCode"
                                Display="Dynamic" ErrorMessage="Please enter a valid zip code" ValidationExpression="^[0-9-]*$"
                                ValidationGroup="ValGrpJobTitle"></asp:RegularExpressionValidator><%--(\d{6}(-\d{4})?)|(\d{5}(-\d{4})?)--%>
                        </div>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="ddlCity"
                            SetFocusOnError="true" ErrorMessage="Please Enter City." InitialValue="0" Display="Dynamic"
                            ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
              
               
            <%-- *************Code change by pravin khot on 11/Feb/2016(ShowAndHideState="true")***************--%>
               <ucl:CountryState ID="uclCountryState" ShowAndHideState="true" runat="server" FirstOption="Please Select"
                    CountryTabIndex="11" ApplyDefaultSiteSetting="true" IsCountryRequired="true" />
                  <%--  **********************************END*******************************************--%>
                <asp:UpdatePanel ID="upSkill" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlSkill" runat="server" DefaultButton="btnAddSkill">
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblSkills" runat="server" Text="Required Skills"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <div id="divskill">
                                    </div>
                                    <asp:HiddenField ID="hfSkillSetNames" runat="server" Value="" />
                                    <asp:TextBox ID="txtSkillName" runat="server" AutoPostBack="false" CssClass="CommonTextBox"
                                        TabIndex="12" AutoComplete="OFF" onclick="LockWheel();" onBlur="ReleaseWheel();" />
                                    <ajaxToolkit:AutoCompleteExtender ID="txtSkillName_AutoCompleteExtender" runat="server"
                                        ServicePath="~/AjaxExtender.asmx" TargetControlID="txtSkillName" MinimumPrefixLength="1"
                                        ServiceMethod="GetSkills" EnableCaching="True" CompletionInterval="0" CompletionListCssClass="AutoCompleteBox"
                                        FirstRowSelected="True" CompletionListElementID="divskill" />
                                    <span class="RequiredField">*</span>
                                    <asp:Button ID="btnAddSkill" runat="server" Text="Add" CssClass="CommonButton" TabIndex="13" />
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 42%">
                                <asp:CustomValidator ID="cvSkill" runat="server" ValidationGroup="ValGrpJobTitle"
                                    Display="Dynamic" ClientValidationFunction="ValidateSkill" ErrorMessage="Please select atleast one skill"></asp:CustomValidator>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                            </div>
                            <div class="TableFormContent" id="divContent" runat="server" enableviewstate="true"
                                style="margin-left: 42%">
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="FormRightColumn">
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblRequisitionType" runat="server" Text="Requisition Type"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlRequisitionType" runat="server" CssClass="chzn-select" TabIndex="14"
                            Width="40%">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblClientJobIdHeader" runat="server" Text="Client Job Id"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox EnableViewState="false" ID="txtClientJobId" runat="server" CssClass="CommonTextBox"
                            TabIndex="15"></asp:TextBox>
                    </div>
                </div>
                <div class="TableRow">
                    <asp:Panel ID="pnlJobtitle" runat="server">
                        <div class="TableFormLeble">
                            <asp:Label ID="lblJobTitle" runat="server" Text="Job Title"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtJobTitle" runat="server" CssClass="CommonTextBox" MaxLength="60"
                                TabIndex="15" EnableViewState="False" ValidationGroup="ValGrpJobTitle" onkeypress="checkEnter(event)"></asp:TextBox>
                            <span class="RequiredField" id="spjobtitle">*</span>
                        </div>
                    </asp:Panel>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvJobTitle" runat="server" ControlToValidate="txtJobTitle"
                            SetFocusOnError="true" ErrorMessage="Please Enter Job title." Display="Dynamic"
                            ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--Added for LandT--%>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblJobCategory" runat="server" Text="Job Category"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="ddlJbCategory" runat="server" CssClass="chzn-select" TabIndex="17"
                            Width="40%">
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvJobCategory" runat="server" ControlToValidate="ddlJbCategory"
                            SetFocusOnError="true" InitialValue="0" ErrorMessage="Please Select Job Category."
                            Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--Added for LandT--%>
                <div class="TableRow" runat="server" id="trjoblocation">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblJobLocation" runat="server" Text="Job Location"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="ddlJobLocation" runat="server" CssClass="chzn-select" TabIndex="17">
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                    </div>
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvJobLocation" runat="server" ControlToValidate="ddlJobLocation"
                            SetFocusOnError="true" ErrorMessage="Please Select Job Location." InitialValue="0"
                            Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div id="dvBillrate" runat="server" >
                 <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblSalary" runat="server" Text="Bill Rate"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <div id="divOpen" style="display: none">
                            <asp:CheckBox ID="chkSalary" runat="server" TabIndex="19" Checked="false" /></div>
                        <asp:TextBox ID="txtSalary" runat="server" CssClass="CommonTextBox" Width="80px"
                            rel="Numeric" TabIndex="20"></asp:TextBox>
                        <div id="divMaxBillRate" runat="server" style="display: none">
                            <asp:TextBox ID="txtMaxPayRate" runat="server" CssClass="CommonTextBox" Width="80px"
                                TabIndex="21" Enabled="false" ToolTip="Max" rel="Numeric"></asp:TextBox></div>
                        <div id="divSalary" runat="server" style="display: none">
                            <asp:DropDownList ID="ddlSalary" runat="server" CssClass="CommonDropDownList" TabIndex="21"
                                Enabled="false" Width="70px">
                                <asp:ListItem Value="4">Yearly</asp:ListItem>
                                <asp:ListItem Value="3">Monthly</asp:ListItem>
                                <asp:ListItem Value="2">Daily</asp:ListItem>
                                <asp:ListItem Value="1" Selected="True">Hourly</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:DropDownList ID="ddlSalaryCurrency" runat="server" CssClass="CommonDropDownList"
                            TabIndex="22" Visible="false" Width="70px">
                        </asp:DropDownList>
                        <asp:Label runat="server" ID="lblSalaryCurrency" Text="$USD"></asp:Label>
                        <span class="RequiredField">*</span>
                        <asp:Label ID="lblPayrateCurrency" runat="server" Width="70px"></asp:Label>
                    </div>
                </div>
                </div>
                <div id="divClientBudget" visible="false" runat="server" >
                 <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblClientBudget" runat="server" Text="Client Budget"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <div style="float: left;">
                            <div style="float: left;">
                                <asp:Label ID="lblClientBudgetMin" runat="server" Text="Min"></asp:Label>
                            </div>
                            <div style="float: left; padding-left: 4px;">
                                <asp:TextBox ID="txtClientBudgetMin" runat="server" CssClass="CommonTextBox" rel="Integer"
                                    Width="35px"  TabIndex="26" ValidationGroup ="ValGrpJobTitle"></asp:TextBox>
                            </div>
                        </div>
                        <div style="float: left;">
                            <div style="float: left;">
                                <asp:Label ID="lblClientBudgetMax" runat="server" Text="Max"></asp:Label>
                            </div>
                            <div style="float: left; padding-left: 4px;">
                                <asp:TextBox ID="txtClientBudgetMax" runat="server" CssClass="CommonTextBox" rel="Integer"
                                    Width="35"  TabIndex="27" ValidationGroup ="ValGrpJobTitle"></asp:TextBox><span class="RequiredField">*</span>
                            </div>
                        </div>
                    <%--</div>
                    <div class="TableFormContent" style="vertical-align: top">--%>
                        <div style="float: right; text-align: right; padding-right: 15px;">
                            <div style="float: right; padding-right: 4px;">
                                <asp:DropDownList ID="ddlpaycycle" runat="server" CssClass ="CommonDropDownList" runat="server" Width="55px" ></asp:DropDownList>
                            </div>
                        
                            <div style="float: right; padding-right: 4px;">
                                <asp:DropDownList ID="ddlCurrency" CssClass ="CommonDropDownList" runat="server" Width="55px" ></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
               
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvBillRate" runat="server" ControlToValidate="txtSalary"
                            SetFocusOnError="true" ErrorMessage="Please Enter Bill Rate." Display="Dynamic"
                            ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--Added for LandT--%>
                <div class="TableRow" runat="server" id="trsalesgroup">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblSalesGroup" runat="server" Text="Sales Group"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="ddlSalesGroup" runat="server" CssClass="chzn-select" TabIndex="24">
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                    </div>
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvSalesGroup" runat="server" ControlToValidate="ddlSalesGroup"
                            SetFocusOnError="true" InitialValue="0" ErrorMessage="Please Select Sales Group."
                            Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--Added for LandT--%>
                <div class="TableRow" runat="server" id="trsalesregion">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblSalesRegion" runat="server" Text="Sales Region"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="ddlSalesRegion" runat="server" TabIndex="23" CssClass="chzn-select"
                            Width="200px">
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                    </div>
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvSalesRegion" runat="server" ControlToValidate="ddlSalesRegion"
                            SetFocusOnError="true" InitialValue="0" ErrorMessage="Please Select Sales Region."
                            Display="Dynamic" ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblEducationQualification" runat="server" Text="Educational Qualification"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <ucl:MultipleSelection ID="uclEducationList" runat="server" />
                        <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                            width: 20%; height: 100px; overflow: auto; float: left; display: none">
                            <asp:CheckBoxList ID="ddlQualification" runat="server" ValidationGroup="ValGrpJobTitle"
                                AutoPostBack="false" TabIndex="25">
                            </asp:CheckBoxList>
                        </div>
                        <div style="display: inline-block">
                            <span class="RequiredField">*</span>
                        </div>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <%--                        <asp:CustomValidator ID="cvEduQualification" runat="server"  OnServerValidate ="ValidateEducationList"
                    Display="Dynamic" EnableClientScript="true" ErrorMessage="Select at least one Educational Qualification"
                    ValidationGroup="ValGrpJobTitle"></asp:CustomValidator>--%>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblExpRequired" runat="server" Text="Experience Required (yrs)"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <div style="float: left;">
                            <div style="float: left;">
                                <asp:Label ID="lblExpRequiredMin" runat="server" Text="Min"></asp:Label>
                            </div>
                            <div style="float: left; padding-left: 4px;">
                                <asp:TextBox ID="txtExpRequiredMin" runat="server" CssClass="CommonTextBox" rel="Integer"
                                    Width="35px" MaxLength="3" TabIndex="26" ValidationGroup ="ValGrpJobTitle"></asp:TextBox>
                            </div>
                        </div>
                        
                        <div style="float: left;">
                            <div style="float: left;">
                                <asp:Label ID="lblExpRequiredMax" runat="server" Text="Max"></asp:Label>
                            </div>
                            <div style="float: left; padding-left: 4px;">
                                <asp:TextBox ID="txtExpRequiredMax" runat="server" CssClass="CommonTextBox" rel="Integer"
                                    Width="35" MaxLength="3" TabIndex="27" ValidationGroup ="ValGrpJobTitle"></asp:TextBox><span class="RequiredField">*</span>
                                 <span class="RequiredField" id="Spexperience">*</span>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:CompareValidator ID="cvExpRequiredMin" runat="server" ControlToValidate="txtExpRequiredMin"
                            Display="Dynamic" ErrorMessage="Invalid Number" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
                        <asp:CompareValidator ID="cvExpRequiredMax" runat="server" ControlToValidate="txtExpRequiredMax"
                            Display="Dynamic" ErrorMessage="Invalid Number" Operator="DataTypeCheck" Type="Double"></asp:CompareValidator>
                        <asp:CompareValidator ID="cmpExperienceValidator" runat="server" ControlToValidate="txtExpRequiredMax"
                            ControlToCompare="txtExpRequiredMin" Display="Dynamic" ErrorMessage="Minimum experience should be less than or equal to maximum experience<br/>"
                            Operator="GreaterThanEqual" ValidationGroup="ValGrpJobTitle" Type="Double"></asp:CompareValidator>
                        <asp:CustomValidator runat="server" Display="Dynamic" ValidationGroup="ValGrpJobTitle"
                            ID="cvExperience" ErrorMessage="Please Enter Min or Max Experience" ClientValidationFunction="ValidateExperience">
                        </asp:CustomValidator>
                    </div>
                </div>
                
                
                <%--<div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblBranch" runat="server" Text="Branch"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlbranch" runat="server" CssClass="chzn-select" TabIndex="14"
                            Width="40%">
                        </asp:DropDownList>
                    </div>
                </div>
           
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblGrade" runat="server" Text="Grade"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList ID="ddlgrade" runat="server" CssClass="chzn-select" TabIndex="15"
                            Width="40%">
                        </asp:DropDownList>
                    </div>
                </div>--%>

           
                
                
            </div>
        </div>
    </asp:Panel>
</div>

<script src="../assets/js/chosen.jquery.js" type="text/javascript"></script>

<script type="text/javascript">
    Sys.Application.add_load(function() {
        $(".chzn-select").chosen();
        $(".chzn-select-deselect").chosen({ allow_single_deselect: true });

        $('#<%=ddlBU.ClientID %>').chosen().change(function() {
        var tz = document.getElementById("<%= ddlBU.ClientID %>").value
          
            $find('<%=BUAutoComplete.ClientID%>').set_contextKey($(this).find('option:selected').val());
        });

        $('#divContactEmaill').css({ 'display': 'none' });

    });

    
  
</script>

