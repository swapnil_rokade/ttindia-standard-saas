<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VendorApplicants.ascx.cs"
    Inherits="VendorApplicants" %>
<%@ Register Src="~/Controls/SmallPagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:Panel ID="pnlSettings" runat="server">
</asp:Panel>
<asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
<asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
<asp:Panel ID="widgetBody" runat="server">
    <asp:ObjectDataSource ID="odsCandidateList" runat="server" SelectMethod="GetPaged"
        TypeName="TPS360.Web.UI.CandidateDataSource" SelectCountMethod="GetListCount"
        EnablePaging="True" SortParameterName="sortExpression">
        <SelectParameters>
            <asp:Parameter Name="memberId" Type="Int32" />
              <asp:Parameter Name="IsVendor" Type="Boolean"  DefaultValue ="false" />
                <asp:Parameter Name="IsVendorContact" Type="Boolean"  DefaultValue ="true" />
                <asp:Parameter Name ="DateFrom"  Type ="DateTime" />
                <asp:Parameter Name ="DateTo"  Type ="DateTime" />
            <asp:Parameter Name="SortOrder" DefaultValue="asc" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ListView ID="lsvCandidateList" runat="server" DataSourceID="odsCandidateList"
        EnableViewState="true" DataKeyNames="Id" OnItemDataBound="lsvCandidateList_ItemDataBound"
        OnItemCommand="lsvCandidateList_ItemCommand" OnPreRender="lsvCandidateList_PreRender">
        <LayoutTemplate>
            <table id="tlbTemplate" class="Grid" cellspacing="0" border="0">
                <tr runat="server" id="trRecentApplicant">
                      <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="lnkCreatedDate" runat="server" ToolTip="Sort By CreateDate" CommandName="Sort"
                            CommandArgument="[C].[CreateDate]" Text="Date Created" />
                    </th>
                      <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnID" runat="server" ToolTip="Sort By ID" CommandName="Sort"
                            CommandArgument="[C].[ID]" Text="ID" />
                    </th>      
                    <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnName" runat="server" ToolTip="Sort By Name" CommandName="Sort"
                            CommandArgument="[C].[FirstName]" Text="Name" />
                    </th>
                    <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnEmail" runat="server" ToolTip="Sort By Email" CommandName="Sort"
                            CommandArgument="[C].[PrimaryEmail]" Text="Email" />
                    </th>
                    
                       <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnContactNumber" runat="server" ToolTip="Sort By Contact Number" CommandName="Sort"
                            CommandArgument="[C].[CellPhone]" Text="Contact Number" />
                    </th>      
                    <th style="min-width: 100px; white-space: nowrap;">
                        <asp:LinkButton ID="btnAvailability" runat="server" ToolTip="Sort By Availability" CommandName="Sort"
                            CommandArgument="[C].[Availability]" Text="Availability" />
                    </th>
                    <th style="min-width: 100px; white-space: nowrap;">
                        <asp:Label ID="lblNPP" runat="server" ToolTip="Notice Period" Text="Notice Period" />
                    </th>
                  
                </tr>
                <tr id="itemPlaceholder" runat="server">
                </tr>
                <tr class="Pager">
                    <td colspan="7">
                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <EmptyDataTemplate>
            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                style="width: 100%; margin: 0px 0px;">
                <tr>
                    <td>
                        No candidates available.
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                 <td>
                    <asp:Label ID="lblDateCreated" runat="server" />
                </td>
                 <td>
                    <asp:Label ID="lblID" runat="server" />
                </td>
                <td>
                    <asp:HyperLink ID="lnkCandidateName"  runat="server"></asp:HyperLink>
                </td>
                <td>
                    <asp:Label ID="lblEmail" runat="server" />
                </td>
                 <td>
                    <asp:Label ID="lblContactNumber" runat="server" />
                </td>
                 <td>
                    <asp:Label ID="lblAvailability" runat="server" />
                </td>
                 <td>
                    <asp:Label ID="lblNP" runat="server" />
                </td>
               
            </tr>
        </ItemTemplate>
    </asp:ListView>
</asp:Panel>
