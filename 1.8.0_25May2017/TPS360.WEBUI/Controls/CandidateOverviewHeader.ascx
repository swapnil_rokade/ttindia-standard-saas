﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CandidateOverviewHeader.ascx.cs"
    Inherits="TPS360.Web.UI.ctlCandidateOverviewHeader" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<script language="JavaScript" src="../Scripts/SendMail.js" type="text/javascript"></script>

<%@ Register Src="~/Controls/CompanyRequisitionPicker.ascx" TagName="CompReq" TagPrefix="ucl" %>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<%@ Register Src="~/Controls/MemberJobCarts.ascx" TagName="JobCart" TagPrefix="ucl" %>
<style type="text/css">
    .field-header
    {
        font-size: 12px;
        color: #666666;
        text-transform: uppercase;
    }
    .field-header A
    {
        cursor: pointer;
        color: #008EE8;
    }
    .field-header + h3
    {
        width: 90%;
        overflow: hidden;
        font-size: 16px;
        text-overflow: ellipsis;
    }
    .overviewColumn
    {
        float: left;
        width: 33%;
        overflow: hidden;
        white-space: nowrap;
    }
    .overviewColumn span
    {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis !important;
    }
    .DontShow
    {
        display: none;
        margin: 1px;
        padding: 2px 5px 2px 5px;
        border-style: solid;
        border-width: 1px;
        border-color: #999999;
        border-top-color: #CCCCCC;
        border-left-color: #CCCCCC;
        background-color: #EEEEEE;
        color: #333333;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-size: 100%;
        font-weight: bold;
        white-space: nowrap;
        text-decoration: none;
        cursor: pointer;
    }
    .TextBoxStyle
    {
        overflow: auto;
    }
    #btnAddToMyCalender
    {
        width: 125px;
    }
</style>

<script id="igClientScript" type="text/javascript">
 var n = 10;
function changeAvailability(avalability)
{
    document.getElementById('<%= lblAvailibility.ClientID  %>').innerHTML=avalability;
}
function changeApplicantionType(candidatetype)
{
}
function changeRating(rating){
document.getElementById('<%=imgInterRating.ClientID %>').setAttribute('src',rating);
}
    function DropdownClicked(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }

    function EnadledTextBox(txtControl)
        {
            var btnCancel = $get('<%=btnCancel.ClientID %>');
            var btnOk = $get('<%=btnOk.ClientID %>');
            
           // if(txtControl.readOnly)
                {
                    txtControl.readOnly = false;
                    txtControl.select();
                    btnOk.style.display='block';
                    btnCancel.style.display='block';
                }
                $('#divRemark').css({'width':'88%'});
        }
         function ShowReqDiv(divId) {
        $('.btn-group').removeClass('open');
        var a = document.getElementById("divReqs");
        var b = document.getElementById("divHotListdiv");
        if (a != null) a.style.display = "none";
        if (b != null) b.style.display = "none";

        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) {
            $(reqDiv).closest('.btn-group').addClass('open');
            reqDiv.style.display = "";
            reqDiv.parentElement.className = "btn-group open";
        } return false;
    }
   
    function monitorClick(e) {
        try {
            var evt = (e) ? e : event;

            var theElem = (evt.srcElement) ? evt.srcElement : evt.target;

            while (theElem != null) {
              //  if (!$('#'+theElem .id).hasClass('active-result') && theElem.id.indexOf('spanTitle') &&  theElem.id.indexOf('txtSearch')&& theElem.id.indexOf('refReq')  && theElem.id.indexOf('refHot')   && theElem.id.indexOf('ddlHotlist') && theElem.id.indexOf('TxtHotList') && theElem.id.indexOf('ddlHotlist') &&   theElem.id.indexOf('btnAddToPSList') && theElem.id.indexOf('btnAddToHotList')<0 && theElem.id.indexOf('list') && theElem.id.indexOf('icon')<0 && n != 0) {
               if( $('#' + theElem .id).parents('.btn-group') ==null || theElem .id==''){


                    n = 10;
                    var divReqs = document.getElementById('divReqs');
                    if (divReqs != null) {
                        $(divReqs).closest('.btn-group').removeClass('open');
                        divReqs.style.display = "none";
                    }
                    var divHotListdiv = document.getElementById('divHotListdiv');
                    if (divHotListdiv != null) {
                        $(divHotListdiv).closest('.btn-group').removeClass('open');
                        divHotListdiv.style.display = "none";
                    }
                    return true;
                }
                else {
                    n = 10;
                    return true;
                }

                theElem = theElem.offsetParent;
            }
        }
        catch (e) {
        } n = 10;
        return true;
    }
    document.onclick = monitorClick;
    
    sys.Application.add_load(function(){
    if($('#<%=hdnApplicationType.ClientID %>').val()=='')
    {
//    $("#divRemarks").hide();    
    $('#divRemarks').css({display:''});
    }
    
    
    });

</script>

<script src="../js/CompanyRequisition.js" type="text/javascript"></script>

<asp:HiddenField ID="outlook1" runat="server" />
<asp:HiddenField ID="ddlReqval" runat="server" />
<asp:HiddenField ID="hdnReqSIndx" runat="server" />
<asp:HiddenField ID="hdnApplicationType" runat="server" Value="LandT" />
<div style="padding-left: 30px; padding-right: 30px;">
    <div class="TabPanelHeader" >
        Overview
    </div>
    <div class="TableRow" style="padding-bottom: 60px;">
        <div class="overviewColumn" >
            <h6 class="field-header">
                Full Name</h6>
            <h3>
                <asp:Label ID="lblCandidateName" runat="server"></asp:Label></h3>
        </div>
        <div class="overviewColumn">
            <h6 class="field-header">
                Job Title</h6>
            <h3>
                <asp:Label ID="lblJobTitle" runat="server"></asp:Label></h3>
        </div>
        <div class="overviewColumn" >
            <h6 class="field-header">
                <asp:HyperLink ID="linkPrimaryManager" Text="Primary Manager" runat="server" Font-Bold="true"></asp:HyperLink></h6>
            <h3>
                <asp:Label ID="lblPrimaryManagerName" runat="server"></asp:Label></h3>
        </div>
    </div>
    <div class="TableRow" style="padding-bottom: 60px;">
        <div class="overviewColumn" >
            <h6 class="field-header">
                Email</h6>
            <h3>
                <asp:Label ID="lblPrimaryEmail" runat="server"></asp:Label></h3>
        </div>
        <div class="overviewColumn" >
            <h6 class="field-header">
                Current Company</h6>
            <h3>
                <asp:Label ID="lblCompany" runat="server"></asp:Label></h3>
        </div>
        <div class="overviewColumn" id="divInternalRating" runat="server">
            <h6 class="field-header">
                <asp:HyperLink ID="linkInternalRating" Text="Internal Rating" runat="server" Font-Bold="true"></asp:HyperLink></h6>
            <h3>
                <asp:Image ID="imgInterRating" runat="server" ImageUrl="~/Images/InternalRating0.png" /></h3>
        </div>
    </div>
    <div class="TableRow" style="padding-bottom: 60px;">
        <div class="overviewColumn" >
            <h6 class="field-header">
                Mobile</h6>
            <h3>
                <asp:Label ID="lblMobile" runat="server"></asp:Label></h3>
        </div>
        <div class="overviewColumn" >
            <h6 class="field-header">
                Yrs of Exp</h6>
            <h3>
                <asp:Label ID="lblExperience" runat="server"></asp:Label></h3>
        </div>
        <div class="overviewColumn" >
            <h6 class="field-header">
                Source
            </h6>            
            <h3><asp:Label ID="lblResumeSource" runat="server" Text="Resume Source" Font-Bold="true"></asp:Label></h3>
            <asp:HyperLink ID="linksourcehistory" Text="View History" runat="server" Font-Bold="true"></asp:HyperLink>
            <div id="dvlnkSrcHistory">
            </div>
        </div>
    </div>
    <div class="TableRow" style="padding-bottom: 60px;">
        <div class="overviewColumn">
            <h6 class="field-header">
                Location</h6>
            <h3>
                <asp:Label ID="lblLocation" runat="server"></asp:Label></h3>
        </div>
        <div class="overviewColumn" >
            <h6 class="field-header">
                Highest Education</h6>
            <h3>
                <asp:Label ID="lblHighestDegree" runat="server" EnableViewState="true"></asp:Label></h3>
        </div>
        <div class="overviewColumn">
            <h6 class="field-header">
                Last Updated</h6>
            <h3>
                <asp:Label ID="lblLastUpdated" runat="server" Text="LastUpdated" Font-Bold="true"></asp:Label></h3>
        </div>
    </div>
    <div class="TableRow" style="padding-bottom: 60px;">
        <div class="overviewColumn">
            <h6 class="field-header">
                ID #</h6>
            <h3>
                <asp:Label ID="lblCandidateId" runat="server" Font-Bold="true"></asp:Label></h3>
        </div>
        <div class="overviewColumn" >
            <h6 class="field-header">
                <asp:HyperLink ID="linkAvailability" Text="Availability" runat="server" Font-Bold="true"></asp:HyperLink></h6>
            <h3>
                <asp:Label ID="lblAvailibility" runat="server" Font-Bold="true"></asp:Label></h3>
        </div>
       
        <div class="overviewColumn" runat="server" id="ocstatus">
            <h6 class="field-header">
                Status</h6>
            <h3>
                <asp:Label ID="lblStatus" runat="server" Font-Bold="true"></asp:Label></h3>
        </div>
    </div>
    <div class="TableRow" style="padding-bottom: 60px;">
        <div class="overviewColumn">
            <h6 class="field-header">
                Current CTC</h6>
            <h3>
                <asp:Label ID="lblCurrentCTC" runat="server" Font-Bold="true"></asp:Label></h3>
        </div>
        <div class="overviewColumn" >
            <h6 class="field-header">
                Expected CTC</h6>
            <h3>
                <asp:Label ID="lblExpectedCTC" runat="server" Font-Bold="true"></asp:Label></h3>
        </div>
        <div class="overviewColumn" runat="server" id="Div1">
            <h6 class="field-header">
                Notice Period</h6>
            <h3>
                <asp:Label ID="lblNoticePeriod" runat="server" Font-Bold="true"></asp:Label></h3>
        </div>
    </div>
     <div class="TableRow" style="padding-bottom: 60px;">
         <div class="overviewColumn" >
            <h6 class="field-header">
              Current Age
            <h3>
                <asp:Label ID="lblAgefromDOB" runat="server" Font-Bold="true"></asp:Label></h3>
        </div>
    </div>
     <%--  ******************Code added by pravin khot on 12/July/2016**********--%>
    <div class="TableRow" style="padding-bottom: 60px;" id="div2" runat="server">
        <h6 class="field-header">
            Recent Note </h6>
            <asp:Label ID="lbldetailsnotes" runat="server"  Text="" Style="font-size:10px " ></asp:Label>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="float: left; width: 100%" id="div3">
                    <asp:Label EnableViewState="false" ID="Label1" runat="server"></asp:Label>
                    <asp:TextBox ID="txtRecentNotes" runat="server" BorderStyle="Groove"  Enabled="false" Height="42px" Style="resize: none;
                        float: left" CssClass="TextBoxStyle" TextMode="MultiLine" 
                        Width="98%" MaxLength="3000"></asp:TextBox>
                    <asp:Label ID="Label2" runat="server" Visible="false" CssClass="CommonLabel"></asp:Label>
                </div>
                <div style="float: left; margin-top: 5px; padding-left: 10px;">
                    <asp:Button ID="Button1" runat="server" Text="Update" CssClass="DontShow" OnClick="btnOk_Click"
                        Width="70px" UseSubmitBehavior="false" />
                    <asp:Button ID="Button2" runat="server" Text="Cancel" CssClass="DontShow" Width="70px"
                        OnClick="btnCancel_Click" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
   <%-- *******************************END*****************************--%>
    <div class="TableRow" style="padding-bottom: 60px;" id="divRemarks" runat="server">
        <h6 class="field-header">
            Remarks</h6>
        <asp:UpdatePanel ID="upRemarks" runat="server">
            <ContentTemplate>
                <div style="float: left; width: 100%" id="divRemark">
                    <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
                    <asp:TextBox ID="txtRemarksR" runat="server" BorderStyle="Solid" Height="42px" Style="resize: none;
                        float: left" CssClass="TextBoxStyle" TextMode="MultiLine" ondblclick="EnadledTextBox(this)"
                        Width="98%" MaxLength="3000"></asp:TextBox>
                    <asp:Label ID="lblRemark" runat="server" Visible="false" CssClass="CommonLabel"></asp:Label>
                </div>
                <div style="float: left; margin-top: 5px; padding-left: 10px;">
                    <asp:Button ID="btnOk" runat="server" Text="Update" CssClass="DontShow" OnClick="btnOk_Click"
                        Width="70px" UseSubmitBehavior="false" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="DontShow" Width="70px"
                        OnClick="btnCancel_Click" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
<asp:UpdatePanel ID="upActions" runat="server">
    <ContentTemplate>
        <div class="well" style="clear: both; margin-top: 20px" id="divActions" runat="server">
            <div style="text-align: center">
                <div class="btn-group" id="divAddRequisition" runat="server" style="display: inline-block">
                    <a href="Javascript:void(0)" onclick="ShowReqDiv('divReqs')" id="refReq" class="btn btn-medium dropdown-toggle">
                        Add to Requisition <span class="caret"></span></a>
                    <ul class="dropdown-menu-custom" id="divReqs" style="display: none">
                        <li style="width: 220px; text-align: left" id="list">
                            <ucl:CompReq ID="uclComReq" runat="server"></ucl:CompReq>
                            <asp:Button ID="btnAddToPSList" ValidationGroup="RequisitionList" CssClass="btn btn-small"
                                runat="server" Text="Add" OnClick="btnAddToPSList_Click" UseSubmitBehavior="False" />
                        </li>
                    </ul>
                </div>
                <div class="btn-group" style="display: inline-block;" id="divAddHotList" runat="server">
                    <a href="Javascript:void(0)" onclick="ShowReqDiv('divHotListdiv')" class="btn dropdown-toggle"
                        id="refHot")">Add to Hot List <span class="caret"></span></a>
                    <ul class="dropdown-menu-custom" id="divHotListdiv" style="display: none">
                        <li style="width: 220px; text-align: left" id="li1">
                            <asp:DropDownList ID="ddlHotlist" runat="server" CssClass="chzn-select" Width = "150px">
                            </asp:DropDownList>
                            <asp:TextBox ID="TxtHotList" runat="server" Visible="true" Width="200"></asp:TextBox>
                            <div id="divHot">
                            </div>
                            <ajaxToolkit:AutoCompleteExtender ID="hot_ListAutoComplete" runat="server" ServicePath="~/AjaxExtender.asmx"
                                TargetControlID="TxtHotList" MinimumPrefixLength="1" ServiceMethod="GetHotList"
                                EnableCaching="true" CompletionListElementID="divHot" CompletionInterval="0"
                                CompletionListCssClass="AutoCompleteBox" FirstRowSelected="True" />
                            <asp:Button ID="btnAddToHotList" CssClass="btn btn-small" runat="server" Text="Add"
                                ValidationGroup="HotList" OnClick="btnAddToHotList_Click" UseSubmitBehavior="False" style=" margin-bottom : 15px" />
                        </li>
                    </ul>
                </div>
                <div class="btn-group" style="display: inline-block">
                    <asp:LinkButton ID="lnkSendEmail" runat="server" CssClass="btn btn-medium dropdown-toggle"
                        Text="New Email"></asp:LinkButton>
                </div>
                <div class="btn-group" style="display: inline-block">
                    <asp:LinkButton ID="lnkAddNote" runat="server" CssClass="btn btn-medium dropdown-toggle"
                        Text="New Note"></asp:LinkButton>
                </div>
                <div class="btn-group" style="display: inline-block">
                    <asp:LinkButton ID="lnkScheduleInterview" runat="server" CssClass="btn btn-medium dropdown-toggle"
                        Text="Schedule Interview"></asp:LinkButton>
                </div>
                <div class="btn-group" style="display: inline-block">
                    <asp:LinkButton ID="lnkInterviewFeedback" runat="server" CssClass="btn btn-medium dropdown-toggle"
                        Text="Recruiter Feedback"></asp:LinkButton>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:HiddenField ID="hiddenMemberId" runat="server" />

<script language="javascript" type="text/javascript">
try{
     var ctlRemarksR = $get('<%= txtRemarksR.ClientID %>'); 
     ctlRemarksR.readOnly=true;
     }
     catch (e)
     {
     }
</script>

<asp:SiteMapDataSource runat="server" ID="MenuSiteMap" ShowStartingNode="false" />
