<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CompanyContact.ascx
    Description: This is the user control page used to provide company(suplier) contact information.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Mar-10-2009          Jagadish N          Defect id: 8687; Aligned alert messages to center.
    0.2             Mar-17-2009          Nagarathna V.B      Defect Id: 10150: corrected spelling mistake .
    0.3             Mar-24-2009          Nagarathna V.B      Defect Id: 10099: removed javascript from checkbox onclick.
    0.4             Apr-06-2009          Jagadish            Defect id: 10191; Restricted zip code text field to 6 characters.
    0.5             May-05-2009          Nagarathna V.B      Defect Id:10420; changed the validation for office phone Extenions and direct phone extesion.
    0.6             May-13-2009          Veda                Defect id: 10459; To disable the "Direct Number" field
    0.7             June-22-2009         Gopala Swamy J      Defect Id: 10732; To make the zip code accept 5 or 6 digit.
    0.8             25/Feb/2016          pravin khot         added div id= divChkExistingUser , divEmailtxt ,divEmailddl

-------------------------------------------------------------------------------------------------------------------------------------------       
--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompanyContact.ascx.cs"
    Inherits="TPS360.Web.UI.cltCompanyContact" %>
<%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script type="text/javascript" language="javascript" src="../Scripts/fixWebkit.js"></script>

<link href="../Style/style.css" />

<script type="text/javascript">
 
  
		function ValidateName(source,arguments) {
		   
		    var first=document .getElementById ('<%=txtFirstName.ClientID %>');
		    var last=document .getElementById ('<%=txtLastName.ClientID %>');
		    if(first .value!="" || last .value!="")
		    {
		    arguments .IsValid=true ;
		    }
		    else 
		    {
		    arguments .IsValid = false ;
		    }
		}
		function ValidatePrimaryEmail(source, arguments) 
		{
		    var ch=document .getElementById ("ctl00_ctl00_cphHomeMaster_cphCompanyMaster_ctlCompanyContact_hdnCheckValidity");
		 
		    if(ch!=null)
    		{
                arguments.IsValid = ch.value=="false" ? false  : true  ; 
            }
        }

</script>

<asp:UpdatePanel ID="UpPanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="TableRow" style="text-align: center">
            <asp:Label ID="lblMessage" EnableViewState="False" runat="server"></asp:Label>
        </div>
        <asp:Panel ID="pnlEditor" runat="server" DefaultButton="btnSave">
            <div class="TabPanelHeader">
                Add Contact
            </div>
            <div class="FormLeftColumn" style="width: 55%;">
                 <%-- **************old code comment by pravin khot on 25/Feb/2016****************--%>
               <%-- <div class="TableRow">
                    <div class="TableFormLeble">
                        Email:
                    </div>
                    <div class="TableFormContent" style =" white-space :nowrap ; overflow :hidden ; text-align : left ; padding-left : 0px;">
                        <asp:HiddenField ID="hdnCheckValidity" runat="server" />
                        <asp:TextBox ID="txtEmail" ValidationGroup="ProfileUpdate" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                        <span class="RequiredField">*</span>
                        <%--<asp:CheckBox ID="chkNoBulkEmail" runat="server" Text="No Bulk Email" Style="white-space: nowrap;" />
                    </div>
                </div>--%>
              <%--  *******************************END***************************************--%>
                
                <%-- **************NEW code added by pravin khot on 25/Feb/2016****************--%>
                 <div class="TableRow" runat="server" id="divEmailtxt">
                    <div class="TableFormLeble">
                        Email:
                    </div>
                    <div class="TableFormContent" style =" white-space :nowrap ; overflow :hidden ; text-align : left ; padding-left : 0px;">
                        <asp:HiddenField ID="hdnCheckValidity" runat="server" />
                        <asp:TextBox ID="txtEmail" ValidationGroup="ProfileUpdate" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                        <span class="RequiredField">*</span>
                        <%--<asp:CheckBox ID="chkNoBulkEmail" runat="server" Text="No Bulk Email" Style="white-space: nowrap;" />--%>
                    </div>
                </div>
                 <div class="TableRow" runat="server" id="divEmailddl" style="display:none;">
                  <div class="TableFormLeble">
                        Email:
                    </div>
                   <div class="TableFormContent" style =" white-space :nowrap ; overflow :hidden ; text-align : left ; padding-left : 0px;">
                         <asp:DropDownList ID="ddlemail" runat="server" OnTextChanged="ddlEmail_Click" class="CommonDropDownList" AutoPostBack="True">
                            </asp:DropDownList> 
                    </div>
                 </div>
              <%--  *****************************END****************************************--%>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:Label ID="lblCheckUser" EnableViewState="False" runat="server" Visible="false" />
                        <asp:CustomValidator ID="cvPrimaryEmail" ValidationGroup="ProfileUpdate" runat="server" EnableViewState="true" ControlToValidate="txtEmail"
                            ClientValidationFunction="ValidatePrimaryEmail" 
                            Display="Dynamic" ErrorMessage=""></asp:CustomValidator>
                        <div id="divNotAvailable" style="color: Red; display: none" enableviewstate="true">
                            Email address already exists, please enter a unique email .</div>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                            ErrorMessage="Please enter the email address." EnableViewState="False" Display="Dynamic"
                            ValidationGroup="ProfileUpdate"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ValidationGroup="ProfileUpdate"  ID="valexEmail" runat="server" ControlToValidate="txtEmail"
                            ErrorMessage="Please enter valid email address." Display="Dynamic" ValidationExpression="\w+([-+.!#$%&*+-/=?^_`{|}~'\\\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        <%--\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*--%>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        First Name:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtFirstName" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                        <span class="RequiredField">*</span>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="valfFirstName" runat="server" ControlToValidate="txtFirstName"
                    ErrorMessage="Please enter first name." EnableViewState="False" Display="Dynamic"
                    ValidationGroup="ProfileUpdate"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        Last Name:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtLastName" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                        <span class="RequiredField">*</span>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                         <asp:RequiredFieldValidator ID="valfLastName" runat="server" ControlToValidate="txtLastName"
                    ErrorMessage="Please enter last name." EnableViewState="False" Display="Dynamic"
                    ValidationGroup="ProfileUpdate"></asp:RequiredFieldValidator>
                        <%--<asp:CustomValidator ID="cvNameValidate" runat="server" ValidationGroup="ProfileUpdate"
                            ClientValidationFunction="ValidateName" Display="Dynamic" ErrorMessage="Please enter First/Last Name."></asp:CustomValidator>--%>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        Title:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblOfficePhone" runat="server" Text="Office Phone"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtOfficePhone" runat="server" CssClass="CommonTextBox"   MaxLength ="15"></asp:TextBox>&nbsp;Ext.&nbsp;<asp:TextBox
                            ID="txtOfficePhoneExtension" Width="30px" runat="server" CssClass="CommonTextBox"  MaxLength ="4" rel="Integer"></asp:TextBox>
                    </div>
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <%--0.1--%>
                        <asp:RegularExpressionValidator runat="server" ID="revOfficePhoneExtension" ControlToValidate="txtOfficePhoneExtension"
                            ErrorMessage="Please enter extension number in 2-4 digits." ValidationExpression="^[0-9]{2,4}$"
                            SetFocusOnError="true" Display="Dynamic" />
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <%--0.1--%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="Dynamic"
                            ControlToValidate="txtOfficePhone" EnableViewState="false" ErrorMessage="Please enter 0-9,(),+- only."
                            ValidationExpression="^[0-9'()+-]{1,20}$"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblMobilePhone" runat="server" Text="Mobile"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtMobilePhone" runat="server" CssClass="CommonTextBox"  MaxLength ="14"></asp:TextBox>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                            ControlToValidate="txtMobilePhone" EnableViewState="false" ErrorMessage="Please enter '0 or +91' format only."
                            ValidationExpression="^([0]|\+91)?\d{10}"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblFax" runat="server" Text="Fax"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtFax" runat="server" CssClass="CommonTextBox"  MaxLength ="15" ></asp:TextBox>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                            ControlToValidate="txtFax" EnableViewState="false" ErrorMessage="Please enter 0-9,(),+- only."
                            ValidationExpression="^[0-9'()+-]{10,20}$"></asp:RegularExpressionValidator>--%>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                            ControlToValidate="txtFax" EnableViewState="false" ErrorMessage="Please enter 0-9,(),+- only."
                            ValidationExpression="^[0-9'()+-]{1,20}$"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        Is Primary Contact:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top;">
                        <asp:CheckBox ID="chkIsPrimary" runat="server" />
                    </div>
                </div>
                <div class="TableRow" style="display: none;">
                    <div class="TableFormLeble">
                        Is Owner:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top;">
                        <asp:CheckBox ID="chkIsOwner" runat="server" />
                    </div>
                </div>
                
                <%-- ********Code added by pravin khot on 25/Feb/2016******************--%>
                 <div class="TableRow" runat="server" id="divChkExistingUser">
                    <div class="TableFormLeble">
                        Is Existing User:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top;">
                        <asp:CheckBox ID="ChkExistingUser" runat="server" OnCheckedChanged="ChkExisting_Click" AutoPostBack="true" />
                    </div>
                </div>
               <%-- ***********************END**************************************--%>
                <div id="divOwner" class="Hidden" runat="server">
                    <div class="TableRow">
                        <div class="TableFormLeble">
                            <asp:Label ID="Label1" runat="server" Text="Ownership percentage"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtOwnership" runat="server" CssClass="CommonTextBox" Width="60"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbeOwnership" runat="server" TargetControlID="txtOwnership"
                                FilterType="Custom, Numbers" ValidChars="." />
                        </div>
                        <div class="TableFormLeble">
                            <asp:Label ID="Label3" runat="server" Text="Ethnic Background"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlEthnicBackgroud" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="FormRightColumn" style="width: 45%;">
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblAddress1" runat="server" Text="Address 1"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtAddress1" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblAddress2" runat="server" Text="Address 2"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtAddress2" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtCity" runat="server" CssClass="CommonTextBox"></asp:TextBox>
                    </div>
                </div>
                <ucl:CountryState ID="uclCountryState" runat="server" FirstOption="Please Select" ApplyDefaultSiteSetting="True"  /> 
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="lblZip" runat="server" Text="Zip/Postal Code"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtZip" Width="70px" runat="server" CssClass="CommonTextBox" MaxLength="6"></asp:TextBox>
                        <%--0.4--%>
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <%--0.1 and 0.6--%>
                        <asp:RegularExpressionValidator ID="revZIPCode" runat="server" ControlToValidate="txtZip"
                            Display="Dynamic" ErrorMessage="Please enter a valid zip code" ValidationExpression="(\d{6}(-\d{4})?)|(\d{5}(-\d{4})?)"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
           
            <asp:Panel ID="pnlContactRemarks" runat="server">
                <div class="TableRow">
                    <div class="TableFormLeble" style="width: 23%">
                        <asp:Label ID="lblContactRemarks" runat="server" Text="Contact Remarks"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtContactRemarks" runat="server" CssClass="CommonTextBox" TextMode="MultiLine"
                            Width="75%"></asp:TextBox>
                    </div>
                </div>
            </asp:Panel>
            <div class="TableRow" style="text-align: center">
                <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save" ValidationGroup="ProfileUpdate"
                    OnClick="btnSave_Click" />
                <asp:Button ID="Button1" CssClass="CommonButton" runat="server" Text="Clear" CausesValidation="false"
                    OnClick="btnClear_Click" />
            </div>
          
        </asp:Panel>
        <div class="TabPanelHeader">
            List of Contacts
        </div>
        <asp:Panel ID="pnlGrid" runat="server">
            <div class="GridContainer">
                <asp:ListView ID="lsvCompanyContact" runat="server" DataKeyNames="Id" OnItemDataBound="lsvCompanyContact_ItemDataBound"
                    OnItemCommand="lsvCompanyContact_ItemCommand">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Title
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Office Phone
                                </th>
                                <th>
                                    Mobile
                                </th>
                                <th>
                                    City
                                </th>
                                <th>
                                    Is Primary
                                </th>
                                <th style="width: 40px">
                                    Action
                                </th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No Contacts.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td align="left">
                                <%--<asp:Label ID="lblName" runat="server" />--%>
                                <asp:LinkButton ID="lblName" runat="server" CommandName="OpenModal"></asp:LinkButton>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblTitle" runat="server" />
                            </td>
                            <td align="left">
                                <asp:HyperLink ID="lnkEmail" runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblDirectNumber" runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblMobile" runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblCity" runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblIsPrimary" runat="server" />
                            </td>
                            <td align="left">
                                <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" CommandName="EditItem"
                                    CausesValidation="False"></asp:ImageButton>
                                <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem"
                                    Visible="false" CausesValidation="False"></asp:ImageButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
