﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: SendEmailList.ascx.cs
    Description: This is the user control page used for sending e-mails
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sep-25-2008           Yogeesh Bhat        Defect ID: 8706; Changes made in Page_Load() method.
                                                             (Checked for Session["ReferenceLink"] if it is from referenceLink, 
                                                              updated _tomemberId to reference ID from session)
    0.2            Dec-05-2008           Jagadish            Defect id: 9358; changed case of variable id's from 'ReceiverId' and
                                                             'SenderId' to 'receiverId' and 'senderId' and '_tomemberId' to '_frommemberId'.
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Configuration;
namespace TPS360.Web.UI
{
    public partial class MemberJobCarts : BaseControl
    {
        public delegate void RefreshJobCartGrid();
        public event RefreshJobCartGrid evntJobCart;

         #region Member Variables
        private int _memberId;
        DropDownList ddlTemStatus;
         #endregion

        #region Properties

        #endregion

        #region Methods

        //public void Databind()
        //{
        //    lsvJobCart.DataBind();
        //}

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton) lsvJobCart.FindControl( txtSortColumn.Text );
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }

        void ReloadGrid()
        { 
            
        }


        #endregion

        #region Events

        #region Page Events 

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            else
            {
                _memberId = base.CurrentMember.Id;
            }
            odsJobCartList.SelectParameters["memberId"].DefaultValue = _memberId.ToString();

            ddlTemStatus = new DropDownList();
            IList<HiringMatrixLevels> levelsList = Facade.GetAllHiringMatrixLevels();
            //HiringMatrixLevels newlevel = new HiringMatrixLevels();
            //newlevel.Id = -1;
            //newlevel.Name = "Rejected";
            //levelsList.Add(newlevel);
            ddlTemStatus.DataSource = levelsList;
            ddlTemStatus.DataTextField = "Name";
            ddlTemStatus.DataValueField = "Id";
            ddlTemStatus.DataBind();
            if(!IsPostBack )  lsvJobCart.DataBind();
            string pagesize = "";
            pagesize = (Request.Cookies["JocartInCandidateOverviewRowPerPage"] == null ? "" : Request.Cookies["JocartInCandidateOverviewRowPerPage"].Value); ;
           
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobCart.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            }
            if (!IsPostBack)
            {
                txtSortColumn.Text = "lnkJobTitle";
                txtSortOrder.Text = "asc";
                PlaceUpDownArrow();
            }

        }
        #endregion

        #region ListView Events

        protected void lsvJobCart_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }




        }

        protected void lsvJobCart_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool isAssingned = false;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Common.BusinessEntities.MemberJobCart memberJobCart = ((ListViewDataItem)e.Item).DataItem as Common.BusinessEntities.MemberJobCart;

                if (memberJobCart != null)
                {
                    JobPostingHiringTeam team = Facade.GetJobPostingHiringTeamByMemberId(base.CurrentMember.Id, memberJobCart .JobPostingId );
                    if (team != null) isAssingned = true;
                    HyperLink lblJobTitle = (HyperLink)e.Item.FindControl("lblJobTitle");
                    Label lblJobCode = (Label)e.Item.FindControl("lblJobCode");
                    Label lblJobStatus = (Label)e.Item.FindControl("lblJobStatus");
                    Label lblCurrentLevel = (Label)e.Item.FindControl("lblCurrentLevel");
                    Label lblPublishedDate = (Label)e.Item.FindControl("lblPublishedDate");
                    Label lblBU = (Label)e.Item.FindControl("lblBU");
                   // Label lblPublishedBy = (Label)e.Item.FindControl("lblPublishedBy");
                    Label lblStatus = (Label)e.Item.FindControl("lblStatus");
                    DropDownList ddlStatus = (DropDownList)e.Item.FindControl("ddlStatus");
                    HiddenField hdnJobPostingId = (HiddenField)e.Item.FindControl("hdnJobPostingId");
                    HiddenField hdnStatusId = (HiddenField)e.Item.FindControl("hdnStatusId");
                    HiddenField hdnCurrentLevel = (HiddenField)e.Item.FindControl("hdnCurrentLevel");
                    ddlStatus.Attributes.Add("onchange", "javascript:Status_OnChange('" + ddlStatus.ClientID + "','" + hdnStatusId.ClientID + "');");
                    hdnJobPostingId.Value = memberJobCart.JobPostingId.ToString();
                    System.Web.UI.HtmlControls.HtmlTableCell thBU = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobCart.FindControl("thBU");
                    System.Web.UI.HtmlControls.HtmlTableCell tdBU = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdBU");

                    System.Web.UI.HtmlControls.HtmlTableCell thCode = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobCart.FindControl("thCode");
                    System.Web.UI.HtmlControls.HtmlTableCell tdJobCode = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdJobCode");

                    lblJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + memberJobCart.JobPostingId + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    lblJobTitle.Text = memberJobCart.JobTitle;
                    lblJobCode.Text = memberJobCart.JobPostingCode;
                    lblBU.Text = memberJobCart.CompanyName;
                    lblPublishedDate.Text = memberJobCart.JobPostingUpdateDate.ToShortDateString();
                   // lblPublishedBy.Text = memberJobCart.PublisherName;
                    ddlStatus.DataValueField = ddlTemStatus.DataValueField;
                    ddlStatus.DataTextField = ddlTemStatus.DataTextField;
                    ddlStatus.DataSource = ddlTemStatus.DataSource;
                    ddlStatus.DataBind();
                    ddlStatus.Attributes.Add("onchange", "javascript:ModalChanged(this,'" + memberJobCart .MemberId  + "','" + CurrentMember .Id + "','" + memberJobCart .JobPostingId + "');");
                    string apptype = "";
                    //if (isMainApplication) apptype = "Common";
                    switch (ApplicationSource)
                    {
                        case ApplicationSource.LandTApplication:
                            apptype = "";
                            break;
                        case ApplicationSource.MainApplication:
                            apptype = "Common";
                            break;
                        default:
                            apptype = "";
                            break;
                    }
                    hdnCurrentLevel.Value = memberJobCart.CurrentLevel.ToString();
                    if (IsUserAdmin || isAssingned)
                    {
                      
                        ddlStatus.Visible = true;
                        lblStatus.Visible = false;
                        ControlHelper.SelectListByValue(ddlStatus, hdnCurrentLevel.Value);
                        var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
                        var submit = webConfig.AppSettings.Settings["Submission"];
                        var offer = webConfig.AppSettings.Settings["Offer"];
                        var join = webConfig.AppSettings.Settings["Join"];
                        var interview = webConfig.AppSettings.Settings["Interview"];
                        bool isMainApplication = false;
                        switch (ApplicationSource)
                        {
                            case ApplicationSource.LandTApplication:
                                isMainApplication = false;
                                break;
                            case ApplicationSource.MainApplication:
                                isMainApplication = true;
                                break;
                            default:
                                isMainApplication = true;
                                break;
                        }


                        foreach (ListItem list in ddlStatus.Items)
                        {
                            if (list.Text == submit.Value)
                            {
                                if (!list.Value.Contains("StatusId")) list.Value = UrlConstants.ApplicationBaseUrl + "Modals/SubmissionDetails.aspx?StatusId=" + list.Value + "&JID=" + memberJobCart.JobPostingId;
                            }
                            else if (list.Text == offer.Value)
                            {
                                if (!list.Value.Contains("StatusId")) list.Value = UrlConstants.ApplicationBaseUrl + "Modals/" + apptype + "HiringDetails.aspx?StatusId=" + list.Value + "&JID=" + memberJobCart.JobPostingId;
                            }
                            else if (list.Text == join.Value)
                            {
                                if (!list.Value.Contains("StatusId")) list.Value = UrlConstants.ApplicationBaseUrl + "Modals/" + apptype + "JoiningDetails.aspx?StatusId=" + list.Value + "&JID=" + memberJobCart.JobPostingId;
                            }
                            else if (list.Text == interview.Value)
                            {
                                if (!list.Value.Contains("StatusId")) list.Value = UrlConstants.ApplicationBaseUrl + "Modals/ScheduleInterview.aspx?StatusId=" + list.Value + "&JID=" + memberJobCart.JobPostingId;
                            }
                            else if (list.Text == "Rejected")
                            {
                                if (!list.Value.Contains("StatusId")) list.Value = UrlConstants.ApplicationBaseUrl + "Modals/RejectionDetails.aspx?StatusId=" + list.Value + "&JID=" + memberJobCart.JobPostingId;
                            }
                            else if (list.Text == "Offer Decline" && !isMainApplication )
                            {
                                if (!list.Value.Contains("StatusId")) list.Value = UrlConstants.ApplicationBaseUrl + "Modals/OfferRejected.aspx?StatusId=" + list.Value + "&JID=" + memberJobCart.JobPostingId;
                            }
                            else
                            {
                            }
                        }

                    }
                    else
                    {
                        lblStatus.Visible = true;
                        lblStatus.Text = memberJobCart.CurrentLevelName;
                        ddlStatus.Visible = false;
                    }

                    if (ApplicationSource == ApplicationSource.GenisysApplication)
                    {
                        thCode.Visible = false;
                        tdJobCode.Visible = false;
                        tdBU.Visible = true;
                        thBU.Visible = true;
                    
                    
                    }
                }
            }
        }
        protected void lsvJobCart_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobCart.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "JocartInCandidateOverviewRowPerPage";
            }

            

            PlaceUpDownArrow();
            lsvJobCart.DataBind();
        }
        protected void ddlStatus_SelectedIndexChanged(object source, EventArgs e)
        {

           

        }

        #endregion
        //public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        //{
        //    if (e.Answer == ConfirmationWindow.enmAnswer.OK)
        //    {
        //        if (_memberId.ToString().Trim() != "")
        //        {

        //        }
        //        else MiscUtil.ShowMessage(lblMessage, "Please select at least one applicant", true);

        //    }
        //}

        #endregion

    

       
}
    }
