﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JoiningDetails.ascx.cs"
    Inherits="TPS360.Web.UI.JoiningDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<style type="text/css">
    .CalendarCSS
    {
        background-color: Gray;
        color: Blue;
        border-color: Maroon;
    }
    .custom-calendar .ajax__calendar_container
    {
        background-color: #ffc; /* pale yellow */
        border: solid 1px #666;
    }
    .custom-calendar .ajax__calendar_title
    {
        background-color: #cf9; /* pale green */
        height: 20px;
        color: #333;
    }
    .custom-calendar .ajax__calendar_prev, .custom-calendar .ajax__calendar_next
    {
        background-color: #aaa; /* darker gray */
        height: 20px;
        width: 20px;
    }
    .custom-calendar .ajax__calendar_today
    {
        background-color: #cff; /* pale blue */
        height: 20px;
    }
    .custom-calendar .ajax__calendar_days table thead tr td
    {
        background-color: #ff9; /* dark yellow */
        color: #333;
    }
    .custom-calendar .ajax__calendar_day
    {
        color: #333; /* normal day - darker gray color */
    }
    .custom-calendar .ajax__calendar_other .ajax__calendar_day
    {
        display: none;
        color: #666; /* day not actually in this month - lighter gray color */
    }
    .calendarDropdown
    {
        margin-left: -18px;
        margin-bottom: -3px;
    }
</style>

<script language="javascript" type="text/javascript">

    function showLocationTextbox()
    {
        if($('#<%=ddlLocation.ClientID %>').find('option:selected').text().trim()=="Others")
        {
        $('#<%=txtLocation.ClientID %>').show();
        }
        else $('#<%=txtLocation.ClientID %>').hide();
        }
        Sys.Application.add_load(function() {
        $('#<%=ddlLocation.ClientID %>').change(function (){showLocationTextbox();});
        showLocationTextbox();
        });

    function WebDateChooser1_InitializeDateChooser(oDateChooser)
    {
        // used to set "drop-down block" variable if calendar was already opened
        oDateChooser._myMouseDown = function()
        {
        var me = igdrp_getComboById('<%=wdcJoiningDate.ClientID%>');
        if(me && me.isDropDownVisible())
        me._calOpenedTime = new Date().getTime();
        }
        // used to open 
        oDateChooser._myClick = function()
        {
        var me = igdrp_getComboById('<%=wdcJoiningDate.ClientID%>');
        if(me && !me.isDropDownVisible() && (!me._calOpenedTime || me._calOpenedTime + 500 < new Date().getTime()))
        me.setDropDownVisible(true);
        }
        oDateChooser.inputBox.onmousedown = oDateChooser._myMouseDown;
        oDateChooser.inputBox.onclick = oDateChooser._myClick;
        //alert('ok');
    }
    
    function cmpLocation(sender, args) {
        var ddlvalue= document.getElementById('<%=ddlLocation.ClientID %>');
        var txtvalue= document.getElementById('<%=txtLocation.ClientID %>');
        var ddl=$('#<%=ddlLocation.ClientID %>').find('option:selected').text().trim();
        
        var txt=$('#<%=txtLocation.ClientID %>').val().trim();
        if(ddl==('Please Select'))
        {args.IsValid=false;}
        
        if(ddl==('Others') && txt=='')
        {args.IsValid=false;}
    }


</script>

<style type="text/css">
    .front
    {
        position: relative;
    }
</style>
<asp:UpdatePanel ID="upJoin" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnBulkAction" runat="server" Value="" />
        <asp:HiddenField ID="hfMemberId" runat="server" />
        <asp:HiddenField ID="hfJobPostingId" runat="server" />
        <asp:HiddenField ID="hfMemberJoiningDetailsId" runat="server" Value="0" />
        <asp:HiddenField ID="hfStatusId" runat="server" />
        <asp:HiddenField ID="hfCurrentMemberId" runat="server" />
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <div class="TableRow">
            <div class="TableFormLeble">
                Actual - DOJ:
            </div>
            <div class="TableFormContent">
                <asp:TextBox ID="txtJoiningDate" runat="server" Width="100px" TabIndex="1">  
                </asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="wdcJoiningDate" runat="server" TargetControlID="txtJoiningDate"
                    CssClass="custom-calendar" PopupButtonID="imgShows" EnableViewState="true">
                </ajaxToolkit:CalendarExtender>
                <asp:ImageButton ID="imgShows" runat="server" CssClass="calendarDropdown" ImageUrl="~/Images/downarrow.gif" />
                <span class="RequiredField">*</span>
                <%--  <asp:TextBox ID="txtJoiningDate" AutoComplete="OFF" runat="server" Width ="100px" ></asp:TextBox>--%>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormValidatorConent" style="margin-left: 43%">
                <asp:RequiredFieldValidator ID="rfDate" runat="server" ControlToValidate="txtJoiningDate"
                    ErrorMessage="Please enter Actual Joining date" Display="Dynamic" ValidationGroup="Joining"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                PS #:
            </div>
            <div class="TableFormContent">
                <asp:TextBox ID="txtPSID" runat="server" CssClass="CommonTextBox" MaxLength="8"></asp:TextBox>
                <span class="RequiredField">*</span>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormValidatorConent" style="margin-left: 43%">
                <asp:RequiredFieldValidator ID="rfvPSID" runat="server" ControlToValidate="txtPSID"
                    ErrorMessage="Please enter PS ID" Display="Dynamic" ValidationGroup="Joining"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                Location:
            </div>
            <div class="TableFormContent">
                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="CommonDropDownList" Width="80px">
                </asp:DropDownList>
                <asp:TextBox ID="txtLocation" runat="server" placeholder="Location" Width="120px"></asp:TextBox>
                <span class="RequiredField">*</span>
                <div class="TableFormValidatorConent" style="margin-left: 44%">
                    <asp:CustomValidator ID="cvddlLocation" runat="server" ClientValidationFunction="cmpLocation"
                        ControlToValidate="ddlLocation" Display="Dynamic" EnableClientScript="true" ErrorMessage="Please select Location"
                        ValidationGroup="Joining"></asp:CustomValidator>
                    <%--<asp:CustomValidator ID="cvtxtLocation" runat="server" ClientValidationFunction="cmpLocation"
                        ControlToValidate="txtLocation" Display="Dynamic" EnableClientScript="true" ErrorMessage="Please enter Location"
                        ValidationGroup="Joining"></asp:CustomValidator>--%>
                </div>
            </div>
        </div>
        <%--<div class ="TableRow">
    <div class ="TableFormLeble">Offered Salary:
    </div>
    <div class ="TableFormContent">
          <asp:TextBox ID="txtSalary" runat="server" CssClass="CommonTextBox"   TabIndex ="2"
                Width="80px" ></asp:TextBox>
                
            <asp:DropDownList ID="ddlSalary" runat="server" CssClass="CommonDropDownList"  Width="75px"  TabIndex ="3"
                >
                <asp:ListItem Value="4">Yearly</asp:ListItem>
                <asp:ListItem Value="3">Monthly</asp:ListItem>
                <asp:ListItem Value="2">Daily</asp:ListItem>
                <asp:ListItem Value="1" Selected="True">Hourly</asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="ddlSalaryCurrency" runat="server"  Width ="80px"
                CssClass="CommonDropDownList"  TabIndex ="4">
            </asp:DropDownList>
            <asp:Label ID="lblPayrateCurrency" runat ="server" ></asp:Label>
    </div>
</div>
 <div class="TableRow" > 
     <div class ="TableFormValidatorContent" style =" margin-left : 42%">
       <asp:RegularExpressionValidator ID ="rgvSalary" runat ="server" ControlToValidate ="txtSalary" Display ="Dynamic" ErrorMessage ="Please enter Numeric values" ValidationExpression ="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)" ValidationGroup ="ValGrpJobTitle"></asp:RegularExpressionValidator>
    </div>
        </div> 
  
<div class ="TableRow">
    <div class ="TableFormLeble">Billable Salary:
    </div>
    <div class ="TableFormContent">
          <asp:TextBox ID="txtBillableSalary" runat="server" CssClass="CommonTextBox" 
                 TabIndex ="5" onblur="CalculateRevenue()"  Width="80px" ></asp:TextBox>
          <asp:DropDownList ID="ddlBillableSalaryCurrency" runat ="server"  TabIndex ="6" Width="80px" CssClass ="CommonDropDownList"  ></asp:DropDownList>
             <asp:Label ID="lblBillablePayRateCurrency" runat ="server" ></asp:Label>
    </div>
</div>
 <div class="TableRow" > 
     <div class ="TableFormValidatorContent" style =" margin-left : 42%">
       <asp:RegularExpressionValidator ID ="rgvBillSalary" runat ="server" ControlToValidate ="txtBillableSalary" Display ="Dynamic" ErrorMessage ="Please enter Numeric values" ValidationExpression ="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)" ValidationGroup ="ValGrpJobTitle"></asp:RegularExpressionValidator>
    </div>
        </div>
<div class ="TableRow">
    <div class ="TableFormLeble">Billing Rate:
    </div>
    <div class ="TableFormContent">
          <asp:TextBox ID="txtBillingRate" runat="server" CssClass="CommonTextBox" 
                Width ="23px"  onblur="CalculateRevenue()" MaxLength="4" TabIndex ="7"></asp:TextBox> 
            <span> %</span>
            
    </div>
</div>
 <div class="TableRow" > 
     <div class ="TableFormValidatorContent" style =" margin-left : 42%">
       <asp:RegularExpressionValidator ID ="rgvBillRate" runat ="server" ControlToValidate ="txtBillingRate" Display ="Dynamic" ErrorMessage ="Please enter Numeric values" ValidationExpression ="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)" ValidationGroup ="ValGrpJobTitle"></asp:RegularExpressionValidator>
    </div>
        </div>
        <div class ="TableRow">
    <div class ="TableFormValidatorContent" style =" margin-left : 42%">
        <asp:RangeValidator ID="gvRate" runat="server" Display ="Dynamic"  ErrorMessage="Enter 0 to 100 only." ControlToValidate="txtBillingRate" Type="Double" ValidationGroup ="ValGrpJobTitle"   ></asp:RangeValidator>
    </div>
</div>
<div class ="TableRow">
    <div class ="TableFormLeble">Revenue:
    </div>
    <div class ="TableFormContent">
          <asp:TextBox ID="txtRevenue" runat="server" CssClass="CommonTextBox" 
                 TabIndex ="8"  Width="80px" ></asp:TextBox>
          <asp:DropDownList ID="ddlRevenueCurrency" runat ="server"  TabIndex ="9" Width="80px" CssClass ="CommonDropDownList"   ></asp:DropDownList>
           <asp:Label ID="lblRevenueCurrency" runat ="server" ></asp:Label>
    </div>
</div>
 <div class="TableRow" > 
     <div class ="TableFormValidatorContent" style =" margin-left : 42%">
       <asp:RegularExpressionValidator ID ="rgvRevenue" runat ="server" ControlToValidate ="txtRevenue" Display ="Dynamic" ErrorMessage ="Please enter Numeric values" ValidationExpression ="(^\d*\.?\d*[0-9]+\d*$)|(^[0-9]+\d*\.\d*$)" ValidationGroup ="ValGrpJobTitle"></asp:RegularExpressionValidator>
    </div>
        </div>--%>
        <div class="TableRow" style="padding-top: 30px; margin-left: 42%">
            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="CommonButton"
                ValidationGroup="Joining" TabIndex="10" />
            <asp:Button ID="btnRemove" runat="server" Text="Remove Joining Details" OnClick="btnRemove_Click"
                CssClass="CommonButton" TabIndex="11" />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
