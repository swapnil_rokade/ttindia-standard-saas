﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RequisitionWorkflow.ascx
    Description: This is the user control page used enter work flow.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Nov-20-2008          Jagadish N            Defect id: 9205;  Added custom validators to validate all currency dropdowns.
    0.2             Mar-31-2009          Jagadish N            Defect id: 10223; Removed the field 'Requisition to be Published Date'.
    0.3             Apr-03-2009          Sandeesh              Defect id: 9235;  Added validation control to validate 'Requisition to be Published Date' 
    0.4             Apr-03-2009          Sandeesh              Defect id: 9303;  Changed the Error message for the 'client rate' validation control
    0.5             Apr-25-2009          Jagadish N            Defect id: 10402; Replaced compare validator with regular expression validator.
    0.6             9/March/2016         pravin khot           <br /> ADDED BY PRAVIN KHOT ON 9/March/2016
    0.7             15/May/2017          Sumit Sonawane        Issue Id 1309
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionWorkflow.ascx.cs"
    Inherits="TPS360.Web.UI.cltRequisitionWorkflow" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/MultipleItemPicker.ascx" TagName="MultipleSelection"
    TagPrefix="ucl" %>
<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
<script src="../Scripts/ToolTipscript.js"></script>
<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
<link href="../assets/css/chosen.css" rel="Stylesheet" />
<script language="javascript" type="text/javascript">
Sys.Application.add_load(function() {

    $('.TableRow').show();
    });
    function RemoveRow(row) 
    {
       
        $(row).parent().parent().remove();
        
    }           
            function toggleCheckBoxes(source) {
                var listView = document.getElementById('<%= lsvRecruiterGroup.FindControl("tlbTemplate").ClientID %>');
                for (var i = 0; i < listView.rows.length; i++) {
                    var inputs = listView.rows[i].getElementsByTagName('input');
                    if (source.checked) {
                        for (var j = 0; j < inputs.length; j++) {
                            inputs[j].checked = false;
                            source.checked = true
                        }
                    }
                }
            }
    function ClientSelected(source, eventArgs)
    {
        hdnSelectedClient=document .getElementById ("ctl00_cphHomeMaster_ucRequisitionWorkflow_hdnSelectedClient");
        hdnSelectedClientText=document .getElementById ("ctl00_cphHomeMaster_ucRequisitionWorkflow_hdnSelectedClientText");
        hdnSelectedClientText.value=eventArgs.get_text();
        hdnSelectedClient.value=eventArgs.get_value(); 

        Company_OnChangeFromTextBox('ctl00_cphHomeMaster_ucRequisitionWorkflow_ddlClientContact','ctl00_cphHomeMaster_ucRequisitionWorkflow_hdnSelectedContactID',eventArgs.get_value());

    }
	function fnValidateSalary(source,args) 
    {
        //debugger;
        var txtClientHourlyRate = $get('<%= txtClientHourlyRate.ClientID %>').value.trim();
        if(txtClientHourlyRate != "" && txtClientHourlyRate > 0 && !isNaN(txtClientHourlyRate))
        { 
            if(args.Value == "Select")        
            args.IsValid=false;        
            else
            args.IsValid=true;
        }
    }
    
    function fnValidateCurrency(source,args)
    {
       //debugger;
       var CVCurrency = source.id.split('_')[3];
       switch(CVCurrency)
       {
            case "CustomValidatorCurrencyCR":
            var txtValidCurrency = $get('<%= txtClientHourlyRate.ClientID %>').value.trim();
            break;
        }
       
       if(txtValidCurrency != "" && txtValidCurrency > 0 && !isNaN(txtValidCurrency))
       {
            if(args.Value == 0)
            args.IsValid = false;
            else
            args.IsValid = true; 
       } 
    }
</script>

<asp:HiddenField ID="hdnSelectedClientText" runat="server" />
<asp:HiddenField ID="hdnSelectedClient" runat="server" />
<asp:HiddenField ID="hdnSelectedChkBox" runat="server" />

<div>
    <div class="TableRow" style="text-align: left" id="div" runat="server">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"   Style="z-index: 9999; position:relative"></asp:Label>
    </div>
    <div class="TableRow">
        <div class="FormLeftColumn" style="width: 40%">
            <%--Added for LandT--%>
            <div class="TableRow" runat="server" id="trcustomername">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="true" ID="lblCustomerName" Text="Customer Name" runat="server"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtCustomerName" runat="server" TabIndex="31"></asp:TextBox><span
                        class="RequiredField">*</span>
                </div>
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator ID="rfvCustomerName" runat="server" ControlToValidate="txtCustomerName"
                        SetFocusOnError="true" ErrorMessage="Please enter Customer Name." Display="Dynamic"
                        ValidationGroup="ValGrpJobTitle"></asp:RequiredFieldValidator>
                </div>
            </div>
            <%--Added for LandT--%>
            <div class="TableRow" runat="server" id="trpoavailability">
                <div class="TableFormLeble">
                    PO Availability:
                </div>
                <div class="TableFormContent" style="margin-left: 43%">
                    <asp:RadioButton ID="rdoYes" runat="server" Text="Yes" GroupName="POAvailabilty"
                        TabIndex="32" />
                    <br />
                    <asp:RadioButton ID="rdoNO" runat="server" Text="NO" GroupName="POAvailabilty" TabIndex="33" />
                    <br />
                    <asp:RadioButton ID="rdoNA" runat="server" Text="NA" GroupName="POAvailabilty" Checked="true"
                        TabIndex="34" /><br />
                </div>
            </div>
            
            
           <%-- ********************************************--%>
           
               <div id="DivSkillGroup" runat="server">
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="LblRecruiterGroup" runat="server" Text="Assign Recruiters"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                        <asp:DropDownList ID="DdlRecruiterGroup"  CssClass="chzn-select" rel="IG" runat="server" Width="158px" AutoPostBack="false">
                        </asp:DropDownList>
                         <asp:HiddenField ID="hdnSelectedSG" runat="server" />
                         <span class="RequiredField">*</span>
                         
                         <%--<ucl:MultipleSelection ID="uclEducationList" runat="server" />
                        <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                            width: 40%; height: 100px; overflow: auto; float: left; display: none">
                            <asp:CheckBoxList ID="ddlQualification" runat="server" ValidationGroup="ValGrpJobTitle"
                                AutoPostBack="false" TabIndex="25">
                            </asp:CheckBoxList>
                        </div>--%>
                    <%--   </div>
                       
                         <div class="TableFormContent" style="vertical-align: top">--%>
                       <%-- <asp:DropDownList ID="DropDownList1"  CssClass="chzn-select" rel="IG" runat="server" Width="158px" AutoPostBack="false">
                        </asp:DropDownList>--%>
                       <%--code added by Sumit Sonawane on 15/May/2017 for Issue Id 1309 Start--%>
                           <asp:ImageButton  ID="imgbtnRefesh" OnClick="imgbtnRefesh_Click" runat="server" Height="20" Width="20" ToolTip="Refresh"  ImageUrl="~/Images/view-refresh-hi.png" />
                            <input class="span2" type="image" style ="  width :20px; height :20px" src ="../Images/tooltip_icon.png"  rel="tooltip" data-original-title="Reset Workflow panel to assign appropriate No.Of Openings." data-placement="right" value="" onclick ="javascript:return false;"   >
                      <%--code added by Sumit Sonawane on 15/May/2017 for Issue Id 1309 End--%>
                       </div>
                       
                    
                </div>
                
                 <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="LblNoofpositiontoassign" runat="server" Text="No. of position to assign"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                       <%-- <asp:DropDownList ID="DdlSkill" rel="IG" runat="server" Width="158px"  AutoPostBack="false">
                        </asp:DropDownList>--%>
                      <asp:TextBox ID="txtNoofpositiontoassign" runat="server" AutoPostBack="false" rel="Integer" CssClass="CommonTextBox"  MaxLength ="2"   AutoComplete="OFF" onclick="LockWheel();" onBlur="ReleaseWheel();" />
                        <span class="RequiredField">*</span>
                         <asp:HiddenField ID="hdnSelectedSkill" runat="server" />
                         <asp:HiddenField ID="hdnSelectedCS" runat="server" />
                        <asp:Button ID="BtnAddGrid" runat="server" Text="Add>>" CssClass="CommonButton"  OnClick="btnAddGrid_Click"  EnableViewState="true" /> 
                    </div>
                </div>
                
                <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblAdditionalNote" runat="server" Text="Additional Notes"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="false" ID="txtAdditionalNote" runat="server" CssClass="CommonTextBox"
                        TextMode="MultiLine" Rows="5" Width="50%" TabIndex="35"></asp:TextBox>
                </div>
                </div>
                    
                
              <%-- <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label ID="LblCompetencyScale" runat="server" Text="Competency Scale"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="vertical-align: top">
                       <asp:DropDownList ID="DdlCompetencyScale" rel="IG" runat="server" Width="158px" TabIndex="3">
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                         <asp:HiddenField ID="hdnSelectedCS" runat="server" />
                        <asp:Button ID="BtnAddGrid" runat="server" Text="Add>>" CssClass="CommonButton"  OnClick="btnAddGrid_Click"  EnableViewState="true" /> 
                    </div>
                </div>--%>
            </div>
             </br>     
              </br>    
               </br>     
          <%--  ********************************************--%>
            
            
            
        </div>
        <div class="FormRightColumn" style="width: 59%">
             
                <div class="TableRow" >
                    <div class="TableFormLeble" >
                        <asp:Label EnableViewState="false" ID="Label1" runat="server" Text="Assigned Recruiters"></asp:Label>:
                    </div>
                    <div class="TableFormContent" style="width: 70%; float: right;">
                        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <asp:ListView ID="lsvRecruiterGroup"  runat="server" DataKeyNames="Id" OnItemDataBound="lsvRecruiterGroup_ItemDataBound"
                                    OnPreRender="lsvRecruiterGroup_PreRender">
                                    <LayoutTemplate>
                                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0" width="100px !important">
                                            <tr>
                                                <th style="white-space: nowrap;">
                                                    Assign Recruiters:
                                                </th>
                                                <th style="white-space: nowrap;">
                                                    No. of position assigned
                                                </th>
                                               <th style="white-space: nowrap;">
                                                    Primary Recruiter
                                                </th>
                                                <th id="thUnAssign" style="text-align: center; white-space: nowrap; width: 55px;">
                                                    Remove
                                                </th>
                                            </tr>
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                            <tr>
                                                <td>
                                                    No Recruiter group assigned.
                                                </td>
                                            </tr>
                                        </table>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                            <td style="text-align: left;">
                                                <asp:Label ID="lblRecruiterGroup" runat="server" />
                                                <asp:HiddenField ID="hdnRecruiterGroup" runat="server" />
                                            </td>
                                            <td style="text-align:center;">
                                                <asp:Label ID="lblNoPosition" runat="server" />
                                                <asp:HiddenField ID="hdnNoPosition" runat="server" />
                                            </td>
                                             <td style="text-align:center;">                                            
                                              <asp:RadioButton ID="RdbPrimaryRecruiter"  runat="server"  OnClick="toggleCheckBoxes(this);" />
                                                <asp:HiddenField ID="hdnPrimaryRecruiter" runat="server" />
                                            </td>
                                            <td id="tdUnAssign" style="text-align: center;">
                                                <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem" 
                                                     OnClientClick="RemoveRow(this); return;"></asp:ImageButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    
                    
        <div class="TableRow">

    </div>
    </div>
     <div class="TableRow">
            <div class="TableFormValidatorContent" style="margin-left: 42%">
               <asp:CustomValidator ID="cvList" runat="server" ValidationGroup="ValGrpJobTitle" Display="Dynamic" ClientValidationFunction="Validatelist" ErrorMessage="Please select atleast one skill Group"></asp:CustomValidator>
             
            </div>
        </div>
        
        <div style="display: none;">
            <asp:UpdatePanel ID="up" runat="server" >
                <ContentTemplate>
                    <div class="TableRow" style="display: none;">
                        <div class="TableFormLeble" style="width: 30%">
                            <asp:Label EnableViewState="false" ID="lblRecruiters" runat="server" Text="Assigned Recruiters"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:ListBox ID="lstRecruiters" runat="server" SelectionMode="multiple" CssClass="CommonDropDownList"
                                Height="110px" Width="45%" TabIndex="36"></asp:ListBox>
                            <asp:Button ID="btnAddRecruiters" CssClass="CommonButton" runat="server" Text="Add"
                                OnClick="btnAddRecruiters_Click" TabIndex="37" EnableViewState="true" />
                        </div>
                    </div>
                    <br />
                    <div class="TableRow" style="display: none;">
                        <div class="TableFormLeble" style="width: 30%">
                            <asp:Label EnableViewState="false" ID="lblAssignedTeam" runat="server" Text="Assigned Team"></asp:Label>:
                        </div>
                        <div class="TableFormContent" style="width: 65%; float: left;">
                            <asp:UpdatePanel ID="upPanel" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <asp:ListView ID="lsvAssignedTeam" runat="server" DataKeyNames="Id" OnItemDataBound="lsvAssignedTeam_ItemDataBound"
                                        OnPreRender="lsvAssignedTeam_PreRender">
                                        <LayoutTemplate>
                                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0" width="100px !important">
                                                <tr>
                                                    <th style="white-space: nowrap;">
                                                        Name
                                                    </th>
                                                    <th style="white-space: nowrap;">
                                                        Email
                                                    </th>
                                                    <th id="thUnAssign" style="text-align: center; white-space: nowrap; width: 55px;">
                                                        Remove
                                                    </th>
                                                </tr>
                                                <tr id="itemPlaceholder" runat="server">
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                        <EmptyDataTemplate>
                                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                                <tr>
                                                    <td>
                                                        No recruiters assigned.
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                        <ItemTemplate>
                                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblMemberName" runat="server" />
                                                    <asp:HiddenField ID="hdnMemberID" runat="server" />
                                                </td>
                                                <td style="text-align: left;">
                                                    <asp:Label ID="lblMemberEmail" runat="server" />
                                                </td>
                                                <td id="tdUnAssign" style="text-align: center;">
                                                    <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem"
                                                        TabIndex="15" OnClientClick="RemoveRow(this); return;"></asp:ImageButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            </div>
        </div>
       </div>  
       
    
      
       <%-- ******************<br /> ADDED BY PRAVIN KHOT ON 9/March/2016******************--%>
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
      <%-- ****************************END**********************--%>
    
    <%--for LandT--%>
    <div id="divDepartment" runat="server" style="display: none">
        <div class="TableRow" id="divEndClientNames" runat="server">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="true" ID="lblClientName" runat="server"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:DropDownList ID="ddlClientName" runat="server" CssClass="CommonDropDownList"
                    Width="200px" EnableViewState="true " AutoPostBack="true" TabIndex="47">
                </asp:DropDownList>
                <asp:Label ID="lblVendorClientName" runat="server" Visible="false"></asp:Label>
                <asp:TextBox ID="txtClientName" runat="server" Visible="true" Width="200" TabIndex="48"></asp:TextBox>
                <div id="divClient">
                </div>
                <ajaxToolkit:AutoCompleteExtender ID="Client_AutoComplete" runat="server" ServicePath="~/AjaxExtender.asmx"
                    TargetControlID="txtClientName" MinimumPrefixLength="1" ServiceMethod="GetClients"
                    EnableCaching="true" CompletionListElementID="divClient" CompletionInterval="0"
                    FirstRowSelected="True" OnClientItemSelected="ClientSelected" />
                <%--OnClientPopulated="BoldChildren" BehaviorID="AutoCompleteEx"--%>
            </div>
        </div>
    </div>
    <%--for LandT--%>
    <div id="divClientContact" style="display: none" runat="server">
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label ID="lblClientContactName" runat="server"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:DropDownList ID="ddlClientContact" runat="server" EnableViewState="true" CssClass="CommonDropDownList"
                    TabIndex="49">
                </asp:DropDownList>
                <asp:HiddenField ID="hdnSelectedContactID" runat="server" />
            </div>
        </div>
    </div>
    <div runat="server" id="divStaffingFirm" visible="false">
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblClientHourlyRate" runat="server" Text="Bill Rate"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtClientHourlyRate" runat="server" CssClass="CommonTextBox"
                    Width="82px" TabIndex="50"></asp:TextBox>
                <asp:DropDownList ID="ddlSalary" runat="server" CssClass="CommonDropDownList" TabIndex="51">
                    <asp:ListItem Value="Select">Select</asp:ListItem>
                    <asp:ListItem Value="4">Yearly</asp:ListItem>
                    <asp:ListItem Value="3">Monthly</asp:ListItem>
                    <asp:ListItem Value="2">Daily</asp:ListItem>
                    <asp:ListItem Value="1" Selected="True">Hourly</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="ddlClientHourlyRateCurrency" runat="server" CssClass="CommonDropDownList"
                    TabIndex="52">
                </asp:DropDownList>
                <asp:Label ID="lblBillrateCurrency" runat="server" Text="(Lacs)"></asp:Label>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormValidatorContent" style="margin-left: 42%">
                <asp:RegularExpressionValidator ID="revClientHourlyRate" runat="server" ControlToValidate="txtClientHourlyRate"
                    ValidationExpression="^\d*[0-9]\d*(\.\d+)?$" ErrorMessage="Please enter positive numeric values<br/>"
                    Display="Dynamic">
                </asp:RegularExpressionValidator>
                <asp:CustomValidator ID="CustomValidatorSalary" runat="server" ControlToValidate="ddlSalary"
                    EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please select bill rate type.<br/>"
                    ClientValidationFunction="fnValidateSalary" />
                <asp:CustomValidator ID="CustomValidatorCurrencyCR" runat="server" ControlToValidate="ddlClientHourlyRateCurrency"
                    ErrorMessage="Please select currency type." Display="Dynamic" ClientValidationFunction="fnValidateCurrency" /><%--0.1--%>
            </div>
        </div>
        <div class="TableRow">
        </div>
    </div>
</div>
<br />
