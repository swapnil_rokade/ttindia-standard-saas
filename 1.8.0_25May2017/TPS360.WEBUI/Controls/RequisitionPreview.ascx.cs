﻿/* 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RequisitionPreview.ascx.cs
    Description: This is the page which is used to present tab controls
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Nov-12-2008          Gopala Swamy          Defect id: 9210; The changed online code to get the proper code of job
    0.2             Dec-05-2008          Yogeesh Bhat          Defect ID: 9417: changes made in PrepareView() method. Added specified 
                                                               fields while displaying preview
    0.3             Dec-18-2008          Yogeesh Bhat          Defect Id: 9537: Changes made in PrepareView() method; checked for ClientHourlyRate for blank
    0.4             Dec-24-2008          Jagadish              Defect ID: 9593; Changes made in lsvRequisitionSkillSet_ItemDataBound method;
    0.5             Dec-24-2008          Yogeesh Bhat          Defect Id: 9591: Changes made in PrepareView() method.
    0.6             Jan-28-2009          Yogeesh Bhat          Defect Id: 9803: Changes made in lsvAssignedTeam_ItemDataBound()
    0.7             Feb-9-2009           Nagarathna             Defect ID:9832: Changes made  in Prepareview()method; In place of start date publish date was
                                                                                displayed.Now its changed and displyed apporpiate data.
 *  0.8             May-15-2009           Sandeesh             Defect id:10440 :Changes made to get the Requisition status from database instead of getting from the Enum

-------------------------------------------------------------------------------------------------------------------------------------------       
*/



using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace TPS360.Web.UI
{
    public partial class cltRequisitionPreview : RequisitionBaseControl
    {
        #region Member Variables
        string SkillName = ""; 
        #endregion

        #region Methods

        private void PopulateHiringMatrixData()
        {
            int[] hiringMatrixCount = Facade.GetHiringMatrixMemberCount(JobPostingId());
            lblPreselected.Text = hiringMatrixCount[0].ToString();
            lblLavelI.Text = hiringMatrixCount[1].ToString();
            lblLavelII.Text = hiringMatrixCount[2].ToString();
            lblLavelIII.Text = hiringMatrixCount[3].ToString();
            lblFinalHired.Text = hiringMatrixCount[4].ToString();
        }

        private int JobPostingId()
        {            
            int id = 0;
            int.TryParse(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID], out id); 
            return id;
        }

        private void PopulateHiringTeamMembers()
        {
            lsvAssignedTeam.DataSource = Facade.GetAllJobPostingHiringTeamByJobPostingId(JobPostingId());
            lsvAssignedTeam.DataBind();
        }

        private void PrepareView()
        {
            JobPosting jobPosting = Facade.GetJobPostingById(JobPostingId());
            if (!jobPosting.IsNew)
            {
                lblJobTitle.Text = jobPosting.JobTitle.ToString() + " - " + jobPosting.JobPostingCode;
                lblCityState.Text = jobPosting.City.Trim();
                if (jobPosting.StateId > 0)
                {
                    State state = Facade.GetStateById(jobPosting.StateId);
                    if (state != null)
                    {
                        if (string.IsNullOrEmpty(lblCityState.Text))
                        {
                            lblCityState.Text = state.Name;
                        }
                        else
                        {
                            lblCityState.Text = lblCityState.Text + ", " + state.Name;
                        }
                    }
                }

                lblSalary.Text = jobPosting.PayRate + (jobPosting.PayRate != "Open" && jobPosting.PayRateCurrencyLookupId > 0 ? "&nbsp;" + MiscUtil.GetLookupNameById(jobPosting.PayRateCurrencyLookupId, Facade) + (!string.IsNullOrEmpty(jobPosting.PayCycle) ? "&nbsp;" + jobPosting.PayCycle : string.Empty) : string.Empty);
                lblMinExperienceRequired.Text = jobPosting.MinExpRequired.ToString();
                lblMaxExperienceRequired.Text = jobPosting.MaxExpRequired.ToString();
                lblEducationQualification.Text = MiscUtil.SplitValues(jobPosting.RequiredDegreeLookupId, ',', Facade);
                Company company = null;
                //if (jobPosting.ClientId3 > 0)
                //{
                //    company = Facade.GetCompanyById(jobPosting.ClientId3);
                //    lblAllClientName.Text = (company != null ? company.CompanyName : string.Empty);
                //}
                lblWorkAuthorization.Text = MiscUtil.GetWorkAuthorizationById(jobPosting.AuthorizationTypeLookupId, Facade);

                if (jobPosting.NoOfOpenings > 0)
                {
                    lblNoofOpening.Text = jobPosting.NoOfOpenings.ToString();
                }
                else
                {
                    lblNoofOpening.Text = "";
                }

                GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(jobPosting.JobStatus);
                if (_RequisitionStatusLookup != null)
                {
                    lblJobStatus.Text = _RequisitionStatusLookup.Name;
                }

                lblPublishingDate.Text = (MiscUtil.IsValidDate(jobPosting.ActivationDate) ? jobPosting.ActivationDate.ToShortDateString() : string.Empty);
                lblHireDate.Text = (MiscUtil.IsValidDate(jobPosting.FinalHiredDate) ? jobPosting.FinalHiredDate.ToShortDateString() : string.Empty);
                lblStartDate.Text = jobPosting.StartDate;
                lblPostingDate.Text = (MiscUtil.IsValidDate(jobPosting.CreateDate) ? jobPosting.CreateDate.ToShortDateString() : string.Empty);
                lblTimeToFill.Text = MiscUtil.FindDateDiffFrmEndtoStart(jobPosting.FinalHiredDate, jobPosting.ActivationDate);
                //if (jobPosting.ClientId2 > 0)
                //{
                //    company = Facade.GetCompanyById(jobPosting.ClientId2);
                //    if (lblAllClientName.Text != string.Empty)
                //    {
                //        lblAllClientName.Text = lblAllClientName.Text + " > " + (company != null ? company.CompanyName : string.Empty);
                //    }
                //    else
                //    {
                //        lblAllClientName.Text = (company != null ? company.CompanyName : string.Empty);
                //    }
                //}
                if (jobPosting.ClientId > 0)
                {
                    company = Facade.GetCompanyById(jobPosting.ClientId);
                    if (lblAllClientName.Text != string.Empty)
                    {
                        lblAllClientName.Text = lblAllClientName.Text + " > " + (company != null ? company.CompanyName : string.Empty);
                    }
                    else
                    {
                        lblAllClientName.Text = (company != null ? company.CompanyName : string.Empty);
                    }
                }
                PopulateHiringTeamMembers();
                PopulateHiringMatrixData();
                jobPosting.JobDurationMonth = (jobPosting.JobDurationMonth == "") ? "0" : jobPosting.JobDurationMonth;

                if (Facade.GetGenericLookupById(Convert.ToInt32(jobPosting.JobDurationMonth)) != null)
                {
                    lblDuration.Text = (jobPosting.JobDurationMonth != "0") ? Facade.GetGenericLookupById(Convert.ToInt32(jobPosting.JobDurationMonth)).Name : "";
                }

                jobPosting.PayCycle = (jobPosting.PayCycle == "Daily") ? "Day" : jobPosting.PayCycle;
                jobPosting.PayCycle = (jobPosting.PayCycle == "Monthly") ? "Month" : jobPosting.PayCycle;
                jobPosting.PayCycle = (jobPosting.PayCycle == "Yearly") ? "Year" : jobPosting.PayCycle;
                jobPosting.PayCycle = (jobPosting.PayCycle == "Hourly") ? "Hour" : jobPosting.PayCycle;

                if (jobPosting.PayRateCurrencyLookupId > 0)
                {
                    if ((Facade.GetGenericLookupById(jobPosting.PayRateCurrencyLookupId)).Name == "$ USD")
                    {
                        lblPayRate.Text = "$" + jobPosting.PayRate + " / " + jobPosting.PayCycle;
                    }
                    else
                    {
                        lblPayRate.Text = jobPosting.PayRate + " " + (Facade.GetGenericLookupById(jobPosting.PayRateCurrencyLookupId)).Name + " / " + jobPosting.PayCycle;
                    }
                }

                lblPayRate.Text = (jobPosting.PayRate == "Open") ? jobPosting.PayRate : lblPayRate.Text;

                string strPaymentTypes = jobPosting.TaxTermLookupIds;
                string[] arrPaymentTypes = strPaymentTypes.Split(',');
                for (int i = 0; i < arrPaymentTypes.Length; i++)
                {
                    if (arrPaymentTypes[i] != "")
                    {
                        lblPaymentType.Text = Facade.GetGenericLookupById(Convert.ToInt32(arrPaymentTypes[i])).Name + "," + lblPaymentType.Text;
                    }
                }
                if (lblPaymentType.Text != "")
                {
                    lblPaymentType.Text = lblPaymentType.Text.Remove(lblPaymentType.Text.Length - 1, 1);
                }
                lblExpPaid.Text = jobPosting.IsExpensesPaid ? "Yes" : "No";

                string strBenefits = jobPosting.OtherBenefits;
                string[] arrBenefits = strBenefits.Split('!');

                for (int j = 0; j < arrBenefits.Length; j++)
                {
                    if (arrBenefits[j].ToString() != "")
                    {
                        lblBenefits.Text = Facade.GetGenericLookupById(Convert.ToInt32(arrBenefits[j])).Name + "," + lblBenefits.Text;
                    }
                }
                if (lblBenefits.Text != "")
                {
                    lblBenefits.Text = lblBenefits.Text.Remove(lblBenefits.Text.Length - 1, 1);
                }
                lblTravelRequired.Text = (jobPosting.TravelRequiredPercent != "0") ? (jobPosting.TravelRequired ? "Yes" : "No") + " (" + jobPosting.TravelRequiredPercent +"%)": (jobPosting.TravelRequired ? "Yes" : "No");

                lblTeleCommuting.Text = jobPosting.TeleCommunication ? "Yes" : "No";
                lblClientJobId.Text = jobPosting.ClientJobId;

                jobPosting.ClientRatePayCycle = (jobPosting.ClientRatePayCycle == "Daily") ? "Day" : jobPosting.ClientRatePayCycle;
                jobPosting.ClientRatePayCycle = (jobPosting.ClientRatePayCycle == "Monthly") ? "Month" : jobPosting.ClientRatePayCycle;
                jobPosting.ClientRatePayCycle = (jobPosting.ClientRatePayCycle == "Yearly") ? "Year" : jobPosting.ClientRatePayCycle;
                jobPosting.ClientRatePayCycle = (jobPosting.ClientRatePayCycle == "Hourly") ? "Hour" : jobPosting.ClientRatePayCycle;

                if (jobPosting.ClientHourlyRateCurrencyLookupId > 0)
                {
                    //evan - check if user is not allowed to view bill rate (if they are a recruiter)
                    if (jobPosting.ClientHourlyRate != "" && isAllowedToViewBillRate())      //0.3
                    {
                        if ((Facade.GetGenericLookupById(jobPosting.ClientHourlyRateCurrencyLookupId)).Name == "$ USD")
                        {
                            lblClientRate.Text = "$" + jobPosting.ClientHourlyRate + " / " + jobPosting.ClientRatePayCycle;
                        }
                        else
                        {
                            lblClientRate.Text = jobPosting.ClientHourlyRate + " " + (Facade.GetGenericLookupById(jobPosting.ClientHourlyRateCurrencyLookupId)).Name + " / " + jobPosting.ClientRatePayCycle;
                        }
                    }
                }

                lblClientRate.Text = (jobPosting.ClientRatePayCycle == "Open") ? jobPosting.PayRate : lblClientRate.Text;

                if (jobPosting.JobDescription != "")
                {
                    lblJobDesc.Text = HttpUtility.HtmlDecode(jobPosting.JobDescription.ToString());    
                    divJobDesc.Style.Add("border-width", "1px");
                    divJobDesc.Style.Add("border-style", "solid");
                    tblJobDesc.Visible = false;
                }

                if (jobPosting.InternalNote != "")
                {
                    lblAdditionaNotes.Text = jobPosting.InternalNote;
                    divAddiNotes.Style.Add("border-width", "1px");
                    divAddiNotes.Style.Add("border-style", "solid");
                    tblAdditionalNotes.Visible = false;
                }
            }
        }

        private bool isAllowedToViewBillRate()
        {
            if (base.IsUserAdmin)
                return true;
            try
            {
                Database _db = DatabaseFactory.CreateDatabase();
                DbConnection sqlcon = _db.CreateConnection();
                SqlConnection sqlconn = new SqlConnection(sqlcon.ConnectionString);
                sqlconn.Open();
                string command = "SELECT [CR].[Name]  AS [SystemAccess] FROM [Employee] as [E] LEFT JOIN [dbo].[MemberCustomRoleMap] [MCRM] ON [E].[Id]=[MCRM].[MemberId] LEFT JOIN [dbo].[CustomRole] [CR] ON [CR].[Id]=[MCRM].[CustomRoleId] where E.PrimaryEmail='" + base.CurrentUserName + "'";
                SqlCommand sqlComm = new SqlCommand(command, sqlconn);
                SqlDataReader r = sqlComm.ExecuteReader();
                r.Read();
                string access = r["SystemAccess"].ToString();
                r.Close();
                if (access != "Recruiter")
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!Page.IsPostBack)
            {
                PrepareView(); 
            }
            if (Page.IsPostBack)
            {
                MiscUtil.LoadAllControlState(Request, this);
            }
        }

        protected void lsvAssignedTeam_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPostingHiringTeam jobPostingHiringTeam = ((ListViewDataItem)e.Item).DataItem as JobPostingHiringTeam;

                if (jobPostingHiringTeam != null)
                {
                    Label lblMemberName = (Label)e.Item.FindControl("lblMemberName");
                    Label lblMemberEmail = (Label)e.Item.FindControl("lblMemberEmail");
                    Label lblEmployeeType = (Label)e.Item.FindControl("lblEmployeeType");

                    if (jobPostingHiringTeam.MemberId > 0)
                    {
                        if (jobPostingHiringTeam.EmployeeType == EmployeeType.Internal.ToString())
                        {
                            Member member = Facade.GetMemberById(jobPostingHiringTeam.MemberId);
                            if (member != null)
                            {
                                lblMemberName.Text = member.FirstName + " " + member.LastName;
                                lblMemberEmail.Text = member.PrimaryEmail;
                            }
                        }
                        else if (jobPostingHiringTeam.EmployeeType == EmployeeType.External.ToString())
                        {
                        
                            CompanyContact companyContact = Facade.GetCompanyContactById(jobPostingHiringTeam.MemberId);
                            if (companyContact != null)
                            {
                                lblMemberName.Text = companyContact.FirstName + " " + companyContact.LastName;
                                lblMemberEmail.Text = companyContact.Email;
                            }
                        }
                    }
                    if (Facade.GetEmployeeById(jobPostingHiringTeam.MemberId) != null)
                    {
                        string strMemberRole = (Facade.GetEmployeeById(jobPostingHiringTeam.MemberId)).SystemAccess;                    //0.6
                        lblEmployeeType.Text = (strMemberRole == "") ? "Employee" : strMemberRole;                                      //0.6
                    }
                    else
                    {
                        lblEmployeeType.Text = "Admin";     
                    }
                }
            }
        }

        #endregion
    }
}