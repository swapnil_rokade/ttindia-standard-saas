﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VendorMySubmission.ascx.cs"
    Inherits="TPS360.Web.UI.Controls_VendorMySubmission" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>

<asp:HiddenField ID="hdnMySubmission" runat="server" />
<asp:UpdatePanel ID="upDetail" runat="server">
    <ContentTemplate>
       <%-- <div class="TabPanelHeader">
            Candidate Submissions by Vendor
        </div>--%>
        <asp:TextBox ID="txtSortColumn" runat="server" Visible="false" Text="lnkDateSubmitted"></asp:TextBox>
        <asp:TextBox ID="txtSortOrder" runat="server" Visible="false" Text="DESC"></asp:TextBox>
        <asp:ObjectDataSource ID="odsVendorSubmissions" runat="server" SelectMethod="GetPaged_ForVendorSubmissionDetail"
            TypeName="TPS360.Web.UI.CompanyConatctDataSource" SelectCountMethod="GetListCount_ForVendorSubmissionDetail"
            EnablePaging="True" SortParameterName="sortExpression">
            <SelectParameters>
            <asp:ControlParameter ControlID ="ucldatePicker" Name ="StartDate" PropertyName ="StartDate" />
            <asp:ControlParameter ControlID ="ucldatePicker" Name ="EndDate" PropertyName ="EndDate" />
                <asp:Parameter Name="JobPostingId" DefaultValue="0" />
                <asp:Parameter Name="VendorId" DefaultValue="0" />
                <asp:Parameter Name="ContactId" DefaultValue="0" />
                <asp:Parameter Name="CandidateId" DefaultValue="0" />
                <asp:Parameter Name="Type" DefaultValue="" />
            </SelectParameters>
        </asp:ObjectDataSource>
         <div class="TableRow" style="white-space: nowrap ">
                                    <div class="TableFormLeble" style="width: 10%">
                                        <asp:Label EnableViewState="false" ID="lblOfferdateinOfferJoined" runat="server"
                                            Text="Select Date"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <ucl:DateRangePicker ID="ucldatePicker" runat="server" />
                                           <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search" CssClass="btn btn-primary"
                                EnableViewState="false"  ><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                                    </div>
                                </div>
                                    
        <asp:ListView ID="lsvVendorSubmissions" runat="server" DataKeyNames="Id" DataSourceID="odsVendorSubmissions"
            OnItemDataBound="lsvVendorSubmissions_ItemDataBound" EnableViewState="true" OnPreRender="lsvVendorSubmissions_PreRender"
            OnItemCommand="lsvVendorSubmissions_ItemCommand">
            <LayoutTemplate>
                <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                    <tr>
                        <th style="width: 110px !important">
                            <asp:LinkButton ID="lnkDateSubmitted" runat="server" ToolTip="Sort By Date Submitted"
                                CommandName="Sort" CommandArgument="[MJC].[CreateDate]" Text="Date Submitted" />
                        </th>
                        <th id="thCandidateName" runat="server">
                            <asp:LinkButton ID="lnkCandidate" runat="server" ToolTip="Sort By Candidate Name"
                                CommandName="Sort" CommandArgument="[C].[CandidateName]" Text="Candidate Name" />
                        </th>
                        <th id="thSubmittedBy" runat="server">
                            <asp:LinkButton ID="lnkSubmittedBy" runat="server" Text="Submitted By" CommandName="Sort"
                                CommandArgument="M.FirstName"></asp:LinkButton>
                        </th>
                        <th>
                            <asp:LinkButton ID="lnkRequisition" runat="server" ToolTip="Sort By Job Title" CommandName="Sort"
                                CommandArgument="[JP].JobTitle" Text="Job Title" />
                        </th>
                        <th>
                            <asp:LinkButton ID="lnkStatus" runat="server" ToolTip="Sort By Status" CommandName="Sort"
                                CommandArgument="[DS].hiringstatus" Text="Status" />
                        </th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                    <tr class="Pager">
                        <td colspan="5" id="tdPager" runat="server">
                            <ucl:Pager ID="pagerControl" runat="server" />
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
            <EmptyDataTemplate>
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                    style="width: 100%; margin: 0px 0px;">
                    <tr>
                        <td>
                            No Vendor Submissions.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <ItemTemplate>
                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                    <td>
                        <asp:Label ID="lblDateTime" runat="server" />
                    </td>
                    <td>
                        <asp:HyperLink ID="hlnkCandidateName" runat="server" ></asp:HyperLink>
                    </td>
                    <td>
                        <asp:HyperLink ID="hlnkSubmittedBy" runat="server"></asp:HyperLink>
                    </td>
                    <td>
                        <asp:HyperLink ID="lnkJobTitle" runat="server"  TabIndex="8" Width="120px"></asp:HyperLink>
                    </td>
                    <td>
                        <asp:Label ID="lblStatus" runat="server" TabIndex="8" Width="120px"></asp:Label>
                    </td>
                </tr>
            </ItemTemplate>
            
            
        </asp:ListView>
    </ContentTemplate>
</asp:UpdatePanel>
