﻿<%------------------------------------------------------------------------------------------------------------------------------------
    FileName: Controls/AllEmailList.ascx
    Created By: 
    Created On:
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date          Author           Modification
    -----------------------------------------------------------------------------------------------------------------------------------
    0.1            Jan-19-2010   Basavaraj Angadi Defect Id:12901;Changed Style attribute
---------------------------------------------------------------------------------------------------------------------------------------

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllEmailList.ascx.cs"
    Inherits="TPS360.Web.UI.ControlAllEmailList" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:UpdatePanel ID="upAllList" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnSortColumn" runat="server" />
        <asp:HiddenField ID="hdnSortOrder" runat="server" />
        <asp:ObjectDataSource ID="odsEmailList" runat="server" SelectMethod="GetPaged" OnSelecting="odsEmailList_Selecting"
            TypeName="TPS360.Web.UI.MemberEmailDataSource" SelectCountMethod="GetListCount"
            EnablePaging="True" SortParameterName="sortExpression">
            <SelectParameters>
                <asp:Parameter Name="emailTypeLookupId" DefaultValue="" Type="String" />
                <asp:Parameter Name="receiverId" DefaultValue="" Type="String" />
                <asp:Parameter Name="senderId" DefaultValue="" Type="String" />
                <asp:Parameter Name="recId" DefaultValue="" Type="String" />
                <asp:Parameter Name="senId" DefaultValue="" Type="String" />
                <asp:Parameter Name="companyId" DefaultValue="" Type="String" />
                <asp:Parameter Name="AnyKey" DefaultValue="" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>

        <script type="text/javascript" src="../Scripts/SendMail.js"></script>

        <asp:HiddenField ID="checking" runat="server" Value="" />
        <div class="HeaderDevider">
        </div>
        <div class="TableRow">
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        </div>
        <div class="TableRow">
            </br>
            <asp:Panel ID="pnlSear" runat="server" DefaultButton="btnSearchEmail">
                <div class="TableFormContent" style="margin-left: 170px;">
                    <asp:TextBox ID="txtSearchEmail" runat="server" TabIndex="1" CssClass="CommonTextBox"
                        onKeyPress="javascript:calling(event)"></asp:TextBox>
                    <asp:LinkButton ID="btnSearchEmail" runat="server"   CssClass ="btn btn-primary"
                        OnClick="btnSearchEmail_Click" TabIndex="2" ><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                </div>
            </asp:Panel>
            <div id="drpselect" class="TableFormContent" style="margin-left: 80px;">
                <asp:Label ID="lblcontent" runat="server" Visible="false"></asp:Label>
                <asp:DropDownList ID="ddlcontact" runat="server" CssClass="CommonDropDownList" Visible="false"
                    OnSelectedIndexChanged="ddlcontact_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        
        <div style="width: 100%; overflow: auto;">
            <asp:ListView ID="lsvEmail" runat="server" DataKeyNames="Id" OnItemDataBound="lsvEmail_ItemDataBound"
                OnItemCommand="lsvEmail_ItemCommand" OnPreRender="lsvEmail_PreRender">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th style="width: 40px !important">
                                <asp:Label ID="lblSelect" runat="server" Text="Select" />
                            </th>
                            <th >
                                <asp:Label ID="btnEmailType" runat="server" ToolTip="Sort By Email Type" CommandName="Sort"
                                    CommandArgument="[GL].[Name]" Text="Email Type" />
                            </th>
                            <th >
                                <asp:LinkButton ID="btnDateTime" runat="server" ToolTip="Sort By Date And Time" CommandName="Sort"
                                    CommandArgument="SentDate" Text="Date & Time" />
                            </th>
                            <th >
                                <asp:LinkButton ID="btnFrom" runat="server" ToolTip="Sort By Sender Email Address"
                                    Width="70%" CommandName="Sort" CommandArgument="SenderEmail" Text="From" />
                            </th>
                            <th >
                                <asp:LinkButton ID="btnTo" runat="server" ToolTip="Sort By Receiver Email Address"
                                     CommandName="Sort" CommandArgument="ReceiverEmail" Text="To" />
                            </th>
                            <th >
                                <asp:LinkButton ID="btnSubject" runat="server" ToolTip="Sort By Subject" 
                                    CommandName="Sort" CommandArgument="Subject" Text="Subject" />
                            </th>
                            <th style="white-space: normal;">
                                Body
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="7">
                                <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblMailList" runat="server" class="EmptyDataTable alert alert-warning">
                        <tr>
                            <td>
                                No data was returned.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td align="left" >
                            <asp:Label ID="lblMemberEmailId" runat="server" style=" width :50px;"/>
                            <asp:Label ID="lblSelected" runat="server" />
                            <asp:HiddenField ID="hdfMemberEmailId" runat="server" />
                        </td>
                        <td >
                            <asp:Label ID="lblEmailType" runat="server"  />
                        </td>
                        <td >
                            <asp:Label ID="lblDataTime" runat="server"  />
                        </td>
                        <td  >
                            <asp:Label ID="lblSent" runat="server" />
                        </td>
                        <td  >
                            <asp:Label ID="lblReceived" runat="server" />
                        </td>
                        <td  >
                            <asp:HyperLink ID="hlnkSubject" runat="server" CssClass="NormalLink" 
                               ></asp:HyperLink>
                        </td>
                        <td >
                            <asp:HyperLink ID="hlnkEmailBody" runat="server" CssClass="NormalLink" ></asp:HyperLink>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
        <br />
        <br />
        <div id="divReply" runat="server" class="TableFormContent" style="margin-left: 200px;
            visibility: visible">
            <asp:Button ID="btnReply" runat="server" CausesValidation="false" CssClass="CommonButton"
                Text="Reply" OnClick="btnReply_Click" />
            <asp:Button ID="btnForward" runat="server" CausesValidation="false" CssClass="CommonButton"
                Text="Forward" OnClick="btnForward_Click" />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
