﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.BusinessFacade;
using TPS360.Web.UI;
using System.Collections.Generic;
using TPS360.Common.Shared;
using System.Web.UI;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Globalization;
using System.Net.Mail;
using System.Net;
using System.Threading;
using System.Text;

public partial class Controls_CandidateHireStatusUpdtae : RequisitionBaseControl
{
    private bool _IsAccessToEmployee = true;
    private static string UrlForEmployee = string.Empty;
    private static int IdForSitemap = 0;
    private string currentSiteMapId = "0";
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (CurrentMember != null && CurrentMember.Id > 0)
                {
                    string CurrentMemberRole = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);
                    IList<CandidatesHireStatus> candlst = Facade.CandidateHiringStatusUpdate_GetAll(Convert.ToInt32(CurrentMember.Id));
                    DisplayCandidateList(candlst);
                }
            }
            catch
            { }
        }
    }

    protected void ddlHiringStatus_OnSelectedIndexChanged(object sender, EventArgs e)
    {
       
    }  

    protected void btnSaveStatus_OnClick(object sender, EventArgs e)
    {       
            IFacade facade = new Facade();
            foreach (GridViewRow row in grdCandidateList.Rows)
            {
                Label lblID = (Label)row.FindControl("lblID");
                Label lblMember = (Label)row.FindControl("lblMemberID");
                Label lblManagerId = (Label)row.FindControl("lblManagerId");
                Label lblJobPostingID = (Label)row.FindControl("lblJobPostingID");
                DropDownList ddlstatus = (DropDownList)row.FindControl("ddlHiringStatus");

                int ID = Convert.ToInt32(lblID.Text);
                int MemberId = Convert.ToInt32(lblMember.Text);
                int JobStatus = Convert.ToInt32(ddlstatus.SelectedValue);
                int ManagerID = Convert.ToInt32(lblManagerId.Text);
                int JobPostingId = Convert.ToInt16(lblJobPostingID.Text);

                if (ddlstatus.SelectedItem.Text != "Please Select") // added by pravin khot on 19/Dec/2016
                {
                    string MemberIDText = lblMember.Text;
                    if (JobStatus > 0)
                    {
                        HiringMatrixLevels obj = facade.GetHiringMatrixLevelsById(Convert.ToInt32(JobStatus));
                        MiscUtil.EventLogForCandidateStatus(EventLogForCandidate.CandidateStatusChange, JobPostingId, MemberIDText, CurrentMember.Id, obj.Name, facade);

                        Facade.CandidateHiringStatus_Update(ID, JobStatus);
                         //Added by pravin khot on 19/Jan/2016**********
                        string AdminEmailId = string.Empty;
                        int AdminMemberid = 0;
                        SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                        if (siteSetting != null)
                        {
                            Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                            AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                            AdminMemberid = Facade.GetMemberIdByEmail(AdminEmailId);
                        }
                        //**********************END*************************
                        Facade.SendCandidateStatusEmail(Convert.ToInt32(CurrentMember.Id), MemberId, JobStatus, ManagerID, JobPostingId, AdminMemberid);
                    }

                    if (CurrentMember != null && CurrentMember.Id > 0)
                    {
                        string CurrentMemberRole = Facade.GetCustomRoleNameByMemberId(CurrentMember.Id);
                        IList<CandidatesHireStatus> candlst = Facade.CandidateHiringStatusUpdate_GetAll(Convert.ToInt32(CurrentMember.Id));
                        DisplayCandidateList(candlst);
                    }
                }
            }
            ScriptManager.RegisterClientScriptBlock(lblMessage, typeof(MiscUtil), "Show", "parent.ModalOperationCompleted('Successfully saved changes for Candidate Status');", true);
            //MiscUtil.ShowMessage(lblMessage,"Successfully saved changes for Candidate Status.", false);       
    }
    #endregion

    #region Functions
    public void DisplayCandidateList(IList<CandidatesHireStatus> candlst)
    {
        try
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("ID", typeof(string)));
            dt.Columns.Add(new DataColumn("ManagerId", typeof(string)));
            dt.Columns.Add(new DataColumn("MemberID", typeof(string)));
            //dt.Columns.Add(new DataColumn("CandidateName", typeof(string)));
            //dt.Columns.Add(new DataColumn("lnkCandidateName", typeof(string)));
            dt.Columns.Add(new DataColumn("RequisitionCode", typeof(string)));
            //dt.Columns.Add(new DataColumn("RequisitionJobTitle", typeof(string)));
            //dt.Columns.Add(new DataColumn("lnkJobTitle", typeof(string)));

            if (candlst != null)
            {
                for (int i = 0; i < candlst.Count; i++)
                {
                    dr = dt.NewRow();
                    dr["ID"] = candlst[i].ID;
                    dr["ManagerId"] = candlst[i].ManagerId;
                    dr["MemberID"] = candlst[i].MemberID;
                    //dr["CandidateName"] = candlst[i].CandidateName;
                    dr["RequisitionCode"] = candlst[i].RequisitionCode;
                    dt.Rows.Add(dr);
                }

                ViewState["CurrentTable"] = dt;
                grdCandidateList.DataSource = dt;
                grdCandidateList.DataBind();

                int itemCount = 0;
                foreach (GridViewRow row in grdCandidateList.Rows)
                {
                    //Bind1(candlst[itemCount].RequisitionJobTitle, candlst[itemCount].JobPostingID, row);
                    BindAndSetddlHiringStatus(candlst[itemCount].ID, candlst[itemCount].MemberID, candlst[itemCount].JobStatus, candlst[itemCount].ManagerId, candlst[itemCount].JobPostingID, Convert.ToDouble(candlst[itemCount].ID), row, candlst[itemCount].RequisitionJobTitle, candlst[itemCount].CandidateName);
                    itemCount++;
                }
                btnSaveStatus.Visible = true;
            }
            else if (candlst == null)
            {
                grdCandidateList.DataSource = null;
                grdCandidateList.DataBind();
                tblEmptyData.Visible = true;
                btnSaveStatus.Visible = false;
                lblEmptyMsg.Text = "No data was returned.";
            }
        }
        catch { }
    }

    public void Bind1(string RequisitionJobTitle, int JobPostingID, GridViewRow row)
    {
        try
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                HyperLink lnkJobTitle = row.Cells[1].FindControl("lnkJobTitle") as HyperLink;
                HyperLink lnkCandidateName = row.Cells[1].FindControl("lnkCandidateName") as HyperLink;
            }
        }
        catch
        {
        }
    }

    public void BindAndSetddlHiringStatus(int ID, int MemberID, int Jobstatus, int ManagerId, int JobPostingID, double value, GridViewRow row,string RequisitionJobTitle,string CandidateName)
    {
        try
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlHiringStatus = row.Cells[1].FindControl("ddlHiringStatus") as DropDownList;
                Label lblID = row.Cells[1].FindControl("lblID") as Label;
                Label lblMemberID = row.Cells[1].FindControl("lblMemberID") as Label;
                Label lblManagerId = row.Cells[1].FindControl("lblManagerId") as Label;
                Label lblJobPostingID = row.Cells[1].FindControl("lblJobPostingID") as Label;
                HyperLink lnkJobTitle = row.Cells[1].FindControl("lnkJobTitle") as HyperLink;
                HyperLink lnkCandidateName = row.Cells[1].FindControl("lnkCandidateName") as HyperLink;
  
                lblID.Text = Convert.ToString(ID);
                lblMemberID.Text = Convert.ToString(MemberID);
                lblManagerId.Text = Convert.ToString(ManagerId);
                lblJobPostingID.Text = Convert.ToString(JobPostingID);
                lnkJobTitle.Text = Convert.ToString(RequisitionJobTitle); 
                lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + JobPostingID + "&FromPage=JobDetail" + "','700px','570px'); return false;");
              
                if (_IsAccessToEmployee)
                {
                    UrlForEmployee = "~/ATS/TPS360Overview.aspx";
                    IdForSitemap = 361;
                    currentSiteMapId = "12";
                    if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                        ControlHelper.SetHyperLink(lnkCandidateName, UrlForEmployee, string.Empty, CandidateName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(MemberID), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentSiteMapId);
                }
                else 
                lnkCandidateName.Text = Convert.ToString(CandidateName);             

                MiscUtil.PopulateHiringMatrixLevelsForHiringManagerList(ddlHiringStatus, Facade, CurrentMember.Id);

                if (ddlHiringStatus.Items.Contains(ddlHiringStatus.Items.FindByValue(Jobstatus.ToString())))
                {
                    ddlHiringStatus.Items.FindByValue(Jobstatus.ToString()).Selected = true;
                }
            }
        }
        catch { }
    }

    #endregion

}
