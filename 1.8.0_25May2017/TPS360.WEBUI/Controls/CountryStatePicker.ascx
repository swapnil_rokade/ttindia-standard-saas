﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CountryStatePicker.ascx.cs"
    Inherits="TPS360.Web.UI.CountryStatePicker" %>

<%--<link href ="../assets/css/chosen.css" rel ="Stylesheet" />--%>
<link href="../assets/css/select2.css" rel="Stylesheet" />
 <div id="divState" runat="server">
  <div class="TableRow">
  
<%--  ****************Code change by pravin khot on 9/Feb/2016****Change sequence state/country  to country/state**********--%>
  <div class="TableRow">
        <div class="TableFormLeble" id ="divCountryLabel" runat ="server" >
            <asp:Label ID="lblCountry"  runat="server" Text="Country"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:DropDownList ID="ddlCountry" runat="server"  data-placeholder="Choose a Country..." 
                AutoPostBack="false" Width="158px" TabIndex="26">
            </asp:DropDownList>
            <span class="RequiredField" id="spanCountryRequired" runat ="server" visible ="false"  >*</span>
        </div>
    </div>
  
    </div></div>
  
        <div class="TableFormLeble" id="divStateLable" runat ="server" >
            <asp:Label ID="lblState" runat="server" Text="State"></asp:Label>:
        </div>
        <div class="TableFormContent">
        <asp:HiddenField ID="hdnSelectedState" runat ="server" Value ="0" />
   
            <asp:DropDownList ID="ddlState" runat="server"  EnableViewState ="true"   data-placeholder="Choose a State..." 
                Width="158px" TabIndex="25">
            </asp:DropDownList>
        </div>
  
<%--    ************************END******************************************--%> 
      <script src ="../js/AjaxScript.js" type ="text/javascript" ></script>
    <script src ="../js/AjaxVariables.js" type ="text/javascript" ></script>
    <script src="../assets/js/select2.js" type ="text/javascript" ></script>
    <%--<script src="../assets/js/chosen.jquery.js" type ="text/javascript" ></script>--%>
    
<%--   <script type="text/javascript"> 
   Sys.Application.add_load(function() { 
   $(".chzn-select").chosen();
  $(".chzn-select-deselect").chosen({allow_single_deselect:true});
  });
    </script>--%>
    
    
    <script type="text/javascript"> 
   Sys.Application.add_load(function() { 
    
    $('#<%=ddlCountry.ClientID %>').select2(
       { allowClear: true,
         placeholder: "Please Select"
       });
    $('#<%=ddlState.ClientID %>').select2(
       { allowClear: true,
         placeholder: "Please Select"
       });
       
    })
    
    $('#<%=ddlCountry.ClientID %>').on('change', function(e) {
    // Access to full data
    console.log($(this).select2('data'));
    });


    </script>
    

