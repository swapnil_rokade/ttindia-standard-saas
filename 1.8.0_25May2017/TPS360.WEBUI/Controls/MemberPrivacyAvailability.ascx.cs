﻿/*-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberPrivacyAvailability.ascx.cs
    Description: This is the user control page used to display the availabilty of the member.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Sep-11-2008          Shivanand           Defect id: 8653--Modified the method SaveResumePrivacy() to check the datefield "wdcDateOfBirth" 
                                                             and to display proper errormessage.
                                                             Modified the method Page_Load() to clear text on label for error message.
    0.2             Sep-29-2008          Shivanand           Defect Id: 8710--In the method SaveResumePrivacy() else if block is added to compare if the radio button
                                                             "I am available from" is selected & to compare the available date with th date selected from dropdown.
    0.3             Oct-07-2008          Jagadish            Defect Id: 8854-- Added method "rdbtnlstAvailability_SelectedIndexChanged" to disable calender control
                                                             when radio buttons other than "I am available from" is selected.
    0.4             Nov-11-2008          Jagadish            Defect Id: 9208-- Changed the alert message from "Successfully Update resume privacy & availability" to
                                                             "Successfully Updated resume privacy & availability".
    0.5             Dec-09-2008          Jagadish            Defect Id: 8857-- Chages made in the method "Page_Load()" to disable the save button
                                                             when login user is not a manager of the selected consultant.
    0.6             May-08-2009          Shivanand           Defect #10452; Changes made in method SaveResumePrivacy().
    0.7             Jul-15-2009         Gopala Swamy J       Enhancement Id:10944 ;Implemented LINQ
    0.8             Sep-24-2009         Nagarathna V.B       Enha#11473;passing current user for send email method call
    0.9             Dec-10-2009         Rajendra             Defect Id: 11979;Changes made in SaveResumePrivacy() & Page_Load();
-------------------------------------------------------------------------------------------------------------------------------------------       
*/
using System;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Xml.Linq;
using System.Collections;
using System.Linq;
using System.Web.UI;

namespace TPS360.Web.UI
{
    public partial class ControlMemberPrivacyAvailability : BaseControl
    {
        #region Member Variables

        private int _memberId = 0;
        private static string _memberrole = string.Empty;

        #endregion

        #region Properties

        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        public Int32 MemberId
        {
            set { _memberId = value; }
        }

        public int WorkFlowId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_WorkFlowId"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_WorkFlowId"] = value;
            }
        }

        public bool IsSendAlertByEmail
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_IsSendAlertByEmail"] ?? false);
            }
            set
            {
                ViewState[this.ClientID + "_IsSendAlertByEmail"] = value;
            }
        }

        #endregion

        #region Methods

        private void LoadRadioButtonList()
        {

            List<GenericLookup> lst = (List<GenericLookup>)Facade.GetAllGenericLookupByLookupType(LookupType.ResumeAvailability);

            var sortById = from o in lst  
                           orderby o.Id
                           select new
                           {
                               o.Id,
                               o.Name
                           };
            rdbtnlstAvailability.Items.Clear();
            rdbtnlstAvailability.DataSource = sortById;
            rdbtnlstAvailability.DataTextField = "Name";
            rdbtnlstAvailability.DataValueField = "Id";
            rdbtnlstAvailability.DataBind();
        }

        private void SaveResumePrivacy()
        {
            try
            {
                lblMessage.Text = String.Empty;
                bool allowSave = true;
                string msg;
                DateTime dtAvailable = DateTime.MinValue;
                if (rdbtnlstAvailability.SelectedIndex == 0)
                {
                    dtAvailable = System.DateTime.Now;
                }
                if (rdbtnlstAvailability.SelectedIndex == 1)
                {
                    if (wdcDateOfBirth.Text.Trim().ToString() != "" && wdcDateOfBirth.Text.Trim().ToString() != null)
                    {
                        dtAvailable = Convert.ToDateTime(wdcDateOfBirth.Text.Trim());
                        if (dtAvailable < DateTime.Today)
                        {
                            allowSave = false;
                        }
                    }
                    else
                    {
                        lblErrorDate.Text = "Please select the date of availabilty.";
                        return;
                    }
                }

                if (allowSave)
                {
                    MemberExtendedInformation memberExtended = Facade.GetMemberExtendedInformationByMemberId(_memberId);
                    if (memberExtended == null)
                    {
                        memberExtended = new MemberExtendedInformation();
                        memberExtended.MemberId = _memberId;
                        memberExtended.Availability = Int32.Parse(rdbtnlstAvailability.SelectedValue);
                        memberExtended.AvailableDate = dtAvailable;
                        Facade.AddMemberExtendedInformation(memberExtended);
                        SendAlert();
                        MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.ChangeResumePrivacy, Facade);
                        msg = "Successfully Updated Availability.";
                       // MiscUtil.ShowMessage(lblMessage, msg, false);
                    }
                    else if (rdbtnlstAvailability.SelectedIndex == 1 && memberExtended.AvailableDate != dtAvailable)
                    {
                        memberExtended.Availability = Int32.Parse(rdbtnlstAvailability.SelectedValue);
                        memberExtended.AvailableDate = dtAvailable;
                        Facade.UpdateMemberExtendedInformation(memberExtended);
                        SendAlert();
                        MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.ChangeResumePrivacy, Facade);
                        msg = "Successfully Updated Availability.";
                       // MiscUtil.ShowMessage(lblMessage, msg, false);
                    }
                    else if (memberExtended.Availability != Int32.Parse(rdbtnlstAvailability.SelectedValue))
                    {
                        memberExtended.Availability = Int32.Parse(rdbtnlstAvailability.SelectedValue);
                        memberExtended.AvailableDate = dtAvailable;
                        Facade.UpdateMemberExtendedInformation(memberExtended);
                        SendAlert();
                        MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.ChangeResumePrivacy, Facade);
                        msg = "Successfully Updated Availability.";
                       // MiscUtil.ShowMessage(lblMessage, msg, false);
                    }
                    else
                    {
                        msg = "No changes to save.";
                        MiscUtil.ShowMessage(lblMessage, msg, true);
                        return;
                    }
                    string availabilitytext= MiscUtil.GetMemberAvailability(memberExtended.Availability, memberExtended.AvailableDate, Facade);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(MiscUtil), "Availabilty", ";parent.changeAvailability('" + availabilitytext + "');parent.ShowStatusMessage('" + msg + "',false);", true);
                    //ScriptManager.RegisterClientScriptBlock(this, typeof(MiscUtil), "Availabilty", ";parent.ShowStatusMessage('" + msg + "',false);", true);
                }
                else
                {
                    
                     msg = "Available date should not be less than current date.";  //0.9
                     MiscUtil.ShowMessage(lblMessage, msg, true);

                }
               
            }
            catch (ArgumentException ex)
            {
                MiscUtil.ShowMessage(lblMessage, ex.Message, true);
            }
        }

        private void LoadResumePrivacy()
        {
            try
            {
                MemberExtendedInformation memberExtended = Facade.GetMemberExtendedInformationByMemberId(_memberId);
                if (memberExtended != null && memberExtended.Availability > 1)
                {
                    rdbtnlstAvailability.SelectedValue = memberExtended.Availability.ToString();
                    if (rdbtnlstAvailability.SelectedIndex == 1)
                    {
                        wdcDateOfBirth.Value = memberExtended.AvailableDate;
                    }
                }
                else
                {
                    rdbtnlstAvailability.SelectedIndex = 0;
                }
            }
            catch (ArgumentException ex)
            {
                MiscUtil.ShowMessage(lblMessage, ex.Message, true);
            }
        }

        private void LoadWorkFlowSetting()
        {
            ApplicationWorkflow applicationWorkflow = Facade.GetApplicationWorkflowByTitle(WorkFlowTitle.ATSShouldAssignedManagersForCandidateAlertedWhenCandidateChangesTheirAvailability.ToString());
            if (applicationWorkflow != null && applicationWorkflow.RequireApproval && applicationWorkflow.IsEmailAlert && (base.CurrentUserRole == ContextConstants.ROLE_CANDIDATE || base.CurrentUserRole == ContextConstants.ROLE_CONSULTANT))
            {
                IsSendAlertByEmail = true;
                WorkFlowId = applicationWorkflow.Id;
            }
            else
            {
                IsSendAlertByEmail = false;
            }
        }

        private void SendAlert()
        {
            if (IsSendAlertByEmail)
            {
                try
                {
                    string strMemberIds = String.Empty;
                    EmailHelper newMailManager = new EmailHelper();
                    newMailManager.From = MiscUtil.GetAdministratorEmail();
                    IList<MemberManager> memberManagerList = Facade.GetAllMemberManagerByMemberId(base.CurrentMember.Id);
                    foreach (MemberManager memberManager in memberManagerList)
                    {
                        Member member = Facade.GetMemberById(memberManager.ManagerId);
                        if (member != null && !string.IsNullOrEmpty(member.PrimaryEmail))
                        {
                            newMailManager.To.Add(member.PrimaryEmail);
                            if (string.IsNullOrEmpty(strMemberIds))
                            {
                                strMemberIds = member.Id.ToString();
                            }
                            else
                            {
                                strMemberIds = strMemberIds + "," + member.Id.ToString();
                            }
                        }
                    }

                    IList<ApplicationWorkflowMap> appWorkFlowMapList = Facade.GetAllApplicationWorkflowMapByApplicationWorkflowId(WorkFlowId);
                    foreach (ApplicationWorkflowMap appWorkFlowMap in appWorkFlowMapList)
                    {
                        Member member = Facade.GetMemberById(appWorkFlowMap.MemberId);
                        if (member != null && strMemberIds.IndexOf(member.Id.ToString()) == -1 && !string.IsNullOrEmpty(member.PrimaryEmail))
                        {
                            newMailManager.To.Add(member.PrimaryEmail);
                        }
                    }

                    newMailManager.Subject = UIConstants.SUBJECT_CHANGEAVAILABILITY_ALERT;
                    EmailHelper newMailHelper = new EmailHelper();

                    newMailManager.Body = newMailHelper.GetAlertChangeAvailability(MiscUtil.GetFirstAndLastName(base.CurrentMember.FirstName, base.CurrentMember.LastName), base.CurrentMember.PrimaryEmail, rdbtnlstAvailability.SelectedItem.Text);
                    newMailManager.Send(base.CurrentMember.Id);

                }
                catch { }
            }
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadRadioButtonList();
                LoadResumePrivacy();
                LoadWorkFlowSetting();
               
            }
            wdcDateOfBirth.Enabled = (rdbtnlstAvailability.SelectedIndex == 1);
            if (!btnSubmit.Enabled)
                btnSubmit.Style.Add("cursor", "auto");
            lblMessage.Text = "";
            lblMessage.CssClass = "";
            lblErrorDate.Text = "";
            if (Session["msg"] != null)
            {
                MiscUtil.ShowMessage(lblMessage, Session["msg"].ToString(), false);
                Session["msg"] = null;
            }
            if (Session["msgs"] != null)
            {
                MiscUtil.ShowMessage(lblMessage, Session["msgs"].ToString(), true );
                Session["msgs"] = null;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            SaveResumePrivacy();

                     
        }

        protected void rdbtnlstAvailability_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdbtnlstAvailability.SelectedIndex != 1)
                wdcDateOfBirth.Enabled = false;
            else
                wdcDateOfBirth.Enabled = true;
        }
        #endregion

    }
}
