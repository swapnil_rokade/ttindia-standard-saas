﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ManageHotList.ascx.cs
    Description: This is the user control page used to display the Hotlist.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 15-Jan-2009         Jagadish            Defect id: 9719: Changed Label 'lblName' to Hyperlink 'lnkCandidateName'.
    0.2                 04-Feb-2009         Jagadish            Defect id: 9699; Made Label text to empty string.
 *  0.3                 24-Mar-2009         Rajendra            Defect id: 10192; Added code to display Error messages.  
 *  0.4                 28-Aug-2009         Nagarathna V.B      Defect ID:11412 used currectmemberID instead memberId. 
 *  0.5                 14-sep-2009         Ranjit Kumar.I      Defect ID:11484:Added code line in AddEmployeeToHotList()
 *  0.6                 Nov-17-2009         Sandeesh            Enahncement#11814 :AM Notification - When a recruiter adds a candidate to a req the app will send a notification email to the Account Manager
 *  0.7                 25-Nov-2009         Rajendra            Defect id: 11909; Modified code in method AddToFinalList().  
 *  0.8                 11-Feb-2010         Gopala Swamy        Defect id: 12903; Put IF statement
 *  0.9                 05-Mar-2010         Sudarshan           Defect id: 12127; Refresh the page after savenotes and added message to session.
 *  1.0                 08-Mar-2010         Sudarshan           Defect id: 12150; Added method AddToPreSelectedList()
 *  1.1                 18-Mar-2010         Sudarshan           Defect id: 12127; Refresh the page after AddtoHotList() & AddToFinalList() and added message to session.
    1.2                  2/May/2016         pravin khot         New code added - use for setting- AllowCandidatetobeaddedonMultipleRequisition        
 * -------------------------------------------------------------------------------------------------------------------------------------------       
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Globalization;
using System.Net.Mail;
using System.Threading;
using System.Net;

namespace TPS360.Web.UI
{
    public partial class ctlManageHotList : BaseControl
    {
        #region Member Variables

        private int _memberId = 0;
        private static int checkODSEventfire = 0;
        private int _membergroupId = 0;
        private static int _managerId = 0;
        private static bool _mailsetting = false;
        private static string UrlForCandidate = string.Empty;
        private static int IdForCandidateSitemap = 0;
        private bool _IsAccessToCandidate = true;
        private  bool IsAccessToRemove = false;

        private bool _isMyList = false;
        #endregion

        #region Property

        public Int32 ManagerId
        {
            set { _managerId = value; }
        }

        private bool isMy = false;
        public bool IsMy
        {
            set { isMy = value; }
        }

        private int groupType = 0;
        public int GroupType
        {
            set { groupType = value; }
        }

        private string candidateOrConsultant;
        public string CandidateOrConsultant
        {
            set { candidateOrConsultant = value; }
        }

        #endregion

        #region Methods

        private void PrepareView()
        {
            
            PopulateDdlHotlist();
            BindListOfCandidate();
        }
     
               
        private void PopulateDdlHotlist()
        {
            int count = Facade.GetMemberGroupCountByManagerIDAndGroupType(base.CurrentMember.Id, (int)MemberGroupType.Candidate);

            if (count < 200)
            {
                TxtHotList.Visible = false;
                ddlSelectHotList.Visible = true;
                MiscUtil.PopulateMemberGroupByMemberId(ddlSelectHotList, Facade, _memberId, groupType);
                ddlSelectHotList.Items.Remove(ddlSelectHotList.Items.FindByValue(_membergroupId.ToString()));
            }
            else
            {
                ddlSelectHotList.Visible = false;
                TxtHotList.Visible = true;
            }
        }
     

        private void BindListOfCandidate()
        {
            lsvlistOfCandidates.DataBind();
        }

        private void AddToHotList()
        {
            string _candidateId = string.Empty;
            int intCounterAdded = 0;
            int intCounterChecked = 0;
            int intCounter = 0;
            int hotListId = 0;
            bool Istrue = false;
            if (ddlSelectHotList.Visible == true)
            {
                hotListId = Convert.ToInt32(ddlSelectHotList.SelectedValue);
            }
            else
            {
                if (TxtHotList.Text != "")
                    hotListId = Facade.GetMemberGroupByName(TxtHotList.Text).Id;
            }
            lblMessageCandidate.Text = "";
            string hotlist = string.Empty;
            hotlist = hdnSelectedIDS.Value;
            IList<string> ids = new List<string>();
            if(hotlist .Trim ()!=string .Empty )
            ids = hotlist.Split(',');
            foreach (string id in ids)
            {
                if (id != string.Empty)
                {
                    Istrue = MiscUtil.AddToHotList(hotListId, Convert.ToInt32(id), CurrentMember.Id, Facade);
                    if (Istrue)
                        intCounterAdded++;
                }
            }
            intCounterChecked = ids.Count;
           

            if (intCounterChecked > 0)
            {
                if (intCounterAdded > 0)
                {
                    BindListOfCandidate();
                    if (intCounterAdded == intCounterChecked)
                    {
                        Session["msg"] = "Successfully added selected candidates.";
                    }
                    else
                    {
                        Session["msg"] = intCounterAdded.ToString() + " of " + intCounterChecked.ToString() + " selected candidate(s) have been added successfully.";
                    }
                }
                else
                {
                    Session["msgs"] = "The Selected candidate(s) are already in the hot list.";
                }
            }
            else
            {
                Session["msgs"] = "Please select at least one candidate to add.";
            }
            Response.Redirect(Request.RawUrl);
        }

        private void RemoveFromHotList()
        {
            int Id = 0;
            int intCounter = 0;
            int counterChecked = 0;
            int counterDeleted = 0;
            string strMessage = string.Empty;
            bool boolError = false;
            checkODSEventfire = 0;
            foreach (ListViewItem lvi in lsvlistOfCandidates.Items)
            {
                if (ControlHelper.IsListItemDataRow(lvi.ItemType))
                {
                    CheckBox chkBox = (CheckBox)lvi.FindControl("chkBox");
                    if (chkBox.Checked)
                    {
                        Id = Convert.ToInt32(lsvlistOfCandidates.DataKeys[intCounter].Value);
                        if (Id > 0)
                        {
                            Facade.DeleteMemberGroupMapByIdandMemberGroupId(Id, _membergroupId);
                            counterDeleted++;
                        }
                        counterChecked++;
                    }
                }
                intCounter++;
            }
            BindListOfCandidate();
            if (counterChecked > 0)
            {
                if (counterDeleted > 0)
                {
                    if (counterDeleted == counterChecked)
                    {
                        strMessage = "The selected candidate(s) have been deleted successfully.";
                    }
                    else
                    {
                        strMessage = counterDeleted.ToString() + " of " + counterChecked.ToString() + " selected candidate(s) have been deleted successfully.";
                    }
                }
            }
            else
            {
                boolError = true;
                strMessage = "Please select at least one candidate to delete.";
            }

            MiscUtil.ShowMessage(lblMessageCandidate, strMessage, boolError);
        }

       

        private void SendEmailToAccountManager()
        {
            int hireCount = 0;
            string strMemberEmails = string.Empty;
            int requisitionid = GetSelectedRequisitionID();
            bool IsError = false;
            string strSMTP = string.Empty;
            string strUser = string.Empty;
            string strPwd = string.Empty;
            string strEmailid = string.Empty; 
            int intPort = 25;
            bool boolsslEnable = false;
            Hashtable siteSettingTable = null;

            SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            if (siteSetting != null)
            {
                siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
            }
            JobPosting jobPosting = Facade.GetJobPostingById(requisitionid);
            if (jobPosting != null)
            {
                Member member = Facade.GetMemberById(jobPosting.CreatorId);
                strEmailid = member.FirstName + " " + member.LastName + "<" + member.PrimaryEmail + ">";
            }
            if (siteSetting != null)
            {
                strUser = siteSettingTable[DefaultSiteSetting.SMTPUser.ToString()].ToString();
                strPwd = siteSettingTable[DefaultSiteSetting.SMTPassword.ToString()].ToString();
                strSMTP = siteSettingTable[DefaultSiteSetting.SMTP.ToString()].ToString();
                if (siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != null && siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString() != "")
                {
                    try
                    {
                        intPort = Convert.ToInt32(siteSettingTable[DefaultSiteSetting.SMTPort.ToString()].ToString());
                    }
                    catch (FormatException)
                    {
                        MiscUtil.ShowMessage(lblMessage, "SMTP Port data is incorrect in site settings.", true);
                        return;
                    }
                }

                if (siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString() != null)
                    boolsslEnable = Convert.ToBoolean(siteSettingTable[DefaultSiteSetting.SMTPEnableSSL.ToString()].ToString());
            }

            if (strUser != "" && strPwd != "" && strSMTP != "")
            {
                if (strEmailid.Trim() != "")
                {
                    IsError = SendEmail(strEmailid, strSMTP, strUser, strPwd, intPort, boolsslEnable);
                }
            }
            else
            {
                IsError = true;
            }

            if (IsError)
            {
                MiscUtil.ShowMessage(lblMessage, "Please set SMTP credentials in site settings.", true);
                return;
            }
        }

        private int GetSelectedRequisitionID()
        {
            return uclComReq.SelectedRequisitionId;
        }
        private void AddToPreSelectedList()
        {
            string msg="";
            bool isError=false ;
            int alreadyadded = 0;
            int requisitionId = 0;
            requisitionId = GetSelectedRequisitionID();
            if (requisitionId == 0) { MiscUtil.ShowMessage(lblMessage, "Please select requisition", true); return; }
            int intCounter = 0;
            int intCounterChecked = 0;
            int intCounterAdded = 0;
            string CanIds = hdnSelectedIDS.Value;
            IList<string> Ids = new List<string>();
            Ids = CanIds.Split(',');
            HiringMatrixLevels hiringMatrixLevel = Facade.HiringMatrixLevel_GetInitialLevel();
            int initialLevel = 0;
            if (hiringMatrixLevel != null) initialLevel = hiringMatrixLevel.Id;

            //**************code added by pravin khot on 2/May/2016  use for setting- AllowCandidatetobeaddedonMultipleRequisition**************
           #region
            Hashtable siteSettingTable = null;
            TPS360.BusinessFacade.Facade f = new TPS360.BusinessFacade.Facade();
            MemberExtendedInformation MailSetting = f.GetMemberExtendedInformationByMemberId(CurrentMember.Id);
            TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
            SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
            string chkstatus;
            chkstatus = siteSettingTable[DefaultSiteSetting.AllowCandidatetobeaddedonMultipleRequisition.ToString()].ToString();

            //***********CODE COMMENT BY PRAVIN KHOT ON 2/May/2016*****************
            //alreadyadded = Facade.MemberJobCart_AddCandidateToRequisition(base.CurrentMember.Id, Convert.ToString(CanIds), requisitionId);
            //*********************************END********************************
            
            if (chkstatus == "False")
            {
                alreadyadded = Facade.MemberJobCart_AddCandidateToRequisition(base.CurrentMember.Id, CanIds, requisitionId);
            }
            else
            {
                alreadyadded = Facade.MemberJobCart_AddCandidateToMultipleRequisition(base.CurrentMember.Id, CanIds, requisitionId);
            }
             #endregion
            //**********************************END 2/May/2016**********************************

           
            //foreach (string candidateId in Ids)
            //{
            //    if (candidateId != string.Empty)
            //    {

            //        //MemberJobCart MemberJobCart = Facade.GetMemberJobCartByMemberIdAndJobPostringId(Convert.ToInt32(candidateId), requisitionId);
            //        //if (MemberJobCart != null)
            //        //{
            //        //    MemberJobCartDetail MemberJobCartDetail = Facade.GetMemberJobCartDetailCurrentByMemberJobCartId(MemberJobCart.Id);
            //        //    if (MemberJobCartDetail == null)
            //        //    {
            //        //        intCounterAdded++;

            //        //    }
            //        //}
            //        //else { intCounterAdded++; }
            //        //MiscUtil.MoveApplicantToHiringLevel(requisitionId, Convert.ToInt32(candidateId), base.CurrentMember.Id, initialLevel, Facade);
                 
                    
            //    }
            //}
            intCounterChecked = Ids.Count;
            foreach (ListViewItem lsvItm in lsvlistOfCandidates.Items)
            {
                if (ControlHelper.IsListItemDataRow(lsvItm.ItemType))
                {
                    CheckBox chkItemCandidate = (CheckBox)lsvItm.FindControl("chkBox");

                    if (chkItemCandidate != null && chkItemCandidate.Checked)
                    {
                        chkItemCandidate.Checked = false;
                    }
                }
            }
            if (intCounterChecked > 0)
            {
                if (intCounterChecked - alreadyadded > 0)
                {
                   // SendEmailToAccountManager();
                    try
                    {
                        //MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateAddedToRequisition, requisitionId, CanIds, CurrentMember.Id, Facade);
                       // Facade.AddCandidateRequisitionStatus(requisitionId, CanIds, base.CurrentMember.Id);
                    }
                    catch
                    {
                    }
                    if (intCounterChecked-alreadyadded  == intCounterChecked)
                    {
                       msg = "Selected candidate(s) added successfully.";
                        
                    }
                    else
                    {
                        msg = (intCounterChecked - alreadyadded).ToString() + " of " + intCounterChecked.ToString() + " selected candidate(s) added successfully.";
                    }

                    uclComReq.SelectedRequisitionId = 0;
                }
                else
                {
                    //msg = "The Selected candidate(s) are already in the list.";//code comment by pravin khot on 2/May/2016
                    msg = "Candidate(s) selected from the list are already added to a Requisition and in the Hiring Process.";//new line added by pravin khot on 2/May/2016
                    isError =true ;
                }
            }
            MiscUtil.ShowMessage(lblMessage, msg, isError);
           // Response.Redirect(Request.RawUrl);
        }

        MemberHiringProcess memberHiringProcess;

        private bool SendEmail(string strEmailId, string strSMTP, string strUser, string strPwd, int intPort, bool boolsslEnable)
        {
            bool strSendStatus = true;

            CultureInfo cultInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo txtInfo = cultInfo.TextInfo;

            string strSenderName = txtInfo.ToTitleCase(base.CurrentMember.FirstName + " " + base.CurrentMember.LastName);
            string[] strUserName = strEmailId.Split('<');

            MailMessage message = new MailMessage();
            MailAddress mailAddressFrom = new MailAddress(base.CurrentMember.PrimaryEmail);//1.8
            MailAddress mailAddressTo = new MailAddress(strEmailId);
            message.From = mailAddressFrom;
            message.To.Add(mailAddressTo);
            message.IsBodyHtml = true;
            message.Subject = "NOTIFICATION: New candidates are added to the requisition - " ;
            message.Body = "Dear " + txtInfo.ToTitleCase(strUserName[0]) + ", <br/><br/> New candidates are added to the requisition - "  + ". Please go to your <b>My Requisition List</b> to view the requisition details." + "<br/><br/>Regards,<br/>" + strSenderName;
            SmtpClient emailClient = new SmtpClient(strSMTP, intPort);
            NetworkCredential SMTPUserInfo = new NetworkCredential(strUser, strPwd);
            emailClient.EnableSsl = boolsslEnable;
            emailClient.UseDefaultCredentials = false;
            emailClient.Credentials = SMTPUserInfo;

            object userState = message;
            emailClient.Timeout = 100;
            try
            {
                emailClient.SendAsync(message, userState);
                strSendStatus = false;
            }
            catch (Exception ex)
            {
                MiscUtil.ShowMessage(lblMessage, ex.Message.ToString(), true);
                strSendStatus = true;
            }

            return strSendStatus;
        }

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ddlSelectHotList.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToCandidate = false;
            else
            {
                IdForCandidateSitemap  = CustomMap.Id;
                UrlForCandidate  = "~/" + CustomMap.Url.ToString();
            }
            _mailsetting = MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id);
            if(_membergroupId >0)  
                    IsAccessToRemove = Facade.GetMemberGroupAccessByCreatorIdAndGroupId(CurrentMember.Id, _membergroupId);
            if (!IsPostBack)
            {
                _memberId = base.CurrentMember.Id;
                PrepareView();
                hdnSortColumn.Value = "btnlsvName";
                hdnSortOrder.Value = "ASC";
                PlaceUpDownArrow();
            }
            lblMessage.Text = lblMessage.CssClass = lblMessageCandidate.Text = lblMessageCandidate.CssClass = lblMessageMember.Text = lblMessageMember.CssClass = ""; //0.2            
           
            if (Session["msg"] != null)
            {
                MiscUtil.ShowMessage(lblMessage, Session["msg"].ToString(), false);
                Session.Remove("msg");
            }
            if (Session["msgs"] != null)
            {
                MiscUtil.ShowMessage(lblMessage, Session["msgs"].ToString(), true );
                Session.Remove("msgs");
            }

            //Setting Page Size
            string pagesize = "";
            pagesize = (Request.Cookies["ManageHotListRowPerPage"] == null ? "" : Request.Cookies["ManageHotListRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvlistOfCandidates.FindControl("pagerControl");
              if (PagerControl != null)
              {
                  DataPager pager = (DataPager)PagerControl.FindControl("pager");
                  if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
              }
              
        }
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvlistOfCandidates.FindControl(hdnSortColumn.Value);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                if (lnk.CommandArgument.Contains("Experience"))
                {
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Descending" : "Ascending"));
                }
                else
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        protected void btnAddToPreSelectList_Click(object sender, EventArgs e)
        {
            int intRowsSelected = 0;
            string CanIds = hdnSelectedIDS.Value;
            IList<string> Ids = new List<string>();
            Ids = CanIds.Split(',');
            foreach (string candidateId in Ids)
            {
                if(candidateId !=string .Empty )
                {
                    intRowsSelected++;
                }
            }
            if (intRowsSelected > 0)
                AddToPreSelectedList();
            else
                MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate to add.", true);
            uclComReq.SelectedRequisitionId = uclComReq.SelectedRequisitionId;
        }

        protected void btnAddToHotList_Click(object sender, EventArgs e)
        {
            AddToHotList();
            uclComReq.SelectedRequisitionId = uclComReq.SelectedRequisitionId;
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            RemoveFromHotList();
            hdnSelectedIDS.Value = "";
            uclComReq.SelectedRequisitionId = uclComReq.SelectedRequisitionId;
        }

        protected void btnEmail_Click(object sender, EventArgs e)
        {
            string applicantIds = hdnSelectedIDS.Value; 
            if (applicantIds != string.Empty)
            {
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Dashboard/" + UrlConstants.Dashboard.NEWMAIL, "", UrlConstants.PARAM_PAGE_FROM, "CandidateList", UrlConstants.PARAM_SELECTED_IDS, applicantIds);
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, typeof(System.Web.UI.Page), "NewMail", "<script>btnMewMail_Click('" + url + "');</script>", false);
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate to email.", true);
            }
            uclComReq.SelectedRequisitionId = uclComReq.SelectedRequisitionId;
        }

      
        #endregion

        #region Listview Event
       
        private bool CheckSelectedList(int ID)
        {
            char[] del = { ',' };
            string[] arrID = hdnSelectedIDS.Value.Split(del, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in arrID)
            {
                if (Convert.ToInt32(s) == ID)
                {
                    return true;
                }
            }
            return false;
        }
        protected void lsvlistOfCandidates_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Candidate member = ((ListViewDataItem)e.Item).DataItem as Candidate ;

                if (member != null)
                {
                    HyperLink lnkCandidateName = (HyperLink)e.Item.FindControl("lnkCandidateName"); // 0.1
                    Label lblPosition = (Label)e.Item.FindControl("lblPosition");
                    Label lblCurrentCompany = (Label)e.Item.FindControl("lblCurrentCompany");
                    Label lblYearsOfExp = (Label)e.Item.FindControl("lblYearsOfExp");
                    Label lblMobile = (Label)e.Item.FindControl("lblMobile");
                    LinkButton  lblEmail = (LinkButton )e.Item.FindControl("lblEmail");
                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    Label lblRemarks = (Label)e.Item.FindControl("lblRemarks");
                    CheckBox chkBox = (CheckBox)e.Item.FindControl("chkBox");
                    chkBox.Checked = CheckSelectedList(member.Id);
                    HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");
                    hdnID.Value = member.Id.ToString ();
                 
                    string strFullName = member.FirstName + " " + member.LastName;
                    if (_IsAccessToCandidate)
                    {
                        if(IdForCandidateSitemap >0 && UrlForCandidate !=string .Empty )
                             ControlHelper.SetHyperLink(lnkCandidateName, UrlForCandidate, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(member.Id), UrlConstants.PARAM_SITEMAP_ID, IdForCandidateSitemap.ToString(), UrlConstants .PARAM_SITEMAP_PARENT_ID ,"12");
                    }
                    else
                        lnkCandidateName.Text = strFullName;
                    lblMobile.Text = member.CellPhone;
                    if (!_mailsetting)
                        lblEmail.Text = "<a href=MailTo:" + member .PrimaryEmail  + ">" + member.PrimaryEmail + "</a>";
                    else
                    {
                        lblEmail.Text = member.PrimaryEmail;
                        SecureUrl urlLink = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_MEMBER_ID, member.Id.ToString());
                        lblEmail.Attributes.Add("onclick", "btnMewMail_Click('" + urlLink + "')"); 

                    }
                    lblCity.Text = member.PermanentCity;
                        lblPosition.Text = member.CurrentPosition;
                       // lblYearsOfExp.Text = member.TotalExperienceYears;
                        //if (member.Remarks.ToString().Trim().Length > 40)
                        //    lblRemarks.Text = member.Remarks.Trim().Substring(0, 40) + "...";
                        //else
                        //    lblRemarks.Text = member.Remarks.Trim();
                        //lblRemarks.ToolTip = member.Remarks;

                        ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
                        if (btnDelete != null)
                        {
                            btnDelete.CommandArgument = member.Id.ToString(); ;
                            btnDelete.OnClientClick = "return ConfirmDelete('candidate from hot list');";
                        }
                }
            }
        }

        protected void lsvlistOfCandidates_PreRender(object sender, EventArgs e)
        {
            divBulkActions.Visible = true;
              ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvlistOfCandidates   .FindControl("pagerControl");
              if (PagerControl != null)
              {
                  DataPager pager = (DataPager)PagerControl.FindControl("pager");
                  if (pager != null)
                  {
                      DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                      if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                  }
                  else
                  {
                      divBulkActions.Visible = false;
                  }
                  HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                  if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "ManageHotListRowPerPage";
              }
              else
              {
                  divBulkActions.Visible = false;
              }
              PlaceUpDownArrow();
        }
        protected void lsvlistOfCandidates_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                }
                if (e.CommandName == "DeleteItem")
                {
                    int id;
                    int.TryParse(e.CommandArgument.ToString(), out id);
                    int MemberGroupid = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);
                    Facade.DeleteMemberGroupMapByIdandMemberGroupId(id, MemberGroupid);
                    MiscUtil.ShowMessage(lblMessageCandidate, "Candidate Successfully removed from Hot List", false );
                    lsvlistOfCandidates.DataBind();
                }
            }
            catch
            {
            }
        }
       
        #endregion

        #region Datasource Event

        protected void odsCandidateList_OnInit(object sender, EventArgs e)
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
            {
                _membergroupId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);
            }

            odsCandidateList.SelectParameters.Add("memberGroupId", _membergroupId.ToString());
            odsCandidateList.SelectParameters.Add("SortOrder", "asc");
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "FirstName";
            }
            odsCandidateList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
        }

        protected void odsCandidateList_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            object totalRows = e.ReturnValue;
            if (totalRows != null)
            {
                checkODSEventfire++;
                if (checkODSEventfire == 2)
                {
                    if (Convert.ToInt32(totalRows) < 1)
                    {
                        btnRemove.Visible = false;
                        checkODSEventfire = 0;
                    }
                }
            }
            else
            {
                btnRemove.Visible = false;
            }
        }

        #endregion

        #endregion

    }
}