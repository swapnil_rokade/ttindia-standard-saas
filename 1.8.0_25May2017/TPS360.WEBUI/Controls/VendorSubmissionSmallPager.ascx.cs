﻿/*<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Controls_VendorSubmissionSmallPager.ascx.cs
    Description: This is the user control page used to display the ooption like ToDo,PDF,Print and many more
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date              Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            15/March/2017     pravin khot         Added new column -status
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>*/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
namespace TPS360.Web.UI
{
    public partial class Controls_VendorSubmissionSmallPager : BaseControl
    {
        public int ContactId
        {
            set
            {
                odsVendorSubmissions.SelectParameters["ContactId"].DefaultValue = value.ToString();
            }
        }

        private bool _IsVendorCandidateProfile=false ;
        public bool VendorCandidateProfile
        {
            get
            {
                return _IsVendorCandidateProfile;
            }
            set
            {
                _IsVendorCandidateProfile = value;
            }
        }
        string currentSiteMapId = "0";
        bool _IsAccessToCandidate = true;
        private string UrlForCandidate = string.Empty;
        private int SitemapIdForCandidate = 0;


        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsUserVendor)
            {
                currentSiteMapId = "12";
                CustomSiteMap CustomMap = new CustomSiteMap();
                CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
                if (CustomMap == null) _IsAccessToCandidate = false;
                else
                {

                    SitemapIdForCandidate = CustomMap.Id;
                    UrlForCandidate = "~/" + CustomMap.Url.ToString();
                }
            }
            else
            {
                currentSiteMapId = "653";
                SitemapIdForCandidate =Convert .ToInt32 ( UrlConstants.Candidate.VENDOR_CANDIDATE_OVERVIEW_SITEMAP_ID);
                UrlForCandidate = UrlConstants.Vendor.OVERVIEW;
            }

            if (IsUserVendor)
            {
                
                if (SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString() != null)
                {
                    hdnMySubmission.Value = SiteSetting[DefaultSiteSetting.CompanyName.ToString()].ToString();
                    //this.Page.Title = hdnMySubmission.Value + " - " + "My Submissions";

                    this.Page.Title = hdnMySubmission.Value + " - " + "Dashboard";
                }
            }
            else
            {
                if (!IsPostBack)
                {
                    string IDs = (Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]);
                    Company com = Facade.GetCompanyById(Convert.ToInt32(IDs));
                    if (com != null)
                    {
                        hdnMySubmission.Value = com.CompanyName;
                    }
                }
                this.Page.Title = hdnMySubmission.Value + " - " + "Submissions";
            }
            odsVendorSubmissions.SelectParameters["StartDate"].DefaultValue = DateTime.MinValue.ToShortDateString();
            odsVendorSubmissions.SelectParameters["EndDate"].DefaultValue = DateTime.MinValue.ToShortDateString();
            if (IsUserVendor)
            {
                odsVendorSubmissions.SelectParameters["ContactId"].DefaultValue = base.CurrentMember.Id.ToString();
            }
            else
            {
              string IDs = (Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]);
                odsVendorSubmissions.SelectParameters["VendorId"].DefaultValue = IDs.ToString ();
            }
            if (VendorCandidateProfile)
            {
                string CanIDs = (Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID ]);
                odsVendorSubmissions.SelectParameters["CandidateId"].DefaultValue = CanIDs;
            }

            //DashboardVendorSubmissionsRowPerPage
            string pagesize = "";
            pagesize = (Request.Cookies["DashboardVendorSubmissionsRowPerPage"] == null ? "" : Request.Cookies["DashboardVendorSubmissionsRowPerPage"].Value); ;
            ASP.controls_smallpagercontrol_ascx  PagerControl = (ASP.controls_smallpagercontrol_ascx )this.lsvVendorSubmissions.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
            }
        }


        protected void lsvVendorSubmissions_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                
                VendorSubmissions vendorSubmission = ((ListViewDataItem)e.Item).DataItem as VendorSubmissions;
                Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
                HyperLink hlnkCandidateName = (HyperLink)e.Item.FindControl("hlnkCandidateName");
                HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                HyperLink hlnkSubmittedBy = (HyperLink)e.Item.FindControl("hlnkSubmittedBy");             
              

                lblDateTime.Text = vendorSubmission.SubmittedDate.ToShortDateString();

                if (IsUserVendor)
                {
                    int creatorid = Facade.GetCreatorIdForMember(vendorSubmission.CandidateId);
                    if (base.CurrentMember.Id == creatorid)
                    {
                        hlnkCandidateName.Text = vendorSubmission.CandidateName;
                        ControlHelper.SetHyperLink(hlnkCandidateName, UrlForCandidate, string.Empty, vendorSubmission.CandidateName, UrlConstants.PARAM_MEMBER_ID, vendorSubmission.CandidateId.ToString(), UrlConstants.PARAM_SITEMAP_ID, SitemapIdForCandidate.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentSiteMapId);
               
                    }
                    else
                    {
                        hlnkCandidateName.Text = vendorSubmission.CandidateName + " (Ownership Period Expired)";
                        hlnkCandidateName.Enabled = false;
                    }
                }
                else
                {
                    hlnkCandidateName.Text = vendorSubmission.CandidateName;
                    ControlHelper.SetHyperLink(hlnkCandidateName, UrlForCandidate, string.Empty, vendorSubmission.CandidateName, UrlConstants.PARAM_MEMBER_ID, vendorSubmission.CandidateId.ToString(), UrlConstants.PARAM_SITEMAP_ID, SitemapIdForCandidate.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentSiteMapId);
                }
                lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + vendorSubmission.JobPostingID + "&FromPage=Vendor" + "','700px','570px'); return false;");
                lnkJobTitle.Text = vendorSubmission.JobTitle;
                hlnkSubmittedBy.Text = vendorSubmission.ContactName;
                SecureUrl ModalUrl = UrlHelper.BuildSecureUrl("../Modals/CompanyContact.aspx",string.Empty,UrlConstants.PARAM_CONTACT_ID,vendorSubmission.Id.ToString());
                hlnkSubmittedBy.Attributes.Add("onclick", "EditModal('"+ModalUrl.ToString() + "','675px','450px'); return false;");
                if (IsUserVendor)
                {
                    if (VendorCandidateProfile)
                    {
                        HtmlTableCell td = (HtmlTableCell)hlnkCandidateName.Parent;
                        HtmlTableCell thSubmittedBy = (HtmlTableCell)lsvVendorSubmissions.FindControl("thCandidateName");
                        HtmlTableCell tdPager = (HtmlTableCell)lsvVendorSubmissions.FindControl("tdPager");
                        thSubmittedBy.Visible = false;
                        td.Visible = false; tdPager.ColSpan = 4;
                    }
                    else
                    {
                        HtmlTableCell td = (HtmlTableCell)hlnkSubmittedBy.Parent;
                        HtmlTableCell thSubmittedBy = (HtmlTableCell)lsvVendorSubmissions.FindControl("thSubmittedBy");
                        HtmlTableCell tdPager = (HtmlTableCell)lsvVendorSubmissions.FindControl("tdPager");
                        thSubmittedBy.Visible = false;
                        td.Visible = false; tdPager.ColSpan = 4;
                    }
                }

                //***********Code added by pravin khot on 15/March/2017***************
                Label lblStatus = (Label)e.Item.FindControl("lblStatus");
                lblStatus.Text = vendorSubmission.CurrentStatus;
                //int reqCount = Facade.GetActiveRequsiutionCountByMemberId(vendorSubmission.CandidateId);
                //CandidateOverviewDetails overviewdetail = Facade.GetCandidateOverviewDetails(vendorSubmission.CandidateId);
                //if (overviewdetail.Status == 1)
                //{
                //    lblStatus.Text = "InActive";
                //}
                //else if (reqCount > 1)
                //{
                //    lblStatus.Text = "Profile in Process (" + reqCount.ToString() + ") Requisitions";
                //}
                //else
                //{
                //    lblStatus.Text = "Profile in Process by " + Facade.GetRecruiternameByMemberId(vendorSubmission.CandidateId);
                //}
                //************************END*********************************************
            }
        }

        protected void lsvVendorSubmissions_PreRender(object sender, EventArgs e)
        {
            lsvVendorSubmissions.DataBind();
            ASP.controls_smallpagercontrol_ascx PagerControl = (ASP.controls_smallpagercontrol_ascx)this.lsvVendorSubmissions.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Controls.Count >= 1)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "DashboardVendorSubmissionsRowPerPage";
            }
            PlaceUpDownArrow();
            if (lsvVendorSubmissions.Controls.Count == 0)
            {
                lsvVendorSubmissions.DataSource = null;
                lsvVendorSubmissions.DataBind();
            }
        }

        protected void lsvVendorSubmissions_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }

                }
            }
            catch
            {
            }
        }
        private void PlaceUpDownArrow()
        {
            try
            {

                LinkButton lnk = (LinkButton)lsvVendorSubmissions.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }
    }
}
