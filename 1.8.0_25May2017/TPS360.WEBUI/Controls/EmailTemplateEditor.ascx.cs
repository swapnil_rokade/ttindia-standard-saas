﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.IO;

namespace TPS360.Web.UI
{
    public partial class EmailTemplateEditor : RequisitionBaseControl 
    {
        #region Member Variables
        bool mailsetting = false;
        #endregion

        #region Properties
        private bool isNew
        {
            get;
            set;
        }
        #endregion

        #region Methods
        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Employee/MailSetup.aspx", string.Empty);
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Open", "<script>window.open('" + url + "');</script>", false);
            }

        }

        private SearchAgentEmailTemplate BuildSearchAgentEmailTemplate(SearchAgentEmailTemplate template, bool isNew)
        {

            template.JobPostingID = CurrentJobPostingId;
            template.Subject = MiscUtil.RemoveScript(txtSubject.Text.Trim());
            template.EmailBody = txtEmailTemplate.InnerHtml ;
            if (isNew)
                template.CreatorId = template.SenderId = CurrentMember.Id;
            else template.UpdatorId = CurrentMember.Id;
            template.IsRemoved = false;
            return template;
        }
        #endregion

        #region PageEvents
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CurrentJobPostingId > 0)
            {


                SearchAgentEmailTemplate template = Facade.GetSearchAgentEmailTemplateByJobPostingId(CurrentJobPostingId);
                if (template != null)
                {
                    if (!IsPostBack)
                    {
                        hdnClear.Value = "0";
                        isNew = false;
                        txtSubject.Text = MiscUtil.RemoveScript(template.Subject, string.Empty);
                        txtEmailTemplate.InnerHtml = template.EmailBody;
                        Member member = Facade.GetMemberById(template.SenderId);
                        if (member != null)
                            lblFrom.Text = member.FirstName + " " + member.LastName + " [" + member.PrimaryEmail + "]";
                    }
                    if (template.SenderId == CurrentMember.Id)
                        mailsetting = MiscUtil.IsValidMailSetting(Facade, template.SenderId);
                    else
                        mailsetting = true;

                }
                else
                {
                    isNew = true;
                    txtEmailTemplate.InnerHtml = getDefaultMailTemplate();
                    if (!IsPostBack || lblFrom .Text =="")
                    {
                        lblFrom.Text = CurrentMember.FirstName + " " + CurrentMember.LastName + " [" + CurrentMember.PrimaryEmail + "]";
                    }
                    mailsetting = MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id);
                }
            }
            if (!mailsetting)
            {
                divs.Visible = true;
                uclConfirmm.MsgBoxAnswered += MessageAnswered;
            }
            
        }
      
        private string getDefaultMailTemplate()
        {
            string JobTitle="", Signature="";
            if (hdnDefaultMailTemplate.Value.Trim() == "")
            {
                hdnDefaultMailTemplate.Value = MiscUtil.getDefaultSearchAgentMailTemplate(CurrentJobPostingId , CurrentMember .Id, out JobTitle,Facade );
                hdnSubject.Value = txtSubject.Text = JobTitle;
            }
            txtSubject.Text =MiscUtil .RemoveScript ( hdnSubject.Value,string .Empty );
            return hdnDefaultMailTemplate.Value;
            //if (hdnDefaultMailTemplate.Value.Trim() == "")
            //{
            //    string JobTitle = "", JobPostingCode = "";
            //    hdnDefaultMailTemplate.Value = MiscUtil.GetJobDetailTable(CurrentJobPostingId, Facade, out JobTitle, out JobPostingCode, "");
            //    hdnSubject .Value = txtSubject.Text = "Open Job Position: " + JobTitle;

            //       MemberSignature signat = new MemberSignature();
            //        signat = Facade.GetActiveMemberSignatureByMemberId(base.CurrentMember.Id);
            //        if (signat != null)
            //        {
            //            hdnSignature.Value = signat.Signature;
            //        }
            //}
            //txtSubject.Text = hdnSubject.Value;
            //string strMess = "Hello &lt;candidate_first_name&gt;,<br/><br/>I came across your resume on a Job portal and wanted to know if you would be interested in the position below. If so, please respond with your updated resume and availability to discuss the position.";
            //strMess += "<br/><br/><hr/>" + hdnDefaultMailTemplate.Value ;
            //if (hdnSignature.Value.Trim() != "")
            //{
            //    strMess += "<br/><br/>" + hdnSignature.Value;
            //}
            //strMess += "<br/><br/>";
            //strMess += "If you do not wish to receive job opening notices from us, please <a href=\"\">click here to unsubscribe.</a>";
           
            //return strMess;
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (hdnClear.Value.Trim() == "1")
            {
                hdnClear.Value = "0";
                SearchAgentEmailTemplate template = Facade.GetSearchAgentEmailTemplateByJobPostingId(CurrentJobPostingId);
                if (template != null)
                {
                    isNew = false;
                    txtSubject.Text =MiscUtil .RemoveScript ( template.Subject,string .Empty );
                    txtEmailTemplate.InnerHtml = template.EmailBody;
                    Member member = Facade.GetMemberById(template.SenderId);
                    if (member != null)
                        lblFrom.Text = member.FirstName + " " + member.LastName + " [" + member.PrimaryEmail + "]";
                }
            }
        }
        #endregion

     
        #region Button Events
        protected void btnResetTemplate_Click(object sender, EventArgs e)
        {
            string JobTitle, JobPostingCode;
            txtEmailTemplate.InnerHtml = getDefaultMailTemplate();
            AjaxControlToolkit.ModalPopupExtender ex = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMasterTitle").FindControl("reqPublish").FindControl("mpeEmailEditor");
            ex.Show();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (mailsetting)
            {
                SearchAgentEmailTemplate template = new SearchAgentEmailTemplate();
                if (isNew)
                    Facade.AddSearchAgentEmailTemplate(BuildSearchAgentEmailTemplate(template, true));
                else
                {
                    template = Facade.GetSearchAgentEmailTemplateByJobPostingId(CurrentJobPostingId);
                    template = BuildSearchAgentEmailTemplate(template, false);
                    Facade.UpdateSearchAgentEmailTemplate(template);
                }
            }
            else
            {
                AjaxControlToolkit.ModalPopupExtender ex = (AjaxControlToolkit.ModalPopupExtender)this.Page.Master.FindControl("cphHomeMasterTitle").FindControl("reqPublish").FindControl("mpeEmailEditor");
                ex.Show();
                divs.Visible = true;
                uclConfirmm.AddMessage("Mail Server SMTP details have not been entered. Would you like to enter them now?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
            }
        }
        #endregion
        

       
}
}