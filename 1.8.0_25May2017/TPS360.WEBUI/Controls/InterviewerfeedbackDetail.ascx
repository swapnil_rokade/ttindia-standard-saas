﻿ <%-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewerfeedbackDetail.ascx
    Description:  
    Created By: pravin khot
    Created On:22/Dec/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------

    ------------------------------------------------------------------------------------------------------------------------------   
    --%>
    

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterviewerfeedbackDetail.ascx.cs" Inherits="TPS360.Web.UI.ControlsInterviewerfeedbackDetail" %>

<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
 <asp:UpdatePanel ID="upNotes" runat ="server" >
 <ContentTemplate >
 
 <asp:HiddenField ID="hdnInterviewFeebackId" runat ="server" />
<div class="TableRow" style="text-align:left">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
    </div>
   <%--  <div class="TableRow">

        <div class="TableFormLeble">
            <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>:
        </div>
        <div class="TableFormContent">
        <asp:TextBox ID="txtTitle" runat="server" CssClass="CommonTextBox"  ValidationGroup ="NoteValidation" ></asp:TextBox>
            <span class="RequiredField">*</span>
        </div>
    </div>
   
    <div class ="TableRow">
        <div  class ="TableFormValidatorContent" style="text-align:center">
            <asp:RequiredFieldValidator ID ="rfvTitle" runat ="server" Display ="Dynamic"  ErrorMessage ="Please Enter Title." ControlToValidate ="txtTitle" ValidationGroup ="NoteValidation"></asp:RequiredFieldValidator> 
        </div>
    </div>
    
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label ID="lblRound" runat="server" Text="Round"></asp:Label>:
        </div>
        <div class="TableFormContent">
        <asp:DropDownList ID="ddlInterviewRounds" runat="server" CssClass="CommonDropDownList" ValidationGroup ="NoteValidation" ></asp:DropDownList>
            <%--<span class="RequiredField">*</span>--%>
      <%--  </div>
    </div>
    
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label ID="lblFeedback" runat="server" Text="Feedback"></asp:Label>:
        </div>
        <div class="TableFormContent">
        <asp:TextBox ID="txtNotes" runat="server" CssClass="CommonTextBox" TextMode="MultiLine" Rows="6" ValidationGroup ="NoteValidation" ></asp:TextBox>
            <span class="RequiredField">*</span>
        </div>
    </div>
    
    <div class ="TableRow">
        <div  class ="TableFormValidatorContent" style="text-align:center">
            <asp:RequiredFieldValidator ID ="rfvNotes" runat ="server" Display ="Dynamic"  ErrorMessage ="Please Enter Feedback." ControlToValidate ="txtNotes" ValidationGroup ="NoteValidation"></asp:RequiredFieldValidator> 
        </div>
    </div>
     <div class ="TableRow">
            <div class="TableFormLeble" style="width:20%">
            </div>
            <div class="TableFormContent" style="margin-left:45%">
                <asp:Button ID="btnSave" CssClass="CommonButton" runat="server" Text="Save"  OnClick="btnSave_Click"  ValidationGroup ="NoteValidation" />
                </div>
    </div>--%>
   <div class="TableRow" style ="max-height : 400px;" >
             <div class="TabPanelHeader">
                    List of Interviewers
                </div>
            <asp:ListView ID="lsvFeedback" runat="server" OnItemDataBound="lsvFeedback_ItemDataBound" OnItemCommand="lsvFeedback_ItemCommand" OnPreRender="lsv_Feedback_OnRender">
                <LayoutTemplate>
                    <table id="tblFeedback" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                        
                            <th style="width: 40%; white-space: nowrap;">
                                Interviewer Name
                            </th>
                            <th style="width: 25%; white-space: nowrap;">
                                Interviewer Type
                            </th>
                             <th style="text-align: center;" id="thAction" runat="server" width="30px">
                                 Action
                             </th>                          
                        </tr>      
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                        <tr>
                            <td>
                                No Feedback Available.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                    
                        <td>
                             <asp:Label ID="lblInterviewerName" runat="server" />
                            <%--<asp:LinkButton ID="lblInterviewerName" runat="server" ToolTip ="Show Interviewer Feedback/Assessment Form"></asp:LinkButton>--%>
                        </td>
                        <td>
                            <asp:Label ID="lblInterviewerType" runat="server" />
                        </td>                                            
                       <td style="text-align: center;" runat ="server" id="tdAction">
                            <asp:ImageButton ID="btnFeedback" SkinID="sknEditButton" runat="server" ToolTip ="Show Interviewer Feedback/Assessment Form"  CommandName="EditItem">
                            </asp:ImageButton>
                       </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
   </div>  

</ContentTemplate>
 </asp:UpdatePanel>