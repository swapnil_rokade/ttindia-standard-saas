﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:Control/ProductivityReport.ascx.cs
    Description :This is user control common for ProductivityReport.aspx and MyProductivityReport.aspx 
    Created By: Sumit Sonawane
    Created On: 25/May/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
  
-------------------------------------------------------------------------------------------------------------------------------------------       

*/



using System;
using System.Web.Security;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using TPS360.Common;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Configuration;
using System.Web;

namespace TPS360.Web.UI
{
    public partial class ControlProductivityReport : BaseControl
    {
        

        bool _IsAccessToCandidate = true;
        private string UrlForCandidate = string.Empty;
        private int SitemapIdForCandidate = 0;
        private static bool _isMyProductivityReport = true;
        private static bool _isTeamProductivityReport = true;
        #region Methods

        #endregion
        public bool IsMyProductivityReport 
        {
            set { _isMyProductivityReport = value; }
        }
        public bool IsTeamProductivityReport
        {
            set { _isTeamProductivityReport = value; }
        }


        #region Events

        private void GenerateProductivityReport(string format, string TypeofReport)
        {
            if (string.Equals(format, "word"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=ProductivityReport" + TypeofReport + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".doc");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msword";
                Response.Output.Write(GetReportTable(TypeofReport,format));
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "excel"))
            {
                Response.AddHeader("content-disposition", "attachment;filename=ProductivityReport" + TypeofReport + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".xls");
                Response.ContentEncoding = System.Text.Encoding.ASCII;
                Response.ContentType = "application/msexcel";
                Response.Output.Write(GetReportTable(TypeofReport, format));
                Response.Flush();
                Response.End();
            }
            else if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PageWidth = 0;
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;

                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetReportTable(TypeofReport, format));

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                response.AddHeader("Content-Disposition", "attachment; filename=ProductivityReport" + TypeofReport + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }

        private string GetReportTable(string TypeOfReport,string format)
        {
            StringBuilder productivityreport = new StringBuilder();
            EmployeeDataSource obemployee = new EmployeeDataSource();
            bool IsMyProductivityReport = _isMyProductivityReport ? true : false;
            bool IsTeamReport = _isTeamProductivityReport ? true : false;
            string userId = "";
            if (_isMyProductivityReport)
                userId = CurrentMember.Id.ToString();
            else
                userId = "";
            string base64Data = "";
            string path = "";
            IList<EmployeeProductivity> loglist = null;
            if (TypeOfReport == "ByEmployee")
                loglist = obemployee.GetPagedEmployeeProductivityReportByEmployee(dtReferralDate.StartDate.ToString(), dtReferralDate.EndDate.ToString(), ddlTeam.SelectedValue, hdnUserSelectedValue.Value, _isTeamProductivityReport, CurrentMember.Id, "", null, -1, -1);
            else loglist = obemployee.GetPagedEmployeeProductivityReportByTeam(dtReferralDate.StartDate.ToString(), dtReferralDate.EndDate.ToString(), ddlTeam.SelectedValue, IsMyProductivityReport, _isTeamProductivityReport, CurrentMember.Id.ToString(), hdnUserSelectedValue.Value != "" ? hdnUserSelectedValue.Value : "", "", null, -1, -1);
            if (loglist != null)
            {
                path = AppDomain.CurrentDomain.BaseDirectory + "Images/logo-left-75px.png";

                if (System.IO.File.Exists(path))
                {
                    base64Data = Convert.ToBase64String(System.IO.File.ReadAllBytes(path));
                }
                if (isMainApplication)
                { }
                productivityreport.Append("<table style='font-family : calibriz;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");
                productivityreport.Append("    <tr>");
                if (format == "pdf")
                {
                    productivityreport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='data:image/png;base64," + base64Data + "' style= style='height:56px;width:56px'/></td>"); //0.7
                }
                else
                {
                    productivityreport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png'; style= style='height:56px;width:56px'/></td>"); //0.7
                } 
                productivityreport.Append("    </tr>");
                productivityreport.Append(" <tr>");
                productivityreport.Append("     <th>User</th>");
                productivityreport.Append("     <th>NewCandidate</th>");
                productivityreport.Append("     <th>Requisition</th>");
                productivityreport.Append("     <th>Sourced </th>");
                productivityreport.Append("     <th>Submissions</th>");
                productivityreport.Append("     <th>Interviews</th>");
                productivityreport.Append("     <th>Offers</th>");
                productivityreport.Append("     <th>OffersRejected</th>");
                productivityreport.Append("     <th>Joined</th>");
                productivityreport.Append("     <th>PendingJoiners</th>");
                productivityreport.Append(" </tr>");

                foreach (EmployeeProductivity log in loglist)
                {
                    if (log != null)
                    {
                        productivityreport.Append(" <tr>");
                        productivityreport.Append("     <td>" + log.EmployeeName + "&nbsp;</td>");
                        productivityreport.Append("     <td>" + log.NewCandidateCount + "&nbsp;</td>");
                        productivityreport.Append("     <td>" + log.JobCount + "&nbsp;</td>");
                        productivityreport.Append("     <td>" + log.CandidateSourcedCount + "&nbsp;</td>");
                        productivityreport.Append("     <td>" + log.SubmissionCount + "&nbsp;</td>");
                        productivityreport.Append("     <td>" + log.InterviewsCount + "&nbsp;</td>");
                        productivityreport.Append("     <td>" + log.OfferCount + "&nbsp;</td>");
                        productivityreport.Append("     <td>" + log.OfferRejectedCount + "&nbsp;</td>");
                        productivityreport.Append("     <td>" + log.JoinedCount + "&nbsp;</td>");
                        productivityreport.Append("     <td>" + log.PendingJoiners + "&nbsp;</td>");
                        productivityreport.Append(" </tr>");
                    }
                }

                productivityreport.Append("    <tr>");
                if (format == "pdf")
                {
                    productivityreport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='data:image/png;base64," + base64Data + "' style= style='height:56px;width:56px'/></td>"); //0.7
                }
                else
                {
                    productivityreport.Append("        <td align=left  width='300' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png'; style= style='height:56px;width:56px'/></td>"); //0.7
                } 
                productivityreport.Append("    </tr>");

                productivityreport.Append(" </table>");
            }
            return productivityreport.ToString();
        }



        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvProductivityReport.FindControl(hdnSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortOrder.Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }

        private void PlaceUpDownArrowForTeamList()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvteamReport.FindControl(hdnSortTeamColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortTeamOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch { }
        }




        protected void Load_TeamUser(string teamID)
        {
            if (teamID == "0" || teamID == "")
            {
                if (!_isTeamProductivityReport)
                {
                    MiscUtil.PopulateMemberListWithEmailByRole(ddlUser, ContextConstants.ROLE_EMPLOYEE, Facade);
                    ddlUser.Items[0].Text = "All";
                }
                else 
                {
                    MiscUtil.PopulateMemberListByTeamMemberId(ddlUser, ContextConstants.ROLE_EMPLOYEE,CurrentMember.Id, Facade);
                    ddlUser.Items[0].Text = "All";
                }
                
            }
            else
            {
                ddlUser.DataSource = Facade.GetAllEmployeeByTeamId(teamID);
                ddlUser.DataValueField = "Id";
                ddlUser.DataTextField = "Firstname";
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem("All", "0"));
            }
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            JobPosting j = new JobPosting();
            setObjectDataSource();
            
            lsvProductivityReport.DataBind();
            lsvteamReport.DataBind();
            Load_TeamUser(ddlTeam.SelectedValue);
            
            ControlHelper.SelectListByValue(ddlUser, hdnUserSelectedValue.Value);
        }
        protected void ddlTeam_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load_TeamUser(ddlTeam.SelectedValue);
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!isMainApplication) hdnAppType.Value = "landt";
            if (_isMyProductivityReport)
            {
                lblUser.Visible = false;
                ddlUser.Visible = false;
            }
            hdnCurrentUserId.Value = CurrentMember.Id.ToString();
            ddlTeam.Attributes.Add("onchange", "TeamOnChange('" + ddlTeam.ClientID + "','" + ddlUser.ClientID + "','" + hdnUserSelectedValue.ClientID + "')");
            ddlUser.Attributes.Add("onchange", "EmployeeOnChange('" + ddlUser.ClientID + "','" + hdnUserSelectedValue.ClientID + "')");
            
            if (!IsPostBack)
            {
                divTeamExportButtons.Visible = false;
                if (_isMyProductivityReport)
                {
                    ddlTeam.DataSource = Facade.EmployeeTeamBuilder_GetTeamsByMemberId(CurrentMember.Id);
                }
                else if (_isTeamProductivityReport)
                {

                    ddlTeam.DataSource = Facade.EmployeeTeamBuilder_GetTeamsByMemberId(CurrentMember.Id);
                    hdnIsTeamReport.Value = "true";
                    hdnUserSelectedValue.Value = CurrentMember.Id.ToString();
                }
                else
                {
                    ddlTeam.DataSource = Facade.EmployeeTeamBuilder_GetAllTeam();
                }
                ddlTeam.DataValueField = "Id";
                ddlTeam.DataTextField = "Title";
                ddlTeam.DataBind();
                if (ddlTeam.Items.Count > 0)
                    ddlTeam.Items.Insert(0, new ListItem("All", "0"));

                else
                    divTeam.Visible = false;
                Load_TeamUser("0");
                //   lsvProductivityReport.DataBind();
                // lsvteamReport.DataBind();

                dtReferralDate.setDateRange(DateTime.Now.AddDays(-6), DateTime.Now);
            }
            if (dtReferralDate.StartDate == DateTime.MinValue) hdnStartDate.Value = "0";
            else hdnStartDate.Value = dtReferralDate.StartDate.ToString("yyyyMMdd");
            if (dtReferralDate.EndDate == DateTime.MinValue) hdnEndDate.Value = "0";
            else hdnEndDate.Value = dtReferralDate.EndDate.ToString("yyyyMMdd");

            setObjectDataSource();
        }

        public void repor(string rep, string TypeofReport)
        {


            GenerateProductivityReport(rep, TypeofReport);

        }
        public void setObjectDataSource() 
        {
            if (_isTeamProductivityReport)
            {
                hdnUserSelectedValue.Value = ddlUser.SelectedValue == "" ? "0" : ddlUser.SelectedValue;
                odsProductivityTeamList.SelectParameters["IsTeamProductivityReport"].DefaultValue = "true";
                odsProductivityList.SelectParameters["IsTeamProductivityReport"].DefaultValue = "true";
                odsProductivityList.SelectParameters["TeamLeaderid"].DefaultValue = CurrentMember.Id.ToString();
                odsProductivityTeamList.SelectParameters["TeamLeaderid"].DefaultValue = CurrentMember.Id.ToString();

            }
            if (_isMyProductivityReport)
            {
                hdnUserSelectedValue.Value = CurrentMember.Id.ToString();
                //odsProductivityTeamList.SelectParameters["User"].DefaultValue = CurrentMember.Id.ToString();
                odsProductivityTeamList.SelectParameters["IsMyProcuvtivityReport"].DefaultValue = "true";
            }
        }


        protected void btnReset_Click(object sender, EventArgs e)
        {

            
            if (divTeam.Visible)
            {
                ddlTeam.SelectedIndex = 0;
                Load_TeamUser("0");
            }
            else Load_TeamUser("0");
            ddlUser.SelectedIndex = 0;

            
            if (_isTeamProductivityReport)
            {
                ddlTeam.SelectedIndex = 0;
                ddlUser.SelectedIndex = 0;

            }
            else
            {
                hdnUserSelectedValue.Value = "";
            }

            if (_isMyProductivityReport)
            {
                ddlTeam.SelectedIndex = -1;
                //ddlUser.SelectedIndex = 0;

            }
            else
            {
                hdnUserSelectedValue.Value = "";
            }
            setObjectDataSource();
            dtReferralDate.setDateRange(DateTime.Now.AddDays(-6), DateTime.Now);
            if (dtReferralDate.StartDate == DateTime.MinValue) hdnStartDate.Value = "0";
            else hdnStartDate.Value = dtReferralDate.StartDate.ToString("yyyyMMdd");
            if (dtReferralDate.EndDate == DateTime.MinValue) hdnEndDate.Value = "0";
            else hdnEndDate.Value = dtReferralDate.EndDate.ToString("yyyyMMdd");

            lsvProductivityReport.DataBind();
            lsvteamReport.DataBind();

        }

        #endregion

        #region ListViewEvent

        protected void lsvProductivityReport_ItemBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                EmployeeProductivity report = ((ListViewDataItem)e.Item).DataItem as EmployeeProductivity;
                if (report != null)
                {
                    Label lblUser = (Label)e.Item.FindControl("lblUser");
                    Label lblNewCandidates = (Label)e.Item.FindControl("lblNewCandidates");
                    Label lblRequisitionPublished = (Label)e.Item.FindControl("lblRequisitionPublished");
                    Label lblSourced = (Label)e.Item.FindControl("lblSourced");
                    Label lblSubmissions = (Label)e.Item.FindControl("lblSubmissions");
                    Label lblInterviews = (Label)e.Item.FindControl("lblInterviews");
                    Label lblOffers = (Label)e.Item.FindControl("lblOffers");
                    Label lblOfferrejected = (Label)e.Item.FindControl("lblOfferrejected");
                    Label lblJoined = (Label)e.Item.FindControl("lblJoined");
                    Label lbljoiners = (Label)e.Item.FindControl("lblPendingJoiners");

                    lblUser.Text = report.EmployeeName;
                    lblNewCandidates.Text = Convert.ToString(report.NewCandidateCount);
                    lblRequisitionPublished.Text = Convert.ToString(report.JobCount);
                    lblSourced.Text = Convert.ToString(report.CandidateSourcedCount);
                    lblSubmissions.Text = Convert.ToString(report.SubmissionCount);
                    lblInterviews.Text = Convert.ToString(report.InterviewsCount);
                    lblOffers.Text = Convert.ToString(report.OfferCount);
                    lblOfferrejected.Text = Convert.ToString(report.OfferRejectedCount);
                    lblJoined.Text = Convert.ToString(report.JoinedCount);
                    lbljoiners.Text = Convert.ToString(report.PendingJoiners);
                    if (isMainApplication)
                    {
                        HtmlTableCell tdPendingJoiners = (HtmlTableCell)e.Item.FindControl("tdPendingJoiners");
                        tdPendingJoiners.Visible = false;
                    }

                }
            }

        }

        protected void lsvteamReport_ItemBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                divTeamExportButtons.Visible = true;
                EmployeeProductivity report = ((ListViewDataItem)e.Item).DataItem as EmployeeProductivity;
                if (report != null)
                {
                    Label lblTeam = (Label)e.Item.FindControl("lblTeam");
                    Label lblNewCandidates = (Label)e.Item.FindControl("lblNewCandidates");
                    Label lblRequisitionPublished = (Label)e.Item.FindControl("lblRequisitionPublished");
                    Label lblSourced = (Label)e.Item.FindControl("lblSourced");
                    Label lblSubmissions = (Label)e.Item.FindControl("lblSubmissions");
                    Label lblInterviews = (Label)e.Item.FindControl("lblInterviews");
                    Label lblOffers = (Label)e.Item.FindControl("lblOffers");
                    Label lblOfferrejected = (Label)e.Item.FindControl("lblOfferrejected");
                    Label lblJoined = (Label)e.Item.FindControl("lblJoined");
                    Label lblJoinersTeam = (Label)e.Item.FindControl("lblPendingJoiners");

                    lblTeam.Text = report.EmployeeName;
                    lblNewCandidates.Text = Convert.ToString(report.NewCandidateCount);
                    lblRequisitionPublished.Text = Convert.ToString(report.JobCount);
                    lblSourced.Text = Convert.ToString(report.CandidateSourcedCount);
                    lblSubmissions.Text = Convert.ToString(report.SubmissionCount);
                    lblInterviews.Text = Convert.ToString(report.InterviewsCount);
                    lblOffers.Text = Convert.ToString(report.OfferCount);
                    lblOfferrejected.Text = Convert.ToString(report.OfferRejectedCount);
                    lblJoined.Text = Convert.ToString(report.JoinedCount);
                    lblJoinersTeam.Text = Convert.ToString(report.PendingJoiners);
                    if (isMainApplication)
                    {
                        HtmlTableCell tdPendingJoiners = (HtmlTableCell)e.Item.FindControl("tdPendingJoiners");
                        tdPendingJoiners.Visible = false;
                    }

                }

            }

        }

        protected void lsvProductivityReport_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvProductivityReport.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "ProductivityReportbyEmployeeRowPerPage";

            }

            PlaceUpDownArrow();
            if (IsPostBack)
            {
                if (PagerControl == null)
                {
                    lsvProductivityReport.Items.Clear();
                    lsvProductivityReport.DataSource = null;
                    lsvProductivityReport.DataBind();

                }

            }
            HtmlTableCell tdPager = (HtmlTableCell)lsvProductivityReport.FindControl("tdPager");


            if (isMainApplication)
            {

                if (tdPager != null)
                    tdPager.ColSpan = 9;

            }
            else
            {
                if (tdPager != null)
                    tdPager.ColSpan = 10;
            }

            if (isMainApplication)
            {
                HtmlTableCell thPendingJoiners = (HtmlTableCell)lsvProductivityReport.FindControl("thPendingJoiners");
                if (thPendingJoiners != null) thPendingJoiners.Visible = false;

            }


        }

        protected void lsvteamReport_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvteamReport.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "ProductivityReportbyTeamRowPerPage";

            }

            PlaceUpDownArrowForTeamList();
            if (IsPostBack)
            {
                if (PagerControl == null)
                {
                    lsvteamReport.Items.Clear();
                    lsvteamReport.DataSource = null;
                    lsvteamReport.DataBind();


                }

            }
            HtmlTableCell tdPager = (HtmlTableCell)lsvteamReport.FindControl("tdPager");
            if (tdPager != null)
            {
                if (isMainApplication)
                {

                    if (tdPager != null)
                        tdPager.ColSpan = 9;

                }
                else
                {
                    tdPager.ColSpan = 10;
                }
            }

            if (isMainApplication)
            {
                HtmlTableCell thPendingJoiners = (HtmlTableCell)lsvteamReport.FindControl("thPendingJoiners");
                if (thPendingJoiners != null) thPendingJoiners.Visible = false;
            }
        }


        protected void lsvProductivityReport_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Text == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Text == "ASC") hdnSortOrder.Text = "DESC";
                        else hdnSortOrder.Text = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Text = lnkbutton.ID;
                        hdnSortOrder.Text = "ASC";
                    }
                }

                if (hdnProListSortColumn.Text == string.Empty || hdnProListSortColumn.Text != e.CommandArgument.ToString())
                    hdnProListSortOrder.Text = "asc";
                else hdnProListSortOrder.Text = hdnProListSortOrder.Text.ToLower() == "asc" ? "desc" : "asc";
                hdnProListSortColumn.Text = e.CommandArgument.ToString();

            }
            catch { }
        }

        protected void lsvteamReport_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortTeamColumn.Text == lnkbutton.ID)
                    {
                        if (hdnSortTeamOrder.Text == "ASC") hdnSortTeamOrder.Text = "DESC";
                        else hdnSortTeamOrder.Text = "ASC";
                    }
                    else
                    {
                        hdnSortTeamColumn.Text = lnkbutton.ID;
                        hdnSortOrder.Text = "ASC";
                    }


                    if (hdnProListSortColumnByTeam.Text == string.Empty || hdnProListSortColumnByTeam.Text != e.CommandArgument.ToString())
                        hdnProListSortOrderByTeam.Text = "asc";
                    else hdnProListSortOrderByTeam.Text = hdnProListSortOrderByTeam.Text.ToLower() == "asc" ? "desc" : "asc";
                    hdnProListSortColumnByTeam.Text = e.CommandArgument.ToString();
                }

            }
            catch { }

        }

        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {
            repor("pdf", "ByEmployee");
            //uclConfirm.AddMessage("Note: Some columns may not appear in report if too many are selected.", ConfirmationWindow.enmMessageType.Attention, true, true, "");
        }
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            repor("excel", "ByEmployee");
        }
        protected void btnExportToWord_Click(object sender, EventArgs e)
        {
            repor("word", "ByEmployee");
        }

        protected void teamExportToExcel_Click(object sender, EventArgs e)
        {
            repor("excel", "ByTeam");
        }

        protected void teamExportToPDF_Click(object sender, EventArgs e)
        {
            repor("pdf", "ByTeam");
        }

        protected void teamExportToWord_Click(object sender, EventArgs e)
        {
            repor("word", "ByTeam");
        }

        #endregion 
      
}
}
