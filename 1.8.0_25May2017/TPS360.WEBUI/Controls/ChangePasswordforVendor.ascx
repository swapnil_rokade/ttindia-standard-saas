﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChangePasswordforVendor.ascx.cs"
    Inherits="TPS360.Web.UI.ChangePasswordforVendor" %>
    
<asp:HiddenField ID="hdnSelectedMemberID" runat="server" />
<div>
    <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
</div>
<asp:Panel ID="pnlPassword" runat="server" DefaultButton="btnSave">
    <div align="center">
        <br />
        <br />
        <br />
        <div class="TableRow">
            <div class="TableFormLeble" style="vertical-align: top;">
                <asp:Label ID="lblNewPass" CssClass="CommonLabel" runat="server" Text="New Password:"></asp:Label>
            </div>
            <div class="TableFormContent">
                <asp:TextBox ID="txtNewPass" runat="server" CssClass="CommonTextBox" TextMode="Password"></asp:TextBox>
                <font color="red"><b>*</b></font>
            </div>
            <div class="TableFormValidatorContent" style="margin-left: 42%">
                <asp:RequiredFieldValidator ID="rfvNewPass" runat="server" ControlToValidate="txtNewPass"
                    ValidationGroup="Password" Text="Enter Password." Display="Dynamic"></asp:RequiredFieldValidator>
                <%--<asp:RegularExpressionValidator ID="revNewPassword" runat="server" ControlToValidate="txtNewPass"
                    Display="Dynamic" ErrorMessage="Minimum password length is 6 characters." ValidationExpression=".{6}.*"
                    ValidationGroup="Password" />--%>
                    <asp:RegularExpressionValidator ID="revNewPassword" runat="server" ControlToValidate="txtNewPass"
                    Display="Dynamic" ErrorMessage="Password must be 8-10 characters long with at least one numeric,</br> one alphabet and one special character." EnableViewState="false"
                    ValidationGroup="Password" ValidationExpression="(?=^.{8,10}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&*()_+}{:;'?/>.<,])(?!.*\s).*$"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble" style="vertical-align: top;">
                <asp:Label ID="lblConfirm" CssClass="CommonLabel" runat="server" Text="Confirm Password:"></asp:Label>
            </div>
            <div class="TableFormContent">
                <asp:TextBox ID="txtConfirmPass" runat="server" CssClass="CommonTextBox" TextMode="Password"></asp:TextBox>
                <font color="red"><b>*</b></font>
            </div>
            <div class="TableFormValidatorContent" style="margin-left: 42%">
                <asp:RequiredFieldValidator ID="rfvConfirmPass" runat="server" ControlToValidate="txtConfirmPass"
                    ValidationGroup="Password" Text="Enter Confirm Password." Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revConfirmPass" runat="server" ControlToValidate="txtConfirmPass"
                    Display="Dynamic" ErrorMessage="Minimum password length is 6 characters." ValidationExpression=".{6}.*"
                    ValidationGroup="Password" />
                <asp:CompareValidator ID="cvValid" runat="server" ControlToValidate="txtConfirmPass"
                    ControlToCompare="txtNewPass" Type="String" Operator="Equal" Text="Password not match."
                    ValidationGroup="Password" Display="Dynamic"></asp:CompareValidator>
            </div>
            <br />
        </div>
        <div class="TableRow" style="text-align: center;">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="CommonButton" Width="44px"
                OnClick="btnSave_Click" ValidationGroup="Password" />
            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="CommonButton" OnClick="btnClear_Click" />
        </div>
    </div>
</asp:Panel>
