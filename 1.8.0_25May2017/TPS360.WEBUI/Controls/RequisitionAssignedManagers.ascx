﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionAssignedManagers.ascx.cs" Inherits="RequisitionAssignedManagers" %>

<asp:UpdatePanel ID="upManagers" runat ="server" >
<ContentTemplate >

<asp:HiddenField ID="hdnJobPostingId" runat ="server" />
 <div class="TableRow" style="text-align:left">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
    </div>
 <div id="divRecruiters" runat ="server" >
 <div class="TableRow" >
        <div class="TableFormLeble" style="width:20%">
            <asp:Label EnableViewState="false" ID="lblRecruiters" runat="server" Text="Select Recruiter"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:ListBox ID="lstRecruiters" runat="server" SelectionMode="multiple" TabIndex ="3" CssClass="CommonDropDownList" Width="160px" Height="100px">
            </asp:ListBox>
            <asp:Button ID="btnAddRecruiters" CssClass="CommonButton" runat="server" Text="Add" OnClick="btnAddRecruiters_Click" TabIndex ="4" ValidationGroup ="RecruiterValidation"/>
        </div>
    </div>
  
  </div>
 <div class="TableRow" style ="max-height : 400px; overflow :auto ;" >
             <div class="TabPanelHeader">
                    Assigned Team
                </div>
            <asp:ListView ID="lsvAssignedTeam" runat="server" DataKeyNames="Id" OnItemDataBound="lsvAssignedTeam_ItemDataBound" 
                   OnItemCommand="lsvAssignedTeam_ItemCommand" OnPreRender ="lsvAssignedTeam_PreRender">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th style="width: 20%; white-space: nowrap;">
                                Date Assigned
                            </th>
                            <th style="width: 40%; white-space: nowrap;">
                            <div style="width:40%; float:left;">
                                Name
                                </div>
                                <img style="float:right" src="../Images/uparrow-header.gif" align="right" ></img> 
                            </th>
                            <th style="width: 30%; white-space: nowrap;">
                                Email
                            </th>                            
                            <th style="text-align: center; white-space: nowrap; width: 10%;" runat ="server" id="thAction">
                                Action
                            </th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                        <tr>
                            <td>
                                No recruiters assigned.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:Label ID="lblAssignedDate" runat="server" />
                        </td>
                        <td>
                            <asp:Label Width="80%" ID="lblMemberName" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblMemberEmail" runat="server" />
                        </td>
                        <td style="text-align: center;" runat ="server" id="tdAction">
                            <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" TabIndex ="5" CommandName="DeleteItem">
                            </asp:ImageButton>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
            
    </div>
</ContentTemplate>
</asp:UpdatePanel>