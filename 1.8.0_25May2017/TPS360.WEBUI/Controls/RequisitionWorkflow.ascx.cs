﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:RequisitionWorkflow.aspx.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
 *  0.1                4/March/2016         pravin khot        added- IList<CustomRole> roleList1 = Facade.GetCustomRoleRequisition(2);
    0.2                15/May/2017          Sumit Sonawane     Issue Id 1309
-------------------------------------------------------------------------------------------------------------------------------------------       

>*/
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.UI;
using System.Collections;
using System.Text.RegularExpressions;
using System.Text;
using System.Data;

namespace TPS360.Web.UI
{
    public partial class cltRequisitionWorkflow : RequisitionBaseControl
    {
        #region Member Variables
        private bool Isdelete = false;
        static int NoOfOp = 0;
        #endregion

        #region Methods

        private int SelectedClientID()
        {
            if (txtClientName.Visible == false)
            {
                return Int32.Parse(ddlClientName.SelectedValue);
            }
            else
            {
                if (txtClientName.Text == hdnSelectedClientText.Value)
                    return Int32.Parse(hdnSelectedClient.Value != "" ? hdnSelectedClient.Value : "0");
                else return 0;
            }
        }

        public JobPosting BuildJobPosting(JobPosting jobPosting)
        {
            jobPosting.IsApprovalRequired = false;
            //jobPosting.ClientId = SelectedClientID();
            //jobPosting.ClientContactId = Convert.ToInt32(hdnSelectedContactID.Value == "" ? "0" : hdnSelectedContactID.Value); //Convert.ToInt32(ddlClientContact.SelectedValue);
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.STAFFING_FIRM)
            {
                jobPosting.ClientHourlyRate = MiscUtil.RemoveScript(txtClientHourlyRate.Text);
                jobPosting.ClientRatePayCycle = ddlSalary.SelectedValue.ToString();
                jobPosting.ClientHourlyRateCurrencyLookupId = Int32.Parse(ddlClientHourlyRateCurrency.SelectedValue);
            }
            jobPosting.InternalNote = MiscUtil.RemoveScript(txtAdditionalNote.Text);
            jobPosting.CustomerName = MiscUtil.RemoveScript(txtCustomerName.Text);
            jobPosting.CustomerName = MiscUtil.RemoveScript(txtCustomerName.Text);

            switch (ApplicationSource)
            {
                case ApplicationSource.LandTApplication:
                    if (rdoYes.Checked)
                    {
                        jobPosting.POAvailability = 1;

                    }
                    else if (rdoNO.Checked)
                    {
                        jobPosting.POAvailability = 2;
                    }
                    else if (rdoNA.Checked)
                    {
                        jobPosting.POAvailability = 3;
                    }
                    break;
                case ApplicationSource.MainApplication:
                    jobPosting.POAvailability = 0;
                    break;
                case ApplicationSource.SelectigenceApplication:
                    jobPosting.POAvailability = 0;
                    break;
                default:
                    jobPosting.POAvailability = 0;
                    break;
            }

            //if (!isMainApplication)
            //{
            //    if (rdoYes.Checked)
            //    {
            //        jobPosting.POAvailability = 1;

            //    }
            //    else if (rdoNO.Checked)
            //    {
            //        jobPosting.POAvailability = 2;
            //    }
            //    else if (rdoNA.Checked)
            //    {
            //        jobPosting.POAvailability = 3;
            //    }
            //}
            //else jobPosting.POAvailability = 0;
            return jobPosting;
        }

        public void SaveRecruiters(int JobPostingID, int ContactId)
        {
            IList<int> recr = getAssignedRecruitersFromList();
            string MemberIDs = "";
            foreach (int r in recr)
            {
                if (MemberIDs != "")
                    MemberIDs += ","; MemberIDs += r;
            }
            //if (ContactId != 0) 
            //{
            //    MemberIDs += ","; MemberIDs += ContactId;
            //}

            //*********Code added by pravin khot on 4/March/2016********************
            IList<CustomRole> roleList1 = Facade.GetCustomRoleRequisition(2);
            if (roleList1 != null)
            {
                foreach (CustomRole role in roleList1)
                {
                     MemberIDs += ","; MemberIDs += role.Id;
                }
            }
            //**************************END*******************************************

            //Facade.AddMultipleJobPostingHiringTeam(MemberIDs, JobPostingID, CurrentMember.Id, EmployeeType.Internal.ToString());
        }
        private void AddSelectedMemberToJobPostingHiringTeam(ListBox listBox, EmployeeType employeeType)
        {
            bool iserror = false;
            int countSelected = 0;
            int countAdded = 0;
            int memberId = 0;
            bool isCurrentmemberadded = false;
            IList<int> AddedRecruiters = getAssignedRecruitersFromList();
            IList<JobPostingHiringTeam> team = new List<JobPostingHiringTeam>();
            string message = string.Empty;
            {
                for (int i = 0; i <= listBox.Items.Count - 1; i++)
                {
                    Int32.TryParse(listBox.Items[i].Value, out memberId);
                    if (memberId > 0)
                    {
                        if (listBox.Items[i].Selected || AddedRecruiters.Contains(memberId))
                        {
                            if (listBox.Items[i].Selected) countSelected++;
                            JobPostingHiringTeam jobPostingHiringTeam = new JobPostingHiringTeam();
                            jobPostingHiringTeam.MemberId = memberId;

                            jobPostingHiringTeam.Name = listBox.Items[i].Text;
                            team.Add(jobPostingHiringTeam);
                            if (!AddedRecruiters.Contains(memberId)) countAdded++;
                            if (memberId == CurrentMember.Id)
                            {
                                isCurrentmemberadded = true;
                            }
                        }
                    }

                }
                if (CurrentJobPostingId == 0 || CurrentJobPosting.CreatorId == CurrentMember.Id)
                {
                    if (isCurrentmemberadded == false)
                    {
                        JobPostingHiringTeam jobPostingHiringTeam = new JobPostingHiringTeam();
                        jobPostingHiringTeam.MemberId = CurrentMember.Id;
                        jobPostingHiringTeam.Name = CurrentMember.FirstName + " " + CurrentMember.LastName + "  [" + CurrentMember.PrimaryEmail + "]";
                        team.Add(jobPostingHiringTeam);
                    }
                }
                lsvAssignedTeam.DataSource = team;
                lsvAssignedTeam.DataBind();
                if (countSelected > 0)
                {
                    if (countAdded > 0)
                    {
                        MiscUtil.EventLogForRequisition(EventLogForRequisition.RequisitionAssigned, CurrentJobPosting.Id, CurrentMember.Id, Facade);
                        int countNotAdded = countSelected - countAdded;
                        if (countNotAdded == 0)
                        {
                            message = "Successfully added all the selected members.";
                        }
                        else
                        {
                            message = "Successfully added " + countAdded + " of " + countSelected + " selected members. The remaining members are already in the list</b></font>";
                        }
                    }
                    else
                    {
                        message = "The selected member(s) are already in the list";
                        iserror = true;
                    }
                }
                else
                {
                    message = "Select at least one member to add.";
                    iserror = true;
                }
            }
            //else 
            //{
            //    message = "Please Select a member to add"; 
            //}

            MiscUtil.ShowMessage(lblMessage, message, iserror);
        }

        private void PopulateClientContactList()
        {
            //ddlClientContact.SelectedIndex = 0;
            //ArrayList contacts = Facade.GetAllCompanyContactsByCompanyId(ddlClientName.Visible ? Convert.ToInt32(ddlClientName.SelectedValue == "" ? "0" : ddlClientName.SelectedValue) : Convert.ToInt32(hdnSelectedClient.Value == "" ? "0" : hdnSelectedClient.Value));
            //CompanyContact con = new CompanyContact();
            //con.Id = 0;
            //con.FirstName = "Please Select";
            //contacts.Insert(0, con);
            //ddlClientContact.DataSource = contacts;
            //ddlClientContact.DataValueField = "Id";
            //ddlClientContact.DataTextField = "FirstName";

            //ddlClientContact.DataBind();
            //ddlClientContact = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientContact);
            //if (ddlClientContact.Items.Count == 1) ddlClientContact.Enabled = false;
            //else ddlClientContact.Enabled = true;


        }

        private void PopulateClientDropdowns()
        {
            int CompanyCount = Facade.Company_GetCompanyCount();
            if (CompanyCount <= 500)
            {
                int companyStatus = (int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting);

                ddlClientName.Visible = true;
                txtClientName.Visible = false;
                ddlClientName.Items.Clear();
                ddlClientName.DataSource = Facade.GetAllClientsByStatus(companyStatus);

                ddlClientName.DataTextField = "CompanyName";
                ddlClientName.DataValueField = "Id";
                ddlClientName.DataBind();
                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                {
                    ddlClientName = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientName);
                    ddlClientName.Items.Insert(0, new ListItem("Select Department", "0"));
                    lblClientName.Text = "Department Name";
                    lblClientContactName.Text = "Department Contact";

                }
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                {
                    ddlClientName = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientName);
                    ddlClientName.Items.Insert(0, new ListItem("Select Account", "0"));
                    lblClientName.Text = "Account Name";
                    lblClientContactName.Text = "Account Contact";
                }
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Vendor)
                {
                    ddlClientName = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientName);
                    ddlClientName.Items.Insert(0, new ListItem("Select Vendor", "0"));
                    lblClientName.Text = "Vendor Name";
                    lblClientContactName.Text = "Vendor Contact";

                }
            }
            else
            {
                ddlClientName.Visible = false;
                txtClientName.Visible = true;
            }
            PopulateClientContactList();

        }

        private void PopulateHiringTeamMembers(JobPosting jobPosting)
        {
            //lsvAssignedTeam.DataSource = Facade.GetAllJobPostingHiringTeamByJobPostingId(jobPosting.Id);
            //lsvAssignedTeam.DataBind();
        }


        public void PostbackCall()
        {
            if (ddlClientHourlyRateCurrency.SelectedItem.Text == "INR")
                lblBillrateCurrency.Text = "(Lacs)";
            PopulateClientContactList();
            ControlHelper.SelectListByValue(ddlClientContact, hdnSelectedContactID.Value);
        }
        private void PopulateFormData(JobPosting jobPosting)
        {
            //if (!txtClientName.Visible) ddlClientName.SelectedValue = jobPosting.ClientId.ToString();
            //  else
            //  {
            //      if (jobPosting.ClientId > 0)
            //      {
            //          Company company = Facade.GetCompanyById(jobPosting.ClientId);
            //          if (company != null)
            //          {
            //              txtClientName.Text = company.CompanyName;
            //              hdnSelectedClient.Value = company.Id.ToString ();
            //              hdnSelectedClientText.Value = company.CompanyName;
            //          }
            //      }

            //  }
            txtClientHourlyRate.Text = jobPosting.ClientHourlyRate;
            txtAdditionalNote.Text = MiscUtil.RemoveScript(jobPosting.InternalNote, string.Empty);
            txtCustomerName.Text = MiscUtil.RemoveScript(jobPosting.CustomerName, string.Empty);
            if (jobPosting.ClientHourlyRateCurrencyLookupId != 0)
            {
                ddlClientHourlyRateCurrency.SelectedValue = jobPosting.ClientHourlyRateCurrencyLookupId.ToString();
                ddlSalary.SelectedValue = jobPosting.ClientRatePayCycle.ToString();
            }
            if (ddlClientHourlyRateCurrency.SelectedItem.Text == "INR")
                lblBillrateCurrency.Text = "(Lacs)";
            //PopulateClientContactList();
            //ControlHelper.SelectListByValue(ddlClientContact, jobPosting.ClientContactId.ToString());
            //hdnSelectedContactID.Value = ddlClientContact.SelectedValue;

            if (jobPosting.POAvailability == 1)
            {
                rdoYes.Checked = true;
                rdoNO.Checked = false;
                rdoNA.Checked = false;

            }
            else if (jobPosting.POAvailability == 2)
            {
                rdoNO.Checked = true;
                rdoYes.Checked = false;
                rdoNA.Checked = false;
            }
            else if (jobPosting.POAvailability == 3)
            {
                rdoNA.Checked = true;
                rdoYes.Checked = false;
                rdoNO.Checked = false;
            }
            txtCustomerName.Text = MiscUtil.RemoveScript(jobPosting.CustomerName, string.Empty);
        }

        private void Prepareview(JobPosting jobPosting)
        {
            NoOfOp = 0;
            if (CurrentUserRole == "Vendor")
            {
                txtClientName.Visible = false;
                ddlClientName.Visible = false;
                lblVendorClientName.Visible = true;
                lblVendorClientName.Text = Facade.GetCompanyNameByVendorMemberId(CurrentMember.Id);
            }
            else PopulateClientDropdowns();
            //MiscUtil.PopulateMemberListByRole(lstRecruiters, ContextConstants.ROLE_EMPLOYEE, Facade);

            //MiscUtil.PopulateSkillGroup(DdlRecruiterGroup, Facade);
            MiscUtil.PopulateMemberListWithEmailByRole(DdlRecruiterGroup, ContextConstants.ROLE_EMPLOYEE, Facade);
            //MiscUtil.PopulateMemberListWithEmailByRole(uclEducationList.ListItem,ContextConstants.ROLE_EMPLOYEE, Facade);
            //uclEducationList.ListItem.Items.RemoveAt(0);
            //DdlRecruiterGroup.Items.RemoveAt(1);

            MiscUtil.PopulateMemberListWithEmailByRole(lstRecruiters, ContextConstants.ROLE_EMPLOYEE, Facade);
            ListItem removeitem = null;
            foreach (ListItem item in lstRecruiters.Items)
            {
                if (Convert.ToInt32(item.Value) == CurrentMember.Id)
                {
                    removeitem = item;
                }
            }
            if (removeitem != null) lstRecruiters.Items.Remove(removeitem);

            if (lstRecruiters.Items.Count > 0) lstRecruiters.Items.RemoveAt(0);


            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                //lblClientName.Text = "Department";
                //lblClientContactName.Text = "Department Contact";
            }
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() != UIConstants.STAFFING_FIRM)
            {
                lstRecruiters.Focus();
                divStaffingFirm.Visible = false;
            }

        }

        private void DisableValidationControls()
        {
            rfvCustomerName.Enabled = false;
            trcustomername.Visible = false;
            trpoavailability.Visible = false;
        }

        #endregion

        #region Events
     
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //BindGridview();
                //BindSecondGrid();
            }
            //if (isMainApplication)
            //{
            //    DisableValidationControls();
            //}

            if (ApplicationSource == ApplicationSource.MainApplication || ApplicationSource == ApplicationSource.SelectigenceApplication || ApplicationSource == ApplicationSource.GenisysApplication)
            {
                DisableValidationControls();
            }
            ddlClientName.Attributes.Add("onChange", "return Company_OnChange('" + ddlClientName.ClientID + "','" + ddlClientContact.ClientID + "','" + hdnSelectedContactID.ClientID + "')");
            ddlClientContact.Attributes.Add("OnChange", "return Contact_OnChange('" + ddlClientContact.ClientID + "','" + hdnSelectedContactID.ClientID + "')");
            ddlClientHourlyRateCurrency.Attributes.Add("onchange", "javascript:ShowYearlyCurrency('" + ddlClientHourlyRateCurrency.ClientID + "','" + lblBillrateCurrency.ClientID + "')");
            ddlClientContact.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlClientName.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            if (!Page.IsPostBack)
            {
                MiscUtil.PopulateCurrency(ddlClientHourlyRateCurrency, Facade);
                ddlClientHourlyRateCurrency.SelectedValue = SiteSetting[DefaultSiteSetting.Currency.ToString()].ToString();
                ddlSalary.SelectedValue = SiteSetting[DefaultSiteSetting.PaymentType.ToString()].ToString();

                JobPostingHiringTeam jobPostingHiringTeam = new JobPostingHiringTeam();
                jobPostingHiringTeam.MemberId = CurrentMember.Id;
                jobPostingHiringTeam.Name = CurrentMember.FirstName + " " + CurrentMember.LastName + "[" + CurrentMember.PrimaryEmail + "]";

                //IList<JobPostingHiringTeam> list = new List<JobPostingHiringTeam>();
                //list.Add(jobPostingHiringTeam);
                //lsvAssignedTeam.DataSource = list;
                //lsvAssignedTeam.DataBind();

                //IList<CustomRole> roleList2 = Facade.GetCustomRoleRequisition(2);
                //lsvAssignedTeam.DataSource = roleList2;
                //lsvAssignedTeam.DataBind();

                Prepareview(CurrentJobPosting);
                if (CurrentJobPostingId > 0)
                {
                    PopulateHiringTeamMembers(CurrentJobPosting);
                    PopulateFormData(CurrentJobPosting);
                }

                IList<JobPostingHiringTeam> List = new List<JobPostingHiringTeam>();
                if (CurrentJobPostingId > 0)
                {
                    PopulateRecruiterGroup(CurrentJobPosting);
                }
                else //Condition introduced by Prasanth on 25/Nov/2015
                {
                    //lsvRecruiterGroup.DataSource = List;
                    //lsvRecruiterGroup.DataBind();
                    IList<JobPostingHiringTeam> SGC_List = new List<JobPostingHiringTeam>();
                    JobPostingHiringTeam SGC = new JobPostingHiringTeam();
                    SGC.RecruiterGroupId = CurrentMember.Id;
                    SGC.NoOfOenings = 0;
                    SGC.IsPRIMARY  = true;
                    Member mm = Facade.GetMemberById(CurrentMember.Id);
                    SGC.RecruiterGroupName = mm.FirstName + " " + mm.LastName;
                    SGC.NoOfOening = "0";
                    SGC_List.Add(SGC);
                    lsvRecruiterGroup.DataSource = null;
                    lsvRecruiterGroup.DataSource = SGC_List;
                    lsvRecruiterGroup.DataBind();

                }
            }
            if (IsPostBack) PostbackCall();
        }

       

        // code modified by Sumit Sonawane on 22/May/2017 for Issue Id 1309
       

        protected void btnAddRecruiters_Click(object sender, EventArgs e)
        {
            AddSelectedMemberToJobPostingHiringTeam(lstRecruiters, EmployeeType.Internal);
            //PopulateHiringTeamMembers();
            PopulateClientContactList();

            ControlHelper.SelectListByValue(ddlClientContact, hdnSelectedContactID.Value);
            lstRecruiters.Focus();
        }

        // code added by Sumit Sonawane on 15/May/2017 for Issue Id 1309
        protected void imgbtnRefesh_Click(object sender, EventArgs e)
        {
            MiscUtil.PopulateMemberListWithEmailByRole(DdlRecruiterGroup, ContextConstants.ROLE_EMPLOYEE, Facade);
            IList<JobPostingHiringTeam> SGC_List = new List<JobPostingHiringTeam>();
            JobPostingHiringTeam SGC = new JobPostingHiringTeam();
            SGC.RecruiterGroupId = CurrentMember.Id;
            SGC.NoOfOenings = 0;
            SGC.IsPRIMARY = true;
            NoOfOp = 0;
            Member mm = Facade.GetMemberById(CurrentMember.Id);
            SGC.RecruiterGroupName = mm.FirstName + " " + mm.LastName;
            SGC.NoOfOening = "0";
            SGC_List.Add(SGC);
            lsvRecruiterGroup.DataSource = null;
            lsvRecruiterGroup.DataSource = SGC_List;
            lsvRecruiterGroup.DataBind();
        }

        protected void lsvAssignedTeam_PreRender(object sender, EventArgs e)
        {
            Isdelete = lsvAssignedTeam.Items.Count > 1;
            System.Web.UI.HtmlControls.HtmlTableCell thUnAssign = (System.Web.UI.HtmlControls.HtmlTableCell)lsvAssignedTeam.FindControl("thUnAssign");
            if (thUnAssign != null)
                thUnAssign.Visible = Isdelete;
            
            foreach (ListViewDataItem list in lsvAssignedTeam.Items)
            {
                
                    System.Web.UI.HtmlControls.HtmlTableCell tdUnAssign = (System.Web.UI.HtmlControls.HtmlTableCell)list.FindControl("tdUnAssign");
                    if (tdUnAssign != null)
                        tdUnAssign.Visible = Isdelete;
  
            }
            //lsvAssignedTeam.DataBind();
            IList<int> selectedemp = getAssignedRecruitersFromList();
            if (selectedemp.Count == 0)
            {
                lsvAssignedTeam.DataSource = null;
                lsvAssignedTeam.DataBind();
            }



            try
            {

                foreach (ListViewDataItem item in lsvAssignedTeam.Items)
                {
                    HiddenField hdnMemberID = (HiddenField)item.FindControl("hdnMemberID");

                    if (hdnMemberID.Value == "")
                    {
                        lsvAssignedTeam.Items.Remove(item);
                        lsvAssignedTeam.DataBind();
                    }
                }
            }
            catch(Exception ex)
            {
                PopulateHiringTeamMembers(CurrentJobPosting);
            }

        }
        protected void lsvAssignedTeam_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _isHiringManager = base.IsHiringManager;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPostingHiringTeam jobPostingHiringTeam = ((ListViewDataItem)e.Item).DataItem as JobPostingHiringTeam;

                if (jobPostingHiringTeam != null)
                {
                    HiddenField hdnMemberID = (HiddenField)e.Item.FindControl("hdnMemberID");
                    Label lblMemberName = (Label)e.Item.FindControl("lblMemberName");
                    Label lblMemberEmail = (Label)e.Item.FindControl("lblMemberEmail");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    if (jobPostingHiringTeam.MemberId > 0)
                    {
                        //  if (jobPostingHiringTeam.EmployeeType == EmployeeType.Internal.ToString())
                        {
                            hdnMemberID.Value = jobPostingHiringTeam.MemberId.ToString();
                            /*lblMemberName.Text = jobPostingHiringTeam .Name ;
                            //lblMemberEmail.Text = jobPostingHiringTeam .PrimaryEmail ;*/
                            char[] delimiter = { '[', ']' };
                            string[] name = jobPostingHiringTeam.Name.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

                            if (name.Length == 2)
                            {
                                lblMemberName.Text = name[0];
                                lblMemberEmail.Text = name[1];
                            }
                            else
                            {
                                lblMemberName.Text = jobPostingHiringTeam.Name;
                                lblMemberEmail.Text = jobPostingHiringTeam.PrimaryEmail;
                            }
                        }
                        if (jobPostingHiringTeam.MemberId == CurrentJobPosting.CreatorId)
                        {
                            btnDelete.Visible = false;

                        }
                        else
                        {
                            Isdelete = true;
                            btnDelete.Visible = true;
                        }
                        if (CurrentJobPostingId == 0)
                        {
                            if (jobPostingHiringTeam.MemberId == CurrentMember.Id) btnDelete.Visible = false;

                        }
                    }
                    // btnDelete.OnClientClick = "return ConfirmDelete('Hiring Team Member')";
                    // btnDelete.CommandArgument = StringHelper.Convert(jobPostingHiringTeam.Id);
                }
            }
        }

        public IList<int> getAssignedRecruitersFromList()
        {
            IList<int> ReqIds = new List<int>();
            //ReqIds.Add(CurrentMember.Id);

          
            foreach (ListViewDataItem item in lsvAssignedTeam.Items)
            {
                HiddenField hdnMemberID = (HiddenField)item.FindControl("hdnMemberID");
                if (hdnMemberID.Value != "")
                {
                    if (!ReqIds.Contains(Convert.ToInt32(hdnMemberID.Value)))
                        ReqIds.Add(Convert.ToInt32(hdnMemberID.Value));
                }

            }
           
            return ReqIds;

        }
        public IList<JobPostingHiringTeam> getHiringTeam()
        {
            IList<JobPostingHiringTeam> team = new List<JobPostingHiringTeam>();
            bool iscurrentmemberexists = false;
            foreach (ListViewDataItem item in lsvAssignedTeam.Items)
            {
                HiddenField hdnMemberID = (HiddenField)item.FindControl("hdnMemberID");
                Label lblMemberName = (Label)item.FindControl("lblMemberName");
                Label lblMemberEmail = (Label)item.FindControl("lblMemberEmail");

                JobPostingHiringTeam te = new JobPostingHiringTeam();
                if (hdnMemberID.Value != "")
                {
                    te.MemberId = Convert.ToInt32(hdnMemberID.Value);
                    if (te.MemberId == CurrentMember.Id) iscurrentmemberexists = true;
                    te.Name = lblMemberName.Text;
                    te.PrimaryEmail = lblMemberEmail.Text;
                    team.Add(te);
                }
                /*else {
                    lsvAssignedTeam.Items.Remove(item);
                    lsvAssignedTeam.DataBind();
                }*/

            }

            if (!iscurrentmemberexists)
            {
                JobPostingHiringTeam te = new JobPostingHiringTeam();
                te.MemberId = CurrentMember.Id;
                te.Name = CurrentMember.FirstName + " " + CurrentMember.LastName;
                te.PrimaryEmail = CurrentMember.PrimaryEmail;
                team.Add(te);
            }
            if (team != null) 
            {
                lsvAssignedTeam.DataSource = team;
                lsvAssignedTeam.DataBind();

            }
            
            return team;
        }

        #endregion

        //************************************************************
        protected void btnAddGrid_Click(object sender, EventArgs e)
        {
            ValidateAddtoGrid();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            
        }
     

        private void ValidateAddtoGrid() // code modified by Sumit Sonawane on 15/May/2017 for Issue Id 1309
        {
            string message = string.Empty;
            String OpeningsVal = Session["txtNoOfOpeningsVal"].ToString();
            if (OpeningsVal == "0" || OpeningsVal == "")
            {
                message = "Please Select No.Of Openings before assigning values to recruiters.";
                MiscUtil.ShowMessage(lblMessage, message, true);
            }
            else
            {
              
                if (DdlRecruiterGroup.SelectedIndex == 0)
                {
                    message = "Please Select Assign Recruiters";

                }
                else if (txtNoofpositiontoassign.Text == "")
                {
                    message = "Please Enter No. of position to assign";
                }

                int cnt = 0;

                foreach (ListViewDataItem item in lsvRecruiterGroup.Items)
                {
                    HiddenField hdnRecruiterGroup = (HiddenField)item.FindControl("hdnRecruiterGroup");
                    HiddenField hdnNoPosition = (HiddenField)item.FindControl("hdnNoPosition");
                    HiddenField hdnPrimaryRecruiter = (HiddenField)item.FindControl("hdnPrimaryRecruiter");

                    //if (DdlRecruiterGroup.SelectedValue == hdnRecruiterGroup.Value && txtNoofpositiontoassign.Text == hdnNoPosition.Value)
                    if (DdlRecruiterGroup.SelectedValue == hdnRecruiterGroup.Value)
                    {
                        cnt = cnt + 1;
                    }
                }

                if (cnt != 0)
                {
                    message = "Assign Recruiters Set allready Seleted.";
                    txtNoofpositiontoassign.Text = "";
                }
                if (message != string.Empty)
                {
                    MiscUtil.ShowMessage(lblMessage, message, true);

                }
                else
                {
                    AddTolsvRecruiterGroup();
                    txtNoofpositiontoassign.Text = "";
                }
            }
        }
      

        public void AddTolsvRecruiterGroup() // code modified by Sumit Sonawane on 15/May/2017 for Issue Id 1309
        {
            int TotNoOfOp = int.Parse(Session["txtNoOfOpeningsVal"].ToString());
            if (int.Parse(txtNoofpositiontoassign.Text) > TotNoOfOp)
            {
                MiscUtil.ShowMessage(lblMessage, "Assigned No.Of Openings should not be greater than Total No.Of Openings", true);
            }

            else
            {
                IList<JobPostingHiringTeam> SGC_List = new List<JobPostingHiringTeam>();
                JobPostingHiringTeam SGC = new JobPostingHiringTeam();
                NoOfOp = NoOfOp + int.Parse(txtNoofpositiontoassign.Text);
                if (NoOfOp > TotNoOfOp)
                {
                    NoOfOp = NoOfOp - int.Parse(txtNoofpositiontoassign.Text);
                    MiscUtil.ShowMessage(lblMessage, "Assigned No.Of Openings should not be greater than Total No.Of Openings", true);
                    //NoOfOp = 0;
                }

                else if (txtNoofpositiontoassign.Text == "0")
                {
                    MiscUtil.ShowMessage(lblMessage, "Assigned No.Of Openings should not be 0 or less", true);                   
                }


                else
                {
                    foreach (ListViewDataItem item in lsvRecruiterGroup.Items)
                    {
                        IList<int> RecruiterGroupIds = new List<int>();
                        JobPostingHiringTeam lsvSGC = new JobPostingHiringTeam();
                        Label lblRecruiterGroup = (Label)item.FindControl("lblRecruiterGroup");
                        Label lblNoPosition = (Label)item.FindControl("lblNoPosition");
                        RadioButton RdbPrimaryRecruiter = (RadioButton)item.FindControl("RdbPrimaryRecruiter");

                        HiddenField hdnRecruiterGroup = (HiddenField)item.FindControl("hdnRecruiterGroup");
                        HiddenField hdnNoPosition = (HiddenField)item.FindControl("hdnNoPosition");

                        if (hdnRecruiterGroup.Value != "")
                        {
                            if (!RecruiterGroupIds.Contains(Convert.ToInt32(hdnRecruiterGroup.Value)))
                                lsvSGC.RecruiterGroupId = Int32.Parse(hdnRecruiterGroup.Value);
                            lsvSGC.NoOfOenings = Int32.Parse(hdnNoPosition.Value);

                            lsvSGC.RecruiterGroupName = lblRecruiterGroup.Text;
                            if (RdbPrimaryRecruiter.Checked == true)
                            {
                                lsvSGC.IsPRIMARY = true;
                            }
                            else
                            {
                                lsvSGC.IsPRIMARY = false;
                            }
                            lsvSGC.NoOfOening = lblNoPosition.Text;

                            SGC_List.Add(lsvSGC);
                        }

                    }
                    SGC.RecruiterGroupId = Int32.Parse(DdlRecruiterGroup.SelectedValue);
                    SGC.NoOfOenings = Int32.Parse(txtNoofpositiontoassign.Text);

                    SGC.RecruiterGroupName = DdlRecruiterGroup.SelectedItem.ToString();
                    DdlRecruiterGroup.Items.RemoveAt(DdlRecruiterGroup.SelectedIndex);// code added by Sumit Sonawane on 15/May/2017 for Issue Id 1309
                    SGC.NoOfOening = txtNoofpositiontoassign.Text;

                    SGC_List.Add(SGC);

                    lsvRecruiterGroup.DataSource = null;
                    lsvRecruiterGroup.DataSource = SGC_List;
                    lsvRecruiterGroup.DataBind();
                }
            }
        }


        //public JobPosting BuildJobPosting(JobPosting jobPosting)
        //{
        //    string skilllistdy = hdnSecSkills.Value.Replace('~', '"');
        //    string mn = Facade.Skill_GetandAddSkillIds("<SkillList>" + skilllistdy.Replace("&", "&amp;") + "</SkillList>", base.CurrentMember.Id);

        //    if (mn.Trim().EndsWith("!")) mn = mn.Substring(0, mn.Length - 1);
        //    jobPosting.JobSecSkillLookUpId = mn;

        //    return jobPosting;
        //}

        public void DeleteJobPostingRecruitersGroup(int JobPostingID)
        {
            Facade.DeleteJobPostingHiringTeamByJobPostingId(JobPostingID);
        }

        public void SaveJobPostingRecruitersGroup(int JobPostingID)
        {
            bool isprimary = false;
            foreach (ListViewDataItem item in lsvRecruiterGroup.Items)
            {
                HiddenField hdnRecruiterGroupID = (HiddenField)item.FindControl("hdnRecruiterGroup");
                HiddenField hdnNoPositionID = (HiddenField)item.FindControl("hdnNoPosition");
                HiddenField hdnPrimaryRecruiter = (HiddenField)item.FindControl("hdnPrimaryRecruiter");

                //int companyAssignedManagerid = Request.Form["rdoPrimaryManager"] != null ? Int32.Parse(Request.Form["rdoPrimaryManager"]) : 0;
                //string mberEmail = Request.Form["rdoMemberEmailId"] != null ? Request.Form["rdoMemberEmailId"].ToString() : string.Empty;
                RadioButton RdbPrimaryRecruiter = (RadioButton)item.FindControl("RdbPrimaryRecruiter");
                isprimary = false;
                if (RdbPrimaryRecruiter.Checked == true)
                {
                    isprimary = true;
                }
                //if (hdnPrimaryRecruiter.Value != "")
                //{
                    if (hdnRecruiterGroupID.Value != "" && hdnNoPositionID.Value != "")
                    {
                        Facade.AddMultipleJobPostingHiringTeamWithOpening(Convert.ToInt32(hdnRecruiterGroupID.Value), Convert.ToInt32(hdnNoPositionID.Value), JobPostingID, CurrentMember.Id, EmployeeType.Internal.ToString(), isprimary);
                    }
                //}
            }
            //*********Code added by pravin khot on 4/March/2017********************
            IList<CustomRole> roleList1 = Facade.GetCustomRoleRequisition(2);
            if (roleList1 != null)
            {
                foreach (CustomRole role in roleList1)
                {
                    //MemberIDs += ","; MemberIDs += role.Id;
                    Facade.AddMultipleJobPostingHiringTeamWithOpening(role.Id, 0, JobPostingID, CurrentMember.Id, EmployeeType.Internal.ToString(), false);
                }
            }
            //**************************END*******************************************
        }
       
        protected void lsvRecruiterGroup_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
   
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
              JobPostingHiringTeam  SGC = ((ListViewDataItem)e.Item).DataItem as JobPostingHiringTeam;
                if (SGC != null)
                {
                 
                    HiddenField hdnRecruiterGroupID = (HiddenField)e.Item.FindControl("hdnRecruiterGroup");

                    Label lblRecruiterGroup = (Label)e.Item.FindControl("lblRecruiterGroup");
                    Label lblNoPosition = (Label)e.Item.FindControl("lblNoPosition");
                    RadioButton RdbPrimaryRecruiter = (RadioButton)e.Item.FindControl("RdbPrimaryRecruiter");
                    //Label RdbPrimaryRecruiter = (Label)e.Item.FindControl("RdbPrimaryRecruiter");

                    HiddenField hdnRecruiterGroup = (HiddenField)e.Item.FindControl("hdnRecruiterGroup");
                    HiddenField hdnNoPosition = (HiddenField)e.Item.FindControl("hdnNoPosition");
                    HiddenField hdnPrimaryRecruiter = (HiddenField)e.Item.FindControl("hdnPrimaryRecruiter");

                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
                    btnDelete.CommandArgument = StringHelper.Convert(SGC.RecruiterGroupName);

                    if (SGC.RecruiterGroupId > 0)
                    {
                        hdnRecruiterGroupID.Value = SGC.RecruiterGroupId.ToString();
                        hdnNoPosition.Value = SGC.NoOfOenings.ToString();

                        lblNoPosition.Text = SGC.NoOfOenings.ToString();
                        lblRecruiterGroup.Text = SGC.RecruiterGroupName;

                        //NoOfOp = NoOfOp + int.Parse(SGC.NoOfOenings.ToString());
                       
                        if (SGC.IsPRIMARY == true)
                        {
                            RdbPrimaryRecruiter.Checked = true;
                        }
                        else
                        {
                            RdbPrimaryRecruiter.Checked = false;
                        }
                        if (RdbPrimaryRecruiter.Checked == true)
                        {
                            hdnPrimaryRecruiter.Value = SGC.RecruiterGroupId.ToString();
                        }
                        else
                        {
                            hdnPrimaryRecruiter.Value = "";
                        }
                        if (SGC.RecruiterGroupId == CurrentMember.Id)
                        {
                            Isdelete = false;
                            btnDelete.Visible = false;
                        }
                        else
                        {
                            Isdelete = true;
                            btnDelete.Visible = true;
                        }
                    }              
                }
            }
        }
        protected void lsvRecruiterGroup_PreRender(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlTableCell thUnAssign = (System.Web.UI.HtmlControls.HtmlTableCell)lsvRecruiterGroup.FindControl("thUnAssign");
            if (thUnAssign != null)
                thUnAssign.Visible = true;
        }

        protected void lsvRecruiterGroup_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            string id;

            //int.TryParse(e.CommandArgument.ToString(), out id);
            id = e.CommandArgument.ToString();

            if (id != "")
            {
                if (string.Equals(e.CommandName, "DeleteItem"))                   
                {
                    DdlRecruiterGroup.Items.Add(e.CommandArgument.ToString());
                }
            }
        }

        private void PopulateRecruiterGroup(JobPosting jobPosting)
        {
            lsvRecruiterGroup.DataSource = Facade.GetAllJobPostingRecruiterGroupByJobPostingId(jobPosting.Id);
            lsvRecruiterGroup.DataBind();
        }


        //************************************************************
    }
}
