﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:RequisitionListBUContactAdd.ascx
    Description:
    Created By: pravin khot
    Created On: 02/May/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
     
-------------------------------------------------------------------------------------------------------------------------------------------     

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionListBUContactAdd.ascx.cs" Inherits="TPS360.Web.UI.RequisitionListBUContactAdd" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ModalTemplate.ascx" TagName="Template" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/RequisitionAssignedManagers.ascx" TagName="Managers"
    TagPrefix="ucl" %>
<%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<%@ Register Src ="~/Controls/MultipleItemPicker.ascx" TagName ="MultipleSelection" TagPrefix="ucl" %>
<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
<script src="../js/CompanyRequisition.js"></script>
<style>
    .dropdown-menu:after
    {
        border-bottom: 7px solid  #FFFFFF;
        border-left: 7px solid transparent;
        border-right: 7px solid transparent;
        content: "";
        display: inline-block;
        left: 80%;
        position: absolute;
        top: -6px;
    }
    .dropdown-menu::before
    {
        position: absolute;
        top: -7px;
        left: 80%;
        display: inline-block;
        border-right: 7px solid transparent;
        border-bottom: 7px solid #CCC;
        border-left: 7px solid transparent;
        border-bottom-color: rgba(0, 0, 0, 0.2);
        content: '';
    }
    .pnlHide
    {
    }
    .pnlHide:first-child
    {
        overflow: hidden;
    }
    
</style>

<script src="../js/CompanyRequisition.js"></script>

<script type="text/javascript">
document.body.onclick = function(e)
  {
  $('#ctl00_cphHomeMaster_uclRequisition_pnlSearchBoxContent').css({'overflow-y':'inherit','overflow-x':'inherit'});
  $('#ctl00_cphHomeMaster_uclRequisition_pnlSearchBoxContent div:first').css({'overflow-y':'inherit','overflow-x':'inherit'});
  }
    //To Keep the scroll bar in the Exact Place
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
    function PageLoadedReq(sender, args) {
        try {
            var chkBox = document.getElementById('chkAllItem');          
            if (chkBox != null) chkBox.disabled = false;
            CheckUnCheckGridViewHeaderCheckbox('tlbTemplate', 'chkAllItem', 0, 1);
        }
        catch (e) {
        }
        
        try {
        var hdnScroll = document.getElementById('ctl00_cphHomeMaster_uclRequisition_hdnScrollPos');
        var bigDiv = document.getElementById('bigDiv');
        bigDiv.scrollLeft = hdnScroll.value;
        var background = $find("mpeModal")._backgroundElement;
        background.onclick = function()
            {
                CloseModal(); 
            }
        }
        catch (e) {
        }
    }

    function onShownManagers() {
        var background = $find("MPEManagers")._backgroundElement;
        background.onclick = function() { $find("MPEManagers").hide(); }
    }
    function SetScrollPosition() {
        var hdnScroll = document.getElementById('ctl00_cphHomeMaster_uclRequisition_hdnScrollPos');
        var bigDiv = document.getElementById('bigDiv');
        hdnScroll.value = bigDiv.scrollLeft;
    } 
    
    function RefreshList()
    { 
        var hdnDoPostPack=document .getElementById ('<%=hdnDoPostPack.ClientID %>');
        hdnDoPostPack .value="1";
        __doPostBack('ctl00_cphHomeMaster_uclRequisition_upcandidateList',''); return false ;
    }
    $('#ctl00_cphHomeMaster_uclRequisition_lsvJobPosting_lnkNoOfOpening').mouseover(function() {
        console.log('asdsaweqw');
    });    
     function ValidateRecruiterList(source, args) {      
        validator = document.getElementById(source.id);
        var hdnField = document.getElementById('ctl00_cphHomeMaster_uclRequisition_hdnSelectedIDS')
        hdnSelectedHotList = document.getElementById("ctl00_cphHomeMaster_uclRequisition_hdnSelectedHotList");
        ddlRecruiterlist = document.getElementById("ctl00_cphHomeMaster_uclRequisition_ddlRecruiterlist");
        if (hdnSelectedHotList.value != '' || ddlRecruiterlist.options[ddlRecruiterlist.selectedIndex].value != "0") {           
           if (hdnField.value != '') args.IsValid = true;
            else {
                validator.innerHTML = "<div>Please select requisition.<div>";
                ShowDiv('divRecruiterListdiv'); n = 0;
                args.IsValid = true ;
            }
        }
        else {
            validator.innerHTML = "Please select recruiter name.";
            ShowDiv('divRecruiterListdiv'); n = 0;
            args.IsValid = false;
        }
    }
    
    function monitorClick(e) {
        try {           
            var evt = (e) ? e : event;
            var theElem = (evt.srcElement) ? evt.srcElement : evt.target;
            while (theElem != null) {
                if ($('#' + theElem.id).parents('.btn-group') == null || theElem.id == '' || theElem.id.indexOf('updPanelCandidateList') >= 0) {
                     n = 10;                  
                    var divRecruiterListdiv = document.getElementById('divRecruiterListdiv');
                    if (divRecruiterListdiv != null) {
                        $(divRecruiterListdiv).closest('.btn-group').removeClass('open');
                        divRecruiterListdiv.style.display = "none";
                    }
                    return true;
                }
                else {
                    n = 10;
                    return true;
                }

                theElem = theElem.offsetParent;
            }
        }
        catch (e) {
        } n = 10;
        return true;
    }
    document.onclick = monitorClick;
    function ShowDiv(divId) {
        n = 10;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function DropdownClicked(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function DropdownChange(divId) {     
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function ShowReqDiv(divId) {
        $('.btn-group').removeClass('open');       
        var b = document.getElementById("divRecruiterListdiv");       
        if (b != null) b.style.display = "none";

        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) {
            $(reqDiv).closest('.btn-group').addClass('open');
            reqDiv.style.display = "";
            reqDiv.parentElement.className = "btn-group open";
        } return false;
    }
    function CLCheckUnCheckAllRowLevelCheckboxes(cntrlHeaderCheckbox, posCheckbox) {
        var cntrlGridView = document.getElementById("tlbTemplate");
        var chkBoxAll = document.getElementById(cntrlHeaderCheckbox);
        var hndID = "";
        var chkBox = "";
        var checked = 0;
        var rowNum = 0;
        if (cntrlHeaderCheckbox != null && cntrlHeaderCheckbox.checked != null) {
            var rowLength = 0;
            rowLength = cntrlGridView.rows.length;
          
            for (var i = 1; i < rowLength; i++) {
                var myrow = cntrlGridView.rows[i];
                var mycel = myrow.getElementsByTagName("td")[posCheckbox];
                if (mycel.childNodes[0].value == "on") {
                    chkBox = mycel.childNodes[0];
                }
                else {
                    chkBox = mycel.childNodes[1];
                }
                try {
                    if (mycel.childNodes[2].value == undefined) {
                        hndID = mycel.childNodes[3];
                    }
                    else {
                        hndID = mycel.childNodes[2];
                    }
                }
                catch (e) {
                }
                if (chkBox != null) {
                    
                    if (chkBox.checked == null) {
                        chkBox = chkBox.nextSibling;
                    }
                    if (chkBox != null) {
                        if (chkBox.checked != null) {
                            chkBox.checked = cntrlHeaderCheckbox.checked;
                            rowNum++;
                        }
                    }
                }
                if (chkBox != null) {
                   
                    if (chkBox.checked != null && chkBox.checked == true) {
                        if (chkBox.id.indexOf('chkItemCandidate') > -1) {
                            if (hndID != null) ADDID(hndID.value, 'ctl00_cphHomeMaster_uclRequisition_hdnSelectedIDS');
                            checked++;
                        }
                    }
                    else {
                        if (chkBox.id.indexOf('chkItemCandidate') > -1) {
                            if (hndID != null) RemoveID(hndID.value, 'ctl00_cphHomeMaster_uclRequisition_hdnSelectedIDS');

                        }
                    }
                }
            }
        }

        if (checked == rowNum) {
           
            if (chkBoxAll != null) {
                chkBoxAll.checked = true;
            }
        }
        else {
           
            if (chkBoxAll != null) {
                chkBoxAll.checked = false;
            }
        }
    }

    function AssignValue(optionvalue) {
        
        var selectedvalue = document.getElementById('<%=txtSelectValue.ClientID %>');
        selectedvalue.value = optionvalue
    }

    function ReAssign() {
       
        var selectedvalue = document.getElementById('<%=txtSelectValue.ClientID %>');
        selectedvalue.value = "NoSelect";
    }
    function checkEnter(e) {
       
        var kC = window.event ? event.keyCode :
            e && e.keyCode ? e.keyCode :
            e && e.which ? e.which : null;
        if (kC == 13) {
            var selectedvalue = document.getElementById('<%=txtSelectValue.ClientID %>');
            if (selectedvalue.value == "Hotlist") {
                var btnclick = document.getElementById('ctl00_cphHomeMaster_uclRequisition_btnAddToRecruiterList');
                btnclick.focus();
            }
        }
    }              
</script>
<asp:HiddenField ID="hdnScrollPos" runat="server" />
 <div style="display: none">
        <asp:TextBox ID="txtSelectValue" Text="NoSelect" runat="server" />
    </div>
  <asp:HiddenField ID="hdnSelectedHotList" runat="server" />
 <asp:HiddenField ID="hdnSelectedHotListText" runat="server" />
<asp:ObjectDataSource ID="odsRequisitionList" runat="server" SelectMethod="GetPagedRequisitionList"
    OnSelecting="odsRequisitionList_Selecting" TypeName="TPS360.Web.UI.JobPostingDataSource"
    SelectCountMethod="GetRequisitionListCount" EnablePaging="True" SortParameterName="sortExpression">
    <SelectParameters>
        <asp:Parameter Name="IsTemplate" DefaultValue="False" Type="Boolean" Direction="Input" />
        <asp:Parameter Name="memberId" />
        <asp:Parameter Name="IsCompanyContact" Type="Boolean" DefaultValue="False" />
        <asp:Parameter Name="CompanyContactId" Type="Int32" DefaultValue="0" />
        <asp:ControlParameter ControlID="txtJobTitle" Name="JobTitle" PropertyName="Text"
            Type="String" />
        <asp:ControlParameter ControlID="txtReqCode" Name="ReqCode" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="ddlJobStatus" Name="jobStatus" PropertyName="SelectedItems"
            Type="String" />
        <asp:ControlParameter ControlID="dtPstedDate" Name="JobPostingFromDate" PropertyName="StartDate"
            Type="String" />
        <asp:ControlParameter ControlID="dtPstedDate" Name="JobPostingToDate" PropertyName="EndDate"
            Type="String" />
        <asp:ControlParameter ControlID="ddlReqCreator" Name="ReqCreator" PropertyName="SelectedValue"
            Type="String" />
        <asp:ControlParameter ControlID="ddlEmployee" Name="employee" PropertyName="SelectedValue"
            Type="String" />
        <asp:ControlParameter ControlID="TxtCity" Name="City" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="uclCountryState" Name="StateID" PropertyName="SelectedStateId"
            Type="String" />
        <asp:ControlParameter ControlID="uclCountryState" Name="CountryID" PropertyName="SelectedCountryId"
            Type="String" />
        <asp:ControlParameter ControlID="ddlEndClient" Name="endClients" PropertyName="SelectedItems"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

 <asp:UpdatePanel ID="updPanelCandidateList" runat="server">  <%--//updPanelCandidateList added by pravin khot on 26/May/2016--%>
  <ContentTemplate>
  <asp:HiddenField ID="hdnSelectedIDS" runat="server"/>
  
<asp:Panel ID="pnlSearchBoxBody" runat="server" CssClass="well well-small nomargin"
    DefaultButton="btnSearch">
    <asp:Panel ID="pnlSearchRegion" runat="server">
        <asp:CollapsiblePanelExtender ID="cpeExperience" runat="server" TargetControlID="pnlSearchBoxContent"
            ExpandControlID="pnlSearchBoxHeader" CollapseControlID="pnlSearchBoxHeader" CollapsedImage="~/Images/expand-plus.png"
            ExpandedImage="~/Images/collapse-minus.png" Collapsed="true" ImageControlID="imgSearchBoxToggle"
            SuppressPostBack="True">
        </asp:CollapsiblePanelExtender>
        <asp:Panel ID="pnlSearchBoxHeader" runat="server">
            <div class="SearchBoxContainer">
                <div class="SearchTitleContainer">
                    <div class="ArrowContainer">
                        <asp:ImageButton ID="imgSearchBoxToggle" runat="server" ImageUrl="~/Images/expand-plus.png"
                            AlternateText="collapse" />
                    </div>
                    <div class="TitleContainer">
                        Filter Options
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlSearchBoxContent" runat="server"  CssClass ="filter-section" Style="" Height="0">
            <div class="TableRow spacer">
                <div class="FormLeftColumn" style="width: 50%;">
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblJobTitle" runat="server" Text="Job Title"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtJobTitle" runat="server" CssClass="SearchTextbox"></asp:TextBox>
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblReqCode" runat="server" Text="Req Code"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox ID="txtReqCode" runat="server" CssClass="SearchTextbox"></asp:TextBox>
                        </div>
                    </div>
                    <div class="TableRow" style="white-space: nowrap">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="Label1" runat="server" Text="Date Posted"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <ucl:DateRangePicker ID="dtPstedDate" runat="server" />
                        </div>
                    </div>
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblJobStatusHeader" runat="server" Text="Requisition Status"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                        <ucl:MultipleSelection ID="ddlJobStatus" runat ="server" />
                        
                       <%--     <asp:DropDownList EnableViewState="true" ID="ddlJobStatus" runat="server" CssClass="CommonDropDownList"
                                AutoPostBack="false">
                            </asp:DropDownList>--%>
                        </div>
                    </div>                                       
                   
                </div>
                <div class="FormRightColumn" style="width: 49%">
                    <div class="TableRow" style="white-space: nowrap;">
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblCreator" runat="server" Text="Req Creator"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlReqCreator" runat="server" CssClass="CommonDropDownList">
                            </asp:DropDownList>
                        </div>
                    </div>
                    
                     <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblByEmployee" runat="server" Text="Assigned User"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:DropDownList ID="ddlEmployee" CssClass="CommonDropDownList" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="false" ID="lblByLocation" runat="server" Text="City"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <asp:TextBox EnableViewState="false" ID="txtCity" CssClass="SearchTextbox" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <ucl:CountryState ID="uclCountryState" runat="server" FirstOption="Please Select" ShowAndHideState="false"
                        TableFormLabel_Width="40%" />
                    <div id="divBU" runat="server">
                    <div class="TableRow">
                        <div class="TableFormLeble" style="width: 40%;">
                            <asp:Label EnableViewState="true" ID="lblByEndClientsHeader" runat="server" Text="Account"></asp:Label>:
                        </div>
                        <div class="TableFormContent">
                            <%--<asp:DropDownList ID="ddlEndClient" CssClass="CommonDropDownList" runat="server">
                            </asp:DropDownList>--%>
                            <ucl:MultipleSelection ID="ddlEndClient" runat ="server" />
                        </div>
                    </div></div>
                </div>
                <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px">
                    <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search"
                        CssClass="btn btn-primary" ValidationGroup="RequisitionList" EnableViewState="false"
                        OnClick="btnSearch_Click"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                    <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                        EnableViewState="false" OnClick="btnClear_Click" />
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>

<%--*************************************************************************************--%>
  <div id="divBulkActions" class="btn-toolbar" runat="server">
               <div class="pull-left">
                      <div class="btn-group">
                        <a href="Javascript:void(0)" onclick="ShowReqDiv('divRecruiterListdiv')" class="btn dropdown-toggle"
                            id="refHot")">Add recruiter in team <span class="caret"></span></a>
                        <ul class="dropdown-menu-custom" id="divRecruiterListdiv" style="display: none">
                            <li style="width: 220px; text-align: left" id="list">
                                <asp:DropDownList EnableViewState="true" ID="ddlRecruiterlist" runat="server" CssClass="chzn-select"
                                    onmouseover="ShowDiv('divRecruiterListdiv')" onclick="DropdownClicked('divRecruiterListdiv')"
                                    Width="150px" onchange="DropdownChange('divRecruiterListdiv')">
                                </asp:DropDownList>
                                <asp:TextBox ID="TxtRecruiterList" runat="server" Visible="true" onfocus="AssignValue('Hotlist')"
                                    onblur="ReAssign()" onkeypress="checkEnter(event)" Width="150px" EnableViewState="true"
                                    onmouseover="ShowDiv('divRecruiterListdiv')"></asp:TextBox>
                                <asp:Button ID="btnAddToRecruiterList" CssClass="btn btn-small" runat="server" Text="Add" style=" margin-bottom : 10px"
                                    ValidationGroup="RecruiterList" OnClick="btnAddToRecruiterList_Click" onmouseover="ShowDiv('divRecruiterListdiv')" />
                                <div id="divRecruiter">
                                </div>
                                <ajaxToolkit:AutoCompleteExtender ID="hot_ListAutoComplete" runat="server" ServicePath="~/AjaxExtender.asmx"
                                    TargetControlID="TxtRecruiterList" MinimumPrefixLength="1" ServiceMethod="GetHotListItems"
                                    EnableCaching="true" CompletionListElementID="divRecruiter" CompletionListCssClass="AutoCompleteBox"
                                    CompletionInterval="0" FirstRowSelected="True" OnClientItemSelected="ValidateRecruiterList" />
                                <asp:CustomValidator ID="cvRecruiterList" runat="server" ErrorMessage="Please select a Recruiter list."
                                    EnableViewState="False" Display="Dynamic" ValidationGroup="RecruiterList" Font-Size="Small"
                                    Font-Bold="false" ClientValidationFunction="ValidateRecruiterList"></asp:CustomValidator>
                            </li>
                        </ul>
                  </div>             
                </div>
                                                          
            </div>

<%--**************************************************************************************--%>
</br>
</br>

<div>
    <asp:UpdatePanel ID="up" runat="server">
        <ContentTemplate>
            <ajaxToolkit:AlwaysVisibleControlExtender ID="avceMessage" runat="server" TargetControlID="lblMessage">
            </ajaxToolkit:AlwaysVisibleControlExtender>
            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<asp:UpdatePanel ID="upcandidateList" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnDoPostPack" runat="server" />
        <asp:HiddenField ID="hdnSortColumn" runat="server" />
        <asp:HiddenField ID="hdnSortOrder" runat="server" />
         <asp:HiddenField ID="HiddenField" runat="server" />
        <div class="GridContainer">
            <div style="overflow: inherit" id="bigDiv" onscroll='SetScrollPosition()'>
                <asp:ListView ID="lsvJobPosting" runat="server" EnableViewState="true" DataKeyNames="Id"
                    OnItemDataBound="lsvJobPosting_ItemDataBound" OnItemCommand="lsvJobPosting_ItemCommand"
                    OnPreRender="lsvJobPosting_PreRender" DataSourceID="odsRequisitionList">
                    <LayoutTemplate>
                        <table id="tlbTemplate"  class="Grid" cellspacing="0" border="0">
                            <tr id="trHeadLevel" runat="server">
                            
                                  <th style="width: 25px !important; white-space: nowrap;">
                                    <input id="chkAllItem" name="chkAllItem" type="checkbox" onclick="CLCheckUnCheckAllRowLevelCheckboxes(this,0)"
                                        disabled="disabled" />
                                </th>
                                <th style="white-space: nowrap; width: 85px !important">
                                    <asp:LinkButton ID="btnPostedDate" runat="server" CommandName="Sort" ToolTip="Sort By Post Date"
                                        CommandArgument="[J].[PostedDate]" Text="Post Date" TabIndex="2" />
                                </th>
                                <th runat="server" id="thReqCode" style="white-space: nowrap; width: 70px !important">
                                    <asp:LinkButton ID="lnkReqCode" runat="server" CommandName="Sort" ToolTip="Sort By Req Code"
                                        CommandArgument="[J].[JobPostingCode]" Text="Req Code" TabIndex="3" />
                                </th>
                                <th style="white-space: nowrap; width: 90px !important">
                                    <asp:LinkButton ID="lnkClientJobId" runat="server" CommandName="Sort" ToolTip="Sort By Client Job ID"
                                        CommandArgument="[J].ClientJobId" Text="Client Job ID" TabIndex="4" />
                                </th>
                                <th style="white-space: nowrap;" runat="server" id="thClient">
                                    <asp:LinkButton ID="lnkClient" runat="server" CommandName="Sort" ToolTip="Sort By Account"
                                        CommandArgument="[C].[CompanyName]" Text="Account" TabIndex="5" />
                                </th>
                                <th style="white-space: nowrap; min-width: 60px">
                                    <asp:LinkButton ID="btnJobTitle" runat="server" CommandName="Sort" ToolTip="Sort By Job Title"
                                        CommandArgument="[J].[JobTitle]" Text="Job Title" TabIndex="6" />
                                </th>
                                <th style="white-space: nowrap; min-width: 65px">
                                    <asp:LinkButton ID="btnCity" runat="server" CommandName="Sort" ToolTip="Sort By Location"
                                        CommandArgument="[J].[City]" Text="Location" TabIndex="7" />
                                </th>
                                <th  id="thNoOfOpening" runat="server" style="width: 97px !important">
                                    <asp:LinkButton ID="lnkNoOfOpening" runat="server" CommandName="Sort" ToolTip="# of Openings"
                                       Text="# of Openings" CommandArgument="[J].NoOfOpenings" TabIndex="8" />
                                </th>
                                <th id="thLevels" runat="server" style="width: 40px !important">
                                    <asp:LinkButton ID="lnkLevel" runat="server" Text="Total" ToolTip="Sort By Total Candidates"
                                        CommandName="Sort" CommandArgument="[1]"></asp:LinkButton>
                                </th>
                                <th style="width: 40px !important">
                                    <asp:LinkButton ID="lnkTeam" runat="server" Text="Team" ToolTip="Sort By Team" CommandArgument="[JMC].[ManagersCount]"
                                        CommandName="Sort"></asp:LinkButton>
                                </th>
                                <th style="white-space: nowrap; min-width: 50px; width: 105px !important">
                                    <asp:LinkButton ID="btnJobStatus" runat="server" Text="Status" CommandName="Sort"
                                        CommandArgument="[GL].[Name]" TabIndex="7" Width="50%" />
                                </th>
                                <th style="white-space: nowrap;" runat="server" id="thAssigningManager">
                                    Assigning Manager
                                </th>
                                <th style="text-align: center; white-space: nowrap; width: 45px !important;" id="thAction"
                                    runat="server">
                                    Action
                                </th>
                            </tr>
                            <tr>
                                <td colspan="7" style="padding: 0px 0px 0px 0px; border-style: none none none none">
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="10" runat="server" id="tdPager">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No data was returned.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        
                            <td>
                                <asp:CheckBox runat="server" ID="chkItemCandidate" OnClick="CheckUnCheckGridViewHeaderCheckbox('tlbTemplate','chkAllItem',0,1)" />
                                <asp:HiddenField ID="hdnID" runat="server" />

                            </td>
                            
                            <td>
                                <asp:Label ID="lblPostedDate" runat="server" Width="80px" />
                            </td>
                            <td id="tdJobCode" runat="server">
                                <asp:Label ID="lblJobCode" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblClientJobId" runat="server" />
                            </td>
                            <td runat="server" id="tdClient">
                                <asp:Label ID="lblClient" runat="server" />
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkJobTitle" runat="server" Target="_blank" TabIndex="8" Style="cursor: pointer;"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:Label ID="lblCity" runat="server" />
                            </td>
                             <td >
                                <asp:Label ID="lblNoOfOpenings" runat="server" />
                            </td>
                            <td id="tdLevels" runat="server">
                                <asp:HyperLink ID="tdLevel" runat="server"></asp:HyperLink>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkTeamCount" runat="server" CommandName="Managers"></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Label ID="lblJobStatus" runat="server" Style="min-height: 100px;" />
                                <asp:DropDownList ID="ddlJobStatus" runat="server" CssClass="CommonDropDownList"
                                    Style="min-width: 90px" Width="100px">
                                </asp:DropDownList>
                            </td>
                            <td runat="server" id="tdAssigningManager">
                                <asp:Label ID="lblAssignedManager" runat="server" />
                            </td>
                            <td style="overflow: inherit;" runat="server" id="tdAction" enableviewstate="true">
                                <ul class="nav" style="margin-bottom: 0px; padding-left: 0px;">
                                    <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle" style="width: 30px;
                                        margin-top: -15px;" href="#">
                                        <div class="container">
                                            <button class="btn btn-mini" type="button">
                                                <i class="icon icon-cog"></i><span class="caret"></span>
                                            </button>
                                        </div>
                                    </a>
                                        <ul class="dropdown-menu" style="margin-left: -120px; margin-top: -25px;">
                                            <li>
                                                <asp:HyperLink ID="hlkOpenHiringMatrix" runat="server"  Text="Open Hiring Matrix"></asp:HyperLink>
                                            </li>
                                            <li>
                                                <asp:LinkButton ID="btnEdit" Text="Edit Requisition" CommandName="EditItem" runat="server"></asp:LinkButton>
                                            </li>
                                            <li>
                                                <asp:LinkButton ID="btnDelete" Text="Delete" CommandName="DeleteItem" runat="server"></asp:LinkButton>
                                            </li>
                                            <li class="divider" id="liDivider" runat="server"></li>
                                            <li>
                                                <asp:LinkButton ID="lnkEmployeeReferralLink" runat="server" Text="Employee Referral Link"></asp:LinkButton></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
    </ContentTemplate>
  </asp:UpdatePanel>
