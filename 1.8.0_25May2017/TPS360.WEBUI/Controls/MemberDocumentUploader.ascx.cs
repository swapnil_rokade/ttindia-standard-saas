﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberDocumentUploader.ascx.cs
    Description: This is the user control page used to dispaly the member documents,insert,update and delete documents
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Oct-01-2008         Jagadish            Defect id 8782; Clicking on uploaded file link it was opening in
                                                               same window changed that to open in new window.
    0.2            Oct-15-2008         Jagadish            Defect id 8870; Disabled the delete button when edit button for particular 
                                                               document clicked.
    0.3            Oct-31-2008         Jagadish            Defect id 8782; Alert message was repeating. Changed the code to show appropriate message.                                                             
    0.4            Nov-20-2008         Jagadish            Defect id 9085; Changed the mouse pointer symbol from hand symbol to arrow when mouse over on 
                                                               the disabled delete button.
    0.5            Dec-18-2008         Gopala Swamy        Defect Id 9480; Created event for delete button in gridview control 
 
    0.6            JAN-28-2009         N.Srilakshmi        Defect Id 9355; Changes made in EnableDisableValidator.
    0.7            Jan-29-2009         Jagadish            Defect Id 9616; Added methods BuildMember(),BuildMemberDetail(),BuildMemberExInfo() and changes made in ParseSaveResume().
    0.8            Feb-13-2009         Kalyani Pallagani   Defect Id 9894; Added code in grdViewDocuments_RowCommand,EnableDisableValidator() and btnUpload_click
    0.9            Feb-13-2009         Rajendra Prasad     Defect Id 9330; Added single line in btnUpload_Click event.
    1.0            Feb-20-2009         Jagadish            Defect Id 9978; Replaced 2 lines of code in method btnUpload_Click();
    1.1            Feb-27-2009         Kalyani Pallagani   Defect Id 9980; Included code to reload the all the tabs when ever photo's and video's are added ,modified and deleted
    1.2            Mar-19-2009         N.Srilakshmi        Defect Id:10089 - Changes made in GetDocumentLink
    1.3            Aug-14-2009         Gopala Swamy        Defect Id:11208; Changed the message
    1.4            Aug-18-2009         Nagarathna .V.B     Defect Id:11295 : made the update button to work.
 *  1.5            Aug-31-2009         Ranjit Kumar.I      Defect Id:11426:Modified Code in grdViewDocuments_RowDataBound and in grdViewDocuments_RowCommand  
 *  1.6            Jan-06-2010         Basavaraj A         Defect Id:12032;Added Code to take the Resume name as 'FirstNameLastName-Resume'.
 *  1.7            May-13-2010         Ganapati Bhat       Defect Id:12806;Modified Code in EnableDisableValidator()
  * 1.8            20Jan2017           Prasanth Kumar G    id : 1153 checking extensions
    1.9            17Feb2017           Prasanth Kumar G   introduced function Check_FileMIMEType issue id 1153
--------------------------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Common;
using TPSTool.Parser;
using ParserLibrary;
using System.Web;
using System.Text;
using System.Net;
using System.Web.UI.HtmlControls;
using TPS360.Web.UI.Helper;



namespace TPS360.Web.UI
{
    public partial class ControlMemberDocumentUploader : BaseControl
    {
        #region Member Variables
        private static int _memberId = 0;
        private static string _memberrole = string.Empty;
        #endregion

        #region Properties

        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        public MemberDocumentType DocumentType
        {
            set
            {
                ViewState["DocumentTypeId"] = value;
                hfDocumentType.Value = EnumHelper.GetDescription(value);
            }
            get { return (MemberDocumentType)ViewState["DocumentTypeId"]; }
        }

        #endregion

        #region Methods

        private void LoadQueryStringData()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            else
            {
                _memberId = base.CurrentMember.Id;
            }
            if (_memberId > 0)
            {
                hfMemberId.Value = _memberId.ToString();
            }
        }

        private void LoadUploadedDocument()
        {
            //if (_memberId > 0)
            //{
            //    if (string.IsNullOrEmpty(hfDocumentType.Value))
            //    {
            //        IList<MemberDocument> lstAddedPosition = Facade.GetAllMemberDocumentByMemberId(_memberId);
            //        lsvDocuments.DataSource = lstAddedPosition;
            //        lsvDocuments.DataBind();
                    
            //    }
            //    else
            //    {
            //        IList<MemberDocument> lstAddedPosition = Facade.GetAllMemberDocumentByTypeAndMemberId(hfDocumentType.Value, _memberId);

            //        lsvDocuments.DataSource = lstAddedPosition;
            //        lsvDocuments.DataBind();
            //    }
            //}
        }

        private void LoadInitialData()
        {
            MiscUtil.PopulateDocumentType(ddlDocumentType, Facade);
            if (!string.IsNullOrEmpty(hfDocumentType.Value))
            {
                ddlDocumentType.SelectedValue = ((int)((MemberDocumentType)ViewState["DocumentTypeId"])).ToString();
                EnableDisableValidator(hfDocumentType.Value);
                rowDocumentType.Visible = false;
                lblDocumentTitle.Text = hfDocumentType.Value + " Title";
                lblDocumentPath.Text = hfDocumentType.Value + " Path";

                revPhoto.ValidationGroup = hfDocumentType.Value;
                revVideo.ValidationGroup = hfDocumentType.Value;
                revDocument.ValidationGroup = hfDocumentType.Value;
                rfvDocumentUpload.ValidationGroup = hfDocumentType.Value;
                rfvDocumentTitle.ValidationGroup = hfDocumentType.Value;
                btnUpload.ValidationGroup = hfDocumentType.Value;
            }
            else
            {
                rowDocumentType.Visible = true;
            }
           // LoadUploadedDocument();
           // lsvDocuments.DataBind();
         }

        private void EnableDisableValidator(string strDocumentType)
        {
            if (strDocumentType.ToLower().IndexOf("photo") >= 0)
            {
                revPhoto.Visible = true;
                revVideo.Visible = false;
                revDocument.Visible = false;
            }
            else if (strDocumentType.ToLower().IndexOf("video") >= 0)
            {
                revPhoto.Visible = false;
                revVideo.Visible = true;
                revDocument.Visible = false;
            }
            else
            {
                revPhoto.Visible = false;
                revVideo.Visible = false;
                revDocument.Visible = true;
            }
        }

        private void UploadDocument()
        {
            if (fuDocument.HasFile)
            {
               
                string strMessage = String.Empty;
                string _fileName = string.Empty;
                lblMessage.Text = string.Empty;
                bool boolError = false;
                string UploadedFilename = Convert.ToString(fuDocument.FileName);
                 //Code introduced by Prasanth on 20Jan2017 Start id 1153
                string extension = Path.GetExtension(fuDocument.FileName);
                string ContentType = fuDocument.PostedFile.ContentType;
                //Code introduced by Prasanth on 17Feb2017 Start id 1153
                if (MiscUtil.Check_FileMIMEType(fuDocument)== false)
                {
                    strMessage = "Such File not allowed to upload";
                }
                 
                //***********************End****************************
                else
                {
                    //********************END************************
                if (ddlDocumentType.SelectedItem.Text == "Word Resume")
                {
                    string[] FileName_Split = UploadedFilename.Split('.');
                    string ResumeName = FileName_Split [0] + " - Resume." + FileName_Split[FileName_Split.Length - 1];
                    UploadedFilename = ResumeName;
                }
                string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, _memberId, UploadedFilename, ddlDocumentType.SelectedItem.Text, false);//1.6
                if (CheckFileSize())
                {
                    fuDocument.SaveAs(strFilePath);
                    
                    if (File.Exists(strFilePath))
                    {
                        MemberDocument memberDocument =Facade .GetMemberDocumentByMemberIdTypeAndFileName (_memberId,ddlDocumentType .SelectedItem .Text .Trim () , UploadedFilename) ;//Facade.GetMemberDocumentByMemberIdAndFileName(_memberId, UploadedFilename);//1.6
                        if (memberDocument == null)
                        {
                            MemberDocument newDoc = new MemberDocument();
                            newDoc.FileName = UploadedFilename;//1.6
                            newDoc.FileTypeLookupId = Int32.Parse(ddlDocumentType.SelectedValue);
                            newDoc.Description =MiscUtil .RemoveScript (txtDocumentDescription.Text);
                            newDoc.MemberId = _memberId;
                            newDoc.Title = MiscUtil.RemoveScript(txtDocumentTitle.Text);
                            if (newDoc.Title.ToString() != string.Empty)
                            {
                                Facade.AddMemberDocument(newDoc);
                                MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.UploadDocument, Facade);
                                if (string.IsNullOrEmpty(strMessage))
                                {
                                    strMessage = "Successfully uploaded the file";
                                }
                            }
                        }
                        else
                        {
                            strMessage = "Already this document available";
                        }
                    }
                }
                else
                {
                    boolError = true;
                    strMessage = "File size should be less than 3 MB";
                }
                    //Code commented by Prasanth on 02Feb2017
                    //if (MiscUtil.RemoveScript(txtDocumentTitle.Text.Trim().ToString()) != string.Empty)
                    //    MiscUtil.ShowMessage(lblMessage, strMessage + lblMessage.Text, boolError);
                }
                //Code introduced by Prasanth On 02Feb2017 start
                if (MiscUtil.RemoveScript(txtDocumentTitle.Text.Trim().ToString()) != string.Empty)
                    MiscUtil.ShowMessage(lblMessage, strMessage + lblMessage.Text, boolError);
                //**********************END*********************
            }
        }

        private void UpdateDocument()
        {
                MemberDocument newDoc = new MemberDocument();
                newDoc.Description =MiscUtil .RemoveScript ( txtDocumentDescription.Text);
                newDoc.Title =MiscUtil .RemoveScript ( txtDocumentTitle.Text);
                newDoc.FileName = hfMemberDocumentName.Value;
                newDoc.FileTypeLookupId = Int32.Parse(ddlDocumentType.SelectedValue);
                newDoc.MemberId = _memberId;
                newDoc.Id = Int32.Parse(hfMemberDocumentId.Value);
                Facade.UpdateMemberDocument(newDoc);
                MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.UpdateDocument, Facade);
                MiscUtil.ShowMessage(lblMessage, "Successfully updated the file", false);
        }

        private bool CheckFileSize()
        {
            decimal  fileSize = Convert.ToDecimal (Convert .ToDecimal ( fuDocument.FileContent.Length) / (1024 * 1024));
            if (fileSize >  Convert .ToDecimal ( 3.0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private string GetDocumentLink(string strFileName, string strDocumenType)
        {

            string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, _memberId, strFileName, strDocumenType, true);

            if (strFilePath != strFileName)
            {
                return "<a href='" + strFilePath + "' target='_blank'>" + strFileName + "</a>"; // 0.1
            }
            else
            {
                return strFileName + "(File not found)";
            }
        }

        private bool DeleteUploadedDocement(int intId, string strFileName, string strDocumentType)
        {
            if (Facade.DeleteMemberDocumentById(intId))
            {
                string strPath = MiscUtil.GetMemberDocumentPath(this.Page, _memberId, strFileName, strDocumentType, false);
                File.Delete(strPath);
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            _memberId = Int32.Parse(hfMemberId.Value);
            if (!IsPostBack)
            {              
                LoadQueryStringData();
                LoadInitialData();
                odsDoc.SelectParameters["MemberID"].DefaultValue = _memberId.ToString();
            }
            lblMessage.Text = "";
            lblMessage.CssClass = "";
        }

        protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //EnableDisableValidator(ddlDocumentType.SelectedItem.Text);
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string strMessage = string.Empty;

            if (hfMemberDocumentId.Value == String.Empty)
            {
                UploadDocument();
                strMessage = "Successfully uploaded the file.";
            }
            else
            {
                ddlDocumentType.Enabled = true;
                fuDocument.Enabled = true;
                rfvDocumentUpload.Enabled = true;
                cvDocumentType.Enabled = true;
                if (btnUpload.Text == "Update")
                {
                    UpdateDocument();
                }
            }
            Refresh(ddlDocumentType.SelectedItem.Text.ToLower(), strMessage);
            if (ddlDocumentType.Visible)
                ddlDocumentType.SelectedValue = "0";
           // LoadUploadedDocument();
            lsvDocuments.DataBind();
            txtDocumentDescription.Text = String.Empty;
            txtDocumentTitle.Text = String.Empty;
            hfMemberDocumentId.Value = String.Empty;
        }
        protected void lsvDocument_OnItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (StringHelper.IsEqual(e.CommandName, "DeleteItem"))
            {
                string[] strKeyWord = e.CommandArgument.ToString().Split(':');
                if (strKeyWord.Length == 3 && DeleteUploadedDocement(Int32.Parse(strKeyWord[0]), strKeyWord[1], strKeyWord[2]))
                {
                    MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.DeleteDocument, Facade);
                    MiscUtil.ShowMessage(lblMessage, "Successfully deleted the file.", false);
//                    LoadUploadedDocument();
                    lsvDocuments.DataBind();

                    Refresh(strKeyWord[2].ToLower(), "Successfully deleted the file.");
                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "Could not delete the file.", true);
                }

                if (ddlDocumentType.Enabled)
                    ddlDocumentType.SelectedValue = "0";
                int intId = Convert.ToInt32(strKeyWord[0].ToString());
                if (string.Compare(intId.ToString(), hfMemberDocumentId.Value) == 0)
                {
                    ddlDocumentType.Enabled = true;
                    fuDocument.Enabled = true;
                    rfvDocumentUpload.Enabled = true;
                    cvDocumentType.Enabled = true;
                    txtDocumentDescription.Text = "";
                    txtDocumentTitle.Text = "";
                    hfMemberDocumentId.Value = string.Empty;
                    ddlDocumentType.SelectedIndex = 0;
                }
            }
            else if (StringHelper.IsEqual(e.CommandName, "EditItem"))
            {
                int intId = Convert.ToInt32(e.CommandArgument);
                MemberDocument curDoc = Facade.GetMemberDocumentById(intId);
                txtDocumentDescription.Text = MiscUtil.RemoveScript(curDoc.Description, string.Empty);
                txtDocumentTitle.Text = MiscUtil.RemoveScript(curDoc.Title, string.Empty);
                ddlDocumentType.SelectedValue = curDoc.FileTypeLookupId.ToString();
                hfMemberDocumentId.Value = intId.ToString();
                hfMemberDocumentName.Value = curDoc.FileName;
                ddlDocumentType.Enabled = false;
                fuDocument.Enabled = false;
                rfvDocumentUpload.Enabled = false;
                cvDocumentType.Enabled = false;
                btnUpload.Text = "Update";
            }
        }

        protected void lsvDocuments_PreRender(object sender, EventArgs e)
        {
            if (lsvDocuments != null)
            {
                HtmlTableCell tdpager = (HtmlTableCell)lsvDocuments.FindControl("tdPager");
                ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvDocuments.FindControl("pagerControl");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }
                    HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                    if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "MemberDocunebtUploaderRowPerPage";
                    
                }
            }
        }

        protected void lsvDocuments_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (TPS360.Web.UI.Helper.ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberDocument memberDocument = ((ListViewDataItem )e.Item ).DataItem as MemberDocument;
                if (memberDocument != null)
                {
                    GenericLookup docType = Facade.GetGenericLookupById(memberDocument.FileTypeLookupId);
                    Label lblDocumentLink = e.Item.FindControl("lblDocumentLink") as Label;
                    Label lblDocumentType = e.Item.FindControl("lblDocumentType") as Label;
                    Label lblDocumentTitle = e.Item.FindControl("lblDocumentTitle") as Label;
                    if (docType != null)
                    {
                        lblDocumentLink.Text = GetDocumentLink(memberDocument.FileName, docType.Name);
                        lblDocumentType.Text = docType.Name;
                        lblDocumentTitle.Text = memberDocument.Title;
                        ((ImageButton)e.Item.FindControl("btnDelete")).CommandArgument = memberDocument.Id.ToString() + ":" + memberDocument.FileName + ":" + docType.Name;// DefectID #11426
                        ((ImageButton)e.Item.FindControl("btnDelete")).OnClientClick = "return ConfirmDelete('uploaded file')"; //0.5
                        ((ImageButton)e.Item.FindControl("btnEdit")).CommandArgument = memberDocument.Id.ToString();
                    }
                }
            }
        }
    

       
        private void Refresh(string strSelected, string strMessage)
        {
            if (base.CurrentUserRole == ContextConstants.ROLE_CANDIDATE || base.CurrentUserRole == ContextConstants.ROLE_CONSULTANT)
            {

                if (strSelected.IndexOf("photo") >= 0 || strSelected.IndexOf("video") >= 0)
                {
                    string strurl = Request.Url.ToString();
                    if (!StringHelper.IsBlank(strurl) && strurl.IndexOf('?') > 0)
                    {
                        int intTab = 0;
                        if (revPhoto.Visible)
                            intTab = 2;
                        else if (revVideo.Visible)
                            intTab = 1;

                        Helper.Session.Set("AlreadyMemberDocumentLoaded", strMessage + "+" + intTab.ToString());
                        string[] urlAndQueryString = strurl.Split('?');
                        string _pageUrl = urlAndQueryString[0];
                        Helper.Url.Redirect(_pageUrl, string.Empty, UrlConstants.PARAM_MEMBER_ID, Convert.ToString(_memberId));
                    }
                }
            }
        }
        #endregion
    }
}
