﻿<%@ control Language="C#" AutoEventWireup="true" CodeFile="MemberJobCarts.ascx.cs"
    Inherits="TPS360.Web.UI.MemberJobCarts" %>
<%@ Register Src ="~/Controls/PagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>  
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
   
   <script >
  
    function ModalOperationCompleted(message)
    {

        var iframe=document .getElementById ('iframe');
        iframe .src ="about:blank";

        $find("mpeModal").hide();
        ShowMessage('<%= lblMessage.ClientID %>',message,false  );
            //  __doPostBack('ctl00_cphCandidateMaster_uclCandidateOverView_uclJobCart_upCart','');
    }
    function pageLoad() {
   
         var background = $find("mpeModal")._backgroundElement;
         background.onclick = function()
          {
            var iframe=document .getElementById ('iframe');
            iframe .src ="about:blank";
            $find("mpeModal").hide(); 
           }

       
    }
    
    function ModalChanged(ddl,canid,UpdatorId,JobPostingId)
    {   

        var iframe=document .getElementById ('iframe');
        iframe .src ="";
        iframe .width ="778px" ;
        iframe .height = "570px" ;
         if(ddl.options[ddl.selectedIndex].text=='Offer Decline')
          {
           iframe .width ="470px";
            iframe .height ="370px";
          }
            if(ddl.options[ddl.selectedIndex].text=='Joined')
          {
           iframe .width ="470px";
            iframe .height ="370px";
          }
          
        if(ddl.options[ddl.selectedIndex].text=='Rejected')
        {
            if(!ConfirmAction('Are you sure that you want to remove this candidate from the Hiring Matrix?')) 
            {
                return false ;
            }
            iframe .width ="500px";
            iframe .height ="275px";
        }
        else if(IsNumeric(ddl.options[ddl.selectedIndex].value))
        { 
            PageMethods.MoveCandidatesToLevel({'Member_Id':canid  ,'JobPosting_Id':JobPostingId ,'StatusId':ddl.options[ddl.selectedIndex].value ,'UpdatorId':UpdatorId    },CandidateMovedCallBack);
            return false ;
        }
        iframe .src =ddl.options[ddl.selectedIndex].value+ '&Canid=' + canid;
        

            if(iframe .src.indexOf('Interview')>=0)
            {
                iframe .width ="700px";
                iframe .height ="570";
                var statussid= getParameterByName("StatusId", iframe .src );
                if(canid ==0) canid =canids .value;
                PageMethods.MoveCandidatesToLevel({'Member_Id':canid  ,'JobPosting_Id':JobPostingId ,'StatusId':statussid ,'UpdatorId':UpdatorId   },CandidateMovedToInterviewCallBack);
            }  
  
          
        $find('mpeModal').show();
      
        return false ;
    }
    
     function getParameterByName(name,url)
{

  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(url);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

    function IsNumeric(input) {
        return (input - 0) == input && input.length > 0;
    }

   function CandidateMovedCallBack()
   {
        ModalOperationCompleted ('Candidate Moved Successfully');
   }
function CandidateMovedToInterviewCallBack()
{
    ShowMessage('<%= lblMessage.ClientID %>','Candidate moved successfully',false );
}
   </script>
   


   
<asp:UpdatePanel ID="upCart" runat ="server" >
<ContentTemplate >

<asp:HiddenField ID ="Level" runat ="server" />
<asp:HiddenField ID ="ReqId" runat ="server" />
 
<asp:TextBox ID="txtSortColumn" runat ="server" Visible ="false"  ></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat ="server" Visible ="false"  ></asp:TextBox>
 <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
           <asp:Panel ID="pnlJobCartContent" runat="server" >
            <div style="text-align: left; ">
              <asp:ObjectDataSource ID="odsJobCartList" runat="server" SelectMethod="GetPaged"
                            TypeName="TPS360.Web.UI.MemberJobCartDataSource" EnablePaging="true" SelectCountMethod="GetListCount"
                            SortParameterName="sortExpression">
                            <SelectParameters>
                                <asp:Parameter Name="memberId" DefaultValue="0" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    
                <asp:ListView ID="lsvJobCart" runat="server" DataKeyNames="Id" DataSourceID ="odsJobCartList" EnableViewState="true" OnItemDataBound="lsvJobCart_ItemDataBound"  OnPreRender ="lsvJobCart_PreRender" OnItemCommand ="lsvJobCart_ItemCommand">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr>
                                <th style="width: 15%; white-space: nowrap;">
                                <asp:LinkButton ID ="lnkJobTitle" runat ="server" CommandArgument ="[J].[JobTitle]" CommandName  ="Sort" Text ="Job Title" ToolTip ="Job Title"></asp:LinkButton> </th>
                                <th runat="server" id="thCode" style="width: 15%; white-space: nowrap;">
                                <asp:LinkButton ID ="lnkCode" runat ="server" CommandArgument ="[J].[JobPostingCode]" CommandName  ="Sort" Text ="Code" ToolTip ="JobPosting Code"></asp:LinkButton> </th>
                                
                               <th runat="server" id="thBU" visible="false" style="width: 15%; white-space: nowrap;">
                                <asp:LinkButton ID ="lnkBU" runat ="server" CommandArgument ="[J].[JobPostingCode]" CommandName  ="Sort" Text ="BU" ToolTip ="BU"></asp:LinkButton> </th>
                                <th style="width: 15%; white-space: nowrap;">
                                <asp:LinkButton ID ="lnkUpdatedate" runat ="server" CommandArgument ="[J].[UpdateDate]" CommandName  ="Sort" Text ="Date Published" ToolTip ="Date Published"></asp:LinkButton></th>  
                                <th style="width: 15%; white-space :nowrap;">
                                <asp:LinkButton ID ="lnkStatus" runat ="server" CommandArgument ="[HL].[Name]" CommandName  ="Sort" Text ="Status" ToolTip ="Status"></asp:LinkButton></th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                              <tr class="Pager">
                        <td colspan="4" runat ="server" id="tdPager">
                             <ucl:Pager Id="pagerControl" runat ="server" EnableViewState ="true"  />
                        </td>
                    </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>  No jobs in Job Cart. </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td> <asp:HyperLink ID="lblJobTitle" Target="_blank" runat="server"></asp:HyperLink><br /></td>
                            
                            <td runat="server" id="tdJobCode" style="white-space: nowrap;"> <asp:Label ID="lblJobCode" runat="server" /> </td>
                             <td runat="server" visible="false" id="tdBU" style="white-space: nowrap;"> <asp:Label ID="lblBU" runat="server" /> </td>
                           
                            <td style="white-space: nowrap;"> <asp:Label  ID="lblPublishedDate" runat="server" /> </td>
                           
                            <td  style="white-space: nowrap;">
                            <asp:HiddenField ID ="hdnJobPostingId" runat ="server" />
                            <asp:HiddenField ID ="hdnStatusId" runat ="server" />
                            <asp:HiddenField ID ="hdnCurrentLevel" runat ="server" />
                            <asp:Label ID ="lblStatus" runat ="server" ></asp:Label>
                            <asp:DropDownList ID="ddlStatus"  runat="server" CssClass ="CommonDropDownList"  Enabled="false" AutoPostBack ="false" Width ="90%"  ></asp:DropDownList></td> 
                          </tr>
                    </ItemTemplate>
                </asp:ListView>
          
            </div>
        </asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>