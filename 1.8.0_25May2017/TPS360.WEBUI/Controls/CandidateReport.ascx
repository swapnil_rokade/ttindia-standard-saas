﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:CandidateReport.ascx
    Description By:This form is common for CandidateReport.aspx and MyCandidateReport.aspx 
    Created By:Sumit Sonawane
    Created On:25/May/2016
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
      0.1             28/July/2016         pravin khot          added- lblReqCode
    ---------------------------------------------------------------------------------------------------------------------------------------     

--%>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CandidateReport.ascx.cs"
    Inherits="TPS360.Web.UI.ControlCandidateReports" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src ="~/Controls/CountryStatePicker.ascx" TagName ="CountryState" TagPrefix ="ucl" %>

<style>
   
    </style>
    <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
    <script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
    <script src="../js/EmployeeAndTeamOnChange.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    
        //To Keep the scroll bar in the Exact Place
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
        function PageLoadedReq(sender, args) {
            try {
                var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
                var bigDiv = document.getElementById('bigDiv');
                bigDiv.scrollLeft = hdnScroll.value;
            }
            catch (e) {
            }
        }
        function SetScrollPosition() {
            var hdnScroll = document.getElementById('ctl00_cphHomeMaster_hdnScrollPos');
            var bigDiv = document.getElementById('bigDiv');
            hdnScroll.value = bigDiv.scrollLeft;
        }

        

        function SelectUnselectColumnOption(chkColumnOption) {
            var chkBoxAll = $get('<%= chkColumns.ClientID%>');
            var chkBoxList = $get('<%= chkColumnList.ClientID%>');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            if (chkBoxAll.checked) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = true;
                }
            }
            if (!chkBoxAll.checked) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = false;
                }
            }
        }
        function SelectUnselectAllColumnOption(chkColumnOptionHeader) {
            var checkedCount = 0;
            //debugger;
            var chkBoxAll = $get('<%= chkColumns.ClientID%>');
            var chkBoxList = $get('<%= chkColumnList.ClientID%>');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            for (var i = 0; i < chkBoxCount.length; i++) {
                if (chkBoxCount[i].checked) {
                    checkedCount++;
                }
            }
            if (checkedCount == chkBoxCount.length) {
                chkBoxAll.checked = true;
            }
            else {
                chkBoxAll.checked = false;
            }
        }
    </script>

    <style>
        .TableFormLeble
        {
            width: 25%;
        }
    </style>
    <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>

    <asp:UpdatePanel ID="pnlCandidatereports" runat="server" UpdateMode="Always" >
        <ContentTemplate>
            <asp:HiddenField ID="hdnScrollPos" runat="server" />
            <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
            <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
            <asp:ObjectDataSource ID="odsCandidateList" runat="server" SelectMethod="GetPaged"
                TypeName="TPS360.Web.UI.CandidateDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:ControlParameter ControlID="wdcCandidatesAdded" Name="addedFrom" PropertyName="StartDate" />
                    <asp:ControlParameter ControlID="wdcCandidatesAdded" Name="addedTo" PropertyName="EndDate" />
                    <asp:ControlParameter ControlID="wdcCandidatesUpdated" Name="updatedFrom" PropertyName="StartDate" />
                    <asp:ControlParameter ControlID="wdcCandidatesUpdated" Name="updatedTo" PropertyName="EndDate" />
                    <asp:Parameter Name="addedBy" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="addedBySource" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="updatedBy" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="interviewFrom" DefaultValue="Value" Type="DateTime" />
                    <asp:Parameter Name="interviewTo" DefaultValue="Value" Type="DateTime" />
                    <asp:Parameter Name="interviewLevel" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="interviewStatus" DefaultValue="0" Type="Int32" />
                    <asp:ControlParameter ControlID ="uclCountryState" Name ="country" PropertyName ="SelectedCountryId" />
                    <asp:ControlParameter ControlID ="uclCountryState" Name ="state" PropertyName ="SelectedStateId" />
                    <asp:Parameter Name="city" DefaultValue="0" Type="String" />
                    <asp:Parameter Name="industry" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="functionalCategory" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="workPermit" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="WorkSchedule" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="gender" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="maritalStatus" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="educationId" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="assessmentId" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="isBroadcastedResume" DefaultValue="false" Type="Boolean" />
                    <asp:Parameter Name="isReferredApplicants" DefaultValue="false" Type="Boolean" />
                    <asp:Parameter Name="hasJobAgent" DefaultValue="false" Type="Boolean" />
                    <asp:Parameter Name="assignedManager" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="IsTeamReport" DefaultValue="false" Type="Boolean" />
                    <asp:Parameter Name="TeamMembersId" DbType="String" DefaultValue="0" />
                    <asp:Parameter Name="TeamId" DbType="Int32" DefaultValue="0" />
                    <asp:Parameter Name="SortOrder" DefaultValue="asc" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Panel ID="pnlSearchRegion" runat="server" DefaultButton="btnSearch">
                <div style="width: 100%;">
                    <div>
                        <div class="TableRow">
                            <div class="TabPanelHeader nomargin" style="border-style: none none none;">Filter Options</div>
                        </div>
                        <div class="TableRow" style="text-align: center">
                            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <div runat="server" id="divTeam" visible="false">
                            <div class="TableRow" style="white-space: nowrap; margin-bottom: 5px;">
                                <div class="TableFormLeble" style="">
                                    <asp:Label EnableViewState="false" ID="lblTeam" runat="server" Text="Candidates by Team"></asp:Label>
                                :
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList EnableViewState="true" ID="ddlTeamList" TabIndex="12" OnSelectedIndexChanged="ddlTeamList_SelectedIndexChanged" CssClass="CommonDropDownList"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble" style="">
                                <asp:Label ID="lblCandidatesAdded" EnableViewState="false" runat="server" Text="New Candidates"></asp:Label>
                                :
                            </div>
                            <div class="TableFormContent">
                                <ucl:DateRangePicker ID="wdcCandidatesAdded1" runat="server" Visible ="false"  />   
                                <div style="float: left;"><ucl:DateRangePicker ID="wdcCandidatesAdded" runat="server" />
                                    <span id="spnAddedByEmployee" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Added By User :&nbsp;&nbsp;</span>
                                </div>
                                <div style="float: left;">
                                    <asp:DropDownList EnableViewState="true" ID="ddlAddedByEmployee" TabIndex="1" CssClass="CommonDropDownList"
                                        runat="server">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnAddedBy" Value="0" runat="server" />
                                </div>
                            </div>
                        </div>
         
                        <div class="TableRow">
                            <div class="TableFormLeble">
                                <asp:Label EnableViewState="false" ID="lblCandidatesUpdated" runat="server" Text="Candidates Updated"></asp:Label>
                                :
                            </div>
                            <div class="TableFormContent">
                               
                                   
                                <div style="float: left;"> <ucl:DateRangePicker ID="wdcCandidatesUpdated" runat="server" />
                                    <span id="spnUpdatedByEmployee" runat="server">&nbsp;&nbsp;Updated By User :&nbsp;&nbsp;</span>
                                </div>
                                <div style="float: left;">
                                    <asp:DropDownList EnableViewState="true" ID="ddlUpdatedByEmployee" TabIndex="2" CssClass="CommonDropDownList"
                                        runat="server">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnUpdatedBy" Value="0" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="TableRow" style="white-space: nowrap">
                            <div class="TableFormLeble" style="">
                               <%--  <asp:Label EnableViewState="false" ID="lblCandidatesByLocation" runat="server" Text="Candidates By Location"></asp:Label>--%>
                               City
                                :
                            </div>
                            <div class="TableFormContent">
                                    <asp:TextBox EnableViewState="false" ID="txtCity" CssClass="SearchTextbox" TabIndex="3" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <ucl:CountryState ID="uclCountryState" runat ="server" FirstOption ="Any" CountryTabIndex="4" StateTabIndex="5" />
                         <div class="TableRow" runat="server" id="divAssignedManager" style="white-space: nowrap">
                            <div class="TableFormLeble" style="">
                                <asp:Label EnableViewState="true" ID="lblAssignedManager" runat="server" Text="Candidates by Manager"></asp:Label>
                                :
                            </div>
                            <div class="TableFormContent">
                                <div style="float: left;">
                                    <asp:DropDownList EnableViewState="true" ID="ddlAssignedManager" TabIndex="6" CssClass="CommonDropDownList"
                                        runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="TableRow" style="white-space: nowrap">
                            <div class="TableFormLeble" style="float: left;">
                                <asp:Label EnableViewState="false" ID="lblCandidatesFunctionalCategory" runat="server"
                                    Text="Candidates by Work Permit Status"></asp:Label>
                                :
                            </div>
                            <div class="TableFormContent">
                                <div style="float: left;">
                                    <asp:DropDownList EnableViewState="true" ID="ddlCandidatesWorkPermit" TabIndex="7" CssClass="CommonDropDownList"
                                        runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="TableRow" style="white-space: nowrap">
                            <div class="TableFormLeble" style="float: left;">
                                <asp:Label EnableViewState="false" ID="lblWorkSchedule" runat="server"  Text="Candidates by Work Schedule"></asp:Label>
                                :
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList EnableViewState="true" ID="ddlWorlSchedule" TabIndex="8" CssClass="CommonDropDownList"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="TableRow" style="white-space: nowrap">
                            <div class="TableFormLeble" style="">
                                <asp:Label EnableViewState="false" ID="lblCandidatesEducation" runat="server" Text="Candidates by Education"></asp:Label>
                                :
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList EnableViewState="true" ID="ddlCandidatesEducation" TabIndex="9" CssClass="CommonDropDownList"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="TableRow" style="white-space: nowrap">
                            <div class="TableFormLeble" style="">
                                <asp:Label EnableViewState="false" ID="lblCandidatesGender" runat="server" Text="Candidates by Gender"></asp:Label>
                                :
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList EnableViewState="true" ID="ddlCandidatesGender" TabIndex="10" CssClass="CommonDropDownList"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="TableRow" style="white-space: nowrap; margin-bottom: 5px;">
                            <div class="TableFormLeble" style="">
                                <asp:Label EnableViewState="false" ID="lblCandidatesMaritalStatus" runat="server"
                                    Text="Candidates by Marital Status"></asp:Label>
                                :
                            </div>
                            <div class="TableFormContent">
                                <asp:DropDownList EnableViewState="true" ID="ddlCandidatesMaritalStatus" TabIndex="11" CssClass="CommonDropDownList"
                                    runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        
                         
                        <div style="text-align: left">
                            <asp:Label ID="lbMessage" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <div class="TableRow well well-small nomargin">
                            <asp:CollapsiblePanelExtender ID="cpnlColumnSelection" runat="server" TargetControlID="pnlColumnSelectionBody"
                                ExpandControlID="pnlColumnSelectionHeader" CollapseControlID="pnlColumnSelectionHeader"
                                Collapsed="true" ImageControlID="imgShowHideColumnSelection" CollapsedImage="~/Images/expand-plus.png"
                                ExpandedImage="~/Images/collapse-minus.png" SuppressPostBack="true">
                            </asp:CollapsiblePanelExtender>
                            <asp:Panel ID="pnlColumnSelectionHeader" runat="server">
                                <div class="" style="clear: both; cursor: pointer">
                                    <asp:Image ID="imgShowHideColumnSelection" ImageUrl="~/Images/expand-plus.png" runat="server" />
                                    <asp:Label ID="lblDisplayColumns" runat="server" Text="Included Columns" EnableViewState="false"></asp:Label>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlColumnSelectionBody" runat="server" Style="overflow: hidden;" Height="0">
                                <div class="TableRow" style="text-align: left; padding-top: 5px">
                                    <asp:CheckBox runat="server" Text="Select All" ID="chkColumns" CssClass ="ColumnSelection" onclick="javascript:SelectUnselectColumnOption(this);" />
                                    <asp:CheckBoxList ID="chkColumnList" RepeatDirection="Horizontal" runat="server" CssClass ="ColumnSelection"
                                        RepeatColumns="7" onclick="javascript:SelectUnselectAllColumnOption('chkColumns')">
                                        <asp:ListItem Value ="Id" Selected ="True" >Candidate ID #</asp:ListItem>
                                        <asp:ListItem Value="Name" Selected="True">Candidate Name</asp:ListItem>
                                        <asp:ListItem Value="NickName">NickName</asp:ListItem>
                                        <asp:ListItem Value="PrimaryEmail" Selected="True">Primary Email</asp:ListItem>
                                        <asp:ListItem Value="AlternateEmail">Alternate Email</asp:ListItem>
                                        <asp:ListItem Value="PermanentAddress">Permanent Address</asp:ListItem>
                                        <asp:ListItem Value="PermanentCity">Permanent City</asp:ListItem>
                                        <asp:ListItem Value="PermanentState">Permanent State</asp:ListItem>
                                        <asp:ListItem Value="PermanentZip">Permanent Zip Code</asp:ListItem>
                                        <asp:ListItem Value="PermanentCountry">Permanent Country</asp:ListItem>
                                        <asp:ListItem Value="PermanentPhone">Permanent Phone</asp:ListItem>
                                        <asp:ListItem Value="PermanentMobile">Permanent Mobile</asp:ListItem>
                                        <asp:ListItem Value="CurrentAddress">Current Address</asp:ListItem>
                                        <asp:ListItem Value="CurrentCity" Selected="True">Current City</asp:ListItem>
                                        <asp:ListItem Value="CurrentStateName" Selected="True">Current State</asp:ListItem>
                                        <asp:ListItem Value="CurrentZip">Current Zip Code</asp:ListItem>
                                        <asp:ListItem Value="CurrentCountry">Current Country</asp:ListItem>
                                        <asp:ListItem Value="PrimaryPhone" Selected="True">Primary Phone</asp:ListItem>
                                        <asp:ListItem Value="CellPhone" Selected="True">Cell Phone</asp:ListItem>
                                        <asp:ListItem Value="HomePhone" Selected="True">Home Phone</asp:ListItem>
                                        <asp:ListItem Value="OfficePhone">Office Phone</asp:ListItem>
                                        <asp:ListItem Value="CurrentPosition" Selected="True">Current Position</asp:ListItem>
                                        <asp:ListItem Value="LastEmployer" Selected="True">Current Company</asp:ListItem>
                                        <asp:ListItem Value="DateOfBirth">Date Of Birth</asp:ListItem>
                                        <asp:ListItem Value="CityOfBirth">City Of Birth</asp:ListItem>
                                        <asp:ListItem Value="CountryOfBirth">Country Of Residence</asp:ListItem>
                                        <asp:ListItem Value="Citizenship">Citizenship</asp:ListItem>
                                        <asp:ListItem Value="Gender">Gender</asp:ListItem>
                                        <asp:ListItem Value="MaritalStatus">Marital Status</asp:ListItem>
                                        <asp:ListItem Value="YearsOfExperience">Years Of Experience</asp:ListItem>
                                        <asp:ListItem Value="Availability">Availability</asp:ListItem>
                                        <asp:ListItem Value="WillingToTravel">Willing To Travel</asp:ListItem>
                                       <%-- <asp:ListItem Value="SalaryType">Salary Type</asp:ListItem>--%>
                                       <%-- <asp:ListItem Value="JobType">Employement Duration</asp:ListItem>--%>
                                       <%-- <asp:ListItem Value="SecurityClearance">Security Clearance</asp:ListItem>--%>
                                      <%--  <asp:ListItem Value="WorkPermit">Work Permit</asp:ListItem>--%>
                                        <asp:ListItem Value="WorkSchedule">Work Schedule</asp:ListItem>
                                        <asp:ListItem Value="CurrentYearlyRate">Current Rate/Salary</asp:ListItem>
                                        <asp:ListItem Value="ExpectedYearlyRate">Expected Rate/Salary</asp:ListItem>
                                        <%--<asp:ListItem Value="ResumeSource">Resume Source</asp:ListItem>--%>
                                        <asp:ListItem Value="Creator">Creator</asp:ListItem>
                                        <asp:ListItem Value="CreateDate">Create Date</asp:ListItem>
                                        <asp:ListItem Value="Updator">Last Updator</asp:ListItem>
                                        <asp:ListItem Value="UpdateDate">Last Update Date</asp:ListItem>
                                         <asp:ListItem Value="Website">Website</asp:ListItem>
                                         <asp:ListItem Value="LinkedinProfile"> LinkedIn Profile</asp:ListItem>
                                         <asp:ListItem Value="PassportStatus"> Passport Status</asp:ListItem>
                                         <asp:ListItem Value="IDProof">ID Proof</asp:ListItem>
                                         <asp:ListItem Value="HighestEducation"> Highest Education</asp:ListItem>
                                         <asp:ListItem Value="Source"> Source</asp:ListItem>
                                         <asp:ListItem Value="SourceDescription">Source Description</asp:ListItem>
                                         <asp:ListItem Value="ReqCode">Req. Code</asp:ListItem>
                                          <asp:ListItem Value="HiringStatus">Hiring Status</asp:ListItem>
                                        </asp:CheckBoxList>
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="TableRow" style="text-align: center; padding-top: 5px; padding-bottom: 5px;">
                            <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search" TabIndex="12"
                                CssClass="btn btn-primary" EnableViewState="false" ValidationGroup="Search" OnClick="btnSearch_Click"><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                            <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                                EnableViewState="false" CausesValidation="false" TabIndex="13" OnClick="btnClear_Click" />
                        </div>
                         <div id="divCandidateList" runat="server">
                        <div class="TableRow" style="padding-bottom: 5px;" id="divExportButton" runat="server"
                            visible="false">
                            <div class="TableRow">
                                <div class="TabPanelHeader nomargin" style="margin-bottom: 5px;border-style: none none none;">Report Results</div>
                            </div>
                            <asp:ImageButton ID="btnExportToExcel" CssClass ="btn" TabIndex="14"  SkinID ="sknExportToExcel" runat ="server" ToolTip ="Export To Excel" OnClick ="btnExportToExcel_Click" />
                            <asp:ImageButton ID="btnExportToPDF" CssClass ="btn" TabIndex="15"  SkinID ="sknExportToPDF" runat ="server" ToolTip ="Export To PDF" OnClick ="btnExportToPDF_Click" />
                            <asp:ImageButton ID="btnExportToWord" CssClass ="btn" TabIndex="16"  SkinID ="sknExportToWord" runat ="server" ToolTip ="Export To Word" OnClick ="btnExportToWord_Click" />
                            
                           <%--   <asp:Button ID="btnExportToPDF" runat="server" Text="Export To PDF" AlternateText="Export To PDF"
                                CssClass="btn" EnableViewState="false" OnClick="btnExportToPDF_Click" />
                            <asp:Button ID="btnExportToExcel" runat="server" Text="Export To Excel" AlternateText="Export To Excel"
                                CssClass="btn" EnableViewState="false" OnClick="btnExportToExcel_Click" />
                            <asp:Button ID="btnExportToWord" runat="server" Text="Export To Word" AlternateText="Export To Word"
                                CssClass="btn" EnableViewState="false" OnClick="btnExportToWord_Click" />--%>
                        </div>
                       
                            <%--                            <asp:CollapsiblePanelExtender ID="cpnlCandidateList" runat="server" TargetControlID="pnlCandidateListBody"
                                ExpandControlID="pnlCandidateListHeader" CollapseControlID="pnlCandidateListHeader"
                                Collapsed="false" ImageControlID="imgShowHideCandidateList" CollapsedImage="~/Images/expand_all.gif"
                                ExpandedImage="~/Images/collapse_all.gif" SuppressPostBack="false">
                            </asp:CollapsiblePanelExtender>--%>
                            <%--                            <asp:Panel ID="pnlCandidateListHeader" runat="server">
                                <div class="CommonHeaderForDashboard" style="clear: both;">
                                    <asp:Image ID="imgShowHideCandidateList" ImageUrl="~/Images/expand_all.gif" runat="server" />
                                    <asp:Label ID="lblListHeader" runat="server" Text="List of Candidates" EnableViewState="false"></asp:Label>
                                </div>
                            </asp:Panel>--%>
                            <asp:Panel ID="pnlCandidateListBody" runat="server">
                                <div class="GridContainer" style="overflow: auto;width: 100%;
                                    text-align: left;" id="bigDiv" onscroll='SetScrollPosition()'>
                                    <asp:ListView ID="lsvCandidateList" runat="server" DataKeyNames="Id" OnItemDataBound="lsvCandidateList_ItemDataBound"
                                        OnItemCommand="lsvCandidateList_ItemCommand" OnPreRender="lsvCandidateList_PreRender">
                                        <LayoutTemplate>
                                            <table id="tlbTemplate" class="Grid ReportGrid" cellspacing="0" border="0">
                                                <tr id="trHeader" runat="server">
                                                   <th id="thId" runat ="server" >
                                                       <asp:LinkButton ID="lnkCandidateID" style="min-width: 120px" runat="server" TabIndex="17" ToolTip="Sort By Candidate ID #" CommandName="Sort"
                                                            CommandArgument="[C].[ID]" Text="Candidate ID #" />
                                                   </th>
                                                    <th runat="server" id="thName" style="min-width: 100px">
                                                        <asp:LinkButton ID="lnkBtnName" runat="server" TabIndex="17" ToolTip="Sort By Name" CommandName="Sort"
                                                            CommandArgument="[C].[FirstName]" Text="Name" />
                                                    </th>
                                                    <th runat="server" id="thNickName" style="min-width: 100px">
                                                        <asp:LinkButton ID="lnkBtnNickName" runat="server" ToolTip="Sort By Nick Name" CommandName="Sort"
                                                            CommandArgument="[C].[NickName]" Text="Nick Name" />
                                                    </th>
                                                    <th runat="server" id="thPrimaryEmail" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnPrimaryEmail" runat="server" TabIndex="18" ToolTip="Sort By Primary Email"
                                                            CommandName="Sort" CommandArgument="[C].[PrimaryEmail]" Text="Primary Email" />
                                                    </th>
                                                    <th runat="server" id="thAlternateEmail" style="min-width: 130px">
                                                        <asp:LinkButton ID="lnkBtnAlternateEmail" runat="server"  ToolTip="Sort By Alternate Email"
                                                            CommandName="Sort" CommandArgument="[C].[AlternateEmail]" Text="Alternate Email" />
                                                    </th>
                                                    <th runat="server" id="thPermanentAddress" style="min-width: 150px">
                                                        <asp:LinkButton ID="lnkBtnPermanentAddress" runat="server"  ToolTip="Sort By Permanent Address"
                                                            CommandName="Sort" CommandArgument="[C].[PermanentAddressLine1]" Text="Permanent Address" /><%--0.1--%>
                                                    </th>
                                                    <th runat="server" id="thPermanentCity" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnPermanentCity" runat="server" TabIndex="19"  ToolTip="Sort By Permanent City"
                                                           CommandName="Sort" CommandArgument="[C].[CurrentCity]" Text="Permanent City" />
                                                    </th>
                                                    <th runat="server" id="thPermanentState" style="min-width: 140px">
                                                        <asp:LinkButton ID="lnkBtnPermanentState" runat="server" TabIndex="20"  ToolTip="Sort By Permanent State"
                                                            CommandName="Sort" CommandArgument="[SCU].[Name]" Text="Permanent State" />
                                                    </th>
                                                    <th runat="server" id="thPermanentZip" style="min-width: 150px">
                                                        <asp:LinkButton ID="lnkBtnPermanentZip" runat="server"  ToolTip="Sort By Permanent Zip Code"
                                                            CommandName="Sort" CommandArgument="[C].[CurrentZip]" Text="Permanent Zip Code" />
                                                    </th>
                                                    <th runat="server" id="thPermanentCountry" style="min-width: 140px">
                                                        <asp:LinkButton ID="lnkBtnPermanentCountry" runat="server"  ToolTip="Sort By Permanent Country"
                                                            CommandName="Sort" CommandArgument="[CCU].[Name]" Text="Permanent Country" />
                                                    </th>
                                                    <th runat="server" id="thPermanentPhone" style="min-width: 130px">
                                                        <asp:LinkButton ID="lnkBtnPermanentPhone" runat="server" TabIndex="21" ToolTip="Sort By Permanent Phone"
                                                            CommandName="Sort" CommandArgument="[C].[PermanentPhone]" Text="Permanent Phone" />
                                                    </th>
                                                    <th runat="server" id="thPermanentMobile" style="min-width: 130px">
                                                        <asp:LinkButton ID="lnkBtnPermanentMobile" runat="server"  ToolTip="Sort By Permanent Mobile"
                                                            CommandName="Sort" CommandArgument="[C].[PermanentMobile]" Text="Permanent Mobile" />
                                                    </th>
                                                    <th runat="server" id="thCurrentAddress" style="min-width: 140px">
                                                        <asp:LinkButton ID="lnkBtnCurrentAddress" runat="server" ToolTip="Sort By Current Address"
                                                            CommandName="Sort" CommandArgument="[C].[CurrentAddressLine1]" Text="Current Address" />
                                                    </th>
                                                    <th runat="server" id="thCurrentCity" style="min-width: 140px">
                                                        <asp:LinkButton ID="lnkBtnCurrentCity" runat="server" ToolTip="Sort By Current City"
                                                            CommandName="Sort" CommandArgument="[C].[PermanentCity]" Text="Current City" />
                                                    </th>
                                                    <th runat="server" id="thCurrentStateName" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnCurrentState" runat="server" ToolTip="Sort By Current State"
                                                            CommandName="Sort" CommandArgument="[SP].[Name]" Text="Current State" />
                                                    </th>
                                                    <th runat="server" id="thCurrentZip" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnCurrentZip" runat="server" ToolTip="Sort By Current Zip Code"
                                                            CommandName="Sort" CommandArgument="[C].[PermanentZip]" Text="Current Zip Code" />
                                                    </th>
                                                    <th runat="server" id="thCurrentCountry" style="min-width: 130px">
                                                        <asp:LinkButton ID="lnkBtnCurrentCountry" runat="server"  ToolTip="Sort By Current Country"
                                                            CommandName="Sort" CommandArgument="[CP].[Name]" Text="Current Country" />
                                                    </th>
                                                    <th runat="server" id="thPrimaryPhone" style="min-width: 130px">
                                                        <asp:LinkButton ID="lnkBtnPrimaryPhone" runat="server"  ToolTip="Sort By Primary Phone"
                                                            CommandName="Sort" CommandArgument="[C].[PrimaryPhone]" Text="Primary Phone" />
                                                    </th>
                                                    <th runat="server" id="thCellPhone" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnCellPhone" runat="server" ToolTip="Sort By Cell Phone"
                                                            CommandName="Sort" CommandArgument="[C].[CellPhone]" Text="Cell Phone" />
                                                    </th>
                                                    <th runat="server" id="thHomePhone" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnHomePhone" runat="server"  ToolTip="Sort By Home Phone"
                                                            CommandName="Sort" CommandArgument="[C].[HomePhone]" Text="Home Phone" />
                                                    </th>
                                                    <th runat="server" id="thOfficePhone" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnOfficePhone" runat="server"  ToolTip="Sort By Office Phone"
                                                            CommandName="Sort" CommandArgument="[C].[OfficePhone]" Text="Office Phone" />
                                                    </th>
                                                    <th runat="server" id="thCurrentPosition" style="min-width: 130px">
                                                        <asp:LinkButton ID="lnkBtnCurrentPosition" runat="server"  ToolTip="Sort By Current Position"
                                                            CommandName="Sort" CommandArgument="[ME].[CurrentPosition]" Text="Current Position" />
                                                    </th>
                                                    <th runat="server" id="thLastEmployer" style="min-width: 130px">
                                                        <asp:LinkButton ID="lnkBtnCurrentCompany" runat="server"   ToolTip="Sort By Current Company"
                                                            CommandName="Sort" CommandArgument="[ME].[LastEmployer]" Text="Current Company" />
                                                    </th>
                                                    <th runat="server" id="thDateOfBirth" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnDateOfBirth" runat="server" ToolTip="Sort By Date Of Birth"
                                                            CommandName="Sort" CommandArgument="[C].[DateOfBirth]" Text="Date Of Birth" />
                                                    </th>
                                                    <th runat="server" id="thCityOfBirth" style="min-width: 110px">
                                                        <asp:LinkButton ID="lnkBtnCityOfBirth" runat="server" ToolTip="Sort By City Of Birth"
                                                            CommandName="Sort" CommandArgument="[C].[CityOfBirth]" Text="City Of Birth" />
                                                    </th>
                                                    <th runat="server" id="thCountryOfBirth" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnCountryOfBirth" runat="server" ToolTip="Sort By Country Of Residence"
                                                            CommandName="Sort" CommandArgument="[C].[CountryIdOfBirth]" Text="Country Of Residence" />
                                                    </th>
                                                    <th runat="server" id="thCitizenship" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnCitizenship" runat="server" ToolTip="Sort By Citizenship"
                                                            CommandName="Sort" CommandArgument="[C].[CountryIdOfCitizenship]" Text="Citizenship" />
                                                    </th>
                                                    <th runat="server" id="thGender" style="min-width: 100px">
                                                        <asp:LinkButton ID="lnkBtnGender" runat="server" ToolTip="Sort By Gender" CommandName="Sort"
                                                            CommandArgument="[C].[GenderLookupId]" Text="Gender" />
                                                    </th>
                                                    <th runat="server" id="thMaritalStatus" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnMaritalStatus" runat="server" ToolTip="Sort By Marital Status"
                                                            CommandName="Sort" CommandArgument="[GMS].[Name]" Text="Marital Status" />
                                                    </th>
                                                    <th runat="server" id="thYearsOfExperience" style="min-width: 150px">
                                                        <asp:LinkButton ID="lnkBtnYearsOfExperience" runat="server" ToolTip="Sort By Years Of Experience"
                                                            CommandName="Sort" CommandArgument="[ME].[TotalExperienceYears]" Text="Years Of Experience" />
                                                    </th>
                                                    <th runat="server" id="thAvailability" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnAvailability" runat="server" ToolTip="Sort By Availability"
                                                            CommandName="Sort" CommandArgument="[ME].[Availability]" Text="Availability" />
                                                    </th>
                                                    <th runat="server" id="thWillingToTravel" style="min-width: 130px">
                                                        <asp:LinkButton ID="lnkBtnWillingToTravel" runat="server" ToolTip="Sort By Willing To Travel"
                                                            CommandName="Sort" CommandArgument="[ME].[WillingToTravel]" Text="Willing To Travel" />
                                                    </th>
                                                   
                                                   
                                                   
                                                    <th runat="server" id="thWorkSchedule" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkWorkSchedule" runat="server" ToolTip="Sort By Work Schedule"
                                                            CommandName="Sort" CommandArgument="[GWS].[Name]" Text="Work Schedule" />
                                                    </th>
                                                    <th runat="server" id="thCurrentYearlyRate" style="min-width: 150px">
                                                        <asp:LinkButton ID="lnkBtnCurrentYearlyRate" runat="server" ToolTip="Sort By Current Rate/Salary"
                                                            CommandName="Sort" CommandArgument="[ME].[CurrentYearlyRate]" Text="Current Rate/Salary" />
                                                    </th>
                                                    <th runat="server" id="thExpectedYearlyRate" style="min-width: 150px">
                                                        <asp:LinkButton ID="lnkBtnExpectedYearlyRate" runat="server" ToolTip="Sort By Expected Rate/Salary"
                                                            CommandName="Sort" CommandArgument="[ME].[ExpectedYearlyRate]" Text="Expected Rate/Salary" />
                                                    </th>
                                                    
                                                    <th runat="server" id="thCreator" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnCreator" runat="server" ToolTip="Sort By Creator" CommandName="Sort"
                                                            CommandArgument="[MC].[FirstName]+'' ''+[MC].[LastName]" Text="Creator" />
                                                    </th>
                                                    <th runat="server" id="thCreateDate" style="min-width: 120px">
                                                        <asp:LinkButton ID="lnkBtnCreateDate" runat="server" ToolTip="Sort By Create Date"
                                                            CommandName="Sort" CommandArgument="[C].[CreateDate]" Text="Create Date" />
                                                    </th>
                                                    <th runat="server" id="thUpdator" style="min-width: 130px">
                                                        <asp:LinkButton ID="lnkBtnLastUpdator" runat="server" ToolTip="Sort By Last Updator"
                                                            CommandName="Sort" CommandArgument="[MU].[FirstName]+'' ''+[MU].[LastName]" Text="Last Updator" />
                                                    </th>
                                                    <th runat="server" id="thUpdateDate" style="min-width: 150px">
                                                        <asp:LinkButton ID="lnkBtnLastUpdateDate" runat="server" ToolTip="Sort By Last Update Date"
                                                            CommandName="Sort" CommandArgument="[C].[UpdateDate]" Text="Last Update Date" />
                                                    </th>
                                                    
                                                    
                                                    
                                                     <th runat="server" id="thWebsite" style="min-width: 170px">
                                                        <asp:LinkButton ID="lnkBtnWebsite" runat="server" ToolTip="Sort By Website"
                                                            CommandName="Sort" CommandArgument="[ME].[Website]" Text="Website" />
                                                    </th>
                                                     <th runat="server" id="thLinkedInProfile" style="min-width: 170px">
                                                        <asp:LinkButton ID="lnkBtnLinkedInProfile" runat="server" ToolTip="Sort By LinkedIn Profile"
                                                            CommandName="Sort" CommandArgument="[ME].[LinkedinProfile]" Text="LinkedIn Profile" />
                                                    </th>
                                                    <th runat="server" id="thPassportStatus" style="min-width: 170px">
                                                        <asp:LinkButton ID="lnkBtnPassportStatus" runat="server" ToolTip="Sort By Passport Status"
                                                            CommandName="Sort" CommandArgument="[ME].[PassportStatus]" Text="Passport Status" />
                                                    </th>
                                                     <th runat="server" id="thIDProof" style="min-width: 170px">
                                                        <asp:Label ID="lnkBtnIDProof" runat="server" ToolTip="Sort By ID Proof"
                                                            CommandName="Sort" CommandArgument="[ME].[IdCardLookUpId]" Text="ID Proof" />
                                                       <%--     <asp:LinkButton ID="lnkBtnIDProof" runat="server" ToolTip="Sort By ID Proof"
                                                            CommandName="Sort" CommandArgument="[GID].[Name]+''''+[ME].[IdCardDetail]" Text="ID Proof" />--%>
                                                    </th>
                                                    <th runat="server" id="thHighestEducation" style="min-width: 170px">
                                                        <%--<asp:LinkButton ID="lnkBtnHighestEducation" runat="server" ToolTip="Sort By Highest Education"
                                                            CommandName="Sort" CommandArgument="[MED].LevelOfEducationLookupId" Text="Highest Education" />--%>
                                                            <asp:Label ID="lblHighestEducation" runat="server" Text="Highest Education" />
                                                    </th>
                                                     <th runat="server" id="thSource" style="min-width: 170px">
                                                        <asp:LinkButton ID="lnkBtnSource" runat="server" ToolTip="Sort By Source"
                                                            CommandName="Sort" CommandArgument="[GS].[Name]" Text="Source" />
                                                    </th>
                                                      <th runat="server" id="thSourceDescription" style="min-width: 170px">
                                                        <asp:Label ID="lnkBtnSourceDescription" runat="server" ToolTip="Sort By Source Description"
                                                            CommandName="Sort" Text="Source Description" />
                                                    </th>
                                                     <th runat="server" id="thReqCode" style="min-width: 170px">
                                                         <asp:Label ID="lblReqCode" runat="server" Text="Req. Code" />
                                                    </th> 
                                                     <th runat="server" id="thHiringStatus" style="min-width: 170px">
                                                         <asp:Label ID="lblHiringStatus" runat="server" Text="Hiring Status" />
                                                    </th>                                                   
                                                         
                                                 </tr>
                                                <tr id="itemPlaceholder" runat="server">
                                                </tr>
                                                <tr class="Pager">
                                                    <td id="tdpager" runat="server">
                                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                        <EmptyDataTemplate>
                                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                                <tr>
                                                    <td>
                                                        No Candidate found.
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                        <ItemTemplate>
                                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                               <td id="tdId" runat ="server" >
                                                <asp:Label ID="lblCandidateID" runat ="server" ></asp:Label>
                                               </td>
                                                <td runat="server" id="tdName" style="text-align: left;">
                                                    <asp:HyperLink ID="lnkCandidateName"  runat="server"></asp:HyperLink>
                                                </td>
                                                <td runat="server" id="tdNickName">
                                                    <asp:Label ID="lblNickName" runat="server" />
                                                </td>
                                                <td runat="server" id="tdPrimaryEmail">
                                                    <asp:Label ID="lblPrimaryEmail" runat="server" />
                                                </td>
                                                <td runat="server" id="tdAlternateEmail">
                                                    <asp:Label ID="lblAlternateEmail" runat="server" />
                                                </td>
                                                <td runat="server" id="tdPermanentAddress">
                                                    <asp:Label ID="lblPermanentAddress" runat="server" />
                                                </td>
                                                <td runat="server" id="tdPermanentCity">
                                                    <asp:Label ID="lblPermanentCity" runat="server" />
                                                </td>
                                                <td runat="server" id="tdPermanentState">
                                                    <asp:Label ID="lblPermanentState" runat="server" />
                                                </td>
                                                <td runat="server" id="tdPermanentZip">
                                                    <asp:Label ID="lblPermanentZip" runat="server" />
                                                </td>
                                                <td runat="server" id="tdPermanentCountry">
                                                    <asp:Label ID="lblPermanentCountry" runat="server" />
                                                </td>
                                                <td runat="server" id="tdPermanentPhone">
                                                    <asp:Label ID="lblPermanentPhone" runat="server" />
                                                </td>
                                                <td runat="server" id="tdPermanentMobile">
                                                    <asp:Label ID="lblPermanentMobile" runat="server" />
                                                </td>
                                                <td runat="server" id="tdCurrentAddress">
                                                    <asp:Label ID="lblCurrentAddress" runat="server" />
                                                </td>
                                                <td runat="server" id="tdCurrentCity">
                                                    <asp:Label ID="lblCurrentCity" runat="server" />
                                                </td>
                                                <td runat="server" id="tdCurrentStateName">
                                                    <asp:Label ID="lblCurrentState" runat="server" />
                                                </td>
                                                <td runat="server" id="tdCurrentZip">
                                                    <asp:Label ID="lblCurrentZip" runat="server" />
                                                </td>
                                                <td runat="server" id="tdCurrentCountry">
                                                    <asp:Label ID="lblCurrentCountry" runat="server" />
                                                </td>
                                                <td runat="server" id="tdPrimaryPhone">
                                                    <asp:Label ID="lblPrimaryPhone" runat="server" />
                                                </td>
                                                <td runat="server" id="tdCellPhone">
                                                    <asp:Label ID="lblCellPhone" runat="server" />
                                                </td>
                                                <td runat="server" id="tdHomePhone">
                                                    <asp:Label ID="lblHomePhone" runat="server" />
                                                </td>
                                                <td runat="server" id="tdOfficePhone">
                                                    <asp:Label ID="lblOfficePhone" runat="server" />
                                                </td>
                                                <td runat="server" id="tdCurrentPosition">
                                                    <asp:Label ID="lblCurrentPosition" runat="server" />
                                                </td>
                                                <td runat="server" id="tdLastEmployer">
                                                    <asp:Label ID="lblCurrentCompany" runat="server" />
                                                </td>
                                                <td runat="server" id="tdDateOfBirth">
                                                    <asp:Label ID="lblDateOfBirth" runat="server" />
                                                </td>
                                                <td runat="server" id="tdCityOfBirth">
                                                    <asp:Label ID="lblCityOfBirth" runat="server" />
                                                </td>
                                                <td runat="server" id="tdCountryOfBirth">
                                                    <asp:Label ID="lblCountryOfBirth" runat="server" />
                                                </td>
                                                <td runat="server" id="tdCitizenship">
                                                    <asp:Label ID="lblCitizenship" runat="server" />
                                                </td>
                                                <td runat="server" id="tdGender">
                                                    <asp:Label ID="lblGender" runat="server" />
                                                </td>
                                                <td runat="server" id="tdMaritalStatus">
                                                    <asp:Label ID="lblMaritalStatus" runat="server" />
                                                </td>
                                                <td runat="server" id="tdYearsOfExperience">
                                                    <asp:Label ID="lblYearsOfExperience" runat="server" />
                                                </td>
                                                <td runat="server" id="tdAvailability">
                                                    <asp:Label ID="lblAvailability" runat="server" />
                                                </td>
                                                <td runat="server" id="tdWillingToTravel">
                                                    <asp:Label ID="lblWillingToTravel" runat="server" />
                                                </td>
                                                    <td runat="server" id="tdWorkSchedule">
                                                    <asp:Label ID="lblWorkSchedule" runat="server" />
                                                </td>
                                                <td runat="server" id="tdCurrentYearlyRate">
                                                    <asp:Label ID="lblCurrentYearlyRate" runat="server" />
                                                </td>
                                                <td runat="server" id="tdExpectedYearlyRate">
                                                    <asp:Label ID="lblExpectedYearlyRate" runat="server" />
                                                </td>
                                                
                                                <td runat="server" id="tdCreator">
                                                    <asp:Label ID="lblCreator" runat="server" />
                                                </td>
                                                <td runat="server" id="tdCreateDate">
                                                    <asp:Label ID="lblCreateDate" runat="server" />
                                                </td>
                                                <td runat="server" id="tdUpdator">
                                                    <asp:Label ID="lblLastUpdator" runat="server" />
                                                </td>
                                                <td runat="server" id="tdUpdateDate">
                                                    <asp:Label ID="lblLastUpdateDate" runat="server" />
                                                </td>
                                                 
                                                    <td runat="server" id="tdWebsite">
                                                    <asp:Label ID="lblWebsite" runat="server" />
                                                </td>
                                                
                                                    <td runat="server" id="tdLinkedInProfile">
                                                    <asp:Label ID="lblLinkedInProfile" runat="server" />
                                                </td>
                                                 
                                                    <td runat="server" id="tdPassportStatus">
                                                    <asp:Label ID="lblPassportStatus" runat="server" />
                                                </td>
                                                <td runat="server" id="tdIDProof">
                                                    <asp:Label ID="lblIDProof" runat="server" />
                                                </td>
                                                 <td runat="server" id="tdHighestEducation">
                                                    <asp:Label ID="lblHighestEducation" runat="server" />
                                                </td>
                                                 <td runat="server" id="tdSource">
                                                    <asp:Label ID="lblSource" runat="server" />
                                                </td>
                                                 <td runat="server" id="tdSourceDescription">
                                                    <asp:Label ID="lblSourceDescription" runat="server" />
                                                </td>
                                                 <td runat="server" id="tdReqCode">
                                                    <asp:Label ID="lblReqCode" runat="server" />
                                                </td>
                                                 <td runat="server" id="tdHiringStatus">
                                                    <asp:Label ID="lblHiringStatus" runat="server" />
                                                </td>
                                                 </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            <asp:PostBackTrigger ControlID="btnExportToWord" />
        </Triggers>
    </asp:UpdatePanel>
