﻿<%--
----------------------------------------------------------------------------------------------------------------------------------
    FileName: Controls/RequisitionPreviewPublish.ascx
    Description: This is the user control page used for publishing requisition.
    Created By: 
    Created On:
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date            Author          Modification
    -----------------------------------------------------------------------------------------------------------------------------------
    0.1             24-Apr-2009     Jagadish        Defect id: 10394; Added UpdateProgress control.
    0.2             05-Jan-2010     Srividya s      Defect id: 10903; Look and feel of 'Preview' and 'Publish Now' buttons made consistent  
    0.3             2/Feb/2016      pravin khot                id=chkCandidatePortal change the visible=false
---------------------------------------------------------------------------------------------------------------------------------------
--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequisitionPreviewPublish.ascx.cs"
    Inherits="TPS360.Web.UI.cltRequisitionPreviewPublish" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/AgentSearchTermEditor.ascx" TagName="SearchTerm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ModalTemplate.ascx" TagName="Template" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/EmailTemplateEditor.ascx" TagName="EmailEditor" TagPrefix="ucl" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.LayoutControls" TagPrefix="ig" %>

<script type="text/javascript" language="javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq); 

function PageLoadedReq(sender,args)
{
AvailabilityChanged ();
HideEndDate ();
ShowAndHide ();
ShowAndHide();
ShowAndHideVendor();
try{
    
 var backgroundmpeSearch = $find("mpeSearch")._backgroundElement;
            backgroundmpeSearch.onclick = function() {  $find("mpeSearch").hide();}
            
                
 var backgroundmpeEmail = $find("mpeEmail")._backgroundElement;
            backgroundmpeEmail.onclick = function() {  $find("mpeEmail").hide();}
            
            
}
catch(e)
{
}
}
    function AvailabilityChanged()
    {
        var rdbNow= document .getElementById ('<%= rdbNow.ClientID %>');
        var wdcStartDate=document .getElementById ('<%= wdcStartDate.ClientID %>');
  var ddlStartDateEmpty=document .getElementById ('<%= ddlStartDateEmpty.ClientID %>');

        var ddlTime=document .getElementById ('<%= ddlTime.ClientID %>');
    
        ddlTime.disabled= wdcStartDate.disabled=ddlStartDateEmpty.disabled=rdbNow .checked ;
        wdcStartDate.style .display =rdbNow .checked ? "none": "";
        ddlStartDateEmpty.style .display =rdbNow .checked ? "": "none";
         var myVal = document.getElementById('<%=cvUpdateDate.ClientID %>');
  ValidatorEnable(myVal, false); 
  
    }
    function HideEndDate()
    {
        var rdbrunonce=document .getElementById ('<%=rdbRunOnce.ClientID %>');
        var wdcEndDate=document .getElementById ('<%=wdcEndDate.ClientID %>');
        var ddlEndDate=document .getElementById ('<%=ddlenddate.ClientID %>');
        wdcEndDate .disabled=ddlEndDate .disabled=rdbrunonce .checked;
        wdcEndDate .style.display=rdbrunonce .checked? "none":"";
        ddlEndDate .style.display=rdbrunonce .checked ? "" : "none"; 
       
         var myVal = document.getElementById('<%=cvenddate.ClientID %>');
        ValidatorEnable(myVal, !rdbrunonce .checked);

    }
    function ShowAndHide()
    {
        var chkbox=document .getElementById ('<%=chkEmailMatchingCandidates.ClientID %>');
        var div=document .getElementById ('<%=dvHiddenMatchingDetails.ClientID%>');
       
        if(chkbox !=null && div !=null )
        {
            if(chkbox .checked)
            {
                div.style.display="";
            }
            else 
            {
                div .style.display="none";
            }
        }
    }
     function ShowAndHideVendor()
       {
        
               var chkDisplayVendor=document .getElementById ('<%=chkVendorPortal.ClientID %>');
               var divVendor=document .getElementById ('<%=divVendor.ClientID %>');
           
               if(chkDisplayVendor != null && divVendor != null)
               {
                   if(chkDisplayVendor.checked)
                   {
                       divVendor.style.display="";
                   }
                   else
                   {
                       divVendor.style.display="none";
                   }
               }
              
       }
 
  

</script>

<div>
    <ajaxToolkit:ModalPopupExtender ID="mpeSearchTerm" BehaviorID="mpeSearch" runat="server"
        BackgroundCssClass="DarkModalBackground" DropShadow="false" PopupControlID="pnlSearchTerm"
        TargetControlID="lblTarget">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Panel ID="pnlSearchTerm" runat="server" Style="display: none;">
        <asp:Label ID="lblTarget" runat="server"></asp:Label>
        <ucl:Template ID="uclTemplateSearchTerm" runat="server" ModalTitle="Edit Agent Search Terms"
            ContentDisplay="table-cell" ModalBehaviourId="mpeSearch" ContentHeight="500px"
            ContentWidth="800px">
            <Contents>
                <ucl:SearchTerm ID="uclSearchTerm" runat="server" />
            </Contents>
        </ucl:Template>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="mpeEmailEditor" BehaviorID="mpeEmail" runat="server"
        BackgroundCssClass="DarkModalBackground" DropShadow="false" PopupControlID="pnlEmailEditor"
        TargetControlID="lblEmailTarget">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Panel ID="pnlEmailEditor" runat="server" Style="display: none;">
        <asp:Label ID="lblEmailTarget" runat="server"></asp:Label>
        <ucl:Template ID="uclTemplateEmailEditor" ModalTitle="Edit Email Template" runat="server"
            ContentDisplay="table-cell" ModalBehaviourId="mpeEmail" ContentHeight="500px"
            ContentWidth="800px">
            <Contents>
                <ucl:EmailEditor ID="uclEmailEditor" runat="server" />
            </Contents>
        </ucl:Template>
    </asp:Panel>
    <asp:UpdatePanel ID="updatepanelbuttons" runat="server">
        <ContentTemplate>
            <div id="dv" runat="server" visible="false">
                <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdnScheduleId" runat="server" />
    <div class="TableRow" style="text-align: left">
        <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="TableRow">
        <div style="width: 30%; float: left;">
            &nbsp;</div>
        <div style="text-align: left; font-size: 11px; color: #444444; font-weight: bold;
            white-space: nowrap; vertical-align: middle; width: 42%; height: 20px; padding: 5px 5px 2px 3px;">
            <asp:Label EnableViewState="false" ID="lblPreviewPublishNotice" runat="server" Text="1. Click ''Preview Requisition'' to see a Preview of the Requisition before Publishing."></asp:Label>
        </div>
    </div>
    <div class="TableRow">
        <div style="width: 30%; float: left;">
            &nbsp;</div>
        <div style="text-align: left; font-size: 11px; color: #444444; font-weight: bold;
            white-space: nowrap; vertical-align: middle; width: 42%; height: 20px; padding: 5px 5px 2px 3px;">
            <asp:Label EnableViewState="false" ID="lblPublishOptions" runat="server" Text="2. Select desired Publish Options."></asp:Label>
        </div>
    </div>
    <div class="TableRow">
        <div style="width: 30%; float: left;">
            &nbsp;</div>
        <div style="text-align: left; font-size: 11px; color: #444444; font-weight: bold;
            white-space: nowrap; vertical-align: middle; width: 42%; height: 20px; padding: 5px 5px 2px 3px;">
            <asp:Label EnableViewState="false" ID="lblPublishNotice" runat="server" Text="3. When ready, click ''Publish Requisition'' to Publish the Requisition."></asp:Label>
        </div>
    </div>
    <div class="TableRow">
        <div style="width: 30%; float: left;">
            &nbsp;</div>
        <div style="font-weight: normal; margin-top: 20px;">
            <table>
                <tr>
                    <td style="vertical-align: top; width: 15%;">
                        <strong>Publish Options:</strong>
                    </td>
                    <td align="left" style="width: 70%;">
                        <asp:CheckBox ID="chkSendEmail" runat="server" Checked="true" Text="Send Notification Email to Assigned Recruiters"
                            TabIndex="38" /><br />
                        <asp:CheckBox ID="chkFindMatchingCandidates" runat="server" Text="Find Matching Candidates Now"
                            TabIndex="39" />
                        <br />
                        <asp:CheckBox ID="chkAllowToChangeStatus" runat="server" Text="Allow Assigned Recruiters to Change Requisition Status<br/>"
                            TabIndex="40" />
                          
                           <%--************code change by pravin khot on 1/Feb/2016 change by remove[Visible="false"] ***********--%>
                        <asp:CheckBox ID="chkCandidatePortal" runat="server" Text="Display Requisition in Candidate Portal <br />"
                            TabIndex="41"  />
                           <%-- *************************************End****************************************--%>
                            
                        <%-- ToolTip="Registered candidates will be able to view and apply to the job from the Candidate Portal"/>--%>
                        <asp:CheckBox ID="chkVendorPortal" runat="server" TabIndex="42" Text="Display Requisition in Vendor Portal <br />" onclick="ShowAndHideVendor()" />
                        <asp:CheckBox ID="chkShowinEmployeeReferal" runat="server" Text="Display Requisition in Employee Referral Portal<br/>"
                            ToolTip="Publish requisition to Employee Referral Portal" TabIndex="43" />
                        <asp:CheckBox ID="chkEmailMatchingCandidates" runat="server" Text="Automatically Email Matching Candidates"
                            onclick="ShowAndHide()" TabIndex="44" Visible="false" />
                        <%--//Label--%>
                        <div id="dvHiddenMatchingDetails" runat="server" style="padding-top: 10px;">
                            <div id="dvSaveandpublish" runat="server" style="display: none; text-align: left;
                                font-size: 11px; color: #444444; font-weight: bold; white-space: nowrap; vertical-align: middle;
                                width: 42%; height: 20px;">
                                <asp:Label EnableViewState="True" ID="lblshowrepulish" Text=" You must publish the requisition or save as a draft before editing email agent settings."
                                    runat="server"></asp:Label>
                                <%--<asp:LinkButton ID="lnkSaveandReload" runat ="server" Text ="Save and reload" Enabled ="true"  OnClick ="lnkSaveAndReload_Click" ></asp:LinkButton>--%>
                            </div>
                            <div id="dvemailMatching" runat="server" style="display: none;">
                                These search fields will be used when the email agent runs.
                                <div id="dvHiddenDetails" runat="server" style="padding-top: 10px; padding-bottom: 10px;">
                                </div>
                                <asp:LinkButton ID="lnkEditMatchingDetails" runat="server" Text="Edit Search Terms"
                                    OnClick="lnkEditMatchingDetails_Click"></asp:LinkButton>
                                <div class="TableRow" style="padding-top: 20px;">
                                    The email will have this format.
                                    <asp:LinkButton ID="lnkEditEmailTemplate" runat="server" Text="View/Edit Email Template"
                                        OnClick="lnkEditEmailTemplate_Click"></asp:LinkButton>
                                </div>
                                <div class="TableRow" style="padding-top: 20px;">
                                    The email agent will run on this schedule:
                                </div>
                                <div class="TableRow" style="padding-top: 10px;">
                                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                                        <tr style="width: 100%">
                                            <td style="vertical-align: top; width: 18%;">
                                                <strong>Start Date:</strong>
                                            </td>
                                            <td style="vertical-align: top; width: 20%;">
                                                <asp:RadioButton ID="rdbNow" runat="server" Text="Now" GroupName="Availability" Checked="true"
                                                    onclick="javascript:AvailabilityChanged()" />
                                            </td>
                                            <td style="vertical-align: top; width: 32%;">
                                                <div style="float: left;">
                                                    <asp:RadioButton ID="rdbDate" runat="server" GroupName="Availability" onclick="javascript:AvailabilityChanged()" /></div>
                                                <%--<igsch:WebDateChooser ID="wdcStartDate" runat="server" Editable="false" CalendarLayout-HideOtherMonthDays="true"
                                                    NullDateLabel="" Width="90px">
                                                </igsch:WebDateChooser>--%>
                                                <ig:WebDatePicker ID="wdcStartDate" DropDownCalendarID="ddcStartDate" runat="server">
                                                </ig:WebDatePicker>
                                                <ig:WebMonthCalendar ID="ddcStartDate" AllowNull="true" runat="server">
                                                </ig:WebMonthCalendar>
                                                <asp:DropDownList ID="ddlStartDateEmpty" runat="server" Width="95px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="vertical-align: top; width: 20%;">
                                                <asp:DropDownList ID="ddlTime" runat="server" Width="100%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <strong>Repeat:</strong>
                                            </td>
                                            <td style="vertical-align: top; width: 25%;">
                                                <asp:RadioButton ID="rdbRunOnce" runat="server" Text="Run Once" GroupName="Repeat"
                                                    Checked="true" onclick="javascript:HideEndDate()" />
                                            </td>
                                            <td style="vertical-align: top; width: 25%;" colspan="2">
                                                <asp:RadioButton ID="rdbDaily" runat="server" Text="Daily" GroupName="Repeat" onclick="javascript:HideEndDate()" />
                                                <asp:RadioButton ID="rdbWeekly" runat="server" Text="Weekly" GroupName="Repeat" onclick="javascript:HideEndDate()" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <strong>End Date:</strong>
                                            </td>
                                            <td colspan="3">
                                                <%--<igsch:WebDateChooser ID="wdcEndDate" runat="server" Editable="True" CalendarLayout-HideOtherMonthDays="true"
                                                    NullDateLabel="" Width="90px">
                                                </igsch:WebDateChooser>--%>
                                                <ig:WebDatePicker ID="wdcEndDate" DropDownCalendarID="ddcEndDate" runat="server">
                                                </ig:WebDatePicker>
                                                <ig:WebMonthCalendar ID="ddcEndDate" AllowNull="true" runat="server">
                                                </ig:WebMonthCalendar>
                                                <asp:DropDownList ID="ddlenddate" runat="server" Width="95px" Enabled="false">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <div class="TableRow">
                                                    <asp:CompareValidator ID="cvUpdateDate" runat="server" ControlToValidate="wdcStartDate"
                                                        Display="Dynamic" ErrorMessage="Start date should be greater than or equal to current date.<br/>"
                                                        Operator="GreaterThanEqual" ValidationGroup="Valpublish" Type="Date"></asp:CompareValidator>
                                                    <asp:CompareValidator ID="cvenddate" runat="server" ControlToCompare="wdcStartDate"
                                                        ControlToValidate="wdcEndDate" Display="Dynamic" ErrorMessage="Start date should be less than or equal to end date."
                                                        Operator="GreaterThanEqual" ValidationGroup="Valpublish" Type="Date"></asp:CompareValidator>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td style="width: 20%;">
                    </td>
                </tr>
            </table>
        </div>
    </div>
     <div id="divVendor" runat="server" >
       <div class="TableRow">
           <div class="TableFormLeble">
               <strong>Publish to Selected Vendors:</strong>
           </div>
           <%--<div style="text-align: left; font-size: 11px; color: #444444; font-weight: bold;
           white-space: nowrap; vertical-align: middle; width: 42%; height: 20px; padding: 5px 5px 2px 3px;">
           <ucl:VendorList ID="uncVendorList" runat="server" />
           <asp:HiddenField ID="hdnSelectedListItems" runat ="server" EnableViewState ="false"  />
            <ucl:VendorList ID="uncVendorList" runat="server" />
       </div>--%>
           <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333; width: 42%; height: 100px; overflow: auto">
               <asp:CheckBoxList ID="chkVendorList" runat="server" AutoPostBack="false" abIndex="5">
               </asp:CheckBoxList>
           </div>
       </div>
   </div>
    <br />
</div>
