﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:BucketViewGrid.ascx.cs
    Description: This is the user control page used to disply the list of candidate in grid
    Created By: Sumit Sonawane
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
   
-------------------------------------------------------------------------------------------------------------------------------------------       

>*/


using System;
using System.Web.UI.WebControls;
using System.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Collections.Generic;
using System.Web.UI;
using System.Collections;
using System.Globalization;
using System.Net.Mail;
using System.Net;
using System.Threading;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace TPS360.Web.UI
{
    public partial class BucketViewGrid : BaseControl
    {
        #region Member Variables

        private static bool _mailsetting = false;
        private static bool _isMyList = false;
        private bool _IsAccessToEditCandidate = false;
        private static bool _isAddToMyList = false;
        private bool _IsAccessToEmployee = true;
        private static string UrlForEmployee = string.Empty;
        private static int IdForSitemap = 0;
        private string currentSiteMapId = "0";
        private string _JobPostingId = ""; //0123456789
        private int _MatixLevels = 0; //0123456789
        private string CurrentJobPostingSkillNames = "";
        JobPosting _jobPosting;
        private bool isAccess = false;
        public string BucketType = "Master";
        #endregion

        #region Properties

        public int CurrentJobPostingId
        {
            get
            {
                int id = 0;

                int.TryParse(StringHelper.Convert(ViewState["JobPostingId"]), out id);

                if (id == 0)
                {
                    int.TryParse(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID], out id);

                }

                return id;
            }
        }

        public JobPosting CurrentJobPosting
        {
            get
            {
                if (_jobPosting == null)
                {
                    if (CurrentJobPostingId > 0)
                    {
                        _jobPosting = Facade.GetJobPostingById(CurrentJobPostingId);
                    }

                    if (_jobPosting == null)
                    {
                        _jobPosting = new JobPosting();
                    }
                }

                return _jobPosting;
            }
            set
            {
                _jobPosting = value;
            }
        }


        public String HeaderTitle
        {
            set { }
        }

        public bool IsMyList
        {
            set { _isMyList = value; }
        }

        public int WorkFlowId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_WorkFlowId"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_WorkFlowId"] = value;
            }
        }

        public bool IsSendAlertByEmail
        {
            get
            {
                return (bool)(ViewState[this.ClientID + "_IsSendAlertByEmail"] ?? false);
            }
            set
            {
                ViewState[this.ClientID + "_IsSendAlertByEmail"] = value;
            }
        }


        public bool isMainApplication
        {
            get
            {
                switch (ApplicationSource)
                {
                    case ApplicationSource.LandTApplication:
                        return false;
                        break;
                    case ApplicationSource.MainApplication:
                        return true;
                        break;
                    default:
                        return true;
                        break;
                }
                return true;
            }
        }


        #endregion


        #region Events

        #region Page Events

        public void Page_Init(object o, EventArgs e)
        {
           
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //System.Threading.Thread.Sleep(5000);
            if (hdnDoPostPack.Value == "1")
            {
                this.lsvGridCandidate.DataBind();
                hdnDoPostPack.Value = "0";
                return;
            }

            if (Page.Request.Url.AbsoluteUri.ToString().Contains("MasterBucketView.aspx")) { BucketType = "Master"; }
            else { BucketType = "My"; }

            string ll = Session["Status"].ToString();
            string kk = Session["Matrix"].ToString();

            if (ll == "PageLoad")
            {
                getAllForpageLoad();
            }

            else if (ll == "PageLoadAllReq")
            {
                getAllForpageLoadWithAllReq();
            }

            else
            {                
                int JobPostId = int.Parse(Session["1"].ToString());               
                int SelStep = int.Parse(Session["2"].ToString());
                int memID = int.Parse(Session["MemberID"].ToString());
                string JobCreator = "0";
                if (BucketType == "My")
                {
                    JobCreator = memID.ToString();
                }

                if (!string.IsNullOrEmpty(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
                {
                    _JobPostingId = Helper.Url.SecureUrl[UrlConstants.PARAM_ID];
                }

                if (JobCreator != "0")
                {
                    odsHiringMatrix.SelectParameters["JobPostingCreatorID"].DefaultValue = JobCreator.ToString();// _JobPostingCreatorID.ToString();
                    //odsHiringMatrix.SelectParameters["MemberCreatorID"].DefaultValue = CurrentMember.Id.ToString();
                }
                if (JobPostId != 0)
                {
                    odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = JobPostId.ToString();// _JobPostingId.ToString();
                }
                odsHiringMatrix.SelectParameters["SelectionStepLookupId"].DefaultValue = SelStep.ToString();// Session["2"].ToString();// "52";
                try
                {
                    lsvGridCandidate.Items.Clear();
                    lsvGridCandidate.DataSourceID = "odsHiringMatrix";
                    this.lsvGridCandidate.DataBind();
                   
                  
                } 
                catch { }
                //HiringMatrixDataSource c = new HiringMatrixDataSource();
                //int l1 = c.GetListCount();
                try
                {
                    int CandiCount = Facade.getAllCandiCountFronView(JobCreator,JobPostId,SelStep);
                    HiringMatrixLevels hhhh = Facade.GetHiringMatrixLevelsById(SelStep);
                    Label1.Text = hhhh.Name;
                    int countVal = Facade.getAllCandiCountFronView(JobCreator, JobPostId, SelStep);
                    //Label2.Text = "(" + (lsvGridCandidate.Items.Count).ToString() + ")";
                    Label2.Text = "(" + countVal.ToString() + ")";
                    //IList<Candidate> getAll = Facade.GetAllCandidateByMemberIdReqIdSelectionStep(CurrentMember.Id, JobPostId, SelStep);
                    //Label2.Text = "(" + getAll.Count.ToString() + ")";
                    
                }
                catch { }
                //Session.Remove("1");
                //Session.Remove("2");
                //Session.Remove("MemberID");
                //Session["1"] = null;
                //Session["MemberID"] = null;
                //Session["2"] = null;
                //lsvGridCandidate.DataSource = c.GetPagedBucket(JobPostId.ToString(), SelStep.ToString(), "", 0, 10000);
                // lsvGridCandidate.DataBind();
            }
           
        }

        private void getAllForpageLoad()
        {
            //lsvGridCandidate.Controls.Clear();
            int cnt = 0;
            IList<HiringMatrixLevels> levels = new List<HiringMatrixLevels>();
            try
            {
                levels = Facade.GetAllHiringMatrixLevels();
                cnt = levels.Count; //take matrix level count,id from database
            }
            catch { }

            int memID = int.Parse(Session["MemberID"].ToString());
            Session["1"] = "0";
            string valSess = Session["Matrix"].ToString();
            string JobPostId = "0";
            string JobCreator = "0";
            //int SelStep = int.Parse(Session["2"].ToString());
            if (BucketType == "My")
            {
                JobCreator = memID.ToString();
            }
            //if(Session["2"].ToString() != "0")
            //{
            //}
            int SelStep = 0;
            int j=0;
            //lsvGridCandidate.Items.Clear();
            for (int i = 0; i <= cnt - 1; i++)
            {   
             
                if(int.Parse(Session["Matrix"].ToString()) == i)
                {
                    SelStep = levels[i].Id;
                    j=i+1;
               
                //int memID = int.Parse(Session["MemberID"].ToString());
                //int SelStep = levels[i].Id;
                //int SelStep = 46;
                if (!string.IsNullOrEmpty(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
                {
                    _JobPostingId = Helper.Url.SecureUrl[UrlConstants.PARAM_ID];
                }

                //if (JobPostId != 0)
                //{
                //    odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = JobPostId.ToString();// _JobPostingId.ToString();
                //}
                odsHiringMatrix.SelectParameters["JobPostingCreatorID"].DefaultValue = JobCreator.ToString();// _JobPostingCreatorID.ToString();
                odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = JobPostId.ToString();// _JobPostingId.ToString();
                odsHiringMatrix.SelectParameters["SelectionStepLookupId"].DefaultValue = SelStep.ToString();// Session["2"].ToString();// "52";
                //odsHiringMatrix.SelectParameters["MemberCreatorID"].DefaultValue = CurrentMember.Id.ToString();
                //odsHiringMatrix.FilterParameters["SelectionStepLookupId"] = SelStep.ToString();// Session["2"].ToString();// "52";
                
                try
                {
                    lsvGridCandidate.DataSourceID = "odsHiringMatrix";
                    this.lsvGridCandidate.DataBind();
                }
                catch { }
                HiringMatrixDataSource c = new HiringMatrixDataSource();
                try
                {
                    HiringMatrixLevels hhhh = Facade.GetHiringMatrixLevelsById(SelStep);
                    Label1.Text = hhhh.Name;
                    Label2.Text = "(" + (lsvGridCandidate.Items.Count).ToString() + ")";
                    //IList<Candidate> getAll = Facade.GetAllCandidateByMemberIdReqIdSelectionStep(CurrentMember.Id, JobPostId, SelStep);
                    //Label2.Text = "(" + getAll.Count.ToString() + ")";

                }
                catch { }
            }
            }

            Session["Matrix"] = j;
        }

        private void getAllForpageLoadWithAllReq()
        {
            int JobPostId = 0;
            int memID = int.Parse(Session["MemberID"].ToString());
            int SelStep = int.Parse(Session["2"].ToString());
            string JobCreator = "0";
            if (BucketType == "My")
            {
                JobCreator = memID.ToString();
            }

            if (!string.IsNullOrEmpty(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
            {
                _JobPostingId = Helper.Url.SecureUrl[UrlConstants.PARAM_ID];
            }

            if (JobCreator != "0")
            {
                odsHiringMatrix.SelectParameters["JobPostingCreatorID"].DefaultValue = JobCreator.ToString();// _JobPostingCreatorID.ToString();
                //odsHiringMatrix.SelectParameters["MemberCreatorID"].DefaultValue = CurrentMember.Id.ToString();
            }
            //if (JobPostId != 0)
            //{
            //    odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = JobPostId.ToString();// _JobPostingId.ToString();
            //}
            odsHiringMatrix.SelectParameters["JobPostingId"].DefaultValue = JobPostId.ToString();// _JobPostingId.ToString();
            odsHiringMatrix.SelectParameters["SelectionStepLookupId"].DefaultValue = SelStep.ToString();// Session["2"].ToString();// "52";
            try
            {
                lsvGridCandidate.Items.Clear();
                lsvGridCandidate.DataSourceID = "odsHiringMatrix";
                this.lsvGridCandidate.DataBind();
            }
            catch { }
            //HiringMatrixDataSource c = new HiringMatrixDataSource();
            //int l1 = c.GetListCount();
            try
            {
                int CandiCount = Facade.getAllCandiCountFronView(JobCreator, JobPostId, SelStep);
                HiringMatrixLevels hhhh = Facade.GetHiringMatrixLevelsById(SelStep);
                Label1.Text = hhhh.Name;
                Label2.Text = "(" + (lsvGridCandidate.Items.Count).ToString() + ")";
                //int countVal = Facade.getAllCandiCountFronView(JobCreator, JobPostId, SelStep);
                //Label2.Text = "(" + countVal.ToString() + ")";
                //IList<Candidate> getAll = Facade.GetAllCandidateByMemberIdReqIdSelectionStep(CurrentMember.Id, JobPostId, SelStep);
                //Label2.Text = "(" + getAll.Count.ToString() + ")";

            }
            catch { }
        }
     
        #endregion

        

     
        #endregion

        protected void lsvGridCandidate_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
               // Candidate candidatInfo = ((ListViewDataItem)e.Item).DataItem as Candidate;
                int JobPostId = int.Parse(Session["1"].ToString());
                HiringMatrix candidatInfo = ((ListViewDataItem)e.Item).DataItem as HiringMatrix;

                if (JobPostId == 0)
                {
                    JobPostId = candidatInfo.JobPostingID;
                }

                if (candidatInfo != null)
                {
                    HyperLink lnklblCandiName = (HyperLink)e.Item.FindControl("lnklblCandiName");
                    //Label lblCandiName = (Label)e.Item.FindControl("lblCandiName");
                    Label lblCandiEmail = (Label)e.Item.FindControl("lblCandiEmail");
                    Label lblCandiMobile = (Label)e.Item.FindControl("lblCandiMobile");
                    Label lblCandiLocation = (Label)e.Item.FindControl("lblCandiLocation");
                    Label lblCandiExperience = (Label)e.Item.FindControl("lblCandiExperience");
                    Label lblCandiRemarks = (Label)e.Item.FindControl("lblCandiRemarks");
                    Label lblCandiSkill = (Label)e.Item.FindControl("lblCandiSkill");
                    Label lblCandiSource = (Label)e.Item.FindControl("lblCandiSource");
                    Label lblCandiEducation = (Label)e.Item.FindControl("lblCandiEducation");

                    ImageButton imgbtnResume = (ImageButton)e.Item.FindControl("imgbtnResume");
                    ImageButton imgbtnNotes = (ImageButton)e.Item.FindControl("imgbtnNotes");
                    ImageButton imgbtnCandEmail = (ImageButton)e.Item.FindControl("imgbtnCandEmail");
                    ImageButton imgbtnFeedbackEmail = (ImageButton)e.Item.FindControl("imgbtnFeedbackEmail");
                    //ImageButton imgbtnSafeHarbour = (ImageButton)e.Item.FindControl("imgbtnSafeHarbour");
              
                    //ImageButton1.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobPostingTeam.aspx?JID=" + candidatInfo.Id + "','700px','570px'); return false;"
                    //ImageButton1.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobPostingTeam.aspx?JID=214" + jobPosting.Id + "','700px','570px'); return false;"; ;
                    //imgbtnFeedbackEmail.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobPostingTeam.aspx?JID=214','700px','570px'); return false;";

                    string CandiName = candidatInfo.FirstName + " " + candidatInfo.LastName;
                    imgbtnResume.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/ResumeDownload.aspx?mid=" + candidatInfo.MemberId + "','790px','570px'); return false;";
                    imgbtnCandEmail.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/EmailEditorPop.aspx?meid=" + candidatInfo.PrimaryEmail + "&mid=" + candidatInfo.MemberId + "','790px','570px'); return false;";
                    imgbtnFeedbackEmail.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/ScheduleInterview.aspx?mid=" + candidatInfo.MemberId + "&JID=" + JobPostId + "','700px','570px'); return false;";
                    imgbtnNotes.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/CandidateNotes.aspx?mid=" + candidatInfo.MemberId + "','880px','570px'); return false;";
                    //imgbtnSafeHarbour.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/CandidateBuilder.aspx?mid=" + candidatInfo.MemberId + " ','800px','700px'); return false;";
                    ControlHelper.SetHyperLink(lnklblCandiName, UrlConstants.Candidate.OVERVIEW, string.Empty, CandiName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(candidatInfo.MemberId), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.OVERVIEW_SITEMAP_ID, UrlConstants.PARAM_SITEMAP_PARENT_ID, UrlConstants.Candidate.OVERVIEW_SITEMAP_PARENTID, UrlConstants.PARAM_JOB_ID, CurrentJobPostingId.ToString());  //1.2
                    
                    
                    //lsvGridCandidate.DataBind();
                   // imgbtnCandEmail.Attributes.Add("onclick", "window.open('Modals/EmailEditorPop.aspx?FileID=" & strFileID & "', 'newwindow','toolbar=no,location=no,menubar=no,width=800,height=600,resizable=no,scrollbars=yes');return false;")
          
                
                }
         
            }
        }
        protected void lsvGridCandidate_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
        
        }
        protected void lsvGridCandidate_PreRender(object sender, EventArgs e)
        {

        }
}
}