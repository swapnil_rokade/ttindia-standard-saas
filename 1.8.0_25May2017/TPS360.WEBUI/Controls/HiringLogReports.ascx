﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:HiringLogReports.ascx
    Description By:This is user control common for HiringLogReports.aspx and MyHiringLogReports.aspx 
    Created By:Sumit Sonawane
    Created On:25/May/2016
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------

    ---------------------------------------------------------------------------------------------------------------------------------------     

--%>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HiringLogReports.ascx.cs"
    Inherits="TPS360.Web.UI.ControlHiringLogReport" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName = "DateRangePicker" TagPrefix ="ucl" %>

    <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

    <script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

    <script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>
    
    <script src="../js/EmployeeTeam.js" type="text/javascript"></script>
    
    <script type="text/javascript" language="javascript">
        var ModalProgress = '<%= Modal.ClientID %>';
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
        function PageLoadedReq(sender, args) {
            try {
                var hdnScroll = document.getElementById('<%=hdnScrollPos.ClientID%>');
                var bigDiv = document.getElementById('ctl00_cphHomeMaster_divlsvInterview');
                bigDiv.scrollLeft = hdnScroll.value;
            }
            catch (e) {
            }
        }
        function SetScrollPosition() {
            var hdnScroll = document.getElementById('<%=hdnScrollPos.ClientID%>');
            var bigDiv = document.getElementById('ctl00_cphHomeMaster_divlsvInterview');
            hdnScroll.value = bigDiv.scrollLeft;
        }
                
    </script>

    <asp:TextBox ID="hdnSortColumn" runat="server" Visible="false" />
    <asp:TextBox ID="hdnSortOrder" runat="server" Visible="false" />
    <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
    <asp:UpdatePanel ID="pnlInterviewReports" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="text-align: left">
                <asp:Label ID="lbMessage" runat="server" EnableViewState="false"></asp:Label>
            </div>
            <asp:HiddenField ID="hdnScrollPos" runat="server" />
            <asp:ObjectDataSource ID="odsHiringLog" runat="server" SelectMethod="GetPaged" TypeName="TPS360.Web.UI.EventLogDataSource"
                SelectCountMethod="GetListCount" EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:ControlParameter ControlID="dtPicker" Name="StartDate" PropertyName="StartDate"
                     Type="String" />
                    <asp:ControlParameter ControlID="dtPicker" Name="EndDate" PropertyName="EndDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlRequisition" Name="JobPostingId" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlClient" Name="ClientId" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlEmployee" Name="CreatorId" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="ddlActionType" Name="ActionType" PropertyName="SelectedValue"
                        Type="String" />
                       
                        <asp:Parameter Name="IsTeamHiringReport" DefaultValue="false" Type="Boolean" />
                        <asp:Parameter Name="TeamMemberId" DbType="String" DefaultValue="0" />
                        <asp:Parameter Name="TeamId" DbType="String" DefaultValue="0" />
                        
              </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Panel ID="pnlSearchRegion" runat="server" DefaultButton="btnSearch">
                <div style="width: 100%;">
                    <div>
                        <div class="TableRow">
                            <div class="TabPanelHeader nomargin" style="border-style: none none none;">Filter Options</div>
                        </div>
                        <div class="TableRow" style="text-align: center">
                            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                      <%--   <asp:CollapsiblePanelExtender ID="cpeSeafvfrchBody" runat="server" TargetControlID="pnlSearchContent"
                            ExpandControlID="pnlSearchHeader" CollapseControlID="pnlSearchHeader" ImageControlID="ImgSearch"
                            CollapsedImage="~/Images/expand_all.gif" ExpandedImage="~/Images/collapse_all.gif"
                            SuppressPostBack="false" EnableViewState="true">
                        </asp:CollapsiblePanelExtender>
                       <asp:Panel ID="pnlSearchHeader" runat="server">
                            <div class="CommonHeader" style="clear: both;">
                                <asp:Image ID="ImgSearch" ImageUrl="~/Images/expand_all.gif" runat="server" />
                                Search Filters
                            </div>
                        </asp:Panel>--%>
                        <asp:Panel ID="pnlSearchContent" runat="server" Visible="true" Style="overflow: hidden;"
                           >
                          
                                      
                            <div class="FormLeftColumn" style="width: 48%">
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label ID="lblDate" runat="server" EnableViewState="false" Text="Date"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                         <ucl:DateRangePicker ID="dtPicker" runat ="server"  />
                                    </div>
                                </div>
                                <div class="TableRow" id="divUser" runat="server" >
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblEmployee" runat="server" Text="User"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlEmployee" CssClass="CommonDropDownList" runat="server">
                                        </asp:DropDownList>
                                        
                                        <asp:HiddenField ID="hdnSelectedUser" runat="server" />
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblActionType" runat="server" Text="Action Type"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlActionType" CssClass="CommonDropDownList" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="FormRightColumn" style="width: 48%">
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="false" ID="lblRequisition" runat="server" Text="Requisition"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlRequisition" runat="server" CssClass="CommonDropDownList">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label EnableViewState="true" ID="lblClient" runat="server"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlClient" CssClass="CommonDropDownList" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="TableRow" runat="server" id="divTeam" visible="false">
                                    <div class="TableFormLeble" style="width: 40%;" >
                                        <asp:Label EnableViewState="false" ID="lblTeam" runat="server" Text="Team"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlTeam" CssClass="CommonDropDownList" runat="server">
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="hdnSelectedTeam" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="HeaderDevider">
                        </div>
                        <div class="TableRow" style="text-align: center; padding-top: 5px">
                            <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search" CssClass="btn btn-primary"
                                EnableViewState="false" OnClick="btnSearch_Click" ><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                            <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                                EnableViewState="false" CausesValidation="false" OnClick="btnClear_Click" />
                        </div>
                        <div class="TableRow" style="padding-bottom: 5px" id="divExportButtons"
                            runat="server" visible="false">
                            <div class="TabPanelHeader nomargin" style="margin-bottom: 5px;border-style: none none none;">Report Results</div>
                            
                            <asp:ImageButton ID="btnExportToExcel" CssClass ="btn"  SkinID ="sknExportToExcel" runat ="server" ToolTip ="Export To Excel" OnClick ="btnExportToExcel_Click" />
                            <asp:ImageButton ID="btnExportToPDF" CssClass ="btn"  SkinID ="sknExportToPDF" runat ="server" ToolTip ="Export To PDF" OnClick ="btnExportToPDF_Click" />
                            <asp:ImageButton ID="btnExportToWord" CssClass ="btn"  SkinID ="sknExportToWord" runat ="server" ToolTip ="Export To Word" OnClick ="btnExportToWord_Click" />
                            
                            
                            
                          <%--  <asp:Button ID="btnExportToPDF" runat="server" Text="Export To PDF" AlternateText="Export To PDF"
                                CssClass="btn" EnableViewState="false" OnClick="btnExportToPDF_Click" />
                            <asp:Button ID="btnExportToExcel" runat="server" Text="Export To Excel" AlternateText="Export To Excel"
                                CssClass="btn" EnableViewState="false" CausesValidation="false" OnClick="btnExportToExcel_Click" />
                            <asp:Button ID="btnExportToWord" runat="server" Text="Export To Word" AlternateText="Export To Word"
                                CssClass="btn" EnableViewState="false" CausesValidation="false" OnClick="btnExportToWord_Click" />
                                
                                --%>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlGridRegion" runat="server">
                <div id="divlsvHiringLog" runat="server" class="GridContainer" style="overflow: auto;
                     width: 100%; text-align: left;" onscroll='SetScrollPosition()'>
                    <asp:ListView ID="lsvHiringLog" runat="server" DataKeyNames="Id" OnItemDataBound="lsvHiringLog_ItemDataBound"
                        OnItemCommand="lsvHiringLog_ItemCommand" OnPreRender="lsvHiringLog_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                <tr>
                                    <th runat="server" id="thDate" style="width: 15%" enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnDate" CommandName="Sort" CommandArgument="[EL].[ActionDate]"
                                            Text="Date & Time" ToolTip="Sort By Date & Time" />
                                    </th>
                                    <th runat="server" id="thUser" style="width: 20%"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnUser" CommandName="Sort" CommandArgument="[M].[FirstName]"
                                            Text="User" ToolTip="Sort By User" />
                                    </th>
                                    <th runat="server" id="thRequisition" style="width: 20%" enableviewstate ="false" >  
                                        <asp:LinkButton runat="server" ID="btnRequisition" CommandName="Sort" CommandArgument="[J].[JobTitle]"
                                            Text="Requisition" ToolTip="Sort By Requisition" />
                                    </th>
                                    <th runat="server" id="thAction" style="width: 45%"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnAction" CommandName="Sort" CommandArgument="[EL].[ActionType]"
                                            Text="Action" ToolTip="Sort By Action" />
                                    </th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td id="tdpager" runat="server" colspan="4">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td runat="server" id="tdDate">
                                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                                </td>
                                <td runat="server" id="tdUser">
                                    <asp:Label runat="server" ID="lblUser" />
                                </td>
                                <td runat="server" id="tdRequisition">
                                    <asp:Label runat="server" ID="lblJobtitle" />
                                </td>
                                <td runat="server" id="tdAction">
                                    <asp:Label runat="server" ID="lblActions" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlmodal" runat="server" Style="display: none">
                <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                    <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="Modal" runat="server" TargetControlID="pnlmodal"
                PopupControlID="pnlmodal" BackgroundCssClass="divModalBackground">
            </ajaxToolkit:ModalPopupExtender>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            <asp:PostBackTrigger ControlID="btnExportToWord" />
        </Triggers>
    </asp:UpdatePanel>