﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
namespace TPS360.Web.UI
{
    public partial class ChangePasswordforVendor :BaseControl 
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string mailId = Helper.Session.Get(SessionConstants.VendorEmail) == null ? "" : Helper.Session.Get(SessionConstants.VendorEmail).ToString();
            string memberid = hdnSelectedMemberID.Value;
            if (mailId != string.Empty || memberid != string .Empty )
            {
                Member _mem = null;
                if(mailId !=string .Empty )
                 _mem=Facade.GetMemberByMemberEmail(mailId.ToString().Trim());
                else if (memberid != string.Empty)
                {
                    _mem = Facade.GetMemberById(Convert.ToInt32(memberid));
                }
                if (_mem != null)
                {
                    MembershipUser user = Membership.GetUser(_mem.UserId);
                    user.ChangePassword(user.GetPassword(), txtNewPass.Text.ToString());
                    Membership.UpdateUser(user);
                }
            }
            Helper.Session.Set(SessionConstants.VendorEmail, string.Empty);
            Helper.Session.Set(SessionConstants.Status, "Success");
            //string prevPage = Helper.Session.Get(SessionConstants.PreviousPath).ToString();
            hdnSelectedMemberID.Value = "";
            Response.Redirect(Request.Url.AbsoluteUri.ToString());
            
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.AbsoluteUri.ToString());
        }
}
}
