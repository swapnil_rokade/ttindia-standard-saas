﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:EmployeeLandingTop.ascx
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
 *  0.1            23-Nov-2009         Sandeesh         Enhancement id:11645 - Changes made  for adding internal ratings for an employee 
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmployeeLandingTop.ascx.cs"
    Inherits="TPS360.Web.UI.EmployeeLandingTop" %>
<asp:UpdatePanel ID="upPanel" runat ="server" ><ContentTemplate >
<div style="float: left; text-align: left; width: 35%;">
    <div>
        <asp:HyperLink ID="lblEmployeeName" runat="server" Font-Size="16px" NavigateUrl ="~/Employee/Overview.aspx" Font-Bold ="true" ></asp:HyperLink>
    </div>
    <div id="divEmployeeAddress" runat ="server" >
        
    </div>
</div>
<div style="float: left; text-align: left; width: 35%;">
    <div>
        Email:
        <asp:Label ID="lblPrimaryEmail" runat="server"></asp:Label>
    </div>
    <div>
        Mobile:
        <asp:Label ID="lblMobile" runat="server"></asp:Label>
    </div>
    <div>
        Phone:
        <asp:Label ID="lblPhone" runat="server"></asp:Label>
    </div>
    <div>
        Office:
        <asp:Label ID="lblOffice" runat="server"></asp:Label>
    </div>
   
</div>
<div style="float: left; text-align: left; width: 30%;">
    <div>
        Requisitions: <asp:Label ID ="lblRequisitions" runat ="server" ></asp:Label>
    </div>
    <div>
        Assigned Candidates: <asp:Label ID ="lblCandidates" runat ="server" ></asp:Label>
    </div>
    <div>
        Access Role: <asp:Label ID ="lblrole" runat ="server" ></asp:Label>
    </div>

</div>
</ContentTemplate></asp:UpdatePanel>