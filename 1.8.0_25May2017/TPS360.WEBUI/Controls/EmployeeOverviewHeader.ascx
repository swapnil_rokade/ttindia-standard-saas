﻿<!-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmployeeOverviewHeader.ascx.cs
    Description: This is the user control page used in resume builder to allow user to provide basic information.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    1.0            10/Jun/2016      Prasanth Kumar G       Introduced LDAP
------------------------------------------------------------------------------------------------------------------------------------------- 
-->
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmployeeOverviewHeader.ascx.cs"
    Inherits="TPS360.Web.UI.EmployeeOverviewHeader" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

    <script language="JavaScript" src="../Scripts/SendMail.js" type="text/javascript"></script>

 <%@ Register Src ="~/Controls/MemberJobCarts.ascx" TagName ="JobCart" TagPrefix ="ucl" %>
<script type="text/javascript">
function activate(tabid){

$('.nav-tabs > .active').removeClass('active');

$('#'+tabid).parent().addClass('active');
var $target = $('html,body'); 
$target.animate({scrollTop: $target.height()}, 1000);
}

</script> 

<style type="text/css">
    
    
.field-header {
font-size:12px;
color:#666666;
text-transform : uppercase ;
}
.field-header A
{
	cursor : pointer ;
	color :#008EE8;
}
.field-header + h3
{
	  width : 90%;
	  overflow : hidden;
	  font-size: 16px;
	  text-overflow: ellipsis;
}
.overviewColumn
{
	float:left ; 
	width : 50%  ;
	overflow : hidden;
	white-space : nowrap ;
	text-overflow:ellipsis !important;

}


    .DontShow
    {
 
        display: none; 
        margin: 1px;
        padding: 2px 5px 2px 5px;
        border-style: solid;
        border-width: 1px;
        border-color: #999999;
        border-top-color: #CCCCCC;
        border-left-color: #CCCCCC;
        background-color: #EEEEEE;
        color: #333333;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-size: 100%;
        font-weight: bold;
        white-space: nowrap;
        text-decoration: none;
        cursor: pointer;
    }
    .TextBoxStyle
    {
    	overflow:auto;
    	
    }
    #btnAddToMyCalender
    {
        width: 125px;
    }
</style>

<script src ="../js/CompanyRequisition.js" type ="text/javascript" ></script>
<asp:HiddenField ID="outlook1" runat ="server"/>
<asp:HiddenField ID="ddlReqval" runat="server" />
<asp:HiddenField ID="hdnReqSIndx" runat="server" />

<div style =" padding-left : 30px;">
<div class ="TableRow"  style =" padding-bottom : 60px;">
    <div  class ="overviewColumn">
        <h6 class="field-header">Full Name</h6>
        <h3><asp:Label ID ="lblCandidateName" runat ="server" ></asp:Label></h3>
    </div>
    <div   class ="overviewColumn">
        <h6 class="field-header"><a href="#tab1"  onclick="activate('tabone')" data-toggle="tab">Requisitions</a></h6>
        <h3><asp:Label ID="lblRequisitionCount" runat="server"></asp:Label></h3>
    </div>
   
</div>
<div  class ="TableRow"  style =" padding-bottom : 60px;">
    <div   class ="overviewColumn">
        <h6 class="field-header">Email</h6>
        <h3><asp:Label ID="lblPrimaryEmail" runat="server"></asp:Label></h3>
    </div>
    <div   class ="overviewColumn">
        <h6 class="field-header"><a href="#tab2" id="AssignedLbl" onclick="activate('tabtwo')" data-toggle="tab">Assigned Candidates</a></h6>
        <h3><asp:Label ID="lblCandidateCount" runat="server"></asp:Label></h3>
    </div>
</div>
<div  class ="TableRow" style =" padding-bottom : 60px;">
    <div   class ="overviewColumn">
        <h6 class="field-header">Mobile</h6>
        <h3> <asp:Label ID="lblMobile" runat="server"></asp:Label></h3>
    </div>
    <div   class ="overviewColumn">
        <h6 class="field-header"><a href="#tab3" onclick="activate('tabthree')" data-toggle="tab">Upcoming Interviews</a></h6>
        <h3><asp:Label ID="lblInterviewCount" runat="server"></asp:Label></h3>
    </div>
    
</div>

<div   class ="TableRow"  style =" padding-bottom : 60px;">
    <div   class ="overviewColumn">
        <h6 class="field-header">Location</h6>
        <h3><asp:Label ID="lblLocation" runat="server"></asp:Label></h3>
    </div>
    <div   class ="overviewColumn">
        <h6 class="field-header"><a href="#tab5" onclick="activate('tabfive')" data-toggle="tab">Documents</a></h6>
        <h3><asp:Label ID="lblDocumentsCount" runat="server" EnableViewState ="true" ></asp:Label></h3>
    </div>
   
</div>

<div  class ="TableRow"  style =" padding-bottom : 60px;">
    <div   class ="overviewColumn">
        <h6 class="field-header"><asp:LinkButton ID="lnkAccessRole" runat ="server" Text ="Access Role" ></asp:LinkButton></h6>
        <h3><asp:Label ID="lblAccess" runat="server"  Font-Bold="true"></asp:Label></h3>
    </div>
    <div   class ="overviewColumn">
     <!-- Code introduced by Prasanth on 10/Jun/2016 Start-->
   <div  class ="overviewColumn">
        <h6 class="field-header">User Name</h6>
        <h3><asp:Label ID ="lblUserName" runat ="server" ></asp:Label></h3>
    </div>
   <!-- **************END************************-->
    </div>
</div>




</div>
<asp:UpdatePanel ID="upActions" runat ="server" >
<ContentTemplate >

<div class ="well" style =" clear :both ;" >
 <div  align="center" style =" text-align :center ;" >
     <asp:LinkButton ID="lnkAddNote" runat ="server" CssClass ="btn btn-medium dropdown-toggle" Text ="New Note"></asp:LinkButton>
     <asp:LinkButton ID="lnkAddDocument" runat ="server" CssClass ="btn btn-medium dropdown-toggle" Text ="New Document"></asp:LinkButton>
     <asp:LinkButton ID="lnkEditAccess" runat ="server" CssClass ="btn btn-medium dropdown-toggle" Text ="Edit Access"></asp:LinkButton>
  </div>
 </div> 
</div>
</ContentTemplate>
</asp:UpdatePanel>
 <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
 <asp:HiddenField ID="hiddenMemberId" runat="server" />
 
<asp:SiteMapDataSource runat="server" ID="MenuSiteMap" ShowStartingNode="false" />

