﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Collections.Generic;
using ParserLibrary;
using System.IO;
using System.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Text.RegularExpressions;
namespace TPS360.Web.UI
{
    public partial class MemberCertificationEditor : BaseControl
    {
        private bool IsManager = false;

        #region Member Variables
        private static int _CertificationId = 0;
        MemberCertificationMap _memberCertificationmap = new MemberCertificationMap();
        private static int _memberId = 0;
        private static string _memberrole = string.Empty;
        #endregion

        #region Properties
        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                hfMemberId.Value = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];
            }
            //From Member Login
            else
            {
                hfMemberId.Value = base.CurrentMember.Id.ToString();
            }
            _memberId = Int32.Parse(hfMemberId.Value);
        }
        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }
        #endregion

        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            setodsCertificationList();
            MemberManager Manager = new MemberManager();
            Manager = Facade.GetMemberManagerByMemberIdAndManagerId(_memberId, CurrentMember.Id);
            if (Manager != null || IsUserAdmin)
                IsManager = true;
            else
                IsManager = false;

            if (!IsPostBack)
            {
                hdnSortColumnOrder.Value = "asc";
                hdnSortColumnName.Value = "CertificationName";

                lsvCertifications.DataBind();
                hdnSortColumn.Text = "btnCertification";
                hdnSortOrder.Text = "ASC";
            }
            string pagesize = "";
            pagesize = (Request.Cookies["LicenseRowPerPage"] == null ? "" : Request.Cookies["LicenseRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCertifications.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)this.lsvCertifications.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            }
        }
        #endregion

        #region Methods
        private void PrepareEditView()
        {
            MemberCertificationMap membercertificationMap = Facade.GetMemberCertificationMapById(_CertificationId);
            if (membercertificationMap != null)
            {
                txtCertificationName.Text = MiscUtil.RemoveScript(membercertificationMap.CerttificationName, string.Empty);
                wdcValidFrom.Value = membercertificationMap.ValidFrom;
                wdcValidTo.Value = membercertificationMap.ValidTo;
                txtIssuer.Text = MiscUtil.RemoveScript(membercertificationMap.IssuingAthority, string.Empty);
            }
        }

        private void Clear()
        {
            txtCertificationName.Text = "";
            txtIssuer.Text = "";
            wdcValidFrom.Value = "";
            wdcValidTo.Value = "";
        }

        private void PlaceUpDownArrow()
        {
            try
            {
                bool alreadyAvailable = true;
                LinkButton lnk = (LinkButton)lsvCertifications.FindControl(hdnSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }
        }
        #endregion

        #region Button Events
        protected void btnSave_Click(object sender, EventArgs e)
        {
            rfvCertification.Enabled = true;
            if (_CertificationId == 0)
            {
                IList<MemberCertificationMap> memberCertificationMapList = new List<MemberCertificationMap>();
                memberCertificationMapList = Facade.GetAllMemberCertificationMapByMemberId(_memberId);
                if (memberCertificationMapList != null)
                {
                    var certificate = from a in memberCertificationMapList
                                      where a.CerttificationName == txtCertificationName.Text.Trim() && (a.ValidFrom == DateTime.MinValue ? DateTime.MinValue : Convert.ToDateTime(a.ValidFrom)) == Convert.ToDateTime(wdcValidFrom.Value == string.Empty ? DateTime.MinValue : wdcValidFrom.Value) && (a.ValidTo == DateTime.MinValue ? DateTime.MinValue : Convert.ToDateTime(a.ValidTo)) == Convert.ToDateTime(wdcValidTo.Value == string.Empty ? DateTime.MinValue : wdcValidTo.Value) && a.IssuingAthority == txtIssuer.Text.Trim()
                                      select a;

                    if (certificate.Count() == 0)
                    {
                        MemberCertificationMap membercertificationmap = new MemberCertificationMap();
                        membercertificationmap.MemberId = _memberId;
                        membercertificationmap.CerttificationName = MiscUtil.RemoveScript(txtCertificationName.Text.Trim());
                        if (wdcValidFrom.Value != null && wdcValidFrom.Value.ToString().Trim() != "")
                            membercertificationmap.ValidFrom = Convert.ToDateTime(wdcValidFrom.Value); //((wdcValidFrom.Value == null || wdcValidFrom.Value.ToString() == string.Empty) ? string.Empty : ((wdcValidFrom.Value.ToString().Split('/')[1] + "/" + wdcValidFrom.Value.ToString().Split('/')[0] + "/" + wdcValidFrom.Value.ToString().Split('/')[2]))).ToString();
                        if (wdcValidTo.Value != null && wdcValidTo.Value.ToString().Trim() != "")
                            membercertificationmap.ValidTo = Convert.ToDateTime(wdcValidTo.Value);// ((wdcValidTo.Value == null || wdcValidTo.Value.ToString() == string.Empty) ? string.Empty : ((wdcValidTo.Value.ToString().Split('/')[1] + "/" + wdcValidTo.Value.ToString().Split('/')[0] + "/" + wdcValidTo.Value.ToString().Split('/')[2]))).ToString();

                        membercertificationmap.IssuingAthority = MiscUtil.RemoveScript(txtIssuer.Text.Trim());
                        if (base.CurrentMember != null)
                        {
                            membercertificationmap.CreatorId = base.CurrentMember.Id;
                            membercertificationmap.UpdatorId = base.CurrentMember.Id;
                        }
                        if (membercertificationmap.CerttificationName.ToString() != string.Empty)
                        {
                            Facade.AddMemberCertificationMap(membercertificationmap);
                            MiscUtil.ShowMessage(lblMessage, "Certification has been added successfully.", false);
                        }

                    }
                    else
                    {
                        MiscUtil.ShowMessage(lblMessage, "Certification is already available.", false);
                    }
                }
                else
                {
                    MemberCertificationMap membercertificationmap = new MemberCertificationMap();
                    membercertificationmap.MemberId = _memberId;
                    membercertificationmap.CerttificationName = MiscUtil.RemoveScript(txtCertificationName.Text.Trim());
                    if (wdcValidFrom.Value != null && wdcValidFrom.Value != string.Empty) membercertificationmap.ValidFrom = Convert.ToDateTime(wdcValidFrom.Value);//==null?"":wdcValidFrom .Value.ToString () ;
                    if (wdcValidTo.Value != null && wdcValidTo.Value != string.Empty) membercertificationmap.ValidTo = Convert.ToDateTime(wdcValidTo.Value);//==null ?"":wdcValidTo .Value.ToString () ;
                    membercertificationmap.IssuingAthority = MiscUtil.RemoveScript(txtIssuer.Text.Trim());
                    if (membercertificationmap.CerttificationName.ToString() != string.Empty)
                    {
                        Facade.AddMemberCertificationMap(membercertificationmap);
                        MiscUtil.ShowMessage(lblMessage, "Certification has been added successfully.", false);
                    }
                }
            }
            else
            {
                MemberCertificationMap membercertificationmap = new MemberCertificationMap();
                membercertificationmap.MemberId = _memberId;
                membercertificationmap.Id = _CertificationId;
                membercertificationmap.CerttificationName = MiscUtil.RemoveScript(txtCertificationName.Text.Trim());
                if (wdcValidFrom.Value != null) membercertificationmap.ValidFrom = Convert.ToDateTime(wdcValidFrom.Value);//==null?"":wdcValidFrom .Value.ToString () ;
                if (wdcValidTo.Value != null) membercertificationmap.ValidTo = Convert.ToDateTime(wdcValidTo.Value);// == null ? "" : wdcValidTo.Value.ToString();//(membercertificationmap .ValidTo ==null || membercertificationmap .ValidTo ==string .Empty )? string .Empty : Convert.ToDateTime(wdcValidTo.Value.ToString()).ToShortDateString ();
                membercertificationmap.IssuingAthority = MiscUtil.RemoveScript(txtIssuer.Text.Trim());
                if (membercertificationmap.CerttificationName.ToString() != string.Empty)
                {
                    Facade.UpdateMemberCertificationMap(membercertificationmap);
                    MiscUtil.ShowMessage(lblMessage, "Certification has been updated successfully.", false);
                    _CertificationId = 0;
                }
            }
            Facade.UpdateUpdateDateByMemberId(_memberId);
            lsvCertifications.DataBind();
            Clear();
        }
        #endregion

        #region ListView Events
        protected void lsvCertifications_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberCertificationMap memberCertificationMap = ((ListViewDataItem)e.Item).DataItem as MemberCertificationMap;
                if (memberCertificationMap != null)
                {
                    Label lblCertification = (Label)e.Item.FindControl("lblCertification");
                    Label lblValid = (Label)e.Item.FindControl("lblValid");
                    Label lblIssuingAthority = (Label)e.Item.FindControl("lblIssuingAthority");
                    lblCertification.Text = memberCertificationMap.CerttificationName == null ? string.Empty : MiscUtil.RemoveScript(memberCertificationMap.CerttificationName);
                    string from = string.Empty;
                    string to = string.Empty;
                    from = memberCertificationMap.ValidFrom.Year == 1 ? "" : memberCertificationMap.ValidFrom.ToShortDateString();
                    to = memberCertificationMap.ValidTo.Year == 1 ? "" : memberCertificationMap.ValidTo.ToShortDateString();
                    lblValid.Text = from + ((string.IsNullOrEmpty(from) || string.IsNullOrEmpty(to)) ? "" : " - ") + to;
                    lblIssuingAthority.Text = memberCertificationMap.IssuingAthority == null ? string.Empty : memberCertificationMap.IssuingAthority;

                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
                    btnDelete.OnClientClick = "return ConfirmDelete('Certification')";
                    btnDelete.Visible = IsManager;
                    btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(memberCertificationMap.Id);
                    if (Page.Request.Url.AbsoluteUri.ToString().Contains("Overview"))
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell table = (System.Web.UI.HtmlControls.HtmlTableCell)lsvCertifications.FindControl("thAction");
                        System.Web.UI.HtmlControls.HtmlTableCell tabledata = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAction");
                        table.Visible = false;
                        tabledata.Visible = false;
                    }
                }
            }

        }

        protected void lsvCertifications_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvCertifications.FindControl("pagerControl");
            if (PagerControl != null)
            {

                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "LicenseRowPerPage";
            }
            PlaceUpDownArrow();

            if (Page.Request.Url.AbsoluteUri.ToString().Contains("Overview"))
            {
                System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvCertifications.FindControl("tdPager");
                if (tdPager != null) tdPager.ColSpan = 3;
            }

            if (lsvCertifications.Items.Count == 0)
            {
                lsvCertifications.DataSource = null;
                lsvCertifications.DataBind();
            }
        }

        protected void lsvCertifications_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    bool alreadyAvailable = true;
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Text == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Text == "ASC") hdnSortOrder.Text = "DESC";
                        else hdnSortOrder.Text = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Text = lnkbutton.ID;
                        hdnSortOrder.Text = "ASC";
                    }

                    if (hdnSortColumnName.Value == string.Empty || hdnSortColumnName.Value != e.CommandArgument.ToString())
                        hdnSortColumnOrder.Value = "asc";
                    else hdnSortColumnOrder.Value = hdnSortColumnOrder.Value == "asc" ? "desc" : "asc";
                    hdnSortColumnName.Value = e.CommandArgument.ToString();
                    odsCertificationList.SelectParameters["SortOrder"].DefaultValue = hdnSortColumnOrder.Value.ToString();
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    _CertificationId = id;

                    PrepareEditView();
                    txtCertificationName.Focus();
                    Page.ClientScript.RegisterStartupScript(typeof(Page), "MoveTop", "<script>self.moveTo(0,0); </script>");
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteMemberCertificationMapById(id))
                        {
                            lsvCertifications.DataBind();
                            MiscUtil.ShowMessage(lblMessage, "License/Certification has been successfully deleted.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }
        #endregion

        #region ObjectDataSource Event

        private void setodsCertificationList()
        {
            GetMemberId();
            if (_memberId > 0)
            {
                if( odsCertificationList.SelectParameters["memberId"]==null)
                    odsCertificationList.SelectParameters.Add("memberId", _memberId.ToString());
            }
            if (hdnSortColumnOrder.Value == string.Empty && hdnSortColumnName.Value == string.Empty)
            {
                hdnSortColumnOrder.Value = "asc";
                hdnSortColumnName.Value = "CertificationName";
            }
            if (odsCertificationList.SelectParameters["SortOrder"] == null)
                 odsCertificationList.SelectParameters.Add("SortOrder", hdnSortColumnOrder.Value);
        }

        protected void odsCertificationList_Init(object sender, EventArgs e)
        {
           
        }

        #endregion
    }
}