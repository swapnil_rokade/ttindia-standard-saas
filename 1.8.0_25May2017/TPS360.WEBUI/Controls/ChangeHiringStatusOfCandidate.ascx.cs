﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ChangeHiringStatusOfCandidate.ascx
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date               Author                 MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             2/Mar/2017          Sumit Sonawane            New function
--------------------------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using TPS360.Common;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using WebChart;
using System.Configuration;
using AjaxControlToolkit;
using System.Data.SqlClient;
using System.Linq;
using System.Data;
using System.Web.Configuration;
using TPS360.BusinessFacade;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.IO;
using System.Net;
using System.Web.Security;
using System.Xml;
using iTextSharp.text.pdf;
using iTextSharp.text;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using System.Globalization;


namespace TPS360.Web.UI
{
    public partial class ChangeHiringStatusOfCandidate : BaseControl
    {
        #region Member Variables
        private static int _memberId = 0;
        private static int _JobPostingId = 0;
        
        private static string _memberrole = string.Empty;
        #endregion

        #region Properties

        public string MemberRole
        {
            set { _memberrole = value; }
            get { return _memberrole; }
        }

        public MemberDocumentType DocumentType
        {
            set
            {
                ViewState["DocumentTypeId"] = value;
                hfDocumentType.Value = EnumHelper.GetDescription(value);
            }
            get { return (MemberDocumentType)ViewState["DocumentTypeId"]; }
        }

        #endregion

        #region Methods

        private void LoadQueryStringData()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            else
            {
                _memberId = base.CurrentMember.Id;
            }
            if (_memberId > 0)
            {
                hfMemberId.Value = _memberId.ToString();
            }


            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]))
            {
                _JobPostingId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]);
            }
        }

        private void LoadUploadedDocument()
        {
            //if (_memberId > 0)
            //{
            //    if (string.IsNullOrEmpty(hfDocumentType.Value))
            //    {
            //        IList<MemberDocument> lstAddedPosition = Facade.GetAllMemberDocumentByMemberId(_memberId);
            //        lsvDocuments.DataSource = lstAddedPosition;
            //        lsvDocuments.DataBind();
                    
            //    }
            //    else
            //    {
            //        IList<MemberDocument> lstAddedPosition = Facade.GetAllMemberDocumentByTypeAndMemberId(hfDocumentType.Value, _memberId);

            //        lsvDocuments.DataSource = lstAddedPosition;
            //        lsvDocuments.DataBind();
            //    }
            //}
        }

        private void LoadInitialData()
        {
            bool _isHiringManager = base.IsHiringManager;
            //***********Code added by pravin khot on 7/March/2017*********
            string role=Facade.GetCustomRoleNameByMemberId(base.CurrentMember.Id);
            if (role.ToLower()=="hiring manager")
            {
                MiscUtil.PopulateHiringMatrixLevelsForHiringManagerList(ddlChangeHiringStatus, Facade, base.CurrentMember.Id);               
            }
                //***********END********************
            else
            {
                MiscUtil.PopulateHiringMatrixLevelsList(ddlChangeHiringStatus, Facade);
            }
            if (!string.IsNullOrEmpty(hfDocumentType.Value))
            {
                ddlChangeHiringStatus.SelectedValue = ((int)((MemberDocumentType)ViewState["DocumentTypeId"])).ToString();
                EnableDisableValidator(hfDocumentType.Value);
                rowDocumentType.Visible = false;
                lblDocumentTitle.Text = hfDocumentType.Value + " Title";
                lblDocumentPath.Text = hfDocumentType.Value + " Path";

                //revPhoto.ValidationGroup = hfDocumentType.Value;
                //revVideo.ValidationGroup = hfDocumentType.Value;
                //revDocument.ValidationGroup = hfDocumentType.Value;
                //rfvDocumentUpload.ValidationGroup = hfDocumentType.Value;
                //rfvDocumentTitle.ValidationGroup = hfDocumentType.Value;
                btnUpload.ValidationGroup = hfDocumentType.Value;
            }
            else
            {
                rowDocumentType.Visible = true;
            }
           // LoadUploadedDocument();
           // lsvDocuments.DataBind();
         }

        private void EnableDisableValidator(string strDocumentType)
        {
            if (strDocumentType.ToLower().IndexOf("photo") >= 0)
            {
                //revPhoto.Visible = true;
                //revVideo.Visible = false;
                //revDocument.Visible = false;
            }
            else if (strDocumentType.ToLower().IndexOf("video") >= 0)
            {
                //revPhoto.Visible = false;
                //revVideo.Visible = true;
                //revDocument.Visible = false;
            }
            else
            {
                //revPhoto.Visible = false;
                //revVideo.Visible = false;
                //revDocument.Visible = true;
            }
        }

        private void SaveHiringStatus()
        {
            //Code added by Sumit Sonawane on 1/Mar/2017 ***********
           // _JobPostingId = int.Parse(Session["JobPostingId"].ToString());
            JobPosting JPDetails = Facade.GetJobPostingById(_JobPostingId);
            HiringMatrix HiringMatrixLogDetails = new HiringMatrix();
            HiringMatrixLogDetails.MemberId = _memberId;
            HiringMatrixLogDetails.HiringLevelLogId = int.Parse(ddlChangeHiringStatus.SelectedItem.Value);
            HiringMatrixLogDetails.JobPostingID = _JobPostingId;
            HiringMatrixLogDetails.HiringLevelComment = txtDocumentDescription.Text;
            HiringMatrixLogDetails.CreatorId = JPDetails.CreatorId;
            HiringMatrixLogDetails.UpdatorId = CurrentMember.Id;
            HiringMatrixLogDetails.CreateDate = JPDetails.CreateDate ;
            HiringMatrixLogDetails.UpdateDate = DateTime.Now;

            HiringMatrixLogDetails = Facade.AddHiringMatrixLogDetails(HiringMatrixLogDetails);
            MiscUtil.MoveApplicantToHiringLevel(_JobPostingId, _memberId, JPDetails.CreatorId, int.Parse(ddlChangeHiringStatus.SelectedItem.Value), Facade);
            //**************END*****************
            MiscUtil.ShowMessage(lblMessage, "Candidate has been successfully Moved to" + " " + ddlChangeHiringStatus.SelectedItem.ToString() , false);

            ddlChangeHiringStatus = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlChangeHiringStatus);
            var webConfig = WebConfigurationManager.OpenWebConfiguration("~");
            var submit = webConfig.AppSettings.Settings["Submission"];
            var offer = webConfig.AppSettings.Settings["Offer"];
            var join = webConfig.AppSettings.Settings["Join"];
            var interview = webConfig.AppSettings.Settings["Interview"];

            string AppPath = "";
            if (isMainApplication) AppPath = "Common";

            Session["_memberId"] = _memberId;
            Session["_JobPostingId"] = _JobPostingId;

            foreach (System.Web.UI.WebControls.ListItem list in ddlChangeHiringStatus.Items)
            {

                if (list.Text == submit.Value && submit.Value == ddlChangeHiringStatus.SelectedItem.Text)
                {
                    //list.Value = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/" + AppPath + "SubmissionDetails.aspx?StatusId=" + 0 + "&JID=" + _JobPostingId + "&Canid=" + _memberId + "','778','570'); return false;";
                    list.Value = UrlConstants.ApplicationBaseUrl + "Modals/SubmissionDetails.aspx?StatusId=" + list.Value + "&JID=" + _JobPostingId + "&Canid=" + _memberId;                    
                    Response.Redirect(list.Value.ToString());
                }
                else if (list.Text == offer.Value && offer.Value == ddlChangeHiringStatus.SelectedItem.Text)
                {
                    list.Value = UrlConstants.ApplicationBaseUrl + "Modals/" + AppPath + "HiringDetails.aspx?StatusId=" + list.Value + "&JID=" + _JobPostingId + "&Canid=" + _memberId;
                    //btnUpload.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/" + AppPath + "HiringDetails.aspx?StatusId=" + 0 + "&JID=" + _JobPostingId + "&Canid=" + _memberId + "','778','570'); return false;";
                    Response.Redirect(list.Value.ToString());                   
                }
                else if (list.Text == join.Value && join.Value == ddlChangeHiringStatus.SelectedItem.Text)
                {
                    //list.Value = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/" + AppPath + "JoiningDetails.aspx?StatusId=" + 0 + "&JID=" + _JobPostingId + "&Canid=" + _memberId + "','450','370'); return false;";
                    list.Value = UrlConstants.ApplicationBaseUrl + "Modals/" + AppPath + "JoiningDetails.aspx?StatusId=" + list.Value + "&JID=" + _JobPostingId + "&Canid=" + _memberId;
                    //btnUpload.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/" + AppPath + "JoiningDetails.aspx?StatusId=" + 0 + "&JID=" + _JobPostingId + "&Canid=" + _memberId + "','450','370'); return false;";
                    Response.Redirect(list.Value.ToString());
                }
                else if (list.Text == interview.Value && interview.Value == ddlChangeHiringStatus.SelectedItem.Text)
                {
                    list.Value = UrlConstants.ApplicationBaseUrl + "Modals/ScheduleInterview.aspx?StatusId=" + list.Value + "&JID=" + _JobPostingId + "&Canid=" + _memberId;
                    Response.Redirect(list.Value.ToString());
                }
                else if (list.Text == "Rejected" && "Rejected" == ddlChangeHiringStatus.SelectedItem.Text)
                {
                    list.Value = UrlConstants.ApplicationBaseUrl + "Modals/RejectionDetails.aspx?StatusId=" + list.Value + "&JID=" + _JobPostingId + "&Canid=" + _memberId;
                    Response.Redirect(list.Value.ToString());
                }
                else if (list.Text == "Offer Decline" && "Offer Decline" == ddlChangeHiringStatus.SelectedItem.Text || list.Text == "Offer Rejected" && "Offer Rejected" == ddlChangeHiringStatus.SelectedItem.Text)
                {
                    list.Value = UrlConstants.ApplicationBaseUrl + "Modals/OfferRejected.aspx?StatusId=" + list.Value + "&JID=" + _JobPostingId + "&Canid=" + _memberId;
                    Response.Redirect(list.Value.ToString());
                }
                else
                {
                }
            }
            string ofRej = ddlChangeHiringStatus.SelectedItem.Text;

            //if (ddlChangeHiringStatus.SelectedItem.ToString() == "Offered")
            //{
            //    SecureUrl url = UrlHelper.BuildSecureUrl("../Modals/HiringDetails.aspx", string.Empty, UrlConstants.PARAM_MEMBER_ID, _memberId.ToString(), UrlConstants.PARAM_JOB_ID, _JobPostingId.ToString(), UrlConstants.PARAM_SITEMAP_ID, "403");
            //    Session["_memberId"] = _memberId;
            //    Session["_JobPostingId"] = _JobPostingId;
            //    Response.Redirect(url.ToString());
            //}
        }

        private void UpdateDocument()
        {
                //MemberDocument newDoc = new MemberDocument();
                //newDoc.Description =MiscUtil .RemoveScript ( txtDocumentDescription.Text);
                //newDoc.Title =MiscUtil .RemoveScript ( txtDocumentTitle.Text);
                //newDoc.FileName = hfMemberDocumentName.Value;
                //newDoc.FileTypeLookupId = Int32.Parse(ddlChangeHiringStatus.SelectedValue);
                //newDoc.MemberId = _memberId;
                //newDoc.Id = Int32.Parse(hfMemberDocumentId.Value);
                //Facade.UpdateMemberDocument(newDoc);
                //MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.UpdateDocument, Facade);
                //MiscUtil.ShowMessage(lblMessage, "Successfully updated the file", false);
        }

        private bool CheckFileSize()
        {
            decimal  fileSize = Convert.ToDecimal (Convert .ToDecimal ( fuDocument.FileContent.Length) / (1024 * 1024));
            if (fileSize >  Convert .ToDecimal ( 3.0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private string GetDocumentLink(string strFileName, string strDocumenType)
        {

            string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, _memberId, strFileName, strDocumenType, true);

            if (strFilePath != strFileName)
            {
                return "<a href='" + strFilePath + "' target='_blank'>" + strFileName + "</a>"; // 0.1
            }
            else
            {
                return strFileName + "(File not found)";
            }
        }

        private bool DeleteUploadedDocement(int intId, string strFileName, string strDocumentType)
        {
            if (Facade.DeleteMemberDocumentById(intId))
            {
                string strPath = MiscUtil.GetMemberDocumentPath(this.Page, _memberId, strFileName, strDocumentType, false);
                File.Delete(strPath);
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            _memberId = Int32.Parse(hfMemberId.Value);
            if (!IsPostBack)
            {              
                LoadQueryStringData();
                LoadInitialData();

                odsDoc.SelectParameters["MemberID"].DefaultValue = _memberId.ToString();
            }
            lblMessage.Text = "";
            lblMessage.CssClass = "";
        }

        protected void ddlChangeHiringStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            //EnableDisableValidator(ddlChangeHiringStatus.SelectedItem.Text);
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string strMessage = string.Empty;

            if (hfMemberDocumentId.Value == String.Empty)
            {
                SaveHiringStatus();
                strMessage = "Successfully uploaded the file.";
            }
            else
            {
                ddlChangeHiringStatus.Enabled = true;
                fuDocument.Enabled = true;
                //rfvDocumentUpload.Enabled = true;
                cvDocumentType.Enabled = true;
                if (btnUpload.Text == "Update")
                {
                    UpdateDocument();
                }
            }
            Refresh(ddlChangeHiringStatus.SelectedItem.Text.ToLower(), strMessage);
            if (ddlChangeHiringStatus.Visible)
                ddlChangeHiringStatus.SelectedValue = "0";
           // LoadUploadedDocument();
            lsvDocuments.DataBind();
            txtDocumentDescription.Text = String.Empty;
            txtDocumentTitle.Text = String.Empty;
            hfMemberDocumentId.Value = String.Empty;
        }
        protected void lsvDocument_OnItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (StringHelper.IsEqual(e.CommandName, "DeleteItem"))
            {
                string[] strKeyWord = e.CommandArgument.ToString().Split(':');
                if (strKeyWord.Length == 3 && DeleteUploadedDocement(Int32.Parse(strKeyWord[0]), strKeyWord[1], strKeyWord[2]))
                {
                    MiscUtil.AddActivity(_memberrole, _memberId, base.CurrentMember.Id, ActivityType.DeleteDocument, Facade);
                    MiscUtil.ShowMessage(lblMessage, "Successfully deleted the file.", false);
//                    LoadUploadedDocument();
                    lsvDocuments.DataBind();

                    Refresh(strKeyWord[2].ToLower(), "Successfully deleted the file.");
                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "Could not delete the file.", true);
                }

                if (ddlChangeHiringStatus.Enabled)
                    ddlChangeHiringStatus.SelectedValue = "0";
                int intId = Convert.ToInt32(strKeyWord[0].ToString());
                if (string.Compare(intId.ToString(), hfMemberDocumentId.Value) == 0)
                {
                    ddlChangeHiringStatus.Enabled = true;
                    fuDocument.Enabled = true;
                    //rfvDocumentUpload.Enabled = true;
                    cvDocumentType.Enabled = true;
                    txtDocumentDescription.Text = "";
                    txtDocumentTitle.Text = "";
                    hfMemberDocumentId.Value = string.Empty;
                    ddlChangeHiringStatus.SelectedIndex = 0;
                }
            }
            else if (StringHelper.IsEqual(e.CommandName, "EditItem"))
            {
                int intId = Convert.ToInt32(e.CommandArgument);
                MemberDocument curDoc = Facade.GetMemberDocumentById(intId);
                txtDocumentDescription.Text = MiscUtil.RemoveScript(curDoc.Description, string.Empty);
                txtDocumentTitle.Text = MiscUtil.RemoveScript(curDoc.Title, string.Empty);
                ddlChangeHiringStatus.SelectedValue = curDoc.FileTypeLookupId.ToString();
                hfMemberDocumentId.Value = intId.ToString();
                hfMemberDocumentName.Value = curDoc.FileName;
                ddlChangeHiringStatus.Enabled = false;
                fuDocument.Enabled = false;
                //rfvDocumentUpload.Enabled = false;
                cvDocumentType.Enabled = false;
                btnUpload.Text = "Update";
            }
        }

        protected void lsvDocuments_PreRender(object sender, EventArgs e)
        {
            if (lsvDocuments != null)
            {
                HtmlTableCell tdpager = (HtmlTableCell)lsvDocuments.FindControl("tdPager");
                ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvDocuments.FindControl("pagerControl");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }
                    HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                    if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "MemberDocunebtUploaderRowPerPage";
                    
                }
            }
        }

        protected void lsvDocuments_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (TPS360.Web.UI.Helper.ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                EventLogForRequisitionAndCandidate memberHiringLog = ((ListViewDataItem)e.Item).DataItem as EventLogForRequisitionAndCandidate;
                if (memberHiringLog != null)
                {

                    Label lblDocumentLink = e.Item.FindControl("lblDocumentLink") as Label;
                    Label lblDocumentType = e.Item.FindControl("lblDocumentType") as Label;
                    Label lblDocumentTitle = e.Item.FindControl("lblDocumentTitle") as Label;
                    Label lblUserNameUpdator = e.Item.FindControl("lblUserNameUpdator") as Label;
                    HiringMatrixLevels glName = Facade.GetHiringMatrixLevelsById(memberHiringLog.HiringLevelLogId);

                    lblDocumentLink.Text = glName.Name;
                    lblDocumentType.Text = memberHiringLog.HiringLevelComment;
                    lblDocumentTitle.Text = memberHiringLog.UpdateDate.ToShortDateString() +" "+ memberHiringLog.UpdateDate.ToShortTimeString();
                    Member updatorInfo = Facade.GetMemberById(memberHiringLog.UpdatorId);
                    lblUserNameUpdator.Text = updatorInfo.FirstName + " " + updatorInfo.LastName;
                }
            }
        }
    

       
        private void Refresh(string strSelected, string strMessage)
        {
            if (base.CurrentUserRole == ContextConstants.ROLE_CANDIDATE || base.CurrentUserRole == ContextConstants.ROLE_CONSULTANT)
            {

                if (strSelected.IndexOf("photo") >= 0 || strSelected.IndexOf("video") >= 0)
                {
                    string strurl = Request.Url.ToString();
                    if (!StringHelper.IsBlank(strurl) && strurl.IndexOf('?') > 0)
                    {
                        int intTab = 0;
                        //if (revPhoto.Visible)
                        //    intTab = 2;
                        //else if (revVideo.Visible)
                        //    intTab = 1;

                        Helper.Session.Set("AlreadyMemberDocumentLoaded", strMessage + "+" + intTab.ToString());
                        string[] urlAndQueryString = strurl.Split('?');
                        string _pageUrl = urlAndQueryString[0];
                        Helper.Url.Redirect(_pageUrl, string.Empty, UrlConstants.PARAM_MEMBER_ID, Convert.ToString(_memberId));
                    }
                }
            }
        }
        #endregion
    }
}
