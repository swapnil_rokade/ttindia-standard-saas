﻿<%--
----------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewPanel.aspx
    Description: Interview Panel
    Created By:  Pravin khot
    Created On:  19/nov/2015
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date            Author            Modification
    -------------------------------------------------------------------------------------------------------------------------------------------------
      0.1        13/Jan/2016       pravin khot       Introduced by encodeURIComponent(text),decodeURIComponent(email)        
      0.2        13/May/2016       pravin khot       modify javascript functions- Check1(str) ,Check(str)                        
   -------------------------------------------------------------------------------------------------------------------------------------------------------
--%> 
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterviewPanel.ascx.cs" Inherits="TPS360.Web.UI.ControlsInterviewPanel" %>

<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<link href="../assets/css/chosen.css" rel="Stylesheet" />
<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>
<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.0.min.js"></script>

<script type="text/javascript" language="javascript">
    window.onload = function() {
        document.getElementById("<%= txtpanelname.ClientID %>").value = "";
        document.getElementById("<%= txtSkillName.ClientID %>").value = "";
        document.getElementById("<%= hfOtherInterviewers.ClientID %>").value = "";
        document.getElementById("<%= hfSkillSetNames.ClientID %>").value = "";
        document.getElementById('<%= hfSkillSetNames.ClientID %>').val() == '';
    };
</script>
 <style>
    .TableFormLeble
    {
        width: 41%;
    }
    
    .EmailDiv
    {
        vertical-align: middle;
        text-align: center;
        border: solid 1px #CCC;
        padding-bottom: 2px;
        padding-top: 2px;
        margin-right: 4px;
        margin-bottom: 4px;
        width: auto;
        float: left;
        background-image: -moz-linear-gradient(center top , #FFFFFF 20%, #F6F6F6 50%, #EEEEEE 52%, #F4F4F4 100%);
        border-radius: 3px;
    }
     </style>

<script language="javascript" type="text/javascript">

    function Clicked(email, parent) {
        //change by pravin khot using skill space remove
        email = decodeURIComponent(email);      
        //*************End*******************************    
        $('#<%= hfSkillSetNames.ClientID %>').val($('#<%= hfSkillSetNames.ClientID %>').val().replace('<item><skill>' + email + '</skill></item>', '').replace(',,', ','));
        $('#<%= hfOtherInterviewers.ClientID %>').val($('#<%= hfOtherInterviewers.ClientID %>').val().replace(email, '').replace(',,', ','));
    }
      
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    
    //*****************************************************************************
    function CheckExistanceOfOtherEmail(curretEmail) {
        var alreadyavailablee = false;
        var alreadyadded = document.getElementById("<%= hfOtherInterviewers.ClientID %>").value

        var emailarray = alreadyadded.split(",");

        var str = curretEmail.trim().toLowerCase();
        str = str.replace(/[,;]$/, '');

        for (var i = 0; i < emailarray.length; i++) {
            if (emailarray[i].trim() != '') {
                if (emailarray[i].trim().toLowerCase() == str) {
                    alreadyavailablee = true;
                    break;
                }
            }
        }
        return alreadyavailablee;
    }

    function checkcheckedInternalInterviewer() {

        var CHK = document.getElementById("<%=chkInternalInterviewer.ClientID%>");
        var checkbox = CHK.getElementsByTagName("input");
        var label = CHK.getElementsByTagName("label");

        for (var i = 0; i < checkbox.length; i++) {
            if (checkbox[i].checked) {
                var prk = label[i].innerHTML;
                var last = prk.lastIndexOf(']');
                var first = prk.lastIndexOf('[');
                first = first + 1;
                var email = prk.substring(first, last);

                if (CheckExistanceOfOtherEmail(email)) {
                    alert(label[i].innerHTML + " This Interviewer Allready Added in Other Interviewers");
                    checkbox[i].checked = false;
                    return;
                    break;
                }

                if (CheckemailClientInterviersSI(email)) {
                    alert(label[i].innerHTML + " This Interviewer Allready Selected In BU Interviewers Panel");
                    checkbox[i].checked = false;
                    return;
                    break;
                }
             }
        }
    }
    function CheckemailClientInterviersSI(email) {
        var already = false;
        var CHK2 = document.getElementById("<%=chkClientInterviewer.ClientID%>");
        var checkbox2 = CHK2.getElementsByTagName("input");
        var label2 = CHK2.getElementsByTagName("label");
        var all = checkbox2.length;
     
        if (all > 0) {
            for (var i = 0; i < checkbox2.length; i++) {
                if (checkbox2[i].checked) {
                    var prk2 = label2[i].innerHTML;
                    var last2 = prk2.lastIndexOf(']');
                    var first2 = prk2.lastIndexOf('[');
                    first2 = first2 + 1;
                    var email2 = prk2.substring(first2, last2);

                    if (email2.trim().toLowerCase() == email.trim().toLowerCase()) {
                        already = true;
                        break;
                    }
                }
            }
        }
        return already;
    }


    // *************************************************************************

    function checkcheckedClientInterviewer() {
        var CHKII = document.getElementById("<%=chkClientInterviewer.ClientID%>");
        var checkboxII = CHKII.getElementsByTagName("input");
        var labelII = CHKII.getElementsByTagName("label");
        for (var i = 0; i < checkboxII.length; i++) {
            if (checkboxII[i].checked) {
                var prkII = labelII[i].innerHTML;
                var lastII = prkII.lastIndexOf(']');
                var firstII = prkII.lastIndexOf('[');
                firstII = firstII + 1;
                var emailII = prkII.substring(firstII, lastII);
                if (CheckExistanceOfOtherEmail(emailII)) {
                    alert(labelII[i].innerHTML + " This Interviewer Allready Added in Other Interviewers");
                    checkboxII[i].checked = false;
                    return;
                    break;
                }

                if (CheckemailInternalInterviewerS(emailII)) {
                    alert(labelII[i].innerHTML + " This Interviewer Allready Selected In User Interviewers");
                    checkboxII[i].checked = false;
                    return;
                    break;
                }              

            }
        }
    }
    function CheckemailInternalInterviewerS(emailII) {
        var prkS = "";
        var lastS = "";
        var firstS = "";
        var emailS = "";

        var already = false;
        var CHKS = document.getElementById("<%=chkInternalInterviewer.ClientID%>");
        var checkboxS = CHKS.getElementsByTagName("input");
        var labelS = CHKS.getElementsByTagName("label");

        for (var i = 0; checkboxS.length; i++) {
            if (checkboxS[i].checked) {
                prkS = labelS[i].innerHTML;
                lastS = prkS.lastIndexOf(']');
                firstS = prkS.lastIndexOf('[');
                firstS = firstS + 1;
                emailS = prkS.substring(firstS, lastS);

                if (emailS.trim().toLowerCase() == emailII.trim().toLowerCase()) {
                    already = true;
                    break;
                }

            }

        }
        return already;
    }
    
    //*************************************End************************************

   

    function Check(str) {
        var already = false;
        var chkListaTipoModificaciones = document.getElementById('<%= chkInternalInterviewer.ClientID %>');
        if (chkListaTipoModificaciones != null && chkListaTipoModificaciones.value == '') { //new condition added by pravin khot on 13/May/2016
            var chkLista = chkListaTipoModificaciones.getElementsByTagName("input");
            var label = chkListaTipoModificaciones.getElementsByTagName("label");
            for (var i = 0; i < chkLista.length; i++) {
                if (chkLista[i].checked) {
                    var prk = label[i].innerHTML;
                    var last = prk.lastIndexOf(']');
                    var first = prk.lastIndexOf('[');
                    first = first + 1;
                    var email = prk.substring(first, last);
                    if (email.trim().toLowerCase() == str) {
                        already = true;
                        break;
                    }
                }
            }
        }

        else {

        }       
         return already;
     }
     function Check1(str) {
         var already = false;
         var chkListaTipoModificaciones = document.getElementById('<%= chkClientInterviewer.ClientID %>');
         if (chkListaTipoModificaciones != null && chkListaTipoModificaciones.value == '') { //new condition added by pravin khot on 13/May/2016
             var chkLista = chkListaTipoModificaciones.getElementsByTagName("input");
             var label = chkListaTipoModificaciones.getElementsByTagName("label");
             for (var i = 0; i < chkLista.length; i++) {
                 if (chkLista[i].checked) {
                     var prk = label[i].innerHTML;
                     var last = prk.lastIndexOf(']');
                     var first = prk.lastIndexOf('[');
                     first = first + 1;
                     var email = prk.substring(first, last);
                     if (email.trim().toLowerCase() == str) {
                         already = true;
                         break;
                     }
                 }
             }
         }
         else {

         }        
         return already;
     }
    function CheckExistanceOfEmail(curretEmail, alreadyadded) {
        var alreadyavailablee = false;
        var emailarray = alreadyadded.split(",");
        var str = curretEmail.trim().toLowerCase();
        str = str.replace(/[,;]$/, '');      
        for (var i = 0; i < emailarray.length; i++) {
            if (emailarray[i].trim() != '') {               
                                       
                if (emailarray[i].trim().toLowerCase() == str) {
                    alreadyavailablee = true;
                    break;
                }
            }
            if (Check(str)) {
                alreadyavailablee = true;
                break;
            }
            if (Check1(str)) {
                alreadyavailablee = true;
                break;
            }    
        }
        return alreadyavailablee;
    }

    function SkillAlreadyAdded(skill) {
        var result = false;
        var skilllist = $('#<%= hfSkillSetNames.ClientID %>').val().replace(/&/g, "-");
        xmlDoc = $.parseXML("<SkillList>" + skilllist + "</SkillList>");
        var $xml = $(xmlDoc);

        $xml.find("item").each(function() {

            if ($(this).find('skill').text().trim().toLowerCase() == skill.trim().toLowerCase().replace(/&/g, "-")) result = true;
        });
        return result;
    }


    Sys.Application.add_load(function() {
        $('.close').click(function() {
        });
        $('#<%= btnAddSkill.ClientID %>').click(function() {

            var txtSkillName = $('#<%= txtSkillName.ClientID %>');
            if (txtSkillName.val().trim() != '') {
                if (!SkillAlreadyAdded(txtSkillName.val())) {
                    var text = txtSkillName.val();
                    $('#<%= divContent.ClientID %>').append("<div class='EmailDiv'><span>" + text + "</span><button type='button' class='close' data-dismiss='alert' onclick=javascript:Clicked('" + encodeURIComponent(text) + "')>\u00D7</button></div>"); //encodeURIComponent(text) code change by pravin khot on 13/Jan/2016
                    $('#<%= hfSkillSetNames.ClientID %>').val($('#<%= hfSkillSetNames.ClientID %>').val() + '<item><skill>' + text + '</skill></item>');
                    txtSkillName.val('');
                }
                else ShowMessage('<%= lblMessage.ClientID %>', 'Skill already added', true);
            }
            return false;
        });

        $('#<%= txtOtherInterviewers.ClientID %>').keyup(function(event) {
            var key = window.event ? event.keyCode : event.which;
            var index = 0;
            if ($(this).val().toLowerCase().indexOf(";") > 0) index = $(this).val().toLowerCase().indexOf(";");
            else if ($(this).val().toLowerCase().indexOf(",") > 0) index = $(this).val().toLowerCase().indexOf(",");
            else if (key == 13) index = $(this).val().trim().length;

            if (index > 0) {
                if (validateEmail($(this).val().substring(0, index))) {
                    var text = $(this).val();
                    if (!CheckExistanceOfEmail($(this).val(), $(this).next().val())) {
                        $(this).parent().prepend("<div class='EmailDiv'><span>" + text.substring(0, index) + "</span><button type='button' class='close' data-dismiss='alert' onclick=javascript:Clicked('" + text.substring(0, index) + "')>\u00D7</button></div>");
                        $(this).val(text.substring(index + 1));
                        $(this).next().val($(this).next().val() + ',' + text.substring(0, index));
                    }
                    else
                        ShowMessage('<%= lblMessage.ClientID %>', 'This Email already added', true);
                    $(this).val('');
                }
                // ShowMessage('<%= lblMessage.ClientID %>', 'Please Enter Valid Email-Id', true);              
                return false;
            }
        });

        $('#<%= txtOtherInterviewers.ClientID %>').focusout(function() {

            var index = $(this).val().length;
            if (index > 0) {
                if (validateEmail($(this).val().substring(0, index))) {
                    var text = $(this).val();
                    if (!CheckExistanceOfEmail($(this).val(), $(this).next().val())) {
                        $(this).parent().prepend("<div class='EmailDiv'><span>" + text.substring(0, index) + "</span><button type='button' class='close' data-dismiss='alert'>\u00D7</button></div>");
                        $(this).val(text.substring(index + 1));
                        $(this).next().val($(this).next().val() + ',' + text.substring(0, index));
                    }
                    else
                        ShowMessage('<%= lblMessage.ClientID %>', 'This Email already added', true);
                    $(this).val('');
                }
            }
        });
    });

    function RemoveRow(row) {
        
        $(row).parent().parent().remove();
    }


  Sys.Application.add_load(function() {
        $('input[rel=Numeric]').keypress(function(event) { return allownumeric(event, $(this)); });
        $('input[rel=Integer]').keypress(function(event) { return allowInteger(event, $(this)); });
       
    });
    function RemoveDefaultText(e) {
       
        if (e.value == e.title) { e.value = ''; e.className = 'CommonTextBox'; }
    }
    function SetDefaultText(e) {
        if (e.value == '') {
            e.value = e.title;
            e.className = 'text-label';
        }
    }
    function ValidateSkill(souce, eventArgs) {
       
        if ($('#<%= hfSkillSetNames.ClientID %>').val() == '')
        
            eventArgs.IsValid = false;
    }

    function MakeEnableDisable(option, control, clearSelectIndex) {
        var _Control = document.getElementById(control);
        _Control.disabled = option;
        if (option && clearSelectIndex) {
            _Control.selectedIndex = 0;
        }
    }        
   
    function checkEnter(e) {
        try {
            var keycode;
            if (window.event) keycode = window.event.keyCode;
            else if (event) keycode = event.keyCode;
            else if (e) keycode = e.which;
            else return true;

            if ((keycode == 13)) {
                var s;
                s = document.getElementById(btnSave);
                s.UseSubmitBehaviour = true;

                //__doPostBack('btnSave','OnClick');
                return true;
            }
            else {
                return false;
            }
            return true;
        }
        catch (e1) {
        }
    }

    //0.7 statrs

   
   

    function FilterNumeric(txtID) {

        var txt = document.getElementById(txtID);
        var t = txt.value + '';
        for (var i = 0; i < t.length; i++) {
            var st = t.substr(i, 1);
            if (st >= 0 && st <= 9) continue;
            else { txt.value = ""; break; }
        }

    }

    function numeric_only(evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;

    }
    function Decimal_only(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if ((charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46))
            return false;

        return true;
    }


    function LockWheel() {
        document.onmousewheel = function() { stopWheel(); } /* IE7, IE8 */
        if (document.addEventListener) { /* Chrome, Safari, Firefox */
            document.addEventListener('DOMMouseScroll', stopWheel, false);
        }
    }

    function stopWheel(e) {
        if (!e) { e = window.event; } /* IE7, IE8, Chrome, Safari */
        if (e.preventDefault) { e.preventDefault(); } /* Chrome, Safari, Firefox */
        e.returnValue = false; /* IE7, IE8 */
    }


    function ReleaseWheel() {
        document.onmousewheel = null;  /* IE7, IE8 */
        if (document.addEventListener) { /* Chrome, Safari, Firefox */
            document.removeEventListener('DOMMouseScroll', stopWheel, false);
        }
    }

   
         //0.7  ends
</script>
   

 <asp:TextBox ID="txtSortColumn" runat="server" Visible="false"></asp:TextBox>
<asp:TextBox ID="txtSortOrder" runat="server" Visible="false"></asp:TextBox>
<div>
<div>
    <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div class="TabPanelHeader" style="">
        Add Panel
    </div>
    <div class="TableRow">
        <div class="TableFormLeble" >
            <asp:Label ID="lblpanel" runat="server" Text="Interview Panel"></asp:Label>
        </div>
               <asp:TextBox ID="txtpanelname" runat="server" style="width :150px" TabIndex="1" CssClass="CommonTextBox" AutoComplete="OFF"
                            EnableViewState="true"></asp:TextBox>
                            
       
             <span class="RequiredField">*</span><asp:Label ID="lblid" runat="server"  Visible="false" Text=""></asp:Label>
        </div>
        </div>
         <div class="TableRow">
            <div class="TableFormValidatorContent" style="margin-left: 41%">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="IP" ControlToValidate="txtpanelname" ErrorMessage="Please Insert Panel Name" ></asp:RequiredFieldValidator>
        
        </div>
        </div>
 
        
    <asp:UpdatePanel ID="upSkill" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlSkill" runat="server" DefaultButton="btnAddSkill">
                            <div class="TableRow">
                                <div class="TableFormLeble">
                                    <asp:Label ID="lblSkills" runat="server" Text="Required Skills"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <div id="divskill">
                                    </div>
                                    <asp:HiddenField ID="hfSkillSetNames" runat="server" Value="" />
                                    <asp:TextBox ID="txtSkillName" runat="server" AutoPostBack="false" CssClass="CommonTextBox"
                                        TabIndex="2" AutoComplete="OFF" onclick="LockWheel();" onBlur="ReleaseWheel();" />
                                    <ajaxToolkit:AutoCompleteExtender ID="txtSkillName_AutoCompleteExtender" runat="server"
                                        ServicePath="~/AjaxExtender.asmx" TargetControlID="txtSkillName" MinimumPrefixLength="1"
                                        ServiceMethod="GetSkills" EnableCaching="True" CompletionInterval="0" CompletionListCssClass="AutoCompleteBox"
                                        FirstRowSelected="True" CompletionListElementID="divskill" />
                                    <span class="RequiredField">*</span>
                                    <asp:Button ID="btnAddSkill" runat="server" Text="Add" CssClass="CommonButton" 
                                        TabIndex="3"  />
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="TableRow">
                            <div class="TableFormValidatorContent" style="margin-left: 41%">
                                <asp:CustomValidator ID="cvSkill" runat="server" ValidationGroup="IP"
                                    Display="Dynamic" ClientValidationFunction="ValidateSkill" ErrorMessage="Please select atleast one skill"></asp:CustomValidator>
                            </div>
                        </div>
                        <div class="TableRow">
                            <div class="TableFormLeble">
                            </div>
                            <div class="TableFormContent" id="divContent" runat="server" enableviewstate="true"
                                style="margin-left: 42% ; width :160px">
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                
                
                   
                 <div class="TableRow" style="text-align: left ;width:100%">                               
                              <table>
                               
                              <tr>      
                             
                                                          
                               
                              <td>                      
                                      <div class="TableRow" style="text-align: left">                                                            
                              
                                    <asp:Label EnableViewState="false" ID="lblInternalInterviewer" runat="server" Text="Users"
                                        Style="white-space: normal;"></asp:Label>:
                               
                                    <div style="float: left;">
                                        <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                            width: 420px; height: 100px; overflow: auto">
                                            <asp:CheckBoxList ID="chkInternalInterviewer" runat="server" AutoPostBack="false" OnClick="javascript:checkcheckedInternalInterviewer(this.id)"
                                                ValidationGroup="IP" TabIndex="5">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                                  
                                
                            
                                         </div> 
                                                                     
                                
                              </td>
                              
                              
                              
                              <td>
                              
                              
                               <div class="TableRow" style="text-align: left">                                                            
                              
                                    <asp:Label EnableViewState="false" ID="Label1" runat="server" Text="BUI"
                                        Style="white-space: normal;"></asp:Label>:
                               
                                    <div style="float: left;">
                                        <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                            width: 420px; height: 100px; overflow: auto">
                                            <asp:CheckBoxList ID="chkClientInterviewer" runat="server" AutoPostBack="false" ValidationGroup="IP" OnClick="javascript:checkcheckedClientInterviewer(this.id)"
                                            TabIndex="6">
                                        </asp:CheckBoxList>
                                        </div>
                                    </div>
                                
                                         </div>       
                              
                            
                                                                                      
                              </td>
                              
                              
                              
                             <td>
                             
                              <div class="TableRow" style="text-align: left">                                                            
                              
                                    <asp:Label EnableViewState="false" ID="Label2" runat="server" Text="OTHER USERS"
                                        Style="white-space: normal;"></asp:Label>:
                               
                                   <div style="float: left;">
                                        <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                            width: 400px; height: 100px; overflow: auto">
                                                                                                                                                
                                           
                                <div class="TableRow"  style ="margin-top :2px;">
                            
                               
                                    <div style="width: 90%; height: auto; border: solid 1px #CCC; margin-left: 2%; padding: 4px;"
                                        id="divContentip" runat="server" enableviewstate="true">
                                        <asp:TextBox EnableViewState="false" ID="txtOtherInterviewers" runat="server" CssClass="CommonTextBox"
                                            TabIndex="4" placeholder="Separate emails by commas" Width="95%">   </asp:TextBox>
                                        <asp:HiddenField ID="hfOtherInterviewers"  runat="server"  Value=""  />
                                    
                                    </div>
                              
                            </div>                                       
                                            
                                            
                                            
                                            
    
        
        
  
        
        
    
        
     
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
     
             
  
                                        
                                        
                                        
                                        </div>
                                    </div>
                                                               
                               
                                                                       
                                                               
                              </td>
                              </tr>
                              </table>                
                 </div>      
     <br />
   
    <div class="TableRow" style="text-align: center">
        <asp:Button ID="btnSave" CssClass="CommonButton" runat="server"  TabIndex ="7" Text="Save" ValidationGroup="IP"
            OnClick="btnSave_Click" />
    </div>
    
    
    <br />
    <div class="TabPanelHeader">
        List of Panels
    </div>
    <div class="GridContainer" style="text-align: left;">
        <asp:ObjectDataSource ID="odsInterviewPanelList" runat="server" 
            EnablePaging="false" SelectMethod="GetAllInterviewPanel"
            TypeName="TPS360.Web.UI.InterviewPanelDataSource" 
            SortParameterName="sortExpression">
                
           <SelectParameters>            
            
             </SelectParameters>
        </asp:ObjectDataSource>           
           <asp:UpdatePanel>              
            <asp:ListView ID="lsvInterviewPanelList" runat="server" 
            DataSourceID="odsInterviewPanelList" DataKeyNames="Id" OnItemDataBound="lsvInterviewPanelList_ItemDataBound" 
                onPreRender="lsvInterviewPanelList_PreRender" EnableViewState="true" 
            onitemcommand="lsvInterviewPanelList_ItemCommand" >
            <layouttemplate>
                <table id="tlbTemplate" runat="server"  class="Grid" cellspacing="0" border="0">
                    <tr>
                        <th style="white-space: nowrap; width :320px !important">
                            <asp:LinkButton ID="btnInterviewPanel" runat="server" ToolTip="Sort by Interview Panel" CommandName="Sort"  
                                CommandArgument="[Q].[InterviewPanel_Name]" Text="Interview Panel Name" />
                        </th>                       
                 
                        
                        <th style="white-space: nowrap;width :380px !important ">
                         
                                Required Skill
                        </th>
                        
                        <th style="white-space: nowrap;width :405px !important  ">
                               Interviewer Name
                         
                        </th>
                        
                      
                        
                      
                                                     
                        <th style="text-align: center;  " runat ="server" id="thAction"  width="100px" visible="True">Action</th>
                    </tr>
                                        
                            
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                    
                    <tr class="Pager">
                        <td id="tdpager" runat="server" colspan="5">
                            <ucl:Pager ID="pagerControl_Requisition" runat="server" EnableViewState="true" />
                        </td>
                    </tr>
                </table>
            </layouttemplate>
                <emptydatatemplate>
                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning"  enableviewstate ="false" >
                    <tr>
                        <td>
                            No data was returned.
                        </td>
                    </tr>
                </table>
            </emptydatatemplate>
                <itemtemplate>
                <tr id="row"  runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                      <td>
                        <asp:Label ID="lblpanelname" runat="server"  />
                        
                    </td>
                      <td>
                        <asp:Label ID="lblskill" runat="server"  />
                    </td>
                    <td>
                        <asp:Label ID="lblName" runat="server"  />
                    </td>
                                    
                 
                      
                    <td style="text-align: center;" runat ="server" id ="tdAction">
                        <asp:ImageButton ID="btnEdit" SkinID="sknEditButton"  runat="server" Visible="True"
                            CommandName="EditItem"></asp:ImageButton>
                        <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" Visible="True"
                            CommandName="DeleteItem"></asp:ImageButton>
                    </td>
                </tr>
            </itemtemplate>
            </asp:ListView>
           </asp:UpdatePanel>
       
    </div> 
</div>
