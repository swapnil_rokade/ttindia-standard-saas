﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:ProductivityReport.ascx
    Description By:This form is common for ProductivityReport.aspx and MyProductivityReport.aspx 
    Created By:Sumit Sonawane
    Created On:25/May/2016
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------

    ---------------------------------------------------------------------------------------------------------------------------------------     

--%>


<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductivityReport.ascx.cs"
    Inherits="TPS360.Web.UI.ControlProductivityReport" %>
<%@ Register Src="~/Controls/CheckedDropDownList.ascx" TagName="CheckedDropDownList"
    TagPrefix="ucl" %>
<%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>


<script src="../js/EmployeeTeam.js" type="text/javascript"></script>
<script src="../js/highcharts.js" type="text/javascript"></script>
<script src="../js/exporting.js" type="text/javascript"></script>
  
    <script type="text/javascript">
       Sys.Application.add_load(function () {
        
		        getReport();
		        getReportByDay();
            });
    function getReportByDay()
    {
    
    }
    function getReport()
    {
    try{
            var ddlUser=document .getElementById ('<%= hdnUserSelectedValue.ClientID %>');
            var ddlTeam=document .getElementById ('<%= ddlTeam.ClientID %>');
            var hdnStartDate=document .getElementById ('<%= hdnStartDate.ClientID %>');
            var hdnEndDate=document .getElementById ('<%= hdnEndDate.ClientID %>');
            var hdnIsTeamReport=document .getElementById ('<%= hdnIsTeamReport.ClientID %>');
            var hdnCurrentUserId=document .getElementById ('<%= hdnCurrentUserId.ClientID %>');
            var requestUrl =  "../AJAXServers/GraphicalReport.aspx?RType=PRByE&TID=" + (ddlTeam ==null ?"0" : ddlTeam.options[ddlTeam.selectedIndex].value)  + "&UID=" +  ddlUser.value + "&SD=" + hdnStartDate.value + "&ED=" + hdnEndDate.value+ "&isTeamReport=" + hdnIsTeamReport.value + "&CurrentUserId=" + hdnCurrentUserId.value;
            
            CreateXmlHttp();
	        if(XmlHttp)
	        {
	        try {
		            XmlHttp.onreadystatechange = HandleResponseExpanded;
		            XmlHttp.open("GET", requestUrl,  true);
		            XmlHttp.send(null);		
		        }
		        catch(e)
				{
		    
				}
				
			}
            
            
            
            
    }
    
        catch(e)
        {
            alert(e.message);
        }
       
    }
    
    function CreateXmlHttp()
{
	try
	{
		XmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		catch(oc)
		{
			XmlHttp = null;
		}
	}
	if(!XmlHttp && typeof XMLHttpRequest != "undefined") 
	{
		XmlHttp = new XMLHttpRequest();
	}
}

function HandleResponseExpanded()
{
    if(XmlHttp.readyState == 4)
    {
        try{
            if(XmlHttp.status == 200)
            {			
                var t1 = XmlHttp.responseText;
                LoadGraphData(t1);
                LoadGraphDataByDay(t1);
            }
            else
            {
                alert("There was a problem retrieving data from the server." );
            }
        }
        catch(e)
        {
            alert(e.Message);
        }
    }
}
    function LoadGraphData(t1)
    {
          var options = {
	            chart: {
	                renderTo: 'visualization',
	                type: 'bar'
	                
	                
	            },
	            title: {
	                text: 'Productivity Report',
	                x: -20 
	            },
	            subtitle: {
	                text: '',
	                x: -20
	            },
	            xAxis: {
	                allowDecimals: false,
	                categories: [],
	                
                    labels: {
                            style: {
                            fontSize: '9px',
                            width: '175px'
                            }
                       },
                title: {
                    text: ''
                    }
	            },
	            yAxis: {
	                min: 0,
	                
	                title: {
	                    text: ''
	                },
	                labels: {
	                    overflow: 'justify'
	                }
	            },
	            tooltip: {
	                formatter: function() {
	                        return '<b>'+ this.series.name +'</b><br/>'+
	                        this.x +': '+ this.y;
	                }
	            },
	            legend: {
	                layout: 'vertical',
	                align: 'right',
	                verticalAlign: 'top',
	                x: -10,
	                y: 100,
	                borderWidth: 0
	            }, 
	            plotOptions: {
	             series: {
	                        pointWidth: 6
                           
	                     },
	                bar: {
	                    dataLabels: {
	                        enabled: false
	                    }
	                }
	            },
	            credits: {
                enabled: false
            },
	            series: []
	        }
	        
	           var x = "system";
               
	          
               var ob = $.parseJSON(t1);
               var byEmp=ob["byEmployee"];
               var byDay = ob["byDay"];
               
               //console.log(data.length * 20 + 120);
               var empcount=0;
               var i=0;
                  $.each(byEmp, function(key, subject) {
                        if(subject.name =='User')
                        {
                            empcount=subject.empList.length;
                            options.xAxis.categories =  subject.empList;
                        }
                        else
                        {
                            var f = { name:subject.name , data:subject.data };
                            options.series[i]=f;
                        }
                           
                    i++;
                    });
	           
                
	            options.chart.height= empcount>7 ? empcount *70 : 450;
	        	
		        chart = new Highcharts.Chart(options);
	      
		        
	            
		
		
    }
    function LoadGraphDataByDay(t1)
    {
        try
        {
            var options = {
	            chart: {
				renderTo: 'ProductivityByDay',
                type: 'spline'
                
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                title: {
                    text: 'Snow depth (m)'
                },
                min: 0
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y} '
                
            },
            credits: {
                enabled: false
            },
                series: []
	        }
	        
	           var x = "system";
               
	          
               var ob = $.parseJSON(t1);
               var byEmp=ob["byEmployee"];
               var byDay = ob["byDay"];
               
               
               
               
               var o;
               var i=0;
                  $.each(byDay, function(key, subject) {
                     
                     
                     
                      o = subject.data;
                      var obj = [];
                      
                      $.each(o, function(k, s){
                        
                        obj.push(new Array(Date.UTC(s.year,s.month,s.date),s.value));
                         
                      });
                      var f = { name:subject.name , data:obj };
                      
                      options.series[i] = f;
                      i++;
                    });
	        	
		        chart = new Highcharts.Chart(options);
		        
		        
		        
        }catch(e)
        {
            alert(e.message);
        }
            
    }
    $(window).resize(function() 
    {    
        chart.setSize(
       
        );   
});
    </script>
   
    
    <link href="../Style/TabStyle.css" rel="Stylesheet" type="text/css" />
    <asp:HiddenField ID="hdnAppType" runat ="server" />
    <asp:HiddenField ID="hdnScrollPos" runat="server" />
    <ucl:CheckedDropDownList ID="uclCheckedDropDown" runat="server" Visible="false" />
    <asp:TextBox ID="hdnSortColumn" runat="server" EnableViewState="true" Visible="false"
        Text="btnUser" />
    <asp:TextBox ID="hdnSortTeamColumn" runat="server" EnableTheming="true" Visible="false"
        Text="btnTeam" />
    <asp:TextBox ID="hdnProListSortColumn" runat="server" Visible="false" Text="[E].[EmployeeName]"></asp:TextBox>
    <asp:TextBox ID="hdnProListSortOrder" runat="server" Visible="false" Text="ASC"></asp:TextBox>
    <asp:TextBox ID="hdnProListSortColumnByTeam" runat="server" Visible="false" Text="[ET].[Title]"></asp:TextBox>
    <asp:TextBox ID="hdnProListSortOrderByTeam" runat="server" Visible="false" Text="ASC"></asp:TextBox>
    <asp:TextBox ID="hdnSortOrder" runat="server" EnableViewState="true" Visible="false"
        Text="ASC" />
    <asp:TextBox ID="hdnSortTeamOrder" runat="server" EnableViewState="true" Visible="false"
        Text="ASC" />
    <div class="well" style="clear: both; padding-top: 0px;">
        <div class="TabPanelHeader">
            Filter Options</div>
        <div style="text-align: center">
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <asp:HiddenField ID="hdnStartDate" runat="server" />
                    <asp:HiddenField ID="hdnEndDate" runat="server" />
                    <asp:HiddenField ID="hdnIsTeamReport" Value="false" runat="server" />
                    <asp:HiddenField ID="hdnCurrentUserId" runat="server" />
                    <asp:ObjectDataSource ID="odsProductivityList" runat="server" SelectMethod="GetPagedEmployeeProductivityReportByEmployee"
                        TypeName="TPS360.Web.UI.EmployeeDataSource" SelectCountMethod="GetListCountEmployeeProductivityReportByEmployee"
                        EnablePaging="True" SortParameterName="sortExpression">
                        
                        <SelectParameters>
                       
                            <asp:ControlParameter ControlID="dtReferralDate" Name="StartDate" PropertyName="StartDate"
                                Type="String" />
                            <asp:ControlParameter ControlID="dtReferralDate" Name="EndDate" PropertyName="EndDate"
                                Type="String" />
                            <asp:ControlParameter ControlID="ddlTeam" Name="TeamList" PropertyName="SelectedValue"
                                Type="String" />
                            <asp:ControlParameter ControlID="hdnUserSelectedValue" Name="User" PropertyName="Value"
                                Type="String" />
                            <asp:ControlParameter ControlID="hdnProListSortOrder" Name="SortOrder" Type="String" />
                            <asp:Parameter Name="IsTeamProductivityReport" Type="Boolean" DefaultValue="false" />
                            <asp:Parameter Name="TeamLeaderid" Type="Int32" DefaultValue="0" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:Label ID="lblDateRange" runat="server" Text="Date Range:"></asp:Label>
                    <ucl:DateRangePicker ID="dtReferralDate" runat="server" />
                    <div id="divTeam" runat="server" style =" display :inline">
                        <asp:Label ID="lblTeam" runat="server" Text="Team:"></asp:Label>
                        <asp:DropDownList ID="ddlTeam" runat="server" AutoPostBack="false" CssClass="CommonDropDownList">
                        </asp:DropDownList>
                    </div>
                    <asp:Label ID="lblUser" runat="server" Text="User:"></asp:Label>
                    <asp:DropDownList ID="ddlUser" runat="server" CssClass="CommonDropDownList">
                    </asp:DropDownList>
                    <asp:HiddenField ID="hdnUserSelectedValue" runat="server" Value="0"  />
                    <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btn btn-primary"
                        OnClick="btnApply_Click" />
                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn" OnClick="btnReset_Click" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="tabbable">
        <ul class="nav nav-pills">
            <li class="active"><a href="#tab1" data-toggle="tab">Summary By User</a> </li>
            <li><a href="#tab2" data-toggle="tab">By Day</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
            <div id="viscontainer" style="width:100%">
            <div id="visualization" style="margin: 0 auto; width: 800px; min-height: 300px; max-height: 500px; overflow: auto; overflow-x: hidden; ">
                </div>
            </div>
                
            </div>
            <div class="tab-pane" id="tab2">
                <div id="ProductivityByDay" style="width: 800px; height: 400px; margin: 0 auto">
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="upReport" runat="server">
        <ContentTemplate>
            <div style="overflow: inherit" id="bigDiv" onscroll='SetScrollPosition()'>
                <div style="padding-bottom: 40px">
                
                <div class="TableRow" style="padding-bottom: 5px" id="divExportButtons" runat="server"
                            visible="true">
                            <div class="TabPanelHeader nomargin" style="margin-bottom: 5px;border-style: none none none;">Report Results</div>
                            <div class="TabPanelHeader">
                              Summary by User 
                </div>
                
                <asp:ImageButton ID="btnExportToExcel" CssClass ="btn"  SkinID ="sknExportToExcel" runat ="server" ToolTip ="Export To Excel" OnClick ="btnExportToExcel_Click" />
                            <asp:ImageButton ID="btnExportToPDF" CssClass ="btn"  SkinID ="sknExportToPDF" runat ="server" ToolTip ="Export To PDF" OnClick ="btnExportToPDF_Click" />
                            <asp:ImageButton ID="btnExportToWord" CssClass ="btn"  SkinID ="sknExportToWord" runat ="server" ToolTip ="Export To Word" OnClick ="btnExportToWord_Click" />
                             </div>
                             
                            
              
                
                    <asp:ListView ID="lsvProductivityReport" runat="server" DataKeyNames="Id" EnableViewState="true"
                        DataSourceID="odsProductivityList" OnItemCommand="lsvProductivityReport_ItemCommand"
                        OnItemDataBound="lsvProductivityReport_ItemBound" OnPreRender="lsvProductivityReport_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                                <tr runat="server" id="trPrecise">
                                    <th>
                                        <asp:LinkButton ID="btnUser" runat="server" CommandName="Sort" ToolTip="Sort By User" CommandArgument="[E].[EmployeeName]"
                                            Text="User" TabIndex="2" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="lnkNewCandidates" runat="server" CommandName="Sort" ToolTip="Sort By New Candidates Count" CommandArgument="NoOfNewCandidates"
                                            Text="New Candidates" TabIndex="3" EnableViewState="false" style="min-width: 130px" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="lnkRequisitionPublished" runat="server" CommandName="Sort" ToolTip="Sort By Requisition Published Count"  CommandArgument="NoOfRequisitionPublished"
                                            Text="Requisitions" TabIndex="4" Width="100%"/>
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnSourced" runat="server" CommandName="Sort" ToolTip="Sort By Sourced Count" CommandArgument="NoOfCandidateSourced"
                                            Text="Sourced" TabIndex="5" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnSort" runat="server" CommandName="Sort" ToolTip="Sort By Submissions Count" CommandArgument="NoOfCandidatesSubmissions"
                                            Text="Submissions" TabIndex="6" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="lnkInterviews" runat="server" Text="Interviews" ToolTip="Sort By Interviews Count"
                                            CommandName="Sort" CommandArgument="NoOfInterviews"></asp:LinkButton>
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="lnkoffers" runat="server" Text="Offers" ToolTip="Sort By Offers Count"
                                            CommandArgument="NoOfCandidatesOffers" CommandName="Sort"></asp:LinkButton>
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="btnoffersrejected" runat="server" Text="Offers Decline" CommandName="Sort" ToolTip="Sort By Offers Decline Count"
                                            CommandArgument="NoOfCamdidateOffersRejected" TabIndex="7" style="min-width: 140px" />
                                    </th>
                                    <th >
                                        <asp:LinkButton ID="btnjoined" runat="server" Text="Joined" CommandName="Sort" ToolTip="Sort By Joined Count" CommandArgument="NoOfCandidateJoined"
                                            TabIndex="7" />
                                    </th>
                                    <th id="thPendingJoiners" runat ="server" >

                                     <asp:LinkButton ID="btnjoiners" runat="server" Text="Pending Joiners" CommandName="Sort" ToolTip="Sort By Pending Joiners Count" CommandArgument="PendingJoiners"

                                            TabIndex="8"  />
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="9" style="padding: 0px 0px 0px 0px; border-style: none none none none">
                                    </td>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td colspan="9"  runat="server" id="tdPager">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" NoOfRowsCookie="ProductivityReportbyEmployeeRowPerPage" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                <td>
                                    <asp:Label ID="lblUser" runat="server" Width="80px" />
                                </td>
                                <td>
                                    <asp:Label ID="lblNewCandidates" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblRequisitionPublished" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblSourced" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblSubmissions" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblInterviews" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblOffers" runat="server" Style="min-height: 100px;" />
                                </td>
                                <td>
                                    <asp:Label ID="lblOfferrejected" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblJoined" runat="server" />
                                </td>
                                <td  id="tdPendingJoiners"  runat ="server" >
                                    <asp:Label ID="lblPendingJoiners" runat="server"  />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                    </div>
                    </asp:Panel>
                
                </div>
                <asp:ObjectDataSource ID="odsProductivityTeamList" runat="server" SelectMethod="GetPagedEmployeeProductivityReportByTeam"
                    TypeName="TPS360.Web.UI.EmployeeDataSource" SelectCountMethod="GetListCountEmployeeProductivityReportByTeam"
                    EnablePaging="True" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="dtReferralDate" Name="StartDate" PropertyName="StartDate"
                            Type="String" />
                        <asp:ControlParameter ControlID="dtReferralDate" Name="EndDate" PropertyName="EndDate"
                            Type="String" />
                        <asp:ControlParameter ControlID="ddlTeam" Name="TeamList" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnUserSelectedValue" Name="User" PropertyName="Value"
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnProListSortOrderByTeam" Name="SortOrder" Type="String" />
                        <asp:Parameter Name="IsMyProcuvtivityReport" DefaultValue="false" Type="Boolean" />
                        <asp:Parameter Name="IsTeamProductivityReport" DefaultValue="false" Type="Boolean" />
                        <asp:Parameter Name="TeamLeaderid" DefaultValue="0" Type="string" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
                <div class="TableRow" style="padding-bottom: 5px" id="divTeamExportButtons" runat="server"
                            visible="true">
                            
                            <div class="TabPanelHeader">
                              Summary by Team 
                </div>
                             <asp:ImageButton ID="teamExportToExcel" CssClass ="btn"  SkinID ="sknExportToExcel" runat ="server" ToolTip ="Export To Excel" OnClick ="teamExportToExcel_Click " />
                            <asp:ImageButton ID="teamExportToPDF" CssClass ="btn"  SkinID ="sknExportToPDF" runat ="server" ToolTip ="Export To PDF" OnClick ="teamExportToPDF_Click " />
                            <asp:ImageButton ID="teamExportToWord" CssClass ="btn"  SkinID ="sknExportToWord" runat ="server" ToolTip ="Export To Word" OnClick ="teamExportToWord_Click " />
                            
                            
                        </div>
                
                <asp:ListView ID="lsvteamReport" DataSourceID="odsProductivityTeamList" runat="server"
                    EnableViewState="true" DataKeyNames="Id" OnItemDataBound="lsvteamReport_ItemBound"
                    OnItemCommand="lsvteamReport_ItemCommand" OnPreRender="lsvteamReport_PreRender">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr id="trHeadLevel" runat="server">
                                <th>
                                    <asp:LinkButton ID="btnTeam" runat="server" CommandName="Sort" ToolTip="Sort By Team" CommandArgument="[ET].[Title]"
                                        Text="Team" TabIndex="2" />
                                </th>
                                <th >
                                    <asp:LinkButton ID="lnkNewCandidates" runat="server" CommandName="Sort" ToolTip="Sort By New Candidates Count" CommandArgument="NoOfNewCandidates"
                                        Text="New Candidates" TabIndex="3" EnableViewState="false" style="min-width: 130px"/>
                                </th>
                                <th>
                                    <asp:LinkButton ID="lnkRequisitionPublished" runat="server" CommandName="Sort" ToolTip="Sort By Requisition Published Count" CommandArgument="NoOfRequisitionPublished"
                                        Text="Requisitions" TabIndex="4" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnSourced" runat="server" CommandName="Sort" ToolTip="Sort By Sourced Count" CommandArgument="NoOfCandidateSourced"
                                        Text="Sourced" TabIndex="5" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnSort" runat="server" CommandName="Sort" ToolTip="Sort By Submissions Count" CommandArgument="NoOfCandidatesSubmissions"
                                        Text="Submissions" TabIndex="6" Width="100%" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="lnkInterviews" runat="server" Text="Interviews" ToolTip="Sort By Interviews Count"
                                        CommandName="Sort" CommandArgument="NoOfInterviews"></asp:LinkButton>
                                </th>
                                <th>
                                    <asp:LinkButton ID="lnkoffers" runat="server" Text="Offers" ToolTip="Sort By Offers Count"
                                        CommandArgument="NoOfCandidatesOffers" CommandName="Sort"></asp:LinkButton>
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnoffersrejected" runat="server" Text="Offers Decline" ToolTip="Sort By Offers Decline Count" CommandName="Sort"
                                        CommandArgument="NoOfCamdidateOffersRejected" TabIndex="7" style="min-width: 130px" />
                                </th>
                                <th>
                                    <asp:LinkButton ID="btnjoined" runat="server" Text="Joined" CommandName="Sort" ToolTip="Sort By Joined Count" CommandArgument="NoOfCandidateJoined"
                                        TabIndex="7" Width="50%" />
                                </th>
                                <th id="thPendingJoiners" runat ="server" >

                                    <asp:LinkButton ID="btnjoinersteam" runat="server" Text="Pending Joiners" CommandName="Sort" ToolTip="Sort By Pending Joiners Count" CommandArgument="PendingJoiners"
                                        TabIndex="8" Width="50%" />
                                </th>
                            </tr>
                            <tr>
                                <td colspan="9" style="padding: 0px 0px 0px 0px; border-style: none none none none">
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="9" runat="server" id="tdPager">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" NoOfRowsCookie="ProductivityReportbyTeamRowPerPage" />
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No data was returned.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td>
                                <asp:Label ID="lblTeam" runat="server" Width="80px" />
                            </td>
                            <td>
                                <asp:Label ID="lblNewCandidates" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblRequisitionPublished" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblsourced" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblSubmissions" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblInterviews" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblOffers" runat="server" Style="min-height: 100px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblofferrejected" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblJoined" runat="server" />
                            </td>
                            <td id="tdPendingJoiners" runat ="server" >
                                <asp:Label ID="lblPendingJoiners" runat="server"/>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToPDF" />
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
            <asp:PostBackTrigger ControlID="btnExportToWord" />
        </Triggers>
         <Triggers>
            <asp:PostBackTrigger ControlID="teamExportToExcel" />
            <asp:PostBackTrigger ControlID="teamExportToPDF" />
            <asp:PostBackTrigger ControlID="teamExportToWord" />
        </Triggers>
        
    </asp:UpdatePanel>
