﻿/* 
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmployeeList.ascx.cs
    Description: This is the user control page used to display Employee list.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Oct-13-2008          Jagadish N            Defect id: 8987; "Edit" icon in "Action" column is removed.
    0.2              Feb-15-2010          Nagarathna V.B        DefectId:12122; commented mylist deletion; 
 *  0.3              10/March/2016        pravin khot           Using user Disable when status InActive ,Event added- ddlStatus_SelectedIndexChanged
------------------------------------------------------------------------------------------------------------------------------------------- 
 */
using System;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Linq;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Collections.Generic ;
namespace TPS360.Web.UI
{
    public partial class cltEmployeeList : BaseControl
    {
        #region Member Variables

        private static int _managerId = 0, _organizationFunctionalityCategoryMapId = 0, _memberTierLookupId = 0;
        private static int _status = 0, _branchOfficeId = 0;
        private static bool _isMyList = false, _isMapping = false, _isTier = false;
        private bool _IsAccessToEmployee = true;
        private static string UrlForEmployee = string.Empty;
        private static int IdForSitemap = 0;
        private static bool _mailsetting = false;
        private string currentSiteMapId = "0";
        #endregion

        #region Properties

        public Int32 ManagerId
        {
            set { _managerId = value; }
        }

        public Int32 OrganizationFunctionalityCategoryMapId
        {
            set { _organizationFunctionalityCategoryMapId = value; }
        }

        public Int32 MemberTierLookupId
        {
            set { _memberTierLookupId = value; }
        }

        public Int32 BranchOfficeId
        {
            set { _branchOfficeId = value; }
        }

        public Int32 Status
        {
            set { _status = value; }
        }

        public String HeaderTitle
        {
            set { lblHeader.Text = value; }
        }

        public bool IsMyList
        {
            set { _isMyList = value; }
        }

        public bool IsMapping
        {
            set { _isMapping = value; }
        }

        public bool IsTier
        {
            set { _isTier = value; }
        }

        #endregion

        #region Methods

        private void BindList()
        {
            if (_isMyList)
            {
                odsEmployeeList.SelectParameters["managerId"].DefaultValue = _managerId.ToString();
            }
            else if (_isMapping)
            {
                odsEmployeeList.SelectParameters["organizationFunctionalityCategoryMapId"].DefaultValue = _organizationFunctionalityCategoryMapId.ToString();
            }
            else if (_isTier)
            {
                odsEmployeeList.SelectParameters["branchOfficeId"].DefaultValue = _branchOfficeId.ToString();
                odsEmployeeList.SelectParameters["tierLookupId"].DefaultValue = _memberTierLookupId.ToString();
            }

            odsEmployeeList.SelectParameters["status"].DefaultValue = _status.ToString();
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "FirstName";
            }
            odsEmployeeList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
       
            lsvEmployeeList.DataSourceID = "odsEmployeeList";

            this.lsvEmployeeList.DataBind();
        }

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvEmployeeList.FindControl( txtSortColumn .Text );
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                if (txtSortColumn.Text == "btnStatus")
                {
                    im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Descending" : "Ascending"));
                }
                else   im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            currentSiteMapId = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID].ToString();
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap=Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(360, CurrentMember.Id);
            if (CustomMap == null) _IsAccessToEmployee = false;
            else
            {
                IdForSitemap = CustomMap.Id;
                UrlForEmployee = "~/" + CustomMap.Url.ToString();
            }
            _mailsetting = MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id);
            if (!IsPostBack)
            {
                BindList();
                txtSortColumn.Text = "btnLookupName";
                txtSortOrder.Text = "ASC";
                PlaceUpDownArrow();
            }
            string pagesize = "";
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("MyEmployeeList.aspx"))
            {
                pagesize = (Request.Cookies["MyEmployeeListRowPerPage"] == null ? "" : Request.Cookies["MyEmployeeListRowPerPage"].Value); ;
            }
            else
            {
                pagesize = (Request.Cookies["EmployeeListRowPerPage"] == null ? "" : Request.Cookies["EmployeeListRowPerPage"].Value); ;
            }
                ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvEmployeeList.FindControl("pagerControl");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null)
                    {
                        pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    }
                }
                //if (lsvEmployeeList.Items.Count == 0)
                //    btnUpdate.Visible = false;

        }


        #endregion

        #region ListView Events

        protected void lsvEmployeeList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _IsAdmin = base.IsUserAdmin;

                     
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                
                Employee memberInfo = ((ListViewDataItem)e.Item).DataItem as Employee;
                DropDownList ddlStatus = (DropDownList)e.Item.FindControl("ddlStatus");
                ddlStatus.Attributes.Add("onchange", "javascript:EmployeeAccessChanged('" + ddlStatus.ClientID + "','" + memberInfo.Id + "','" + base.CurrentMember.Id + "','" + lblMessage.ClientID + "')");
                if (memberInfo != null)
                {
                    HyperLink lnkEmployeeName = (HyperLink)e.Item.FindControl("lnkEmployeeName");
                    Label lblReportingTo = (Label)e.Item.FindControl("lblReportingTo");
                   // Label lblCity = (Label)e.Item.FindControl("lblCity");
                    LinkButton  lblEmail = (LinkButton )e.Item.FindControl("lblEmail");
                    //Label lblPhone = (Label)e.Item.FindControl("lblPhone");
                   // Label lblHomePhone = (Label)e.Item.FindControl("lblHomePhone");
                    Label lblSystemAccess = (Label)e.Item.FindControl("lblSystemAccess");
                    //DropDownList ddlStatus = (DropDownList)e.Item.FindControl("ddlStatus");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    lblReportingTo.Text = memberInfo.PrimaryManager;

                    string strFullName = memberInfo.FirstName + " " + memberInfo.LastName;

                    if (_IsAccessToEmployee)
                    {
                        if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                        {

                            ControlHelper.SetHyperLink(lnkEmployeeName, UrlForEmployee, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(memberInfo.Id), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentSiteMapId);

                        }
                    }
                    else
                    {
                        lnkEmployeeName.Text = strFullName;
                        lnkEmployeeName.Enabled = false;
                    }

    
                   // lblPosition.Text = memberInfo.CurrentPosition;
                   // lblPhone.Text = memberInfo.CellPhone;

                    if(!string.IsNullOrEmpty(memberInfo.SystemAccess))
                    {

                        lblSystemAccess.Text = memberInfo.SystemAccess;
                    }

                    //if (!string.IsNullOrEmpty(memberInfo.PrimaryPhoneExtension))
                    //{
                    //    lblHomePhone.Text = memberInfo.PrimaryPhone + " " + "(" + memberInfo.PrimaryPhoneExtension + ")";
                    //}
                    //else
                    //{
                    //    lblHomePhone.Text = memberInfo.PrimaryPhone;
                    //}
                   // lblCity.Text = memberInfo.CurrentCity;
                        if (!_mailsetting)
                            lblEmail.Text = "<a href=MailTo:" + memberInfo.PrimaryEmail + ">" + memberInfo.PrimaryEmail + "</a>";
                        else
                        {
                            lblEmail.Text = memberInfo.PrimaryEmail;
                            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_MEMBER_ID,memberInfo .Id .ToString () ,UrlConstants .PARAM_EMAIL_TYPE ,"Employee");
                            //lblEmail.Attributes.Add("onclick", "window.open('" + url + "','NewMail','height=626,width=770,toolbar=no,directories=no,status=no,linemenubar=no,scrollbars=no,resizable=no,modal=no'); return false;");
                            lblEmail.Attributes.Add("onclick", "btnMewMail_Click('" + url + "')"); 
                        }
                    //ControlHelper.PopulateEnumIntoList(ddlStatus, typeof(MemberStatus));
                    ddlStatus.SelectedValue = memberInfo.Status.ToString();
                    //***********Code added by pravin khot on 10/March/2016 - Using user Disable when status InActive******************
                    if (memberInfo.Status == 3)
                    {
                        lblEmail.Text = "";
                        lblEmail.Text = memberInfo.PrimaryEmail;
                      lnkEmployeeName.Enabled = false;
                      lblEmail.Enabled = false;
                      lnkEmployeeName.Attributes["style"] = "color: red";
                      lblEmail.Attributes["style"] = "color: red";
                        lblSystemAccess.Attributes["style"] = "color: red";
                        lblReportingTo.Attributes["style"] = "color: red";
                        //ddlStatus.Attributes["style"] = "color: red";
                    }
                    //****************************END*************************************

                    btnDelete.Visible = _IsAdmin;
                    btnDelete.OnClientClick = "return ConfirmDelete('user')";
                    btnDelete.CommandArgument = StringHelper.Convert(memberInfo.Id);
                    System.Web.UI.HtmlControls.HtmlTableCell headerAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvEmployeeList.FindControl("hdrAction");
                    System.Web.UI.HtmlControls.HtmlTableCell contentAction = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("CntAction");
                    System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvEmployeeList.FindControl("tdPager");
                    if (!_IsAdmin)
                    {
                        headerAction.Visible = false;
                        contentAction.Visible = false;
                        tdPager.ColSpan = 5;
                    }

                }
            }
        }
      
        protected void lsvEmployeeList_PreRender(object sender, EventArgs e)
        {

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvEmployeeList.FindControl("pagerControl");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }
                    HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                    if (hdnRowPerPageName != null)
                    {
                        if (Page.Request.Url.AbsoluteUri.ToString().Contains("MyEmployeeList.aspx")) hdnRowPerPageName.Value = "MyEmployeeListRowPerPage";
                        else hdnRowPerPageName.Value = "EmployeeListRowPerPage";
                    }
                }
                PlaceUpDownArrow();
        }
        protected void lsvEmployeeList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if ( txtSortColumn .Text  == lnkbutton.ID)
                    {
                        if ( txtSortOrder .Text  == "ASC") txtSortOrder .Text  = "DESC";
                        else  txtSortOrder .Text  = "ASC";
                    }
                    else
                    {
                         txtSortColumn .Text  = lnkbutton.ID;
                         txtSortOrder .Text  = "ASC";
                    }
                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else
                        SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsEmployeeList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    Helper.Url.Redirect(UrlConstants.Employee.EMPLOYEE_BASICINFO, string.Empty, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(id));
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                      
                            Member member = Facade.GetMemberById(id);
                            MembershipUser membershipUser = Membership.GetUser(member.UserId);

                            if (Facade.DeleteMemberById(id))
                            {
                                Roles.RemoveUserFromRole(membershipUser.UserName, ContextConstants.ROLE_EMPLOYEE);
                                Membership.DeleteUser(membershipUser.UserName, true);

                                BindList();
                                if (lsvEmployeeList.Items.Count == 1)
                                    Helper.Url.Redirect(Request.RawUrl);
                                MiscUtil.ShowMessage(lblMessage, "User has been deleted successfully.", false);
                                
                            }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                    
                }
            }
        }

        #endregion

        #region Button Events

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int counter = 0, selcounter = 0;
            int employeeId = 0;
            Member member = null;
            foreach (ListViewDataItem rwEmployee in lsvEmployeeList.Items)
            {
                CheckBox chkItem = (CheckBox)rwEmployee.FindControl("chkItem");
                if (chkItem != null && chkItem.Checked)
                {
                    DropDownList ddlStatus = rwEmployee.FindControl("ddlStatus") as DropDownList;
                    Int32.TryParse(lsvEmployeeList.DataKeys[counter].Value.ToString(), out employeeId);
                    if (employeeId > 0)
                    {
                        member = Facade.GetMemberById(employeeId);
                        if (member != null)
                        {
                            if (ddlStatus.SelectedIndex == 0)
                            {
                                MembershipUser thisUser = Membership.GetUser(member.UserId);
                                if (thisUser.UnlockUser())
                                {
                                    member.Status = (int)AccessStatus.Enabled;
                                    member.UpdatorId = base.CurrentMember.Id;
                                    Facade.UpdateMemberStatus(member);
                                }
                            }
                            else if (ddlStatus.SelectedIndex == 1)
                            {
                                if (Facade.LockUser(member.UserId))
                                {
                                    member.Status = (int)AccessStatus.Blocked;
                                    member.UpdatorId = base.CurrentMember.Id;
                                    Facade.UpdateMemberStatus(member);
                                }
                            }
                        }
                    }
                    selcounter++;
                }
                counter++;
            }
            BindList();
            if (selcounter > 0)
            {
                MiscUtil.ShowMessage(lblMessage, "Updated successfully.", false);
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select at least one employee to update.", true);
            }
        }
        //*************Event added by pravin khot on 10/March/2016********************
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindList();
        }
        //******************************END**************************************

        #endregion

        #endregion
    }
}
