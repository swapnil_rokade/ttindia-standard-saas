﻿using System;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;

namespace TPS360.Web.UI
{
    public partial class ControlLookUpEditor : BaseControl
    {
        #region Member Variables

        GenericLookup _genericLookup;

        #endregion

        #region Properties

        private GenericLookup CurrentGenericLookup
        {
            get
            {
                if (_genericLookup == null)
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
                    {
                        int id = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);

                        if (id > 0)
                        {
                            _genericLookup = Facade.GetGenericLookupById(id);
                        }
                    }

                    if (_genericLookup == null)
                    {
                        _genericLookup = new GenericLookup();
                    }
                }

                return _genericLookup;
            }
        }

        #endregion

        #region Methods

        private string GetTabKey(int tabPosition)
        {
            string test = "editorTab,listTab";

            string[] tabs = test.Split(',');

            return tabs[tabPosition];
        }

        private void RedirectToList(string message, string tabKey)
        {
            string returnUrl = Helper.Url.SecureUrl.ReturnUrl;

            if (StringHelper.IsBlank(returnUrl))
            {
                returnUrl = UrlConstants.Admin.LOOKUP_EDITOR_PAGE;
            }

            if (!StringHelper.IsBlank(message))
            {
                Helper.Url.Redirect(returnUrl, null, UrlConstants.PARAM_MSG, message, UrlConstants.PARAM_TAB, StringHelper.Convert(tabKey));
            }
            else
            {
                Helper.Url.Redirect(returnUrl);
            }
        }

        private void SaveLookup()
        {
            if (IsValid)
            {
                try
                {
                    GenericLookup genericLookup = BuildGenericLookup();

                    if (genericLookup.IsNew)
                    {
                        Facade.AddGenericLookup(genericLookup);
                        RedirectToList("Generic Lookup  has been added successfully.", StringHelper.Convert(GetTabKey(1)));
                    }
                    else
                    {
                        genericLookup.UpdatorId = CurrentMember.Id;
                        Facade.UpdateGenericLookup(genericLookup);
                        RedirectToList("Generic Lookup  has been updated successfully.", StringHelper.Convert(GetTabKey(1)));
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }

            }
        }

        private void PrepareView()
        {
            GenericLookup genericLookup = CurrentGenericLookup;
           
            if (genericLookup != null)
            {
                txtLookupName.Text =MiscUtil .RemoveScript ( genericLookup.Name);
                ControlHelper.SelectListByValue(ddlType, genericLookup.Type.ToString());
                txtLookupDescription.Text =MiscUtil .RemoveScript ( genericLookup.Description);
            }
        }

        private GenericLookup BuildGenericLookup()
        {
            GenericLookup genericLookup = CurrentGenericLookup;
            genericLookup.Name =MiscUtil .RemoveScript ( txtLookupName.Text.Trim());
            genericLookup.Description =MiscUtil .RemoveScript ( txtLookupDescription.Text.Trim());
            genericLookup.Type = Convert.ToInt32(ddlType.SelectedValue);
            genericLookup.CreatorId = base.CurrentMember.Id;
            genericLookup.UpdatorId = base.CurrentMember.Id;

            return genericLookup;
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ControlHelper.PopulateEnumDescriptionIntoList(ddlType, typeof(LookupType),true);
            if (!IsPostBack)
            {
                PrepareView();
            }

            if (IsPostBack)
            {                
                MiscUtil.LoadAllControlState(Request, this);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveLookup();
        }

        #endregion
    }
}