﻿using System;
using System.Web.UI.WebControls;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
namespace TPS360.Web.UI
{
    public partial class IPAccessRules : BaseControl
    {
        #region Member Variables

       
        #endregion

        #region Properties

        
        #endregion

        #region Methods

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                SortColumn.Text = "[M].[FirstName]";
                SortOrder.Text = "asc";
                txtSortColumn.Text = "lnkUserId";
                txtSortOrder.Text = "ASC";
                PlaceUpDownArrow();
                MiscUtil.PopulateMemberListByRole(ddlEmployees , ContextConstants.ROLE_EMPLOYEE, Facade);
                ddlEmployees.Items[0].Text = "All Users";
            }
            string pagesize = "";
            pagesize = (Request.Cookies["IPAccessRulesRowPerPage"] == null ? "" : Request.Cookies["IPAccessRulesRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvIPRules .FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            }

        

        }
        public void ClearControls() 
        {
            ddlEmployees.SelectedIndex = 0;
            txtAllowedIPAddressTo.Text = "";
            txtAllowedIPAdress.Text = "";
            chkAnyIP.Checked = false;
            txtAllowedIPAdress.Enabled = true;
            txtAllowedIPAddressTo.Enabled = true;
           
        }
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton) lsvIPRules.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", ( txtSortOrder .Text == "ASC" ? "Ascending" : "Descending"));

            }
            catch
            {
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Common.BusinessEntities.IPAccessRules Rules = new TPS360.Common.BusinessEntities.IPAccessRules();
            BuildIPAccessRules(Rules);
            if (Rules.Id > 0)
            {
                Facade.UpdateIPAccessRules(Rules);
                MiscUtil.ShowMessage(lblMessage, "IP Access Rule successfully updated", false);
            }
            else
            {
                Facade.AddIPAccessRules(Rules);
                MiscUtil.ShowMessage(lblMessage, "IP Access Rule successfully Added", false);
            }
            ClearControls();

           dataBind();
            IPAccessRulesID.Value = "";
        }
        private void dataBind()
        {
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "[M].[FirstName]";
            }
            odsIPRules.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            lsvIPRules.DataBind();
        }
        protected void lsvIPRules_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }

                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                   odsIPRules.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {

                        Facade.IPAccessRules_DeleteById(id);
                        dataBind();
                        MiscUtil.ShowMessage(lblMessage, "IP Access rule successfully deleted",false );
                    }
                    catch (ArgumentException ex)
                    {
                    }
                    ClearControls();
                    IPAccessRulesID.Value = "";
                }
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    Common.BusinessEntities.IPAccessRules rules = Facade.IPAccessRules_GetById(id);
                    if (rules != null)
                    {
                        if (rules.MemberId == "*") ddlEmployees.SelectedIndex = 0;
                        else ControlHelper.SelectListByValue(ddlEmployees, rules.MemberId);
                        if (rules.AllowedIP == "*") { chkAnyIP.Checked = true; txtAllowedIPAdress.Enabled = false; txtAllowedIPAdress.Text = ""; txtAllowedIPAddressTo.Enabled = false; txtAllowedIPAddressTo.Text = ""; }
                        else { chkAnyIP.Checked = false; txtAllowedIPAdress.Text = rules.AllowedIP; txtAllowedIPAdress.Enabled = true; txtAllowedIPAddressTo.Text = rules.AllowedIPTo; txtAllowedIPAddressTo.Enabled = true; }
                        IPAccessRulesID.Value = rules.Id.ToString();
                    }
                   
                }
            }
        }
        protected void lsvIPRules_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
               Common .BusinessEntities .IPAccessRules  rules = ((ListViewDataItem)e.Item).DataItem as  Common .BusinessEntities .IPAccessRules ;

                if (rules  != null)
                {
                    Label lblAppliesTo = (Label)e.Item.FindControl("lblAppliesTo");
                    Label AllowedIP = (Label)e.Item.FindControl("AllowedIP");
                    Label AllowedIPTo = (Label)e.Item.FindControl("lblAllowedIPTo");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    btnDelete.CommandArgument = btnEdit .CommandArgument = rules.Id.ToString();
                    if (rules.MemberId == "*") lblAppliesTo.Text = "All";
                    else lblAppliesTo.Text = rules.UserName;
                    if (rules.AllowedIP == "*") AllowedIP.Text = "Any";
                    else AllowedIP.Text = rules.AllowedIP;
                    if (rules.AllowedIPTo == "*") AllowedIPTo.Text = "Any";
                    else AllowedIPTo.Text = rules.AllowedIPTo;
                }
            }

        }
        protected void lsvIPRules_PreRender(object sender, EventArgs e)
        {
     
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvIPRules .FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null)
                {
                  hdnRowPerPageName.Value = "IPAccessRulesRowPerPage";
                }
            }
            PlaceUpDownArrow();

        }
        private void BuildIPAccessRules(Common .BusinessEntities .IPAccessRules IPrules)
        {
            if (IPAccessRulesID.Value != "" && Convert.ToInt32(IPAccessRulesID.Value) > 0)
            {
                IPrules.Id = Convert.ToInt32(IPAccessRulesID.Value);
            }
            if (ddlEmployees.SelectedIndex == 0) IPrules.MemberId = "*";
            else IPrules.MemberId = ddlEmployees.SelectedValue.ToString();
            if (chkAnyIP.Checked)
            {
                IPrules.AllowedIP = "*";
                IPrules.AllowedIPTo = "*";
            }
            else 
            {
                IPrules.AllowedIP = txtAllowedIPAdress.Text;
                IPrules.AllowedIPTo = txtAllowedIPAddressTo.Text;
       
            }
                
        }
        #endregion
    }
}

