﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: TPS360Overview.ascx.cs
    Description: This is the control which used to display the overview for candidate/consultant
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Nov-15-2008          Yogeesh Bhat          Defect id:9232; Changes made in lsvSkillSet_ItemDataBound() method.
                                                               Displaying month in skills, LastUsed field
    0.2             Nov-18-2008          Yogeesh Bhat          Defect ID: 9232; Rework: 0 proficiency level is displayed
    0.3             Dec-04-2008          Jagadish              Defect ID: 9350; Changed the datasource of Listview "lsvExperience".
 *  0.4             Jan-22-2009          Gopala Swamy          Enhancement Id:9750(Kaizon):changed made in  "lsvSkillSet_ItemDataBound"
 *  0.5             Jan-27-2009          N.Srilakshmi          Defect Id:9701: Changes made in lsvJobCart_ItemDataBound  
    0.6             Mar-09-2009          Shivanand             Defect #10045; Changes made in methods PrepareView(),GethighlightedText(),lsvSkillSet_ItemDataBound(), for highlighting 
                                                                               matching skills.
 * 0.7              May-06-2009          Sandeesh              Defect #10378  To Combine the "All Keywords" and "Any Keyword" searching boxes into one "Keyword Search" box that supports boolean syntax searching
   0.8              Jun-30-2009          Veda                  Defect #10825 :  when the user tries to sort Experience details in overview tab, Skill&Educations details are disappearing 
   0.9              Aug-14-2009          Shivanand             Defect #10045; Changes made in method HighlightSearchKeyWords().
 * 1.0              Sep-09-2009         Ranjit Kumar.I         EnhancementID:10045;Added Code in Page_Load by checking Session["RequisitionSkillSet"]
 *                                                             EnhancementID:10045:Added Code in PrepareView For Highlighting the SkillSet in Summery 
   1.1               Sept-24-2009         Nagarathna V.B      Enha#11473;passing current user for send email method call.                                           
 * 1.2              Jan-29-2010          Sudarshan.R.           Defect ID:12108; Changes made to display the current status in lsvJobCart_ItemDataBound method
 * 1.3              Apr-19-2010          Sudarshan.R.           Defect Id:12661 ; Changes made to display the objective and summary label text properly.
-------------------------------------------------------------------------------------------------------------------------------------------       
*/


using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Linq;
using AjaxControlToolkit;
namespace TPS360.Web.UI
{
    public partial class ControlTPS360Overview : ATSBaseControl
    {
        #region Member Variables

        private int _memberId;
        private int JobId = 0;
        Member member;
        MemberDetail memberDetail;
        MemberExtendedInformation memberExInfo;

        #endregion

        #region Properties

        #endregion

        #region Methods
        private void GetJobId()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]))
            {
                JobId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]);
            }
        }


        private string HighLightSkillName(string text)
        {
            GetJobId();
            string CurrentJobPostingSkillNames = Facade.JobPosting_GetRequiredSkillNamesByJobPostingID(JobId);
            char[] delim = { ',' };
            string[] oriArr = CurrentJobPostingSkillNames.Trim().Split(delim, StringSplitOptions.RemoveEmptyEntries);
            CurrentJobPostingSkillNames = CurrentJobPostingSkillNames.Trim();
            if (CurrentJobPostingSkillNames != "" && CurrentJobPostingSkillNames.LastIndexOf(',') == CurrentJobPostingSkillNames.Length - 1)
            {
                CurrentJobPostingSkillNames = CurrentJobPostingSkillNames.Substring(0, CurrentJobPostingSkillNames.Length - 1);
            }
            string ret = string.Empty;
            if (oriArr.Length > 0 && text.Trim() != "")
            {

                string pattern = "";
                string sss = "";
                foreach (string s in oriArr)
                {

                    try
                    {
                        text = Regex.Replace("" + text + "", "" + Regex.Matches(s.Trim(), " ") + "", " <span class=highlight>" + Regex.Matches(s, " ") + "</span> ", RegexOptions.IgnoreCase);
                    }
                    catch { }
                }

            }
            else return text;
            return text;
        }


        private void PrepareView()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            else
            {
                _memberId = base.CurrentMember.Id;
            }

            MemberObjectiveAndSummary memberObjective = Facade.GetMemberObjectiveAndSummaryByMemberId(_memberId);
            if (memberObjective != null)
            {
                if ((string.IsNullOrEmpty(memberObjective.Objective)) && (string.IsNullOrEmpty(memberObjective.Summary)))
                {
                    divObjectiveandSummary.Visible = true;
                    tdObjectiveSummary.InnerText = "No Objective & Summary available.";
                }

                if (!(string.IsNullOrEmpty(memberObjective.Objective)))
                {
                    lblObjective.Text = "<strong>Objective:</strong><br/>" + memberObjective.Objective;
                }

                if ((string.IsNullOrEmpty(memberObjective.Objective)) && !(string.IsNullOrEmpty(memberObjective.Summary)))
                {
                    divObjectiveandSummary.Visible = true;
                    tdObjectiveSummary.InnerText = "No Objective available.";
                }

                if (!(string.IsNullOrEmpty(memberObjective.Summary)))
                {
                    lblSummery.Text = "<strong>Summary:</strong><br/>" + memberObjective.Summary;
                }

                if (!(string.IsNullOrEmpty(memberObjective.Objective)) && (string.IsNullOrEmpty(memberObjective.Summary)))
                {
                    divObjectiveandSummary.Visible = true;
                    tdObjectiveSummary.InnerText = "No Summary available.";
                }

                if (!(string.IsNullOrEmpty(memberObjective.CopyPasteResume)))
                {
                    lblCopyPasteResume.Text = memberObjective.CopyPasteResume; GetJobId();
                    if (JobId > 0) lblCopyPasteResume.Text = HighLightSkillName(" " + lblCopyPasteResume.Text + " ");
                }
                else
                {
                    divCopyPasteResume.Visible = true;
                }
            }
            else
            {
                divObjectiveandSummary.Visible = true;
                divCopyPasteResume.Visible = true;
            }

            odsInterviewSchedule.SelectParameters["CandidateId"].DefaultValue = _memberId.ToString();
            if (Session["PreciseSearch"] != null)
            {
                // For All_search_keywords
                if (Session["AllSearchKeywords"] != null)
                {
                    string strSearchKeywords = Convert.ToString(Session["AllSearchKeywords"]);


                    HighlightSearchKeyWords(lblObjective, strSearchKeywords);
                    HighlightSearchKeyWords(lblSummery, strSearchKeywords);
                    HighlightSearchKeyWords(lblCopyPasteResume, strSearchKeywords);
                }
            }
            else if (Session["RequisitionSkillSet"] != null)
            {
                string strSearchKeywords = Convert.ToString(Session["RequisitionSkillSet"]);
                HighlightKeyWords(lblObjective, strSearchKeywords);
                HighlightKeyWords(lblSummery, strSearchKeywords);
                HighlightKeyWords(lblCopyPasteResume, strSearchKeywords);
            }
        }

        public string GethighlightedText(string strInputTxt, string strSearchString)
        {
            strSearchString = strSearchString.Replace("(", string.Empty);
            strSearchString = strSearchString.Replace(")", string.Empty);
            strSearchString = strSearchString.Replace("[", string.Empty);
            strSearchString = strSearchString.Replace("]", string.Empty);
            if (strSearchString.Contains("++"))
            {
                strSearchString = Regex.Escape(strSearchString);
            }
            string strReturnExpression = Regex.Replace(strInputTxt, strSearchString.Trim(), new MatchEvaluator(ReplaceKeyWords), RegexOptions.IgnoreCase);
            return strReturnExpression;
        }

        public static string ReplaceKeyWords(Match m)
        {
            return "<span class=highlight>" + m.Value + "</span>";
        }

        public void HighlightKeyWords(Label lblForHighlight, string strSearchKeywords)
        {
            if (!string.IsNullOrEmpty(lblForHighlight.Text))
            {
                if (!string.IsNullOrEmpty(strSearchKeywords))
                {
                    string[] arrStrAllkeyWords = strSearchKeywords.Split(',');

                    var v = from a in arrStrAllkeyWords
                            orderby a.Length descending, a ascending
                            select a;


                    arrStrAllkeyWords = v.ToArray<string>();


                    if (arrStrAllkeyWords != null && arrStrAllkeyWords.Length > 0)
                    {
                        string strOriginalText = lblForHighlight.Text;

                        for (int i = 0; i < arrStrAllkeyWords.Length; i++)
                        {
                            if (!(string.IsNullOrEmpty(arrStrAllkeyWords[i].ToString())))
                            {
                                strOriginalText = GethighlightedText(strOriginalText, arrStrAllkeyWords[i].ToString());
                            }
                        }
                        lblForHighlight.Text = strOriginalText;
                    }
                }
            }
        }

        public void HighlightSearchKeyWords(Label lblForHighlight, string strSearchKeywords)
        {
            if (!string.IsNullOrEmpty(lblForHighlight.Text))
            {
                if (!string.IsNullOrEmpty(strSearchKeywords))
                {
                    string pattern = string.Empty;
                    if (strSearchKeywords.Contains("'"))
                    {
                        strSearchKeywords = strSearchKeywords.Replace("'", "");
                        pattern = @"\band\b | \bor\b | \bnot\b ";
                    }
                    else if (strSearchKeywords.Contains("\""))
                    {
                        strSearchKeywords = strSearchKeywords.Replace("\"", "");
                        pattern = @"\band\b | \bor\b | \bnot\b ";
                    }
                    else
                    {
                        pattern = @"\band\b | \bor\b | \bnot\b | \s?";
                    }
                    string[] arrStrAllkeyWords = Regex.Split(strSearchKeywords, pattern, RegexOptions.IgnoreCase);
                    var v = from a in arrStrAllkeyWords
                            orderby a.Length descending, a ascending
                            select a;

                    arrStrAllkeyWords = v.ToArray<string>();
                    if (arrStrAllkeyWords != null && arrStrAllkeyWords.Length > 0)
                    {
                        string strOriginalText = lblForHighlight.Text;

                        for (int i = 0; i < arrStrAllkeyWords.Length; i++)
                        {
                            if (!(string.IsNullOrEmpty(arrStrAllkeyWords[i].Trim())))
                            {
                                strOriginalText = GethighlightedText(strOriginalText, arrStrAllkeyWords[i].Trim());
                            }
                        }
                        lblForHighlight.Text = strOriginalText;
                    }
                }
            }
        }
        public void RequisitionAddedForCandidate()
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(System.Web.UI.Page), "refresh", "<script>document.location.reload();</script>", false);
            Session["PageRefresh"] = "Refresh";

        }
        #endregion

        #region Page event

        protected void Page_Load(object sender, EventArgs e)
        {
            hdnisPostback.Value = "0";
            PrepareView();
            System.Web.UI.Control contrlSkill = uclSkills.FindControl("dvSkills");
            contrlSkill.Visible = false;
            System.Web.UI.Control contrlCertification = uclCertification.FindControl("dvCertification");
            contrlCertification.Visible = false;

            if (!IsPostBack)
            {
                if (Helper.Url.SecureUrl[UrlConstants.PARAM_PAGE_FROM] != null)
                {
                    string s = Helper.Url.SecureUrl[UrlConstants.PARAM_PAGE_FROM].ToString();
                    if (Helper.Url.SecureUrl[UrlConstants.PARAM_PAGE_FROM].ToString() == "Precise")
                    {
                        hdnisPostback.Value = "1";
                        // System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(System.Web.UI.Page), "refresh", "<script></script>", false);
                    }
                }
            }

        }

        #endregion

        #region ListView Events

        protected void lsvInterviewSchedule_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _IsAdmin = base.IsUserAdmin;

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberInterview memberInterview = ((ListViewDataItem)e.Item).DataItem as MemberInterview;

                if (memberInterview != null)
                {
                    Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
                    Label lblLocation = (Label)e.Item.FindControl("lblLocation");
                    LinkButton lblJobTitle = (LinkButton)e.Item.FindControl("lblJobTitle");
                    Label lblType = (Label)e.Item.FindControl("lblType");
                    Label lblTitle = (Label)e.Item.FindControl("lblTitle");
                    Label lblInterviewers = (Label)e.Item.FindControl("lblInterviewers");
                    Label lblNotes = (Label)e.Item.FindControl("lblNotes");
                    Label lblClient = (Label)e.Item.FindControl("lblClient");
                    HiddenField hfActivityId = (HiddenField)e.Item.FindControl("hfActivityId");
                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
                    if (memberInterview.AllDayEvent)
                    {
                        lblDateTime.Text = memberInterview.StartDateTime.ToShortDateString() + " All Day";
                    }
                    else
                    {
                        DateTime date = new DateTime();
                        date = getTime(memberInterview.StartDateTime, memberInterview.Duration);
                        if (date.Hour > 12 || memberInterview.StartDateTime.Hour > 12)
                            lblDateTime.Text = memberInterview.StartDateTime.ToShortDateString() + " " + memberInterview.StartDateTime.ToString("hh:mm tt") + " - " + getTime(memberInterview.StartDateTime, memberInterview.Duration).ToString("hh:mm tt");
                        else
                            lblDateTime.Text = memberInterview.StartDateTime.ToShortDateString() + " " + memberInterview.StartDateTime.ToString("hh:mm") + " - " + getTime(memberInterview.StartDateTime, memberInterview.Duration).ToString("hh:mm tt");
                    }
                    if (memberInterview.Location.Length > 13)
                    {
                        lblLocation.Text = memberInterview.Location.Substring(0, 13) + "...";
                        lblLocation.ToolTip = memberInterview.Location;
                    }
                    else
                        lblLocation.Text = memberInterview.Location;
                    SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Requisition.DIR + UrlConstants.Requisition.JOB_POSTING_PREVIEW, string.Empty, UrlConstants.PARAM_JOB_ID, memberInterview.JobPostingId.ToString());

                    lblJobTitle.Text = memberInterview.JobTitle;
                    lblJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + memberInterview.JobPostingId + "&FromPage=JobDetail" + "','700px','570px'); return false;");

                    lblType.Text = memberInterview.InterviewTypeName;
                    lblInterviewers.Text = memberInterview.InterviewerName;
                    if (memberInterview.Remark.Length > 13)
                    {
                        lblNotes.Text = memberInterview.Remark.Substring(0, 13) + "...";
                        lblNotes.ToolTip = memberInterview.Remark;
                    }
                    else
                        lblNotes.Text = memberInterview.Remark;
                    lblTitle.Text = memberInterview.Title;
                    if (lblTitle.Text.Length > 18)
                    {
                        lblTitle.Text = lblTitle.Text.Substring(0, 18) + "...";
                        lblTitle.ToolTip = memberInterview.Title;
                    }
                    if (memberInterview.CompanyName.Length > 15)
                    {
                        lblClient.Text = memberInterview.CompanyName.Substring(0, 15);
                        lblClient.ToolTip = memberInterview.CompanyName;
                    }
                    else
                        lblClient.Text = memberInterview.CompanyName;

                }
            }
        }
        private DateTime getTime(DateTime date, int duration)
        {
            if (duration > 0)
            {
                duration = duration / 60;
            }
            return date.AddMinutes(Convert.ToDouble(duration));
        }
        private void PlaceUpDownArrow()
        {

            try
            {
                LinkButton lnk = (LinkButton)lsvInterviewSchedule.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        protected void lsvInterviewSchedule_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvInterviewSchedule.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                else
                {
                    lsvInterviewSchedule.DataSource = null;
                    lsvInterviewSchedule.DataBind();
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "InterviewRowPerPage";
            }
            else
            {
                lsvInterviewSchedule.DataSource = null;
                lsvInterviewSchedule.DataBind();
            }
            PlaceUpDownArrow();
        }
        protected void lsvInterviewSchedule_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }

        }
        #endregion
    }
}