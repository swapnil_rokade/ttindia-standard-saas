﻿/*
 --------------------------------------------------------------------------------------------------------------------------------------
    FileName: ReceivedEmailList.ascx.cs
    Description: This page is used to display Received Email List.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ------------------------------------------------------------------------------------------------------------------------------    
    0.1            Dec-15-2008        Shivanand        Defect #9540; In the method lsvReceivedEmail_ItemDataBound(), Facade.GetMemberById() is
                                                                     called if value of "_tomemberId" is non-zero.
                                                                     In the method "odsEmailList_Selecting()", typo error in Input parameters
                                                                     receiverId,senderId is corrected.
  * 0.2            21/June/2016          pravin khot         modify- hlnkSubject,hlnkEmailBody

-----------------------------------------------------------------------------------------------------------------------------------------       
*/

using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Linq;

namespace TPS360.Web.UI
{
    public partial class ControlReceivedEmailList : BaseControl
    {

        #region Member Variables

        //private int _memberTimeSheetId = 2;
        public int _emailTypeLookupId = 391;
        public int _frommemberId = 0;
        public int _tomemberId = 0;
        public int _memberId = 0;
     
        public int _contactId = 0;
        public bool status = false;
        #endregion

        #region Properties
        public int EmailTypeLookupId
        {
            get
            {
                return _emailTypeLookupId;
            }

            set
            {
                _emailTypeLookupId = value;
            }

        }
        public EmailFromType EmailFromType
        {
            get
            {
                return (EmailFromType)(ViewState[this.ClientID + "_EmailFromType"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_EmailFromType"] = value;
            }
        }
        private int CompanyID
           {
             get
             {
                 if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]))
                 {
                     return   Int32.Parse(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]);
                 }
                 return 0;
             }
           
         }

        #endregion

        #region Methods

        private void PlaceUpDownArrow()
        {
            try
            {

                LinkButton lnk = (LinkButton)lsvReceivedEmail.FindControl(hdnSortColumn.Value);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        private void BindList()
        {
            lsvReceivedEmail.DataSourceID = "odsEmailList";
            lsvReceivedEmail.DataBind();
        }

        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Int32.Parse(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                _tomemberId = _memberId;
                _frommemberId = base.CurrentMember.Id; 
            }
            //From Member Login
            else
            {
                _memberId = base.CurrentMember.Id;
                _frommemberId = _memberId;
            }
        
        }

        private void PrePareView()
        {
            status = false;
            lsvReceivedEmail.DataSourceID = "odsEmailList";
            lsvReceivedEmail.DataBind();
        }

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ddlcontact.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            if (EmailFromType == (EmailFromType)EmailFromType.CompanyEmail)
            {
                System.Web.UI.HtmlControls.HtmlTableCell thClientEmail = (System.Web.UI.HtmlControls.HtmlTableCell)lsvReceivedEmail.FindControl("thClientEmail");
                if (thClientEmail != null) thClientEmail.Visible = true;
            }
            if (!IsPostBack)
            {
                 GetMemberId();

                string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];
                PrePareView();
                if (EmailFromType  ==(EmailFromType )EmailFromType .CompanyEmail )
                {
                    System.Web.UI.HtmlControls.HtmlTableCell thClientEmail = (System.Web.UI.HtmlControls.HtmlTableCell)lsvReceivedEmail.FindControl("thClientEmail");
                    if(thClientEmail!=null) thClientEmail.Visible = true;
                    lblcontent.Visible = true;
                    ddlcontact.Visible = true;
                    lblcontent.Text = "Filter by Contact: ";
                    checks.Value = "Company";
                 
                    IList<CompanyContact> contact = new List<CompanyContact>();

                    contact = Facade.GetAllCompanyContactByComapnyId(CompanyID );


                    if (contact != null)
                    {
                        contact = contact.OrderBy(f => f.Email).ToList();
                        int p_contact = 0;
                        ddlcontact.Items.Add("Please Select");
                        foreach (CompanyContact con in contact)
                        {
                            ListItem list = new ListItem();
                            if (con.Email != string.Empty)
                            {
                                list.Text =MiscUtil .RemoveScript ( con.FirstName,string .Empty ) + " " +MiscUtil .RemoveScript ( con.LastName,string .Empty ) + " [" + con.Email + "]";
                                list.Value = con.Id.ToString();
                                list.Attributes.Add("title", list.Text);
                                ddlcontact.Items.Add(list);

                            }
                        }
                    }
                }
              
                this.EmailTypeLookupId = 391;
                hdnSortColumn.Value = "btnDateTime";
                hdnSortOrder.Value = "DESC";
                PlaceUpDownArrow();
            }
             if (EmailFromType == (EmailFromType)EmailFromType.EmailHistory || EmailFromType == (EmailFromType)EmailFromType.EmployeeEmail)
            {
                LinkButton btnTo = (LinkButton)lsvReceivedEmail.FindControl("btnTo");
                if (btnTo != null)
                {
                    btnTo.Text = "From";
                    btnTo.CommandArgument = "SenderEmail";
                    btnTo.ToolTip = "Sort By Sender Email Address.";
                }
            }
            string pagesize = "";
            pagesize = (Request.Cookies["ReceivedEmailRowPerPage"] == null ? "" : Request.Cookies["ReceivedEmailRowPerPage"].Value); ;
            
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvReceivedEmail.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);

                if (EmailFromType == (EmailFromType)EmailFromType.CompanyEmail)
                {
                    System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell) lsvReceivedEmail .FindControl("tdPager");
                    if (tdPager != null) tdPager.ColSpan = 5;

                }
            }
        }
        private string StripHTML(string source)
        {
            try
            {
                string result;
                result = source.Replace("\r", " ");
                result = result.Replace("\n", " ");
                result = result.Replace("\t", string.Empty);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = result.Replace("\n", "\r");
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\t)", "\t\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\r)", "\t\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\t)", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                string breaks = "\r\r\r";
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }
                return result;
            }
            catch
            {
                return source;
            }
        }
        protected void btnSearchEmail_Click(object sender, EventArgs e)
        {
            if (ddlcontact.Items.Count > 0) ddlcontact.SelectedIndex = 0;
            status = false;
            lsvReceivedEmail.DataSourceID = "odsEmailList";
            lsvReceivedEmail.DataBind();
            hdnSortColumn.Value = "btnDateTime";
            hdnSortOrder.Value = "DESC";
            PlaceUpDownArrow();
           
        }

        #endregion

        #region ListView Events

        protected void lsvReceivedEmail_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                MemberEmail memberEmail = ((ListViewDataItem)e.Item).DataItem as MemberEmail;
                string body = StripHTML(memberEmail.EmailBody.ToString());
             
                if (memberEmail != null)
                {
                    int size = 0;
                    Label lblDateTime = (Label)e.Item.FindControl("lblDateTime");
                    Label lblReceivedFrom = (Label)e.Item.FindControl("lblReceivedFrom");
                    HyperLink hlnkSubject = (HyperLink)e.Item.FindControl("hlnkSubject");
                    HyperLink hlnkEmailBody = (HyperLink)e.Item.FindControl("hlnkEmailBody");
                    HiddenField hdfMemberEmailId = (HiddenField)e.Item.FindControl("hdfMemberEmailId");
                    Member member = null;
                    if (EmailFromType  ==(EmailFromType )EmailFromType .CompanyEmail )
                    {
                        if (CompanyID  > 0)
                        {
                            if (Request.Url.ToString().Contains("SFA/CompanyEmail.aspx"))
                            {
                                //*******Code modify by pravin khot on 21/June/2016************
                                hlnkSubject.Text = memberEmail.Subject;
                                hlnkSubject.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                                hlnkSubject.Target = "_blank";

                                hlnkEmailBody.Text = body;
                                hlnkEmailBody.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                                hlnkEmailBody.Target = "_blank";
                                //ControlHelper.SetHyperLink(hlnkSubject, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, memberEmail.Subject, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                                //ControlHelper.SetHyperLink(hlnkEmailBody, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, body, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                                //*********************END***********************************

                            }
                            else
                            {
                                //ControlHelper.SetHyperLink(hlnkSubject, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, memberEmail.Subject, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                                // ControlHelper.SetHyperLink(hlnkEmailBody, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, body, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                            }
                              lblReceivedFrom.Text = memberEmail.ReceiverEmail;
                        }
                    }
                    else if ((EmailFromType  ==(EmailFromType )EmailFromType .EmailHistory) || (EmailFromType == (EmailFromType)EmailFromType.EmployeeEmail) )
                    {
                        GetMemberId();
                        if (_memberId > 0)
                        {
                            //*******Code modify by pravin khot on 21/June/2016************
                            hlnkSubject.Text = memberEmail.Subject;
                            hlnkSubject.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                            hlnkSubject.Target = "_blank";

                            hlnkEmailBody.Text = body;
                            hlnkEmailBody.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                            hlnkEmailBody.Target = "_blank";
                            //ControlHelper.SetHyperLink(hlnkSubject, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, memberEmail.Subject, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                            //ControlHelper.SetHyperLink(hlnkEmailBody, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, body, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                            //*********************END***********************************

                            lblReceivedFrom.Text = memberEmail.SenderEmail;
                        }
                    }
                    else
                    {
                        if (_tomemberId > 0)
                        {
                            member = Facade.GetMemberById(_tomemberId);
                        }
                        if (member != null)
                        {
                            MembershipUser user = Membership.GetUser(member.UserId);
                            if (user != null)
                            {
                                if (Roles.IsUserInRole(user.UserName, MemberType.Candidate.ToString()))
                                {
                                    //*******Code modify by pravin khot on 21/June/2016************
                                    hlnkSubject.Text = memberEmail.Subject;
                                    hlnkSubject.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                                    hlnkSubject.Target = "_blank";
                                    //ControlHelper.SetHyperLink(hlnkSubject, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, memberEmail.Subject, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                                }
                                else if (Roles.IsUserInRole(user.UserName, MemberType.Employee.ToString()))
                                {
                                    //*******Code modify by pravin khot on 21/June/2016************
                                    hlnkSubject.Text = memberEmail.Subject;
                                    hlnkSubject.NavigateUrl = UrlConstants.ApplicationBaseUrl + "Employee/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                                    hlnkSubject.Target = "_blank";
                                    //ControlHelper.SetHyperLink(hlnkSubject, UrlConstants.Employee.EMPLOYEE_EMAIL_BODY_PREVIEW, string.Empty, memberEmail.Subject, UrlConstants.PARAM_ID, memberEmail.Id.ToString());

                                }

                            }
                            if (user != null)
                            {
                                if (Roles.IsUserInRole(user.UserName, MemberType.Candidate.ToString()))
                                {
                                    //*******Code modify by pravin khot on 21/June/2016************
                                    hlnkEmailBody.Text = body;
                                    hlnkEmailBody.NavigateUrl = UrlConstants.ApplicationBaseUrl + "ATS/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                                    hlnkEmailBody.Target = "_blank";
                                    //ControlHelper.SetHyperLink(hlnkEmailBody, UrlConstants.ATS.ATS_EMAIL_BODY_PREVIEW, string.Empty, body, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                                }
                                else if (Roles.IsUserInRole(user.UserName, MemberType.Employee.ToString()))
                                {
                                    //*******Code modify by pravin khot on 21/June/2016************
                                    hlnkEmailBody.Text = body;
                                    hlnkEmailBody.NavigateUrl = UrlConstants.ApplicationBaseUrl + "Employee/EmailPreView.aspx?id=" + memberEmail.Id.ToString();
                                    hlnkEmailBody.Target = "_blank";
                                    //ControlHelper.SetHyperLink(hlnkEmailBody, UrlConstants.Employee.EMPLOYEE_EMAIL_BODY_PREVIEW, string.Empty, body, UrlConstants.PARAM_ID, memberEmail.Id.ToString());
                                }
                            }
                        }
                        lblReceivedFrom.Text = memberEmail.ReceiverEmail;
                    }

                    hdfMemberEmailId.Value = memberEmail.Id.ToString();
                    lblDateTime.Text = memberEmail.SentDate.ToString();

                    if (EmailFromType == (EmailFromType)EmailFromType.CompanyEmail)
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell tdClientEmail = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdClientEmail");
                        tdClientEmail.Visible = true;
                        Label lblSender = (Label)e.Item.FindControl("lblSender");
                        lblSender.Text = memberEmail.SenderEmail ;
                    }



                }
            }
        }
        protected void lsvReceivedEmail_PreRender(object sender, EventArgs e)
        {

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvReceivedEmail.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "ReceivedEmailRowPerPage";
            }
            PlaceUpDownArrow();
        }


        protected void lsvReceivedEmail_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                        else  hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteMemberEmailById(id))
                        {
                            BindList();
                            MiscUtil.ShowMessage(lblMessage, "Email has been successfully deleted.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }

        }

        #endregion


        #endregion

        #region ObjectDataSource Event

        protected void odsEmailList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (EmailFromType  ==(EmailFromType )EmailFromType .CompanyEmail )
            {
                if (status == true)
                {
                    //e.InputParameters["emailTypeLookupId"] = EmailTypeLookupId.ToString();
                    e.InputParameters["emailTypeLookupId"] = "390";
                    e.InputParameters["senderId"] = _contactId.ToString();
                    e.InputParameters["AnyKey"] =MiscUtil .RemoveScript ( txtSearchEmail.Text);
                }
                else
                {
                    if (Request.Url.ToString().Contains("SFA/CompanyEmail.aspx"))
                    {
                        e.InputParameters["emailTypeLookupId"] = "";
                    }
                    else
                    {
                        e.InputParameters["emailTypeLookupId"] = EmailTypeLookupId.ToString();
                    }
                    e.InputParameters["comId"] = CompanyID.ToString();
                    e.InputParameters["AnyKey"] = MiscUtil.RemoveScript(txtSearchEmail.Text);
                    //e.InputParameters["emailTypeLookupId"] = EmailTypeLookupId.ToString();
                    //e.InputParameters["comId"] = CompanyID.ToString();
                    //e.InputParameters["AnyKey"] = MiscUtil.RemoveScript(txtSearchEmail.Text);
                }
            }
            else if ((EmailFromType  ==(EmailFromType )EmailFromType .EmailHistory) || (EmailFromType == (EmailFromType)EmailFromType.EmployeeEmail)) 
            {
                GetMemberId();
                e.InputParameters["emailTypeLookupId"] = "0";
                e.InputParameters["receiverId"] = _memberId.ToString();
                e.InputParameters["AnyKey"] =MiscUtil .RemoveScript ( txtSearchEmail.Text);
            }
            else
            {
                e.InputParameters["emailTypeLookupId"] = EmailTypeLookupId.ToString();
                e.InputParameters["senderId"] = _tomemberId.ToString();
                e.InputParameters["AnyKey"] =MiscUtil .RemoveScript ( txtSearchEmail.Text);
            }
        }


        #endregion       
       
        protected void ddlcontact_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlcontact.SelectedIndex != 0)
            {
                string selMailAddress = ddlcontact.SelectedItem.Text.Substring((ddlcontact.SelectedItem.Text.IndexOf('[') + 1), ((ddlcontact.SelectedItem.Text.LastIndexOf(']') - ddlcontact.SelectedItem.Text.IndexOf('[')) - 1));
                ddlcontact.SelectedItem.Attributes.Add("title", ddlcontact.SelectedItem.Text);
                Member mem = new Member();
                mem = Facade.GetMemberByMemberEmail(selMailAddress);
                _contactId = mem.Id;
                status = true;
                lsvReceivedEmail.DataSourceID = "odsEmailList";
                lsvReceivedEmail.DataBind();
            }
            else
            {

                status = false;
                lsvReceivedEmail.DataSourceID = "odsEmailList";
                lsvReceivedEmail.DataBind();
            }
        }
}
}