﻿using System;
using System.Web.Security;

using TPS360.Common.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Web.UI.WebControls;

namespace TPS360.Web.UI
{
    public partial class ForgotPassword : BaseControl
    {

        #region Veriables

        #endregion

        #region Properties

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                txt_UserEmailName.Focus();

        }

        protected void btnPasswordReset_Click(object sender, EventArgs e)
        {
            SentEmail();

            divContinue.Visible = true;
            divEmail.Visible = false;

        }
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Helper.Url.Redirect(UrlConstants.LOGIN_PAGE);

        }

        private void SentEmail()
        {

            string sentStatus = string.Empty;
            string Footer = string.Empty;
            string UnsubscribeFooter = string.Empty;
            EmailHelper emailManager = new EmailHelper();
            emailManager.To.Clear();
            emailManager.To.Add(txt_UserEmailName.Text);
            emailManager.Subject = "Your Talentrackr password reset link";
            string password = String.Empty;
            string strEmailText = String.Empty;
            Member member = Facade.GetMemberByMemberEmail(txt_UserEmailName.Text.Trim());
            if (member != null)
            {
                //***********condition commented by pravin khot on 2/June/2016
                //string rolename = Facade.GetCustomRoleNameByMemberId(member.Id);
                //if (rolename != "") 
                //{
                //**************END*******************
                    System.Web.Security.MembershipUser user = System.Web.Security.Membership.GetUser(member.UserId);
                    TPS360.Common.BusinessEntities.PasswordReset reset = new TPS360.Common.BusinessEntities.PasswordReset();

                    reset.MemberID = member.Id;
                    reset.Link = UrlConstants.ApplicationBaseUrl + "PasswordReset.aspx"
                        ;
                    reset = Facade.AddPasswordReset(reset);
                    string pagename = "";
                    if(Request.Url .ToString ().ToLower ().Contains ("/candidateportal/"))                    
                    {
                        pagename = "CandidatePortal/PasswordReset.aspx";
                    }
                    else 
                    {
                        pagename = "PasswordReset.aspx";
                    }
                    
                        
                    ControlHelper.SetHyperLink(lnkMail, UrlConstants.ApplicationBaseUrl + pagename , string.Empty, "Advanced Search", UrlConstants.PORTAL_PASSWORDRESET_ID, reset.Id.ToString());

                    strEmailText = "<html><body> <b>Dear " + member.FirstName + " " + member.LastName + ",</b> <p> We received a request from you to reset your Talentrackr password. Click the link below to choose a new password. </p>";
                    strEmailText = strEmailText + "<p> <a href=" + lnkMail.NavigateUrl + "> Reset My Password</a> </p></body></html>";
                    emailManager.Body = strEmailText + UnsubscribeFooter;
                    //emailManager.From = "";
                    //sentStatus = emailManager.Send();//comment by pravin khot on 17/June/2016
                    sentStatus = emailManager.SendMailFromSystemEmailId();//added by pravin khot on 17/June/2016
                    if (sentStatus == "1")
                    {
                        string encodedStr = Server.HtmlEncode(txt_UserEmailName.Text);
                        lblConfirmHeading.Text = "We have sent an email to " + encodedStr + ". Please follow the instructions to reset your password.";

                    }
                    else
                    {
                        lblConfirmHeading.Text = "SMTP Details are Missing";
                    }
                //}//condition commented by pravin khot on 2/June/2016
            }
            else
            {
                string encodedStr = Server.HtmlEncode(txt_UserEmailName.Text);
                lblConfirmHeading.Text = "No User found with that email address";

            }

            if (sentStatus == "1")
            {
                sentStatus = "Sent";
            }
            else
            {
                sentStatus = "Not Sent";
            }
        }
        #endregion
    }
}