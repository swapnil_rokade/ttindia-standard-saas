﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmailEditor.ascx.cs
    Description: This is the user control page used to edit emails.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sep-26-2008           Yogeesh Bhat        Defect ID: 8706; Changes made in PrepareView() method.
                                                             (toMember is taken from GetMemberReferenceById() method)
    0.2            Oct-15-2008           Gopala Swamy J      Defect ID:8898  I put else statement to make boolean variable to false if session is NULL
    0.3            Oct-17-2008           Jagadish            Defect ID: 8969; Modified the code not to attach previous attachments of the sent mails when replying.
    0.4            Oct-21-2008           Jagadish            Defect ID: 8966; Added members alternate email id to from drop down.
    0.5            Nov-03-2008           Yogeesh Bhat        Defect ID: 8895; Added TO text field; Added validations;
                                                             Changes made in SaveMemberEmailDetail method; 
    0.6            Nov-19-2008           Anand Dixit         Defect ID: 8990; Added lines of code from ; 598-618
    0.7            Dec-02-2008           Jagadish            Defect ID: 9356; Added value to 'To' email id filed.
    0.8            Dec-04-2008           Jagadish            Defect ID: 9357; Checked '_tomemberId' for greater than zero.
    0.9            Dec-12-2008           Jagadish            Defect ID: 8990; Modified the code to send password text in the mail and to display ****** in the mail editor.
                                                             worked on related issues.
    0.10           Dec-17-2008           Jagadish            Defect ID: 9292; Added code to display subject and email body of the previous mail when it is forwarded or replied.
    0.11           Dec-31-2008           Jagadish            Defect ID: 9490; Added asterix symbol infront of 'To' text box.
    0.12           Feb-02-2009           Jagadish            Defect ID: 9653; Changes made in SentEmail() method.
    0.13           Feb-06-2009           Yogeesh Bhat        Defect ID: 8900; Changes made in btnPreview_Click()
    0.14           Feb-24-2009           Jagadish            Defect ID: 9551; Added method 'OnPreRender()' and property 'IsRefreshed'.
    0.15           Mar-24-2009           Jagadish            Defect ID: 9521; Changes made in method 'PrepareView()'.
    1.6            May-6-2009            Gopala Swamy        Defect ID: 10441; put one condition statement 
 *  1.7              Sept-24-2009         Nagarathna V.B      Enha#11473;passing current user for send email method call.
 *  1.8            17/May/2016           pravin khot          added condition-if (Roles.IsUserInRole(toMember.PrimaryEmail, ContextConstants.ROLE_CANDIDATE))

-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Text;
using System.Web.UI;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using TPS360.Common.Helper;
using TPS360.Common.BusinessEntities;
using System.Web.UI.WebControls;
using TPS360.Common.Shared;
using System.IO;
using System.Web;
using TPS360.Web.UI.Helper;
using System.Text.RegularExpressions;
using System.Linq;
using System.Web.Security;
using TPS360.BusinessFacade;
using System.Configuration;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
namespace TPS360.Web.UI
{
    public partial class cltEmail : BaseControl
    {
        #region Member Variables
        private static int _frommemberId = 0;
        public static int _tomemberId = 0;
        private static int _memberEmailId = 0;
        private static string _linkToRespond = String.Empty;
        public static bool _fromReference = false;
        public static int _toReferenceMemberId = 0;
        public string MailBody;
        private bool _mailsetting = false;
        MemberEmail _memberEmail;
        
        #endregion

        #region Property

        public string TempDirectory
        {
            get
            {
                return (string)Helper.Session.Get("tempDir");
            }
            set
            {
                Helper.Session.Set("tempDir", value);
            }
        }
        public string PreviewDir
        {
            get;
            set;
        }
        int companyID
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]))
                {
                    return Convert .ToInt32 ( Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]);

                }
                ////From Member Login
                else
                {
                    return 0;
                }
                
            }
        }
        string EmailFrom
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_EMAIL_TYPE]))
                {
                    return Helper.Url.SecureUrl[UrlConstants.PARAM_EMAIL_TYPE];
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public StringDictionary UploadedFiles
        {
            get
            {
                if (Helper.Session.Get("EmailFileUpload") == null)
                {
                    Helper.Session.Set("EmailFileUpload", new StringDictionary());
                }

                return (StringDictionary)Helper.Session.Get("EmailFileUpload");
            }
            set
            {
                Helper.Session.Set("EmailFileUpload", value);
            }
        }

        private MemberEmail CurrentMemberEmail
        {
            get
            {
                if (_memberEmail == null)
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
                    {
                        _memberEmailId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);

                        _memberEmail = Facade.GetMemberEmailById(_memberEmailId);
                    }

                    if (_memberEmail == null)
                    {
                        _memberEmail = new MemberEmail();
                    }
                }

                return _memberEmail;
            }
        }
        
        public string ToEmailAddress
        {
            get
            {
                return (string)(ViewState[this.ClientID + "_ToEmailAddress"] ?? "");
            }
            set
            {
                ViewState[this.ClientID + "_ToEmailAddress"] = value;
            }
        }

        public int? TemplateId
        {
            get
            {
                return (int?)(ViewState[this.ClientID + "_TemplateId"] ?? null);
            }
            set
            {
                ViewState[this.ClientID + "_TemplateId"] = value;
            }
        }

        public EmailFromType EmailFromType
        {
            get
            {
                return (EmailFromType)(ViewState[this.ClientID + "_EmailFromType"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_EmailFromType"] = value;
            }
        }

        public string  PageFrom
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_PAGE_FROM ]))
                {
                    return Helper.Url.SecureUrl[UrlConstants.PARAM_PAGE_FROM];

                }
                return string .Empty ;
            }
        }
        public string SelectedList
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_SELECTED_IDS ]))
                {
                    return Helper.Url.SecureUrl[UrlConstants.PARAM_SELECTED_IDS ];

                }
                return string.Empty;
            }
        }
        public int JobID
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID ]))
                {
                    return Convert .ToInt32 ( Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]);
                }
                    return 0;
            }
        }
        public string Messages
        {
            
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MSG]))
                {
                    return Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];
                }
                return string.Empty;
            }
        }

        public bool IsRefreshed
        {
            get
            {
                if (Session["REFRESH_CHECK_GUID"] == null
                || ViewState["REFRESH_CHECK_GUID"] == null
                || Session["REFRESH_CHECK_GUID"].ToString().Equals(ViewState["REFRESH_CHECK_GUID"].ToString()))
                {
                    return false;
                }
                else
                    return true;
            }
        }
        

        #endregion

        #region Methods

        public void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
               
                hdfToMemberId.Value = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];
                _tomemberId = Convert.ToInt32(hdfToMemberId.Value);
            }
            //From Member Login

            hdfFromMemberId.Value = base.CurrentMember.Id.ToString();
            _frommemberId = base.CurrentMember.Id;
        }
        public void PrepareView()
        {
            MemberEmail memberEmail = CurrentMemberEmail;
        
           
            if (memberEmail.IsNew)
            {
                if (_frommemberId > 0)
                {
                    if (EmailFromType == EmailFromType.CompanyEmail)
                    {
                        drpToText.Items.Clear();
                        IList<CompanyContact> contact = new List<CompanyContact>();
                        contact = Facade.GetAllCompanyContactByComapnyId(Convert.ToInt32(companyID));
                        if (contact != null)
                        {
                            int p_contact = 0;
                            contact = contact.OrderBy(f => f.Email).ToList();
                            foreach (CompanyContact con in contact)
                            {
                                ListItem list = new ListItem();
                                if (con.Email != string.Empty)
                                {
                                    list.Text =MiscUtil .RemoveScript ( con.FirstName,string .Empty ) + " " +MiscUtil .RemoveScript ( con.LastName ,string .Empty )+ " [" + con.Email + "]";
                                    list.Value = con.Id.ToString();
                                    list.Attributes.Add("title", list.Text);
                                    drpToText.Items.Add(list);
                                }
                                if (con.IsPrimaryContact == true)
                                {
                                    p_contact = con.Id;
                                }
                            }
                            if (p_contact > 0) drpToText.SelectedValue = p_contact.ToString();
                            else drpToText.SelectedIndex = 0;
                        }
                        
                    }
                    if (base .CurrentMember  != null)
                    {

                        lblFromAdd.Text =CurrentMember .FirstName + " " + CurrentMember .LastName + " [" + CurrentMember.PrimaryEmail + "]";
                        //ddlFrom.Items.Add(CurrentMember.FirstName + " " + CurrentMember.LastName + " [" + CurrentMember.PrimaryEmail + "]");
                        //if (!string.IsNullOrEmpty(CurrentMember .AlternateEmail ))
                        //    ddlFrom.Items.Add(CurrentMember.FirstName + " " + CurrentMember.LastName + " [" + CurrentMember.AlternateEmail  + "]");
                    }
                }

                if (_tomemberId > 0 || _toReferenceMemberId > 0)
                {
                    if (_fromReference) // if from Reference Check
                    {
                        string toMemberEmail = Facade.GetMemberReferenceEmailById(_toReferenceMemberId);     //0.1

                        if (toMemberEmail != string .Empty )
                        {
                            if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Forward") != 0 && !StringHelper.IsBlank(toMemberEmail.Trim ()))
                                lblToText.Text = toMemberEmail.Trim ();        
                            else
                            {
                                drpToText.Visible = true;
                                lblAsteric.Visible = true;
                            }
                        }
                        else
                        {
                            drpToText.Visible = true;
                            lblAsteric.Visible = true; 
                        }
                    }

                    else
                    {
                        string PrimaryEmail = Facade.GetMemberUserNameById(_tomemberId);
                        if (PrimaryEmail != string.Empty)
                        {
                            if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Forward") != 0 && !StringHelper.IsBlank(PrimaryEmail))
                                lblToText.Text = PrimaryEmail;       
                            else
                            {
                                drpToText.Visible = true;
                                lblAsteric.Visible = true; 
                            }
                        }
                        else                    
                        {
                            drpToText.Visible = true;
                            lblAsteric.Visible = true; 
                        }
                    }
                }
                else                    
                {
                    drpToText.Visible = true;
                    lblAsteric.Visible = true; 
                }

                if (EmailFromType == EmailFromType.CampaignEmail)
                {                    
                    if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Forward") != 0 && !StringHelper.IsBlank(this.ToEmailAddress))
                        lblToText.Text = this.ToEmailAddress;        
                    else
                    {
                        txtToText.Visible = true;
                        lblAsteric.Visible = true; 
                    }
                }
                if (EmailFromType == EmailFromType.CompanyEmail )
                {
                    // 0.5
                    if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Forward") != 0 && !StringHelper.IsBlank(this.ToEmailAddress))
                        lblToText.Text = this.ToEmailAddress;       
                    else
                    {
                        lblToText.Text = "";
                        drpToText.Visible = true;
                        lblAsteric.Visible = true; 
                    }
                }

                if (PageFrom == "Dashboard" || PageFrom == "JobDetail" || PageFrom == "HiringMatrix" || PageFrom == "CandidateList")
                {
                   
                    drpToText.Visible = false;
                    txtToText.Visible = true;
                    lblToText.Visible = false;
                    lblAsteric.Visible = true;
                    if (PageFrom == "JobDetail" || PageFrom =="HiringMatrix" ) PopulateJobDetail();
                    if (PageFrom == "CandidateList")  PopulateCandidateEmail();
                }
                else if (PageFrom == "Referrer")
                {
                    lblToText.Visible = false;
                    drpToText.Visible = false;
                    txtToText.Visible = true;
                    JobPosting job = Facade.GetJobPostingById(JobID);
                    txtSubject.Text = "Refer candidates for position " + job.JobTitle ;
                    SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Referral/EmployeeReferral.aspx", string.Empty, UrlConstants.PARAM_JOB_ID, JobID .ToString ());
                    txtEmailEditor.InnerHtml ="<a href=''>"+ url.ToString() +"</a>";
                }
            }

            if (EmailFromType == EmailFromType.CampaignEmail)
            {
                divMyTemplate.Visible = false;
                
            }            

            if (EmailFromType == EmailFromType.TPS360Access)
            {
                EmailHelper emailHelper = new EmailHelper();
                if (_tomemberId > 0)
                {
                    Member toMember = Facade.GetMemberById(_tomemberId);
                    if (toMember != null)
                    {
                        //***********code modify and if condition added by pravin khot on 17/May/2016******
                        string strHTMLText = "";
                        if (Roles.IsUserInRole(toMember.PrimaryEmail, ContextConstants.ROLE_CANDIDATE))
                        {
                            strHTMLText = emailHelper.PrepareViewforTps360AccessCandidate(toMember);
                            txtSubject.Text = "Login Credentials For Candidate Portal Access";
                        }
                        else
                        {
                            strHTMLText = emailHelper.PrepareViewforTps360Access(toMember);
                            txtSubject.Text = "New Password For Talentrackr Access";
                        }
                        //************************END*******************************
                         
                        txtEmailEditor.InnerHtml = strHTMLText; //MiscUtil.RemoveScript(strHTMLText,string .Empty );
                        //txtEmailEditor.ReadOnly = true;
                        
                
                    }
                } 
                divMasterTemplate.Visible = false;
                divCCBCC.Visible = false;
                chkResetPassword.Visible = false;
                divFileUpload.Visible = false;
                btnPreview.Visible = false;
                btnSendEmail.Text = "Reset & Send Password";
            }


            if (!memberEmail.IsNew)
            {
                IList<MemberEmailDetail> memberEmailDetails = Facade.GetAllMemberEmailDetailByMemberEmailId(memberEmail.Id);
                if (memberEmailDetails != null)
                {
                    foreach (MemberEmailDetail memberEmailDetail in memberEmailDetails)
                    {
                        if (memberEmailDetail.SendingTypeId == (int)EmailSendingType.Broadcast)
                        {
                            if (memberEmailDetail.AddressTypeId == (int)EmailAddressType.CC)
                            {
                                if (txtCC.Text != string.Empty)
                                    txtCC.Text += ",";
                                txtCC.Text += memberEmailDetail.EmailAddress;
                            }
                            if (memberEmailDetail.AddressTypeId == (int)EmailAddressType.BCC)
                            {
                                if (txtBCC.Text != string.Empty)
                                    txtBCC.Text += ",";
                                txtBCC.Text += memberEmailDetail.EmailAddress;
                            }
                        }

                    }
                }

                lblFromAdd.Text=CurrentMember.FirstName + " " + CurrentMember.LastName + " [" + base.CurrentMember.PrimaryEmail + "]"; // Defect # 9566

               
                if (!StringHelper.IsBlank(memberEmail.ReceiverEmail))
                {
                    if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Forward") != 0)// && !(memberEmail.SenderEmail.ToLower().Contains(base.CurrentMember.PrimaryEmail.ToLower()) || memberEmail.SenderEmail.ToLower().Contains(base.CurrentMember.AlternateEmail.ToLower()))) // Defect # 9566
                        lblToText.Text = memberEmail.SenderEmail;        
                    else
                    {
                        txtToText.Visible = true;
                        lblAsteric.Visible = true;
                        lblToText.Visible = false;
                    }
                }
                else
                {
                    txtToText.Visible = true;
                    lblAsteric.Visible = true; 
                }
                
                IList<MemberEmailAttachment> memberEmailAttachments = null;
                if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Reply") != 0)
                {
                    memberEmailAttachments = Facade.GetAllMemberEmailAttachmentByMemberEmailId(memberEmail.Id);
                    txtSubject.Text = "FW: " +MiscUtil .RemoveScript ( memberEmail.Subject,string .Empty );
                    txtEmailEditor.InnerHtml =MiscUtil .RemoveScript ( memberEmail.EmailBody,string .Empty );
                    
                }
                else
                {
                    txtSubject.Text = "RE: " +MiscUtil.RemoveScript ( memberEmail.Subject,string .Empty );
                    txtEmailEditor.InnerHtml =MiscUtil .RemoveScript ( memberEmail.EmailBody,string .Empty );
                }

                //txtEmailEditor.ReadOnly = memberEmail.EmailBody.Contains("</TBODY>"); // to be refixed

                string filePath = null;
                string contentPath = UrlConstants.GetEmailDocumentDirectory(_frommemberId.ToString());
                if (memberEmailAttachments != null)
                {
                    //if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Forward") == 0)
                    UploadedFiles.Clear();
                    foreach (MemberEmailAttachment memberEmailAttachment in memberEmailAttachments)
                    {
                        filePath = Path.Combine(contentPath, memberEmailAttachment.AttachmentFile);
                        UploadedFiles.Add(memberEmailAttachment.AttachmentFile+"$%"+memberEmailAttachment .MemberEmailId , filePath);
                    }
                }                
                else { Helper.Session.Set("EmailFileUpload", new StringDictionary()); }
                if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Forward") == 0)
                    EmailFileUpload1.setClear();
                EmailFileUpload1.BindFiles();
            }
            if (Helper.Url.SecureUrl.ToString().Contains("CompanyEmail"))
            {
                lblAsteric.Visible = true; 
            }
            MemberSignature signat = new MemberSignature();
            signat = Facade.GetActiveMemberSignatureByMemberId(base.CurrentMember.Id);
            if (signat != null)
            {
                chkIncludeSignature.Checked = true;
                txtEmailEditor.InnerHtml = txtEmailEditor.InnerHtml + signat.Signature.ToString();
                hfSignature.Value = signat.Signature.ToString();
                Signature.Value =  signat.Signature.ToString();
            }
             
        }

        private void PopulateCandidateEmail()
        {
            if (SelectedList != string.Empty)
            {
                IList<Member> member = Facade.GetAllMemberByIds(SelectedList);
                if (member != null)
                {
                    if (member.Count == 1)
                    {
                        txtToText.Text =MiscUtil .RemoveScript ( member[0].PrimaryEmail,string .Empty );
                    }
                    else
                    {
                        string bcc = "";
                        foreach (Member mem in member)
                        {
                            if (bcc != string.Empty) bcc += ";";
                            bcc += mem.PrimaryEmail;
                        }
                        txtBCC.Text = bcc;
                    }
                }
            }
        }

        private void PopulateJobDetail()
        {
            PopulateCandidateEmail();
            string JobTitle = "", JobPostingCode = "";
            txtEmailEditor.InnerHtml = "<br/><br/><hr/>" + MiscUtil.GetJobDetailTable(JobID, Facade, out JobTitle, out JobPostingCode, "Email",null,CurrentMember .Id  );

            txtSubject .Text =MiscUtil .RemoveScript ( JobTitle,string .Empty ) + "-" + JobPostingCode ;
        }
       
        private MemberEmail BuildMemberEmail()
        {
            MemberEmail memberEmail = CurrentMemberEmail;
            memberEmail.SenderId = _frommemberId;
            memberEmail.SenderEmail =  CurrentMember.PrimaryEmail ;
            if (_fromReference)
                memberEmail.ReceiverId = _toReferenceMemberId;
            else
                memberEmail.ReceiverId = _tomemberId;
            string to=string .Empty ;
            if (EmailFromType == EmailFromType.CompanyEmail)
            {
                if ((String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Reply") == 0 && !memberEmail.IsNew) || String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Forward") == 0 )
                        memberEmail.ReceiverEmail = (StringHelper.IsBlank(lblToText.Text)) ? txtToText.Text.Replace (',',';') : lblToText.Text;
                else
                {
                    try
                    {
                        if (drpToText.SelectedItem.Text.Contains('['))
                        {
                            to = drpToText.SelectedItem.Text.Substring((drpToText.SelectedItem.Text.IndexOf('[') + 1), ((drpToText.SelectedItem.Text.LastIndexOf(']') - drpToText.SelectedItem.Text.IndexOf('[')) - 1));
                            memberEmail.ReceiverEmail = (StringHelper.IsBlank(to.ToString())) ? txtToText.Text.Replace (',',';') : to.ToString();
                        }
                    }
                    catch
                    {
                    }
                }
            }
            else
                memberEmail.ReceiverEmail = (StringHelper.IsBlank(lblToText.Text)|| lblToText .Visible ==false ) ? txtToText.Text.Replace (',',';') : lblToText.Text;     //0.5

            if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Reply") == 0 && !memberEmail.IsNew)
            {
                memberEmail.EmailTypeLookupId = (int)EmailType.Replied;
                
                    memberEmail.ParentId = memberEmail.Id;

            }
            else if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Forward") == 0 && !memberEmail.IsNew)
            {
                memberEmail.EmailTypeLookupId = (int)EmailType.Forwarded;
                memberEmail.ParentId = memberEmail.Id;

            }
            else
            {
                memberEmail.EmailTypeLookupId = (int)EmailType.Sent;
                memberEmail.ParentId = 0;
            }
            memberEmail.Subject =MiscUtil .RemoveScript (txtSubject.Text);
            memberEmail.EmailBody = txtEmailEditor.InnerHtml;
            memberEmail.Status = 0;

            memberEmail.IsRemoved = false;
            memberEmail.CreatorId = base.CurrentMember.Id;
            memberEmail.UpdatorId = base.CurrentMember.Id;
            memberEmail.SentDate = DateTime.Now.ToString ();
            memberEmail.BCC = MiscUtil.RemoveScript(txtBCC.Text != string.Empty ? txtBCC.Text.Replace(',', ';') : string.Empty);
            memberEmail.CC = MiscUtil.RemoveScript(txtCC.Text != string.Empty ? txtCC.Text.Replace(',', ';') : string.Empty);
           
            return memberEmail;
        }

        private void SaveMemberEmailDetail(MemberEmail memberEmail)
        {
            MemberEmailDetail memberEmailDetail = new MemberEmailDetail();
           
            memberEmailDetail.MemberId = _frommemberId;
            memberEmailDetail.SendingTypeId = (int)EmailSendingType.Broadcast;
            memberEmailDetail.MemberEmailId = memberEmail.Id;
            memberEmailDetail.CreatorId = base.CurrentMember.Id;
            string[] TOarr;
            TOarr = txtToText.Text.Split((char)Convert.ToChar(","));
            if (TOarr.Length == 1)     
                TOarr = txtToText.Text.Split((char)Convert.ToChar(";"));
            if (TOarr != null)
            {
                

                foreach (string to in TOarr)
                {
                    memberEmailDetail.AddressTypeId = (int)EmailAddressType.To;
                    memberEmailDetail.EmailAddress = to.Trim();
                    memberEmailDetail = Facade.AddMemberEmailDetail(memberEmailDetail);
                }

            }
            string[] CCarr; string[] BCCarr;
            CCarr = txtCC.Text.Split((char)Convert.ToChar(","));
            if (CCarr == null)
                CCarr = txtCC.Text.Split((char)Convert.ToChar(";"));
            if (CCarr != null)
            {
                foreach (string cc in CCarr)
                {
                    memberEmailDetail.AddressTypeId = (int)EmailAddressType.CC;
                    memberEmailDetail.EmailAddress = cc.Trim();
                    memberEmailDetail = Facade.AddMemberEmailDetail(memberEmailDetail);
                }

            }

            BCCarr = txtBCC.Text.Split((char)Convert.ToChar(","));
            if (BCCarr == null)
                BCCarr = txtBCC.Text.Split((char)Convert.ToChar(";"));

            if (BCCarr != null)
            {
                foreach (string bcc in BCCarr)
                {
                    memberEmailDetail.AddressTypeId = (int)EmailAddressType.BCC;
                    memberEmailDetail.EmailAddress = bcc.Trim();
                    memberEmailDetail = Facade.AddMemberEmailDetail(memberEmailDetail);
                }
            }

            if (CCarr == null && BCCarr == null)
            {
                memberEmailDetail.AddressTypeId = 0;
                memberEmailDetail.EmailAddress = "";
                memberEmailDetail.SendingTypeId = (int)EmailSendingType.Single;
                memberEmailDetail = Facade.AddMemberEmailDetail(memberEmailDetail);

            }
        }

        private void SaveMemberEmailAttachment(MemberEmail memberEmail)
        {
            string[] file;

            if (UploadedFiles.Count > 0)
            {
                string contentPath = UrlConstants.GetEmailDocumentDirectory(_frommemberId.ToString());
                MiscUtil.CopyEmailFiles(UploadedFiles, contentPath);
                memberEmail.AttachedFileNames = string.Empty;
                foreach (DictionaryEntry entry in UploadedFiles)
                {
                    MemberEmailAttachment memberEmailAttachment = new MemberEmailAttachment();
                    file = Path.GetFileName((string)entry.Value).Split((char)Convert.ToChar("?"));
                    GetPathToSave(memberEmail, ((string)entry.Value));
                    memberEmailAttachment.AttachmentFile = file[0];
                    memberEmail.NoOfAttachedFiles = UploadedFiles.Count;
                    if (memberEmail.AttachedFileNames.Trim() != string.Empty) memberEmail.AttachedFileNames += ";";
                    memberEmail.AttachedFileNames += file[0];
                    if (file.Length >= 2)
                        memberEmailAttachment.AttachmentSize = Convert.ToInt32(file[1]);
                    else
                    {
                        FileInfo fi = new FileInfo((string)entry.Value);
                        if (fi != null)
                            memberEmailAttachment.AttachmentSize =Convert .ToInt32 ( fi.Length);
                        else
                            memberEmailAttachment.AttachmentSize = 0;
                    }
                    memberEmailAttachment.MemberEmailId = memberEmail.Id;
                    memberEmailAttachment.CreatorId = base.CurrentMember.Id;
                    _memberEmailId = memberEmail.Id;
                    Facade.AddMemberEmailAttachment(memberEmailAttachment);
                }

                UploadedFiles.Clear();

                if (Directory.Exists(TempDirectory))
                {
                    try
                    {
                        Directory.Delete(TempDirectory);
                    }
                    catch
                    {
                    }
                }

                UploadedFiles = null;
                TempDirectory = null;
            }
            else
            {
                memberEmail.NoOfAttachedFiles = 0;
                memberEmail.AttachedFileNames = string.Empty;
            }

        }

        private void GetPathToSave(MemberEmail memberEmail, string filepathandname)
        {
            try
            {
                string pth = Page.Server.MapPath("..\\Resources\\Member");
                pth += "\\" + memberEmail.ReceiverId;
                DirectoryCreation(pth);
                pth += "\\Outlook Sync";
                DirectoryCreation(pth);
                pth += "\\" + memberEmail.Id;
                DirectoryCreation(pth);
                string[] filename = Path.GetFileName(filepathandname).Split((char)Convert.ToChar("?"));
                pth += "\\" + filename[0];
                if (File.Exists(pth))
                {
                    File.Delete(pth);
                }
                string[] file = filepathandname.Split((char)Convert.ToChar("?"));
                File.Copy(file[0].ToString(), pth);
            }
            catch { }
        }

        void DirectoryCreation(string pth)
        {
            if (!Directory.Exists(pth))
            {
                Directory.CreateDirectory(pth);
            }
        }

        private void SaveMemberEmail()
        {
            if (true) // Isvalid
            {
                try
                {
                    MemberEmail memberEmail = BuildMemberEmail();
                    memberEmail = Facade.AddMemberEmail(memberEmail);
                    SaveMemberEmailDetail(memberEmail);
                    SaveMemberEmailAttachment(memberEmail);
                    memberEmail = Facade.UpdateMemberEmail(memberEmail);
                   
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }

            }
        }
        private string getToEmailAddress()
        {
            string to = "";
            if (lblToText.Visible && lblToText.Text != "")
            {
                return lblToText.Text;
            }
            else if (txtToText.Visible)
            {
                return txtToText.Text;
            }
            else if (drpToText.Visible)
            {
                if (drpToText.SelectedItem.Text.Contains('['))
                {
                    to = drpToText.SelectedItem.Text.Substring((drpToText.SelectedItem.Text.IndexOf('[') + 1), ((drpToText.SelectedItem.Text.LastIndexOf(']') - drpToText.SelectedItem.Text.IndexOf('[')) - 1));
                    return (StringHelper.IsBlank(to.ToString())) ? txtToText.Text.Replace(',', ';') : to.ToString();
                }
            }
            return "";
           
        }
        private void SentEmail()
        {
            string receiverEMailId = getToEmailAddress();
            string password = String.Empty;
            string strEmailText = String.Empty;
            if (PageFrom == "Dashboard" || PageFrom == "JobDetail" || PageFrom == "HiringMatrix" || PageFrom == "CandidateList" || Request.RawUrl.Contains("CompanyEmail.aspx"))
                strEmailText = txtEmailEditor.InnerHtml;
            else
            {
                if (Request.Url.ToString().ToLower().Contains("internalemaileditor.aspx"))
                {
                    strEmailText = txtEmailEditor.InnerHtml;
                }
                else
                {
                    if (_tomemberId > 0)
                    {
                        string PrimaryEmail = Facade.GetMemberUserNameById(_tomemberId);
                        System.Web.Security.MembershipUser user = System.Web.Security.Membership.GetUser(PrimaryEmail);
                        try
                        {
                            password = user.ResetPassword();
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    strEmailText = txtEmailEditor.InnerHtml.Replace("**********", password);
                    SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.LOGIN_PAGE, string.Empty);
                    strEmailText = strEmailText.Replace("../Login.aspx", url.ToString());
                }
            }

            if (txtSubject.Text != "")
            {
                MailQueueData.AddMailToMailQueue(CurrentMember.Id, receiverEMailId, txtSubject.Text, strEmailText, txtBCC.Text, txtCC.Text, EmailFileUpload1.Files, Facade);
                //if (PageFrom == "HiringMatrix")
                //           MiscUtil.EventLogForCandidate(EventLogForCandidate.RequisitionDetailsEmailedToCandidate, JobID, SelectedList, CurrentMember.Id, Facade);
                if (PageFrom == "Dashboard" || PageFrom == "JobDetail" || PageFrom == "HiringMatrix" || PageFrom == "CandidateList" || PageFrom == "History" || this.Page.Request.Url.AbsoluteUri.ToString().Contains("NewMail.aspx"))
                {
                    txtEmailEditor.Text = "";
                    string str = "alert('" + "Successfully sent email" + "');self.close();";
                    ScriptManager.RegisterStartupScript(updatepanelbuttons, updatepanelbuttons.GetType(), "JVM", str, true);
                }
                else
                {
                    if (EmailFromType == (EmailFromType)EmailFromType.CompanyEmail || EmailFrom == "Company")
                        lblMessage.Text = "Successfully sent email to Contact.";
                    else if (EmailFromType == (EmailFromType)EmailFromType.EmployeeEmail || EmailFrom == "Employee")
                        lblMessage.Text = "Successfully sent email to User.";
                    else if (EmailFromType == (EmailFromType)EmailFromType.TPS360Access)
                        lblMessage.Text = "Successfully sent password reset email to User.";
                    else
                        lblMessage.Text = "Successfully sent email to Candidate.";
                    try
                    {
                        //Response.Cookies["Message"].Value = lblMessage.Text;
                        //Helper.Url.Redirect(Request.Url.AbsoluteUri .ToString(), string.Empty, UrlConstants.PARAM_MSG, lblMessage.Text);
                        MiscUtil.ShowMessage(lblMessage, lblMessage.Text, false);
                        Clear();
                    }
                    catch
                    {
                    }
                }
            }
        }

       

        //private void SentEmail()
        //{
        //    txtSubject.Text = MiscUtil .RemoveScript (txtSubject.Text,string .Empty );
        //    if (txtSubject.Text.ToString() != string.Empty)
        //    {
        //        string sentStatus = string.Empty;
        //        EmailHelper emailManager = new EmailHelper();
                 
        //        emailManager.To.Clear();
        //        emailManager.From =  CurrentMember.PrimaryEmail ;
        //        char[] Delimeter = { ',', ';' };
        //        if (lblToText.Visible ==false )
        //        {
        //            string[] TOarr;
        //            TOarr = txtToText.Text.Split(Delimeter ,StringSplitOptions .RemoveEmptyEntries );//(char)Convert.ToChar(","));
        //            if (TOarr.Length > 0)
        //            {
        //                foreach (string s in TOarr) emailManager.To.Add(s.Trim());
        //            }
        //        }
                
        //        else
        //            emailManager.To.Add(lblToText.Text);
        //        if (EmailFromType == EmailFromType.CompanyEmail)
        //        {
        //            if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Reply") == 0)
        //            {
        //                emailManager.To.Clear();
        //                emailManager.To.Add(lblToText.Text);
        //            }
        //            if(drpToText .Items .Count !=0)
        //            {
        //                emailManager.To.Clear();
        //                emailManager.To.Add(drpToText.SelectedItem.Text.Substring((drpToText.SelectedItem.Text.IndexOf('[') + 1), ((drpToText.SelectedItem.Text.LastIndexOf(']') - drpToText.SelectedItem.Text.IndexOf('['))) - 1));
        //            }
        //        }
        //        if (txtCC.Text.Trim() != string.Empty)
        //        {
        //            string[] CCarr;
        //            CCarr = txtCC.Text.Split(Delimeter , StringSplitOptions.RemoveEmptyEntries);
        //            if (CCarr.Length > 0)
        //            {
        //                foreach (string s in CCarr)emailManager.Cc.Add(s.Trim());
        //            }
        //        }
        //        //Adding BCC
        //        if (txtBCC.Text.Trim() != string.Empty)
        //        {
        //            string[] BCCarr;
        //            BCCarr = txtBCC.Text.Split(Delimeter, StringSplitOptions.RemoveEmptyEntries);
        //            if (BCCarr.Length > 0)
        //            {
        //                foreach (string s in BCCarr) emailManager.Bcc.Add(s.Trim());
        //            }
        //        }

        //        emailManager.Subject = MiscUtil.RemoveScript(txtSubject.Text);

        //        //For Adding Attachment
        //        if (PageFrom == "Dashboard" || PageFrom == "JobDetail" || PageFrom == "HiringMatrix" || PageFrom == "CandidateList" || (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Forward") != 0))
        //        {
        //            GridView gvFiles = (GridView)EmailFileUpload1.FindControl("gvFiles");
        //            HiddenField hdnTmpFolder = (HiddenField)EmailFileUpload1.FindControl("hdnTmpFolder");

        //            string path = System.IO.Path.Combine(UrlConstants.TempDirectory + "\\", CurrentUserId.ToString()) + "\\" + hdnTmpFolder.Value;
        //            foreach (GridViewRow row in gvFiles.Rows)
        //            {
        //                HyperLink lnkFileName = (HyperLink)row.FindControl("lnkFileName");
        //                emailManager.Attachments.Add(path + "\\" + lnkFileName.Text);
        //            }
        //        }
        //        else
        //        {
        //            if (_memberEmailId > 0)
        //            {
        //                IList<MemberEmailAttachment> memberEmailAttachments = null;
        //                if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Reply") != 0)
        //                {
        //                    memberEmailAttachments = Facade.GetAllMemberEmailAttachmentByMemberEmailId(_memberEmailId);
        //                }
        //                string filePath = null;
        //                string contentPath = UrlConstants.GetEmailDocumentDirectory(_frommemberId.ToString());
        //                if (memberEmailAttachments != null)
        //                {
        //                    foreach (MemberEmailAttachment memberEmailAttachment in memberEmailAttachments)
        //                    {
        //                        filePath = Path.Combine(contentPath, memberEmailAttachment.AttachmentFile);
        //                        emailManager.Attachments.Add(filePath);
        //                    }
        //                }
        //            }

        //        }
        //        string password = String.Empty;
        //        string strEmailText = String.Empty;
        //        if (PageFrom == "Dashboard" || PageFrom == "JobDetail" || PageFrom == "HiringMatrix" || PageFrom == "CandidateList" || Request .RawUrl .Contains ("CompanyEmail.aspx"))
        //            strEmailText = txtEmailEditor.InnerHtml;
        //        else
        //        {
        //            if (Request.Url.ToString().ToLower().Contains("internalemaileditor.aspx"))
        //            {
        //                strEmailText = txtEmailEditor.InnerHtml;
        //            }
        //            else
        //            {
        //                if (_tomemberId > 0)
        //                {
        //                    string PrimaryEmail = Facade.GetMemberUserNameById(_tomemberId);
        //                    System.Web.Security.MembershipUser user = System.Web.Security.Membership.GetUser(PrimaryEmail);
        //                    // if (chkResetPassword.Checked)
        //                    {
        //                        try
        //                        {
        //                            password = user.ResetPassword();
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                        }
        //                    }
        //                    //else
        //                    //{
        //                    //    try
        //                    //    {
        //                    //        password = user.GetPassword();
        //                    //    }
        //                    //    catch (Exception ex)
        //                    //    {
        //                    //    }
        //                    //}
        //                    strEmailText = txtEmailEditor.InnerHtml.Replace("**********", password);
        //                    SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.LOGIN_PAGE, string.Empty);
        //                    strEmailText = strEmailText.Replace("../Login.aspx", url.ToString());

        //                }
        //            }
        //        }
                
        //        emailManager.Body = strEmailText;
        //        sentStatus = emailManager.Send(base.CurrentMember.Id);
        //        if (sentStatus == "1")
        //        {
        //            SaveMemberEmail();
        //            sentStatus = "Sent";
        //            if (PageFrom == "HiringMatrix")
        //                MiscUtil.EventLogForCandidate(EventLogForCandidate.RequisitionDetailsEmailedToCandidate, JobID, SelectedList, CurrentMember.Id, Facade);
        //            if (PageFrom == "Dashboard" || PageFrom == "JobDetail" || PageFrom == "HiringMatrix" || PageFrom == "CandidateList" || PageFrom == "History" || this.Page.Request.Url.AbsoluteUri.ToString().Contains("NewMail.aspx"))
        //            {
        //                txtEmailEditor.Text = "";
        //                string str = "alert('" + "Successfully sent email" + "');self.close();";
        //                ScriptManager.RegisterStartupScript(updatepanelbuttons, updatepanelbuttons.GetType(), "JVM", str, true);
        //            }
        //            else
        //            {
        //                if (EmailFromType == (EmailFromType)EmailFromType.CompanyEmail || EmailFrom =="Company")
        //                    lblMessage.Text = "Successfully sent email to Contact.";
        //                else if (EmailFromType == (EmailFromType)EmailFromType.EmployeeEmail || EmailFrom =="Employee")
        //                    lblMessage.Text = "Successfully sent email to User.";
        //                else if (EmailFromType == (EmailFromType)EmailFromType.TPS360Access)
        //                    lblMessage.Text = "Successfully sent password reset email to User.";
        //                else
        //                    lblMessage.Text = "Successfully sent email to Candidate.";
        //                  try
        //                  {
        //                      //Response.Cookies["Message"].Value = lblMessage.Text;
        //                      //Helper.Url.Redirect(Request.Url.AbsoluteUri .ToString(), string.Empty, UrlConstants.PARAM_MSG, lblMessage.Text);
        //                      MiscUtil.ShowMessage(lblMessage, lblMessage.Text, false  );
        //                      Clear();
        //                  }
        //                  catch
        //                  {
        //                  }
        //            }
        //        }
        //        else
        //        {
        //            sentStatus = "Not Sent";
        //            if (_memberEmailId > 0)
        //            {
        //                Facade.DeleteMemberEmailAttachmentByMemberEmailId(_memberEmailId);
        //                Facade.DeleteMemberEmailDetailByMemberEmailId(_memberEmailId);
        //                Facade.DeleteMemberEmailById(_memberEmailId);
        //            }
        //            lblMessage.Text = "email has not been sent, please check for valid email id.";
        //            MiscUtil.ShowMessage(lblMessage, lblMessage.Text, true); //0.12
        //        }
        //    }
        //}


        private void Clear()
        {
            txtBCC.Text = "";
            txtCC.Text = "";
            txtSubject.Text = "";
            txtEmailEditor.InnerHtml = "";
            GetMemberId();
            PrepareView();
            UploadedFiles.Clear();
            GridView gview = (GridView)EmailFileUpload1.FindControl("gvFiles");
            gview.DataSource = null;
            gview.DataBind();
            System.Web.UI.HtmlControls.HtmlTableRow row = (System.Web.UI.HtmlControls.HtmlTableRow)EmailFileUpload1.FindControl("tdUploadedFiles");
            row .Visible =false ;
            System.Web.UI.HtmlControls.HtmlTableRow tdAttachFile = (System.Web.UI.HtmlControls.HtmlTableRow)EmailFileUpload1.FindControl("tdAttachFile");
            tdAttachFile.Visible = false;
        }
        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
		   if (Request.Url.AbsoluteUri.ToString().Contains("/ATS/InternalEmailEditor.aspx"))
             {
                 hdnclear.Value = "true";
             }
            _mailsetting = MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id);
            drpToText.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            string CountryName = MiscUtil.GetCountryNameById(Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString()), Facade);
            if (CountryName.Trim () != string .Empty )
            {
                if (CountryName.Trim () == "India")
                {
                    System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                    System.Globalization.DateTimeFormatInfo info = new System.Globalization.DateTimeFormatInfo();
                    info.ShortDatePattern = "dd/MM/yyyy";
                    culture.DateTimeFormat = info;
                    System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                    System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
                }
            }
            if (!_mailsetting)
            dv.Visible = true;
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            if (!IsPostBack)
            {
                MailBody = txtEmailEditor.ClientID;
                Response.Cookies["Message"].Value = null;
                GetMemberId();
                string[] arr;  
                if (Session["ReferenceLink"] != null)
                {
                    _fromReference = true;
                    arr = Session["ReferenceLink"].ToString().Split((char)Convert.ToChar(" "));
                    _toReferenceMemberId = Convert.ToInt32(arr[1]);
                    _linkToRespond = arr[0];
                    EmailFromType = EmailFromType.Reference;
                    Session.Abandon();
                }
                else
                {
                    _fromReference = false;
                }

                PrepareView();
                if (txtToText.Visible)
                {
                    if (txtToText.Text.Trim() == string.Empty)
                    {
                        txtToText.Focus();
                    }
                    else
                    {
                        txtSubject.Focus();
                    }
                }
                else if (drpToText.Visible || lblToText.Visible)
                {
                    txtSubject.Focus();
                }
            }
            if (Session["Editor"] != null)
            {
                txtEmailEditor.InnerHtml = Session["Editor"].ToString();
                Session.Abandon();
            }
            lblMessage.Text = "";
            lblMessage.CssClass = "";
          //  txtEmailEditor.Focus = false;
            if (EmailFromType == (EmailFromType)EmailFromType.CompanyEmail)
            {
                Helper.Session.Set(SessionConstants.EmailFromType, "CompanyEmail");
                RFVToEmail.Visible = false;
                lblAsteric.Visible = false;
                check.Value  = "";
                check.Value  = "Company";
                string Companyname = MiscUtil.GetCompanyNameById(companyID, Facade); 
                this.Page.Title =MiscUtil .RemoveScript ( Companyname,string .Empty ) + " - " + "Email";
            }
            else
            {

                if (PageFrom == "Dashboard")
                {
                    Helper.Session.Set(SessionConstants.EmailFromType, string.Empty);
                    check.Value = "";
                    check.Value = "Candidate";
                }
                else
                {
                    Helper.Session.Set(SessionConstants.EmailFromType, string.Empty);
                    check.Value = "";
                    check.Value = "Candidate";
                }
            }
            //if (Request.Cookies["Message"].Value != null && Request.Cookies["Message"].Value != string.Empty)
            //{
            //    MiscUtil.ShowMessage(lblMessage, Request.Cookies["Message"].Value, false);
            //    Response.Cookies["Message"].Value = null;
            //}
            if(!IsPostBack )
                if (Messages != string.Empty)
                    MiscUtil.ShowMessage(lblMessage, Messages, false);
           
        }
        
        #endregion
        #region PreRender
        protected override void OnPreRender(EventArgs e)
        {
            Session["REFRESH_CHECK_GUID"] = System.Guid.NewGuid().ToString();
            ViewState["REFRESH_CHECK_GUID"] = (string)Session["REFRESH_CHECK_GUID"];
        }
        #endregion

        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                SecureUrl url=UrlHelper .BuildSecureUrl ( UrlConstants .ApplicationBaseUrl +"Employee/MailSetup.aspx",string .Empty);
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Open", "<script>window.open('" + url + "');</script>", false);
            }

        }
       
        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            if (_mailsetting) SentEmail();
            else
            {
                dv.Visible = true;
                uclConfirm.AddMessage("Mail Server SMTP details have not been entered. Would you like to enter them now?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
            }
        }
      
        private void BuildPreviewXML(string strFrom,string strTo,string strSubject, string strBCC,string strCC,string strBody,string strAttachements,bool AddSignature)
        {

            string xmlfileDirectory = string.Empty;
            string PreviewXML = string.Empty;
            PreviewXML += "<Preview>";
            PreviewXML += "<From>" + strFrom.Replace("<", "&lt;") + "</From>";
            PreviewXML += "<To>" + strTo.Replace("<", "&lt;") + "</To>";
            PreviewXML += "<Subject>" + strSubject.Replace("<", "&lt;") + "</Subject>";
            PreviewXML += "<Body>" + strBody.Replace ("<" ,"&lt;") + "</Body>";
            PreviewXML += "<BCC>" + strBCC.Replace("<", "&lt;") + "</BCC>";
            PreviewXML += "<CC>" + strCC.Replace("<", "&lt;") + "</CC>";
            PreviewXML += "<Attachments>" + strAttachements.Replace ("&","&amp;")  + "</Attachments>";
            PreviewXML += "<Signature>" + (AddSignature ==true ?"Yes":"No") + "</Signature>";
            PreviewXML += "</Preview>";

            if (StringHelper.IsBlank(TempDirectory))
            {
                string dir = CurrentUserId.ToString();
                string path = Path.Combine(UrlConstants.TempDirectory, dir);

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                TempDirectory = path;
            }
            HiddenField hdnTmpFolder = (HiddenField)EmailFileUpload1.FindControl("hdnTmpFolder");
            if (hdnTmpFolder.Value != "")
                PreviewDir = TempDirectory + "\\" + hdnTmpFolder.Value;
            else
            {
                DateTime dt = DateTime.Now;
                string s = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString() + "";
                if (!Directory.Exists(TempDirectory + "\\" + s))
                {
                    Directory.CreateDirectory(TempDirectory + "\\" + s);
                    hdnTmpFolder.Value = s;
                }
                PreviewDir  = TempDirectory + "\\" + s;
            }
            try
            {
                if (System.IO.File.Exists(PreviewDir + "\\Preview.xml"))
                {
                    System.IO.File.Delete(PreviewDir + "\\Preview.xml");
                }
            }
            catch { }
            StreamWriter sWriter = new StreamWriter(PreviewDir + "\\Preview.xml");
            sWriter .WriteLine (PreviewXML );
            sWriter.Close();
        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            GridView  gvFiles = (GridView )EmailFileUpload1.FindControl("gvFiles");
            string attach = "";
            HiddenField hdnTmpFolder = (HiddenField)EmailFileUpload1.FindControl("hdnTmpFolder");
            foreach (GridViewRow  row in gvFiles.Rows )
            {
                HyperLink lnkFileName=(HyperLink )row .FindControl ("lnkFileName");
                    if (attach != "") attach += ";";
                    attach += lnkFileName.Text + "?" + lnkFileName.NavigateUrl;
            }
            string MailToAddress = "";
            if (drpToText.Visible)
                MailToAddress = drpToText.SelectedItem.Text.Trim();
            else if (txtToText.Visible)
                MailToAddress = txtToText.Text.Trim();
            else if (lblToText.Visible)
                MailToAddress = lblToText.Text.Trim();

            if (EmailFromType == EmailFromType.TPS360Access)
            {
                //EmailHelper emailHelper = new EmailHelper();
                //Member toMember = Facade.GetMemberById(_tomemberId);
                //string body = emailHelper.PrepareViewforTps360Access(toMember);

                //     if(chkIncludeSignature .Checked )
                //     {body += "</tr><tr><td>" +     hfSignature.Value + "</td>";
                //     }
                //body += "</tr></table>";
                
          
                BuildPreviewXML(CurrentMember.PrimaryEmail, MailToAddress,MiscUtil .RemoveScript ( txtSubject.Text), txtBCC.Text, txtCC.Text,txtEmailEditor .TextXhtml, attach, chkIncludeSignature.Checked);
            }
            else
            {
                if (txtEmailEditor.InnerHtml.StartsWith("<body"))
                {
                    txtEmailEditor.InnerHtml = txtEmailEditor.InnerHtml.Replace("<body bgcolor='White' style='font-family:sans-serif;font-size:10pt;'><tr><td>", "");
                    txtEmailEditor.InnerHtml = txtEmailEditor.InnerHtml.Replace("</td></tr></table></body>", "");
                }
                BuildPreviewXML(CurrentMember.PrimaryEmail, MailToAddress,MiscUtil .RemoveScript ( txtSubject.Text), txtBCC.Text, txtCC.Text, txtEmailEditor.InnerHtml, attach, chkIncludeSignature.Checked);
            }
             
             SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.CommonPages.EMAIL_PREVIEW , string .Empty , "TempFolder",hdnTmpFolder .Value );
             string scriptRun = "<script> var w= window.open('" + url.ToString() + "','PreviewEmailWindow', '');</script>";
             ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "PreviewEmailWindow", scriptRun,false );
        }
        #endregion
        protected void chkIncludeSignature_CheckedChanged(object sender, EventArgs e)
        {
            
            if (hfSignature.Value  != string .Empty )
            {
                if (chkIncludeSignature.Checked)
                    txtEmailEditor.InnerHtml = txtEmailEditor.InnerHtml.Trim() + hfSignature.Value.ToString();
                else
                    txtEmailEditor.InnerHtml = txtEmailEditor.InnerHtml.Replace(hfSignature.Value.Trim().ToString(), "");
            }
        }
}
}