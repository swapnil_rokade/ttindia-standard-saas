﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VendorSubmissionSmallPager.ascx.cs" Inherits="TPS360.Web.UI.Controls_VendorSubmissionSmallPager" %>
<%@ Register Src ="~/Controls/SmallPagerControl.ascx" TagName ="Pager" TagPrefix ="ucl" %>  
 
<asp:HiddenField ID="hdnMySubmission" runat="server" /> 
 <asp:UpdatePanel ID="upDetail" runat ="server" >
                <ContentTemplate >
                <asp:TextBox ID="txtSortColumn" runat="server" Visible="false" Text ="lnkDateSubmitted"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Visible="false" Text ="DESC"></asp:TextBox>
                  <asp:ObjectDataSource ID="odsVendorSubmissions" runat="server" SelectMethod="GetPaged_ForVendorSubmissionDetail"
                TypeName="TPS360.Web.UI.CompanyConatctDataSource" SelectCountMethod="GetListCount_ForVendorSubmissionDetail"
                EnablePaging="True" SortParameterName="sortExpression" >
                <SelectParameters>
               
                                  <asp:Parameter Name ="StartDate" DefaultValue ="" />
                                        <asp:Parameter Name ="EndDate" DefaultValue ="" />
                       <asp:Parameter Name ="JobPostingId" DefaultValue ="0" />
                            <asp:Parameter Name ="VendorId" DefaultValue ="0" />
                            <asp:Parameter Name ="ContactId" DefaultValue ="0" />
                            <asp:Parameter Name ="CandidateId" DefaultValue ="0" />
                            <asp:Parameter Name="Type" DefaultValue ="" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:ListView ID="lsvVendorSubmissions" runat="server" DataKeyNames="Id" DataSourceID="odsVendorSubmissions"
                OnItemDataBound="lsvVendorSubmissions_ItemDataBound" EnableViewState="true" OnPreRender="lsvVendorSubmissions_PreRender"
                OnItemCommand="lsvVendorSubmissions_ItemCommand">
                <LayoutTemplate>
                    <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                        <tr>
                            <th  style =" width : 110px !important" >
                                <asp:LinkButton ID="lnkDateSubmitted" runat="server" ToolTip="Sort By Date Submitted" CommandName="Sort"
                                    CommandArgument="[MJC].[CreateDate]" Text="Date Submitted" />
                            </th>
                            <th id="thCandidateName" runat ="server">
                                <asp:LinkButton ID="lnkCandidate" runat="server" ToolTip="Sort By Candidate Name" CommandName="Sort"
                                    CommandArgument="[C].[CandidateName]" Text="Candidate Name" />
                            </th>
                             <th  id="thSubmittedBy" runat ="server" >
                                <asp:LinkButton ID="lnkSubmittedBy" runat ="server" Text ="Submitted By" CommandName ="Sort" CommandArgument ="M.FirstName"></asp:LinkButton>
                               
                            </th>
                            <th>
                                <asp:LinkButton ID="lnkRequisition" runat="server" ToolTip="Sort By Job Title" CommandName="Sort"
                                    CommandArgument="[JP].JobTitle" Text="Job Title" />
                            </th>
                            <%-- ***********Code added by pravin khot on 15/March/2017***************--%>
                            <th style="min-width: 100px; white-space: nowrap;">
                                   <asp:Label ID="lblStatus" runat="server" ToolTip="Status" Text="Status" />
                            </th>
                           <%-- ************************END*********************************************--%>
                        </tr>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr class="Pager">
                            <td colspan="4" id="tdPager" runat="server">
                                <ucl:Pager ID="pagerControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server"
                        style="width: 100%; margin: 0px 0px;">
                        <tr>
                            <td>
                                No Recent Status Changes.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td>
                            <asp:Label ID="lblDateTime" runat="server"  />
                        </td>
                        <td>
                           <asp:HyperLink ID="hlnkCandidateName" runat="server" ></asp:HyperLink>
                        </td>
                       <td>
                            <asp:HyperLink ID="hlnkSubmittedBy" runat ="server"  ></asp:HyperLink>
                        </td>
                        <td>
                           <asp:HyperLink ID="lnkJobTitle" runat="server" Target="_blank" TabIndex="8" Width="120px" ></asp:HyperLink>
                        </td>
                         <td>
                            <asp:Label ID="lblStatus" runat="server"  />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>

                  
                  </ContentTemplate>
                </asp:UpdatePanel>