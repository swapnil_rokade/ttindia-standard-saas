﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ChangeHiringStatusOfCandidate.ascx
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date               Author                 MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             2/Mar/2017          Sumit Sonawane            New function
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChangeHiringStatusOfCandidate.ascx.cs"
    Inherits="TPS360.Web.UI.ChangeHiringStatusOfCandidate" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<script>
      function EditClicked()
      {
      $('html, body').animate({
						scrollTop: 0
					}, 400);
      }
</script>

<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<asp:UpdatePanel ID="upDocuments" runat="server">
    <ContentTemplate>
        <div>
            <div class="TableRow" style="text-align: center">
                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
                <asp:HiddenField ID="hfMemberDocumentId" runat="server" />
                <asp:HiddenField ID="hfMemberDocumentName" runat="server" />
                <asp:HiddenField ID="hfDocumentType" runat="server" />
            </div>
            <div class="TabPanelHeader">
                Hiring Status
            </div>
            <div class="TableRow" runat="server" id="rowDocumentType">
                <div class="TableFormLeble">
                    <asp:Label ID="lblDocumentType" runat="server" Text="Hiring Status"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList ID="ddlChangeHiringStatus" runat="server" CssClass="CommonDropDownList"
                        OnSelectedIndexChanged="ddlChangeHiringStatus_SelectedIndexChanged">
                    </asp:DropDownList>
                      <asp:HiddenField ID="hdnSelectedIndex" runat="server" />
                      <asp:HiddenField ID="hdnCurrentlevelId" runat="server" />
                    <span class="RequiredField">*</span>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:CompareValidator ID="cvDocumentType" ValidationGroup="UploadDocument" runat="server"
                        ControlToValidate="ddlChangeHiringStatus" ErrorMessage="Please Select Hiring Status."
                        Operator="NotEqual" ValueToCompare="0" Display="Dynamic"></asp:CompareValidator>
                </div>
            </div>
            <div class="TableRow" style="visibility: hidden">
                <div class="TableFormLeble">
                    <asp:Label ID="lblDocumentTitle" runat="server" Text="Document Title"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtDocumentTitle" runat="server" CssClass="CommonTextBox" Width="250px"></asp:TextBox>
                    <span class="RequiredField">*</span>
                </div>
            </div>
            <%--<div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator ID="rfvDocumentTitle" runat="server" ValidationGroup="UploadDocument"
                        ControlToValidate="txtDocumentTitle" Display="Dynamic" ErrorMessage="Please enter document title."></asp:RequiredFieldValidator>
                </div>
            </div>--%>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblDocumentDescription" runat="server" Text="Add Comment"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtDocumentDescription" TextMode="MultiLine" Height="100px" runat="server"
                        CssClass="CommonTextBox" Width="250px"></asp:TextBox>
                        <span class="RequiredField">*</span>
                </div>                
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator ID="rfvDocumentDescription" runat="server" ValidationGroup="UploadDocument"
                        ControlToValidate="txtDocumentDescription" Display="Dynamic" ErrorMessage="Please Enter Comment."></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="TableRow" style="visibility: hidden">
                <div class="TableFormLeble">
                    <asp:Label ID="lblDocumentPath" runat="server" Text="Document Path"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:FileUpload ID="fuDocument" runat="server" />
                    <span class="RequiredField">*</span>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:UpdatePanel ID="pnlValidator" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                          
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlChangeHiringStatus" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                </div>
                <div class="TableFormContent">
                    <asp:Button ID="btnUpload" CssClass="CommonButton" ValidationGroup="UploadDocument"
                        runat="server" Text="Submit" OnClick="btnUpload_Click" />
                </div>
            </div>
            <div class="TabPanelHeader">
                List of Hiring Status
            </div>
            <div class="TableRow" style="text-align: left; padding-bottom: 20px;">
                <asp:ObjectDataSource ID="odsDoc" runat="server" SelectMethod="GetPagedHiringMatrixLogByMemberId"
                    TypeName="TPS360.Web.UI.EventLogDataSource" SelectCountMethod="GetListCount"
                    EnablePaging="true" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:Parameter Name="MemberID" DefaultValue="" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ListView ID="lsvDocuments" runat="server" DataKeyNames="Id" DataSourceID="odsDoc"
                    EnableViewState="true" OnPreRender="lsvDocuments_PreRender" OnItemDataBound="lsvDocuments_ItemDataBound" OnItemCommand="lsvDocument_OnItemCommand">
                    <LayoutTemplate>
                        <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                            <tr>
                                <th>
                                    Hiring Status
                                </th>
                                 <th>
                                    User Name
                                </th>
                                <th>
                                    Date Time
                                </th>
                                <th>
                                    Comment
                                </th>
                                <th style="width: 0px; visibility: hidden;">
                                    Action
                                </th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <tr class="Pager">
                                <td colspan="4">
                                    <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" NoOfRowsCookie="MemberDocunebtUploaderRowPerPage"/>
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <EmptyDataTemplate>
                        <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                            <tr>
                                <td>
                                    No data was returned.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                            <td >
                                <asp:Label ID="lblDocumentLink" runat="server"></asp:Label>
                            </td>
                              <td >
                                <asp:Label ID="lblUserNameUpdator" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblDocumentTitle" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblDocumentType" runat="server"></asp:Label>
                            </td>
                            <td visible="False">
                                <asp:ImageButton ID="btnEdit" SkinID="sknEditButton"  runat="server" CommandName="EditItem"
                                    OnClientClick="EditClicked()" ImageUrl="~/Images/EditImage.png"></asp:ImageButton>
                                <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem" ImageUrl="~/Images/DeleteImage.png">
                                </asp:ImageButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpload" />
    </Triggers>
</asp:UpdatePanel>
