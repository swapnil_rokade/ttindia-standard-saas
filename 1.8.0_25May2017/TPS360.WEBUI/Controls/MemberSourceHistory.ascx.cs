﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using System.IO;

namespace TPS360.Web.UI
{
    public partial class MemberSourceHistory :  ATSBaseControl   
    {
       
        #region Member Variables
    
        #endregion

        #region Properties
       
    
        #endregion

        #region Methods

         
        
        #endregion

        #region PageEvents
        protected void Page_Load(object sender, EventArgs e)
        {
            string canid = Request.QueryString["Canid"].ToString();
            odsSourceHistory.SelectParameters["MemberId"].DefaultValue = canid;
            if (!IsPostBack)
            {
                Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
                if (lbModalTitle != null)
                {
                    lbModalTitle.Text = "Source History";
                }

               // lsvSourceHistoryView.DataBind();
                //hdncheck.Value = "";
            }
           /* else
                hdncheck.Value = "s";*/
            if (!IsPostBack)
            {
                hdnSortColumn.Value = "lblDate";
                hdnSortOrder.Value = "DESC";
            }
        }
        protected void lsvSourceHistoryView_PreRender(object sender, EventArgs e)
        {
            if (lsvSourceHistoryView != null)
            {
                HtmlTableCell tdpager = (HtmlTableCell)lsvSourceHistoryView.FindControl("tdPager");
                ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvSourceHistoryView.FindControl("pagerControl");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }
                    HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                    if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "MemberSourceHistorybyTeamRowPerPage";
            
                     
                }

            }
        }
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvSourceHistoryView.FindControl(hdnSortColumn.Value);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        protected void lsvSourceHistoryView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                TPS360.Common.BusinessEntities.MemberSourceHistory membersourcehistory = ((ListViewDataItem)e.Item).DataItem as TPS360.Common.BusinessEntities.MemberSourceHistory;

                if (membersourcehistory != null)
                {
                    Label lblDate = (Label)e.Item.FindControl("lblDate");
                   // Label lblUser = (Label)e.Item.FindControl("lblUser");
                    Label lblSource = (Label)e.Item.FindControl("lblSource");
                    Label lblsourcedescription = (Label)e.Item.FindControl("lblsourcedescription");

                    if (membersourcehistory.MemberId > 0)
                    {
                        lblDate.Text = membersourcehistory.ExpireDate.ToString();// member.FirstName + " " + member.LastName;
                       // lblUser.Text = membersourcehistory.UserId.ToString();
                        lblSource.Text = membersourcehistory.SourceName.ToString();
                        lblsourcedescription.Text = membersourcehistory.SourceDescriptionName.ToString();
                    }
                    
                }
            }
        }

        #endregion

     
        #region Button Events
        
       
        #endregion
    }
}