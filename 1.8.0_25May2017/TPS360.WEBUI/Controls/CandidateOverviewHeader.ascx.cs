﻿/*<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateOverviewHeader.ascx.cs
    Description: This is the user control page used to display the ooption like ToDo,PDF,Print and many more
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date              Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Oct-2-2008        Gopala Swamy J      Defect id: 8914; supllied correct Path to  UrlPdf Variable
    0.2             Oct-20-2008       Gopala Swamy J      Defect id: 8991; changed the From "Hour to salary " to " Yearls" salary 
    0.3             Dec-10-2008       Shivanand           Defect id: 9494; 
                                                                In the method LoadCandidateBasicData(), label "lblSSN" is popuplated 
                                                                from currentCandidateDetail object.
                                                                In the method LoadConsultantBasicData(), label "lblSSN" is popuplated 
                                                                from currentConsultantDetail object.
    0.4             Dec-19-2008       Yogeesh Bhat        Defect Id: 9534: Changes made in LoadConsultantBasicData() method
    0.5             Dec-24-2008       Yogeesh Bhat        Defect Id: 9602: Changes made in Page_Load()method; Made "ResumeShare" link visible false.
    0.6             Jan-13-2009       Jagadish            Defect Id: 9680; Commented the code that use hyperlink 'linkLastActivities'.
    0.7             Jan-15-2009       Jagadish            Defect ID: 9690,9729: Changed the hyperlink 'lnkJobs' to lable.
    0.8             Jan-19-2009       Jagadish            Defect ID: 9682: Made the visibility os the button 'btnSendHRMS' false if login user is not a 'Super admin'
    0.9             Feb-16-2009       Kalyani pallagani   Defect ID: 9885: Added LoadMemberVideo() method to load video resume for both candidate and consultant
    1.0             Mar-03-2009       Jagadish            Defect id: 10038; Changes made in the method 'Page_Load()'.
 *  1.1             Apr-16-2009       Rajendra            Defect id: 10308; Changes made in the Show error Message().
    1.2             Jul-08-2009       Veda                Defect id: 10845: Primary Manager" link can only be accessed by the Superuser and primary manager of that candidate. 
    1.3             Aug-07-2009       Gopala Swamy J      Defect id: 11209: Changed "LoadConsultantBasicData()" and "LoadCandidateBasicData()"
 *  1.4             Nov-19-2009       Sandeesh            Enhancement#11814 :AM Notification - When a recruiter adds a candidate to a req the app will send a notification email to the Account Manager
    1.5             Dec-23-2009       Rajendra            Defect id: 12036: Added code in Page_Load(), btnSaveNotes_Click();
    1.6             apr-16-2010        Nagarathna V.B     enhance:12140: removed "btnSendHRMS" button.
 *  1.7             Apr-22-2010       Ganapati Bhat       Defect id: 12668; Code Added in LoadCandidateBasicData()
 *  1.8             Apr-22-2010       Basavaraj A         Defect 11923 , Added code to btnSaveNotes_Click , to Restrict User who is already added to same requisition
 *  1.9             May-04-2010       Ganapati Bhat       Defect id:12750 ; Added Code in LoadCandidateBasicData to avoid overlapping Firstname & Lastname 
 *  2.0             28/April/2016     pravin khot         added new feture code- AllowCandidatetobeaddedonMultipleRequisition
 *  2.1             16/June/2016      pravin khot         modify lblStatus.text
 *  2.2             20/Jan/2017       Sumit Sonawane      fetch age from DOB GetCandidateAgeByMemberID(MemberID)
 *  2.3             15/March/2016     pravin khot         When hiring manager login then action button visible false
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>*/
using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI;
using TPS360.Common.Shared;
using TPS360.Common;
using System.Configuration;
using System.Collections;
using System.Net.Mail;
using System.Net;
using System.Threading;
using System.Globalization;
using System.Web.Security;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Web;
using TPS360.BusinessFacade;


namespace TPS360.Web.UI
{
    public partial class ctlCandidateOverviewHeader : BaseControl
    {
        #region Member Variables

        private  int _memberId = 0;
        private bool isVolumeHire;
        private string _roleMember = string.Empty;
        private bool IsPrimaryManager = false;
        private static string UrlForCandidate = string.Empty;
        private static int IdForSitemap = 0;
        private int JobId = 0;
        #endregion

        #region Property

        private void getJobPostingId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID ]))
            {
                JobId = Int32.Parse(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]);

            }
        }

     
        public string RoleMember
        {
            get
            {
                return _roleMember;
            }
            set
            {
                _roleMember = value;
            }
        }
        public bool IsVolumeHire
        {
            get
            {
                return isVolumeHire;
            }
            set
            {
                isVolumeHire = value;
            }
        }

        public delegate void CandidateJobcartEventHandler();
        public event CandidateJobcartEventHandler RequisitionAddedForCandidate;
        #endregion

        #region Methods
        public bool IsRefreshed
        {
            get
            {
                if (Session["REFRESH_CHECK_GUID"] == null
                || ViewState["REFRESH_CHECK_GUID"] == null
                || Session["REFRESH_CHECK_GUID"].ToString().Equals(ViewState["REFRESH_CHECK_GUID"].ToString()))
                {
                    return false;
                }
                else
                    return true;
            }
        }
        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Int32.Parse(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

            }
            //From Member Login
            else
            {
                Member currentCandidate = Facade.GetMemberById(_memberId);
                _memberId = currentCandidate.Id;
            }
            hiddenMemberId.Value = _memberId.ToString();
        }

        public void RenderCurrentCandidate()
        {
            Member currentCandidate = Facade.GetMemberById(_memberId);

            if (currentCandidate != null)
            {
                LoadHotList();
                    LoadCandidateBasicData(currentCandidate);

                    ArrayList AccessArray = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                    if (AccessArray.Contains(408))
                    {
                        SecureUrl url = UrlHelper.BuildSecureUrl("../ATS/InternalEmailEditor.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, "408", UrlConstants.PARAM_MEMBER_ID, currentCandidate.Id.ToString());
                        lnkSendEmail.Attributes.Add("href", url.ToString());// +">" + currentCandidate.PrimaryEmail + "</a>";

                    }
                    else
                    {
                        lnkSendEmail.Attributes.Add("href", "MailTo:" + currentCandidate.PrimaryEmail + ">" + currentCandidate.PrimaryEmail);
                        //lblPrimaryEmail.Text = "<a href=MailTo:" + currentCandidate.PrimaryEmail + ">" + currentCandidate.PrimaryEmail + "</a>";
                    }
                    if (AccessArray.Contains(407))
                    {
                        SecureUrl url = UrlHelper.BuildSecureUrl("../ATS/InternalNotesAndActivities.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, "407", UrlConstants.PARAM_MEMBER_ID, currentCandidate.Id.ToString());
                        lnkAddNote.Attributes.Add("href", url.ToString());
                    }
                    else lnkAddNote.Visible = false;
                    if (AccessArray.Contains(403))
                    {
                        SecureUrl url = UrlHelper.BuildSecureUrl("../ATS/InternalInterviewSchedule.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, "403", UrlConstants.PARAM_MEMBER_ID, currentCandidate.Id.ToString());
                        lnkScheduleInterview.Attributes.Add("href", url.ToString());
                    }
                    else lnkScheduleInterview.Visible = false;

            }
        }

        private void populateCandidateOverviewdetails()
        {
            GetMemberId();
            CandidateOverviewDetails overviewdetail = Facade.GetCandidateOverviewDetails(_memberId);
            if (overviewdetail != null)
            {
                lblCandidateId.Text = "A" + overviewdetail.Id.ToString();
                lblCandidateName.Text = overviewdetail.CandidateName;

                int reqCount = Facade.GetActiveRequsiutionCountByMemberId(overviewdetail.Id);
                lblJobTitle.Text = overviewdetail.JobTitle;
                lblPrimaryManagerName.Text = overviewdetail.PrimaryManager;
                lblPrimaryEmail.Text = overviewdetail.CandidateEmail;
                lblCompany.Text = overviewdetail.CurrentCompany;
                imgInterRating.ImageUrl = MiscUtil.GetInternalRatingImage(overviewdetail.InternalRating.ToString());
                lblMobile.Text = overviewdetail.Mobile;
                lblExperience.Text = overviewdetail.YearsOfExperience;
       
                if (overviewdetail.Status == 1)
                {
                    lblStatus.Text = "InActive";
                }
                else if (reqCount > 1)
                {
                    //*******Code modify by pravin khot on 16/June/2016*******
                   // lblStatus.Text = "Profile is Process By " + reqCount.ToString(); 
                    lblStatus.Text = "Profile in Process (" + reqCount.ToString() + ") Requisitions";
                    //*******************END******************************
                }
                else
                {
                    lblStatus.Text = "Profile in Process by " + Facade.GetRecruiternameByMemberId(_memberId);
                    divAddRequisition.Visible = false;
                }


                //ResumeSource resumeSource = (ResumeSource)overviewdetail .ResumeSourceId;
                //string strResumeSource = EnumHelper.GetDescription(resumeSource);
                //if (resumeSource == ResumeSource.ReferralProgram)
                //{
                //    EmployeeReferral reff = Facade.EmployeeReferral_GetByMemberId(_memberId);
                //    if (reff != null)
                //    {
                //        lblResumeSource.Text = "Referred by " + reff.RefererFirstName + " " + reff.RefererLastName + " " + reff.CreateDate.ToShortDateString();
                //    }
                //}
                ////else if (resumeSource == ResumeSource.Vendor)
                ////{
                ////    string comname = "Vendor";//Facade .GetCompanyById ( Facade .GetCompanyContactByMemberId (overviewdetail.  ).CompanyId ).CompanyName ;
                ////    lblResumeSource.Text ="Sourced by: "+ overviewdetail.ResumeSourceName + ", Vendor: " +  comname + " on " + overviewdetail .CreateDate .ToShortDateString ();
                ////}
                //else
                //{
                //    if (resumeSource != ResumeSource.SelfRegistration)
                //    {
                //        strResumeSource = strResumeSource + " (" + overviewdetail.ResumeSourceName + ")";

                //    }
                //}


                lblResumeSource.Text = overviewdetail.ResumeSourceName;


                //lblResumeSource.ToolTip =lblResumeSource.Text = strResumeSource;
                lblLocation.Text = overviewdetail.Location;
                lblHighestDegree.Text = overviewdetail.HighestEducation;

                lblAvailibility.Text = overviewdetail.Availability;
                lblLastUpdated.Text = overviewdetail.LastUpdated.ToShortDateString();

                string currentparentid = Helper.Url.SecureUrl[UrlConstants.PARAM_SITEMAP_PARENT_ID];
                ArrayList AccessArray = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                if (AccessArray.Contains(408))
                {
                    SecureUrl url = UrlHelper.BuildSecureUrl("../ATS/InternalEmailEditor.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, "408", UrlConstants.PARAM_MEMBER_ID, overviewdetail.Id.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentparentid);
                    lnkSendEmail.Attributes.Add("href", url.ToString());

                }
                else
                {
                    lnkSendEmail.Attributes.Add("href", "MailTo:" + overviewdetail.CandidateEmail + ">" + overviewdetail.CandidateEmail);
                }
                if (AccessArray.Contains(407))
                {
                    SecureUrl url = UrlHelper.BuildSecureUrl("../ATS/InternalNotesAndActivities.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, "407", UrlConstants.PARAM_MEMBER_ID, overviewdetail.Id.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentparentid);
                    lnkAddNote.Attributes.Add("href", url.ToString());
                }
                else lnkAddNote.Visible = false;
                if (AccessArray.Contains(403))
                {
                    SecureUrl url = UrlHelper.BuildSecureUrl("../ATS/InternalInterviewSchedule.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, "403", UrlConstants.PARAM_MEMBER_ID, overviewdetail.Id.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentparentid, UrlConstants.PARAM_MSG, "Interview Schedule");
                    lnkScheduleInterview.Attributes.Add("href", url.ToString());
                    SecureUrl url1 = UrlHelper.BuildSecureUrl("../ATS/InternalInterviewSchedule.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, "403", UrlConstants.PARAM_MEMBER_ID, overviewdetail.Id.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentparentid, UrlConstants.PARAM_MSG, "Interview Feedback");
                    lnkInterviewFeedback.Attributes.Add("href", url1.ToString());
                }
                else lnkScheduleInterview.Visible = false;
                if (AccessArray.Contains(620))
                {

                    SecureUrl url = UrlHelper.BuildSecureUrl("../ATS/AssignedManager.aspx", string.Empty, UrlConstants.PARAM_SITEMAP_ID, "620", UrlConstants.PARAM_MEMBER_ID, overviewdetail.Id.ToString(), UrlConstants.PARAM_SITEMAP_PARENT_ID, currentparentid);
                    linkPrimaryManager.Attributes.Add("href", url.ToString());
                }
                else linkPrimaryManager.Enabled = false;



                MemberManager manager = Facade.GetMemberManagerByMemberIdAndManagerId(_memberId, CurrentMember.Id);
                if (manager != null || IsUserAdmin)
                {
                    txtRemarksR.Text = MiscUtil.RemoveScript(overviewdetail.Remarks.Trim(), string.Empty);
                    txtRemarksR.ReadOnly = false;
                }
                else
                {
                   lblRemark.Text = "&nbsp;" + overviewdetail.Remarks.Trim();
                   lblRemark.Visible = true;
                   txtRemarksR.Visible = false;
                }

                lblCurrentCTC.Text = overviewdetail.CurrentCTC;
                lblExpectedCTC.Text = overviewdetail.ExpectedCTC;
                lblNoticePeriod.Text = overviewdetail.NoticePeriod;
                // ********Code added by Sumit Sonawane on 20/Jan/2017********
                try
                {
                    string ageInYears = Facade.GetCandidateAgebodByMemberId(_memberId);
                    //lblAgefromDOB.Text = ageInYears + " " + "Years";
                    lblAgefromDOB.Text = ageInYears ;
                }
                catch { lblAgefromDOB.Text = ""; }
                // *******************************END*************************
                txtRecentNotes.Text = overviewdetail.Notes; //added by pravin khot on 13/July/2016
                if (overviewdetail.Notes != "")
                {
                    lbldetailsnotes.Text = overviewdetail.CreateDate.ToShortDateString() + " " + overviewdetail.CreateDate.ToShortTimeString() + " || " + overviewdetail.MemberName + " || " + overviewdetail.Categary;
                }
            }

        }

        private string HighLightText(string OriginalText, string Compare)
        {
            if (Compare != "" && OriginalText != "")
            {
                if (OriginalText.ToLower().Contains(Compare.ToLower()))
                {
                    OriginalText = OriginalText.ToLower().Replace(Compare.ToLower(), "<span class=highlight>" + Compare + "</span>");
                    return OriginalText;

                }
            }
            return OriginalText;
        }
        private string HighLightExactText(string OriginalText, string Compare)
        {
            if (Compare.ToLower() == OriginalText.ToLower() && OriginalText.Trim() != "")
            {
                OriginalText = "<span class=highlight>" + OriginalText + "</span>";
                return OriginalText;
            }
            return OriginalText;
        }

        private string HighLightSkillName(string text)
        {
            char[] delim = { ',' };
            string[] oriArr = "".Split(',') ;
            string[] curArr = text.Split(',');
            string ret = string.Empty;
            return ret;
        }

        private void LoadCandidateBasicData(Member currentCandidate)
        {
            getJobPostingId();
            JobPosting jobposting =null;
            if(JobId >0) jobposting=Facade.GetJobPostingById(JobId);
            imgInterRating.ImageUrl = MiscUtil.GetInternalRatingImage("0"); 
            string strFullName = MiscUtil.GetFullName(currentCandidate.FirstName, currentCandidate.MiddleName, currentCandidate.LastName);
            lblCandidateName.Text = strFullName;
            MemberEducation memberEducation = Facade.GetHighestEducationByMemberId(currentCandidate.Id);
            int educationId = 0;
            try
            {
                if (memberEducation.LevelOfEducationLookupId != null)
                {
                     educationId = memberEducation.LevelOfEducationLookupId;
                    lblHighestDegree.Text = Facade.GetLevelofEducationByLookupId(educationId);
                }
            }
            catch (Exception a) {}

            if (educationId > 0)
            {
                if (jobposting != null && jobposting.RequiredDegreeLookupId != "" && ("," + jobposting.RequiredDegreeLookupId + ",").Contains(educationId .ToString ()))
                    lblHighestDegree.Text = "<span class=highlight>" + lblHighestDegree.Text + "</span>";
            }
            lblMobile.Text = currentCandidate.CellPhone;
            lblPrimaryEmail.Text = currentCandidate.PrimaryEmail;
            if (currentCandidate.ResumeSource != 0)
            {
                string resumesour=MiscUtil.GetResumeSourceByMemberId(Facade, currentCandidate.CreatorId, (int)currentCandidate.ResumeSource);
                if (resumesour.ToLower().Trim().StartsWith("self"))
                {
                    string companyName = Facade.GetCompanyNameByVendorMemberId(CurrentMember.Id);
                    if (companyName != string.Empty)
                    {
                        lblResumeSource.Text = companyName + " - " + CurrentMember .FirstName  + " " + CurrentMember .LastName+" (Vendor)" ;
                    }
                    //lblResumeSource .Text =
                }
                else if (resumesour.ToLower().Trim().StartsWith("referral"))
                {
                    EmployeeReferral reff=Facade .EmployeeReferral_GetByMemberId (currentCandidate .Id );
                    if (reff != null)
                    {
                        lblResumeSource.Text = "Referred by " + reff.RefererFirstName + " " + reff.RefererLastName + " " + reff.CreateDate.ToShortDateString();
                    }
                }
                else
                    lblResumeSource.Text = resumesour;
            }
                       
            DateTime dtLastUpdate = currentCandidate.UpdateDate;
            string strDateLastUpdated = dtLastUpdate.ToLongDateString();
            lblLastUpdated.Text = strDateLastUpdated.Substring(strDateLastUpdated.IndexOf(",") + 1);
            lblCandidateId.Text = "A"+currentCandidate.Id.ToString();
            MemberExtendedInformation currentCandidateExInfo = Facade.GetMemberExtendedInformationByMemberId(currentCandidate.Id);
            if (currentCandidateExInfo != null)
            {
                if (currentCandidateExInfo.TotalExperienceYears != String.Empty && currentCandidateExInfo.TotalExperienceYears != "0")
                {
                   lblExperience.Text =  currentCandidateExInfo.TotalExperienceYears;
                }
                else
                {
                    lblExperience.Text = String.Empty;
                }
                double  exp = 0;
                double.TryParse(currentCandidateExInfo.TotalExperienceYears, out exp);
                if (jobposting != null && Convert.ToDouble(jobposting.MinExpRequired.Trim() == "" ? "0" : jobposting.MinExpRequired.Trim()) <= exp && Convert.ToDouble(jobposting.MaxExpRequired.Trim() == "" ? "0" : jobposting.MaxExpRequired) >= exp)
                    lblExperience.Text = "<span class=highlight>" + lblExperience.Text + "</span>";


                if (currentCandidateExInfo.InternalRatingLookupId != 0)
                {
                    GenericLookup rating = Facade.GetGenericLookupById(currentCandidateExInfo.InternalRatingLookupId);
                    if (rating != null && !string.IsNullOrEmpty(rating.Name))
                    {
                        imgInterRating.ImageUrl = MiscUtil.GetInternalRatingImage(rating.Name);
                    }
                }
                else
                {
                    imgInterRating.ImageUrl = MiscUtil.GetInternalRatingImage("0");
                }
                lblJobTitle.Text = HighLightText(currentCandidateExInfo.CurrentPosition, jobposting ==null?"": jobposting .JobTitle); ;
                lblCompany.Text = currentCandidateExInfo.LastEmployer;
                lblAvailibility.Text = MiscUtil.GetMemberAvailability(currentCandidateExInfo.Availability, currentCandidateExInfo.AvailableDate, Facade);

                MemberManager manager = Facade.GetMemberManagerByMemberIdAndManagerId(_memberId, CurrentMember.Id);
                if (manager != null || IsUserAdmin)
                {
                    txtRemarksR.Text = MiscUtil.RemoveScript(currentCandidateExInfo.Remarks.Trim(), string.Empty);
                    txtRemarksR.ReadOnly = false;
                }
                else
                {
                    lblRemark.Text = "&nbsp;" + currentCandidateExInfo.Remarks.Trim();
                   lblRemark.Visible = true;
                   txtRemarksR.Visible = false;
                }
                //if (currentCandidateExInfo.WorkAuthorizationLookupId > 0)
                //{
                    //GenericLookup workPermit = Facade.GetGenericLookupById(currentCandidateExInfo.WorkAuthorizationLookupId);
                    //if (workPermit != null && !string.IsNullOrEmpty(workPermit.Name))
                  //  {
                        //lblWrokPermit.Text = workPermit.Name;
                   // }
                //}

                //if (jobposting != null && jobposting.AuthorizationTypeLookupId != "" && ("," + jobposting.AuthorizationTypeLookupId + ",").Contains("," + currentCandidateExInfo.WorkAuthorizationLookupId + ","))
                    //lblWrokPermit.Text = "<span class=highlight>" + lblWrokPermit.Text + "</span>";
                //string[] payCycle = { "Hour", "Day", "Month", "Year" };
                ////lblCurrentSalary.Text = "";
                //lblExpectedSalary.Text = "";
                //    GenericLookup genericlookup = Facade.GetGenericLookupById(currentCandidateExInfo.CurrentYearlyCurrencyLookupId);
                //    if (currentCandidateExInfo.CurrentYearlyRate != null && currentCandidateExInfo.CurrentYearlyRate != 0 && currentCandidateExInfo.CurrentSalaryPayCycle != 0) lblCurrentSalary.Text = (genericlookup != null ? genericlookup.Name + " " : "") + currentCandidateExInfo.CurrentYearlyRate.ToString() +(genericlookup !=null ? (genericlookup .Name =="INR" ? " Lacs":""):"")+ " / " + payCycle[currentCandidateExInfo.CurrentSalaryPayCycle - 1];
                //    else if (currentCandidateExInfo.CurrentYearlyRate != null && currentCandidateExInfo.CurrentYearlyRate != 0) lblCurrentSalary.Text = (genericlookup != null ? genericlookup.Name + " " : "") + currentCandidateExInfo.CurrentYearlyRate.ToString()+(genericlookup !=null ? (genericlookup .Name =="INR" ? " Lacs":""):"");

                //    genericlookup = Facade.GetGenericLookupById(currentCandidateExInfo.ExpectedYearlyCurrencyLookupId );
                //    if (currentCandidateExInfo.ExpectedYearlyRate != null && currentCandidateExInfo.ExpectedYearlyRate != 0 && currentCandidateExInfo.ExpectedYearlyMaxRate != null && currentCandidateExInfo.ExpectedYearlyMaxRate != 0 && currentCandidateExInfo.ExpectedSalaryPayCycle != 0) lblExpectedSalary.Text = (genericlookup != null ? genericlookup.Name + " " : "") + currentCandidateExInfo.ExpectedYearlyRate.ToString() + " - " + currentCandidateExInfo.ExpectedYearlyMaxRate.ToString() + (genericlookup != null ? (genericlookup.Name == "INR" ? " Lacs" : "") : "") + " / " + payCycle[currentCandidateExInfo.ExpectedSalaryPayCycle - 1];
                //    else if (currentCandidateExInfo.ExpectedYearlyRate != null && currentCandidateExInfo.ExpectedYearlyRate != 0 && currentCandidateExInfo.ExpectedYearlyMaxRate != null && currentCandidateExInfo.ExpectedYearlyMaxRate != 0) lblExpectedSalary.Text = (genericlookup != null ? genericlookup.Name + " " : "") + currentCandidateExInfo.ExpectedYearlyRate.ToString() + " - " + currentCandidateExInfo.ExpectedYearlyMaxRate.ToString()+(genericlookup !=null ? (genericlookup .Name =="INR" ? " Lacs":""):"");
                //    if (jobposting != null)
                //    {
                //        if (jobposting.PayRate != "Open" && jobposting.PayRate.Trim () != string.Empty &&  Convert.ToDouble(jobposting.PayRate) > 0)
                //        {
                //            if (Convert.ToDecimal(jobposting.PayRate) >= currentCandidateExInfo.ExpectedYearlyRate && Convert.ToDecimal(jobposting.PayRate) <= currentCandidateExInfo.ExpectedYearlyMaxRate)
                //            {
                //                lblExpectedSalary.Text = "<span class=highlight>" + lblExpectedSalary.Text + "</span>";
                //            }
                //        }
                //    }
            }
    
            MemberDetail currentCandidateDetail = Facade.GetMemberDetailByMemberId(currentCandidate.Id);
            if (currentCandidateDetail != null)
            {
                string stateName = MiscUtil.GetStateNameById(currentCandidate.PermanentStateId, Facade);
                if (stateName != "" && ( jobposting ==null ?-1:jobposting.StateId )== currentCandidate.PermanentStateId)
                        stateName = "<span class=highlight>" + stateName + "</span>";
                lblLocation.Text = MiscUtil.GetLocation(currentCandidate.PermanentCity.Trim() == "" ? "" : (HighLightExactText(currentCandidate.PermanentCity ,jobposting==null?"": jobposting .City)), stateName, currentCandidate.PermanentZip);
                //lblofficePhone.Text = ":" + currentCandidateDetail.OfficePhone;
            }

            MemberManager Manager = Facade.GetMemberManagerByMemberIdAndManagerId(_memberId, CurrentMember.Id);
            int CurrentMemberLogged = Convert.ToInt32(CurrentMember.Id);  

            
            if (Manager != null || IsUserAdmin || IsUserVendor  )
            {
                    linkAvailability.Visible = true;
                    SecureUrl urlavailability = UrlHelper.BuildSecureUrl("../" + UrlConstants.Candidate.CANDIDATE_INTERNALPRIVACYSHARING, string.Empty, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(_memberId));
                    linkAvailability.Attributes.Add("onclick", "EditModal('" + urlavailability.ToString() + "','500px','400px'); return false;");
                
                //ControlHelper.SetHyperLink(linkAvailability, UrlConstants.Candidate.CANDIDATE_INTERNALPRIVACYSHARING, string.Empty, "Availability", UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(_memberId));
            }
            else
            {
                linkAvailability.Enabled = false;
                linkAvailability.Text = "Availability";
            }


            SecureUrl newurl = UrlHelper.BuildSecureUrl("../"+UrlConstants.Candidate.CANDIDATE_INTERNALRATINGS ,string .Empty , UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(_memberId), UrlConstants.PARAM_OVERVIEW_INTERNALRATING, "Internal Rating");
            linkInternalRating.Attributes.Add("onclick", "EditModal('" + newurl.ToString() + "','500px','300px'); return false;");
            
            // ControlHelper.SetHyperLink(linkInternalRating, UrlConstants.Candidate.CANDIDATE_INTERNALRATINGS , string.Empty, "Internal Rating ", UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(_memberId), UrlConstants.PARAM_OVERVIEW_INTERNALRATING, "Internal Rating");     //9692       
        }
        private void LoadLink()
        {


            SecureUrl newurl = UrlHelper.BuildSecureUrl("../" + UrlConstants.Candidate.CANDIDATE_INTERNALRATINGS, string.Empty, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(_memberId), UrlConstants.PARAM_OVERVIEW_INTERNALRATING, "Internal Rating");
            linkInternalRating.Attributes.Add("onclick", "EditModal('" + newurl.ToString() + "','500px','300px'); return false;");


            MemberManager Manager = Facade.GetMemberManagerByMemberIdAndManagerId(_memberId, CurrentMember.Id);
            int CurrentMemberLogged = Convert.ToInt32(CurrentMember.Id);

           
            if (Manager != null || IsUserAdmin || IsUserVendor )
            {
                linkAvailability.Visible = true;
                SecureUrl urlavailability = UrlHelper.BuildSecureUrl("../" + UrlConstants.Candidate.CANDIDATE_INTERNALPRIVACYSHARING, string.Empty, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(_memberId));
                linkAvailability.Attributes.Add("onclick", "EditModal('" + urlavailability.ToString() + "','500px','350px'); return false;");
            }
            else
            {
                linkAvailability.Enabled = false;
                linkAvailability.Text = "Availability";
            }

            linksourcehistory.Attributes.Add("onclick","EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/MemberSourceHistory.aspx?StatusId=" + 0 + "&JID=" +
                _memberId + "&Canid=" + StringHelper.Convert(_memberId) + "','700px','500px'); return false;");
            
        }
        private void LoadHotList()
        {
            int rolemem = 0;
            //if (RoleMember == "Candidate")
                rolemem = (int)MemberGroupType.Candidate;
            int count = Facade.GetMemberGroupCountByManagerIDAndGroupType(base.CurrentMember.Id, rolemem );

            if (count < 200)
            {
                TxtHotList.Visible = false;
                ddlHotlist.Visible = true;
                MiscUtil.PopulateMemberGroupByMemberId(ddlHotlist, Facade, base.CurrentMember.Id, rolemem );
            }
            else
            {
                ddlHotlist.Visible = false;
                TxtHotList.Visible = true;
            }
        }
        private void Disablecontrols()
        {

            ocstatus.Visible = false;
        }

      

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (isMainApplication)
            //{
            //    //Disablecontrols();
            //    hdnApplicationType.Value = "";
            //}

            if (ApplicationSource == ApplicationSource.MainApplication) 
            {
                hdnApplicationType.Value = "";
            }
            uclComReq.ISMyList = true;
            ddlHotlist.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            GetMemberId();
            getJobPostingId();
            CustomSiteMap CustomMap = new CustomSiteMap();
            CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(358, CurrentMember.Id);
            if (CustomMap != null)
            {
                IdForSitemap = CustomMap.Id;
                UrlForCandidate = "~/" + CustomMap.Url.ToString();
            }
            hot_ListAutoComplete.ContextKey = base.CurrentMember.Id.ToString();
            string strMessage = String.Empty; 
            bool boolError = false; 
            if (!IsPostBack)
            {
                if (IsUserVendor)
                {
                    divInternalRating.Visible = divRemarks.Visible = divActions.Visible = false;
                }

                populateCandidateOverviewdetails();
                LoadLink();
                LoadHotList();

                //lblJobs.Text = "# of Jobs :";
                //lblRemarkstext.Text = "Remarks : ";
                //GetMemberId();
               // RenderCurrentCandidate();
                //if (Session["Message"] != null)
                //{
                //    if (Convert.ToInt32(Session["Message"]) == 1)
                //    {
                //        strMessage = "Successfully added candidate to requisition.";
                //        MiscUtil.ShowMessage(lblMessage, strMessage, boolError);
                //        Session.Remove("Message");
                //    }
                //}

                 // Country country = Facade.GetCountryById(Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString()));
                //  if (country != null)
                  {
                    //  if (country.Name != "United States")
                      {
                          //divWorkPermit.Visible = false;
                      }
                  }
            }

            //*********Code added by pravin khot on 15/March/2016**********When hiring manager login then action button visible false
            string role = Facade.GetCustomRoleNameByMemberId(base.CurrentMember.Id);
            if (role.ToLower() == "hiring manager")
            {
                //lnkSendEmail.Visible = false;
                //lnkInterviewFeedback.Visible = false;
                //divAddRequisition.Visible = false;
                //divAddHotList.Visible = false;
                divActions.Visible = false;
            }
           
            //***********END********************
           
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            if (!IsRefreshed)
            {
               // if (txtRemarksR.Text != string.Empty)
                {
                    MemberExtendedInformation memberExtInfo = Facade.GetMemberExtendedInformationByMemberId(_memberId);
                    memberExtInfo.Remarks =txtRemarksR .Text = MiscUtil .RemoveScript ( txtRemarksR.Text.Trim());
                    txtRemarksR.Text = MiscUtil.RemoveScript(txtRemarksR.Text.Trim(),string .Empty );
                    Facade.UpdateMemberExtendedInformation(memberExtInfo);
                    MiscUtil.ShowMessage(lblMessage, "Successfully updated Remarks.", false);
                    
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            MemberExtendedInformation memberExtInfo = Facade.GetMemberExtendedInformationByMemberId(_memberId);
            txtRemarksR.Text = "";
            txtRemarksR.Text=memberExtInfo.Remarks;
        }

        protected void btnAddToPSList_Click(object sender, EventArgs e)
        {
            int requisitionId = uclComReq.SelectedRequisitionId;
            
            if (requisitionId > 0 && _memberId !=0)
            {
                     string strMessage = String.Empty;
                     bool boolError = false;
                     int intCounterAdded = 0;
                     //**************code added by pravin khot on 28/April/2016  use for setting- AllowCandidatetobeaddedonMultipleRequisition**************
                     #region
                     Hashtable siteSettingTable = null;

                     TPS360.BusinessFacade.Facade f = new TPS360.BusinessFacade.Facade();
                     MemberExtendedInformation MailSetting = f.GetMemberExtendedInformationByMemberId(CurrentMember.Id);

                     TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
                     SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                     siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);

                     string chkstatus;
                     string RequisitionName;
                     chkstatus = siteSettingTable[DefaultSiteSetting.AllowCandidatetobeaddedonMultipleRequisition.ToString()].ToString();
                    
                     if (chkstatus == "False")
                     {
                         RequisitionName = Facade.CandidateAvailableInRequisition_Name(_memberId);

                         if (RequisitionName == "" || RequisitionName == null)
                         {
                             MemberJobCart memberJobCart = Facade.GetMemberJobCartByMemberIdAndJobPostringId(_memberId, requisitionId);
                             if (memberJobCart != null)
                             {
                                 MemberJobCartDetail memberJobCartDetailList = Facade.GetMemberJobCartDetailCurrentByMemberJobCartId(memberJobCart.Id);
                                 if (memberJobCartDetailList == null)
                                 {
                                     intCounterAdded++;
                                 }
                             }
                             else
                             {
                                 intCounterAdded++;
                             }

                             if (intCounterAdded > 0)
                             {
                                 try
                                 {
                                     //MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateAddedToRequisition, requisitionId, _memberId.ToString (), CurrentMember.Id, Facade);
                                     Facade.AddCandidateRequisitionStatus(requisitionId, _memberId.ToString(), base.CurrentMember.Id);
                                 }
                                 catch
                                 {
                                 }
                                 HiringMatrixLevels hiringMatrixLevel = Facade.HiringMatrixLevel_GetInitialLevel();
                                 int initialLevel = 0;
                                 if (hiringMatrixLevel != null) initialLevel = hiringMatrixLevel.Id;
                                 Facade.MemberJobCart_AddCandidateToRequisition(base.CurrentMember.Id, Convert.ToString(_memberId), requisitionId);
                                 //MiscUtil.MoveApplicantToHiringLevel(requisitionId, _memberId, base.CurrentMember.Id, initialLevel, Facade);

                                 if (RequisitionAddedForCandidate != null)
                                 {
                                     ASP.controls_memberjobcarts_ascx jobcart = (ASP.controls_memberjobcarts_ascx)this.Page.Master.FindControl("cphCandidateMaster").FindControl("uclCandidateOverView").FindControl("uclJobCart");
                                     if (jobcart != null)
                                     {
                                         jobcart.DataBind();

                                     }
                                 }
                                 MiscUtil.ShowMessage(lblMessage, "Successfully added candidate to requisition.", false);
                             }
                             else
                             {
                                 boolError = true;
                                 strMessage = "Candidate are already added to a Requisition and in the Hiring Process."; 
                                 MiscUtil.ShowMessage(lblMessage, strMessage, true);
                             }
                         }
                         else
                         {
                             boolError = true;
                             strMessage = "Candidate already added in the '" + RequisitionName + "' and in the Hiring Process.";
                             MiscUtil.ShowMessage(lblMessage, strMessage, true);
                         }
                     }
                     #endregion
                     //****************END- 28/April/2016 ********************************************
                     else
                     {
                         MemberJobCart memberJobCart = Facade.GetMemberJobCartByMemberIdAndJobPostringId(_memberId, requisitionId);
                         if (memberJobCart != null)
                         {
                             MemberJobCartDetail memberJobCartDetailList = Facade.GetMemberJobCartDetailCurrentByMemberJobCartId(memberJobCart.Id);
                             if (memberJobCartDetailList == null)
                             {
                                 intCounterAdded++;
                             }
                         }
                         else
                         {
                             intCounterAdded++;
                         }

                         if (intCounterAdded > 0)
                         {
                             try
                             {
                                 //MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateAddedToRequisition, requisitionId, _memberId.ToString (), CurrentMember.Id, Facade);
                                 Facade.AddCandidateRequisitionStatus(requisitionId, _memberId.ToString(), base.CurrentMember.Id);
                             }
                             catch
                             {
                             }
                             HiringMatrixLevels hiringMatrixLevel = Facade.HiringMatrixLevel_GetInitialLevel();
                             int initialLevel = 0;
                             if (hiringMatrixLevel != null) initialLevel = hiringMatrixLevel.Id;
                             Facade.MemberJobCart_AddCandidateToRequisition(base.CurrentMember.Id, Convert.ToString(_memberId), requisitionId);
                             //MiscUtil.MoveApplicantToHiringLevel(requisitionId, _memberId, base.CurrentMember.Id, initialLevel, Facade);

                             if (RequisitionAddedForCandidate != null)
                             {
                                 ASP.controls_memberjobcarts_ascx jobcart = (ASP.controls_memberjobcarts_ascx)this.Page.Master.FindControl("cphCandidateMaster").FindControl("uclCandidateOverView").FindControl("uclJobCart");
                                 if (jobcart != null)
                                 {
                                     jobcart.DataBind();

                                 }
                             }
                             MiscUtil.ShowMessage(lblMessage, "Successfully added candidate to requisition.", false);
                         }
                         else
                         {
                             boolError = true;
                             strMessage = "Candidate(s) selected from the list are already added to a Requisition and in the Hiring Process.";
                             MiscUtil.ShowMessage(lblMessage, strMessage, true);
                         }
                     }

                     uclComReq.SelectedRequisitionId = 0;
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select a requisition", true);
                uclComReq.SelectedRequisitionId = uclComReq.SelectedRequisitionId;
            }
           // ddlRequisition.Items.Clear();

            
        }
        protected void btnAddToHotList_Click(object sender, EventArgs e)
        {
            try
            {
                int hotlistId = 0;
                if (ddlHotlist.Visible == true)
                    hotlistId = Int32.Parse(ddlHotlist.SelectedValue);
                else
                {
                    if (TxtHotList.Text != "")
                    {
                        MemberGroup memGroup = Facade.GetMemberGroupByName(TxtHotList.Text);
                        hotlistId = memGroup == null ? 0 : memGroup.Id;
                    }
                }
                if (hotlistId > 0)
                {
                    Button btnSender = (Button)sender;
                    if (btnSender.ID == "btnAddToHotList")
                    {
                        if (MiscUtil.AddToHotList(hotlistId, _memberId, base.CurrentMember.Id, Facade))
                            MiscUtil.ShowMessage(lblMessage, "Successfully added candidate to Hot List", false);
                        else
                            MiscUtil.ShowMessage(lblMessage, "Candidate is already in Hot List.", true);
                    }
                    else
                    {
                        MiscUtil.RemoveFromHotList(hotlistId, _memberId, Facade);
                        MiscUtil.ShowMessage(lblMessage, "Successfully removed candidate from Hot List", false);
                    }
                    if (ddlHotlist.Visible == true)
                        ddlHotlist.SelectedIndex = 0;
                    else
                        TxtHotList.Text = "";
                }
                else
                    MiscUtil.ShowMessage(lblMessage, "Please select a Hot List", true);

            }
            catch (Exception)
            {
                MiscUtil.ShowMessage(lblMessage, "Please select a Hot List", true);
            }
        }
        #endregion
    }
}