﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
namespace TPS360.Web.UI
{
    public partial class Controls_VendorPortalAccess : CompanyBaseControl
    {
        #region Member Variables
        private static int _companyId = 0;
        public static int _memberId = 0;
        public static string _role = string.Empty;
        private static string Status = string.Empty;
        private string Message = string.Empty;
        private bool _mailsetting = false;
        private static int _memberEmailId = 0;
        public string MailBody;
        private static int _frommemberId = 0;
        public static int _tomemberId = 0;
        MemberEmail _memberEmail;

        #endregion

        #region Properties


        #endregion

        #region Methods

        private void CommonLoad()
        {
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ROLE_TYPE]))
            {
                _role = Helper.Url.SecureUrl[UrlConstants.PARAM_ROLE_TYPE];
            }
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]))
            {
                _companyId = Int32.Parse(Helper.Url.SecureUrl[UrlConstants.PARAM_COMPANY_ID]);
            }
            try
            {
                Status = Helper.Session.Get(SessionConstants.Status).ToString();
                Helper.Session.Set(SessionConstants.Status, string.Empty);
            }
            catch
            {
            }
        }
        public string Role
        {
            get
            {
                return _role;
            }
            set
            {
                _role = value;
            }
        }
        protected void ddlAccess_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            CommonLoad();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CommonLoad();
            if (!IsPostBack)
            {
                switch (CurrentCompanyStatus)
                {
                    case CompanyStatus.Department:
                        lblTitle.Text = "Manage BU Contact Access";
                        Page.Title = base.CurrentCompany.CompanyName + " - " + "BU Contact Access";
                        lblNote.Text = "<b>Note:</b> Only contacts with an email address present can be granted access.";
                        break;

                    case CompanyStatus.Vendor:
                        lblTitle.Text = "Manage Vendor Portal Access";
                        Page.Title = base.CurrentCompany.CompanyName + " - " + "Vendor Portal Access";
                        lblNote.Text = "<b>Note:</b> Only contacts with an email address present can be granted access to the vendor portal.";
                        break;

                }
                if (Status == "Success")
                {
                    MiscUtil.ShowMessage(lblMessage, "Successfully updated password.", false);
                    //ModalPopupExtender.Enabled = false;
                }
                BindList();
                // btnUpdate.Visible = lsvVendorPortalAccess.Items.Count > 0;
                txtSortColumn.Text = "btnName";
                txtSortOrder.Text = "ASC";
                PlaceUpDownArrow();
            }
            string pagesize1 = "";
            pagesize1 = (Request.Cookies["VendorPortalAccessRowPerPage"] == null ? "" : Request.Cookies["VendorPortalAccessRowPerPage"].Value);
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvVendorPortalAccess.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize1 == "" ? "20" : pagesize1);
            }
            _mailsetting = MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id);
        }
        protected void lsvVendorPortalAccess_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvVendorPortalAccess.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "VendorPortalAccessRowPerPage";
            }
            PlaceUpDownArrow();
        }
        protected void lsvVendorPortalAccess_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                CompanyContact Contact = ((ListViewDataItem)e.Item).DataItem as CompanyContact;
                Label lblContactName = (Label)e.Item.FindControl("lblContactName");
                Label lblEmail = (Label)e.Item.FindControl("lblEmail");
                DropDownList ddlAccess = (DropDownList)e.Item.FindControl("ddlAccess");
                LinkButton lnkChangePassword = (LinkButton)e.Item.FindControl("lnkChangePassword");
                LinkButton lnksendmail = (LinkButton)e.Item.FindControl("lnkSendAccessEmail");
                HtmlGenericControl ulOptions = (HtmlGenericControl)e.Item.FindControl("ulOptions");

                lblEmail.Text = Contact.Email.ToString();
                lblContactName.Text = Contact.FirstName.ToString() + " " + Contact.LastName.ToString();

                if (Contact.PortalAccess == true)
                {
                    //ddlAccess.Enabled = false; // code commented by pravin khot on 22/July/2016
                    ddlAccess.SelectedIndex = 1;
                    ulOptions.Style.Add("display", "");
                }
                ddlAccess.Attributes.Add("onchange", "javascript:EmployeeAccessChanged('" + ddlAccess.ClientID + "','" + Contact.MemberId + "','" + base.CurrentMember.Id + "','" + lblMessage.ClientID + "');$('#" + ddlAccess.ClientID + "').parents('td').next().children('ul').css({'display':($('#" + ddlAccess.ClientID + "').val()==1?'':'none')});");
                lnkChangePassword.CommandArgument = Contact.Email.ToString();
                lnksendmail.CommandArgument = Contact.Email.ToString();
            }
        }

        protected void lsvVendorPortalAccess_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                    if (SortColumn.Text == string.Empty || SortColumn.Text != e.CommandArgument.ToString())
                        SortOrder.Text = "asc";
                    else SortOrder.Text = SortOrder.Text == "asc" ? "desc" : "asc";
                    SortColumn.Text = e.CommandArgument.ToString();
                    odsContactList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
                }
            }
            catch
            {
            }
            string email = string.Empty;
            GetMemberId();
            email = e.CommandArgument.ToString();
            if (email != string.Empty)
            {
                if (string.Equals(e.CommandName, "Password"))
                {
                    Helper.Session.Set(SessionConstants.VendorEmail, email.ToString());
                    ModalPopupExtender.Enabled = true;
                    uclPassword.Focus();
                    ModalPopupExtender.Show();
                    Helper.Session.Set(SessionConstants.PreviousPath, Page.Request.Url.AbsoluteUri.ToString());

                }
                else if (string.Equals(e.CommandName, "AccessEmail"))
                {
                    if (_mailsetting)
                    {
                        SentEmail(email);
                        lsvVendorPortalAccess.DataBind();
                    }
                    else
                    {
                        //dv.Visible = true;
                        //uclConfirm.AddMessage("Mail Server SMTP details have not been entered. Would you like to enter them now?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
                        MiscUtil.ShowMessage(lblMessage, "Failed to send access email. Please configure your email account under Account Settings->Mail Setup", false);
                    }
                }
            }
        }
        public void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                hdfToMemberId.Value = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];
                _tomemberId = Convert.ToInt32(hdfToMemberId.Value);
            }
            //From Member Login
            hdfFromMemberId.Value = base.CurrentMember.Id.ToString();
            _frommemberId = base.CurrentMember.Id;
        }
        private void SentEmail(string toEmail)
        {
            string password = String.Empty;
            string strEmailText = String.Empty;
            string strHTMLText = String.Empty;
            string subject = String.Empty;

            subject = "New Password For Talentrackr Access";


            string sentStatus = string.Empty;
            //EmailHelper emailManager = new EmailHelper();
            //emailManager.To.Clear();
            //emailManager.From = CurrentMember.PrimaryEmail;
            //emailManager.To.Add(toEmail);

            //emailManager.Subject = subject;
            EmailHelper emailHelper = new EmailHelper();
            Member toMember = Facade.GetMemberByMemberEmail(toEmail);
            if (toMember != null)
            {
                strHTMLText = emailHelper.PrepareViewforTps360Access(toMember);
            }

            strEmailText = strHTMLText;
            System.Web.Security.MembershipUser user = System.Web.Security.Membership.GetUser(toEmail);
            try
            {
                password = user.GetPassword();
            }
            catch (Exception ex)
            {
            }

            strEmailText = strHTMLText.Replace("**********", password);
            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Vendor.LOGIN, string.Empty);
            // strEmailText = strEmailText.Replace("Login.aspx", "Vendor/Login.aspx");

            //MailQueueData.AddMailToMailQueue(CurrentMember.Id, toEmail, subject, strEmailText, "", "", null, Facade);
            switch (CurrentCompanyStatus)
            {
                case CompanyStatus.Vendor:
                    lblMessage.Text = "Successfully sent email to vendor.";
                    strEmailText = strEmailText.Replace("Login.aspx", "Vendor/Login.aspx");
                    break;

                case CompanyStatus.Department:
                    lblMessage.Text = "Successfully sent email to BU.";
                    strEmailText = strEmailText.Replace("Login.aspx", "Login.aspx");
                    break;
            }

            MailQueueData.AddMailToMailQueue(CurrentMember.Id, toEmail, subject, strEmailText, "", "", null, Facade);
            MiscUtil.ShowMessage(lblMessage, lblMessage.Text, false);

            //emailManager.Body = strEmailText;
            //sentStatus = emailManager.Send(base.CurrentMember.Id);
            //if (sentStatus == "1")
            //{
            //    //save mail
            //    SaveMemberEmail(toEmail, subject, strEmailText);
            //    sentStatus = "Sent";
            //    lblMessage.Text = "Successfully sent email to vendor.";
            //    try
            //    {
            //        MiscUtil.ShowMessage(lblMessage, lblMessage.Text, false);
            //    }
            //    catch
            //    {
            //    }
            //}
            //else
            //{
            //    sentStatus = "Not Sent";
            //    if (_memberEmailId > 0)
            //    {
            //        Facade.DeleteMemberEmailAttachmentByMemberEmailId(_memberEmailId);
            //        Facade.DeleteMemberEmailDetailByMemberEmailId(_memberEmailId);
            //        Facade.DeleteMemberEmailById(_memberEmailId);
            //    }
            //    lblMessage.Text = "email has not been sent, please check for valid email id.";
            //    MiscUtil.ShowMessage(lblMessage, lblMessage.Text, true); //0.12
            //}

        }
        private void SaveMemberEmailDetail(MemberEmail memberEmail)
        {
            MemberEmailDetail memberEmailDetail = new MemberEmailDetail();
            memberEmailDetail.MemberId = _frommemberId;
            memberEmailDetail.SendingTypeId = (int)EmailSendingType.Broadcast;
            memberEmailDetail.MemberEmailId = memberEmail.Id;
            memberEmailDetail.CreatorId = base.CurrentMember.Id;
            memberEmailDetail.AddressTypeId = (int)EmailAddressType.To;
        }
        private void SaveMemberEmail(String tomail, String subject, String mailbody)
        {
            if (true) // Isvalid
            {
                try
                {
                    MemberEmail memberEmail = BuildMemberEmail(tomail, subject, mailbody);
                    memberEmail = Facade.AddMemberEmail(memberEmail);
                    SaveMemberEmailDetail(memberEmail);
                    memberEmail = Facade.UpdateMemberEmail(memberEmail);

                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }

            }
        }
        private MemberEmail BuildMemberEmail(String tomail, String sub, String mailbody)
        {
            MemberEmail memberEmail = CurrentMemberEmail;
            memberEmail.Subject = sub;
            memberEmail.SenderId = _frommemberId;
            memberEmail.SenderEmail = CurrentMember.PrimaryEmail;
            memberEmail.ReceiverEmail = tomail;
            memberEmail.EmailBody = mailbody;
            memberEmail.ReceiverId = _tomemberId;
            string to = string.Empty;

            if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Reply") == 0 && !memberEmail.IsNew)
            {
                memberEmail.EmailTypeLookupId = (int)EmailType.Replied;
                memberEmail.ParentId = memberEmail.Id;
            }
            else if (String.Compare(Helper.Url.SecureUrl[UrlConstants.PARAM_FLAG_ID], "Forward") == 0 && !memberEmail.IsNew)
            {
                memberEmail.EmailTypeLookupId = (int)EmailType.Forwarded;
                memberEmail.ParentId = memberEmail.Id;
            }
            else
            {
                memberEmail.EmailTypeLookupId = (int)EmailType.Sent;
                memberEmail.ParentId = 0;
            }
            memberEmail.Status = 0;
            memberEmail.IsRemoved = false;
            memberEmail.CreatorId = base.CurrentMember.Id;
            memberEmail.UpdatorId = base.CurrentMember.Id;
            memberEmail.SentDate = DateTime.Now.ToString();
            return memberEmail;
        }
        public EmailFromType EmailFromType
        {
            get
            {
                return (EmailFromType)(ViewState[this.ClientID + "_EmailFromType"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_EmailFromType"] = value;
            }
        }
        private MemberEmail CurrentMemberEmail
        {
            get
            {
                if (_memberEmail == null)
                {
                    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
                    {
                        _memberEmailId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);

                        _memberEmail = Facade.GetMemberEmailById(_memberEmailId);
                    }

                    if (_memberEmail == null)
                    {
                        _memberEmail = new MemberEmail();
                    }
                }

                return _memberEmail;
            }
        }
        private void BindList()
        {
            odsContactList.SelectParameters["CompanyId"].DefaultValue = _companyId.ToString();
            if (SortOrder.Text == string.Empty && SortColumn.Text == string.Empty)
            {
                SortOrder.Text = "asc";
                SortColumn.Text = "FirstName";
            }
            odsContactList.SelectParameters["SortOrder"].DefaultValue = SortOrder.Text.ToString();
            lsvVendorPortalAccess.DataSourceID = "odsContactList";
            this.lsvVendorPortalAccess.DataBind();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            bool IsMessage = false;
            foreach (ListViewDataItem item in lsvVendorPortalAccess.Items)
            {
                Label lblContactName = (Label)item.FindControl("lblContactName");
                Label lblEmail = (Label)item.FindControl("lblEmail");
                DropDownList ddlAccess = (DropDownList)item.FindControl("ddlAccess");
                CommonLoad();
                string email = lblEmail.Text.Trim();
                string name = lblContactName.Text;
                Member mem = new Member();

                if (ddlAccess.SelectedIndex == 1)
                {
                    MembershipUser user = Membership.GetUser(lblEmail.Text.Trim());

                    if (user != null)
                    {
                        IsMessage = true;
                        user.IsApproved = true;
                        Membership.UpdateUser(user);
                        CompanyContact con = Facade.GetCompanyContactByEmail(lblEmail.Text.Trim());
                        con.PortalAccess = true;
                        Facade.UpdateCompanyContact(con);
                        MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(con.MemberId);
                        mem = Facade.GetMemberById(con.MemberId);
                        if (mem != null)
                        {
                            mem.IsRemoved = false;
                            Facade.UpdateMember(mem);
                        }
                        if (map != null)
                        {
                            map.IsRemoved = false;
                            Facade.UpdateMemberCustomRoleMap(map);
                        }
                        Message = "Successfully updated Vendor Portal access. The default password is vendoraccess.";
                    }
                }
                else
                {
                    MembershipUser user = Membership.GetUser(lblEmail.Text.Trim());

                    if (user != null)
                    {
                        if (_role != string.Empty)
                        {
                            user.IsApproved = false;
                            Membership.UpdateUser(user);
                            CompanyContact con = Facade.GetCompanyContactByEmail(lblEmail.Text.Trim());
                            con.PortalAccess = false;
                            Facade.UpdateCompanyContact(con);
                            MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(con.MemberId);
                            mem = Facade.GetMemberById(con.MemberId);
                            if (mem != null)
                            {
                                mem.IsRemoved = true;
                                Facade.UpdateMember(mem);
                            }
                            if (map != null)
                            {
                                map.IsRemoved = true;
                                Facade.UpdateMemberCustomRoleMap(map);
                            }
                            if (Message == string.Empty)
                                Message = "Successfully updated Vendor Portal access.";
                        }
                    }
                    else
                        if (Message == string.Empty)
                            Message = "Successfully updated Vendor Portal access.";

                }
            }
            if (Message != string.Empty)
                MiscUtil.ShowMessage(lblMessage, Message.ToString(), false);
            Message = string.Empty;
            lsvVendorPortalAccess.DataSourceID = "odsContactList";
            lsvVendorPortalAccess.DataBind();
        }
        private void PlaceUpDownArrow()
        {
            try
            {

                LinkButton lnk = (LinkButton)lsvVendorPortalAccess.FindControl(txtSortColumn.Text);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                im.Attributes.Add("class", (txtSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }

        #endregion
    }
}