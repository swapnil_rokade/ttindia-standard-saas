﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewerAssessmentForm.cs
    Description         :   This page is used to fill up Interviewer AssessmentForm.
    Created By          :   Prasanth
    Created On          :   16/Oct/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
   0.1              27/Dec/2015         Prasanth Kumar G        Introduced RowNumber
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Text;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
namespace TPS360.Web.UI
{
    public partial class ControlsInterviewerAssessmentForm : BaseControl
    {

        private static int _memberId = 0;
        //private bool _IsAccessForOverview = true;
        private string UrlForAccess = string.Empty;
        //private int IdForSitemap = 0;
        //private bool IsNew = false;
        private static string _InterviewerEmail;
        private static int _interviewId;
        public int MemberId
        {
            set { _memberId = value; }
        }

        public string InterviewerEmail
        {
            set { _InterviewerEmail = value; }
        }

        public int InterviewId
        {
            set { _interviewId = value; }
        }




        //public string InterviewerEmail
        //{
        //    get
        //    {
        //        string _InterviewerEmail = "";
        //        string[] words = URL_Parameter.Split('~');

        //        for (int i = 0; i < words.Length; i++)
        //        {
        //            if (i > 0)
        //            {
        //                _InterviewerEmail = words[i];
        //            }

        //        }


        //        return _InterviewerEmail;

        //    }
        //}


        //public int InterviewId
        //{
        //    get
        //    {
        //        int _interviewId = 0;

        //        string[] words = URL_Parameter.Split('~');

        //        for (int i = 0; i < words.Length; i++)
        //        {
        //            if (i == 0)
        //            {
        //                _interviewId = int.Parse(words[i]);
        //            }

        //        }


        //        return _interviewId;


        //    }
        //}

        //public string URL_Parameter
        //{
        //    get
        //    {
        //        string _URLParameter = "";
        //        if (!TPS360.Common.Helper.StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWID]))
        //        {
        //            return Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWID];
        //        }
        //        else
        //            return _URLParameter;

        //    }
        //}


        public string InterviewTitle
        {
            get
            {
                string _interviewTitle = "";
                _interviewTitle = Helper.Url.SecureUrl[UrlConstants.PARAM_INTERVIEWTITLE].ToString();
                return _interviewTitle;
            }
        }

#region Methods
        public string ValidateListview()    
        {
            string valstr = "";
            
            foreach (ListViewItem item in lsvQuestion.Items)
            {

                //string a = item.SubItems[1].Text;
                //string a = item.Controls[1].ToString();
                
                Label lsvlblId = (Label)item.FindControl("lblId");
                Label lsvlblAnswertype = (Label)item.FindControl("lblAnswertype");
                

                DropDownList lsvDdlAnswer = (DropDownList)item.FindControl("DdlAnswer");
                TextBox lsvTxtAnswer = (TextBox)item.FindControl("TxtAnswer");
                TextBox lsvTxtRationale = (TextBox)item.FindControl("TxtRationale");
                Label lblRowNo = (Label)item.FindControl("lblRowNo");//Line introduced by Prasanth on 27/Dec/2015
                if (lsvlblAnswertype.Text == "Text")
                {
                    if (lsvTxtAnswer.Text == "")
                    {

                        
                        valstr = "Please enter Response For Row : " + lblRowNo.Text; //Line Modified by Prasanth on 27/Dec/2015
                         return valstr;
                    }
                }
                else
                {
                    if (lsvDdlAnswer.SelectedIndex == 0)
                    {
                         
                        valstr = " Please enter Response For Row : " + lblRowNo.Text;//Line Modified by Prasanth on 27/Dec/2015
                         return valstr;
                    }
 
                }


                if (lsvTxtRationale.Text == "")
                {
               
                    valstr = " Please enter Rationale For Row : " + lblRowNo.Text;//Line Modified by Prasanth on 27/Dec/2015
                     return valstr;
                }

                
            }
            return valstr;

        }
#endregion



        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
           if (!IsPostBack)
            {
                
                PopulateInterviewAssessment();

            
               //Code modified by Prasanth on 21/Dec/2015 Start
                if (DdlAssessment.Items.Count > 0)
                {
                    BindList(Convert.ToInt32(DdlAssessment.SelectedValue));

                }
                else
                {
                    BindList(0);
                }

               //************************END**********************
                //PrepareView();
                //popup.Visible = true;
            }


        }

 
        protected void DdlAssessment_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string A = ((System.Web.UI.WebControls.ListControl)(sender)).SelectedValue;
            //BindList(Convert.ToInt32(DdlAssessment.SelectedValue));
            BindList(Convert.ToInt32(((System.Web.UI.WebControls.ListControl)(sender)).SelectedValue));
            //TxtAnswer.Text = ddlAnswer.SelectedItem.ToString();
        }
        //*****end*****
      
        #endregion 

        #region Methods

        

        public void UpdateResponse()
        {

            foreach (ListViewItem item in lsvQuestion.Items)
            {

                
                string Answer = "";
                Label lsvlblId = (Label)item.FindControl("lblId");
                Label lsvlblAnswertype = (Label)item.FindControl("lblAnswertype");
                DropDownList lsvDdlAnswer = (DropDownList)item.FindControl("DdlAnswer");
                TextBox lsvTxtAnswer = (TextBox)item.FindControl("TxtAnswer");
                TextBox lsvTxtRationale = (TextBox)item.FindControl("TxtRationale");

                if (lsvlblAnswertype.Text != "Text")
                {
                    Answer = lsvDdlAnswer.SelectedValue;
                }
                else
                {
                    Answer = lsvTxtAnswer.Text;

                }

                InterviewResponse InterviewResponse = BuildInterviewResponse(lsvlblId.Text, Answer, lsvTxtRationale.Text);
                Facade.InterviewResponse_Update(InterviewResponse);

            }
            //Line commented by Prasanth on 27/Dec/2015
            //MiscUtil.ShowMessage(lblMessage, "Assessment Saved Successfully.", false);
        }


        public InterviewResponse BuildInterviewResponse(string Id, string Response, string Rationale)
        {
            InterviewResponse InterviewResponse = new InterviewResponse();

            InterviewResponse.InterviewResponse_Id = Convert.ToInt32(Id);
            
            InterviewResponse.Response = Response;
            InterviewResponse.Rationale = Rationale;
            
            return InterviewResponse;
        }

        private void PrepareAnswerView(DropDownList DdlAnswer, string Answertype)
        {

            
            DdlAnswer.Items.Add(new ListItem("Select Answer", "0"));
            if (Answertype == "Y/N")
            {
                 
                DdlAnswer.Items.Add(new ListItem("Y", "1"));
                DdlAnswer.Items.Add(new ListItem("N", "2"));
                
            }
            
            else if (Answertype == "Rating")
            {
                
                DdlAnswer.Items.Add(new ListItem("1", "1"));
                DdlAnswer.Items.Add(new ListItem("2", "2"));
                DdlAnswer.Items.Add(new ListItem("3", "3"));
                DdlAnswer.Items.Add(new ListItem("4", "4"));
                DdlAnswer.Items.Add(new ListItem("5", "5"));
                 
            }
            else if (Answertype == "Y/N/NA")
            {
                
                DdlAnswer.Items.Add(new ListItem("Y", "1"));
                DdlAnswer.Items.Add(new ListItem("N", "2"));
                DdlAnswer.Items.Add(new ListItem("NA", "3"));
                
            }
            
           
            

        }
       


        public void BindList(int AssessmentId)
        {

            odsInterviewResponseList.SelectParameters["InterviewerEmail"].DefaultValue = _InterviewerEmail;// "gpkprashanth83@gmail.com";
            odsInterviewResponseList.SelectParameters["InterviewId"].DefaultValue = _interviewId.ToString();//"128";
            odsInterviewResponseList.SelectParameters["AssessmentId"].DefaultValue = AssessmentId.ToString();


             
            string pagesize = "";
             
            pagesize = (Request.Cookies["InterviewerResponseRowPerPage"] == null ? "" : Request.Cookies["InterviewerResponseRowPerPage"].Value);  
            lsvQuestion.DataSourceID = "odsInterviewResponseList";
            lsvQuestion.DataBind();

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvQuestion.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "10" : pagesize);
            }

        }

        
        public void PopulateInterviewAssessment()
        {

            DdlAssessment.DataSource = Facade.InterviewAssessment_GetBy_InterviewId_InterviewerEmail(_interviewId, _InterviewerEmail);
            DdlAssessment.DataTextField = "Name";
            DdlAssessment.DataValueField = "Id";
            DdlAssessment.DataBind();
        }



        

        #endregion
        #region Listview Events

       

        protected void lsvQuestion_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                InterviewResponse Response = ((ListViewDataItem)e.Item).DataItem as InterviewResponse;

                if (Response != null)
                {
                    Label lblId = (Label)e.Item.FindControl("lblId");
                    Label lblAnswerType = (Label)e.Item.FindControl("lblAnswertype");
                     
                    TextBox TxtQuestion = (TextBox)e.Item.FindControl("TxtQuestion");
                    
                   
                    


                    TextBox TxtAnswer = (TextBox)e.Item.FindControl("TxtAnswer");
                    DropDownList DdlAnswer = (DropDownList)e.Item.FindControl("DdlAnswer");

                     
                    TextBox TxtRationale = (TextBox)e.Item.FindControl("TxtRationale");
                    HyperLink lnkAnswer = (HyperLink)e.Item.FindControl("lnkAnswer");
                    Label lblRowNo = (Label)e.Item.FindControl("lblRowNo");//Line introduced by Prasanth on 27/Dec/2015


                     
                    lblId.Text = Response.InterviewResponse_Id.ToString();
                    TxtQuestion.Text = Response.Question;
                    
                    TxtRationale.Text = Response.Rationale;

                    lblAnswerType.Text = Response.AnswerType;

                    DdlAnswer.Visible = false;
                    TxtAnswer.Visible = false;


                    if (Response.AnswerType != "Text")
                    {
                        DdlAnswer.Visible = true;
                        PrepareAnswerView(DdlAnswer, Response.AnswerType);
                        if (Response.Response != "")
                        {
                            DdlAnswer.SelectedValue = Response.Response;
                        }
                    }
                    else
                    {
                        TxtAnswer.Visible = true;
                        if (Response.Response != "")
                        {
                            TxtAnswer.Text = Response.Response;
                        }
                    }

                    lblRowNo.Text = Response.RowNo.ToString(); //Line introduced by Prasanth on 27/Dec/2015

                 }

            }
        }
  
        protected void lsvQuestion_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (txtSortColumn.Text == lnkbutton.ID)
                    {
                        if (txtSortOrder.Text == "ASC") txtSortOrder.Text = "DESC";
                        else txtSortOrder.Text = "ASC";
                    }
                    else
                    {
                        txtSortColumn.Text = lnkbutton.ID;
                        txtSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            {
            }
        }

        protected void lsvQuestion_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvQuestion.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null)
                {
                    if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "InterviewerResponseRowPerPage";
                }
            }
          
        }

        #endregion
        
}
}


