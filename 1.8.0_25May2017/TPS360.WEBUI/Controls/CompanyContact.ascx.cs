/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CompanyContact.ascx.cs
    Description: This is the user control page used for company contact information
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sep-15-2008           Yogeesh Bhat        Defect ID: 8700; changes made in SaveCompanyContact()method;
                                                             (changed CurrentMember.Id to companyContact.CreatorId 
                                                              and checked for companyContact.CompanyId for zero)
    0.2            Jan-27-2009            Kalyani P          Defect ID: 8823;Added code to clear the division dropdown list
    0.3            Feb-10-2009           N.Srilakshmi        DefectId:8809; Modified a line in PrepareView
    0.4           March-2-2009            Nagarathna             DefectId:10011 
                                                                   In BindList() method added datasource as null.
 *  0.5            Apr-27-2009           Rajendra Prasad        Defect id: 10397;Added new method PopulateSiteSettings() to display
 *                                                              ddlcountry selected value according to sitesettings.
    0.6             May-13-2009            Veda                 Defect id: 10459; To disable the "Direct Number" field                                            
 *  0.7             May-14-2009            Veda                 Defect id: 10458; "Is Primary" should be checked by default when the user is adding a new contact 
    0.8             May-21-2009          Rajendra Prasad        Defect id: 8809;Resume hyperlink should be opened in a new window and stored in server with same file name.
    0.9             May-25-2009          Rajendra Prasad        Defect id: 8809;Server error if resume hyperlink name is having symbols in it.
 *  1.0             Jun-15-2009          Veda                   Defect id: 10650;Runtime error occurs in the Company overview page.  
 *  1.1             Jun-17-2009          Veda                   Defect id: 10640;When the user is adding a contact for a company, no matter what the status of the "Is Primary" checkbox, the contact is not saved as primary.   
    1.2             Jun-29-2009          Veda                   Defect id: 10793;By default if "Is Primary" checkbox is checked and disabled then it should not get unchecked when the user clicks on the 'Clear' button. 
 *  1.3             Aug-04-2009          Veda                   Defect id: 10901; Validating the email adress entered.
    1.4             Aug-17-2009          Gopala Swamy           Defect id: 11291; Call function called "PopulateSiteSettings(ddlCountry)" in btnSave_Click() event                                     
 *  1.5             Oct-14-2009         Ranjit Kumar.I          DefectID:11604  ; modified code in lsvCompanyContact_ItemCommand for the defect 
 *  1.6             Nov-03-2009         Rajendra Prasad         Defect id :8809 ; Encoded url file name...
 *  1.7              25/Feb/2016         pravin khot            added ddlEmail_Click,ChkExisting_Click,EnableforBucontact
 *                                                               Modify - function --PrepareView() ,clear()
 *  1.8              26/Feb/2016         pravin khot            Replace whole two functions- 1.SaveCompanyContact ,2.BuildCompanyContact 
 *                                                              Function rename 1.SaveCompanyContact_Old ,2.BuildCompanyContact_Old
 *  1.9              2/June/2016         pravin khot            added on page load-divChkExistingUser.Visible = false   
 *  2.0              23/June/2016        pravin khot            added -  if (companyContact.MemberId == 0)
 *  2.1              29/June/2016        pravin khot            added- if (_role.ToLower().ToString() == "companycontact" && _memberid == 0)
 *  2.2              14/March/2017       pravin khot            added- string companycontactByJobPostingHiring = Facade.GetCompanyContactByJobPostingHiringTeam(id);

 * -------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Web.Security;
using System.Net;
using System.Net.Sockets;
using System.Web.Mail;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;

namespace TPS360.Web.UI
{
    public partial class cltCompanyContact : CompanyBaseControl
    {
        #region Member Variable

        CompanyContact _companyContact;
        private bool IsMailSetting = false;
        private int _memberid = 0;
        private bool IsManager = false;
        #endregion

        #region Properties        
        
        public int CompanyContactId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_CompanyContactId"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_CompanyContactId"] = value;
            }
        }
        private string role;
        public static string _role
        {
            get;
            set;
            
        }
        public string AccountType
        {
            get
            {
                
                switch (CurrentCompanyStatus)
                {
                    case CompanyStatus.Client:
                       return  "Company contact";
                    case CompanyStatus.Department:
                         return  "BU contact";
                    case CompanyStatus.Vendor:
                        return  "Vendor contact";
                    default :
                        return "";
                }
                
            }

        }
        private CompanyContact CurrentCompanyContact
        {
            get
            {
                if (_companyContact == null)
                {
                    if (CompanyContactId > 0)
                    {
                        _companyContact = Facade.GetCompanyContactById(CompanyContactId);
                    }

                    if (_companyContact == null)
                    {
                        _companyContact = new CompanyContact();
                    }
                }

                return _companyContact;
            }
        }

        #endregion

        #region Methods

        public void BindList()
        {
            IList<CompanyContact> companyContactList = Facade.GetAllCompanyContactByComapnyId(base.CurrentCompanyId);
           
            if (companyContactList != null && companyContactList.Count > 0)
            {               
                lsvCompanyContact.DataSource = companyContactList;
                lsvCompanyContact.DataBind();             
            }
            else
            {
                chkIsPrimary.Enabled = false;
                chkIsPrimary.Checked = true;
                lsvCompanyContact.DataSource = null;
                lsvCompanyContact.DataBind();
            }
        }

        private void DeleteView()
        {
            CompanyContact companyContact = CurrentCompanyContact;           
            IList<CompanyContact> companyContactList = Facade.GetAllCompanyContactByComapnyId(base.CurrentCompanyId);

            if (companyContactList == null)
            {
                companyContact.IsPrimaryContact = true;
                chkIsPrimary.Checked = companyContact.IsPrimaryContact;
                chkIsPrimary.Enabled = false;
            }
            
            if (companyContactList != null && companyContactList.Count > 0)
            {
                for (int i = 0; i < companyContactList.Count; i++)
                {
                    if (companyContactList[i].IsPrimaryContact)
                    {
                        companyContact.IsPrimaryContact = false;
                        chkIsPrimary.Checked = companyContact.IsPrimaryContact;
                        chkIsPrimary.Enabled = true;
                        return;
                    }
                    else
                    {
                        companyContact.IsPrimaryContact = true;
                        chkIsPrimary.Checked = companyContact.IsPrimaryContact;
                        chkIsPrimary.Enabled = false;
                    }
                }
            }            
        }
        private void PrepareView()
        {            
            CompanyContact companyContact = CurrentCompanyContact;
            IList<CompanyContact> companyContactList = Facade.GetAllCompanyContactByComapnyId(base.CurrentCompanyId);
            MiscUtil.PopulateMemberListWithEmailByRoleIfExisting(ddlemail, ContextConstants.ROLE_EMPLOYEE, Facade); //line code added by pravin khot on 25/Feb/2016
            if (companyContactList == null)
            {
                companyContact.IsPrimaryContact = true;
                chkIsPrimary.Checked = companyContact.IsPrimaryContact;
            }
            if (!companyContact.IsPrimaryContact)
            {
                chkIsPrimary.Enabled = true; 
                if (!IsPostBack)
                {
                    if (companyContactList != null && companyContactList.Count > 0)
                    {
                        for (int i = 0; i < companyContactList.Count; i++)
                        {
                            if (companyContactList[i].IsPrimaryContact)
                            {
                                companyContact.IsPrimaryContact = false;
                                chkIsPrimary.Checked = companyContact.IsPrimaryContact;
                                chkIsPrimary.Enabled = true; 
                                return;
                            }
                            else
                            {
                                companyContact.IsPrimaryContact = true;
                                chkIsPrimary.Checked = companyContact.IsPrimaryContact;
                                chkIsPrimary.Enabled = false; 
                            }
                        }
                    }
                }
            }
            else
            {
                chkIsPrimary.Enabled = false; 
            }

            if (companyContactList == null) 
            {
                chkIsPrimary.Enabled = false;
            }
            
            if (!companyContact.IsNew)
            {               
                txtFirstName.Text =MiscUtil .RemoveScript ( companyContact.FirstName,string .Empty );
                txtLastName.Text =MiscUtil .RemoveScript ( companyContact.LastName,string.Empty );
                txtTitle.Text = MiscUtil .RemoveScript (companyContact.Title,string .Empty );
                txtEmail.Text = MiscUtil .RemoveScript (companyContact.Email,string .Empty );
                txtAddress1.Text = MiscUtil .RemoveScript (companyContact.Address1,string .Empty );
                txtAddress2.Text = MiscUtil .RemoveScript (companyContact.Address2,string .Empty );
                uclCountryState.SelectedCountryId = companyContact.CountryId;
                uclCountryState.SelectedStateId = companyContact.StateId;
                txtCity.Text = MiscUtil .RemoveScript (companyContact.City,string .Empty );
                txtZip.Text = MiscUtil .RemoveScript (companyContact.ZipCode,string .Empty );
                txtOfficePhone.Text = MiscUtil .RemoveScript (companyContact.OfficePhone,string .Empty );
                txtOfficePhoneExtension.Text = MiscUtil .RemoveScript (companyContact.OfficePhoneExtension,string .Empty );
                txtMobilePhone.Text = MiscUtil .RemoveScript (companyContact.MobilePhone,string .Empty );
                txtFax.Text = MiscUtil .RemoveScript (companyContact.Fax,string .Empty );
                chkIsPrimary.Checked = companyContact.IsPrimaryContact; //0.7  
                //chkNoBulkEmail.Checked = companyContact.NoBulkEmail;
                txtContactRemarks.Text = MiscUtil .RemoveScript (companyContact.ContactRemarks,string .Empty );

                //******Code added by pravin khot on 25/Feb/2016***************
                try
                {
                    Member memb = Facade.GetMemberByMemberEmail(txtEmail.Text);
                    ddlemail.SelectedValue = memb.Id.ToString();
                    //ControlHelper.SelectListByValue(ddlemail, StringHelper.Convert(companyContact.Email));

                }
                catch { }
                //********************End*******************

                if (companyContact.IsOwner)
                {
                    chkIsOwner.Checked = true;
                    txtOwnership.Text = MiscUtil.RemoveScript(StringHelper.Convert(companyContact.OwnershipPercentage), string.Empty);
                    ControlHelper.SelectListByValue(ddlEthnicBackgroud, StringHelper.Convert(companyContact.EthnicGroupLookupId));
                    divOwner.Style.Remove("Hidden");
                }
            }

            if (IsExternalRegistration)
            {
                pnlContactRemarks.Visible = false;
            }
            
        }
  
        private void RegisterUser()
        {
            string strMessage = String.Empty;
            bool IsCompany = false;
            SecureUrl url;
            string SecureURL;

            if (IsValid)
            {
                if (_role != "")
                {
                    try
                    {
                        MembershipUser newUser;

                        newUser = Membership.CreateUser(txtEmail.Text, "vendoraccess", txtEmail.Text.Trim());
                        if (newUser.IsApproved == true)
                        {
                            if (!Roles.RoleExists(_role))
                            {
                                Roles.CreateRole(_role);
                                CustomRole ro = Facade.GetCustomRoleByName(_role);
                                if (ro == null)
                                {
                                    CustomRole rol = new CustomRole();
                                    rol.Name = _role;
                                    rol.CreatorId = base.CurrentMember.Id;
                                    Facade.AddCustomRole(rol);
                                }
                            }
                            try
                            {
                                Roles.AddUserToRole(newUser.UserName, _role);
                               
                            }
                            catch
                            {
                            }
                            Member newMember = new Member();
                            newMember.UserId = (Guid)newUser.ProviderUserKey;
                            newMember.ResumeSource = Convert.ToInt32(ResumeSource.SelfRegistration);
                            newMember.PermanentCountryId = 0;
                            newMember.PermanentStateId = 0;
                            newMember.IsRemoved = false  ;
                            newMember.AutomatedEmailStatus = true;
                            newMember.FirstName =MiscUtil .RemoveScript ( txtFirstName.Text.Trim());
                            newMember.LastName = MiscUtil .RemoveScript (txtLastName.Text.Trim());
                            newMember.DateOfBirth = DateTime.MinValue;
                            newMember.PrimaryEmail = newUser.Email;
                            newMember.Status = (int)MemberStatus.Active;
                            if (base.CurrentUserRole == ContextConstants.ROLE_EMPLOYEE)
                            {
                                newMember.ResumeSource = (int)ResumeSource.Employee;
                            }
                            else if (base.CurrentUserRole == ContextConstants.ROLE_CLIENT)
                            {
                                IsCompany = true;
                                newMember.ResumeSource = (int)ResumeSource.Client;
                            }
                            else if (base.CurrentUserRole == ContextConstants.ROLE_VENDOR)
                            {
                                IsCompany = true;
                                newMember.ResumeSource = (int)ResumeSource.Supplier;
                            }
                            else if (base.CurrentUserRole == ContextConstants.ROLE_ADMIN)
                            {
                                newMember.ResumeSource = (int)ResumeSource.Admin;
                            }
                            newMember.CreatorId = base.CurrentMember.Id;
                            newMember.CreateDate = DateTime.Now;
                            newMember.UpdatorId = base.CurrentMember.Id;
                            newMember.UpdateDate = DateTime.Now;
                            newMember = Facade.AddMember(newMember);
                            _memberid = newMember.Id;
                            Resource newResource = new Resource();
                            newResource.EmailAddress = newMember.PrimaryEmail;
                            newResource.ResourceName = newMember.Id.ToString();
                            Facade.AddResource(newResource);
                            //If not a candidate registratiion
                            if (_role.ToLower() != ContextConstants.ROLE_CANDIDATE.ToLower())
                            {
                                MemberDetail newMemberDetail = new MemberDetail();
                                newMemberDetail.MemberId = newMember.Id;
                                newMemberDetail.CurrentStateId = 0;
                                newMemberDetail.CurrentCountryId = 0;
                                newMemberDetail.GenderLookupId = 0;
                                newMemberDetail.EthnicGroupLookupId = 0;
                                newMemberDetail.MaritalStatusLookupId = 0;
                                newMemberDetail.NumberOfChildren = 0;
                                newMemberDetail.BloodGroupLookupId = 0;
                                newMemberDetail.AnniversaryDate = DateTime.MinValue;

                                Facade.AddMemberDetail(newMemberDetail);

                                //--Trace
                                MemberExtendedInformation memberExtended = new MemberExtendedInformation();
                                memberExtended.MemberId = newMember.Id;
                                memberExtended.Availability = ContextConstants.MEMBER_DEFAULT_AVAILABILITY;
                                Facade.AddMemberExtendedInformation(memberExtended);

                            }
                            //  Trace Tech
                            MemberObjectiveAndSummary m_memberObjectiveAndSummary = new MemberObjectiveAndSummary();
                            m_memberObjectiveAndSummary.CreatorId = base.CurrentMember.Id;
                            m_memberObjectiveAndSummary.UpdatorId = base.CurrentMember.Id;
                            m_memberObjectiveAndSummary.CopyPasteResume = "";
                            m_memberObjectiveAndSummary.CreateDate = DateTime.Today;
                            m_memberObjectiveAndSummary.UpdateDate = DateTime.Today;
                            m_memberObjectiveAndSummary.RawCopyPasteResume = "";// StripTagsCharArray(m_memberObjectiveAndSummary.CopyPasteResume);
                            m_memberObjectiveAndSummary.MemberId = newMember.Id;
                            Facade.AddMemberObjectiveAndSummary(m_memberObjectiveAndSummary);


                            //Add to self as manager                            
                            if (base.CurrentUserRole == ContextConstants.ROLE_EMPLOYEE || base.CurrentUserRole == ContextConstants.ROLE_ADMIN) //0.3
                            {
                                MemberManager memberManager = new MemberManager();
                                memberManager.IsPrimaryManager = true;
                                memberManager.ManagerId = base.CurrentMember.Id;
                                memberManager.MemberId = newMember.Id;
                                memberManager.CreateDate = DateTime.Now;
                                memberManager.CreatorId = base.CurrentMember.Id;
                                memberManager.UpdateDate = DateTime.Now;
                                memberManager.UpdatorId = base.CurrentMember.Id;
                                Facade.AddMemberManager(memberManager);
                            }

                            MiscUtil.AddActivity(_role, _memberid , base.CurrentMember.Id, ActivityType.AddMember, Facade);
                            string countryName = string.Empty;
                            if (SiteSetting != null)
                            {
                                Country coun = Facade.GetCountryById(Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString()));
                                if (coun != null)
                                    countryName = coun.Name;
                            }
                            MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(newMember .Id );
                            CustomRole role = Facade.GetCustomRoleByName(_role.ToString());
                            int roleid = 0;
                            if (role != null)
                                roleid = role.Id;


                            
                            if (map == null)
                            {
                                map = new MemberCustomRoleMap();
                                map.CustomRoleId = roleid;
                                map.MemberId = newMember.Id;
                                map.CreatorId = base.CurrentMember.Id;
                                
                                Facade.AddMemberCustomRoleMap(map);
                                AddDefaultMenuAccess(newMember.Id, roleid);
                            }
                            else
                            {
                                if (map.CustomRoleId == roleid)
                                {
                                    map = new MemberCustomRoleMap();
                                    map.CustomRoleId = roleid;
                                    map.UpdatorId = base.CurrentMember.Id;
                                    Facade.UpdateMemberCustomRoleMap(map);
                                    AddDefaultMenuAccess(newMember.Id, roleid);
                                }
                            }
                        }
                        newUser.IsApproved = false;
                        Membership.UpdateUser(newUser);
                    }
                    catch (MembershipCreateUserException ex)
                    {
                       
                    }

                }
                else
                {
                    MiscUtil.ShowMessage(lblMessage, "No role specified.", true);
                }
            }
        }
        private void AddDefaultMenuAccess(int memId, int role)
        {

            ArrayList previlegeList = Facade.GetAllCustomRolePrivilegeIdsByRoleId(role);

            if (previlegeList != null && previlegeList.Count > 0)
            {
                Facade.DeleteMemberPrivilegeByMemberId(memId);

                for (int i = 0; i <= previlegeList.Count - 1; i++)
                {
                    MemberPrivilege previlege = new MemberPrivilege();

                    previlege.CustomSiteMapId = Convert.ToInt32(previlegeList[i]);
                    previlege.MemberId = memId;
                    Facade.AddMemberPrivilege(previlege);
                }
            }
        }
        private CompanyContact BuildContact()
        {
            CompanyContact companyContact = CurrentCompanyContact;
            companyContact.FirstName = txtFirstName.Text = MiscUtil .RemoveScript (txtFirstName.Text.Trim());
            companyContact.LastName = txtLastName.Text = MiscUtil .RemoveScript (txtLastName.Text.Trim());
            companyContact.Title = txtTitle.Text = MiscUtil .RemoveScript (txtTitle.Text.Trim());
            companyContact.Email = txtEmail.Text = MiscUtil .RemoveScript (txtEmail.Text.Trim());
            companyContact.Address1 = txtAddress1.Text = MiscUtil .RemoveScript (txtAddress1.Text.Trim());
            companyContact.Address2 = txtAddress2.Text = MiscUtil .RemoveScript (txtAddress2.Text.Trim());
            companyContact.CountryId = uclCountryState.SelectedCountryId;
            companyContact.StateId = uclCountryState.SelectedStateId;
            companyContact.City = txtCity.Text = MiscUtil .RemoveScript (txtCity.Text.Trim());
            companyContact.ZipCode = txtZip.Text = MiscUtil .RemoveScript (txtZip.Text.Trim());
            companyContact.OfficePhone = txtOfficePhone.Text = MiscUtil .RemoveScript (txtOfficePhone.Text.Trim());
            companyContact.OfficePhoneExtension = txtOfficePhoneExtension.Text = MiscUtil .RemoveScript (txtOfficePhoneExtension.Text.Trim());
            companyContact.MobilePhone = txtMobilePhone.Text = MiscUtil .RemoveScript (txtMobilePhone.Text.Trim());
            companyContact.Fax = txtFax.Text = MiscUtil .RemoveScript (txtFax.Text.Trim());
            companyContact.IsPrimaryContact = chkIsPrimary.Checked;
            //companyContact.NoBulkEmail = chkNoBulkEmail.Checked;
            companyContact.ContactRemarks = txtContactRemarks.Text = MiscUtil .RemoveScript (txtContactRemarks.Text);

            if (CurrentMember != null)     
            {
                companyContact.CreatorId = base.CurrentMember.Id;
                companyContact.UpdatorId = base.CurrentMember.Id;
            }

            if (chkIsOwner.Checked)
            {
                companyContact.IsOwner = true;

                decimal ownershipPercentage = 0m;

                decimal.TryParse(MiscUtil .RemoveScript (txtOwnership.Text.Trim()), out ownershipPercentage);
                companyContact.OwnershipPercentage = ownershipPercentage;
                if (ddlEthnicBackgroud.SelectedItem != null) {

                    companyContact.EthnicGroupLookupId = Convert.ToInt32(ddlEthnicBackgroud.SelectedItem.Value);
                }
                
            }

            return companyContact;
        }
        #region OLD CODE
        private CompanyContact BuildCompanyContact_Old()  // Function rename by pravin khot on 26/Feb/2016  old name- (BuildCompanyContact) 
        {
            CompanyContact contact = new CompanyContact();
            MembershipUser user = Membership.GetUser(txtEmail.Text.Trim());

            if (user == null && this .CompanyContactId ==0)
            {
                if (_role != string.Empty)
                {
                    if (_memberid == 0)
                        RegisterUser();
                    else
                    {

                    }
                   contact = BuildContact();
                   return contact;
                }
            }
            else
            {
                Member memb = Facade.GetMemberByMemberEmail(txtEmail.Text);
                if (memb != null)
                {

                    memb.FirstName = txtFirstName.Text;
                    memb.LastName = txtLastName.Text;
                    Facade.UpdateMember(memb);
                    _memberid = memb.Id;
                }
                /*Commented out for allow dublicate*/
                contact = Facade.GetCompanyContactByEmail(txtEmail.Text.Trim());
                if (contact != null)
                {
                    contact = BuildContact();

                    return contact;
                }
                else
                {
                    if (this.CompanyContactId > 0)
                    {
                        if (CurrentCompanyContact.MemberId > 0)
                        {
                            Member member = new Member();
                            //member = Facade.GetMemberByMemberEmail(CurrentCompanyContact.Email);
                            member = Facade.GetMemberById(CurrentCompanyContact.MemberId);
                            member.PrimaryEmail = txtEmail.Text;
                            member.FirstName = txtFirstName.Text;
                            member.LastName = txtLastName.Text;
                            member = Facade.UpdateMember(member);
                            Facade.UpdateAspNet_User(txtEmail.Text.Trim(), member.UserId);

                        }
                        else
                        {
                            RegisterUser();
                        }
                            contact = BuildContact();
                            return contact;
                    }
                    else
                        return null;
                }
            }

            return null;
        }
        #endregion

        //New code added/Modify by pravin khot on 26/Feb/2016***********START****
        #region New CODE
        private CompanyContact BuildCompanyContact()
        {
            Company currentcompany = base.CurrentCompany;
            CompanyContact contact = new CompanyContact();
            if (_role.ToLower().ToString() == "vendor" || (_role.ToLower().ToString() == "department contact" && !ChkExistingUser.Checked))
            {
                MembershipUser user = Membership.GetUser(txtEmail.Text.Trim());
                if (user == null && this.CompanyContactId == 0)
                {
                    if (_role != string.Empty)
                    {
                        if (_memberid == 0)
                            RegisterUser();
                        else
                        {

                        }
                        contact = BuildContact();
                        return contact;
                    }
                }
                else if (user != null && this.CompanyContactId == 0)
                {
                    Member member = Facade.GetMemberByMemberEmail(txtEmail.Text);
                    if (member != null)
                    {
                        contact = BuildContact();
                        _memberid = member.Id;
                    }

                    return contact;
                }
                else
                {
                    Member memb = Facade.GetMemberByMemberEmail(txtEmail.Text);
                    contact = Facade.GetCompanyContactByEmail(txtEmail.Text.Trim());
                    if (memb != null)
                    {
                        memb.FirstName = txtFirstName.Text;
                        memb.LastName = txtLastName.Text;
                        Facade.UpdateMember(memb);
                    }

                    if (contact != null)
                    {
                        contact = BuildContact();

                        return contact;
                    }
                    else
                    {
                        if (this.CompanyContactId > 0)
                        {
                            if (CurrentCompanyContact.MemberId > 0)
                            {
                                Member member = new Member();
                                member = Facade.GetMemberById(CurrentCompanyContact.MemberId);
                                member.PrimaryEmail = txtEmail.Text;
                                member = Facade.UpdateMember(member);
                                Facade.UpdateAspNet_User(txtEmail.Text.Trim(), member.UserId);
                            }
                            else
                            {
                                RegisterUser();
                            }
                            contact = BuildContact();
                            return contact;
                        }
                        else
                            return null;
                    }
                }
            }
            if (_role.ToLower().ToString() == "department contact" || _role.ToLower().ToString() == "companycontact")
            {
                contact = BuildContact();

                //**********Added by pravin khot on 29/June/2016****** 
                if (ChkExistingUser.Checked == false)
                {
                    if (_role.ToLower().ToString() == "companycontact" && _memberid == 0)
                    {
                        MembershipUser user = Membership.GetUser(txtEmail.Text.Trim());
                        if (user == null && this.CompanyContactId == 0)
                        {
                            if (_role != string.Empty)
                            {
                                if (_memberid == 0)
                                    RegisterUser();
                                else
                                {

                                }
                                return contact;
                            }
                        }
                    }
                }
                //*******************END**********************************

                CompanyContact temp = null;
                if (this.CompanyContactId == 0)
                {
                    if (contact != null)
                        if (ChkExistingUser.Checked)
                            temp = Facade.GetCompanyContactByEmailAndCompanyId(txtEmail.Text.Trim(), CurrentCompanyId);
                        else
                            temp = Facade.GetCompanyContactByEmail(txtEmail.Text.Trim());
                    if (temp != null)
                    {
                        return null;
                    }
                    else
                        return contact;
                }
                else
                {
                    if (contact != null)
                        return contact;
                }
            }

            return null;
        }
        #endregion

        private void checkPrimaryEmailAvail()
        {
            if (CurrentCompanyContact.Email == null)
            {
                Member m = Facade.GetMemberByMemberEmail(txtEmail.Text);
                if (m != null)
                {
                    cvPrimaryEmail.IsValid = false;
                    cvPrimaryEmail.ErrorMessage = "Email address already exists, please enter a unique email.";
                    return;
                }
            }
            else
            {
                if (CurrentCompanyContact.Email.Trim().ToLower() != txtEmail.Text.Trim().ToLower())
                {
                    Member m = Facade.GetMemberByMemberEmail(txtEmail.Text);
                    if (m != null)
                    {
                        cvPrimaryEmail.IsValid = false;
                        cvPrimaryEmail.ErrorMessage = "Email address already exists, please enter a unique email.";
                        return;
                    }
                }
            }
        }

        #region OLD CODE
        private void SaveCompanyContact_Old() // Function rename by pravin khot on 26/Feb/2016  old name- (SaveCompanyContact) 
        {
            if (IsValid)
            {
                try
                {

                    CompanyContact companyContact = BuildCompanyContact();
                    if (companyContact != null)
                    {
                        if (companyContact.IsNew)
                        {
                            companyContact.CompanyId = base.CurrentCompany.Id;
                            companyContact.MemberId = _memberid;
                            if (companyContact.CompanyId == 0)
                            {
                                MiscUtil.ShowMessage(lblMessage, "Company not found, Please register a company in CompanyInfo tab", false);
                                lblMessage.ForeColor = System.Drawing.Color.Red;
                                return;
                            }
                            Facade.AddCompanyContact(companyContact);
                            MiscUtil.ShowMessage(lblMessage, AccountType + " has been added successfully.", false);
                            MiscUtil.AddActivityOnObject(MemberType.Company, base.CurrentCompany.Id, companyContact.CreatorId, ActivityType.AddCompanyContact, Facade);     //0.1
                        }
                        else
                        {
                            companyContact.UpdatorId = base.CurrentMember.Id;
                            Facade.UpdateCompanyContact(companyContact);
                            MiscUtil.ShowMessage(lblMessage, AccountType + " has been updated successfully.", false);
                            MiscUtil.AddActivityOnObject(MemberType.Company, base.CurrentCompany.Id, companyContact.CreatorId, ActivityType.UpdateCompanyContact, Facade);      //0.1                       
                        }
                        Clear();
                        BindList();
                        IList<CompanyContact> companyContactList = Facade.GetAllCompanyContactByComapnyId(base.CurrentCompanyId);
                        if (companyContactList != null && companyContactList.Count > 0)
                        {
                            for (int i = 0; i < companyContactList.Count; i++)
                            {
                                if (companyContactList[i].IsPrimaryContact)
                                {
                                    companyContact.IsPrimaryContact = false;
                                    chkIsPrimary.Checked = companyContact.IsPrimaryContact;
                                    chkIsPrimary.Enabled = true;
                                    return;
                                }
                                else
                                {
                                    companyContact.IsPrimaryContact = true;
                                    chkIsPrimary.Checked = companyContact.IsPrimaryContact;
                                    chkIsPrimary.Enabled = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        lblCheckUser.Visible = true;
                        lblCheckUser.Text = string.Empty;
                        lblCheckUser.Text = "Email Id not available. Please try with another email Id.";
                        lblCheckUser.ForeColor = System.Drawing.Color.Red;
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }
            }
        }
        #endregion

        //New code added/Modify by pravin khot on 26/Feb/2016***********START****
        #region New CODE
        private void SaveCompanyContact()
        {
            bool isapproved = true;
            if (_role.ToLower().ToString() == "vendor" || _role.ToLower().ToString() == "CompanyContact")
            {
                isapproved = IsValid;
            }
            if (isapproved)
            {
                try
                {

                    CompanyContact companyContact = BuildCompanyContact();
                   
                    if (companyContact != null)
                    {                        

                        if (companyContact.IsNew)
                        {
                            companyContact.CompanyId = base.CurrentCompany.Id;
                            companyContact.MemberId = _memberid;
                            //**********Code added by pravin khot on 23/June/2016******
                            if (companyContact.MemberId == 0)
                            {
                                Member memb = Facade.GetMemberByMemberEmail(txtEmail.Text);
                                companyContact.MemberId = memb.Id;
                            }
                            //************END*************************
                            if (companyContact.CompanyId == 0)
                            {
                                MiscUtil.ShowMessage(lblMessage, "Company not found, Please register a company in CompanyInfo tab", false);
                                lblMessage.ForeColor = System.Drawing.Color.Red;
                                return;
                            }

                            Facade.AddCompanyContact(companyContact);
                            MiscUtil.ShowMessage(lblMessage, AccountType + " has been added successfully.", false);
                            MiscUtil.AddActivityOnObject(MemberType.Company, base.CurrentCompany.Id, companyContact.CreatorId, ActivityType.AddCompanyContact, Facade);     //0.1
                        }
                        else
                        {
                            companyContact.UpdatorId = base.CurrentMember.Id;
                            Facade.UpdateCompanyContact(companyContact);



                            MiscUtil.ShowMessage(lblMessage, AccountType + " has been updated successfully.", false);
                            MiscUtil.AddActivityOnObject(MemberType.Company, base.CurrentCompany.Id, companyContact.CreatorId, ActivityType.UpdateCompanyContact, Facade);      //0.1                       
                        }
                        Clear();
                        BindList();
                        IList<CompanyContact> companyContactList = Facade.GetAllCompanyContactByComapnyId(base.CurrentCompanyId);
                        if (companyContactList != null && companyContactList.Count > 0)
                        {
                            for (int i = 0; i < companyContactList.Count; i++)
                            {
                                if (companyContactList[i].IsPrimaryContact)
                                {
                                    companyContact.IsPrimaryContact = false;
                                    chkIsPrimary.Checked = companyContact.IsPrimaryContact;
                                    chkIsPrimary.Enabled = true;
                                    return;
                                }
                                else
                                {
                                    companyContact.IsPrimaryContact = true;
                                    chkIsPrimary.Checked = companyContact.IsPrimaryContact;
                                    chkIsPrimary.Enabled = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        lblCheckUser.Visible = true;
                        lblCheckUser.Text = string.Empty;
                        lblCheckUser.Text = "Email Id not available. Please try with another email Id.";
                        lblCheckUser.ForeColor = System.Drawing.Color.Red;
                    }
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message.Replace("Company", "BU").Replace("company", "BU"), true);
                }
            }
        }
        #endregion
        //**********************END new CODE****************************
        private void Clear()
        {
            CompanyContact companycontact = CurrentCompanyContact;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtTitle.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtZip.Text = string.Empty;
            txtOfficePhone.Text = string.Empty;
            txtOfficePhoneExtension.Text = string.Empty;
            txtMobilePhone.Text = string.Empty;
            txtFax.Text = string.Empty;
            chkIsPrimary.Checked = false;
            chkIsPrimary.Enabled = true;
            //chkNoBulkEmail.Checked = false;
            txtContactRemarks.Text = string.Empty;
            txtOwnership.Text = string.Empty;
            ddlEthnicBackgroud.SelectedIndex = 0;
            chkIsOwner.Checked = false;
            CompanyContactId = 0;
            uclCountryState.SelectedCountryId = 0;
            //**********Added bu pravin khot on 25/Feb/2016*
            ddlemail.SelectedIndex = 0;
            ChkExistingUser.Checked = false;
            EnableforBucontact();
            //****************END***********************
        }

        #endregion

        #region Events

        private void Page_Load(object sender, System.EventArgs e)
        {          
            MemberExtendedInformation mailsetting = Facade.GetMemberExtendedInformationByMemberId(CurrentMember.Id);
            if (mailsetting.MailSetting != null)
            {
                ArrayList AccessArray = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                if (AccessArray.Contains(592))
                    IsMailSetting = true;
                else
                    IsMailSetting = false;
            }
            else
                IsMailSetting = false;
            CompanyAssignedManager Manager = Facade.GetCompanyAssignedManagerByCompanyIdAndMemberId(CurrentCompanyId , CurrentMember.Id);
            if (Manager != null)
                IsManager = true;
            else
                IsManager = false;
            if (!IsPostBack)
            {
                ChkExistingUser.Checked = false;
                txtEmail.Focus();
                txtEmail.Attributes.Remove("OnChange");
                /*Commented out for allow dublicate*/
                txtEmail.Attributes.Add("OnChange", "CheckPrimaryEmailAvailability('" + txtEmail.ClientID + "','" + 0 + "','" + hdnCheckValidity.ClientID + "');");
                PrepareView();
                Country country = Facade.GetCountryById(Convert.ToInt32(SiteSetting[DefaultSiteSetting.Country.ToString()].ToString()));
                if (country != null)
                {
                    if (country.Name == "India")
                    {
                        revZIPCode.ErrorMessage = "Please enter a valid PIN code";
                    }
                }
                MiscUtil.PopulateEthnicGroupType(ddlEthnicBackgroud, Facade);
                pnlEditor.Visible = !IsInformationReadOnly;
                BindList();
                //***********code added by pravin khot on 2/June/2016*****************
                if (_role.ToLower() == ContextConstants.ROLE_VENDOR.ToLower())
                {
                    divChkExistingUser.Visible = false;
                }
                //*************END********************************
            }           
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            //checkPrimaryEmailAvail();
            SaveCompanyContact();
            txtEmail.Attributes.Remove("OnChange");
            /*Commented out for allow dublicate*/
            txtEmail.Attributes.Add("OnChange", "CheckPrimaryEmailAvailability('" + txtEmail.ClientID + "','" + 0 + "','" + hdnCheckValidity .ClientID  + "');");
            uclCountryState.SelectedCountryId =0;
            ChkExistingUser.Checked = false;
        }

        //*****************Code added by pravin khot on 25/Feb/2016*****START******************
        protected void ddlEmail_Click(object sender, EventArgs e)
        {
            string[] str;
            if (ddlemail.SelectedIndex > 0)
            {
                str = ddlemail.SelectedItem.Text.ToString().Split('[');
                txtEmail.Text = str[1].Replace("]", "").Trim();
            }
            //EmployeeOverviewDetails Emp=Facade.GetEmployeeOverviewDetails(Convert.ToInt32(ddlemail.SelectedValue));
            Member mem = Facade.GetMemberById(Convert.ToInt32(ddlemail.SelectedValue));
            if (mem != null)
            {

                txtFirstName.Text = mem.FirstName;
                txtLastName.Text = mem.LastName;
            }
        }
        protected void ChkExisting_Click(object sender, EventArgs e)
        {
            if (ChkExistingUser.Checked)
            {
                divEmailtxt.Style.Add("display", "none");
                divEmailddl.Style.Add("display", "");
                txtFirstName.Enabled = false;
                txtLastName.Enabled = false;

                txtEmail.Attributes.Clear();
                txtEmail.Text = "";
                cvPrimaryEmail.Enabled = false;

            }
            else
            {
                divEmailtxt.Style.Add("display", "");
                divEmailddl.Style.Add("display", "none");
                txtFirstName.Enabled = true;
                txtLastName.Enabled = true;

                cvPrimaryEmail.Enabled = true;
                txtEmail.Attributes.Add("OnChange", "CheckPrimaryEmailAvailability('" + txtEmail.ClientID + "','" + 0 + "','" + hdnCheckValidity.ClientID + "');");
                Clear();
            }
            //ddlemail.Visible = true;
        }
        protected void EnableforBucontact()
        {
            divEmailtxt.Style.Add("display", "");
            divEmailddl.Style.Add("display", "none");
            txtFirstName.Enabled = true;
            txtLastName.Enabled = true;

            ChkExistingUser.Checked = false;
            ChkExistingUser.Enabled = true;
            ddlemail.Enabled = true;
            ddlemail.SelectedIndex = 0;
        }

        //*********END***************************************

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            BindList();
            uclCountryState.SelectedCountryId = 0;
            uclCountryState.SelectedStateId = 0;
        }

        #endregion

        #region ListView Events

        protected void lsvCompanyContact_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _IsAdmin = base.IsUserAdmin;
            
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                CompanyContact companyContact = ((ListViewDataItem)e.Item).DataItem as CompanyContact;

                if (companyContact != null)
                {
                    LinkButton lblName = (LinkButton)e.Item.FindControl("lblName");
                    Label lblTitle = (Label)e.Item.FindControl("lblTitle");
                    HyperLink lnkEmail = (HyperLink)e.Item.FindControl("lnkEmail");
                    Label lblDirectNumber = (Label)e.Item.FindControl("lblDirectNumber"); 
                    Label lblMobile = (Label)e.Item.FindControl("lblMobile");
                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    Label lblIsPrimary = (Label)e.Item.FindControl("lblIsPrimary");

                    ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                    lblName.Text = companyContact.FirstName + " " + companyContact.LastName;
                    SecureUrl Modalurl = UrlHelper.BuildSecureUrl("../Modals/CompanyContact.aspx", string.Empty, UrlConstants.PARAM_CONTACT_ID, companyContact.Id.ToString());
                    lblName.OnClientClick = "javascript:EditModal('" + Modalurl.ToString() + "','675px','450px'); return false;";

                    lblTitle.Text = companyContact.Title;
                    lblMobile.Text = companyContact.MobilePhone;
                    lblCity.Text = companyContact.City;
                    
                    if (!string.IsNullOrEmpty(companyContact.OfficePhone ))
                    {
                        if (!string.IsNullOrEmpty(companyContact.OfficePhoneExtension ))
                        {
                            lblDirectNumber.Text = companyContact.OfficePhone  + "  x" +  companyContact.OfficePhoneExtension  ;
                        }
                        else
                        {
                            lblDirectNumber.Text = companyContact.OfficePhone ;
                        }
                    }
                    if (IsMailSetting)
                    {

                        int sitemapid = 0;
                        int parentid = 0;
                        if (CurrentCompanyStatus == CompanyStatus.Client) { sitemapid = 592; parentid = 11; }
                        else if (CurrentCompanyStatus == CompanyStatus.Department) { sitemapid = 632; parentid = 622; }
                        else if (CurrentCompanyStatus == CompanyStatus.Vendor) { sitemapid = 647; parentid = 638; }
                        if (!string.IsNullOrEmpty(companyContact.Email))
                            ControlHelper.SetHyperLink(((HyperLink)e.Item.FindControl("lnkEmail")), "../SFA/CompanyEmail.aspx", string.Empty, companyContact.Email.ToLower(), UrlConstants.PARAM_COMPANY_ID, companyContact.CompanyId.ToString(), UrlConstants.PARAM_SITEMAP_ID, sitemapid.ToString () , UrlConstants .PARAM_SITEMAP_PARENT_ID ,parentid .ToString (), UrlConstants.PARAM_MEMBER_ID, companyContact.MemberId.ToString());
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(companyContact.Email))
                            ControlHelper.SetHyperLink(((HyperLink)e.Item.FindControl("lnkEmail")), "mailto:" + companyContact.Email.ToLower(), companyContact.Email.ToLower());
                    }
                    if (companyContact.IsPrimaryContact)
                    {
                        lblIsPrimary.Text = "Yes";
                    }
                    else
                    {
                        lblIsPrimary.Text = "No";
                    }
                    btnDelete.OnClientClick = "return ConfirmDelete('"+AccountType + "')";
                    btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(companyContact.Id);

                    btnDelete.Visible = btnEdit.Visible = !IsInformationReadOnly;
                    if (IsManager || _IsAdmin)
                        btnDelete.Visible = true;
                    else
                        btnDelete.Visible = false;
                }
            }
        }

        protected void lsvCompanyContact_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);
            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    this.CompanyContactId = id;
                    
                    PrepareView();
                    txtEmail.Attributes.Remove("OnChange");
                    /*Commented out for allow dublicate*/
                    txtEmail.Attributes.Add("OnChange", "CheckPrimaryEmailAvailability('" + txtEmail.ClientID + "','" + CurrentCompanyContact.MemberId + "','" + hdnCheckValidity.ClientID + "');");
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        Company currentcompany = base.CurrentCompany;
                        CompanyContact companycontact = Facade.GetCompanyContactById(id);

                        //*********Condition added by pravin khot on 23/March/2017***********
                        string companycontactByJobPostingHiring = Facade.GetCompanyContactByJobPostingHiringTeam(id);
                        if (companycontactByJobPostingHiring == "")//*********Condition added by pravin khot on 14/March/2017***********
                        {

                            if (companycontact != null)
                            {
                                string email = companycontact.Email;
                                //if (email != string.Empty)
                                //{
                                //    string userName = Membership.GetUserNameByEmail(email);
                                //    if (userName != null)
                                //    {
                                //        MembershipUser membershipUser = Membership.GetUser(userName);
                                //        int length = Roles.GetRolesForUser(membershipUser.UserName).Length;
                                //        if (length > 0)
                                //        {
                                //            try
                                //            {
                                //                Roles.RemoveUserFromRole(membershipUser.UserName, currentcompany.CompanyStatus.ToString());
                                //            }
                                //            catch
                                //            {
                                //            }

                                //        }
                                //        Facade.DeleteMemberById(companycontact.MemberId);
                                //        Membership.DeleteUser(membershipUser.UserName, true);
                                //    }
                                //}
                                if (Facade.DeleteCompanyContactById(id))
                                {
                                    BindList();
                                    uclCountryState.SelectedCountryId = 0;
                                    MiscUtil.AddActivityOnObject(MemberType.Company, base.CurrentCompany.Id, CurrentMember.Id, ActivityType.DeleteCompanyContact, Facade);
                                    MiscUtil.ShowMessage(lblMessage, AccountType + " has been successfully deleted.", false);
                                }
                                if (this.CompanyContactId == id)
                                    Clear();
                                IList<CompanyContact> companyContactList = Facade.GetAllCompanyContactByComapnyId(base.CurrentCompanyId);
                                if (companyContactList == null)
                                {
                                    companycontact.IsPrimaryContact = true;
                                    chkIsPrimary.Checked = companycontact.IsPrimaryContact;
                                    chkIsPrimary.Enabled = false;
                                }
                                if (companyContactList != null && companyContactList.Count > 0)
                                {
                                    for (int i = 0; i < companyContactList.Count; i++)
                                    {
                                        if (companyContactList[i].IsPrimaryContact)
                                        {
                                            companycontact.IsPrimaryContact = false;
                                            chkIsPrimary.Checked = companycontact.IsPrimaryContact;
                                            chkIsPrimary.Enabled = true;
                                            return;
                                        }

                                        else
                                        {
                                            companycontact.IsPrimaryContact = true;
                                            chkIsPrimary.Checked = companycontact.IsPrimaryContact;
                                            chkIsPrimary.Enabled = false;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                BindList();
                                uclCountryState.SelectedCountryId = 0;
                                MiscUtil.AddActivityOnObject(MemberType.Company, base.CurrentCompany.Id, CurrentMember.Id, ActivityType.DeleteCompanyContact, Facade);
                                MiscUtil.ShowMessage(lblMessage, AccountType + " has been successfully deleted.", false);
                                Clear();

                                DeleteView();
                            }

                        }
                        else//*********Condition added by pravin khot on 23/March/2017***********
                        {
                            BindList();
                            uclCountryState.SelectedCountryId = 0;
                            MiscUtil.AddActivityOnObject(MemberType.Company, base.CurrentCompany.Id, CurrentMember.Id, ActivityType.DeleteCompanyContact, Facade);
                            MiscUtil.ShowMessage(lblMessage, "Cannot delete the BU Contact. Allready Added to requisition.", true);
                            Clear();

                            DeleteView();
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
               
            }
            
        }

        #endregion
    }
}