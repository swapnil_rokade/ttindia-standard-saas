﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:           JobDetails.ascx.cs
    Description:         
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date             Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 10/Feb/2016      Prasanth Kumar G    Sort out Exception other than firefox browser
 ------------------------------------------------------------------------------------------------------------------------------------------       
*/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections.Generic;
using System.Collections;
using System.Drawing.Text;
namespace TPS360.Web.UI
{
    public partial class Controls_JobDetail : RequisitionBaseControl
    {
        private int _memberId
        {
            get
            {
                return base.CurrentMember != null ? base.CurrentMember.Id : 0;
            }
        }
        private int JobId
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID ]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_JOB_ID]);
                }
                else return 0;

            }
        }

        private string FromPage
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl["FromPage"]))
                {
                    return Helper.Url.SecureUrl["FromPage"].ToString ();
                }
                else return string .Empty ;
            }
        }
        private int DefaultCountryCode
        {
            get
            {
                //Code Modified by Prasanth on 10/Feb/2016
                return 2;  //SiteSetting[DefaultSiteSetting.Country.ToString()].ToString();
            }

        }
        private string CountryName
        {
            get;
            set;
        }

        public JobPosting DraftJobPosting
        {
            
            set
            {
                Label lbModalTitle = (Label)this.Page.Master.FindControl("cphHomeMasterTitle").FindControl ("uclTemplate").FindControl ("lbModalTitle");
                if (lbModalTitle != null) lbModalTitle.Text = value.JobTitle;
                string jobtitle, jobpostingcode = "";
                divBasicInfo.InnerHtml = MiscUtil.GetJobDetailTable(0, Facade, out jobtitle, out jobpostingcode, FromPage ==string .Empty ? "JobDetail" : FromPage ,value,CurrentMember .Id  );
                LoadAdditionalInfo(value);
                if (Document != null && Document.Count > 0) { divDocuments.Visible = true; bindDocuments(Document); }
                else divDocuments.Visible = false;

            }
        }
        IList<JobPostingDocument> _Document = null;
        public IList<JobPostingDocument> Document
        {
            set
            {
                _Document = value;
            }
            get
            {
                return _Document;
            }
        }

        IList<JobPostingHiringTeam> _AssignedRecruitersId = null;
        public IList<JobPostingHiringTeam > AssignedRecuiters
        {
            set
            {
                lsvAssignedTeam.DataSource = value;
                lsvAssignedTeam.DataBind();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (DefaultCountryCode != "") //line modified by Prasanth on 10/Feb/2016
                if (DefaultCountryCode > 0)
            {
                Country country = Facade.GetCountryById(Convert.ToInt32(DefaultCountryCode));
                if (country != null)
                {
                    CountryName = country.Name;
                }
            }
            if (!IsPostBack)
            {

                if (JobId > 0)
                {
					//Job Description View in Employee Referral Portal.
                    if (FromPage != "Referrer")
                    if (base.CurrentMember == null) Response.Redirect("..//Login.aspx");
                   
                    // LoadJobDeatils();
                    Label lbModalTitle = (Label)this.Page.Master.FindControl("lbModalTitle");
                    string jobtitle, jobpostingcode = "";
                     if (FromPage != "Referrer")
                        divBasicInfo.InnerHtml = MiscUtil.GetJobDetailTable(JobId, Facade, out jobtitle, out jobpostingcode,FromPage==string.Empty ? "JobDetail":FromPage, null,CurrentMember .Id );
                     else
                        divBasicInfo.InnerHtml = MiscUtil.GetJobDetailTable(JobId, Facade, out jobtitle, out jobpostingcode, FromPage == string.Empty ? "JobDetail" : FromPage, null, 0);				
                    if (FromPage == "JobDetail")
                    {
                        if (lbModalTitle != null) lbModalTitle.Text = jobtitle + " - " + jobpostingcode;
                    }
                    else { if (lbModalTitle != null) lbModalTitle.Text = "Job Summary - " + jobtitle; }

                   
                    LoadAdditionalInfo(null);
                    if (FromPage != "Vendor" && FromPage != "Referrer")
                    {
                        PopulateHiringTeamMembers();
                        divHiringTeam.Visible = true;
                    }
                }
            }
            if (FromPage == "JobDetail")
            {
                System.Web.UI.HtmlControls.HtmlGenericControl divButtons = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Page.Master.FindControl("divButtons");
                SecureUrl urlEmail = UrlHelper.BuildSecureUrl("../Dashboard/" + UrlConstants.Dashboard.NEWMAIL, "", UrlConstants.PARAM_JOB_ID, JobId.ToString(), UrlConstants.PARAM_PAGE_FROM, "JobDetail");
                SecureUrl urlPrint = UrlHelper.BuildSecureUrl("../Requisition/" + UrlConstants.Requisition.JOB_POSTING_PRINT, string.Empty, UrlConstants.PARAM_JOB_ID, JobId.ToString());
                SecureUrl urlEditJob = UrlHelper.BuildSecureUrl("../" + (UrlConstants.Requisition.JOB_POSTING_EDITOR_PAGE.Replace ("~/","").Replace ("//","/")), string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(JobId ),UrlConstants .PARAM_SITEMAP_PARENT_ID ,"13");
                string buttons ="";
                JobPostingHiringTeam team = Facade.GetJobPostingHiringTeamByMemberId(base.CurrentMember.Id, JobId );
                if (base.IsUserAdmin || team != null)
                {
                    buttons += "<a    class=\"btn\"  href='" + urlEditJob.ToString() + "' Target='_blank' style='margin-right:2px' >Edit</a>";
                }
                buttons += "<input type=\"button\"   class=\"btn\" value=\"Email\"  style='margin-right:2px' onclick=\"window.open('" + urlEmail.ToString() + "','EmailView', 'height=626,width=770, toolbar=0, scrollbars=1, menubar=0, location=0');\"/>";
                buttons += "<input type=\"button\"   class=\"btn\" value=\"Print\"  style='margin-right:2px' onclick=\"window.open('" + urlPrint.ToString() + "','EmailView', 'height=626,width=770, toolbar=0, scrollbars=1, menubar=0, location=0');\"/>";
                if (divButtons != null) divButtons.InnerHtml = buttons;
            }
            else if (FromPage == "Vendor" && FromPage != "Referrer")
            {
                System.Web.UI.HtmlControls.HtmlGenericControl divButtons = (System.Web.UI.HtmlControls.HtmlGenericControl)this.Page.Master.FindControl("divButtons");
                SecureUrl urlPrint = UrlHelper.BuildSecureUrl("../Requisition/" + UrlConstants.Requisition.JOB_POSTING_PRINT, string.Empty, UrlConstants.PARAM_JOB_ID, JobId.ToString(),UrlConstants .PARAM_PAGE_FROM ,"Vendor");
                string buttons = "";
                buttons += "<input type=\"button\"   class=\"btn\" value=\"Print\"  style='margin-right:2px' onclick=\"window.open('" + urlPrint.ToString() + "','EmailView', 'height=626,width=770, toolbar=0, scrollbars=1, menubar=0, location=0');\"/>";
                if (divButtons != null) divButtons.InnerHtml = buttons;
            }
           
        }
        private void PopulateHiringTeamMembers()
        {
            lsvAssignedTeam.DataSource = Facade.GetAllJobPostingHiringTeamByJobPostingId(JobId );
            lsvAssignedTeam.DataBind();
        }
        protected void lsvAssignedTeam_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPostingHiringTeam jobPostingHiringTeam = ((ListViewDataItem)e.Item).DataItem as JobPostingHiringTeam;

                if (jobPostingHiringTeam != null)
                {
                    Label lblMemberName = (Label)e.Item.FindControl("lblMemberName");
                    Label lblMemberEmail = (Label)e.Item.FindControl("lblMemberEmail");
                    Label lblEmployeeType = (Label)e.Item.FindControl("lblEmployeeType");

                    if (jobPostingHiringTeam.MemberId > 0)
                    {
                        //if (jobPostingHiringTeam.EmployeeType == EmployeeType.Internal.ToString())
                        {
                          //  Member member = Facade.GetMemberById(jobPostingHiringTeam.MemberId);
                            //if (member != null)
                            {
                                lblMemberName.Text = jobPostingHiringTeam.Name;// member.FirstName + " " + member.LastName;
                                lblMemberEmail.Text =jobPostingHiringTeam .PrimaryEmail  ;
                            }
                        }
                        //else if (jobPostingHiringTeam.EmployeeType == EmployeeType.External.ToString())
                        //{

                        //    CompanyContact companyContact = Facade.GetCompanyContactById(jobPostingHiringTeam.MemberId);
                        //    if (companyContact != null)
                        //    {
                        //        lblMemberName.Text = companyContact.FirstName + " " + companyContact.LastName;
                        //        lblMemberEmail.Text = companyContact.Email;
                        //    }
                        //}
                    }
                    if (jobPostingHiringTeam.MemberId > 0)
                    {
                        if (Facade.GetEmployeeById(jobPostingHiringTeam.MemberId) != null)
                        {
                            string strMemberRole = (Facade.GetEmployeeById(jobPostingHiringTeam.MemberId)).SystemAccess;                    //0.6
                            lblEmployeeType.Text = (strMemberRole == "") ? "Employee" : strMemberRole;                                      //0.6
                        }
                        else
                        {
                            lblEmployeeType.Text = "Admin";
                        }
                    }
                }
            }
        }
        public IList<int> getAssignedRecruitersFromList()
        {
            IList<int> ReqIds = new List<int>();
            ReqIds.Add(CurrentMember.Id);
            foreach (ListViewDataItem item in lsvAssignedTeam.Items)
            {
                HiddenField hdnMemberID = (HiddenField)item.FindControl("hdnMemberID");
                if (hdnMemberID.Value != "")
                {
                    if (!ReqIds.Contains(Convert.ToInt32(hdnMemberID.Value)))
                        ReqIds.Add(Convert.ToInt32(hdnMemberID.Value));
                }

            }
            return ReqIds;

        }
        protected void lsvAssignedTeam_PreRender(object sender, EventArgs e)
        {
            //IList<int> selectedemp = getAssignedRecruitersFromList();
                //lsvAssignedTeam.DataSource = null;
                lsvAssignedTeam.DataBind();           
        }

        private void LoadJobDeatils()
        {
            IList<Details> JobDetails = new List<Details>();
           
            JobPosting jobposting;
            if (JobId != null)
            {
                jobposting = Facade.GetJobPostingById(JobId);

                //string ApplicationEdition = MiscUtil.GetApplicationEdition(Facade);
                //if (ApplicationEdition == UIConstants.INTERNAL_HIRING)
                //{
                //    //Department
                //    if (jobposting.JobDepartmentLookUpId != null)
                //    {
                //        if (jobposting.JobDepartmentLookUpId > 0)
                //        {
                //            JobDetails.Add(BuildDetials("Department", Header.Head));
                //            JobDetails.Add(BuildDetials(Facade.GetDepartmentById(jobposting.JobDepartmentLookUpId).Name, Header.Content));
                //        }
                //    }
                //}
                //else
                {

                    if (jobposting.ClientId > 0)
                    {
                        Company com = Facade.GetCompanyById(Convert.ToInt32(jobposting.ClientId));
                        if (com != null)
                        {
                            if (SiteSetting [DefaultSiteSetting .ApplicationEdition .ToString ()].ToString () == UIConstants.INTERNAL_HIRING)
                            {
                                JobDetails.Add(BuildDetials("Department", Header.Head));
                            }
                            else JobDetails.Add(BuildDetials("Company", Header.Head));
                            JobDetails.Add(BuildDetials(com.CompanyName, Header.Content));
                        }

                        if (jobposting.ClientContactId > 0)
                        {
                            CompanyContact contact = Facade.GetCompanyContactById(jobposting.ClientContactId);
                            if (contact != null)
                            {
                                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                                {
                                    JobDetails.Add(BuildDetials("Department Contact", Header.Head));
                                }
                                else JobDetails.Add(BuildDetials("Client Contact", Header.Head));
                                JobDetails.Add(BuildDetials(contact.FirstName + " " + contact.LastName, Header.Content));
                            }
                        }
                    }
                }
                if(jobposting .OccuptionalSeriesLookupId >0)
                {
                  JobDetails .Add (BuildDetials ("Occupational Series", Header.Head ));
                 OccupationalSeries Occuseries=Facade.GetOccupationalSeriesById (jobposting.OccuptionalSeriesLookupId);
                    JobDetails .Add (BuildDetials (Occuseries==null ? "" :Occuseries .Id +" - " + Occuseries.SeriesTitle  ,Header .Content));

                }
                if (jobposting.ClientJobId  .Trim ()!=string .Empty )
                {
                    if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.GOVERNMENT_PORTAL)
                    {
                        JobDetails.Add(BuildDetials("Announcement #", Header.Head));
                    }
                    else if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                    {
                        JobDetails.Add(BuildDetials("Job ID", Header.Head));
                    }
                    else
                    {
                        JobDetails.Add(BuildDetials("Client Job ID", Header.Head));
                    }
                    JobDetails.Add(BuildDetials(jobposting .ClientJobId , Header.Content));

                }
                if (jobposting.NoOfOpenings  > 0)
                {
                    JobDetails.Add(BuildDetials("No. of Openings", Header.Head));
                    JobDetails.Add(BuildDetials(jobposting .NoOfOpenings .ToString (), Header.Content));

                }
                if (jobposting.WorkScheduleLookupId > 0)
                {
                    
                    GenericLookup glookup = Facade.GetGenericLookupById (jobposting.WorkScheduleLookupId);
                    if (glookup != null)
                    {
                        JobDetails.Add(BuildDetials("Work Schedule", Header.Head));
                        JobDetails.Add(BuildDetials(glookup .Name , Header.Content));
                    }
                }
                if (jobposting.PayGradeLookupId > 0)
                {
                    JobDetails.Add(BuildDetials("Pay Grade (GS)", Header.Head));
                    JobDetails.Add(BuildDetials(jobposting.PayGradeLookupId + "", Header.Content));
                }
                if (jobposting.JobCategoryLookupId > 0)
                {
                    JobDetails.Add(BuildDetials("Job Category", Header.Head));
                    Category category = Facade.GetCategoryById(jobposting.JobCategoryLookupId);
                    JobDetails.Add(BuildDetials(category == null ? "" :category .Name ,Header.Content));
                    
                }
                if (CountryName == "United States")
                {
                    if (jobposting.JobDurationLookupId != string.Empty && jobposting.JobDurationLookupId != "0")
                    {
                        JobDetails.Add(BuildDetials("Employment Type", Header.Head));
                        JobDetails.Add(BuildDetials(MiscUtil.SplitValues(jobposting.JobDurationLookupId, ',', Facade), Header.Content));
                    }
                }
                if (CountryName == "United States")
                {
                    if (jobposting.JobDurationMonth != "0")
                    {
                        GenericLookup glookup = Facade.GetGenericLookupById(Convert.ToInt32(jobposting.JobDurationMonth));
                        JobDetails.Add(BuildDetials("Duration", Header.Head));
                        JobDetails.Add(BuildDetials(glookup == null ? "" : glookup.Name, Header.Content));
                    }
                }

               if (jobposting.MinExpRequired .Trim() !=string .Empty || jobposting .MaxExpRequired.Trim () !=string .Empty )
               {
                   string exp="";
                 
                    exp+=jobposting.MinExpRequired;
                    if (jobposting.MinExpRequired.Trim() != string.Empty && jobposting.MaxExpRequired.Trim() != string.Empty) exp += " - ";
                    exp += jobposting .MaxExpRequired .Trim ()   +" years";
                    JobDetails.Add(BuildDetials("Experience Required", Header.Head));
                    JobDetails.Add(BuildDetials(exp, Header.Content));
               }

               JobDetails.Add(BuildDetials("Location", Header.Head));
               string add = jobposting.JobAddress1;
               add = add == string.Empty ? jobposting.JobAddress2 : add + ", " + jobposting.JobAddress2;
               add = add == string.Empty ? jobposting.City : add + ", " + jobposting.City;
               add = jobposting.StateId != 0 ? add + (add != "" ? ", " : "") + Facade.GetStateById(jobposting.StateId).Name : add;
               add = jobposting.CountryId != 0 ? add + (add != "" ? ", " : "") + Facade.GetCountryById(jobposting.CountryId).Name : add;
               add = jobposting.ZipCode != "" ? add + ", " + jobposting.ZipCode : add + "";
               JobDetails.Add(BuildDetials(add, Header.Content));
                if (jobposting.ClientHourlyRate != "")
                {
                         GenericLookup gsalary = new GenericLookup();
                         string pay="";
                         gsalary = Facade.GetGenericLookupById(jobposting.ClientHourlyRateCurrencyLookupId);
                         if (gsalary != null)
                         {
                             if (jobposting.PayRateCurrencyLookupId == 49)
                             {
                                 if (gsalary.Name.Trim().ToLower() == "inr")
                                     pay = "INR ";
                                 else   pay = gsalary.Name.Substring(0, 1);
                                 pay += jobposting.ClientHourlyRate ;
                             }
                             else
                             {
                                 pay = jobposting.ClientHourlyRate ;
                                 pay += " " + gsalary.Name;
                             }
                         }
                         else
                         {
                             pay = jobposting.ClientHourlyRate ;
                         }
                         string paycycle = "";
                         switch (jobposting.ClientRatePayCycle)
                         {
                             case "1":
                                 paycycle = "hour";
                                 break;
                             case "4":
                                 paycycle = "year";
                                 break;
                             case "3":
                                 paycycle = "month";
                                 break;
                             case "2":
                                 paycycle = "day";
                                 break;
                         }
                    if (paycycle.Trim() != string.Empty) pay += " / " + paycycle + ( gsalary !=null && gsalary .Name =="INR"?"  (Lacs)":"");
                         JobDetails.Add(BuildDetials("Bill Rate", Header.Head));
                         JobDetails.Add(BuildDetials(pay, Header.Content));
                }

                //Reporting To
                if (jobposting.ReportingTo.Trim() != string.Empty)
                {
                    JobDetails.Add(BuildDetials("Reporting To", Header.Head));
                    JobDetails.Add(BuildDetials(jobposting .ReportingTo , Header.Content));
                }

                //No Of Reportees
                if (jobposting.NoOfReportees  > 0)
                {
                    JobDetails.Add(BuildDetials("No. of Reportees", Header.Head));
                    JobDetails.Add(BuildDetials(jobposting.NoOfReportees.ToString () , Header.Content));
                }

                if (jobposting.PayRate.Trim() != string.Empty || jobposting .MaxPayRate >0)
                {
                    string payrate = "";
                    payrate = jobposting.PayRate;
                    if (Convert.ToInt32(jobposting.MaxPayRate) == jobposting.MaxPayRate) jobposting.MaxPayRate = Convert.ToInt32(jobposting.MaxPayRate);
                    if (payrate != string.Empty && jobposting .MaxPayRate >0 && payrate.ToLower ().Trim () !="open") payrate += "-";
                    if (jobposting.MaxPayRate > 0 && payrate.ToLower().Trim() != "open") payrate += jobposting.MaxPayRate;

                       GenericLookup gsalary = new GenericLookup();
                       string salary="";
                        if (jobposting.PayRateCurrencyLookupId > 0 && jobposting.PayRate.Trim().ToLower() != "open") gsalary = Facade.GetGenericLookupById(jobposting.PayRateCurrencyLookupId);
                        else gsalary = null;
                        if (gsalary != null)
                        {
                            if (jobposting.PayRateCurrencyLookupId == 49)
                            {
                                salary = gsalary.Name.Substring(0, 1);
                                salary += payrate ;
                            }
                            else
                            {
                                salary = payrate;
                                salary += " " + gsalary.Name;
                            }
                        }
                        else
                        {
                            salary = payrate;
                        }


                        string paycycle = "";
                        switch (jobposting.PayCycle)
                        {
                            case "1":
                                paycycle = "hour";
                                break;
                            case "4":
                                paycycle = "year";
                                break;
                            case "3":
                                paycycle = "month";
                                break;
                            case "2":
                                paycycle = "day";
                                break;
                        }
            if (paycycle.Trim() != string.Empty) salary += " / " + paycycle ;
            if (jobposting.PayRate != "Open" && gsalary.Name == "INR")
                salary += "  (Lacs)";
                    JobDetails.Add(BuildDetials("Salary", Header.Head));
                    JobDetails.Add(BuildDetials(salary, Header.Content));
                }
                //Education
                if (jobposting.RequiredDegreeLookupId != "0" && jobposting.RequiredDegreeLookupId !=string .Empty )
                {
                    JobDetails.Add(BuildDetials("Education", Header.Head));
                    JobDetails.Add(BuildDetials(MiscUtil .SplitValues (jobposting .RequiredDegreeLookupId ,',',Facade ), Header.Content));
                }

                if (jobposting.TaxTermLookupIds.Trim() != string.Empty)
                {
                    JobDetails.Add(BuildDetials("Payment Type", Header.Head));
                    JobDetails.Add(BuildDetials(SplitValues (jobposting.TaxTermLookupIds,','), Header.Content));
                }


                if (jobposting.StartDate.Trim() != string.Empty)
                {
                    JobDetails.Add(BuildDetials("Start Date", Header.Head));
                    if (System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern  == "dd/MM/yyyy")
                            JobDetails.Add(BuildDetials(jobposting.StartDate.ToString() == "" || jobposting.StartDate.ToString().Trim() == "ASAP" ? jobposting.StartDate.ToString() : (jobposting.StartDate.ToString().Split('/')[1] + "/" + jobposting.StartDate.ToString().Split('/')[0] + "/" + jobposting.StartDate.ToString().Split('/')[2]), Header.Content));
                        else   JobDetails.Add(BuildDetials(jobposting.StartDate.Trim() != string.Empty ? (jobposting.StartDate.Trim() != "ASAP" ? Convert.ToDateTime(jobposting.StartDate).ToShortDateString () : "ASAP") : " ", Header.Content));
                }
                if (jobposting.FinalHiredDate != DateTime.MinValue)
                {
                    if (jobposting.FinalHiredDate != Convert.ToDateTime("01/01/1900"))
                    {
                        JobDetails.Add(BuildDetials("Closing Date", Header.Head));
                        JobDetails.Add(BuildDetials(jobposting.FinalHiredDate.ToShortDateString (), Header.Content));
                    }
                }

                if (jobposting.TeleCommunication == true)
                {

                    JobDetails.Add(BuildDetials("Telecommuting", Header.Head));
                    JobDetails.Add(BuildDetials("Yes" , Header.Content));

                }
                if(jobposting .TravelRequired ==true )
                {
                    JobDetails.Add(BuildDetials("Travel Required", Header.Head));
                    JobDetails.Add(BuildDetials("Yes" + (jobposting.TravelRequiredPercent=="0"? "" : "-" +   jobposting.TravelRequiredPercent + "%") , Header.Content));

                }
                if (CountryName == "United States")
                {
                    if (jobposting.SecurityClearance == true)
                    {
                        JobDetails.Add(BuildDetials("Security Clearance", Header.Head));
                        JobDetails.Add(BuildDetials("Yes", Header.Content));

                    }
                }
                if (CountryName == "United States")
                {
                    if (jobposting.IsExpensesPaid == true)
                    {
                        JobDetails.Add(BuildDetials("Expenses Paid", Header.Head));
                        JobDetails.Add(BuildDetials("Yes", Header.Content));
                    }
                }
                //Work Athorization
                if (CountryName == "United States")
                {
                    if (jobposting.AuthorizationTypeLookupId != null && jobposting.AuthorizationTypeLookupId.Trim() != string.Empty)
                    {
                        JobDetails.Add(BuildDetials("Work Authorization", Header.Head));
                        JobDetails.Add(BuildDetials(SplitValues(jobposting.AuthorizationTypeLookupId, ','), Header.Content));
                    }
                }
               
            }
        }

        private void LoadAdditionalInfo(JobPosting jobposting)
        {
            if(jobposting ==null && JobId >0)                 jobposting = Facade.GetJobPostingById(JobId);
            if (jobposting.JobSkillLookUpId == string.Empty || FromPage=="Vendor") divjobskill.Visible = false;
            else
            {
                divjobskill.Visible = true ;
                IList<string> skills = Buildlists(jobposting.JobSkillLookUpId);
                if (skills != null)
                {
                    string content = "<ui>";
                    foreach (string s in skills)
                    {
                        Skill skill = Facade.GetSkillById(Convert.ToInt32(s));
                        if (skill != null)
                            content += "<li>" + skill.Name + "</li>";
                    }
                    content += "</ui>";
                    divSkillsContent.InnerHtml = content;
                }
            }


            if (jobposting.MinimumQualifyingParameters != null && jobposting.MinimumQualifyingParameters.Trim() != string.Empty)
            {
                divMQP.Visible = true ;
                char[] delim = { ',' };
                string[] MQP = jobposting.MinimumQualifyingParameters.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                string content = "<ui>";
                if (MQP != null)
                {
                    foreach (string s in MQP)
                    {
                        content += "<li>" + s + "</li>";
                    }
                }
                content += "</ui>";
                divMQPContent.InnerHtml = content;
            }
            else divMQP.Visible = false;

            if (CurrentJobPostingId > 0)
            {
                IList<JobPostingDocument> documents = Facade.GetAllJobPostingDocumentByJobPostingId(CurrentJobPostingId);
                if (documents != null)
                {
                    divDocuments.Visible = true;
                    bindDocuments(documents);
                   
                }
                else divDocuments.Visible = false;
            }
            

            if (jobposting.JobDescription.Trim() != "" && FromPage!="Vendor" && FromPage != "Referrer")
            {
                divJobDescription.Visible = true;
                divDescription.InnerHtml = jobposting.JobDescription;
            }
            else
            {
                divJobDescription.Visible = false;
            }

            if (jobposting.ClientBrief.Trim() != "")
            {
                divClientBrief.Visible = true;
                divCBrief.InnerHtml = MiscUtil.RemoveScript(jobposting.ClientBrief).Replace("\n", "<br/>");
            }
            else
            {
                divClientBrief.Visible = false;
            }
            if (jobposting.OtherBenefits == string.Empty) divbenefits.Visible = false;
            else
            {
                if (CountryName == "United States")
                {
                    divbenefits.Visible = true;
                    IList<string> benefits = Buildlists(jobposting.OtherBenefits);
                    if (benefits != null)
                    {
                        string content = "<ui>";
                        foreach (string s in benefits)
                        {
                            GenericLookup lookupName = Facade.GetGenericLookupById(Convert.ToInt32(s));
                            if (lookupName != null)
                                content += "<li>" + lookupName.Name + "</li>";
                        }
                        content += "</ui>";
                        divBenefitsContent.InnerHtml = content;
                    }
                }
                else
                    divbenefits.Visible = false;

            }
            //lsvJobDetails.DataSource = JobDetails;
            //lsvJobDetails.DataBind();

            if (jobposting.InternalNote != ""&&FromPage!="Vendor")
            {
                divAddiNotes.Visible = true;
                divAdditionalNotes.InnerText = jobposting.InternalNote;
            }
            else
                divAddiNotes.Visible = false;
            //this.Page.Title = "Requisition Preview - " + jobposting.JobPostingCode + " " + jobposting.JobTitle;
                
        }
        private void bindDocuments(IList <JobPostingDocument> documents)
        {
            string content = "<ui>";
            foreach (JobPostingDocument doc in documents)
            {
                string filetag = "";
                if (!doc.FileName.ToLower ().Contains ("/temp/"))
                {
                    filetag = MiscUtil.GetJobPostingDocumentPath(this.Page, CurrentJobPostingId, doc.FileName, "", true);
                    if (filetag == doc.FileName) filetag = "File not found (" + doc.FileName + ")";
                    else filetag = "<a href=\"" + filetag + "\" Target='_blank'>" + doc.FileName + "</a>";
                }
                else
                {
                    string filename = doc.FileName.Substring(doc.FileName.LastIndexOf("/")+1);
                    filetag = "<a href=\"" + doc.FileName + "\" Target='_blank'>" + filename + "</a>";
                }
                
                content += "<li> "+filetag +"</li>";
            }
            content += "</ui>";
            divDocumentContent.InnerHtml = content;
        }
        private string SplitValues(string value, char delim)
        {
            string result = "";
            char[] delimiters = new char[] { delim, '\n' };
            IList<string> splitarray = value.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in splitarray)
            {
                if (Convert.ToInt32(s) > 0)
                {
                    if (result.Length > 0) result += ", ";
                    result += Facade.GetGenericLookupById(Convert.ToInt32(s)).Name;
                }
            }
            return result;
        }
        private IList<string> Buildlists(string lists)
        {
            lists = lists.Trim();
            IList<string> benefitlist = new List<string>();
            char[] delimiters = new char[] { '!', '\n' };
            benefitlist = lists.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            return benefitlist;
        }
        private Details  BuildDetials(string text, Header  type)
        {
            Details _detail = new Details();
            _detail.Text = text;
            _detail.type = type;
            return _detail;

        }
        public  struct Details
        {
           public string Text;
            public Header  type;
        }
        public  enum Header
        {
            Head,
            Content
        }
        protected void lsvJobDetails_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            Details data = ( Details)((ListViewDataItem)e.Item).DataItem ;
            if (data.type == Header.Head)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl Div = new System.Web.UI.HtmlControls.HtmlGenericControl("divHeader");
                Div.Visible = true ;
               
                Label lblHeader = (Label)e.Item.FindControl("lblHeader");
                Label lblJob = (Label)e.Item.FindControl("lblJob");
                lblHeader.Text = data.Text;
                lblJob.Visible = false;
            }
            else
            {

                System.Web.UI.HtmlControls.HtmlGenericControl Div = new System.Web.UI.HtmlControls.HtmlGenericControl("divHeader");
                Div.Visible = false;
                Label lblJob = (Label)e.Item.FindControl("lblJob");
                lblJob.Text = data.Text;
                lblJob.Visible = true ;
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {

            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.Requisition .JOB_POSTING_PRINT , string.Empty, UrlConstants.PARAM_JOB_ID , JobId.ToString());
            string scriptRun = "<script> var w= window.open('" + url.ToString() + "','PrintView', 'width=600, height=600, toolbar=0, scrollbars=1, menubar=0, location=0');</script>";
            Page.ClientScript.RegisterStartupScript(typeof(Page), "PrintView", scriptRun);
        }
        protected void btnEmail_Click(object sender, EventArgs e)
        {
            SecureUrl url = UrlHelper.BuildSecureUrl("../Dashboard/"+UrlConstants.Dashboard .NEWMAIL , "", UrlConstants.PARAM_JOB_ID , JobId.ToString(), UrlConstants .PARAM_PAGE_FROM ,"JobDetail");
            string scriptRun = "<script> var w= window.open('" + url.ToString() + "','EmailView', 'height=626,width=770, toolbar=0, scrollbars=1, menubar=0, location=0');</script>";
            Page.ClientScript.RegisterStartupScript(typeof(Page), "NewMail", scriptRun);
        }
        
}

}