<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompanyDocument.ascx.cs"
    Inherits="TPS360.Web.UI.cltCompanyDocument" EnableViewState="true" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<div>
    <div class="TableRow" style="text-align: center">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
        <asp:HiddenField ID="hfMemberDocumentId" runat="server" />
        <asp:HiddenField ID="hfMemberDocumentName" runat="server" />
        <asp:HiddenField ID="hfDocumentType" runat="server" />
        <asp:HiddenField ID="hfResumeBuilderOption" runat="server" Value="0" />
    </div>
    <div class="TabPanelHeader">
        Upload Document
    </div>
    <div class="TableRow" runat="server" id="rowDocumentType">
        <div class="TableFormLeble">
            <asp:Label ID="lblDocumentType" runat="server" Text="Document Type"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:DropDownList ID="ddlDocumentType" runat="server" CssClass="CommonDropDownList"
                AutoPostBack="True" OnSelectedIndexChanged="ddlDocumentType_SelectedIndexChanged">
            </asp:DropDownList>
            <span class="RequiredField">*</span>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
            <asp:CompareValidator ID="cvDocumentType" ValidationGroup="UploadDocument" runat="server"
                ControlToValidate="ddlDocumentType" ErrorMessage="Please select document type."
                Operator="NotEqual" ValueToCompare="0" Display="Dynamic"></asp:CompareValidator>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label ID="lblDocumentTitle" runat="server" Text="Document Title"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox ID="txtDocumentTitle" runat="server" CssClass="CommonTextBox" Width="250px"
                EnableViewState="true"></asp:TextBox>
            <span class="RequiredField">*</span>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
            <asp:RequiredFieldValidator ID="rfvDocumentTitle" runat="server" ValidationGroup="UploadDocument"
                ControlToValidate="txtDocumentTitle" Display="Dynamic" ErrorMessage="Please enter document title."></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label ID="lblDocumentDescription" runat="server" Text="Description"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:TextBox ID="txtDocumentDescription" TextMode="MultiLine" Height="100px" runat="server"
                CssClass="CommonTextBox" Width="250px"></asp:TextBox>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
            <asp:Label ID="lblDocumentPath" runat="server" Text="Document Path"></asp:Label>:
        </div>
        <div class="TableFormContent">
            <asp:FileUpload ID="fuDocument" runat="server" />
            <span class="RequiredField">*</span>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
               <asp:UpdatePanel ID="pnlValidator" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
            <div class="TableRow">
                <asp:RequiredFieldValidator ID="rfvDocumentUpload" runat="server" ErrorMessage="Please select a file to upload."
                    ControlToValidate="fuDocument" ValidationGroup="UploadDocument"></asp:RequiredFieldValidator>
            </div>
            <div class="TableRow">
                <asp:RegularExpressionValidator Visible="false" ID="revDocument" runat="server" ErrorMessage="Please Upload JPGs,BMPs,GIFs,DOCs/DOCXs,RTFs,TXTs,ZIPs,XLS,XLSX,CSV,PPT,PPTX and PDFs only."
                    ValidationExpression="(.*\.[jJ][pP][gG])|(.*\.[dD][oO][cC][xX])|(.*\.[dD][oO][cC])|(.*\.[rR][tT][fF])|(.*\.[tT][xX][tT])|(.*\.[zZ][iI][pP])|(.*\.[pP][dD][fF])|(.*\.[xX][lL][sS][xX])|(.*\.[xX][lL][sS])|(.*\.[cC][sS][vV])|(.*\.[pP][pP][tT][xX])|(.*\.[pP][pP][tT]|(.*\.[gG][iI][fF])|(.*\.[bB][mM][pP]))"
                    ControlToValidate="fuDocument" ValidationGroup="UploadDocument"></asp:RegularExpressionValidator>
            </div>
            <div class="TableRow">
                <asp:RegularExpressionValidator Visible="false" ID="revPhoto" runat="server" ErrorMessage="Please Upload JPEGs,JPGs , GIFs ,PDFs and ZIPs only."
                    ValidationExpression="(.*\.[jJ][pP][eE][gG])|(.*\.[pP][dD][fF])|(.*\.[jJ][pP][gG])|(.*\.[gG][iI][fF])|(.*\.[zZ][iI][pP])|(.*\.[xX][lL][sS][xX])|(.*\.[xX][lL][sS])|(.*\.[cC][sS][vV])"
                    ControlToValidate="fuDocument" ValidationGroup="UploadDocument"></asp:RegularExpressionValidator>
            </div>
            <div class="TableRow">
                <asp:RegularExpressionValidator Visible="false" ID="revVideo" runat="server" ErrorMessage="Please Upload WMV,MPG and AVI only."
                    ValidationExpression="(.*\.[wW][mM][vV])|(.*\.[mM][pP][gG])|(.*\.[aA][vV][iI])|(.*\.[xX][lL][sS][xX])|(.*\.[xX][lL][sS])|(.*\.[cC][sS][vV])"
                    ControlToValidate="fuDocument" ValidationGroup="UploadDocument"></asp:RegularExpressionValidator>
            </div>
             </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlDocumentType" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormLeble">
        </div>
        <div class="TableFormContent">
            <asp:Button ID="btnUpload" CssClass="CommonButton" ValidationGroup="UploadDocument"
                runat="server" Text="Upload" OnClick="btnUpload_Click" />
        </div>
    </div>
    <div class="TabPanelHeader">
        List of Documents
    </div>
    
    <asp:ObjectDataSource ID="odsDoc" runat="server" SelectMethod="GetPaged"
                    TypeName="TPS360.Web.UI.CompanyDocumentsDataSource" SelectCountMethod="GetListCount"
                    EnablePaging="true" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:Parameter Name="companyId" DefaultValue="" />
                    </SelectParameters>
                </asp:ObjectDataSource>
    <asp:ListView ID="lsvCompanyDocument" runat="server" DataKeyNames="Id" DataSourceID ="odsDoc" OnItemDataBound="lsvDocuments_ItemDataBound"
        OnItemCommand="lsvCompanyDocument_OnItemCommand" OnPreRender="lsvDocuments_PreRender">
        <LayoutTemplate>
            <table id="tlbTemplate" runat="server" class="Grid" cellspacing="0" border="0">
                <tr>
                    <th>
                        Uploaded File
                    </th>
                    <th>
                        Title
                    </th>
                    <th>
                        Type
                    </th>
                    <th style="width: 60px">
                        Action
                    </th>
                </tr>
                <tr id="itemPlaceholder" runat="server">
                </tr>
                <tr class="Pager">
                    <td colspan="4">
                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" NoOfRowsCookie ="CompanyDocumentListRowPerPage"/>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <EmptyDataTemplate>
            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                <tr>
                    <td>
                        No data was returned.
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                <td>
                    <asp:Label ID="lblDocumentLink" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblComDocumentTitle" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblDocumentType" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" CommandName="EditItem">
                    </asp:ImageButton>
                    <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem">
                    </asp:ImageButton>
                </td>
            </tr>
        </ItemTemplate>
    </asp:ListView>
</div>
