﻿
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TPS360.BusinessFacade;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Linq;
using System.Web.UI;
namespace TPS360.Web.UI
{
    public partial class HotListManagerList : BaseControl
    {

        int _membergroupId = 0;
        private bool IsAccessToRemove = false;
        private  bool _mailsetting = false;
        private void PrepareView()
        {
          
            MiscUtil.PopulateMemberListByRole(lstSelectEmployee, "Employee", Facade);
            lstSelectEmployee.Items.RemoveAt(0);

            BindEmployeeList();
        }
        private void BindEmployeeList()
        {
            int _membergroupId = 0;
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
            {
                _membergroupId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);
            }

            lsvEmployees.Items.Clear();
            IList<Member> memberList = Facade.GetAllMemberGroupManagerByMemberGroupId(_membergroupId);
            if (memberList != null)
            {
                lsvEmployees.DataSource = memberList;
                lsvEmployees.DataBind();
            }
            else
            {
                lsvEmployees.DataSource = null;
                lsvEmployees.DataBind();
            }
        }
        private void AddEmployeeToHotList()
        {
           
            string EmployeeName = string.Empty;
            int counter = 0;
            int selectCounter = 0;
            lblAddEmployee.Text = "";
            MemberGroupManager memberGroupManager = new MemberGroupManager();
            //0.3
            if (lstSelectEmployee.SelectedValue != "")
            {
                if (lstSelectEmployee.SelectedItem.Text == "Please Select")
                {
                    MiscUtil.ShowMessage(lblAddEmployee, "Please select manager to add", false);
                }
                else
                {
                    foreach (ListItem li in lstSelectEmployee.Items)
                    {
                        if (li.Selected)
                        {
                            memberGroupManager.MemberGroupId = _membergroupId;
                            memberGroupManager.MemberId = Convert.ToInt32(li.Value);
                            memberGroupManager.CreatorId = base.CurrentMember.Id;
                            try
                            {
                                Facade.AddMemberGroupManager(memberGroupManager);
                                counter++;
                            }
                            catch
                            {
                                EmployeeName += MiscUtil.GetMemberNameById(Convert.ToInt32(li.Value), Facade) + " ";
                            }
                            selectCounter++;
                        }
                    }
                    if (selectCounter > 0)
                    {
                        if (counter > 0)
                        {
                            if (counter == selectCounter)
                            {
                                MiscUtil.ShowMessage(lblAddEmployee, "Successfully assigned manager(s) to Hot List.", false);
                            }
                            else
                            {
                                MiscUtil.ShowMessage(lblAddEmployee, "" + counter + " of " + selectCounter.ToString() + " selected manager(s) have been added to the hot list successfully.", false);
                            }
                        }
                        else
                        {
                            MiscUtil.ShowMessage(lblAddEmployee, "Selected manager(s) is already assigned to Hot List.", true);
                        }
                    }
                    else
                    {
                        MiscUtil.ShowMessage(lblAddEmployee, "Please select at least one manager to assign.", true);
                    }
                }
            }
            else
            {
                MiscUtil.ShowMessage(lblAddEmployee, "Please select any one manager to add", true);
            }
            BindEmployeeList();
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]))
            {
                _membergroupId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);
            }
            _mailsetting = MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id);
            if (_membergroupId > 0)
                IsAccessToRemove = Facade.GetMemberGroupAccessByCreatorIdAndGroupId(CurrentMember.Id, _membergroupId);
            if (!IsPostBack)
            {
                PrepareView();
            }
        }
        protected void lsvEmployees_PreRender(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlTableCell thremove = (System.Web.UI.HtmlControls.HtmlTableCell)lsvEmployees.FindControl("thremove");
            if (IsAccessToRemove || IsUserAdmin)
            {
                if (thremove != null)
                    thremove.Visible = true;
                foreach (ListViewDataItem list in lsvEmployees.Items)
                {
                    System.Web.UI.HtmlControls.HtmlTableCell tdremove = (System.Web.UI.HtmlControls.HtmlTableCell)list.FindControl("tdremove");
                    if (tdremove != null)
                        tdremove.Visible = true;
                }
            }
            else
            {
                if (thremove != null)
                    thremove.Visible = true;
                foreach (ListViewDataItem list in lsvEmployees.Items)
                {
                    System.Web.UI.HtmlControls.HtmlTableCell tdremove = (System.Web.UI.HtmlControls.HtmlTableCell)list.FindControl("tdremove");
                    if (tdremove != null)
                        tdremove.Visible = true;
                }
            }

        }
        protected void lsvEmployees_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                Member member = ((ListViewDataItem)e.Item).DataItem as Member;

                if (member != null)
                {
                    Label lblName = (Label)e.Item.FindControl("lblName");
                    LinkButton lblEmail = (LinkButton)e.Item.FindControl("lblEmail");
                    Label lblDateAssigned = (Label)e.Item.FindControl("lblDateAssigned");
                    ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
                    lblName.Text = MiscUtil.GetMemberNameById(member.Id, Facade);
                    if (!_mailsetting)
                        lblEmail.Text = "<a href=MailTo:" + member.PrimaryEmail + ">" + member.PrimaryEmail + "</a>";
                    else
                    {
                        lblEmail.Text = member.PrimaryEmail;
                        SecureUrl Linkurl = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_MEMBER_ID, member.Id.ToString());
                        lblEmail.Attributes.Add("onclick", "window.open('" + Linkurl + "','NewMail','height=626,width=770,toolbar=no,directories=no,status=no,linemenubar=no,scrollbars=no,resizable=no,modal=no'); return false;");
                    }
                    lblDateAssigned.Text = member.CreateDate.ToShortDateString();


                    btnDelete.OnClientClick = "return ConfirmDelete('manager')";
                    if ((IsAccessToRemove && CurrentMember.Id != member.Id) || IsUserAdmin)
                        btnDelete.Visible = true;
                    else
                        btnDelete.Visible = false;
                    btnDelete.CommandArgument = StringHelper.Convert(member.Status);
                }
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            AddEmployeeToHotList();
          //  MiscUtil.PopulateJobPostingByClientIdAndManagerId(ddlAssignedRequisitions, Facade, Convert.ToInt32(ddlClientList.SelectedValue), base.CurrentMember.Id);

        }

        protected void lsvEmployees_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            lblAddEmployee.Text = "";

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteMemberGroupManagerById(id))
                        {
                            BindEmployeeList();
                            MiscUtil.ShowMessage(lblAddEmployee, "Manager has been deleted successfully.", false);
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblAddEmployee, ex.Message, false);
                    }
                }
            }
        }

    }
}