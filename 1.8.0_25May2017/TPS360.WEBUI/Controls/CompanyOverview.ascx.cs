﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CompanyOverview.aspx.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 Apr-27-2009         N.Srilakshmi        Defect id 10211; Changes made in method 'lsvFillName_ItemDataBound'.
    0.2                 May-13-2009         Gopala Swamy        Defect id 10442; Replaced hyperlink with Label control
 *  0.3                 May-15-2009         Sandeesh            Defect id:10440 :Changes made to get the Requisition status from database instead of reading from the  Enum
    0.4                 Apr-16-2010         Ganapati Bhat       Defect id:12608; Modified Code in RenderCompanyDetail()
 *  0.5                 Apr-19-2010         Ganapati Bhat       Defect id:12630; Added Code in RenderCompanyDetail()
------------------------------------------------------------------------------------------------------------------------------------------- 
*/


using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using System.Web.UI.WebControls;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using TPS360.Common;
using System.IO;

namespace TPS360.Web.UI
{
    public partial class cltCompanyOverview : CompanyBaseControl
    {
        #region Variables
        private static bool _mailsetting = false;
        #endregion

        #region Method

        public int ManagerId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_ManagerId"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_ManagerId"] = value;
            }
        }

        public int CompanyId
        {
            get
            {
                return (int)(ViewState[this.ClientID + "_CompanyId"] ?? 0);
            }
            set
            {
                ViewState[this.ClientID + "_CompanyId"] = value;
            }
        }

        private void RenderCompanyDetail()
        {
            Company comapny = base.CurrentCompany;

            if (!comapny.IsNew)
            {
                lblCompanyName.Text = comapny.CompanyName;
                lblAddress1.Text = comapny.Address1;
                lblAddress2.Text = comapny.Address2;
                lblCity.Text = comapny.City;
                lblZip.Text = comapny.Zip;
               // lblEINNumber.Text = comapny.EINNumber;
                //lblAnnualRevenue.Text = comapny.AnnualRevenue;
             //   lblTickerSymbol.Text = comapny.TickerSymbol;
                lblFaxNumber.Text = comapny.FaxNumber;
                lblCompanyInformation.Text = comapny.CompanyInformation;
                lblCompanyName.Text = comapny.CompanyName;
                lblAddress1.Text = comapny.Address1;
                lblAddress2.Text = comapny.Address2;
                lblCity.Text = comapny.City;
                lblZip.Text = comapny.Zip;
               
                //lblAnnualRevenue.Text = comapny.AnnualRevenue;
               // lblNoofEmployees.Text = StringHelper.Convert(comapny.NumberOfEmployee) == "0" ? "" : StringHelper.Convert(comapny.NumberOfEmployee);

           
                if (!string.IsNullOrEmpty(comapny.OfficePhone))
                {
                    if (!string.IsNullOrEmpty(comapny.OfficePhoneExtension))
                    {
                        lblOfficeNumber.Text = comapny.OfficePhone + " " + "(" + comapny.OfficePhoneExtension + ")";
                    }
                    else
                    {
                        lblOfficeNumber.Text = comapny.OfficePhone;
                    }
                }
                else
                {
                    lblOfficeNumber.Text = "";
                }

                lblTollFreeNumber.Text = comapny.TollFreePhone;
                lblCompanyEmail.Text = comapny.PrimaryEmail;
               // lblTickerSymbol.Text = comapny.TickerSymbol;
                lblWebSiteURL.Text = comapny.WebAddress;
                lblFaxNumber.Text = comapny.FaxNumber;

                if (comapny.CountryId > 0)
                    lblCountryId.Text = Facade.GetCountryNameById(comapny.CountryId);
                else
                    lblCountryId.Text = "";

                if (comapny.StateId > 0)
                    lblStateId.Text = Facade.GetStateNameById(comapny.StateId);
                else
                    lblStateId.Text = "";
                GenericLookup lookup = null;
                if (comapny.IndustryType > 0)
                {
                    lookup = Facade.GetGenericLookupById(comapny.IndustryType);
                }

                if (lookup != null)
                {
                    lblIndustryType.Text = lookup.Name;
                }
                else
                {
                    lblIndustryType.Text = "";
                }

                if (comapny.CompanySize > 0)
                {
                    lookup = Facade.GetGenericLookupById(comapny.CompanySize);
                }
                if (lookup != null)
                {
                    lblCompanySize.Text = lookup.Name;
                }
                else
                {
                    lblCompanySize.Text = "";
                }

                if (comapny.OwnerType > 0)
                {
                    lookup = Facade.GetGenericLookupById(comapny.OwnerType);
                }
                if (lookup != null)
                {
                    lblOwnerType.Text = lookup.Name;
                }
                else
                {
                    lblOwnerType.Text = "";
                }
            }
            if (lblCountryId.Text.Trim() == "United States")
            {
                divEINNUmber.Visible = true;
                lblEINNumber.Text = comapny.EINNumber;
            }
            else divEINNUmber.Visible = false;

           
            lsvCompanyInformation.DataSource = lblCompanyInformation.Text; 
            lsvCompanyInformation.DataBind(); 
        }

        private void RenderCompanyContact()
        {
            IList<CompanyContact> companyContactList = Facade.GetAllCompanyContactByComapnyId(base.CurrentCompanyId);

            lsvCompanyContact.DataSource = companyContactList;
            lsvCompanyContact.DataBind();
        }

        private void RenderCompanyFile()
        {
            IList<CompanyDocument> companyDocumentList = Facade.GetAllCompanyDocumentByCompanyId(base.CurrentCompanyId);
            lsvFileName.DataSource = companyDocumentList;
            lsvFileName.DataBind();
        }

        private void RenderJobPosting()
        {
            lsvJobPosting.DataSourceID = "odsJobPostingList";
            this.lsvJobPosting.DataBind();
        }

        private string GetDocumentLink(string strFileName, string strDocumenType, int ComId)
        {

            string strFilePath = MiscUtil.GetCompanyDocumentPath(this.Page, ComId, strFileName, strDocumenType, true);

            if (strFilePath != strFileName)
            {
                return "<a href='" + strFilePath + "' target='_blank'>" + strFileName + "</a>"; // 0.1
            }
            else
            {
                return strFileName + "(File Not Exist)";
            }
        }
        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
         {
            _mailsetting = MiscUtil.IsValidMailSetting(Facade, CurrentMember.Id);
            if (!IsPostBack)
            {
                RenderCompanyDetail();
                RenderCompanyContact();
                RenderCompanyFile();
                RenderJobPosting();
                PrepareView();
                if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
                {
                    divOwnerType.Visible = false;
                }
            }
            string pagesize = "";
            pagesize = (Request.Cookies["CompanyOverviewJobPostingRowPerPage"] == null ? "" : Request.Cookies["CompanyOverviewJobPostingRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting .FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null) pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
            }

            string name = "";
            if (!IsPostBack)
            {
                switch (CurrentCompanyStatus)
                {
                    case CompanyStatus.Client:
                       
                        name = "Account";
                        break;
                    case CompanyStatus.Department:
                        name = "BU";
                        break;
                    case CompanyStatus.Vendor:
                        lsvJobPosting.Visible = false;
                       
                        liRequisition.Visible = false;
                        name = "Vendor";
                        break;
                }
                lblCompanyNameLabel.Text = name + " Name";
                lblCompanyEmailLabel.Text = name + " Email";
                lblCompanySizeLabel.Text = name +" Size";
                lblDescription.Text = "About the " + name;
            }
        }

        private void PrepareView()
        {
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                //lblDescription.Text = "About the BU";
              //  lblCompanyNameLabel.Text = "BU Name";
                //lblcompany_Profile.Text = "Department Profile";
              // lblCompanyEmailLabel.Text = "BU Email";
              //  lblCompanySizeLabel.Text = "BU Size";
                //lblCompanyDescriptionLabel.Text = "Department Description";
                //lblCompanyContactLabel.Text = "Department Contacts";
            }
        }
        #endregion

        #region ListView Events
        protected void lsvCompanyContact_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

        }
        protected void lsvCompanyContact_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                CompanyContact companyContact = ((ListViewDataItem)e.Item).DataItem as CompanyContact;
                if (companyContact != null)
                {
                    LinkButton lblName = (LinkButton)e.Item.FindControl("lblName");
                    SecureUrl Modalurl = UrlHelper.BuildSecureUrl("../Modals/CompanyContact.aspx", string.Empty, UrlConstants.PARAM_CONTACT_ID, companyContact.Id.ToString());
                    lblName.OnClientClick = "javascript:EditModal('" + Modalurl.ToString() + "','675px','450px'); return false;";
                    Label lblTitle = (Label)e.Item.FindControl("lblTitle");
                    LinkButton  lnkEmail = (LinkButton )e.Item.FindControl("lnkEmail");
                    Label lblDirectNumber = (Label)e.Item.FindControl("lblDirectNumber");
                    Label lblMobile = (Label)e.Item.FindControl("lblMobile");
                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    lblName.Text = companyContact.FirstName + " " + companyContact.LastName;
                    lblName.CommandArgument = companyContact.Id.ToString();
                    lblTitle.Text = companyContact.Title;
                    lblMobile.Text = companyContact.MobilePhone;
                    lblCity.Text = companyContact.City;
                    if (!string.IsNullOrEmpty(companyContact.OfficePhone ))
                    {
                        if (!string.IsNullOrEmpty(companyContact.OfficePhoneExtension ))
                            lblDirectNumber.Text = companyContact.OfficePhone  + "  x" +  companyContact.OfficePhoneExtension ;
                        else lblDirectNumber.Text = companyContact.OfficePhone;
                    }
                    if (!string.IsNullOrEmpty(companyContact.Email))
                    {
                            if (!_mailsetting)
                                lnkEmail.Text = "<a href=MailTo:" + companyContact.Email + ">" + companyContact.Email + "</a>";
                            else
                            {
                                lnkEmail.Text = companyContact.Email;
                                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + UrlConstants.Dashboard.DIR + UrlConstants.Dashboard.NEWMAIL, string.Empty, UrlConstants.PARAM_MEMBER_ID, companyContact .MemberId .ToString (), UrlConstants.PARAM_COMPANY_ID, companyContact .CompanyId .ToString (), UrlConstants.PARAM_EMAIL_TYPE, "Company");
                                lnkEmail.Attributes.Add("onclick", "window.open('" + url + "','NewMail','height=626,width=770,toolbar=no,directories=no,status=no,linemenubar=no,scrollbars=1,resizable=no,modal=no'); return false;");
                            }
                    }
                }
            }
        }

        protected void lsvFillName_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            string link = string.Empty;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                CompanyDocument companyDocument = ((ListViewDataItem)e.Item).DataItem as CompanyDocument;

                if (companyDocument != null)
                {
                    GenericLookup docType = Facade.GetGenericLookupById(companyDocument.DocumentType);
                    Label lblFileName = ((Label)e.Item.FindControl("lblFileName"));
                    Label lblDocumentTitle = (Label)e.Item.FindControl("lblDocumentTitle");
                    lblFileName.Text = GetDocumentLink(companyDocument.FileName, docType.Name,companyDocument .CompanyId );
                    lblDocumentTitle.Text = companyDocument.Title;
                }
            }
        }
       
        protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
                if (jobPosting != null)
                {
                    Label lblPostedDate = (Label)e.Item.FindControl("lblPostedDate");
                    HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    lblPostedDate.Text = jobPosting.PostedDate.ToShortDateString();                    
                    lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                    lnkJobTitle.Text = jobPosting.JobTitle;
                    lblCity.Text = jobPosting.City + ((!string.IsNullOrEmpty(jobPosting.City) && jobPosting.StateId > 0) ? ", " : string.Empty) + MiscUtil.GetStateNameById(jobPosting.StateId, Facade);
                }
            }
        }

        protected void lsvJobPosting_PreRender(object sender, EventArgs e)
        {
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "CompanyOverviewJobPostingRowPerPage";
            }
        }

        #endregion

        #region ObjectDataSource Event
        protected void odsJobPostingList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["companyId"] = base.CurrentCompanyId.ToString();
        }
        protected void odsJobPostingList_Init(object sender, EventArgs e)
        {
            int JobStatusid = 0;
            //IList<GenericLookup> RequisitionStatusList = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.RequisitionStatus, JobStatus.Open.ToString());
            //if (RequisitionStatusList != null)
            //{
            //    JobStatusid = RequisitionStatusList[0].Id;
            //}
            odsJobPostingList.SelectParameters["JobStatus"].DefaultValue = JobStatusid.ToString();
        }
        #endregion       
    }
}