﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LookUpList.ascx.cs" Inherits="TPS360.Web.UI.ControlLookUpList" %>
<div class="TabPanelHeader">
    List of Lookup Values
</div>
<asp:UpdatePanel ID="updPanelTableAlias" runat="server">
    <ContentTemplate>
        <div class="BoxContainer">
            <div>
                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                <div style="text-align: right;">
                    <asp:HyperLink ID="lnkAddLookup" CssClass="NormalLink" ToolTip="Add Lookup" runat="server"></asp:HyperLink>
                </div>
            </div>
            <asp:Panel ID="ccc" runat="server">
                <table id="tlbTemplate" width="100%" class="Grid" cellspacing="0" border="0">
                    <tr class="head">
                        <th style="width: 50%; white-space: nowrap;">
                            Name
                        </th>
                        <%-- <th id="thCreateDate" runat="server" style="width: 20%; text-align: center; white-space: nowrap;">
                                                Create Date
                                            </th>
                                            <th id="thUpdateDate" runat="server" style="width: 20%; text-align: center; white-space: nowrap;">
                                                Update Date
                                            </th>--%>
                        <th style="width: 10%; text-align: center; white-space: nowrap;">
                            Action
                        </th>
                    </tr>
                    <asp:Repeater ID="rptLookUpList" runat="server" OnItemDataBound="rptLookUpList_ItemDataBound">
                        <ItemTemplate>
                            <tr>
                                <td colspan="2" style="text-align: left; white-space: nowrap; cursor: pointer;">
                                    <asp:CollapsiblePanelExtender TargetControlID="pnlSubList" ID="cpeLookUpList" runat="server"
                                        ExpandControlID="pnlSubListHeader" CollapseControlID="pnlSubListHeader" Collapsed="True"
                                        ImageControlID="imgShowHide" CollapsedImage="~/Images/expand-plus.png" ExpandedImage="~/Images/collapse-minus.png"
                                        SuppressPostBack="false">
                                    </asp:CollapsiblePanelExtender>
                                    <asp:Panel ID="pnlSubListHeader" runat="server">
                                        &nbsp;&nbsp;<asp:Image ID="imgShowHide" ImageUrl="~/Images/expand_all.png" runat="server" />&nbsp;&nbsp;
                                        <strong>
                                            <asp:HyperLink ID="lnkTableName" runat="server"></asp:HyperLink></strong>&nbsp;
                                        <asp:Label ID="lblSubListTotal" runat="server" />
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Panel ID="pnlSubList" runat="server">
                                        <asp:Repeater ID="rptSubLookUpList" runat="server" OnItemCommand="rptSubLookUpList_ItemCommand"
                                            OnItemDataBound="rptSubLookUpList_ItemDataBound">
                                            <HeaderTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr style="border: 0px;">
                                                    <td align="left" style="width: 50%;">
                                                        <asp:Label ID="lblName" runat="server" />
                                                    </td>
                                                    <%-- <td id="tdCreatedate" runat="server" style="text-align: left; white-space: nowrap; width: 20%;">
                                                                            <asp:Label ID="lblCreateDate" runat="server" />
                                                                        </td>
                                                                        <td id="tdUpdatedate" runat="server" style="text-align: left; white-space: nowrap; width: 20%;">
                                                                            <asp:Label ID="lblUpdateDate" runat="server" />
                                                                        </td>--%>
                                                    <td style="text-align: center; width: 8%;">
                                                        <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" CommandName="EditItem"
                                                            CausesValidation="false"></asp:ImageButton>
                                                        <asp:ImageButton ID="btnDelete" SkinID="sknDeleteButton" runat="server" CommandName="DeleteItem"
                                                            CausesValidation="false" Visible="False"></asp:ImageButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </asp:Panel>
            <br />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
