﻿using System;
using System.Web.Security;
using TPS360.Common.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Common;
using System.Text.RegularExpressions;
using System.Web;
using TPS360.Providers;
using System.Collections;
using System.Linq;
namespace TPS360.Web.UI
{
    public partial class RequisitionList : BaseControl
    {
        #region Member Variables
        IList<HiringMatrixLevels> hiringLevels = null;
        bool ShowAction = false;
        public int HeaderAdd = 0;
        ArrayList _permittedMenuIdList;
        public string RequisitionType = "Master";
        public static bool IsAccss = false;

        public DropDownList ddlJobStatusCommon
        {
            set;
            get;
        }
        #endregion

        #region Methods
        private void BindList()
        {
            this.lsvJobPosting.DataBind();

        }
        private void MemberPrivilege()
        {
            //SiteMapNodeCollection permittedNodeList = new SiteMapNodeCollection();

            //SqlSiteMapProvider provider = SiteMap.Providers["SqlSiteMap"] as SqlSiteMapProvider;
            //SiteMapNode root = provider.FindSiteMapNodeFromKey(StringHelper.Convert((int)SiteMapType.Requisition));

            //if (root != null)
            //{
            //    SiteMapNodeCollection nodeList = root.ChildNodes;

            //    if (nodeList != null && nodeList.Count > 0)
            //    {
            //        foreach (SiteMapNode node in nodeList)
            //        {
            //            if (IsInPermitedMenuList(Convert.ToInt32(node["Id"])))
            //            {
            //                if (node.Title == "New Requisition")
            //                {
            //                    lnkAddJobPosting.Visible = true;
            //                }
            //            }
            //        }
            //    }
            //}
        }
        private bool IsInPermitedMenuList(int menuId)
        {
            if (_permittedMenuIdList == null)
            {
                if (CurrentMember != null) _permittedMenuIdList = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
            }

            if ((_permittedMenuIdList != null) && (_permittedMenuIdList.Count > 0))
            {
                if (_permittedMenuIdList.Contains(menuId))
                {
                    return true;
                }
            }

            return false;
        }

        private void SelectJobStatusList()
        {
            if (Request.Cookies["RequisitionStatus"] != null)
            {
                ddlJobStatus.SelectedItems = Request.Cookies["RequisitionStatus"].Value;
            }
            else
            {
                foreach (ListItem item in ddlJobStatus.ListItem.Items)
                {
                    item.Selected = false;
                }
            }
        }

        private void PrepareView()
        {


            MiscUtil.PopulateRequistionStatus(ddlJobStatus.ListItem, Facade);
            SelectJobStatusList();
            // if (Request.Cookies["RequisitionStatus"] != null)
            //   ddlJobStatus.ListItem .SelectedValue = Request.Cookies["RequisitionStatus"].Value;
            //else
            //  ControlHelper.SelectListByText(ddlJobStatus.ListItem , "Open");

            MiscUtil.PopulateMemberListByRole(ddlReqCreator, ContextConstants.ROLE_EMPLOYEE, Facade);
            MiscUtil.PopulateMemberListByRole(ddlEmployee, ContextConstants.ROLE_EMPLOYEE, Facade);
            int companyStatus = (int)MiscUtil.GetCompanyStatusFromDefaultSiteSetting(SiteSetting);
            MiscUtil.PopulateClients(ddlEndClient.ListItem, companyStatus, Facade);
            //ddlEndClient.Items[0].Value = "0";
            if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
            {
                lblByEndClientsHeader.Text = "BU";
                //ddlEndClient.Items[0].Text = "Select BU";
            }
            else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
            {
                lblByEndClientsHeader.Text = "Account";
                //ddlEndClient.Items[0].Text = "Select Account";
            }
            else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Vendor)
            {
                lblByEndClientsHeader.Text = "Vendor";
                //ddlEndClient.Items[0].Text = "Select Vendor";
            }

        }


        #endregion

        #region Events

        #region Page Events
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string pagesize = "";
            if (RequisitionType == "My")
            {
                pagesize = (Request.Cookies["MyRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MyRequisitionListRowPerPage"].Value); ;
            }
            else
            {
                pagesize = (Request.Cookies["MasterRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MasterRequisitionListRowPerPage"].Value); ;
            }
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }

            this.lsvJobPosting.DataBind();
            uclCountryState.SelectedCountryId = uclCountryState.SelectedCountryId;
            uclCountryState.SelectedStateId = uclCountryState.SelectedStateId;
            AddHiringMatrixColumns();

            Response.Cookies["RequisitionStatus"].Value = ddlJobStatus.SelectedItems;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtJobTitle.Text = "";
            txtReqCode.Text = "";
            foreach (ListItem item in ddlJobStatus.ListItem.Items)
            {
                item.Selected = false;
            }
            Response.Cookies["RequisitionStatus"].Value = ddlJobStatus.SelectedItems;
            dtPstedDate.ClearRange();
            ddlReqCreator.SelectedIndex = 0;
            ddlEmployee.SelectedIndex = 0;
            txtCity.Text = "";
            uclCountryState.SelectedCountryId = 0;
            uclCountryState.SelectedStateId = 0;
            //ddlEndClient.SelectedIndex = 0;
            foreach (ListItem item in ddlEndClient.ListItem.Items)
            {
                item.Selected = false;
            }
            lsvJobPosting.DataBind();
            string pagesize = "";
            if (RequisitionType == "My")
            {
                pagesize = (Request.Cookies["MyRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MyRequisitionListRowPerPage"].Value); ;
            }
            else
            {
                pagesize = (Request.Cookies["MasterRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MasterRequisitionListRowPerPage"].Value); ;
            }
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                }
            }
            AddHiringMatrixColumns();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ddlJobStatusCommon = new DropDownList();
            MiscUtil.PopulateRequistionStatus(ddlJobStatusCommon, Facade);
            if (hdnDoPostPack.Value == "1")
            {
                lsvJobPosting.DataBind();
                hdnDoPostPack.Value = "0";
                return;
            }
            string testUrl = HttpContext.Current.Request.Url.Authority;
            ddlReqCreator.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlEndClient.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            ddlEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title");
            this.Page.MaintainScrollPositionOnPostBack = true;
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("MasterRequisitionList.aspx")) { RequisitionType = "Master"; }
            else { RequisitionType = "My"; }
            //  AjaxControlToolkit.Utility.SetFocusOnLoad(lnkAddJobPosting);
            if (CurrentMember != null)
            {
                ArrayList Privilege = Facade.GetAllMemberPrivilegeIdsByMemberId(CurrentMember.Id);
                if (Privilege.Contains(56))
                    IsAccss = true;
                else IsAccss = false;
            }

            if (!IsPostBack)
            {
                PrepareView();

                string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];

                // ControlHelper.SetHyperLink(lnkAddJobPosting, UrlConstants.Requisition.JOB_POSTING_EDITOR_PAGE, string.Empty, "[ Add Requisition ]");

                if (!StringHelper.IsBlank(message))
                {
                    MiscUtil.ShowMessage(lblMessage, message, false);
                }
                hdnSortColumn.Value = "btnPostedDate";
                hdnSortOrder.Value = "DESC";
                PlaceUpDownArrow();
            }
            string pagesize = "";
            if (RequisitionType == "My")
            {
                pagesize = (Request.Cookies["MyRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MyRequisitionListRowPerPage"].Value); ;
            }
            else
            {
                pagesize = (Request.Cookies["MasterRequisitionListRowPerPage"] == null ? "" : Request.Cookies["MasterRequisitionListRowPerPage"].Value); ;
            }
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                }
            }
            if (Facade.GetCustomRoleNameByMemberId(CurrentMember.Id) == ContextConstants.ROLE_DEPARTMENT_CONTACT)
            {
                divBU.Visible = false;
            }
            AddHiringMatrixColumns();
            MemberPrivilege();
        }

        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvJobPosting.FindControl(hdnSortColumn.Value);
                System.Web.UI.HtmlControls.HtmlTableCell im = (System.Web.UI.HtmlControls.HtmlTableCell)lnk.Parent;
                im.EnableViewState = false;
                if (lnk.CommandArgument.Contains("[1]") || lnk.CommandArgument.Contains("[JMC].[ManagersCount]"))
                {
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Descending" : "Ascending"));
                }
                else
                    im.Attributes.Add("class", (hdnSortOrder.Value == "ASC" ? "Ascending" : "Descending"));
            }
            catch
            {
            }

        }
        #endregion

        #region ListView Events

        private void AddHiringMatrixColumns()
        {
            System.Web.UI.HtmlControls.HtmlTableCell thLevels = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("thLevels");
            if (thLevels != null)
            {
                //thLevels.Controls.Clear();
                //IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
                //hiringLevels = hiringMatrixLevels;
                //int ColLocation = 0;

                //if (hiringMatrixLevels != null)
                //{
                //    if (hiringMatrixLevels.Count > 0)
                //    {
                //        foreach (HiringMatrixLevels levels in hiringMatrixLevels)
                //        {
                //            System.Web.UI.HtmlControls.HtmlTableCell col = new System.Web.UI.HtmlControls.HtmlTableCell("TH");
                //            LinkButton lnkLevel = new LinkButton();
                //            lnkLevel.Text = levels.Name.Trim();
                //            lnkLevel.CommandName = "Sort";
                //            lnkLevel.ID = "thLevel" + ColLocation;
                //            LinkButton lnktemp = (LinkButton)lsvJobPosting.FindControl(lnkLevel.ID);
                //            if (lnktemp != null) lsvJobPosting.Controls.Remove(lnktemp);
                //            lnkLevel.EnableViewState = false;
                //            lnkLevel.CommandArgument = "Level[" + levels.Id + "]";
                //            lnkLevel.EnableViewState = false;
                //            if (levels.SortingOrder == 0)
                //                thLevels.Controls.AddAt(ColLocation, lnkLevel);
                //            else
                //            {
                //                col.Controls.Add(lnkLevel);
                //                thLevels.Controls.AddAt(ColLocation, col);
                //            }
                //            ColLocation++;
                //        }
                //    }
                //}
                //System.Web.UI.HtmlControls.HtmlTableCell tdPager = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("tdPager");
                //tdPager.ColSpan = Convert.ToInt32(tdPager.ColSpan) + hiringMatrixLevels.Count;
            }
        }
        public bool isMemberinHinringTeam(int memberid, int jobpostingid)
        {
            bool returnValue = false;

            JobPostingHiringTeam team = Facade.GetJobPostingHiringTeamByMemberId(memberid, jobpostingid);

            if (team != null)
            {
                returnValue = true;
            }
            else
            {
                returnValue = false;
            }

            return returnValue;
        }
        protected void lsvJobPosting_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            bool _isAdmin = base.IsUserAdmin;
            bool _isHiringManager = base.IsHiringManager;
            bool isAssingned = false;
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))
            {
                JobPosting jobPosting = ((ListViewDataItem)e.Item).DataItem as JobPosting;
                int ColLocation = 0;
                if (jobPosting != null)
                {
                    JobPostingHiringTeam team = Facade.GetJobPostingHiringTeamByMemberId(base.CurrentMember.Id, jobPosting.Id);
                    if (team != null) isAssingned = true;
                    ColLocation = 0;
                    System.Web.UI.HtmlControls.HtmlTableCell tdLevels = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdLevels");
                    int i = 0;
                    LinkButton lnkTeamCount = (LinkButton)e.Item.FindControl("lnkTeamCount");
                    lnkTeamCount.Text = jobPosting.NoOfAssignedManagers.ToString();
                    lnkTeamCount.CommandArgument = jobPosting.Id.ToString();

                    lnkTeamCount.OnClientClick = "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobPostingTeam.aspx?JID=" + jobPosting.Id + "','700px','570px'); return false;";
                    System.Web.UI.HtmlControls.HtmlTableCell thReqCode = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thReqCode");
                    System.Web.UI.HtmlControls.HtmlTableCell tdJobCode = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdJobCode");

                    HyperLink tdLevel = (HyperLink)e.Item.FindControl("tdLevel");
                    Hiring_Levels lev = jobPosting.HiringMatrixLevels[0];
                    tdLevel.Text = lev.CandidateCount.ToString();
                    // tdLevel.Target = "_blank";
                    HiringMatrixLevels l = Facade.HiringMatrixLevel_GetInitialLevel();// (lev.HiringMatrixLevelID);
                    HyperLink hlkOpenHiringMatrix = (HyperLink)e.Item.FindControl("hlkOpenHiringMatrix");

                    if (_isAdmin || jobPosting.CreatorId == base.CurrentMember.Id || isAssingned || _isHiringManager)
                    {
                        ControlHelper.SetHyperLink(tdLevel, UrlConstants.ATS.ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX, string.Empty, lev.CandidateCount.ToString(), UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id), UrlConstants.PARAM_TAB, l.Id.ToString());
                        SecureUrl urlh = UrlHelper.BuildSecureUrl("../" + UrlConstants.ATS.ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX.Replace("~/", ""), string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id), UrlConstants.PARAM_TAB, l.Id.ToString());
                        hlkOpenHiringMatrix.Attributes.Add("href", urlh.ToString());

                    }
                    else tdLevel.Enabled = false;// lnkLevel.Enabled = false;


                    DropDownList ddlJobStatus = (DropDownList)e.Item.FindControl("ddlJobStatus");
                    Label lblJobStatus = (Label)e.Item.FindControl("lblJobStatus");
                    if ((jobPosting.AllowRecruitersToChangeStatus == true && isAssingned) || jobPosting.CreatorId == base.CurrentMember.Id)
                    {

                        ddlJobStatus.DataValueField = ddlJobStatusCommon.DataValueField;
                        ddlJobStatus.DataTextField = ddlJobStatusCommon.DataTextField;
                        ddlJobStatus.DataSource = ddlJobStatusCommon.DataSource;
                        ddlJobStatus.DataBind();
                        ControlHelper.SelectListByValue(ddlJobStatus, jobPosting.JobStatus.ToString());

                        ddlJobStatus.Attributes.Add("onchange", "javascript:RequisitionJobStatusChanged('" + ddlJobStatus.ClientID + "','" + jobPosting.Id + "','" + base.CurrentMember.Id + "','" + lblMessage.ClientID + "','" + jobPosting.JobTitle + "')");
                        lblJobStatus.Visible = false;
                        ddlJobStatus.Visible = true;
                    }
                    else
                    {
                        ddlJobStatus.Visible = false;
                        lblJobStatus.Visible = true;
                    }
                    Label lblPostedDate = (Label)e.Item.FindControl("lblPostedDate");
                    Label lblRequisitionDate = (Label)e.Item.FindControl("lblRequisitionDate");
                    Label lblJobCode = (Label)e.Item.FindControl("lblJobCode");
                    HyperLink lnkJobTitle = (HyperLink)e.Item.FindControl("lnkJobTitle");
                    Label lblCity = (Label)e.Item.FindControl("lblCity");
                    Label lblClient = (Label)e.Item.FindControl("lblClient");       //0.4
                    Label lblNoOfOpenings = (Label)e.Item.FindControl("lblNoOfOpenings");
                    Label lblClientJobId = (Label)e.Item.FindControl("lblClientJobId");
                    LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");
                    // LinkButton btnCreateNewFromExisting = (LinkButton)e.Item.FindControl("btnCreateNewFromExisting");
                    LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete");
                    LinkButton lnkEmployeeReferralLink = (LinkButton)e.Item.FindControl("lnkEmployeeReferralLink");


                    lblJobCode.Text = jobPosting.JobPostingCode.ToString();

                     //CODE MODIFY BY PRAVIN ON 22/June/2016*****          
                    if (jobPosting.StartDate != null)
                    {
                        string date = jobPosting.StartDate.ToString().Split('/')[0] + "/" + jobPosting.StartDate.ToString().Split('/')[1] + "/" + jobPosting.StartDate.ToString().Split('/')[2];
                        lblPostedDate.Text = date;
                    }
                    //lblPostedDate.Text = jobPosting.PostedDate.ToShortDateString();
                    //**********************END***************************

                    lblRequisitionDate.Text = jobPosting.UpdateDate.ToShortDateString();
                    //ControlHelper.SetHyperLink(lnkJobTitle, UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE, string.Empty, MiscUtil.RemoveScript(jobPosting.JobTitle), UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id));
                    lnkJobTitle.Text = jobPosting.JobTitle;
                    lnkJobTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "','700px','570px'); return false;");


                    lblCity.Text = jobPosting.City + ((!string.IsNullOrEmpty(jobPosting.City) && jobPosting.StateId > 0) ? ", " : string.Empty) + MiscUtil.GetStateNameById(jobPosting.StateId, Facade);
                    GenericLookup _RequisitionStatusLookup = Facade.GetGenericLookupById(jobPosting.JobStatus);
                    if (_RequisitionStatusLookup != null) lblJobStatus.Text = _RequisitionStatusLookup.Name;
                    lblClient.Text = "";
                    {
                        if (jobPosting.ClientId > 0)
                        {
                            Company Client = Facade.GetCompanyById(jobPosting.ClientId);
                            if (Client != null)
                            {
                                lblClient.Text = Client.CompanyName;
                                lblClient.ToolTip = Client.CompanyName;
                            }
                        }
                    }
                    System.Web.UI.HtmlControls.HtmlTableCell tdAction = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAction");
                    System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thAction");

                   // if (!ShowAction) ShowAction = _isAdmin || jobPosting.CreatorId == CurrentMember.Id || _isHiringManager || isMemberinHinringTeam(CurrentMember.Id, jobPosting.Id);
                    //else
                    {
                        thAction.Visible = true;
                        tdAction.Visible = true;
                    }
                    if (!_isAdmin && jobPosting.CreatorId != CurrentMember.Id && !_isHiringManager)
                    {
                        btnDelete.Visible = false;
                        btnEdit.Visible = false;
                        //                        btnCreateNewFromExisting.Visible = false;

                    }
                    //if (_isHiringManager)
                    //{
                    //    ShowAction = !_isHiringManager;
                    //    thAction.Visible = false;
                    //    tdAction.Visible = false;
                    //}

                    Label lblAssignedManager = (Label)e.Item.FindControl("lblAssignedManager");
                    Member MemberCreated = null;
                    if (jobPosting.CreatorId != 0) MemberCreated = Facade.GetMemberById(jobPosting.CreatorId);                  //0.5
                    if (MemberCreated != null)
                        lblAssignedManager.Text = MemberCreated.FirstName + " " + MemberCreated.LastName;   //0.5
                    try
                    {
                        System.Web.UI.HtmlControls.HtmlTableCell thAssigningManager = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("thAssigningManager");
                        System.Web.UI.HtmlControls.HtmlTableCell tdAssigningManager = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAssigningManager");
                        System.Web.UI.HtmlControls.HtmlTableCell thClient = (System.Web.UI.HtmlControls.HtmlTableCell)this.lsvJobPosting.FindControl("thClient");
                        System.Web.UI.HtmlControls.HtmlTableCell tdClient = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdClient");
                        if (RequisitionType == "Master")
                        {
                            thAssigningManager.Visible = false;
                            tdAssigningManager.Visible = false;
                            thClient.Visible = true;
                            tdClient.Visible = true;
                        }
                        else
                        {
                            thAssigningManager.Visible = true;
                            tdAssigningManager.Visible = true;
                            thClient.Visible = true;
                            tdClient.Visible = true;
                        }
                    }
                    catch
                    {
                    }
                    btnDelete.OnClientClick = "return ConfirmDelete('Requisition')";
                    btnEdit.CommandArgument = btnDelete.CommandArgument = StringHelper.Convert(jobPosting.Id);

                    if (jobPosting.ShowInEmployeeReferralPortal)
                        lnkEmployeeReferralLink.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/ReferralLink.aspx?JID=" + jobPosting.Id + "&FromPage=JobDetail" + "','680px','360px'); return false;");
                    else
                    {
                        System.Web.UI.HtmlControls.HtmlContainerControl liDivider = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Item.FindControl("liDivider");
                        if (liDivider != null) liDivider.Visible = false;
                        lnkEmployeeReferralLink.Visible = false;
                    }

                    if (!IsAccss)
                    {
                        btnEdit.Visible = false;
                        //                        btnCreateNewFromExisting.Visible = false;
                    }
                    lblNoOfOpenings.Text = jobPosting.NoOfOpenings.ToString();

                    lblClientJobId.Text = jobPosting.ClientJobId;

                    if (ApplicationSource == ApplicationSource.GenisysApplication)
                    {
                        //code modify by pravin khot on 22/June/2016
                        //thReqCode.Visible = false;
                        //tdJobCode.Visible = false;
                        thReqCode.Visible =true ;
                        tdJobCode.Visible = true ;

                        System.Web.UI.HtmlControls.HtmlTableCell thClientJobId = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thClientJobId");
                        System.Web.UI.HtmlControls.HtmlTableCell tdClientJobId = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdClientJobId");

                        thClientJobId.Visible = false;
                        tdClientJobId.Visible = false;
                        //***************END*************************
                    }
                }
            }
        }
        protected void lsvJobPosting_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Value == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Value == "ASC") hdnSortOrder.Value = "DESC";
                        else hdnSortOrder.Value = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Value = lnkbutton.ID;
                        hdnSortOrder.Value = "ASC";
                    }
                }
            }
            catch
            {
            }
            int id;

            int.TryParse(e.CommandArgument.ToString(), out id);

            if (id > 0)
            {
                if (string.Equals(e.CommandName, "EditItem"))
                {
                    Session.Add("List", 3);
                    Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_EDITOR_PAGE, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(id));
                }
                else if (string.Equals(e.CommandName, "Managers"))
                {
                    ////uclTemplateManagers.ModalTitle = uclManagers .setCurrentJobPosting (Convert .ToInt32 (e.CommandArgument )) + " - Assigned Team";
                    //uclManagers.JobPostingId = Convert.ToInt32(e.CommandArgument);
                    //uclTemplateManagers.ModalTitle = uclManagers.Job_Posting.JobTitle + " - Assigned Team";
                    //mpeManagers.Show();
                    //System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thAction");
                    //ShowAction = thAction.Visible;

                }
                else if (string.Equals(e.CommandName, "CreateNewFromExisting"))
                {
                    try
                    {
                        JobPosting jobPosting = new JobPosting();
                        int intReqCount = Facade.GetLastRequisitionCodeByMemberId(CurrentMember.Id);
                        string strMemberJobCount = "";
                        if (strMemberJobCount.Length < 4)
                            strMemberJobCount = (intReqCount + 1).ToString("#0000");
                        else strMemberJobCount = ((intReqCount + 1)).ToString();

                        string jobPostingCode = (base.CurrentMember.LastName.IsNullOrEmpty() ? (Convert.ToString(base.CurrentMember.FirstName.Substring(0, 2))) : ((Convert.ToString(base.CurrentMember.FirstName.Substring(0, 1)) + Convert.ToString(base.CurrentMember.LastName.Substring(0, 1))))).ToUpper() + strMemberJobCount;
                        jobPosting = Facade.CreateJobPostingFromExistingJob(id, jobPostingCode, base.CurrentMember.Id);//0.1 and 1.2
                        IList<JobPostingHiringTeam> JPHTT = Facade.GetAllJobPostingHiringTeamByJobPostingId(id);
                        bool IsCurrentMember = false;
                        if (JPHTT != null)
                        {
                            for (int i = 0; i < JPHTT.Count; i++)
                            {
                                if (JPHTT[i].MemberId == base.CurrentMember.Id)
                                {
                                    IsCurrentMember = true;
                                }
                            }
                            for (int i = 0; i < JPHTT.Count; i++)
                            {
                                JobPostingHiringTeam JPHT = new JobPostingHiringTeam();
                                JPHT.MemberId = JPHTT[i].MemberId;
                                JPHT.JobPostingId = jobPosting.Id;
                                JPHT.UpdateDate = System.DateTime.Now;
                                JPHT.UpdatorId = base.CurrentMember.Id;
                                JPHT.CreateDate = System.DateTime.Now;
                                JPHT.EmployeeType = "Internal";
                                JPHT.CreatorId = base.CurrentMember.Id;
                                Facade.AddJobPostingHiringTeam(JPHT);
                                MemberJobCart memberJobCart = new MemberJobCart();
                                memberJobCart.MemberId = base.CurrentMember.Id;
                                memberJobCart.JobPostingId = jobPosting.Id;
                                memberJobCart.ApplyDate = DateTime.Now;
                                Facade.AddMemberJobCart(memberJobCart);
                                Facade.UpdateMemberRequisitionCount(base.CurrentMember.Id, intReqCount + 1);
                                if (!IsCurrentMember)
                                {
                                    JobPostingHiringTeam JPHT1 = new JobPostingHiringTeam();
                                    JPHT1.MemberId = base.CurrentMember.Id;
                                    JPHT1.JobPostingId = jobPosting.Id;
                                    JPHT1.UpdateDate = System.DateTime.Now;
                                    JPHT1.UpdatorId = base.CurrentMember.Id;
                                    JPHT1.CreateDate = System.DateTime.Now;
                                    JPHT1.EmployeeType = "Internal";
                                    JPHT1.CreatorId = base.CurrentMember.Id;
                                    Facade.AddJobPostingHiringTeam(JPHT1);

                                    MemberJobCart memberJobCart1 = new MemberJobCart();
                                    memberJobCart1.MemberId = base.CurrentMember.Id;
                                    memberJobCart1.JobPostingId = jobPosting.Id;
                                    memberJobCart1.ApplyDate = DateTime.Now;
                                    Facade.AddMemberJobCart(memberJobCart1);
                                    Facade.UpdateMemberRequisitionCount(base.CurrentMember.Id, intReqCount + 1);
                                }
                            }
                        }
                        if (jobPosting != null)
                            Helper.Url.Redirect(UrlConstants.Requisition.JOB_POSTING_EDITOR_PAGE, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(jobPosting.Id));
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
                else if (string.Equals(e.CommandName, "DeleteItem"))
                {
                    try
                    {
                        if (Facade.DeleteJobPostingById(id, base.CurrentMember.Id))
                        {
                            BindList();
                            MiscUtil.ShowMessage(lblMessage, "Requisition has been successfully deleted.", false);
                        }
                        Facade.DeleteMemberJobCartById(base.CurrentMember.Id);
                    }
                    catch (ArgumentException ex)
                    {
                        MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                    }
                }
            }
        }

        protected void lsvJobPosting_PreRender(object sender, EventArgs e)
        {
            try
            {
                ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvJobPosting.FindControl("pagerControl");
                if (PagerControl != null)
                {
                    DataPager pager = (DataPager)PagerControl.FindControl("pager");
                    if (pager != null)
                    {
                        DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                        if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                    }

                    HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                    if (hdnRowPerPageName != null)
                    {
                        if (RequisitionType == "My") hdnRowPerPageName.Value = "MyRequisitionListRowPerPage";
                        else hdnRowPerPageName.Value = "MasterRequisitionListRowPerPage";
                    }
                    //if (!ShowAction)
                    //{
                    //    foreach (ListViewItem item in lsvJobPosting.Items)
                    //    {
                    //        System.Web.UI.HtmlControls.HtmlTableCell tdAction = (System.Web.UI.HtmlControls.HtmlTableCell)item.FindControl("tdAction");
                    //        tdAction.Visible = false;

                    //    }
                    //    System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("tdPager");
                    //    tdpager.ColSpan = 10;
                    //}
                    //else
                    //{
                    //    System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thAction");
                    //    thAction.Visible = true;
                    //    System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("tdPager");
                    //    tdpager.ColSpan = 11;
                    //}
                }
                if (ApplicationSource == ApplicationSource.GenisysApplication)
                {
                    System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("tdPager");
                    if (RequisitionType == "My")
                    {
                        tdpager.ColSpan = 12;
                    }
                    else
                    {
                        tdpager.ColSpan = 11;
                    }
                              
                }




            }
            catch
            {
            }
            PlaceUpDownArrow();
            if (SiteSetting[DefaultSiteSetting.ApplicationEdition.ToString()].ToString() == UIConstants.INTERNAL_HIRING)
            {
                LinkButton lnkClient = (LinkButton)lsvJobPosting.FindControl("lnkClient");
                if (lnkClient != null)
                {
                    if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                        lnkClient.Text = "Account";
                    else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                        lnkClient.Text = "BU";
                    else lnkClient.Text = "Vendor";
                }
            }
        }
        protected void odsRequisitionList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (RequisitionType == "Master")
            {
                ShowAction = false;
                System.Web.UI.HtmlControls.HtmlTableCell thAction = (System.Web.UI.HtmlControls.HtmlTableCell)lsvJobPosting.FindControl("thAction");
                thAction.Visible = false;
            }
        }

        #endregion

        #endregion
    }
}