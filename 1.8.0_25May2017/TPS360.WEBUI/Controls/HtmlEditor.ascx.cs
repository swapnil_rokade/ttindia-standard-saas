﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Company.ascx.cs
    Description: This is the user control page used for company registration
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author                  MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Sep-15-2008           Yogeesh Bhat           Defect ID: 8660,8685; Changes made in SaveCompany() method.
                                                                (changed CurrentMember.Id to company.CreatorId)
    0.2            Jan-30-2009           Kalyani Pallagani      Defect ID: 8932,9203; Changes made in SaveCompany() method
                                                                to redirect to my company list page.Added RedirectToList method.
    0.3            Jan-30-2009           Kalyani Pallagani      Defect id: 9045; Commented company logo image dispaly code
 *  0.4            Apr-27-2009           Rajendra Prasad        Defect id: 10397;Added new method PopulateSiteSettings() to display
 *                                                              ddlcountry selected value according to sitesettings.
    0.5            Jun-18-2009               Veda               Defect id: 10647; when the user saves Companyinfo it should redirect to ContactsTab/FinancialData /DocumentTab  
 *  0.6            Apr-30-2010           Ganapati Bhat          Defect id: 12733; Commented lines to disable Toll free number extension
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Collections;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
namespace TPS360.Web.UI
{
    public partial class HtmlEditor : System .Web .UI .UserControl  
    {
        #region Member Variables

       

        #endregion

        #region Properties

        public bool Focus
        {
            set
            {
                if (value)
                    txtEditor.Focus();
            }
        }
        public string Text
        {
            get
            {
                return txtEditor.InnerText;
            }
            set
            {
                txtEditor.InnerText = value;
            }
        }
        public string TextXhtml
        {
            get
            {
                return txtEditor.InnerText;
            }
            set
            {
                txtEditor.InnerText = value;
            }
        }
        public string InnerHtml
        {
            get
            {
                return txtEditor.InnerText   ;
            }
            set
            {
                txtEditor.InnerText = value;
            }
        }
        public string RawText
        {
            get
            { 
                   return  Regex.Replace(Text.Trim(), @"<(.|\n)*?>", string.Empty);
                   //return System .Web .HttpUtility .HtmlDecode(rawDes);
            }

        }
        public string TextPlain
        {
            get
            {
                return Regex.Replace(Text.Trim(), @"<(.|\n)*?>", string.Empty);
                //return System .Web .HttpUtility .HtmlDecode(rawDes);
            }

        }
        #endregion

        #region Methods

     

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Request.Browser.Browser == "IE")
            //{
            //    string _removeEditorLoop = "for (var i=0; i<tinymce.editors.length; i++) {tinyMCE.execCommand('mceRemoveControl',false, tinymce.editors[i].id);};";
            //    System.Web.UI.ScriptManager.RegisterOnSubmitStatement(Page, Page.GetType(), "", _removeEditorLoop);
            //}
            System.Web.UI.ScriptManager.RegisterOnSubmitStatement(this, this.GetType(), "", "try{tinyMCE.triggerSave();}catch(e){}");
            if (Page.Request.Url.AbsoluteUri.ToString().Contains("SubmitToClient.aspx"))
            {
                txtEditor.Style.Add("height", "250px");

            }
        }
        #endregion
    }
}