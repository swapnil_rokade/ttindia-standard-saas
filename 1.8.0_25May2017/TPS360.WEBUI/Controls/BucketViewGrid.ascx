﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:BucketViewGrid.ascx
    Description:
    Created By: Sumit Sonawane
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
   
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BucketViewGrid.ascx.cs"
    Inherits="TPS360.Web.UI.BucketViewGrid" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>

<%@ Register Src ="~/Controls/CountryStatePicker.ascx" TagName ="CountryState" TagPrefix ="ucl" %>
<%@ Register Src ="~/Controls/CompanyRequisitionPicker.ascx" TagName ="CompReq" TagPrefix ="ucl" %>
<%@ Register Src="~/Controls/ModalTemplate.ascx" TagName="Template" TagPrefix="ucl" %>
<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<style>
  
    .BulkActionContext
    {
        cursor: pointer;
        position: absolute;
    }
    .BulkActionContext_List
    {
        display: block;
        top: 0px;
        clear: both;
        font-weight: bold;
        margin: 0;
        margin-left: 113px;
        padding: 0;
        border: 1px solid #4291df;
        background-color: #4291df;
        position: relative;
        width: 122px;
    }
    .BulkActionContext_List li
    {
        margin-left: -114px;
        position: relative;
        clear: both;
        padding-left: 8px;
        padding-bottom: 20px;
        padding-top: 7px;
        color: #555555;
        border-bottom: 1px solid #FCFCFC;
        border-top-style: none;
        border-top-width: 0px;
        border-bottom-color: #4291df;
        border-left-color: #4291df;
        border-right-color: #4291df;
        margin-top: 0px;
        z-index: 9999;
        background-color: #EFF6FF;
    }
    .BulkActionContext_List li:hover
    {
        border-top: none;
        border-color: #4291df;
    }
    .BulkActionContext #icon
    {
        padding: 0 7px;
        position: relative;
        z-index: 9999;
        float: left;
        border-width: 1px;
        border-style: solid;
        border-color: #4291df #4291df #4291df;
        top: 1px;
        clear: both;
        background-color: #EFF6FF;
        border-bottom-style: none;
        display: block;
        height: 20px;
        line-height: 23px;
        text-align: center;
        margin: 2px 2px 0 0;
        padding-bottom: 6px;
    }
    .BulkActionContext #icon:hover
    {
        position: relative;
        border-top: none;
        border-color: #4291df;
        margin-top: 3px;
    }
    .BulkActionContext a
    {
        color: #1F1FFF;
        text-decoration: none;
    }
    .BulkActionContext a:hover
    {
        color: #0000ff;
        text-decoration: none;
    }
</style>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/themes/smoothness/jquery-ui.css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js"></script>

<script type="text/javascript">
    $(function() {
    $("[id*=lsvGridCandidate]").sortable({
            items: 'tr:not(tr:first-child)',
            cursor: 'pointer',
            axis: 'y',
            dropOnEmpty: false,
            start: function(e, ui) {
                ui.item.addClass("selected");
            },
            stop: function(e, ui) {
                ui.item.removeClass("selected");
            },
            receive: function(e, ui) {
                $(this).find("tbody").append(ui.item);
            }
        });
    });
</script>





<script src="../js/CompanyRequisition.js"></script>
 
<script language="javascript" type="text/javascript">

    var n = 10;
    function ValidateReq(source, args) {
        validator = document.getElementById(source.id);
        var hdnField = document.getElementById('ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS')
        if (hdnField.value != '') args.IsValid = true;
        else {
            validator.innerHTML = "<br/>Please select candidate.";

            ShowDiv('divReqs'); n = 0;
            args.IsValid = false;
        }

    }
    function ValidateHotList(source, args) {

        alert("1");
        validator = document.getElementById(source.id);
        var hdnField = document.getElementById('ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS')
        hdnSelectedHotList = document.getElementById("ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedHotList");
        ddlHotlist = document.getElementById("ctl00_cphHomeMaster_ucntrlCandidateList_ddlHotlist");
        if (hdnSelectedHotList.value != '' || ddlHotlist.options[ddlHotlist.selectedIndex].value != "0") {
            if (hdnField.value != '') args.IsValid = true;
            else {
                validator.innerHTML = "<div>Please select candidate.<div>";
                ShowDiv('divHotListdiv'); n = 0;
                args.IsValid = false;
            }
        }
        else {
            validator.innerHTML = "Please select a hot list.";

            ShowDiv('divHotListdiv'); n = 0;
            args.IsValid = false;
        }

    }



    function monitorClick(e) {
        try {


            var evt = (e) ? e : event;

            var theElem = (evt.srcElement) ? evt.srcElement : evt.target;

            while (theElem != null) {
                if ($('#' + theElem.id).parents('.btn-group') == null || theElem.id == '' || theElem.id.indexOf('updPanelCandidateList') >= 0) {

                    n = 10;
                    var divReqs = document.getElementById('divReqs');
                    if (divReqs != null) {
                        $(divReqs).closest('.btn-group').removeClass('open');
                        divReqs.style.display = "none";
                    }
                    var divHotListdiv = document.getElementById('divHotListdiv');
                    if (divHotListdiv != null) {
                        $(divHotListdiv).closest('.btn-group').removeClass('open');
                        divHotListdiv.style.display = "none";
                    }
                    return true;
                }
                else {
                    n = 10;
                    return true;
                }

                theElem = theElem.offsetParent;
            }
        }
        catch (e) {
        } n = 10;
        return true;
    }
    document.onclick = monitorClick;

    function ShowDiv(divId) {

        n = 10;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function DropdownClicked(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }

    function DropdownChange(divId) {
        n = 0;
        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) reqDiv.style.display = "";
    }
    function ShowReqDiv(divId) {
        $('.btn-group').removeClass('open');
        var a = document.getElementById("divReqs");
        var b = document.getElementById("divHotListdiv");
        if (a != null) a.style.display = "none";
        if (b != null) b.style.display = "none";

        var reqDiv = document.getElementById(divId);
        if (reqDiv != null) {
            $(reqDiv).closest('.btn-group').addClass('open');
            reqDiv.style.display = "";
            reqDiv.parentElement.className = "btn-group open";
        } return false;
    }

    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq);
    function PageLoadedReq(sender, args) {

        try {
            var chkBox = document.getElementById('chkAllItem');
            if (chkBox != null) chkBox.disabled = false;
            CheckUnCheckGridViewHeaderCheckbox('tlbTemplate', 'chkAllItem', 0, 1);
        }
        catch (e) {
        }
    }

    function RequisitionSelected(source, eventArgs) {
        hdnSelectedRequisition = document.getElementById("ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedRequisition");
        hdnSelectedRequisitionText = document.getElementById("ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedRequisitionText");
        hdnSelectedRequisitionText.value = eventArgs.get_text();
        hdnSelectedRequisition.value = eventArgs.get_value();
    }

    function CLCheckUnCheckAllRowLevelCheckboxes(cntrlHeaderCheckbox, posCheckbox) {
        alert("3");

        var cntrlGridView = document.getElementById("tlbTemplate");
        var chkBoxAll = document.getElementById(cntrlHeaderCheckbox);
        var hndID = "";
        var chkBox = "";
        var checked = 0;
        var rowNum = 0;
        if (cntrlHeaderCheckbox != null && cntrlHeaderCheckbox.checked != null) {
            var rowLength = 0;
            rowLength = cntrlGridView.rows.length;
            for (var i = 1; i < rowLength; i++) {
                var myrow = cntrlGridView.rows[i];
                var mycel = myrow.getElementsByTagName("td")[posCheckbox];
                if (mycel.childNodes[0].value == "on") {
                    chkBox = mycel.childNodes[0];
                }
                else {
                    chkBox = mycel.childNodes[1];
                }
                try {
                    if (mycel.childNodes[2].value == undefined) {
                        hndID = mycel.childNodes[3];
                    }
                    else {
                        hndID = mycel.childNodes[2];
                    }
                }
                catch (e) {
                }
                if (chkBox != null) {
                    if (chkBox.checked == null) {
                        chkBox = chkBox.nextSibling;
                    }
                    if (chkBox != null) {
                        if (chkBox.checked != null) {
                            chkBox.checked = cntrlHeaderCheckbox.checked;
                            rowNum++;
                        }
                    }
                }
                if (chkBox != null) {
                    if (chkBox.checked != null && chkBox.checked == true) {
                        if (chkBox.id.indexOf('chkItemCandidate') > -1) {
                            if (hndID != null) ADDID(hndID.value, 'ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS');
                            checked++;

                        }
                    }
                    else {
                        if (chkBox.id.indexOf('chkItemCandidate') > -1) {
                            if (hndID != null) RemoveID(hndID.value, 'ctl00_cphHomeMaster_ucntrlCandidateList_hdnSelectedIDS');
                        }
                    }
                }
            }
        }

        if (checked == rowNum) {
            if (chkBoxAll != null) {
                chkBoxAll.checked = true;
            }
        }
        else {
            if (chkBoxAll != null) {
                chkBoxAll.checked = false;
            }
        }
    }

    function AssignValue(optionvalue) {
        var selectedvalue = document.getElementById('<%=txtSelectValue.ClientID %>');
        selectedvalue.value = optionvalue
    }

    function ReAssign() {
        var selectedvalue = document.getElementById('<%=txtSelectValue.ClientID %>');
        selectedvalue.value = "NoSelect";
    }
    
    

    function checkEnter(e) {
        var kC = window.event ? event.keyCode :
            e && e.keyCode ? e.keyCode :
            e && e.which ? e.which : null;
        if (kC == 13) {
            var selectedvalue = document.getElementById('<%=txtSelectValue.ClientID %>');
            if (selectedvalue.value == "Hotlist") {
                var btnclick = document.getElementById('ctl00_cphHomeMaster_ucntrlCandidateList_btnAddToHotList');
                btnclick.focus();
            }
            else if (selectedvalue.value == "Requisition") {
                var btnclick = document.getElementById('ctl00_cphHomeMaster_ucntrlCandidateList_btnAddToPSList');
                btnclick.focus();
            }
        }
    }
              
</script>


<div>
    <asp:TextBox ID="txtSortColumn" runat="server" Visible="false" Text ="btnName"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Visible="false" Text ="ASC"></asp:TextBox>
    <div style="display: none">
        <asp:TextBox ID="txtSelectValue" Text="NoSelect" runat="server" />
    </div>
    <asp:HiddenField ID="hdnSelectedRequisitionText" runat="server" />
    <asp:HiddenField ID="hdnSelectedRequisition" runat="server" />
    <asp:HiddenField ID="hdnCurrentMemberId" runat="server" />
    <asp:HiddenField ID="hdnSelectedHotList" runat="server" />
    <asp:HiddenField ID="hdnSelectedHotListText" runat="server" />
    <asp:TextBox ID="SortColumn" runat="server" EnableViewState="true" Visible="false" />
    <asp:TextBox ID="SortOrder" runat="server" EnableViewState="true" Visible="false" />
    
    
     <asp:ObjectDataSource ID="odsHiringMatrix" runat="server" SelectMethod="GetPagedBucket" 
                    TypeName="TPS360.Web.UI.HiringMatrixDataSource" 
                    EnablePaging="True" SortParameterName="sortExpression">
                    <SelectParameters>
                        <asp:Parameter Name="JobPostingId"  Type="String" Direction="Input" />
                        <asp:Parameter Name="SelectionStepLookupId"  Type="String" Direction="Input" />
                         <asp:Parameter Name="JobPostingCreatorID"  Type="String" Direction="Input" />
                         <asp:Parameter Name="MemberCreatorID"  Type="String" Direction="Input" />
                    </SelectParameters>
                </asp:ObjectDataSource>
    
    <%--<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updPanelCandidateList">
        <ProgressTemplate>
            <div class="modal">
                <div class="center">
                    <img alt="" src="Images/AjaxLoading.gif" />
                </div>
            </div>
        </ProgressTemplate>
</asp:UpdateProgress>--%>
    <asp:UpdatePanel ID="updPanelCandidateList" runat="server">
        <ContentTemplate>
         <asp:HiddenField ID="hdnDoPostPack" runat="server" />
            <asp:HiddenField ID="hdnSelectedIDS" runat="server" />
            <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
            <div class="TableRow" style="text-align: left;">
               <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
                <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
            </div>
       
            
             <div class="GridContainer" >
                 <div  style="float: left; font-size: 15px; background-color: #0074cc; border-radius: 4px; vertical-align: top; margin-bottom: 2px; margin-top:2px; height: 20px; width: 98.6%;">
               &nbsp;
                <asp:Label ID="Label1" runat="server" EnableViewState="False" ForeColor="White" 
                         Font-Bold="True"></asp:Label>
                 <asp:Label ID="Label2" runat="server" EnableViewState="False" ForeColor="White" 
                         Font-Bold="True"></asp:Label>
               </div>
               
            <asp:ListView ID="lsvGridCandidate" runat="server" 
                     onitemcommand="lsvGridCandidate_ItemCommand" DataSourceID="odsHiringMatrix"
                     onitemdatabound="lsvGridCandidate_ItemDataBound" >
                     
                <LayoutTemplate >
              
                    <table id="tblGridCandidate" runat="server"  class="Grid" cellspacing="0" border="0" style="width: 98.6%">
                        <tr >
                            <th style="width: 14%; white-space: nowrap;">
                                Name
                            </th>
                            <th style="width: 15%; white-space: nowrap;">
                                Email
                            </th>
                            <th style="width: 10%; white-space: nowrap;">
                                Mobile
                            </th>
                            <th style="width: 8%; white-space: nowrap;">
                                Location
                            </th>
                            <th style="width: 4%; white-space: nowrap;">
                                Exp.
                            </th>
                            <th style="width: 11%; white-space: nowrap;">
                                Job Code
                            </th>
                             <th style="width: 9%; white-space: nowrap;">
                                Notice Period
                            </th>
                           
                            <th style="width: 12%; white-space: nowrap;">
                                Current Company
                            </th>
                             <th style="width: 8%; white-space: nowrap;">
                                Source
                            </th>
                             <th style="width: 10%; white-space: nowrap;">
                                Action
                            </th>
                        </tr>     
                        </table>
                        <div style="height:200px;overflow: scroll">
                         <table id="Table1" runat="server"  class="Grid" cellspacing="0" border="0">
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                    </table>
                   </div>
                </LayoutTemplate>
                <EmptyDataTemplate>
                    <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                        <tr>
                            <td>
                                No Candidate Available.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                        <td style="width: 14%; ">
                        <asp:HyperLink ID="lnklblCandiName"  Text='<%#Eval("FirstName") + " " + Eval("LastName")%>' runat="server" Target="_blank"></asp:HyperLink>
                         
                        </td>
                        <td style="width: 15%; ">
                            <asp:Label ID="lblCandiEmail"  Text='<%#Eval("PrimaryEmail") %>' runat="server" />
                        </td>
                         <td style="width: 10%; ">
                            <asp:Label ID="lblCandiMobile" Text='<%#Eval("CellPhone") %>' runat="server" />
                        </td>
                         <td style="width: 8%; ">
                            <asp:Label ID="lblCandiLocation" Text='<%#Eval("CurrentCity") %>' runat="server" />
                        </td>
                         <td style="width: 4%; ">
                            <asp:Label ID="lblCandiExperience" Text='<%#Eval("TotalExperienceYears") %>' runat="server" />
                        </td>
                         <td style="width: 11%; ">
                            <asp:Label ID="lblCandiRemarks" Text='<%#Eval("Requisition") %>' runat="server" />
                        </td>
                         <td style="width: 9%; ">
                            <asp:Label ID="lblCandiSkill" Text='<%#Eval("NoticePeriod") %>' runat="server" />
                        </td>
                      
                           <td style="width: 12%; ">
                            <asp:Label ID="lblCandiEducation" Text='<%#Eval("CurrentPosition") %>' runat="server" />
                        </td>
                            <td style="width: 8%; ">
                            <asp:Label ID="lblCandiSource" Text='<%#Eval("SourceName") %>' runat="server" />
                        </td>
                        <td style="text-align: center; width: 10%" runat ="server" id="tdAction">
    
                             <asp:ImageButton ID="imgbtnResume" ToolTip="Documents"  runat="server" ImageUrl="../Images/Bucket%20View/Documents.png">
                              </asp:ImageButton>                           
                             <asp:ImageButton ID="imgbtnCandEmail" ToolTip="Email to Candidate"  runat="server"  ImageUrl="../Images/Bucket%20View/EmailCandidate.png">
                            </asp:ImageButton>
                             <asp:ImageButton ID="imgbtnFeedbackEmail" ToolTip="Schedule Interview"  runat="server"  ImageUrl="../Images/Bucket%20View/Interview_Sch.png" Visible="true">
                            </asp:ImageButton>
                              <asp:ImageButton ID="imgbtnNotes" ToolTip="Candidate Notes" runat="server"  ImageUrl="../Images/Bucket%20View/Notes.png">
                             </asp:ImageButton>
                          
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
            
            </div>
           
            
               </ContentTemplate>
    </asp:UpdatePanel>
</div>
