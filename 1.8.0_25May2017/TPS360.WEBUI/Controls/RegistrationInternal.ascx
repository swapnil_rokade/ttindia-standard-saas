﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RegistrationInternal.ascx
    Description: This is the user control page used to register the candidate
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Sep-09-2008          Jagadish            Defect id: 8640; Button with text "Check Availablity" is changed to "Check Availability".
    0.2             Sep-10-2008          Yogeesh Bhat        Defect ID: 8639; changes made in function DisableRequiredFieldValidator()
    0.3             07-Aug-2009          Veda                Defect Id:11215 space before and after the mail id has been trimmed
                                                             Commented DisableRequiredFieldValidator(),EnableRequiredFieldValidator() and regular expression for txtUserName
    0.4             11-Aug-2009          Gopala Swamy J      Defect Id:11204 Changed the label text             
    0.5             27-Apr-2010          Ganapati Bhat       Defect Id:12724 Added Required field validator for First Name & Last Name                                                
    0.6             13-May-2010          Sudarshan.R.        Defect Id:12793,12794 Fixed the error message display issues
    0.7             11/May/2016          pravin khot         new added-divNoticePeriod
    0.8             19/May/2016          pravin khot         comment and modify - divNotAvailable
    0.9             10/Jun/2016          Prasanth Kumar G    Introduced User Name for AD Login, Currently User name is using for Email it has changed to userEmail. 
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RegistrationInternal.ascx.cs"
    Inherits="TPS360.Web.UI.ControlRegistrationInternal" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ReferrerInfo.ascx" TagName="Refer" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/CountryStatePicker.ascx" TagName="CountryState" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ExperiencePicker.ascx" TagName="Experience" TagPrefix="ucl" %>
<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../Scripts/ToolTipscript.js"></script>

<link type="text/css" href="../Style/ToolTip.css" rel="Stylesheet" />

<script> 

    function ValidateCopyPaste(sender,args) {
 
         if($('.CopyPasteoption').children(":first").is(':checked'))
         {
            if($('#<%= txtCopyPasteResume.ClientID %>').val().trim()=="")
              args.IsValid=false ;
         }
    }

    
function ValidateSourceDescription(sender,args)
    {
        args .IsValid=true;
        if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")=="Job Portals")
        {
                if( $('#<%=ddlSourceDescription.ClientID %>').prop("selectedIndex")<0) args .IsValid=false ;
        }
        else if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")=="Employee Referral")
        {
            if( $('#<%=ddlEmployeeReferrer.ClientID %>').prop("selectedIndex")<=0) args .IsValid=false ;
        }
//        else if ($('#<%=ddlSource.ClientID %>').prop("selectedIndex")==4)
//        {
//            if( $('#<%=ddlVendorContact.ClientID %>').prop("selectedIndex")<=0) args .IsValid=false ;
//        }
        else
        {
        if( $('#<%= txtsrcdesc.ClientID %>').val().trim()=="") args .IsValid=false ;
        }
//        if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")>0 && $('#<%=ddlEmployeeReferrer.ClientID %>').prop("selectedIndex")>0 && $('#<%=ddlSourceDescription.ClientID %>').prop("selectedIndex")>0 && $('#<%= txtsrcdesc.ClientID %>').val().trim()=="")
//        {
//           args.IsValid=false;
//        }
        
    }
    
    Sys.Application.add_load(function() {
      $('input[rel=Numeric]').keypress(function (event){return allownumeric(event,$(this));});
      $('input[rel=Integer]').keypress(function (event){return allowInteger(event,$(this));});
      
        $('#<%=ddlPassordOption.ClientID %>').change(function() {
            if ($(this).val() == 1) {
                $('#divPasswordAlert').show();
                $('#divEmployeePassword').hide(); 
                ValidatorEnable(document .getElementById ( '<%= rgvEmployeePassword.ClientID %>'), false);
                ValidatorEnable(document .getElementById ( '<%= rgvEmployeeConfirmPassword.ClientID %>'), false);
                
            }
            else {
                $('#divPasswordAlert').hide();
                $('#divEmployeePassword').show();
            }
        });
         if ($('#<%=ddlPassordOption.ClientID %>').val() == 1) {
                $('#divPasswordAlert').show();
                $('#divEmployeePassword').hide(); 
                ValidatorEnable(document .getElementById ( '<%= rgvEmployeePassword.ClientID %>'), false);
                ValidatorEnable(document .getElementById ( '<%= rgvEmployeeConfirmPassword.ClientID %>'), false);
                
            }
            else {
                $('#divPasswordAlert').hide();
                $('#divEmployeePassword').show();
            }
        
    });
    
</script>

<script language="javascript" type="text/javascript">

function ValidateSourceDescription(sender,args)
    {
    var ddlSource= $("#<%=ddlSource.ClientID %>");
        args .IsValid=true;
//        if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")==2)
//        {
//            if( $('#<%=ddlVendorContact.ClientID %>').prop("selectedIndex")<=0) args .IsValid=false ;
//        }
        if ($(ddlSource).find('option:selected').text().trim() == "Job Portals")
        {
           if( $('#<%=ddlSourceDescription.ClientID %>').prop("selectedIndex")<0) args .IsValid=false ;
           
        }
        else  if ($(ddlSource).find('option:selected').text().trim() == "Employee Referral")
        {
            if( $('#<%=ddlEmployeeReferrer.ClientID %>').prop("selectedIndex")<=0) args .IsValid=false ;
        }
        else 
        {
            if( $('#<%= txtsrcdesc.ClientID %>').val().trim()=="") args .IsValid=false ;
        }
//        if($('#<%=ddlSource.ClientID %>').prop("selectedIndex")>0 && $('#<%=ddlEmployeeReferrer.ClientID %>').prop("selectedIndex")>0 && $('#<%=ddlSourceDescription.ClientID %>').prop("selectedIndex")>0 && $('#<%= txtsrcdesc.ClientID %>').val().trim()=="")
//        {
//           args.IsValid=false;
//        }
        
    }
    





function showHideSource(i)
{
var ddlSource= $("#<%=ddlSource.ClientID %>");
    $("#<%=divEmpReferral.ClientID %>").css("display", "none");     
                   $("#<%=divVendor.ClientID %>").css("display", "none");     
                    $("#<%=txtsrcdesc.ClientID %>").css("display", "none");                   
                    $("#<%=ddlSourceDescription.ClientID %>").css("display", "none");    
                    
                    
                if ($(ddlSource).find('option:selected').text().trim() == "Job Portals") {
                    $("#<%=ddlSourceDescription.ClientID %>").css("display", "");                     
                }
                
                else  if ($(ddlSource).find('option:selected').text().trim() == "Employee Referral")
                {
                $("#<%=divEmpReferral.ClientID %>").css("display", "");     
                }
//                else  if ($(ddlSource).find('option:selected').text().trim() == "Vendor")
//                {
//                   $("#<%=divVendor.ClientID %>").css("display", "");     
//                }
                else {
                     $("#<%=txtsrcdesc.ClientID %>").css("display",""); 
                     if(i==0)
                        $("#<%=txtsrcdesc.ClientID %>").val('');  
                }
}
//function showHideSource(i)
//{
//    var ddlSource= $("#<%=ddlSource.ClientID %>");
//                   $("#<%=divEmpReferral.ClientID %>").css("display", "none");     
//                   $("#<%=divVendor.ClientID %>").css("display", "none");     
//                   $("#<%=txtsrcdesc.ClientID %>").css("display", "none");                   
//                   $("#<%=ddlSourceDescription.ClientID %>").css("display", "none");    
//                    
//                    
//                if ($(ddlSource).find('option:selected').text().trim() == "Job Portals") {
//                   $("#<%=ddlSourceDescription.ClientID %>").css("display", "");     
//                   $("#<%=spnReqforddlvendor.ClientID %>").css('visibility','hidden'); 
//                   $("#<%=spnReqforddlemployeereferral.ClientID %>").css('visibility','hidden');  
//                   $("#<%=spnReqfortxtSrcDesc.ClientID %>").css('visibility','Visible');                  
//                }
//                
//                else  if ($(ddlSource).find('option:selected').text().trim() == "Employee Referral")
//                {
//                   $("#<%=divEmpReferral.ClientID %>").css("display", "");  
//                   $("#<%=spnReqforddlvendor.ClientID %>").css('visibility','hidden'); 
//                   $("#<%=spnReqfortxtSrcDesc.ClientID %>").css('visibility','hidden'); 
//                   $("#<%=spnReqforddlemployeereferral.ClientID %>").css('visibility','Visible');
//                }
////                else  if ($(ddlSource).find('option:selected').text().trim() == "Placement Consultants")
////                {
////                   $("#<%=divVendor.ClientID %>").css("display", "");     
////                   $("#<%=spnReqforddlemployeereferral.ClientID %>").css('visibility','hidden'); 
////                   $("#<%=spnReqfortxtSrcDesc.ClientID %>").css('visibility','hidden');  
////                   $("#<%=spnReqforddlvendor.ClientID %>").css('visibility','Visible'); 
////                }
//                else {
//                   $("#<%=txtsrcdesc.ClientID %>").css("display", "");  
//                   if(i==0)
//                        $("#<%=txtsrcdesc.ClientID %>").val(''); 
//                   $("#<%=spnReqforddlemployeereferral.ClientID %>").css('visibility','hidden'); 
//                   $("#<%=spnReqforddlvendor.ClientID %>").css('visibility','hidden'); 
//                   $("#<%=spnReqfortxtSrcDesc.ClientID %>").css('visibility','Visible');  
//                }
//}
       
       
Sys.Application.add_load(function() {
            $("#<%=ddlSource.ClientID %>").change(function() {
         
               showHideSource(0);
                
            });
            showHideSource(1);
        });
        
</script>

<script runat="server">
    

    void ValidateUploadFile(Object source, ServerValidateEventArgs args)
    {
        if (rdoUploadResume.Checked == true)
        {
            args.IsValid = (fuDocument.HasFile);
        }
        //if (rdoCopyPaste.Checked == true)
        //{
        //    args.IsValid = (txtCopyPasteResume.Text.Trim() == string.Empty ? false : true);
        //}
    }
    void CaptchaValidation(object source, ServerValidateEventArgs args)
    {
        if (divCaptcha.Visible == true)
        {
            String challenge = Request["recaptcha_challenge_field"];
            String reponseField = Request["recaptcha_response_field"];
            String privateKey = "6Lcm1b8SAAAAAG9CkoHFnn7g6vWKiXI4piqMsIzh";
            Recaptcha.RecaptchaValidator abc = new Recaptcha.RecaptchaValidator();
            abc.Challenge = challenge;
            abc.Response = reponseField;
            abc.PrivateKey = privateKey;
            abc.RemoteIP = Request.ServerVariables["REMOTE_ADDR"];
            Recaptcha.RecaptchaResponse resp = abc.Validate();
            if (!resp.IsValid)
            {
                args.IsValid = false;
                System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "whatever1", "Recaptcha.reload();", true);
            }
            else
            {
                args.IsValid = true;
            }

            //cvSecurityQuestion.Validate();
            //rqfCheck.Validate();
            //args.IsValid = (Page.IsValid);
        }


    }

</script>

<script type="text/javascript" language="javascript">

    function EmptyCheckUserLabel() {
        //debugger;        k
        var txtUserEmail = $get('<%= txtUserEmail.ClientID %>');
        var lblCheckUser = $get('<%= lblCheckUser.ClientID %>');

        if (txtUserEmail.value == "") {
            lblCheckUser.innerHTML = "";
        }
    } 
 

    function alphanumeric_only(e) {
        var keycode;
        if (window.event) keycode = window.event.keyCode;
        else if (event) keycode = event.keyCode;

        else if (e) keycode = e.which;
        else return true; if ((keycode >= 48 && keycode <= 57) || (keycode >= 65 && keycode <= 90) || (keycode >= 97 && keycode <= 122 || keycode == 45)) {
            return true;

        }

        else {
            return false;

        }
        return true;

    }

</script>

<style>
    .UploadResumeoption
    {
    }
    .CopyPasteoption
    {
    }
    .StepByStepoption
    {
    }
</style>

<script>
Sys.Application.add_load(function() { 
if($('#<%=hdnApplicationType.ClientID %>').val()=='')
{
    $('.RequiredField').css({'display':'none'});
    $('#spemail,#spfirstname,#splastname,#spfile').css({'display':''});
        
}
  if($('.UploadResumeoption').children(":first").is(':checked')) {
      
  $("#divUploadResume").show();
            $("#divCopyPasteResume").hide();
  }
 if($('.CopyPasteoption').children(":first").is(':checked'))
  { $("#divUploadResume").hide();
            $("#divCopyPasteResume").show();
  }
  if($('.StepByStepoption').children(":first").is(':checked')) {
      
  $("#divUploadResume").hide();
            $("#divCopyPasteResume").hide();
  }

  $(".UploadResumeoption").click(function() {
    
            $("#divUploadResume").show();
            $("#divCopyPasteResume").hide();
        });
        $(".CopyPasteoption").click(function() {
       
            $("#divUploadResume").hide();
            $("#divCopyPasteResume").show();

        });
        $(".StepByStepoption").click(function() {
     
            $("#divUploadResume").hide();
            $("#divCopyPasteResume").hide();
        });
    });


    function pageLoad() {
    
    try{

        if (document.getElementById('<%=rdoUploadResume.ClientID %>').checked) {
            //rdoUploadResume radio button is checked
        
            $("#divUploadResume").show();
            $("#divCopyPasteResume").hide();

        }
        else if (document.getElementById('<%=rdoUploadResume.ClientID %>').checked) {
        //rdoUploadResume radio button is checked
        
            $("#divUploadResume").hide();
            $("#divCopyPasteResume").show();
        }
        else if (document.getElementById('<%=rdoStepByStep.ClientID %>').checked) {
        //rdoStepByStep radio button is checked
        
            $("#divUploadResume").hide();
            $("#divCopyPasteResume").hide();
        }
}
catch (e)
{
}

    }
    


</script>

<script type = "text/javascript">
    function ValidateCheckBox(sender, args) {
        if (document.getElementById("<%=CheckBox1.ClientID %>").checked == true) {
            args.IsValid = true;
        } else {
            args.IsValid = false;
        }
    }
    </script>

<uc1:Refer ID="uclRefer" runat="server" />
<asp:HiddenField ID="hdnApplicationType" runat="server" Value="LandT" />
<div class="TableRow" style="text-align: center">
    <asp:Label EnableViewState="false" ID="lblMessage" runat="server"></asp:Label>
 <div id="dv" runat="server" visible="false">
                        <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
                    </div>
</div>
<div class="ajaxloading">
    <asp:UpdateProgress ID="upbRegistration" runat="server" AssociatedUpdatePanelID="uplRegistration">
        <ProgressTemplate>
            <asp:Image EnableViewState="false" runat="server" ID="imgLogo" ImageUrl="~/Images/AjaxLoading.gif" />
            <br />
        </ProgressTemplate>
    </asp:UpdateProgress>
</div>
<asp:Panel ID="pnlRegistration" runat="server" DefaultButton="btnSave">
    <asp:UpdatePanel ID="uplRegistration" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="TabPanelHeader">
                <asp:Label ID="lblCandidateInfoHeader" runat="server" Text="Basic Details"></asp:Label>
                <span class="TabPanelHeadersmall" id="spanCandidateInfoHeader" runat="server">Fill in
                    the name and email to get started.</span></div>
            <asp:Panel ID="pnlEmail" runat="server" DefaultButton="btnSave">
            <!-- Code introduced by prasanth on 10/Jun/2016 Start-->
            <div  id="divUserName" runat="server" visible="false">
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblUsrName" runat="server" Text="User Name"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox ID="txtUserName" runat="server" Width="200px"
                            AutoComplete="OFF" CssClass="CommonTextBox"
                            TabIndex="1" ValidationGroup="RegInternal"></asp:TextBox>
                        <span class="RequiredField" id='Span5'>*</span>
                        
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:Label ID="lblUserName" EnableViewState="False" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserName"
                            ErrorMessage="Please Enter User Name address." ValidationGroup="RegInternal" EnableViewState="False"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <!-- ******************END***************************-->
                <div class="TableRow">
                    <div class="TableFormLeble">
                        Email :
                    </div>
                    <div class="TableFormContent">
                        <asp:TextBox EnableViewState="false" ID="txtUserEmail" runat="server" Width="200px"
                            OnChange='CheckUserAvailability()' AutoComplete="OFF" CssClass="CommonTextBox"
                            TabIndex="1" ValidationGroup="RegInternal"></asp:TextBox> 
                          
                             <span class="RequiredField" id='spemail'>*</span>    
                       <%--  //***********ADDED BY PRAVIN KHOT ON 18/May/2016*****************             --%>    
                     
                       <%-- <asp:CheckBox ID="ChkExistingUser" runat="server"  OnCheckedChanged="ChkExisting_Click" AutoPostBack="true" />Check Availablity: --%>
                        
                    <%--  ***********************************END******************************--%>
                       
                        <div id="divAvailableUser" style="color: Green; display: none" enableviewstate="true">
                            User name available</div>
                           <%-- **************Code commented by pravin khot on 19/May/2016**********--%>
                            <%-- <div id="divNotAvailable"  style="color: Red; display: none" enableviewstate="true">
                              User name not available
                           </div>  --%>     
                           <%--**********************END*******************************       --%>         
                    </div>
                    
                      
                       
                </div>              
                         
               <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:Label ID="lblCheckUser" EnableViewState="False" runat="server" />
                       <%-- ********************Code added by pravin khot on 16/May/2016***************--%>
                        <div id="divNotAvailable"  style="color: Red; display: none" enableviewstate="true">
                              User name allready Exists.  
                            <asp:Button ID="btnCandidateLogin" runat="server" Text="Go Candidate Login"  CssClass="CommonButton"  OnClick="btnCandidateLogin_Click"  />
                       </div>
                       <%-- //***************************END********************************--%>
                        <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserEmail"
                            ErrorMessage="Please Enter Email address." ValidationGroup="RegInternal" EnableViewState="False"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revUserName" runat="server" ControlToValidate="txtUserEmail"
                            Display="Dynamic" ErrorMessage="Please enter valid email address." ValidationExpression="\w+([-+.!#$%&*+-/=?^_`{|}~'\\\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            SetFocusOnError="true" ValidationGroup="RegInternal"></asp:RegularExpressionValidator>
                        <%--\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*--%>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divPassword" runat="server" visible="false">
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblPassword" runat="server" Text="Password"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox ID="txtPassword" runat="server" Width="200" CssClass="CommonTextBox" TabIndex="2"
                    TextMode="Password" rel="tooltipInput" data-original-title="Password must be at least 8 characters long."
                    data-placement="right" value=""></asp:TextBox>
                <span class="RequiredField">*</span>
            </div>
            <div class="TableFormValidatorContent" style="margin-left: 42%">
                <asp:RequiredFieldValidator ID="rfvNewPassword" runat="server" ControlToValidate="txtPassword"
                    ErrorMessage="Please Enter New Password." EnableViewState="False" Display="Dynamic"
                    ValidationGroup="RegInternal">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revNewPassword" runat="server" ControlToValidate="txtPassword"
                    Display="Dynamic" ErrorMessage="Password must be greater than 8 characters with at least one numeric,</br> one alphabet and one special character." EnableViewState="false"
                    ValidationGroup="RegInternal" ValidationExpression="(?=^.{8,30}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&*()_+}{:;'?/>.<,])(?!.*\s).*$"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblConformPassword" runat="server" Text="Confirm Password"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox ID="txtConformPassword" runat="server" Width="200" CssClass="CommonTextBox" TabIndex="3"
                    TextMode="Password"></asp:TextBox>
                <span class="RequiredField">*</span>
            </div>
            <div class="TableFormValidatorContent" style="margin-left: 42%">
                <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" ControlToValidate="txtConformPassword"
                    ValidationGroup="RegInternal" ErrorMessage="Please retype new password." EnableViewState="False"
                    Display="Dynamic">
                </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvNewPassword" runat="server" ControlToValidate="txtConformPassword"
                    Type="String" Operator="Equal" ControlToCompare="txtPassword" ErrorMessage="Passwords do not match."
                    ValidationGroup="RegInternal" EnableViewState="False" Display="Dynamic"></asp:CompareValidator>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlNames" runat="server" DefaultButton="btnSave">
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblFirstName" runat="server" Text="First Name"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtFirstName" runat="server" Width="200"
                    CssClass="CommonTextBox" OnKeyPress="return alphanumeric_only(this)" TabIndex="4"> 
                </asp:TextBox>
                <span class="RequiredField" id='spfirstname'>*</span>
            </div>
            <div class="TableFormValidatorContent" style="margin-left: 42%">
                <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                    ErrorMessage="Please Enter First Name." ValidationGroup="RegInternal" EnableViewState="False"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableView="false" ID="lblLastName" runat="server" Text="Last Name"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox EnableViewState="false" ID="txtLastName" runat="server" Width="200"
                    CssClass="CommonTextBox" OnKeyPress="return alphanumeric_only(this)" TabIndex="5"></asp:TextBox>
                <span class="RequiredField" id='splastname'>*</span>
            </div>
            <div class="TableFormValidatorContent" style="margin-left: 42%">
                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                    ErrorMessage="Please Enter Last Name." ValidationGroup="RegInternal" EnableViewState="False"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
    </asp:Panel>
    <div id="divAdditionalInfo" runat="server">
        <div class="TableRow">
            <div class="TableFormLeble">
                <asp:Label EnableViewState="false" ID="lblMobile" runat="server" Text="Mobile"></asp:Label>:
            </div>
            <div class="TableFormContent">
                <asp:TextBox ID="txtMobile" runat="server" Width="200" CssClass="CommonTextBox" TabIndex="6"
                    MaxLength="15"></asp:TextBox>
                <span class="RequiredField">*</span>
            </div>
            <div class="TableFormValidatorContent" style="margin-left: 42%">
                <asp:RequiredFieldValidator ID="rfvmobile" runat="server" ControlToValidate="txtMobile"
                    ErrorMessage="Please Enter Mobile Number." ValidationGroup="RegInternal" EnableViewState="False"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="TableRow" style="width: 100%">
            <div class="TableFormValidatorContent" style="margin-left: 42%">
                <asp:RegularExpressionValidator ID="revMobile" runat="server" Display="Dynamic" ControlToValidate="txtMobile"
                    EnableViewState="false" ErrorMessage="Please enter 0-9,(),+- only." ValidationGroup="BasicInfo"
                    ValidationExpression="^([\+][0-9]{1,10}([ \-])?)?([\(]{1}[0-9]{1,5}[\)])?([0-9 \-]{1,32})((x|X)?[0-9]{2,4}?)$"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="TableRow" id="divLinkedIn" runat="server" visible="false">
            <div class="TableFormLeble">
                LinkedIn Profile:</div>
            <div class="TableFormContent">
                <asp:TextBox ID="txtLinkedIn" runat="server" CssClass="CommonTextBox" Width="200"
                    TabIndex="5"></asp:TextBox>
            </div>
        </div>
        
        <div id="divNotInEmployeeReferal" runat="server">
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblCity" runat="server" Text="City"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtCity" runat="server" Width="200" CssClass="CommonTextBox" TabIndex="7"></asp:TextBox>
                    <span class="RequiredField">*</span>
                </div>
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="txtCity"
                        ErrorMessage="Please Enter City." ValidationGroup="RegInternal" EnableViewState="False"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblPincode" runat="server" Text="PIN Code"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtPincode" runat="server" Width="200" CssClass="CommonTextBox"
                        TabIndex="8"></asp:TextBox>
                </div>
            </div>
            <div class="TableRow" style="width: 100%">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RegularExpressionValidator ID="revZIPCode" runat="server" ControlToValidate="txtPincode"
                        Display="Dynamic" ErrorMessage="Please Enter valid Pin code" ValidationExpression="^[0-9-]*$"
                        ValidationGroup="BasicInfo"></asp:RegularExpressionValidator><%--(\d{6}(-\d{4})?)|(\d{5}(-\d{4})?)--%>
                </div>
            </div>
            <ucl:CountryState ID="uclCountryState" runat="server" FirstOption="Please Select"
                CountryTabIndex="9" StateTabIndex="10" />
                
                
        <div id="divTotalExp" runat="server">
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lbltotalworkexp" runat="server" Text="Total Work Exp (Months)"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <ucl:Experience ID="uclExperience" runat="server" />
                    <span class="RequiredField">*</span>
                </div>
            </div>
        </div>
        <div id="divCurrentCTC" runat="server">
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblCurrentCTC" runat="server" Text="Current CTC"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <div style="width: 40px; float: left;">
                        <asp:TextBox EnableViewState="false" ID="txtCurrentYearlyCTC" Width="40px" runat="server" Text="0.00"
                            CssClass="CommonTextBox" rel="Numeric" TabIndex="18" onmouseover="this.title=this.value;"></asp:TextBox> <%--code added by pravin khot only(Text="0.00") on 21/Jan/2016--%>
                    </div>
                    <div style="float: left; text-align: left; padding-left: 15px;">
                        <asp:DropDownList EnableViewState="true" ID="ddlCurrentYearlyCurrency" runat="server"
                            CssClass="CommonDropDownList" TabIndex="19" Width="70px">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlCurrentSalaryCycle" runat="server" CssClass="CommonDropDownList"
                            TabIndex="20" Width="70px">
                            <asp:ListItem Value="4" Selected="True">Yearly</asp:ListItem>
                            <asp:ListItem Value="3">Monthly</asp:ListItem>
                            <asp:ListItem Value="2">Daily</asp:ListItem>
                            <asp:ListItem Value="1">Hourly</asp:ListItem>
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                        <asp:Label ID="lblCurrentSalaryCurrency" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator ID="rfvcurrentCTC" runat="server" ControlToValidate="txtCurrentYearlyCTC"
                        ErrorMessage="Please Enter Current CTC." ValidationGroup="RegInternal" EnableViewState="False"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RegularExpressionValidator runat="server" ID="revCurrentYearlyCTC" ControlToValidate="txtCurrentYearlyCTC"
                        ErrorMessage="Invalid Current CTC <br/>" ValidationExpression="([0-9]+\.[0-9]+|[0-9]+)"
                        EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ValidationGroup="RegInternal"></asp:RegularExpressionValidator>
                </div>
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator runat="server" ID="rfvCurrentYearlyCTC" ControlToValidate="txtCurrentYearlyCTC"
                        EnableViewState="false" ErrorMessage="Invalid Current CTC <br/>" SetFocusOnError="true"
                        Display="Dynamic" ValidationGroup="RegInternal" InitialValue="<%#String.Empty%>"
                        Visible="False"></asp:RequiredFieldValidator>
                </div>
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <%-- <asp:CustomValidator ID="CustomValidatorCYCurrency" runat="server" ControlToValidate="ddlCurrentYearlyCurrency"
                    EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please select currency type. <br/>"
                    ClientValidationFunction="fnValidateSalary" ValidationGroup="RegInternal" />--%>
                </div>
            </div>
        </div>
        <div id="divExpectedCTC" runat="server">
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblExpectedCTC" runat="server" Text="Expected CTC"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <div style="width: 40px; float: left;">
                        <asp:TextBox EnableViewState="false" ID="txtExpectedYearlyCTC" Width="40px" runat="server" Text="0.00"  
                            CssClass="CommonTextBox" rel="Numeric" TabIndex="21" onmouseover="this.title=this.value;"></asp:TextBox> <%--code added by pravin khot only(Text="0.00") on 21/Jan/2016--%>
                    </div>
                    <div style="float: left; text-align: left; padding-left: 15px;">
                        <asp:DropDownList EnableViewState="true" ID="ddlExpectedYearlyCurrency" runat="server"
                            CssClass="CommonDropDownList" TabIndex="22" Width="70px">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlExpectedSalaryCycle" runat="server" CssClass="CommonDropDownList"
                            TabIndex="23" Width="70px">
                            <asp:ListItem Value="4" Selected="True">Yearly</asp:ListItem>
                            <asp:ListItem Value="3">Monthly</asp:ListItem>
                            <asp:ListItem Value="2">Daily</asp:ListItem>
                            <asp:ListItem Value="1">Hourly</asp:ListItem>
                        </asp:DropDownList>
                        <span class="RequiredField">*</span>
                        <asp:Label ID="lblExpectedSalaryCurrency" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator ID="rfvExpectedCTC" runat="server" ControlToValidate="txtExpectedYearlyCTC"
                        ErrorMessage="Please Enter Expected CTC." ValidationGroup="RegInternal" EnableViewState="False"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="TableRow" style="width: 100%">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RegularExpressionValidator runat="server" ID="revExpectedYearlyCTC" ControlToValidate="txtExpectedYearlyCTC"
                        ErrorMessage="Invalid Expected CTC <br/>" ValidationExpression="([0-9]+\.[0-9]+|[0-9]+)"
                        EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ValidationGroup="RegInternal"></asp:RegularExpressionValidator>
                </div>
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator runat="server" ID="rfvExpectedYearlyCTC" ControlToValidate="txtExpectedYearlyCTC"
                        EnableViewState="false" ErrorMessage="Invalid Expected CTC <br/>" SetFocusOnError="true"
                        Display="Dynamic" ValidationGroup="RegInternal" InitialValue="<%#String.Empty%>"
                        Visible="False"></asp:RequiredFieldValidator>
                </div>
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <%--                <asp:CustomValidator ID="CustomValidator1Currency" runat="server" ControlToValidate="ddlCurrentYearlyCurrency"
                    EnableViewState="false" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Please select currency type. <br/>"
                    ClientValidationFunction="fnValidateSalary" ValidationGroup="RegInternal" />--%>
                </div>
            </div>
        </div>
        
         <%--*************New code added by pravin khot on 11/May/2016*********************--%>
           <div class="TableRow" runat="server" id="divNoticePeriod"> 
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblNoticePeriod" runat="server" Text="Notice Period"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox EnableViewState="true" ID="txtNoticePeriod" runat="server" CssClass="CommonTextBox"
                        ValidationGroup="RegInternal" TabIndex ="25" onmouseover  ="this.title=this.value;"  Width ="100px" ></asp:TextBox>
                   <span class="RequiredField" id='SpNoticePeriod'>*</span>
                </div>
                  <div class="TableFormValidatorContent" style="margin-left: 42%">
                <asp:RequiredFieldValidator ID="rfvNoticePeriod" runat="server" ControlToValidate="txtNoticePeriod"
                    ErrorMessage="Please Enter Notice Period." ValidationGroup="RegInternal" EnableViewState="False"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
            </div>            
       <%--****************************************END**********************************--%>                
        </div>
      <div id="divHide" runat="server">
      <div id="divCurrentCompany" runat="server">
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lbcurrnetcompany" runat="server" Text="Current Company"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtCurrentCompany" runat="server" Width="200" CssClass="CommonTextBox"
                        TabIndex="11"></asp:TextBox>
                    <span class="RequiredField">*</span>
                </div>
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator ID="rfvcurrentcompany" runat="server" ControlToValidate="txtCurrentCompany"
                        ErrorMessage="Please Enter Current Company." ValidationGroup="RegInternal" EnableViewState="False"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
       
        <div id="divSource" runat="server">
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblSource" runat="server" Text="Source"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList ID="ddlSource" runat="server" CssClass="CommonDropDownList" Width="17%"
                        TabIndex="12">
                    </asp:DropDownList>
                    <span class="RequiredField">*</span>
                </div>
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <%--<asp:CustomValidator runat="server"  Display="Dynamic" ValidationGroup="RegInternal" ID="CVSource" ErrorMessage="Please Select Source" ClientValidationFunction ="ValidateSource" >
                    </asp:CustomValidator> --%>
                    <asp:RequiredFieldValidator ID="rfvSource" runat="server" InitialValue="Employee Referral"
                        ControlToValidate="ddlSource" ErrorMessage="Please Select Source." ValidationGroup="RegInternal"
                        EnableViewState="False" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label ID="lblSourceDescription" runat="server" Text="Source Description"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <div id="divVendor" runat="server" style="display: none">
                        <asp:DropDownList ID="ddlVendor" runat="server" CssClass="chzn-select" Width="200px"
                            TabIndex="13">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlVendorContact" runat="server" CssClass="chzn-select" Width="200px"
                            TabIndex="14">
                        </asp:DropDownList>
                        <asp:HiddenField ID="hdnSelectedVendorContact" runat="server" Value="0" />
                        <span class="RequiredField" id="spnReqforddlvendor" runat="server">*</span>
                    </div>
                    <asp:HiddenField runat="server" ID="hdntemp" Value="0" />
                    <div id="divEmpReferral" runat="server" style="display: none">
                        <asp:DropDownList ID="ddlEmployeeReferrer" runat="server" CssClass="chzn-select"
                            Width="200px" TabIndex="15">
                        </asp:DropDownList>
                        <span class="RequiredField" id="spnReqforddlemployeereferral" runat="server">*</span>
                    </div>
                    <asp:DropDownList ID="ddlSourceDescription" runat="server" Width="17%" CssClass="CommonDropDownList"
                        TabIndex="16">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtsrcdesc" runat="server" CssClass="CommonTextBox" Style="display: none"
                        TabIndex="17"></asp:TextBox>
                    <span class="RequiredField" id="spnReqfortxtSrcDesc" runat="server">*</span>
                </div>
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:CustomValidator runat="server" Display="Dynamic" ValidationGroup="RegInternal"
                        ID="CVSourceDescription" ErrorMessage="Please Select Source Description" ClientValidationFunction="ValidateSourceDescription"></asp:CustomValidator>
                </div>
            </div>
        </div>
       
        
      
       
        <div id="divIDProof" runat="server">
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblIDProof" runat="server" Text="ID Proof"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:DropDownList ID="ddlIDProof" runat="server" CssClass="CommonDropDownList" Width="15%"
                        TabIndex="24">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtIDProof" runat="server" Width="130" CssClass="CommonTextBox"
                        TabIndex="25"></asp:TextBox>
                   <%--<span id="spnRequiredField" runat="server" class="RequiredField">*</span>--%>
                </div>
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <%--<asp:RequiredFieldValidator ID="rfvproof" runat="server" ControlToValidate="txtIDProof"
                        ErrorMessage="Please Enter ID Proof." ValidationGroup="RegInternal" EnableViewState="false" 
                       Display="Dynamic"></asp:RequiredFieldValidator>--%>
                </div>
            </div>
        </div>
        </div>
      </div>
    <div id="divApplicationAccess" runat="server" visible="false">
        <div class="TabPanelHeader">
            Application Access <span class="TabPanelHeadersmall">Choose an access role for the user.</span></div>
        <asp:UpdatePanel ID="updPnlRoleAccess" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="TableRow" style="text-align: center;">
                    <asp:Label EnableViewState="false" ID="lblMessageAssignRole" runat="server"></asp:Label>
                </div>
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblRole" runat="server" Text="Role"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                        <asp:DropDownList EnableViewState="true" ID="ddlRole" ValidationGroup="RoleAccess"
                            runat="server" Width="200" AutoPostBack="false" TabIndex="4" />
                    </div>
                </div>
                <div class="TableRow">
                    <div class="TableFormValidatorContent" style="margin-left: 42%">
                        <asp:RequiredFieldValidator ID="rfvRole" runat="server" ControlToValidate="ddlRole"
                            ErrorMessage="Please select a role." ValidationGroup="RegInternal" EnableViewState="False"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="TableRow">
            <div class="TableFormLeble">
                Password Option:</div>
            <div class="TableFormContent">
                <asp:DropDownList ID="ddlPassordOption" runat="server" TabIndex="5" CssClass="CommonDropdownList">
                    <asp:ListItem Value="0" Text="Set password now" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Generate random password and send by email"></asp:ListItem>
                </asp:DropDownList>
                <div class="alert alert-info" style="margin-top: 10px; width: 70%; display: none;
                    margin-bottom: 0px; margin-left: 15%;" id="divPasswordAlert">
                    An email will be sent to the user with a default password and application link.
                </div>
            </div>
        </div>
        <div id="divEmployeePassword">
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="lblEmpPassword" runat="server" Text="Password"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtEmployeePassword" runat="server" TabIndex="6" Width="200" CssClass="CommonTextBox"
                        TextMode="Password" rel="tooltipInputP" data-original-title="Password must be at least 8 characters long."
                        data-placement="right" value=""></asp:TextBox>
                    <span class="RequiredField">*</span>
                </div>
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator ID="rgvEmployeePassword" runat="server" ControlToValidate="txtEmployeePassword"
                        ErrorMessage="Please enter new password." EnableViewState="False" Display="Dynamic"
                        ValidationGroup="RegInternal">
                    </asp:RequiredFieldValidator>
                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmployeePassword"
                        Display="Dynamic" ErrorMessage=" Please enter at least 8 characters." EnableViewState="false"
                        ValidationGroup="RegInternal" ValidationExpression="^[\s\S]{8,}$"></asp:RegularExpressionValidator>--%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmployeePassword"
                    Display="Dynamic" ErrorMessage="Password must be greater than 8 characters long with at least one numeric,</br> one alphabet and one special character." EnableViewState="false"
                    ValidationGroup="RegInternal" ValidationExpression="(?=^.{8,30}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&*()_+}{:;'?/>.<,])(?!.*\s).*$"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormLeble">
                    <asp:Label EnableViewState="false" ID="Label2" runat="server" Text="Confirm Password"></asp:Label>:
                </div>
                <div class="TableFormContent">
                    <asp:TextBox ID="txtEmployeeConfirmPassword" runat="server" Width="200" CssClass="CommonTextBox"
                        TextMode="Password" TabIndex="7"></asp:TextBox>
                    <span class="RequiredField">*</span>
                </div>
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RequiredFieldValidator ID="rgvEmployeeConfirmPassword" runat="server" ControlToValidate="txtEmployeeConfirmPassword"
                        ValidationGroup="RegInternal" ErrorMessage="Please retype new password." EnableViewState="False"
                        Display="Dynamic">
                    </asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtEmployeeConfirmPassword"
                        Type="String" Operator="Equal" ControlToCompare="txtEmployeePassword" ErrorMessage="Passwords do not match."
                        ValidationGroup="RegInternal" EnableViewState="False" Display="Dynamic"></asp:CompareValidator>
                </div>
            </div>
        </div>
         <!-- Code introduced by Prasanth on 10/Jun/2016-->
             <div  id="divLDAP" runat="server" visible="false">
                <div class="TableRow">
                    <div class="TableFormLeble">
                        <asp:Label EnableViewState="false" ID="lblAdLogin" runat="server" Text="LDAP Authentication"></asp:Label>:
                    </div>
                    <div class="TableFormContent">
                       <asp:CheckBox ID="ChkLDAP" runat="server" />
                    </div>
                </div>
             </div>
            <!-- ********************END*********************-->
    </div>
    <div class="TableRow" id="divResumeOption" runat="server">
        <div id="divprofileBuilder" runat="server">
            <div class="TabPanelHeader">
                Profile Builder</div>
            <div class="TableFormLeble">
                Resume Option:
            </div>
            <div class="TableFormContent" style="margin-left: 43%">
                <asp:RadioButton ID="rdoUploadResume" runat="server" Text="Upload Resume" CssClass="UploadResumeoption"
                    GroupName="country" Checked="true" TabIndex="26" />
                <br />
                <asp:RadioButton ID="rdoCopyPaste" runat="server" Text="Copy/Paste Resume" CssClass="CopyPasteoption"
                    GroupName="country" TabIndex="27" />
                <br />
                <asp:RadioButton ID="rdoStepByStep" runat="server" Text="Step by Step Resume Builder"
                    CssClass="StepByStepoption" GroupName="country" TabIndex="28" /><br />
            </div>
        </div>
        <div id="divUploadResume" enableviewstate="true">
            <div class="TableRow">
                <div class="TableFormLeble">
                    Upload Resume:</div>
                <div class="TableFormContent">
                    <asp:FileUpload ID="fuDocument" size="40" runat="server" Style="float: left;" EnableViewState="true"
                        CssClass="CommonButton" TabIndex="29" />
                </div>
                <span class="RequiredField" id="spfile">*</span>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="margin-left: 42%">
                    <asp:RegularExpressionValidator ID="revfuDocument" runat="server" ErrorMessage="Invalid File Format"
                        ControlToValidate="fuDocument" ValidationExpression="^.+\.((doc)|(DOC)|(txt)|(TXT)|(pdf)|(PDF)|(rtf)|(RTF)|(docx)|(DOCX))$"
                        Display="Dynamic" ValidationGroup="RegInternal"></asp:RegularExpressionValidator>
                    <asp:CustomValidator ID="rqfCheck" runat="server" ValidationGroup="RegInternal" OnServerValidate="ValidateUploadFile"
                        ErrorMessage="Please Select Resume." Display="Dynamic"></asp:CustomValidator>
                </div>
            </div>
            <div class="TableRow">
                <div class="TableFormValidatorContent" style="clear: both; color: #888888; margin-left: 42%">
                    Supported File Formats: .doc, .docx, .pdf, .rtf, .txt</div>
            </div>
            <br />
        </div>
    </div>
    <div class="TableRow" id="divCopyPasteResume" style="display: none">
        <div class="TableFormLeble">
            Copy/Paste Resume:
        </div>
        <div class="TableFormContent">
            <asp:TextBox ID="txtCopyPasteResume" runat="server" Height="141px" TextMode="MultiLine"
                TabIndex="30" Width="404px" Style="float: left; margin-top: 10px; margin-bottom: 10px;"></asp:TextBox>
        </div>
    </div>
    <div class="TableRow">
        <div class="TableFormValidatorContent" style="margin-left: 42%">
            <asp:CustomValidator ID="cvCopyPaste" runat="server" ControlToValidate="txtCopyPasteResume"
                Display="Dynamic" ValidationGroup="RegInternal" ValidateEmptyText="true" ErrorMessage="Please enter resume text"
                ClientValidationFunction="ValidateCopyPaste"></asp:CustomValidator>
        </div>
    </div>
    <div id="dvRecaptcha" runat="server">
        <div class="TabPanelHeader" id="divCapthaHeader" runat="server" visible="true">
            One last step...</div>
        <div class="TableRow" id="divCaptcha" runat="server" visible="true">
            <div class="TableFormLeble">
                <font color="red">*</font>Verification :
            </div>
            <div style="margin-left: 42%;">
                <div class="TableFormContent">
                    <div runat="server" id="pbTarget" visible="true">
                    </div>
                 <%--   <recaptcha:RecaptchaControl ID="recaptcha" Theme="white" runat="server" PublicKey="6Lcm1b8SAAAAAKMG2RQaDtToGs-eY5Ja64j5X0g7"
                        PrivateKey="6Lcm1b8SAAAAAG9CkoHFnn7g6vWKiXI4piqMsIzh" AllowMultipleInstances="true" />--%>
                    <input type="checkbox" id="CheckBox1" style="width:1em; height:1em;" runat="server">&nbsp;
                    Terms and Conditions <input class="span2" type="image" style ="  width :20px; height :20px" src ="../Images/Ambox_warning_psycho.gif"  rel="tooltip" data-original-title="I am self-assured of my ability to work in a team with commitment and dedication and hereby declare that the information furnished above is true to the best of my knowledge." data-placement="right" value="" onclick ="javascript:return false;"   >                                                             
                </div>
                
            </div>
        </div>
        <div class="TableRow">
            <div class="TableFormValidatorContent" style="margin-left: 42%">
                <%--<asp:CustomValidator ID="cvRecaptcha" runat="server" OnServerValidate="CaptchaValidation"
                    ErrorMessage="The text that you entered is not correct. Please try again." ValidationGroup="RegInternal"></asp:CustomValidator>--%>
              <asp:CustomValidator ID="CustomValidator1" runat="server" ValidationGroup="RegInternal" ErrorMessage="Please check the checkbox to proceed" ClientValidationFunction = "ValidateCheckBox"></asp:CustomValidator>
            </div>
        </div>
    </div>
   
    <div class="TableRow">
        <div class="TableFormLeble">
            &nbsp;</div>
        <div class="TableFormContent">
            <div class="form-actions centered">
            <%--  ***************Button added by pravin khot on 28/Jan/2016 Using back to the career page********************--%>
               <asp:Button ID="btnbacktoCareer" runat="server" Text="Back" CssClass="btn btn-primary "
                      OnClick="btnbacktoCareer_Click"  ToolTip ="Back to the Career Page"
                    />
                   <%-- ****************************End****************************************--%>
                <asp:Button ID="btnSave" runat="server" Text="Submit" CssClass="btn btn-primary "
                    TabIndex="31" ValidationGroup="RegInternal" OnClick="btnSaveRegistration_Click"
                    OnClientClick="javascript:EmptyCheckUserLabel()" UseSubmitBehavior="true" />
                <asp:Button ID="btnCancel" runat="server" CssClass="CommonButton" Text="Cancel" OnClick="btnCancel_Click"
                    Visible="false" />
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="upButton" runat="server">
        <ContentTemplate>
            <div class="contentContainer">
                <asp:UpdateProgress ID="UpdateProgress1" EnableViewState="false" runat="server" DisplayAfter="10">
                    <ProgressTemplate>
                        <center>
                            <asp:Image runat="server" ID="imgLogo1" ImageUrl="~/Images/progress.gif" />
                        </center>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
