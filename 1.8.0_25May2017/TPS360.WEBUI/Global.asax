<%--
/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   Global.asax
    Description         :   
    Created By          :   
    Created On          :   
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 24/Dec/2015         Prasanth Kumar      Introduced Interviewfeedback form authorization for all users   
    0.2                 15/Feb/2016         Pravin Khot         Introduced careerpage form authorization for all users 
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
--%>

<%@ Application Language="C#"%>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Reflection" %>
<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System" %>

<%@ Import Namespace="TPS360.Common" %>
<%@ Import Namespace="TPS360.Common.BusinessEntities" %>
<%@ Import Namespace="TPS360.Common.Helper" %>
<%@ Import Namespace="TPS360.BusinessFacade" %>
<%@ Import Namespace="TPS360.Web.UI" %>
<%@ Import Namespace="TPS360.Web.UI.Helper" %>
<%@ Import Namespace="TPS360.Common.Shared" %>
<%@ Import Namespace="System.Web.SessionState" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Import Namespace="System.Web.Configuration" %>


<script runat="server">
    bool aa;
    bool IsPublic;
    void Application_Start(object sender, EventArgs e)
    {
        EnsureRole(ContextConstants.ROLE_ADMIN);
        CreateDefaultUserIfNotExists();
    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {
        //Response.AppendHeader("X-UA-Compatible", "IE=EmulateIE7"); //10759
        //if (Request.HttpMethod == "GET")
        //{
        //    if (Request.AppRelativeCurrentExecutionFilePath.EndsWith(".aspx") && (Request.QueryString["DDL"].IsNullOrEmpty()))
        //    {
        //        Response.Filter = new ScriptDeferFilter(Response);
        //    }
        //}
        HttpRuntimeSection runTime = (HttpRuntimeSection)WebConfigurationManager.GetSection("system.web/httpRuntime");
        int maxRequestLength = (runTime.MaxRequestLength - 100) * 1024;
        HttpContext context = ((HttpApplication)sender).Context;
        if (context.Request.ContentLength > maxRequestLength)
        {
            IServiceProvider provider = (IServiceProvider)context;
            HttpWorkerRequest workerRequest = (HttpWorkerRequest)provider.GetService(typeof(HttpWorkerRequest));

            if (workerRequest.HasEntityBody())
            {
                int requestLength = workerRequest.GetTotalEntityBodyLength();
                int initialBytes = 0;
                if (workerRequest.GetPreloadedEntityBody() != null)
                    initialBytes = workerRequest.GetPreloadedEntityBody().Length;
                if (!workerRequest.IsEntireEntityBodyIsPreloaded())
                {
                    byte[] buffer = new byte[512000];
                    int receivedBytes = initialBytes;
                    while (requestLength - receivedBytes >= initialBytes)
                    {
                        initialBytes = workerRequest.ReadEntityBody(buffer, buffer.Length);
                        receivedBytes += initialBytes;
                    }
                    initialBytes = workerRequest.ReadEntityBody(buffer, requestLength - receivedBytes);
                }
            }
            context.Response.Redirect(this.Request.Url.Segments [0]+this .Request .Url .Segments [1]+"ErrorPage.aspx"+ "?Exception=UploadFileSizeExceedMax.Limit. ");
            
            

}
    }
    
    protected void Session_Start(object sender, EventArgs e)
    {
        string s = Request.RawUrl;
        // 10531 starts
        string strFilePath = HttpContext.Current.Request.FilePath.ToString();
        string[] arrPublicPages = ConfigurationSettings.AppSettings["PublicPages"].ToString().Split(',');
        IsPublic = false;
        
        for (int i = 0; i < arrPublicPages.Length; i++)
        {
            if (strFilePath.Contains(arrPublicPages[i].ToString()))
            {
                IsPublic = true;
            }
        }
        Session["Loggedin"] = null;
        CheckLoggedIn();

        string ApplicationSource = WebConfigurationManager.AppSettings["ApplicationSource"].ToString();
        if (ApplicationSource.ToLower() == "genisys")
        {


            if (!IsPublic)
            {
                //Response.Redirect("Login.aspx");
                TPS360SectionHandler licenseSection = WebConfigurationManager.GetSection("tps360license") as TPS360SectionHandler; ;
                if (licenseSection != null)
                {
                    try
                    {
                        System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                        System.Globalization.DateTimeFormatInfo info = new System.Globalization.DateTimeFormatInfo();
                        info.ShortDatePattern = "MM/dd/yyyy";
                        culture.DateTimeFormat = info;

                        System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                        System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
                        string LicenseKey = (licenseSection.Details["LicenseKey"].Value != null) ? licenseSection.Details["LicenseKey"].Value : String.Empty;
                        DateTime StartDate = Convert.ToDateTime(licenseSection.Details["StartDate"].Value);
                        DateTime ExpireDate = Convert.ToDateTime(licenseSection.Details["ExpireDate"].Value);
                        DateTime currentDate = DateTime.Now;
                        if (LicenseKey != null && ExpireDate != null && StartDate != null) //to handle if there are no details
                        {
                            DateTime MaxDate = ExpireDate.AddDays(1);
                            if (StartDate <= currentDate && MaxDate >= currentDate)
                            {
                                if (!Request.RawUrl.ToLower().Contains("login.aspx") && !Request.RawUrl.ToLower().Contains("employeereferral.aspx") && !Request.RawUrl.ToLower().Contains("interviewerfeedback.aspx") && !Request.RawUrl.ToLower().Contains("careerpage.aspx"))  //careerpage.aspx added by pravin khot on 15/Jan/2016
                                    if (!Response.IsRequestBeingRedirected)
                                        Response.Redirect("Login.aspx");
                            }
                            else
                            {
                                DeleteLicenseDetails(); //11534
                                Response.Redirect("StartUp.aspx");
                            }
                        }


                    }
                    catch (FormatException ex)
                    {
                        DeleteLicenseDetails();//11534
                        Response.Redirect("StartUp.aspx");
                    }
                    catch (NullReferenceException ex)
                    {
                        DeleteLicenseDetails();//11534
                        Response.Redirect("StartUp.aspx");
                    }
                }
                else
                {
                    Response.Redirect("StartUp.aspx");
                }
            }
            //10628

        }
    } //10493
  
    void Application_AcquireRequestState(object sender, EventArgs e)
    {
        
        HttpContext context = HttpContext.Current;

        IFacade facade = context.Items[ContextConstants.Facade] as IFacade;

        if (facade == null)
        {
            facade = MiscUtil.CreateFacade();
            context.Items[ContextConstants.Facade] = facade;
        }

        if (context.User.Identity.IsAuthenticated)
        {
            Member member = context.Items[ContextConstants.MEMBER] as Member;
            
            if(member == null)
            {
                member = facade.GetMemberByUserName(context.User.Identity.Name );
                context.Items[ContextConstants.MEMBER] = member;
            }

            SiteSetting siteSetting = facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);

            if (context.Items[ContextConstants.SITESETTING] == null)
            {
                if (siteSetting != null)
                {
                    context.Items[ContextConstants.SITESETTING] = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);

                    Country country = facade.GetCountryById(Convert.ToInt32(ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration)[DefaultSiteSetting.Country.ToString()]));
                    if (country != null)
                    {
                        if (country.Name == "India")
                        {
                            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                            System.Globalization.DateTimeFormatInfo info = new System.Globalization.DateTimeFormatInfo();
                            info.ShortDatePattern = "MM/dd/yyyy";
                            culture.DateTimeFormat = info;

                            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
                        }
                    }
                }
            }
        }
       
    }

    void Application_ReleaseRequestState(object sender, EventArgs e)
    {
        IFacade facade = HttpContext.Current.Items[ContextConstants.Facade] as IFacade;

        if (facade != null)
        {
            facade.Dispose();
        }
    }
    
    void Application_End(object sender, EventArgs e)
    { 
    }

    protected void Session_End(object sender, EventArgs e)
    {
       
    }
   

    void Application_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError().GetBaseException();
        //Logger.LogUnhandledException(ex);
        //Server.ClearError();
    }

    private static void EnsureRole(string roleName)
    {
        try
        {
            if (!Roles.RoleExists(roleName))
            {
                Roles.CreateRole(roleName);
            }
        }
        catch
        {
        }
    }
    private void CheckLoggedIn()
    {
        if (Request.RawUrl.IndexOf("Dashboard.aspx") >= 1)
        {
            Response.Redirect(Request.RawUrl);
            return;
        }
        if (Request.RawUrl.ToLower().Contains("employeereferral.aspx")) return;
        if (Request.RawUrl.ToLower().Contains("/vendor/")) return;
        if (Request.RawUrl.ToLower().Contains("interviewerfeedback.aspx")) return; //Code introduced by Prasanth on 24/Dec/2015
        if (Request.RawUrl.ToLower().Contains("careerpage.aspx")) return; //Code introduced by Pravin khot on 15/Jan/2016
        
        if ((Session["LoggedIn"] == null) && (Request.RawUrl.IndexOf("Login.aspx") == -1) && !IsPublic)
        {
            if (Request.RawUrl.ToLower().Contains("login.aspx"))
                Response.Redirect("~/Login.aspx");
            else if (Request.RawUrl.ToLower().Contains("employeereferral.aspx")) return;
            else if (Request.RawUrl.ToLower().Contains("/vendor/")) return;
            else if (Request.RawUrl.ToLower().Contains("interviewerfeedback.aspx")) return; //Code introduced by Prasanth on 24/Dec/2015
            else if (Request.RawUrl.ToLower().Contains("careerpage.aspx")) return; //Code introduced by Pravin khot on 15/Jan/2016
            else
            {
                Response.Redirect("~/Login.aspx?SeOut=SessionOut", true);
            }
        }
    }
    
    private static void CreateDefaultUserIfNotExists()
    {
        const string DefaultUser = "adminn";
        const string DefaultPassword = "adminn";
        const string DefaultEmail = "admin@ecm.com";
        if (Membership.GetUser(DefaultUser) != null) return;
        Membership.CreateUser(DefaultUser, DefaultPassword, DefaultEmail);
    }

    private void DeleteLicenseDetails()//11534
    {
        Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
        TPS360SectionHandler license = config.GetSection("tps360license") as TPS360SectionHandler;
        TPS360ConfigElement lkey= new TPS360ConfigElement("LicenseKey","");
        TPS360ConfigElement sdate= new TPS360ConfigElement("StartDate","");
        TPS360ConfigElement expdate= new TPS360ConfigElement("ExpireDate","");
        TPS360ConfigElement noofusersallowed = new TPS360ConfigElement("NoofUsersAllowed", "");
        license.Details.Remove(lkey);
        license.Details.Remove(sdate);
        license.Details.Remove(expdate);
        license.Details.Remove(noofusersallowed);
        config.Save(ConfigurationSaveMode.Modified);
        ConfigurationManager.RefreshSection("tps360license"); 
    }
</script>