USE [LnTIJP]
GO
/****** Object:  StoredProcedure [dbo].[JobPosting_GetReport]    Script Date: 10/07/2015 12:13:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








ALTER PROCEDURE [dbo].[JobPosting_GetReport]  
 @StartRowIndex int,  
 @RowPerPage  int,  
 @WhereClause nvarchar(MAX),  
 @SortColumn  nvarchar(128),  
 @SortOrder  nvarchar(4)  
  
--WITH ENCRYPTION  
AS
BEGIN   
  
SET @StartRowIndex = isnull(@StartRowIndex, -1)  
SET @RowPerPage = isnull(@RowPerPage, -1)  
SET @WhereClause = isnull(@WhereClause, '')  
SET @SortColumn = isnull(@SortColumn, '')  
SET @SortOrder = isnull(@SortOrder, '')  
declare @sC varchar(200)  

DECLARE @SQL nvarchar(MAX)  
DECLARE @PagingClause nvarchar(512)  

    if(CHARINDEX('level', @SortColumn)>0)
		begin
			 set @SortColumn= replace(@SortColumn,'level','')
			  if(@sortOrder='asc' or @SortOrder='') set @SortOrder='desc'
			  else set @SortOrder='asc'
		end
if(charindex('[JPTAT].',@SortColumn)>0)
begin
	if(@sortOrder='asc' or @SortOrder='') set @SortOrder='desc'
			  else set @SortOrder='asc'
end
set @sC=@SortColumn  
IF (@WhereClause != '')  
BEGIN  
 SET @WhereClause = 'WHERE ' + char(13) + @WhereClause  
END  
if(@SortColumn='NoOfUnfilledOpenings')
begin
	set @sc=' cast((NoOfOpenings-cast(isnull(JoiningCount,0) as int)) as int) '
end 
if(@SortColumn='[J].[InternalNote]')
begin
	set @sc='cast([J].[InternalNote] as varchar(max))'
end
IF (@SortColumn != '')  
BEGIN  
 SET @SortColumn = 'ORDER BY ' + @SortColumn  
  
 IF (@SortOrder != '')  
 BEGIN  
  SET @SortColumn = @SortColumn + ' ' + @SortOrder  
 END  
END  
ELSE  
BEGIN  
 SET @SortColumn = @SortColumn + ' ORDER BY [J].[JobTitle] ASC'  
END  
  
IF ((@RowPerPage > -1) AND (@StartRowIndex > -1))  
BEGIN    
 SET @PagingClause = 'WHERE Row between '+ CONVERT(nvarchar(10), @StartRowIndex) +'And ('+ CONVERT(nvarchar(10), @StartRowIndex) +' + '+ CONVERT(nvarchar(10), @RowPerPage) +')'  
END  
declare @hiringLevel varchar(max)  
set @hiringLevel=''  
declare @n int  
declare @i int  
set @i=0  
select @n =max(sortorder) from hiringmatrixlevels  
while(@i<=@n)  
begin  
  
 if(@hiringLevel<>'' and substring(@hiringLevel,len(@hiringLevel),1)<>',') set @hiringLevel=@hiringLevel+','  
 select  @hiringLevel=@hiringLevel +'['+ cast(ID as varchar(10)) + ']' from hiringmatrixlevels where sortorder=@i  

   set @i= @i + 1  
end  

  
  
  
  
if(isnumeric(substring(@sc,2,len(@sC)-2))=1)  
begin  
set @SQL='WITH JobPostingEntries AS ( select  ROW_NUMBER() OVER ('+@sortColumn+')AS Row ,jobpostingid,ReqStageName,'+  @hiringLevel + '   from  (         
SELECT '+@sC + ' as sortColumn , H.ID,MJD.ID as IDs,j.id as jobpostingid ,CASE WHEN [AST].IsStateApprovedByUser=1 THEN [AE].StageName ELSE ''Pending Approval for Stage '' + [AE].StageName END as ReqStageName 
 FROM [JobPosting] AS [J]'  
    
end  
else if(@sc='[J].[Name]')  
begin  
 set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by Name  ' + @sortOrder + ')AS Row ,jobpostingid,ReqStageName,'+  @hiringLevel + '   from  (         
SELECT GL.Name as Name   , H.ID,MJD.ID as IDs,j.id as jobpostingid ,CASE WHEN [AST].IsStateApprovedByUser=1 THEN [AE].StageName ELSE ''Pending Approval for Stage '' + [AE].StageName END as ReqStageName  
 FROM [JobPosting] AS [J] Join GenericLookup [GL] on [GL].[ID] = [J].[JobStatus]'  
end  
else if(@sc='[J].[MinExpRequired]')  
begin  
 set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by MinExp  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (         
SELECT case when [J].[MinExpRequired]='''' then -1 else  cast( [J].[MinExpRequired] as decimal(15,3))  end AS MinExp    , H.ID,MJD.ID as IDs,j.id as jobpostingid  
 FROM [JobPosting] AS [J] '  
end  
else if(@sc='[J].[MaxExpRequired]')  
begin  
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by MaxExp  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (         
SELECT case when [J].[MaxExpRequired]='''' then -1 else  cast( [J].[MaxExpRequired] as decimal(15,3))  end AS MaxExp   , H.ID,MJD.ID as IDs,j.id as jobpostingid  
 FROM [JobPosting] AS [J] '  
end  
else if(@sc='[J].[ClientHourlyRate]')  
begin  
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by rate  ' + @sortOrder + ', Cycle
 '+@sortOrder+' 
)AS Row ,jobpostingid,'+  @hiringLevel + '   from  (         
SELECT case when [J].[ClientHourlyRate]='''' then -1 else  cast( [J].[ClientHourlyRate] as decimal(15,3))  end AS rate,


case When [J].[ClientRatePayCycle] =0 then '' ''
when [J].[ClientRatePayCycle] =1 then ''Hourly''
when [J].[ClientRatePayCycle] =2 then ''Daily''
when [J].[ClientRatePayCycle] =3 then ''Monthly''
when [J].[ClientRatePayCycle] =4 then ''Yearly''
 end as Cycle

   , H.ID,MJD.ID as IDs,j.id as jobpostingid  
 FROM [JobPosting] AS [J] '  
end  


else if(@sc='[J].[PayRate]')  
begin  
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by rate  ' + @sortOrder + ',Cycle '+@sortOrder+' )
AS Row ,jobpostingid,'+  @hiringLevel + '   from  (         
SELECT case  when [J].[PayRate]='''' or  [J].[PayRate] =''open'' then -1 else 
cast ([J].[PayRate] as decimal(18,3)) end as rate,

case When [J].[PayCycle] =0 then '' ''
when [J].[PayCycle] =1 then ''Hourly''
when [J].[PayCycle] =2 then ''Daily''
when [J].[PayCycle] =3 then ''Monthly''
when [J].[PayCycle] =4 then ''Yearly''
 end as Cycle


 , H.ID,MJD.ID as IDs,j.id as jobpostingid  
 FROM [JobPosting] AS [J] left join Company [C] on [J].[ClientId]=[C].[Id]'  
end  



else if(@sc='[J].[ClientId]')  
begin  
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by CompanyName  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (         
SELECT C.CompanyName  , H.ID,MJD.ID as IDs,j.id as jobpostingid  
 FROM [JobPosting] AS [J] left join Company [C] on [J].[ClientId]=[C].[Id]'  
end  
  
else if(@sc='[CC].[FirstName]')  
begin  
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by FirstName  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (         
SELECT C.FirstName  , H.ID,MJD.ID as IDs,j.id as jobpostingid  
 FROM [JobPosting] AS [J] left join CompanyContact [C] on [J].[ClientContactId]=[C].[Id]'  
end  

  
else if(@sc='[J].[CreatorId]')  
begin  
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by name  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (         SELECT M.FirstName as name  , H.ID,MJD.ID as IDs,j.id as jobpostingid  
 FROM [JobPosting] AS [J] left join Member [M] on [J].[CreatorId]=[M].[Id]'  
end  
  
else if(@sc='[J].[UpdatorId]')  
begin  
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by name  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (         
SELECT M.FirstName as name  , H.ID,MJD.ID as IDs,j.id as jobpostingid  
 FROM [JobPosting] AS [J] left join Member [M] on [J].[UpdatorId]=[M].[Id]'  
end  
  
else if(@sc='[J].[dateDifferce]')  
begin  
 set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by dateDifferce  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (         
SELECT  DATEDIFF (ww ,[J].[ActivationDate] , [J].[FinalHiredDate] ) as dateDifferce   , H.ID,MJD.ID as IDs,j.id as jobpostingid  
 FROM [JobPosting] AS [J]'  
end  
  
else if(@sc='[J].[JobCategoryLookupId]')  
begin  
 set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by JobCategoryLookupId  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (         
SELECT  [CAT].[Name] as JobCategoryLookupId    , H.ID,MJD.ID as IDs,j.id as jobpostingid  
 FROM [JobPosting] AS [J] left join [Category] [CAT] on [J].[JobCategoryLookupId]=[CAT].[ID]'  
end  
else if(@sc='[WorkScheduleLookupId]' or @sc='[JobLocationLookUpId]' or @sc='[SalesGroupLookup]' or @sc='[SalesRegionLookup]' or @sc='[JobCategoryLookupId]' )  
begin  
 set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by '+@sc+'  ' + @sortOrder + ')AS Row ,jobpostingid,'+  @hiringLevel + '   from  (         
SELECT  isnull([GL].[Name],'''') as '+@sc+'    , H.ID,MJD.ID as IDs,j.id as jobpostingid  
 FROM [JobPosting] AS [J] left join [GenericLookup] [GL] on  [J].'+@sc+'=[GL].[ID]'  
end  


else  
begin  
set @SQL='WITH JobPostingEntries AS (select  ROW_NUMBER() OVER (order by sortColumn  ' + @sortOrder + ')AS Row ,jobpostingid,ReqStageName,'+  @hiringLevel + '   from  (         
SELECT '+@sC + ' as sortColumn , H.ID,MJD.ID as IDs,j.id as jobpostingid ,CASE WHEN [AST].IsStateApprovedByUser=1 THEN [AE].StageName ELSE ''Pending Approval for Stage '' + [AE].StageName END as ReqStageName 
 FROM [JobPosting] AS [J]'  
   
end             
  set @SQL=@SQL+'     
                 
               left  JOIN [State] [S] ON [J].[StateId]=[S].[Id]   
                        left  JOIN [GenericLookup] [G] ON [G].[ID] = [J].[JobStatus]  
                       left join MemberJobCart [MJ] on mj.jobpostingid=j.id  
      left join MemberJobCartDetail MJD  on MJD.MemberJobCartId = MJ.Id and   MJD.IsRemoved = 0  and MJ.IsRemoved=0
	  left join [JobPostingTurnArroundTime] [JPTAT] on [J].[Id]=[JPTAT].[Id]
      Left Join [vwJobPostingInterviewCount] [JIC] on [J].[Id]=[JIC].[JobPostingID]
	  Left Join [vwJobPostingJoiningDetailsCount] [JJDC] on [J].[Id]=[JJDC].[JobPostingID]
      Left Join [vwJobPostingOfferDetailsCount] [JODC]  on [J].[Id]=[JODC].[JobPostingID]
      Left Join [vwJobPostingSubmissionCount] [JSDC]   on [J].[Id]=[JSDC].[JobPostingID]
      left join HiringMatrixLevels H on H.ID=MJD.SelectionStepLookupId 
	  left join [ApprovalStageTransition] [AST] on [AST].jobpostingId=j.id AND [AST].IsRemoved=0
	  left join [ApprovalEditor] [AE] on [AE].id=[AST].CurrentApprovalStageId 
	  left join [GenericLookUp] [GLSR] on [GLSR].Id=[J].[SalesRegionLookup]
	  left join [Company] [COM] On [COM].Id=[J].ClientId 
    '+ @WhereClause +'  )p  
PIVOT  
(  
COUNT (IDs)  
FOR ID in( ' + @hiringLevel+ ')  
) AS pvt   
    )  
    SELECT   
		[J].[Id],  
		[JobTitle],  
		[JobPostingCode],  
		[ClientJobId],   
		[NoOfOpenings],  
		[PayRate],  
		[PayRateCurrencyLookupId],  
		[PayCycle],  
		[TravelRequired],  
		[TravelRequiredPercent],  
		[OtherBenefits],  
		[JobStatus],  
		[JobDurationLookupId],  
		[JobDurationMonth],  
		[JobAddress1],  
		[JobAddress2],  
		[J].[City],  
		[J].[ZipCode],  
		[J].[CountryId],  
		[J].[StateId],  
		[StartDate],  
		[FinalHiredDate],  
		[JobDescription],  
		[AuthorizationTypeLookupId],  
		[PostedDate],  
		[ActivationDate],  
		[RequiredDegreeLookupId],  
		[MinExpRequired],  
		[MaxExpRequired],  

		[IsJobActive],  

		[JobType],  
		[IsApprovalRequired],  
		[InternalNote],  
		[ClientId],  

		[ClientHourlyRate],  
		[ClientHourlyRateCurrencyLookupId],  
		[ClientRatePayCycle],  

		[TaxTermLookupIds],  
		[JobCategoryLookupId],
		[ExpectedRevenue],  
		[ExpectedRevenueCurrencyLookupId],  
		[WorkflowApproved],  
		[TeleCommunication],  
		[IsTemplate],  
		[IsRemoved],  
		[J].[CreatorId],  
		[J].[UpdatorId],  
		[J].[CreateDate],  
		[J].[UpdateDate],  
		[IsExpensesPaid],  
		[RawDescription],  
		[JobDepartmentLookupId],  
		[SkillLookupId],
		[SecSkillLookupId],  
		[OccupationalSeriesLookupId],  
		[PayGradeLookupId],  
		[WorkScheduleLookupId],  
		[SecurityClearance]      , 
		[ClientContactID], '
declare @SQL1 varchar(max)

		SET @SQL1='
		[ReportingTo],
		[NoOfReportees],
		[MinimumQualifyingParameters]   ,
		[MaxPayRate],
		[AllowRecruitersToChangeStatus],[ShowInCandidatePortal],[ShowInEmployeeReferralPortal],[DisplayRequisitionInVendorPortal],
		[OpenDate],
		[SalesRegionLookUp],
		[SalesGroupLookUp],
		[CustomerName],
		[POAvailability],
		[JobLocationLookUpID],
		DBO.GetGenericLookupNameById([JobLocationLookUpID]),
		DBO.GetSalesGroupRegionNameById([SalesRegionLookUp]) AS SalesRegion,
		DBO.GetSalesGroupNameById([SalesGroupLookUp])AS SalesGroup,
		CC.FirstName + '' '' + CC.LastName,
		DBO.GetRequirementCategoryNameById([JobCategoryLookupId]),
        [EmployementTypeLookUpID],
		[ClientBrief],
		[IndustryGroupId],
		[newEmploymentTypeLookUpId],
		[ProjectDuration],
		[ProjectName],
		[RevisedFulfillmentDate],
		isnull([TurnArroundTime],0) as TurnArroundTime,
		cast(isnull( SubmissionCount,0) as int) as SubmissionCount,
		cast(isnull(OfferCount,0) as int) as OfferCount,
		cast(isnull(JoiningCount,0) as int) as JoiningCount,
		cast(isnull(InterviewCount,0) as int) as InterviewCount,
		cast((NoOfOpenings-cast(isnull(JoiningCount,0) as int)) as int) as NoOfUnfilled,
		[RequisitionSource],  
		[EmailSubject],  
		dbo.GetInterviewCandidateName(J.id),
	  dbo.GetSubmittedCandidateName(J.id),
	  dbo.GetOfferedCandidateName(J.id),
	  dbo.GetJoinedCandidateName(J.id),
	  [J].InternalNote,
		[OpportunityID],
		[Remarks],
		[OpportunityName],
		[J].[RequestTypeLookupId],
		J.[EngineeringGroupLookupid],
		J.[DomainLookupid],
		J.[ToolLookupid],
		J.[FunctionalLookupid],
		J.[DomainSkillId],
		J.[ToolSkillId],
		J.[FunctionSkillId],
		'''',
		CustomerNameLookUpId,
		[SalesContact],J.[ProjectDurationLookupId],ReqStageName,[dbo].[GetTAAssignedDateByJobpostingId](J.id),[J].NewPayRate,[dbo].[GetRMGAssignedDateByJobpostingId](J.id) as RMGAssingedDate,[dbo].[GetPEAssignedDateByJobpostingId](J.id) as PEAssinedDate,[dbo].[GetPendingDateByJobpostingId](J.id) as PendingDate,[dbo].[GetTATReasonForApprovalByJobPostingId](J.id),
		[dbo].[GetOfferedCandidateNameWithEDOJ](J.id),
		
                  ' + @hiringLevel 
Declare @SQL2 varchar(max)
set @SQL2=
 '   
                      
	from  JobPosting  J   
	join  JobPostingEntries   JE on  J.ID= JE.JobPostingID 
	left join [JobPostingTurnArroundTime] [JPTAT] on [J].[Id]=[JPTAT].[Id]
	Left Join [vwJobPostingInterviewCount] [JIC] on [J].[Id]=[JIC].[JobPostingID]
	Left Join [vwJobPostingJoiningDetailsCount] [JJDC] on [J].[Id]=[JJDC].[JobPostingID]
	Left Join [vwJobPostingOfferDetailsCount] [JODC]  on [J].[Id]=[JODC].[JobPostingID]
	Left Join [vwJobPostingSubmissionCount] [JSDC]   on [J].[Id]=[JSDC].[JobPostingID]
	Left Join CompanyContact CC on [J].[ClientContactID]=[CC].[Id]
--    Left Join jobpostingReasonForApproval JR on [JR].[JobPostingId]=[J].[Id] and [JR].StageId=8 

'+case when (@StartRowIndex=-1 And @RowPerPage=-1) then '' else 'WHERE  Row between '+ CONVERT(nvarchar(10), @StartRowIndex+1)+'And ('+ CONVERT(nvarchar(10), @StartRowIndex) +' + '+ CONVERT(nvarchar(10), @RowPerPage) +') 'end  

EXEC ( @SQL  + @SQL1 +@SQL2)
--print @SQL  + @SQL1 + @SQL2
PRINT @SQL
print @SQL1
print @SQL2
SET @SQL = 'SELECT COUNT([J].[Id])     
   FROM [JobPosting] AS [J]  
   LEFT JOIN [State] [S] ON [J].[StateId]=[S].[Id]
   left join [ApprovalStageTransition] [AST] on [AST].jobpostingId=[J].Id AND [AST].IsRemoved=0
	left join [ApprovalEditor] [AE] on [AE].id=[AST].CurrentApprovalStageId 
   ' + @WhereClause  
  
EXEC sp_executesql @SQL  
  
END

