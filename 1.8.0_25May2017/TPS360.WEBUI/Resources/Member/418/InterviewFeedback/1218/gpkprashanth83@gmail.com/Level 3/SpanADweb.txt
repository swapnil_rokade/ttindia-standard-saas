<?xml version="1.0"?>
<configuration>
  <configSections>
    <sectionGroup name="system.web.extensions" type="System.Web.Configuration.SystemWebExtensionsSectionGroup, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
      <sectionGroup name="scripting" type="System.Web.Configuration.ScriptingSectionGroup, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
        <section name="scriptResourceHandler" type="System.Web.Configuration.ScriptingScriptResourceHandlerSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication"/>
        <sectionGroup name="webServices" type="System.Web.Configuration.ScriptingWebServicesSectionGroup, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
          <section name="jsonSerialization" type="System.Web.Configuration.ScriptingJsonSerializationSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="Everywhere"/>
          <section name="profileService" type="System.Web.Configuration.ScriptingProfileServiceSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication"/>
          <section name="authenticationService" type="System.Web.Configuration.ScriptingAuthenticationServiceSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication"/>
          <section name="roleService" type="System.Web.Configuration.ScriptingRoleServiceSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication"/>
        </sectionGroup>
      </sectionGroup>
    </sectionGroup>
    <section name="tps360license" type="TPS360.Common.TPS360SectionHandler,TPS360.Common" allowLocation="true" allowDefinition="Everywhere" allowExeDefinition="MachineToApplication" overrideModeDefault="Allow" restartOnExternalChanges="true" requirePermission="true" />
    <section name="dataConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Data.Configuration.DatabaseSettings, Microsoft.Practices.EnterpriseLibrary.Data"/>
    <section name="dataAccessFactoryConfiguration" type="TPS360.DataAccess.Configuration.ConfigurationSectionHandler, TPS360.IDataAccess, Version=1.0.0.0, Culture=neutral"/>
    <section name="codeBenchmark" type="System.Configuration.SingleTagSectionHandler"/>
    <section name="scriptSettings" type="System.Configuration.SingleTagSectionHandler, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"/>
    <section name="commonSetting" type="TPS360.Common.ConfigurationSectionHandler, TPS360.Common"/>
    <section name="TinyMCE" type="Moxiecode.TinyMCE.ConfigHandler,Moxiecode.TinyMCE" requirePermission="false"/>
    <section name="loggingConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.LoggingSettings, Microsoft.Practices.EnterpriseLibrary.Logging, Version=3.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" allowLocation="true" allowDefinition="Everywhere" allowExeDefinition="MachineToApplication" overrideModeDefault="Allow" restartOnExternalChanges="true" requirePermission="true"/>

  </configSections>

  <tps360license configProtectionProvider="RsaProtectedConfigurationProvider">
    <EncryptedData Type="http://www.w3.org/2001/04/xmlenc#Element"
      xmlns="http://www.w3.org/2001/04/xmlenc#">
      <EncryptionMethod Algorithm="http://www.w3.org/2001/04/xmlenc#tripledes-cbc" />
      <KeyInfo xmlns="http://www.w3.org/2000/09/xmldsig#">
        <EncryptedKey xmlns="http://www.w3.org/2001/04/xmlenc#">
          <EncryptionMethod Algorithm="http://www.w3.org/2001/04/xmlenc#rsa-1_5" />
          <KeyInfo xmlns="http://www.w3.org/2000/09/xmldsig#">
            <KeyName>Rsa Key</KeyName>
          </KeyInfo>
          <CipherData>
            <CipherValue>PLATCpar6nb8uyG0ahT4QY9SkLg3dOIlZv4eeIFbGIq5+xZSH9TS3l/j7LORkQ6Xj8aDl+vcd26TWwa1fv+rNgOiLeOEzgS4TCuoAm2OGFZk9hUTa3Fn5sSJo39mKfLNK8iEyVSHuB8ZZGZ9ixrcvH//yCNRxDhUr69Bss85Xvh2ZzQhFGiEPK5ND5dgTdpk7OrlTzACAbyYY5PFdh9/lmhybqit84wjt7ECsG+goFI2AlCaOiXWOCZ8HhaSqqsmngPDROKidJv4dWNcjEaVwel32j1kSowKum5IL4aHz5Z+3MMmxAkV72Z3dvFy/VC16TGjI32Pzapjo2aMIplp6A==</CipherValue>
          </CipherData>
        </EncryptedKey>
      </KeyInfo>
      <CipherData>
        <CipherValue>NpPpUq8UTSTrikYfFxXX5EkOga/RuK7eGiJLhEb32dWVxuFfZWAnJ4ZDNBtyvxp3zeOO80QONCOubKR3FJrv/EPSQ1OGvQt9XSrWaVZz9IiQ3MDmMgLefS+xaz//WJ3wS9Vvo0vaZGY2hteGbshWaHOjWWr5Vr/6mq4X+BO5ZBgd894AbUhc/o/6ovuR+R8+adquEe+ejqcTJEU+Kwc651vfesY635TEXsA56RPbWfts1Oy3qfyrA3YTOC8ppqpYcqhyMXcXsb7nMHsEEwBdHJ8LwjtcyOlpxMA5Eha5NCqVty8rx+dDQQuuBNTYDCr3xfXuhdMVVzbz8B3lv9eyXMR0T+A2rGWP+dkNTgBmD8a3cUamXQRV/5jnJbZv/P0EPouaaS+/3pUcz6hiqn9haOvaWsdBzShAvk7pAiOKv8nxuiloAhsKpR2QuSQFP2IQfbj4jO8S+34PVw6AMN+Y+3kZ2DHssqSGzqDf9MfUomod/ZHtu13sljE9vgWBpMRtIX6c0i/WrVkrs9b3JJ4wLReTKzYId0aesvKQh+JrkYOHLGI9MPurrT8bR8efjhPa/LnXklthHIHrI55S5JROwwBBgr73Zhj7c8SZ4xjQuig8Fq+T8Qt3DAX5Fgvu+T2OYyL35VAoAId7qCQwaQIR69j5Voibah80iNivOWsj3TCFhoEJuoQU4g==</CipherValue>
      </CipherData>
    </EncryptedData>
  </tps360license>
  <loggingConfiguration name="Logging Application Block" tracingEnabled="true" defaultCategory="General" logWarningsWhenNoCategoriesMatch="true">
    <listeners>
      <add source="TPS360 Log" formatter="Text Formatter" log="Application" machineName="" listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.FormattedEventLogTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging, Version=3.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" traceOutputOptions="None" type="Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners.FormattedEventLogTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging, Version=3.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" name="Formatted EventLog TraceListener"/>
    </listeners>
    <formatters>
      <add template="Timestamp: {timestamp}&#xA;Message: {message}&#xA;Category: {category}&#xA;Priority: {priority}&#xA;EventId: {eventid}&#xA;Severity: {severity}&#xA;Title:{title}&#xA;Machine: {machine}&#xA;Application Domain: {appDomain}&#xA;Process Id: {processId}&#xA;Process Name: {processName}&#xA;Win32 Thread Id: {win32ThreadId}&#xA;Thread Name: {threadName}&#xA;Extended Properties: {dictionary({key} - {value}&#xA;)}" type="Microsoft.Practices.EnterpriseLibrary.Logging.Formatters.TextFormatter, Microsoft.Practices.EnterpriseLibrary.Logging, Version=3.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" name="Text Formatter"/>
    </formatters>
    <categorySources>
      <add switchValue="All" name="General">
        <listeners>
          <add name="Formatted EventLog TraceListener"/>
        </listeners>
      </add>
    </categorySources>
    <specialSources>
      <allEvents switchValue="All" name="All Events"/>
      <notProcessed switchValue="All" name="Unprocessed Category"/>
      <errors switchValue="All" name="Logging Errors &amp; Warnings">
        <listeners>
          <add name="Formatted EventLog TraceListener"/>
        </listeners>
      </errors>
    </specialSources>
  </loggingConfiguration>
  <appSettings>
    <add key="defaultCountry" value="Please Select" />
    <add key="defaultCurrency" value="Select Currency" />
    <add key="DictionaryFolder" value="~/Resources/Dictionary" />
    <add key="SmtpServer" value="10.0.0.10" />
    <add key="webRoot" value="http://localhost:1477/TPS360.WEBUI/" />
    <add key="forgotPasswordTemplateUrl" value="MailTemplate/ForgotPasswordTemplate.html" />
    <add key="changePasswordTemplateUrl" value="MailTemplate/ChangePasswordTemplate.html" />
    <add key="invoiceTemplateUrl" value="MailTemplate/InvoiceTemplate.htm" />
    <add key="InterviewMailTemplateUrl" value="MailTemplate/InterviewMailTemplate.htm" />
    <add key="pdfGenerateUrl" value="PdfCreator.aspx" />
    <add key="immigrationReportUrl" value="PDFReport/ImmigrationReport.aspx" />
    <add key="checkListReportUrl" value="HR/MemberCheckListPreview.aspx" />
    <add key="AlertInterval" value="5000" />
    <add key="Constants.Prefix.Css" value="" />
    <add key="Constants.Suffix.Css" value="v=1" />
    <add key="Constants.Prefix.Img" value="" />
    <add key="Constants.Suffix.Img" value="v=1" />
    <add key="FileSet_Css_Style1" value="~/Style/Common.css" />
    <add key="FileSet_Css_Style2" value="~/Style/Common.css,~/Style/Dashboard.css" />
    <add key="ConnectionTimeOut" value="600" />
    <add key="PublicPages" value="HotJobListing.aspx,HotJobRequisitionPreview.aspx" />
    <add key="TestUploader.FileUploader" value="http://localhost:2157/TPS360.WebUI/Uploader/FileUploader.asmx" />
    <add key="ErrorLogging" value="Database" />
    <add key="Appointment" value="http://192.168.1.210:8080/ig_common/20122clr35/Forms/WebSchedule/AppointmentAdd.aspx" />
    <add key="Reminder" value="http://192.168.1.210:8080/ig_common/20122clr35/Forms/WebSchedule/Reminder.aspx" />
    <add key="Submission" value="Please Select" />
    <add key="Offer" value="Offered" />
    <add key="Join" value="Joined" />
    <add key="Interview" value="To Schedule Interview" />
    <add key="Referral" value="Please Select" />
    <add key="VendorLevel" value="Please Select" />
    <add key="ApplicationSource" value="landt" />
    <add key="ErrorLog" value="yes" />
    <add key="ErrorLogLocation" value="D:\TPS\Error_Log" />
    <add key="CopyRightsText" value="Talentrackr " />
    <add key="VersionText" value="Version 1.6.4" />
    <add key="LoginStatusPath" value="/" />
    <add key="ActiveDirectoryCheck" value="True" />
    <add key="EnableMembershipIntegration" value="true" />
    <add key="ADConnectionLDAP" value="LDAP://labservices.local/DC=labservices,DC=local" />
    <!--<add key="ADDomains" value="labservices.local,Span" />-->
    <add key="ADUsersFile" value="C:\TPS\ADUsersFile" />
  </appSettings>
  <connectionStrings>
    <clear/>
	  <add name="TPS360CS" connectionString="server=INTRADBSVR;Database=Talentrackr_RMG_SPAN;User Id=tt_admin; password=T8Adm1N;Connection Timeout=600" providerName="System.Data.SqlClient" />
	  <add name="TPS360CS_SV" connectionString="server=INTRADBSVR;Database=Talentrackr_RMG_SPAN;User Id=tt_admin; password=T8Adm1N;" providerName="System.Data.SqlClient" />
	  <add name="TPS360TrailDB" connectionString="server=INTRADBSVR;Database=Talentrackr_RMG_SPAN;User Id=tt_admin; password=T8Adm1N" providerName="System.Data.SqlClient" />
	<!--Added for Active Directory-->
    <add name="ADConnectionString" connectionString="LDAP://labservices.local/DC=labservices,DC=local"/>
    </connectionStrings>
  <dataConfiguration defaultDatabase="TPS360CS"/>
  <dataAccessFactoryConfiguration>
    <dataAccessFactory typeName="TPS360.DataAccess.DataAccessFactory" assemblyName="TPS360.DataAccess, Version=1.0.0.0, Culture=neutral"/>
  </dataAccessFactoryConfiguration>
  <codeBenchmark enabled="false" includeParameters="false" logFile="PerformanceLog.csv"/>
  <scriptSettings versionNo="1.0" useSet="~/App_Data/FileSets.xml"/>
  <TinyMCE installPath="~/Scripts/tiny_mce" mode="src">
    <!-- Default settings for all TinyMCE instances 
		<globalSettings>
			<add key="relative_urls" value="false"/>
		</globalSettings>
		 Compressor specific settings -->
    <gzipCompressor enabled="yes" diskCache="no" cachePath="c:\temp" expiresOffset="10d"/>
  </TinyMCE>
  <commonSetting>
    <mailMap location="MailTemplate">
      <map key="leadCreated" from="admin@tps360.com" subject="A New Lead Created." file="MailTemplate\LeadCreated.htm"/>
      <map key="leadStatusChanged" from="admin@tps360.com" subject="Lead Status Changed." file="MailTemplate\LeadStatusChanged.htm"/>
      <map key="companyStatusChangeReq" from="admin@tps360.com" subject="Company Status Change Request." file="MailTemplate\CompanyStatusChangeRequest.htm"/>
      <map key="companyAddedToCampaign" from="admin@tps360.com" subject="Company Added To Campaign." file="MailTemplate\CompanyAddedToCampaign.htm"/>
      <map key="leadCreatedInsideCampaign" from="admin@tps360.com" subject="Lead created From Campaign." file="MailTemplate\LeadCreatedInsideCampaign.htm"/>
      <map key="companyRegistration" from="admin@tps360.com" subject="Company Registration." file="MailTemplate\CompanyRegistration.htm"/>
      <map key="jobPostingMail" from="admin@tps360.com" subject="Your matched job" file="JobPostingMail.htm"/>
      <map key="candidateOverview" from="admin@tps360.com" subject="Candidate Overview" file="CandidateOverviewTemplate.htm"/>
    </mailMap>
  </commonSetting>
  <location path="Referral/employeereferral.aspx">
    <system.web>
      <authorization>
        <allow users="*"/>
      </authorization>
    </system.web>
  </location>
  <location path="Vendor/Login.aspx">
    <system.web>
      <authorization>
        <allow users="*"/>
      </authorization>
    </system.web>
  </location>
  <system.web>
    <!-- sessionState -->
	  <sessionState mode="SQLServer" sqlConnectionString="server=INTRADBSVR;Database=Talentrackr_RMG_SPAN;User Id=tt_admin; password=T8Adm1N;Connection Timeout=600" allowCustomSqlDatabase="true" />
    <trace enabled="false" requestLimit="10" pageOutput="false" traceMode="SortByTime" localOnly="false"/>
    <compilation debug="true">
      <assemblies>
        <add assembly="System.Core, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
        <add assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        <add assembly="System.Xml.Linq, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
        <add assembly="System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
        <add assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7DD5C3163F2CD0CB"/>
        <add assembly="Infragistics35.WebUI.Documents.Reports.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7DD5C3163F2CD0CB"/>
        <add assembly="System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"/>

        <!--Added for Active Directory-->
        <add assembly="System.DirectoryServices, Version=2.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"/>
        <add assembly="System.DirectoryServices.AccountManagement, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
        <add assembly="System.DirectoryServices.Protocols, Version=2.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"/>
       
      </assemblies>
    </compilation>
    <authentication mode="Forms">
      <forms name=".DBAUTH" loginUrl="Login.aspx?SeOut=SessionOut" protection="All" timeout="28800" path="/" requireSSL="false" slidingExpiration="true" defaultUrl="Default.aspx" cookieless="UseCookies" enableCrossAppRedirects="false"/>
    </authentication>
    <authorization>
	<!--Added for Active Directory-->
      <!--<deny users="?"/>-->
      <allow users="*"/>
    </authorization>
    <customErrors mode="Off" defaultRedirect="~/ErrorPage.aspx">
      <!--<error statusCode="404" redirect="~/Login.aspx" />-->
    </customErrors>
    <pages theme="Default" validateRequest="false">
      <controls>
        <add tagPrefix="asp" namespace="System.Web.UI" assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        <add tagPrefix="asp" namespace="System.Web.UI.WebControls" assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        <add tagPrefix="asp" assembly="AjaxControlToolkit" namespace="AjaxControlToolkit"/>
        <add namespace="TPS360.Web.UI.Controls" tagPrefix="cc1"/>
        <add namespace="TPS360.Controls" assembly="TPS360.Controls" tagPrefix="TPSControl"/>
        <add assembly="AjaxControls" namespace="AjaxControls" tagPrefix="AjaxControl"/>
      </controls>
      <namespaces>
        <add namespace="System.Linq"/>
      </namespaces>
    </pages>
    <siteMap enabled="true" defaultProvider="SqlSiteMap">
      <providers>
        <clear/>
        <add name="SqlSiteMaps" type="TPS360.Providers.SqlSiteMapProviders" connectionStringName="TPS360CS" defaultUrl="Default.aspx" sqlCacheDependency="CommandNotification"/>
        <add name="SqlSiteMap" type="TPS360.Providers.SqlSiteMapProvider" connectionStringName="TPS360CS" defaultUrl="Default.aspx" sqlCacheDependency="CommandNotification"/>
        <add name="DefaultSiteMap" type="System.Web.XmlSiteMapProvider" siteMapFile="~/Web.sitemap"/>
      </providers>
    </siteMap>
    <caching>
      <sqlCacheDependency enabled="true"/>
    </caching>
    <roleManager enabled="true" defaultProvider="TPS360Role" cacheRolesInCookie="true">
      <providers>
        <clear/>
        <add name="TPS360Role" applicationName="TPS360" connectionStringName="TPS360CS" type="System.Web.Security.SqlRoleProvider"/>
      </providers>
    </roleManager>
    <membership defaultProvider="TPS360Membership">
      <providers>
        <clear/>
        <add name="TPS360Membership" applicationName="TPS360" connectionStringName="TPS360CS" type="System.Web.Security.SqlMembershipProvider" enablePasswordRetrieval="true" enablePasswordReset="true" requiresQuestionAndAnswer="false" requiresUniqueEmail="false" passwordFormat="Encrypted" maxInvalidPasswordAttempts="1000" minRequiredPasswordLength="6" minRequiredNonalphanumericCharacters="0" passwordAttemptWindow="3" passwordStrengthRegularExpression=""/>
		 <add name="TPS360ADMembership" connectionStringName="ADConnectionString" type="System.Web.Security.ActiveDirectoryMembershipProvider,               
             System.Web, Version=2.0.0.0,Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" attributeMapUsername="sAMAccountName"
/>
            <!--attributeMapUsername="userPrincipalName"-->
      </providers>
    </membership>
    <machineKey validationKey="CE56C8154C2D6C1B3DBB017621F977497EA57C36" decryptionKey="9441457843A6CD09CDD744F29661B33924009F494AC9FF4A" validation="SHA1" decryption="3DES"/>
    <anonymousIdentification enabled="true" cookieName=".DBANON" cookieTimeout="43200" cookiePath="/" cookieRequireSSL="false" cookieSlidingExpiration="true" cookieProtection="All" cookieless="UseCookies"/>
    <httpHandlers>
      <remove verb="*" path="*.asmx"/>
      <add verb="*" path="*.aspx" type="System.Web.UI.PageHandlerFactory"/>
      <add verb="*" path="*.asmx" validate="false" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
      <add verb="*" path="*_AppService.axd" validate="false" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
      <add verb="GET,HEAD" path="ScriptResource.axd" type="System.Web.Handlers.ScriptResourceHandler, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" validate="false"/>
      <add verb="*" path="css.axd" type="CssHandler" validate="false"/>
      <add verb="GET,HEAD,POST" path="TinyMCE.ashx" type="Moxiecode.TinyMCE.Web.HttpHandler,Moxiecode.TinyMCE"/>
    </httpHandlers>
    <httpModules>
      <!--<add name="AuditTrailHttpModule" type="AuditTrailHttpModule" />-->
      <add name="ScriptModule" type="System.Web.Handlers.ScriptModule, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
    </httpModules>
    <httpRuntime executionTimeout="240" maxRequestLength="55000"/>
  </system.web>
  <system.codedom>
    <compilers>
      <compiler language="c#;cs;csharp" extension=".cs" warningLevel="4" type="Microsoft.CSharp.CSharpCodeProvider, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089">
        <providerOption name="CompilerVersion" value="v3.5"/>
        <providerOption name="WarnAsError" value="false"/>
      </compiler>
      <compiler language="vb;vbs;visualbasic;vbscript" extension=".vb" warningLevel="4" type="Microsoft.VisualBasic.VBCodeProvider, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089">
        <providerOption name="CompilerVersion" value="v3.5"/>
        <providerOption name="OptionInfer" value="true"/>
        <providerOption name="WarnAsError" value="false"/>
      </compiler>
    </compilers>
  </system.codedom>
  <system.webServer>
    <validation validateIntegratedModeConfiguration="false"/>
    <modules>
      <remove name="ScriptModule"/>
      <add name="ScriptModule" preCondition="managedHandler" type="System.Web.Handlers.ScriptModule, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
    </modules>
    <handlers>
      <remove name="WebServiceHandlerFactory-Integrated"/>
      <remove name="ScriptHandlerFactory"/>
      <remove name="ScriptHandlerFactoryAppServices"/>
      <remove name="ScriptResource"/>
      <add name="ScriptHandlerFactory" verb="*" path="*.asmx" preCondition="integratedMode" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
      <add name="ScriptHandlerFactoryAppServices" verb="*" path="*_AppService.axd" preCondition="integratedMode" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
      <add name="ScriptResource" preCondition="integratedMode" verb="GET,HEAD" path="ScriptResource.axd" type="System.Web.Handlers.ScriptResourceHandler, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
    </handlers>
  </system.webServer>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Extensions" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="1.0.0.0-1.1.0.0" newVersion="3.5.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Extensions.Design" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="1.0.0.0-1.1.0.0" newVersion="3.5.0.0"/>
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.serviceModel>
    <bindings/>
    <client/>
  </system.serviceModel>
</configuration>
