﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Startup.aspx.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
 *  0.1              24-Jun-2009           Anand Dixit          Changes due to renaming of LicenseSectionHandler to TPS360Section handler are implemented
 *  0.2              08-Jul-2009           Veda                 Defect Id:10913; Error message has been changed.  
 *  0.3              07-Oct-2009           Veda                 Defect Id:11534 : Delete teh saved license details after teh expirydate
 *  0.4              13-Apr-2010           Ganapati Bhat        Defect id:12439 ; Code Added in SaveButton_Click()   
 ---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Web.Security;

using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
using TPS360.Common.Shared;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using TPS360.Common.Helper;
using System.Web;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Xml;
using System.Configuration;
using TPS360.Common;
using System.Web.Configuration;

namespace TPS360.Web.UI
{
    public partial class StartUp : BasePage
    {
        string intLicenseKey;  

        protected void Page_Load(object sender, EventArgs e)
        {
            LoginStatus save;
            string ApplicationSource = WebConfigurationManager.AppSettings["ApplicationSource"].ToString();
            if (ApplicationSource.ToLower() == "landt") 
            {
                Response.Redirect("Login.aspx");
            }
            save = (LoginStatus)Page.Master.FindControl("lgsLogin");
            save.Visible = false;
            if (!IsPostBack)
            {
                Context.Items[ContextConstants.MEMBER] = null;
                Context.Items[ContextConstants.SITESETTING] = null;
                Context.Items[ContextConstants.MEMBER_DEFAULT_AVAILABILITY] = null;                
            }
            TPS360SectionHandler licenseSection = WebConfigurationManager.GetSection("tps360license") as TPS360SectionHandler; ;
            if (licenseSection != null)
            {
                try
                {
                    string LicenseKey = (licenseSection.Details["LicenseKey"].Value != null) ? licenseSection.Details["LicenseKey"].Value : String.Empty;
                    DateTime StartDate = Convert.ToDateTime(licenseSection.Details["StartDate"].Value);
                    DateTime ExpireDate = Convert.ToDateTime(licenseSection.Details["ExpireDate"].Value);
                    DateTime currentDate = DateTime.Now;
                    if (LicenseKey != null && ExpireDate != null && StartDate != null) //to handle if there are no details
                    {
                        DateTime MaxDate = ExpireDate.AddDays(1);
                        if (StartDate <= currentDate && MaxDate >= currentDate)
                        {
                            Response.Redirect("login.aspx");
                        }
                    }
                }
                catch (FormatException ex)
                {

                }
                catch (NullReferenceException ex)
                {

                }
            }
        }
       
        protected void SaveButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(LicenseKey.Text))
            {
                LicenseKeyRequired.Visible = false;
                lblMessage.Text = "Please wait, as the application is restarting it will take time to log in";

                intLicenseKey = (LicenseKey.Text).ToString();
                string varchrDomainName = txtDomainName.Text; //Request.ServerVariables["HTTP_HOST"];
                int LicenseValidFlag = 0;
                ArrayList arr = new ArrayList();
                arr = Facade.GetLicenseKeybyId(intLicenseKey, varchrDomainName);
                if (arr.Count > 0)
                {
                    DateTime dttmStartDate = Convert.ToDateTime(arr[0]);
                    DateTime dttmExpiredDate = Convert.ToDateTime(arr[1]);
                    int LicenseFlag = Convert.ToInt32(arr[2]);
                    int NoofUsersAllowed = Convert.ToInt32(arr[3]);
                    DateTime currentDate = DateTime.Now;
                    DateTime MaxDate1 = dttmExpiredDate.AddDays(1); 

                    if (LicenseFlag == 1) 
                    {
                        SaveDetails(varchrDomainName, intLicenseKey, dttmStartDate, dttmExpiredDate, NoofUsersAllowed);
                        Response.Redirect("login.aspx");
                    }
                    else
                    {
                        lblMessage.Visible = true;  
                        lblMessage.Text = "Your License is not valid or has expired. Please contact the TPS360 team for re-validation.";
                        SetFocus(LicenseKey);
                    }
                }
                else
                {
                    lblMessage.Visible = true; 
                    lblMessage.Text = "Your License is not valid or has expired. Please contact the TPS360 team for re-validation.";
                    SetFocus(LicenseKey); 
                }
            }
            else
            {
                LicenseKeyRequired.Visible = true;
                lblMessage.Visible = false;
                SetFocus(LicenseKey);
            }
        }

        public void SaveDetails(string DomainName,string intLicenseKey, DateTime dttmStartDate, DateTime dttmExpiredDate,int Noofuserallowed) //10628 
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);            
            TPS360SectionHandler license = config.GetSection("tps360license") as TPS360SectionHandler; ;
           license.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
            license.SectionInformation.ForceSave=true;
            license.SectionInformation.ForceDeclaration(true);

            TPS360ConfigElement domainName = new TPS360ConfigElement("DomainName", DomainName);
            TPS360ConfigElement lkey= new TPS360ConfigElement("LicenseKey",intLicenseKey.ToString());
            TPS360ConfigElement sdate= new TPS360ConfigElement("StartDate",dttmStartDate.ToString());
            TPS360ConfigElement expdate= new TPS360ConfigElement("ExpireDate",dttmExpiredDate.ToString());
            TPS360ConfigElement noofusersallowed = new TPS360ConfigElement("NoofUsersAllowed", Noofuserallowed.ToString());
            license.Details.Add(domainName);    
            license.Details.Add(lkey);    
            license.Details.Add(sdate);
            license.Details.Add(expdate);
            license.Details.Add(noofusersallowed);
            config.Save(ConfigurationSaveMode.Full);            
            ConfigurationManager.RefreshSection("tps360license");        
        }
    }
}