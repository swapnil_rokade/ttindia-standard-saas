﻿/*
----------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateInternalNotesAvailability.cs
    Description: This page contains URL constants.
    Created By: pravin khot
    Created On: 13/July/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date              Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------ 
 *  0.1        10/May/2017         Sumit Sonawane      IssueId 1310

   --------------------------------------------------------------------------------------------------------------------------------  
*/


using System;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using System.Web.UI.WebControls;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections.Generic;
using System.Collections;
using TPS360.BusinessFacade;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using TPS360.Web.UI.Helper;
using System.Linq;
namespace TPS360.Web.UI
{
    public partial class CandidateInternalNotesAvailability : CandidateBasePage
    {
        #region Member Variables
        private int _memberId = 0;
        #endregion

        #region Methods

        private void GetMemberId()
        {
            //From Member Profile
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                hfMemberId.Value = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];

            }
            //From Member Login
            else
            {
                hfMemberId.Value = base.CurrentMember.Id.ToString();
            }
            _memberId = Int32.Parse(hfMemberId.Value);
        }

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            _memberId = Int32.Parse(hfMemberId.Value);
            GetMemberId();
            MemberManager Manager = Facade.GetMemberManagerByMemberIdAndManagerId(_memberId, CurrentMember.Id);
            //if (Manager == null && !IsUserAdmin && !IsUserVendor ) // Line commented by Sumit On 10/May/2017 for IssueId 1310
            //    Helper.Url.Redirect(UrlConstants.Dashboard.DEFAULT_PAGE); // Line commented by Sumit On 10/May/2017 for IssueId 1310
            if (!IsPostBack)
            {
                if (_memberId > 0)
                {
                    string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                    Label lbModalTitle = (Label)this.Master.FindControl("lbModalTitle");
                    if (lbModalTitle != null) lbModalTitle.Text = name + " - " + "Notes";
                    
                    //this.Page.Title = name + " - " + " Availability";
                }
                hdnPageTitle.Value = Page.Title;
            }
            Page.Title = hdnPageTitle.Value;
            ucntrlNotesAvailability.MemberId = _memberId;
            //NotesEditor.PrepareView1();
        }

        #endregion

        #endregion
    }
}