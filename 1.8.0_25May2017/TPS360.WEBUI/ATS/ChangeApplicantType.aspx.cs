﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using TPS360.Common.Helper;

namespace TPS360.Web.UI
{
    public partial class CandidateList : CandidateBasePage
    {

        #region Member Variables
        private int _memberId = 0;
        #endregion

        #region Methods

        private void GetMemberId()
        {
            
            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                hfMemberId.Value = Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID];
               
            }
           
            else
            {
                hfMemberId.Value = base.CurrentMember.Id.ToString();
            }
            _memberId = Int32.Parse(hfMemberId.Value);
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            _memberId = Int32.Parse(hfMemberId.Value);
            if (!IsPostBack)
            {
                string message = Helper.Url.SecureUrl[UrlConstants.PARAM_MSG];
                GetMemberId();
                if (_memberId > 0)
                {
                    string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                    Label lbModalTitle = (Label)this.Master.FindControl("lbModalTitle");
                    if (lbModalTitle != null) lbModalTitle.Text = name + " - " + "Change Candidate Type";
                    
                   // this.Page.Title = name + " - " + "Change Candidate Type";
                }
            }
        }
    }
}