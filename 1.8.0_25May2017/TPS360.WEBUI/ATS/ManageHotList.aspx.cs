﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ManageHotList.ascx.cs
    Description: Used to display the Hotlist.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 11-Feb-2010         Gopala Swamy        Defect id: 12903; Put Page_Init statement
    ------------------------------------------------------------------------------------------------------------------------------
 */
using System;
using TPS360.Common.Shared;

namespace TPS360.Web.UI
{
    public partial class ManageHotList : CandidateBasePage
    {
        #region Events

        #region Page Events
        protected void Page_Init(object sender, EventArgs e)
        {
            ucntrlManageHotList.GroupType = Convert.ToInt32(MemberGroupType.Candidate);
            ucntrlManageHotList.CandidateOrConsultant = "Candidates";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int HotListID=Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_ID]);
                lblHotListName.Text = Facade.GetMemberGroupById(HotListID ).Name;
            }

        }

        #endregion

        #endregion
    }
}