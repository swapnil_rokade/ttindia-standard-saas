﻿<%@ page Language="C#" MasterPageFile="~/Masters/CandidateProfile.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="InternalEmailEditor.aspx.cs" Inherits="TPS360.Web.UI.CandidateInternalEmailEditor"
    Title="Candidate Email Editor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ register Src="~/Controls/EmailEditor.ascx" TagName="EmailEditor" TagPrefix="uc1" %>
<%@ register Src="~/Controls/AllEmailList.ascx" TagName="AllEmailList" TagPrefix="uc2" %>
<%@ register Src="~/Controls/SendEmailList.ascx" TagName="SendEmailList" TagPrefix="uc3" %>
<%@ register Src="~/Controls/ReceivedEmailList.ascx" TagName="ReceivedEmailList"
    TagPrefix="uc4" %>
<asp:content ID="cntConsultantEmailBody" ContentPlaceHolderID="cphCandidateMaster"
    runat="Server">
    <script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>
<script type ="text/javascript" >
var ModalProgress ='<%= Modal.ClientID %>';
</script>
  <asp:HiddenField ID="hdnPageTitle" runat ="server" />
  <div class="TabPanelHeader">Email</div>
   <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">New Email</a></li>
            <li><a href="#tab2" data-toggle="tab">All Emails</a></li>
            <li><a href="#tab3" data-toggle="tab">Sent Emails</a></li>
            <li><a href="#tab4" data-toggle="tab">Received Emails</a></li>
    </ul>
    <div class="tab-content">
         <div class="tab-pane active" id="tab1">
            <uc1:EmailEditor ID="ucntrlEmailEditor" runat="server" />
         </div>
         <div class="tab-pane" id="tab2">
            <uc2:allemaillist ID="AllEmailList1" runat="server" />
         </div>
         <div class="tab-pane" id="tab3">
            <uc3:sendemaillist ID="SendEmailList1" runat="server" />
         </div>
         <div class="tab-pane" id="tab4">
         <uc4:receivedemaillist ID="ReceivedEmailList1" runat="server" />
         </div>
    </div>
    </div>
    
    
                    
                  
    <asp:Panel ID="pnlmodal" runat="server" style="display: none">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID ="Modal" runat ="server" TargetControlID ="pnlmodal" PopupControlID ="pnlmodal"
             BackgroundCssClass ="divModalBackground" ></ajaxToolkit:ModalPopupExtender>

</asp:content>
