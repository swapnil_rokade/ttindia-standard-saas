﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="MasterHotList.aspx.cs"
    Inherits="TPS360.Web.UI.CandidateMasterHotList" Title="Master Candidate Hot Lists" %>

<%@ Register Src="~/Controls/HotList.ascx" TagName="uclMasterHotList" TagPrefix="uc1" %>
<asp:Content ID="cntLookupEditorHeader" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="cntLookupEditorTitle" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    Master Hot Lists
</asp:Content>
<asp:Content ID="cntLookupEditor" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
    <uc1:uclMasterHotList ID="uclMasterHotList" CandidateOrConsultant="Candidates" IsMy="false"
        runat="server" />
</asp:Content>
