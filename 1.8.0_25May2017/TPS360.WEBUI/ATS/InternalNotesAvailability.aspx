﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalNotesAvailability.aspx
    Description: This is the page used to update availability.
    Created By: pravin khot 
    Created On: 13/July/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
   -------------------------------------------------------------------------------------------------------------------------------------------       
--%>
<%@ Page Language="C#" MasterPageFile="~/Modals/Modal.master" AutoEventWireup="true" CodeFile="InternalNotesAvailability.aspx.cs"
    Inherits="TPS360.Web.UI.CandidateInternalNotesAvailability"  ValidateRequest="true" Title="Applicant Availability" %>

<%@ Register Src="~/Controls/MemberNotesAvailability.ascx" TagName="NotesAvailability" TagPrefix="uc1" %>
<%--<%@ Register Src="~/Controls/NotesAndActivitiesEditor.ascx" TagName="NotesAndActivitiesEditor"
    TagPrefix="ucl" %>
--%>

<asp:Content ID="cntCandidatePrivacyAvailability" ContentPlaceHolderID="cphModalMaster"
    runat="Server">
    
    <div  style =" width : 475px; height : 330px; max-height : 330px ; overflow :auto ; display : table-cell ; vertical-align :50px; " align="center"  >
                 <asp:HiddenField ID="hdnPageTitle" runat ="server" />
                 <asp:HiddenField ID="hfMemberId" runat="server" Value="0"/>
                 <uc1:NotesAvailability id="ucntrlNotesAvailability" runat="server"  />
                 </div> 
                 
         <%--   <div  id="id1" visible="false" runat="Server" >
                 <ucl:NotesAndActivitiesEditor id="NotesEditor" runat="server"  />
             </div>  --%>               
               
                          
</asp:Content>
