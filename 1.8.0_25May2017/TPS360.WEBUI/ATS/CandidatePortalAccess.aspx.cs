﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalResumeBuilder.aspx.cs
    Description: This is the user control page used for resume builder.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.           Date               Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1               Mar-04-2009        Shivanand          Defect #10053; usercontrol "ucntrlJobTitle" is removed.
    0.2               July-01-2009       Shivanand          Defect #10464; AsyncOptions are provided for Tabs.
 *  0.3               Apr-07-2010        Ganapati Bhat      Enhancement #12139; Removed tab "Skill Set"
------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;

using TPS360.Common.Helper;
using System.Drawing;
using System.Web.Security;
using TPS360.Common.BusinessEntities;
using TPS360.Web.UI.Helper;
namespace TPS360.Web.UI
{
    public partial class CandidatePortalAccess : CandidateBasePage
    {
        #region Veriables
        private int CurrentCandidateID
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                }
                return 0;

            }
        }
        #endregion

        #region Methods


        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            

            if (!IsPostBack)
            {
               Member curMember  =Facade.GetMemberById  (CurrentCandidateID);
                MembershipUser user = Membership.GetUser(curMember .PrimaryEmail );
                ddlAccess.SelectedIndex = user.IsApproved ? 1 : 0;
                btnSend.Visible = ddlAccess.SelectedIndex == 0 ? false : true;
                string name = Facade.GetMemberNameById(CurrentCandidateID);
                hdnPageTitle.Value = name + " - " + "Candidate Portal Access";
            }

            this.Page.Title = hdnPageTitle.Value;
        }
        protected void ddlAccess_SelectedIndexChanged(object sender, EventArgs e)
        {
            Member curMember = Facade.GetMemberById(CurrentCandidateID);
            MembershipUser user = Membership.GetUser(curMember .PrimaryEmail  );
            user.IsApproved = ddlAccess.SelectedIndex == 0 ? false : true;
            Membership.UpdateUser(user);
            MiscUtil.ShowMessage(lblMessage, (ddlAccess.SelectedIndex == 0 ? "Successfully blocked candidate portal access" : "Successfully enabled candidate portal access"), false);
            btnSend.Visible = ddlAccess.SelectedIndex == 0 ? false : true;
        
        }
        protected void btnSend_Click(object sender, EventArgs e)
        {
            Member curMember = Facade.GetMemberById(CurrentCandidateID);
            EmailHelper emailhelper = new EmailHelper();
            string emailbody = emailhelper.PrepareViewforTps360Access(curMember);
            MembershipUser user = Membership.GetUser(curMember.PrimaryEmail);
            string Password= user.ResetPassword();
            emailbody= emailbody.Replace("**********", Password);


            //Building Email
            //emailhelper.From = CurrentMember.PrimaryEmail;
            emailhelper.To.Add(curMember.PrimaryEmail);
            emailhelper.Subject = "New Password For Talentrackr Access";
            emailhelper.Body = emailbody;
            string result=emailhelper.Send();

            if(result=="1"){

                MiscUtil.ShowMessage(lblMessage,"Successfully reset password and sent access details to candidate",false);
            }
        
        }
       
        #endregion
    }
}