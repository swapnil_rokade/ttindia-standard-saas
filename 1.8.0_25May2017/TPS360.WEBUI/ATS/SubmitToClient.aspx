﻿<%---------------------------------------------------------------------------------------------------------------------------------------------
    FileName: SubmitToClient.aspx
    Description: This is the page used to submit the candidate to the client.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Sep-30-2008          Jagadish N           Defect id: 8790; Removed the fields/labels: Client Rate, Requisition Rate, 
                                                               Applicant Desired Rate, Submission Rate.
    0.2              Oct-21-2008          Jagadish N           Defect id: 8966; Replaced text box infront of 'From' label with dropdown.
    0.3              Apr-16-2008          Sandeesh             Defect id : 10353 "Select All" checkbox should be displayed in the List of Resumes Grid
------------------------------------------------------------------------------------------------------------------------------------------- 
--%>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.EditorControls" TagPrefix="ig" %>
<%@ Register Assembly="Infragistics35.Web.v12.2, Version=12.2.20122.1007, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.Web.UI.LayoutControls" TagPrefix="ig" %>
<%@ Page Language="C#" MasterPageFile="~/Default-nosection.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeFile="SubmitToClient.aspx.cs" Inherits="TPS360.Web.UI.ATS.SubmitToClient" ValidateRequest="false" Title="Submission" %> <%--added ValidateRequest="false" 15/Feb/2016 Pravin khot--%>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/HtmlEditor.ascx" TagName="HtmlEditor" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ModalTemplate.ascx" TagName="Template" TagPrefix="ucl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%--<%@ Register Assembly="Infragistics2.WebUI.WebHtmlEditor.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.WebHtmlEditor" TagPrefix="ighedit" %>--%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/ResumeAttachmentModal.ascx" TagName="AttachResume" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/MultipleItemPicker.ascx" TagName="MultipleSelection"
    TagPrefix="ucl" %>
  <%@ Register Src="~/Controls/MultipleSelectItemPicker.ascx" TagName="MultipleSelectItemPicker"
    TagPrefix="ucl" %>    
<asp:Content ID="cntSendRequisitionToApplicantHeader" ContentPlaceHolderID="head"
    runat="Server">
</asp:Content>
<asp:Content ID="cntSendRequisitionToApplicantTitle" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
</asp:Content>
<asp:Content ID="cntSendRequisitionToApplicantEditor" ContentPlaceHolderID="cphHomeMaster"
    runat="Server">
    <%--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>--%>
    <style>
        .text-label
        {
            color: #cdcdcd;
            font-weight: bold;
            font-size: small;
        }
    </style>

<script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

<script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

<script src="../Scripts/fixWebkit.js" type="text/javascript"></script>
<script src="../assets/js/chosen.jquery.js" type="text/javascript"></script>
<link href="../assets/css/chosen.css" rel="Stylesheet" />

<style type="text/css">
    .TableFormLeble
    {
        width: 33%;
    }
    .EmailDiv
    {
        vertical-align: middle;
        text-align: center;
        border: solid 1px #CCC;
        padding-bottom: 2px;
        padding-top: 2px;
        margin-right: 4px;
        margin-bottom: 4px;
        width: auto;
        float: left;
        background-image: -moz-linear-gradient(center top , #FFFFFF 20%, #F6F6F6 50%, #EEEEEE 52%, #F4F4F4 100%);
        border-radius: 3px;
    }
</style>
<script type="text/javascript" language="javascript">

  window.onload = function() {
 
    document.getElementById("<%= hfOtherInterviewers.ClientID %>").value = "";  
);</script>
<script type="text/javascript" language="javascript">
//0.3 start
var TotalChkBx;
var Counter;

//function SelectChanged(Radiolst)
//{
//    var mailBdy = document.getElementById("<%=WHECoverLetter.FindControl("txtEditor").ClientID  %>");
//    //var mailBdy = document.getElementById("<%=WHECoverLetter.ClientID  %>");
//    var hdnName = document.getElementById("<%=hdnmemberName.ClientID  %>");
//    var toMember = document.getElementById(Radiolst);
//    var selectedTxt=$(toMember).find(":checked").next().html();
//    var arr=selectedTxt.split('[');
//    var memberName = arr[0].trim();
//    var newBod = $(mailBdy).val();
//    newBod=newBod.replace(hdnName.value,memberName);
//    hdnName.value = memberName;
//     $(mailBdy).val(newBod);
//   
//    alert(mailBdy.value);
//    ShowMessage("<%=lblMessage.ClientID  %>" ,"message" ,false  );
//    
//}

//ShowMessage=function (lableid,message,isError)
//{
//        var lblMesage=document .getElementById (lableid);
//        lblMesage.style.display="";
//        lblMesage = document.getElementById(lableid);
//        lblMesage.style.width="40%";
//        lblMesage.style.textAlign="center";
//        lblMesage.style.position="fixed";
//        lblMesage.style.top = "10px"
//        lblMesage.style.left = "0";
//         $('#'+ lableid ).css({'margin-left':'30%'});
//        lblMesage .innerHTML=getMessageText(lableid ,message,isError );
//        window.setTimeout(function(){hideMeClick(lableid ); }, 5000)
//       //alert ( $('#'+ lableid ).parent().attr('id'));//.css({'text-align':'center'});
//}

function CancelModal()
{
var check=document .getElementById ('<%=hdncheck.ClientID %>');
check .value="";
}
window.onload = function()
{
   Counter = 0;
}

function HeaderClick(CheckBox)
{
   var TargetChildControl = "chkResume";
   var Inputs = TargetBaseControl.getElementsByTagName("input");
   for(var n = 0; n < Inputs.length; ++n)
      if(Inputs[n].type == 'checkbox' && 
                Inputs[n].id.indexOf(TargetChildControl,0) >= 0)
         Inputs[n].checked = CheckBox.checked;
   Counter = CheckBox.checked ? TotalChkBx : 0;
}

function ChildClick(CheckBox, HCheckBox)
{
   var HeaderCheckBox = document.getElementById(HCheckBox);
   if(CheckBox.checked && Counter < TotalChkBx)
      Counter++;
   else if(Counter > 0) 
      Counter--;
   if(Counter < TotalChkBx)
      HeaderCheckBox.checked = false;
   else if(Counter == TotalChkBx)
      HeaderCheckBox.checked = true;
}
    function fnMoveItems(lstbxFrom,lstbxTo)
    {
         var varFromBox = document.all(lstbxFrom);
         var varToBox = document.all(lstbxTo); 
         if ((varFromBox != null) && (varToBox != null)) 
         { 
              if(varFromBox.length < 1) 
              {
               alert('There are no items in the source ListBox');
               return false;
              }
              if(varFromBox.options.selectedIndex == -1) // when no Item is selected the index will be -1
              {
               alert('Please select an Item to move');
               return false;
              }
              while ( varFromBox.options.selectedIndex >= 0 ) 
              { 
               var newOption = new Option(); // Create a new instance of ListItem 
               newOption.text = varFromBox.options[varFromBox.options.selectedIndex].text; 
               newOption.value = varFromBox.options[varFromBox.options.selectedIndex].value; 
               varToBox.options[varToBox.length] = newOption; //Append the item in Target Listbox
               varFromBox.remove(varFromBox.options.selectedIndex); //Remove the item from Source Listbox 
              } 
         }
         return false; 
    }
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoadedReq); 
    function PageLoadedReq(sender,args)
    {
         try
         {
             var chkBox=document .getElementById ('chkAllItems');
             
             if(chkBox!=null)chkBox.disabled=false ;
             CheckUnCheckGridViewsHeaderCheckboxs('tlbTemplates','chkAllItems',0,1);
              
         }
         catch (e)
         {
          
         }
    }
    function CLCheckUnCheckAllRowLevelsCheckboxes(cntrlHeaderCheckbox,posCheckbox)
    {

      var cntrlGridView = document.getElementById("tlbTemplates");
      var chkBoxAll=document.getElementById(cntrlHeaderCheckbox);
      var hndID="";
      var chkBox="";
      var checked=0;
      var rowNum=0;
      if (cntrlHeaderCheckbox!=null&&cntrlHeaderCheckbox.checked!=null)
      {
            var rowLength=0;
            rowLength=cntrlGridView.rows.length;
            for(var i=1;i<rowLength;i++)
            {
                var myrow=cntrlGridView.rows[i];
                var mycel=myrow.getElementsByTagName("td")[posCheckbox];
                if (mycel.childNodes[0].value=="on")
                {
                    chkBox=mycel.childNodes[0];
                }
                else 
                {
                    chkBox=mycel.childNodes[1];
                }
                try
                {
                    if (mycel.childNodes[2].value==undefined)
                    {
                        hndID=mycel.childNodes[3];
                    }
                    else 
                    {
                        hndID=mycel.childNodes[2];
                    }
                }
                catch(e ) 
                {
                }
                if(chkBox!=null)
                {
                    if(chkBox.checked==null)
                    {
                        chkBox=chkBox.nextSibling;}
                        if(chkBox!=null)
                        {
                            if(chkBox.checked!=null)
                            {
                                    chkBox.checked=cntrlHeaderCheckbox.checked;
                                    rowNum ++;
                            }
                        }
                    }
                    if(chkBox !=null )
                    {
                        if(chkBox.checked!=null&&chkBox.checked==true)
                        {
                            if  (chkBox.id.indexOf('chkItemCandidates')>-1)
                            {
                                if(hndID != null)ADDID(hndID.value,'ctl00_cphHomeMaster_uclTemplate_ucAttachmentModal_hdnSelectedIDS');
                                checked ++;
                               
                            }
                        }
                        else    
                        {
                            if  (chkBox.id.indexOf('chkItemCandidates')>-1)
                            {
                                if(hndID != null)RemoveID(hndID.value,'ctl00_cphHomeMaster_uclTemplate_ucAttachmentModal_hdnSelectedIDS');
                            }
                        }
                   }
                }
           }

           if(checked==rowNum)
            {
                if(chkBoxAll !=null )
                {
                    chkBoxAll.checked=true;
                }
            }
            else
            {
                if(chkBoxAll !=null )
                {
                    chkBoxAll.checked=false;
                }
            }
   }
   function CheckUnCheckGridViewsHeaderCheckboxs(dgrName,chkboxName,posRowChkbox,p)
{

var chkBoxAll=document.getElementById(chkboxName);
var tbl=document.getElementById(dgrName).getElementsByTagName("tbody")[0];
var checked=0;
var rowNum=0;
var rowLength=tbl.rows.length;
var chkBox="";
var hndID="";
for(var i=1;i<rowLength;i++)
{
var myrow=tbl.rows[i];
var mycel=myrow.getElementsByTagName("td")[posRowChkbox];

if (mycel.childNodes[0].value=="on")
{
chkBox=mycel.childNodes[0];
}
else 
{
chkBox=mycel.childNodes[1];
}
try
{
if (mycel.childNodes[2].value==undefined)
{

hndID=mycel.childNodes[3];
}
else 
{
hndID=mycel.childNodes[2];
}
}
catch(e ) 
{
}

if(hndID!=null )
{
if(hndID.value==null)
{
try{
hndID=hndID.childNodes[3];

}
catch (e)
{
}
}
else
{
   
}
}

if(chkBox!=null)
{

if(chkBox.checked==null)
{

chkBox=chkBox.childNodes[1];}
if(chkBox!=null)
{

if(chkBox.checked!=null)
{
rowNum++;}

if(chkBox.checked!=null&&chkBox.checked==true)
{;if(hndID != null)ADDID(hndID.value,'ctl00_cphHomeMaster_uclTemplate_ucAttachmentModal_hdnSelectedIDS');

checked++;}
else{if(hndID != null)RemoveID(hndID.value,'ctl00_cphHomeMaster_uclTemplate_ucAttachmentModal_hdnSelectedIDS');
}

}}}

if(checked==rowNum)
{
chkBoxAll.checked=true;}
else
{
chkBoxAll.checked=false;}}

function RemoveID(ID,hndCtrl)
{

var hdnSelectedIDS=document .getElementById (hndCtrl); 

var arrID=hdnSelectedIDS.value.split(',');
var IDS="";
for(var i=0;i<arrID .length;i++)
{
    if(arrID [i]!=ID)
    {
       if(IDS !="")IDS +=",";
        IDS +=arrID [i];
    }
    hdnSelectedIDS .value=IDS;
    
}

}

function ADDID(ID,hndCtrl)
{

var idAvailable=false ;
var hdnSelectedIDS=document .getElementById (hndCtrl);
var arrID=hdnSelectedIDS.value.split(',');
for(var i=0;i<arrID .length;i++)
{

    if(arrID [i]==ID)
    {
       idAvailable= true ;
        break ;
    }
    
}
if(!idAvailable )
{
       if(hdnSelectedIDS .value!="")hdnSelectedIDS .value +=",";
     hdnSelectedIDS .value= hdnSelectedIDS .value  + ID;
}

}
    function ValidateChkList(source, arguments)
    {                                                   
        arguments.IsValid = IsCheckBoxChecked() ? true : false;  
    }
 
    function IsCheckBoxChecked()
    {   
        var isChecked = false;
        var list =document.getElementById('<%= lstAttachments.ClientID %>');
        if(list != null)
        {
         for (var i=0; i<list.rows.length; i++)
         {
	          for (var j=0; j<list.rows[i].cells.length; j++)
              {
	           var listControl = list.rows[i].cells[j].childNodes[0];                      
                   if(listControl.checked)
                   {                      
                        isChecked = true;
                   }
	          }
	     }
	   }
         return isChecked;
	}
function RemoveDefaultText(e)
{
    if(e.value == e.title){e.value = ''; e.className='CommonTextBox';}
}
function SetDefaultText(e)
{
    if (e.value == ''){ e.value = e.title;
     e.className='text-label';}
 }

   function CheckExistanceOfOtherEmail(curretEmail) {
       
        var alreadyavailablee = false;
        var alreadyadded=document.getElementById("<%= hfOtherInterviewers.ClientID %>").value
       
        var emailarray = alreadyadded.split(",");
       
        var str = curretEmail.trim().toLowerCase();
        str = str.replace(/[,;]$/, '');
       
        for (var i = 0; i < emailarray.length; i++) {
            if (emailarray[i].trim() != '') {
                if (emailarray[i].trim().toLowerCase() == str) {
                    alreadyavailablee = true;
                    break;
                }
            }           
        }
        return alreadyavailablee;
    }
    function CheckExistanceOfEmail(curretEmail, alreadyadded) { //ref to Other interviewers
       
        var alreadyavailablee = false;
        var emailarray = alreadyadded.split(",");
        var str = curretEmail.trim().toLowerCase();
        str = str.replace(/[,;]$/, '');
        for (var i = 0; i < emailarray.length; i++) {
            if (emailarray[i].trim() != '') {
                if (emailarray[i].trim().toLowerCase() == str) {
                    alreadyavailablee = true;
                    break;
                }
            }           
        }
        return alreadyavailablee;
    }

    function Clicked(email, parent) {
      
        $('#<%= hfOtherInterviewers.ClientID %>').val($('#<%= hfOtherInterviewers.ClientID %>').val().replace(email, '').replace(',,', ','));
    }
     function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
Sys.Application.add_load(function() { 

        $('.close').click(function() {
    });

    $('#<%= txtOtherInterviewers.ClientID %>').keyup(function(event) {
    
  
        var key = window.event ? event.keyCode : event.which;
        var index = 0;
        if ($(this).val().toLowerCase().indexOf(";") > 0) index = $(this).val().toLowerCase().indexOf(";");
        else if ($(this).val().toLowerCase().indexOf(",") > 0) index = $(this).val().toLowerCase().indexOf(",");
        else if (key == 13) index = $(this).val().trim().length;

        if (index > 0) {
            if (validateEmail($(this).val().substring(0, index))) {
                var text = $(this).val();
                if (!CheckExistanceOfEmail($(this).val(), $(this).next().val())) {
                    $(this).parent().prepend("<div class='EmailDiv'><span>" + text.substring(0, index) + "</span><button type='button' class='close' data-dismiss='alert' onclick=javascript:Clicked('" + text.substring(0, index) + "')>\u00D7</button></div>");
                    $(this).val(text.substring(index + 1));
                    $(this).next().val($(this).next().val() + ',' + text.substring(0, index));
                }
                else
                    ShowMessage('<%= lblMessage.ClientID %>', 'This Email already added', true);  //code added by pravin
                $(this).val('');
            }
            // ShowMessage('<%= lblMessage.ClientID %>', 'Please Enter Valid Email-Id', true);              
            return false;
        }
    });


    $('#<%= txtOtherInterviewers.ClientID %>').focusout(function() {
       
        var index = $(this).val().length;
        if (index > 0) {
            if (validateEmail($(this).val().substring(0, index))) {
                var text = $(this).val();
                if (!CheckExistanceOfEmail($(this).val(), $(this).next().val())) {
                    //alert(text);
                    $(this).parent().prepend("<div class='EmailDiv'><span>" + text.substring(0, index) + "</span><button type='button' class='close' data-dismiss='alert'>\u00D7</button></div>");
                    $(this).val(text.substring(index + 1));
                    $(this).next().val($(this).next().val() + ',' + text.substring(0, index));
                }
                else
                    ShowMessage('<%= lblMessage.ClientID %>', 'This Email already added', true); //code added by pravin
                $(this).val('');
            }
        }
    });
 
      SetDefaultText (document .getElementById ('<%=txtCCAdd.ClientID %>'));   
      SetDefaultText (document .getElementById ('<%=txtBCCAdd.ClientID %>'));           
      
      try{
     
         var background = $find("MPES")._backgroundElement;
            background.onclick = function() {$find("MPES").hide();}
             var backgroundmpeModal = $find("mpeModal")._backgroundElement;
            backgroundmpeModal.onclick = function() {$find("mpeModal").hide();}
            }
            catch (e)
            {
            }
        });

function fnValidateCCEmail(source,args)
{
var email = document .getElementById ('<%=txtCCAdd.ClientID %>');
var filter =/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*/;
try{
if (filter.test(email.value)==true || email.value=="Other CC Emails"){args .IsValid=true ; }
else {args .IsValid=false ;}}
catch (e)
{
alert (e.message);
}
}
function fnValidateBCCEmail(source,args)
{
var email = document .getElementById ('<%=txtBCCAdd.ClientID %>');
var filter =/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*/;
if (filter.test(email.value)==true || email.value=="Other BCC Emails"){args .IsValid=true ; }
else {args .IsValid=false ;}
}
    </script>

    <script type="text/javascript">
    function EditModal(src,width,height)
    {   
        var iframe=document .getElementById ('iframe');
        iframe .src ="";
        iframe .width =width ;
        iframe .height = height ;
        iframe .src =src;
        $find('mpeModal').show();
        return false ;
    }
    function CloseModal()
    { 
        var iframe=document .getElementById ('iframe');
        
        iframe .src ="about:blank";
        $find('mpeModal').hide();
       
    }
 
    </script>

    <ajaxToolkit:ModalPopupExtender ID="mpeManagers" BehaviorID="mpeModal" runat="server"
        BackgroundCssClass="DarkModalBackground" DropShadow="false" PopupControlID="pnlHiringDetails"
        TargetControlID="lblTarget" OnCancelScript="ClosePopUP()">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Panel ID="pnlHiringDetails" runat="server" Style="display: none;">
        <asp:Label ID="lblTarget" runat="server"></asp:Label>
        <iframe id="iframe" width="778px" height="570px" frameborder="0" style="border-radius: 12px"
            scrolling="no"></iframe>
    </asp:Panel>
    <asp:UpdatePanel ID="upPanel" runat="server">
        <ContentTemplate>
            <ajaxToolkit:ModalPopupExtender Enabled="true" ID="ModalPopupExtender" runat="server"
                BackgroundCssClass="DarkModalBackground" DropShadow="false" PopupControlID="pnlResume"
                TargetControlID="lblRough" BehaviorID="MPES" OnCancelScript="CancelModal()">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnlResume" runat="server" Style="display: none;">
                <asp:Label ID="lblRough" runat="server"></asp:Label>
                <ucl:Template ID="uclTemplate" runat="server" ContentDisplay="table-cell" ContentWidth="785px"
                    ContentHeight="470px" ModalBehaviourId="MPES">
                    <Contents>
                        <uc1:AttachResume ID="ucAttachmentModal" runat="server" />
                    </Contents>
                </ucl:Template>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
    <div id="BodyMiddle" style="width: 99%">
        <div id="HMatrixContainer" style="height: 100%; padding-left: 0px;">
            <div id="HMatrixContent" style="overflow: auto; vertical-align: top; float: left;
                width: 100%; position: relative;">
                <div class="header">
                    <asp:HyperLink ID="lnkReqTitle" runat="server" CssClass="title" Target="_blank"></asp:HyperLink>
                    <asp:Label ID="lblClientTitle" runat="server" CssClass="title" Text=" - Submit to "
                        Style="text-decoration: none;"></asp:Label>
                    <asp:HyperLink ID="lnkClientTitle" runat="server" CssClass="title" ></asp:HyperLink>
                </div>
                <div class="content">
                    <div class="BulkActions" id="dvbulkAction" runat="server">
                        <div class="BulkActions">
                            <ul style="margin-top: 0px; margin-bottom: 0px; float: left; padding-left: 0px; margin-left: 0px">
                                <li>
                                    <asp:LinkButton ID="lnkBack" runat="server" Text="&laquo; Back" OnClick="btnBack_Click"></asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="CandidateGrid">
                        <div id="dvhirng" runat="server">
                            <div style="clear: both; padding: 5px; font-size: 11px;" id="divList">
                                <div class="SpecializedTitle" style="padding-bottom: 25px; display: none;">
                                    <asp:Label ID="lblTitle" runat="server" Font-Size="X-Large" Font-Bold="true"></asp:Label>
                                    <asp:HyperLink ID="lnkTitle" runat="server" Font-Size="X-Large" ></asp:HyperLink>
                                </div>
                                <asp:UpdatePanel ID="pnlDynamic" runat="server">
                                    <ContentTemplate>
                                        <asp:HiddenField ID="hdnSelectedIDS" runat="server" />
                                        <asp:HiddenField ID="hdnTo" runat="server" Value="" />
                                        <asp:HiddenField ID="hdncheck" runat="server" Value="" />
                                        <asp:HiddenField ID="hdnResumeDocumentID" runat="server" EnableViewState="true" />
                                        <asp:HiddenField ID="hdnMemberId" runat="server" EnableViewState="true" />
                                        <asp:HiddenField ID="hdnmemberName" runat="server" Value="(BU Contact)" />
                                        <div class="TableRow" style="text-align: left">
                                            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                                        </div>
                                        <div class="CommonHeader">
                                            <asp:Label ID="lblSelectedApplicantHeader" runat="server" Text="Candidates to Submit"></asp:Label>
                                        </div>
                                        <asp:ListView ID="lsvViewApplicantList" runat="server" DataKeyNames="Id" OnItemDataBound="lsvViewApplicantList_ItemDataBound"
                                            OnItemCommand="lsvViewApplicantList_ItemCommand">
                                            <LayoutTemplate>
                                                <table id="tlbTemplates" class="Grid" cellspacing="0" border="0">
                                                    <tr runat="server" id="trresuem">
                                                        <th style="width: 30px !important; white-space: nowrap;">
                                                            <input id="chkAllItems" name="chkAllItem" type="checkbox" onclick="CLCheckUnCheckAllRowLevelsCheckboxes(this,0)" />
                                                        </th>
                                                        <th style="width: 15%; white-space: nowrap;">
                                                            Name
                                                        </th>
                                                        <th style="width: 15%; white-space: nowrap;">
                                                            Location
                                                        </th>
                                                        <th style="width: 20%; white-space: nowrap;">
                                                            Email
                                                        </th>
                                                        <th style="width: 15%; white-space: nowrap;">
                                                            Mobile
                                                        </th>
                                                        <th style="width: 200px; white-space: nowrap;">
                                                            Resumes to Submit
                                                        </th>
                                                    </tr>
                                                    <tr id="itemPlaceholder" runat="server">
                                                    </tr>
                                                    </tr>
                                                </table>
                                            </LayoutTemplate>
                                            <EmptyDataTemplate>
                                                <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                                    <tr>
                                                        <td>
                                                            No resume found.
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EmptyDataTemplate>
                                            <ItemTemplate>
                                                <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                                    <td align="left">
                                                        <asp:CheckBox runat="server" ID="chkItemCandidates" OnClick="CheckUnCheckGridViewsHeaderCheckboxs('tlbTemplates','chkAllItems',0,1)"
                                                            Checked="true" />
                                                        <asp:HiddenField ID="hdnnID" runat="server" />
                                                    </td>
                                                    <td align="left">
                                                        <asp:HyperLink ID="lnkApplicant" runat="server" ></asp:HyperLink>
                                                        <asp:Label ID="lblApplicantName" runat="server" Text="" Visible="false"></asp:Label>
                                                        <asp:HiddenField ID="hndIds" runat="server" EnableViewState="true" />
                                                        <asp:HiddenField ID="hdnSelectedres" runat="server" EnableViewState="true" />
                                                        <asp:HiddenField ID="hdnDocumentiD" runat="server" EnableViewState="true" />
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblCityState" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblMobileNo" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <div style="width: 60%; overflow: hidden; text-overflow: ellipsis; float: left;">
                                                            <asp:Label ID="lblJobTitle" runat="server" Text=""></asp:Label>
                                                            <asp:HiddenField ID="hdnFileTypeId" runat="server" EnableViewState="true" />
                                                        </div>
                                                        <div align="center">
                                                            <asp:ImageButton ID="btnEdit" SkinID="sknEditButton" runat="server" CommandName="EditItem">
                                                            </asp:ImageButton></div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                        <br />
                                        <div class="CommonHeader">
                                            <asp:Label ID="lblSendEmailHeader" runat="server" Text="Submit Candidates by Email"></asp:Label>
                                        </div>
                                        <div class="TableRow">
                                            <div class="TableFormLeble" style="width: 20%">
                                                <asp:Label ID="lblFromHeader" runat="server" Text="From"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:Label ID="lblFromAdd" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="TableRow">
                                            <div class="TableFormLeble" style="width: 20%">
                                                <asp:Label ID="lblSenderNameHeader" runat="server" Text="Name"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:TextBox ID="txtSenderName" runat="server" CssClass="CommonTextBox"></asp:TextBox><span
                                                    class="RequiredField">*</span>
                                                <asp:RequiredFieldValidator ID="rfvSenderName" runat="server" ControlToValidate="txtSenderName"
                                                    ErrorMessage="Please enter name." Display="Dynamic"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="TableRow">
                                            <div class="TableFormLeble" style="width: 20%">
                                                <asp:Label ID="lblClientNameHeader" runat="server" Text="Account Name"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:Label ID="lblClientName" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        
                                        <div class="TableRow" div style="display: none">
                                            <div class="TableFormLeble" style="width: 20%">
                                                <asp:Label ID="lblClientEmailAddressHeader" runat="server" Text="To"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:Label ID="lblTo" runat="server" Text="" Visible="false"></asp:Label>
                                                <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                                    width: 25%; height: 100px; overflow: auto">
                                                    <div style="border-bottom: solid 1px #7f7f7f; color: Black; height: 15px; text-align: center;
                                                        background-color: #dcdde0; font-weight: bold;">
                                                        <asp:Label ID="toClientcontact" Text="Client Contacts" runat="server"></asp:Label></div>
                                                  <%--  <asp:CheckBoxList ID="lstTo" runat="server" SelectionMode="Multiple">
                                                    </asp:CheckBoxList>--%>
                                                    <asp:RadioButtonList ID="lstTo" runat="server"   ></asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                           <div class="TableRow">
                                            <div class="TableFormLeble" style="width: 20%">
                                                <asp:Label ID="lblDivisionTO" runat="server" Text="To" ></asp:Label>:
                                            </div>
                                            <div class="TableFormContent" style="vertical-align: top">                                            
                                                <asp:DropDownList ID="ddlDivisionListTO" runat="server" CssClass="chzn-select" TabIndex="30"
                                                Width="11%" >
                                                </asp:DropDownList>
                                               <span class="" style="font-family: Tahoma; font-size: 10pt; color: #FF0000;">*</span> 
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlDivisionListTO"
                                                    ErrorMessage="Please Select To." ValidationGroup="SendValGrpSubmit"  Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>                   
                                            </div>     
                                            </div>
                                        <%--<div class="TableRow" >
                                            <div class="TableFormContent" style="width: 25%; margin-left: 20.5%; font-size: small;" >
                                                <asp:TextBox ID="txtToAdd" runat="server" CssClass="CommonTextBox" onfocus="RemoveDefaultText(this)"
                                                    onblur="SetDefaultText(this)" ToolTip="Other TO Emails" Width="240px" visible="false"></asp:TextBox>
                                                <asp:CustomValidator ID="CustomValidator2" ControlToValidate="txtToAdd" runat="server"
                                                    ClientValidationFunction="fnValidateTOEmail" ErrorMessage="Please Enter Valid Email Address."
                                                    Display="Dynamic" ValidationGroup="ValGrpSubmit" SetFocusOnError="true"></asp:CustomValidator>
                                            </div>
                                        </div>--%>
                                        
                                        
                                          <div class="TableRow">
                                            <div class="TableFormLeble" style="width: 20%">
                                                <asp:Label ID="Label2" runat="server" Text="CC"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                 <ucl:MultipleSelection ID="uclDivisionListCC" runat="server" />
                                                   <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                                     width: 20%; height: 100px; overflow: auto; float: left; display: none">
                                                   <asp:CheckBoxList ID="ddlDivisionListCC" runat="server" ValidationGroup="ValGrpSubmit"
                                               AutoPostBack="false" TabIndex="25">
                                             </asp:CheckBoxList>
                                              </div>
                                            </div>
                                          </div>
                                        
                                         <div style="display: none">
                                        <div class="TableRow" style="width: 100%;">
                                            <div class="TableFormLeble" style="width: 20%">
                                                <asp:Label ID="lblCCHeader" runat="server" Text="CC"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                                    width: 25%; height: 100px; overflow: auto; float: left;">
                                                    <div style="border-bottom: solid 1px #7f7f7f; color: Black; height: 15px; text-align: center;
                                                        background-color: #dcdde0; font-weight: bold;">
                                                        <asp:Label ID="ccClientContacts" Text="Client Contacts" runat="server"></asp:Label></div>
                                                    <asp:CheckBoxList ID="lstCC" runat="server" SelectionMode="Multiple">
                                                    </asp:CheckBoxList>
                                                </div>
                                                <div style="float: right; width: 52%" align="left">
                                                    <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                                        width: 50%; height: 100px; overflow: auto;">
                                                        <div style="border-bottom: solid 1px #7f7f7f; color: Black; height: 15px; text-align: center;
                                                            background-color: #dcdde0; font-weight: bold;">
                                                            Assigned Recruiters</div>
                                                        <asp:CheckBoxList ID="lstCCAssigned" runat="server" SelectionMode="Multiple">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="TableRow">
                                            <div class="TableFormContent" style="width: 25%; margin-left: 20.5%; font-size: small;">
                                                <asp:TextBox ID="txtCCAdd" runat="server" CssClass="CommonTextBox" onfocus="RemoveDefaultText(this)"
                                                    onblur="SetDefaultText(this)" ToolTip="Other CC Emails" Width="240px"></asp:TextBox>
                                                <asp:CustomValidator ID="csvCC" ControlToValidate="txtCCAdd" runat="server" ClientValidationFunction="fnValidateCCEmail"
                                                    ErrorMessage="Please Enter Valid Email Address." Display="Dynamic" ValidationGroup="ValGrpSubmit"
                                                    SetFocusOnError="true"></asp:CustomValidator>
                                            </div>
                                          </div>
                                         </div>
                                         
                                         <div style="display: none">
                                        <div class="TableRow">
                                            <div class="TableFormLeble" style="width: 20%">
                                                <asp:Label ID="lblBCC" runat="server" Text="BCC"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                                    width: 25%; height: 100px; overflow: auto; float: left;">
                                                    <div style="border-bottom: solid 1px #7f7f7f; color: Black; height: 15px; text-align: center;
                                                        background-color: #dcdde0; font-weight: bold;">
                                                        <asp:Label ID="bccClientContacts" Text="Client Contacts" runat="server"></asp:Label></div>
                                                    <asp:CheckBoxList ID="lstBCC" runat="server" SelectionMode="Multiple">
                                                    </asp:CheckBoxList>
                                                </div>
                                                <div style="float: right; width: 52%" align="left">
                                                    <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                                        width: 50%; height: 100px; overflow: auto;">
                                                        <div style="border-bottom: solid 1px #7f7f7f; color: Black; height: 15px; text-align: center;
                                                            background-color: #dcdde0; font-weight: bold;">
                                                            Assigned Recruiters</div>
                                                        <asp:CheckBoxList ID="lstBCCAssigned" runat="server" SelectionMode="Multiple">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         </div>
                                         
                                         
                               <div class="TableRow">
                                <div class="TableFormLeble" style="width: 20%">
                                    <asp:Label EnableViewState="false" ID="lbOtherInterviewers" runat="server" Text="Other CC"></asp:Label>:
                                </div>
                                <div class="TableFormContent">
                                    <div style="width: 15%; height: auto; border: solid 1px #CCC; float: left;  padding: 4px;"
                                        id="divContent" runat="server" enableviewstate="true">
                                        <asp:TextBox EnableViewState="false" ID="txtOtherInterviewers" runat="server" CssClass="CommonTextBox"
                                            TabIndex="2" placeholder="Separate emails by commas" Width="95%">   </asp:TextBox>
                                        <asp:HiddenField ID="hfOtherInterviewers" runat="server" Value="" />
                                    </div>                                    
                                </div>
                               <%-- **************ADDED BY PRAVIN KHOT ON 24/June/2016***********--%>
                                <div class="TableFormContent">
                                    <div style="width: 100%; height: auto; margin-left: 34%; padding: 4px;"
                                        id="div1" runat="server" enableviewstate="true">
                                       <asp:Label EnableViewState="false" ID="lblotheremailids" runat="server" Text="( Email ID's separated by commas(,) or Semicolon(;) or tab )"
                                        Style="white-space: normal;  font-size: smaller"> </asp:Label>                                
                                    </div>                                   
                                </div>
                                <%--****************************END*****************************--%>
                            </div>
                          
                                  <div style="display: none">        
                                <div class="TableRow">
                                            <div class="TableFormContent" style="width: 25%; margin-left: 20.5%; font-size: small;">
                                                <asp:TextBox ID="txtBCCAdd" runat="server" CssClass="CommonTextBox" onfocus="RemoveDefaultText(this)"
                                                    onblur="SetDefaultText(this)" ToolTip="Other CC Emails" Width="240px"></asp:TextBox>
                                                <asp:CustomValidator ID="CustomValidator1" ControlToValidate="txtBCCAdd" runat="server"
                                                    ClientValidationFunction="fnValidateBCCEmail" ErrorMessage="Please Enter Valid Email Address."
                                                    Display="Dynamic" ValidationGroup="ValGrpSubmit" SetFocusOnError="true"></asp:CustomValidator>
                                            </div>
                                  </div>
                                     </div> 
                                        
                                        <div class="TableRow">
                                            <div class="TableFormLeble" style="width: 20%">
                                                <asp:Label ID="lblSubjectHeader" runat="server" Text="Subject"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent">
                                                <asp:TextBox ID="txtSubject" runat="server" CssClass="CommonTextBox" Width="600px"></asp:TextBox>
                                            </div>
                                        </div>
                                        
                                        
                                        <div>
                                            <div style="width: 45%; float: left;" runat="server" id="divMyTemplate">
                                                <div class="TableFormLeble" style="width: 45%">
                                                    <asp:Label ID="lblMyTemplateHeader" runat="server" Text="My Templates"></asp:Label>:
                                                </div>
                                                <div class="TableFormContent">
                                                    <asp:DropDownList ID="ddlMyTemplate" runat="server" CssClass="CommonDropDownList"
                                                        Width="158px" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div style="width: 45%; float: left;" runat="server" id="divMasterTemplate">
                                                <div class="TableFormLeble" style="width: 30%">
                                                    <asp:Label ID="lblMasterTemplateHeader" runat="server" Text="Master Templates"></asp:Label>:
                                                </div>
                                                <div class="TableFormContent">
                                                    <asp:DropDownList ID="ddlMasterTemplate" runat="server" CssClass="CommonDropDownList"
                                                        Width="158px" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="TableRow">
                                            <div class="TableFormLeble" style="width: 20%">
                                                <asp:Label ID="lblCoverLetterHeader" runat="server" Text="Cover Letter"></asp:Label>:
                                            </div>
                                            <div class="TableFormContent" style="width: 78%; float: right" align="left">
                                                <div style="margin-left: -12px">
                                                    <ucl:HtmlEditor ID="WHECoverLetter" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 20%">
                                        <asp:Label ID="lblAttachFileHeader" runat="server" Text="Attach File"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:FileUpload ID="FileUpload1" runat="server" Width="350px" />
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormValidatorContent" style="margin-left: 20%">
                                        <asp:RequiredFieldValidator ID="rfvDocumentUpload" runat="server" ErrorMessage="Please select a file to upload."
                                            ControlToValidate="FileUpload1" ValidationGroup="UploadDocumentValid" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revfuDocument" runat="server" ErrorMessage="Invalid File Format"
                                            ControlToValidate="FileUpload1" ValidationExpression="^(?!.*\.exe$).*$" Display="Dynamic"
                                            ValidationGroup="UploadDocumentValid"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 20%">
                                        <asp:Label ID="lblAttachmentsHeader" runat="server" Text="Attachments"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <table>
                                            <tr>
                                                <td rowspan=2 style="width: 300px;">
                                                    <div style="border: solid 1px #7f7f7f; border-collapse: collapse; color: #333333;
                                                        width: 98%; height: 100px; overflow: auto;">
                                                        <asp:CheckBoxList ID="lstAttachments" runat="server">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </td>
                                                <td >
                                                <asp:Button ID="btnTracker" runat="server" CssClass="CommonButton" Text="BU Tracker" OnClick="btnTracker_Click"
                                                             />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: bottom">
                                                    <div style="display: inline">
                                                        <asp:Button ID="btnAtach" runat="server" CssClass="CommonButton" Text="Attach" OnClick="btnAtach_Click"
                                                            ValidationGroup="UploadDocumentValid" />
                                                        <asp:Button ID="btnRemove" runat="server" CssClass="CommonButton" Text="Remove" OnClick="btnRemove_Click"
                                                            ValidationGroup="RemoveDocumentValid" /></div>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:HiddenField ID="hdnTmpFolder" runat="server" />
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormValidatorContent" style="margin-left: 20%">
                                        <asp:CustomValidator ID="CVAttachments" ClientValidationFunction="ValidateChkList"
                                            runat="server" ErrorMessage="Please select a file to remove." ValidationGroup="RemoveDocumentValid"></asp:CustomValidator>
                                    </div>
                                </div>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 20%">
                                    </div>
                                    <div class="TableFormContent" style="font-size: 11px;">
                                        <asp:CheckBox ID="chkMovetoNextLevel" runat="server" Checked="false" Text="Move Candidates to " />
                                        <asp:DropDownList ID="ddlNextLevel" runat="server" CssClass="CommonDropDownList">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                  <div style="display: none"> <%--****added by pravin on 2/Dec/2016********--%>
                                <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 20%">
                                    </div>
                                    <div class="TableFormContent" style="font-size: 11px;">
                                        <asp:CheckBox ID="chkReqStatus" runat="server" Text="Change Req Status to " Checked="false" />
                                        <asp:DropDownList ID="ddlReqStatus" runat="server" CssClass="CommonDropDownList">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                </div>
                                
                                <div class="TableFormContent" style="text-align: right; width: 58%; padding-bottom: 5px;
                                    display: none;">
                                    <asp:CheckBox ID="chkRequisitionDescription" runat="server" Text="Include Requisition Description"
                                        Checked="false"></asp:CheckBox>
                                </div>
                                <div class="TableRow" style="text-align: center; padding-top: 10px">
                                    <asp:Button ID="btnSend" CssClass="CommonButton" runat="server" Text="Send" OnClick="btnSend_Click" />
                                    <asp:Button ID="btnPreview" CssClass="CommonButton" runat="server" Text="Preview"
                                        OnClick="btnPreview_Click" ValidationGroup="ValGrpSubmit" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_load(function() {
            $(".chzn-select").chosen();
            $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
        });
</script>
</asp:Content>



