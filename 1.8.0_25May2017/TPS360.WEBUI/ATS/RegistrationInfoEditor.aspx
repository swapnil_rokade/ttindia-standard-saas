﻿<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="RegistrationInfoEditor.aspx.cs"  EnableEventValidation ="false" 
    Inherits="TPS360.Web.UI.CandidateRegistrationInfoEditor" Title="New Candidate Registration" %>

<%@ Register Src="~/Controls/RegistrationInternal.ascx" TagName="regI" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    New Candidate Registration
</asp:Content>
<asp:Content ID="cntRegistrationInformation" ContentPlaceHolderID="cphHomeMaster"
    runat="Server">
  
    <asp:UpdatePanel ID="pnlRegistrationInformation" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divRegInternal" runat="server">
                 <uc1:regI ID="rgInternal" runat="server" />                                      
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

