﻿<%-- 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: TPS360AccessCandidate.aspx
    Description: This is the page which is used ACCESS TO CANDIDATE
    Created By: PRAVIN KHOT 
    Created On: 17/May/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------       
--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/CandidateProfile.master" AutoEventWireup="true"  Async="true"
    CodeFile="TPS360AccessCandidate.aspx.cs" Inherits="TPS360.Web.UI.TPS360AccessCandidateControl" ValidateRequest="false"
    Title="Candidate Access" Trace="false" EnableEventValidation="false"%>

<%@ Register Src="~/Controls/EmailEditor.ascx" TagName="EmailEditor" TagPrefix="uc1" %>
<%--<%@ Register Src="~/Controls/AccessControl.ascx" TagName="AccessControl" TagPrefix="ucl" %>
<%@ Register Src="../Controls/MenuSelector.ascx" TagName="MenuSelector" TagPrefix="uc1" %>
<%@ Register Src="../Controls/ChangePassword.ascx" TagName="NewPassword" TagPrefix="uc2" %>
--%>
   
<asp:Content ID="cntTPS360AccessCandidate" ContentPlaceHolderID="cphCandidateMaster" runat="Server">

    <script src="../Scripts/fixWebkit.js" type="text/javascript"></script>

    <div class="tabbable">
        <div class="TabPanelHeader">
            Application Access</div>
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">Send Password Reset Email</a></li>
         <%--   <li><a href="#tab2" data-toggle="tab">Access Control</a></li>
            <li><a href="#tab3" data-toggle="tab">Assign Role</a></li>
            <li><a href="#tab4" data-toggle="tab">Assign Application Access</a></li>
            <li><a href="#tab5" data-toggle="tab">Change Password</a></li>--%>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <uc1:EmailEditor ID="ucntrlEmailEditor" runat="server" />
            </div>        
          
          
        </div>
    </div>
</asp:Content>
