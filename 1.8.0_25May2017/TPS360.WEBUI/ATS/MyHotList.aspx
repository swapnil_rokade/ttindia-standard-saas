﻿<%------------------------------------------------------------------------------------------------------------------------------------
    FileName: MyHotList.aspx
    Description: 
    Created By: 
    Created On:
    Modification Log:
    -----------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date           Author            Modification
    -----------------------------------------------------------------------------------------------------------------------------------
    0.1            Nov-18-2009          Sandeesh          Enhancement#11814 :AM Notification - When a recruiter adds a candidate to a req the app will send a notification email to the Account Manager
---------------------------------------------------------------------------------------------------------------------------------------

--%>

<%@ Page Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="MyHotList.aspx.cs"
    Async="true" Inherits="TPS360.Web.UI.MyHotList" Title="My Candidate Hot Lists" %>

<%@ Register Src="~/Controls/HotList.ascx" TagName="HotList" TagPrefix="uc1" %>
<asp:Content ID="cntLookupEditorHeader" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="cntLookupEditorTitle" ContentPlaceHolderID="cphHomeMasterTitle"
    runat="Server">
    My Hot Lists
</asp:Content>
<asp:Content ID="cntLookupEditor" ContentPlaceHolderID="cphHomeMaster" runat="Server">
    <asp:HiddenField ID="hfMemberId" runat="server" Value="0" />
    <uc1:HotList ID="uclHotList" CandidateOrConsultant="Candidates" IsMy="true" runat="server" />
</asp:Content>
