﻿
/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewFeedback.aspx
    Description: This is the InterviewFeedback page.
    Created By: Sumit Sonawane
    Created On: 08/Mar/2017
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TPS360.Common.Helper;
using System.Text;
using TPS360.Web.UI.Helper;
using TPS360.Common.BusinessEntities;
using TPS360.BusinessFacade;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using TPS360.Common.Shared;

namespace TPS360.Web.UI
{
    public partial class InternalInterviewFeedback : CandidateBasePage
    {
        #region Variables
        private bool isAccess = false;
        private static string UrlForCandidate = string.Empty;
        private static int SitemapIdForCandidate = 0;
        bool _IsAccessToCandidate = true;
        #endregion
        #region Properties
        int CandidateId
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);

                }
                else
                {
                    return 0;
                }

            }
        }
        string _cookiename;
        string CookieName
        {
            get { return _cookiename; }
            set { _cookiename = value; }
        }
        #endregion
        #region Methods
        private void PlaceUpDownArrow()
        {
            try
            {
                LinkButton lnk = (LinkButton)lsvInterviewFeedback.FindControl(hdnSortColumn.Text);
                HtmlTableCell im = (HtmlTableCell)lnk.Parent;
                im.Attributes.Add("class", (hdnSortOrder.Text == "ASC" ? "Ascending" : "Descending"));
                
            }
            catch
            {
            }

        }
        private void BindList()
        {
                this.lsvInterviewFeedback.DataSourceID = "odsInterviewFeedbackList";          
                this.divlsvInterview.Visible = true;
                this.lsvInterviewFeedback.DataBind();         
        }
        private void ClearControls()
        {
            //ddlClientInterviewers.SelectedIndex = 0;
            dtPicker.ClearRange();
            ddlClientInterviewers.Items.Clear();
            PrepareView();  
            ddlClientInterviewers.Enabled = true;
            txtCId.Text = string.Empty;
            txtname.Text = string.Empty;
            lsvInterviewFeedback.Visible = false;
        }       
        private void RenameListHeader(string headerTitle, string LinkName)
        {
            HtmlTableCell thHeaderTitle = (HtmlTableCell)lsvInterviewFeedback.FindControl(headerTitle);
            if (thHeaderTitle != null)
            {
                LinkButton link = (LinkButton)thHeaderTitle.FindControl(LinkName);
              if(link !=null)  link.Text = link.Text.Replace("Account", "Department");
              if (link != null) link.ToolTip  = link.ToolTip .Replace("Account", "Department");
            }
        }
      
        string[] ColumnValues(string val, string split)
        {
            string[] separator = new string[] { split };
            string[] splitvalues = val.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            return splitvalues;
        }            
      
        private void populateClientInterviers(int CompanyId)
        {
            PrepareView();
           
        }    
       
        private void PrepareView()
        {
            ddlClientInterviewers.Enabled = true;
            int InterviewId = 1; //Code by pravin khot -InterviewId is only mention is not used in SQL query 
            ddlClientInterviewers.DataSource = Facade.InterviewFeedback_GetByInterviewIdAllEmail(InterviewId);//Code by pravin khot -InterviewId is only mention is not used in SQL query 
            ddlClientInterviewers.DataTextField = "Title";
            //ddlClientInterviewers.DataValueField = "Id";
            ddlClientInterviewers.DataBind();
            ddlClientInterviewers = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlClientInterviewers);
           ddlClientInterviewers.Items.Insert(0, new ListItem("Please Select", "0"));
                  
        }
        private void GenerateRequisitionReport(string format)
        {          
           if (string.Equals(format, "pdf"))
            {
                PdfConverter pdfConverter = new PdfConverter();
                UnicodeEncoding unicodeEncoding = new UnicodeEncoding();
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfDocumentOptions.ShowFooter = false;
                pdfConverter.PdfDocumentOptions.LeftMargin = 15;
                pdfConverter.PdfDocumentOptions.RightMargin = 5;
                pdfConverter.PdfDocumentOptions.TopMargin = 15;
                pdfConverter.PdfDocumentOptions.BottomMargin = 5;
                pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = false;
                pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
                pdfConverter.PdfFooterOptions.FooterTextColor = Color.Black;
                pdfConverter.PdfFooterOptions.DrawFooterLine = true;
                pdfConverter.PdfFooterOptions.PageNumberText = "Page";
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;

                pdfConverter.LicenseKey = ContextConstants.EPHTMLTOPDFKEY;
                byte[] downloadBytes = null;
                downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(GetRequisitionReportTable());

                string mberEmail = Request.Form["rdoMemberEmailId"] != null ? Request.Form["rdoMemberEmailId"].ToString() : string.Empty;
                string[] memberEmail = mberEmail.Split(',');
                string Interviewmail =  memberEmail[1];

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.AddHeader("Content-Type", "binary/octet-stream");
                //************************************Code modify by Pravin khot on 2/Feb/2016 [CHENAGE BY FILENAME REMOVE SPACE AND USE (-)] ******************  
                response.AddHeader("Content-Disposition", "attachment; filename=InterviewFeedback-" + Interviewmail + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
                //response.AddHeader("Content-Disposition", "attachment; filename=InterviewFeedback-" + Interviewmail + " " + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf; size=" + downloadBytes.Length.ToString());
             
                response.Flush();
                response.BinaryWrite(downloadBytes);
                response.Flush();
                response.End();
            }
        }

        private string GetRequisitionReportTable()
        {
            StringBuilder FeedbackReport = new StringBuilder();
            string InterviewId = "";
            string Interviewmail = "";
            string candidateid = "";
            string candidatename = "";
            string Interviewdate = "";

          
            string mberEmail = Request.Form["rdoMemberEmailId"] != null ? Request.Form["rdoMemberEmailId"].ToString() : string.Empty;
            string[] memberEmail = mberEmail.Split(',');
           
           
            if (mberEmail != "")
                InterviewId = Convert.ToString(memberEmail[0]);
            Interviewmail = memberEmail[1];
            candidateid = Convert.ToString(memberEmail[2]);
            Interviewdate = memberEmail[3];
            candidatename = memberEmail[4];
            candidateid = "A" + candidateid;
                        
                     
                InterviewerFeedbackDataSource InterviewerFeedbackDS = new InterviewerFeedbackDataSource();
                IList<InterviewFeedback> InterviewList = InterviewerFeedbackDS.InterviewFeedbackReportPrint(InterviewId, Interviewmail, null, -1, -1);


                InterviewerFeedbackDataSource AssesReportDS = new InterviewerFeedbackDataSource();
                IList<InterviewFeedback> InterviewListAsses = AssesReportDS.InterviewAssesReportPrint(InterviewId, Interviewmail, null, -1, -1);

                if (InterviewList != null)
                {
                    string WebRoot = ConfigurationManager.AppSettings["WebRoot"].ToString();
                    string LogoPath;
                    if (string.IsNullOrEmpty(HttpRuntime.AppDomainAppVirtualPath))
                    {
                        LogoPath = WebRoot;
                    }
                    else
                    {
                        LogoPath = WebRoot + HttpRuntime.AppDomainAppVirtualPath;
                    }
                    FeedbackReport.Append("<table style='font-family : Arial;'>");

                    FeedbackReport.Append(" <tr>");
                    FeedbackReport.Append("     <td align=left  width='500' height='75'> <font size=5> <u> <b> Interview Feedback </b></u> </font> </td>");
                    FeedbackReport.Append("     <td align=right  width='500' height='75'>&nbsp;<img src='" + UrlConstants.ApplicationBaseUrl + "Images/logo-left-75px.png' style= style='height:56px;width:40px'/></td>");
                    FeedbackReport.Append(" </tr>");

                    FeedbackReport.Append("<table style='font-family : Arial;'>");
                 
                    foreach (InterviewFeedback interview in InterviewList)
                    {
                        if (interview != null)
                        {
                            FeedbackReport.Append(" <tr>");
                            FeedbackReport.Append("     <td>  Interview ID : </td>");
                            FeedbackReport.Append("     <td  align='left'>" + interview.Id + "&nbsp;</td>");
                            FeedbackReport.Append(" </tr>");

                            //FeedbackReport.Append(" <tr>");
                            //FeedbackReport.Append("     <td>  Candidate Name : </td>");
                            //FeedbackReport.Append("     <td  align='left'>" + candidateid + " " + "  " + candidatename + "&nbsp;</td>");
                            //FeedbackReport.Append("     <td>  Interview Date : </td>");
                            //FeedbackReport.Append("     <td  align='left'>" + Interviewdate.ToString() + "&nbsp;</td>");
                            //FeedbackReport.Append(" </tr>");

                            FeedbackReport.Append(" <tr>");
                            FeedbackReport.Append("     <td>  Interview Date : </td>");
                            FeedbackReport.Append("     <td  align='left'>" + Interviewdate.ToString() + "&nbsp;</td>");
                            FeedbackReport.Append(" </tr>");

                            FeedbackReport.Append(" <tr>");
                            FeedbackReport.Append("     <td>  Candidate Name : </td>");
                            FeedbackReport.Append("     <td  align='left'>" + candidateid + " " + "  " + candidatename + "&nbsp;</td>");
                            FeedbackReport.Append(" </tr>");                           

                            FeedbackReport.Append(" <tr>");
                            FeedbackReport.Append("     <td>  Feedback Comments : </td>");
                            FeedbackReport.Append("     <td  align='left'>" + interview.OverallFeedback.ToString() + "&nbsp;</td>");
                            FeedbackReport.Append(" </tr>");


                            FeedbackReport.Append(" <tr>");
                            FeedbackReport.Append("     <td>  Overall Status : </td>");
                            FeedbackReport.Append("     <td  align='left'>" + interview.Feedback.ToString() + "&nbsp;</td>");
                            FeedbackReport.Append(" </tr>");

                            FeedbackReport.Append(" <tr>");
                            FeedbackReport.Append("     <td>  Round : </td>");
                            FeedbackReport.Append("     <td  align='left'>" + interview.InterviewRoundRpt.ToString() + "&nbsp;</td>");
                            FeedbackReport.Append(" </tr>");

                            FeedbackReport.Append(" <tr>");
                            FeedbackReport.Append("     <td>  Document Type : </td>");
                            FeedbackReport.Append("     <td  align='left'>" + interview.DocumentName.ToString() + "&nbsp;</td>");
                            FeedbackReport.Append(" </tr>");

                            FeedbackReport.Append(" <tr>");
                            FeedbackReport.Append("     <td>  Interviewer Name : </td>");//Code modified by Pravin Khot on 15/Feb/2016
                            FeedbackReport.Append("     <td  align='left'>" + interview.CandidateName + " &nbsp;</td>"); //Code modified by Pravin Khot on 15/Feb/2016
                            FeedbackReport.Append(" </tr>");                           

                            FeedbackReport.Append(" <tr>");
                            FeedbackReport.Append("     <td>  Interviewer Email : </td>");
                            FeedbackReport.Append("     <td  align='left'>" + interview.Email.ToString() + "&nbsp;</td>");
                            FeedbackReport.Append(" </tr>");

                            FeedbackReport.Append(" <tr>");
                            FeedbackReport.Append("     <td>  Emp ID : </td>");
                            FeedbackReport.Append("     <td  align='left'>" + interview.EmployeeID.ToString() + "&nbsp;</td>");
                            FeedbackReport.Append(" </tr>");

                            FeedbackReport.Append(" <tr>");
                            FeedbackReport.Append("     <td>  </td>");
                            FeedbackReport.Append("     <td  >&nbsp;</td>");
                            FeedbackReport.Append(" </tr>");
                        }
                    }
                    FeedbackReport.Append("</table>");

                    FeedbackReport.Append("    <tr>");
                    FeedbackReport.Append("   <td height='30'></td>");
                    FeedbackReport.Append("    </tr>");

                    FeedbackReport.Append("<table style='font-family : Arial;'>");
                    FeedbackReport.Append(" <tr>");
                    FeedbackReport.Append("     <td align=left  width='500' height='75'> <font size=5> Interview Assessment Sheet </font> </td>");
                    FeedbackReport.Append(" </tr>");
                    FeedbackReport.Append("</table>");


                    FeedbackReport.Append("    <tr>");
                    FeedbackReport.Append("   <td height='10'></td>");
                    FeedbackReport.Append("    </tr>");

                     
                    FeedbackReport.Append("<table style='font-family : Arial;width:100%; border-collapse:collapse; border-spacing: 1px;' align='left' border = '1' bordercolor='#000000'  cellspacing='0' cellpadding='0'>");


                    if (InterviewListAsses != null)
                    {

                        FeedbackReport.Append(" <tr>");

                        FeedbackReport.Append("     <th align='center'>Sr.No.</th>");

                        FeedbackReport.Append("     <th align='center'>Question</th>");

                        FeedbackReport.Append("     <th align='center'>Response</th>");

                        FeedbackReport.Append("     <th align='center'>Rationale</th>");

                        FeedbackReport.Append(" </tr>");

                        int Count = 0;
                        foreach (InterviewFeedback interviewAsses in InterviewListAsses)
                        {
                            if (interviewAsses != null)
                            {
                                Count = Count + 1;
                                FeedbackReport.Append(" <tr>");
                                FeedbackReport.Append("     <td align='center' style='width:50px;'>" + Count + "&nbsp;</td>");

                                FeedbackReport.Append("     <td align='left' >" + interviewAsses.Question.ToString() + "&nbsp;</td>");
                                //*****************code added by pravin khot on 24Jan/2016*********************
                                string QAnswerType = interviewAsses.AnswerType.ToString();
                                if (QAnswerType == "Y/N")
                                {
                                    if (interviewAsses.Response == "1")
                                    {
                                        interviewAsses.Response = "Yes";
                                    }
                                    else if (interviewAsses.Response == "2")
                                    {
                                        interviewAsses.Response = "No";
                                    }                                    
                                }
                                else if (QAnswerType == "Y/N/NA")
                                {
                                    if (interviewAsses.Response == "1")
                                    {
                                        interviewAsses.Response = "Yes";
                                    }
                                    else if (interviewAsses.Response == "2")
                                    {
                                        interviewAsses.Response = "No";
                                    }
                                    else if (interviewAsses.Response == "3")
                                    {
                                        interviewAsses.Response = "NA";
                                    }   
                                }
                                //********************************End*********************************************

                                FeedbackReport.Append("     <td align='center' >" + interviewAsses.Response.ToString() + "&nbsp;</td>");

                                FeedbackReport.Append("     <td align='center' >" + interviewAsses.rationale.ToString() + "&nbsp;</td>");

                                FeedbackReport.Append(" </tr>");
                            }
                        }
                    }

                    FeedbackReport.Append(" </table>");
                }               
          
            return FeedbackReport.ToString();
        }
      
        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {

            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                repor("pdf");
            }

        }
        public void repor(string rep)
        {
            
                GenerateRequisitionReport(rep);
           
        }
        #endregion
        # region Page Event
        protected void Page_Load(object sender, EventArgs e)
        {
            //***************code*************************
            //String val = Session["key"].ToString();
            //txtname.Text = val;
            //txtCId.Text = val;
            //ddlClientInterviewers.Enabled = true;
            //if (Request.Cookies["PreviousPage"] != null)
                //ddlClientInterviewers.SelectedItem.Text  = Request.Cookies["PreviousPage"].Value   ;
               //string cookiesvalue = Server.HtmlEncode(aCookie.Value);
               //ddlClientInterviewers.SelectedValue = Server.HtmlEncode(aCookie.Value);
            //************************end****************************************

            //btnSearch_Click(sender, e);

            if (!IsPostBack)
            {
                PrepareView();
                btnSearch_Click(sender, e);
            }  
        }
        #endregion
        #region Button Events
        protected void btnClear_Click(object sender, EventArgs e)
        {
            ClearControls();
            divExportButtons.Visible = false;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lsvInterviewFeedback.Visible = true;
            //hdnScrollPos.Value = "0";
            lsvInterviewFeedback.Items.Clear();
            ASP.controls_pagercontrol_ascx PagerControls = (ASP.controls_pagercontrol_ascx)this.lsvInterviewFeedback.FindControl("pagerControl");
            if (PagerControls != null)
            {
                DataPager pager = (DataPager)PagerControls.FindControl("pager");
                if (pager != null)
                {
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            divExportButtons.Visible = false;
            txtCId.Text = CandidateId.ToString();
                BindList();
            
            if (lsvInterviewFeedback != null && lsvInterviewFeedback.Items.Count > 0)
            {
                divExportButtons.Visible = true;
                lsvInterviewFeedback.Visible = true;

                if (hdnSortColumn.Text == "") hdnSortColumn.Text = "btnDate";
                if (hdnSortOrder.Text == "") hdnSortOrder.Text = "DESC";
                PlaceUpDownArrow();
            }
            else divExportButtons.Visible = false;
            string pagesize = "";
            pagesize = (Request.Cookies["InterviewFeedbackReportRowPerPage"] == null ? "" : Request.Cookies["InterviewFeedbackReportRowPerPage"].Value); ;
            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvInterviewFeedback.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    pager.PageSize = Convert.ToInt32(pagesize == "" ? "20" : pagesize);
                    if (pager.Page.IsPostBack)
                        pager.SetPageProperties(0, pager.MaximumRows, true);
                }
            }
            System.Web.UI.HtmlControls.HtmlTableCell tdpager = (System.Web.UI.HtmlControls.HtmlTableCell)lsvInterviewFeedback.FindControl("tdpager");
            int count = 0;           
            count = 8;
            if (tdpager != null) tdpager.ColSpan = count;

            //populateClientInterviers(Convert.ToInt32(ddlClient.SelectedValue));
            //ControlHelper .SelectListByValue (ddlClientInterviewers ,hdnSelectedContactID .Value );
        }
      
        protected void btnExportToPDF_Click(object sender, EventArgs e)
        {           
            string mberEmail = Request.Form["rdoMemberEmailId"] != null ? Request.Form["rdoMemberEmailId"].ToString() : string.Empty;
            if (mberEmail != "")
            {
                repor("pdf");
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please Select One Interview In The List.", true);
            }
        }
      
        #endregion
        #region Listview Events

        protected void lsvInterviewFeedback_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
           
            if (ControlHelper.IsListItemDataRow(e.Item.ItemType))        
            {
               InterviewFeedback interview = ((ListViewDataItem)e.Item).DataItem as InterviewFeedback;

                if (interview != null)
                {
                  
                    Label lblMemberEmailId = (Label)e.Item.FindControl("lblMemberEmailId");

                    HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnID");
                  
                   
                    Label lblCandidateID = (Label)e.Item.FindControl("lblCandidateID");
                    Label lblCandidateName = (Label)e.Item.FindControl("lblCandidateName");
                    Label lblReqCode = (Label)e.Item.FindControl("lblReqCode");
                    Label lblJobTitle = (Label)e.Item.FindControl("lblJobTitle");
                    Label lblInterviewerName = (Label)e.Item.FindControl("lblInterviewerName");
                    Label lblInterviewerDate = (Label)e.Item.FindControl("lblInterviewerDate");
                    Label lblFeedback = (Label)e.Item.FindControl("lblFeedback");

                        hdnID.Value = interview.InterviewId.ToString();

                        //lblCandidateID.Text = "A" + (interview.CandidateID).ToString();   //add by pravin khot add  "A" + on 13/Jan/2016                  
                  
                        //lblCandidateName.Text = interview.CandidateName ;

                        lblReqCode.Text =  interview.JobTitle;

                        //lblJobTitle.Text = Convert.ToString(interview.ReqCode);   
                        lblJobTitle.Text = interview.JobPostingCode;

                        lblInterviewerName.Text = interview.InterviewerName;

                        //lblInterviewerDate.Text = Convert.ToString(interview.InterviewerDate);
                        lblInterviewerDate.Text = interview.InterviewerDate.ToShortDateString();
                               
                        lblFeedback.Text = interview.Feedback;

                        string InterviewerEmail = interview.InterviewerName;
                        lblMemberEmailId.Text = "<input id='rdoMemberEmailId' name='rdoMemberEmailId' type='radio' value='" + interview.InterviewId + "," + InterviewerEmail + "," + interview.CandidateID + "," + interview.InterviewerDate + "," + interview.CandidateName + "' style='width:20px;'/>"; 
                    
                                   
                }

            }
        }
        protected void lsvInterviewFeedback_PreRender(object sender, EventArgs e)
        {

            ASP.controls_pagercontrol_ascx PagerControl = (ASP.controls_pagercontrol_ascx)this.lsvInterviewFeedback.FindControl("pagerControl");
            if (PagerControl != null)
            {
                DataPager pager = (DataPager)PagerControl.FindControl("pager");
                if (pager != null)
                {
                    DropDownList ddlrowPerPage = (DropDownList)pager.Controls[0].FindControl("ddlRowPerPage");
                    if (ddlrowPerPage != null) ControlHelper.SelectListByValue(ddlrowPerPage, pager.PageSize.ToString());
                }
                HiddenField hdnRowPerPageName = (HiddenField)PagerControl.FindControl("hdnRowPerPageName");
                if (hdnRowPerPageName != null) hdnRowPerPageName.Value = "InterViewFeedbackReportRowPerPage";
            }
            PlaceUpDownArrow();
                  
            if (IsPostBack)
            {
                if (PagerControl == null)
                {
                    lsvInterviewFeedback.Items.Clear();
                    lsvInterviewFeedback.DataSource = null;
                    lsvInterviewFeedback.DataBind();
                    divExportButtons.Visible = lsvInterviewFeedback.Items.Count > 0;
                }
            }
        }
        protected void lsvInterviewFeedback_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Sort")
                {
                    LinkButton lnkbutton = (LinkButton)e.CommandSource;
                    if (hdnSortColumn.Text == lnkbutton.ID)
                    {
                        if (hdnSortOrder.Text == "ASC") hdnSortOrder.Text = "DESC";
                        else hdnSortOrder.Text = "ASC";
                    }
                    else
                    {
                        hdnSortColumn.Text = lnkbutton.ID;
                        hdnSortOrder.Text = "ASC";
                    }
                }
            }
            catch
            { }                                   
                                           
                                    
        }
        #endregion
    }
}
