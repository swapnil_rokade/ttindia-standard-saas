﻿<%@ Page Language="C#" MasterPageFile="~/Masters/CandidateProfile.master" AutoEventWireup="true" MaintainScrollPositionOnPostback ="true" 
    EnableEventValidation="false" CodeFile="InternalInterviewSchedule.aspx.cs" Inherits="TPS360.Web.UI.CandidateInternalInterviewSchedule"
    Title="Interviews" %>

<%@ Register Src="~/Controls/MemberInterviewSchedule.ascx" TagName="MemberInterview"
    TagPrefix="uc1" %>
<asp:Content ID="cntInternalInterviewSchedule" ContentPlaceHolderID="cphCandidateMaster"
    runat="Server">
    <uc1:MemberInterview ID="ucntrlMemberInterview" runat="server" />
</asp:Content>
