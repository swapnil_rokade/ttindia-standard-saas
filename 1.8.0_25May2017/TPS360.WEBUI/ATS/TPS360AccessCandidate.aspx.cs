﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: TPS360AccessCandidateControl.aspx.cs
    Description: This page is used for Employee Access tab functionality.
    Created By: pravin khot
    Created On: 17/May/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using TPS360.Common.BusinessEntities;
using System;
using System.Collections;
using System.Web.UI.WebControls;
using TPS360.Web.UI.Helper;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Web.Security;
using System.Collections.Generic;

namespace TPS360.Web.UI
{
    public partial class TPS360AccessCandidateControl : CandidateBasePage
    {
        #region Member Variables

        int _memberId = 0;

        #endregion

        #region Properties

        protected int WorkingEmployeeId
        {
            get
            {
                int memberId = 0;

                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                }
                else
                {
                    memberId = base.CurrentMember.Id;
                }

                return memberId;
            }
        }

        #endregion

        #region Methods

        private void PrepareView()
        {
            MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(this.WorkingEmployeeId);

            if (map != null)
            {
                //ControlHelper.SelectListByValue(ddlRole, StringHelper.Convert(map.CustomRoleId));
               
            }

            SiteMapType[] siteMapTypes = new SiteMapType[] {SiteMapType.ApplicationTopMenu,
                                                                    SiteMapType.EmployeeOverviewMenu,
                                                                    SiteMapType.CandidateOverviewMenu,
                                                                    SiteMapType.CompanyOverviewMenu,
                                                                    SiteMapType.EmployeePortalMenu, 
                                                                    SiteMapType.CandidateCareerPortalMenu, 
                                                                      SiteMapType .DepartmentOverviewMenu ,
                                                                    SiteMapType .VendorProfileMenu,
                                                                    SiteMapType .VendorPortalTopMenu ,
                                                                    SiteMapType .CandidateProfileMenuForVendor 
                                                            };

            //ctlMenuSelector.CurrentSiteMapTypes = siteMapTypes;

            //ctlMenuSelector.SelectList = Facade.GetAllMemberPrivilegeIdsByMemberId(this.WorkingEmployeeId);
            //ctlMenuSelector.BindList();

            int employeeId = 0;
            int currentMemberId = CurrentMember.Id;
            MemberManager memberManager = null;

            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                employeeId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }

            if (employeeId > 0)
            {
                memberManager = Facade.GetMemberManagerByMemberIdAndManagerId(employeeId, currentMemberId);
            }

            if (base.IsUserAdmin)
            {
                //uwtTPS360Access.Tabs[4].Visible = true;
            }
            else if (memberManager != null)
            {
            //    uwtTPS360Access.Tabs[4].Visible = true;
            }
        }

        private void AssignRole()
        {
            //if (ddlRole.SelectedItem.Text.ToLower() == ContextConstants.ROLE_ADMIN.ToLower())
            //{
            //    string strUserName = string.Empty;
            //    if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            //    {
            //        _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            //    }

            //    if (_memberId > 0)
            //    {
            //        strUserName = Facade.GetMemberUserNameById(_memberId);
            //        if (!Roles.IsUserInRole(strUserName, ContextConstants.ROLE_ADMIN))
            //            Roles.AddUserToRole(strUserName, ContextConstants.ROLE_ADMIN);

            //    }

            //}
            //else{
            //      if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            //    {
            //        _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            //    }

            //      if (_memberId > 0)
            //      {
            //          string strUserName = Facade.GetMemberUserNameById(_memberId);
            //          if (Roles.IsUserInRole(strUserName, ContextConstants.ROLE_ADMIN))
            //              Roles.RemoveUserFromRole(strUserName, ContextConstants.ROLE_ADMIN);


            //      }

            //}

            //MemberCustomRoleMap map = Facade.GetMemberCustomRoleMapByMemberId(this.WorkingEmployeeId);

            //int roleId = Convert.ToInt32(ddlRole.SelectedItem.Value);

            //if (map == null)
            //{
            //    map = new MemberCustomRoleMap();
            //    map.CustomRoleId = roleId;
            //    map.MemberId = this.WorkingEmployeeId;
            //    map.CreatorId = base.CurrentMember.Id;

            //    Facade.AddMemberCustomRoleMap(map);
            //    AddDefaultMenuAccess();
            //}
            //else if (map.CustomRoleId != roleId)
            //{
            //    map.CustomRoleId = roleId;
            //    map.UpdatorId = base.CurrentMember.Id;
            //    Facade.UpdateMemberCustomRoleMap(map);
            //    AddDefaultMenuAccess();
            //}
       

            PrepareView();

            //MiscUtil.ShowMessage(lblMessageAssignRole, "Role assigned successfully.", false);
        }

        private void AddDefaultMenuAccess()
        {
            int roleId = 5;//Convert.ToInt32(ddlRole.SelectedItem.Value);
            ArrayList previlegeList = Facade.GetAllCustomRolePrivilegeIdsByRoleId(roleId);

            if (previlegeList != null && previlegeList.Count >= 0)
            {
                Facade.DeleteMemberPrivilegeByMemberId(this.WorkingEmployeeId);

                for (int i = 0; i <= previlegeList.Count - 1; i++)
                {
                    MemberPrivilege previlege = new MemberPrivilege();

                    previlege.CustomSiteMapId = Convert.ToInt32(previlegeList[i]);
                    previlege.MemberId = this.WorkingEmployeeId;
                    Facade.AddMemberPrivilege(previlege);
                }
            }
        }

        private void SaveMenuAccess()
        {
            //TreeNodeCollection checkedNodes = ctlMenuSelector.SiteMapTree.CheckedNodes;
            //if (IsUserAdmin)
            //    Facade.DeleteMemberPrivilegeByMemberId(this.WorkingEmployeeId);
            //else
            //    Facade.DeleteByMemberIdWithOutAdminAccess(this.WorkingEmployeeId);

            //foreach (TreeNode node in checkedNodes)
            //{
            //    MemberPrivilege memberPrivilege = new MemberPrivilege();
            //    memberPrivilege.CustomSiteMapId = Convert.ToInt32(node.Value);
            //    memberPrivilege.MemberId = this.WorkingEmployeeId;
            //    Facade.AddMemberPrivilege(memberPrivilege);
                        
            //}
            //MiscUtil.ShowMessage(lblMessageAssignAccess, "Permission assigned successfully.", false);
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ucntrlEmailEditor.EmailFromType = EmailFromType.TPS360Access;    
            //uwtTPS360Access.Tabs[4].Visible = false;

            //MiscUtil.PopulateCustomRole(ddlRole, Facade);
            //if (!base.IsUserAdmin)
            //{
            //    ListItem list = new ListItem();
            //    list = ddlRole.Items.FindByText("Admin");
            //    ddlRole.Items.Remove(list);
            //}
            if (!IsPostBack)
            {
                PrepareView();
                //ddlRole.Items.Remove(ddlRole.Items.FindByText("Candidate"));
                //ddlRole.Items.Remove(ddlRole.Items.FindByText("Vendor"));
            }

            if (IsPostBack)
            {
                MiscUtil.LoadAllControlState(Request, this);
            }

            if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
            {
                _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
            }
            if (_memberId > 0)
            {
                string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                this.Page.Title =MiscUtil .RemoveScript ( name,string .Empty ) + " - " + "Access";
            }
        }

        protected void btnAssignRole_Click(object sender, EventArgs e)
        {

            //if (ddlRole.SelectedIndex == 0)
            //{
            //    MiscUtil.ShowMessage(lblMessageAssignRole, "Please select role to assign.", false);//0.3
            //    return;
            //}

            AssignRole();
           // updPnlMenuAccess.Update();
        }

        protected void btnSaveMenuAccess_Click(object sender, EventArgs e)
        {
            SaveMenuAccess();
        }

        #endregion
    }
}