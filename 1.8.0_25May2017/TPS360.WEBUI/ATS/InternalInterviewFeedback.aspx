﻿<%-- -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewFeedback.aspx
    Description: This is the InterviewFeedback page.
    Created By: Sumit Sonawane
    Created On: 08/Mar/2017
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/CandidateProfile.master" AutoEventWireup="true" CodeFile="InternalInterviewFeedback.aspx.cs"
    Inherits="TPS360.Web.UI.InternalInterviewFeedback" Title="Interview Feedback" EnableEventValidation="false"
    EnableViewStateMac="false" %>
    <%@ Register Src="~/Controls/DateRangePicker.ascx" TagName="DateRangePicker" TagPrefix ="ucl" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/PagerControl.ascx" TagName="Pager" TagPrefix="ucl" %>
<%@ Register Src="~/Controls/ConfirmationWindow.ascx" TagName="Confirm" TagPrefix="ucl" %>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="cphHomeMasterTitle" runat="Server">
    Interview Feedback
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="cphCandidateMaster" runat="Server">
                        
    <script type="text/javascript" language="javascript" src="../js/AjaxVariables.js"></script>

    <script type="text/javascript" language="javascript" src="../js/AjaxScript.js"></script>

    <script src="../Scripts/jsUpdateProgress.js" type="text/javascript"></script>
  

    <asp:TextBox ID="hdnSortColumn" runat="server" Visible="false" />
    <asp:TextBox ID="hdnSortOrder" runat="server" Visible="false" />
    <ucl:Confirm ID="uclConfirm" runat="server"></ucl:Confirm>
    <asp:UpdatePanel ID="pnlInterviewFeedbackReports" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="text-align: left">
                <asp:Label ID="lbMessage" runat="server" EnableViewState="false"></asp:Label>
            </div>
           
            <asp:ObjectDataSource ID="odsInterviewFeedbackList" runat="server" SelectMethod="InterviewFeedbackReportGetPaged"
                TypeName="TPS360.Web.UI.InterviewerFeedbackDataSource" SelectCountMethod="GetListCount"
                EnablePaging="True" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlClientInterviewers" Name="InterviewerName" PropertyName="SelectedItem.Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="dtPicker" Name="StartDate" PropertyName="StartDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="dtPicker" Name="EndDate" PropertyName="EndDate"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtCId" Name="Cid" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtname" Name="CName" PropertyName="Text"
                        Type="String" />                  
                </SelectParameters>
            </asp:ObjectDataSource>
                                       
            <asp:Panel ID="pnlSearchRegion"  runat="server" DefaultButton="btnSearch">
            
                <div style="width: 100%;">
                      <div class="TableRow" style="visibility: hidden">
                            <div class="TabPanelHeader nomargin" style="border-style: none none none;">Filter Options</div>
                        </div>
                        <div class="TableRow" style="text-align: center">
                            <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                   <%-- <asp:CollapsiblePanelExtender ID="cpeSeafvfrchBody" runat="server" TargetControlID="pnlSearchContent"
                            ExpandControlID="pnlSearchHeader" CollapseControlID="pnlSearchHeader" ImageControlID="ImgSearch"
                            CollapsedImage="~/Images/expand-plus.png" ExpandedImage="~/Images/collapse-minus.png"
                            SuppressPostBack="false" EnableViewState="true">
                        </asp:CollapsiblePanelExtender> --%>               
                        
                      
                       <%-- <asp:Panel ID="pnlSearchContent" runat="server" Visible="true" Style="overflow: hidden;" Height="0">  --%>                         
                            
                            <div class="FormLeftColumn" style="width: 48%; visibility: hidden; height: 0px;">
                            
                           
                          
                            
                            <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label ID="lblcid" runat="server" EnableViewState="false" Text="Candidate Id"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:TextBox ID="txtCId" runat="server" CssClass="ApplicantIDTextbox"></asp:TextBox>  <%--code change by pravin khot change CssClass="ApplicantIDTextbox" for added 'A' --%>  
                                    </div>                                   
                                   
                                </div>
                                
                                  <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label ID="lbldate" runat="server" EnableViewState="false"  Text="Date"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                       <ucl:DateRangePicker ID="dtPicker" runat ="server"  />                                 
                                    </div>
                                </div>     
                                                      
                                                           
                               
                            </div>
                             <div class="FormRightColumn" style="width: 48% ; visibility: hidden; height: 0px;">
                                  <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label ID="lblname" runat="server" EnableViewState="false" Text="Candidate Name"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:TextBox ID="txtname" runat="server" CssClass="SearchTextbox"></asp:TextBox>
                                    </div>                                  
                                   
                                </div>                       
                                                                                   
                             
                                 <div class="TableRow">
                                    <div class="TableFormLeble" style="width: 40%;">
                                        <asp:Label ID="lblClientInterviewers" runat="server" EnableViewState="false" Text="Interviewer Name"></asp:Label>:
                                    </div>
                                    <div class="TableFormContent">
                                        <asp:DropDownList ID="ddlClientInterviewers"  AutoPostBack ="false"  Width="30%"  runat="server" CssClass="CommonDropDownList"
                                            Enabled="false">
                                        </asp:DropDownList>
                                       <%-- <asp:HiddenField ID="hdnSelectedContactID" runat="server" Value="0" />--%>
                                    </div>                                                          
                                
                                </div>   
                                
                            </div>                           
                                                                       
                        <div class="TableRow" style="text-align: center; visibility: hidden; height: 0px; padding-top: 5px">
                            <asp:LinkButton ID="btnSearch" runat="server" Text="Search" AlternateText="Search" CssClass="btn btn-primary"
                                EnableViewState="false" OnClick="btnSearch_Click" ><i class="icon-search icon-white"></i> Search</asp:LinkButton>
                            <asp:Button ID="btnClear" runat="server" Text="Clear" AlternateText="Clear" CssClass="btn"
                                EnableViewState="false" CausesValidation="false" OnClick="btnClear_Click" />
                        </div>
                     <%--   </asp:Panel>--%>
                     
                        <div class="TableRow" style="padding-bottom: 5px" id="divExportButtons" runat="server"
                            visible="true">
                            <div class="TabPanelHeader nomargin" style="margin-bottom: 5px;border-style: none none none;">Interview Feedback</div>
                                                        
                            <asp:ImageButton ID="btnExportToPDF" CssClass ="btn"  SkinID ="sknExportToPDF" runat ="server" ToolTip ="Export To PDF" ValidationGroup="chklist" OnClientClick="javascript:Validate_Checkbox();return true;" OnClick ="btnExportToPDF_Click" />
                             <asp:CustomValidator ID="cvInternalInterviewer" runat="server" ClientValidationFunction="chklist"
                                        Display="Dynamic" EnableClientScript="true" ErrorMessage="Select at least one Interviewer"
                                        ValidationGroup="InterviewSchedule"></asp:CustomValidator>                       
                         </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlGridRegion" runat="server">
                <div id="divlsvInterview" runat="server" class="GridContainer" style="overflow: auto;
                     width: 100%; text-align: left;" onscroll='SetScrollPosition()'>
                    <asp:ListView ID="lsvInterviewFeedback" runat="server" DataKeyNames="Id" OnItemDataBound="lsvInterviewFeedback_ItemDataBound"
                        OnItemCommand="lsvInterviewFeedback_ItemCommand" OnPreRender="lsvInterviewFeedback_PreRender">
                        <LayoutTemplate>
                            <table id="tlbTemplate" runat="server" class="Grid ReportGrid" cellspacing="0" border="0">
                                <tr>                                  
                                   
                                   <th style="width: 40px !important">
                                        <asp:Label ID="lblSelect" runat="server" Text="Select" />
                                    </th>
                                    
                                    <%-- <th runat="server" id="thId" style="min-width: 50px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="lnkID" style="min-width: 50px"  CommandName="Sort" CommandArgument="[IV].[MemberId]"
                                            Text="Candidate ID #" ToolTip="Sort By Candidate ID #" />
                                    </th>
                                    
                                    <th runat="server" id="thCandidateName" style="min-width: 120px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnCandidateName" CommandName="Sort" CommandArgument="[C].[CandidateName]"
                                            Text="Candidate Name" ToolTip="Sort By Candidate Name" />
                                    </th>--%>
                                   
                                    <th runat="server" id="thReqCode" style="min-width: 80px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnReqCode" CommandName="Sort" CommandArgument="[Iv].[JobPostingId]"
                                            Text="Req. Code" ToolTip="Sort By Req Code" />
                                    </th>
                                     <th runat="server" id="thJobTitle" style="min-width: 80px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnJobTitle" CommandName="Sort" CommandArgument="[Iv].[Title]"
                                            Text="Interview Title" ToolTip="Sort By Job Title" />
                                    </th>
                                    
                                     <th runat="server" id="thInterviewerName" style="min-width: 150px"  enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnInterviewerName" CommandName="Sort" CommandArgument="[Ifeed].[EmailId]"
                                            Text="Interviewer Email" ToolTip="Sort By Interviewer Name" />
                                    </th>
                                    
                                     <th runat="server" id="thDate" style="min-width: 80px" enableviewstate ="false" >
                                        <asp:LinkButton runat="server" ID="btnDate" CommandName="Sort" CommandArgument="[IV].[startdatetime]"
                                            Text="Interview Date" ToolTip="Sort By Date" />
                                    </th>
                                   
                                    <th runat="server" id="thFeedback" style="min-width: 80px" enableviewstate ="false" >
                                    
                                            <asp:Label ID="btnFeedback" runat="server" Text="Feedback" />   <%--change by pravin khot on 25/Jan/2016--%>
                                       <%-- <asp:LinkButton runat="server" ID="btnFeedback" CommandName="Sort" CommandArgument="[Ifeed].[InterviewFeedback]"
                                            Text="Feedback" ToolTip="Sort By Feedback" />--%>
                                    </th>
                                     
                                    <%-- <th style="text-align: center;" id="thAction" runat="server" width="30px">
                                        Action
                                       </th>  --%>                      
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                                <tr class="Pager">
                                    <td id="tdpager" runat="server">
                                        <ucl:Pager ID="pagerControl" runat="server" EnableViewState="true" />
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <EmptyDataTemplate>
                            <table id="tblEmptyData" class="EmptyDataTable alert alert-warning" runat="server">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                               
                                 <td style="text-align: center;" runat ="server" id="tdAction">
<%--                                     <asp:ImageButton ID="btnExportToPDF1" CssClass ="btn"  SkinID ="sknExportToPDF" runat ="server" ToolTip ="Interviewer Feedback/Assessment Form Export To PDF" OnClick ="btnExportToPDF_Click" />
--%>                                    <%-- <asp:RadioButton ID="chk"  runat ="server" Checked ="false"  GroupName="pp"/>
                                         <asp:RadioButton ID="RadioButton1"  runat ="server" Checked ="false"  GroupName="pp"/>--%>
<%--                                   <asp:ImageButton ID="btnFeedback" SkinID="sknEditButton" runat="server" ToolTip ="Show Interviewer Feedback/Assessment Form"  CommandName="Feedbackpdf">
                                  </asp:ImageButton>--%> 
                                   <asp:Label ID="lblMemberEmailId" runat="server" style=" width :100px;"/>
                                     <asp:Label ID="lblSelected" runat="server" />
                          <%--  <asp:HiddenField ID="hdfMemberEmailId" runat="server" />
                               <asp:CheckBox ID="chkItemCandidate" runat="server"  />--%>
                                <asp:HiddenField ID="hdnID" runat="server" />
                                </td>     
                               
                                <%-- <td runat="server" id="tdCandidateID">
                                    <asp:Label ID="lblCandidateID" runat="server"  ></asp:Label>
                                </td>
                                <td runat="server" id="tdCandidateName">
                                    <asp:Label ID="lblCandidateName" runat="server"  ></asp:Label>
                                </td>--%>
                                
                                <td runat="server" id="tdJobTitle">
                                    <asp:Label ID="lblJobTitle" runat="server" ></asp:Label>
                                </td>
                                 <td runat="server" id="tdReqCode">
                                    <asp:Label runat="server" ID="lblReqCode"  />
                                </td>
                               
                                 <td runat="server" id="tdInterviewerName">
                                    <asp:Label ID="lblInterviewerName" runat="server"  ></asp:Label>
                                </td>
                                 <td runat="server" id="tdInterviewerDate">
                                    <asp:Label ID="lblInterviewerDate" runat="server" ></asp:Label>
                                </td>                            
                               
                                <td runat="server" id="tdFeedback">
                                    <asp:Label runat="server" ID="lblFeedback" />
                                </td>   

                                                     
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlmodal" runat="server" Style="display: none">
                <asp:UpdatePanel runat="server" ID="UpdatePanel2" RenderMode="Inline" UpdateMode="Conditional">
                    <ContentTemplate>
                        <img src="../Images/AjaxLoading.gif" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="Modal" runat="server" TargetControlID="pnlmodal"
                PopupControlID="pnlmodal" BackgroundCssClass="divModalBackground">
            </ajaxToolkit:ModalPopupExtender>
        </ContentTemplate>
              <Triggers>
             <asp:PostBackTrigger ControlID="btnExportToPDF" />

       </Triggers>
    </asp:UpdatePanel>
</asp:Content>
