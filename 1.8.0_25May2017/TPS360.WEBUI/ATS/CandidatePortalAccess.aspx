﻿<%-- -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InternalResumeBuilder.aspx
    Description: This is the page which is used to present tab controls
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date           Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Oct-16-2008        Gopala Swamy         Defect id: 8994; Renamed in tabcontrol from "Job tittle & Category" to "Job tittles"
    0.2             Mar-04-2009        Shivanand            Defect #10053; Removed tab "Job Titles".
    0.3             May-26-2009         Sandeesh            Defect id : 10464 Changes made to load the tabs instantaneously
    0.4             July-01-2009       Shivanand            Defect #10464; AsyncMode is enabled for Tabs.
    0.5             Jul-24-2009           Veda              Defect id: 11072 ; Contineous Spinning occurs in the browser tab.
    0.6             Jul-30-2009           Veda              Defect id: 11132; 
    0.7             Aug-31-2009         Ranjit Kumar.I      Defect id:#11421;Desabled Trace in Page Level Script
    0.8             Apr-07-2010        Ganapati Bhat        Enhancement #12139; Removed tab "Skill Set"
-------------------------------------------------------------------------------------------------------------------------------------------       

--%>


<%@ Page Language="C#" MasterPageFile="~/Masters/CandidateProfile.master" AutoEventWireup="true" CodeFile="CandidatePortalAccess.aspx.cs" Inherits="TPS360.Web.UI.CandidatePortalAccess"  EnableEventValidation="false" ValidateRequest="false" Title="Hiring Log" %>
<asp:Content ID="cntCandidateResumeBuilder" ContentPlaceHolderID="cphCandidateMaster"
    runat="Server">
    <asp:UpdatePanel ID="Portal" runat ="server" >
    <ContentTemplate >
    
<div class="TabPanelHeader">Candidate Portal Access</div>
<div class="TableRow" style="text-align: left;padding:30px">
   <asp:HiddenField ID="hdnPageTitle" runat ="server" />

                        <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
   <div style="text-align:center"><asp:Label ID ="lblNote" runat ="server"><b>Note:</b> Candidates with a primary email address present can be granted access to the Candidate Portal.
</asp:Label></div> 

<div class ="TableRow" style =" padding-top : 30px; padding-bottom:15px;">
    <div class="TableFormLeble">
            <asp:Label EnableViewState="false" ID="lblPortalAccess" runat="server" Text="Portal Access"></asp:Label>:
     </div>
    <div class ="TableFormContent">
                          <asp:DropDownList ID ="ddlAccess" runat ="server" CssClass ="CommonDropDownList" AutoPostBack ="true" OnSelectedIndexChanged ="ddlAccess_SelectedIndexChanged">
                            <asp:ListItem>Blocked</asp:ListItem>
                            <asp:ListItem>Enabled</asp:ListItem>
                        </asp:DropDownList>
    
    </div>
</div>
<div class ="TableRow" align="center" > 
  <asp:Button ID="btnSend" runat ="server" Text ="Reset Password & Send Access Email" CssClass ="CommonButton" OnClick="btnSend_Click" />
</div>
                        
                        </div> 
                        </ContentTemplate>
    </asp:UpdatePanel>
  </asp:Content>