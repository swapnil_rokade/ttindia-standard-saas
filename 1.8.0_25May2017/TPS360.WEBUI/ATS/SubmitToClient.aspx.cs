﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: SubmitToClient.aspx.cs
    Description: This is the page used to submit the candidate to the client.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Sep-30-2008          Jagadish            Defect id: 8790; Removed the fields/labels: Client Rate, Requisition Rate, 
                                                              Applicant Desired Rate, Submission Rate.
    0.2              Oct-21-2008          Jagadish N          Defect id: 8966; Loaded from dropdown with candidate's primary and alternate email ids.
    0.3              Mar-10-2009          Yogeesh Bhat        Defect Id: 10058; Changes made in SetControlValues()
    0.4              Mar-18-2009          Yogeesh Bhat        Defect Id: 10109: Changes made in btnPreview_Click(), btnSaveAsPdf_Click(), btnSend_Click()
    0.5              Apr-01-2009          Yogeesh Bhat        Defect Id: 10085; Added new methods MoveListItems() and GetCandidateProfile()
    0.6              Apr-02-2009          Yogeesh Bhat        Defect Id: 10217; Changes made in SetControlValues()
    0.7              Apr-07-2009          Yogeesh Bhat        Defect Id: 10243; Added new method CoverLetter()
    0.8              Apr-10-2009          Jagadish            Defect Id: 10278; Changes made id method 'grdViewUploadedResume_RowDataBound()'.
    0.9              Apr-13-2009          Sandeesh            Defect id: 10316;  Changes made in GetCandidateProfile()
    1.0              Apr-21-2009          Gopala Swamy J      Defect Id:10367; Set the properties 
    1.1              May-11-2009          Yogeesh Bhat        Defect Id: 10418: Changes made in btnSend_Click()
    1.2              Aug-20-2009          Shivanand           Defect Id: 10320; Changes made in method CoverLetter().
    1.3              Aug-20-2009          Shivanand           Defect Id: 10320; Changes made in method GetCandidateProfile()
    1.4              Sept-24-2009         Nagarathna V.B      Enha#11473;passing current user for send email method call  
    1.5               Nov-17-2009         Sandeesh            Enhancement#11812:Multiple Submission Contacts - When sending submission mails the user should be able to choose any client contact to send to, not just the primary contact.  
    1.6              Dec-16-2009         Rajendra A.S         Enhance #11983 ; Added code to insert Submission details.
    1.7              Dec-24-2009         Rajendra A.S         Defect Id: 12041; Code has been changed in btnSend_Click().
 *  1.8              Jan-19-2010         Srividya.S           commented lines in btnSend_Click()
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using System.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using TPS360.Web.UI.Helper;
using System.Collections;
using System.Collections.Specialized;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;


namespace TPS360.Web.UI.ATS
{
    public partial class SubmitToClient : ATSBasePage
    {
        static CheckBoxList chkBoxListShowHideInfo = new CheckBoxList();
        private static int _memberEmailId = 0;
        private bool _IsAccessToEmployee = true;
        private static string UrlForEmployee = string.Empty;
        private static int IdForSitemap = 0;
        private bool Shown = false;
        private string _sessionKey;
        private static int _memberId = 0;
        public int HiringMatrixInterviewLevel
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_HIRING_MATRIX_INTERVIEW_LAVEL ]))
                {
                    return Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_HIRING_MATRIX_INTERVIEW_LAVEL]);
                }
                return 0;
            }
        }
        public string PreviewDir
        {
            get;
            set;
        }

        public string TempDirectory
        {
            get
            {
                return (string)Helper.Session.Get("tempDir");
            }
            set
            {
                Helper.Session.Set("tempDir", value);
            }
        }
        public string SelectedApplicants
        {
            get
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_APPLICANT_ID]))
                {
                    return Helper.Url.SecureUrl[UrlConstants.PARAM_APPLICANT_ID];
                }
                return string.Empty;
            }
        }
        public string SessionKey
        {
            get
            {
                return _sessionKey;
            }
            set
            {
                _sessionKey = value;
            }
        }
        public StringDictionary Files
        {
            get
            {
                if (Helper.Session.Get(SessionKey) == null)
                {
                    Helper.Session.Set(SessionKey, new StringDictionary());
                }

                return (StringDictionary)Helper.Session.Get(SessionKey);
            }
        }
        public StringDictionary UploadedFiles
        {
            get
            {
                if (Helper.Session.Get("EmailFileUploads") == null)
                {
                    Helper.Session.Set("EmailFileUploads", new StringDictionary());
                }

                return (StringDictionary)Helper.Session.Get("EmailFileUploads");
            }
            set
            {
                Helper.Session.Set("EmailFileUploads", value);
            }
        }

        
        #region Methods

        //16.When a recruiter submits a consultant/candidate to the client for a requisition, should the candidate/consultant and the assigned recruiters for the requisition be alerted?
        private void ApplyWorkFlowSetting()
        {
            ApplicationWorkflow applicationWorkflow = Facade.GetApplicationWorkflowByTitle(WorkFlowTitle.RequisitionManagementWhenRecruiterSubmitsConsultantCandidateClientForRequisitionAlerted.ToString());
            if (applicationWorkflow != null && applicationWorkflow.RequireApproval)
            {
                if (applicationWorkflow.IsEmailAlert)
                {
                    List<string> listItem = new List<string>();
                    foreach (ListViewDataItem item in lsvViewApplicantList.Items )
                    {
                        Label lblEmail = (Label)item.FindControl("lblEmail");
                        listItem.Add(lblEmail.Text);
                    }
                    //foreach (ListItem ccItem in chklJobpostingInternalHiringTeam.Items)
                    //{
                    //    listItem.Add(ccItem.Value);
                    //    WorkFlowMailHelper workFlowMailHelper = new WorkFlowMailHelper(WorkFlowTitle.RequisitionManagementWhenRecruiterSubmitsConsultantCandidateClientForRequisitionAlerted);
                    //    workFlowMailHelper.SendAlertByEmail(listItem);
                    //}
                }
            }
        }

        private void BindSelectedApplicant()
        {
            IList<Member> memberList = Facade.GetAllMemberByIds(SelectedApplicants);
            if (memberList != null)
            {
                lsvViewApplicantList.DataSource = memberList;
                lsvViewApplicantList.DataBind();
            }
                //CreateTrackr(memberList);
        }

        private void LoadShowHideList()
        {
            chkBoxListShowHideInfo.Items.Add(new ListItem("Applicant ID", "ApplicantID"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("First Name", "FirstName"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Middle Name", "MiddleName"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Last Name", "LastName"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Nick Name", "NickName"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Address", "Address"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Current City", "CurrentCiry"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("ZipCode", "ZipCode"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Country", "Country"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("State", "State"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Primary Email", "PrimaryEmail"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Alternate Email", "AltEmail"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Home Phone", "HomePhone"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Cell Phone", "CellPhone"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Office Phone", "OfficePhone"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Current Position", "CurrentPosition"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Current Company", "CurrentCompany"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Total Experience", "TotalExperience"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Willing To Travel", "WillingToTravel"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Date Of Birth", "DOB"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("City Of Birth", "CityOfBirth"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Country Of Birth", "CountryOfBirth"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Country Of Residence", "CountryOfResidence"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Gender", "Gender"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Marital Status", "MaritalStatus"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("SSN", "SSN"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Work Permit Status", "WorkPermitStatus"));            
            chkBoxListShowHideInfo.Items.Add(new ListItem("Availability", "AvailabilityStatus"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Skill Set", "SkillSet"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("References", "References"));
            chkBoxListShowHideInfo.Items.Add(new ListItem("Summary", "Summary"));
        }

        private void UploadFile()
        {
            if (!FileUpload1.HasFile)
            {
                MiscUtil.ShowMessage(lblMessage, "Please select a file to attach.", true);
                return;
            }
            if (true)  
            {
                PreviewDir = GetTempFolder();
                string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                string filePath = Path.Combine(PreviewDir, fileName);
                FileUpload1.PostedFile.SaveAs(filePath);
                lstAttachments.Items.Add(new ListItem(FileUpload1.FileName,filePath ));
                string fileUrl = "../Temp/" + CurrentUserId.ToString() + "/" + System.Uri.EscapeDataString(Path .GetFileName ( filePath));
                Helper.Session.Set(SessionConstants.UserId, CurrentUserId.ToString());
            }
        }

        private string GetTempFolder()
        {
            if (StringHelper.IsBlank(TempDirectory))
            {
                string dir = CurrentUserId.ToString();
                string path = Path.Combine(UrlConstants.TempDirectory, dir);

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                TempDirectory = path;
            }
            if (hdnTmpFolder.Value == string.Empty)
            {
                DateTime dt = DateTime.Now;
                string s = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString() + "";
                if (!Directory.Exists(TempDirectory + "\\" + s))
                {
                    Directory.CreateDirectory(TempDirectory + "\\" + s);
                    hdnTmpFolder.Value = s;
                    PreviewDir = TempDirectory + "\\" + s;
                }
            }
            else
            {
                PreviewDir = TempDirectory + "\\" + hdnTmpFolder.Value;
            }
            return PreviewDir;
        }



        private void PopulateToAndCCList()
        {
            if (base.CurrentJobPosting.ClientId > 0)
            {
                Company CurrentCompany = Facade.GetCompanyById(base.CurrentJobPosting.ClientId);

                IList<CompanyContact> companyContactList = Facade.GetAllCompanyContactByComapnyId(CurrentCompany.Id);

                if (companyContactList != null)
                {
                    for (int i = 0; i < companyContactList.Count; i++)
                    {
                        if (companyContactList[i].Email.Trim() != string.Empty)
                        {
                            ListItem lst = new ListItem(companyContactList[i].FirstName + " " + companyContactList[i].LastName + " [" + companyContactList[i].Email + "]", companyContactList[i].Email);
                            lstTo.Items.Add(lst);
                            lstCC.Items.Add(lst);
                            lstBCC.Items.Add(lst);
                        }
                    }                   
                }
            }
            
            IList<JobPostingHiringTeam> jobPostingHiringTeamList = Facade.GetAllJobPostingHiringTeamByJobPostingId(base.CurrentJobPostingId);
            if (jobPostingHiringTeamList != null && jobPostingHiringTeamList.Count > 0)
            {
                foreach (JobPostingHiringTeam jobPostingHiringTeam in jobPostingHiringTeamList)
                {
                    if (jobPostingHiringTeam != null)
                    {
                        if (jobPostingHiringTeam.MemberId > 0)
                        {
                            if (jobPostingHiringTeam.EmployeeType == EmployeeType.Internal.ToString())
                            {
                                Member member = Facade.GetMemberById(jobPostingHiringTeam.MemberId);
                                if (member != null)
                                {
                                    ListItem item = new ListItem(member.FirstName + " " + member.LastName + " [" + member.PrimaryEmail + "]", member.PrimaryEmail);
                                    lstCCAssigned.Items.Add(item);
                                    ListItem item1 = new ListItem(member.FirstName + " " + member.LastName + " [" + member.PrimaryEmail + "]", member.PrimaryEmail);
                                    item1.Selected = true;
                                    lstBCCAssigned.Items.Add(item1);
                                   
                                }
                            }
                            CompanyContact companyContact = Facade.GetCompanyContactByMemberId(jobPostingHiringTeam.MemberId);
                        }
                    }
                }                                
            }
        }

        private void PopulateApplicantSummary()
        {
           // string applicant = SelectedApplicants; 
           // MemberObjectiveAndSummary memberObjectiveAndSummary = Facade.GetMemberObjectiveAndSummaryByMemberId(Int32.Parse(applicant.Substring(0, (applicant.IndexOf(',') > 0 ? applicant.IndexOf(',') : applicant.Length))));
           // WHEApplicantSummary.Text = (memberObjectiveAndSummary != null ? memberObjectiveAndSummary.Summary : string.Empty);
        }

        private void SetControlValues(bool isControlClear)
        {
            string applicant = SelectedApplicants; 
            if (isControlClear)
            {
                lblFromAdd.Text =CurrentMember.FirstName + " " + CurrentMember.LastName + "[" + base.CurrentMember.PrimaryEmail + "]"; 
                
                txtSenderName.Text = base.CurrentMember.FirstName + " " + base.CurrentMember.LastName;
                lblClientName.Text = MiscUtil.GetClientNameByJobId(base.CurrentJobPostingId, Facade);
                //ControlHelper.SetHyperLink(lnkReqTitle  , UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE, string.Empty, CurrentJobPosting .JobTitle  , UrlConstants.PARAM_JOB_ID, StringHelper.Convert(CurrentJobPostingId ));  //0.1
                lnkReqTitle.Text = CurrentJobPosting.JobTitle;
                CustomSiteMap CustomMap = new CustomSiteMap();

                int targetsitemap = 0;
                int SiteMapParentID = 0;

                if (DefaultCompanyStatus == CompanyStatus.Client)
                {
                    targetsitemap = 235;
                    SiteMapParentID = 11;
                }
                else if (DefaultCompanyStatus == CompanyStatus.Department)
                {
                    targetsitemap = 626;
                    SiteMapParentID = 622;
                }
                else
                {
                    targetsitemap = 642;
                    SiteMapParentID = 638;
                }
                CustomMap = Facade.GetAllCustomSiteMapByParentIdAndMemberPrivilege(targetsitemap , CurrentMember.Id);
                if (CustomMap == null) _IsAccessToEmployee = false;
                else
                {
                    IdForSitemap = CustomMap.Id;
                    UrlForEmployee = "~/" + CustomMap.Url.ToString();
                }
                if (_IsAccessToEmployee)
                {
                    if (IdForSitemap > 0 && UrlForEmployee != string.Empty)
                        ControlHelper.SetHyperLink(lnkClientTitle , UrlForEmployee, string.Empty, lblClientName .Text , UrlConstants.PARAM_COMPANY_ID, StringHelper.Convert(CurrentJobPosting .ClientId ), UrlConstants.PARAM_SITEMAP_ID, IdForSitemap.ToString(),UrlConstants .PARAM_SITEMAP_PARENT_ID ,SiteMapParentID .ToString ());
                }
                else
                {
                    lnkClientTitle.Text = lblClientName.Text;
                }
                this.Page.Title = lnkReqTitle.Text + lblClientTitle.Text + lnkClientTitle.Text;
               // lblClientTitle.Text = "Submit to " + lblClientName.Text.Trim();
                //PopulateToAndCCList();//commented by pravin khot on 3/March/2017

                if (base.CurrentJobPosting.EmailSubject != "")
                {
                    txtSubject.Text = "Re: " +MiscUtil .RemoveScript ( base.CurrentJobPosting.EmailSubject,string .Empty );    
                }
                else                                                                    //0.6
                {
                    string strLocation = "";
                    string strJobDuration="";

                    if (base.CurrentJobPosting.City != "")
                    {
                        strLocation = ", " + base.CurrentJobPosting.City;
                    }

                    if (base.CurrentJobPosting.StateId > 0)
                    {
                        State stateName = Facade.GetStateById(base.CurrentJobPosting.StateId);

                        if (stateName != null)
                        {
                            if (strLocation != "")
                            {
                                strLocation = strLocation + ", " + stateName.Name;
                            }
                            else
                            {
                                strLocation = ", " + stateName.Name; ;
                            }
                        }
                    }

                    ////GenericLookup JobDuarationLookUP = Facade.GetGenericLookupById(Convert.ToInt32(base.CurrentJobPosting.JobDurationMonth));
                    ////if (JobDuarationLookUP != null)
                    ////{
                    ////    strJobDuration = ", " + JobDuarationLookUP.Name;
                    ////}

                    if (base.CurrentJobPosting.ClientJobId != "")
                    {
                        txtSubject.Text = "Candidate Hiring Status Update | " + MiscUtil.RemoveScript(base.CurrentJobPosting.JobTitle, string.Empty) + MiscUtil.RemoveScript(strLocation, string.Empty) + strJobDuration + " - " + base.CurrentJobPosting.ClientJobId;
                    }
                    else
                    {
                        txtSubject.Text = "Candidate Hiring Status Update | " + MiscUtil.RemoveScript(base.CurrentJobPosting.JobTitle, string.Empty) + MiscUtil.RemoveScript(strLocation, string.Empty) + strJobDuration + " - " + base.CurrentJobPosting.JobPostingCode;
                    }
                }

                PopulateApplicantSummary();
                MemberExtendedInformation memberExtendedInformation = Facade.GetMemberExtendedInformationByMemberId(Int32.Parse(applicant.Substring(0, (applicant.IndexOf(',') > 0 ? applicant.IndexOf(',') : applicant.Length))));
            }
            else if (!isControlClear)
            {
                txtSubject.Text = string.Empty;
                WHECoverLetter.Text = string.Empty;
            }
        }
        private void LoadRequisionList()
        {
            IList<HiringMatrixLevels> level = Facade.GetAllHiringMatrixLevels();
            ddlNextLevel.DataSource = level;
            ddlNextLevel.DataTextField = "Name";
            ddlNextLevel.DataValueField = "Id";
            ddlNextLevel.DataBind();
            ddlNextLevel = (DropDownList)MiscUtil.RemoveScriptForDropDown(ddlNextLevel);
            HiringMatrixLevels lev=new HiringMatrixLevels ();
            if (HiringMatrixInterviewLevel == 0 || HiringMatrixInterviewLevel ==-1)
                lev = Facade.HiringMatrixLevel_GetInitialLevel();
            else 
                lev = Facade.HiringMatrixLevel_GetNextLevelById(HiringMatrixInterviewLevel);
            if(lev!=null)  ddlNextLevel.SelectedValue = lev.Id.ToString();


        }

        private string AccountType
        {
            get
            {
                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                    return "Account";
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                    return "BU";
                else return "Vendor";
            }
        }

        private CompanyStatus DefaultCompanyStatus
        {
            get
            {
                if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Client)
                    return CompanyStatus.Client;
                else if (SiteSetting[DefaultSiteSetting.AssociateRequisitionsWithEntitiesFromModule.ToString()].ToString() == UIConstants.AccountType_Department)
                    return CompanyStatus.Department;
                else return CompanyStatus.Vendor;
            }
        }
        private void PrepareView()
        {
            BindSelectedApplicant();
            SetControlValues(true);
            LoadShowHideList();
            LoadRequisionList();
            MiscUtil.PopulateRequistionStatus(ddlReqStatus, Facade);
            //ddlReqStatus.SelectedValue = "553";
            if (Convert.ToString(CurrentJobPosting.JobStatus) != "")
            {
                ddlReqStatus.SelectedValue = Convert.ToString(CurrentJobPosting.JobStatus);
            }
            ccClientContacts.Text = bccClientContacts.Text = toClientcontact.Text = AccountType +  " Contacts";
            lblClientNameHeader.Text = AccountType +  " Name";
            GenerateCoverLetter();
        }
        #endregion

        #region Events
       
        public void RenderCurrentJobPosting()
        {
            JobPosting currentJobPosting = base.CurrentJobPosting;
            //lblTitle.Visible = false;                                                                           //0.1
            ControlHelper.SetHyperLink(lnkTitle, UrlConstants.Requisition.JOB_POSTING_INTERNAL_PREVIEW_PAGE, string.Empty, currentJobPosting.JobTitle + "&nbsp;&nbsp;" + currentJobPosting.JobPostingCode, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(currentJobPosting.Id));  //0.1
        }
        //protected void uwtRequisitionHiringMatrixNavigationTopMenu_TabClick(object sender, Infragistics.WebUI.UltraWebTab.WebTabEvent e)
        //{
        //        Helper.Url.Redirect(UrlConstants.ATS.ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(base.CurrentJobPostingId), UrlConstants.PARAM_TAB, e.Tab.Key == "StatusReport" ? "0" : e.Tab.Key);
        //}

        //public void SetTabText()
        //{
        //    int[] hiringMatrixCount = Facade.GetHiringMatrixMemberCount(base.CurrentJobPostingId);
        //    IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
        //    for (int i = 0; i < hiringMatrixLevels.Count; i++)
        //    {
        //        try
        //        {
        //            uwtRequisitionHiringMatrixNavigationTopMenu.Tabs[i].Text = hiringMatrixLevels[i].Name + " (" + hiringMatrixCount[i].ToString() + ")";
        //        }
        //        catch
        //        {
        //        }
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            uclConfirm.MsgBoxAnswered += MessageAnswered;
            ucAttachmentModal.ResumeAttachment += ResumeAttachInList;
            UploadedFiles.Clear();
            _sessionKey = "InterviewEmailFileUpload";
            if (!IsPostBack)
            {
                if(Request .UrlReferrer !=null )
                ViewState["PreviousPage"] = Request.UrlReferrer.AbsoluteUri ;
                hdnSelectedIDS.Value = SelectedApplicants;
                WHECoverLetter.FindControl("txtEditor");
                lnkReqTitle.Attributes.Add("onclick", "EditModal('" + UrlConstants.ApplicationBaseUrl + "Modals/JobDetail.aspx?JID=" + CurrentJobPostingId + "&FromPage=JobDetail" + "','700px','570px'); return false;");
                //lstTo.Attributes.Add("onclick", "javascript:SelectChanged('" + lstTo.ClientID + "')");
                int i = 0;
                int selectedtab = 0;
                IList<HiringMatrixLevels> hiringMatrixLevels = Facade.GetAllHiringMatrixLevels();
                bool isLevelSelected = false;
                if (hiringMatrixLevels != null)
                {
                    foreach (HiringMatrixLevels levels in hiringMatrixLevels)
                    {
                        i++;
                        if (Helper.Url.SecureUrl[UrlConstants.PARAM_TAB].ToString() == levels.Id.ToString())
                            this.Page.Title = levels.Name + " - " + CurrentJobPosting.JobTitle + " - " + CurrentJobPosting.JobPostingCode;
                    }
                }
                RenderCurrentJobPosting();
                PrepareView();
                DivisionChangeEvent(); // added by pravin khot 25/nov/2016  
            }
            this.Page.Title = lnkReqTitle.Text + lblClientTitle.Text + lnkClientTitle.Text;
            divMasterTemplate.Visible = divMyTemplate.Visible = false;     
            WHECoverLetter.Focus = false;
        }
        private void SaveMemberEmailAttachment(MemberEmail memberEmail)
        {
            string[] file;
            string filePaths = string.Empty;
            string TempPath=GetTempFolder ();
            if (UploadedFiles.Count == 0)
            {
                foreach (ListViewDataItem item in lsvViewApplicantList.Items )
                {
                    Label _editControl = (Label)item.FindControl("lblJobTitle");
                    HiddenField hdnFileTypeId = (HiddenField)item.FindControl("hdnFileTypeId"); 
                    IList<string> FileTypeName = hdnFileTypeId.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    HiddenField MId = (HiddenField)item.FindControl("hndIds");
                    CheckBox chkMember = (CheckBox)item.FindControl("chkItemCandidates");
                    IList<string> resTitle = _editControl.Text.Split(new string[] { "</br>" }, StringSplitOptions.RemoveEmptyEntries);
                    int i = 0;
                    foreach (string res in resTitle)
                    {
                        if (chkMember.Checked)
                        {
                            string filePath = MiscUtil.GetMemberDocumentPath(this.Page, Int32.Parse(MId.Value), MiscUtil.RemoveScript(res), FileTypeName[i], true);
                            if (Directory.Exists(TempPath))
                            {
                                if (!Directory.Exists(TempPath + "\\" + MId.Value  + FileTypeName[i]))
                                {
                                    Directory.CreateDirectory(TempPath + "\\" + MId.Value  + FileTypeName[i]);
                                    filePaths = Path.Combine(TempPath + "\\" + MId.Value  + FileTypeName[i], MiscUtil.RemoveScript(res));
                                }
                                else
                                    filePaths = Path.Combine(TempPath + "\\" + MId.Value  + FileTypeName[i], MiscUtil.RemoveScript(res));
                            }
                            //filePaths = Path.Combine(TempPath, MiscUtil.RemoveScript(res));
                            filePath = filePath.Replace("%20", " ");
                            if (File.Exists(Server .MapPath ( filePath)))
                            {
                                try
                                {
                                    File.Copy(Server.MapPath(filePath), filePaths);
                                }
                                catch
                                {

                                }
                                    if (!UploadedFiles.ContainsKey(MiscUtil.RemoveScript(res + MId.Value + FileTypeName[i])))
                                        UploadedFiles.Add(MiscUtil.RemoveScript(res +"$$$"+ MId.Value + FileTypeName[i]), filePaths + "?" + MiscUtil.RemoveScript(res).Length.ToString());
                                    
                                
                            }
                        }
                        i++;
                    }

                }
            }
                UploadedFiles.Clear();
                foreach (ListItem chkItem in lstAttachments.Items)
                {
                    if (chkItem.Selected == true)
                    {
                        filePaths = Path.Combine(TempPath, chkItem .Text );
                        UploadedFiles.Add(chkItem.Text+"$$$uploadedfile", filePaths + "?"+chkItem .Text .Length .ToString ());
                    }
                }
                if (UploadedFiles.Count > 0)
                {
                memberEmail.AttachedFileNames = string.Empty;
                
                foreach (DictionaryEntry entry in UploadedFiles)
                {
                    MemberEmailAttachment memberEmailAttachment = new MemberEmailAttachment();
                    file = Path.GetFileName((string)entry.Value).Split((char)Convert.ToChar("?"));
                    GetPathToSave(memberEmail, ((string)entry.Value),entry .Key.ToString ().Replace (file [0],"").Trim ());
                    memberEmailAttachment.AttachmentFile = entry .Key .ToString () ;
                    memberEmail.NoOfAttachedFiles = UploadedFiles.Count;
                    memberEmail.AttachedFileNames += entry .Key .ToString () +";";
                    memberEmailAttachment.AttachmentSize = Convert.ToInt32(file[1]);
                    memberEmailAttachment.MemberEmailId = memberEmail.Id;
                    memberEmailAttachment.CreatorId = base.CurrentMember.Id;
                    _memberEmailId = memberEmail.Id;
                    Facade.AddMemberEmailAttachment(memberEmailAttachment);
                }

                UploadedFiles.Clear();

                if (Directory.Exists(TempDirectory))
                {
                    try
                    {
                        Directory.Delete(TempDirectory);
                    }
                    catch
                    {
                    }
                }

                UploadedFiles = null;
                TempDirectory = null;
            }
            else
            {
                memberEmail.NoOfAttachedFiles = 0;
                memberEmail.AttachedFileNames = string.Empty;
            }

        }
        private void GetPathToSave(MemberEmail memberEmail, string filepathandname,string foldername)
        {
            try
            {
                string pth = Page.Server.MapPath("..\\Resources\\Member");
                pth += "\\" + memberEmail.ReceiverId;
                DirectoryCreation(pth);
                pth += "\\Outlook Sync";
                DirectoryCreation(pth);
                pth += "\\" + memberEmail.Id;
                DirectoryCreation(pth);
                pth += "\\" + foldername;
                DirectoryCreation(pth);
                string[] filename = Path.GetFileName(filepathandname).Split((char)Convert.ToChar("?"));
                pth += "\\" + filename[0];

                if (File.Exists(pth))
                {
                    File.Delete(pth);
                }
                string[] file = filepathandname.Split((char)Convert.ToChar("?"));
                File.Copy(file[0].ToString(), pth);
            }
            catch { }
        }
        void DirectoryCreation(string pth)
        {
            if (!Directory.Exists(pth))
            {
                Directory.CreateDirectory(pth);
            }
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            //if (ViewState["PreviousPage"] != null)	
            {
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants .ATS .ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX  , string.Empty, UrlConstants.PARAM_MSG, "BackFromSubmit", UrlConstants.PARAM_SELECTEDTAB, HiringMatrixInterviewLevel.ToString(),UrlConstants .PARAM_JOB_ID ,CurrentJobPostingId .ToString (), UrlConstants.PARAM_APPLICANT_ID , hdnSelectedIDS .Value  );
                Response.Redirect(url.ToString());
            }
        }
        protected void btnSend_Click(object sender, EventArgs e)
        {
            bool IsError = true;
            if (lstAttachments.Items.Count > 0)
            {
                foreach (ListItem chkItem in lstAttachments.Items)
                {
                    if (chkItem.Selected == true)
                    {
                        IsError = false;
                        break;
                    }
                    else
                    {
                        IsError = true;
                    }
                }
            }

            if (txtSubject.Text.Trim() == string.Empty && WHECoverLetter.TextPlain.Trim() == string.Empty && IsError)//lstAttachments.Items.Count == 0
            {
                MiscUtil.ShowMessage(lblMessage, "Mail should have subject or body or attachment", false);
                return;
            }

            MemberExtendedInformation MailSetting = Facade.GetMemberExtendedInformationByMemberId(CurrentMember.Id);
            TPS360.BusinessFacade.Facade f1 = new TPS360.BusinessFacade.Facade();
            SiteSetting siteSetting = f1.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
            string MailQueueCC = string.Empty;
            if (MailSetting.MailSetting != null)
            {
                        string sentStatus = string.Empty;
                        EmailHelper emailHelper = new EmailHelper();
                        emailHelper.FromEmail = (lblFromAdd.Text.Replace("[", "<")).Replace("]", ">");        //0.4
                        emailHelper.AdditionalMailMessage = HttpUtility.HtmlDecode(WHECoverLetter.Text);
                        emailHelper.ShowJobDescription = false;
                        emailHelper.HiringMatrixApplicant = SelectedApplicants; //Helper.Session.Get(SessionConstants.SELECTED_APPLICANT).ToString();
                        emailHelper.ShowScreeningQuestion = false;

                        Company CurrentCompany = Facade.GetCompanyById(base.CurrentJobPosting.ClientId);
                        State ContactState = new State();
                        Country ContactCountry = new Country();
                        string strState = string.Empty;
                        string strCountry = string.Empty;

                        if (CurrentCompany.PrimaryContact.StateId > 0)
                        {
                            ContactState = Facade.GetStateById(CurrentCompany.PrimaryContact.StateId);
                        }
                        strState = (ContactState == null) ? "" : ContactState.Name;

                        if (CurrentCompany.PrimaryContact.CountryId > 0)
                        {
                            ContactCountry = Facade.GetCountryById(CurrentCompany.PrimaryContact.CountryId);
                        }
                        strCountry = (ContactCountry == null) ? "" : ContactCountry.Name;

                        StringBuilder strbldrAddress = new StringBuilder();
                        if (CurrentCompany.PrimaryContact.Address1.IsNotNullOrEmpty())
                            strbldrAddress.Append(CurrentCompany.PrimaryContact.Address1);
                        if (CurrentCompany.PrimaryContact.Address2.IsNotNullOrEmpty())
                            strbldrAddress.Append("<br/>" + CurrentCompany.PrimaryContact.Address2);
                        if (CurrentCompany.PrimaryContact.City.IsNotNullOrEmpty())
                            strbldrAddress.Append("<br/>" + CurrentCompany.PrimaryContact.City);
                        if (strState.IsNotNullOrEmpty())
                            strbldrAddress.Append("<br/>" + strState);
                        if (strCountry.IsNotNullOrEmpty())
                            strbldrAddress.Append("<br/>" + strCountry);
                        if (CurrentCompany.PrimaryContact.ZipCode.IsNotNullOrEmpty())
                            strbldrAddress.Append("<br/>" + CurrentCompany.PrimaryContact.ZipCode);

                        emailHelper.RecipientAddress = strbldrAddress.ToString(); 

                        EmailHelper emailManager = new EmailHelper();
                        emailManager.From = (lblFromAdd.Text.Replace("[", "<")).Replace("]", ">");   
                        emailManager.To.Clear();
                        emailManager.Cc.Clear();
                        //********Code commeneted and modify by pravin khot on 28/Nov/2016***************

                        //****************OLD********************
                        //string[] arrCc = txtCCAdd.Text.Split(','); //txtCc.Text.Split(',');
                        //for (int j = 0; j < arrCc.Length; j++)
                        //    if (arrCc[j] != "") emailManager.Cc.Add(arrCc[j]);
                        //foreach (ListItem chkItem in lstCC.Items)
                        //    if (chkItem.Selected) emailManager.Cc.Add(chkItem.Value);
                        //foreach (ListItem chkItem in lstCCAssigned .Items )
                        //    if (chkItem.Selected) emailManager.Cc.Add(chkItem.Value);

                        //**********************NEW**************************
                        //string[] arrCc = txtBCCAdd.Text.Split(','); 
                        //for (int j = 0; j < arrCc.Length; j++)
                        //    if (arrCc[j] != "") emailManager.Cc.Add(arrCc[j]);

                        if (hfOtherInterviewers.Value != "")
                        {
                            char[] delim = { ',' };
                            string[] otherinterviewers = hfOtherInterviewers.Value.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string s in otherinterviewers)
                            {
                                 if (s != "")
                                 {
                                     if (MailQueueCC != "")
                                     {
                                         MailQueueCC = MailQueueCC + ',' + s;
                                     }
                                     else
                                     {
                                         MailQueueCC = s;
                                     }
                                 }                              
                            }
                        }

                        //if (txtBCCAdd.Text != "Other CC Emails")
                        //{
                        //    string[] arrCc = txtBCCAdd.Text.Split(',');
                        //    for (int j = 0; j < arrCc.Length; j++)
                        //        if (arrCc[j] != "")
                        //        {
                        //            if (MailQueueCC != "")
                        //            {
                        //                MailQueueCC = MailQueueCC + ',' + arrCc[j];
                        //            }
                        //            else
                        //            {
                        //                MailQueueCC = arrCc[j];
                        //            }
                        //        }
                        //}

                        string DivisionIdCC = uclDivisionListCC.SelectedItems; //added by pravin khot on 24/Nov/2016
                        if (DivisionIdCC != string.Empty)
                        {
                            string[] DivisionIdsCC = DivisionIdCC.Split(',');
                            foreach (string idCC in DivisionIdsCC)
                            {
                                CompanyContact cc = Facade.GetCompanyContactById(Convert.ToInt32(idCC));                           
                              
                                if (MailQueueCC != "")
                                {
                                    MailQueueCC = MailQueueCC + ',' + cc.Email;
                                }
                                else
                                {
                                    MailQueueCC = cc.Email;
                                }

                            }
                            //emailManager.Cc.Add(emailHelper.FromEmail);    
                        }

                        bool IsToSelected = false;
                        // string DivisionId = ddlDivisionListTO.SelectedValue;
                        //string DivisionId = uclDivisionList.SelectedItems; //added by pravin khot on 24/Nov/2016
                        if (Convert.ToInt32(ddlDivisionListTO.SelectedValue) > 0)
                        {
                            IsToSelected = true;
                        }
                        //***************END by pravin khot on 28/Nov/2016**************************************


                        //string[] arrCc = txtCCAdd.Text.Split(','); //txtCc.Text.Split(',');
                        //for (int j = 0; j < arrCc.Length; j++)
                        //    if (arrCc[j] != "") emailManager.Cc.Add(arrCc[j]);

                        //********Code commeneted by pravin khot on 28/Nov/2016***************
                        //emailManager.Bcc.Clear();
                        //string[] arrBCC = txtBCCAdd.Text.Split(',');//txtBCC.Text.Split(',');

                        //foreach (string s in arrBCC)
                        //    if (s != "") emailManager.Bcc.Add(s);
                        //foreach (ListItem chkItem in lstBCC.Items)
                        //    if (chkItem.Selected) emailManager.Bcc .Add(chkItem.Value);
                        //foreach (ListItem chkItem in lstBCCAssigned.Items)
                        //    if (chkItem.Selected) emailManager.Bcc .Add(chkItem.Value);
                        //bool IsToSelected = false;
                        //for (int sel = 0; sel < lstTo.Items.Count; sel++)
                        //    if (lstTo.Items[sel].Selected) IsToSelected = true;

                        //***************END by pravin khot on 28/Nov/2016**************************************

                        if (!IsToSelected)
                        {
                           MiscUtil.ShowMessage(lblMessage, "Please select at least one contact in the To listbox", true );//1.5
                            return;
                        }

                        emailManager.Subject = txtSubject.Text.Trim();

                        //***********Code modify and added by pravin khot on 28/Nov/2016**********
                        //string[] name = lstTo.SelectedItem.Text.Split('[');
                        //string BUContact = name[0].TrimEnd();

                        string BUContact = "";
                        //string DivisionIdTO = ddlDivisionListTO.SelectedValue;
                        if (Convert.ToInt32(ddlDivisionListTO.SelectedValue) > 0)
                        {
                            CompanyContact  cc = Facade.GetCompanyContactById(Convert.ToInt32(ddlDivisionListTO.SelectedValue));
                            if (cc != null)
                            {
                                BUContact = cc.FirstName ;
                            }

                            //Member member = Facade.GetMemberById(Convert.ToInt32(ddlDivisionListTO.SelectedValue));
                            //if (member != null)
                            //{
                            //    BUContact = member.FirstName + " " + member.MiddleName + " " + member.LastName;
                            //}
                        }

                        //string DivisionIdTO = uclDivisionList.SelectedItems; 
                        //if (DivisionIdTO != string.Empty)
                        //{                       
                        //    string[] DivisionIdsTO = DivisionIdTO.Split(',');
                        //    foreach (string idTO in DivisionIdsTO)
                        //    {
                        //        Member member = Facade.GetMemberById(Convert.ToInt32(idTO));
                        //        BUContact = member.FirstName + " " + member.MiddleName + " " +  member.LastName;
                        //    }
                        //}        
                        //********************* END*********************************************
                        string EmailBody = WHECoverLetter.TextXhtml.Replace("(BU Contact)", BUContact);

                        string mailTo = "";
                        if (Convert.ToInt32(ddlDivisionListTO.SelectedValue) > 0)
                        {
                            CompanyContact  cc = Facade.GetCompanyContactById(Convert.ToInt32(ddlDivisionListTO.SelectedValue));
                            if (cc != null)
                            {
                                string MEMEBERNAME = cc.FirstName;//+ " " + cc.LastName;

                                mailTo = cc.Email;
                                emailHelper.RecipientName = MEMEBERNAME;
                            }
                            //Member member = Facade.GetMemberById(Convert.ToInt32(ddlDivisionListTO.SelectedValue));
                            //if (member != null)
                            //{
                            //    string MEMEBERNAME = member.FirstName + " " + member.MiddleName + " " + member.LastName;

                            //    mailTo = member.PrimaryEmail;
                            //    emailHelper.RecipientName = MEMEBERNAME;
                            //}
                        }
                        //foreach (ListItem chkItem in lstAttachments.Items)
                        //    if (chkItem.Selected == true) emailManager.Attachments.Add(chkItem.Value);

                        Files.Clear();
                        SendAttachment(mailTo, "");

                        emailManager.Body = EmailBody;
                        bool IsSent = false;
                        IsSent = true;
                        //foreach (ListViewDataItem  item in lsvViewApplicantList.Items )
                        //{
                        //    Label _editControl = (Label)item.FindControl("lblJobTitle");
                        //    HiddenField hdnFileTypeId = (HiddenField)item.FindControl("hdnFileTypeId");
                        //    IList<string> FileTypeName = hdnFileTypeId.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        //    HiddenField MId = (HiddenField)item.FindControl("hndIds");
                        //    CheckBox chkMember = (CheckBox)item.FindControl("chkItemCandidates");
                        //    IList <string > resTitle=_editControl .Text .Split (new string [] {"</br>"},StringSplitOptions .RemoveEmptyEntries );
                        //    int i = 0;
                        //    foreach (string res in resTitle)
                        //    {
                        //        if (chkMember.Checked)
                        //        {
                        //            string lcpath = MiscUtil.GetMemberDocumentPath(this.Page, Int32.Parse(MId.Value),MiscUtil .ScriptRemove ( res), FileTypeName[i], true);
                        //            lcpath = System.Uri.UnescapeDataString(lcpath);
                        //            string mapPath = Server.MapPath(lcpath);
                        //            if (File.Exists(mapPath)) emailManager.Attachments.Add(mapPath);
                        //           // IsSent = true;
                        //        }
                        //        i++;
                        //    }
                        //    if (chkMember.Checked)
                        //        IsSent = true;
                            
                        //}
                        //foreach (ListItem chkItem in lstAttachments.Items)
                        //    if (chkItem.Selected == true)  emailManager.Attachments.Add(chkItem.Value);
                        ////string[] arrTo = txtToAdd.Text.Split(',');//txtBCC.Text.Split(',');
                        ////foreach (string s in arrTo)
                        ////    if (s != "") emailManager.To .Add(s);
                        //foreach (ListItem chkItem in lstTo.Items)
                        //{
                        //    if (chkItem.Selected)
                        //    {
                        //        emailManager.To.Add(chkItem.Value);
                        //        emailHelper.RecipientName = chkItem.Text;
                        //        string[] RecipientName = chkItem.Text.Split('[');
                        //    }
                        //}
                        if (IsSent)
                        {
                            if (MailQueueCC != "")
                            {
                                MailQueueCC = MailQueueCC + ',' + base.CurrentMember.PrimaryEmail;
                            }
                            else
                            {
                                MailQueueCC = base.CurrentMember.PrimaryEmail;
                            }

                            if (CurrentJobPostingId > 0)
                            {
                                JobPosting jps = Facade.GetJobPostingById(CurrentJobPostingId);
                                if (jps != null)
                                {
                                    Member m = Facade.GetMemberById(jps.CreatorId);
                                    if (m != null)
                                    {
                                        if (MailQueueCC != "")
                                        {
                                            MailQueueCC = MailQueueCC + ',' + m.PrimaryEmail;
                                        }
                                        else
                                        {
                                            MailQueueCC = m.PrimaryEmail;
                                        }
                                    }
                                    //OrganizationUnit OrganizationUnit = Facade.GetOrgApprovalMatrixByOrgCode(jps.SupervisoryOrganizationCode);
                                    //if (OrganizationUnit != null)
                                    //{
                                    //    if (MailQueueCC != "")
                                    //    {
                                    //        MailQueueCC = MailQueueCC + ',' + OrganizationUnit.Department_Head_Approval_Email_ID;
                                    //    }
                                    //    else
                                    //    {
                                    //        MailQueueCC = OrganizationUnit.Department_Head_Approval_Email_ID;
                                    //    }
                                    //}
                                }
                            }



                            try
                            {
                                //Added by pravin khot on 19/Jan/2016**********
                                string AdminEmailId = string.Empty;
                                int AdminMemberid = 0;
                                //SiteSetting siteSetting = Facade.GetSiteSettingBySettingType((int)SettingType.SiteSetting);
                                if (siteSetting != null)
                                {
                                    Hashtable siteSettingTable = ObjectEncrypter.Decrypt<Hashtable>(siteSetting.SettingConfiguration);
                                    AdminEmailId = siteSettingTable[DefaultSiteSetting.AdminEmail.ToString()].ToString();
                                    AdminMemberid = Facade.GetMemberIdByEmail(AdminEmailId);
                                }
                                //**********************END*************************
                                MailQueueData.AddMailToMailQueue(CurrentMember.Id, mailTo, txtSubject.Text.Trim(), EmailBody, "", MailQueueCC, Files, Facade);
                                sentStatus = "1";
                            }
                            catch
                            {
                                sentStatus = "";
                            }
                            //sentStatus = emailManager.Send(base.CurrentMember.Id, base.CurrentJobPostingId);
                            string MemberEmailDetailid = "";

                            if (sentStatus == "1")
                            {
                                MemberEmailDetailid = SaveMemberEmail();
                                string strNextLevel = string.Empty;
                                strNextLevel = ddlNextLevel.SelectedValue;
                                if (chkMovetoNextLevel.Checked)
                                {
                                    if (hdnSelectedIDS.Value != string.Empty)
                                    {
                                        Facade.MemberJobCart_MoveToNextLevel(HiringMatrixInterviewLevel, base.CurrentMember.Id, hdnSelectedIDS.Value, CurrentJobPosting.Id, strNextLevel);
                                        Facade.UpdateCandidateRequisitionStatus(CurrentJobPosting.Id, hdnSelectedIDS.Value, CurrentMember.Id,Convert .ToInt32 ( strNextLevel));
                                    }
                                }
                                sentStatus = "Successfully Sent Email.";
                                ApplyWorkFlowSetting();

                                //******Code commented by pravin khot on 2/Dec/2016**********
                                //JobPosting CurJobPosting = base.CurrentJobPosting;
                                //CurJobPosting.IsJobActive = true;
                                //CurJobPosting.IsTemplate = false;
                                //if (chkReqStatus.Checked)
                                //{
                                //    IList<GenericLookup> look = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.RequisitionStatusValues , ddlReqStatus .SelectedItem .Text );
                                //    CurJobPosting.JobStatus = look[0].Id;
                                //}

                                //Facade.UpdateJobPosting(CurJobPosting);
                                //**************END by pravin khot on 2/Dec/2016***************

                                int status = 0;
                              // IList < GenericLookup> g = Facade.GetAllGenericLookupByLookupTypeAndName(LookupType.CandidateRequisitionStatus, "Submitted");
                              // if (g != null) status = g[0].Id;
                              //  Facade.UpdateByStatus(this.CurrentJobPostingId, hdnSelectedIDS.Value, status , CurrentMember.Id);
                                string[] memberList = hdnSelectedIDS.Value.Split(',');
                                string[] EmailDetailId = MemberEmailDetailid.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                               
                                for (int i = 0; i < memberList.Length; i++)
                                {
                                    string strReceiverEmails = "";
                                    if (memberList[i] != "" && Convert.ToInt32(memberList[i]) > 0)
                                    {
                                        MemberSubmission memberSubmission = new MemberSubmission();
                                        memberSubmission.MemberId = Convert.ToInt32(memberList[i]);
                                        memberSubmission.JobPostingId = base.CurrentJobPostingId;
                                        memberSubmission.SubmittedDate = DateTime.Now;
                                        memberSubmission.CompanyId = CurrentJobPosting.ClientId;
                                        memberSubmission.IsRemoved = false;
                                        memberSubmission.CreatorId = base.CurrentMember.Id;
                                        int count = 0;

                                        //***********Code modify and added by pravin khot on 28/Nov/2016**********
                                        //for (int k = 0; k < lstTo.Items.Count; k++)
                                        //{
                                        //    if (lstTo.Items[k].Selected)
                                        //    {

                                        //        if (strReceiverEmails.Trim() != "") strReceiverEmails += "; ";
                                        //        strReceiverEmails += lstTo.Items[k].Value;
                                        //    }
                                        //}

                                        if (Convert.ToInt32(ddlDivisionListTO.SelectedValue) > 0)
                                        {
                                            CompanyContact cc = Facade.GetCompanyContactById(Convert.ToInt32(ddlDivisionListTO.SelectedValue));
                                            if (cc != null)
                                            {
                                                if (strReceiverEmails.Trim() != "") strReceiverEmails += "; ";
                                                strReceiverEmails += cc.Email;                                              
                                            }
                                            //Member member = Facade.GetMemberById(Convert.ToInt32(ddlDivisionListTO.SelectedValue));

                                            //if (strReceiverEmails.Trim() != "") strReceiverEmails += "; ";
                                            //strReceiverEmails += member.PrimaryEmail;
                                        }

                                        //if (uclDivisionList.SelectedItems != string.Empty)
                                        //{
                                        //    string[] DivisionIdsTO = DivisionIdTO.Split(',');
                                        //    foreach (string idTO in DivisionIdsTO)
                                        //    {
                                        //        Member member = Facade.GetMemberById(Convert.ToInt32(idTO));

                                        //        if (strReceiverEmails.Trim() != "") strReceiverEmails += "; ";
                                        //        strReceiverEmails += member.PrimaryEmail;
                                        //    }
                                        //}  
                                        //**************************END***********************************

                                        memberSubmission.MemberEmailDetailId = Convert.ToInt32(EmailDetailId[count]);
                                       // memberSubmission.ReceiverEmail = lstTo.Items[k].Value;
                                        memberSubmission.ReceiverEmail = strReceiverEmails;
                                        Facade.AddMemberSubmission(memberSubmission,memberSubmission .MemberId .ToString ());
                                        count = count + 1;
                                    }

                                }
                                MiscUtil.EventLogForCandidate(EventLogForCandidate.CandidateSubmittedToBU , CurrentJobPostingId, hdnSelectedIDS.Value, CurrentMember.Id, Facade);
                                Session["SentStatus"] = "Successfully Sent Email.";
                                var webConfig = System .Web .Configuration . WebConfigurationManager.OpenWebConfiguration("~");
                                var submit = webConfig.AppSettings.Settings["Submission"];
                                var offer = webConfig.AppSettings.Settings["Offer"];
                                var join = webConfig.AppSettings.Settings["Join"];
                                string showModal = "";
                                if (chkMovetoNextLevel.Checked)
                                {
                                    if (ddlNextLevel.SelectedItem.Text == join.Value)
                                    {
                                        showModal = "Joined";
                                    }
                                    if (ddlNextLevel.SelectedItem.Text == offer.Value)
                                    {
                                        showModal = "Offered";
                                    }
                                    if (ddlNextLevel.SelectedItem.Text == "Offer Decline")
                                    {
                                        showModal = "Offer Decline";

                                    }
                                }
                                if(ddlNextLevel .SelectedItem .Text =="Offered" || ddlNextLevel .SelectedItem .Text =="Joined")
                                    Session["PageFrom"] = "Submission";
                                sentStatus = "";
                                //Helper.Url.Redirect(UrlConstants.ATS.ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX, string.Empty, UrlConstants.PARAM_JOB_ID, StringHelper.Convert(base.CurrentJobPostingId), UrlConstants.PARAM_MSG, sentStatus, UrlConstants.PARAM_TAB, strNextLevel,UrlConstants .PARAM_MEMBER_ID ,hdnSelectedIDS .Value ); // 10315  
                               // if (ViewState["PreviousPage"] != null)
                                {
                                    SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ATS.ATS_PRESELECTED_INTERVIEW_HIRINGMATRIX, string.Empty, UrlConstants.PARAM_MSG, sentStatus, UrlConstants.PARAM_SELECTEDTAB, HiringMatrixInterviewLevel.ToString(), "SelectedLevel", ddlNextLevel.SelectedValue, UrlConstants.PARAM_MEMBER_ID, hdnSelectedIDS.Value, UrlConstants .PARAM_JOB_ID ,CurrentJobPostingId .ToString (),UrlConstants .PARAM_PAGE_FROM ,showModal,"TargetLevelId",ddlNextLevel .SelectedValue  );
                                    Response.Redirect(url.ToString());
                                }

                            }

                            MiscUtil.ShowMessage(lblMessage, sentStatus, false);
                        }
                        else
                            MiscUtil.ShowMessage(lblMessage, "Please select at least one candidate", true);
            }
            else
            {
                uclConfirm.AddMessage("Mail Server SMTP details have not been entered. Would you like to enter them now?", ConfirmationWindow.enmMessageType.Attention, true, true, "");
            }
        }
        public void MessageAnswered(object sender, ConfirmationWindow.MsgBoxEventArgs e)
        {
            if (e.Answer == ConfirmationWindow.enmAnswer.OK)
            {
                SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.ApplicationBaseUrl + "Employee/MailSetup.aspx", string.Empty);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(MiscUtil), "open", "<script>window.open('" + url + "');</script>", false);
            }

        }

        public void ResumeAttachInList()
        {
            if (hdnResumeDocumentID.Value != string.Empty)
            {
                for (int i = 0; i < lsvViewApplicantList.Items .Count ; i++)
                {
                    Label  _editControl =(Label ) lsvViewApplicantList.Items [i].FindControl ("lblJobTitle");
                    HiddenField hdnFileTypeId = (HiddenField)lsvViewApplicantList.Items [i].FindControl ("hdnFileTypeId");
                    HiddenField   MId =(HiddenField ) lsvViewApplicantList.Items [i].FindControl ("hndIds");
                    HiddenField hdnSelectedRes = (HiddenField)lsvViewApplicantList.Items[i].FindControl("hdnSelectedres");
                    IList<string> DocIds = hdnResumeDocumentID.Value.Split(new char [] {','}, StringSplitOptions.RemoveEmptyEntries);
                    string ResLink = string.Empty;
                    bool isempty = true;
                    foreach (string id in DocIds)
                    {
                        MemberDocument doc = Facade.GetMemberDocumentById(Int32.Parse(id));
                        if (doc != null && MId != null)
                        {
                            if (hdnMemberId.Value == MId.Value)
                            {
                                if (isempty)
                                    hdnSelectedRes.Value = string.Empty;
                                GenericLookup look = Facade.GetGenericLookupById(doc.FileTypeLookupId);
                                if (ResLink == string.Empty)
                                    hdnFileTypeId.Value = string.Empty;
                                hdnFileTypeId.Value = hdnFileTypeId.Value + "," + look.Name;
                                ResLink  =ResLink+"</br>"+ GetDocumentLink(doc.FileName, look.Name, doc.MemberId);
                                _editControl.Text = ResLink.Substring (5);
                                if (hdnSelectedRes.Value == string.Empty)
                                    hdnSelectedRes.Value = id;
                                else
                                    hdnSelectedRes.Value = hdnSelectedRes.Value + "," + id;
                                isempty = false;
                            }
                        }
                    }
                }
            }
            this.Page.Title = lnkReqTitle.Text + lblClientTitle.Text + lnkClientTitle.Text;
        }
        protected void btnPreview_Click(object sender, EventArgs e)
        {
            Company CurrentCompany = Facade.GetCompanyById(base.CurrentJobPosting.ClientId);
            State ContactState = new State();
            Country ContactCountry = new Country();
            string strState = string.Empty;
            string strCountry = string.Empty;
            if (CurrentCompany.PrimaryContact.StateId > 0)    ContactState = Facade.GetStateById(CurrentCompany.PrimaryContact.StateId);
            strState = (ContactState == null) ? "" : ContactState.Name;
            if (CurrentCompany.PrimaryContact.CountryId > 0)   ContactCountry = Facade.GetCountryById(CurrentCompany.PrimaryContact.CountryId);
            strCountry = (ContactCountry == null) ? "" : ContactCountry.Name;

            StringBuilder strbldrAddress = new StringBuilder();
            if (CurrentCompany.PrimaryContact.Address1.IsNotNullOrEmpty())
                strbldrAddress.Append(CurrentCompany.PrimaryContact.Address1);
            if (CurrentCompany.PrimaryContact.Address2.IsNotNullOrEmpty())
                strbldrAddress.Append("<br/>" + CurrentCompany.PrimaryContact.Address2);
            if (CurrentCompany.PrimaryContact.City.IsNotNullOrEmpty())
                strbldrAddress.Append("<br/>" + CurrentCompany.PrimaryContact.City);
            if (strState.IsNotNullOrEmpty())
                strbldrAddress.Append("<br/>" + strState);
            if (strCountry.IsNotNullOrEmpty())
                strbldrAddress.Append("<br/>" + strCountry);
            if (CurrentCompany.PrimaryContact.ZipCode.IsNotNullOrEmpty())
                strbldrAddress.Append("<br/>" + CurrentCompany.PrimaryContact.ZipCode);

            string strTo=string .Empty ;
            //strTo = (txtToAdd.Text != string.Empty && txtToAdd.Text != "Other TO Emails") ? txtToAdd.Text + "; " : string.Empty;
            foreach (ListItem litem in  lstTo.Items  )
            {
                if(litem .Selected ==true ) strTo = strTo + litem .Value +"; ";
            }
            string key = "";
            int SelectedAttachments = 0;
            
            string filePaths = string.Empty;
            string TempPath = string.Empty;
            UploadedFiles.Clear();
            if (PreviewDir !=null && PreviewDir != string.Empty)
            {
                TempPath = PreviewDir;
            }
            else
                TempPath = GetTempFolder();
            foreach (ListViewDataItem item in lsvViewApplicantList.Items )
            {
                Label _editControl = (Label)item.FindControl("lblJobTitle");
                HiddenField hdnFileTypeId = (HiddenField)item.FindControl("hdnFileTypeId");
                IList<string> FileTypeName = hdnFileTypeId.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                HiddenField MId = (HiddenField)item.FindControl("hndIds");
                CheckBox chkMember = (CheckBox)item.FindControl("chkItemCandidates");
                IList<string> resTitle = _editControl.Text.Split(new string[] { "</br>" }, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                foreach (string res in resTitle)
                {
                    if (chkMember.Checked)
                    {
                        string filePath = MiscUtil.GetMemberDocumentPath(this.Page, Int32.Parse(MId.Value), MiscUtil.RemoveScript(res), FileTypeName[i], true);
                        if (Directory.Exists(TempPath))
                        {
                            if (!Directory.Exists(TempPath + "\\" + MId.Value + FileTypeName[i]))
                            {
                                Directory.CreateDirectory(TempPath + "\\" + MId.Value + FileTypeName[i]);
                                filePaths = Path.Combine(TempPath + "\\" + MId.Value + FileTypeName[i], MiscUtil.RemoveScript(res));
                            }
                            else
                                filePaths = Path.Combine(TempPath + "\\" + MId.Value + FileTypeName[i], MiscUtil.RemoveScript(res));
                        }
                        //filePaths = Path.Combine(TempPath, MiscUtil.RemoveScript(res));
                        try
                        {
                            File.Copy(Server.MapPath(filePath.Replace("%20", " ")), filePaths);
                            UploadedFiles.Add(MiscUtil.RemoveScript(res), filePaths + "?" + MiscUtil.RemoveScript(res).Length.ToString());
                        }
                        catch
                        {
                        }
                        key = key + MiscUtil.RemoveScript(res) + "?" + hdnTmpFolder .Value  + "\\" + MId.Value + FileTypeName[i] + ";";
                        SelectedAttachments++;
                    }
                }
            }
        
            foreach (ListItem litem in lstAttachments .Items)
            {
                if (litem.Selected == true)
                {
                    key = key + litem.Text+"?"+hdnTmpFolder .Value + ";";
                    SelectedAttachments++;
                }
            }
            string CC = string.Empty;
            string sss = txtCCAdd.Text;
            CC = (txtCCAdd.Text != string.Empty && txtCCAdd.Text !="Other CC Emails")? txtCCAdd.Text + "; " : string.Empty;
            foreach (ListItem chkItem in lstCC .Items )
            {
                if (chkItem.Selected)
                {
                    CC = CC + chkItem.Value + "; ";
                }
            }
            foreach (ListItem chkItem in lstCCAssigned .Items )
            {
                if (chkItem.Selected)
                {
                    CC = CC + chkItem.Value + "; ";
                }
            }
            string BCC = string.Empty;
            //BCC = (txtBCCAdd.Text != string.Empty && txtBCCAdd.Text != "Other BCC Emails") ? txtBCCAdd.Text + "; " : string.Empty;
            BCC = (hfOtherInterviewers.Value != string.Empty && hfOtherInterviewers.Value != "Other BCC Emails") ? hfOtherInterviewers.Value + "; " : string.Empty;
          

            foreach (ListItem chkItem in lstBCC.Items)
            {
                if (chkItem.Selected)
                {
                    BCC = BCC + chkItem.Value + "; ";
                }
            }
            foreach (ListItem chkItem in lstBCCAssigned.Items)
            {
                if (chkItem.Selected)
                {
                    BCC = BCC + chkItem.Value + "; ";
                }
            }
            BuildPreviewXML(lblFromAdd.Text, strTo, txtSubject.Text, BCC, CC, WHECoverLetter.TextXhtml, key, false);
            SecureUrl url = UrlHelper.BuildSecureUrl(UrlConstants.CommonPages.EMAIL_PREVIEW, string.Empty, "TempFolder", hdnTmpFolder.Value);
            this.Page.ClientScript.RegisterStartupScript(typeof(Page), "PreviewEmailWindow", "<script language=JavaScript>window.open('" + url + "');</script>");        //0.13
        }

        private void BuildPreviewXML(string strFrom, string strTo, string strSubject, string strBCC, string strCC, string strBody, string strAttachements, bool AddSignature)
        {

            string xmlfileDirectory = string.Empty;
            string PreviewXML = string.Empty;
            PreviewXML += "<Preview>";
            PreviewXML += "<From>" + strFrom.Replace("<", "&lt;") + "</From>";
            PreviewXML += "<To>" + strTo.Replace("<", "&lt;") + "</To>";
            PreviewXML += "<Subject>" + strSubject.Replace("<","&lt;") + "</Subject>";
            PreviewXML += "<Body>" + strBody.Replace("<", "&lt;") + "</Body>";
            PreviewXML += "<BCC>" + strBCC.Replace("<", "&lt;") + "</BCC>";
            PreviewXML += "<CC>" + strCC.Replace("<", "&lt;") + "</CC>";
            PreviewXML += "<Attachments>" + strAttachements + "</Attachments>";
            PreviewXML += "<Signature>" + (AddSignature == true ? "Yes" : "No") + "</Signature>";
            PreviewXML += "</Preview>";
            PreviewDir = GetTempFolder();
            if (System.IO.File.Exists(PreviewDir + "\\Preview.xml"))
            {
                System.IO.File.Delete(PreviewDir + "\\Preview.xml");
            }
            StreamWriter sWriter = new StreamWriter(PreviewDir + "\\Preview.xml");
            sWriter.WriteLine(PreviewXML);
            sWriter.Close();
        }

        private string GetDocumentLink(string strFileName, string strDocumenType,int memberId)
        {
            string strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, memberId, strFileName, strDocumenType, true);

            if (strFilePath != strFileName)
            {
                return "<a href='" + strFilePath + "' TARGET='_blank' >" + strFileName + "</a>";
            }
            else
            {
                return strFileName + "(File not found)";
            }
        }

        protected void btnTracker_Click(object sender, EventArgs e)
        {
            IList<Member> memberList = Facade.GetAllMemberByIds(SelectedApplicants);
            CreateTrackr(memberList);
        }
        protected void btnAtach_Click(object sender, EventArgs e)
        {
            UploadFile();
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            if (lstAttachments.Items.Count > 0 && lstAttachments.SelectedIndex >= 0)
            {
                string dir = base.CurrentJobPostingId.ToString();
                string path = Path.Combine(UrlConstants.RequisitionDocumentDirectory, dir);
                string filePath = path + "\\" + lstAttachments.SelectedItem.Text;
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
                lstAttachments.Items.RemoveAt(lstAttachments.SelectedIndex);
            }
            else
            {
                MiscUtil.ShowMessage(lblMessage, "Please select file to remove.", true);
                return;
            }
        }

        protected void lsvViewApplicantList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            Member member = ((ListViewDataItem)e.Item).DataItem as Member;
            if(member!=null )
            {
                HyperLink lnkApplicant = (HyperLink)e.Item .FindControl("lnkApplicant");
                Label lblApplicantName = (Label)e.Item.FindControl("lblApplicantName");
                Label lblCityState = (Label)e.Item.FindControl("lblCityState");
                Label lblEmail = (Label)e.Item.FindControl("lblEmail");
                Label lblMobileNo = (Label)e.Item.FindControl("lblMobileNo");
                Label lblJobTitle = (Label)e.Item.FindControl("lblJobTitle");
                ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
                HiddenField hdnIds = (HiddenField)e.Item.FindControl("hndIds");
                HiddenField hdnFileTypeId = (HiddenField)e.Item.FindControl("hdnFileTypeId");
                HiddenField hdnID = (HiddenField)e.Item.FindControl("hdnnID");
                HiddenField hdnSelectedRes = (HiddenField)e.Item.FindControl("hdnSelectedres");
                HiddenField hdnDocumentiD = (HiddenField)e.Item.FindControl("hdnDocumentiD");
                hdnID.Value = member.Id.ToString();
                hdnIds.Value = member.Id.ToString();
                string strFullName = member.FirstName + " " + member.LastName;
                if (strFullName.Trim() == "")
                {
                    strFullName = "No Candidate Name";
                }
                ControlHelper.SetHyperLink(lnkApplicant, UrlConstants.Candidate.OVERVIEW, string.Empty, strFullName, UrlConstants.PARAM_MEMBER_ID, StringHelper.Convert(member.Id), UrlConstants.PARAM_SITEMAP_ID, UrlConstants.Candidate.OVERVIEW_SITEMAP_ID, UrlConstants.PARAM_SITEMAP_PARENT_ID,"12");
                lblApplicantName.Text = strFullName;// member.FirstName + " " + member.LastName;
                lblEmail.Text = member.PrimaryEmail.ToString();
                lblMobileNo.Text = member.CellPhone.ToString();
                IList<MemberDocument> documen = Facade.GetLatestResumeByMemberID(member.Id);
                if (documen != null)
                {
                    foreach (MemberDocument doc in documen)
                    {
                        if (doc != null)
                        {
                            string stpath = MiscUtil.GetMemberDocumentPath(this.Page, member.Id, doc.FileName, "Word Resume", false);
                            if (File.Exists(stpath))
                            {
                                GenericLookup look = Facade.GetGenericLookupById(doc.FileTypeLookupId);
                                lblJobTitle.Text = GetDocumentLink(doc.FileName, look.Name, member.Id);
                                hdnFileTypeId.Value = look.Name;
                                hdnDocumentiD.Value = doc.Id.ToString();
                                //hdnSelectedRes.Value = doc.Id.ToString();
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (!Shown)
                    {
                        MiscUtil.ShowMessage(lblMessage, "Not all candidates have resume(s) attached.", false);
                        Shown = true;
                    }
                }
                MemberDetail memberDetail = Facade.GetMemberDetailByMemberId(member.Id);
                if (memberDetail != null)
                {
                    lblCityState.Text = memberDetail.CurrentCity + ((!string.IsNullOrEmpty(memberDetail.CurrentCity) && memberDetail.CurrentStateId > 0) ? ", " : string.Empty) + MiscUtil.GetStateNameById(memberDetail.CurrentStateId, Facade);
                }
                btnEdit.CommandArgument = member.Id .ToString();
            }
        }

        protected void lsvViewApplicantList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int memberID=Int32 .Parse ( e.CommandArgument .ToString ());
            if(StringHelper .IsEqual (e.CommandName ,"EditItem"))
            {
                uclTemplate .ModalTitle = MiscUtil.GetMemberNameById(memberID, Facade) + " - Attach Resume";
                ModalPopupExtender.Enabled = true;
                ucAttachmentModal.MemberID = memberID;
                ucAttachmentModal.clear();
                ucAttachmentModal.Focus();
                ModalPopupExtender.Show();
                hdnMemberId.Value = memberID.ToString();
                 HiddenField hdn = (HiddenField)ucAttachmentModal.FindControl("hdnSelectedIDS");
                 hdn.Value = string.Empty;
                 HiddenField hdnselres = (HiddenField)ucAttachmentModal.FindControl("hdnSelRes");
                 HiddenField hdnSelectedRes = (HiddenField)e.Item .FindControl("hdnSelectedres");
                 HiddenField hdnDocumentiD = (HiddenField)e.Item.FindControl("hdnDocumentiD");
                 if (hdnSelectedRes.Value != string.Empty)
                     hdnselres.Value = hdnSelectedRes.Value;
                 else
                     hdnselres.Value = hdnDocumentiD.Value;
                 
            }
        }

        private void CreateTrackr(IList<Member> mem)
        {
            int i, j;
            int count = mem.Count;
            string location = System.Web.Hosting.HostingEnvironment.MapPath(UrlConstants.Teamplate.RESOURCE_DIR);
            string fileName = UrlConstants.Teamplate.BU_TRACKER_TEAMPLATE_FILENAME;
            string newfile = "BUTracker-" + CurrentJobPosting.JobPostingCode + ".xlsx";
            string url = location + fileName;
            string newURL = location + newfile;
            var File1 = new FileInfo(url);
            try
            {
                ExcelPackage pck = new ExcelPackage(File1);
                pck.Load(new FileStream(url,FileMode.Open));
                ExcelWorksheet xlWorkSheet = pck.Workbook.Worksheets["Sheet1"];
                xlWorkSheet.Column(7).Width = 15; 
                xlWorkSheet.Column(9).Width = 30;
                xlWorkSheet.Column(10).Width = 10;
                xlWorkSheet.Column(12).Width = 20;
                DataTable  dt = GenerateTrckr(mem);
                xlWorkSheet.Cells["A1"].LoadFromDataTable(dt, true);
                for (i = 1; i <= count+1; i++)
                {
                    for (j = 1; j <= 15; j++)
                    {
                        xlWorkSheet.Row(i).Height = 31;
                        xlWorkSheet.Cells[i,j].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        xlWorkSheet.Cells[i, j].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        xlWorkSheet.Cells[i, j].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        xlWorkSheet.Cells[i, j].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        xlWorkSheet.Cells[i, j].Style.Border.Left.Color.SetColor(Color.Black);
                        xlWorkSheet.Cells[i, j].Style.Border.Right.Color.SetColor(Color.Black);
                        xlWorkSheet.Cells[i, j].Style.Border.Top.Color.SetColor(Color.Black);
                        xlWorkSheet.Cells[i, j].Style.Border.Bottom.Color.SetColor(Color.Black);
                        if (i > 1)
                        {
                            xlWorkSheet.Cells[i, j].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            xlWorkSheet.Cells[i, j].Style.Fill.BackgroundColor.SetColor(Color.White);
                        }
                    }
                }
                for (j = 1; j <= 15; j++)
                {
                    xlWorkSheet.Cells[i, j].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    xlWorkSheet.Cells[i, j].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    xlWorkSheet.Cells[i, j].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    xlWorkSheet.Cells[i, j].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    xlWorkSheet.Cells[i, j].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    xlWorkSheet.Cells[i, j].Style.Border.Left.Color.SetColor(Color.Black);
                    xlWorkSheet.Cells[i, j].Style.Border.Right.Color.SetColor(Color.Black);
                    xlWorkSheet.Cells[i, j].Style.Border.Top.Color.SetColor(Color.Black);
                    xlWorkSheet.Cells[i, j].Style.Border.Bottom.Color.SetColor(Color.Black);
                    xlWorkSheet.Cells[i, j].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                }
                xlWorkSheet.Row(i).Height = 31;
                if (File.Exists(newURL))
                    File.Delete(newURL);
                //pck.SaveAs(new FileStream(newURL,FileMode.Create));

                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", String.Format(System.Globalization.CultureInfo.InvariantCulture, "attachment; filename={0}", newfile));
                Response.BinaryWrite(pck.GetAsByteArray());
                Response.End();
            }
            catch
            {
            }
        }

        private DataTable GenerateTrckr(IList<Member> mem)
        {
            DataTable data = new DataTable();
            int i = 1;
            data.Columns.Add("Sr. No.", typeof(string));
            data.Columns.Add("Date", typeof(string));
            data.Columns.Add("Name", typeof(string));
            data.Columns.Add("Skills", typeof(string));
            data.Columns.Add("Total Years", typeof(string));
            data.Columns.Add("Relevant Years", typeof(string));
            data.Columns.Add("Availability", typeof(string));
            data.Columns.Add("Mobile Number", typeof(string));
            data.Columns.Add("Email ID", typeof(string));
            data.Columns.Add("Current CTC (LPA)", typeof(string));
            data.Columns.Add("Expected CTC (LPA)", typeof(string));
            data.Columns.Add("Current Employer", typeof(string));
            data.Columns.Add("Current Location", typeof(string));
            data.Columns.Add("Reason for Change", typeof(string));
            data.Columns.Add("Remarks", typeof(string));

            foreach (Member member in mem)
            {
                data.Rows.Add( i.ToString(),System.DateTime.Today.ToString(),(member.FirstName + " " + member.MiddleName + " " + member.LastName).ToString(), member.Skills.TrimEnd(','), member.ExpYears, " ",  member.Avaliability, member.CellPhone, member.PrimaryEmail,member.CurrentCTC, member.ExpectedCTC, member.CurrentEmployer, member.PermanentCity, " ", member.Remarks );
                data.NewRow();
                i++;
            }

            return data;
        }
        #endregion

        private string SaveMemberEmail()
        {
            string MemberEmailDetailid = "";

            if (true) // Isvalid
            {
                try
                {
                    string Toaddress = "";
                    //Toaddress = (txtToAdd.Text != string.Empty && txtToAdd.Text != "Other To Emails") ? txtToAdd.Text + "; " : string.Empty;

                    //****************Code commented and modify by pravin khot on 29/Nov/2016*************
                    //for (int i = 0; i < lstTo.Items.Count; i++)
                    //{
                    //    if (lstTo.Items[i].Selected)
                    //    {
                    //        Toaddress +=lstTo.Items[i].Value+"; ";
                    //    }
                    //}
                    if (Convert.ToInt32(ddlDivisionListTO.SelectedValue) > 0)
                    {
                        //Member member = Facade.GetMemberById(Convert.ToInt32(ddlDivisionListTO.SelectedValue));
                        //Toaddress = member.PrimaryEmail;
                        CompanyContact cc = Facade.GetCompanyContactById(Convert.ToInt32(ddlDivisionListTO.SelectedValue));
                        if (cc != null)
                        {
                            Toaddress = cc.Email ;
                        }
                        
                    }

                    //**************************END**************************************

                    MemberEmail memberEmail = BuildMemberEmail("", Toaddress);

                    if (memberEmail.IsNew)
                    {
                        memberEmail = Facade.AddMemberEmail(memberEmail);
                        MemberEmailDetailid += SaveMemberEmailDetail(memberEmail).ToString() + "::";
                        SaveMemberEmailAttachment(memberEmail);
                        Facade.UpdateMemberEmail(memberEmail);
                    }
                    else
                        memberEmail = Facade.UpdateMemberEmail(memberEmail);
                }
                catch (ArgumentException ex)
                {
                    MiscUtil.ShowMessage(lblMessage, ex.Message, true);
                }

            }
            return MemberEmailDetailid;
        }

        private MemberEmail BuildMemberEmail(string strToName, string strToEmail)
        {
            MemberEmail memberEmail = new MemberEmail();
            EmailHelper emailHelper = new EmailHelper();

            emailHelper.FromEmail = (lblFromAdd.Text.Replace("[", "<")).Replace("]", ">");
            emailHelper.AdditionalMailMessage = HttpUtility.HtmlDecode(WHECoverLetter.Text);
            emailHelper.RecipientName = strToName;
            emailHelper.ShowJobDescription = false;
            emailHelper.HiringMatrixApplicant = SelectedApplicants;
            emailHelper.ShowScreeningQuestion = false;
            memberEmail.SenderEmail = lblFromAdd.Text.Substring(lblFromAdd.Text.IndexOf('[') + 1, (lblFromAdd.Text.IndexOf(']') - lblFromAdd.Text .IndexOf('[')) - 1);
            memberEmail.ReceiverEmail = strToEmail;
            memberEmail.EmailTypeLookupId = (int)EmailType.Sent;
            memberEmail.ParentId = 0;
            memberEmail.Subject = txtSubject.Text.Trim();
            memberEmail.EmailBody = WHECoverLetter.TextXhtml;
            memberEmail.Status = 0;
            memberEmail.IsRemoved = false;
            memberEmail.CreatorId = base.CurrentMember.Id;
            memberEmail.UpdatorId = base.CurrentMember.Id;
            memberEmail.SentDate = DateTime.Now.ToString();
            string strCc = string.Empty;
            //strCc = (txtBCCAdd.Text != string.Empty && txtBCCAdd.Text != "Other CC Emails") ? txtBCCAdd.Text.Replace(',', ';') + "; " : string.Empty;
            strCc = (hfOtherInterviewers.Value != string.Empty && hfOtherInterviewers.Value != "Other BCC Emails") ? hfOtherInterviewers.Value + "; " : string.Empty;

            string DivisionIdCC = uclDivisionListCC.SelectedItems; //added by pravin khot on 24/Nov/2016
            if (DivisionIdCC != string.Empty)
            {
                string[] DivisionIdsCC = DivisionIdCC.Split(',');
                foreach (string idCC in DivisionIdsCC)
                {
                    CompanyContact cc = Facade.GetCompanyContactById(Convert.ToInt32(idCC));
                    //Member member = Facade.GetMemberById(Convert.ToInt32(idCC));
                    strCc = strCc + cc.Email + "; ";
                }
            }

            //foreach (ListItem chkItem in lstCC .Items )
            //{
            //    if (chkItem.Selected)
            //    {
            //        strCc = strCc + chkItem.Value+"; ";
            //    }
            //}
            //foreach (ListItem chkItem in lstCCAssigned .Items )
            //{
            //    if (chkItem.Selected)
            //    {
            //        strCc = strCc + chkItem.Value + "; ";
            //    }
            //}
            memberEmail.CC = strCc.Trim ();
            string strBCc = string.Empty;
            //strBCc = (txtBCCAdd.Text != string.Empty && txtBCCAdd .Text !="Other BCC Emails") ? txtBCCAdd.Text.Replace(',', ';')+"; " : string.Empty;
            //foreach (ListItem chkItem in lstBCC.Items)
            //{
            //    if (chkItem.Selected)
            //    {
            //        strBCc = strBCc + chkItem.Value+ ";";
            //    }
            //}
            //foreach (ListItem chkItem in lstBCCAssigned.Items)
            //{
            //    if (chkItem.Selected)
            //    {
            //        strBCc = strBCc + chkItem.Value+ ";";
            //    }
            //}
            memberEmail.BCC = strBCc.Trim();
            return memberEmail;
        }

        private int SaveMemberEmailDetail(MemberEmail memberEmail)
        {
            int memberEmailDetailId = 0;
            if (hdnTo.Value != string.Empty)
            {
                MemberEmailDetail memberEmailDetail = new MemberEmailDetail();
                memberEmailDetail.AddressTypeId = (int)EmailAddressType.To;
                memberEmailDetail.EmailAddress = hdnTo.Value;
                memberEmailDetail.MemberId = CurrentMember.Id;
                memberEmailDetail.SendingTypeId = (int)EmailSendingType.Single;
                memberEmailDetail.MemberEmailId = memberEmail.Id;
                memberEmailDetail.CreatorId = base.CurrentMember.Id;
                memberEmailDetail = Facade.AddMemberEmailDetail(memberEmailDetail);
                memberEmailDetailId = memberEmailDetail.Id;
            }

            string[] CCarr; string[] BCCarr;
            string strCc = string.Empty;

            string DivisionIdCC = uclDivisionListCC.SelectedItems; //added by pravin khot on 24/Nov/2016
            if (DivisionIdCC != string.Empty)
            {
                string[] DivisionIdsCC = DivisionIdCC.Split(',');
                foreach (string idCC in DivisionIdsCC)
                {
                    CompanyContact cc = Facade.GetCompanyContactById(Convert.ToInt32(idCC));
                    strCc = cc.Email + "," + strCc;
                }
            }
            //foreach (ListItem chkItem in lstCC .Items )
            //{
            //    if (chkItem.Selected)
            //        strCc = chkItem.Value + "," + strCc;
            //}
            //foreach (ListItem chkItem in lstCCAssigned .Items )
            //{
            //    if (chkItem.Selected)
            //        strCc = chkItem.Value + "," + strCc;
            //}
            CCarr = strCc.Split((char)Convert.ToChar(","));
            if (CCarr == null)
                CCarr = strCc.Split((char)Convert.ToChar(";"));
            if (CCarr != null)
            {
                foreach (string cc in CCarr)
                {
                    MemberEmailDetail memberEmailDetail = new MemberEmailDetail();
                    memberEmailDetail.AddressTypeId = (int)EmailAddressType.CC;
                    memberEmailDetail.EmailAddress = cc.Trim();
                    memberEmailDetail.MemberId = CurrentMember.Id;
                    memberEmailDetail.SendingTypeId = (int)EmailSendingType.Single;
                    memberEmailDetail.MemberEmailId = memberEmail.Id;
                    memberEmailDetail.CreatorId = base.CurrentMember.Id;
                    memberEmailDetail = Facade.AddMemberEmailDetail(memberEmailDetail);
                    if (memberEmailDetailId == 0)
                    {
                        memberEmailDetailId = memberEmailDetail.Id;
                    }

                }

            }


            if (CCarr == null)
            {
                MemberEmailDetail memberEmailDetail = new MemberEmailDetail();
                memberEmailDetail.AddressTypeId = 0;
                memberEmailDetail.EmailAddress = "";
                memberEmailDetail.MemberId = CurrentMember.Id;
                memberEmailDetail.SendingTypeId = (int)EmailSendingType.Single;
                memberEmailDetail.MemberEmailId = memberEmail.Id;
                memberEmailDetail.CreatorId = base.CurrentMember.Id;
                memberEmailDetail = Facade.AddMemberEmailDetail(memberEmailDetail);
                if (memberEmailDetailId == 0)
                {
                    memberEmailDetailId = memberEmailDetail.Id;
                }

            }
            return memberEmailDetailId;
        }

        private void GenerateCoverLetter()
        {
            string Letter = "<div> Dear (BU Contact),<br><br>Please find attached shortlisted candidates for " + base.CurrentJobPosting.JobTitle + ". Looking forward for your feedback to process these candidates to the next level.<br><br>Signature<br>"+ CurrentMember.FirstName+" " + CurrentMember.LastName+"</div>";
            WHECoverLetter.InnerHtml = Letter;
        }
        public void DivisionChangeEvent() //added by pravin on 25/Nov/2016
        {
            try
            {
                if (CurrentJobPostingId > 0)
                {
                    if (base.CurrentJobPosting.ClientId > 0)
                    {
                        Company CurrentCompany = Facade.GetCompanyById(base.CurrentJobPosting.ClientId);
                        IList<CompanyContact> companyContactList = Facade.GetAllCompanyContactByComapnyId(CurrentCompany.Id);
                        int divisionValue = CurrentCompany.Id;
                        if (companyContactList != null)
                        {                          

                            MiscUtil.PopulateDivisionInterviewers(uclDivisionListCC.ListItem, divisionValue, Facade);
                            uclDivisionListCC.ListItem.Items.RemoveAt(0);

                            MiscUtil.PopulateDivisionInterviewers(ddlDivisionListTO, divisionValue, Facade);
                        }
                    }
    
                }
            }
            catch
            {


            }
        }//****************END**************************
        private void SendAttachment(string RecipientEmail, string cid)
        {
            foreach (ListViewDataItem item in lsvViewApplicantList.Items)
            {
                Label _editControl = (Label)item.FindControl("lblJobTitle");
                HiddenField hdnFileTypeId = (HiddenField)item.FindControl("hdnFileTypeId");
                IList<string> FileTypeName = hdnFileTypeId.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                HiddenField MId = (HiddenField)item.FindControl("hndIds");
                CheckBox chkMember = (CheckBox)item.FindControl("chkItemCandidates");
                IList<string> resTitle = _editControl.Text.Split(new string[] { "</br>" }, StringSplitOptions.RemoveEmptyEntries);
                int i = 0;
                foreach (string res in resTitle)
                {
                    if (chkMember.Checked)
                    {
                        //string lcpath = MiscUtil.GetMemberDocumentPath(this.Page, Int32.Parse(MId.Value), MiscUtil.ScriptRemove(res), FileTypeName[i], true);
                        string lcpath = MiscUtil.GetMemberDocumentPathOnly(this.Page, Int32.Parse(MId.Value), string.Empty, FileTypeName[i], true);
                        lcpath = System.Uri.UnescapeDataString(lcpath);
                        string mapPath = Server.MapPath(lcpath);
                        if (Directory.Exists(mapPath))
                        {
                            if (Files.ContainsValue(mapPath))
                                Files.Remove(mapPath);
                            Files.Add(MiscUtil.ScriptRemove(res), mapPath);
                        }

                    }
                    i++;
                }
            }


            if (lstAttachments.Items.Count != 0)
            {
                PreviewDir = GetTempFolder();

                foreach (ListItem chkItem in lstAttachments.Items)
                {
                    chkItem.Selected = true;
                    if (chkItem.Selected)
                    {
                        string strFilePath;
                        string filePath = Path.Combine(PreviewDir, chkItem.Text);

                        if (Files.ContainsKey(lstAttachments.SelectedItem.Text))
                            Files.Remove(lstAttachments.SelectedItem.Text);

                        Files.Add(lstAttachments.SelectedItem.Text, filePath);

                        string DocumentType = "Interview attachments";
                        strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, _memberId, chkItem.Text, DocumentType, false);//1.

                        if (File.Exists(strFilePath))
                        {
                            try
                            {
                                if (!File.Exists(filePath))
                                    File.Copy(strFilePath, filePath);
                            }
                            catch { }
                            if (Files.ContainsKey(lstAttachments.SelectedItem.Text))
                                Files.Remove(lstAttachments.SelectedItem.Text);
                            int canId = Facade.getCandidateIdbyPrimaryEmail(RecipientEmail.ToString());
                            //if (chkSendEmailAlerttoInterviewers.Checked)
                            //{
                            if (canId == 0)
                            {
                                Files.Add(lstAttachments.SelectedItem.Text, filePath);
                            }
                            //}
                        }

                        if (cid != "" || cid != string.Empty)
                        {
                            DocumentType = "Word Resume";
                            strFilePath = MiscUtil.GetMemberDocumentPath(this.Page, Convert.ToInt32(cid), chkItem.Text, DocumentType, false);//1.
                        }
                        if (File.Exists(strFilePath))
                        {
                            try
                            {
                                if (!File.Exists(filePath))
                                    File.Copy(strFilePath, filePath);
                            }
                            catch { }
                            if (Files.ContainsKey(lstAttachments.SelectedItem.Text))
                                Files.Remove(lstAttachments.SelectedItem.Text);
                            int canId = Facade.getCandidateIdbyPrimaryEmail(RecipientEmail.ToString());
                            //if (chkSendEmailAlerttoInterviewers.Checked)
                            //{
                            if (canId == 0)
                            {
                                Files.Add(lstAttachments.SelectedItem.Text, filePath);
                            }
                            //}
                        }
                    }
                }
            }
        }


}
}