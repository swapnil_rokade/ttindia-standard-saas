﻿using System;

using TPS360.Common.Helper;

namespace TPS360.Web.UI
{
    public partial class CandidateInternalEmailEditor : CandidateBasePage
    {
        #region Member Variables
        private int _memberId = 0;
        #endregion

        #region Methods

        #endregion

        #region Events

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
          
            if (!IsPostBack)
            {
                if (!StringHelper.IsBlank(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]))
                {
                    _memberId = Convert.ToInt32(Helper.Url.SecureUrl[UrlConstants.PARAM_MEMBER_ID]);
                }
                if (_memberId > 0)
                {
                    string name = MiscUtil.GetMemberNameById(_memberId, Facade);
                    this.Page.Title = name + " - " + "Email";
                }
                hdnPageTitle.Value = Page.Title;
            }
            Page.Title = hdnPageTitle.Value;
            
        }

        #endregion

        #endregion
    }
}