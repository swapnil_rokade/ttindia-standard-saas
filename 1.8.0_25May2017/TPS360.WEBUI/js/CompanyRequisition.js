
//Global XMLHTTP Request object
var XmlHttp;
//Creating and setting the instance of appropriate XMLHTTP Request object to a �XmlHttp� variable  

function CreateXmlHttp()
{
	//Creating object of XMLHTTP in IE
	try
	{
		XmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		catch(oc)
		{
			XmlHttp = null;
		}
	}
	//Creating object of XMLHTTP in Mozilla and Safari 
	if(!XmlHttp && typeof XMLHttpRequest != "undefined") 
	{
		XmlHttp = new XMLHttpRequest();
	}
}

    var ddlReq='';
    
    
function RequisitionOnChange(ddlRequisition,hdnSelectedRequisition)
{
    $('#' +hdnSelectedRequisition ).val($('#' + ddlRequisition ).val());
}
function ClientForRequisitionOnChange(ddlClientList,ddlRequisitionList,hdnCandidateId,ajaxAutoComplete) 
{
var t=$find(ajaxAutoComplete  );

ddlReq =ddlRequisitionList ;

	var ClientList = document.getElementById (ddlClientList );
    var selectedClient= ClientList.options[ClientList .selectedIndex].value;

if(t!=null)t.set_contextKey(hdnCandidateId  + '|'+ selectedClient); 
    var requestUrl = "../AJAXServers/CompanyAjaxServer.aspx?SelectedCompanyId=" + encodeURIComponent(selectedClient) + "&CurrentMemberId=" + hdnCandidateId  ;
	CreateXmlHttp();
	if(XmlHttp)
	{
		XmlHttp.onreadystatechange = HandleResponseCompanyRequisition;
		XmlHttp.open("GET", requestUrl,  true);
		XmlHttp.send(null);		
	}
}


function HandleResponseCompanyRequisition()
{	
	if(XmlHttp.readyState == 4)
	{

		if(XmlHttp.status == 200)
		{			
			
			ClearAndSetRequisitionListItems(XmlHttp.responseText);
					
		}
		else
		{
			alert("There was a problem retrieving data from the server." );
		}
	}
}

function ClearAndSetRequisitionListItems(CompanyNode)
{

     var ReqList = document.getElementById(ddlReq );
      if( ReqList.options.length>0)
      {
	    for (var count = ReqList.options.length-1; count >-1; count--)
	    {
		    ReqList.options[count] = null;
	    }
      }
   
    $("#"+ddlReq).next().find('.chzn-results').html('');
    xmlDoc = $.parseXML( CompanyNode );
    var $xml = $( xmlDoc );
    $xml.find( "Requisition" ).each(function (){
        optionItem = new Option( $(this).find('Name').text() , $(this).find('Id').text());
		ReqList.options[ReqList.length] = optionItem;
		
		$("#"+ddlReq).next().remove();
		    $("#"+ddlReq).removeClass('chzn-done');
		    
		   
    });
    
    if( ReqList.options.length==1)
	{

	     ReqList .options[0].text="No Requisitions";
	     ReqList .disabled=true ;
	     $('.chzn-container').attr('disabled', true);
	}
	else
	{
	     ReqList.options[0].text="Select Requisition" ;
	     ReqList .disabled =false;
	      $('.chzn-container').attr('disabled', false);
	}
	$(".chzn-select").chosen();

}
