/* 
-------------------------------------------------------------------------------------------------------------------------------------------
FileName: AjaxScript.js
Description: 
Created By: 
Created On:
Modification Log:
------------------------------------------------------------------------------------------------------------------------------
Ver.No.             Date                Author              Modification
------------------------------------------------------------------------------------------------------------------------------
0.1              23/Feb/2016           pravin khot          Condition added - else if (ddlEmp.options[ddlEmp.selectedIndex].value == "3")
0.2              19/May/2016           pravin khot          added condition  CandidateRegistration in function - CheckUserAvailability()
0.3              10/Jun/2016           prasanth kumar G     Introduced function CheckADUserAvailability
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
//Global XMLHTTP Request object
var XmlHttp;
var stateid;
var contactid;
var txtPrimaryEmail;
var hdnCheckValidity;
var lblMessId;
var showMess;
//Creating and setting the instance of appropriate XMLHTTP Request object to a �XmlHttp� variable  

function CreateXmlHttp()
{
	//Creating object of XMLHTTP in IE
	try
	{
		XmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		catch(oc)
		{
			XmlHttp = null;
		}
	}
	//Creating object of XMLHTTP in Mozilla and Safari 
	if(!XmlHttp && typeof XMLHttpRequest != "undefined") 
	{
		XmlHttp = new XMLHttpRequest();
	}
}


    function RequisitionJobStatusChanged(ddlJobStatus,RequisitionId,UpdatorId,lblMessage,Jobtitle)
    {
        lblMessId=lblMessage ;
        var ddlReq=document .getElementById (ddlJobStatus );
        showMess ="Successfully changed status for '" + Jobtitle + "' to '" + ddlReq .options[ddlReq .selectedIndex].text +"'";
        PageMethods.ChangeRequisitionStatus({'StatusId':ddlReq.options[ddlReq .selectedIndex].value ,'RequisitionId':RequisitionId,'UpdatorId':UpdatorId  },
        RequisitionStatusChangedCallback, 
        RequisitionStatusChangedFailed);
    }

    function EmployeeAccessChanged(ddlStatus,MemberId,UpdatorId,lblMessage)
    {
        var ddlEmp=document.getElementById(ddlStatus);
        try{
            PageMethods.EmployeeAccessStatusChange({'StatusId':ddlEmp.options[ddlEmp.selectedIndex].value,'MemberId':MemberId,'UpdatorId':UpdatorId});
            var message="";
            if(ddlEmp .options[ddlEmp .selectedIndex].value=="1")
            {
                message ="Successfully enabled application access";
                try{
                    $('#ctl00_ctl00_cphHomeMaster_cphCompanyMaster_VendorPortalAccess_uclPassword_hdnSelectedMemberID').val(MemberId );
                    $find('MPE').show();

                }catch(e)
                {
                }
            }
            //********Code added by pravin khot on 23/Feb/2016***************
            else if (ddlEmp.options[ddlEmp.selectedIndex].value == "3") {
                message = "Succesfully In-Active application access";
            }
            //*******************END**********************************
            else
            {
                message ="Succesfully blocked application access";
            }
            ShowMessage(lblMessage ,message ,false  ); 
            return;
        }
        catch(e)
        {
        }
    }



getMessageText=function (lableid,message,isError)
{
  var text="";

    text += "<div id=\"showMessageContainer\" style=\"width: 100%;margin: 10px auto;\" class=\"alert" + (!isError ? " alert-info" : "") + "\">";
    text += message;
    text += "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">\u00D7</button>";
    text += "</div>";
    
    return text ;
}
ShowMessage=function (lableid,message,isError)
{
        var lblMesage=document .getElementById (lableid);
        lblMesage.style.display="";
        lblMesage = document.getElementById(lableid);
        lblMesage.style.width="40%";
        lblMesage.style.textAlign="center";
        lblMesage.style.position="fixed";
        lblMesage.style.top = "10px"
        lblMesage.style.left = "0";
         $('#'+ lableid ).css({'margin-left':'30%'});
        lblMesage .innerHTML=getMessageText(lableid ,message,isError );
        window.setTimeout(function(){hideMeClick(lableid ); }, 5000)
       //alert ( $('#'+ lableid ).parent().attr('id'));//.css({'text-align':'center'});
}
RequisitionStatusChangedCallback = function (result ) 
{ 

 
    if(result) {
        try {
        if(result[0]=="Changed")
        {
       
           ShowMessage(lblMessId ,showMess );
        }
            mainScreen.result = result[0];
            eval(result[1]+"(mainScreen.result);");
        } catch(err) {
            ; 
        }
    }
};
RequisitionStatusChangedFailed = function (
    error, 
    userContext, 
    methodName
    ) {
    /// <summary>
    /// Callback function invoked on failure of the page method 
    /// </summary>
    /// <param name="error">error object containing error</param>
    /// <param name="userContext">userContext object</param>
    /// <param name="methodName">methodName object</param>
    if(error) {
        ;// TODO: add error handling, and show it to the user
    }
};


function Company_OnChangeFromTextBox(ddlContact,hdnSelectedContactId,selectedCompanyID)
{
    var hdnContact=document .getElementById (hdnSelectedContactId );
    if(hdnContact !=null) hdnContact .value="0";
    contactid =ddlContact;
    var selectedCompany = selectedCompanyID;
    var requestUrl = "../AJAXServers/CompanyContactAjaxServer.aspx" + "?SelectedCompanyId=" + encodeURIComponent(selectedCompany);
	CreateXmlHttp();
	if(XmlHttp)
	{
		XmlHttp.onreadystatechange = HandleCompanyContactResponse;
		XmlHttp.open("GET", requestUrl,  true);
		XmlHttp.send(null);		
	}
}
function Company_OnChange(ddlCompany,ddlContact,hdnSelectedContactId,valueType)
{
    var hdnContact=document .getElementById (hdnSelectedContactId );
    if(hdnContact !=null) hdnContact .value="0";
    companyid=ddlCompany;
    contactid =ddlContact;
    var CompanyList = document.getElementById (ddlCompany);
    var selectedCompany = CompanyList.options[CompanyList .selectedIndex].value;
    var requestUrl = "../AJAXServers/CompanyContactAjaxServer.aspx" + "?SelectedCompanyId=" + encodeURIComponent(selectedCompany) +"&FillEmail=" + valueType ;

	CreateXmlHttp();
	if(XmlHttp)
	{
		XmlHttp.onreadystatechange = HandleCompanyContactResponse;
		XmlHttp.open("GET", requestUrl,  true);
		XmlHttp.send(null);		
	}
}




function SaveHiringNote(HiringNote,CandidateId,rowid,NoteUpdatorName,NoteUpdatetime,JobPostingID, CandidateName)
{
try{

mainScreen.ModalTitle=CandidateName + " - Hiring Notes" ;
                
var note=document .getElementById (HiringNote);
var name=document .getElementById (NoteUpdatorName);
var time=document .getElementById (NoteUpdatetime);

        mainScreen.LoadServerControlHtml(
                    'Welcome',
                    {'pageID':3,'data':CandidateId,'DataValue':note.value,'NoteUpdatorName':name.value,'NoteUpdatetime':time .value,'JobPostingId':JobPostingID}, 
                    'methodHandlers.BeginRecieve');
             mainScreen.RowId=rowid ;  
    }
    catch (e)
    {
    alert (e.message);
    }            
	 return false ;
}

function EditHiringMatrixSource(SourceId,CandidateId,rowid,JobPostingID,CandidateName)
{
mainScreen.ModalTitle=CandidateName +" - Edit Candidate Source" ;
        mainScreen.LoadServerControlHtml(
                    'Welcome',
                    {'pageID':4,'data':CandidateId,'JobPostingId':JobPostingID}, 
                    'methodHandlers.BeginRecieve');
             mainScreen.RowId=rowid ;  
 
	 
}


function Company_OnChangeFillContactEmail(ddlCompany,ddlContact,hdnSelectedContactId,EmailFill)
{
    var hdnContact=document .getElementById (hdnSelectedContactId );
    if(hdnContact !=null) hdnContact .value="0";
    companyid=ddlCompany;
    contactid =ddlContact;
    var CompanyList = document.getElementById (ddlCompany);
    var selectedCompany = CompanyList.options[CompanyList .selectedIndex].value;

    var requestUrl = "../AJAXServers/CompanyContactAjaxServer.aspx" + "?SelectedCompanyId=" + encodeURIComponent(selectedCompany) + "&FillEmail=" +(EmailFill=="Vendor" ?"Email":"Memberid");

	CreateXmlHttp();
	if(XmlHttp)
	{
		XmlHttp.onreadystatechange = HandleCompanyContactResponse;
		XmlHttp.open("GET", requestUrl,  true);
		XmlHttp.send(null);		
	}
}

function Contact_OnChange(ddlCompany,hdnContact)
{ 

        var e = document.getElementById(ddlCompany );
        var hdnSelectedContact= document .getElementById (hdnContact);
        hdnSelectedContact .value=e.options[e.selectedIndex].value;
        
}


//Called when response comes back from server
function HandleCompanyContactResponse()
{
	// To make sure receiving response data from server is completed
	if(XmlHttp.readyState == 4)
	{
		// To make sure valid response is received from the server, 200 means response received is OK
		if(XmlHttp.status == 200)
		{			
			ClearAndSetCompanycontactListItems(XmlHttp.responseText);
		}
		else
		{
			alert("There was a problem retrieving data from the server." );
		}
	}
}


//Clears the contents of state combo box and adds the states of currently selected country
function ClearAndSetCompanycontactListItems(CompanyNode)
{
   
  var ContactList = document.getElementById(contactid);
    var FistText=ContactList.options[0].text;
  if( ContactList.options.length>0)
  {
  	//Clears the state combo box contents.
	for (var count = ContactList.options.length-1; count >-1; count--)
	{
		ContactList.options[count] = null;
	}
   }

   
   $("#"+contactid).next().find('.chzn-results').html('');
  xmlDoc = $.parseXML(CompanyNode);
    var $xml = $( xmlDoc );
    var i=0;
    $xml.find( "contact" ).each(function (){
        if( $(this).find('Name').text()!='')
        { i++;
            optionItem = new Option( (i==0?FistText : $(this).find('Name').text() ), $(this).find('Id').text());
		    ContactList.options[ContactList.length] = optionItem;
	    }
    });
    
    if(parseInt (i)<=1)$("#"+contactid).attr('disabled','disabled');
    else{ $("#"+contactid).removeAttr('disabled');}
      if($("#"+contactid).hasClass('chzn-select'))
		  {
            $("#"+contactid).next().remove();
		    $("#"+contactid).removeClass('chzn-done');
		    $(".chzn-select").chosen();		
		    
		   }
	ContactList.options[0].text=FistText ;

	
}


function RemoveDefaultTexts(city,radius,state)
{
    var txtCity=document .getElementById (city );
    var ddlRadius=document .getElementById (radius );
    var ddlState=document .getElementById (state );
    
    if(txtCity.value == txtCity.title)
    {
        txtCity.value = ''; 
        txtCity.className='CommonTextBox';
        if(txtCity .value!='City' && (ddlState .value!='' && ddlState .value!='0')){
        ddlRadius .disabled=false ;}
    }
}
function SetDefaultTexts(city,radius,state,zipcode)
{
var txtCity=document .getElementById (city );
var ddlRadius=document .getElementById (radius );
var ddlState=document .getElementById (state );
var txtZip=document .getElementById (zipcode );
    if (txtCity.value == ''){ txtCity.value = txtCity.title;
     txtCity.className='text-label';}
     if(txtCity.value=='City' )
     {
     txtCity.className='text-label';
     if(txtZip .value=='Zip Code' || txtZip .value=='PIN Code'){
     ddlRadius .disabled=true ;}
     }
}
function RemoveDefaultTextZipCode(city,radius,state,zipcode)
{
var txtCity=document .getElementById (city );
var ddlRadius=document .getElementById (radius );
var ddlState=document .getElementById (state );
var txtZip=document .getElementById (zipcode );
    if(txtZip.value == txtZip.title)
    {
        txtZip.value = ''; 
        txtZip.className='CommonTextBox';
        if(txtZip .value!='Zip Code' && txtZip .value!='PIN Code')
        {
        ddlRadius .disabled=false ;
        }
    }
}
function SetDefaultTextZipCode(city,radius,state,zipcode)
{
var txtCity=document .getElementById (city );
var ddlRadius=document .getElementById (radius );
var ddlState=document .getElementById (state );
var txtZip=document .getElementById (zipcode );
    if (txtZip.value == ''){ txtZip.value = txtZip.title;
     txtZip.className='text-label';}
     if(txtZip.value=='Zip Code' || txtZip .value=='PIN Code' )
     {
     txtZip.className='text-label';
     if(txtCity .value=='City' || ddlState .value=='' || ddlState .value=='0'){
     ddlRadius .disabled=true ;}
     }
}
function SetDefaultTextForZipCode(city,ddl,zip,state)
{
    if (city.value == ''){ city.value = city.title;
     city.className='text-label';}
     if (zip.value == ''){ zip.value = zip.title;
     zip.className='text-label';}
     if((zip.value=='Zip Code' || zip.value=='PIN Code') && (city.value=='City' || state .value=='' || state .value=='0'))
     {
     ddl .disabled=true ;
     }
     else {
     ddl.disabled=false ;}
     if(zip.value=='Zip Code' || zip.value=='PIN Code')
     {
     zip.className='text-label';
     }
     if(city.value=='City' || state .value=='' || state .value=='0')
     {
     if(city .value=='City'){
     city.className='text-label';}
     }
}

function State_OnChanges(ddlState,hdnState,ddlRadius,txtCity,zipcode)
{ 
        var e = document.getElementById(ddlState );
        var hdnSelectedState= document .getElementById (hdnState);
        var strUser = e.options[e.selectedIndex].value;
        hdnSelectedState .value=strUser ;
        var ddl=document .getElementById (ddlRadius );
        var city=document .getElementById (txtCity );
        var txtZip=document .getElementById (zipcode );
        if(city.value !='City' && (hdnSelectedState .value!='' && hdnSelectedState .value!='0')){
        ddl.disabled=false;}
        else 
        {
        if(txtZip .value=='Zip Code' || txtZip .value=='PIN Code'){
        ddl .disabled=true ;}
        else {ddl .disabled=false ;}
        }
}



function State_OnChange(ddlState,hdnState)
{ 
        var e = document.getElementById(ddlState );
        var hdnSelectedState= document .getElementById (hdnState);
        var strUser = e.options[e.selectedIndex].value;
        hdnSelectedState .value=strUser ;
}

function SelectedState(from)
{

    var e;
    var hdnSelectedState;
    if(from=='RequsitionEditor')
    {
        e = document.getElementById("ctl00_cphHomeMaster_ucRequisitionEditor_ddlState");
        hdnSelectedState= document .getElementById ("ctl00_cphHomeMaster_ucRequisitionEditor_hdnSelectedState");
    }
     else if(from=='PreciseSearch')
    {
        e = document.getElementById("ctl00_cphHomeMaster_UltraWebTab1__ctl0_ctrlEntireInternalDatabase_ddlState");
        hdnSelectedState= document .getElementById ("ctl00_cphHomeMaster_UltraWebTab1__ctl0_ctrlEntireInternalDatabase_hdnSelectedState");
    }
    else if(from=='portal')
    {
        e = document.getElementById("ctl00_cphHomeMaster_ucRequisitionEditor_ddlState");
        hdnSelectedState= document .getElementById ("ctl00_cphHomeMaster_ucRequisitionEditor_hdnSelectedState");
    }
       else if(from=='resume')
    {
        e = document.getElementById("ctl00_cphCandidateMaster_uwtResumeBuilder__ctl0_ucntrlBasicInfo_ddlState");
        hdnSelectedState= document .getElementById ("ctl00_cphCandidateMaster_uwtResumeBuilder__ctl0_ucntrlBasicInfo_hdnSelectedState");
    }
        var strUser = e.options[e.selectedIndex].value;
        hdnSelectedState .value=strUser ;
//alert(strUser );
}
function Country_OnChanges(ddlCountry,ddlState,hdnSelectedStateID,txtZip,ddlRadius)
{
var hdnstate=document .getElementById (hdnSelectedStateID );
var zipcode=document .getElementById (txtZip );
if(hdnstate !=null) hdnstate .value="0";
    countryid=ddlCountry;
    stateid =ddlState;
    var countryList = document.getElementById (ddlCountry);
    var radius=document .getElementById (ddlRadius );
    var selectedCountry = countryList.options[countryList .selectedIndex].value;
    if(countryList.options[countryList .selectedIndex].text=='India')
    {
        if(zipcode .value=='PIN Code'|| zipcode .value=='Zip Code')
        {
            zipcode .title='PIN Code';
            zipcode .value='PIN Code';
            radius .disabled=true ;
        }    
    }
    else 
    {
        if(zipcode .value=='PIN Code'|| zipcode .value=='Zip Code')
        {
            zipcode .title='Zip Code';
            zipcode .value='Zip Code';
            radius .disabled=true ;
        }    
    }
    var requestUrl = AjaxServerPageName + "?SelectedCountry=" + encodeURIComponent(selectedCountry);
	CreateXmlHttp();
	if(XmlHttp)
	{
		XmlHttp.onreadystatechange = HandleResponse;
		XmlHttp.open("GET", requestUrl,  true);
		XmlHttp.send(null);		
	}
}
function Country_OnChange(ddlCountry,ddlState,hdnSelectedStateID)
{	
    var hdnstate=document .getElementById (hdnSelectedStateID );
    if(hdnstate !=null) hdnstate .value="0";
        countryid=ddlCountry;
    stateid =ddlState;

    var countryList = document.getElementById (ddlCountry);
    var stateList=document.getElementById (ddlState);
    var selectedCountry = countryList.options[countryList .selectedIndex].value;
    
        if( stateList.options.length>0)
      {
        $(stateList).select2('val', "")
      }
//     if( countryList.children('option').length>0)
//      {
//      
//        countryList.select2('data', null)
//	    for (var count = countryList.children('option').length-1; count >-1; count--)
//	    {
//            countryList.children('option').remove();
//	    }
//      }
    
    var requestUrl = AjaxServerPageName + "?SelectedCountry=" + encodeURIComponent(selectedCountry);
    CreateXmlHttp();
    if(XmlHttp)
    {
	    XmlHttp.onreadystatechange = HandleResponse;
	    XmlHttp.open("GET", requestUrl,  true);
	    XmlHttp.send(null);		
    }
}

//Gets called when country combo box selection changes
function CountryListOnChange(from) 
{

var countryid;
if(from =="editor")
{
countryid=editorcountryid;
stateid =editorstateid;
//alert ("hi");
//alert (stateid);
}

if (from =="requisition")
{

countryid =reqcountryid;
stateid =reqstateid;
}
if (from =="contact")
{
countryid=contactcompanyid;
stateid =contactstateid;
}
if(from =="search")
{
countryid =searchid;
stateid =searchstateid;
}
if(from =="resume")
{
countryid =resumecountryid;
stateid =resumestateid;
}
//"ctl00_ctl00_cphHomeMaster_cphCompanyMaster_ctlCompanyContact_ddlCountry"
	var countryList = document.getElementById (countryid);
	//Getting the selected country from country combo box.
var selectedCountry = countryList.options[countryList .selectedIndex].value;
	//alert (countryList.options[countryList.selectedIndex].value);
	//ClearAndSetStateListItems(selectedCountry );
	// URL to get states for a given country
//	var selectedCountry=2;


var requestUrl = AjaxServerPageName + "?SelectedCountry=" + encodeURIComponent(selectedCountry);
	CreateXmlHttp();
	
	// If browser supports XMLHTTPRequest object
	if(XmlHttp)
	{
		//Setting the event handler for the response
		XmlHttp.onreadystatechange = HandleResponse;
		
		//Initializes the request object with GET (METHOD of posting), 
		//Request URL and sets the request as asynchronous.
		XmlHttp.open("GET", requestUrl,  true);
		
		//Sends the request to server
		XmlHttp.send(null);		
	}
}


//Called when response comes back from server
function HandleResponse()
{
	// To make sure receiving response data from server is completed
	if(XmlHttp.readyState == 4)
	{
		// To make sure valid response is received from the server, 200 means response received is OK
		if(XmlHttp.status == 200)
		{			
			ClearAndSetStateListItems(XmlHttp.responseText);
		}
		else
		{
			alert("There was a problem retrieving data from the server." );
		}
	}
}

//Clears the contents of state combo box and adds the states of currently selected country
function ClearAndSetStateListItems(countryNode)
{
    var stateList = document.getElementById(stateid);
    var FistText=stateList.options[0].text;
   
  if( stateList.options.length>0)
  {
  	//Clears the state combo box contents.
	for (var count = stateList.options.length-1; count >-1; count--)
	{

		stateList.options[count] = null;
	}
   }
   
   $("#"+stateid).next().find('.chzn-results').html('');
   
  xmlDoc = $.parseXML( countryNode );
    var $xml = $( xmlDoc );
    var i=0;
    $xml.find( "state" ).each(function (){
        if( $(this).find('Name').text()!='')
        {
            optionItem = new Option( (i==0?FistText : $(this).find('Name').text() ), $(this).find('Id').text());
		    stateList.options[stateList.length] = optionItem;
//		   $("#"+stateid).next().remove();
//		    $("#"+stateid > option).remove();
//            $('.select2-chosen').remove()
//		   $(".chzn-select").chosen();
		   // $("#"+stateid).next().find('.chzn-results').append('<li id=\''+stateid + '_0_' +  $(this).find('Id').text() +'\' class=\'active-result\''+(i==0? ' result-selected highlighted': '')+' style>'+$(this).find('Name').text()+'</li>');
		    i++;
	    }
    });
    
	stateList.options[0].text=FistText ;
    
}

//Returns the node text value 
function GetInnerText (node)
{
	 return (node.textContent || node.innerText || node.text) ;
}



//--------------------------
function CheckPrimaryEmailAvailability(PrimaryEmail,MemberId,checkControl)
{
var divNotAvailable=document .getElementById ('divNotAvailable');
            divNotAvailable.style.display ="none";
 txtPrimaryEmail=PrimaryEmail ;
     var txtUserName= document.getElementById (PrimaryEmail);
     hdnCheckValidity =checkControl ;
     var UserName = txtUserName.value;
    
    if (UserName =="")
    {
     return "Please enter user name.";
    }
    if(echeck(UserName)==true )
    {

	        var requestUrl = CheckUserNameAjaxServerPage + "?SelectedUserId=" + encodeURIComponent(UserName)+ 

"&MemberId=" + encodeURIComponent (MemberId ) ;
	     
	        CreateXmlHttp();
	        // If browser supports XMLHTTPRequest object
	        if(XmlHttp)
	        {
		        //Setting the event handler for the response
		        XmlHttp.onreadystatechange = HandleResponsePrimaryEmail;
        		
		        //Initializes the request object with GET (METHOD of posting), 
		        //Request URL and sets the request as asynchronous.
		        XmlHttp.open("GET", requestUrl,  true);
        		
		        //Sends the request to server
		        XmlHttp.send(null);		
	        }
    }
 }



//Called when response comes back from server
function HandleResponsePrimaryEmail()
{


	// To make sure receiving response data from server is completed
	if(XmlHttp.readyState == 4)
	{
		// To make sure valid response is received from the server, 200 means response received is OK
		if(XmlHttp.status == 200)
		{
	       CheckPrimaryEmail(XmlHttp.responseXML.documentElement);
		}
		else
		{
			alert("There was a problem retrieving data from the server." );
		}
	}
}




function CheckPrimaryEmail(MemberName)
{
    //var ValidationControl = document.getElementById("lblAvailableuser");
     var divAvailableUser = document.getElementById("divAvailableUser");
     
    var ch=document .getElementById (hdnCheckValidity );
	var member = MemberName.getElementsByTagName('firstName');
	
	if(member.length>0)
	{
	      divNotAvailable.style.display="inline";
	      ch.value="false" ;
	}
	else
	{
	divNotAvailable.style.display="none";
	ch.value="true" ;
	}

          
}


//---------------------------------------------------------------------------



function CheckUserAvailability() {
    
    var divAvailableUser = document.getElementById("divAvailableUser");
    var divNotAvailable = document.getElementById("divNotAvailable");
    divAvailableUser.style.visible="none";
    divNotAvailable.style.visible="none";

    //   try{     var ValidationControl = document.getElementById("lblAvailableuser"); ValidationControl.innerText ="";}
    //   catch (e){}
    var userid;
    var sPath = window.location.pathname;

    if (sPath.indexOf('RegistrationInfoEditor.aspx') > 0 || sPath.indexOf('CandidateRegistration.aspx') > 0) { //added CandidateRegistration by pravin khot on 19/May/2016
       
        userid = NewCandidateUsername;
       
    }
    else if(sPath.indexOf('BasicInfoEditor.aspx')>0){       
        userid='ctl00_cphHomeMasterTitle_reqEditor_txtContactEmail';
    }
    var txtUserName = document.getElementById(userid);
    var UserName = txtUserName.value;    
    if (UserName =="")
    {
        return "Please enter user name.";
    }
  
    if(echeck(UserName)==true )
    {
        var requestUrl = CheckUserNameAjaxServerPage + "?SelectedUserId=" + encodeURIComponent(UserName);
        
        CreateXmlHttp();
        // If browser supports XMLHTTPRequest object
        if(XmlHttp) {
                     //Setting the event handler for the response
            XmlHttp.onreadystatechange = HandleResponseUserName;        	
            //Initializes the request object with GET (METHOD of posting), 
            //Request URL and sets the request as asynchronous.
            XmlHttp.open("GET", requestUrl,  true);        	
            //Sends the request to server
            XmlHttp.send(null);		
        }
    }
}


function echeck(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   return false
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   return false
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    return false
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    return false
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    return false
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    return false
		 }
		
		 if (str.indexOf(" ")!=-1){
		    return false
		 }

 		 return true					
	}





//Called when response comes back from server
function HandleResponseUserName()
{


	// To make sure receiving response data from server is completed
	if(XmlHttp.readyState == 4)
	{
		// To make sure valid response is received from the server, 200 means response received is OK
		if(XmlHttp.status == 200)
		{
		    ShowCheckUser(XmlHttp.responseXML.documentElement);
		   
		}
		else
		{
			alert("There was a problem retrieving data from the server." );
		}
	}
}

function ShowCheckUser(MemberName)
{
    
    var ValidationControl = document.getElementById("lblAvailableuser");
    var divAvailableUser = document.getElementById("divAvailableUser");
    var divNotAvailable = document.getElementById("divNotAvailable");
    var member = MemberName.getElementsByTagName('firstName');
   
	if(member.length>0)
	{
	   
	    var sPath = window.location.pathname;
        var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);
        if (sPage == 'ForgotPassword.aspx') {
            ValidationControl.innerText = "";
        }

        else if (sPath.indexOf('RegistrationInfoEditor.aspx') > 0) {
            divAvailableUser.style.display = "none";
            divNotAvailable.style.display = "inline";
            var textBox = document.getElementById("ctl00_cphHomeMaster_rgInternal_txtUserName");
            textBox.value = "";
            textBox.focus();
        }
        //***********Code added by pravin khot on 19/May/2016**********
        else if (sPath.indexOf('CandidateRegistration.aspx') > 0) {
            divAvailableUser.style.display = "none";
            divNotAvailable.style.display = "inline";
            var textBox = document.getElementById("ctl00_cphHomeMaster_rgInternal_txtUserName");
            textBox.value = "";
            textBox.focus();
        }
        //***********************END***************************
        else if (sPath.indexOf('BasicInfoEditor.aspx') > 0) {
            divAvailableUser.style.display = "none";
            divNotAvailable.style.display = "inline";
            var textBox = document.getElementById("ctl00_cphHomeMasterTitle_reqEditor_txtContactEmail");
            textBox.value = "";
            textBox.focus();

        }
        else {

        }
	}
    else
	    {
	        
	        var sPath = window.location.pathname;
            var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);           
            if(sPage =='ForgotPassword.aspx')
            {
            
                ValidationControl.innerText ="User Name is incorrect";
	            ValidationControl .style .color = "red";
	            var userid;
                userid=portalUsername;
	            var textBox=document .getElementById ("ctl00_cphHomeMaster_txtUserName");
	            textBox.value="";
	            textBox.focus();
	            
            } 
             else if(sPath.indexOf('BasicInfoEditor.aspx')>=0)
            {                       
	           divNotAvailable.style.display="none";
	           divAvailableUser.style.display="none";
	        }	       
           
            else if(sPath.indexOf('RegistrationInfoEditor.aspx')>=0)
            {                
                divAvailableUser.style.display="inline";
                divNotAvailable.style.display="none";
            }
          
            //************Code added by pravin khot on 19/May/2016**************
	         else if(sPath.indexOf('CandidateRegistration.aspx')>=0)
            {                
                divAvailableUser.style.display="inline";
                divNotAvailable.style.display="none";
	        }
	        //***********************END**************************    
            else{
            
            }
        }
        
        
          //Function introduced by Prasanth on 10/Jun/2016 
        function CheckADUserAvailability(ADUserName,MemberId,checkControl)
{
        var divNotAvailable=document .getElementById ('divNotAvailableUserName');
        divNotAvailable.style.display ="none";
        txtPrimaryEmail=ADUserName ;
        var txtUserName= document.getElementById (ADUserName);
        hdnCheckValidity =checkControl ;
        var UserName = txtUserName.value;
        
        if (UserName =="")
        {
         return "Please enter user name.";
        }
        if(echeck(UserName)==true )
        {

	            var requestUrl = CheckADUserNameAjaxServerPage + "?SelectedUserId=" + encodeURIComponent(UserName)+ "&MemberId=" + encodeURIComponent (MemberId ) ;
    	     
	            CreateXmlHttp();
	            // If browser supports XMLHTTPRequest object
	            if(XmlHttp)
	            {
		            //Setting the event handler for the response
		            XmlHttp.onreadystatechange = HandleResponseADUserName;
            		
		            //Initializes the request object with GET (METHOD of posting), 
		            //Request URL and sets the request as asynchronous.
		            XmlHttp.open("GET", requestUrl,  true);
            		
		            //Sends the request to server
		            XmlHttp.send(null);		
	            }
        }
 }
         
}
