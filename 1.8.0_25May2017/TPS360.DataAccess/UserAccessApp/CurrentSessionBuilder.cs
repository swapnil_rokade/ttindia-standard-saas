﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CurrentSessionsBuilder : IEntityBuilder<CurrentSessions >
    {
        IList<CurrentSessions > IEntityBuilder<CurrentSessions >.BuildEntities(IDataReader reader)
        {
            List<CurrentSessions > _currentSessions = new List<CurrentSessions >();

            while (reader.Read())
            {
                _currentSessions.Add(((IEntityBuilder<CurrentSessions >)this).BuildEntity(reader));
            }

            return (_currentSessions.Count > 0) ? _currentSessions  : null;
        }


        public IList<CurrentSessions> BuildCurrentSessionEntities(IDataReader reader)
        {
            List<CurrentSessions> _currentSessions = new List<CurrentSessions>();

            while (reader.Read())
            {
                _currentSessions.Add(((IEntityBuilder<CurrentSessions>)this).BuildEntity(reader));
            }

            return (_currentSessions.Count > 0) ? _currentSessions : null;
        }


        CurrentSessions  IEntityBuilder<CurrentSessions >.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_EMPLOYEENAME = 1;
            const int FLD_SESSIONTYPE = 2;
            const int FLD_LOGGEDIN = 3;

            CurrentSessions  currentSessions = new CurrentSessions ();

            currentSessions.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            currentSessions.EmployeeName = reader.IsDBNull(FLD_EMPLOYEENAME) ? string.Empty : reader.GetString(FLD_EMPLOYEENAME);
            currentSessions.ApplicationName = reader.IsDBNull(FLD_SESSIONTYPE) ? string.Empty : reader.GetString(FLD_SESSIONTYPE);
            currentSessions.LoggedIn = reader.IsDBNull(FLD_LOGGEDIN) ? DateTime .MinValue  : reader.GetDateTime (FLD_LOGGEDIN);

            return currentSessions;
        }
    }
}