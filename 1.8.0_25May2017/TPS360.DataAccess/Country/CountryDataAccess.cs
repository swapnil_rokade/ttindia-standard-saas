﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:CountryDataAccess.cs
    Description: This page is used for Countries  functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetCountryIdByCountryCode() to assist 
                                                                Resume parser team
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class CountryDataAccess : BaseDataAccess, ICountryDataAccess
    {
        #region Constructors

        public CountryDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<Country> CreateEntityBuilder<Country>()
        {
            return (new CountryBuilder()) as IEntityBuilder<Country>;
        }

        #endregion

        #region  Methods

        Country ICountryDataAccess.Add(Country country)
        {
            const string SP = "dbo.Country_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, StringHelper.Convert(country.Name));
                Database.AddInParameter(cmd, "@CountryCode", DbType.AnsiString, StringHelper.Convert(country.CountryCode));

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        country = CreateEntityBuilder<Country>().BuildEntity(reader);
                    }
                    else
                    {
                        country = null;
                    }
                }

                if (country == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Country already exists. Please specify another country.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this country.");
                            }
                    }
                }

                return country;
            }
        }

        Country ICountryDataAccess.Update(Country country)
        {
            const string SP = "dbo.Country_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, country.Id);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, StringHelper.Convert(country.Name));
                Database.AddInParameter(cmd, "@CountryCode", DbType.AnsiString, StringHelper.Convert(country.CountryCode));

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        country = CreateEntityBuilder<Country>().BuildEntity(reader);
                    }
                    else
                    {
                        country = null;
                    }
                }

                if (country == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Country already exists. Please specify another country.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this country.");
                            }
                    }
                }

                return country;
            }
        }

        Country ICountryDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Country_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Country>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<Country> ICountryDataAccess.GetAll()
        {
            const string SP = "dbo.Country_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Country>().BuildEntities(reader);
                }
            }
        }

        bool ICountryDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Country_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a country which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this country.");
                        }
                }
            }
        }

        Int32 ICountryDataAccess.GetCountryIdByCountryName(string countryName)
        {
            if (countryName==null)
            {
                countryName="";
            }

            const string SP = "dbo.Country_GetCountryIdByCountryName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CountryName", DbType.String, countryName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 countryId = 0;

                    while (reader.Read())
                    {
                        countryId = reader.GetInt32(0);
                    }

                    return countryId;
                }
            }
        }

        //0.1 starts here
        Int32 ICountryDataAccess.GetStateIdByStateCode(string StateCode)
        {
            if (StateCode == null)
            {
                StateCode = "";
            }

            const string SP = "dbo.State_GetSateIdByStateCode";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@StateCode", DbType.String, StateCode);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 stateId = 0;

                    while (reader.Read())
                    {
                        stateId = reader.GetInt32(0);
                    }

                    return stateId;
                }
            }
        }

        Int32 ICountryDataAccess.GetStateIdByStateCodeAndCountryId(string StateCode,int CountryId)
        {
            if (StateCode == null)
            {
                StateCode = "";
            }

            const string SP = "dbo.State_GetSateIdByStateCodeAndCountryId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@StateCode", DbType.String, StateCode);
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, CountryId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 stateId = 0;

                    while (reader.Read())
                    {
                        stateId = reader.GetInt32(0);
                    }

                    return stateId;
                }
            }
        }

        Int32 ICountryDataAccess.GetStateIdByStateNameAndCountryId(string Name, int CountryId)
        {
            if (Name == null)
            {
                Name = "";
            }

            const string SP = "dbo.State_GetStateIdByStateNameAndCountryId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Name", DbType.String, Name);
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, CountryId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 stateId = 0;

                    while (reader.Read())
                    {
                        stateId = reader.GetInt32(0);
                    }

                    return stateId;
                }
            }
        }

        Int32 ICountryDataAccess.GetCountryIdByCountryCode(string CountryCode)
        {
            if (CountryCode == null)
            {
                CountryCode = "";
            }

            const string SP = "dbo.Country_GetCountryIdByCountryCode";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CountryCode", DbType.String, CountryCode);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 countryId = 0;

                    while (reader.Read())
                    {
                        countryId = reader.GetInt32(0);
                    }

                    return countryId;
                }
            }
        }

        string ICountryDataAccess.GetCountryNameById(int CountryId)
        {
            if (CountryId < 1)
            {
                throw new ArgumentNullException("CountryId");
            }

            const string SP = "dbo.Country_GetCountryNameById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, CountryId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                    return string.Empty;
                }
            }
        }
        //0.1 end here
        #endregion
    }
}