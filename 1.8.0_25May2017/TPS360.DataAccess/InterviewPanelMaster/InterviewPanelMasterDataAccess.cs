﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewPanelMasterDataAccess.cs
    Description         :   This page is used Call the Stored Proceedures for Interview Panel InterviewPanelMaster.
    Created By          :   Pravin
    Created On          :   20/Nov/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;
using System.Text;
using TPS360.Common.BusinessEntities;
namespace TPS360.DataAccess
{
    internal sealed class InterviewPanelMasterDataAccess : BaseDataAccess, IInterviewPanelMasterDataAccess
    {
         #region Constructors

        public InterviewPanelMasterDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<InterviewPanelMaster> CreateEntityBuilder<InterviewPanelMaster>()
        {
            return (new InterviewPanelMasterBuilder()) as IEntityBuilder<InterviewPanelMaster>;
        }

        #endregion

        #region  Methods




        InterviewPanelMaster IInterviewPanelMasterDataAccess.Add(InterviewPanelMaster InterviewPanelMaster) 
        {
            const string SP = "dbo.InterviewPanelMaster_Create";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@InterviewPanel_Name", DbType.String, InterviewPanelMaster.InterviewPanel_Name);
                Database.AddInParameter(cmd, "@InterviewSkill", DbType.String, InterviewPanelMaster.InterviewSkill);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, InterviewPanelMaster.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewPanelMaster.UpdatorId);
              
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        InterviewPanelMaster = CreateEntityBuilder<InterviewPanelMaster>().BuildEntity(reader);
                    }
                    else
                    {
                        InterviewPanelMaster = null;
                    }
                }

                if (InterviewPanelMaster == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);
                    //int returnCode = 2003;

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Interview Panel Master already exists. Please specify another Interview Panel Master.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this Interview Panel Master.");
                            }
                    }
                }

                return InterviewPanelMaster;

             
            }
        
        }

        InterviewPanelMaster IInterviewPanelMasterDataAccess.Update(InterviewPanelMaster InterviewPanelMaster)
        {
            const string SP = "dbo.InterviewPanelMaster_Update";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {

                Database.AddInParameter(cmd, "@InterviewPanelType_Id", DbType.Int32, InterviewPanelMaster.InterviewPanel_Id);
                Database.AddInParameter(cmd, "@InterviewPanel_Name", DbType.String, InterviewPanelMaster.InterviewPanel_Name);
                Database.AddInParameter(cmd, "@InterviewSkill", DbType.String, InterviewPanelMaster.InterviewSkill);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, InterviewPanelMaster.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewPanelMaster.UpdatorId);
             
                //Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewPanelMaster.InterviewId);
                Database.ExecuteReader(cmd);

                //Database.ExecuteReader(cmd);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        InterviewPanelMaster = CreateEntityBuilder<InterviewPanelMaster>().BuildEntity(reader);
                    }
                    else
                    {
                        InterviewPanelMaster = null;
                    }
                }

                if (InterviewPanelMaster == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Interview Panel Form already exists. Please specify another Interview Panel.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this commonNote.");
                            }
                    }
                }

                return InterviewPanelMaster;
            }
        }



        IList<InterviewPanelMaster> IInterviewPanelMasterDataAccess.GetAll()
        {
            const string SP = "dbo.InterviewPanelMaster_GetAll";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<InterviewPanelMaster>().BuildEntities(reader);
                }
            }

        }

        string IInterviewPanelMasterDataAccess.GetSkills(int InterviewPanelMaster_ById)
        {
            const string SP = "dbo.InterviewPanelMaster_GetSkills";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "Ids", DbType.AnsiString, InterviewPanelMaster_ById);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
            }
        }

        IList<InterviewPanelMaster> IInterviewPanelMasterDataAccess.GetAll(string SortExpression)
        {
            const string SP = "dbo.InterviewPanel_GetAllWithSort";
            //const string SP = "dbo.QuestionBank_GetAllWithSort";
            
            

            string SortColumn = "[Q].[CreateDate]";
            string SortOrder = "desc";
            string[] part = (string.IsNullOrEmpty(SortExpression)) ? null : SortExpression.Split(' ');

            if (part != null && part.Length > 0)
            {
                SortColumn = part[0];

                if (part.Length > 1 && part[1] != null)
                {
                    SortOrder = part[1];
                }
                else
                {
                    SortOrder = "asc";
                }
            }

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
               
                Database.AddInParameter(cmd, "@SortColumn", DbType.String, SortColumn);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<InterviewPanelMaster>().BuildEntities(reader);
                }
            }
        }

        bool IInterviewPanelMasterDataAccess.DeleteById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.InterviewPanel_DeleteById";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                Database.ExecuteNonQuery(cmd);
                return true;
                //int returnCode = GetReturnCodeFromParameter(cmd);
                //int returnCode = Database.ExecuteNonQuery(cmd);



                //switch (returnCode)
                //{
                //    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                //        {
                //            return true;
                //        }
                //    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                //        {
                //            throw new ArgumentException("Cannot delete a Assessment Form which has association.");
                //        }
                //    default:
                //        {
                //            throw new SystemException("An unexpected error has occurred while deleting this Assessment Form.");
                //        }
                //}
            }

        }

        Int32  IInterviewPanelMasterDataAccess.InterviewPanelMaster_Id(string iname)
        {
            const string SP = "dbo.InterviewPanelMaster_Id";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@iname", DbType.String, iname);
                 using (IDataReader reader = Database.ExecuteReader(cmd))
                   if (reader.Read())
                    {
                        return reader.IsDBNull(0) ?  0 : reader.GetInt32(0);
                    }
                    else
                    {
                        return 0;
                    }
            }
        }
        Int32 IInterviewPanelMasterDataAccess.InterviewPanelMaster_Id(string iname,int editid)
        {
            const string SP = "dbo.InterviewPanelMaster_EditId";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@iname", DbType.String, iname);
                Database.AddInParameter(cmd, "@editid", DbType.Int32, editid);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                    if (reader.Read())
                    {
                        return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                    else
                    {
                        return 0;
                    }
            }
        }

        InterviewPanelMaster IInterviewPanelMasterDataAccess.GetById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.InterviewPanel_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<InterviewPanelMaster>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

        }
       
       
        //Check this Functionality Later 0123456789
        PagedResponse<InterviewPanelMaster> IInterviewPanelMasterDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.InterviewerFeedback_GetPaged";
            
            string whereClause = string.Empty;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<InterviewPanelMaster> response = new PagedResponse<InterviewPanelMaster>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<InterviewPanelMaster>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        #endregion
    }
}
