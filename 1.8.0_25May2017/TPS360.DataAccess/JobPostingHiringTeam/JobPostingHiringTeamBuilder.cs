﻿using System;
using System.Data;
using System.Collections.Generic;


using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingHiringTeamBuilder : IEntityBuilder<JobPostingHiringTeam>
    {
        IList<JobPostingHiringTeam> IEntityBuilder<JobPostingHiringTeam>.BuildEntities(IDataReader reader)
        {
            List<JobPostingHiringTeam> jobPostingHiringTeams = new List<JobPostingHiringTeam>();

            while (reader.Read())
            {
                jobPostingHiringTeams.Add(((IEntityBuilder<JobPostingHiringTeam>)this).BuildEntity(reader));
            }

            return (jobPostingHiringTeams.Count > 0) ? jobPostingHiringTeams : null;
        }

        JobPostingHiringTeam IEntityBuilder<JobPostingHiringTeam>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_EMPLOYEETYPE = 1;
            const int FLD_JOBPOSTINGID = 2;            
            const int FLD_MEMBERID = 3;
            const int FLD_CREATORID = 4;
            const int FLD_UPDATORID = 5;
            const int FLD_CREATEDATE = 6;
            const int FLD_UPDATEDATE = 7;

            const int FLD_NAME = 8;
            const int FLD_PRIMARYEMAIL = 9;
            const int FLD_NAMEPRIMARYEMAIL = 10;
            JobPostingHiringTeam jobPostingHiringTeam = new JobPostingHiringTeam();

            if (reader.FieldCount == 7)
            {
                const int FLD_NoOfOenings = 4;
                const int FLD_RecruiterGroupName = 5;
                const int FLD_IsPRIMARY = 6;
     
                jobPostingHiringTeam.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                jobPostingHiringTeam.EmployeeType = reader.IsDBNull(FLD_EMPLOYEETYPE) ? string.Empty : reader.GetString(FLD_EMPLOYEETYPE);
                jobPostingHiringTeam.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
                jobPostingHiringTeam.RecruiterGroupId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
                jobPostingHiringTeam.NoOfOenings = reader.IsDBNull(FLD_NoOfOenings) ? 0 : reader.GetInt32(FLD_NoOfOenings);
                jobPostingHiringTeam.RecruiterGroupName = reader.IsDBNull(FLD_RecruiterGroupName) ? string.Empty : reader.GetString(FLD_RecruiterGroupName);
                jobPostingHiringTeam.IsPRIMARY = reader.IsDBNull(FLD_IsPRIMARY) ? false : reader.GetBoolean(FLD_IsPRIMARY);
            }
            else
            {
                jobPostingHiringTeam.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                jobPostingHiringTeam.EmployeeType = reader.IsDBNull(FLD_EMPLOYEETYPE) ? string.Empty : reader.GetString(FLD_EMPLOYEETYPE);
                jobPostingHiringTeam.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
                jobPostingHiringTeam.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
                jobPostingHiringTeam.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                jobPostingHiringTeam.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                jobPostingHiringTeam.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                jobPostingHiringTeam.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

                if (reader.FieldCount > 8)
                {
                    try
                    {
                        jobPostingHiringTeam.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
                        jobPostingHiringTeam.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
                        jobPostingHiringTeam.NamePrimaryEmail = reader.IsDBNull(FLD_NAMEPRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_NAMEPRIMARYEMAIL);
                    }
                    catch { }
                }
            }
            return jobPostingHiringTeam;
        }      
    }
}