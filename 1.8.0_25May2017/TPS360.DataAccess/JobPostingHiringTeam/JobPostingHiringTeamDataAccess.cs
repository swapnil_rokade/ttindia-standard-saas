﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingHiringTeamDataAccess : BaseDataAccess, IJobPostingHiringTeamDataAccess
    {
        #region Constructors

        public JobPostingHiringTeamDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<JobPostingHiringTeam> CreateEntityBuilder<JobPostingHiringTeam>()
        {
            return (new JobPostingHiringTeamBuilder()) as IEntityBuilder<JobPostingHiringTeam>;
        }

        #endregion

        #region  Methods

        void  IJobPostingHiringTeamDataAccess.AddMultipleRecruiters(string MemberIds,int JobpostingId,int CreatorId,string employeeType)
        {
            const string SP = "dbo.JobPostingHiringTeam_CreateMultipleRecruiters";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@EmployeeType", DbType.AnsiString,employeeType );
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobpostingId );
                Database.AddInParameter(cmd, "@MemberId", DbType.AnsiString , MemberIds );
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32,CreatorId );
                Database.ExecuteNonQuery(cmd);
            }
        }
        //Added by pravin khot on 2/March/2017
        void IJobPostingHiringTeamDataAccess.AddMultipleRecruitersWithOpening(int MemberId, int AssignOpenings, int JobpostingId, int CreatorId, string employeeType, bool IsPrimary)
        {
            const string SP = "dbo.JobPostingHiringTeam_CreateMultipleRecruitersWithOpening";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@EmployeeType", DbType.AnsiString, employeeType);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobpostingId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                Database.AddInParameter(cmd, "@AssignOpenings", DbType.Int32, AssignOpenings);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, CreatorId);
                Database.AddInParameter(cmd, "@IsPrimary", DbType.Boolean, IsPrimary);
                Database.ExecuteNonQuery(cmd);
            }
        }
        IList<JobPostingHiringTeam> IJobPostingHiringTeamDataAccess.GetAllRecruiterGroupByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "dbo.JobPostingRecruiterGroup_GetAllByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                PagedResponse<JobPostingHiringTeam> response = new PagedResponse<JobPostingHiringTeam>();
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingHiringTeam>().BuildEntities(reader);
                }
              
            }
        }
        //******************END*************************

        JobPostingHiringTeam IJobPostingHiringTeamDataAccess.Add(JobPostingHiringTeam jobPostingHiringTeam)
        {
            const string SP = "dbo.JobPostingHiringTeam_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@EmployeeType", DbType.AnsiString, StringHelper.Convert(jobPostingHiringTeam.EmployeeType));
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingHiringTeam.JobPostingId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, jobPostingHiringTeam.MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, jobPostingHiringTeam.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPostingHiringTeam = CreateEntityBuilder<JobPostingHiringTeam>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPostingHiringTeam = null;
                    }
                }

                if (jobPostingHiringTeam == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting hiring team already exists. Please specify another job posting hiring team.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this job posting hiring team.");
                            }
                    }
                }

                return jobPostingHiringTeam;
            }
        }

        JobPostingHiringTeam IJobPostingHiringTeamDataAccess.Update(JobPostingHiringTeam jobPostingHiringTeam)
        {
            const string SP = "dbo.JobPostingHiringTeam_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, jobPostingHiringTeam.Id);
                Database.AddInParameter(cmd, "@EmployeeType", DbType.AnsiString, StringHelper.Convert(jobPostingHiringTeam.EmployeeType));
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, jobPostingHiringTeam.MemberId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, jobPostingHiringTeam.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPostingHiringTeam = CreateEntityBuilder<JobPostingHiringTeam>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPostingHiringTeam = null;
                    }
                }

                if (jobPostingHiringTeam == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting hiring team already exists. Please specify another job posting hiring team.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this job posting hiring team.");
                            }
                    }
                }

                return jobPostingHiringTeam;
            }
        }

        JobPostingHiringTeam IJobPostingHiringTeamDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingHiringTeam_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingHiringTeam>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        JobPostingHiringTeam IJobPostingHiringTeamDataAccess.GetByMemberId(int memberId, int jobPostingId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            if (jobPostingId < 1)
            {
                throw new ArgumentException("jobPostingId");
            }

            const string SP = "dbo.JobPostingHiringTeam_GetJobPostingHiringTeamByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingHiringTeam>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<JobPostingHiringTeam> IJobPostingHiringTeamDataAccess.GetAll()
        {
            const string SP = "dbo.JobPostingHiringTeam_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingHiringTeam>().BuildEntities(reader);
                }
            }
        }

        IList<JobPostingHiringTeam> IJobPostingHiringTeamDataAccess.GetAllByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }
            
            const string SP = "dbo.JobPostingHiringTeam_GetAllByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingHiringTeam>().BuildEntities(reader);
                }
            }
        }





        bool IJobPostingHiringTeamDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingHiringTeam_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting hiring team which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this job posting hiring team.");
                        }
                }
            }
        }

        bool IJobPostingHiringTeamDataAccess.DeleteByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "dbo.JobPostingHiringTeam_DeleteJobPostingHiringTeamByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting hiring team which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this job posting hiring team.");
                        }
                }
            }
        }

        IList<JobPostingHiringTeam> IJobPostingHiringTeamDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.JobPostingHiringTeam_GetAllJobPostingHiringTeamByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingHiringTeam>().BuildEntities(reader);
                }
            }
        }

        #endregion
    }
}