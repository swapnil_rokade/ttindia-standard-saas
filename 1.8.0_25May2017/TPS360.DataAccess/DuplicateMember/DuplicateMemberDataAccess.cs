﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: DuplicateMemberDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author                      Modification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              Sept-10-2009           Sukanta Ghorui            Added GetDuplicateRecords
 ---------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Configuration;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Collections.Specialized;
using System.Web;
using System.Data.SqlTypes;
using TPS360.Common;

namespace TPS360.DataAccess
{
    internal sealed class DuplicateMemberDataAccess : BaseDataAccess, IDuplicateMemberDataAccess 
    {
        #region Constructors

        public DuplicateMemberDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<DuplicateMember> CreateEntityBuilder<DuplicateMember>()
        {
            return (new DuplicateMemberBuilder()) as IEntityBuilder<DuplicateMember>;
        }

        #endregion

        #region  Methods

        IList<DuplicateMember> IDuplicateMemberDataAccess.GetDuplicateRecords(bool checkFirstName, string strFirstName,
            bool checkMiddleName, string strMiddleName,
            bool checkLastName, string strLastName,
            bool checkEmailId, string strEmailId,
            bool checkDOB, DateTime? DOB,
            bool checkSSN, string strSSN)
        {
            const string SP = "dbo.Member_GetDuplicates";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@IsFNameExists", DbType.Boolean, checkFirstName);
                Database.AddInParameter(cmd, "@FName", DbType.String, StringHelper.Convert(strFirstName));

                Database.AddInParameter(cmd, "@IsMNameExists", DbType.Boolean, checkMiddleName);
                Database.AddInParameter(cmd, "@MName", DbType.String, StringHelper.Convert(strMiddleName));

                Database.AddInParameter(cmd, "@IsLNameExists", DbType.Boolean, checkLastName);
                Database.AddInParameter(cmd, "@LName", DbType.String, StringHelper.Convert(strLastName));

                Database.AddInParameter(cmd, "@IsEmailIdExists", DbType.Boolean, checkEmailId);
                Database.AddInParameter(cmd, "@EmailId", DbType.String, StringHelper.Convert(strEmailId));

                Database.AddInParameter(cmd, "@IsDOBExists", DbType.Boolean, checkDOB);
                Database.AddInParameter(cmd, "@DOB", DbType.DateTime, DOB);

                Database.AddInParameter(cmd, "@IsSSNExists", DbType.Boolean, checkSSN);
                Database.AddInParameter(cmd, "@SSN", DbType.String, StringHelper.Convert(strSSN));

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<DuplicateMember>().BuildEntities(reader);
                }
            }
        }


        #endregion
    }
}
