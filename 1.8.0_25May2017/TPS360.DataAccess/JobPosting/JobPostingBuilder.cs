﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: JobPostingBuilder.cs
    Description: This page is used for Job posting functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Nov-13-2008           Yogeesh Bhat           Defect ID:9180; Added new entity IsExpensesPaid
    0.2            Mar-06-2009           Yogeesh Bhat           Defect Id: 10049: Added new entity EmailSubject and RequisitionSource
    0.3            Mar-10-2009           Yogeesh Bhat           Handled exception with new column "jobPosting.EmailSubject,jobPosting.RequisitionSource".
    0.4            22/May/2015           Prasanth Kumar G       Introduced RequisitionAgingBuildPagedEntity
    0.5            11/Jun/2015           Prasanth Kumar G       Introduced ActivatedDate
 *  0.6            15/Jan/2016           Pravin khot            add if condition (reader.FieldCount == 6)
 *  0.7            2/Feb/2016            Pravin khot            Introduced RequisitionSourceBreakupBuildPagedEntities
 *  0.8            22/June/2016          pravin khot            added- FLD_UPDATEDATE

-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Data;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingBuilder : IEntityBuilder<JobPosting>
    {
        IList<JobPosting> IEntityBuilder<JobPosting>.BuildEntities(IDataReader reader)
        {
            List<JobPosting> jobPostings = new List<JobPosting>();

            while (reader.Read())
            {
                jobPostings.Add(((IEntityBuilder<JobPosting>)this).BuildEntity(reader));
            }

            return (jobPostings.Count > 0) ? jobPostings : null;
        }

        //Code introduced by Pravin khot on 2/Feb/2016 Start
        public IList<JobPosting> RequisitionSourceBreakupBuildPagedEntities(IDataReader reader)
        {
            List<JobPosting> jobPostings = new List<JobPosting>();

            while (reader.Read())
            {
                jobPostings.Add(RequisitionSourceBreakupBuildPagedEntity(reader));
            }

            return (jobPostings.Count > 0) ? jobPostings : null;
        }

        //*************************END****************************



        public IList<JobPosting> BuildPagedEntities(IDataReader reader)
        {
            List<JobPosting> jobPostings = new List<JobPosting>();

            while (reader.Read())
            {
                jobPostings.Add(BuildPagedEntity(reader));
            }

            return (jobPostings.Count > 0) ? jobPostings : null;
        }

        public IList<JobPosting> BuildEntitiesForCandidatePortal(IDataReader reader)
        {
            List<JobPosting> jobPostings = new List<JobPosting>();

            while (reader.Read())
            {
                jobPostings.Add(BuildEntityForCandidatePortal(reader));
            }

            return (jobPostings.Count > 0) ? jobPostings : null;
        }
        public IList<JobPosting> BuildEntitiesForVendorPortalForMyPerformance(IDataReader reader)
        {
            List<JobPosting> jobPostings = new List<JobPosting>();

            while (reader.Read())
            {
                jobPostings.Add(BuildEntityForVendorPortalForMyPerformance(reader));
            }

            return (jobPostings.Count > 0) ? jobPostings : null;
        }
        //Code introduced by Prasanth on 22/May/2015 Start
        public IList<JobPosting> RequisitionAgingBuildPagedEntities(IDataReader reader)
        {
            List<JobPosting> jobPostings = new List<JobPosting>();

            while (reader.Read())
            {
                jobPostings.Add(RequisitionAgingBuildPagedEntity(reader));
            }

            return (jobPostings.Count > 0) ? jobPostings : null;
        }

        //*************************END****************************

        JobPosting BuildEntityForVendorPortalForMyPerformance(IDataReader reader)
        {
            JobPosting jobPosting = new JobPosting();
            const int FLD_ID = 0;
            const int FLD_JOBTITLE = 1;
            const int FLD_JOBPOSTINGCODE = 2;   
            const int FLD_POSTEDDATE = 3;
            const int FLD_NoofSubmissions = 4;
            const int FLD_WIP = 5;
            const int FLD_Rejected = 6;
            const int FLD_Joined = 7;

            jobPosting.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            jobPosting.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            jobPosting.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
             jobPosting.PostedDate = reader.IsDBNull(FLD_POSTEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_POSTEDDATE);
             jobPosting.NoofSubmissions = reader.IsDBNull(FLD_NoofSubmissions) ? 0 : reader.GetInt32(FLD_NoofSubmissions);
             jobPosting.WIP = reader.IsDBNull(FLD_WIP) ? 0 : reader.GetInt32(FLD_WIP);
             jobPosting.Rejected = reader.IsDBNull(FLD_Rejected) ? 0 : reader.GetInt32(FLD_Rejected);
             jobPosting.Joined = reader.IsDBNull(FLD_Joined) ? 0 : reader.GetInt32(FLD_Joined);
            return jobPosting;
        }

        JobPosting BuildEntityForCandidatePortal(IDataReader reader)
        {
            JobPosting jobPosting = new JobPosting();
            const int FLD_ID = 0;
            const int FLD_JOBTITLE = 1;
            const int FLD_JOBPOSTINGCODE = 2;
            const int FLD_CITY = 3;
            const int FLD_STATEID = 4;
            const int FLD_POSTEDDATE = 5;
            const int FLD_ISAPPLIED = 6;
            const int FLD_CREATORID = 7;
            const int FLD_CREATORNAME = 8;

            jobPosting.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            jobPosting.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            jobPosting.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
            jobPosting.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
            jobPosting.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
            jobPosting.PostedDate = reader.IsDBNull(FLD_POSTEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_POSTEDDATE);
            jobPosting.IsApplied = reader.IsDBNull(FLD_ISAPPLIED) ? false : reader.GetBoolean(FLD_ISAPPLIED);
            if (reader.FieldCount > FLD_CREATORID)
            {
                jobPosting.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                jobPosting.CreatorName = reader.IsDBNull(FLD_CREATORNAME) ? string.Empty : reader.GetString(FLD_CREATORNAME);
            }
            return jobPosting;

        }
        //Code introduced by Pravin khot on 2/Feb/2015 Start
        JobPosting RequisitionSourceBreakupBuildPagedEntity(IDataReader reader)
        {
            JobPosting jobPosting = new JobPosting();
            const int FLD_JOBPOSTID = 0;
            const int FLD_JOBTITLE = 1;
            const int FLD_JOBPOSTINGCODE = 2;
            const int FLD_OPENDATE = 3;
            const int FLD_NOOFOPENING = 4;
            const int FLD_SUBMISSIONCOUNT = 5;


            jobPosting.Id = reader.IsDBNull(FLD_JOBPOSTID) ? 0 : reader.GetInt32(FLD_JOBPOSTID);
            jobPosting.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            jobPosting.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
            jobPosting.OpenDate = reader.IsDBNull(FLD_OPENDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_OPENDATE);
            jobPosting.NoOfOpenings = reader.IsDBNull(FLD_NOOFOPENING) ? 0 : reader.GetInt32(FLD_NOOFOPENING);
            jobPosting.SubmissionCount = reader.IsDBNull(FLD_SUBMISSIONCOUNT) ? 0 : reader.GetInt32(FLD_SUBMISSIONCOUNT);

            jobPosting.GenericLookupLevels = new List<GenericLookup_Levels>();

            for (int i = 6; i < reader.FieldCount; i++)
            {
                GenericLookup_Levels levels = new GenericLookup_Levels();
                levels.GenericLookupID = Convert.ToInt32(reader.GetName(i));
                levels.GenericLookupCount = reader.IsDBNull(i) ? 0 : reader.GetInt32(i);
                jobPosting.GenericLookupLevels.Add(levels);
            }

            return jobPosting;
        }
        //******************END**********************
        JobPosting IEntityBuilder<JobPosting>.BuildEntity(IDataReader reader)
        {
            JobPosting jobPosting = new JobPosting();
            if (reader.FieldCount == 9)
            {
                const int FLD_ID = 0;
                const int FLD_JOBTITLE = 1;
                const int FLD_JOBPOSTINGCODE = 2;
                const int FLD_JOBSTATUS = 3;
                const int FLD_CITY = 4;
                const int FLD_STATEID = 5;
                const int FLD_POSTEDDATE = 6;
                const int FLD_CLIENTID = 7;
                const int FLD_JOBDEPARTMENTLOOKUPID = 8;


                jobPosting.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                jobPosting.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
                jobPosting.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
                jobPosting.JobStatus = reader.IsDBNull(FLD_JOBSTATUS) ? 0 : reader.GetInt32(FLD_JOBSTATUS);
                jobPosting.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
                jobPosting.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
                jobPosting.PostedDate = reader.IsDBNull(FLD_POSTEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_POSTEDDATE);
                jobPosting.ClientId = reader.IsDBNull(FLD_CLIENTID) ? 0 : reader.GetInt32(FLD_CLIENTID);
                jobPosting.JobDepartmentLookUpId = reader.IsDBNull(FLD_JOBDEPARTMENTLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBDEPARTMENTLOOKUPID);

            }
            //******************Code added by pravin khot 19/Jan/2016 Using Job apply by career page******************
            else if (reader.FieldCount == 6)
            {
                const int FLD_ID = 0;
                const int FLD_JobPostingCode = 1;
                const int FLD_MemberId = 2;
                const int FLD_CREATEDATE = 3;
                const int FLD_UPDATEDATE = 4;
                const int FLD_ISAPPLIED = 5;

                jobPosting.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                jobPosting.CareerJobId = reader.IsDBNull(FLD_JobPostingCode) ? 0 : reader.GetInt32(FLD_JobPostingCode);
                jobPosting.MemberId = reader.IsDBNull(FLD_MemberId) ? 0 : reader.GetInt32(FLD_MemberId);
                jobPosting.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                jobPosting.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
                jobPosting.IsApplied = reader.IsDBNull(FLD_ISAPPLIED) ? false : reader.GetBoolean(FLD_ISAPPLIED);

            }
            //********************************End******************************************
            else
            {

                const int FLD_ID = 0;
                const int FLD_JOBTITLE = 1;
                const int FLD_JOBPOSTINGCODE = 2;
                const int FLD_CLIENTJOBID = 3;
                const int FLD_NOOFOPENINGS = 4;
                const int FLD_PAYRATE = 5;
                const int FLD_PAYRATECURRENCYLOOKUPID = 6;
                const int FLD_PAYCYCLE = 7;
                const int FLD_TRAVELREQUIRED = 8;
                const int FLD_TRAVELREQUIREDPERCENT = 9;
                const int FLD_OTHERBENEFITS = 10;
                const int FLD_JOBSTATUS = 11;
                const int FLD_JOBDURATIONLOOKUPID = 12;
                const int FLD_JOBDURATIONMONTH = 13;
                const int FLD_JOBADDRESS1 = 14;
                const int FLD_JOBADDRESS2 = 15;
                const int FLD_CITY = 16;
                const int FLD_ZIPCODE = 17;
                const int FLD_COUNTRYID = 18;
                const int FLD_STATEID = 19;
                const int FLD_STARTDATE = 20;
                const int FLD_FINALHIREDDATE = 21;
                const int FLD_JOBDESCRIPTION = 22;
                const int FLD_AUTHORIZATIONTYPELOOKUPID = 23;
                const int FLD_POSTEDDATE = 24;
                const int FLD_ACTIVATIONDATE = 25;
                const int FLD_REQUIREDDEGREELOOKUPID = 26;
                const int FLD_MINEXPREQUIRED = 27;
                const int FLD_MAXEXPREQUIRED = 28;
                //  const int FLD_VENDORGROUPID = 29;
                const int FLD_ISJOBACTIVE = 29;
                // const int FLD_PUBLISHEDFORINTERNAL = 31;
                // const int FLD_PUBLISHEDFORPUBLIC = 32;
                // const int FLD_PUBLISHEDFORPARTNER = 33;
                //  const int FLD_MINAGEREQUIRED = 34;
                //  const int FLD_MAXAGEREQUIRED = 35;
                const int FLD_JOBTYPE = 30;
                const int FLD_ISAPPROVALREQUIRED = 31;
                const int FLD_INTERNALNOTE = 32;
                const int FLD_CLIENTID = 33;
                //  const int FLD_CLIENTPROJECTID = 40;
                //   const int FLD_CLIENTPROJECT = 41;
                //  const int FLD_CLIENTENDCLIENTID = 42;
                const int FLD_CLIENTHOURLYRATE = 34;
                const int FLD_CLIENTHOURLYRATECURRENCYLOOKUPID = 35;
                const int FLD_CLIENTRATEPAYCYCLE = 36;
                //  const int FLD_CLIENTDISPLAYNAME = 46;
                //  const int FLD_CLIENTID2 = 47;
                //  const int FLD_CLIENTID3 = 48;
                //  const int FLD_CLIENTJOBDESCRIPTION = 49;
                const int FLD_TAXTERMLOOKUPIDS = 37;
                const int FLD_JOBCATEGORYLOOKUPID = 38;
                //   const int FLD_JOBCATEGORYSUBID = 52;
                //   const int FLD_JOBINDUSTRYLOOKUPID = 53;
                const int FLD_EXPECTEDREVENUE = 39;
                const int FLD_EXPECTEDREVENUECURRENCYLOOKUPID = 40;
                //   const int FLD_SOURCINGCHANNEL = 56;
                //   const int FLD_SOURCINGEXPENSES = 57;
                //   const int FLD_SOURCINGEXPENSESCURRENCYLOOKUPID = 58;
                const int FLD_WORKFLOWAPPROVED = 41;
                //   const int FLD_PUBLISHEDFORVENDOR = 60;
                const int FLD_TELECOMMUNICATION = 42;
                const int FLD_ISTEMPLATE = 43;
                const int FLD_ISREMOVED = 44;
                const int FLD_CREATORID = 45;
                const int FLD_UPDATORID = 46;
                const int FLD_CREATEDATE = 47;
                const int FLD_UPDATEDATE = 48;
                const int FLD_ISEXPENSESPAID = 49;      //0.1
                const int FLD_RAWDESCRIPTION = 50;
                const int FLD_JOBDEPARTMENTLOOKUPID = 51;
                const int FLD_JOBSKILLLOOKUPID = 52;

                const int FLD_OCCUPATIONALSERIESLOOKUPID = 53;
                const int FLD_PAYGRADELOOKUPID = 54;
                const int FLD_WORKSCHEDULELOOKUPID = 55;
                const int FLD_SECURITUCLEARANCE = 56;
                const int FLD_CLIENTCONTACTID = 57;

                const int FLD_REPORTINGTO = 58;
                const int FLD_NOOFREPORTEES = 59;
                const int FLD_MINIMUMQUALIFYINGPARAMETERS = 60;
                const int FLD_MAXPAYRATE = 61;
                const int FLD_ALLOWRECRUITORTOCHANGESTATUS = 62;
                const int FLD_SHOWINCANDIDATEPORTAL = 63;
                const int FLD_SHOWINEMPLOYEEREFERRALPORTAL = 64;
                const int FLD_DISPLAYREQUISITIONINVENDORPORTAL = 65;
                const int FLD_OPENDATE = 66;
                const int FLD_SALESREGIONLOOKUPID = 67;
                const int FLD_SALESGROUPLOOKUPID = 68;
                const int FLD_CUSTOMERNAME = 69;
                const int FLD_POAVAILABILTY = 70;
                const int FLD_JOBLOCATION = 71;
                const int FLD_EMPLOYEMENTTYPTLOOKUOID = 72;
                const int FLD_VENDORLIST = 73;
                const int FLD_CLIENTBRIEF = 74;
                const int FLD_REQUISITIONSOURCE = 75;
                const int FLD_EMAILSUBJECT = 76;
                const int FLD_REQUISITIONTYPE = 77;

                const int FLD_REQUISITIONBRANCH = 78;
                const int FLD_REQUISITIONGRADE = 79;
              






                jobPosting.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                jobPosting.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
                jobPosting.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
                jobPosting.ClientJobId = reader.IsDBNull(FLD_CLIENTJOBID) ? string.Empty : reader.GetString(FLD_CLIENTJOBID);
                jobPosting.NoOfOpenings = reader.IsDBNull(FLD_NOOFOPENINGS) ? 0 : reader.GetInt32(FLD_NOOFOPENINGS);
                jobPosting.PayRate = reader.IsDBNull(FLD_PAYRATE) ? string.Empty : reader.GetString(FLD_PAYRATE);
                jobPosting.PayRateCurrencyLookupId = reader.IsDBNull(FLD_PAYRATECURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_PAYRATECURRENCYLOOKUPID);
                jobPosting.PayCycle = reader.IsDBNull(FLD_PAYCYCLE) ? string.Empty : reader.GetString(FLD_PAYCYCLE);
                jobPosting.TravelRequired = reader.IsDBNull(FLD_TRAVELREQUIRED) ? false : reader.GetBoolean(FLD_TRAVELREQUIRED);
                jobPosting.TravelRequiredPercent = reader.IsDBNull(FLD_TRAVELREQUIREDPERCENT) ? string.Empty : reader.GetString(FLD_TRAVELREQUIREDPERCENT);
                jobPosting.OtherBenefits = reader.IsDBNull(FLD_OTHERBENEFITS) ? string.Empty : reader.GetString(FLD_OTHERBENEFITS);
                jobPosting.JobStatus = reader.IsDBNull(FLD_JOBSTATUS) ? 0 : reader.GetInt32(FLD_JOBSTATUS);
                jobPosting.JobDurationLookupId = reader.IsDBNull(FLD_JOBDURATIONLOOKUPID) ? string.Empty : reader.GetString(FLD_JOBDURATIONLOOKUPID);
                jobPosting.JobDurationMonth = reader.IsDBNull(FLD_JOBDURATIONMONTH) ? string.Empty : reader.GetString(FLD_JOBDURATIONMONTH);
                jobPosting.JobAddress1 = reader.IsDBNull(FLD_JOBADDRESS1) ? string.Empty : reader.GetString(FLD_JOBADDRESS1);
                jobPosting.JobAddress2 = reader.IsDBNull(FLD_JOBADDRESS2) ? string.Empty : reader.GetString(FLD_JOBADDRESS2);
                jobPosting.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
                jobPosting.ZipCode = reader.IsDBNull(FLD_ZIPCODE) ? string.Empty : reader.GetString(FLD_ZIPCODE);
                jobPosting.CountryId = reader.IsDBNull(FLD_COUNTRYID) ? 0 : reader.GetInt32(FLD_COUNTRYID);
                jobPosting.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
                jobPosting.StartDate = reader.IsDBNull(FLD_STARTDATE) ? string.Empty : reader.GetString(FLD_STARTDATE);
                jobPosting.FinalHiredDate = reader.IsDBNull(FLD_FINALHIREDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_FINALHIREDDATE);
                jobPosting.JobDescription = reader.IsDBNull(FLD_JOBDESCRIPTION) ? string.Empty : reader.GetString(FLD_JOBDESCRIPTION);
                jobPosting.AuthorizationTypeLookupId = reader.IsDBNull(FLD_AUTHORIZATIONTYPELOOKUPID) ? string.Empty : reader.GetString(FLD_AUTHORIZATIONTYPELOOKUPID);
                jobPosting.PostedDate = reader.IsDBNull(FLD_POSTEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_POSTEDDATE);
                jobPosting.ActivationDate = reader.IsDBNull(FLD_ACTIVATIONDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ACTIVATIONDATE);
                jobPosting.RequiredDegreeLookupId = reader.IsDBNull(FLD_REQUIREDDEGREELOOKUPID) ? string.Empty : reader.GetString(FLD_REQUIREDDEGREELOOKUPID);
                jobPosting.MinExpRequired = reader.IsDBNull(FLD_MINEXPREQUIRED) ? string.Empty : reader.GetString(FLD_MINEXPREQUIRED);
                jobPosting.MaxExpRequired = reader.IsDBNull(FLD_MAXEXPREQUIRED) ? string.Empty : reader.GetString(FLD_MAXEXPREQUIRED);
                // jobPosting.VendorGroupId = reader.IsDBNull(FLD_VENDORGROUPID) ? 0 : reader.GetInt32(FLD_VENDORGROUPID);
                jobPosting.IsJobActive = reader.IsDBNull(FLD_ISJOBACTIVE) ? false : reader.GetBoolean(FLD_ISJOBACTIVE);
                //jobPosting.PublishedForInternal = reader.IsDBNull(FLD_PUBLISHEDFORINTERNAL) ? false : reader.GetBoolean(FLD_PUBLISHEDFORINTERNAL);
                // jobPosting.PublishedForPublic = reader.IsDBNull(FLD_PUBLISHEDFORPUBLIC) ? false : reader.GetBoolean(FLD_PUBLISHEDFORPUBLIC);
                // jobPosting.PublishedForPartner = reader.IsDBNull(FLD_PUBLISHEDFORPARTNER) ? false : reader.GetBoolean(FLD_PUBLISHEDFORPARTNER);
                // jobPosting.MinAgeRequired = reader.IsDBNull(FLD_MINAGEREQUIRED) ? 0 : reader.GetInt32(FLD_MINAGEREQUIRED);
                // jobPosting.MaxAgeRequired = reader.IsDBNull(FLD_MAXAGEREQUIRED) ? 0 : reader.GetInt32(FLD_MAXAGEREQUIRED);
                jobPosting.JobType = reader.IsDBNull(FLD_JOBTYPE) ? 0 : reader.GetInt32(FLD_JOBTYPE);
                jobPosting.IsApprovalRequired = reader.IsDBNull(FLD_ISAPPROVALREQUIRED) ? false : reader.GetBoolean(FLD_ISAPPROVALREQUIRED);
                jobPosting.InternalNote = reader.IsDBNull(FLD_INTERNALNOTE) ? string.Empty : reader.GetString(FLD_INTERNALNOTE);
                jobPosting.ClientId = reader.IsDBNull(FLD_CLIENTID) ? 0 : reader.GetInt32(FLD_CLIENTID);
                //jobPosting.ClientProjectId = reader.IsDBNull(FLD_CLIENTPROJECTID) ? 0 : reader.GetInt32(FLD_CLIENTPROJECTID);
                // jobPosting.ClientProject = reader.IsDBNull(FLD_CLIENTPROJECT) ? string.Empty : reader.GetString(FLD_CLIENTPROJECT);
                // jobPosting.ClientEndClientId = reader.IsDBNull(FLD_CLIENTENDCLIENTID) ? 0 : reader.GetInt32(FLD_CLIENTENDCLIENTID);
                jobPosting.ClientHourlyRate = reader.IsDBNull(FLD_CLIENTHOURLYRATE) ? string.Empty : reader.GetString(FLD_CLIENTHOURLYRATE);
                jobPosting.ClientHourlyRateCurrencyLookupId = reader.IsDBNull(FLD_CLIENTHOURLYRATECURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CLIENTHOURLYRATECURRENCYLOOKUPID);
                jobPosting.ClientRatePayCycle = reader.IsDBNull(FLD_CLIENTRATEPAYCYCLE) ? string.Empty : reader.GetString(FLD_CLIENTRATEPAYCYCLE);
                //jobPosting.ClientDisplayName = reader.IsDBNull(FLD_CLIENTDISPLAYNAME) ? string.Empty : reader.GetString(FLD_CLIENTDISPLAYNAME);
                // jobPosting.ClientId2 = reader.IsDBNull(FLD_CLIENTID2) ? 0 : reader.GetInt32(FLD_CLIENTID2);
                // jobPosting.ClientId3 = reader.IsDBNull(FLD_CLIENTID3) ? 0 : reader.GetInt32(FLD_CLIENTID3);
                // jobPosting.ClientJobDescription = reader.IsDBNull(FLD_CLIENTJOBDESCRIPTION) ? string.Empty : reader.GetString(FLD_CLIENTJOBDESCRIPTION);
                jobPosting.TaxTermLookupIds = reader.IsDBNull(FLD_TAXTERMLOOKUPIDS) ? string.Empty : reader.GetString(FLD_TAXTERMLOOKUPIDS);
                jobPosting.JobCategoryLookupId = reader.IsDBNull(FLD_JOBCATEGORYLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBCATEGORYLOOKUPID);
                //  jobPosting.JobCategorySubId = reader.IsDBNull(FLD_JOBCATEGORYSUBID) ? 0 : reader.GetInt32(FLD_JOBCATEGORYSUBID);
                //  jobPosting.JobIndustryLookupId = reader.IsDBNull(FLD_JOBINDUSTRYLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBINDUSTRYLOOKUPID);
                jobPosting.ExpectedRevenue = reader.IsDBNull(FLD_EXPECTEDREVENUE) ? 0 : reader.GetDecimal(FLD_EXPECTEDREVENUE);
                jobPosting.ExpectedRevenueCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDREVENUECURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDREVENUECURRENCYLOOKUPID);
                // jobPosting.SourcingChannel = reader.IsDBNull(FLD_SOURCINGCHANNEL) ? string.Empty : reader.GetString(FLD_SOURCINGCHANNEL);
                //  jobPosting.SourcingExpenses = reader.IsDBNull(FLD_SOURCINGEXPENSES) ? 0 : reader.GetDecimal(FLD_SOURCINGEXPENSES);
                //   jobPosting.SourcingExpensesCurrencyLookupId = reader.IsDBNull(FLD_SOURCINGEXPENSESCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_SOURCINGEXPENSESCURRENCYLOOKUPID);
                jobPosting.WorkflowApproved = reader.IsDBNull(FLD_WORKFLOWAPPROVED) ? false : reader.GetBoolean(FLD_WORKFLOWAPPROVED);
                //  jobPosting.PublishedForVendor = reader.IsDBNull(FLD_PUBLISHEDFORVENDOR) ? false : reader.GetBoolean(FLD_PUBLISHEDFORVENDOR);
                jobPosting.TeleCommunication = reader.IsDBNull(FLD_TELECOMMUNICATION) ? false : reader.GetBoolean(FLD_TELECOMMUNICATION);
                jobPosting.IsTemplate = reader.IsDBNull(FLD_ISTEMPLATE) ? false : reader.GetBoolean(FLD_ISTEMPLATE);
                jobPosting.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
                jobPosting.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                jobPosting.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                jobPosting.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                jobPosting.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
                jobPosting.IsExpensesPaid = reader.IsDBNull(FLD_ISEXPENSESPAID) ? false : reader.GetBoolean(FLD_ISEXPENSESPAID);        //0.1
                jobPosting.RawDescription = reader.IsDBNull(FLD_RAWDESCRIPTION) ? string.Empty : reader.GetString(FLD_RAWDESCRIPTION);
                jobPosting.JobDepartmentLookUpId = reader.IsDBNull(FLD_JOBDEPARTMENTLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBDEPARTMENTLOOKUPID);
                jobPosting.JobSkillLookUpId = reader.IsDBNull(FLD_JOBSKILLLOOKUPID) ? string.Empty : reader.GetString(FLD_JOBSKILLLOOKUPID);
                jobPosting.OccuptionalSeriesLookupId = reader.IsDBNull(FLD_OCCUPATIONALSERIESLOOKUPID) ? 0 : reader.GetInt32(FLD_OCCUPATIONALSERIESLOOKUPID);
                jobPosting.PayGradeLookupId = reader.IsDBNull(FLD_PAYGRADELOOKUPID) ? 0 : reader.GetInt32(FLD_PAYGRADELOOKUPID);
                jobPosting.WorkScheduleLookupId = reader.IsDBNull(FLD_WORKSCHEDULELOOKUPID) ? 0 : reader.GetInt32(FLD_WORKSCHEDULELOOKUPID);
                jobPosting.SecurityClearance = reader.IsDBNull(FLD_SECURITUCLEARANCE) ? false : reader.GetBoolean(FLD_SECURITUCLEARANCE);
                jobPosting.ClientContactId = reader.IsDBNull(FLD_CLIENTCONTACTID) ? 0 : reader.GetInt32(FLD_CLIENTCONTACTID);

                jobPosting.ReportingTo = reader.IsDBNull(FLD_REPORTINGTO) ? string.Empty : reader.GetString(FLD_REPORTINGTO);
                jobPosting.NoOfReportees = reader.IsDBNull(FLD_NOOFREPORTEES) ? 0 : reader.GetInt32(FLD_NOOFREPORTEES);
                jobPosting.MinimumQualifyingParameters = reader.IsDBNull(FLD_MINIMUMQUALIFYINGPARAMETERS) ? string.Empty : reader.GetString(FLD_MINIMUMQUALIFYINGPARAMETERS);
                jobPosting.MaxPayRate = reader.IsDBNull(FLD_MAXPAYRATE) ? 0 : reader.GetDecimal(FLD_MAXPAYRATE);
                jobPosting.AllowRecruitersToChangeStatus = reader.IsDBNull(FLD_ALLOWRECRUITORTOCHANGESTATUS) ? false : reader.GetBoolean(FLD_ALLOWRECRUITORTOCHANGESTATUS);
                jobPosting.ShowInCandidatePortal = reader.IsDBNull(FLD_SHOWINCANDIDATEPORTAL) ? false : reader.GetBoolean(FLD_SHOWINCANDIDATEPORTAL);
                jobPosting.ShowInEmployeeReferralPortal = reader.IsDBNull(FLD_SHOWINEMPLOYEEREFERRALPORTAL) ? false : reader.GetBoolean(FLD_SHOWINEMPLOYEEREFERRALPORTAL);
                jobPosting.DisplayRequisitionInVendorPortal = reader.IsDBNull(FLD_DISPLAYREQUISITIONINVENDORPORTAL) ? false : reader.GetBoolean(FLD_DISPLAYREQUISITIONINVENDORPORTAL);
                jobPosting.OpenDate = reader.IsDBNull(FLD_OPENDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_OPENDATE);
                jobPosting.SalesRegionLookUpId = reader.IsDBNull(FLD_SALESREGIONLOOKUPID) ? 0 : reader.GetInt32(FLD_SALESREGIONLOOKUPID);
                jobPosting.SalesGroupLookUpId = reader.IsDBNull(FLD_SALESGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_SALESGROUPLOOKUPID);
                jobPosting.JobCategoryLookupId = reader.IsDBNull(FLD_JOBCATEGORYLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBCATEGORYLOOKUPID);
                jobPosting.CustomerName = reader.IsDBNull(FLD_CUSTOMERNAME) ? string.Empty : reader.GetString(FLD_CUSTOMERNAME);
                jobPosting.POAvailability = reader.IsDBNull(FLD_POAVAILABILTY) ? 0 : reader.GetInt32(FLD_POAVAILABILTY);
                jobPosting.JobLocationLookUpID = reader.IsDBNull(FLD_JOBLOCATION) ? 0 : reader.GetInt32(FLD_JOBLOCATION);
                jobPosting.EmployementTypeLookUpID = reader.IsDBNull(FLD_EMPLOYEMENTTYPTLOOKUOID) ? 0 : reader.GetInt32(FLD_EMPLOYEMENTTYPTLOOKUOID);
                jobPosting.VendorList = reader.IsDBNull(FLD_VENDORLIST) ? string.Empty : reader.GetString(FLD_VENDORLIST);
                jobPosting.ClientBrief = reader.IsDBNull(FLD_CLIENTBRIEF) ? string.Empty : reader.GetString(FLD_CLIENTBRIEF);
              
                // 0.3 starts

                if (reader.FieldCount > FLD_REQUISITIONSOURCE)
                {
                    jobPosting.RequisitionSource = reader.IsDBNull(FLD_REQUISITIONSOURCE) ? string.Empty : reader.GetString(FLD_REQUISITIONSOURCE);
                }

                if (reader.FieldCount > FLD_EMAILSUBJECT)
                {
                    jobPosting.EmailSubject = reader.IsDBNull(FLD_EMAILSUBJECT) ? string.Empty : reader.GetString(FLD_EMAILSUBJECT);
                }

                if (reader.FieldCount > FLD_REQUISITIONTYPE)
                {
                    jobPosting.RequisitionType = reader.IsDBNull(FLD_REQUISITIONTYPE) ? string.Empty : reader.GetString(FLD_REQUISITIONTYPE);
                }

                if (reader.FieldCount > FLD_REQUISITIONBRANCH)
                {
                    jobPosting.RequestionBranch = reader.IsDBNull(FLD_REQUISITIONBRANCH) ? string.Empty : reader.GetString(FLD_REQUISITIONBRANCH);
                }

                if (reader.FieldCount > FLD_REQUISITIONGRADE)
                {
                    jobPosting.RequestionGrade = reader.IsDBNull(FLD_REQUISITIONGRADE) ? string.Empty : reader.GetString(FLD_REQUISITIONGRADE);
                }
              
 




            }


            return jobPosting;
        }

        //Code introduced by Prasanth on 22/May/2015 Start
        JobPosting RequisitionAgingBuildPagedEntity(IDataReader reader)
        {
            JobPosting jobPosting = new JobPosting();
            const int FLD_JOBTITLE = 0;
            const int FLD_OPENDATE = 1;
            const int FLD_FINALHIREDDATE = 2;
            const int FLD_TOTALTIMETAKEN = 3;
            const int FLD_JOBPOSTID = 4;
            const int FLD_ACTIVATIONDATE = 5; //Code introduced by Prasanth on 11/Jun/2015
            jobPosting.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            jobPosting.OpenDate = reader.IsDBNull(FLD_OPENDATE) ?  DateTime.MinValue : reader.GetDateTime(FLD_OPENDATE);
            jobPosting.FinalHiredDate = reader.IsDBNull(FLD_FINALHIREDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_FINALHIREDDATE);
            jobPosting.TotalTimeTaken = reader.IsDBNull(FLD_TOTALTIMETAKEN) ? 0 : reader.GetInt32(FLD_TOTALTIMETAKEN);
            jobPosting.Id = reader.IsDBNull(FLD_JOBPOSTID) ? 0 : reader.GetInt32(FLD_JOBPOSTID);
            jobPosting.ActivationDate = reader.IsDBNull(FLD_ACTIVATIONDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ACTIVATIONDATE);
            jobPosting.HiringMatrixLevels = new List<Hiring_Levels>();

            for (int i = 6; i < reader.FieldCount; i++)
            {
                Hiring_Levels levels = new Hiring_Levels();
                levels.HiringMatrixLevelID = Convert.ToInt32(reader.GetName(i));
                levels.CandidateCount = reader.IsDBNull(i) ? 0 : reader.GetInt32(i);
                jobPosting.HiringMatrixLevels.Add(levels);
            }


            return jobPosting;
        }
        //******************END**********************


        JobPosting BuildPagedEntity(IDataReader reader)
        {
            JobPosting jobPosting = new JobPosting();
            //if (reader.FieldCount < 67 && reader.FieldCount != 16 && reader.FieldCount != 23)
            if (reader.FieldCount < 67 && reader.FieldCount != 18)  //16 replace by 17 pravin on 22/June/2016
            {
                //****************Code aadded by pravin khot on 15/Jan/2016**************
                if (reader.FieldCount == 6)
                {
                    const int FLD_ID = 0;
                    const int FLD_JOBTITLE = 1;
                    const int FLD_JOBPOSTINGCODE = 2;
                    const int FLD_CITY = 3;
                    const int FLD_MINEXPREQUIRED = 4;
                    const int FLD_MAXEXPREQUIRED = 5;

                    jobPosting.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                    jobPosting.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
                    jobPosting.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
                    jobPosting.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
                    jobPosting.MinExpRequired = reader.IsDBNull(FLD_MINEXPREQUIRED) ? string.Empty : reader.GetString(FLD_MINEXPREQUIRED);
                    jobPosting.MaxExpRequired = reader.IsDBNull(FLD_MAXEXPREQUIRED) ? string.Empty : reader.GetString(FLD_MAXEXPREQUIRED);

                }
                //********************************End********************************
                else
                {
                    const int FLD_ID = 0;
                    const int FLD_JOBTITLE = 1;
                    const int FLD_JOBPOSTINGCODE = 2;
                    const int FLD_JOBSTATUS = 3;
                    const int FLD_CITY = 4;
                    const int FLD_STATEID = 5;
                    const int FLD_POSTEDDATE = 6;
                    const int FLD_CLIENTID = 7;
                    const int FLD_JOBDEPARTMENTLOOKUPID = 8;
                    const int FLD_CREATORID = 9;
                    const int FLD_JOBDURATIONLOOKUPID = 10;
                    const int FLD_NOOFASSIGNEDMANAGERS = 11;

                    jobPosting.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                    jobPosting.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
                    jobPosting.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
                    jobPosting.JobStatus = reader.IsDBNull(FLD_JOBSTATUS) ? 0 : reader.GetInt32(FLD_JOBSTATUS);
                    jobPosting.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
                    jobPosting.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
                    jobPosting.PostedDate = reader.IsDBNull(FLD_POSTEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_POSTEDDATE);
                    jobPosting.ClientId = reader.IsDBNull(FLD_CLIENTID) ? 0 : reader.GetInt32(FLD_CLIENTID);
                    jobPosting.JobDepartmentLookUpId = reader.IsDBNull(FLD_JOBDEPARTMENTLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBDEPARTMENTLOOKUPID);
                    jobPosting.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                    jobPosting.JobDurationLookupId = reader.IsDBNull(FLD_JOBDURATIONLOOKUPID) ? string.Empty : reader.GetString(FLD_JOBDURATIONLOOKUPID);
                    jobPosting.NoOfAssignedManagers = reader.IsDBNull(FLD_NOOFASSIGNEDMANAGERS) ? 0 : reader.GetInt32(FLD_NOOFASSIGNEDMANAGERS);

                    jobPosting.HiringMatrixLevels = new List<Hiring_Levels>();
                    for (int i = 14; i < reader.FieldCount; i++)
                    {
                        Hiring_Levels levels = new Hiring_Levels();
                        levels.HiringMatrixLevelID = Convert.ToInt32(reader.GetName(i));
                        levels.CandidateCount = reader.IsDBNull(i) ? 0 : reader.GetInt32(i);
                        jobPosting.HiringMatrixLevels.Add(levels);
                    }
                }
            }
            else if (reader.FieldCount == 18)//16 replace by 18 pravin on 22/June/2016
            {
                const int FLD_ID = 0;
                const int FLD_JOBTITLE = 1;
                const int FLD_JOBPOSTINGCODE = 2;
                const int FLD_JOBSTATUS = 3;
                const int FLD_CITY = 4;
                const int FLD_STATEID = 5;
                const int FLD_POSTEDDATE = 6;
                const int FLD_CLIENTID = 7;
                const int FLD_CLIENTJOBID = 8;
                const int FLD_NOOFOPENINGS = 9;
                const int FLD_JOBDEPARTMENTLOOKUPID = 10;
                const int FLD_CREATORID = 11;
                const int FLD_ALLOWRECRUITERTOCHANGESTATUS = 12;
                const int FLD_NOOFASSIGNEDMANAGERS = 13;
                const int FLD_SHOWINEMPLOYEEPORTAL = 14;
                const int FLD_UPDATEDATE = 15;
                const int FLD_STARTDATE = 16;
                //const int FLD_JOBDURATIONLOOKUPID = 10;

                jobPosting.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                jobPosting.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
                jobPosting.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
                jobPosting.JobStatus = reader.IsDBNull(FLD_JOBSTATUS) ? 0 : reader.GetInt32(FLD_JOBSTATUS);
                jobPosting.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
                jobPosting.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
                jobPosting.PostedDate = reader.IsDBNull(FLD_POSTEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_POSTEDDATE);
                jobPosting.ClientId = reader.IsDBNull(FLD_CLIENTID) ? 0 : reader.GetInt32(FLD_CLIENTID);
                jobPosting.ClientJobId = reader.IsDBNull(FLD_CLIENTJOBID) ? string.Empty : reader.GetString(FLD_CLIENTJOBID);
                jobPosting.NoOfOpenings = reader.IsDBNull(FLD_NOOFOPENINGS) ? 0 : reader.GetInt32(FLD_NOOFOPENINGS);
                jobPosting.JobDepartmentLookUpId = reader.IsDBNull(FLD_JOBDEPARTMENTLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBDEPARTMENTLOOKUPID);
                jobPosting.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                jobPosting.AllowRecruitersToChangeStatus = reader.IsDBNull(FLD_ALLOWRECRUITERTOCHANGESTATUS) ? false : reader.GetBoolean(FLD_ALLOWRECRUITERTOCHANGESTATUS);
                jobPosting.NoOfAssignedManagers = reader.IsDBNull(FLD_NOOFASSIGNEDMANAGERS) ? 0 : reader.GetInt32(FLD_NOOFASSIGNEDMANAGERS);
                jobPosting.ShowInEmployeeReferralPortal = reader.IsDBNull(FLD_SHOWINEMPLOYEEPORTAL) ? false : reader.GetBoolean(FLD_SHOWINEMPLOYEEPORTAL);
                //jobPosting.JobDurationLookupId = reader.IsDBNull(FLD_JOBDURATIONLOOKUPID) ? string.Empty : reader.GetString(FLD_JOBDURATIONLOOKUPID);
                jobPosting.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE); //added by pravin khot on 22/June/2016
                jobPosting.StartDate = reader.IsDBNull(FLD_STARTDATE) ? string.Empty : reader.GetString(FLD_STARTDATE); //added by pravin khot on 22/June/2016

                jobPosting.HiringMatrixLevels = new List<Hiring_Levels>();

                for (int i = FLD_SHOWINEMPLOYEEPORTAL + 3; i < reader.FieldCount; i++)
                {
                    Hiring_Levels levels = new Hiring_Levels();
                    levels.HiringMatrixLevelID = Convert.ToInt32(reader.GetName(i));
                    levels.CandidateCount = reader.IsDBNull(i) ? 0 : reader.GetInt32(i);
                    jobPosting.HiringMatrixLevels.Add(levels);
                }

            }


            /*else if (reader.FieldCount == 23)
            {
                const int FLD_ID = 0;
                const int FLD_JOBTITLE = 1;
                const int FLD_JOBPOSTINGCODE = 2;
                const int FLD_JOBSTATUS = 3;
                const int FLD_CITY = 4;
                const int FLD_STATEID = 5;
                const int FLD_POSTEDDATE = 6;
                const int FLD_CLIENTID = 7;
                const int FLD_CLIENTJOBID = 8;
                const int FLD_NOOFOPENINGS = 9;
                const int FLD_JOBDEPARTMENTLOOKUPID = 10;
                const int FLD_CREATORID = 11;
                const int FLD_ALLOWRECRUITERTOCHANGESTATUS = 12;
                const int FLD_NOOFASSIGNEDMANAGERS = 13;
                const int FLD_SHOWINEMPLOYEEPORTAL = 14;

                const int FLD_OPENDATE = 15;
                const int FLD_FINALHIREDDATE = 16;
                const int FLD_BRANCH = 17;
                const int FLD_GRADE = 18;
                
                const int FLD_ONNOTICEPERIOD = 19;
                const int FLD_FILLED = 20;
                const int FLD_AVAILABLE = 21; 
              




                //const int FLD_JOBDURATIONLOOKUPID = 10;

                jobPosting.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                jobPosting.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
                jobPosting.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
                jobPosting.JobStatus = reader.IsDBNull(FLD_JOBSTATUS) ? 0 : reader.GetInt32(FLD_JOBSTATUS);
                jobPosting.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
                jobPosting.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
                jobPosting.PostedDate = reader.IsDBNull(FLD_POSTEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_POSTEDDATE);
                jobPosting.ClientId = reader.IsDBNull(FLD_CLIENTID) ? 0 : reader.GetInt32(FLD_CLIENTID);
                jobPosting.ClientJobId = reader.IsDBNull(FLD_CLIENTJOBID) ? string.Empty : reader.GetString(FLD_CLIENTJOBID);
                jobPosting.NoOfOpenings = reader.IsDBNull(FLD_NOOFOPENINGS) ? 0 : reader.GetInt32(FLD_NOOFOPENINGS);
                jobPosting.JobDepartmentLookUpId = reader.IsDBNull(FLD_JOBDEPARTMENTLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBDEPARTMENTLOOKUPID);
                jobPosting.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                jobPosting.AllowRecruitersToChangeStatus = reader.IsDBNull(FLD_ALLOWRECRUITERTOCHANGESTATUS) ? false : reader.GetBoolean(FLD_ALLOWRECRUITERTOCHANGESTATUS);
                jobPosting.NoOfAssignedManagers = reader.IsDBNull(FLD_NOOFASSIGNEDMANAGERS) ? 0 : reader.GetInt32(FLD_NOOFASSIGNEDMANAGERS);
                jobPosting.ShowInEmployeeReferralPortal = reader.IsDBNull(FLD_SHOWINEMPLOYEEPORTAL) ? false : reader.GetBoolean(FLD_SHOWINEMPLOYEEPORTAL);


                jobPosting.OpenDate = reader.IsDBNull(FLD_OPENDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_OPENDATE);
                jobPosting.FinalHiredDate = reader.IsDBNull(FLD_FINALHIREDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_FINALHIREDDATE);
                jobPosting.RequestionBranch = reader.IsDBNull(FLD_BRANCH) ? string.Empty : reader.GetString(FLD_BRANCH);
                jobPosting.RequestionGrade = reader.IsDBNull(FLD_GRADE) ? string.Empty : reader.GetString(FLD_GRADE);
                
                jobPosting.onnoticeperiod = reader.IsDBNull(FLD_ONNOTICEPERIOD) ? string.Empty : reader.GetString(FLD_ONNOTICEPERIOD);
                jobPosting.Filled = reader.IsDBNull(FLD_FILLED) ? string.Empty : reader.GetString(FLD_FILLED);
                jobPosting.Available = reader.IsDBNull(FLD_AVAILABLE) ? 0 : reader.GetInt32(FLD_AVAILABLE);
                   



                //jobPosting.JobDurationLookupId = reader.IsDBNull(FLD_JOBDURATIONLOOKUPID) ? string.Empty : reader.GetString(FLD_JOBDURATIONLOOKUPID);

                //jobPosting.HiringMatrixLevels = new List<Hiring_Levels>();

                //for (int i = FLD_SHOWINEMPLOYEEPORTAL + 1; i < reader.FieldCount; i++)
                //{
                //    Hiring_Levels levels = new Hiring_Levels();
                //    levels.HiringMatrixLevelID = Convert.ToInt32(reader.GetName(i));
                //    levels.CandidateCount = reader.IsDBNull(i) ? 0 : reader.GetInt32(i);
                //    jobPosting.HiringMatrixLevels.Add(levels);
                //}

            }*/


            else
            {

                const int FLD_ID = 0;
                const int FLD_JOBTITLE = 1;
                const int FLD_JOBPOSTINGCODE = 2;
                const int FLD_CLIENTJOBID = 3;
                const int FLD_NOOFOPENINGS = 4;
                const int FLD_PAYRATE = 5;
                const int FLD_PAYRATECURRENCYLOOKUPID = 6;
                const int FLD_PAYCYCLE = 7;
                const int FLD_TRAVELREQUIRED = 8;
                const int FLD_TRAVELREQUIREDPERCENT = 9;
                const int FLD_OTHERBENEFITS = 10;
                const int FLD_JOBSTATUS = 11;
                const int FLD_JOBDURATIONLOOKUPID = 12;
                const int FLD_JOBDURATIONMONTH = 13;
                const int FLD_JOBADDRESS1 = 14;
                const int FLD_JOBADDRESS2 = 15;
                const int FLD_CITY = 16;
                const int FLD_ZIPCODE = 17;
                const int FLD_COUNTRYID = 18;
                const int FLD_STATEID = 19;
                const int FLD_STARTDATE = 20;
                const int FLD_FINALHIREDDATE = 21;
                const int FLD_JOBDESCRIPTION = 22;
                const int FLD_AUTHORIZATIONTYPELOOKUPID = 23;
                const int FLD_POSTEDDATE = 24;
                const int FLD_ACTIVATIONDATE = 25;
                const int FLD_REQUIREDDEGREELOOKUPID = 26;
                const int FLD_MINEXPREQUIRED = 27;
                const int FLD_MAXEXPREQUIRED = 28;
                //  const int FLD_VENDORGROUPID = 29;
                const int FLD_ISJOBACTIVE = 29;
                //  const int FLD_PUBLISHEDFORINTERNAL = 31;
                //  const int FLD_PUBLISHEDFORPUBLIC = 32;
                //   const int FLD_PUBLISHEDFORPARTNER = 33;
                //  const int FLD_MINAGEREQUIRED = 34;
                //  const int FLD_MAXAGEREQUIRED = 35;
                const int FLD_JOBTYPE = 30;
                const int FLD_ISAPPROVALREQUIRED = 31;
                const int FLD_INTERNALNOTE = 32;
                const int FLD_CLIENTID = 33;
                //  const int FLD_CLIENTPROJECTID = 40;
                //  const int FLD_CLIENTPROJECT = 41;
                //   const int FLD_CLIENTENDCLIENTID = 42;
                const int FLD_CLIENTHOURLYRATE = 34;
                const int FLD_CLIENTHOURLYRATECURRENCYLOOKUPID = 35;
                const int FLD_CLIENTRATEPAYCYCLE = 36;
                //   const int FLD_CLIENTDISPLAYNAME = 46;
                //  const int FLD_CLIENTID2 = 47;
                //  const int FLD_CLIENTID3 = 48;
                //  const int FLD_CLIENTJOBDESCRIPTION = 49;
                const int FLD_TAXTERMLOOKUPIDS = 37;
                const int FLD_JOBCATEGORYLOOKUPID = 38;
                //   const int FLD_JOBCATEGORYSUBID = 52;
                //   const int FLD_JOBINDUSTRYLOOKUPID = 53;
                const int FLD_EXPECTEDREVENUE = 39;
                const int FLD_EXPECTEDREVENUECURRENCYLOOKUPID = 40;
                //    const int FLD_SOURCINGCHANNEL = 56;
                //   const int FLD_SOURCINGEXPENSES = 57;
                //   const int FLD_SOURCINGEXPENSESCURRENCYLOOKUPID = 58;
                const int FLD_WORKFLOWAPPROVED = 41;
                //   const int FLD_PUBLISHEDFORVENDOR = 60;
                const int FLD_TELECOMMUNICATION = 42;
                const int FLD_ISTEMPLATE = 43;
                const int FLD_ISREMOVED = 44;
                const int FLD_CREATORID = 45;
                const int FLD_UPDATORID = 46;
                const int FLD_CREATEDATE = 47;
                const int FLD_UPDATEDATE = 48;
                const int FLD_ISEXPENSESPAID = 49;      //0.1
                const int FLD_RAWDESCRIPTION = 50;
                const int FLD_JOBDEPARTMENTLOOKUPID = 51;
                const int FLD_JOBSKILLLOOKUPID = 52;

                const int FLD_OCCUPATIONALSERIESLOOKUPID = 53;
                const int FLD_PAYGRADELOOKUPID = 54;
                const int FLD_WORKSCHEDULELOOKUPID = 55;
                const int FLD_SECURITUCLEARANCE = 56;
                const int FLD_CLIENTCONTACTID = 57;
                const int FLD_REPORTINGTO = 58;
                const int FLD_NOOFREPORTEES = 59;
                const int FLD_MINIMUMQUALIFYINGPARAMETERS = 60;
                const int FLD_MAXPAYRATE = 61;
                const int FLD_ALLOWRECRUITERTOCHANGESTATUS = 62;
                const int FLD_SHOWINCANDIDATEPORTAL = 63;
                const int FLD_SHOWINEMPLOYEEREFERRALPORTAL = 64;
                const int FLD_DISPLAYREQUISITIONINVENDORPORTAL = 65;
                const int FLD_OPENDATE = 66;
                const int FLD_SALESREGIONLOOKUPID = 67;
                const int FLD_SALESGROUPLOOKUPID = 68;

                const int FLD_CUSTOMERNAME = 69;
                const int FLD_POAVAILABILTY = 70;
                const int FLD_JOBLOCATION = 71;

                const int FLD_JOBLOCATIONNAME = 72;
                const int FLD_SALESREGIONNAME = 73;
                const int FLD_SALESGROUPNAME = 74;
                const int FLD_BUCONTACTNAME = 75;
                const int FLD_JOBCATEGORYNAME = 76;

                const int FLD_EMPLOYEMENTTYPTLOOKUOID = 77;
                const int FLD_CLIENTBRIEF = 78;
                const int FLD_TURNARROUNDTIME = 79;
                const int FLD_SUBMISSIONSCOUNT = 80;
                const int FLD_OFFERDETAILSCOUNT = 81;
                const int FLD_JOININGDETAILSCOUNT = 82;
                const int FLD_INTERVIEWCOUNT = 83;

                const int FLD_NoOfUnfilledOpenings = 84;
                const int FLD_REQUISITIONSOURCE = 85;
                const int FLD_EMAILSUBJECT = 86;

                const int FLD_InterviewCandidateName = 87;
                const int FLD_SubmittedCandidateName = 88;
                const int FLD_OfferedCandidateName = 89;
                const int FLD_JoinedCandidateName = 90;
                const int FLD_JobPostNotes = 91;
                const int FLD_RequisitionType = 92;


                //const int FLD_REQUISITIONSOURCE = 69;
                //const int FLD_EMAILSUBJECT = 70;

                jobPosting.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                jobPosting.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
                jobPosting.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
                jobPosting.ClientJobId = reader.IsDBNull(FLD_CLIENTJOBID) ? string.Empty : reader.GetString(FLD_CLIENTJOBID);
                jobPosting.NoOfOpenings = reader.IsDBNull(FLD_NOOFOPENINGS) ? 0 : reader.GetInt32(FLD_NOOFOPENINGS);
                jobPosting.PayRate = reader.IsDBNull(FLD_PAYRATE) ? string.Empty : reader.GetString(FLD_PAYRATE);
                jobPosting.PayRateCurrencyLookupId = reader.IsDBNull(FLD_PAYRATECURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_PAYRATECURRENCYLOOKUPID);
                jobPosting.PayCycle = reader.IsDBNull(FLD_PAYCYCLE) ? string.Empty : reader.GetString(FLD_PAYCYCLE);
                jobPosting.TravelRequired = reader.IsDBNull(FLD_TRAVELREQUIRED) ? false : reader.GetBoolean(FLD_TRAVELREQUIRED);
                jobPosting.TravelRequiredPercent = reader.IsDBNull(FLD_TRAVELREQUIREDPERCENT) ? string.Empty : reader.GetString(FLD_TRAVELREQUIREDPERCENT);
                jobPosting.OtherBenefits = reader.IsDBNull(FLD_OTHERBENEFITS) ? string.Empty : reader.GetString(FLD_OTHERBENEFITS);
                jobPosting.JobStatus = reader.IsDBNull(FLD_JOBSTATUS) ? 0 : reader.GetInt32(FLD_JOBSTATUS);
                jobPosting.JobDurationLookupId = reader.IsDBNull(FLD_JOBDURATIONLOOKUPID) ? string.Empty : reader.GetString(FLD_JOBDURATIONLOOKUPID);
                jobPosting.JobDurationMonth = reader.IsDBNull(FLD_JOBDURATIONMONTH) ? string.Empty : reader.GetString(FLD_JOBDURATIONMONTH);
                jobPosting.JobAddress1 = reader.IsDBNull(FLD_JOBADDRESS1) ? string.Empty : reader.GetString(FLD_JOBADDRESS1);
                jobPosting.JobAddress2 = reader.IsDBNull(FLD_JOBADDRESS2) ? string.Empty : reader.GetString(FLD_JOBADDRESS2);
                jobPosting.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
                jobPosting.ZipCode = reader.IsDBNull(FLD_ZIPCODE) ? string.Empty : reader.GetString(FLD_ZIPCODE);
                jobPosting.CountryId = reader.IsDBNull(FLD_COUNTRYID) ? 0 : reader.GetInt32(FLD_COUNTRYID);
                jobPosting.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
                jobPosting.StartDate = reader.IsDBNull(FLD_STARTDATE) ? string.Empty : reader.GetString(FLD_STARTDATE);
                jobPosting.FinalHiredDate = reader.IsDBNull(FLD_FINALHIREDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_FINALHIREDDATE);
                jobPosting.JobDescription = reader.IsDBNull(FLD_JOBDESCRIPTION) ? string.Empty : reader.GetString(FLD_JOBDESCRIPTION);
                jobPosting.AuthorizationTypeLookupId = reader.IsDBNull(FLD_AUTHORIZATIONTYPELOOKUPID) ? string.Empty : reader.GetString(FLD_AUTHORIZATIONTYPELOOKUPID);
                jobPosting.PostedDate = reader.IsDBNull(FLD_POSTEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_POSTEDDATE);
                jobPosting.ActivationDate = reader.IsDBNull(FLD_ACTIVATIONDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ACTIVATIONDATE);
                jobPosting.RequiredDegreeLookupId = reader.IsDBNull(FLD_REQUIREDDEGREELOOKUPID) ? string.Empty : reader.GetString(FLD_REQUIREDDEGREELOOKUPID);
                jobPosting.MinExpRequired = reader.IsDBNull(FLD_MINEXPREQUIRED) ? string.Empty : reader.GetString(FLD_MINEXPREQUIRED);
                jobPosting.MaxExpRequired = reader.IsDBNull(FLD_MAXEXPREQUIRED) ? string.Empty : reader.GetString(FLD_MAXEXPREQUIRED);
                // jobPosting.VendorGroupId = reader.IsDBNull(FLD_VENDORGROUPID) ? 0 : reader.GetInt32(FLD_VENDORGROUPID);
                jobPosting.IsJobActive = reader.IsDBNull(FLD_ISJOBACTIVE) ? false : reader.GetBoolean(FLD_ISJOBACTIVE);
                //jobPosting.PublishedForInternal = reader.IsDBNull(FLD_PUBLISHEDFORINTERNAL) ? false : reader.GetBoolean(FLD_PUBLISHEDFORINTERNAL);
                //jobPosting.PublishedForPublic = reader.IsDBNull(FLD_PUBLISHEDFORPUBLIC) ? false : reader.GetBoolean(FLD_PUBLISHEDFORPUBLIC);
                //jobPosting.PublishedForPartner = reader.IsDBNull(FLD_PUBLISHEDFORPARTNER) ? false : reader.GetBoolean(FLD_PUBLISHEDFORPARTNER);
                //jobPosting.MinAgeRequired = reader.IsDBNull(FLD_MINAGEREQUIRED) ? 0 : reader.GetInt32(FLD_MINAGEREQUIRED);
                //jobPosting.MaxAgeRequired = reader.IsDBNull(FLD_MAXAGEREQUIRED) ? 0 : reader.GetInt32(FLD_MAXAGEREQUIRED);
                jobPosting.JobType = reader.IsDBNull(FLD_JOBTYPE) ? 0 : reader.GetInt32(FLD_JOBTYPE);
                jobPosting.IsApprovalRequired = reader.IsDBNull(FLD_ISAPPROVALREQUIRED) ? false : reader.GetBoolean(FLD_ISAPPROVALREQUIRED);
                jobPosting.InternalNote = reader.IsDBNull(FLD_INTERNALNOTE) ? string.Empty : reader.GetString(FLD_INTERNALNOTE);
                jobPosting.ClientId = reader.IsDBNull(FLD_CLIENTID) ? 0 : reader.GetInt32(FLD_CLIENTID);
                // jobPosting.ClientProjectId = reader.IsDBNull(FLD_CLIENTPROJECTID) ? 0 : reader.GetInt32(FLD_CLIENTPROJECTID);
                // jobPosting.ClientProject = reader.IsDBNull(FLD_CLIENTPROJECT) ? string.Empty : reader.GetString(FLD_CLIENTPROJECT);
                // jobPosting.ClientEndClientId = reader.IsDBNull(FLD_CLIENTENDCLIENTID) ? 0 : reader.GetInt32(FLD_CLIENTENDCLIENTID);
                jobPosting.ClientHourlyRate = reader.IsDBNull(FLD_CLIENTHOURLYRATE) ? string.Empty : reader.GetString(FLD_CLIENTHOURLYRATE);
                jobPosting.ClientHourlyRateCurrencyLookupId = reader.IsDBNull(FLD_CLIENTHOURLYRATECURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CLIENTHOURLYRATECURRENCYLOOKUPID);
                jobPosting.ClientRatePayCycle = reader.IsDBNull(FLD_CLIENTRATEPAYCYCLE) ? string.Empty : reader.GetString(FLD_CLIENTRATEPAYCYCLE);
                //  jobPosting.ClientDisplayName = reader.IsDBNull(FLD_CLIENTDISPLAYNAME) ? string.Empty : reader.GetString(FLD_CLIENTDISPLAYNAME);
                //  jobPosting.ClientId2 = reader.IsDBNull(FLD_CLIENTID2) ? 0 : reader.GetInt32(FLD_CLIENTID2);
                //  jobPosting.ClientId3 = reader.IsDBNull(FLD_CLIENTID3) ? 0 : reader.GetInt32(FLD_CLIENTID3);
                //   jobPosting.ClientJobDescription = reader.IsDBNull(FLD_CLIENTJOBDESCRIPTION) ? string.Empty : reader.GetString(FLD_CLIENTJOBDESCRIPTION);
                jobPosting.TaxTermLookupIds = reader.IsDBNull(FLD_TAXTERMLOOKUPIDS) ? string.Empty : reader.GetString(FLD_TAXTERMLOOKUPIDS);
                jobPosting.JobCategoryLookupId = reader.IsDBNull(FLD_JOBCATEGORYLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBCATEGORYLOOKUPID);
                //   jobPosting.JobCategorySubId = reader.IsDBNull(FLD_JOBCATEGORYSUBID) ? 0 : reader.GetInt32(FLD_JOBCATEGORYSUBID);
                //   jobPosting.JobIndustryLookupId = reader.IsDBNull(FLD_JOBINDUSTRYLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBINDUSTRYLOOKUPID);
                jobPosting.ExpectedRevenue = reader.IsDBNull(FLD_EXPECTEDREVENUE) ? 0 : reader.GetDecimal(FLD_EXPECTEDREVENUE);
                jobPosting.ExpectedRevenueCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDREVENUECURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDREVENUECURRENCYLOOKUPID);
                //   jobPosting.SourcingChannel = reader.IsDBNull(FLD_SOURCINGCHANNEL) ? string.Empty : reader.GetString(FLD_SOURCINGCHANNEL);
                //    jobPosting.SourcingExpenses = reader.IsDBNull(FLD_SOURCINGEXPENSES) ? 0 : reader.GetDecimal(FLD_SOURCINGEXPENSES);
                //    jobPosting.SourcingExpensesCurrencyLookupId = reader.IsDBNull(FLD_SOURCINGEXPENSESCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_SOURCINGEXPENSESCURRENCYLOOKUPID);
                jobPosting.WorkflowApproved = reader.IsDBNull(FLD_WORKFLOWAPPROVED) ? false : reader.GetBoolean(FLD_WORKFLOWAPPROVED);
                //   jobPosting.PublishedForVendor = reader.IsDBNull(FLD_PUBLISHEDFORVENDOR) ? false : reader.GetBoolean(FLD_PUBLISHEDFORVENDOR);
                jobPosting.TeleCommunication = reader.IsDBNull(FLD_TELECOMMUNICATION) ? false : reader.GetBoolean(FLD_TELECOMMUNICATION);
                jobPosting.IsTemplate = reader.IsDBNull(FLD_ISTEMPLATE) ? false : reader.GetBoolean(FLD_ISTEMPLATE);
                jobPosting.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
                jobPosting.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                jobPosting.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                jobPosting.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                jobPosting.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
                jobPosting.IsExpensesPaid = reader.IsDBNull(FLD_ISEXPENSESPAID) ? false : reader.GetBoolean(FLD_ISEXPENSESPAID);        //0.1
                jobPosting.RawDescription = reader.IsDBNull(FLD_RAWDESCRIPTION) ? string.Empty : reader.GetString(FLD_RAWDESCRIPTION);
                jobPosting.JobDepartmentLookUpId = reader.IsDBNull(FLD_JOBDEPARTMENTLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBDEPARTMENTLOOKUPID);
                jobPosting.JobSkillLookUpId = reader.IsDBNull(FLD_JOBSKILLLOOKUPID) ? string.Empty : reader.GetString(FLD_JOBSKILLLOOKUPID);
                jobPosting.OccuptionalSeriesLookupId = reader.IsDBNull(FLD_OCCUPATIONALSERIESLOOKUPID) ? 0 : reader.GetInt32(FLD_OCCUPATIONALSERIESLOOKUPID);
                jobPosting.PayGradeLookupId = reader.IsDBNull(FLD_PAYGRADELOOKUPID) ? 0 : reader.GetInt32(FLD_PAYGRADELOOKUPID);
                jobPosting.WorkScheduleLookupId = reader.IsDBNull(FLD_WORKSCHEDULELOOKUPID) ? 0 : reader.GetInt32(FLD_WORKSCHEDULELOOKUPID);
                jobPosting.SecurityClearance = reader.IsDBNull(FLD_SECURITUCLEARANCE) ? false : reader.GetBoolean(FLD_SECURITUCLEARANCE);
                jobPosting.ClientContactId = reader.IsDBNull(FLD_CLIENTCONTACTID) ? 0 : reader.GetInt32(FLD_CLIENTCONTACTID);
                // 0.3 starts
                jobPosting.ReportingTo = reader.IsDBNull(FLD_REPORTINGTO) ? string.Empty : reader.GetString(FLD_REPORTINGTO);
                jobPosting.NoOfReportees = reader.IsDBNull(FLD_NOOFREPORTEES) ? 0 : reader.GetInt32(FLD_NOOFREPORTEES);
                jobPosting.MinimumQualifyingParameters = reader.IsDBNull(FLD_MINIMUMQUALIFYINGPARAMETERS) ? string.Empty : reader.GetString(FLD_MINIMUMQUALIFYINGPARAMETERS);
                jobPosting.MaxPayRate = reader.IsDBNull(FLD_MAXPAYRATE) ? 0 : reader.GetDecimal(FLD_MAXPAYRATE);
                jobPosting.AllowRecruitersToChangeStatus = reader.IsDBNull(FLD_ALLOWRECRUITERTOCHANGESTATUS) ? false : reader.GetBoolean(FLD_ALLOWRECRUITERTOCHANGESTATUS);
                jobPosting.ShowInCandidatePortal = reader.IsDBNull(FLD_SHOWINCANDIDATEPORTAL) ? false : reader.GetBoolean(FLD_SHOWINCANDIDATEPORTAL);
                jobPosting.ShowInEmployeeReferralPortal = reader.IsDBNull(FLD_SHOWINEMPLOYEEREFERRALPORTAL) ? false : reader.GetBoolean(FLD_SHOWINEMPLOYEEREFERRALPORTAL);
                jobPosting.DisplayRequisitionInVendorPortal = reader.IsDBNull(FLD_DISPLAYREQUISITIONINVENDORPORTAL) ? false : reader.GetBoolean(FLD_DISPLAYREQUISITIONINVENDORPORTAL);
                jobPosting.OpenDate = reader.IsDBNull(FLD_OPENDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_OPENDATE);
                jobPosting.SalesRegionLookUpId = reader.IsDBNull(FLD_SALESREGIONLOOKUPID) ? 0 : reader.GetInt32(FLD_SALESREGIONLOOKUPID);
                jobPosting.SalesGroupLookUpId = reader.IsDBNull(FLD_SALESGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_SALESGROUPLOOKUPID);
                jobPosting.JobCategoryLookupId = reader.IsDBNull(FLD_JOBCATEGORYLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBCATEGORYLOOKUPID);
                jobPosting.CustomerName = reader.IsDBNull(FLD_CUSTOMERNAME) ? string.Empty : reader.GetString(FLD_CUSTOMERNAME);
                jobPosting.POAvailability = reader.IsDBNull(FLD_POAVAILABILTY) ? 0 : reader.GetInt32(FLD_POAVAILABILTY);
                jobPosting.JobLocationLookUpID = reader.IsDBNull(FLD_JOBLOCATION) ? 0 : reader.GetInt32(FLD_JOBLOCATION);

                jobPosting.JobLocationText = reader.IsDBNull(FLD_JOBLOCATIONNAME) ? string.Empty : reader.GetString(FLD_JOBLOCATIONNAME);
                jobPosting.SalesRegionText = reader.IsDBNull(FLD_SALESREGIONNAME) ? string.Empty : reader.GetString(FLD_SALESREGIONNAME);
                jobPosting.SalesGroupText = reader.IsDBNull(FLD_SALESGROUPNAME) ? string.Empty : reader.GetString(FLD_SALESGROUPNAME);
                jobPosting.BUContactText = reader.IsDBNull(FLD_BUCONTACTNAME) ? string.Empty : reader.GetString(FLD_BUCONTACTNAME);
                jobPosting.JobCategoryText = reader.IsDBNull(FLD_JOBCATEGORYNAME) ? string.Empty : reader.GetString(FLD_JOBCATEGORYNAME);

                jobPosting.EmployementTypeLookUpID = reader.IsDBNull(FLD_EMPLOYEMENTTYPTLOOKUOID) ? 0 : reader.GetInt32(FLD_EMPLOYEMENTTYPTLOOKUOID);
                jobPosting.ClientBrief = reader.IsDBNull(FLD_CLIENTBRIEF) ? string.Empty : reader.GetString(FLD_CLIENTBRIEF);
                jobPosting.TurnArroundTime = reader.IsDBNull(FLD_TURNARROUNDTIME) ? 0 : reader.GetInt32(FLD_TURNARROUNDTIME);
                jobPosting.SubmissionCount = reader.IsDBNull(FLD_SUBMISSIONSCOUNT) ? 0 : reader.GetInt32(FLD_SUBMISSIONSCOUNT);
                jobPosting.OfferDetailsCount = reader.IsDBNull(FLD_OFFERDETAILSCOUNT) ? 0 : reader.GetInt32(FLD_OFFERDETAILSCOUNT);
                jobPosting.JoiningDetailsCount = reader.IsDBNull(FLD_JOININGDETAILSCOUNT) ? 0 : reader.GetInt32(FLD_JOININGDETAILSCOUNT);
                jobPosting.InterViewCount = reader.IsDBNull(FLD_INTERVIEWCOUNT) ? 0 : reader.GetInt32(FLD_INTERVIEWCOUNT);
                jobPosting.NoOfUnfilledOpenings = reader.IsDBNull(FLD_NoOfUnfilledOpenings) ? 0 : reader.GetInt32(FLD_NoOfUnfilledOpenings);

                if (reader.FieldCount > FLD_InterviewCandidateName)
                {
                    jobPosting.InterviewCandidateName = reader.IsDBNull(FLD_InterviewCandidateName) ? string.Empty : reader.GetString(FLD_InterviewCandidateName);
                    jobPosting.SubmittedCandidateName = reader.IsDBNull(FLD_SubmittedCandidateName) ? string.Empty : reader.GetString(FLD_SubmittedCandidateName);
                    jobPosting.OfferedCandidateName = reader.IsDBNull(FLD_OfferedCandidateName) ? string.Empty : reader.GetString(FLD_OfferedCandidateName);
                    jobPosting.JoinedCandidateName = reader.IsDBNull(FLD_JoinedCandidateName) ? string.Empty : reader.GetString(FLD_JoinedCandidateName);
                    jobPosting.JobPostNotes = reader.IsDBNull(FLD_JobPostNotes) ? string.Empty : reader.GetString(FLD_JobPostNotes);
                    jobPosting.RequisitionType = reader.IsDBNull(FLD_RequisitionType) ? string.Empty : reader.GetString(FLD_RequisitionType);
                }


                if (reader.FieldCount > FLD_REQUISITIONSOURCE)
                {
                    jobPosting.RequisitionSource = reader.IsDBNull(FLD_REQUISITIONSOURCE) ? string.Empty : reader.GetString(FLD_REQUISITIONSOURCE);
                }

                if (reader.FieldCount > FLD_EMAILSUBJECT)
                {
                    jobPosting.EmailSubject = reader.IsDBNull(FLD_EMAILSUBJECT) ? string.Empty : reader.GetString(FLD_EMAILSUBJECT);
                }

                jobPosting.HiringMatrixLevels = new List<Hiring_Levels>();
                for (int i = FLD_RequisitionType + 1; i < reader.FieldCount; i++)
                {
                    Hiring_Levels levels = new Hiring_Levels();
                    levels.HiringMatrixLevelID = Convert.ToInt32(reader.GetName(i));
                    levels.CandidateCount = reader.IsDBNull(i) ? 0 : reader.GetInt32(i);
                    jobPosting.HiringMatrixLevels.Add(levels);
                }


            }


            return jobPosting;
        }
        public IList<Submission> BuildSubmissionReports(IDataReader reader)
        {
            List<Submission> Submission = new List<Submission>();

            while (reader.Read())
            {
                Submission.Add(this.BuildSubmissionReport(reader));
            }

            return (Submission.Count > 0) ? Submission : null;
        }

        public Submission BuildSubmissionReport(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_JOBTITLE = 1;
            const int FLD_JOBPOSTINGCODE = 2;
            const int FLD_JOBDURATIONMONTH = 3;
            const int FLD_JOBDURATIONLOOKUPID = 4;
            const int FLD_CITY = 5;
            const int FLD_JOBSTATUS = 6;
            const int FLD_STARTDATE = 7;
            const int FLD_POSTEDDATE = 8;
            const int FLD_CREATORID = 9;
            const int FLD_COMPANYNAME = 10;
            const int FLD_ACCOUNTCONTACT = 11;
            const int FLD_COMPANYSTATUS = 12;
            const int FLD_DATASUBMITTED = 13;
            const int FLD_SUBMISSIONEMAIL = 14;
            const int FLD_SUBMITTEDBY = 15;
            const int FLD_SUBMITTEDTO = 16;
            const int FLD_APPLICANTNAME = 17;
            const int FLD_APPLICANTTYPE = 18;
            const int FLD_MEMBERID = 19; //12410 
            const int FLD_CLIENTID = 20;
            const int FLD_MemberEmailId = 21;//12129


            Submission submission = new Submission();
            submission.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            submission.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            submission.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
            submission.JobDurationMonth = reader.IsDBNull(FLD_JOBDURATIONMONTH) ? string.Empty : reader.GetString(FLD_JOBDURATIONMONTH);
            submission.JobDurationLookupId = reader.IsDBNull(FLD_JOBDURATIONLOOKUPID) ? string.Empty : reader.GetString(FLD_JOBDURATIONLOOKUPID);
            submission.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
            submission.JobStatus = reader.IsDBNull(FLD_JOBSTATUS) ? 0 : reader.GetInt32(FLD_JOBSTATUS);
            submission.StartDate = reader.IsDBNull(FLD_STARTDATE) ? string.Empty : reader.GetString(FLD_STARTDATE);
            submission.PostedDate = reader.IsDBNull(FLD_POSTEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_POSTEDDATE);
            submission.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            submission.CompanyName = reader.IsDBNull(FLD_COMPANYNAME) ? string.Empty : reader.GetString(FLD_COMPANYNAME);
            submission.CompanyPrimaryEmail = reader.IsDBNull(FLD_ACCOUNTCONTACT) ? string.Empty : reader.GetString(FLD_ACCOUNTCONTACT);
            //submission.CompanyStatus = reader.IsDBNull(FLD_COMPANYSTATUS) ? 0 : reader.GetInt32(FLD_COMPANYSTATUS);
            submission.CompanyStatus = (CompanyStatus)(reader.IsDBNull(FLD_COMPANYSTATUS) ? 0 : reader.GetInt32(FLD_COMPANYSTATUS));

            submission.SubmissionDate = reader.IsDBNull(FLD_DATASUBMITTED) ? DateTime.MinValue : reader.GetDateTime(FLD_DATASUBMITTED);
            submission.SubmissionPrimaryEmail = reader.IsDBNull(FLD_SUBMISSIONEMAIL) ? string.Empty : reader.GetString(FLD_SUBMISSIONEMAIL);
            submission.SubmittedBy = reader.IsDBNull(FLD_SUBMITTEDBY) ? string.Empty : reader.GetString(FLD_SUBMITTEDBY);
            submission.SubmittedTo = reader.IsDBNull(FLD_SUBMITTEDTO) ? string.Empty : reader.GetString(FLD_SUBMITTEDTO);
            submission.ApplicantName = reader.IsDBNull(FLD_APPLICANTNAME) ? string.Empty : reader.GetString(FLD_APPLICANTNAME);
            submission.MemberType = reader.IsDBNull(FLD_APPLICANTTYPE) ? 0 : reader.GetInt32(FLD_APPLICANTTYPE);
            submission.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);//12410
            submission.ClientId = reader.IsDBNull(FLD_CLIENTID) ? 0 : reader.GetInt32(FLD_CLIENTID);//12410
            submission.MemberEmailId = reader.IsDBNull(FLD_MemberEmailId) ? 0 : reader.GetInt32(FLD_MemberEmailId);//12129

            return submission;
        }
    }
}