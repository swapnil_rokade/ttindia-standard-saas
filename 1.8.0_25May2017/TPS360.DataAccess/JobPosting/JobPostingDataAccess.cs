﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: JobPostingDataAccess.cs
    Description: This page is used for Job posting functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Nov-13-2008           Yogeesh Bhat           Defect ID:9180; Added new entity IsExpensesPaid
    0.2            Nov-27-2008           Gopala Swamy           Defect Id:9336; Added new parameter called "@JobPostingCode" to the function "CreateFromExistingJob"
    0.3            Dec-10-2008           Yogeesh Bhat           Defect# 9398: Added new methods GetRequisitionCountByMemberId, UpdateMemberRequisitionCount
    0.4            Feb-17-2009           Gopala Swamy           Defect Id:9927 Modified a line
    0.5            Feb-20-2009           Rajendra               Defect Id:9811 Added an parameter "string jobIndustry" for overload method 'GetPagedVolumeHire'.   
    0.6            Mar-06-2009           Yogeesh Bhat           Defect Id: 10049: Added new parameters RequisitionSource and EmailSubject
    0.7            Mar-10-2009           Nagarathna V.B         Defect Id:10068:  added an overload method "".
    0.8            Apr-08-2009           Jagadish               Defect Id:10269; Changes made in method 'GetReport()'.
 *  0.9            May-12-2009           Sandeesh               Defect id:10440 :Populated the Requisition status dropdown list from the database
    1.0            Jul-01-2009           Nagarathna V.B         Enhancement 10778: checking Jobstatus not equal to zero in "IJobPostingDataAccess" method.
    1.1            Nov-25-2009           Gopala Swamy J         Defect Id:11588 :Added one parameter
 * 1.2             Dec-03-2009           Sandeesh               Defect Id:10958 - To dispaly only the Requisitions created by current user in  'My Requisition List' page
 * 1.3             Dec-08-2009           Sandeesh               Defect id: 11988 - To return a proper generic list for the methods -GetAllByStatusAndManagerId,GetAllByStatus
 * 1.4             22/May/2015           Prasanth Kumar G       Introduced GetRequisitionAgingReport
 * 1.5             05/June/2015          Prasanth Kumar G       Changed PostedDate to OpenDate 
 * 1.6             8/Jan/2016            pravin khot            Introduced by MemberCareerOpportunities
 * 1.6            18/Jan/2016           pravin khot             Introduced by AddCareerJob 
 * 1.7             29/Jan/2016           pravin khot            Introduced by GetAllByBUID
 * 1.8             2/Feb/2016            Pravin khot            Introduced by GetRequisitionSourceBreakupReport
 * 1.9             25/May/2016           pravin khot            added- GetRequisitionStatusBy_Id,GetTimeZoneId_MemberId,GetRequisitionStatus_Id

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingDataAccess : BaseDataAccess, IJobPostingDataAccess
    {
        #region Constructors

        public JobPostingDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<JobPosting> CreateEntityBuilder<JobPosting>()
        {
            return (new JobPostingBuilder()) as IEntityBuilder<JobPosting>;
        }

        #endregion

        #region  Methods

        JobPosting IJobPostingDataAccess.Add(JobPosting jobPosting)
        {
            const string SP = "dbo.JobPosting_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobTitle", DbType.AnsiString, StringHelper.Convert(jobPosting.JobTitle));
                Database.AddInParameter(cmd, "@JobPostingCode", DbType.AnsiString, StringHelper.Convert(jobPosting.JobPostingCode));
                Database.AddInParameter(cmd, "@ClientJobId", DbType.AnsiString, StringHelper.Convert(jobPosting.ClientJobId));
                Database.AddInParameter(cmd, "@NoOfOpenings", DbType.Int32, jobPosting.NoOfOpenings);
                Database.AddInParameter(cmd, "@PayRate", DbType.AnsiString, StringHelper.Convert(jobPosting.PayRate));
                Database.AddInParameter(cmd, "@PayRateCurrencyLookupId", DbType.Int32, jobPosting.PayRateCurrencyLookupId);
                Database.AddInParameter(cmd, "@PayCycle", DbType.AnsiString, StringHelper.Convert(jobPosting.PayCycle));
                Database.AddInParameter(cmd, "@TravelRequired", DbType.Boolean, jobPosting.TravelRequired);
                Database.AddInParameter(cmd, "@TravelRequiredPercent", DbType.AnsiString, StringHelper.Convert(jobPosting.TravelRequiredPercent));
                Database.AddInParameter(cmd, "@OtherBenefits", DbType.AnsiString, StringHelper.Convert(jobPosting.OtherBenefits));
                Database.AddInParameter(cmd, "@JobStatus", DbType.Int32, jobPosting.JobStatus);
                Database.AddInParameter(cmd, "@JobDurationLookupId", DbType.AnsiString ,StringHelper .Convert ( jobPosting.JobDurationLookupId));
                Database.AddInParameter(cmd, "@JobDurationMonth", DbType.AnsiString, StringHelper.Convert(jobPosting.JobDurationMonth));
                Database.AddInParameter(cmd, "@JobAddress1", DbType.AnsiString, StringHelper.Convert(jobPosting.JobAddress1));
                Database.AddInParameter(cmd, "@JobAddress2", DbType.AnsiString, StringHelper.Convert(jobPosting.JobAddress2));
                Database.AddInParameter(cmd, "@City", DbType.AnsiString, StringHelper.Convert(jobPosting.City));
                Database.AddInParameter(cmd, "@ZipCode", DbType.AnsiString, StringHelper.Convert(jobPosting.ZipCode));
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, jobPosting.CountryId);
                Database.AddInParameter(cmd, "@StateId", DbType.Int32, jobPosting.StateId);
                Database.AddInParameter(cmd, "@StartDate", DbType.AnsiString, StringHelper.Convert(jobPosting.StartDate));
                Database.AddInParameter(cmd, "@FinalHiredDate", DbType.DateTime, NullConverter.Convert(jobPosting.FinalHiredDate));
                Database.AddInParameter(cmd, "@JobDescription", DbType.AnsiString, StringHelper.Convert(jobPosting.JobDescription));
                Database.AddInParameter(cmd, "@AuthorizationTypeLookupId", DbType.AnsiString, StringHelper.Convert(jobPosting.AuthorizationTypeLookupId));
                Database.AddInParameter(cmd, "@PostedDate", DbType.DateTime, NullConverter.Convert(jobPosting.PostedDate));
                Database.AddInParameter(cmd, "@ActivationDate", DbType.DateTime, NullConverter.Convert(jobPosting.ActivationDate));
                Database.AddInParameter(cmd, "@RequiredDegreeLookupId", DbType.AnsiString ,StringHelper .Convert ( jobPosting.RequiredDegreeLookupId));
                Database.AddInParameter(cmd, "@MinExpRequired", DbType.AnsiString, StringHelper.Convert(jobPosting.MinExpRequired));
                Database.AddInParameter(cmd, "@MaxExpRequired", DbType.AnsiString, StringHelper.Convert(jobPosting.MaxExpRequired));
                //Database.AddInParameter(cmd, "@VendorGroupId", DbType.Int32, jobPosting.VendorGroupId);
                Database.AddInParameter(cmd, "@IsJobActive", DbType.Boolean, jobPosting.IsJobActive);
                //Database.AddInParameter(cmd, "@PublishedForInternal", DbType.Boolean, jobPosting.PublishedForInternal);
                //Database.AddInParameter(cmd, "@PublishedForPublic", DbType.Boolean, jobPosting.PublishedForPublic);
                //Database.AddInParameter(cmd, "@PublishedForPartner", DbType.Boolean, jobPosting.PublishedForPartner);
               // Database.AddInParameter(cmd, "@MinAgeRequired", DbType.Int32, jobPosting.MinAgeRequired);
               // Database.AddInParameter(cmd, "@MaxAgeRequired", DbType.Int32, jobPosting.MaxAgeRequired);
                Database.AddInParameter(cmd, "@JobType", DbType.Int32, jobPosting.JobType);
                Database.AddInParameter(cmd, "@IsApprovalRequired", DbType.Boolean, jobPosting.IsApprovalRequired);
                Database.AddInParameter(cmd, "@InternalNote", DbType.AnsiString, StringHelper.Convert(jobPosting.InternalNote));
                Database.AddInParameter(cmd, "@ClientId", DbType.Int32, jobPosting.ClientId);
               // Database.AddInParameter(cmd, "@ClientProjectId", DbType.Int32, jobPosting.ClientProjectId);
               // Database.AddInParameter(cmd, "@ClientProject", DbType.AnsiString, StringHelper.Convert(jobPosting.ClientProject));
                //Database.AddInParameter(cmd, "@ClientEndClientId", DbType.Int32, jobPosting.ClientEndClientId);
                Database.AddInParameter(cmd, "@ClientHourlyRate", DbType.AnsiString, StringHelper.Convert(jobPosting.ClientHourlyRate));
                Database.AddInParameter(cmd, "@ClientHourlyRateCurrencyLookupId", DbType.Int32, jobPosting.ClientHourlyRateCurrencyLookupId);
                Database.AddInParameter(cmd, "@ClientRatePayCycle", DbType.AnsiString, StringHelper.Convert(jobPosting.ClientRatePayCycle));
               // Database.AddInParameter(cmd, "@ClientDisplayName", DbType.AnsiString, StringHelper.Convert(jobPosting.ClientDisplayName));
               // Database.AddInParameter(cmd, "@ClientId2", DbType.Int32, jobPosting.ClientId2);
               // Database.AddInParameter(cmd, "@ClientId3", DbType.Int32, jobPosting.ClientId3);
               // Database.AddInParameter(cmd, "@ClientJobDescription", DbType.AnsiString, StringHelper.Convert(jobPosting.ClientJobDescription));
                Database.AddInParameter(cmd, "@TaxTermLookupIds", DbType.AnsiString, StringHelper.Convert(jobPosting.TaxTermLookupIds));
                Database.AddInParameter(cmd, "@JobCategoryLookupId", DbType.Int32, jobPosting.JobCategoryLookupId);
               // Database.AddInParameter(cmd, "@JobCategorySubId", DbType.Int32, jobPosting.JobCategorySubId);
               // Database.AddInParameter(cmd, "@JobIndustryLookupId", DbType.Int32, jobPosting.JobIndustryLookupId);
                Database.AddInParameter(cmd, "@ExpectedRevenue", DbType.Decimal, jobPosting.ExpectedRevenue);
                Database.AddInParameter(cmd, "@ExpectedRevenueCurrencyLookupId", DbType.Int32, jobPosting.ExpectedRevenueCurrencyLookupId);
                //Database.AddInParameter(cmd, "@SourcingChannel", DbType.AnsiString, StringHelper.Convert(jobPosting.SourcingChannel));
               // Database.AddInParameter(cmd, "@SourcingExpenses", DbType.Decimal, jobPosting.SourcingExpenses);
               // Database.AddInParameter(cmd, "@SourcingExpensesCurrencyLookupId", DbType.Int32, jobPosting.SourcingExpensesCurrencyLookupId);
                Database.AddInParameter(cmd, "@WorkflowApproved", DbType.Boolean, jobPosting.WorkflowApproved);
               // Database.AddInParameter(cmd, "@PublishedForVendor", DbType.Boolean, jobPosting.PublishedForVendor);
                Database.AddInParameter(cmd, "@TeleCommunication", DbType.Boolean, jobPosting.TeleCommunication);
                Database.AddInParameter(cmd, "@IsTemplate", DbType.Boolean, jobPosting.IsTemplate);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, jobPosting.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, jobPosting.CreatorId);
                Database.AddInParameter(cmd, "@IsExpensesPaid", DbType.Boolean, jobPosting.IsExpensesPaid);     //0.1
                Database.AddInParameter(cmd, "@RawDescription", DbType.AnsiString, jobPosting.RawDescription);
                Database.AddInParameter(cmd, "@JobDepartmentLookUpId", DbType.Int32, jobPosting.JobDepartmentLookUpId);
                Database.AddInParameter(cmd, "@RequisitionSource", DbType.AnsiString, jobPosting.RequisitionSource);
                Database.AddInParameter(cmd, "@EmailSubject", DbType.AnsiString, jobPosting.EmailSubject);
                Database.AddInParameter(cmd, "@SkillLookupId", DbType.AnsiString, jobPosting.JobSkillLookUpId );
                Database.AddInParameter(cmd, "@OccupationalSeriesLookupId", DbType.Int32 , jobPosting.OccuptionalSeriesLookupId );
                Database.AddInParameter(cmd, "@PayGradeLookupId", DbType.Int32 , jobPosting.PayGradeLookupId );
                Database.AddInParameter(cmd, "@WorkScheduleLookupId", DbType.Int32 , jobPosting.WorkScheduleLookupId );
                Database.AddInParameter(cmd, "@SecurityClearance", DbType.Boolean , jobPosting.SecurityClearance );
                Database.AddInParameter(cmd, "@ClientContactId", DbType.Int32 , jobPosting.ClientContactId );

                Database.AddInParameter(cmd, "@ReportingTo", DbType.AnsiString , jobPosting.ReportingTo );
                Database.AddInParameter(cmd, "@NoOfReportees", DbType.Int32, jobPosting.NoOfReportees );
                Database.AddInParameter(cmd, "@MQP", DbType.AnsiString , jobPosting.MinimumQualifyingParameters );
                Database.AddInParameter(cmd, "@MaxPayRate", DbType.Decimal , jobPosting.MaxPayRate );
                Database.AddInParameter(cmd, "@AllowRecruitersToChangeStatus", DbType.Boolean, jobPosting.AllowRecruitersToChangeStatus);
                Database.AddInParameter(cmd, "@ClientBrief", DbType.AnsiString , jobPosting.ClientBrief );
                Database.AddInParameter(cmd, "@ShowInCandidatePortal", DbType.Boolean, jobPosting.ShowInCandidatePortal);
                Database.AddInParameter(cmd, "@ShowInEmployeeReferralPortal", DbType.Boolean, jobPosting.ShowInEmployeeReferralPortal);
                Database.AddInParameter(cmd, "@DisplayRequisitionInVendorPortal", DbType.Boolean, jobPosting.DisplayRequisitionInVendorPortal);
                Database.AddInParameter(cmd, "@OpenDate", DbType.DateTime, jobPosting.OpenDate);
                Database.AddInParameter(cmd, "@SalesRegionLookUpId", DbType.Int32, jobPosting.SalesRegionLookUpId);
                Database.AddInParameter(cmd, "@SalesGroupLookUpId", DbType.Int32, jobPosting.SalesGroupLookUpId);
                Database.AddInParameter(cmd, "@CustomerName", DbType.String, jobPosting.CustomerName);
                Database.AddInParameter(cmd, "@POAvailability", DbType.Int32, jobPosting.POAvailability);
                Database.AddInParameter(cmd, "@JobLocationLookUpID", DbType.Int32, jobPosting.JobLocationLookUpID);
                Database.AddInParameter(cmd, "@EmployementTypeLookUpID", DbType.Int32, jobPosting.EmployementTypeLookUpID);
                Database.AddInParameter(cmd, "@VendorList", DbType.String, jobPosting.VendorList);
                Database.AddInParameter(cmd, "@RequisitionType", DbType.String, jobPosting.RequisitionType);

                //Database.AddInParameter(cmd, "@RequestionBranch", DbType.String, jobPosting.RequestionBranch);
                //Database.AddInParameter(cmd, "@RequestionGrade", DbType.String, jobPosting.RequestionGrade);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPosting = CreateEntityBuilder<JobPosting>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPosting = null;
                    }
                }

                if (jobPosting == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting already exists. Please specify another job posting.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this job posting.");
                            }
                    }
                }

                return jobPosting;
            }
        }

        void IJobPostingDataAccess.UpdateStatusById(int statusid, int jobpostingid, int updatorid)
        {
            const string SP = "dbo.JobPosting_UpdateStatusById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, jobpostingid );
                Database.AddInParameter(cmd, "@JobStatus", DbType.Int32, statusid );
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, updatorid );
                Database.ExecuteNonQuery(cmd );
            }
        }
        JobPosting IJobPostingDataAccess.Update(JobPosting jobPosting)
        {
            const string SP = "dbo.JobPosting_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, jobPosting.Id);
                Database.AddInParameter(cmd, "@JobTitle", DbType.AnsiString, StringHelper.Convert(jobPosting.JobTitle));
                Database.AddInParameter(cmd, "@JobPostingCode", DbType.AnsiString, StringHelper.Convert(jobPosting.JobPostingCode));
                Database.AddInParameter(cmd, "@ClientJobId", DbType.AnsiString, StringHelper.Convert(jobPosting.ClientJobId));
                Database.AddInParameter(cmd, "@NoOfOpenings", DbType.Int32, jobPosting.NoOfOpenings);
                Database.AddInParameter(cmd, "@PayRate", DbType.AnsiString, StringHelper.Convert(jobPosting.PayRate));
                Database.AddInParameter(cmd, "@PayRateCurrencyLookupId", DbType.Int32, jobPosting.PayRateCurrencyLookupId);
                Database.AddInParameter(cmd, "@PayCycle", DbType.AnsiString, StringHelper.Convert(jobPosting.PayCycle));
                Database.AddInParameter(cmd, "@TravelRequired", DbType.Boolean, jobPosting.TravelRequired);
                Database.AddInParameter(cmd, "@TravelRequiredPercent", DbType.AnsiString, StringHelper.Convert(jobPosting.TravelRequiredPercent));
                Database.AddInParameter(cmd, "@OtherBenefits", DbType.AnsiString, StringHelper.Convert(jobPosting.OtherBenefits));
                Database.AddInParameter(cmd, "@JobStatus", DbType.Int32, jobPosting.JobStatus);
                Database.AddInParameter(cmd, "@JobDurationLookupId", DbType.AnsiString ,StringHelper .Convert ( jobPosting.JobDurationLookupId));
                Database.AddInParameter(cmd, "@JobDurationMonth", DbType.AnsiString, StringHelper.Convert(jobPosting.JobDurationMonth));
                Database.AddInParameter(cmd, "@JobAddress1", DbType.AnsiString, StringHelper.Convert(jobPosting.JobAddress1));
                Database.AddInParameter(cmd, "@JobAddress2", DbType.AnsiString, StringHelper.Convert(jobPosting.JobAddress2));
                Database.AddInParameter(cmd, "@City", DbType.AnsiString, StringHelper.Convert(jobPosting.City));
                Database.AddInParameter(cmd, "@ZipCode", DbType.AnsiString, StringHelper.Convert(jobPosting.ZipCode));
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, jobPosting.CountryId);
                Database.AddInParameter(cmd, "@StateId", DbType.Int32, jobPosting.StateId);
                Database.AddInParameter(cmd, "@StartDate", DbType.AnsiString, StringHelper.Convert(jobPosting.StartDate));
                Database.AddInParameter(cmd, "@FinalHiredDate", DbType.DateTime, NullConverter.Convert(jobPosting.FinalHiredDate));
                Database.AddInParameter(cmd, "@JobDescription", DbType.AnsiString, StringHelper.Convert(jobPosting.JobDescription));
                Database.AddInParameter(cmd, "@AuthorizationTypeLookupId", DbType.AnsiString, StringHelper.Convert(jobPosting.AuthorizationTypeLookupId));
                Database.AddInParameter(cmd, "@PostedDate", DbType.DateTime, NullConverter.Convert(jobPosting.PostedDate));
                Database.AddInParameter(cmd, "@ActivationDate", DbType.DateTime, NullConverter.Convert(jobPosting.ActivationDate));
                Database.AddInParameter(cmd, "@RequiredDegreeLookupId", DbType.AnsiString ,StringHelper .Convert ( jobPosting.RequiredDegreeLookupId));
                Database.AddInParameter(cmd, "@MinExpRequired", DbType.AnsiString, StringHelper.Convert(jobPosting.MinExpRequired));
                Database.AddInParameter(cmd, "@MaxExpRequired", DbType.AnsiString, StringHelper.Convert(jobPosting.MaxExpRequired));
                //Database.AddInParameter(cmd, "@VendorGroupId", DbType.Int32, jobPosting.VendorGroupId);
                Database.AddInParameter(cmd, "@IsJobActive", DbType.Boolean, jobPosting.IsJobActive);
               // Database.AddInParameter(cmd, "@PublishedForInternal", DbType.Boolean, jobPosting.PublishedForInternal);
               // Database.AddInParameter(cmd, "@PublishedForPublic", DbType.Boolean, jobPosting.PublishedForPublic);
               // Database.AddInParameter(cmd, "@PublishedForPartner", DbType.Boolean, jobPosting.PublishedForPartner);
               // Database.AddInParameter(cmd, "@MinAgeRequired", DbType.Int32, jobPosting.MinAgeRequired);
               // Database.AddInParameter(cmd, "@MaxAgeRequired", DbType.Int32, jobPosting.MaxAgeRequired);
                Database.AddInParameter(cmd, "@JobType", DbType.Int32, jobPosting.JobType);
                Database.AddInParameter(cmd, "@IsApprovalRequired", DbType.Boolean, jobPosting.IsApprovalRequired);
                Database.AddInParameter(cmd, "@InternalNote", DbType.AnsiString, StringHelper.Convert(jobPosting.InternalNote));
                Database.AddInParameter(cmd, "@ClientId", DbType.Int32, jobPosting.ClientId);
               // Database.AddInParameter(cmd, "@ClientProjectId", DbType.Int32, jobPosting.ClientProjectId);
               // Database.AddInParameter(cmd, "@ClientProject", DbType.AnsiString, StringHelper.Convert(jobPosting.ClientProject));
               // Database.AddInParameter(cmd, "@ClientEndClientId", DbType.Int32, jobPosting.ClientEndClientId);
                Database.AddInParameter(cmd, "@ClientHourlyRate", DbType.AnsiString, StringHelper.Convert(jobPosting.ClientHourlyRate));
                Database.AddInParameter(cmd, "@ClientHourlyRateCurrencyLookupId", DbType.Int32, jobPosting.ClientHourlyRateCurrencyLookupId);
                Database.AddInParameter(cmd, "@ClientRatePayCycle", DbType.AnsiString, StringHelper.Convert(jobPosting.ClientRatePayCycle));
              //  Database.AddInParameter(cmd, "@ClientDisplayName", DbType.AnsiString, StringHelper.Convert(jobPosting.ClientDisplayName));
              //  Database.AddInParameter(cmd, "@ClientId2", DbType.Int32, jobPosting.ClientId2);
               // Database.AddInParameter(cmd, "@ClientId3", DbType.Int32, jobPosting.ClientId3);
               // Database.AddInParameter(cmd, "@ClientJobDescription", DbType.AnsiString, StringHelper.Convert(jobPosting.ClientJobDescription));
                Database.AddInParameter(cmd, "@TaxTermLookupIds", DbType.AnsiString, StringHelper.Convert(jobPosting.TaxTermLookupIds));
                Database.AddInParameter(cmd, "@JobCategoryLookupId", DbType.Int32, jobPosting.JobCategoryLookupId);
               // Database.AddInParameter(cmd, "@JobCategorySubId", DbType.Int32, jobPosting.JobCategorySubId);
               // Database.AddInParameter(cmd, "@JobIndustryLookupId", DbType.Int32, jobPosting.JobIndustryLookupId);
                Database.AddInParameter(cmd, "@ExpectedRevenue", DbType.Decimal, jobPosting.ExpectedRevenue);
                Database.AddInParameter(cmd, "@ExpectedRevenueCurrencyLookupId", DbType.Int32, jobPosting.ExpectedRevenueCurrencyLookupId);
               // Database.AddInParameter(cmd, "@SourcingChannel", DbType.AnsiString, StringHelper.Convert(jobPosting.SourcingChannel));
               // Database.AddInParameter(cmd, "@SourcingExpenses", DbType.Decimal, jobPosting.SourcingExpenses);
               // Database.AddInParameter(cmd, "@SourcingExpensesCurrencyLookupId", DbType.Int32, jobPosting.SourcingExpensesCurrencyLookupId);
                Database.AddInParameter(cmd, "@WorkflowApproved", DbType.Boolean, jobPosting.WorkflowApproved);
              //  Database.AddInParameter(cmd, "@PublishedForVendor", DbType.Boolean, jobPosting.PublishedForVendor);
                Database.AddInParameter(cmd, "@TeleCommunication", DbType.Boolean, jobPosting.TeleCommunication);
                Database.AddInParameter(cmd, "@IsTemplate", DbType.Boolean, jobPosting.IsTemplate);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, jobPosting.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, jobPosting.UpdatorId);
                Database.AddInParameter(cmd, "@IsExpensesPaid", DbType.Boolean, jobPosting.IsExpensesPaid);     //0.1
                Database.AddInParameter(cmd, "@RawDescription", DbType.AnsiString, jobPosting.RawDescription);
                Database.AddInParameter(cmd, "@JobDepartmentLookUpId", DbType.Int32, jobPosting.JobDepartmentLookUpId);
                Database.AddInParameter(cmd, "@RequisitionSource", DbType.AnsiString, StringHelper.Convert(jobPosting.RequisitionSource));
                Database.AddInParameter(cmd, "@EmailSubject", DbType.AnsiString, StringHelper.Convert(jobPosting.EmailSubject));
                Database.AddInParameter(cmd, "@SkillLookupId", DbType.AnsiString, jobPosting.JobSkillLookUpId);
                Database.AddInParameter(cmd, "@OccupationalSeriesLookupId", DbType.Int32, jobPosting.OccuptionalSeriesLookupId);
                Database.AddInParameter(cmd, "@PayGradeLookupId", DbType.Int32, jobPosting.PayGradeLookupId);
                Database.AddInParameter(cmd, "@WorkScheduleLookupId", DbType.Int32, jobPosting.WorkScheduleLookupId);
                Database.AddInParameter(cmd, "@SecurityClearance", DbType.Boolean, jobPosting.SecurityClearance);
                Database.AddInParameter(cmd, "@ClientContactId", DbType.Int32 , jobPosting.ClientContactId );

                Database.AddInParameter(cmd, "@ReportingTo", DbType.AnsiString, jobPosting.ReportingTo);
                Database.AddInParameter(cmd, "@NoOfReportees", DbType.Int32, jobPosting.NoOfReportees);
                Database.AddInParameter(cmd, "@MQP", DbType.AnsiString, jobPosting.MinimumQualifyingParameters);
                Database.AddInParameter(cmd, "@MaxPayRate", DbType.Decimal, jobPosting.MaxPayRate);
                Database.AddInParameter(cmd, "@AllowRecruitersToChangeStatus", DbType.Boolean, jobPosting.AllowRecruitersToChangeStatus);
                Database.AddInParameter(cmd, "@ClientBrief", DbType.AnsiString, jobPosting.ClientBrief);
                Database.AddInParameter(cmd, "@ShowInCandidatePortal", DbType.Boolean, jobPosting.ShowInCandidatePortal);
                Database.AddInParameter(cmd, "@ShowInEmployeeReferralPortal", DbType.Boolean, jobPosting.ShowInEmployeeReferralPortal );
                Database.AddInParameter(cmd, "@DisplayRequisitionInVendorPortal", DbType.Boolean, jobPosting.DisplayRequisitionInVendorPortal);
                Database.AddInParameter(cmd, "@OpenDate", DbType.DateTime, jobPosting.OpenDate);
                Database.AddInParameter(cmd, "@SalesRegionLookUpId", DbType.Int32, jobPosting.SalesRegionLookUpId);
                Database.AddInParameter(cmd, "@SalesGroupLookUpId", DbType.Int32, jobPosting.SalesGroupLookUpId);
                Database.AddInParameter(cmd, "@CustomerName", DbType.String, jobPosting.CustomerName);
                Database.AddInParameter(cmd, "@POAvailability", DbType.Int32, jobPosting.POAvailability);
                Database.AddInParameter(cmd, "@JobLocationLookUpID", DbType.Int32, jobPosting.JobLocationLookUpID);
                Database.AddInParameter(cmd, "@EmployementTypeLookUpID", DbType.Int32, jobPosting.EmployementTypeLookUpID);
                Database.AddInParameter(cmd, "@VendorList", DbType.String, jobPosting.VendorList);
                Database.AddInParameter(cmd, "@RequisitionType", DbType.String, jobPosting.RequisitionType);

                //Database.AddInParameter(cmd, "@RequestionBranch", DbType.String, jobPosting.RequestionBranch);
                //Database.AddInParameter(cmd, "@RequestionGrade", DbType.String, jobPosting.RequestionGrade);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPosting = CreateEntityBuilder<JobPosting>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPosting = null;
                    }
                }

                if (jobPosting == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting already exists. Please specify another job posting.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this job posting.");
                            }
                    }
                }

                return jobPosting;
            }
        }

        JobPosting IJobPostingDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPosting_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {   
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPosting>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        JobPosting IJobPostingDataAccess.GetByMemberIdAndJobPostingCode(int MemberId,string JobPostingCode)
        {
            if (MemberId < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPosting_GetByMemberIDAndJobPostingCode";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ManagerId", DbType.Int32, MemberId);
                Database.AddInParameter(cmd, "@JobPostingCode", DbType.AnsiString , JobPostingCode  );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPosting>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }



        IList<JobPosting> IJobPostingDataAccess.GetAll()
        {
            const string SP = "dbo.JobPosting_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {   
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPosting>().BuildEntities(reader);
                }
            }
        }

        IList<JobPosting> IJobPostingDataAccess.GetAllRequisitionByEmployeeId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.JobPosting_GetAllRequisitionByEmployeeId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPosting>().BuildEntities(reader);
                }
            }
        }

        IList<JobPosting> IJobPostingDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            
            const string SP = "dbo.JobPosting_GetAllJobPostingByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPosting>().BuildEntities(reader);
                }
            }
        }

        int IJobPostingDataAccess.GetCountOfJobPostingByStatusAndManagerId(int ManagerId)
          {
              const string SP = "dbo.JobPosting_GetCountOfJobPostingByStatusAndManagerId";
              using (DbCommand cmd = Database.GetStoredProcCommand(SP))
              {
                  Database.AddInParameter(cmd, "@ManagerId", DbType.Int32, ManagerId );
                  using (IDataReader reader = Database.ExecuteReader(cmd))
                  {
                      while (reader.Read())
                      {
                          return reader.GetValue(0)==null?0:Convert.ToInt32(reader.GetValue(0));
                      }
                      return 0;
                  }
              }
          }
        ArrayList  IJobPostingDataAccess.GetPagedJobPostingByStatusAndManagerId(int status, int ManagerId, int count, string SearchKey)
         {
             const string SP = "dbo.JobPosting_GetPagedByStausAndManagerID";
             string whereClause = string.Empty;
             if (ManagerId != 0) whereClause = " [J].[Id] IN (SELECT DISTINCT JobPostingId FROM dbo.JobPostingHiringTeam WHERE MemberId=" + ManagerId + ")  and ([J].[JobTitle] like '" + SearchKey + "%')  AND [J].[JobStatus]=" + status;
            else whereClause = " ([J].[JobTitle] like '" + SearchKey + "%')  AND [J].[JobStatus]=" + status;

             object[] paramValues = new object[] {	0,
													count ,
													StringHelper.Convert(whereClause ),
													StringHelper.Convert(""),
                                                    StringHelper.Convert("")
												};

             using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
             {

                 using (IDataReader reader = Database.ExecuteReader(cmd))
                 {
                     ArrayList jobPostingList = new ArrayList();

                     while (reader.Read())
                     {
                         // jobPostingList.Add(new { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                         jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) ,CreatorId = reader.GetInt32(2)});//1.3
                     }

                     return jobPostingList;
                 }

                 return null;
             }
         }

        PagedResponse<JobPosting> IJobPostingDataAccess.GetPaged(string allKeys, string anyKey, string jobTitle, string jobType, string city, string lastUpdateDate, int jobStatus, string jobIndustry, PagedRequest request)
        {
            const string SP = "dbo.JobPosting_GetPagedSearch";
            string whereClause = string.Empty;

            StringBuilder whereClauseSb = new StringBuilder();

            if (!StringHelper.IsBlank(allKeys) && !StringHelper.IsBlank(anyKey))
            {
                whereClauseSb.Append(BuildAllAndAnyKeyWordQuery(allKeys, anyKey));
            }
            else if (!StringHelper.IsBlank(allKeys) && StringHelper.IsBlank(anyKey))
            {
                whereClauseSb.Append(BuildAllKeyWordsQuery(allKeys));
            }
            else if (StringHelper.IsBlank(allKeys) && !StringHelper.IsBlank(anyKey))
            {
                whereClauseSb.Append(BuildAnyKeyWordQuery(anyKey));
            }

            if (!string.IsNullOrEmpty(jobTitle))
            {
                if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
                {
                    whereClauseSb.Append(" AND [J].[JobTitle] LIKE '%" + jobTitle.Trim() + "%'");
                }
                else
                {
                    whereClauseSb.Append(" [J].[JobTitle] LIKE '%" + jobTitle.Trim() + "%'");
                }
            }

            if (jobType != "0")
            {
                if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
                {
                    whereClauseSb.Append(" AND [J].[JobDurationLookupId] =" + jobType);
                }
                else
                {
                    whereClauseSb.Append(" [J].[JobDurationLookupId] =" + jobType);
                }
            }

            if (!string.IsNullOrEmpty(city))
            {
                string cityQuery = BuildLocationQuery(city);
                if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
                {
                    whereClauseSb.Append(" AND (" + cityQuery + ")");
                }
                else
                {
                    whereClauseSb.Append("(" + cityQuery + ")");
                }
            }

            if (jobStatus != 0)
            {
                if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
                {
                    whereClauseSb.Append(" AND [J].[JobStatus] =" + jobStatus);
                }
                else
                {
                    whereClauseSb.Append(" [J].[JobStatus] =" + jobStatus);
                }
            }

            if (!string.IsNullOrEmpty(lastUpdateDate))
            {
                DateTime dt = DateTime.Now;
                try
                {
                    dt = dt.AddDays(-int.Parse(lastUpdateDate));
                    if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
                    {
                        whereClauseSb.Append(" AND [J].[PostedDate] >= '" + dt.ToShortDateString() + "'");
                    }
                    else
                    {
                        whereClauseSb.Append(" [J].[PostedDate] >= '" + dt.ToShortDateString() + "'");
                    }
                }
                catch
                {

                }
            }

            // 8971

            if (!string.IsNullOrEmpty(jobIndustry))
            {
                whereClauseSb.Append("AND [J].[JobIndustryLookupId]=" + jobIndustry);
            }
            // 8971

            whereClause = whereClauseSb.ToString();

            if (whereClause.StartsWith(" AND"))
            {
                whereClause = whereClause.Substring(4);
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[J].[JobTitle]";   //8971
                request.SortOrder = "ASC";
            }

            //  request.SortColumn = "[J].[" + request.SortColumn + "]";  //8971

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<JobPosting> response = new PagedResponse<JobPosting>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<JobPosting>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
          }

        IList<JobPosting> IJobPostingDataAccess.GetAllByJobCartAlertId(int jobCartAlertId)
        {
            if (jobCartAlertId < 1)
            {
                throw new ArgumentNullException("jobCartAlertId");
            }

            const string SP = "dbo.JobPosting_JobMatch";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobCartAlertId", DbType.Int32, jobCartAlertId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPosting>().BuildEntities(reader);
                }
            }
        }

        IList<JobPosting> IJobPostingDataAccess.GetAllByProjectId(int projectId)
        {
            if (projectId < 1)
            {
                throw new ArgumentNullException("projectId");
            }

            const string SP = "dbo.JobPosting_GetAllJobPostingByProjectId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ProjectId", DbType.Int32, projectId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPosting>().BuildEntities(reader);
                }
            }
        }

        //Starts  0.7 
        IList<JobPosting> IJobPostingDataAccess.GetAllByProjectId(int projectId, string sortExpression)
        {
            if (projectId < 1)
            {
                throw new ArgumentNullException("projectId");
            }

            const string SP = "dbo.JobPosting_GetAllJobPostingByProjectId";

            string SortColumn = "PostedDate";
            string SortOrder = "ASC";
            string[] part = (string.IsNullOrEmpty(sortExpression)) ? null : sortExpression.Split(' ');

            if (part != null && part.Length > 0)
            {
                SortColumn = part[0];

                if (part.Length > 1 && part[1] != null)
                {
                    SortOrder = part[1];
                }
                else
                {
                    SortOrder = "ASC";
                }
            }

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ProjectId", DbType.Int32, projectId);
                Database.AddInParameter(cmd, "@SortColumn", DbType.String, SortColumn);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPosting>().BuildEntities(reader);
                }
            }
        }
        //ends 0.7 
        PagedResponse<JobPosting> IJobPostingDataAccess.GetPagedWithCandidateCount(PagedRequest request)
        {
            const string SP = "dbo.JobPosting_GetPagedWithCandidateCount";
            string whereClause = string.Empty;
            string jobStatus = String.Empty;//0.9
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");
                        if (StringHelper.IsEqual(column, "IsTemplate"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append("[J].[IsTemplate]");
                            sb.Append(" = ");

                            sb.Append(value == "True" ? "1" : "0");
                        }
                        if (StringHelper.IsEqual(column, "memberId"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append(" ([J].CreatorId = ");
                            sb.Append(value);
                            sb.Append(" OR ");
                            sb.Append("[JPHT].[MemberId] =  ");
                            sb.Append(value);
                            sb.Append(")");
                        }
                    }
                }

                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[J].[PostedDate]";
                request.SortOrder = "DESC";
            }

            //request.SortColumn = "[J].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    StringHelper.Convert(jobStatus),//0.9
                                                    0
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<JobPosting> response = new PagedResponse<JobPosting>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new JobPostingBuilder()).BuildPagedEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        PagedResponse<JobPosting> IJobPostingDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.JobPosting_GetPagedWithCandidateCount";
            string whereClause = string.Empty;
            string jobStatus = String.Empty;//0.9
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobTitle"))
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[JobTitle]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");
                            }
                        }
                        if (StringHelper.IsEqual(column, "ReqCode"))
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[JobPostingCode]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");
                            }
                        }
                        if (StringHelper.IsEqual(column, "IsTemplate"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append("[J].[IsTemplate]");
                            sb.Append(" = ");
                            
                            sb.Append(value == "True" ? "1" : "0");
                        }

                   

                        if (StringHelper.IsEqual(column, "memberId"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                //1.2 start
                                sb.Append(" ([J].CreatorId = ");
                                sb.Append(value);
                                sb.Append(" OR ");
                                //1.2 End
                                sb.Append("[JPHT].[MemberId] =  ");
                                sb.Append(value);
                                sb.Append(")");
                            }
                        }

                          if (StringHelper.IsEqual(column, "memberId"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                //1.2 start
                                sb.Append(" ([J].CreatorId = ");
                                sb.Append(value);
                                sb.Append(" OR ");
                                //1.2 End
                                sb.Append("[JPHT].[MemberId] =  ");
                                sb.Append(value);
                                sb.Append(")");
                            }
                        }

                        if (StringHelper.IsEqual(column, "memberId"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                //1.2 start
                                sb.Append(" ([J].CreatorId = ");
                                sb.Append(value);
                                sb.Append(" OR ");
                                //1.2 End
                                sb.Append("[JPHT].[MemberId] =  ");
                                sb.Append(value);
                                sb.Append(")");
                                //sb.Append(" OR ");//Sumit 25/Sep/2017
                                ////1.2 End
                                //sb.Append("[CC].[MemberId] =  ");
                                //sb.Append(value);
                            }
                        }

                          if (StringHelper.IsEqual(column, "JobStatus"))
                          {
                              if (!string.Equals(value, "0"))
                              {
                                  sb.Append("  and [J].[JobStatus]");
                                  sb.Append(" in (");
                                  sb.Append(value);
                                  sb.Append(")");
                              }
                          }

                          if (StringHelper.IsEqual(column, "JobPostingFromDate"))
                          {
                              if (value != DateTime.MinValue.ToString())
                              {
                                  if (sb.Length > 0)
                                  {
                                      sb.Append(" and ");
                                  }
                                  sb.Append("[J].[PostedDate]");
                                  sb.Append(" >= ");
                                  sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ");

                              }
                          }
                          if (StringHelper.IsEqual(column, "JobPostingToDate"))
                          {
                              if (value != DateTime.MinValue.ToString())
                              {
                                  if (sb.Length > 0)
                                  {
                                      sb.Append(" and ");
                                  }
                                  sb.Append("[J].[PostedDate]");
                                  sb.Append(" < ");
                                  sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ");
                              }
                          }

                          if (StringHelper.IsEqual(column, "Creator"))
                          {
                              if (!string.Equals(value, "0"))
                              {
                                  if (sb.Length > 0)
                                  {
                                      sb.Append(" and ");
                                  }
                                  sb.Append("[J].[CreatorId]");

                                  sb.Append(" = ");

                                  sb.Append(value);
                              }
                          }

                          if (StringHelper.IsEqual(column, "Employee"))
                          {
                              if (!string.Equals(value, "0"))
                              {
                                  if (sb.Length > 0)
                                  {
                                      sb.Append(" and ");
                                  }
                                  sb.Append("[J].[Id] IN ( SELECT [JPHT].[JobPostingId] FROM [JobPostingHiringTeam] [JPHT] WHERE [JPHT].[MemberId] = ");

                                  sb.Append(value);
                                  sb.Append(")");
                              }
                          }

                          if (StringHelper.IsEqual(column, "StateId"))
                          {
                              if (!string.Equals(value, "0"))
                              {
                                  if (sb.Length > 0)
                                  {
                                      sb.Append(" and ");
                                  }
                                  sb.Append("[J].[StateId]");
                                  sb.Append(" = ");

                                  sb.Append(value);
                              }
                          }

                          if (StringHelper.IsEqual(column, "City"))
                          {
                              if (!string.Equals(value, ""))
                              {
                                  if (sb.Length > 0)
                                  {
                                      sb.Append(" and ");
                                  }
                                  sb.Append("[J].[City]");
                                  sb.Append(" LIKE '");
                                  if (!value.StartsWith("%"))
                                  {
                                      sb.Append("%");
                                  }
                                  sb.Append(value);
                                  if (!value.EndsWith("%"))
                                  {
                                      sb.Append("%");
                                  }
                                  sb.Append("'");


                              }
                          }

                          if (StringHelper.IsEqual(column, "CountryId"))
                          {
                              if (!string.Equals(value, "0"))
                              {
                                  if (sb.Length > 0)
                                  {
                                      sb.Append(" and ");
                                  }
                                  sb.Append("[J].[CountryId]");
                                  sb.Append(" = ");

                                  sb.Append(value);
                              }
                          }


                          if (StringHelper.IsEqual(column, "Client"))
                          {
                              if (!string.Equals(value, ""))
                              {
                                  if (sb.Length > 0)
                                  {
                                      sb.Append(" and ");
                                  }
                                  sb.Append("[J].[ClientId]");
                                  sb.Append(" in (");

                                  sb.Append(value);
                                  sb.Append(")");
                              }
                          }



                        if (StringHelper.IsEqual(column, "IsValumeHire"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" ,[VolumeHireJobPosting] as [VHJ] ");
                            }
                            sb.Append("[J].[Id] IN (Select [JobPostingId] From [VolumeHireJobPosting])");
                        }

                        if (StringHelper.IsEqual(column, "IsJobActive"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append("[J].[IsJobActive]");
                            sb.Append(" = ");

                            sb.Append(value == "True" ? "1" : "0");
                        }

                        if (StringHelper.IsEqual(column, "companyId"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append("(");

                            //sb.Append("[J].[ClientId3]");
                            //sb.Append(" = ");
                            //sb.Append(value);
                            //sb.Append(" or ");

                            //sb.Append("[J].[ClientId2]");
                            //sb.Append(" = ");
                            //sb.Append(value);
                            //sb.Append(" or ");

                            sb.Append("[J].[ClientId]");
                            sb.Append(" = ");
                            sb.Append(value);                            

                            sb.Append(")");
                        }

                        // 10531 starts

                        if (StringHelper.IsEqual(column, "intDaysDifference"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }

                            sb.Append("[J].[PostedDate] >= getdate() -");
                            sb.Append(value); 
                        }
                        if (StringHelper.IsEqual(column, "IsCompanyContact")) 
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }

                            sb.Append("[J].ClientContactID=");
                            sb.Append(value);
                        }
                        // 10531 ends
                    }
                }

                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[J].[PostedDate]";
                request.SortOrder = "DESC";
            }

            //request.SortColumn = "[J].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    StringHelper.Convert(jobStatus),//0.9
                                                    1
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<JobPosting> response = new PagedResponse<JobPosting>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new JobPostingBuilder() ).BuildPagedEntities (reader );

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        //************************************Code added by pravin khot on 8/Jan/2016*****************************


        IList<JobPosting> IJobPostingDataAccess.MemberCareerOpportunities()
        {
            const string SP = "dbo.MemberCareerOpportunities";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPosting>().BuildEntities(reader);
                }
            }
        }

        //*********************************End*****************************************

        //*****************************Code added by pravin khot on 18/Jan/2016**************************
        JobPosting IJobPostingDataAccess.AddCareerJob(JobPosting jobPosting)
        {
            const string SP = "dbo.MemberCareerPortalSubmissionList_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingCode", DbType.Int32, jobPosting.CareerJobId);
                Database.AddInParameter(cmd, "@MemberId", DbType.AnsiString, jobPosting.MemberId);
                Database.AddInParameter(cmd, "@IsApplied", DbType.Boolean, jobPosting.IsApplied);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPosting = CreateEntityBuilder<JobPosting>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPosting = null;
                    }
                }

                if (jobPosting == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting already exists. Please specify another job posting.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this job posting.");
                            }
                    }
                }

                return jobPosting;
            }
        }
        //********************************************End*********************************************************

        //**********************Code added by pravin khot on 29/Jan/2016***************************
        ArrayList IJobPostingDataAccess.GetAllByBUID(int BUID)
        {
            if (BUID < 0)
            {
                throw new ArgumentException("status");
            }

            const string SP = "dbo.JobPosting_GetAllJobPosting_CareerRequisition";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@BUID", DbType.Int32, BUID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();

                    while (reader.Read())
                    {
                        //jobPostingList.Add(new { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                        jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                    }

                    return jobPostingList;
                }
            }
        }
        //***********************************End**********************************************

        //Code introduced by Pravin khot on 2/Feb/2015 Start
        PagedResponse<JobPosting> IJobPostingDataAccess.GetRequisitionSourceBreakupReport(PagedRequest request)
        {
            //string sp = "SP_Rpt_Aging_In_Days_Avg_NEW";
            string sp = "USP_HM_SourceBreakup_Dynamic";
            string whereClause = string.Empty;



            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;
                //bool doOnceSuccessfullHire= true;
                //bool doOnceMissedHire = true;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        //if (StringHelper.IsEqual(column, "IsTemplate"))
                        //{
                        //    if (sb.Length > 0)
                        //    {
                        //        sb.Append(" and ");
                        //    }
                        //    sb.Append("[JP].[IsTemplate]");
                        //    sb.Append(" = ");

                        //    sb.Append(value == "True" ? "1" : "0");
                        //}
                        //if (StringHelper.IsEqual(column, "JobPostingId"))
                        //{
                        //    if (!string.Equals(value, "0"))
                        //    {

                        //        if (sb.Length > 0)
                        //        {
                        //            sb.Append(" and ");

                        //        }
                        //        sb.Append("[J].[Id]");
                        //        sb.Append(" = ");

                        //        sb.Append(value);
                        //    }
                        //}

                        //if (StringHelper.IsEqual(column, "endClients"))
                        //{
                        //    if (!string.Equals(value, "0"))
                        //    {
                        //        if (sb.Length > 0)
                        //        {
                        //            sb.Append(" and ");
                        //        }
                        //        sb.Append("[J].[ClientId]");
                        //        sb.Append(" = ");

                        //        sb.Append(value);
                        //    }
                        //}

                        if (StringHelper.IsEqual(column, "ReqStartDateFrom"))
                        {

                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("dbo.GetDateOnly([JP].[CreateDate])"); //Code Modified by Prasanth on 05/June/2015 Changed PostedDate to OpenDate 
                                sb.Append(" >= ");
                                sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ");
                            }

                        }

                        if (StringHelper.IsEqual(column, "ReqStartDateTo"))
                        {

                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("dbo.GetDateOnly([JP].[CreateDate])"); //Code Modified by Prasanth on 05/June/2015 Changed PostedDate to OpenDate
                                sb.Append(" <= ");
                                sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ");
                            }
                        }


                        //if (StringHelper.IsEqual(column, "ReqCreator"))
                        //{
                        //    if (!string.Equals(value, "0"))
                        //    {
                        //        if (sb.Length > 0)
                        //        {
                        //            sb.Append(" and ");
                        //        }
                        //        sb.Append("[J].[CreatorId]");

                        //        sb.Append(" = ");

                        //        sb.Append(value);
                        //    }
                        //}


                    }
                }
                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn)) request.SortColumn = "CreateDate";
            if (StringHelper.IsBlank(request.SortOrder)) request.SortOrder = "DESC";
            int d = 0;
            Int32.TryParse(request.SortColumn, out d);
            if (d == 0)
            {
                //request.SortColumn = "[JP].[" + request.SortColumn + "]";
            }
            else
            {
                request.SortColumn = "level[" + request.SortColumn + "]";
            }
            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};


            using (DbCommand cmd = Database.GetStoredProcCommand(sp, paramValues))
            {
                PagedResponse<JobPosting> response = new PagedResponse<JobPosting>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    response.Response = (new JobPostingBuilder()).RequisitionSourceBreakupBuildPagedEntities(reader);
                    //response.TotalRow = response.Response.Count;
                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        //***************************END***********************************

/*
        PagedResponse<JobPosting> IJobPostingDataAccess.GetPagedBudget(PagedRequest request)
        {
            const string SP = "dbo.JobPosting_GetPagedWithCandidateCountBudget";
            string whereClause = string.Empty;
            string jobStatus = String.Empty;//0.9
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobTitle"))
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[JobTitle]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");
                            }
                        }
                        if (StringHelper.IsEqual(column, "ReqCode"))
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[JobPostingCode]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");
                            }
                        }
                        if (StringHelper.IsEqual(column, "IsTemplate"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append("[J].[IsTemplate]");
                            sb.Append(" = ");

                            sb.Append(value == "True" ? "1" : "0");
                        }



                        if (StringHelper.IsEqual(column, "memberId"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                //1.2 start
                                sb.Append(" ([J].CreatorId = ");
                                sb.Append(value);
                                sb.Append(" OR ");
                                //1.2 End
                                sb.Append("[JPHT].[MemberId] =  ");
                                sb.Append(value);
                                sb.Append(")");
                            }
                        }

                        if (StringHelper.IsEqual(column, "memberId"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                //1.2 start
                                sb.Append(" ([J].CreatorId = ");
                                sb.Append(value);
                                sb.Append(" OR ");
                                //1.2 End
                                sb.Append("[JPHT].[MemberId] =  ");
                                sb.Append(value);
                                sb.Append(")");
                            }
                        }

                        if (StringHelper.IsEqual(column, "memberId"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                //1.2 start
                                sb.Append(" ([J].CreatorId = ");
                                sb.Append(value);
                                sb.Append(" OR ");
                                //1.2 End
                                sb.Append("[JPHT].[MemberId] =  ");
                                sb.Append(value);
                                sb.Append(")");
                                sb.Append(" OR ");
                                //1.2 End
                                sb.Append("[CC].[MemberId] =  ");
                                sb.Append(value);
                            }
                        }

                        if (StringHelper.IsEqual(column, "JobStatus"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                sb.Append("  and [J].[JobStatus]");
                                sb.Append(" in (");
                                sb.Append(value);
                                sb.Append(")");
                            }
                        }

                        if (StringHelper.IsEqual(column, "JobPostingFromDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[PostedDate]");
                                sb.Append(" >= ");
                                sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ");

                            }
                        }
                        if (StringHelper.IsEqual(column, "JobPostingToDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[PostedDate]");
                                sb.Append(" < ");
                                sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ");
                            }
                        }

                        if (StringHelper.IsEqual(column, "Creator"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[CreatorId]");

                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }

                        if (StringHelper.IsEqual(column, "Employee"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[Id] IN ( SELECT [JPHT].[JobPostingId] FROM [JobPostingHiringTeam] [JPHT] WHERE [JPHT].[MemberId] = ");

                                sb.Append(value);
                                sb.Append(")");
                            }
                        }

                        if (StringHelper.IsEqual(column, "StateId"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[StateId]");
                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }

                        if (StringHelper.IsEqual(column, "City"))
                        {
                            if (!string.Equals(value, ""))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[City]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");


                            }
                        }

                        if (StringHelper.IsEqual(column, "CountryId"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[CountryId]");
                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }


                        if (StringHelper.IsEqual(column, "Client"))
                        {
                            if (!string.Equals(value, ""))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[ClientId]");
                                sb.Append(" in (");

                                sb.Append(value);
                                sb.Append(")");
                            }
                        }



                        if (StringHelper.IsEqual(column, "IsValumeHire"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" ,[VolumeHireJobPosting] as [VHJ] ");
                            }
                            sb.Append("[J].[Id] IN (Select [JobPostingId] From [VolumeHireJobPosting])");
                        }

                        if (StringHelper.IsEqual(column, "IsJobActive"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append("[J].[IsJobActive]");
                            sb.Append(" = ");

                            sb.Append(value == "True" ? "1" : "0");
                        }

                        if (StringHelper.IsEqual(column, "companyId"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append("(");

                            //sb.Append("[J].[ClientId3]");
                            //sb.Append(" = ");
                            //sb.Append(value);
                            //sb.Append(" or ");

                            //sb.Append("[J].[ClientId2]");
                            //sb.Append(" = ");
                            //sb.Append(value);
                            //sb.Append(" or ");

                            sb.Append("[J].[ClientId]");
                            sb.Append(" = ");
                            sb.Append(value);

                            sb.Append(")");
                        }

                        // 10531 starts

                        if (StringHelper.IsEqual(column, "intDaysDifference"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }

                            sb.Append("[J].[PostedDate] >= getdate() -");
                            sb.Append(value);
                        }
                        if (StringHelper.IsEqual(column, "IsCompanyContact"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }

                            sb.Append("[J].ClientContactID=");
                            sb.Append(value);
                        }
                        // 10531 ends
                    }
                }

                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[J].[PostedDate]";
                request.SortOrder = "DESC";
            }

            //request.SortColumn = "[J].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    StringHelper.Convert(jobStatus),//0.9
                                                    1
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<JobPosting> response = new PagedResponse<JobPosting>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new JobPostingBuilder()).BuildPagedEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
*/


        //PagedResponse<JobPosting> IJobPostingDataAccess.GetPaged(string allKeys, string anyKey, string jobTitle, string jobType, string city, string lastUpdateDate, int jobStatus, string jobIndustry, PagedRequest request)
        //{
        //    const string SP = "dbo.JobPosting_GetPagedSearch";
        //    string whereClause = string.Empty;

        //    StringBuilder whereClauseSb = new StringBuilder();

        //    if (!StringHelper.IsBlank(allKeys) && !StringHelper.IsBlank(anyKey))
        //    {
        //        whereClauseSb.Append(BuildAllAndAnyKeyWordQuery(allKeys, anyKey));
        //    }
        //    else if (!StringHelper.IsBlank(allKeys) && StringHelper.IsBlank(anyKey))
        //    {
        //        whereClauseSb.Append(BuildAllKeyWordsQuery(allKeys));
        //    }
        //    else if (StringHelper.IsBlank(allKeys) && !StringHelper.IsBlank(anyKey))
        //    {
        //        whereClauseSb.Append(BuildAnyKeyWordQuery(anyKey));
        //    }

        //    if (!string.IsNullOrEmpty(jobTitle))
        //    {
        //        if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
        //        {
        //            whereClauseSb.Append(" AND [J].[JobTitle] LIKE '%" + jobTitle.Trim() + "%'");
        //        }
        //        else
        //        {
        //            whereClauseSb.Append(" [J].[JobTitle] LIKE '%" + jobTitle.Trim() + "%'");
        //        }
        //    }

        //    if (jobType!="0")
        //    {
        //        if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
        //        {
        //            whereClauseSb.Append(" AND [J].[JobDurationLookupId] =" + jobType);
        //        }
        //        else
        //        {
        //            whereClauseSb.Append(" [J].[JobDurationLookupId] =" + jobType);
        //        }
        //    }

        //    if (!string.IsNullOrEmpty(city))
        //    {
        //        string cityQuery = BuildLocationQuery(city);
        //        if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
        //        {
        //            whereClauseSb.Append(" AND (" + cityQuery+")");
        //        }
        //        else
        //        {
        //            whereClauseSb.Append("("+cityQuery+")");
        //        }
        //    }

        //    if (jobStatus!=0)
        //    {
        //        if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
        //        {
        //            whereClauseSb.Append(" AND [J].[JobStatus] =" + jobStatus);
        //        }
        //        else
        //        {
        //            whereClauseSb.Append(" [J].[JobStatus] =" + jobStatus);
        //        }
        //    }

        //    if (!string.IsNullOrEmpty(lastUpdateDate))
        //    {
        //        DateTime dt = DateTime.Now;
        //        try
        //        {
        //            dt = dt.AddDays(-int.Parse(lastUpdateDate));
        //            if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
        //            {
        //                whereClauseSb.Append(" AND [J].[PostedDate] >= '" + dt.ToShortDateString() + "'");
        //            }
        //            else
        //            {
        //                whereClauseSb.Append(" [J].[PostedDate] >= '" + dt.ToShortDateString() + "'");
        //            }
        //        }
        //        catch
        //        {

        //        }
        //    }

        //  // 8971

        //    if (!string.IsNullOrEmpty(jobIndustry))
        //    {
        //        whereClauseSb.Append("AND [J].[JobIndustryLookupId]=" +jobIndustry);
        //    }
        //  // 8971

        //    whereClause = whereClauseSb.ToString();

        //    if (whereClause.StartsWith(" AND"))
        //    {
        //        whereClause = whereClause.Substring(4);
        //    }

        //    if (StringHelper.IsBlank(request.SortColumn))
        //    {
        //        request.SortColumn = "[J].[JobTitle]";   //8971
        //        request.SortOrder = "ASC";
        //    }

        //  //  request.SortColumn = "[J].[" + request.SortColumn + "]";  //8971

        //    object[] paramValues = new object[] {	request.PageIndex,
        //                                            request.RowPerPage,
        //                                            StringHelper.Convert(whereClause),
        //                                            StringHelper.Convert(request.SortColumn),
        //                                            StringHelper.Convert(request.SortOrder)
        //                                        };

        //    using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
        //    {
        //        PagedResponse<JobPosting> response = new PagedResponse<JobPosting>();

        //        using (IDataReader reader = Database.ExecuteReader(cmd))
        //        {
        //            response.Response = CreateEntityBuilder<JobPosting>().BuildEntities(reader);

        //            if ((reader.NextResult()) && (reader.Read()))
        //            {
        //                response.TotalRow = reader.GetInt32(0);
        //            }
        //        }

        //        return response;
        //    }
        //}

        PagedResponse<JobPosting> IJobPostingDataAccess.GetPagedVolumeHire(string allKeys, string anyKey, string jobTitle, string jobType, string city, string lastUpdateDate, string volumeHire, int jobStatus,string jobIndustry,PagedRequest request)
        {
            const string SP = "dbo.JobPosting_GetPagedSearchVolumeHire";
            string whereClause = string.Empty;

            StringBuilder whereClauseSb = new StringBuilder();

            if (!StringHelper.IsBlank(allKeys) && !StringHelper.IsBlank(anyKey))
            {
                whereClauseSb.Append(BuildAllAndAnyKeyWordQuery(allKeys, anyKey));
            }
            else if (!StringHelper.IsBlank(allKeys) && StringHelper.IsBlank(anyKey))
            {
                whereClauseSb.Append(BuildAllKeyWordsQuery(allKeys));
            }
            else if (StringHelper.IsBlank(allKeys) && !StringHelper.IsBlank(anyKey))
            {
                whereClauseSb.Append(BuildAnyKeyWordQuery(anyKey));
            }

            if (!string.IsNullOrEmpty(jobTitle))
            {
                if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
                {
                    whereClauseSb.Append(" AND [J].[JobTitle] LIKE '%" + jobTitle.Trim() + "%'");
                }
                else
                {
                    whereClauseSb.Append(" [J].[JobTitle] LIKE '%" + jobTitle.Trim() + "%'");
                }
            }

            if (jobType != "0")
            {
                if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
                {
                    whereClauseSb.Append(" AND [J].[JobDurationLookupId] =" + jobType);
                }
                else
                {
                    whereClauseSb.Append(" [J].[JobDurationLookupId] =" + jobType);
                }
            }

            if (!string.IsNullOrEmpty(city))
            {
                string cityQuery = BuildLocationQuery(city);
                if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
                {
                    whereClauseSb.Append(" AND (" + cityQuery + ")");
                }
                else
                {
                    whereClauseSb.Append("(" + cityQuery + ")");
                }
            }

            if (jobStatus != 0)
            {
                if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
                {
                    whereClauseSb.Append(" AND [J].[JobStatus] =" + jobStatus);
                }
                else
                {
                    whereClauseSb.Append(" [J].[JobStatus] =" + jobStatus);
                }
            }

            if (!string.IsNullOrEmpty(lastUpdateDate))
            {
                DateTime dt = DateTime.Now;
                try
                {
                    dt = dt.AddDays(-int.Parse(lastUpdateDate));
                    if (!string.IsNullOrEmpty(whereClauseSb.ToString()))
                    {
                        whereClauseSb.Append(" AND [J].[PostedDate] >= '" + dt.ToShortDateString() + "'");
                    }
                    else
                    {
                        whereClauseSb.Append(" [J].[PostedDate] >= '" + dt.ToShortDateString() + "'");
                    }
                }
                catch
                {

                }
            }

            whereClause = whereClauseSb.ToString();

            if (whereClause.StartsWith(" AND"))
            {
                whereClause = whereClause.Substring(4);
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "JobTitle";
                request.SortOrder = "ASC";
            }

            request.SortColumn = "[J].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<JobPosting> response = new PagedResponse<JobPosting>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<JobPosting>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        // 0.8 starts here
        PagedResponse<JobPosting> IJobPostingDataAccess.GetReport(PagedRequest request)
        {
            //const string SP = "dbo.JobPosting_GetReport";
            //const 
            string SP = "dbo.JobPosting_GetReport";
            if (request.PageIndex == -1 && request.RowPerPage == -1)
            {
                SP = "JobPosting_GetReport_New";
            }
            string whereClause = string.Empty;
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;
                //bool doOnceSuccessfullHire= true;
                //bool doOnceMissedHire = true;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "IsTemplate"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append("[J].[IsTemplate]");
                            sb.Append(" = ");

                            sb.Append(value == "True" ? "1" : "0");
                        }
                        if (StringHelper.IsEqual(column, "JobPostingId"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append("[J].[Id]");
                            sb.Append(" = ");

                            sb.Append(value);
                        }


                        if (StringHelper.IsEqual(column, "jobStatus"))
                        {
                            //0.9 Start
                            if (!string.Equals(value, "0"))//1.0
                            {
                                sb.Append("  and [J].[JobStatus]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                            //0.9 End
                        }
//0.9 Start
                       /* if (StringHelper.IsEqual(column, "subJobStatus"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }

                                if (Int32.Parse(value) < 10)
                                {
                                    sb.Append("[J].[JobStatus]");

                                    sb.Append(" IN ");

                                    switch (value)
                                    {
                                        case "1":
                                            sb.Append("(1,11,12,13,14)");
                                            break;
                                        case "2":
                                            sb.Append("(2,21,22)");
                                            break;
                                        case "3":
                                            sb.Append("(3,31,32,33,34)");
                                            break;
                                        case "4":
                                            sb.Append("(4)");
                                            break;
                                        default:
                                            sb.Append("(0)");
                                            break;
                                    }
                                }
                                else
                                {
                                    sb.Append("[J].[JobStatus]");

                                    sb.Append(" = ");

                                    sb.Append(value);
                                }
                            }
                        }*/ //0.9 End


                        if (StringHelper.IsEqual(column, "jobStatusStartDate"))
                        {
                            //if (value != DateTime.MinValue.ToString())
                            //{
                            //    if (sb.Length > 0)
                            //    {
                            //        sb.Append(" and ");
                            //    }

                            //      sb.Append("[J].[UpdateDate]");
                            //      sb.Append(" >= ");
                            //      sb.Append("'" + value + "'");
                            //}
                            // 1.4   start
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[UpdateDate]");
                                sb.Append(" >= ");
                                sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ");

                              //  value = "[dbo].[GetDateOnly]('" + Convert.ToDateTime(value) + "')";
                               // sb.Append(value);
                            }
                            // 1.4   End
                        }
                        if (StringHelper.IsEqual(column, "jobStatusEndDate"))
                        {
                            //if (value != DateTime.MinValue.ToString())
                            //{
                            //    if (sb.Length > 0)
                            //    {
                            //        sb.Append(" and ");
                            //    }
                            //     sb.Append("[J].[UpdateDate]");
                            //     sb.Append(" <= ");
                            //     sb.Append("'" + value + "'");
                            //}
                            // 1.4   start
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[UpdateDate]");
                                sb.Append(" < ");
                                sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ");

                              //  value = "[dbo].[GetDateOnly]('" + Convert.ToDateTime(value) + "')";
                               // sb.Append(value);
                            }
                            // 1.4   End
                        }
                        
                        if (StringHelper.IsEqual(column, "ReqCreator"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[CreatorId]");

                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }
                        
                        if (StringHelper.IsEqual(column, "employee"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[Id] IN ( SELECT [JPHT].[JobPostingId] FROM [JobPostingHiringTeam] [JPHT] WHERE [JPHT].[MemberId] = ");

                                sb.Append(value);
                                sb.Append(")");
                            }
                        }

                        if (StringHelper.IsEqual(column, "StateID"))
                        {
                            if (!string.Equals(value,"0" ))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[StateId]");
                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }

                        if (StringHelper.IsEqual(column, "City"))
                        {
                            if (!string.Equals(value, ""))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[City]");
                                sb.Append(" like '%");

                                sb.Append(value);
                                sb.Append("%'");
                            }
                        }

                        if (StringHelper.IsEqual(column, "CountryID"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[CountryId]");
                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }


                        if (StringHelper.IsEqual(column, "endClients"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[ClientId]");
                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }

                        //if (StringHelper.IsEqual(column, "tier1Client"))
                        //{
                        //    if (!string.Equals(value, "0"))
                        //    {
                        //        if (sb.Length > 0)
                        //        {
                        //            sb.Append(" and ");
                        //        }
                        //        sb.Append("[J].[ClientId2]");
                        //        sb.Append(" = ");

                        //        sb.Append(value);
                        //    }
                        //}
                        //if (StringHelper.IsEqual(column, "tier2Client"))
                        //{
                        //    if (!string.Equals(value, "0"))
                        //    {
                        //        if (sb.Length > 0)
                        //        {
                        //            sb.Append(" and ");
                        //        }
                        //        sb.Append("[J].[ClientId3]");
                        //        sb.Append(" = ");

                        //        sb.Append(value);
                        //    }
                        //}
                    }
                }
                // 0.8 ends here
                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn))request.SortColumn = "PostedDate";
            if (StringHelper.IsBlank(request.SortOrder )) request.SortOrder = "DESC";
            int d = 0;
            Int32.TryParse(request.SortColumn, out d);
            if (d == 0)
            {
                //request.SortColumn = "[J].[" + request.SortColumn + "]";
            }
            else
            {
                request .SortColumn ="level["+request .SortColumn +"]";
            }

             object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<JobPosting> response = new PagedResponse<JobPosting>();
                cmd.CommandTimeout = 0;
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new JobPostingBuilder()).BuildPagedEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }


        //Code introduced by Prasanth on 22/May/2015 Start
        PagedResponse<JobPosting> IJobPostingDataAccess.GetRequisitionAgingReport(PagedRequest request)
        {
            string sp = "SP_Rpt_Aging_In_Days_Avg_NEW";
            string whereClause = string.Empty;



            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;
                //bool doOnceSuccessfullHire= true;
                //bool doOnceMissedHire = true;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "IsTemplate"))
                        {
                            if (sb.Length > 0)
                            {
                                sb.Append(" and ");
                            }
                            sb.Append("[J].[IsTemplate]");
                            sb.Append(" = ");

                            sb.Append(value == "True" ? "1" : "0");
                        }
                        //if (StringHelper.IsEqual(column, "JobPostingId"))
                        //{
                        //    if (!string.Equals(value, "0"))
                        //    {

                        //        if (sb.Length > 0)
                        //        {
                        //            sb.Append(" and ");

                        //        }
                        //        sb.Append("[J].[Id]");
                        //        sb.Append(" = ");

                        //        sb.Append(value);
                        //    }
                        //}

                        if (StringHelper.IsEqual(column, "endClients"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[ClientId]");
                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }

                        if (StringHelper.IsEqual(column, "ReqStartDateFrom"))
                        {
                          
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[OpenDate]"); //Code Modified by Prasanth on 05/June/2015 Changed PostedDate to OpenDate 
                                sb.Append(" >= ");
                                sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ");

                            }
             
                        }


                        if (StringHelper.IsEqual(column, "ReqStartDateTo"))
                        {

                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[OpenDate]"); //Code Modified by Prasanth on 05/June/2015 Changed PostedDate to OpenDate
                                sb.Append(" <= ");
                                sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ");

                            }

                        }


                        if (StringHelper.IsEqual(column, "ReqCreator"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[CreatorId]");

                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }

 
                    }
                }
                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn)) request.SortColumn = "OpenDate"; //Code Modified by Prasanth on 05/June/2015 Changed PostedDate to OpenDate
            if (StringHelper.IsBlank(request.SortOrder)) request.SortOrder = "DESC";
            int d = 0;
            Int32.TryParse(request.SortColumn, out d);
            if (d == 0)
            {
                //request.SortColumn = "[J].[" + request.SortColumn + "]";
            }
            else
            {
                request.SortColumn = "level[" + request.SortColumn + "]";
            }
            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};


            using (DbCommand cmd = Database.GetStoredProcCommand(sp,paramValues))
            {
                PagedResponse<JobPosting> response = new PagedResponse<JobPosting>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    
                    response.Response = (new JobPostingBuilder()).RequisitionAgingBuildPagedEntities(reader);
                    //response.TotalRow = response.Response.Count;
                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        //***************************EDN***********************************

        PagedResponse<Submission> IJobPostingDataAccess.GetDashBoardSubmission(PagedRequest request)
        {
             const string SP = "dbo.Submission_GetReport";
            string whereClause = string.Empty;
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");
                        if (!StringHelper.IsBlank(column))
                        {
                            value = value.Replace("'", "''");

                            if (StringHelper.IsEqual(column, "DashBoardSubmission"))
                            {
                                if (sb.Length > 0)
                                    sb.Append(" and ");
                                sb.Append(" [GL].[Name] not like '%close%' and [GL].[Name] not like '%draft%' ");
                            }
                            if (StringHelper.IsEqual(column, "MemberId"))
                            {
                                if (sb.Length > 0)
                                    sb.Append(" and ");
                                sb.Append("([MS].[CreatorId] ");
                                sb.Append(" = ");
                                sb.Append(value);
                                sb.Append (" or [MJC].[CreatorId]=");
                                sb .Append (value );
                                sb.Append(" ) ");
                            }
                        }
                    }
                }
                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = " [MS].[CreateDate]";
                request.SortOrder = "DESC";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
												request.RowPerPage,
												StringHelper.Convert(whereClause),
												StringHelper.Convert(request.SortColumn),
                                                StringHelper.Convert(request.SortOrder)
											};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Submission> response = new PagedResponse<Submission>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new JobPostingBuilder()).BuildSubmissionReports(reader);//CreateEntityBuilder<JobPosting>().BuildSubmissionReports(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        //12129 starts
        PagedResponse<Submission> IJobPostingDataAccess.GetSubmissionReport(PagedRequest request)
        {
            const string SP = "dbo.Submission_GetReport";
            string whereClause = string.Empty;
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;
                
                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobPostStartDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[PostedDate]");
                                sb.Append(" >= ");
                                value = "Convert(DateTime,'" + Convert.ToDateTime(value).ToString ("dd/MM/yyyy") + "',103) ";
                                sb.Append(value);
                            }
                        }
                        if (StringHelper.IsEqual(column, "JobPostEndDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[PostedDate]");
                                sb.Append(" < ");

                                value = "Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ";
                                sb.Append(value);
                                
                            }
                        }
                        if (StringHelper.IsEqual(column, "JobSubmitStartDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[MS].[submitteddate]");
                                sb.Append(" >= ");
                                value = "Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ";
                                sb.Append(value);
                            }
                        }
                        if (StringHelper.IsEqual(column, "JobSubmitEndDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[MS].[submitteddate] ");
                                sb.Append(" < ");
                                value = "Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ";
                                sb.Append(value);
                                
                            }
                        }
                        if (StringHelper.IsEqual(column, "ReqStartDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[dbo].[GetStartDateOnly]([J].[StartDate],[J].[CreateDate])");
                                sb.Append(" >= ");
                                value = "Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ";
                                sb.Append(value);
                              
                            }
                        }
                        if (StringHelper.IsEqual(column, "ReqEndDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[dbo].[GetStartDateOnly]([J].[StartDate],[J].[CreateDate])");
                                sb.Append(" < ");
                                value = "Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ";
                                sb.Append(value);
                            }
                        }

                        if (StringHelper.IsEqual(column, "ApplicantTpye"))
                          {
                              if (!string.Equals(value, "0"))
                              {
                                  if (sb.Length > 0)
                                  {
                                      sb.Append(" and ");
                                  }
                                  sb.Append("[MN].[MemberType]");
                                  sb.Append(" = ");
                                  sb.Append(value);
                              }
                           
                          }

                        if (StringHelper.IsEqual(column, "ReqOwner"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[CreatorId]");

                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }
                        if (StringHelper.IsEqual(column, "SubmittedBy"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                //sb.Append("  and [MS].[CreatorId]");
                                sb.Append(" [MS].[CreatorId]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                           
                        }

                        if (StringHelper.IsEqual(column, "Account"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                //sb.Append("[C].[CompanyName]");
                                sb.Append("[C].[Id]");
                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }

                       if (StringHelper.IsEqual(column, "Submittedto"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                //sb.Append(" [MS].[ReceiverEmail]");
                                sb.Append(" [MS].[ReceiverEmail] LIKE '%" + value.Trim() + "%' ");
                            }
                        }
                    }
                }
                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = " [MS].[CreateDate]";
                request.SortOrder = "DESC";
            }

           // request.SortColumn = "[J].[" + request.SortColumn + "]";
            request.SortColumn =  request.SortColumn ;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Submission> response = new PagedResponse<Submission>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new JobPostingBuilder()).BuildSubmissionReports(reader);//CreateEntityBuilder<JobPosting>().BuildSubmissionReports(reader);
                    
                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        //12129 ends
        bool IJobPostingDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPosting_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this job posting.");
                        }
                }
            }
        }

        JobPosting IJobPostingDataAccess.CreateFromExistingJob(int id,string jobPostingCode,int creatorId)//0.2 & 1.2
        {
            JobPosting jobPosting = null;
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPosting_CreateFromExistingJob";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);
                Database.AddInParameter(cmd, "@JobPostingCode", DbType.AnsiString, jobPostingCode); //0.2 
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, creatorId); //0.2 
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {                    
                    if (reader.Read())
                    {
                        jobPosting = CreateEntityBuilder<JobPosting>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPosting = null;
                    }
                }

                if (jobPosting == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting already exists.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this job posting.");
                            }
                    }
                }
            }

            return jobPosting;
        }

        JobPosting IJobPostingDataAccess.CreateProjectJobFromExistingTemplateJob(int projectId, int jobId)
        {
            JobPosting jobPosting = null;
            if (jobId < 1)
            {
                throw new ArgumentNullException("jobId");
            }
            if (projectId < 1)
            {
                throw new ArgumentNullException("projectId");
            }

            const string SP = "dbo.JobPosting_CreateProjectJobFromExistingTemplateJob";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ProjectId", DbType.Int32, projectId);
                Database.AddInParameter(cmd, "@JobId", DbType.Int32, jobId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPosting = CreateEntityBuilder<JobPosting>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPosting = null;
                    }
                }

                if (jobPosting == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting already exists.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this job posting.");
                            }
                    }
                }
            }

            return jobPosting;
        }

        ArrayList IJobPostingDataAccess.GetAllByStatus(int status)
        {
            if (status < 0)
            {
                throw new ArgumentException("status");
            }

            const string SP = "dbo.JobPosting_GetAllJobPostingByStatus";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Status", DbType.Int32, status);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();

                    while (reader.Read())
                    {
                        //jobPostingList.Add(new { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                        jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                    }

                    return jobPostingList;
                }
            }
        }

        ArrayList IJobPostingDataAccess.GetAllByStatusId(int statusId)
        {
          

            const string SP = "dbo.JobPosting_GetAllJobPostingByStatusId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@StatusId", DbType.Int32, statusId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();

                    while (reader.Read())
                    {
                        //jobPostingList.Add(new { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                        jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                    }

                    return jobPostingList;
                }
            }
        }
        //**************Code added by pravin khot on 8/June/2016******
        ArrayList IJobPostingDataAccess.GetAllByCandidatesId(int ClientId, int CandidateId)
        {
            if (ClientId < 0)
            {
                throw new ArgumentException("status");
            }

            const string SP = "dbo.JobPosting_GetAllJobPostingByClientByCandidateId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ClientId", DbType.Int32, ClientId);
                Database.AddInParameter(cmd, "@CandidateId", DbType.Int32, CandidateId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();

                    while (reader.Read())
                    {
                        //jobPostingList.Add(new { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                        jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                    }

                    return jobPostingList;
                }
            }
        }
        //*************************END***************************

        ArrayList IJobPostingDataAccess.GetAllByCleintId(int ClientId)
        {
            if (ClientId < 0)
            {
                throw new ArgumentException("status");
            }

            const string SP = "dbo.JobPosting_GetAllJobPostingByClient";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ClientId", DbType.Int32, ClientId );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();

                    while (reader.Read())
                    {
                        //jobPostingList.Add(new { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                        jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                    }

                    return jobPostingList;
                }
            }
        }

        ArrayList IJobPostingDataAccess.GetAllByCleintIdAndManagerId(int ClientId,int ManagerId)
        {
            if (ClientId < 0)
            {
                throw new ArgumentException("ClientID");
            }
            if (ManagerId < 0)
            {
                throw new ArgumentException("ManagerId");
            }

            const string SP = "dbo.JobPosting_GetAllJobPostingByClientAndManagerId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ClientId", DbType.Int32, ClientId);
                Database.AddInParameter(cmd, "@ManagerId", DbType.Int32, ManagerId );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();

                    while (reader.Read())
                    {
                        //jobPostingList.Add(new { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                        jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                    }

                    return jobPostingList;
                }
            }
        }


        ArrayList IJobPostingDataAccess.GetByCleintIdAndManagerId(int ClientId, int ManagerId, int count, string SearchKey)
        {
            if (ClientId < 0)
            {
                throw new ArgumentException("ClientId");
            }
            if (ManagerId < 0)
            {
                throw new ArgumentException("MemberId");
            }

            const string SP = "dbo.JobPosting_GetPagedByClientIdAndManagerID";
            string whereClause = string.Empty;
            if (ManagerId != 0) whereClause = " [J].[Id] IN (SELECT DISTINCT JobPostingId FROM dbo.JobPostingHiringTeam WHERE MemberId=" + ManagerId + ")  and ([J].[JobTitle] like '" + SearchKey + "%')";
            else whereClause = " ([J].[JobTitle] like '" + SearchKey + "%') ";

            if (ClientId > 0)
            {
                whereClause += " AND [J].[ClientId]=" + ClientId;
            }
            object[] paramValues = new object[] {	0,
													count ,
													StringHelper.Convert(whereClause ),
													StringHelper.Convert(""),
                                                    StringHelper.Convert("")
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();

                    while (reader.Read())
                    {
                        // jobPostingList.Add(new { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                        jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1), CreatorId = reader.GetInt32(2) });//1.3
                    }

                    return jobPostingList;
                }

                return null;
            }
        }





        ArrayList IJobPostingDataAccess.GetAllByInterview()
        {
            const string SP = "dbo.JobPosting_GetAllByInterview";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();

                    while (reader.Read())
                    {
                        jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });
                    }

                    return jobPostingList;
                }
            }
        }

        DataTable IJobPostingDataAccess.GetAllJobPostingByStatus(int status)
        {
            if (status < 0)
            {
                throw new ArgumentException("status");
            }

            const string SP = "dbo.JobPosting_GetAllJobPostingCodeByStatus";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Status", DbType.Int32, status);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    //DataSet dSet = new DataSet();
                    DataTable jobPostingList = new DataTable("dTab");
                    jobPostingList.Columns.Add("Id");
                    jobPostingList.Columns.Add("Code");
                    
                    while (reader.Read())
                    {
                        DataRow dRow = jobPostingList.NewRow();
                        dRow["Id"]=reader.GetInt32(0);
                        dRow["Code"]=reader.GetString(1);
                        jobPostingList.Rows.Add(dRow);
                    }
                    //dSet.Tables.Add(jobPostingList);
                    return jobPostingList;
                }
            }
        }

        int IJobPostingDataAccess.GetCountOfJobPostingByStatus()
        {
           

            const string SP = "dbo.JobPosting_GetCountOfJobPostingCodeByStatus";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
          

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                   

                    while (reader.Read())
                    {
                        return reader != null ? Convert .ToInt32 ( reader.GetValue(0).ToString()) : 0;
                    }
                    return 0;
                }
            }
        }

        string  IJobPostingDataAccess.GetJobPostingIdByJobPostingCode(string JobPostingCode)
        {
            if (string.IsNullOrEmpty(JobPostingCode))
            {
                throw new ArgumentNullException("JobPostingCode");
            }

            const string SP = "dbo.JobPosting_GetJobPostingIdByJobPostingCode";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingCode", DbType.String, JobPostingCode);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    string JobPostingId = string.Empty;

                    while (reader.Read())
                    {
                        JobPostingId = reader.GetString(0);
                    }

                    return JobPostingId;
                }
            }
        }

        ArrayList IJobPostingDataAccess.GetAllByStatusAndManagerId(int status, int managerId)
        {
            if (status < 0)
            {
                throw new ArgumentException("status");
            }

            const string SP = "dbo.JobPosting_GetAllJobPostingByStatusAndManagerId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Status", DbType.Int32, status);
                Database.AddInParameter(cmd, "@ManagerId", DbType.Int32, managerId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();
                   
                    while (reader.Read())
                    {
                       // jobPostingList.Add(new { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                        jobPostingList.Add(new JobPosting(){ Id = reader.GetInt32(0), JobTitle = reader.GetString(1),ClientId=reader.GetInt32(2) });//1.3
                    }

                    return jobPostingList;
                }
            }
        }

        ArrayList IJobPostingDataAccess.GetAllEmployeeReferralEnabled()
        {
            
            const string SP = "dbo.JobPosting_GetAllEmployeeReferralEnabled";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();

                    while (reader.Read())
                    {
                        jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                    }

                    return jobPostingList;
                }
            }
        }

        ArrayList IJobPostingDataAccess.GetJobPostingIdByReferrerIdForERPortal(int ReferrerId)
        {

            const string SP = "JobPosting_GetJobPostingIdByReferrerIdForERPortal";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ReferrerId", DbType.Int32, ReferrerId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();

                    while (reader.Read())
                    {
                        jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                    }

                    return jobPostingList;
                }
            }
        }

        ArrayList IJobPostingDataAccess.GetAllVendorPortalEnabled()
        {

            const string SP = "dbo.JobPosting_GetAllVendorPortalEnabled";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();

                    while (reader.Read())
                    {
                        jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                    }

                    return jobPostingList;
                }
            }
        }

        ArrayList IJobPostingDataAccess.GetAllJobPostingByCandidateId(int candidateId)
        {
            
            if (candidateId < 0)
            {
                throw new ArgumentException("candidateId");
            }
            const string SP = "dbo.JobPosting_GetAllJobPostingByCandidateId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CandidateId", DbType.Int32, candidateId );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();

                    while (reader.Read())
                    {
                        jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                    }

                    return jobPostingList;
                }
            }
        }
        IList<JobPosting> IJobPostingDataAccess.GetAllVolumeHireByStatus(int jobStatus)
        {
            if (jobStatus < 1)
            {
                throw new ArgumentNullException("jobStatus");
            }

            const string SP = "dbo.JobPosting_GetAllVolumeHireByStatus";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobStatus", DbType.Int32, jobStatus);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPosting>().BuildEntities(reader);
                }
            }
        }

        private string BuildAllKeyWordsQuery(string allKeyWords)
        {
            StringBuilder sbWhereClause = new StringBuilder();

            string[] keyWords = StringHelper.BuildKeyWordArray(allKeyWords);

            for (int i = 0; i < keyWords.Length; i++)
            {
                if (sbWhereClause.ToString() == String.Empty)
                {
                    sbWhereClause.Append(" ( [JPS].[SkillSet] LIKE '%" + keyWords[i].Trim() + "%' ");
                    sbWhereClause.Append(" OR [J].[JobTitle] LIKE '%" + keyWords[i].Trim() + "%' ");                    
                    sbWhereClause.Append(" OR Contains([J].[JobDescription], '\"" + keyWords[i].Trim() + "\"') ) ");
                }
                else
                {
                    sbWhereClause.Append(" AND ");
                    sbWhereClause.Append(" ( [JPS].[SkillSet] LIKE '%" + keyWords[i].Trim() + "%' ");
                    sbWhereClause.Append(" OR [J].[JobTitle] LIKE '%" + keyWords[i].Trim() + "%' ");
                    sbWhereClause.Append(" OR Contains([J].[JobDescription], '\"" + keyWords[i].Trim() + "\"') ) ");
                }
            }

            return sbWhereClause.ToString();
        }

        private string BuildAnyKeyWordQuery(string anyKeyWords)
        {
            StringBuilder sbWhereClause = new StringBuilder();

            string[] keyWords = StringHelper.BuildKeyWordArray(anyKeyWords);

            for (int i = 0; i < keyWords.Length; i++)
            {
                if (sbWhereClause.ToString() == String.Empty)
                {
                    sbWhereClause.Append(" ( [JPS].[SkillSet] LIKE '%" + keyWords[i].Trim() + "%'");
                    sbWhereClause.Append(" OR [J].[JobTitle] LIKE '%" + keyWords[i].Trim() + "%' ");
                    sbWhereClause.Append(" OR Contains([J].[JobDescription], '\"" + keyWords[i].Trim() + "\"') ) ");
                }
                else
                {
                    sbWhereClause.Append(" OR ");
                    sbWhereClause.Append(" ( [JPS].[SkillSet] LIKE '%" + keyWords[i].Trim() + "%'");
                    sbWhereClause.Append(" OR [J].[JobTitle] LIKE '%" + keyWords[i].Trim() + "%' ");
                    sbWhereClause.Append(" OR Contains([J].[JobDescription], '\"" + keyWords[i].Trim() + "\"') ) ");
                }
            }

            return sbWhereClause.ToString();
        }

        private string BuildLocationQuery(string location)
        {
            StringBuilder sbWhereClause = new StringBuilder();

            string[] keyWords = StringHelper.BuildKeyWordArray(location);

            for (int i = 0; i < keyWords.Length; i++)
            {
                if (sbWhereClause.ToString() == String.Empty)
                {
                    sbWhereClause.Append(" ( [J].[City] LIKE '%" + keyWords[i].Trim() + "%'");
                    sbWhereClause.Append(" OR [S].[Name] LIKE '%" + keyWords[i].Trim() + "%'");
                    sbWhereClause.Append(" OR [S].[StateCode] LIKE '%" + keyWords[i].Trim() + "%')");
                }
                else
                {
                    sbWhereClause.Append(" OR ( [J].[City] LIKE '%" + keyWords[i].Trim() + "%'");
                    sbWhereClause.Append(" OR [S].[Name] LIKE '%" + keyWords[i].Trim() + "%'");
                    sbWhereClause.Append(" OR [S].[StateCode] LIKE '%" + keyWords[i].Trim() + "%')");
                }
            }

            return sbWhereClause.ToString();
        }

        private string BuildAllAndAnyKeyWordQuery(string allKeyWords, string anyKeyWord)
        {
            string strQuery = string.Empty;
            strQuery = "( (" + BuildAllKeyWordsQuery(allKeyWords) + ") AND (" + BuildAnyKeyWordQuery(anyKeyWord) + ") )";
            return strQuery;
        }

        public int GetRequisitionCountByMemberId(int MemberId)      //0.3
        {
            const string SP = "dbo.JobPosting_GetRequisitionCountByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                int intRequisitionCount = (Database.ExecuteScalar(cmd) == null || Database.ExecuteScalar(cmd).ToString() == "") ? 0 : Convert.ToInt32(Database.ExecuteScalar(cmd)); //0.4 "put null" here 
                return intRequisitionCount;
            }
        }


        public int GetLastRequisitionCodeByMemberId(int MemberId)      //0.3
        {
            const string SP = "dbo.JobPosting_GetLastRequisitionCodeByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? 0 : reader.GetInt32(0) ;
                    } return 0;
                }
               
            }
        }

        public int UpdateMemberRequisitionCount(int MemberId, int RequisitionCount)     //0.3
        {
            const string SP = "dbo.JobPosting_UpdateMemberRequisitionCount";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                Database.AddInParameter(cmd, "@RequisitionCount", DbType.Int32, RequisitionCount);
                int intReqCount = 0;
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        intReqCount = reader.GetInt32(0);
                    }
                }
                return intReqCount;
            }
        }
        string IJobPostingDataAccess.GetJobPosting_RequiredSkillNamesByJobPostingId(int JobPostingId)
        {

            if (JobPostingId < 1)
            {
                throw new ArgumentNullException("JobPostingId");
            }

            const string SP = "dbo.JobPosting_GetRequiredSkillNamesByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                    return string.Empty;
                }
            }
        }

        PagedResponse<JobPosting> IJobPostingDataAccess.GetPagedForCandidatePortal(PagedRequest request,int MemberId)
        {
            const string SP = "dbo.JobPosting_GetPagedForCandidatePortal";
            string whereClause = string.Empty;
            string jobStatus = String.Empty;//0.9
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobTitle"))
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[JobTitle]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");
                            }
                        }
                        if (StringHelper.IsEqual(column, "ReqCode"))
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[JobPostingCode]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");
                            }
                        }
                        if (StringHelper.IsEqual(column, "StateId"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[StateId]");
                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }

                        if (StringHelper.IsEqual(column, "City"))
                        {
                            if (!string.Equals(value, ""))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[City]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");


                            }
                        }

                        if (StringHelper.IsEqual(column, "CountryId"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[CountryId]");
                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }




                    }
                }
                whereClause = sb.ToString();
            }
          
            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    MemberId 
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<JobPosting> response = new PagedResponse<JobPosting>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new JobPostingBuilder()).BuildEntitiesForCandidatePortal (reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        PagedResponse<JobPosting> IJobPostingDataAccess.GetPagedForVendorPortal(PagedRequest request,int VendorId)
        {
            const string SP = "dbo.JobPosting_GetPagedForVendorPortal";
            string whereClause = string.Empty;
            string jobStatus = String.Empty;//0.9
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobTitle"))
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[JobTitle]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");
                            }
                        }
                        if (StringHelper.IsEqual(column, "ReqCode"))
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[JobPostingCode]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");
                            }
                        }
                        if (StringHelper.IsEqual(column, "StateId"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[StateId]");
                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }

                        if (StringHelper.IsEqual(column, "City"))
                        {
                            if (!string.Equals(value, ""))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[City]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");


                            }
                        }

                        if (StringHelper.IsEqual(column, "CountryId"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[CountryId]");
                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }




                    }
                }
                whereClause = sb.ToString();
            }

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    VendorId 
                                                    
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<JobPosting> response = new PagedResponse<JobPosting>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new JobPostingBuilder()).BuildEntitiesForCandidatePortal(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }


        PagedResponse<JobPosting> IJobPostingDataAccess.GetPagedForVendorPortalForMyPerformance(PagedRequest request, int VendorId, int MemberId)
        {
            const string SP = "dbo.JobPosting_GetPagedForVendorPortalForMyPerformance";
            string whereClause = string.Empty;
            string jobStatus = String.Empty;//0.9
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobTitle"))
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[JobTitle]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");
                            }
                        }
                        if (StringHelper.IsEqual(column, "ReqCode"))
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[JobPostingCode]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");
                            }
                        }
                        if (StringHelper.IsEqual(column, "StateId"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[StateId]");
                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }

                        if (StringHelper.IsEqual(column, "City"))
                        {
                            if (!string.Equals(value, ""))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[City]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");


                            }
                        }

                        if (StringHelper.IsEqual(column, "CountryId"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[CountryId]");
                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }




                    }
                }
                whereClause = sb.ToString();
            }

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    VendorId ,
                                                    MemberId
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<JobPosting> response = new PagedResponse<JobPosting>();

                using (IDataReader reader = Database.ExecuteReader(cmd)) 
                {
                    response.Response = (new JobPostingBuilder()).BuildEntitiesForVendorPortalForMyPerformance(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<JobPosting> IJobPostingDataAccess.GetPagedEmployeeReferal(PagedRequest request)
        {
            const string SP = "dbo.JobPosting_GetPagedForEmpoloyeeReferal";
            string whereClause = string.Empty;
            string jobStatus = String.Empty;//0.9
            

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													string.Empty ,
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<JobPosting> response = new PagedResponse<JobPosting>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response =CreateEntityBuilder <JobPosting >().BuildEntities (reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        //***************Code added by pravin khot on 25/May/2016
        Int32 IJobPostingDataAccess.GetRequisitionStatusBy_Id(int JobPostingId)
        {
            const string SP = "dbo.GetRequisitionStatusBy_Id";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                    if (reader.Read())
                    {
                        return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                    else
                    {
                        return 0;
                    }
            }
        }

        Int32 IJobPostingDataAccess.GetTimeZoneId_MemberId(int MemberId)
        {
            const string SP = "dbo.GetTimeZoneId_MemberId";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                    if (reader.Read())
                    {
                        return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                    else
                    {
                        return 0;
                    }
            }
        }

        string IJobPostingDataAccess.GetRequisitionStatus_Id(int JobPostingId)
        {
            const string SP = "dbo.GetRequisitionStatus_Id";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                return string.Empty;
            }
        }
       
        //***************************END**********************************

        ArrayList IJobPostingDataAccess.GetAllBUId(int BUId)
        {
            if (BUId < 0)
            {
                throw new ArgumentException("status");
            }

            const string SP = "dbo.GetAllBUId_ByBUId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyBUId", DbType.Int32, BUId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList RequisitionMappingList = new ArrayList();

                    while (reader.Read())
                    {
                        RequisitionMappingList.Add(new JobPosting() { Id = reader.GetInt32(0), Name = reader.GetString(1) });//1.3
                    }

                    return RequisitionMappingList;
                }
            }
        }

        ArrayList IJobPostingDataAccess.GetAllBymemberId(int memberId)
        {


            const string SP = "dbo.[JobPosting_GetAllJobPostingListByMemberId]";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList jobPostingList = new ArrayList();

                    while (reader.Read())
                    {
                        //jobPostingList.Add(new { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                        jobPostingList.Add(new JobPosting() { Id = reader.GetInt32(0), JobTitle = reader.GetString(1) });//1.3
                    }

                    return jobPostingList;
                }
            }
        }

        #endregion
    }
}