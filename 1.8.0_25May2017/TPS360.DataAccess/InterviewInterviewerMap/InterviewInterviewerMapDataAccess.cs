﻿
/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewInterviewerMapDataAccess.cs
    Description: This is the class library page.
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              14/Dec/2015           Pravin khot          AddSuggestedInterviewer
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class InterviewInterviewerMapDataAccess : BaseDataAccess, IInterviewInterviewerMapDataAccess
    {
        #region Constructors

        public InterviewInterviewerMapDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<InterviewInterviewerMap> CreateEntityBuilder<InterviewInterviewerMap>()
        {
            return (new InterviewInterviewerMapBuilder()) as IEntityBuilder<InterviewInterviewerMap>;
        }

        #endregion

        #region  Methods

        InterviewInterviewerMap IInterviewInterviewerMapDataAccess.Add(InterviewInterviewerMap InterviewerMap)
        {
            const string SP = "dbo.InterviewInterviewerMap_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewerMap.InterviewId);
                Database.AddInParameter(cmd, "@InterviewerId", DbType.Int32  , InterviewerMap.InterviewerId );
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, InterviewerMap.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewerMap.UpdatorId);
                Database .AddInParameter (cmd ,"@IsClient", DbType .Boolean  ,InterviewerMap .IsClient  );
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        InterviewerMap = CreateEntityBuilder<InterviewInterviewerMap>().BuildEntity(reader);
                    }
                    else
                    {
                        InterviewerMap = null;
                    }
                }

                if (InterviewerMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("interviewer map already exists. Please specify another interviewer map.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this interviewer map.");
                            }
                    }
                }

                return InterviewerMap;
            }
        }

        //***************************code added by pravin khot on 14/Dec/2015

        InterviewInterviewerMap IInterviewInterviewerMapDataAccess.AddSuggestedInterviewer(InterviewInterviewerMap InterviewerMap)
        {
            const string SP = "dbo.InterviewSuggestedInterviewerMap_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewerMap.InterviewId);
                Database.AddInParameter(cmd, "@InterviewerId", DbType.Int32, InterviewerMap.InterviewerId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, InterviewerMap.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewerMap.UpdatorId);
                Database.AddInParameter(cmd, "@IsClient", DbType.Boolean, InterviewerMap.IsClient);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        InterviewerMap = CreateEntityBuilder<InterviewInterviewerMap>().BuildEntity(reader);
                    }
                    else
                    {
                        InterviewerMap = null;
                    }
                }

                if (InterviewerMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("interviewer map already exists. Please specify another interviewer map.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this interviewer map.");
                            }
                    }
                }

                return InterviewerMap;
            }
        }
        //****************************************End*********************************************************


        InterviewInterviewerMap IInterviewInterviewerMapDataAccess.Update(InterviewInterviewerMap InterviewerMap)
        {
            const string SP = "dbo.InterviewInterviewerMap_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, InterviewerMap.Id);
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32 , InterviewerMap.InterviewId);
                Database.AddInParameter(cmd, "@InterviewerId", DbType.Int32, InterviewerMap.InterviewerId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewerMap.UpdatorId);
                Database.AddInParameter(cmd, "@IsClient", DbType.Boolean, InterviewerMap.IsClient);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        InterviewerMap = CreateEntityBuilder<InterviewInterviewerMap>().BuildEntity(reader);
                    }
                    else
                    {
                        InterviewerMap = null;
                    }
                }

                if (InterviewerMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Interviewer map already exists. Please specify another interviewer map.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this interviewer map.");
                            }
                    }
                }

                return InterviewerMap;
            }
        }

        InterviewInterviewerMap IInterviewInterviewerMapDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.InterviewInterviewerMap_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<InterviewInterviewerMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        IList<InterviewInterviewerMap> IInterviewInterviewerMapDataAccess.GetAllInterviewersByInterviewId (int InterviewId)
        {
            if (InterviewId < 0)
            {
                throw new ArgumentNullException("InterviewId");
            }

            const string SP = "dbo.InterviewInterviewerMap_GetAllInterviewersByInterviewId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<InterviewInterviewerMap>().BuildEntities(reader);
                }
            }
        }
        //********Code added by pravin khot on 5/May/2016**********************
        IList<InterviewInterviewerMap> IInterviewInterviewerMapDataAccess.GetAllsuggestedInterviewId(int InterviewId)
        {
            if (InterviewId < 0)
            {
                throw new ArgumentNullException("InterviewId");
            }

            const string SP = "dbo.InterviewMap_GetAllsuggestedInterviewId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<InterviewInterviewerMap>().BuildEntities(reader);
                }
            }
        }
        //************************END***********************************
        IList<InterviewInterviewerMap> IInterviewInterviewerMapDataAccess.GetAll()
        {
            const string SP = "dbo.InterviewInterviewerMap_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<InterviewInterviewerMap>().BuildEntities(reader);
                }
            }
        }

        bool IInterviewInterviewerMapDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.InterviewInterviewerMap_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a interview interviewer mapp which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this interview interviewer map.");
                        }
                }
            }
        }


        bool IInterviewInterviewerMapDataAccess.DeleteByInterviewerId(int interviewerId)
        {
            if (interviewerId < 1)
            {
                throw new ArgumentNullException("interviewerId");
            }

            const string SP = "dbo.InterviewInterviewerMap_DeleteByInterviewerId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@InterviewerId", DbType.Int32, interviewerId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a interview interviewer mapp which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this interview interviewer map.");
                        }
                }
            }
        }

        bool IInterviewInterviewerMapDataAccess.DeleteByInterviewId(int interviewId)
        {
            if (interviewId < 1)
            {
                throw new ArgumentNullException("interviewId");
            }

            const string SP = "dbo.InterviewInterviewerMap_DeleteByInterviewId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, interviewId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a interview interviewer mapp which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this interview interviewer map.");
                        }
                }
            }
        }
        string IInterviewInterviewerMapDataAccess.GetInterviersNameByInterviewId (int InterviewId)
        {
            if (InterviewId < 1)
            {
                throw new ArgumentNullException("InterviewId");
            }
            const string SP = "dbo.Interview_GetInterviewsNameById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewId );

                string InterviewersName = (Database.ExecuteScalar(cmd) == null || Database.ExecuteScalar(cmd).ToString() == "") ? "" : Convert.ToString(Database.ExecuteScalar(cmd));
                return InterviewersName;
            }
        }
        #endregion
    }
}