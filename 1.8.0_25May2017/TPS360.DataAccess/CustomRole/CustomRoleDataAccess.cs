﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName:CustomRoleDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             3/March/2016          pravin khot           added GetAllUser        
-------------------------------------------------------------------------------------------------------------------------------------------       

>*/


using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class CustomRoleDataAccess : BaseDataAccess, ICustomRoleDataAccess
    {
        #region Constructors

        public CustomRoleDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CustomRole> CreateEntityBuilder<CustomRole>()
        {
            return (new CustomRoleBuilder()) as IEntityBuilder<CustomRole>;
        }

        #endregion

        #region  Methods

        CustomRole ICustomRoleDataAccess.Add(CustomRole customRole)
        {
            const string SP = "dbo.CustomRole_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, customRole.Name);
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(customRole.Description));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, customRole.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, customRole.CreatorId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        customRole = CreateEntityBuilder<CustomRole>().BuildEntity(reader);
                    }
                    else
                    {
                        customRole = null;
                    }
                }

                if (customRole == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Custom role already exists. Please specify another custom role.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this custom role.");
                            }
                    }
                }

                return customRole;
            }
        }

        CustomRole ICustomRoleDataAccess.Update(CustomRole customRole)
        {
            const string SP = "dbo.CustomRole_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, customRole.Id);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, customRole.Name);
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(customRole.Description));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, customRole.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, customRole.UpdatorId);
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        customRole = CreateEntityBuilder<CustomRole>().BuildEntity(reader);
                    }
                    else
                    {
                        customRole = null;
                    }
                }

                if (customRole == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Custom role already exists. Please specify another custom role.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this custom role.");
                            }
                    }
                }

                return customRole;
            }
        }

        CustomRole ICustomRoleDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CustomRole_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CustomRole>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        string ICustomRoleDataAccess.GetCustomRoleNameByMemberId(int MemberId)
        {
            if (MemberId < 1)
            {
                throw new ArgumentNullException("MemberId");
            }

            const string SP = "dbo.CustomRole_GetByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                    return string.Empty;
                }
            }
        }

        CustomRole ICustomRoleDataAccess.GetByName(string Name)
        {

            const string SP = "dbo.CustomRole_GetByName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString , Name );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CustomRole>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<CustomRole> ICustomRoleDataAccess.GetAll()
        {
            const string SP = "dbo.CustomRole_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CustomRole>().BuildEntities(reader);
                }
            }
        }
        //********Code added by pravin khot on 3/March/2016***************
        IList<CustomRole> ICustomRoleDataAccess.GetAllUser()
        {
            const string SP = "dbo.CustomRole_GetAllUser";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CustomRole>().BuildEntities(reader);
                }
            }        
           
         }
        //**********************END*****************************************
        PagedResponse<CustomRole> ICustomRoleDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.CustomRole_GetPaged";
            string whereClause = string.Empty;

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "Name";
            }

            request.SortColumn = "[CR].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<CustomRole> response = new PagedResponse<CustomRole>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<CustomRole>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        bool ICustomRoleDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CustomRole_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a custom role which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this custom role.");
                        }
                }
            }
        }

        #endregion
    }
}