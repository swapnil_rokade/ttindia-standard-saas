﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class EmployeeReferralBuilder : IEntityBuilder<EmployeeReferral>
    {



        IList<EmployeeReferral> IEntityBuilder<EmployeeReferral>.BuildEntities(IDataReader reader)
        {
            List<EmployeeReferral> employeesref = new List<EmployeeReferral>();

            while (reader.Read())
            {
                employeesref.Add(((IEntityBuilder<EmployeeReferral>)this).BuildEntity(reader));
            }

            return (employeesref.Count > 0) ? employeesref : null;
        }


     

        EmployeeReferral IEntityBuilder<EmployeeReferral>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_PRIMARYEMAIL = 1;
            const int FLD_FIRSTNAME = 2;
            const int FLD_LASTNAME = 3;            
            const int FLD_EMPLOYYEEID = 4;
            const int FLD_MEMBERID = 5;
            const int FLD_JOBPOSTINGID = 6;
            const int FLD_CREATORID = 7;
            const int FLD_UPDATORID = 8;
            const int FLD_CREATEDATE = 9;
            const int FLD_UPDATEDATE = 10;
            const int  FLD_CANDIDATENAME=11;
            const int FLD_JOBTITLE=12;
            EmployeeReferral employeeRef = new EmployeeReferral();

            employeeRef.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            employeeRef.RefererEmail  = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
            employeeRef.RefererFirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);            
            employeeRef.RefererLastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            employeeRef.EmployeeId = reader.IsDBNull(FLD_EMPLOYYEEID) ? string .Empty  : reader.GetString (FLD_EMPLOYYEEID);
            employeeRef.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            employeeRef.JobpostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? string.Empty : reader.GetString(FLD_JOBPOSTINGID);
            employeeRef.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            employeeRef.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            employeeRef.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            employeeRef.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            employeeRef.CandidateName = reader.IsDBNull(FLD_CANDIDATENAME) ? string.Empty : reader.GetString(FLD_CANDIDATENAME);
            employeeRef.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            return employeeRef;
        }
    }
}
