﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmployeeDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             8-Jnue-2009             Nagarathna V.B      Enhancement:10525; added "GetProductivityOverviewByDate"
 *  0.2             15-Dec-2009             Sandeesh            Enhancement:10525;Changes made for Submissions Count.    
 *  0.3             Mar-15-2010             Sudarshan R         Defect ID:12297; Changed made in the method GetPaged(). Changed Current country,state,city to Permanent country,state,city
 *  0.4             26/May/2016             Sumit Sonawane      GetAllListGroupByDate
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections;
using System.Data;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Text;
using TPS360.Common.BusinessEntity;
using System.Collections.Generic;
namespace TPS360.DataAccess
{
    internal sealed class EmployeeReferralDataAccess : BaseDataAccess, IEmployeeReferralDataAccess
    {
        #region Constructors

        public EmployeeReferralDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<EmployeeReferral> CreateEntityBuilder<EmployeeReferral>()
        {
            return (new EmployeeReferralBuilder()) as IEntityBuilder<EmployeeReferral>;
        }


       
        #endregion

        #region  Methods

        int IEmployeeReferralDataAccess.Add(EmployeeReferral employeeref)
        {
            const string SP = "dbo.EmployeeReferral_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@RefererEmail", DbType.AnsiString, StringHelper.Convert(employeeref.RefererEmail));
                Database.AddInParameter(cmd, "@RefererFirstName", DbType.AnsiString, StringHelper.Convert(employeeref.RefererFirstName));
                Database.AddInParameter(cmd, "@RefererLastName", DbType.AnsiString, StringHelper.Convert(employeeref.RefererLastName));
                Database.AddInParameter(cmd, "@EmployeeId", DbType.AnsiString, StringHelper.Convert(employeeref.EmployeeId));
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, StringHelper.Convert(employeeref.MemberId));
                Database.AddInParameter(cmd, "@JobpostingId", DbType.AnsiString, StringHelper.Convert(employeeref.JobpostingId));
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, StringHelper.Convert(employeeref.CreatorId));

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                }
                return 0;
            }
        }
        EmployeeReferral IEmployeeReferralDataAccess.GetByMemberId(int MemberID)
        {
            const string SP = "dbo.EmployeeReferral_GetByMemberID";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberID", DbType.Int32, MemberID);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<EmployeeReferral>()).BuildEntity(reader);
                    }
                    else return null;
                }
            }
        }

        PagedResponse<EmployeeReferral> IEmployeeReferralDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.EmployeeReferral_GetPaged";
            string WhereClause = BuildWhereClause(request);

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(WhereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<EmployeeReferral> response = new PagedResponse<EmployeeReferral>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<EmployeeReferral>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        private string BuildWhereClause(PagedRequest request)
        {
            string whereClause = string.Empty, strbranchmap = string.Empty;
            bool flag = false;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "RefererId"))
                        {
                            if (sb.ToString().Trim() != string.Empty) sb.Append(" And ");
                            sb.Append("[ER].[EmployeeReferrerId]");
                            sb.Append(" ='");
                            sb.Append(value);
                            sb.Append("'");
                        }

                        if (StringHelper.IsEqual(column, "JobPostingID"))
                        {
                            if (sb.ToString().Trim() != string.Empty) sb.Append(" And ");
                            sb.Append("[ER].[JobPostingID]");
                            sb.Append(" =");
                            sb.Append(value);
                            sb.Append(" ");
                        }

                        if (StringHelper.IsEqual(column, "AddedFrom"))
                        {
                            if (sb.ToString().Trim() != string.Empty) sb.Append(" And ");
                            sb.Append("[ER].[CreateDate]");
                            sb.Append(" >=convert(datetime,'");
                            sb.Append(value);
                            sb.Append("',103)");

                        }
                        if (StringHelper.IsEqual(column, "AddedTo"))
                        {
                            if (sb.ToString().Trim() != string.Empty) sb.Append(" And ");
                            sb.Append("[ER].[CreateDate]");
                            sb.Append(" <=convert(datetime,'");
                            sb.Append(value);
                            sb.Append("',103) +1");
                        }
                    }
                }

                whereClause = sb.ToString();
            }
            return whereClause;
        }

        PagedResponse<ListWithCount> IEmployeeReferralDataAccess.GetPagedReferralSummary(PagedRequest request, string type)
        {

            string SP = "dbo.EmployeeReferral_GetPagedByReferrer";
            if (type == "ByRequisition") SP = "dbo.EmployeeReferral_GetPagedByRequisition";
            string WhereClause = BuildWhereClause(request);
            
            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(WhereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};


            using (DbCommand cmd = Database.GetStoredProcCommand(SP,paramValues ))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    
                    PagedResponse<ListWithCount> response = new PagedResponse<ListWithCount >();
                    IList<ListWithCount> Resule = new List<ListWithCount>();
                    while (reader.Read())
                    {
                        ListWithCount list = new ListWithCount();
                        list.Id = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        list.Name = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
                        list.Count = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                        Resule .Add (list );
                            
                    }
                    response.Response = Resule;
                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                    return response;
                }
            }
        }


        ArrayList IEmployeeReferralDataAccess.GetAllReferrerList()
        {
            const string SP = "dbo.EmployeeReferral_GetAllReferrerList";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList clientList = new ArrayList();

                    while (reader.Read())
                    {
                        clientList.Add(new { ID=reader .GetInt32(0), Email = reader.GetString (1), Name = reader.GetString(2) });
                    }

                    return clientList;
                }
            }
        }

        IList<ListWithCount> IEmployeeReferralDataAccess.GetAllListGroupByDate(int JobPostingId, int ReferrerID, int StartDate,int EndDate)
        {
            const string SP = "dbo.EmployeeReferral_GetCount";
            StringBuilder WhereClause = new StringBuilder();
            if (JobPostingId > 0)
            {
                WhereClause.Append(" JobPostingId=" + JobPostingId);
            }
            if (ReferrerID > 0)
            {
                if (WhereClause.ToString() != string.Empty) WhereClause.Append(" And ");
                WhereClause.Append(" [EmployeeReferrerId] = " + ReferrerID);
            }
            if (StartDate > 0)
            {
                if (WhereClause.ToString() != string.Empty) WhereClause.Append(" And ");
                WhereClause.Append(" [intCreateDate] >= " + StartDate );
            }
            if (EndDate  > 0)
            {
                if (WhereClause.ToString() != string.Empty) WhereClause.Append(" And ");
                WhereClause.Append(" [intCreateDate] <= " + EndDate );
            }

            IList<ListWithCount> result = new List<ListWithCount>();
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@WhereClause", DbType.AnsiString, StringHelper.Convert(WhereClause ));
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList clientList = new ArrayList();

                    while (reader.Read())
                    {
                        ListWithCount list = new ListWithCount();
                        list.Name = reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                        list.Count = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                        result.Add(list);
                    }

                    return result;
                }
            }
        }
        //////////////////////code added by Sumit on 26/May/2016////////////////////////////////////////////////////////////////////////////
        IList<ListWithCount> IEmployeeReferralDataAccess.GetAllListGroupByDate(string JobPostingId, int ReferrerID, int StartDate, int EndDate)
        {
            const string SP = "dbo.EmployeeReferral_GetCount";
            StringBuilder WhereClause = new StringBuilder();
            int JobPostingId1 = int.Parse(JobPostingId);
            if (JobPostingId1 > 0)
            {
                WhereClause.Append(" JobPostingId=" + JobPostingId1);
            }
            if (ReferrerID > 0)
            {
                if (WhereClause.ToString() != string.Empty) WhereClause.Append(" And ");
                WhereClause.Append(" [EmployeeReferrerId] = " + ReferrerID);
            }
            if (StartDate > 0)
            {
                if (WhereClause.ToString() != string.Empty) WhereClause.Append(" And ");
                WhereClause.Append(" [intCreateDate] >= " + StartDate);
            }
            if (EndDate > 0)
            {
                if (WhereClause.ToString() != string.Empty) WhereClause.Append(" And ");
                WhereClause.Append(" [intCreateDate] <= " + EndDate);
            }

            IList<ListWithCount> result = new List<ListWithCount>();
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@WhereClause", DbType.AnsiString, StringHelper.Convert(WhereClause));
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList clientList = new ArrayList();

                    while (reader.Read())
                    {
                        ListWithCount list = new ListWithCount();
                        list.Name = reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                        list.Count = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                        result.Add(list);
                    }

                    return result;
                }
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #endregion
    }
}