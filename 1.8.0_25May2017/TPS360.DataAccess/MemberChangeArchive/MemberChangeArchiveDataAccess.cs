﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberChangeArchiveDataAccess : BaseDataAccess, IMemberChangeArchiveDataAccess
    {
        #region Constructors

        public MemberChangeArchiveDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberChangeArchive> CreateEntityBuilder<MemberChangeArchive>()
        {
            return (new MemberChangeArchiveBuilder()) as IEntityBuilder<MemberChangeArchive>;
        }

        #endregion

        #region  Methods

        MemberChangeArchive IMemberChangeArchiveDataAccess.Add(MemberChangeArchive memberChangeArchive)
        {
            const string SP = "dbo.MemberChangeArchive_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberChangeArchive.MemberId);
                Database.AddInParameter(cmd, "@ChangedObject", DbType.Binary, memberChangeArchive.ChangedObject);
                Database.AddInParameter(cmd, "@IsRemove", DbType.Boolean, memberChangeArchive.IsRemove);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberChangeArchive.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberChangeArchive = CreateEntityBuilder<MemberChangeArchive>().BuildEntity(reader);
                    }
                    else
                    {
                        memberChangeArchive = null;
                    }
                }

                if (memberChangeArchive == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member change archive already exists. Please specify another member change archive.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member change archive.");
                            }
                    }
                }

                return memberChangeArchive;
            }
        }

        MemberChangeArchive IMemberChangeArchiveDataAccess.Update(MemberChangeArchive memberChangeArchive)
        {
            const string SP = "dbo.MemberChangeArchive_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberChangeArchive.Id);

                Database.AddInParameter(cmd, "@ChangedObject", DbType.Binary, memberChangeArchive.ChangedObject);
                Database.AddInParameter(cmd, "@IsRemove", DbType.Boolean, memberChangeArchive.IsRemove);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberChangeArchive.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberChangeArchive = CreateEntityBuilder<MemberChangeArchive>().BuildEntity(reader);
                    }
                    else
                    {
                        memberChangeArchive = null;
                    }
                }

                if (memberChangeArchive == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member change archive already exists. Please specify another member change archive.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member change archive.");
                            }
                    }
                }

                return memberChangeArchive;
            }
        }

        MemberChangeArchive IMemberChangeArchiveDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberChangeArchive_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberChangeArchive>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberChangeArchive> IMemberChangeArchiveDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberChangeArchive_GetAllMemberChangeArchiveByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberChangeArchive>().BuildEntities(reader);
                }
            }
        }

        IList<MemberChangeArchive> IMemberChangeArchiveDataAccess.GetAll()
        {
            const string SP = "dbo.MemberChangeArchive_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberChangeArchive>().BuildEntities(reader);
                }
            }
        }

        bool IMemberChangeArchiveDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberChangeArchive_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member change archive which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member change archive.");
                        }
                }
            }
        }

        #endregion
    }
}