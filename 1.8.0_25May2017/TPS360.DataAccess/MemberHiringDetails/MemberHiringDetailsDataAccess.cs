﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Text;
namespace TPS360.DataAccess
{
    internal sealed class MemberHiringDetailsDataAccess : BaseDataAccess, IMemberHiringDetailsDataAccess
    {
        #region Constructors

        public MemberHiringDetailsDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberHiringDetails> CreateEntityBuilder<MemberHiringDetails>()
        {
            return (new MemberHiringDetailsBuilder()) as IEntityBuilder<MemberHiringDetails>;
        }

        #endregion

        #region  Methods

        void  IMemberHiringDetailsDataAccess.Add(MemberHiringDetails memberHiringDetails,string MemberId)
        {
            const string SP = "dbo.MemberHiringDetails_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.AnsiString  , MemberId );
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32 , memberHiringDetails.JobPostingId );
                Database.AddInParameter(cmd, "@OfferedPosition", DbType.AnsiString, StringHelper.Convert(memberHiringDetails.OfferedPosition ));
                Database.AddInParameter(cmd, "@OfferedSalary", DbType.AnsiString  , memberHiringDetails.OfferedSalary );
                Database.AddInParameter(cmd, "@OfferedSalaryPayCycle", DbType.Int32 , memberHiringDetails.OfferedSalaryPayCycle );
                Database.AddInParameter(cmd, "@OfferedSalaryCurrency", DbType.Int32 , memberHiringDetails.OfferedSalaryCurrency );
                Database.AddInParameter(cmd, "@OfferAccepted", DbType.Boolean, memberHiringDetails.OfferAccepted );
                Database.AddInParameter(cmd, "@JoiningDate", DbType.DateTime ,NullConverter.Convert( memberHiringDetails .JoiningDate)  );
                Database.AddInParameter(cmd, "@CommissionPayRate", DbType.AnsiString  , memberHiringDetails.CommissionPayRate );
                Database.AddInParameter(cmd, "@CommissionCurrency", DbType.Int32 , memberHiringDetails.CommissionCurrency );
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberHiringDetails.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberHiringDetails.CreatorId);
                Database.AddInParameter(cmd, "@OfferedDate", DbType.Date, NullConverter.Convert(memberHiringDetails.OfferedDate));
                Database.AddInParameter(cmd, "@CurrentCTC", DbType.AnsiString, memberHiringDetails.CurrentCTC);
                Database.AddInParameter(cmd, "@CurrentCTCLookupId", DbType.Int32, memberHiringDetails.CurrentCTCLookupId);
                Database.AddInParameter(cmd, "@MinExpectedCTC", DbType.AnsiString, memberHiringDetails.MinExpectedCTC);
                Database.AddInParameter(cmd, "@MaxExpectedCTC", DbType.AnsiString, memberHiringDetails.MaxExpectedCTC);
                Database.AddInParameter(cmd, "@ExpectedCTCLookupId", DbType.Int32, memberHiringDetails.ExpectedCTCLookupId);
                Database.AddInParameter(cmd, "@Grade", DbType.AnsiString, memberHiringDetails.Grade);
                Database.AddInParameter(cmd, "@Experience", DbType.AnsiString, memberHiringDetails.Experience);
                Database.AddInParameter(cmd, "@Source", DbType.Int32, memberHiringDetails.Source);
                Database.AddInParameter(cmd, "@SourceDescription", DbType.AnsiString, memberHiringDetails.SourceDescription);
                Database.AddInParameter(cmd, "@FitmentPercentile", DbType.Int32, memberHiringDetails.FitmentPercentile);
                Database.AddInParameter(cmd, "@JoiningBonus", DbType.AnsiString, memberHiringDetails.JoiningBonus);
                Database.AddInParameter(cmd, "@RelocationAllowance", DbType.AnsiString, memberHiringDetails.RelocationAllowance);
                Database.AddInParameter(cmd, "@SpecialLumpSum", DbType.AnsiString, memberHiringDetails.SpecialLumpSum);
                Database.AddInParameter(cmd, "@NoticePeriodPayout", DbType.AnsiString, memberHiringDetails.NoticePeriodPayout);
                Database.AddInParameter(cmd, "@PlacementFees", DbType.AnsiString, memberHiringDetails.PlacementFees);
                Database.AddInParameter(cmd, "@BVStatus", DbType.Int32, memberHiringDetails.BVStatus);

                Database.AddInParameter(cmd, "@OfferedCategory", DbType.Int32,memberHiringDetails.OfferCategory);

                Database.AddInParameter(cmd, "@BasicEmbedded", DbType.Decimal, memberHiringDetails.BasicEmbedded);
                Database.AddInParameter(cmd, "@HRAEmbedded",  DbType.Decimal, memberHiringDetails.HRAEmbedded);
                Database.AddInParameter(cmd, "@Conveyance",  DbType.Decimal, memberHiringDetails.Conveyance);
                Database.AddInParameter(cmd, "@Medical",  DbType.Decimal, memberHiringDetails.Medical);
                Database.AddInParameter(cmd, "@EducationalAllowanceEmbedded",  DbType.Decimal, memberHiringDetails.EducationalAllowanceEmbedded);
                Database.AddInParameter(cmd, "@LUFS",  DbType.Decimal, memberHiringDetails.LUFS);
                Database.AddInParameter(cmd, "@Adhoc",  DbType.Decimal, memberHiringDetails.Adhoc);
                Database.AddInParameter(cmd, "@MPP",  DbType.Decimal, memberHiringDetails.MPP);
                Database.AddInParameter(cmd, "@AGVI",  DbType.Decimal, memberHiringDetails.AGVI);

                Database.AddInParameter(cmd, "@LTAEmbedded",  DbType.Decimal, memberHiringDetails.LTAEmbedded);
                Database.AddInParameter(cmd, "@PFEmbedded",  DbType.Decimal, memberHiringDetails.PFEmbedded);
                Database.AddInParameter(cmd, "@GratuityEmbedded",  DbType.Decimal, memberHiringDetails.GratuityEmbedded);
                Database.AddInParameter(cmd, "@MaximumAnnualIncentive",  DbType.Decimal, memberHiringDetails.MaximumAnnualIncentive);
                Database.AddInParameter(cmd, "@EmbeddedAllowance", DbType.Decimal, memberHiringDetails.EmbeddedAllowance);
                Database.AddInParameter(cmd, "@SalesIncentive", DbType.Decimal, memberHiringDetails.SalesIncentive);
                Database.AddInParameter(cmd, "@PLMAllowance", DbType.Decimal, memberHiringDetails.PLMAllowance);
                Database.AddInParameter(cmd, "@CarAllowance", DbType.Decimal, memberHiringDetails.CarAllowance);               
                Database.AddInParameter(cmd, "@GrandTotal",  DbType.Decimal, memberHiringDetails.GrandTotal);
                Database.AddInParameter(cmd, "@BasicMech",  DbType.Decimal, memberHiringDetails.BasicMech);
                Database.AddInParameter(cmd, "@FlexiPay1",  DbType.Decimal, memberHiringDetails.FlexiPay1);
                Database.AddInParameter(cmd, "@FlexiPay2",  DbType.Decimal, memberHiringDetails.FlexiPay2);
                Database.AddInParameter(cmd, "@AdditionalAllowance",  DbType.Decimal, memberHiringDetails.AdditionalAllowance);
                Database.AddInParameter(cmd, "@AdHocAllowance",  DbType.Decimal, memberHiringDetails.AdHocAllowance  );

                Database.AddInParameter(cmd, "@LocationAllowance",  DbType.Decimal, memberHiringDetails.LocationAllowance);
                Database.AddInParameter(cmd, "@SAFAllowance",  DbType.Decimal, memberHiringDetails.SAFAllowance);
                Database.AddInParameter(cmd, "@HRAMech",  DbType.Decimal, memberHiringDetails.HRAMech);
                Database.AddInParameter(cmd, "@HLISA",  DbType.Decimal, memberHiringDetails.HLISA);
                Database.AddInParameter(cmd, "@EducationAllowanceMech",  DbType.Decimal, memberHiringDetails.EducationAllowanceMech);
                Database.AddInParameter(cmd, "@ACLRA",  DbType.Decimal, memberHiringDetails.ACLRA);
                Database.AddInParameter(cmd, "@CLRA",  DbType.Decimal, memberHiringDetails.CLRA);
                Database.AddInParameter(cmd, "@SpecialAllowance",  DbType.Decimal, memberHiringDetails.SpecialAllowance);
                Database.AddInParameter(cmd, "@SpecialPerformancePay",  DbType.Decimal, memberHiringDetails.SpecialPerformancePay);
                Database.AddInParameter(cmd, "@ECAL",  DbType.Decimal, memberHiringDetails.ECAL);

                Database.AddInParameter(cmd, "@CarMileageReimbursement",  DbType.Decimal, memberHiringDetails.CarMileageReimbursement);
                Database.AddInParameter(cmd, "@TelephoneReimbursement",  DbType.Decimal, memberHiringDetails.TelephoneReimbursement);
                Database.AddInParameter(cmd, "@LTAMech",  DbType.Decimal, memberHiringDetails.LTAMech);
                Database.AddInParameter(cmd, "@PFMech",  DbType.Decimal, memberHiringDetails.PFMech);
                Database.AddInParameter(cmd, "@GratuityMech",  DbType.Decimal, memberHiringDetails.GratuityMech);
                Database.AddInParameter(cmd, "@MedicalreimbursementsDomiciliary",  DbType.Decimal, memberHiringDetails.MedicalreimbursementsDomiciliary);
                Database.AddInParameter(cmd, "@PCScheme",  DbType.Decimal, memberHiringDetails.PCScheme);
                Database.AddInParameter(cmd, "@RetentionPay",  DbType.Decimal, memberHiringDetails.RetentionPay);
                Database.AddInParameter(cmd, "@PLRMech", DbType.Decimal, memberHiringDetails.PLRMech);
                Database.AddInParameter(cmd, "@SalesIncentiveMech", DbType.Decimal, memberHiringDetails.SalesIncentiveMech);
                Database.AddInParameter(cmd, "@RoleAllowance", DbType.Decimal, memberHiringDetails.RoleAllowance);
                Database.AddInParameter(cmd, "@SiteAllowance", DbType.Decimal, memberHiringDetails.SiteAllowance);
                Database.AddInParameter(cmd, "@RetentionBonus", DbType.Decimal, memberHiringDetails.RetentionBonus);
                Database.AddInParameter(cmd, "@Reimbursement", DbType.Decimal, memberHiringDetails.Reimbursement);
                Database.AddInParameter(cmd, "@ESIC", DbType.Decimal, memberHiringDetails.ESIC);
                Database.AddInParameter(cmd, "@Bonus", DbType.Decimal, memberHiringDetails.Bonus);
                Database.AddInParameter(cmd, "@RelocationAllowanceForCom", DbType.Decimal, memberHiringDetails.RelocationAllowanceForCom);
                Database.AddInParameter(cmd, "@PerformanceLinkedIncentive", DbType.Decimal, memberHiringDetails.PerformanceLinkedIncentive);
                Database.AddInParameter(cmd, "@TotalCTC",  DbType.Decimal, memberHiringDetails.TotalCTC);
                Database.AddInParameter(cmd, "@ActiveRecruiter", DbType.Int32, memberHiringDetails.ActiveRecruiterID);
                Database.AddInParameter(cmd, "@ActivityID", DbType.Int32, memberHiringDetails.ActiveRecruiterID);
                Database.AddInParameter(cmd, "@JobTitle", DbType.AnsiString, memberHiringDetails.JobTitle1);
                Database.AddInParameter(cmd, "@Supervisor", DbType.AnsiString, memberHiringDetails.Supervisor);
                Database.AddInParameter(cmd, "@CityId", DbType.Int32, memberHiringDetails.CityId);
                try 
                {
                    Database.ExecuteNonQuery(cmd);
                }
                catch { }
                
            }
        }

        MemberHiringDetails IMemberHiringDetailsDataAccess.Update(MemberHiringDetails memberHiringDetails)
        {
            const string SP = "dbo.MemberHiringDetails_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberHiringDetails.Id);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberHiringDetails.MemberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, memberHiringDetails.JobPostingId);
                Database.AddInParameter(cmd, "@OfferedPosition", DbType.AnsiString, StringHelper.Convert(memberHiringDetails.OfferedPosition));
                Database.AddInParameter(cmd, "@OfferedSalary", DbType.AnsiString, memberHiringDetails.OfferedSalary);
                Database.AddInParameter(cmd, "@OfferedSalaryPayCycle", DbType.Int32, memberHiringDetails.OfferedSalaryPayCycle);
                Database.AddInParameter(cmd, "@OfferedSalaryCurrency", DbType.Int32, memberHiringDetails.OfferedSalaryCurrency);
                Database.AddInParameter(cmd, "@OfferAccepted", DbType.Boolean, memberHiringDetails.OfferAccepted);
                Database.AddInParameter(cmd, "@JoiningDate", DbType.DateTime, NullConverter.Convert(memberHiringDetails.JoiningDate));
               
                Database.AddInParameter(cmd, "@CommissionPayRate", DbType.AnsiString , memberHiringDetails.CommissionPayRate);
                Database.AddInParameter(cmd, "@CommissionCurrency", DbType.Int32, memberHiringDetails.CommissionCurrency);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberHiringDetails.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberHiringDetails.UpdatorId );

                Database.AddInParameter(cmd, "OfferedDate", DbType.Date, NullConverter.Convert (memberHiringDetails.OfferedDate));
                Database.AddInParameter(cmd, "@CurrentCTC", DbType.AnsiString, memberHiringDetails.CurrentCTC);
                Database.AddInParameter(cmd, "@CurrentCTCLookupId", DbType.Int32, memberHiringDetails.CurrentCTCLookupId);
                Database.AddInParameter(cmd, "@MinExpectedCTC", DbType.AnsiString, memberHiringDetails.MinExpectedCTC);
                Database.AddInParameter(cmd, "@MaxExpectedCTC", DbType.AnsiString, memberHiringDetails.MaxExpectedCTC);
                Database.AddInParameter(cmd, "@ExpectedCTCLookupId", DbType.Int32, memberHiringDetails.ExpectedCTCLookupId);
                Database.AddInParameter(cmd, "@Grade", DbType.AnsiString, memberHiringDetails.Grade);
                Database.AddInParameter(cmd, "@Experience", DbType.AnsiString, memberHiringDetails.Experience);
                Database.AddInParameter(cmd, "@Source", DbType.Int32, memberHiringDetails.Source);
                Database.AddInParameter(cmd, "@SourceDescription", DbType.AnsiString, memberHiringDetails.SourceDescription);
                Database.AddInParameter(cmd, "@FitmentPercentile", DbType.Int32, memberHiringDetails.FitmentPercentile);
                Database.AddInParameter(cmd, "@JoiningBonus", DbType.AnsiString, memberHiringDetails.JoiningBonus);
                Database.AddInParameter(cmd, "@RelocationAllowance", DbType.AnsiString, memberHiringDetails.RelocationAllowance);
                Database.AddInParameter(cmd, "@SpecialLumpSum", DbType.AnsiString, memberHiringDetails.SpecialLumpSum);
                Database.AddInParameter(cmd, "@NoticePeriodPayout", DbType.AnsiString, memberHiringDetails.NoticePeriodPayout);
                Database.AddInParameter(cmd, "@PlacementFees", DbType.AnsiString, memberHiringDetails.PlacementFees);
                Database.AddInParameter(cmd, "@BVStatus", DbType.Int32, memberHiringDetails.BVStatus);


                Database.AddInParameter(cmd, "@OfferCategory", DbType.Int32, memberHiringDetails.OfferCategory);

                Database.AddInParameter(cmd, "@BasicEmbedded", DbType.Decimal, memberHiringDetails.BasicEmbedded);
                Database.AddInParameter(cmd, "@HRAEmbedded", DbType.Decimal, memberHiringDetails.HRAEmbedded);
                Database.AddInParameter(cmd, "@Conveyance", DbType.Decimal, memberHiringDetails.Conveyance);
                Database.AddInParameter(cmd, "@Medical", DbType.Decimal, memberHiringDetails.Medical);
                Database.AddInParameter(cmd, "@EducationalAllowanceEmbedded", DbType.Decimal, memberHiringDetails.EducationalAllowanceEmbedded);
                Database.AddInParameter(cmd, "@LUFS", DbType.Decimal, memberHiringDetails.LUFS);
                Database.AddInParameter(cmd, "@Adhoc", DbType.Decimal, memberHiringDetails.Adhoc);
                Database.AddInParameter(cmd, "@MPP", DbType.Decimal, memberHiringDetails.MPP);
                Database.AddInParameter(cmd, "@AGVI", DbType.Decimal, memberHiringDetails.AGVI);

                Database.AddInParameter(cmd, "@LTAEmbedded", DbType.Decimal, memberHiringDetails.LTAEmbedded);
                Database.AddInParameter(cmd, "@PFEmbedded", DbType.Decimal, memberHiringDetails.PFEmbedded);
                Database.AddInParameter(cmd, "@GratuityEmbedded", DbType.Decimal, memberHiringDetails.GratuityEmbedded);
                Database.AddInParameter(cmd, "@MaximumAnnualIncentive", DbType.Decimal, memberHiringDetails.MaximumAnnualIncentive);
                Database.AddInParameter(cmd, "@EmbeddedAllowance", DbType.Decimal, memberHiringDetails.EmbeddedAllowance);
                Database.AddInParameter(cmd, "@SalesIncentive", DbType.Decimal, memberHiringDetails.SalesIncentive);
                Database.AddInParameter(cmd, "@PLMAllowance", DbType.Decimal, memberHiringDetails.PLMAllowance);
                Database.AddInParameter(cmd, "@CarAllowance", DbType.Decimal, memberHiringDetails.CarAllowance);               
                Database.AddInParameter(cmd, "@GrandTotal", DbType.Decimal, memberHiringDetails.GrandTotal);
                Database.AddInParameter(cmd, "@BasicMech", DbType.Decimal, memberHiringDetails.BasicMech);
                Database.AddInParameter(cmd, "@FlexiPay1", DbType.Decimal, memberHiringDetails.FlexiPay1);
                Database.AddInParameter(cmd, "@FlexiPay2", DbType.Decimal, memberHiringDetails.FlexiPay2);
                Database.AddInParameter(cmd, "@AdditionalAllowance", DbType.Decimal, memberHiringDetails.AdditionalAllowance);
                Database.AddInParameter(cmd, "@AdHocAllowance", DbType.Decimal, memberHiringDetails.AdditionalAllowance);

                Database.AddInParameter(cmd, "@LocationAllowance", DbType.Decimal, memberHiringDetails.LocationAllowance);
                Database.AddInParameter(cmd, "@SAFAllowance", DbType.Decimal, memberHiringDetails.SAFAllowance);
                Database.AddInParameter(cmd, "@HRAMech", DbType.Decimal, memberHiringDetails.HRAMech);
                Database.AddInParameter(cmd, "@HLISA", DbType.Decimal, memberHiringDetails.HLISA);
                Database.AddInParameter(cmd, "@EducationAllowanceMech", DbType.Decimal, memberHiringDetails.EducationAllowanceMech);
                Database.AddInParameter(cmd, "@ACLRA", DbType.Decimal, memberHiringDetails.ACLRA);
                Database.AddInParameter(cmd, "@CLRA", DbType.Decimal, memberHiringDetails.CLRA);
                Database.AddInParameter(cmd, "@SpecialAllowance", DbType.Decimal, memberHiringDetails.SpecialAllowance);
                Database.AddInParameter(cmd, "@SpecialPerformancePay", DbType.Decimal, memberHiringDetails.SpecialPerformancePay);
                Database.AddInParameter(cmd, "@ECAL", DbType.Decimal, memberHiringDetails.ECAL);

                Database.AddInParameter(cmd, "@CarMileageReimbursement", DbType.Decimal, memberHiringDetails.CarMileageReimbursement);
                Database.AddInParameter(cmd, "@TelephoneReimbursement", DbType.Decimal, memberHiringDetails.TelephoneReimbursement);
                Database.AddInParameter(cmd, "@LTAMech", DbType.Decimal, memberHiringDetails.LTAMech);
                Database.AddInParameter(cmd, "@PFMech", DbType.Decimal, memberHiringDetails.PFMech);
                Database.AddInParameter(cmd, "@GratuityMech", DbType.Decimal, memberHiringDetails.GratuityMech);
                Database.AddInParameter(cmd, "@MedicalreimbursementsDomiciliary", DbType.Decimal, memberHiringDetails.MedicalreimbursementsDomiciliary);
                Database.AddInParameter(cmd, "@PCScheme", DbType.Decimal, memberHiringDetails.PCScheme);
                Database.AddInParameter(cmd, "@RetentionPay", DbType.Decimal, memberHiringDetails.RetentionPay);
                Database.AddInParameter(cmd, "@PLRMech", DbType.Decimal, memberHiringDetails.PLRMech);
                Database.AddInParameter(cmd, "@SalesIncentiveMech", DbType.Decimal, memberHiringDetails.SalesIncentiveMech);
               
                Database.AddInParameter(cmd, "@TotalCTC", DbType.Decimal, memberHiringDetails.TotalCTC);
                Database.AddInParameter(cmd, "@ActiveRecruiter", DbType.Int32 , memberHiringDetails.ActiveRecruiterID );
                Database.AddInParameter(cmd, "@CityId", DbType.Int32, memberHiringDetails.CityId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberHiringDetails = CreateEntityBuilder<MemberHiringDetails>().BuildEntity(reader);
                    }
                    else
                    {
                        memberHiringDetails = null;
                    }
                }

                if (memberHiringDetails == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("memberHiringDetails already exists. Please specify another memberHiringDetails.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberHiringDetails.");
                            }
                    }
                }

                return memberHiringDetails;
            }
        }

        MemberHiringDetails IMemberHiringDetailsDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberHiringDetails_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberHiringDetails>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberHiringDetails IMemberHiringDetailsDataAccess.GetByMemberIdAndJobPosstingID (int memberId, int jobpostingid)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            if (jobpostingid  < 1)
            {
                throw new ArgumentNullException("jobpostingid");
            }
            const string SP = "dbo.MemberHiringDetails_GetByMemberIdAndJobPostingID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobpostingid );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberHiringDetails>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }           
        }

        bool IMemberHiringDetailsDataAccess.DeleteHiringDetialsById(int HiringDetailsID)
        {
            if (HiringDetailsID < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberHiringDetails_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, HiringDetailsID);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member Hiring Detail which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member hiring detail.");
                        }
                }
            }
        }

        PagedResponse<MemberHiringDetails> IMemberHiringDetailsDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberHiringDetails_GetPaged";
            string whereClause = string.Empty;
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");
                        if (!StringHelper.IsBlank(column))
                        {
                            value = value.Replace("'", "''");
                            if (StringHelper.IsEqual(column, "MemberId"))
                            {
                                sb.Append("([MHD].[CreatorId]");
                                sb.Append(" = ");
                                sb.Append(value);
                                sb.Append(" or [MJC].[CreatorId]=");
                                sb.Append(value);
                                sb.Append(" ) ");
                            }

                            if (StringHelper.IsEqual(column, "OfferLevelName"))
                            {
                                if (sb.ToString().Trim() != string.Empty) sb.Append(" And");
                                sb.Append("[HML].[Name]");
                                sb.Append(" = '");
                                sb.Append(value);
                                sb.Append("'");
                            }

                        }
                    }
                }
                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[MHD].[CreateDate]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberHiringDetails> response = new PagedResponse<MemberHiringDetails>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberHiringDetails>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<MemberOfferJoinDetails > IMemberHiringDetailsDataAccess.GetPagedOfferJoined(PagedRequest request)
        {
            const string SP = "dbo.MemberHiringDetails_GetPagedOfferAndJoineddetails";
            string whereClause = getWhereClause(request,true ).ToString ();
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[MHD].[OfferedDate]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberOfferJoinDetails> response = new PagedResponse<MemberOfferJoinDetails>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new MemberHiringDetailsBuilder()).BuildOfferJoinedEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        StringBuilder getWhereClause(PagedRequest request, bool isOfferJoined)
        {
            StringBuilder sb = new System.Text.StringBuilder();
            if (request.Conditions.Count > 0)
            {
               

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                      
                        if (!StringHelper.IsBlank(column))
                        {
                          
                            if (StringHelper.IsEqual(column, "OfferDate"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([MHD].[OfferedDate]");
                                sb.Append(value);
                                sb.Append(" ) ");
                            }
                            if (StringHelper.IsEqual(column, "DOJDate"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([MHD].[JoiningDate]");
                                sb.Append(value);
                                sb.Append(" ) ");
                            }
                            if (StringHelper.IsEqual(column, "EDOJDate"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([MHD].[JoiningDate]");
                                sb.Append(value);
                                sb.Append(" ) ");
                            }

                            if (StringHelper.IsEqual(column, "OfferDeclineDate"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([MOR].[RejectedDate]");
                                sb.Append(value);
                                sb.Append(" ) ");
                            }
                            if (StringHelper.IsEqual(column, "CreateDate"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([MHD].[CreateDate] ");
                                sb.Append(value);
                                if (isOfferJoined)
                                {
                                    sb.Append(" OR [MOR].[CreateDate] ");
                                    sb.Append(value);
                                    sb.Append(" OR [MJD].[CreateDate]  ");
                                    sb.Append(value);
                                }
                                sb.Append(" ) ");
                            }
                            if (StringHelper.IsEqual(column, "PS"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([MHD].[MemberId] in ");
                                sb.Append( "(select MemberID from Memberjoiningdetails where PSID='" +value + "') or ");
                                if (isOfferJoined)
                                {
                                    sb.Append("[MOR].[MemberId] in ");

                                    sb.Append("(select MemberID from Memberjoiningdetails where PSID='" + value + "') or  ");
                                    sb.Append("[MJD].[MemberId] in ");
                                    sb.Append("(select MemberID from Memberjoiningdetails where PSID='" + value + "')");
                                }
                                sb.Append(")");
                            }
                            if (StringHelper.IsEqual(column, "CreatedBy"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([MHD].[CreatorId] = ");
                                sb.Append(value);
                                if (isOfferJoined)
                                {
                                    sb.Append(" OR [MOR].[CreatorID] = ");
                                    sb.Append(value);
                                    sb.Append(" OR [MJD].[CreatorID] = ");
                                    sb.Append(value);
                                }
                                sb.Append(")");
                            }
                            if (StringHelper.IsEqual(column, "Requisition"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([MHD].[JobPostingId] = ");
                                sb.Append(value);
                                if (isOfferJoined)
                                {
                                    sb.Append(" OR [MOR].[JobPostingId] = ");
                                    sb.Append(value);
                                    sb.Append(" OR [MJD].[JobPostingId] = ");
                                    sb.Append(value);
                                }
                                sb.Append(")");
                            }
                           
                           
                        }
                    }
                }
              
            }
            return sb;
        }

        PagedResponse<MemberHiringDetails>  IMemberHiringDetailsDataAccess.GetPagedEmbeddedSalarydetails(PagedRequest request)
        {
            const string SP = "dbo.MemberHiringDetails_GetPagedEmbeddedSalarydetails";
            string whereClause = getWhereClause(request, false ).ToString ();
            
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[MHD].[CreateDate]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberHiringDetails> response = new PagedResponse<MemberHiringDetails>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberHiringDetails>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<MemberHiringDetails> IMemberHiringDetailsDataAccess.GetPagedMechSalarydetails(PagedRequest request)
        {
            const string SP = "dbo.MemberHiringDetails_GetPagedMechSalarydetails";
            string whereClause = getWhereClause(request,false ).ToString ();
           
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[MHD].[CreateDate]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberHiringDetails> response = new PagedResponse<MemberHiringDetails>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberHiringDetails>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        PagedResponse<MemberOfferJoinDetails> IMemberHiringDetailsDataAccess.GetPagedOfferJoinedCommon(PagedRequest request)
        {
            const string SP = "dbo.MemberHiringDetails_GetPagedOfferAndJoineddetailsForCommon";
            string whereClause = getWhereClauseForCommonOfferJoinedReport(request).ToString();
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[MHD].[OfferedDate]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberOfferJoinDetails> response = new PagedResponse<MemberOfferJoinDetails>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new MemberHiringDetailsBuilder()).BuildOfferJoinedForCommon(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        StringBuilder getWhereClauseForCommon(PagedRequest request)
        {
            StringBuilder sb = new System.Text.StringBuilder();
            if (request.Conditions.Count > 0)
            {


                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {

                        if (!StringHelper.IsBlank(column))
                        {

                            if (StringHelper.IsEqual(column, "OfferDate"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([MHD].[OfferedDate]");
                                sb.Append(value);
                                sb.Append(" ) ");
                            }
                            if (StringHelper.IsEqual(column, "DOJDate"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("(([MJD].[JoiningDate] is null and [MHD].[JoiningDate]");
                                sb.Append(value);
                                //sb.Append(" OR [MHD].[JoiningDate] is NULL)");
                                sb.Append(" ) OR (");
                                sb.Append("[MJD].[JoiningDate] is not null and [MJD].[JoiningDate]");
                                sb.Append(value);
                                //sb.Append(" OR [MJD].[JoiningDate] is NULL))");
                                sb.Append(" )) ");
                            }
                            if (StringHelper.IsEqual(column, "CreateDate"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([MHD].[CreateDate] ");
                                sb.Append(value);
                                sb.Append(" OR [MOR].[CreateDate] ");
                                sb.Append(value);
                                sb.Append(" OR [MJD].[CreateDate]  ");
                                sb.Append(value);
                                sb.Append(" ) ");
                            }
                            if (StringHelper.IsEqual(column, "CreatedBy"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([MHD].[CreatorId] = ");
                                sb.Append(value);
                                sb.Append(" OR [MOR].[CreatorID] = ");
                                sb.Append(value);
                                sb.Append(" OR [MJD].[CreatorID] = ");
                                sb.Append(value);
                                sb.Append(")");
                            }
                            if (StringHelper.IsEqual(column, "Requisition"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([MHD].[JobPostingId] = ");
                                sb.Append(value);
                                sb.Append(" OR [MOR].[JobPostingId] = ");
                                sb.Append(value);
                                sb.Append(" OR [MJD].[JobPostingId] = ");
                                sb.Append(value);
                                sb.Append(")");
                            }


                        }
                    }
                }

            }
            return sb;
        }

        StringBuilder getWhereClauseForCommonOfferJoinedReport(PagedRequest request)
        {
            StringBuilder sb = new System.Text.StringBuilder();
            if (request.Conditions.Count > 0)
            {


                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {

                        if (!StringHelper.IsBlank(column))
                        {

                            if (StringHelper.IsEqual(column, "OfferDate"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([OfferedDate]");
                                sb.Append(value);
                                sb.Append(" ) ");
                            }
                            if (StringHelper.IsEqual(column, "DOJDate"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("(([JoiningDate] is null and [JoiningDate]");
                                sb.Append(value);
                                //sb.Append(" OR [MHD].[JoiningDate] is NULL)");
                                sb.Append(" ) OR (");
                                sb.Append("[JoiningDate] is not null and [JoiningDate]");
                                sb.Append(value);
                                //sb.Append(" OR [MJD].[JoiningDate] is NULL))");
                                sb.Append(" )) ");
                            }
                            if (StringHelper.IsEqual(column, "CreateDate"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([CreateDate] ");
                                sb.Append(value);
                               
                                sb.Append(" ) ");
                            }
                            if (StringHelper.IsEqual(column, "CreatedBy"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([CreatorId] = ");
                                sb.Append(value);
                               
                                sb.Append(")");
                            }
                            if (StringHelper.IsEqual(column, "Requisition"))
                            {
                                if (sb.ToString() != string.Empty) sb.Append(" And");
                                sb.Append("([JobPostingId] = ");
                                sb.Append(value);
                               
                                sb.Append(")");
                            }


                        }
                    }
                }

            }
            return sb;
        }

        MemberHiringDetails IMemberHiringDetailsDataAccess.GetSalaryComponentsByJobpostingIdAndMemberId(int jobpostingid, int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            if (jobpostingid < 1)
            {
                throw new ArgumentNullException("jobpostingid");
            }
            const string SP = "dbo.MemberHiringDetails_GetSalaryComponentsByJobpostingIdAndMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobpostingid);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                MemberHiringDetails mem = new MemberHiringDetails();
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        mem.CandidateName = reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                        mem.Designation = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
                        mem.Location = reader.IsDBNull(2) ? string.Empty : reader.GetString(2);
                        mem.BasicEmbedded = reader.IsDBNull(3) ? 0 : reader.GetDecimal(3);
                        mem.HRAEmbedded = reader.IsDBNull(4) ? 0 : reader.GetDecimal(4);
                        mem.Conveyance = reader.IsDBNull(5) ? 0 : reader.GetDecimal(5);
                        mem.Medical = reader.IsDBNull(6) ? 0 : reader.GetDecimal(6);
                        mem.RoleAllowance = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                        mem.SiteAllowance = reader.IsDBNull(8) ? 0 : reader.GetDecimal(8);
                        mem.RetentionBonus = reader.IsDBNull(9) ? 0 : reader.GetDecimal(9);
                        mem.RelocationAllowanceForCom = reader.IsDBNull(10) ? 0 : reader.GetDecimal(10);
                        mem.Reimbursement = reader.IsDBNull(11) ? 0 : reader.GetDecimal(11);
                        mem.GrossSalary = reader.IsDBNull(12) ? 0 : reader.GetDecimal(12);
                        mem.PFEmbedded = reader.IsDBNull(13) ? 0 : reader.GetDecimal(13);
                        mem.ESIC = reader.IsDBNull(14) ? 0 : reader.GetDecimal(14);
                        mem.GratuityEmbedded = reader.IsDBNull(15) ? 0 : reader.GetDecimal(15);
                        mem.Bonus = reader.IsDBNull(16) ? 0 : reader.GetDecimal(16);
                        mem.SpecialAllowance = reader.IsDBNull(17) ? 0 : reader.GetDecimal(17);
                        mem.SalesIncentive = reader.IsDBNull(18) ? 0 : reader.GetDecimal(18);
                        mem.PerformanceLinkedIncentive = reader.IsDBNull(19) ? 0 : reader.GetDecimal(19);
                        mem.ManagementBand = reader.IsDBNull(20) ? string.Empty : reader.GetString(20);
                        mem.Band = reader.IsDBNull(21) ? string.Empty : reader.GetString(21);
                        mem.BU = reader.IsDBNull(22) ? string.Empty : reader.GetString(22);
                        mem.OfferedSalary = reader.IsDBNull(23) ? string.Empty : reader.GetString(23);
                        mem.FlexiPay1 = reader.IsDBNull(24) ? 0 : reader.GetDecimal(24);
                        mem.LTAEmbedded = reader.IsDBNull(25) ? 0 : reader.GetDecimal(25);
                        mem.OfferedPosition = reader.IsDBNull(26) ? string.Empty : reader.GetString(26);
                       
                    }
                    return mem;
                }
            }
        }

        MemberHiringDetails IMemberHiringDetailsDataAccess.GetOfferDetailsByJobpostingIdAndMemberId(int jobpostingid, int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            if (jobpostingid < 1)
            {
                throw new ArgumentNullException("jobpostingid");
            }
            const string SP = "dbo.MemberHiringDetails_GetOfferDetailsByJobpostingIdAndMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobpostingid);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                MemberHiringDetails mem = new MemberHiringDetails();
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        mem.CandidateName = reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                        mem.JobTitle = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
                        mem.Designation = reader.IsDBNull(2) ? string.Empty : reader.GetString(2);
                        mem.Location = reader.IsDBNull(3) ? string.Empty : reader.GetString(3);
                        mem.ManagementBand = reader.IsDBNull(4) ? string.Empty : reader.GetString(4);
                        mem.Band = reader.IsDBNull(5) ? string.Empty : reader.GetString(5);
                        mem.JoiningDate = reader.IsDBNull(6) ? DateTime.Today.AddDays(7) : reader.GetDateTime(6);
                        mem.MemberId = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
                        mem.Name = reader.IsDBNull(8) ? string.Empty : reader.GetString(8);
                        mem.Address = reader.IsDBNull(9) ? string.Empty : reader.GetString(9);
                        mem.Address2 = reader.IsDBNull(10) ? string.Empty : reader.GetString(10);
                        mem.City = reader.IsDBNull(11) ? string.Empty : reader.GetString(11);
                        mem.PostCode = reader.IsDBNull(12) ? Convert.ToInt32("0") : reader.GetInt32(12);
                        mem.OfferedDate = reader.IsDBNull(13) ? DateTime.Today.AddDays(7) : reader.GetDateTime(13);
                    }

                    return mem;
                }
            }
        }
        
        IList<MemberHiringDetails> IMemberHiringDetailsDataAccess.GetAllManagementBand() 
        {
            const string SP = "dbo.MemberHiringDetails_GetAllManagementBand";
            IList<MemberHiringDetails> details = new List<MemberHiringDetails>();
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                using (IDataReader reader = Database.ExecuteReader(cmd)) 
                {
                    while (reader.Read()) 
                    {
                        details.Add(new MemberHiringDetails { ManagementBandId = reader.GetInt32(0), ManagementBand = reader.GetString(1) });
                    }
                    
                }
            }
            return details;
        }

        IList<MemberHiringDetails> IMemberHiringDetailsDataAccess.GetAllLocation()
        {
            const string SP = "dbo.MemberHiringDetails_GetAllLocation";
            IList<MemberHiringDetails> details = new List<MemberHiringDetails>();
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        details.Add(new MemberHiringDetails {CityId = reader.GetInt32(0), City = reader.GetString(1) });
                    }

                }
            }
            return details;
        }

        IList<MemberHiringDetails> IMemberHiringDetailsDataAccess.GetAllManagementBandByManagementBandId(int ManagementBandId)
        {
            const string SP = "dbo.MemberHiringDetails_GetAllBandByManagementBandId";
            IList<MemberHiringDetails> details = new List<MemberHiringDetails>();
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ManagementBandId", DbType.Int32, ManagementBandId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        details.Add(new MemberHiringDetails { BandId = reader.GetInt32(0), Band = reader.GetString(1) });
                    }

                }
            }
            return details;
        }


        IList<MemberHiringDetails> IMemberHiringDetailsDataAccess.GetAllBand()
        {
            const string SP = "dbo.MemberHiringDetails_GetAllBand";
            IList<MemberHiringDetails> details = new List<MemberHiringDetails>();
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        details.Add(new MemberHiringDetails { BandId = reader.GetInt32(0), Band = reader.GetString(1) });
                    }

                }
            }
            return details;
        }
        #endregion
    }
}