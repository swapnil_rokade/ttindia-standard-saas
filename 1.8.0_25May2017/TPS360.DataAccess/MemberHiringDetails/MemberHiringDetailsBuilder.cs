﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberHiringDetailsBuilder.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberHiringDetailsBuilder : IEntityBuilder<MemberHiringDetails>
    {
        IList<MemberHiringDetails> IEntityBuilder<MemberHiringDetails>.BuildEntities(IDataReader reader)
        {
            List<MemberHiringDetails> MemberHiringDetailss = new List<MemberHiringDetails>();

            while (reader.Read())
            {
                MemberHiringDetailss.Add(((IEntityBuilder<MemberHiringDetails>)this).BuildEntity(reader));
            }

            return (MemberHiringDetailss.Count > 0) ? MemberHiringDetailss : null;
        }
        public IList<MemberOfferJoinDetails> BuildOfferJoinedForCommon(IDataReader reader) 
        {
            List<MemberOfferJoinDetails> MemberOfferDetails = new List<MemberOfferJoinDetails>();
            while (reader.Read()) 
            {
                MemberOfferDetails.Add(BuildOfferJoinEntityForCommon(reader));
            }
            return (MemberOfferDetails.Count > 0) ? MemberOfferDetails : null;
        }

        public IList<MemberOfferJoinDetails> BuildOfferJoinedEntities(IDataReader reader)
        {
            List<MemberOfferJoinDetails> MemberHiringDetailss = new List<MemberOfferJoinDetails>();

            while (reader.Read())
            {
                MemberHiringDetailss.Add(BuildOfferJoinEntity(reader));
            }

            return (MemberHiringDetailss.Count > 0) ? MemberHiringDetailss : null;
        }

        MemberOfferJoinDetails BuildOfferJoinEntityForCommon(IDataReader reader) 
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERID = 1;
            const int FLD_JOBPOSTINGID = 2;
            const int FLD_OFFEREDPOSITION = 3;
            const int FLD_OFFEREDSALARY = 4;
            const int FLD_OFFEREDSALARYPAYCYCLE = 5;
            const int FLD_OFFEREDSALARYCURRENCY = 6;
            const int FLD_OFFERACCEPTED = 7;
            const int FLD_JOININGDATE = 8;
            const int FLD_COMMISSIONPAYRATE = 9;
            const int FLD_COMMISSIONCURRENCY = 10;
            const int FLD_ISREMOVED = 11;
            const int FLD_CREATORID = 12;
            const int FLD_UPDATORID = 13;
            const int FLD_CREATEDATE = 14;
            const int FLD_UPDATEDATE = 15;
            const int FLD_OFFEREDDATE = 16;
            const int FLD_CURRENTCTC = 17;
            const int FLD_CURRENTCTCLOOKUPID = 18;
            const int FLD_MINEXPECTEDCTC = 19;
            const int FLD_MAXEXPECTEDCTC = 20;
            const int FLD_EXPECTEDCTCLOOKUPID = 21;
            const int FLD_EXPERIENCE = 22;
            const int FLD_SOURCE = 23;
            const int FLD_SOURCEDESCRIPTION = 24;
            const int FLD_BASIC = 25;
            const int FLD_HRA = 26;
            const int FLD_CONVEYANCE = 27;
            const int FLD_SPECIAL = 28;
            const int FLD_MEDICAL = 29;
            //const int FLD_EDUCATIONALLOWANCE = 29;
            //const int FLD_LTA = 30;
            const int FLD_ROLE = 30;
            const int FLD_SITE = 31;
            const int FLD_PERFORMANCE = 32;
            const int FLD_RETENTION = 33;
            const int FLD_RELOCATION = 34;
            const int FLD_REIMBURSEMENT = 35;
            const int FLD_PF = 36;
            const int FLD_ESIC = 37;
            const int FLD_GRATUITY = 38;
            //const int FLD_MAXIMUMANNUALINCENTIVES = 33;
            const int FLD_BONUS = 39;
            const int FLD_SALESINCENTIVE=40;
            //const int FLD_CARALLOWANCE = 35;
            const int FLD_GRANDTOTAL = 41;
            const int FLD_BILLABLESALARY = 42;
            const int FLD_BILLINGRATE = 43;
            const int FLD_REVENUE = 44;
            const int FLD_OFFERLOCATION = 45;
            const int FLD_JOBPOSTINGCODE = 46;
            const int FLD_JOBTITLE = 47;
            const int FLD_APPLICANTNAME = 48;
            const int FLD_ACTUALJOININGDATE = 49;
            const int FLD_SKILLS = 50;
            const int FLD_EDUCATIONALQUALIFICATION = 51;
            const int FLD_OFFEREDCTC = 52;
            const int FLD_SOURCENAME = 53;
            const int FLD_SOURCEDESCRIPTIONNAME = 54;
            const int FLD_COMPANYID = 55;
            const int FLD_COMPANYNAME = 56;
            const int FLD_ACTIVERECRUITERID = 57;
            const int FLD_ACTIVERECRUITERNAME = 58;
            const int FLD_OFFERSTATUS = 59;
            const int FLD_CURRENTCOMPANY = 60;
            const int FLD_MANAGEMENTBAND = 61;
            const int FLD_BAND = 62;

            MemberOfferJoinDetails commonOfferJoin = new MemberOfferJoinDetails();

            commonOfferJoin.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            commonOfferJoin.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            commonOfferJoin.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            commonOfferJoin.OfferedPosition = reader.IsDBNull(FLD_OFFEREDPOSITION) ? string.Empty : reader.GetString(FLD_OFFEREDPOSITION);
            commonOfferJoin.OfferedSalary = reader.IsDBNull(FLD_OFFEREDSALARY) ? string.Empty : reader.GetString(FLD_OFFEREDSALARY);
            commonOfferJoin.OfferedSalaryPayCycle = reader.IsDBNull(FLD_OFFEREDSALARYPAYCYCLE) ? 0 : reader.GetInt32(FLD_OFFEREDSALARYPAYCYCLE);
            commonOfferJoin.OfferedSalaryCurrency = reader.IsDBNull(FLD_OFFEREDSALARYCURRENCY) ? 0 : reader.GetInt32(FLD_OFFEREDSALARYCURRENCY);
            commonOfferJoin.OfferAccepted = reader.IsDBNull(FLD_OFFERACCEPTED) ? true : reader.GetBoolean(FLD_OFFERACCEPTED);
            commonOfferJoin.ActualDateOfJoining = reader.IsDBNull(FLD_ACTUALJOININGDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ACTUALJOININGDATE);
            commonOfferJoin.CommissionPayRate = reader.IsDBNull(FLD_COMMISSIONPAYRATE) ? string.Empty : reader.GetString(FLD_COMMISSIONPAYRATE);
            commonOfferJoin.CommissionCurrency = reader.IsDBNull(FLD_COMMISSIONCURRENCY) ? 0 : reader.GetInt32(FLD_COMMISSIONCURRENCY);
            commonOfferJoin.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            commonOfferJoin.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            commonOfferJoin.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            commonOfferJoin.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            commonOfferJoin.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            commonOfferJoin.OfferedDate = reader.IsDBNull(FLD_OFFEREDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_OFFEREDDATE);
            commonOfferJoin.CurrentCTC = reader.IsDBNull(FLD_CURRENTCTC) ? string.Empty : reader.GetString(FLD_CURRENTCTC);
            commonOfferJoin.CurrentCTCLookupId = reader.IsDBNull(FLD_CURRENTCTCLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTCTCLOOKUPID);
            commonOfferJoin.MinExpectedCTC = reader.IsDBNull(FLD_MINEXPECTEDCTC) ? string.Empty : reader.GetString(FLD_MINEXPECTEDCTC);
            commonOfferJoin.MaxExpectedCTC = reader.IsDBNull(FLD_MAXEXPECTEDCTC) ? string.Empty : reader.GetString(FLD_MAXEXPECTEDCTC);
            commonOfferJoin.ExpectedCTCLookupId = reader.IsDBNull(FLD_EXPECTEDCTCLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDCTCLOOKUPID);
            commonOfferJoin.Experience = reader.IsDBNull(FLD_EXPERIENCE) ? string.Empty : reader.GetString(FLD_EXPERIENCE);
            commonOfferJoin.Source = reader.IsDBNull(FLD_SOURCE) ? 0 : reader.GetInt32(FLD_SOURCE);
            commonOfferJoin.SourceDescription = reader.IsDBNull(FLD_SOURCEDESCRIPTION) ? string.Empty : reader.GetString(FLD_SOURCEDESCRIPTION);
            commonOfferJoin.BasicEmbedded = reader.IsDBNull(FLD_BASIC) ? 0 : reader.GetDecimal(FLD_BASIC);
            commonOfferJoin.HRAEmbedded = reader.IsDBNull(FLD_HRA) ? 0 : reader.GetDecimal(FLD_HRA);
            commonOfferJoin.Conveyance = reader.IsDBNull(FLD_CONVEYANCE) ? 0 : reader.GetDecimal(FLD_CONVEYANCE);
            commonOfferJoin.SpecialAllowance = reader.IsDBNull(FLD_SPECIAL) ? 0 : reader.GetDecimal(FLD_SPECIAL);
            commonOfferJoin.Medical = reader.IsDBNull(FLD_MEDICAL) ? 0 : reader.GetDecimal(FLD_MEDICAL);
            //commonOfferJoin.EducationalAllowanceEmbedded = reader.IsDBNull(FLD_EDUCATIONALLOWANCE) ? 0 : reader.GetDecimal(FLD_EDUCATIONALLOWANCE);
            //commonOfferJoin.LTAEmbedded = reader.IsDBNull(FLD_LTA) ? 0 : reader.GetDecimal(FLD_LTA);
            commonOfferJoin.RoleAllowance = reader.IsDBNull(FLD_ROLE) ? 0 : reader.GetDecimal(FLD_ROLE);
            commonOfferJoin.SiteAllowance = reader.IsDBNull(FLD_SITE) ? 0 : reader.GetDecimal(FLD_SITE);
            commonOfferJoin.PerformanceLinkedIncentive = reader.IsDBNull(FLD_PERFORMANCE) ? 0 : reader.GetDecimal(FLD_PERFORMANCE);
            commonOfferJoin.RetentionBonus = reader.IsDBNull(FLD_RETENTION) ? 0 : reader.GetDecimal(FLD_RETENTION);
            commonOfferJoin.RelocationAllowanceForCom = reader.IsDBNull(FLD_RELOCATION) ? 0 : reader.GetDecimal(FLD_RELOCATION);
            commonOfferJoin.Reimbursement = reader.IsDBNull(FLD_REIMBURSEMENT) ? 0 : reader.GetDecimal(FLD_REIMBURSEMENT);
            commonOfferJoin.PFEmbedded = reader.IsDBNull(FLD_PF) ? 0 : reader.GetDecimal(FLD_PF);
            commonOfferJoin.ESIC = reader.IsDBNull(FLD_ESIC) ? 0 : reader.GetDecimal(FLD_ESIC);
            commonOfferJoin.GratuityEmbedded = reader.IsDBNull(FLD_GRATUITY) ? 0 : reader.GetDecimal(FLD_GRATUITY);
            //commonOfferJoin.MaximumAnnualIncentive = reader.IsDBNull(FLD_MAXIMUMANNUALINCENTIVES) ? 0 : reader.GetDecimal(FLD_MAXIMUMANNUALINCENTIVES);
            commonOfferJoin.Bonus = reader.IsDBNull(FLD_BONUS) ? 0 : reader.GetDecimal(FLD_BONUS);
            commonOfferJoin.SalesIncentive = reader.IsDBNull(FLD_SALESINCENTIVE) ? 0 : reader.GetDecimal(FLD_SALESINCENTIVE);
            //commonOfferJoin.CarAllowance = reader.IsDBNull(FLD_CARALLOWANCE) ? 0 : reader.GetDecimal(FLD_CARALLOWANCE);
            commonOfferJoin.GrandTotal = reader.IsDBNull(FLD_GRANDTOTAL) ? 0 : reader.GetDecimal(FLD_GRANDTOTAL);
            commonOfferJoin.BillableSalary = (reader.IsDBNull(FLD_BILLABLESALARY) || reader.GetString(FLD_BILLABLESALARY) == string.Empty) ? string.Empty : reader.GetString(FLD_BILLABLESALARY);
            commonOfferJoin.BillingRate = (reader.IsDBNull(FLD_BILLINGRATE) || reader.GetString(FLD_BILLINGRATE) == string.Empty) ? string.Empty : reader.GetString(FLD_BILLINGRATE);
            commonOfferJoin.Revenue = (reader.IsDBNull(FLD_REVENUE) || reader.GetString(FLD_REVENUE) == string.Empty) ? string.Empty : reader.GetString(FLD_REVENUE);
            commonOfferJoin.OfferLocation = (reader.IsDBNull(FLD_OFFERLOCATION)  || reader.GetString(FLD_OFFERLOCATION)==string.Empty) ? string.Empty : reader.GetString(FLD_OFFERLOCATION);
            commonOfferJoin.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
            commonOfferJoin.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            commonOfferJoin.ApplicantName = reader.IsDBNull(FLD_APPLICANTNAME) ? string.Empty : reader.GetString(FLD_APPLICANTNAME);
            commonOfferJoin.JoiningDate = reader.IsDBNull(FLD_JOININGDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_JOININGDATE);
            commonOfferJoin.Skills = reader.IsDBNull(FLD_SKILLS) ? string.Empty : reader.GetString(FLD_SKILLS);
            commonOfferJoin.EducationalQualification = reader.IsDBNull(FLD_EDUCATIONALQUALIFICATION) ? string.Empty : reader.GetString(FLD_EDUCATIONALQUALIFICATION);
            commonOfferJoin.OfferedCTC = reader.IsDBNull(FLD_OFFEREDCTC) ? string.Empty : reader.GetString(FLD_OFFEREDCTC);
            commonOfferJoin.SOURCENAME = reader.IsDBNull(FLD_SOURCENAME) ? string.Empty : reader.GetString(FLD_SOURCENAME);
            commonOfferJoin.SOURCEDESCRIPTIONNAME = reader.IsDBNull(FLD_SOURCEDESCRIPTIONNAME) ? string.Empty : reader.GetString(FLD_SOURCEDESCRIPTIONNAME);
            commonOfferJoin.BUID = reader.IsDBNull(FLD_COMPANYID) ? 0 : reader.GetInt32(FLD_COMPANYID);
            commonOfferJoin.BUNAME = reader.IsDBNull(FLD_COMPANYNAME) ? string.Empty : reader.GetString(FLD_COMPANYNAME);
            commonOfferJoin.ActiveRecruiterID = reader.IsDBNull(FLD_ACTIVERECRUITERID) ? 0 : reader.GetInt32(FLD_ACTIVERECRUITERID);
            commonOfferJoin.ActiveRecruiterName = reader.IsDBNull(FLD_ACTIVERECRUITERNAME) ? string.Empty : reader.GetString(FLD_ACTIVERECRUITERNAME);
            commonOfferJoin.OfferStatus = reader.IsDBNull(FLD_OFFERSTATUS) ? string.Empty : reader.GetString(FLD_OFFERSTATUS);
            commonOfferJoin.CurrentCompany = reader.IsDBNull(FLD_CURRENTCOMPANY) ? string.Empty : reader.GetString(FLD_CURRENTCOMPANY);
            commonOfferJoin.ManagementBand = reader.IsDBNull(FLD_MANAGEMENTBAND) ? string.Empty : reader.GetString(FLD_MANAGEMENTBAND);
            commonOfferJoin.Band= reader.IsDBNull(FLD_BAND) ? string.Empty : reader.GetString(FLD_BAND);
            return commonOfferJoin;

            



        }
        MemberOfferJoinDetails BuildOfferJoinEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERID = 1;
            const int FLD_JOBPOSTINGID = 2;
            const int FLD_OFFEREDPOSITION = 3;
            const int FLD_OFFEREDSALARY = 4;
            const int FLD_OFFEREDSALARYPAYCYCLE = 5;
            const int FLD_OFFEREDSALARYCURRENCY = 6;
            const int FLD_OFFERACCEPTED = 7;
            const int FLD_JOININGDATE = 8;
            const int FLD_COMMISSIONPAYRATE = 9;
            const int FLD_COMMISSIONCURRENCY = 10;
            const int FLD_ISREMOVED = 11;
            const int FLD_CREATORID = 12;
            const int FLD_UPDATORID = 13;
            const int FLD_CREATEDATE = 14;
            const int FLD_UPDATEDATE = 15;
            const int FLD_OFFEREDDATE = 16;
            const int FLD_CURRENTCTC = 17;
            const int FLD_CURRENTCTCLOOKUPID = 18;
            const int FLD_MINEXPECTEDCTC = 19;
            const int FLD_MAXEXPECTEDCTC = 20;
            const int FLD_EXPECTEDCTCLOOKUPID = 21;
            const int FLD_EXPERIENCE = 22;
            const int FLD_SOURCE = 23;
            const int FLD_SOURCEDESCRIPTION = 24;
            const int FLD_FITMENTPERCENTILE = 25;
            const int FLD_JOININGBONUS = 26;
            const int FLD_RELOCATIONALLOWANCE = 27;
            const int FLD_SPLLUMPSUM = 28;
            const int FLD_NOTICEPRDPAYOUT = 29;
            const int FLD_PLACEMENTFEES = 30;
            const int FLD_BVSTATUS = 31;
            const int FLD_JOBPOSTINGCODE = 32;
            const int FLD_JOBTITLE = 33;
            const int FLD_APPLICANTNAME = 34;
            const int FLD_OFFERCATEGORYNAME = 35;
            const int FLD_REJECTEDDATE = 36;
            const int FLD_REASONTEXT = 37;
            const int FLD_ACTUALJOININGDATE = 38;
            const int FLD_PSID = 39;
            const int FLD_CURRENTCOMPANY = 40;
            const int FLD_WORLKOCATION = 41;
            const int FLD_SKILLS = 42;
            const int FLD_EDUCATIONALQUALIFICATION=43;
            const int FLD_BUID = 44;
            const int FLD_BUNAME = 45;
            const int FLD_OFFEREDCTC = 46;
            const int FLD_GRADE = 47;
            const int FLD_BVSTATUSTEXT = 48;
            const int FLD_FITMENTPERCENTILETEXT = 49;
            const int FLD_SOURCENAME = 50;
            const int FLD_SOURCEDESCRIPTIONNAME = 51;

            const int FLD_ACTIVERECRUITERID = 52;
            const int FLD_ACTIVERECRUITERNAME = 53;

            const int FLD_OFFERSTATUS = 54;
            const int FLD_WORKLOCATION = 55;

            MemberOfferJoinDetails offerjoined = new MemberOfferJoinDetails();
            offerjoined.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            offerjoined.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            offerjoined.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            offerjoined.OfferedPosition = reader.IsDBNull(FLD_OFFEREDPOSITION) ? string.Empty : reader.GetString(FLD_OFFEREDPOSITION);
            offerjoined.OfferedSalary = reader.IsDBNull(FLD_OFFEREDSALARY) ? string.Empty : reader.GetString(FLD_OFFEREDSALARY);
            offerjoined.OfferedSalaryPayCycle = reader.IsDBNull(FLD_OFFEREDSALARYPAYCYCLE) ? 0 : reader.GetInt32(FLD_OFFEREDSALARYPAYCYCLE);
            offerjoined.OfferedSalaryCurrency = reader.IsDBNull(FLD_OFFEREDSALARYCURRENCY) ? 0 : reader.GetInt32(FLD_OFFEREDSALARYCURRENCY);
            offerjoined.OfferAccepted = reader.IsDBNull(FLD_OFFERACCEPTED) ? true : reader.GetBoolean(FLD_OFFERACCEPTED);
            offerjoined.JoiningDate = reader.IsDBNull(FLD_JOININGDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_JOININGDATE);
            offerjoined.CommissionPayRate = reader.IsDBNull(FLD_COMMISSIONPAYRATE) ? string.Empty : reader.GetString(FLD_COMMISSIONPAYRATE);
            offerjoined.CommissionCurrency = reader.IsDBNull(FLD_COMMISSIONCURRENCY) ? 0 : reader.GetInt32(FLD_COMMISSIONCURRENCY);
            offerjoined.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            offerjoined.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            offerjoined.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            offerjoined.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            offerjoined.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            offerjoined.OfferedDate = reader.IsDBNull(FLD_OFFEREDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_OFFEREDDATE);
            


            offerjoined.CurrentCTC = reader.IsDBNull(FLD_CURRENTCTC) ? string.Empty : reader.GetString(FLD_CURRENTCTC);
            offerjoined.CurrentCTCLookupId = reader.IsDBNull(FLD_CURRENTCTCLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTCTCLOOKUPID);
            offerjoined.MinExpectedCTC = reader.IsDBNull(FLD_MINEXPECTEDCTC) ? string.Empty : reader.GetString(FLD_MINEXPECTEDCTC);
            offerjoined.MaxExpectedCTC = reader.IsDBNull(FLD_MAXEXPECTEDCTC) ? string.Empty : reader.GetString(FLD_MAXEXPECTEDCTC);
            offerjoined.ExpectedCTCLookupId = reader.IsDBNull(FLD_EXPECTEDCTCLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDCTCLOOKUPID);
            offerjoined.Experience = reader.IsDBNull(FLD_EXPERIENCE) ? string.Empty : reader.GetString(FLD_EXPERIENCE);
            offerjoined.Source = reader.IsDBNull(FLD_SOURCE) ? 0 : reader.GetInt32(FLD_SOURCE);
            offerjoined.SourceDescription = reader.IsDBNull(FLD_SOURCEDESCRIPTION) ? string.Empty : reader.GetString(FLD_SOURCEDESCRIPTION);
            offerjoined.FitmentPercentile = reader.IsDBNull(FLD_FITMENTPERCENTILE) ? 0 : reader.GetInt32(FLD_FITMENTPERCENTILE);
            offerjoined.JoiningBonus = reader.IsDBNull(FLD_JOININGBONUS) ? string.Empty : reader.GetString(FLD_JOININGBONUS);
            offerjoined.RelocationAllowance = reader.IsDBNull(FLD_RELOCATIONALLOWANCE) ? string.Empty : reader.GetString(FLD_RELOCATIONALLOWANCE);
            offerjoined.SpecialLumpSum = reader.IsDBNull(FLD_SPLLUMPSUM) ? string.Empty : reader.GetString(FLD_SPLLUMPSUM);
            offerjoined.NoticePeriodPayout = reader.IsDBNull(FLD_NOTICEPRDPAYOUT) ? string.Empty : reader.GetString(FLD_NOTICEPRDPAYOUT);
            offerjoined.PlacementFees = reader.IsDBNull(FLD_PLACEMENTFEES) ? string.Empty : reader.GetString(FLD_PLACEMENTFEES);
            offerjoined.BVStatus = reader.IsDBNull(FLD_BVSTATUS) ? 0 : reader.GetInt32(FLD_BVSTATUS);


            offerjoined.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string .Empty  : reader.GetString(FLD_JOBPOSTINGCODE);
            offerjoined.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            offerjoined.ApplicantName = reader.IsDBNull(FLD_APPLICANTNAME) ? string.Empty : reader.GetString(FLD_APPLICANTNAME);
            offerjoined.OfferCategoryName = reader.IsDBNull(FLD_OFFERCATEGORYNAME) ? string.Empty : reader.GetString(FLD_OFFERCATEGORYNAME);
            offerjoined.OfferDeclinedDate = reader.IsDBNull(FLD_REJECTEDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_REJECTEDDATE);
            offerjoined.ReasonForRejection = reader.IsDBNull(FLD_REASONTEXT) ? string.Empty : reader.GetString(FLD_REASONTEXT);
            offerjoined.ActualDateOfJoining = reader.IsDBNull(FLD_ACTUALJOININGDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ACTUALJOININGDATE);
            offerjoined.PSNo = reader.IsDBNull(FLD_PSID) ? string.Empty : reader.GetString(FLD_PSID);
            offerjoined.CurrentCompany = reader.IsDBNull(FLD_CURRENTCOMPANY) ? string.Empty : reader.GetString(FLD_CURRENTCOMPANY);
            offerjoined.WorkLocation = reader.IsDBNull(FLD_WORLKOCATION) ? string.Empty : reader.GetString(FLD_WORLKOCATION);
            offerjoined.Skills = reader.IsDBNull(FLD_SKILLS) ? string.Empty : reader.GetString(FLD_SKILLS);
            offerjoined.EducationalQualification = reader.IsDBNull(FLD_EDUCATIONALQUALIFICATION) ? string.Empty : reader.GetString(FLD_EDUCATIONALQUALIFICATION);
            offerjoined.BUID = reader.IsDBNull(FLD_BUID) ? 0 : reader.GetInt32(FLD_BUID);
            offerjoined.BUNAME = reader.IsDBNull(FLD_BUNAME) ? string.Empty : reader.GetString(FLD_BUNAME);
            offerjoined.OfferedCTC = reader.IsDBNull(FLD_OFFEREDCTC) ? string.Empty : reader.GetString(FLD_OFFEREDCTC);
            offerjoined.Grade = reader.IsDBNull(FLD_GRADE) ? string.Empty : reader.GetString(FLD_GRADE);
            offerjoined.BVSTATUSTEXT = reader.IsDBNull(FLD_BVSTATUSTEXT) ? string.Empty : reader.GetString(FLD_BVSTATUSTEXT);
            offerjoined.FITMENTPERCENTILETEXT = reader.IsDBNull(FLD_FITMENTPERCENTILETEXT) ? string.Empty : reader.GetString(FLD_FITMENTPERCENTILETEXT);
            offerjoined.SOURCENAME = reader.IsDBNull(FLD_SOURCENAME) ? string.Empty : reader.GetString(FLD_SOURCENAME);
            offerjoined.SOURCEDESCRIPTIONNAME = reader.IsDBNull(FLD_SOURCEDESCRIPTIONNAME) ? string.Empty : reader.GetString(FLD_SOURCEDESCRIPTIONNAME);

            offerjoined.ActiveRecruiterID = reader.IsDBNull(FLD_ACTIVERECRUITERID) ? 0 : reader.GetInt32(FLD_ACTIVERECRUITERID);
            offerjoined.ActiveRecruiterName = reader.IsDBNull(FLD_ACTIVERECRUITERNAME) ? string.Empty : reader.GetString(FLD_ACTIVERECRUITERNAME);


            offerjoined.OfferStatus = reader.IsDBNull(FLD_OFFERSTATUS) ? string.Empty : reader.GetString(FLD_OFFERSTATUS);
            offerjoined.WorkLocation = reader.IsDBNull(FLD_WORKLOCATION) ? string.Empty : reader.GetString(FLD_WORKLOCATION);
           
            return offerjoined;
        }
        MemberHiringDetails IEntityBuilder<MemberHiringDetails>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERID = 1;
            const int FLD_JOBPOSTINGID = 2;
            const int FLD_OFFEREDPOSITION = 3;
            const int FLD_OFFEREDSALARY = 4;
            const int FLD_OFFEREDSALARYPAYCYCLE = 5;
            const int FLD_OFFEREDSALARYCURRENCY = 6;
            const int FLD_OFFERACCEPTED = 7;
            const int FLD_JOININGDATE = 8;
            const int FLD_COMMISSIONPAYRATE = 9;
            const int FLD_COMMISSIONCURRENCY = 10;
            const int FLD_ISREMOVED = 11;
            const int FLD_CREATORID = 12;
            const int FLD_UPDATORID = 13;
            const int FLD_CREATEDATE = 14;
            const int FLD_UPDATEDATE = 15;
            const int FLD_OFFEREDDATE = 16;

           



            const int FLD_CURRENTCTC = 17;
            const int FLD_CURRENTCTCLOOKUPID = 18;
            const int FLD_MINEXPECTEDCTC = 19;
            const int FLD_MAXEXPECTEDCTC = 20;
            const int FLD_EXPECTEDCTCLOOKUPID = 21;
            const int FLD_GRADE = 22;
            const int FLD_EXPERIENCE = 23;
            const int FLD_SOURCE = 24;
            const int FLD_SOURCEDESCRIPTION = 25;
            const int FLD_FITMENTPERCENTILE = 26;
            const int FLD_JOININGBONUS = 27;
            const int FLD_RELOCATIONALLOWANCE = 28;
            const int FLD_SPLLUMPSUM = 29;
            const int FLD_NOTICEPRDPAYOUT = 30;
            const int FLD_PLACEMENTFEES = 31;
            const int FLD_BVSTATUS = 32;


            const int FLD_JOBPOSTINGCODE = 33;
            const int FLD_JOBTITLE = 34;
            const int FLD_APPLICANTNAME = 35;



            const int FLD_OFFERCATEGORY = 36;
            const int FLD_BASICEMBEDDED = 37;
            const int FLD_HRAEMBEDDED = 38;
            const int FLD_CONVEYANCE = 39;
            const int FLD_MEDICAL = 40;
            const int FLD_EDUCATIONALALLOWANCEEMBEDDED = 41;
            const int FLD_LUFS = 42;
            const int FLD_ADHOCEMBEDDED = 43;
            const int FLD_MPP = 44;
            const int FLD_AGVI = 45;
            const int FLD_LTAEMBEDDED = 46;
            const int FLD_PFEMBEDDED = 47;
            const int FLD_GRATUITYEMBEDDED = 48;
            const int FLD_MAXIMUMANNUALINCENTIVE = 49;
            const int FLD_GRANDTOTAL = 50;


            const int FLD_BASICMECH = 51;
            const int FLD_FLEXIPAY1 = 52;
            const int FLD_FLEXIPAY2 = 53;
            const int FLD_ADDITIONALALLOWANCE = 54;
            const int FLD_ADHOCALLOWANCE = 55;
            const int FLD_LOCATIONALLOWANCE = 56;
            const int FLD_SAFALLOWANCE = 57;
            const int FLD_HRAMECH = 58;
            const int FLD_HLISA = 59;
            const int FLD_EDUCATIONALLOWANCEMECH = 60;
            const int FLD_ACLRA = 61;
            const int FLD_CLRA = 62;
            const int FLD_SPECIALALLOWANCE = 63;
            const int FLD_SPECIALPERFORMANCEPAY = 64;
            const int FLD_ECAL = 65;
            const int FLD_CARMILEAGEREIMBUSEMENT = 66;
            const int FLD_TELEPHONEREIMBUSEMENT = 67;
            const int FLD_LTAMECH = 68;
            const int FLD_PFMECH = 69;
            const int FLD_GRATUITYMECH = 70;
            const int FLD_MEDICALREIMBURSEMENTSDOMICILIARY = 71;
            const int FLD_PCSCHEME = 72;
            const int FLD_RETENTIONPAY = 73;
            const int FLD_TOTALCTC = 74;
            const int FLD_EMBEDDEDALLOWANCE = 75;
            const int FLD_SALESINCENTIVE = 76;
            const int FLD_PLMALLOWANCE = 77;
            const int FLD_CARALLOWANCE = 78;
            const int FLD_PLRMECH = 79;
            const int FLD_SALESINCENTIVEMECH = 80;
            const int FLD_ROLEALLOWANCE = 81;
            const int FLD_SITEALLOWANCE = 82;
            const int FLD_RETENTIONBONUS = 83;
            const int FLD_REIMBURSEMENT = 84;
            const int FLD_ESIC = 85;
            const int FLD_BONUS = 86;
            const int FLD_RELOCATIONALLOWANCEFORCOM = 87;
            const int FLD_PERFORMANCELINKEDINCENTIVE = 88;
            const int FLD_ACTIVERECRUITERID = 89;
            const int FLD_ACTIVERECRUITERNAME = 90;
            const int FLD_OFFERSTATUS = 91;
            const int FLD_WORKLOCATION = 92;
            const int FLD_JOBTITLE1 = 93;
            const int FLD_SUPERVISOR = 94;
            const int FLD_CITYID = 95;
           
            MemberHiringDetails MemberHiringDetails = new MemberHiringDetails();

            MemberHiringDetails.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            MemberHiringDetails.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            MemberHiringDetails.JobPostingId  = reader.IsDBNull(FLD_JOBPOSTINGID ) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID );
            MemberHiringDetails.OfferedPosition = reader.IsDBNull(FLD_OFFEREDPOSITION) ? string .Empty  : reader.GetString (FLD_OFFEREDPOSITION);
            MemberHiringDetails.OfferedSalary = reader.IsDBNull(FLD_OFFEREDSALARY) ? string .Empty  : reader.GetString  (FLD_OFFEREDSALARY);
            MemberHiringDetails.OfferedSalaryPayCycle = reader.IsDBNull(FLD_OFFEREDSALARYPAYCYCLE) ? 0 : reader.GetInt32(FLD_OFFEREDSALARYPAYCYCLE);
            MemberHiringDetails.OfferedSalaryCurrency = reader.IsDBNull(FLD_OFFEREDSALARYCURRENCY) ? 0 : reader.GetInt32(FLD_OFFEREDSALARYCURRENCY);
            MemberHiringDetails.OfferAccepted = reader.IsDBNull(FLD_OFFERACCEPTED) ? true   : reader.GetBoolean (FLD_OFFERACCEPTED);
            MemberHiringDetails.JoiningDate = reader.IsDBNull(FLD_JOININGDATE) ? DateTime .MinValue  : reader.GetDateTime (FLD_JOININGDATE);
            MemberHiringDetails.CommissionPayRate = reader.IsDBNull(FLD_COMMISSIONPAYRATE) ? string .Empty  : reader.GetString  (FLD_COMMISSIONPAYRATE);
            MemberHiringDetails.CommissionCurrency = reader.IsDBNull(FLD_COMMISSIONCURRENCY) ? 0 : reader.GetInt32(FLD_COMMISSIONCURRENCY);
            MemberHiringDetails.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false  : reader.GetBoolean (FLD_ISREMOVED);
            MemberHiringDetails.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            MemberHiringDetails.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            MemberHiringDetails.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            MemberHiringDetails.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            MemberHiringDetails.OfferedDate = reader.IsDBNull(FLD_OFFEREDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_OFFEREDDATE);



            MemberHiringDetails.CurrentCTC = reader.IsDBNull(FLD_CURRENTCTC) ? string.Empty : reader.GetString(FLD_CURRENTCTC);
            MemberHiringDetails.CurrentCTCLookupId = reader.IsDBNull(FLD_CURRENTCTCLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTCTCLOOKUPID);
            MemberHiringDetails.MinExpectedCTC = reader.IsDBNull(FLD_MINEXPECTEDCTC) ? string.Empty : reader.GetString(FLD_MINEXPECTEDCTC);
            MemberHiringDetails.MaxExpectedCTC = reader.IsDBNull(FLD_MAXEXPECTEDCTC) ? string.Empty : reader.GetString(FLD_MAXEXPECTEDCTC);
            MemberHiringDetails.ExpectedCTCLookupId = reader.IsDBNull(FLD_EXPECTEDCTCLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDCTCLOOKUPID);
            MemberHiringDetails.Grade = reader.IsDBNull(FLD_GRADE) ? string.Empty : reader.GetString(FLD_GRADE);
            MemberHiringDetails.Experience = reader.IsDBNull(FLD_EXPERIENCE) ? string.Empty : reader.GetString(FLD_EXPERIENCE);
            MemberHiringDetails.Source = reader.IsDBNull(FLD_SOURCE) ? 0 : reader.GetInt32(FLD_SOURCE);
            MemberHiringDetails.SourceDescription = reader.IsDBNull(FLD_SOURCEDESCRIPTION) ? string.Empty : reader.GetString(FLD_SOURCEDESCRIPTION);
            MemberHiringDetails.FitmentPercentile = reader.IsDBNull(FLD_FITMENTPERCENTILE) ? 0 : reader.GetInt32(FLD_FITMENTPERCENTILE);
            MemberHiringDetails.JoiningBonus = reader.IsDBNull(FLD_JOININGBONUS) ? string.Empty : reader.GetString(FLD_JOININGBONUS);
            MemberHiringDetails.RelocationAllowance = reader.IsDBNull(FLD_RELOCATIONALLOWANCE) ? string.Empty : reader.GetString(FLD_RELOCATIONALLOWANCE);
            MemberHiringDetails.SpecialLumpSum = reader.IsDBNull(FLD_SPLLUMPSUM) ? string.Empty : reader.GetString(FLD_SPLLUMPSUM);
            MemberHiringDetails.NoticePeriodPayout = reader.IsDBNull(FLD_NOTICEPRDPAYOUT) ? string.Empty : reader.GetString(FLD_NOTICEPRDPAYOUT);
            MemberHiringDetails.PlacementFees = reader.IsDBNull(FLD_PLACEMENTFEES) ? string.Empty : reader.GetString(FLD_PLACEMENTFEES);
            MemberHiringDetails.BVStatus = reader.IsDBNull(FLD_BVSTATUS) ? 0 : reader.GetInt32(FLD_BVSTATUS);
           
            if (reader.FieldCount > FLD_JOBPOSTINGCODE)
            {
                MemberHiringDetails.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
                MemberHiringDetails.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
                MemberHiringDetails.ApplicantName = reader.IsDBNull(FLD_APPLICANTNAME) ? string.Empty : reader.GetString(FLD_APPLICANTNAME);
            }

            if (reader.FieldCount > FLD_OFFERCATEGORY)
            {
                MemberHiringDetails.OfferCategory = reader.IsDBNull(FLD_OFFERCATEGORY) ? 0 : reader.GetInt32(FLD_OFFERCATEGORY);

                MemberHiringDetails.BasicEmbedded = reader.IsDBNull(FLD_BASICEMBEDDED) ? 0 : reader.GetDecimal(FLD_BASICEMBEDDED);
                MemberHiringDetails.HRAEmbedded = reader.IsDBNull(FLD_HRAEMBEDDED) ? 0 : reader.GetDecimal(FLD_HRAEMBEDDED);
                MemberHiringDetails.Conveyance = reader.IsDBNull(FLD_CONVEYANCE) ? 0 : reader.GetDecimal(FLD_CONVEYANCE);
                MemberHiringDetails.Medical = reader.IsDBNull(FLD_MEDICAL) ? 0 : reader.GetDecimal(FLD_MEDICAL);
                MemberHiringDetails.EducationalAllowanceEmbedded = reader.IsDBNull(FLD_EDUCATIONALALLOWANCEEMBEDDED) ? 0 : reader.GetDecimal(FLD_EDUCATIONALALLOWANCEEMBEDDED);
                MemberHiringDetails.LUFS = reader.IsDBNull(FLD_LUFS) ? 0 : reader.GetDecimal(FLD_LUFS);
                MemberHiringDetails.Adhoc = reader.IsDBNull(FLD_ADHOCEMBEDDED) ? 0 : reader.GetDecimal(FLD_ADHOCEMBEDDED);
                MemberHiringDetails.MPP = reader.IsDBNull(FLD_MPP) ? 0 : reader.GetDecimal(FLD_MPP);
                MemberHiringDetails.AGVI = reader.IsDBNull(FLD_AGVI) ? 0 : reader.GetDecimal(FLD_AGVI);
                MemberHiringDetails.LTAEmbedded = reader.IsDBNull(FLD_LTAEMBEDDED) ? 0 : reader.GetDecimal(FLD_LTAEMBEDDED);
                MemberHiringDetails.PFEmbedded = reader.IsDBNull(FLD_PFEMBEDDED) ? 0 : reader.GetDecimal(FLD_PFEMBEDDED);
                MemberHiringDetails.GratuityEmbedded = reader.IsDBNull(FLD_GRATUITYEMBEDDED) ? 0 : reader.GetDecimal(FLD_GRATUITYEMBEDDED);
                MemberHiringDetails.MaximumAnnualIncentive = reader.IsDBNull(FLD_MAXIMUMANNUALINCENTIVE) ? 0 : reader.GetDecimal(FLD_MAXIMUMANNUALINCENTIVE);
                MemberHiringDetails.GrandTotal = reader.IsDBNull(FLD_GRANDTOTAL) ? 0 : reader.GetDecimal(FLD_GRANDTOTAL);

                MemberHiringDetails.BasicMech = reader.IsDBNull(FLD_BASICMECH) ? 0 : reader.GetDecimal(FLD_BASICMECH);
                MemberHiringDetails.FlexiPay1 = reader.IsDBNull(FLD_FLEXIPAY1) ? 0 : reader.GetDecimal(FLD_FLEXIPAY1);
                MemberHiringDetails.FlexiPay2 = reader.IsDBNull(FLD_FLEXIPAY2) ? 0 : reader.GetDecimal(FLD_FLEXIPAY2);
                MemberHiringDetails.AdditionalAllowance = reader.IsDBNull(FLD_ADDITIONALALLOWANCE) ? 0 : reader.GetDecimal(FLD_ADDITIONALALLOWANCE);
                MemberHiringDetails.AdHocAllowance = reader.IsDBNull(FLD_ADHOCALLOWANCE) ? 0 : reader.GetDecimal(FLD_ADHOCALLOWANCE);
                MemberHiringDetails.LocationAllowance = reader.IsDBNull(FLD_LOCATIONALLOWANCE) ? 0 : reader.GetDecimal(FLD_LOCATIONALLOWANCE);
                MemberHiringDetails.SAFAllowance = reader.IsDBNull(FLD_SAFALLOWANCE) ? 0 : reader.GetDecimal(FLD_SAFALLOWANCE);
                MemberHiringDetails.HRAMech = reader.IsDBNull(FLD_HRAMECH) ? 0 : reader.GetDecimal(FLD_HRAMECH);
                MemberHiringDetails.HLISA = reader.IsDBNull(FLD_HLISA) ? 0 : reader.GetDecimal(FLD_HLISA);
                MemberHiringDetails.EducationAllowanceMech = reader.IsDBNull(FLD_EDUCATIONALLOWANCEMECH) ? 0 : reader.GetDecimal(FLD_EDUCATIONALLOWANCEMECH);
                MemberHiringDetails.ACLRA = reader.IsDBNull(FLD_ACLRA) ? 0 : reader.GetDecimal(FLD_ACLRA);
                MemberHiringDetails.CLRA = reader.IsDBNull(FLD_CLRA) ? 0 : reader.GetDecimal(FLD_CLRA);
                MemberHiringDetails.SpecialAllowance = reader.IsDBNull(FLD_SPECIALALLOWANCE) ? 0 : reader.GetDecimal(FLD_SPECIALALLOWANCE);
                MemberHiringDetails.SpecialPerformancePay = reader.IsDBNull(FLD_SPECIALPERFORMANCEPAY) ? 0 : reader.GetDecimal(FLD_SPECIALPERFORMANCEPAY);
                MemberHiringDetails.ECAL = reader.IsDBNull(FLD_ECAL) ? 0 : reader.GetDecimal(FLD_ECAL);
                MemberHiringDetails.CarMileageReimbursement = reader.IsDBNull(FLD_CARMILEAGEREIMBUSEMENT) ? 0 : reader.GetDecimal(FLD_CARMILEAGEREIMBUSEMENT);
                MemberHiringDetails.TelephoneReimbursement = reader.IsDBNull(FLD_TELEPHONEREIMBUSEMENT) ? 0 : reader.GetDecimal(FLD_TELEPHONEREIMBUSEMENT);
                MemberHiringDetails.LTAMech = reader.IsDBNull(FLD_LTAMECH) ? 0 : reader.GetDecimal(FLD_LTAMECH);
                MemberHiringDetails.PFMech = reader.IsDBNull(FLD_PFMECH) ? 0 : reader.GetDecimal(FLD_PFMECH);
                MemberHiringDetails.GratuityMech = reader.IsDBNull(FLD_GRATUITYMECH) ? 0 : reader.GetDecimal(FLD_GRATUITYMECH);
                MemberHiringDetails.MedicalreimbursementsDomiciliary = reader.IsDBNull(FLD_MEDICALREIMBURSEMENTSDOMICILIARY) ? 0 : reader.GetDecimal(FLD_MEDICALREIMBURSEMENTSDOMICILIARY);
                MemberHiringDetails.PCScheme = reader.IsDBNull(FLD_PCSCHEME) ? 0 : reader.GetDecimal(FLD_PCSCHEME);
                MemberHiringDetails.RetentionPay = reader.IsDBNull(FLD_RETENTIONPAY) ? 0 : reader.GetDecimal(FLD_RETENTIONPAY);
                MemberHiringDetails.TotalCTC = reader.IsDBNull(FLD_TOTALCTC) ? 0 : reader.GetDecimal(FLD_TOTALCTC);
                MemberHiringDetails.EmbeddedAllowance = reader.IsDBNull(FLD_EMBEDDEDALLOWANCE) ? 0 : reader.GetDecimal(FLD_EMBEDDEDALLOWANCE);
                MemberHiringDetails.SalesIncentive = reader.IsDBNull(FLD_SALESINCENTIVE) ? 0 : reader.GetDecimal(FLD_SALESINCENTIVE);
                MemberHiringDetails.PLMAllowance = reader.IsDBNull(FLD_PLMALLOWANCE) ? 0 : reader.GetDecimal(FLD_PLMALLOWANCE);
                MemberHiringDetails.CarAllowance = reader.IsDBNull(FLD_CARALLOWANCE) ? 0 : reader.GetDecimal(FLD_CARALLOWANCE);
                MemberHiringDetails.PLRMech = reader.IsDBNull(FLD_PLRMECH) ? 0 : reader.GetDecimal(FLD_PLRMECH);
                MemberHiringDetails.SalesIncentiveMech = reader.IsDBNull(FLD_SALESINCENTIVEMECH) ? 0 : reader.GetDecimal(FLD_SALESINCENTIVEMECH);
                MemberHiringDetails.RoleAllowance = reader.IsDBNull(FLD_ROLEALLOWANCE) ? 0 : reader.GetDecimal(FLD_ROLEALLOWANCE);
                MemberHiringDetails.SiteAllowance = reader.IsDBNull(FLD_SITEALLOWANCE) ? 0 : reader.GetDecimal(FLD_SITEALLOWANCE);
                MemberHiringDetails.RetentionBonus = reader.IsDBNull(FLD_RETENTIONBONUS) ? 0 : reader.GetDecimal(FLD_RETENTIONBONUS);
                MemberHiringDetails.Reimbursement = reader.IsDBNull(FLD_REIMBURSEMENT) ? 0 : reader.GetDecimal(FLD_REIMBURSEMENT);
                MemberHiringDetails.ESIC = reader.IsDBNull(FLD_ESIC) ? 0 : reader.GetDecimal(FLD_ESIC);
                MemberHiringDetails.Bonus = reader.IsDBNull(FLD_BONUS) ? 0 : reader.GetDecimal(FLD_BONUS);
                MemberHiringDetails.RelocationAllowanceForCom = reader.IsDBNull(FLD_RELOCATIONALLOWANCEFORCOM) ? 0 : reader.GetDecimal(FLD_RELOCATIONALLOWANCEFORCOM);
                MemberHiringDetails.PerformanceLinkedIncentive = reader.IsDBNull(FLD_PERFORMANCELINKEDINCENTIVE) ? 0 : reader.GetDecimal(FLD_PERFORMANCELINKEDINCENTIVE);
                MemberHiringDetails.ActiveRecruiterID = reader.IsDBNull(FLD_ACTIVERECRUITERID) ? 0 : reader.GetInt32(FLD_ACTIVERECRUITERID);
                MemberHiringDetails.ActiveRecruiterName = reader.IsDBNull(FLD_ACTIVERECRUITERNAME) ? string.Empty : reader.GetString(FLD_ACTIVERECRUITERNAME);
                MemberHiringDetails.OfferStatus = reader.IsDBNull(FLD_OFFERSTATUS) ? string.Empty : reader.GetString(FLD_OFFERSTATUS);
                MemberHiringDetails.WorkLocation = reader.IsDBNull(FLD_WORKLOCATION) ? string.Empty : reader.GetString(FLD_WORKLOCATION);
                MemberHiringDetails.JobTitle1 = reader.IsDBNull(FLD_JOBTITLE1) ? string.Empty : reader.GetString(FLD_JOBTITLE1);
                MemberHiringDetails.Supervisor = reader.IsDBNull(FLD_SUPERVISOR) ? string.Empty : reader.GetString(FLD_SUPERVISOR);
                MemberHiringDetails.CityId = reader.IsDBNull(FLD_CITYID) ? 0 : reader.GetInt32(FLD_CITYID);
             
            }
          
            return MemberHiringDetails;
        }
    }
}