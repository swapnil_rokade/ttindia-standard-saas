﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CompanyContactDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.3              02/dec/2015           pravin khot      GetAllByCompanyId_IP(int companyId, int paneltypeid) added 
 *  0.4              26/fEB/2016           PRAVIN KHOT      ADDED GetCompanyContactByEmailAndCompanyId
    0.5              23/March/2017         pravin khot      added - GetCompanyContactByJobPostingHiringTeam
 ---------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class CompanyContactDataAccess : BaseDataAccess, ICompanyContactDataAccess
    {
        #region Constructors

        public CompanyContactDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CompanyContact> CreateEntityBuilder<CompanyContact>()
        {
            return (new CompanyContactBuilder()) as IEntityBuilder<CompanyContact>;
        }

        #endregion

        #region  Methods
        IList<CompanyContact> ICompanyContactDataAccess.GetAllContactByIdAndDate(int ID, string Date)
        {
            if (ID < 1)
            {
                throw new ArgumentNullException("ID");
            }

            const string SP = "dbo.CompanyContact_GetAllContactByIDAndDate";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ID", DbType.Int32, ID);
                Database.AddInParameter(cmd, "@Date", DbType.String, Date);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyContact>().BuildEntities(reader);
                }
            }
        }
        CompanyContact ICompanyContactDataAccess.Add(CompanyContact companyContact)
        {
            const string SP = "dbo.CompanyContact_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyContact.CompanyId);
                Database.AddInParameter(cmd, "@IsPrimaryContact", DbType.Boolean, companyContact.IsPrimaryContact);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, StringHelper.Convert(companyContact.Title));
                Database.AddInParameter(cmd, "@NoBulkEmail", DbType.Boolean, companyContact.NoBulkEmail);
                Database.AddInParameter(cmd, "@Email", DbType.AnsiString, StringHelper.Convert(companyContact.Email));
                Database.AddInParameter(cmd, "@FirstName", DbType.AnsiString, StringHelper.Convert(companyContact.FirstName));
                Database.AddInParameter(cmd, "@LastName", DbType.AnsiString, StringHelper.Convert(companyContact.LastName));
                Database.AddInParameter(cmd, "@Address1", DbType.AnsiString, StringHelper.Convert(companyContact.Address1));
                Database.AddInParameter(cmd, "@Address2", DbType.AnsiString, StringHelper.Convert(companyContact.Address2));
                Database.AddInParameter(cmd, "@City", DbType.AnsiString, StringHelper.Convert(companyContact.City));
                Database.AddInParameter(cmd, "@StateId", DbType.Int32, companyContact.StateId);
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, companyContact.CountryId);
                Database.AddInParameter(cmd, "@ZipCode", DbType.AnsiString, StringHelper.Convert(companyContact.ZipCode));
                Database.AddInParameter(cmd, "@OfficePhone", DbType.AnsiString, StringHelper.Convert(companyContact.OfficePhone));
                Database.AddInParameter(cmd, "@OfficePhoneExtension", DbType.AnsiString, StringHelper.Convert(companyContact.OfficePhoneExtension));
                Database.AddInParameter(cmd, "@MobilePhone", DbType.AnsiString, StringHelper.Convert(companyContact.MobilePhone));
                Database.AddInParameter(cmd, "@DirectNumber", DbType.AnsiString, StringHelper.Convert(companyContact.DirectNumber));
                Database.AddInParameter(cmd, "@DirectNumberExtension", DbType.AnsiString, StringHelper.Convert(companyContact.DirectNumberExtension));
                Database.AddInParameter(cmd, "@ContactRemarks", DbType.AnsiString, StringHelper.Convert(companyContact.ContactRemarks));
                Database.AddInParameter(cmd, "@Fax", DbType.AnsiString, StringHelper.Convert(companyContact.Fax));
                Database.AddInParameter(cmd, "@IsOwner", DbType.Boolean, companyContact.IsOwner);
                Database.AddInParameter(cmd, "@OwnershipPercentage", DbType.Decimal, companyContact.OwnershipPercentage);
                Database.AddInParameter(cmd, "@ResumeFile", DbType.AnsiString, StringHelper.Convert(companyContact.ResumeFile));
                Database.AddInParameter(cmd, "@EthnicGroupLookupId", DbType.Int32, companyContact.EthnicGroupLookupId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, companyContact.MemberId);
                Database.AddInParameter(cmd, "@DivisionLookupId", DbType.Int32, companyContact.DivisionLookupId);
                Database.AddInParameter(cmd, "@FolderName", DbType.AnsiString, companyContact.FolderName);
                Database.AddInParameter(cmd, "@PortalAccess", DbType.Boolean, companyContact.PortalAccess);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, companyContact.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, companyContact.UpdatorId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        companyContact = CreateEntityBuilder<CompanyContact>().BuildEntity(reader);
                    }
                    else
                    {
                        companyContact = null;
                    }
                }

                if (companyContact == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Company contact already exists. Please specify another company contact.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this company contact.");
                            }
                    }
                }

                return companyContact;
            }
        }

        CompanyContact ICompanyContactDataAccess.Update(CompanyContact companyContact)
        {
            const string SP = "dbo.CompanyContact_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, companyContact.Id);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyContact.CompanyId);
                Database.AddInParameter(cmd, "@IsPrimaryContact", DbType.Boolean, companyContact.IsPrimaryContact);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, StringHelper.Convert(companyContact.Title));
                Database.AddInParameter(cmd, "@NoBulkEmail", DbType.Boolean, companyContact.NoBulkEmail);
                Database.AddInParameter(cmd, "@Email", DbType.AnsiString, StringHelper.Convert(companyContact.Email));
                Database.AddInParameter(cmd, "@FirstName", DbType.AnsiString, StringHelper.Convert(companyContact.FirstName));
                Database.AddInParameter(cmd, "@LastName", DbType.AnsiString, StringHelper.Convert(companyContact.LastName));
                Database.AddInParameter(cmd, "@Address1", DbType.AnsiString, StringHelper.Convert(companyContact.Address1));
                Database.AddInParameter(cmd, "@Address2", DbType.AnsiString, StringHelper.Convert(companyContact.Address2));
                Database.AddInParameter(cmd, "@City", DbType.AnsiString, StringHelper.Convert(companyContact.City));
                Database.AddInParameter(cmd, "@StateId", DbType.Int32, companyContact.StateId);
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, companyContact.CountryId);
                Database.AddInParameter(cmd, "@ZipCode", DbType.AnsiString, StringHelper.Convert(companyContact.ZipCode));
                Database.AddInParameter(cmd, "@OfficePhone", DbType.AnsiString, StringHelper.Convert(companyContact.OfficePhone));
                Database.AddInParameter(cmd, "@OfficePhoneExtension", DbType.AnsiString, StringHelper.Convert(companyContact.OfficePhoneExtension));
                Database.AddInParameter(cmd, "@MobilePhone", DbType.AnsiString, StringHelper.Convert(companyContact.MobilePhone));
                Database.AddInParameter(cmd, "@DirectNumber", DbType.AnsiString, StringHelper.Convert(companyContact.DirectNumber));
                Database.AddInParameter(cmd, "@DirectNumberExtension", DbType.AnsiString, StringHelper.Convert(companyContact.DirectNumberExtension));
                Database.AddInParameter(cmd, "@ContactRemarks", DbType.AnsiString, StringHelper.Convert(companyContact.ContactRemarks));
                Database.AddInParameter(cmd, "@Fax", DbType.AnsiString, StringHelper.Convert(companyContact.Fax));
                Database.AddInParameter(cmd, "@IsOwner", DbType.Boolean, companyContact.IsOwner);
                Database.AddInParameter(cmd, "@OwnershipPercentage", DbType.Decimal, companyContact.OwnershipPercentage);
                Database.AddInParameter(cmd, "@ResumeFile", DbType.AnsiString, StringHelper.Convert(companyContact.ResumeFile));
                Database.AddInParameter(cmd, "@EthnicGroupLookupId", DbType.Int32, companyContact.EthnicGroupLookupId);
                Database.AddInParameter(cmd, "@DivisionLookupId", DbType.Int32, companyContact.DivisionLookupId);
                Database.AddInParameter(cmd, "@FolderName", DbType.AnsiString, companyContact.FolderName);
                Database.AddInParameter(cmd, "@PortalAccess", DbType.Boolean, companyContact.PortalAccess);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, companyContact.UpdatorId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        companyContact = CreateEntityBuilder<CompanyContact>().BuildEntity(reader);
                    }
                    else
                    {
                        companyContact = null;
                    }
                }

                if (companyContact == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    //switch (returnCode)
                    //{
                    //    //case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                    //       // {
                    //            throw new ArgumentException("CompanyContact already exists. Please specify another company contact.");
                    //       // }
                    //    default:
                    //        {
                    //            throw new SystemException("An unexpected error has occurred while updating this company contact.");
                    //        }
                    //}
                }

                return companyContact;
            }
        }

        bool ICompanyContactDataAccess.UpdateMemberId(int companyContactId, int memberId)
        {
            const string SP = "dbo.CompanyContact_UpdateMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyContactId", DbType.Int32, companyContactId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_SUCCESS_UPDATE:
                            {
                                return true;

                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this company contact.");
                            }
                    }
                }
            }
        }

        bool ICompanyContactDataAccess.UpdateResumeFile(int contactId, string fileName)
        {
            const string SP = "dbo.CompanyContact_UpdateResumeFile";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ContactId", DbType.Int32, contactId);
                Database.AddInParameter(cmd, "@FileName", DbType.AnsiString, fileName);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_UPDATE:
                        {
                            return true;
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while updating this contact file name.");
                        }
                }
            }
        }

        CompanyContact ICompanyContactDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CompanyContact_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CompanyContact>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        CompanyContact ICompanyContactDataAccess.GetByMemberId(int memberId)
        {
            const string SP = "dbo.CompanyContact_GetByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CompanyContact>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        CompanyContact ICompanyContactDataAccess.GetByEmail(string email)
        {
            const string SP = "dbo.CompanyContact_GetByEmail";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Email", DbType.AnsiString, email);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CompanyContact>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        CompanyContact ICompanyContactDataAccess.GetPrimary(int companyId)
        {
            if (companyId < 1)
            {
                throw new ArgumentNullException("companyId");
            }

            const string SP = "dbo.CompanyContact_GetPrimary";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CompanyContact>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<CompanyContact> ICompanyContactDataAccess.GetAll()
        {
            const string SP = "dbo.CompanyContact_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyContact>().BuildEntities(reader);
                }
            }
        }

        IList<CompanyContact> ICompanyContactDataAccess.GetAllByCompanyId(int companyId)
        {
            const string SP = "dbo.CompanyContact_GetAllByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyContact>().BuildEntities(reader);
                }
            }
        }

        IList<CompanyContact> ICompanyContactDataAccess.GetAllByCompanyGroupId(int companyGroupId)
        {
            const string SP = "dbo.CompanyContact_GetAllByCompanyGroupId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyGroupId", DbType.Int32, companyGroupId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyContact>().BuildEntities(reader);
                }
            }
        }

        IList<CompanyContact> ICompanyContactDataAccess.GetAllByComapnyIdAndLogin(int companyId, bool IsLoginCreated)
        {
            const string SP = "dbo.CompanyContact_GetAllByComapnyIdAndLogin";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);
                Database.AddInParameter(cmd, "@IsLoginCreated", DbType.Boolean, IsLoginCreated);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyContact>().BuildEntities(reader);
                }
            }
        }

        bool ICompanyContactDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CompanyContact_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a company contact which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company contact.");
                        }
                }
            }
        }

        bool ICompanyContactDataAccess.DeleteByCompanyId(int companyId)
        {
            if (companyId < 1)
            {
                throw new ArgumentNullException("companyId");
            }

            const string SP = "dbo.CompanyContact_DeleteByComapnyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a company contact which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company contact.");
                        }
                }
            }
        }
        PagedResponse<CompanyContact> ICompanyContactDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.CompanyContact_GetPaged";
            bool flag = false;
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");
                      
                        
                            if (StringHelper.IsEqual(column, "CompanyId"))
                            {
                                if (sb.ToString() != "")
                                    sb.Append(" AND ");
                                sb.Append("  [C].[CompanyId]");
                                sb.Append(" = ");
                                sb.Append(value);

                            }

                            if (StringHelper.IsEqual(column, "contactPrefix"))
                            {
                                if (sb.ToString() != "")
                                    sb.Append(" AND ");
                                sb.Append(" ([C].[FIRSTNAME] LIKE '%");

                                sb.Append(value); sb.Append("%' ");
                                sb.Append(" OR [C].[LASTNAME] LIKE '%");

                                sb.Append(value); sb.Append("%') ");
                            }
                        
                       


                    }
                }
                if (!flag)
                {
                    whereClause = sb.ToString();
                }
            }



            if (StringHelper.IsBlank(request.SortColumn))
            {
                //request.SortColumn = "CreateDate";
                request.SortColumn = "FirstName";
                //request.SortOrder = "ASC";
            }

            request.SortColumn = "[C].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<CompanyContact> response = new PagedResponse<CompanyContact>();
                try
                {
                    using (IDataReader reader = Database.ExecuteReader(cmd))
                    {
                        response.Response = CreateEntityBuilder<CompanyContact>().BuildEntities(reader);

                        if ((reader.NextResult()) && (reader.Read()))
                        {
                            response.TotalRow = reader.GetInt32(0);
                        }
                    }

                    return response;
                }
                catch (Exception ex)
                {
                    return response;
                }
            }
        }
        IList<CompanyContact> ICompanyContactDataAccess.GetAllByVolumeHireGroup()
        {
            const string SP = "dbo.CompanyContact_GetAllByVolumeHire";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyContact>().BuildEntities(reader);
                }
            }
        }

        ArrayList ICompanyContactDataAccess.GetAllCompanyContactsByCompanyId(int CompanyId)
        {

            if (CompanyId < 0)
            {
                throw new ArgumentException("CompanyId");
            }
            const string SP = "dbo.CompanyContact_GetAllContactsByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, CompanyId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList ContactList = new ArrayList();

                    while (reader.Read())
                    {
                        ContactList.Add(new CompanyContact () { MemberId = reader.GetInt32(0), FirstName = reader.GetString(1),Id =reader .GetInt32 (2),Email =reader .GetString (3) });//1.3
                    }

                    return ContactList;
                }
            }
        }



        private string BuildWhereClauseForVendorSubmission(PagedRequest request)
        {
            string whereClause = string.Empty, strbranchmap = string.Empty;
            bool flag = false;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "ContactId"))
                        {
                            if (sb.ToString().Trim() != string.Empty) sb.Append(" And ");
                            sb.Append("[CC].[Id]");
                            sb.Append(" ='");
                            sb.Append(value);
                            sb.Append("'");
                        }
                        if (StringHelper.IsEqual(column, "ContactMemberId"))
                        {
                            if (sb.ToString().Trim() != string.Empty) sb.Append(" And ");
                            sb.Append("[CC].[MemberId]");
                            sb.Append(" ='");
                            sb.Append(value);
                            sb.Append("'");
                        }

                        if (StringHelper.IsEqual(column, "VendorId"))
                        {
                            if (sb.ToString().Trim() != string.Empty) sb.Append(" And ");
                            sb.Append("[CC].[CompanyId]");
                            sb.Append(" ='");
                            sb.Append(value);
                            sb.Append("'");
                        }

                        if (StringHelper.IsEqual(column, "JobPostingID"))
                        {
                            if (sb.ToString().Trim() != string.Empty) sb.Append(" And ");
                            sb.Append("[MJC].[JobPostingID]");
                            sb.Append(" =");
                            sb.Append(value);
                            sb.Append(" ");
                        }
                        if (StringHelper.IsEqual(column, "CandidateId"))
                        {
                            if (sb.ToString().Trim() != string.Empty) sb.Append(" And ");
                            sb.Append("[MJC].[MemberId]");
                            sb.Append(" =");
                            sb.Append(value);
                            sb.Append(" ");
                        }

                        if (StringHelper.IsEqual(column, "AddedFrom"))
                        {
                            if (sb.ToString().Trim() != string.Empty) sb.Append(" And ");
                            sb.Append("[MJC].[CreateDate]");
                            sb.Append(" >=convert(datetime,'");
                            sb.Append(value);
                            sb.Append("',103)");

                        }
                        if (StringHelper.IsEqual(column, "AddedTo"))
                        {
                            if (sb.ToString().Trim() != string.Empty) sb.Append(" And ");
                            sb.Append("[MJC].[CreateDate]");
                            sb.Append(" <=convert(datetime,'");
                            sb.Append(value);
                            sb.Append("',103) +1");
                        }
                    }
                }

                whereClause = sb.ToString();
            }
            return whereClause;
        }

        PagedResponse<ListWithCount> ICompanyContactDataAccess.GetPagedVendorSubmissionSummary(PagedRequest request, string type)
        {

            string SP = "dbo.VendorSubmissions_GetPagedByVendorContact";
            if (type == "ByRequisition") SP = "dbo.VendorSubmissions_GetPagedByRequisition";
            string WhereClause = BuildWhereClauseForVendorSubmission(request);

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(WhereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};


            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    PagedResponse<ListWithCount> response = new PagedResponse<ListWithCount>();
                    IList<ListWithCount> Resule = new List<ListWithCount>();
                    while (reader.Read())
                    {
                       
                        ListWithCount list = new ListWithCount();
                        list.Id = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        list.Name = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
                        list.Count = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                        if (reader.FieldCount == 5)
                        {
                            list.AdditionalID = reader.IsDBNull(3) ? 0 : reader.GetInt32(0);
                            list.AdditionalName = reader.IsDBNull(4) ? string.Empty : reader.GetString(4);
                        }
                        Resule.Add(list);

                    }
                    response.Response = Resule;
                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                    return response;
                }
            }
        }




        IList<ListWithCount> ICompanyContactDataAccess.GetAllVendorSubmissionsGroupByDate(int JobPostingId, int VendorId,int ContactID, int StartDate, int EndDate)
        {
            const string SP = "dbo.VendorSubmissions_GetCount";
           System.Text . StringBuilder WhereClause = new System .Text . StringBuilder();
            if (JobPostingId > 0)
            {
                WhereClause.Append(" JobPostingId=" + JobPostingId);
            }
            if (VendorId  > 0)
            {
                if (WhereClause.ToString() != string.Empty) WhereClause.Append(" And ");
                WhereClause.Append(" [CC].[CompanyId] = " + VendorId );
            }
            if (ContactID  > 0)
            {
                if (WhereClause.ToString() != string.Empty) WhereClause.Append(" And ");
                WhereClause.Append(" [CC].[Id] = " + ContactID );
            }
            if (StartDate > 0)
            {
                if (WhereClause.ToString() != string.Empty) WhereClause.Append(" And ");
                WhereClause.Append(" convert(varchar(8),MJC.CreateDate,112)  >= " + StartDate);
            }
            if (EndDate > 0)
            {
                if (WhereClause.ToString() != string.Empty) WhereClause.Append(" And ");
                WhereClause.Append(" convert(varchar(8),MJC.CreateDate,112)  <= " + EndDate);
            }

            IList<ListWithCount> result = new List<ListWithCount>();
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@WhereClause", DbType.AnsiString, StringHelper.Convert(WhereClause));
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList clientList = new ArrayList();

                    while (reader.Read())
                    {
                        ListWithCount list = new ListWithCount();
                        list.Name = reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                        list.Count = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                        result.Add(list);
                    }

                    return result;
                }
            }
        }

        //------------------code introduced by pravin khot 02/dec/2015
        IList<CompanyContact> ICompanyContactDataAccess.GetAllByCompanyId_IP(int companyId)
        {
            const string SP = "dbo.CompanyContact_GetAllByCompanyId_IP";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyContact>().BuildEntities(reader);
                }
            }
        }

        //----------------------------------END------------------------------------------


        PagedResponse<VendorSubmissions > ICompanyContactDataAccess.GetPagedVendorSubmissions(PagedRequest request)
        {
            const string SP = "dbo.VendorSubmissions_GetPaged";
            bool flag = false;
               string whereClause= BuildWhereClauseForVendorSubmission(request);

               if (request.SortColumn == null || request.SortColumn == string.Empty) request.SortColumn = "[MJC].[CreateDate]";
               if (request.SortOrder == null || request.SortOrder == string.Empty) request.SortOrder = "Desc";
    
                object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<VendorSubmissions > response = new PagedResponse<VendorSubmissions >();
                try
                {
                    using (IDataReader reader = Database.ExecuteReader(cmd))
                    {
                        response.Response = (new CompanyContactBuilder()).BuildVendorSubmissionEntities(reader);// CreateEntityBuilder<CompanyContact>().BuildEntities(reader);

                        if ((reader.NextResult()) && (reader.Read()))
                        {
                            response.TotalRow = reader.GetInt32(0);
                        }
                    }

                    return response;
                }
                catch (Exception ex)
                {
                    return response;
                }
            }
        }

        //*************Code added by pravin khot on 26/Feb/2016********************
        CompanyContact ICompanyContactDataAccess.GetCompanyContactByEmailAndCompanyId(string email, int CompanyId)
        {
            const string SP = "CompanyContact_GetByEmailAndCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Email", DbType.AnsiString, email);
                Database.AddInParameter(cmd, "@CompanyId", DbType.AnsiString, CompanyId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CompanyContact>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        //**************************************END********************************************

        //*************Code added by pravin khot on 23/March/2017********************
        string ICompanyContactDataAccess.GetCompanyContactByJobPostingHiringTeam(int CompanyContactId)
        {
            if (CompanyContactId < 1)
            {
                throw new ArgumentNullException("MemberId");
            }

            const string SP = "dbo.CompanyContactByJobPostingHiringTeam";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyContactId", DbType.Int32, CompanyContactId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                    return string.Empty;
                }
            }
        }
        //**************************************END********************************************
        #endregion
    }
}