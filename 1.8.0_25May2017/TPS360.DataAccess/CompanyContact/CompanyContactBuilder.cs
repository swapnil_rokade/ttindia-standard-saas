﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CompanyContactBuilder : IEntityBuilder<CompanyContact>
    {
        IList<CompanyContact> IEntityBuilder<CompanyContact>.BuildEntities(IDataReader reader)
        {
            List<CompanyContact> companyContacts = new List<CompanyContact>();

            while (reader.Read())
            {
                companyContacts.Add(((IEntityBuilder<CompanyContact>)this).BuildEntity(reader));
            }

            return (companyContacts.Count > 0) ? companyContacts : null;
        }


        public IList<VendorSubmissions > BuildVendorSubmissionEntities(IDataReader reader)
        {
            List<VendorSubmissions> vendorsubmissions = new List<VendorSubmissions >();

            while (reader.Read())
            {
                vendorsubmissions.Add(BuildVendorSubmissionEntity (reader));
            }

            return (vendorsubmissions.Count > 0) ? vendorsubmissions : null;
        }

        VendorSubmissions BuildVendorSubmissionEntity(IDataReader reader)
        {
            const int FLD_CONTACTID = 0;
            const int FLD_CONTACTNAME = 1;
            const int FLD_COMPANYID = 2;
            const int FLD_COMPANYNAME = 3;
            const int FLD_JOBPOSTINGID = 4;
            const int FLD_JOBTITLE = 5;
            const int FLD_CANDIDATEID = 6;
            const int FLD_CANDIDATENAME = 7;
            const int FLD_SUBMITTEDDATE = 8;
            const int FLD_CURRENTSTATUS = 9;
            VendorSubmissions vs = new VendorSubmissions();
            vs.Id = reader.IsDBNull(FLD_CONTACTID) ? 0 : reader.GetInt32(FLD_CONTACTID);
            vs.ContactName = reader.IsDBNull(FLD_CONTACTNAME) ? string .Empty  : reader.GetString(FLD_CONTACTNAME);
            vs.VendorID = reader.IsDBNull(FLD_COMPANYID) ? 0 : reader.GetInt32 (FLD_COMPANYID);
            vs.VendorName = reader.IsDBNull(FLD_COMPANYNAME) ? string.Empty : reader.GetString(FLD_COMPANYNAME);
            vs.JobPostingID = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            vs.JobTitle  = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            vs.CandidateId = reader.IsDBNull(FLD_CANDIDATEID) ? 0 : reader.GetInt32(FLD_CANDIDATEID);
            vs.CandidateName = reader.IsDBNull(FLD_CANDIDATENAME) ? string.Empty : reader.GetString(FLD_CANDIDATENAME);
            vs.SubmittedDate =reader .IsDBNull (FLD_SUBMITTEDDATE )? System.DateTime .MinValue : reader.GetDateTime (FLD_SUBMITTEDDATE );
            vs.CurrentStatus = reader.IsDBNull(FLD_CURRENTSTATUS) ? string.Empty : reader.GetString(FLD_CURRENTSTATUS);
            return vs;
        }
        CompanyContact IEntityBuilder<CompanyContact>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_COMPANYID = 1;
            const int FLD_ISPRIMARYCONTACT = 2;
            const int FLD_TITLE = 3;
            const int FLD_NOBULKEMAIL = 4;
            const int FLD_EMAIL = 5;
            const int FLD_FIRSTNAME = 6;
            const int FLD_LASTNAME = 7;
            const int FLD_ADDRESS1 = 8;
            const int FLD_ADDRESS2 = 9;
            const int FLD_CITY = 10;
            const int FLD_STATEID = 11;
            const int FLD_COUNTRYID = 12;
            const int FLD_ZIPCODE = 13;
            const int FLD_OFFICEPHONE = 14;
            const int FLD_OFFICEPHONEEXTENSION = 15;
            const int FLD_MOBILEPHONE = 16;
            const int FLD_DIRECTNUMBER = 17;
            const int FLD_DIRECTNUMBEREXTENSION = 18;
            const int FLD_CONTACTREMARKS = 19;
            const int FLD_FAX = 20;
            const int FLD_ISOWNER = 21;
            const int FLD_OWNERSHIPPERCENTAGE = 22;
            const int FLD_RESUMEFILE = 23;
            const int FLD_ETHNICGROUPLOOKUPID = 24;
            const int FLD_MEMBERID = 25;
            const int FLD_FUNCTIONAL_CATEGORY = 26;
            const int FLD_CREATEDATE = 27;
            const int FLD_UPDATEDATE = 28;
            const int FLD_FOLDERNAME = 29;
            const int FLD_PORTALACCESS = 30;
            const int FLD_CREATORID = 31;
            const int FLD_UPDATORID = 32;
            CompanyContact companyContact = new CompanyContact();

            companyContact.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            companyContact.CompanyId = reader.IsDBNull(FLD_COMPANYID) ? 0 : reader.GetInt32(FLD_COMPANYID);
            companyContact.IsPrimaryContact = reader.IsDBNull(FLD_ISPRIMARYCONTACT) ? false : reader.GetBoolean(FLD_ISPRIMARYCONTACT);
            companyContact.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString(FLD_TITLE);
            companyContact.NoBulkEmail = reader.IsDBNull(FLD_NOBULKEMAIL) ? false : reader.GetBoolean(FLD_NOBULKEMAIL);
            companyContact.Email = reader.IsDBNull(FLD_EMAIL) ? string.Empty : reader.GetString(FLD_EMAIL);
            companyContact.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            companyContact.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            companyContact.Address1 = reader.IsDBNull(FLD_ADDRESS1) ? string.Empty : reader.GetString(FLD_ADDRESS1);
            companyContact.Address2 = reader.IsDBNull(FLD_ADDRESS2) ? string.Empty : reader.GetString(FLD_ADDRESS2);
            companyContact.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
            companyContact.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
            companyContact.CountryId = reader.IsDBNull(FLD_COUNTRYID) ? 0 : reader.GetInt32(FLD_COUNTRYID);
            companyContact.ZipCode = reader.IsDBNull(FLD_ZIPCODE) ? string.Empty : reader.GetString(FLD_ZIPCODE);
            companyContact.OfficePhone = reader.IsDBNull(FLD_OFFICEPHONE) ? string.Empty : reader.GetString(FLD_OFFICEPHONE);
            companyContact.OfficePhoneExtension = reader.IsDBNull(FLD_OFFICEPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_OFFICEPHONEEXTENSION);
            companyContact.MobilePhone = reader.IsDBNull(FLD_MOBILEPHONE) ? string.Empty : reader.GetString(FLD_MOBILEPHONE);
            companyContact.DirectNumber = reader.IsDBNull(FLD_DIRECTNUMBER) ? string.Empty : reader.GetString(FLD_DIRECTNUMBER);
            companyContact.DirectNumberExtension = reader.IsDBNull(FLD_DIRECTNUMBEREXTENSION) ? string.Empty : reader.GetString(FLD_DIRECTNUMBEREXTENSION);
            companyContact.ContactRemarks = reader.IsDBNull(FLD_CONTACTREMARKS) ? string.Empty : reader.GetString(FLD_CONTACTREMARKS);
            companyContact.Fax = reader.IsDBNull(FLD_FAX) ? string.Empty : reader.GetString(FLD_FAX);
            companyContact.IsOwner = reader.IsDBNull(FLD_ISOWNER) ? false : reader.GetBoolean(FLD_ISOWNER);
            companyContact.OwnershipPercentage = reader.IsDBNull(FLD_OWNERSHIPPERCENTAGE) ? 0 : reader.GetDecimal(FLD_OWNERSHIPPERCENTAGE);
            companyContact.ResumeFile = reader.IsDBNull(FLD_RESUMEFILE) ? string.Empty : reader.GetString(FLD_RESUMEFILE);
            companyContact.EthnicGroupLookupId = reader.IsDBNull(FLD_ETHNICGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_ETHNICGROUPLOOKUPID);
            companyContact.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            companyContact.DivisionLookupId = reader.IsDBNull(FLD_FUNCTIONAL_CATEGORY) ? 0 : reader.GetInt32(FLD_FUNCTIONAL_CATEGORY);
            companyContact.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? System.DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            companyContact.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? System.DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            companyContact.FolderName = reader.IsDBNull(FLD_FOLDERNAME) ? string.Empty : reader.GetString(FLD_FOLDERNAME);
            companyContact.PortalAccess = reader.IsDBNull(FLD_PORTALACCESS) ? false : reader.GetBoolean(FLD_PORTALACCESS);
            companyContact.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            companyContact.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            return companyContact;
        }
    }
}
