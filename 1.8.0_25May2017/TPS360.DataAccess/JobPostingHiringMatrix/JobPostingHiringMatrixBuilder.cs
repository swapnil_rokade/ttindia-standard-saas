﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingHiringMatrixBuilder : IEntityBuilder<JobPostingHiringMatrix>
    {
        IList<JobPostingHiringMatrix> IEntityBuilder<JobPostingHiringMatrix>.BuildEntities(IDataReader reader)
        {
            List<JobPostingHiringMatrix> jobPostingHiringMatrixs = new List<JobPostingHiringMatrix>();

            while (reader.Read())
            {
                jobPostingHiringMatrixs.Add(((IEntityBuilder<JobPostingHiringMatrix>)this).BuildEntity(reader));
            }

            return (jobPostingHiringMatrixs.Count > 0) ? jobPostingHiringMatrixs : null;
        }

        JobPostingHiringMatrix IEntityBuilder<JobPostingHiringMatrix>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_JOBTITLEDESCRIPTION = 1;
            const int FLD_JOBTITLEPERCENTAGE = 2;
            const int FLD_LOCATIONDESCRIPTION = 3;
            const int FLD_LOCATIONPERCENTAGE = 4;
            const int FLD_YEAROFEXPDESCRIPTION = 5;
            const int FLD_YEAROFEXPPERCENTAGE = 6;
            const int FLD_INDUSTRYEXPDESCRIPTION = 7;
            const int FLD_INDUSTRYEXPPERCENTAGE = 8;
            const int FLD_TECHNICALSKILLDESCRIPTION = 9;
            const int FLD_TECHNICALSKILLPERCENTAGE = 10;
            const int FLD_EDUCATIONDESCRIPTION = 11;
            const int FLD_EDUCATIONPERCENTAGE = 12;
            const int FLD_LEGALSTATUSDESCRIPTION = 13;
            const int FLD_LEGALSTATUSPERCENTAGE = 14;
            const int FLD_SALARYDESCRIPTION = 15;
            const int FLD_SALARYPERCENTAGE = 16;
            const int FLD_MINSCOREFORFIRSTINTERVIEW = 17;
            const int FLD_JOBPOSTINGID = 18;
            const int FLD_CREATORID = 19;
            const int FLD_UPDATORID = 20;
            const int FLD_CREATEDATE = 21;
            const int FLD_UPDATEDATE = 22;

            JobPostingHiringMatrix jobPostingHiringMatrix = new JobPostingHiringMatrix();

            jobPostingHiringMatrix.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            jobPostingHiringMatrix.JobTitleDescription = reader.IsDBNull(FLD_JOBTITLEDESCRIPTION) ? string.Empty : reader.GetString(FLD_JOBTITLEDESCRIPTION);
            jobPostingHiringMatrix.JobTitlePercentage = reader.IsDBNull(FLD_JOBTITLEPERCENTAGE) ? 0 : reader.GetInt32(FLD_JOBTITLEPERCENTAGE);
            jobPostingHiringMatrix.LocationDescription = reader.IsDBNull(FLD_LOCATIONDESCRIPTION) ? string.Empty : reader.GetString(FLD_LOCATIONDESCRIPTION);
            jobPostingHiringMatrix.LocationPercentage = reader.IsDBNull(FLD_LOCATIONPERCENTAGE) ? 0 : reader.GetInt32(FLD_LOCATIONPERCENTAGE);
            jobPostingHiringMatrix.YearOfExpDescription = reader.IsDBNull(FLD_YEAROFEXPDESCRIPTION) ? string.Empty : reader.GetString(FLD_YEAROFEXPDESCRIPTION);
            jobPostingHiringMatrix.YearOfExpPercentage = reader.IsDBNull(FLD_YEAROFEXPPERCENTAGE) ? 0 : reader.GetInt32(FLD_YEAROFEXPPERCENTAGE);
            jobPostingHiringMatrix.IndustryExpDescription = reader.IsDBNull(FLD_INDUSTRYEXPDESCRIPTION) ? string.Empty : reader.GetString(FLD_INDUSTRYEXPDESCRIPTION);
            jobPostingHiringMatrix.IndustryExpPercentage = reader.IsDBNull(FLD_INDUSTRYEXPPERCENTAGE) ? 0 : reader.GetInt32(FLD_INDUSTRYEXPPERCENTAGE);
            jobPostingHiringMatrix.TechnicalSkillDescription = reader.IsDBNull(FLD_TECHNICALSKILLDESCRIPTION) ? string.Empty : reader.GetString(FLD_TECHNICALSKILLDESCRIPTION);
            jobPostingHiringMatrix.TechnicalSkillPercentage = reader.IsDBNull(FLD_TECHNICALSKILLPERCENTAGE) ? 0 : reader.GetInt32(FLD_TECHNICALSKILLPERCENTAGE);
            jobPostingHiringMatrix.EducationDescription = reader.IsDBNull(FLD_EDUCATIONDESCRIPTION) ? string.Empty : reader.GetString(FLD_EDUCATIONDESCRIPTION);
            jobPostingHiringMatrix.EducationPercentage = reader.IsDBNull(FLD_EDUCATIONPERCENTAGE) ? 0 : reader.GetInt32(FLD_EDUCATIONPERCENTAGE);
            jobPostingHiringMatrix.LegalStatusDescription = reader.IsDBNull(FLD_LEGALSTATUSDESCRIPTION) ? string.Empty : reader.GetString(FLD_LEGALSTATUSDESCRIPTION);
            jobPostingHiringMatrix.LegalStatusPercentage = reader.IsDBNull(FLD_LEGALSTATUSPERCENTAGE) ? 0 : reader.GetInt32(FLD_LEGALSTATUSPERCENTAGE);
            jobPostingHiringMatrix.SalaryDescription = reader.IsDBNull(FLD_SALARYDESCRIPTION) ? string.Empty : reader.GetString(FLD_SALARYDESCRIPTION);
            jobPostingHiringMatrix.SalaryPercentage = reader.IsDBNull(FLD_SALARYPERCENTAGE) ? 0 : reader.GetInt32(FLD_SALARYPERCENTAGE);
            jobPostingHiringMatrix.MinScoreForFirstInterview = reader.IsDBNull(FLD_MINSCOREFORFIRSTINTERVIEW) ? 0 : reader.GetInt32(FLD_MINSCOREFORFIRSTINTERVIEW);
            jobPostingHiringMatrix.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            jobPostingHiringMatrix.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            jobPostingHiringMatrix.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            jobPostingHiringMatrix.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            jobPostingHiringMatrix.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return jobPostingHiringMatrix;
        }
    }
}