﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingHiringMatrixDataAccess : BaseDataAccess, IJobPostingHiringMatrixDataAccess
    {
        #region Constructors

        public JobPostingHiringMatrixDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<JobPostingHiringMatrix> CreateEntityBuilder<JobPostingHiringMatrix>()
        {
            return (new JobPostingHiringMatrixBuilder()) as IEntityBuilder<JobPostingHiringMatrix>;
        }

        #endregion

        #region  Methods

        JobPostingHiringMatrix IJobPostingHiringMatrixDataAccess.Add(JobPostingHiringMatrix jobPostingHiringMatrix)
        {
            const string SP = "dbo.JobPostingHiringMatrix_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobTitleDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.JobTitleDescription));
                Database.AddInParameter(cmd, "@JobTitlePercentage", DbType.Int32, jobPostingHiringMatrix.JobTitlePercentage);
                Database.AddInParameter(cmd, "@LocationDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.LocationDescription));
                Database.AddInParameter(cmd, "@LocationPercentage", DbType.Int32, jobPostingHiringMatrix.LocationPercentage);
                Database.AddInParameter(cmd, "@YearOfExpDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.YearOfExpDescription));
                Database.AddInParameter(cmd, "@YearOfExpPercentage", DbType.Int32, jobPostingHiringMatrix.YearOfExpPercentage);
                Database.AddInParameter(cmd, "@IndustryExpDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.IndustryExpDescription));
                Database.AddInParameter(cmd, "@IndustryExpPercentage", DbType.Int32, jobPostingHiringMatrix.IndustryExpPercentage);
                Database.AddInParameter(cmd, "@TechnicalSkillDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.TechnicalSkillDescription));
                Database.AddInParameter(cmd, "@TechnicalSkillPercentage", DbType.Int32, jobPostingHiringMatrix.TechnicalSkillPercentage);
                Database.AddInParameter(cmd, "@EducationDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.EducationDescription));
                Database.AddInParameter(cmd, "@EducationPercentage", DbType.Int32, jobPostingHiringMatrix.EducationPercentage);
                Database.AddInParameter(cmd, "@LegalStatusDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.LegalStatusDescription));
                Database.AddInParameter(cmd, "@LegalStatusPercentage", DbType.Int32, jobPostingHiringMatrix.LegalStatusPercentage);
                Database.AddInParameter(cmd, "@SalaryDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.SalaryDescription));
                Database.AddInParameter(cmd, "@SalaryPercentage", DbType.Int32, jobPostingHiringMatrix.SalaryPercentage);
                Database.AddInParameter(cmd, "@MinScoreForFirstInterview", DbType.Int32, jobPostingHiringMatrix.MinScoreForFirstInterview);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingHiringMatrix.JobPostingId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, jobPostingHiringMatrix.CreatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPostingHiringMatrix = CreateEntityBuilder<JobPostingHiringMatrix>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPostingHiringMatrix = null;
                    }
                }

                if (jobPostingHiringMatrix == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting hiring matrix already exists. Please specify another job posting hiring matrix.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this job posting hiring matrix.");
                            }
                    }
                }

                return jobPostingHiringMatrix;
            }
        }

        JobPostingHiringMatrix IJobPostingHiringMatrixDataAccess.Update(JobPostingHiringMatrix jobPostingHiringMatrix)
        {
            const string SP = "dbo.JobPostingHiringMatrix_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, jobPostingHiringMatrix.Id);
                Database.AddInParameter(cmd, "@JobTitleDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.JobTitleDescription));
                Database.AddInParameter(cmd, "@JobTitlePercentage", DbType.Int32, jobPostingHiringMatrix.JobTitlePercentage);
                Database.AddInParameter(cmd, "@LocationDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.LocationDescription));
                Database.AddInParameter(cmd, "@LocationPercentage", DbType.Int32, jobPostingHiringMatrix.LocationPercentage);
                Database.AddInParameter(cmd, "@YearOfExpDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.YearOfExpDescription));
                Database.AddInParameter(cmd, "@YearOfExpPercentage", DbType.Int32, jobPostingHiringMatrix.YearOfExpPercentage);
                Database.AddInParameter(cmd, "@IndustryExpDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.IndustryExpDescription));
                Database.AddInParameter(cmd, "@IndustryExpPercentage", DbType.Int32, jobPostingHiringMatrix.IndustryExpPercentage);
                Database.AddInParameter(cmd, "@TechnicalSkillDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.TechnicalSkillDescription));
                Database.AddInParameter(cmd, "@TechnicalSkillPercentage", DbType.Int32, jobPostingHiringMatrix.TechnicalSkillPercentage);
                Database.AddInParameter(cmd, "@EducationDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.EducationDescription));
                Database.AddInParameter(cmd, "@EducationPercentage", DbType.Int32, jobPostingHiringMatrix.EducationPercentage);
                Database.AddInParameter(cmd, "@LegalStatusDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.LegalStatusDescription));
                Database.AddInParameter(cmd, "@LegalStatusPercentage", DbType.Int32, jobPostingHiringMatrix.LegalStatusPercentage);
                Database.AddInParameter(cmd, "@SalaryDescription", DbType.AnsiString, StringHelper.Convert(jobPostingHiringMatrix.SalaryDescription));
                Database.AddInParameter(cmd, "@SalaryPercentage", DbType.Int32, jobPostingHiringMatrix.SalaryPercentage);
                Database.AddInParameter(cmd, "@MinScoreForFirstInterview", DbType.Int32, jobPostingHiringMatrix.MinScoreForFirstInterview);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, jobPostingHiringMatrix.UpdatorId);               

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPostingHiringMatrix = CreateEntityBuilder<JobPostingHiringMatrix>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPostingHiringMatrix = null;
                    }
                }

                if (jobPostingHiringMatrix == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting hiring matrix already exists. Please specify another job posting hiring matrix.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this job posting hiring matrix.");
                            }
                    }
                }

                return jobPostingHiringMatrix;
            }
        }

        JobPostingHiringMatrix IJobPostingHiringMatrixDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingHiringMatrix_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingHiringMatrix>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<JobPostingHiringMatrix> IJobPostingHiringMatrixDataAccess.GetAll()
        {
            const string SP = "dbo.JobPostingHiringMatrix_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingHiringMatrix>().BuildEntities(reader);
                }
            }
        }

        bool IJobPostingHiringMatrixDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingHiringMatrix_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting hiring matrix which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this job posting hiring matrix.");
                        }
                }
            }
        }

        JobPostingHiringMatrix IJobPostingHiringMatrixDataAccess.GetByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "dbo.JobPostingHiringMatrix_GetJobPostingHiringMatrixByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingHiringMatrix>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        bool IJobPostingHiringMatrixDataAccess.DeleteByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "dbo.JobPostingHiringMatrix_DeleteJobPostingHiringMatrixByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting hiring matrix which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this job posting hiring matrix.");
                        }
                }
            }
        }

        #endregion
    }
}