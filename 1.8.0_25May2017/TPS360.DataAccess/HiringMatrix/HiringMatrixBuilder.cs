﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: HiringMatrixBuilder.cs
    Description: This page is used for building entities related to hiring matrix.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Apr-20-2009         Shivanand           Defect # 10361; New constant added "FLD_ROLENAME" & used to assign value to RoleName.
 *  0.2            23/June/2016        pravin khot         addded- FLD_SOURCENAME
    ------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class HiringMatrixBuilder : IEntityBuilder<HiringMatrix>
    {
        IList<HiringMatrix> IEntityBuilder<HiringMatrix> .BuildEntities(IDataReader reader)
        {
            List<HiringMatrix> HiringMatrixs = new List<HiringMatrix>();

            while (reader.Read())
            {
                HiringMatrixs.Add(((IEntityBuilder<HiringMatrix> )this).BuildEntity(reader));
            }

            return (HiringMatrixs.Count > 0) ? HiringMatrixs : null;
        }

        HiringMatrix IEntityBuilder<HiringMatrix>.BuildEntity(IDataReader reader)
        {
            if (reader.FieldCount == 17) //if (reader.FieldCount == 13) modify by pravin khot on 23/June/2016
            {
                HiringMatrix hiringMatrix = new HiringMatrix();
                const int FLD_MEMBERID = 0;
                const int FLD_CANDIDATENAME = 1;
                const int FLD_PRIMARYEMAIL = 2;
                const int FLD_TOTALEXPYEARS = 3;
                const int FLD_CURRENTPOSITION = 4;
                const int FLD_MATCHINGPERCENTAGE = 5;
                const int FLD_CURRENTLEVEL = 6;
                const int FLD_HASUPCOMMINGINTERVIEW = 7;
                const int FLD_HASSUBMISSIOMDETAILS=8;
                const int FLD_HASOFFERDETAILS=9;
                const int FLD_HASJOININGDETAILS=10;
                const int FLD_DATEADDED = 11;
                const int FLD_HASOFFERDECLINE = 12;
                const int FLD_SOURCENAME = 13;//ADDED BY PRAVIN KHOT  23/June/2016
                const int FLD_CurrentCity = 14;//ADDED BY PRAVIN KHOT  23/June/2016
                const int FLD_CurrentState = 15;//ADDED BY PRAVIN KHOT  23/June/2016
                const int FLD_PrimaryManager = 16;//ADDED BY Sumit Sonawane 22/Sep/2017

                hiringMatrix.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
                hiringMatrix.FirstName = reader.IsDBNull(FLD_CANDIDATENAME) ? string .Empty  : reader.GetString (FLD_CANDIDATENAME );
                hiringMatrix.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
                hiringMatrix.TotalExperienceYears = reader.IsDBNull(FLD_TOTALEXPYEARS) ? string.Empty : reader.GetString(FLD_TOTALEXPYEARS);
                hiringMatrix.CurrentPosition  = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
                hiringMatrix.MatchingPercentage = reader.IsDBNull(FLD_MATCHINGPERCENTAGE) ? 0 : reader.GetDecimal(FLD_MATCHINGPERCENTAGE);
                hiringMatrix.CurrentLevel = reader.IsDBNull(FLD_CURRENTLEVEL) ? 0 : reader.GetInt32(FLD_CURRENTLEVEL);
                hiringMatrix.HasUpcommingInterview = reader.IsDBNull(FLD_HASUPCOMMINGINTERVIEW) ? false : reader.GetBoolean(FLD_HASUPCOMMINGINTERVIEW);
                hiringMatrix.HasSubmissionDetails = reader.IsDBNull(FLD_HASSUBMISSIOMDETAILS) ? false : reader.GetBoolean(FLD_HASSUBMISSIOMDETAILS);
                hiringMatrix.HasOfferDetails = reader.IsDBNull(FLD_HASOFFERDETAILS) ? false : reader.GetBoolean(FLD_HASOFFERDETAILS);
                hiringMatrix.HasJoiningDetails = reader.IsDBNull(FLD_HASJOININGDETAILS) ? false : reader.GetBoolean(FLD_HASJOININGDETAILS);
                hiringMatrix.AddedOn = reader.IsDBNull(FLD_DATEADDED) ? DateTime.MinValue : reader.GetDateTime(FLD_DATEADDED);
                hiringMatrix.HasOfferDeclineDetails = reader.IsDBNull(FLD_HASOFFERDECLINE) ? false : reader.GetBoolean(FLD_HASOFFERDECLINE);
                //*************Added by pravin khot on 23/June/2016
                if (reader.FieldCount > FLD_SOURCENAME)
                {
                    hiringMatrix.SourceName  = reader.IsDBNull(FLD_SOURCENAME) ? string.Empty : reader.GetString(FLD_SOURCENAME);
                }
                if (reader.FieldCount > FLD_CurrentCity)
                {
                    hiringMatrix.CurrentCity = reader.IsDBNull(FLD_CurrentCity) ? string.Empty : reader.GetString(FLD_CurrentCity);
                }
                if (reader.FieldCount > FLD_CurrentState)
                {
                    hiringMatrix.CurrentState = reader.IsDBNull(FLD_CurrentState) ? string.Empty : reader.GetString(FLD_CurrentState);
                }
                if (reader.FieldCount > FLD_PrimaryManager)
                {
                    hiringMatrix.PrimaryManager = reader.IsDBNull(FLD_PrimaryManager) ? string.Empty : reader.GetString(FLD_PrimaryManager);
                }
                //******************END*************************
                return hiringMatrix;
                // hiringMatrix .FirstName 
            }

            if (reader.FieldCount == 23)
            {
                HiringMatrix hiringMatrix = new HiringMatrix();
                const int FLD_MemberID = 0;
                const int FLD_MemberCreatorID = 1;
                const int FLD_FirstName = 2;
                const int FLD_MiddleName = 3;
                const int FLD_LastName = 4;
                const int FLD_CandidateName = 5;
                const int FLD_PrimaryEmail = 6;
                const int FLD_CellPhone = 7;
                const int FLD_PermanentCity = 8;
                const int FLD_PermanentState = 9;
                const int FLD_PermanentCountry = 10;
                const int FLD_CurrentPosition = 11;
                const int FLD_TotalExperienceYears = 12;
                const int FLD_NoticePeriod = 13;
                //const int FLD_Remarks = 14;
                const int FLD_JobPostingID = 14;
                const int FLD_JobPostingCreatorID = 15;
                const int FLD_RequisitionStatusID = 16;
                const int FLD_RequisitionStatus = 17;
                const int FLD_Requisition = 18;
                const int FLD_SelectionStepLookupId = 19;
                const int FLD_HiringLevelStatus = 20;
                const int FLD_Source = 21;
                const int FLD_TeamMemberId = 22;

                hiringMatrix.MemberId = reader.IsDBNull(FLD_MemberID) ? 0 : reader.GetInt32(FLD_MemberID);
                hiringMatrix.CreatorId = reader.IsDBNull(FLD_MemberCreatorID) ? 0 : reader.GetInt32(FLD_MemberCreatorID);
                hiringMatrix.FirstName = reader.IsDBNull(FLD_FirstName) ? string.Empty : reader.GetString(FLD_FirstName);
                hiringMatrix.MiddleName = reader.IsDBNull(FLD_MiddleName) ? string.Empty : reader.GetString(FLD_MiddleName);
                hiringMatrix.LastName = reader.IsDBNull(FLD_LastName) ? string.Empty : reader.GetString(FLD_LastName);
                hiringMatrix.CandidateName = reader.IsDBNull(FLD_CandidateName) ? string.Empty : reader.GetString(FLD_CandidateName);
                hiringMatrix.PrimaryEmail = reader.IsDBNull(FLD_PrimaryEmail) ? string.Empty : reader.GetString(FLD_PrimaryEmail);
                hiringMatrix.CellPhone = reader.IsDBNull(FLD_CellPhone) ? string.Empty : reader.GetString(FLD_CellPhone);
                hiringMatrix.CurrentCity = reader.IsDBNull(FLD_PermanentCity) ? string.Empty : reader.GetString(FLD_PermanentCity);
                hiringMatrix.CurrentState = reader.IsDBNull(FLD_PermanentState) ? string.Empty : reader.GetString(FLD_PermanentState);
                hiringMatrix.PermanentCountry = reader.IsDBNull(FLD_PermanentCountry) ? string.Empty : reader.GetString(FLD_PermanentCountry);
                hiringMatrix.HighestDegree = reader.IsDBNull(FLD_CurrentPosition) ? string.Empty : reader.GetString(FLD_CurrentPosition);
                hiringMatrix.TotalExperienceYears = reader.IsDBNull(FLD_TotalExperienceYears) ? string.Empty : reader.GetString(FLD_TotalExperienceYears);
                hiringMatrix.Skills = reader.IsDBNull(FLD_NoticePeriod) ? string.Empty : reader.GetString(FLD_NoticePeriod);
                //hiringMatrix.Remarks = reader.IsDBNull(FLD_Remarks) ? string.Empty : reader.GetString(FLD_Remarks);
                hiringMatrix.JobPostingID = reader.IsDBNull(FLD_JobPostingID) ? 0 : reader.GetInt32(FLD_JobPostingID);
                hiringMatrix.JobPostingCreatorID = reader.IsDBNull(FLD_JobPostingCreatorID) ? 0 : reader.GetInt32(FLD_JobPostingCreatorID);
                hiringMatrix.RequisitionStatusID = reader.IsDBNull(FLD_RequisitionStatusID) ? 0 : reader.GetInt32(FLD_RequisitionStatusID);
                hiringMatrix.RequisitionStatus = reader.IsDBNull(FLD_RequisitionStatus) ? string.Empty : reader.GetString(FLD_RequisitionStatus);
                hiringMatrix.Requisition = reader.IsDBNull(FLD_Requisition) ? string.Empty : reader.GetString(FLD_Requisition);
                hiringMatrix.SelectionStepLookupId = reader.IsDBNull(FLD_SelectionStepLookupId) ? 0 : reader.GetInt32(FLD_SelectionStepLookupId);
                hiringMatrix.HiringLevelStatus = reader.IsDBNull(FLD_HiringLevelStatus) ? string.Empty : reader.GetString(FLD_HiringLevelStatus);
                hiringMatrix.SourceName = reader.IsDBNull(FLD_Source) ? string.Empty : reader.GetString(FLD_Source);
                hiringMatrix.TeamMemberId = reader.IsDBNull(FLD_TeamMemberId) ? 0 : reader.GetInt32(FLD_TeamMemberId);

                return hiringMatrix;
            }

            else if (reader.FieldCount == 24)
            {
                HiringMatrix hiringMatrix = new HiringMatrix();
                const int FLD_MemberID = 0;
                const int FLD_MemberCreatorID = 1;
                const int FLD_FirstName = 2;
                const int FLD_MiddleName = 3;
                const int FLD_LastName = 4;
                const int FLD_CandidateName = 5;
                const int FLD_PrimaryEmail = 6;
                const int FLD_CellPhone = 7;
                const int FLD_PermanentCity = 8;
                const int FLD_PermanentState = 9;
                const int FLD_PermanentCountry = 10;
                const int FLD_CurrentPosition = 11;
                const int FLD_TotalExperienceYears = 12;
                const int FLD_NoticePeriod = 13;
                //const int FLD_Skills = 13;
                //const int FLD_Remarks = 14;
                const int FLD_JobPostingID = 14;
                const int FLD_JobPostingCreatorID = 15;
                const int FLD_RequisitionStatusID = 16;
                const int FLD_RequisitionStatus = 17;
                const int FLD_Requisition = 18;
                const int FLD_SelectionStepLookupId = 19;
                const int FLD_HiringLevelStatus = 20;
                const int FLD_Source = 21;
                const int FLD_TeamMemberId = 22;
                const int FLD_Teamrn = 23;

                hiringMatrix.MemberId = reader.IsDBNull(FLD_MemberID) ? 0 : reader.GetInt32(FLD_MemberID);
                hiringMatrix.CreatorId = reader.IsDBNull(FLD_MemberCreatorID) ? 0 : reader.GetInt32(FLD_MemberCreatorID);
                hiringMatrix.FirstName = reader.IsDBNull(FLD_FirstName) ? string.Empty : reader.GetString(FLD_FirstName);
                hiringMatrix.MiddleName = reader.IsDBNull(FLD_MiddleName) ? string.Empty : reader.GetString(FLD_MiddleName);
                hiringMatrix.LastName = reader.IsDBNull(FLD_LastName) ? string.Empty : reader.GetString(FLD_LastName);
                hiringMatrix.CandidateName = reader.IsDBNull(FLD_CandidateName) ? string.Empty : reader.GetString(FLD_CandidateName);
                hiringMatrix.PrimaryEmail = reader.IsDBNull(FLD_PrimaryEmail) ? string.Empty : reader.GetString(FLD_PrimaryEmail);
                hiringMatrix.CellPhone = reader.IsDBNull(FLD_CellPhone) ? string.Empty : reader.GetString(FLD_CellPhone);
                hiringMatrix.CurrentCity = reader.IsDBNull(FLD_PermanentCity) ? string.Empty : reader.GetString(FLD_PermanentCity);
                hiringMatrix.CurrentState = reader.IsDBNull(FLD_PermanentState) ? string.Empty : reader.GetString(FLD_PermanentState);
                hiringMatrix.PermanentCountry = reader.IsDBNull(FLD_PermanentCountry) ? string.Empty : reader.GetString(FLD_PermanentCountry);
                hiringMatrix.CurrentPosition = reader.IsDBNull(FLD_CurrentPosition) ? string.Empty : reader.GetString(FLD_CurrentPosition);
                hiringMatrix.TotalExperienceYears = reader.IsDBNull(FLD_TotalExperienceYears) ? string.Empty : reader.GetString(FLD_TotalExperienceYears);
                hiringMatrix.NoticePeriod = reader.IsDBNull(FLD_NoticePeriod) ? string.Empty : reader.GetString(FLD_NoticePeriod);
                //hiringMatrix.Remarks = reader.IsDBNull(FLD_Remarks) ? string.Empty : reader.GetString(FLD_Remarks);
                hiringMatrix.JobPostingID = reader.IsDBNull(FLD_JobPostingID) ? 0 : reader.GetInt32(FLD_JobPostingID);
                hiringMatrix.JobPostingCreatorID = reader.IsDBNull(FLD_JobPostingCreatorID) ? 0 : reader.GetInt32(FLD_JobPostingCreatorID);
                hiringMatrix.RequisitionStatusID = reader.IsDBNull(FLD_RequisitionStatusID) ? 0 : reader.GetInt32(FLD_RequisitionStatusID);
                hiringMatrix.RequisitionStatus = reader.IsDBNull(FLD_RequisitionStatus) ? string.Empty : reader.GetString(FLD_RequisitionStatus);
                hiringMatrix.Requisition = reader.IsDBNull(FLD_Requisition) ? string.Empty : reader.GetString(FLD_Requisition);
                hiringMatrix.SelectionStepLookupId = reader.IsDBNull(FLD_SelectionStepLookupId) ? 0 : reader.GetInt32(FLD_SelectionStepLookupId);
                hiringMatrix.HiringLevelStatus = reader.IsDBNull(FLD_HiringLevelStatus) ? string.Empty : reader.GetString(FLD_HiringLevelStatus);
                hiringMatrix.SourceName = reader.IsDBNull(FLD_Source) ? string.Empty : reader.GetString(FLD_Source);
                hiringMatrix.TeamMemberId = reader.IsDBNull(FLD_TeamMemberId) ? 0 : reader.GetInt32(FLD_TeamMemberId);
                hiringMatrix.Teamrn = reader.IsDBNull(FLD_Teamrn) ? 0 : reader.GetInt32(FLD_Teamrn);

                return hiringMatrix;
            }


            else
            {
                const int FLD_MEMBERID = 0;
                const int FLD_FIRSTNAME = 1;
                const int FLD_MIDDLENAME = 2;
                const int FLD_LASTNAME = 3;
                const int FLD_CELLPHONE = 4;
                const int FLD_PRIMARYEMAIL = 5;
                const int FLD_TOTALEXPERIENCEYEARS = 6;
                const int FLD_UPDATEDATE = 7;
                const int FLD_CURRENTPOSITION = 8;
                const int FLD_LASTEMPLOYER = 9;
                const int FLD_AVAILABILITY = 10;
                const int FLD_AVAILABLEFROM = 11;
                const int FLD_WORKSCHEDULE = 12;
                const int FLD_CURRENTCITY = 13;
                const int FLD_CURRENTSTATE = 14;
                const int FLD_MEMBERTYPE = 15;
                const int FLD_WORKAUTHORIZATION = 16;
                const int FLD_OBJECTIVE = 17;
                const int FLD_REMARKS = 18;
                const int FLD_CURRENTYEARLYCURRENCY = 19;
                const int FLD_EXPECTEDYEARLYCURRENCY = 20;
                const int FLD_CURRENTYEARLYRATE = 21;
                const int FLD_EXPECTEDYEARLYRATE = 22;
                const int FLD_EXPECTEDYEARLYMAXRATE = 23;
                const int FLD_PRIMARYMANAGERNAME = 24;
                const int FLD_SKILLS = 25;
                const int FLD_HIGHESTDEGREE = 26;
                const int FLD_MATCHINGPERCENTAGE = 27;
                const int FLD_AVAILABILITYTEXT = 28;
                const int FLD_CURRENTSALARYPAYCYCLE = 29;
                const int FLD_EXPECTEDSALARYPAYCYCLE = 30;
                const int FLD_WORKSCHEDULELOOKUPID = 31;
                const int FLD_WORKSTATUSID = 32;
                const int FLD_EDUCATIONLEVELIDS = 33;
                const int FLD_CURRENTSTATEID = 34;
                const int FLD_STATUSID = 35;
                const int FLD_AddedBy = 36;
                const int FLD_ADDEDON = 37;
                const int FLD_HIRINGNOTE = 38;
                const int FLD_NOTEUPDATORNAME = 39;
                const int FLD_NOTEUPDATEDATE = 40;
                const int FLD_SOURCEDID = 41;
                const int FLD_SOURCENAME = 42;
                const int FLD_CURRENTLEVEL = 43;

                HiringMatrix hiringMatrix = new HiringMatrix();



                hiringMatrix.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
                hiringMatrix.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
                hiringMatrix.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
                hiringMatrix.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
                hiringMatrix.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
                hiringMatrix.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
                hiringMatrix.TotalExperienceYears = reader.IsDBNull(FLD_TOTALEXPERIENCEYEARS) ? string.Empty : reader.GetString(FLD_TOTALEXPERIENCEYEARS);
                hiringMatrix.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
                hiringMatrix.CurrentPosition = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
                hiringMatrix.LastEmployer = reader.IsDBNull(FLD_LASTEMPLOYER) ? string.Empty : reader.GetString(FLD_LASTEMPLOYER);
                hiringMatrix.Availability = reader.IsDBNull(FLD_AVAILABILITY) ? 0 : reader.GetInt32(FLD_AVAILABILITY);
                hiringMatrix.AvailableFrom = reader.IsDBNull(FLD_AVAILABLEFROM) ? DateTime.MinValue : reader.GetDateTime(FLD_AVAILABLEFROM);
                hiringMatrix.WorkSchedule = reader.IsDBNull(FLD_WORKSCHEDULE) ? string.Empty : reader.GetString(FLD_WORKSCHEDULE);
                hiringMatrix.CurrentCity = reader.IsDBNull(FLD_CURRENTCITY) ? string.Empty : reader.GetString(FLD_CURRENTCITY);
                hiringMatrix.CurrentState = reader.IsDBNull(FLD_CURRENTSTATE) ? string.Empty : reader.GetString(FLD_CURRENTSTATE);
                hiringMatrix.MemberType = reader.IsDBNull(FLD_MEMBERTYPE) ? string.Empty : reader.GetString(FLD_MEMBERTYPE);
                hiringMatrix.WorkAuthorization = reader.IsDBNull(FLD_WORKAUTHORIZATION) ? string.Empty : reader.GetString(FLD_WORKAUTHORIZATION);
                hiringMatrix.Objective = reader.IsDBNull(FLD_OBJECTIVE) ? string.Empty : reader.GetString(FLD_OBJECTIVE);
                hiringMatrix.Remarks = reader.IsDBNull(FLD_REMARKS) ? string.Empty : reader.GetString(FLD_REMARKS);
                hiringMatrix.CurrentYearlyCurrency = reader.IsDBNull(FLD_CURRENTYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTYEARLYCURRENCY);
                hiringMatrix.ExpectedYearlyCurrency = reader.IsDBNull(FLD_EXPECTEDYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDYEARLYCURRENCY);
                hiringMatrix.CurrentYearlyRate = reader.IsDBNull(FLD_CURRENTYEARLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTYEARLYRATE);
                hiringMatrix.ExpectedYearlyRate = reader.IsDBNull(FLD_EXPECTEDYEARLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDYEARLYRATE);
                hiringMatrix.ExpectedYearlyMaxRate = reader.IsDBNull(FLD_EXPECTEDYEARLYMAXRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDYEARLYMAXRATE);
                hiringMatrix.PrimaryManager = reader.IsDBNull(FLD_PRIMARYMANAGERNAME) ? string.Empty : reader.GetString(FLD_PRIMARYMANAGERNAME);
                hiringMatrix.Skills = reader.IsDBNull(FLD_SKILLS) ? string.Empty : reader.GetString(FLD_SKILLS);
                hiringMatrix.HighestDegree = reader.IsDBNull(FLD_HIGHESTDEGREE) ? string.Empty : reader.GetString(FLD_HIGHESTDEGREE);
                hiringMatrix.MatchingPercentage = reader.IsDBNull(FLD_MATCHINGPERCENTAGE) ? 0 : reader.GetDecimal(FLD_MATCHINGPERCENTAGE);
                hiringMatrix.AvailabilityText = reader.IsDBNull(FLD_AVAILABILITYTEXT) ? string.Empty : reader.GetString(FLD_AVAILABILITYTEXT);
                hiringMatrix.CurrentSalaryPayCycle = reader.IsDBNull(FLD_CURRENTSALARYPAYCYCLE) ? 0 : reader.GetInt32(FLD_CURRENTSALARYPAYCYCLE);
                hiringMatrix.ExpectedSalaryPayCycle = reader.IsDBNull(FLD_EXPECTEDSALARYPAYCYCLE) ? 0 : reader.GetInt32(FLD_EXPECTEDSALARYPAYCYCLE);
                hiringMatrix.WorkScheduleLookupId = reader.IsDBNull(FLD_WORKSCHEDULELOOKUPID) ? 0 : reader.GetInt32(FLD_WORKSCHEDULELOOKUPID);
                hiringMatrix.WorkStatusID = reader.IsDBNull(FLD_WORKSTATUSID) ? 0 : reader.GetInt32(FLD_WORKSTATUSID);
                hiringMatrix.EducationLevelIDs = reader.IsDBNull(FLD_EDUCATIONLEVELIDS) ? string.Empty : reader.GetString(FLD_EDUCATIONLEVELIDS);
                hiringMatrix.CurrentStateID = reader.IsDBNull(FLD_CURRENTSTATEID) ? 0 : reader.GetInt32(FLD_CURRENTSTATEID);
                hiringMatrix.StatusId = reader.IsDBNull(FLD_STATUSID) ? 0 : reader.GetInt32(FLD_STATUSID);
                hiringMatrix.AddedBy = reader.IsDBNull(FLD_AddedBy) ? string.Empty : reader.GetString(FLD_AddedBy);
                hiringMatrix.AddedOn = reader.IsDBNull(FLD_ADDEDON) ? DateTime.MinValue : reader.GetDateTime(FLD_ADDEDON);
                hiringMatrix.HiringNote = reader.IsDBNull(FLD_HIRINGNOTE) ? string.Empty : reader.GetString(FLD_HIRINGNOTE);
                hiringMatrix.NoteUpdatorName = reader.IsDBNull(FLD_NOTEUPDATORNAME) ? string.Empty : reader.GetString(FLD_NOTEUPDATORNAME);
                hiringMatrix.NoteUpdatedDate = reader.IsDBNull(FLD_NOTEUPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_NOTEUPDATEDATE);
                hiringMatrix.SourceID = reader.IsDBNull(FLD_SOURCEDID) ? 0 : reader.GetInt32(FLD_SOURCEDID);
                hiringMatrix.SourceName = reader.IsDBNull(FLD_SOURCENAME) ? string.Empty : reader.GetString(FLD_SOURCENAME);
                hiringMatrix.CurrentLevel = reader.IsDBNull(FLD_CURRENTLEVEL) ? 0 : reader.GetInt32(FLD_CURRENTLEVEL);
                return hiringMatrix;
            }
        }
    }
}

