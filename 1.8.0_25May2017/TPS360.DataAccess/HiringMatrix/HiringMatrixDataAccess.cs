﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class HiringMatrixDataAccess : BaseDataAccess, IHiringMatrixDataAccess
    {
        #region Constructors

        public HiringMatrixDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<HiringMatrix> CreateEntityBuilder<HiringMatrix>()
        {
            return (new HiringMatrixBuilder()) as IEntityBuilder<HiringMatrix>;
        }

        #endregion

        #region  Methods
        //-------added new filter option by Sumit Sonawane on 19/Sep/2017------
        PagedResponse<HiringMatrix> IHiringMatrixDataAccess.MinGetPaged(PagedRequest request)
        {
            const string SP = "dbo.HiringMatrix_minGetPaged";
            string whereClause = string.Empty;
            string jobpostingid = "";
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobPostingId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJC].[JobPostingId]");
                            sb.Append(" = ");
                            sb.Append(value);
                            jobpostingid = value;
                        }
                        else if (StringHelper.IsEqual(column, "CandidateName"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                char[] delim = { ' ' };
                                string[] valueList = value.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                                if (valueList.Length == 2)
                                {
                                    sb.Append(" ([C].[FirstName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[0]);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[LastName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[1]);
                                    sb.Append("%') ");
                                }
                                else if (valueList.Length == 3)
                                {
                                    sb.Append(" ([C].[FirstName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[0]);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[LastName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[1]);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[MiddleName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[2]);
                                    sb.Append("%') ");
                                }
                                else
                                {
                                    sb.Append(" ([C].[FirstName]");
                                    sb.Append(" like '%");
                                    sb.Append(value);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[LastName]");
                                    sb.Append(" like '%");
                                    sb.Append(value);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[MiddleName]");
                                    sb.Append(" like '%");
                                    sb.Append(value);
                                    sb.Append("%') ");
                                }

                            }
                        }

                        else if (StringHelper.IsEqual(column, "CandidateEmail"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append("( [C].[PrimaryEmail]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                                sb.Append(" or ");
                                sb.Append("[C].[AlternateEmail]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                                sb.Append(") ");
                            }
                        }

                        else if (StringHelper.IsEqual(column, "CandidateID"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[Id] =");
                            sb.Append(value);
                        }

                        else if (StringHelper.IsEqual(column, "SourceId"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[SourceLookupId]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }

                        else if (StringHelper.IsEqual(column, "PrimaryManager"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[CreatorId]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }
                       
                        else if (StringHelper.IsEqual(column, "SelectionStepLookupId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJCD].[SelectionStepLookupId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }                       
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "MatchingPercentage";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "Desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    StringHelper .Convert (jobpostingid ) 
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<HiringMatrix> response = new PagedResponse<HiringMatrix>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<HiringMatrix>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        //---------------------------------End--------------------------------

        PagedResponse<HiringMatrix> IHiringMatrixDataAccess.MinGetPagedAll(PagedRequest request)
        {
            const string SP = "dbo.HiringMatrix_minGetPagedAll";
            string whereClause = string.Empty;
            string jobpostingid = "";
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobPostingId"))
                        {
                            //if (sb.ToString() != String.Empty)
                            //{
                            //    sb.Append(" AND ");
                            //}
                            //sb.Append("[MJC].[JobPostingId]");
                            //sb.Append(" = ");
                            //sb.Append(value);
                            jobpostingid = value;
                        }
                        else if (StringHelper.IsEqual(column, "SelectionStepLookupId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJCD].[SelectionStepLookupId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "MatchingPercentage";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "Desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    StringHelper .Convert (jobpostingid ) 
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<HiringMatrix> response = new PagedResponse<HiringMatrix>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<HiringMatrix>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }


        PagedResponse<HiringMatrix> IHiringMatrixDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.HiringMatrix_GetPaged";
            string whereClause = string.Empty;
            string jobpostingid = "";
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobPostingId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJC].[JobPostingId]");
                            sb.Append(" = ");
                            sb.Append(value);
                            jobpostingid = value;
                        }
                        else if (StringHelper.IsEqual(column, "SelectionStepLookupId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJCD].[SelectionStepLookupId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "MatchingPercentage";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "Desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    StringHelper .Convert (jobpostingid ) 
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<HiringMatrix> response = new PagedResponse<HiringMatrix>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<HiringMatrix>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        ////////////////////////////Sumit Sonawane 22/07/2016///////////////////////////////////////////////////////////////////
        PagedResponse<HiringMatrix> IHiringMatrixDataAccess.GetPagedBucketView(PagedRequest request)
        {
            const string SP = "dbo.HiringMatrix_GetPagedBucketView";
            //const string SP = "dbo.HiringMatrix_GetPaged";
            string whereClause = string.Empty;
            string jobpostingid = "";
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobPostingId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJC].[JobPostingId]");
                            sb.Append(" = ");
                            sb.Append(value);
                            jobpostingid = value;
                        }

                        else if (StringHelper.IsEqual(column, "SelectionStepLookupId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[MJCD].[SelectionStepLookupId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "MatchingPercentage";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "Desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    StringHelper .Convert (jobpostingid ) 
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<HiringMatrix> response = new PagedResponse<HiringMatrix>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<HiringMatrix>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }


        ////////////////////////////Sumit Sonawane 22/07/2016///////////////////////////////////////////////////////////////////
        PagedResponse<HiringMatrix> IHiringMatrixDataAccess.BucketView(PagedRequest request)
        {
            const string SP = "dbo.BucketViewDataSource";
            //const string SP = "dbo.HiringMatrix_GetPaged";
            string whereClause = string.Empty;
            string jobpostingid = "0";
            string jobpostcreatorId = "0";
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobPostingId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[JobPostingId]");
                            sb.Append(" = ");
                            sb.Append(value);
                            jobpostingid = value;
                        }

                        else if (StringHelper.IsEqual(column, "TeamMemberId")) // changed JobPostingCreatorID to TeamMemberId
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[TeamMemberId]");  // changed JobPostingCreatorID to TeamMemberId
                            sb.Append(" = ");
                            sb.Append(value);
                            jobpostcreatorId = value;
                        }

                        else if (StringHelper.IsEqual(column, "MemberCreatorID"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[MemberCreatorID]");

                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }


                        else if (StringHelper.IsEqual(column, "SelectionStepLookupId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[SelectionStepLookupId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "MatchingPercentage";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "Desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {
													StringHelper.Convert(whereClause),													
                                                    StringHelper .Convert (jobpostingid ) ,
                                                    StringHelper .Convert (jobpostcreatorId )
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<HiringMatrix> response = new PagedResponse<HiringMatrix>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<HiringMatrix>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<HiringMatrix> IHiringMatrixDataAccess.BucketViewLoadAll(PagedRequest request)
        {
            const string SP = "dbo.BucketViewDataSourceLoadAll";
            //const string SP = "dbo.HiringMatrix_GetPaged";
            string whereClause = string.Empty;
            string jobpostcreatorId = "0";

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "TeamMemberId"))  // changed JobPostingCreatorID to TeamMemberId
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[TeamMemberId]"); // changed JobPostingCreatorID to TeamMemberId
                            sb.Append(" = ");
                            sb.Append(value);
                            jobpostcreatorId = value;
                        }

                        else if (StringHelper.IsEqual(column, "MemberCreatorID"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[MemberCreatorID]");

                                sb.Append(" = ");

                                sb.Append(value);
                            }
                        }


                        else if (StringHelper.IsEqual(column, "SelectionStepLookupId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[SelectionStepLookupId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "MatchingPercentage";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "Desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {StringHelper.Convert(whereClause),
                                                 StringHelper .Convert (jobpostcreatorId )
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<HiringMatrix> response = new PagedResponse<HiringMatrix>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<HiringMatrix>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        HiringMatrix IHiringMatrixDataAccess.GetByJobPostingIDandCandidateId(int JobPostingId, int CandidateId)
        {

            const string SP = "dbo.HiringMatrix_GetByCandidateIdAndJobPostingID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);
                Database.AddInParameter(cmd, "@CandidateId", DbType.Int32, CandidateId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<HiringMatrix>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        ////////////////////////////Sumit Sonawane 1/Mar/2017///////////////////////////////////////////////////////////////////
        HiringMatrix IHiringMatrixDataAccess.getMatrixLevelByJobPostingIDAndCandidateId(int JobPostingId, int cid)
        {

            const string SP = "dbo.getMatrixLevelByJobPostingIDAndCandidateId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);
                Database.AddInParameter(cmd, "@cid", DbType.Int32, cid);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    HiringMatrix rp = new HiringMatrix();
                    while (reader.Read())
                    {
                        rp.Id = reader.GetInt32(0);
                    }
                    return rp;
                }
            }
        }


        HiringMatrix IHiringMatrixDataAccess.getHiringMatrixLogDetails(HiringMatrix AddHiringMatrixLogDetails)
        {

            const string SP = "dbo.HiringLog_Create_HiringMatrixLogDetails";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);

                //************** Code added by Sumit Sonawane on 1/Mar/2017 **************************
                Database.AddInParameter(cmd, "@CandidateId", DbType.Int32, AddHiringMatrixLogDetails.MemberId);
                Database.AddInParameter(cmd, "@HiringLevel", DbType.Int32, AddHiringMatrixLogDetails.HiringLevelLogId);
                Database.AddInParameter(cmd, "@JobPostingId ", DbType.Int32, AddHiringMatrixLogDetails.JobPostingID);
                Database.AddInParameter(cmd, "@Comment", DbType.AnsiString, AddHiringMatrixLogDetails.HiringLevelComment);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, AddHiringMatrixLogDetails.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, AddHiringMatrixLogDetails.UpdatorId);
                Database.AddInParameter(cmd, "@CreateDate", DbType.DateTime, AddHiringMatrixLogDetails.CreateDate);
                Database.AddInParameter(cmd, "@UpdateDate", DbType.DateTime, AddHiringMatrixLogDetails.UpdateDate);

                //********************************END*****************************************

                Database.ExecuteNonQuery(cmd);
                return AddHiringMatrixLogDetails;
            }

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #endregion
        }
    }
}