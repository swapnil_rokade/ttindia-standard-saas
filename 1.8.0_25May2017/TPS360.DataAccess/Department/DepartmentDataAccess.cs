﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:DepartmentDataAccess.cs
    Description: This page is used for Countries  functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetDepartmentIdByDepartmentCode() to assist 
                                                                Resume parser team
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class DepartmentDataAccess : BaseDataAccess, IDepartmentDataAccess
    {
        #region Constructors

        public DepartmentDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<Department> CreateEntityBuilder<Department>()
        {
            return (new DepartmentBuilder()) as IEntityBuilder<Department>;
        }

        #endregion

        #region  Methods

        Department IDepartmentDataAccess.Add(Department Department)
        {
            const string SP = "dbo.Department_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, StringHelper.Convert(Department.Name));
             
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        Department = CreateEntityBuilder<Department>().BuildEntity(reader);
                    }
                    else
                    {
                        Department = null;
                    }
                }

                if (Department == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Department already exists. Please specify another Department.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this Department.");
                            }
                    }
                }

                return Department;
            }
        }

        Department IDepartmentDataAccess.Update(Department Department)
        {
            const string SP = "dbo.Department_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Department.Id);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, StringHelper.Convert(Department.Name));
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        Department = CreateEntityBuilder<Department>().BuildEntity(reader);
                    }
                    else
                    {
                        Department = null;
                    }
                }

                if (Department == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Department already exists. Please specify another Department.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this Department.");
                            }
                    }
                }

                return Department;
            }
        }

        Department IDepartmentDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Department_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Department>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<Department> IDepartmentDataAccess.GetAll()
        {
            const string SP = "dbo.Department_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Department>().BuildEntities(reader);
                }
            }
        }

        bool IDepartmentDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Department_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a Department which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this Department.");
                        }
                }
            }
        }

        Int32 IDepartmentDataAccess.GetDepartmentIdByDepartmentName(string DepartmentName)
        {
            if (DepartmentName==null)
            {
                DepartmentName="";
            }

            const string SP = "dbo.Department_GetDepartmentIdByDepartmentName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@DepartmentName", DbType.String, DepartmentName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 DepartmentId = 0;

                    while (reader.Read())
                    {
                        DepartmentId = reader.GetInt32(0);
                    }

                    return DepartmentId;
                }
            }
        }

        IList<Department > IDepartmentDataAccess.GetAllBySearch(string keyword, int count)
        {
            const string SP = "dbo.Department_GetPaged";
            string whereClause = string.Empty;

            whereClause = "[D].Name like '" + keyword + "%'";
            object[] paramValues = new object[] {	0,
													count ,
													StringHelper.Convert(whereClause),
													StringHelper.Convert("[D].[Name]"),
                                                    StringHelper.Convert("Asc")
												};
            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Department>().BuildEntities(reader);
                }
            }
        }
        #endregion
    }
}