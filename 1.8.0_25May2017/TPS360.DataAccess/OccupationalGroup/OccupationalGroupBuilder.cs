﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class OccupationalGroupBuilder : IEntityBuilder<OccupationalGroup>
    {
        IList<OccupationalGroup> IEntityBuilder<OccupationalGroup>.BuildEntities(IDataReader reader)
        {
            List<OccupationalGroup> OccupationalGroups = new List<OccupationalGroup>();

            while (reader.Read())
            {
                OccupationalGroups.Add(((IEntityBuilder<OccupationalGroup>)this).BuildEntity(reader));
            }

            return (OccupationalGroups.Count > 0) ? OccupationalGroups : null;
        }

        OccupationalGroup IEntityBuilder<OccupationalGroup>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_GROUPTITLE = 1;


            OccupationalGroup OccupationalGroup = new OccupationalGroup();

            OccupationalGroup.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            OccupationalGroup.GroupTitle = reader.IsDBNull(FLD_GROUPTITLE) ? string.Empty : reader.GetString(FLD_GROUPTITLE);

            return OccupationalGroup;
        }
    }
}