﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class ActivityResourceDataAccess : BaseDataAccess, IActivityResourceDataAccess
    {
        #region Constructors

        public ActivityResourceDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<ActivityResource> CreateEntityBuilder<ActivityResource>()
        {
            return (new ActivityResourceBuilder()) as IEntityBuilder<ActivityResource>;
        }

        #endregion

        #region  Methods

        ActivityResource IActivityResourceDataAccess.Add(ActivityResource activityResource)
        {
            const string SP = "dbo.ActivityResource_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ActivityID", DbType.Int32, activityResource.ActivityID);
                Database.AddInParameter(cmd, "@ResourceID", DbType.Int32, activityResource.ResourceID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        activityResource = CreateEntityBuilder<ActivityResource>().BuildEntity(reader);
                    }
                    else
                    {
                        activityResource = null;
                    }
                }

                if (activityResource == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("ActivityResource already exists. Please specify another activityResource.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this activityResource.");
                            }
                    }
                }

                return activityResource;
            }
        }

        //ActivityResource IActivityResourceDataAccess.Update(ActivityResource activityResource)
        //{
        //    const string SP = "dbo.ActivityResource_Update";

        //    using (DbCommand cmd = Database.GetStoredProcCommand(SP))
        //    {
        //        AddOutputParameter(cmd);
        //        Database.AddInParameter(cmd, "@ActivityID", DbType.Int32, activityResource.ActivityID);
        //        Database.AddInParameter(cmd, "@ResourceID", DbType.Int32, activityResource.ResourceID);

        //        using (IDataReader reader = Database.ExecuteReader(cmd))
        //        {
        //            if (reader.Read())
        //            {
        //                activityResource = CreateEntityBuilder<ActivityResource>().BuildEntity(reader);
        //            }
        //            else
        //            {
        //                activityResource = null;
        //            }
        //        }

        //        if (activityResource == null)
        //        {
        //            int returnCode = GetReturnCodeFromParameter(cmd);

        //            switch (returnCode)
        //            {
        //                case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
        //                    {
        //                        throw new ArgumentException("ActivityResource already exists. Please specify another activityResource.");
        //                    }
        //                default:
        //                    {
        //                        throw new SystemException("An unexpected error has occurred while updating this activityResource.");
        //                    }
        //            }
        //        }

        //        return activityResource;
        //    }
        //}

        ActivityResource IActivityResourceDataAccess.GetByActivityID(int activityID)
        {
            if (activityID < 1)
            {
                throw new ArgumentNullException("activityID");
            }

            const string SP = "dbo.ActivityResource_GetByActivityID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ActivityID", DbType.Int32, activityID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<ActivityResource>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<ActivityResource> IActivityResourceDataAccess.GetAll()
        {
            const string SP = "dbo.ActivityResource_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<ActivityResource>().BuildEntities(reader);
                }
            }
        }

        bool IActivityResourceDataAccess.DeleteByActivityID(int activityID)
        {
            if (activityID < 1)
            {
                throw new ArgumentNullException("activityID");
            }

            const string SP = "dbo.ActivityResource_DeleteByActivityID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ActivityID", DbType.Int32, activityID);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a activityResource which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this activityResource.");
                        }
                }
            }
        }

        #endregion
    }
}