﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class ActivityResourceBuilder : IEntityBuilder<ActivityResource>
    {
        IList<ActivityResource> IEntityBuilder<ActivityResource>.BuildEntities(IDataReader reader)
        {
            List<ActivityResource> activityResources = new List<ActivityResource>();

            while (reader.Read())
            {
                activityResources.Add(((IEntityBuilder<ActivityResource>)this).BuildEntity(reader));
            }

            return (activityResources.Count > 0) ? activityResources : null;
        }

        ActivityResource IEntityBuilder<ActivityResource>.BuildEntity(IDataReader reader)
        {
            const int FLD_ACTIVITYID = 0;
            const int FLD_RESOURCEID = 1;

            ActivityResource activityResource = new ActivityResource();

            activityResource.ActivityID = reader.IsDBNull(FLD_ACTIVITYID) ? 0 : reader.GetInt32(FLD_ACTIVITYID);
            activityResource.ResourceID = reader.IsDBNull(FLD_RESOURCEID) ? 0 : reader.GetInt32(FLD_RESOURCEID);

            return activityResource;
        }
    }
}