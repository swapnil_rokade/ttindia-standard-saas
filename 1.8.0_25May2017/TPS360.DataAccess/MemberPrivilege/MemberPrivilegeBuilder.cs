﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberPrivilegeBuilder : IEntityBuilder<MemberPrivilege>
    {
        IList<MemberPrivilege> IEntityBuilder<MemberPrivilege>.BuildEntities(IDataReader reader)
        {
            List<MemberPrivilege> memberPrivileges = new List<MemberPrivilege>();

            while (reader.Read())
            {
                memberPrivileges.Add(((IEntityBuilder<MemberPrivilege>)this).BuildEntity(reader));
            }

            return (memberPrivileges.Count > 0) ? memberPrivileges : null;
        }

        MemberPrivilege IEntityBuilder<MemberPrivilege>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_PRIVILEGE = 1;
            const int FLD_ISREMOVED = 2;
            const int FLD_MEMBERCUSTOMROLEMAPID = 3;
            const int FLD_CUSTOMSITEMAPID = 4;
            const int FLD_CREATORID = 5;
            const int FLD_UPDATORID = 6;
            const int FLD_CREATEDATE = 7;
            const int FLD_UPDATEDATE = 8;

            MemberPrivilege memberPrivilege = new MemberPrivilege();

            memberPrivilege.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberPrivilege.Privilege = reader.IsDBNull(FLD_PRIVILEGE) ? 0 : reader.GetInt32(FLD_PRIVILEGE);
            memberPrivilege.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberPrivilege.MemberId = reader.IsDBNull(FLD_MEMBERCUSTOMROLEMAPID) ? 0 : reader.GetInt32(FLD_MEMBERCUSTOMROLEMAPID);
            memberPrivilege.CustomSiteMapId = reader.IsDBNull(FLD_CUSTOMSITEMAPID) ? 0 : reader.GetInt32(FLD_CUSTOMSITEMAPID);
            memberPrivilege.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberPrivilege.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberPrivilege.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberPrivilege.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberPrivilege;
        }
    }
}