﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: GenericLookupDataAccess.cs
    Description:  
    Created By:  
    Created On:  
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  1.0                 5/Dec/2015          Prasanth Kumar G    Introduced Function InterviewAssessment_GetBy_InterviewId_InterviewerEmail
 *  2.0                 25/May/2016         pravin khot         GetByDescription
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class GenericLookupDataAccess : BaseDataAccess, IGenericLookupDataAccess
    {
        #region Constructors

        public GenericLookupDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<GenericLookup> CreateEntityBuilder<GenericLookup>()
        {
            return (new GenericLookupBuilder()) as IEntityBuilder<GenericLookup>;
        }

        #endregion

        #region  Methods

        GenericLookup IGenericLookupDataAccess.Add(GenericLookup genericLookup)
        {
            const string SP = "dbo.GenericLookup_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Name", DbType.String, StringHelper.Convert(genericLookup.Name));
                Database.AddInParameter(cmd, "@Description", DbType.String, StringHelper.Convert(genericLookup.Description));
                Database.AddInParameter(cmd, "@Type", DbType.Int32, genericLookup.Type);
                Database.AddInParameter(cmd, "@SortOrder", DbType.Int32, genericLookup.SortOrder);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, genericLookup.CreatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        genericLookup = CreateEntityBuilder<GenericLookup>().BuildEntity(reader);
                    }
                    else
                    {
                        genericLookup = null;
                    }
                }

                if (genericLookup == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Generic lookup already exists. Please specify another generic lookup.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this generic lookup.");
                            }
                    }
                }

                return genericLookup;
            }
        }

        GenericLookup IGenericLookupDataAccess.Update(GenericLookup genericLookup)
        {
            const string SP = "dbo.GenericLookup_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, genericLookup.Id);
                Database.AddInParameter(cmd, "@Name", DbType.String, genericLookup.Name);
                Database.AddInParameter(cmd, "@Description", DbType.String, StringHelper.Convert(genericLookup.Description));
                Database.AddInParameter(cmd, "@Type", DbType.Int32, genericLookup.Type);
                Database.AddInParameter(cmd, "@SortOrder", DbType.Int32, genericLookup.SortOrder);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, genericLookup.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        genericLookup = CreateEntityBuilder<GenericLookup>().BuildEntity(reader);
                    }
                    else
                    {
                        genericLookup = null;
                    }
                }

                if (genericLookup == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Generic lookup already exists. Please specify another generic lookup.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this generic lookup.");
                            }
                    }
                }

                return genericLookup;
            }
        }

        GenericLookup IGenericLookupDataAccess.GetById(int id)
        {
            const string SP = "dbo.GenericLookup_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<GenericLookup>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        //added by pravin khot on 25/May/2016*************
        GenericLookup IGenericLookupDataAccess.GetByDescription(string  location)
        {
            const string SP = "dbo.GenericLookup_GetByDescription";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@location", DbType.String, location);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<GenericLookup>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        //************************END***********************************
        IList<GenericLookup> IGenericLookupDataAccess.GetAll()
        {
            const string SP = "dbo.GenericLookup_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<GenericLookup>().BuildEntities(reader);
                }
            }
        }

        IList<GenericLookup> IGenericLookupDataAccess.GetByIds(string IDs)
        {
            const string SP = "dbo.GenericLookup_GetByIds";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Ids", DbType.AnsiString , IDs);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<GenericLookup>().BuildEntities(reader);
                }
            }
        }


        PagedResponse<GenericLookup> IGenericLookupDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.GenericLookup_GetPaged";
            string whereClause = string.Empty;

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "Name";
            }

            request.SortColumn = "[G].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<GenericLookup> response = new PagedResponse<GenericLookup>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<GenericLookup>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        public IList<GenericLookup> GetAllByLookupType(LookupType lookupType)
        {
            const string SP = "dbo.GenericLookup_GetAllByLookupType";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Type", DbType.Int32, lookupType);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<GenericLookup>().BuildEntities(reader);
                }
            }
        }
        // For Optimization by Vignesh
        public IList<GenericLookup> GetAllByLookupType(string lookupTypes)
        {
            const string SP = "dbo.GenericLookup_GetAllByLookupTypes";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Type", DbType.String, lookupTypes);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<GenericLookup>().BuildEntities(reader);
                }
            }
        }

        public IList<GenericLookup> GetAllByLookupTypeAndName(LookupType lookupType,string lookupName)
        {
            const string SP = "dbo.GenericLookup_GetAllByLookupTypeAndName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Type", DbType.Int32, lookupType);
                Database.AddInParameter(cmd, "@Name", DbType.String, lookupName);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<GenericLookup>().BuildEntities(reader);
                }
            }
        }

        bool IGenericLookupDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.GenericLookup_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a generic lookup which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this generic lookup.");
                        }
                }
            }
        }

        string  IGenericLookupDataAccess.GetLookUPNamesByIds(string  ids)
        {
            const string SP = "dbo.GenericLookup_GetLookupNameByIds";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Ids", DbType.AnsiString , ids);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
            }
        }

        //Function introduced by Prasanth on 05/Dec/2015 
        IList<GenericLookup> IGenericLookupDataAccess.InterviewAssessment_GetBy_InterviewId_InterviewerEmail(int InterviewId, string Email)
        {
            if (InterviewId < 0)
            {
                throw new ArgumentNullException("InterviewId");
            }
            const string SP = "InterviewAssessment_GetBy_InterviewId_InterviewerEmail";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Interviewid", DbType.Int32, InterviewId);
                Database.AddInParameter(cmd, "@InterviewerEmail", DbType.String, Email);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<GenericLookup>().BuildEntities(reader);
                }
            }
        }

        #endregion
    }
}