﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmployeeBuilder.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              10/Jun/2016            Prasanth            Introduced LDAP
------------------------------------------------------------------------------------------------------------------------------------------- 
*/
using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class EmployeeBuilder : IEntityBuilder<Employee>
    {



        IList<Employee> IEntityBuilder<Employee>.BuildEntities(IDataReader reader)
        {
            List<Employee> employees = new List<Employee>();

            while (reader.Read())
            {
                employees.Add(((IEntityBuilder<Employee>)this).BuildEntity(reader));
            }

            return (employees.Count > 0) ? employees : null;
        }


        public IList <EmployeeProductivity > BuildEmployeeProductivity(IDataReader reader)
        {
            List<EmployeeProductivity> employeesProductivity = new List<EmployeeProductivity>();

            while (reader.Read())
            {
                employeesProductivity.Add(BuildEmployeeProductivityEntity(reader));
            }

            return (employeesProductivity.Count > 0) ? employeesProductivity : null;
        }

        EmployeeProductivity BuildEmployeeProductivityEntity(IDataReader reader)
        {
            const int FLD_EMPLOYEEID = 0;
            const int FLD_EMPLOYEENAME = 1;
            const int FLD_JOBID = 2;
            const int FLD_SUBMISSIONS = 3;
            const int FLD_CLIENTS = 4;
            EmployeeProductivity emp = new EmployeeProductivity();
            emp.EmployeeId = reader.IsDBNull(FLD_EMPLOYEEID) ? 0 : reader.GetInt32(FLD_EMPLOYEEID);
            emp.EmployeeName = reader.IsDBNull(FLD_EMPLOYEENAME) ? string.Empty : reader.GetString(FLD_EMPLOYEENAME);
            emp.JobCount  = reader.IsDBNull(FLD_JOBID) ? 0 : reader.GetInt32(FLD_JOBID);
            emp.SubmissionCount   = reader.IsDBNull(FLD_SUBMISSIONS) ? 0 : reader.GetInt32(FLD_SUBMISSIONS);
            emp.NewClientCount  =reader .IsDBNull (FLD_CLIENTS)?0: reader .GetInt32 (FLD_CLIENTS );
            emp.HiringMatrixLevels = new List<Hiring_Levels>();
            for (int i = 5; i < reader.FieldCount; i++)
            {
                Hiring_Levels levels = new Hiring_Levels();
                levels.HiringMatrixLevelID = Convert.ToInt32(reader.GetName(i));
                levels.CandidateCount = reader.IsDBNull(i) ? 0 : reader.GetInt32(i);
                emp.HiringMatrixLevels.Add(levels);
            }
            return emp;

        }


        public IList<EmployeeProductivity> BuildEmployeeProductivityReport(IDataReader reader)
        {
            List<EmployeeProductivity> employeesProductivity = new List<EmployeeProductivity>();

            while (reader.Read())
            {
                employeesProductivity.Add(BuildEmployeeProductivityReportEntity(reader));
            }

            return (employeesProductivity.Count > 0) ? employeesProductivity : null;
        }



        EmployeeProductivity BuildEmployeeProductivityReportEntity(IDataReader reader)
        {
            if (reader.FieldCount == 5)
            {
                const int FLD_EMPLOYEEID = 0;
                const int FLD_EMPLOYEENAME = 1;
                const int FLD_NoOfInterviewCompleted = 2;
                const int FLD_NoOfInterviewNoShow = 3;
                const int FLD_NoOfInterviewReschedule = 4;

                EmployeeProductivity emp = new EmployeeProductivity();
                emp.EmployeeId = reader.IsDBNull(FLD_EMPLOYEEID) ? 0 : reader.GetInt32(FLD_EMPLOYEEID);
                emp.EmployeeName = reader.IsDBNull(FLD_EMPLOYEENAME) ? string.Empty : reader.GetString(FLD_EMPLOYEENAME);
                emp.InterviewCompleted = reader.IsDBNull(FLD_NoOfInterviewCompleted) ? 0 : reader.GetInt32(FLD_NoOfInterviewCompleted);
                emp.InterviewNoShow = reader.IsDBNull(FLD_NoOfInterviewNoShow) ? 0 : reader.GetInt32(FLD_NoOfInterviewNoShow);
                emp.InterviewReschedule = reader.IsDBNull(FLD_NoOfInterviewReschedule) ? 0 : reader.GetInt32(FLD_NoOfInterviewReschedule);

                return emp;
            }


            else if (reader.FieldCount == 6)
            {
                const int FLD_YearGraph = 0;
                const int FLD_MonthGraph = 1;
                const int FLD_MonthNameGraph = 2;
                const int FLD_NoOfOpeningsGraph = 3;
                const int FLD_NoOfOpenings1Graph = 4;
                const int FLD_RequisitionsCountGraph = 5;

                EmployeeProductivity emp = new EmployeeProductivity();
                emp.YearGraph = reader.IsDBNull(FLD_YearGraph) ? 0 : reader.GetInt32(FLD_YearGraph);
                emp.MonthGraph = reader.IsDBNull(FLD_MonthGraph) ? 0 : reader.GetInt32(FLD_MonthGraph);
                emp.MonthNameGraph = reader.IsDBNull(FLD_MonthNameGraph) ? string.Empty : reader.GetString(FLD_MonthNameGraph);
                emp.NoOfOpeningsGraph = reader.IsDBNull(FLD_NoOfOpeningsGraph) ? 0 : reader.GetInt32(FLD_NoOfOpeningsGraph);
                emp.NoOfOpenings1Graph = reader.IsDBNull(FLD_NoOfOpenings1Graph) ? 0 : reader.GetInt32(FLD_NoOfOpenings1Graph);
                emp.RequisitionsCountGraph = reader.IsDBNull(FLD_RequisitionsCountGraph) ? 0 : reader.GetInt32(FLD_RequisitionsCountGraph);              
              
                return emp;
            }

            else if (reader.FieldCount == 9)
            {
                const int FLD_EMPLOYEEID = 0;
                const int FLD_EMPLOYEENAME = 1;
                const int FLD_ExpectedTimeToFill = 2;
                const int FLD_ActualTimeToFill = 3;
                const int FLD_NoOfOpeningsGraph = 4;
                const int FLD_JobPostingIdGraph = 5;
                const int FLD_JoinedCandidatesGraph = 6;
                const int FLD_UpdateDateGraph = 7;
                const int FLD_OpenDateGraph = 8;
               

                EmployeeProductivity emp = new EmployeeProductivity();
                emp.EmployeeId = reader.IsDBNull(FLD_EMPLOYEEID) ? 0 : reader.GetInt32(FLD_EMPLOYEEID);
                emp.EmployeeName = reader.IsDBNull(FLD_EMPLOYEENAME) ? string.Empty : reader.GetString(FLD_EMPLOYEENAME);
                emp.ExpectedTimeToFill = reader.IsDBNull(FLD_ExpectedTimeToFill) ? 0 : reader.GetInt32(FLD_ExpectedTimeToFill);
                emp.ActualTimeToFill = reader.IsDBNull(FLD_ActualTimeToFill) ? 0 : reader.GetInt32(FLD_ActualTimeToFill);
                emp.NoOfOpeningsJoinedGraph = reader.IsDBNull(FLD_NoOfOpeningsGraph) ? 0 : reader.GetInt32(FLD_NoOfOpeningsGraph);
                emp.JobPostingIdGraph = reader.IsDBNull(FLD_JobPostingIdGraph) ? 0 : reader.GetInt32(FLD_JobPostingIdGraph);
                emp.JoinedCandidatesGraph = reader.IsDBNull(FLD_JoinedCandidatesGraph) ? 0 : reader.GetInt32(FLD_JoinedCandidatesGraph);
                emp.UpdateDateGraph = reader.IsDBNull(FLD_UpdateDateGraph) ? DateTime.MinValue : reader.GetDateTime(FLD_UpdateDateGraph);
                emp.OpenDateGraph = reader.IsDBNull(FLD_OpenDateGraph) ? DateTime.MinValue : reader.GetDateTime(FLD_OpenDateGraph);

                return emp;
            }
            else if (reader.FieldCount == 3)
            {
                const int FLD_ClientIdGraph = 0;
                const int FLD_NoOfCandidateJoinedGraph = 1;
                const int FLD_NoOfCandidateRejectedGraph = 2;
              

                EmployeeProductivity emp = new EmployeeProductivity();
                emp.ClientIdGraph = reader.IsDBNull(FLD_ClientIdGraph) ? 0 : reader.GetInt32(FLD_ClientIdGraph);
                emp.NoOfCandidateJoinedGraph = reader.IsDBNull(FLD_NoOfCandidateJoinedGraph) ? 0 : reader.GetInt32(FLD_NoOfCandidateJoinedGraph);
                emp.NoOfCandidateRejectedGraph = reader.IsDBNull(FLD_NoOfCandidateRejectedGraph) ? 0 : reader.GetInt32(FLD_NoOfCandidateRejectedGraph);
             
                return emp;
            }

            else if (reader.FieldCount == 4)
            {
                const int FLD_CountByJobStatusGraph = 0;
                const int FLD_JobStatusGraph = 1;
                const int FLD_CountNoOfOpeningsGraph = 2;
                const int FLD_CountNoOfOpeningsTempGraph = 3;


                EmployeeProductivity emp = new EmployeeProductivity();
                emp.CountByJobStatusGraph = reader.IsDBNull(FLD_CountByJobStatusGraph) ? 0 : reader.GetInt32(FLD_CountByJobStatusGraph);
                emp.JobStatusGraph = reader.IsDBNull(FLD_JobStatusGraph) ? 0 : reader.GetInt32(FLD_JobStatusGraph);
                emp.CountNoOfOpeningsGraph = reader.IsDBNull(FLD_CountNoOfOpeningsGraph) ? 0 : reader.GetInt32(FLD_CountNoOfOpeningsGraph);
                emp.CountNoOfOpeningsTempGraph = reader.IsDBNull(FLD_CountNoOfOpeningsTempGraph) ? 0 : reader.GetInt32(FLD_CountNoOfOpeningsTempGraph);

                return emp;
            }

            else if (reader.FieldCount == 2)
            {
                const int FLD_SkillId = 0;
                const int FLD_NoOfCandidateForSkill = 1;
                
                EmployeeProductivity emp = new EmployeeProductivity();
                emp.SkillIdGraph = reader.IsDBNull(FLD_SkillId) ? 0 : reader.GetInt32(FLD_SkillId);
                emp.NoOfCandidateForSkillGraph = reader.IsDBNull(FLD_NoOfCandidateForSkill) ? 0 : reader.GetInt32(FLD_NoOfCandidateForSkill);
              
                return emp;
            }
            else
            {
                const int FLD_EMPLOYEEID = 0;
                const int FLD_EMPLOYEENAME = 1;
                const int FLD_NOOFCANDIDATES = 2;
                const int FLD_JOBID = 3;
                const int FLD_CANDIDATESOURCED = 4;
                const int FLD_SUBMISSIONS = 5;
                const int FLD_INTERVIEWS = 6;
                const int FLD_OFFERS = 7;
                const int FLD_OFFERREJECTED = 8;
                const int FLD_JOINED = 9;
                const int FLD_PENDING = 10;
                EmployeeProductivity emp = new EmployeeProductivity();
                emp.EmployeeId = reader.IsDBNull(FLD_EMPLOYEEID) ? 0 : reader.GetInt32(FLD_EMPLOYEEID);
                emp.EmployeeName = reader.IsDBNull(FLD_EMPLOYEENAME) ? string.Empty : reader.GetString(FLD_EMPLOYEENAME);
                emp.NewCandidateCount = reader.IsDBNull(FLD_NOOFCANDIDATES) ? 0 : reader.GetInt32(FLD_NOOFCANDIDATES);
                emp.JobCount = reader.IsDBNull(FLD_JOBID) ? 0 : reader.GetInt32(FLD_JOBID);
                emp.CandidateSourcedCount = reader.IsDBNull(FLD_CANDIDATESOURCED) ? 0 : reader.GetInt32(FLD_CANDIDATESOURCED);
                emp.SubmissionCount = reader.IsDBNull(FLD_SUBMISSIONS) ? 0 : reader.GetInt32(FLD_SUBMISSIONS);
                emp.InterviewsCount = reader.IsDBNull(FLD_INTERVIEWS) ? 0 : reader.GetInt32(FLD_INTERVIEWS);
                emp.OfferCount = reader.IsDBNull(FLD_OFFERS) ? 0 : reader.GetInt32(FLD_OFFERS);
                emp.OfferRejectedCount = reader.IsDBNull(FLD_OFFERREJECTED) ? 0 : reader.GetInt32(FLD_OFFERREJECTED);
                emp.JoinedCount = reader.IsDBNull(FLD_JOINED) ? 0 : reader.GetInt32(FLD_JOINED);
                emp.PendingJoiners = reader.IsDBNull(FLD_PENDING) ? 0 : reader.GetInt32(FLD_PENDING);
                emp.HiringMatrixLevels = new List<Hiring_Levels>();

                return emp;
            }
        }


        Employee IEntityBuilder<Employee>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_FIRSTNAME = 1;
            const int FLD_MIDDLENAME = 2;
            const int FLD_LASTNAME = 3;
            const int FLD_PRIMARYEMAIL = 4;
            const int FLD_PRIMARYPHONE = 5;
            const int FLD_PRIMARYPHONEEXTENSION = 6;
            const int FLD_CELLPHONE = 7;
            const int FLD_STATUS = 8;
            const int FLD_HOMEPHONE = 9;
            const int FLD_CURRENTCITY = 10;
            const int FLD_STATENAME = 11;
            const int FLD_STATECODE = 12;
            const int FLD_OFFICECITY = 13;
            const int FLD_CURRENTPOSITION = 14;
            const int FLD_SYSTEMACCESS = 15;
            const int FLD_REMARKS = 16;
            const int FLD_OBJECTIVE = 17;
            const int FLD_SUMMARY = 18;
            const int FLD_LASTEMPLOYER = 19;
            const int FLD_USERID = 20;
            const int FLD_CREATORID = 21;
            const int FLD_UPDATORID = 22;
            const int FLD_CREATEDATE = 23;
            const int FLD_UPDATEDATE = 24;

            const int FLD_RECORDTYPE = 25;
            //For list report
            const int FLD_NICKNAME = 26;
            const int FLD_DATEOFBIRTH = 27;
            const int FLD_PERMANENTADDRESSLINE1 = 28;
            const int FLD_PERMANENTADDRESSLINE2 = 29;
            const int FLD_PERMANENTCITY = 30;
            const int FLD_PERMANENTSTATEID = 31;
            const int FLD_PERMANENTSTATENAME = 32;
            const int FLD_PERMANENTSTATECODE = 33;
            const int FLD_PERMANENTZIP = 34;
            const int FLD_PERMANENTCOUNTRYID = 35;
            const int FLD_PERMANENTCOUNTRYNAME = 36;
            const int FLD_PERMANENTCOUNTRYCODE = 37;
            const int FLD_PERMANENTPHONE = 38;
            const int FLD_PERMANENTPHONEEXT = 39;
            const int FLD_PERMANENTMOBILE = 40;
            const int FLD_ALTERNATEEMAIL = 41;
            const int FLD_RESUMESOURCE = 42;
            const int FLD_CURRENTADDRESSLINE1 = 43;
            const int FLD_CURRENTADDRESSLINE2 = 44;
            const int FLD_CURRENTSTATEID = 45;
            const int FLD_CURRENTSTATENAME = 46;
            const int FLD_CURRENTSTATECODE = 47;
            const int FLD_CURRENTZIP = 48;
            const int FLD_CURRENTCOUNTRYID = 49;
            const int FLD_CURRENTCOUNTRYNAME = 50;
            const int FLD_CURRENTCOUNTRYCODE = 51;
            const int FLD_OFFICEPHONE = 52;
            const int FLD_OFFICEPHONEEXTENSION = 53;
            const int FLD_CITYOFBIRTH = 54;
            const int FLD_COUNTRYIDOFBIRTH = 55;
            const int FLD_BIRTHCOUNTRYNAME = 56;
            const int FLD_BIRTHCOUNTRYCODE = 57;
            const int FLD_COUNTRYIDOFCITIZENSHIP = 58;
            const int FLD_CITIZENSHIPCOUNTRYNAME = 59;
            const int FLD_CITIZENSHIPCOUNTRYCODE = 60;
            const int FLD_GENDERLOOKUPID = 61;
            const int FLD_GENDER = 62;
            const int FLD_ETHNICGROUPLOOKUPID = 63;
            const int FLD_ETHNICGROUP = 64;
            const int FLD_BLOODGROUPLOOKUPID = 65;
            const int FLD_BLOODGROUP = 66;
            const int FLD_MARITALSTATUSLOOKUPID = 67;
            const int FLD_MARITALSTATUS = 68;
            const int FLD_RELOCATION = 69;
            const int FLD_SKILLS = 70;
            const int FLD_TOTALEXPERIENCEYEARS = 71;
            const int FLD_AVAILABILITY = 72;
            const int FLD_AVAILABILITYTEXT = 73;
            const int FLD_AVAILABLEDATE = 74;
            const int FLD_CURRENTYEARLYRATE = 75;
            const int FLD_CURRENTYEARLYCURRENCYLOOKUPID = 76;
            const int FLD_CURRENTYEARLYCURRENCY = 77;
            const int FLD_CURRENTMONTHLYRATE = 78;
            const int FLD_CURRENTMONTHLYCURRENCYLOOKUPID = 79;
            const int FLD_CURRENTMONTHLYCURRENCY = 80;
            const int FLD_CURRENTHOURLYRATE = 81;
            const int FLD_CURRENTHOURLYCURRENCYLOOKUPID = 82;
            const int FLD_CURRENTHOURLYCURRENCY = 83;
            const int FLD_EXPECTEDYEARLYRATE = 84;
            const int FLD_EXPECTEDYEARLYCURRENCYLOOKUPID = 85;
            const int FLD_EXPECTEDYEARLYCURRENCY = 86;
            const int FLD_EXPECTEDMONTHLYRATE = 87;
            const int FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID = 88;
            const int FLD_EXPECTEDMONTHLYCURRENCY = 89;
            const int FLD_EXPECTEDHOURLYRATE = 90;
            const int FLD_EXPECTEDHOURLYCURRENCYLOOKUPID = 91;
            const int FLD_EXPECTEDHOURLYCURRENCY = 92;
            const int FLD_WILLINGTOTRAVEL = 93;
            const int FLD_SALARYTYPELOOKUPID = 94;
            const int FLD_SALARYTYPE = 95;
            const int FLD_JOBTYPELOOKUPID = 96;
            const int FLD_JOBTYPE = 97;
            const int FLD_SECURITYCLEARANCE = 98;
            const int FLD_WORKAUTHORIZATIONLOOKUPID = 99;
            const int FLD_WORKAUTHORIZATION = 100;
            const int FLD_CREATOR = 101;
            const int FLD_UPDATOR = 102;

            //For work report
            const int FLD_LEADS = 26;
            const int FLD_LEADSAMOUNT = 27;
            const int FLD_OPPORTUNITY = 28;
            const int FLD_OPPORTUNITYAMOUNT = 29;
            const int FLD_LOSTOPPORTUNITY = 30;
            const int FLD_LOSTOPPORTUNITYAMOUNT = 31;
            const int FLD_SUCCESSFULSALES = 32;
            const int FLD_SUCCESSFULSALESAMOUNT = 33;
            const int FLD_COMPANY = 34;
            const int FLD_PROSPECTIVECLIENT = 35;
            const int FLD_CLIENT = 36;
            const int FLD_VENDOR = 37;
            const int FLD_PARTNER = 38;
            const int FLD_OPENREQUISITION = 39;
            const int FLD_CLOSEDREQUISITION = 40;
            const int FLD_SUBMITTEDREQUISITION = 41;
            const int FLD_SUCCESSFULHIRE = 42;
            const int FLD_UNSUCCESSFULHIRE = 43;
            const int FLD_CANDIDATE = 44;
            const int FLD_CONSULTANT = 45;
            const int FLD_ENGAGEDCONSULTANT = 46;
            const int FLD_BENCHCONSULTANT = 47;
            const int FLD_HOTLIST = 48;
            const int FLD_INVOICE = 49;
            const int FLD_INVOICEAMOUNT = 50;
            const int FLD_INVOICEPAID = 51;
            const int FLD_INVOICEPAIDAMOUNT = 52;
            const int FLD_EXPENSE = 53;
            const int FLD_EXPENSEAMOUNT = 54;
            const int FLD_EXPENSEPAID = 55;
            const int FLD_EXPENSEPAIDAMOUNT = 56;

            const int FLD_PRIMARYMANAGER = FLD_RECORDTYPE + 1;
            const int FLD_USERNAME = FLD_PRIMARYMANAGER + 1; //Line introduced by Prasanth on 10/Jun/2016
            const int FLD_ISLDAP = FLD_USERNAME + 1; //Line introduced by Prasanth on 10/Jun/2016
            Employee employee = new Employee();

            employee.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            employee.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            employee.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
            employee.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            employee.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
            employee.PrimaryPhone = reader.IsDBNull(FLD_PRIMARYPHONE) ? string.Empty : reader.GetString(FLD_PRIMARYPHONE);
            employee.PrimaryPhoneExtension = reader.IsDBNull(FLD_PRIMARYPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_PRIMARYPHONEEXTENSION);
            employee.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
            employee.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
            employee.HomePhone = reader.IsDBNull(FLD_HOMEPHONE) ? string.Empty : reader.GetString(FLD_HOMEPHONE);
            employee.CurrentCity = reader.IsDBNull(FLD_CURRENTCITY) ? string.Empty : reader.GetString(FLD_CURRENTCITY);
            employee.StateName = reader.IsDBNull(FLD_STATENAME) ? string.Empty : reader.GetString(FLD_STATENAME);
            employee.StateCode = reader.IsDBNull(FLD_STATECODE) ? string.Empty : reader.GetString(FLD_STATECODE);
            employee.OfficeCity = reader.IsDBNull(FLD_OFFICECITY) ? string.Empty : reader.GetString(FLD_OFFICECITY);
            employee.CurrentPosition = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
            employee.SystemAccess = reader.IsDBNull(FLD_SYSTEMACCESS) ? string.Empty : reader.GetString(FLD_SYSTEMACCESS);
            employee.Remarks = reader.IsDBNull(FLD_REMARKS) ? string.Empty : reader.GetString(FLD_REMARKS);
            employee.Objective = reader.IsDBNull(FLD_OBJECTIVE) ? string.Empty : reader.GetString(FLD_OBJECTIVE);
            employee.Summary = reader.IsDBNull(FLD_SUMMARY) ? string.Empty : reader.GetString(FLD_SUMMARY);
            employee.LastEmployer = reader.IsDBNull(FLD_LASTEMPLOYER) ? string.Empty : reader.GetString(FLD_LASTEMPLOYER);
            employee.UserId = reader.IsDBNull(FLD_USERID) ? Guid.Empty : reader.GetGuid(FLD_USERID);
            employee.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            employee.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            employee.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            employee.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            string strRecordType = reader.IsDBNull(FLD_RECORDTYPE) ? string.Empty : reader.GetString(FLD_RECORDTYPE);
            if (strRecordType.ToUpper() == "REPORT")
            {
                employee.NickName = reader.IsDBNull(FLD_NICKNAME) ? string.Empty : reader.GetString(FLD_NICKNAME);
                employee.DateOfBirth = reader.IsDBNull(FLD_DATEOFBIRTH) ? DateTime.MinValue : reader.GetDateTime(FLD_DATEOFBIRTH);
                employee.PermanentAddressLine1 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE1);
                employee.PermanentAddressLine2 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE2);
                employee.PermanentCity = reader.IsDBNull(FLD_PERMANENTCITY) ? string.Empty : reader.GetString(FLD_PERMANENTCITY);
                employee.PermanentStateId = reader.IsDBNull(FLD_PERMANENTSTATEID) ? 0 : reader.GetInt32(FLD_PERMANENTSTATEID);
                employee.PermanentStateName = reader.IsDBNull(FLD_PERMANENTSTATENAME) ? string.Empty : reader.GetString(FLD_PERMANENTSTATENAME);
                employee.PermanentStateCode = reader.IsDBNull(FLD_PERMANENTSTATECODE) ? string.Empty : reader.GetString(FLD_PERMANENTSTATECODE);
                employee.PermanentZip = reader.IsDBNull(FLD_PERMANENTZIP) ? string.Empty : reader.GetString(FLD_PERMANENTZIP);
                employee.PermanentCountryId = reader.IsDBNull(FLD_PERMANENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_PERMANENTCOUNTRYID);
                employee.PermanentCountryName = reader.IsDBNull(FLD_PERMANENTCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_PERMANENTCOUNTRYNAME);
                employee.PermanentCountryCode = reader.IsDBNull(FLD_PERMANENTCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_PERMANENTCOUNTRYCODE);
                employee.PermanentPhone = reader.IsDBNull(FLD_PERMANENTPHONE) ? string.Empty : reader.GetString(FLD_PERMANENTPHONE);
                employee.PermanentPhoneExt = reader.IsDBNull(FLD_PERMANENTPHONEEXT) ? string.Empty : reader.GetString(FLD_PERMANENTPHONEEXT);
                employee.PermanentMobile = reader.IsDBNull(FLD_PERMANENTMOBILE) ? string.Empty : reader.GetString(FLD_PERMANENTMOBILE);
                employee.AlternateEmail = reader.IsDBNull(FLD_ALTERNATEEMAIL) ? string.Empty : reader.GetString(FLD_ALTERNATEEMAIL);
                employee.ResumeSource = reader.IsDBNull(FLD_RESUMESOURCE) ? 0 : reader.GetInt32(FLD_RESUMESOURCE);
                employee.CurrentAddressLine1 = reader.IsDBNull(FLD_CURRENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE1);
                employee.CurrentAddressLine2 = reader.IsDBNull(FLD_CURRENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE2);
                employee.CurrentStateId = reader.IsDBNull(FLD_CURRENTSTATEID) ? 0 : reader.GetInt32(FLD_CURRENTSTATEID);
                employee.CurrentStateName = reader.IsDBNull(FLD_CURRENTSTATENAME) ? string.Empty : reader.GetString(FLD_CURRENTSTATENAME);
                employee.CurrentStateCode = reader.IsDBNull(FLD_CURRENTSTATECODE) ? string.Empty : reader.GetString(FLD_CURRENTSTATECODE);
                employee.CurrentZip = reader.IsDBNull(FLD_CURRENTZIP) ? string.Empty : reader.GetString(FLD_CURRENTZIP);
                employee.CurrentCountryId = reader.IsDBNull(FLD_CURRENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_CURRENTCOUNTRYID);
                employee.CurrentCountryName = reader.IsDBNull(FLD_CURRENTCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_CURRENTCOUNTRYNAME);
                employee.CurrentCountryCode = reader.IsDBNull(FLD_CURRENTCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_CURRENTCOUNTRYCODE);
                employee.OfficePhone = reader.IsDBNull(FLD_OFFICEPHONE) ? string.Empty : reader.GetString(FLD_OFFICEPHONE);
                employee.OfficePhoneExtension = reader.IsDBNull(FLD_OFFICEPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_OFFICEPHONEEXTENSION);
                employee.CityOfBirth = reader.IsDBNull(FLD_CITYOFBIRTH) ? string.Empty : reader.GetString(FLD_CITYOFBIRTH);
                employee.CountryIdOfBirth = reader.IsDBNull(FLD_COUNTRYIDOFBIRTH) ? string.Empty : reader.GetString(FLD_COUNTRYIDOFBIRTH);
                employee.BirthCountryName = reader.IsDBNull(FLD_BIRTHCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_BIRTHCOUNTRYNAME);
                employee.BirthCountryCode = reader.IsDBNull(FLD_BIRTHCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_BIRTHCOUNTRYCODE);
                employee.CountryIdOfCitizenship = reader.IsDBNull(FLD_COUNTRYIDOFCITIZENSHIP) ? string.Empty : reader.GetString(FLD_COUNTRYIDOFCITIZENSHIP);
                employee.CitizenshipCountryName = reader.IsDBNull(FLD_CITIZENSHIPCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_CITIZENSHIPCOUNTRYNAME);
                employee.CitizenshipCountryCode = reader.IsDBNull(FLD_CITIZENSHIPCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_CITIZENSHIPCOUNTRYCODE);
                employee.GenderLookupId = reader.IsDBNull(FLD_GENDERLOOKUPID) ? 0 : reader.GetInt32(FLD_GENDERLOOKUPID);
                employee.Gender = reader.IsDBNull(FLD_GENDER) ? string.Empty : reader.GetString(FLD_GENDER);
                employee.EthnicGroupLookupId = reader.IsDBNull(FLD_ETHNICGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_ETHNICGROUPLOOKUPID);
                employee.EthnicGroup = reader.IsDBNull(FLD_ETHNICGROUP) ? string.Empty : reader.GetString(FLD_ETHNICGROUP);
                employee.BloodGroupLookupId = reader.IsDBNull(FLD_BLOODGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_BLOODGROUPLOOKUPID);
                employee.BloodGroup = reader.IsDBNull(FLD_BLOODGROUP) ? string.Empty : reader.GetString(FLD_BLOODGROUP);
                employee.MaritalStatusLookupId = reader.IsDBNull(FLD_MARITALSTATUSLOOKUPID) ? 0 : reader.GetInt32(FLD_MARITALSTATUSLOOKUPID);
                employee.MaritalStatus = reader.IsDBNull(FLD_MARITALSTATUS) ? string.Empty : reader.GetString(FLD_MARITALSTATUS);
                employee.Relocation = reader.IsDBNull(FLD_RELOCATION) ? false : reader.GetBoolean(FLD_RELOCATION);
                employee.Skills = reader.IsDBNull(FLD_SKILLS) ? string.Empty : reader.GetString(FLD_SKILLS);
                employee.TotalExperienceYears = reader.IsDBNull(FLD_TOTALEXPERIENCEYEARS) ? string.Empty : reader.GetString(FLD_TOTALEXPERIENCEYEARS);
                employee.Availability = reader.IsDBNull(FLD_AVAILABILITY) ? 0 : reader.GetInt32(FLD_AVAILABILITY);
                employee.AvailabilityText = reader.IsDBNull(FLD_AVAILABILITYTEXT) ? string.Empty : reader.GetString(FLD_AVAILABILITYTEXT);
                employee.AvailableDate = reader.IsDBNull(FLD_AVAILABLEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_AVAILABLEDATE);
                employee.CurrentYearlyRate = reader.IsDBNull(FLD_CURRENTYEARLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTYEARLYRATE);
                employee.CurrentYearlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTYEARLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTYEARLYCURRENCYLOOKUPID);
                employee.CurrentYearlyCurrency = reader.IsDBNull(FLD_CURRENTYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTYEARLYCURRENCY);
                employee.CurrentMonthlyRate = reader.IsDBNull(FLD_CURRENTMONTHLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTMONTHLYRATE);
                employee.CurrentMonthlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTMONTHLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTMONTHLYCURRENCYLOOKUPID);
                employee.CurrentMonthlyCurrency = reader.IsDBNull(FLD_CURRENTMONTHLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTMONTHLYCURRENCY);
                employee.CurrentHourlyRate = reader.IsDBNull(FLD_CURRENTHOURLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTHOURLYRATE);
                employee.CurrentHourlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTHOURLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTHOURLYCURRENCYLOOKUPID);
                employee.CurrentHourlyCurrency = reader.IsDBNull(FLD_CURRENTHOURLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTHOURLYCURRENCY);
                employee.ExpectedYearlyRate = reader.IsDBNull(FLD_EXPECTEDYEARLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDYEARLYRATE);
                employee.ExpectedYearlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDYEARLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDYEARLYCURRENCYLOOKUPID);
                employee.ExpectedYearlyCurrency = reader.IsDBNull(FLD_EXPECTEDYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDYEARLYCURRENCY);
                employee.ExpectedMonthlyRate = reader.IsDBNull(FLD_EXPECTEDMONTHLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDMONTHLYRATE);
                employee.ExpectedMonthlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID);
                employee.ExpectedMonthlyCurrency = reader.IsDBNull(FLD_EXPECTEDMONTHLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDMONTHLYCURRENCY);
                employee.ExpectedHourlyRate = reader.IsDBNull(FLD_EXPECTEDHOURLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDHOURLYRATE);
                employee.ExpectedHourlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDHOURLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDHOURLYCURRENCYLOOKUPID);
                employee.ExpectedHourlyCurrency = reader.IsDBNull(FLD_EXPECTEDHOURLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDHOURLYCURRENCY);
                employee.WillingToTravel = reader.IsDBNull(FLD_WILLINGTOTRAVEL) ? false : reader.GetBoolean(FLD_WILLINGTOTRAVEL);
                employee.SalaryTypeLookupId = reader.IsDBNull(FLD_SALARYTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_SALARYTYPELOOKUPID);
                employee.SalaryType = reader.IsDBNull(FLD_SALARYTYPE) ? string.Empty : reader.GetString(FLD_SALARYTYPE);
                employee.JobTypeLookupId = reader.IsDBNull(FLD_JOBTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_JOBTYPELOOKUPID);
                employee.JobType = reader.IsDBNull(FLD_JOBTYPE) ? string.Empty : reader.GetString(FLD_JOBTYPE);
                employee.SecurityClearance = reader.IsDBNull(FLD_SECURITYCLEARANCE) ? false : reader.GetBoolean(FLD_SECURITYCLEARANCE);
                employee.WorkAuthorizationLookupId = reader.IsDBNull(FLD_WORKAUTHORIZATIONLOOKUPID) ? 0 : reader.GetInt32(FLD_WORKAUTHORIZATIONLOOKUPID);
                employee.WorkAuthorization = reader.IsDBNull(FLD_WORKAUTHORIZATION) ? string.Empty : reader.GetString(FLD_WORKAUTHORIZATION);
                employee.CreatorName = reader.IsDBNull(FLD_CREATOR) ? string.Empty : reader.GetString(FLD_CREATOR);
                employee.LastUpdatorName = reader.IsDBNull(FLD_UPDATOR) ? string.Empty : reader.GetString(FLD_UPDATOR);
            }
            else if (strRecordType.ToUpper() == "WORKREPORT")
            {
                employee.Leads = reader.IsDBNull(FLD_LEADS) ? 0 : reader.GetInt32(FLD_LEADS);
                employee.LeadsAmount = reader.IsDBNull(FLD_LEADSAMOUNT) ? 0 : reader.GetDecimal(FLD_LEADSAMOUNT);
                employee.Opportunity = reader.IsDBNull(FLD_OPPORTUNITY) ? 0 : reader.GetInt32(FLD_OPPORTUNITY);
                employee.OpportunityAmount = reader.IsDBNull(FLD_OPPORTUNITYAMOUNT) ? 0 : reader.GetDecimal(FLD_OPPORTUNITYAMOUNT);
                employee.LostOpportunity = reader.IsDBNull(FLD_LOSTOPPORTUNITY) ? 0 : reader.GetInt32(FLD_LOSTOPPORTUNITY);
                employee.LostOpportunityAmount = reader.IsDBNull(FLD_LOSTOPPORTUNITYAMOUNT) ? 0 : reader.GetDecimal(FLD_LOSTOPPORTUNITYAMOUNT);
                employee.SuccessfulSales = reader.IsDBNull(FLD_SUCCESSFULSALES) ? 0 : reader.GetInt32(FLD_SUCCESSFULSALES);
                employee.SuccessfulSalesAmount = reader.IsDBNull(FLD_SUCCESSFULSALESAMOUNT) ? 0 : reader.GetDecimal(FLD_SUCCESSFULSALESAMOUNT);
                employee.Company = reader.IsDBNull(FLD_COMPANY) ? 0 : reader.GetInt32(FLD_COMPANY);
                employee.ProspectiveClient = reader.IsDBNull(FLD_PROSPECTIVECLIENT) ? 0 : reader.GetInt32(FLD_PROSPECTIVECLIENT);
                employee.Client = reader.IsDBNull(FLD_CLIENT) ? 0 : reader.GetInt32(FLD_CLIENT);
                employee.Vendor = reader.IsDBNull(FLD_VENDOR) ? 0 : reader.GetInt32(FLD_VENDOR);
                employee.Partner = reader.IsDBNull(FLD_PARTNER) ? 0 : reader.GetInt32(FLD_PARTNER);
                employee.RequisitionOpen = reader.IsDBNull(FLD_OPENREQUISITION) ? 0 : reader.GetInt32(FLD_OPENREQUISITION);
                employee.RequisitionClosed = reader.IsDBNull(FLD_CLOSEDREQUISITION) ? 0 : reader.GetInt32(FLD_CLOSEDREQUISITION);
                employee.RequisitionSubmitted = reader.IsDBNull(FLD_SUBMITTEDREQUISITION) ? 0 : reader.GetInt32(FLD_SUBMITTEDREQUISITION);
                employee.SuccessfulHire = reader.IsDBNull(FLD_SUCCESSFULHIRE) ? 0 : reader.GetInt32(FLD_SUCCESSFULHIRE);
                employee.UnsuccessfulHire = reader.IsDBNull(FLD_UNSUCCESSFULHIRE) ? 0 : reader.GetInt32(FLD_UNSUCCESSFULHIRE);
                employee.Applicant = reader.IsDBNull(FLD_CANDIDATE) ? 0 : reader.GetInt32(FLD_CANDIDATE);
                employee.Consultant = reader.IsDBNull(FLD_CONSULTANT) ? 0 : reader.GetInt32(FLD_CONSULTANT);
                employee.ConsultantEngaged = reader.IsDBNull(FLD_ENGAGEDCONSULTANT) ? 0 : reader.GetInt32(FLD_ENGAGEDCONSULTANT);
                employee.ConsultantBench = reader.IsDBNull(FLD_BENCHCONSULTANT) ? 0 : reader.GetInt32(FLD_BENCHCONSULTANT);
                employee.HotList = reader.IsDBNull(FLD_HOTLIST) ? 0 : reader.GetInt32(FLD_HOTLIST);
                employee.Invoice = reader.IsDBNull(FLD_INVOICE) ? 0 : reader.GetInt32(FLD_INVOICE);
                employee.InvoiceAmount = reader.IsDBNull(FLD_INVOICEAMOUNT) ? 0 : reader.GetInt32(FLD_INVOICEAMOUNT);
                employee.InvoicePaid = reader.IsDBNull(FLD_INVOICEPAID) ? 0 : reader.GetInt32(FLD_INVOICEPAID);
                employee.InvoicePaidAmount = reader.IsDBNull(FLD_INVOICEPAIDAMOUNT) ? 0 : reader.GetInt32(FLD_INVOICEPAIDAMOUNT);
                employee.Expense = reader.IsDBNull(FLD_EXPENSE) ? 0 : reader.GetInt32(FLD_EXPENSE);
                employee.ExpenseAmount = reader.IsDBNull(FLD_EXPENSEAMOUNT) ? 0 : reader.GetInt32(FLD_EXPENSEAMOUNT);
                employee.ExpensePaid = reader.IsDBNull(FLD_EXPENSEPAID) ? 0 : reader.GetInt32(FLD_EXPENSEPAID);
                employee.ExpensePaidAmount = reader.IsDBNull(FLD_EXPENSEPAIDAMOUNT) ? 0 : reader.GetInt32(FLD_EXPENSEPAIDAMOUNT);
            }
            else if (strRecordType.ToUpper() == "ACTIVITYREPORT")
            {
                employee.Applicant = reader.IsDBNull(26) ? 0 : reader.GetInt32(26);
                employee.Consultant = reader.IsDBNull(27) ? 0 : reader.GetInt32(27);
                employee.Company = reader.IsDBNull(28) ? 0 : reader.GetInt32(28);
                employee.ProspectiveClient = reader.IsDBNull(29) ? 0 : reader.GetInt32(29);
                employee.Client = reader.IsDBNull(30) ? 0 : reader.GetInt32(30);
                employee.Vendor = reader.IsDBNull(31) ? 0 : reader.GetInt32(31);
                employee.Partner = reader.IsDBNull(32) ? 0 : reader.GetInt32(32);
                employee.Leads = reader.IsDBNull(33) ? 0 : reader.GetInt32(33);
                employee.Requisition = reader.IsDBNull(34) ? 0 : reader.GetInt32(34);
            }
            else if (strRecordType.ToUpper() == "LIST")
            {
                employee.PrimaryManager = reader.IsDBNull(FLD_PRIMARYMANAGER) ? string.Empty : reader.GetString(FLD_PRIMARYMANAGER);
                //Code introduced by Prasanth on 10/Jun/2016 Start
                employee.UserName = reader.IsDBNull(FLD_USERNAME) ? string.Empty : reader.GetString(FLD_USERNAME);
                employee.IsLDAP = reader.IsDBNull(FLD_ISLDAP) ? false : reader.GetBoolean(FLD_ISLDAP);
                //******************END***********************
            }
            return employee;
        }
    }
}
