﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmployeeDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             8-Jnue-2009             Nagarathna V.B      Enhancement:10525; added "GetProductivityOverviewByDate"
 *  0.2             15-Dec-2009             Sandeesh            Enhancement:10525;Changes made for Submissions Count.    
 *  0.3             Mar-15-2010             Sudarshan R         Defect ID:12297; Changed made in the method GetPaged(). Changed Current country,state,city to Permanent country,state,city
    0.4             25/May/2016             pravin khot         added- TIMEZONE
 *  0.5             7/June/2016             pravin khot         added - GetByEmailId
 *  0.6             10/June/2016            Prasanth Kumar G    Introduced LDAP
 * -------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections;
using System.Data;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Text;
using TPS360.Common.BusinessEntity;

namespace TPS360.DataAccess
{
    internal sealed class EmployeeDataAccess : BaseDataAccess, IEmployeeDataAccess
    {
        #region Constructors

        public EmployeeDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<Employee> CreateEntityBuilder<Employee>()
        {
            return (new EmployeeBuilder()) as IEntityBuilder<Employee>;
        }


       
        #endregion

        #region  Methods

        EmployeeOverviewDetails IEmployeeDataAccess.GetEmployeeOverviewDetails(int memberid) 
        {
            const string SP = "dbo.EmployeeOverviewDetails_GetById";
            
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                EmployeeOverviewDetails details = new EmployeeOverviewDetails();
                Database.AddInParameter(cmd, "@EmployeeId", DbType.Int32, memberid );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        details.Id = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        details.Name = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
                        details.EmailId = reader.IsDBNull(2) ? string.Empty : reader.GetString(2);
                        details.Mobile = reader.IsDBNull(3) ? string.Empty : reader.GetString(3);
                        details.Location = reader.IsDBNull(4) ? string.Empty : reader.GetString(4);
                        details.AccessRole = reader.IsDBNull(5) ? string.Empty : reader.GetString(5);
                        details.RequisitionCount=reader.IsDBNull(6)? 0:reader.GetInt32(6);
                        details.AssignedCandidateCount = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
                        details.InterviewCount = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                        details.DocumentsCount = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);
                        //Code introduced by Prasanth on 28/May/2016 Start
                        details.UserName = reader.IsDBNull(10) ? string.Empty : reader.GetString(10);
                        details.IsLDAP = reader.IsDBNull(11) ? false : reader.GetBoolean(11);
                        //*****************END*********************88
                        return details;

                    }
                    else
                    {
                        return null;
                    }
                }
            }
        
        }

        System.Collections.Generic.IList<EmployeeProductivity> IEmployeeDataAccess.GetEmployeeProductivityDetails()
        {
            //const string SP = "dbo.SP_Graph_OfferedToJoined";

            //using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            //{
            //    EmployeeProductivity details = new EmployeeProductivity();
            //    //Database.AddInParameter(cmd, "@EmployeeId", DbType.Int32, memberid);

            //    using (IDataReader reader = Database.ExecuteReader(cmd))
            //    {
            //        if (reader.Read())
            //        {
            //            details.Id = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
            //            details.EmployeeName = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
            //            details.NewCandidateCount = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
            //            details.JobCount = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
            //            details.CandidateSourcedCount = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
            //            details.SubmissionCount = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
            //            details.InterviewsCount = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
            //            details.OfferCount = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
            //            details.OfferRejectedCount = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
            //            details.JoinedCount = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);
            //            details.PendingJoiners = reader.IsDBNull(9) ? 0 : reader.GetInt32(10);

            //            return details;

            //        }
            //        else
            //        {
            //            return null;
            //        }
            //    }
            //}
            const string SP = "dbo.SP_Graph_OfferedToJoined";
            string whereClause = "";
            string sdate = "";
            string edate = "";
            StringBuilder sb = new StringBuilder();
            string column = null;
            string value = null;

            whereClause = sb.ToString();
            string @WhereDateClause = "";
            object[] paramValues = new object[] { whereClause, @WhereDateClause };
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                System.Collections.Generic.IList<EmployeeProductivity> response = new System.Collections.Generic.List<EmployeeProductivity>();
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response = (new EmployeeBuilder()).BuildEmployeeProductivityReport(reader); ;

                }

                return response;
            }
        }

        System.Collections.Generic.IList<EmployeeProductivity> IEmployeeDataAccess.GetEmployeePresenttoInterviewRatio()
        {
            const string SP = "dbo.SP_Graph_PresenttoInterview";
            string whereClause = "";          
            StringBuilder sb = new StringBuilder();     
            whereClause = sb.ToString();
            string @WhereDateClause = "";
            object[] paramValues = new object[] { whereClause, @WhereDateClause };
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                System.Collections.Generic.IList<EmployeeProductivity> response = new System.Collections.Generic.List<EmployeeProductivity>();
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response = (new EmployeeBuilder()).BuildEmployeeProductivityReport(reader); ;

                }

                return response;
            }
        }

        System.Collections.Generic.IList<EmployeeProductivity> IEmployeeDataAccess.GetEmployeeTimetoHire()
        {
            const string SP = "dbo.SP_Graph_TimetoHire";
            string whereClause = "";
            StringBuilder sb = new StringBuilder();
            whereClause = sb.ToString();
            string @WhereDateClause = "";
            object[] paramValues = new object[] { whereClause, @WhereDateClause };
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                System.Collections.Generic.IList<EmployeeProductivity> response = new System.Collections.Generic.List<EmployeeProductivity>();
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response = (new EmployeeBuilder()).BuildEmployeeProductivityReport(reader); ;

                }

                return response;
            }
        }

        System.Collections.Generic.IList<EmployeeProductivity> IEmployeeDataAccess.GetJoinedToRejectedByBusiness()
        {
            const string SP = "dbo.SP_Graph_JoinedToRejectedByBusiness";
            string whereClause = "";
            StringBuilder sb = new StringBuilder();
            whereClause = sb.ToString();
            string @WhereDateClause = "";
            object[] paramValues = new object[] { whereClause, @WhereDateClause };
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                System.Collections.Generic.IList<EmployeeProductivity> response = new System.Collections.Generic.List<EmployeeProductivity>();
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response = (new EmployeeBuilder()).BuildEmployeeProductivityReport(reader); ;

                }

                return response;
            }
        }

        System.Collections.Generic.IList<EmployeeProductivity> IEmployeeDataAccess.GetCandidateBySkillId()
        {
            const string SP = "dbo.SP_Graph_CandidateForSkill";
            string whereClause = "";
            StringBuilder sb = new StringBuilder();
            whereClause = sb.ToString();
            string @WhereDateClause = "";
            object[] paramValues = new object[] { whereClause, @WhereDateClause };
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                System.Collections.Generic.IList<EmployeeProductivity> response = new System.Collections.Generic.List<EmployeeProductivity>();
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response = (new EmployeeBuilder()).BuildEmployeeProductivityReport(reader); ;

                }

                return response;
            }
        }

        System.Collections.Generic.IList<EmployeeProductivity> IEmployeeDataAccess.GetJobPostingByMonthYear()
        {
            const string SP = "dbo.SP_Graph_PostingByMonthYear";
            string whereClause = "";
            StringBuilder sb = new StringBuilder();
            whereClause = sb.ToString();
            string @WhereDateClause = "";
            object[] paramValues = new object[] { whereClause, @WhereDateClause };
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                System.Collections.Generic.IList<EmployeeProductivity> response = new System.Collections.Generic.List<EmployeeProductivity>();
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response = (new EmployeeBuilder()).BuildEmployeeProductivityReport(reader); ;

                }

                return response;
            }
        }

        Employee IEmployeeDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Employee_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Employee>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        //************Added by pravin khot on 7/June/2016***************
        Employee IEmployeeDataAccess.GetByEmailId(string  Emailid)
        {
            const string SP = "dbo.Employee_GetByEmailId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@EmailId", DbType.String, Emailid);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Employee>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        //*************************END********************************
      

        Employee IEmployeeDataAccess.GetByIdForHR(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Employee_GetByIdForHR";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Employee>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        Employee IEmployeeDataAccess.GetActivitiesReportByIdAndCreateDate(int id, DateTime createDate)
        {
            
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            if (createDate == null || createDate==DateTime.MinValue)
            {
                createDate = DateTime.Parse("01/01/1800");
            }

            const string SP = "dbo.Employee_GetActivitiesCountByEmployeeIdAndCreatedDate";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);
                Database.AddInParameter(cmd, "@CreateDate", DbType.DateTime, createDate);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Employee>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        PagedResponse<Employee> IEmployeeDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.Employee_GetPaged";
            string whereClause = string.Empty, strbranchmap = string.Empty;
            bool flag = false;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "organizationFunctionalityCategoryMapId"))
                        {
                            sb.Append("[E].[Id]");
                            sb.Append(" IN ");
                            sb.Append(" ( SELECT DISTINCT MemberId FROM MemberOrganizationFunctionalityCategoryMap WHERE OrganizationFunctionalityCategoryMapId = ");
                            sb.Append(value);
                            sb.Append(" ) ");
                        }
                        else if (StringHelper.IsEqual(column, "tierLookupId"))
                        {
                            // sb.Append("[E].[Id]");
                            // sb.Append(" IN ");
                            //// sb.Append(" ( SELECT DISTINCT MemberId FROM MemberTierMap WHERE TierLookupId = ");
                            // sb.Append(" ( SELECT DISTINCT MemberId FROM MemberOrganizationBranchOfficeMap WHERE OrganizationBranchOfficeId = ");
                            // sb.Append(value);
                            // sb.Append(" ) ");
                            strbranchmap = " And TierLookUpId = " + value;

                            flag = true;
                        }
                        else if (StringHelper.IsEqual(column, "memberId"))
                        {
                            sb.Append("[E].[Id]");
                            sb.Append(" IN ");
                            sb.Append(" ( SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId= ");
                            sb.Append(value);
                            sb.Append(" ) ");
                        }
                        else if (StringHelper.IsEqual(column, "CreatorId"))
                        {
                            if (sb.ToString() != string.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[E].[CreatorId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }

                        else if (StringHelper.IsEqual(column, "status"))
                        {
                            if (sb.ToString() != string.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[E].[Status]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else if (StringHelper.IsEqual(column, "quickSearchKeyWord"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            //split the search string by space char. if more than one word, use first word as first name and second word as last name
                            string[] valueTokens = value.Split(' ');
                            if (valueTokens.Length > 1)
                            {
                                sb.Append("([E].[FirstName] LIKE '%" + valueTokens[0] + "%'");
                                sb.Append(" OR ");
                                sb.Append("[E].[LastName] LIKE '%" + valueTokens[1] + "%'");
                                sb.Append(" OR ");
                            }
                            else
                            {
                                sb.Append("([E].[FirstName] LIKE '%" + value + "%'");
                                sb.Append(" OR ");
                                sb.Append("[E].[LastName] LIKE '%" + value + "%'");
                                sb.Append(" OR ");
                            }

                            sb.Append("[E].[MiddleName] LIKE '%" + value + "%'");
                            sb.Append(" OR ");
                            sb.Append("[E].[PrimaryEmail] LIKE '%" + value + "%'");
                            sb.Append(" OR ");
                            sb.Append("[E].[AlternateEmail] LIKE '%" + value + "%')");
                        }

                        if ((flag) && (StringHelper.IsEqual(column, "branchOfficeId")))
                        {
                            sb.Append("[E].[Id]");
                            sb.Append(" IN ");
                            // sb.Append(" ( SELECT DISTINCT MemberId FROM MemberTierMap WHERE TierLookupId = ");
                            sb.Append(" ( SELECT DISTINCT MemberId FROM BranchOfficeMemberTierMap WHERE OrganizationBranchOfficeId = ");
                            sb.Append(value);
                            sb.Append(strbranchmap);
                            sb.Append(" ) ");

                        }
                    }
                }

                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "FirstName";
            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Employee> response = new PagedResponse<Employee>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<Employee>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<Employee> IEmployeeDataAccess.GetPagedWorkReport(PagedRequest request)
        {
            DateTime startDateTime = DateTime.Parse("01/01/1800");
            DateTime endDateTime = DateTime.Parse("01/01/1800");
            const string SP = "dbo.Employee_GetPagedReport";
            string whereClause = string.Empty, strbranchmap = string.Empty;
            //bool flag = false;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "startDate"))
                        {
                            startDateTime = DateTime.Parse(value);
                        }
                        else if (StringHelper.IsEqual(column, "endDate"))
                        {
                            endDateTime = DateTime.Parse(value);
                        }
                        else if (StringHelper.IsEqual(column, "memberId"))
                        {
                            if (sb.ToString() != string.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[E].[Id]");
                            sb.Append(" IN ");
                            sb.Append(" ( SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId= ");
                            sb.Append(value);
                            sb.Append(" ) ");
                        }
                        else if (StringHelper.IsEqual(column, "employeeId"))
                        {
                            if (sb.ToString() != string.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[E].[Id]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else if (StringHelper.IsEqual(column, "status"))
                        {
                            if (sb.ToString() != string.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[E].[Status]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "FirstName";
            }

            request.SortColumn = "[E].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {
                                                    startDateTime,
                                                    endDateTime,
                                                    request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Employee> response = new PagedResponse<Employee>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<Employee>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        EmployeeStatistics IEmployeeDataAccess.GetStatisticsByEmployeeId(int employeeId)
        {
            if (employeeId < 1)
            {
                throw new ArgumentNullException("employeeId");
            }

            const string SP = "dbo.Employee_GetStatisticsByEmployeeId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@EmployeeId", DbType.Int32, employeeId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        EmployeeStatistics employeeStatistics = new EmployeeStatistics();

                        employeeStatistics.NumberOfCreatedLead = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        employeeStatistics.NumberOfAssociatedLead = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                        employeeStatistics.NumberOfCreatedCampaign = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                        employeeStatistics.NumberOfAssociatedCampaign = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                        employeeStatistics.NumberOfCreatedClient = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                        employeeStatistics.NumberOfAssociatedClient = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);

                        employeeStatistics.NumberOfCreatedInvoice = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                        employeeStatistics.EmployeeOpenRequisition = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
                        employeeStatistics.EmployeeSubmittedRequisition = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);                        
                        employeeStatistics.EmployeeSuccessfullPlacement = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);
                        employeeStatistics.OpenRequisition = reader.IsDBNull(10) ? 0 : reader.GetInt32(10);
                        employeeStatistics.EmployeeClosedUnsuccessfulRequisition = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
                        return employeeStatistics;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        PagedResponse<Employee> IEmployeeDataAccess.GetPaged(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, int industry, int functionalCategory, int workPermit,
            int gender, int maritalStatus, int educationId,int assignedManager)
        {
            const string SP = "dbo.Employee_GetPagedReport";
            StringBuilder whereClause = new StringBuilder();
            if (addedFrom != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[E].[CreateDate]");
                whereClause.Append(" >= ");
              //  whereClause.Append("'" + addedFrom.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(addedFrom).ToString("dd/MM/yyyy") + "',103) ");

            }
            if (addedTo != null && addedTo != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[E].[CreateDate]");
                whereClause.Append(" < ");
               //whereClause.Append("'" + addedTo.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(addedTo).AddDays(1).ToString("dd/MM/yyyy") + "',103) ");

            }
            if (addedBy > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[E].[CreatorId]");
                whereClause.Append(" = ");
                whereClause.Append(addedBy.ToString());
            }
            if (updatedFrom != null && updatedFrom != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[E].[UpdateDate]");
                whereClause.Append(" >= ");
               // whereClause.Append("'" + updatedFrom.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(updatedFrom).ToString("dd/MM/yyyy") + "',103) ");

            }
            if (updatedTo != null && updatedTo != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[E].[UpdateDate]");
                whereClause.Append(" < ");
                //whereClause.Append("'" + updatedTo.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(updatedTo).AddDays(1).ToString("dd/MM/yyyy") + "',103) ");

            }
            if (updatedBy > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[E].[UpdatorId]");
                whereClause.Append(" = ");
                whereClause.Append(updatedBy.ToString());
            }

            if (country > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[E].[PermanentCountryId]");//0.3
                whereClause.Append(" = ");
                whereClause.Append(country);
            }
            if (state > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[E].[PermanentStateId]");//0.3
                whereClause.Append(" = ");
                whereClause.Append(state);
            }
            if (!string.IsNullOrEmpty(city))
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[E].[PermanentCity]");//0.3
                whereClause.Append(" LIKE ");
                whereClause.Append("'%" + city + "%'");
            }

            
            StringBuilder experienceWhereBuilder = new StringBuilder();
            if (industry > 0)
            {
                if (!StringHelper.IsBlank(experienceWhereBuilder))
                {
                    experienceWhereBuilder.Append(" AND ");
                }
                experienceWhereBuilder.Append("[E].[IndustryCategoryLookupId]");
                experienceWhereBuilder.Append(" = ");
                experienceWhereBuilder.Append(industry.ToString());
            }

            if (functionalCategory > 0)
            {
                if (!StringHelper.IsBlank(experienceWhereBuilder))
                {
                    experienceWhereBuilder.Append(" AND ");
                }
                experienceWhereBuilder.Append("[E].[FunctionalCategoryLookupId]");
                experienceWhereBuilder.Append(" = ");
                experienceWhereBuilder.Append(functionalCategory.ToString());
            }

            if (!StringHelper.IsBlank(experienceWhereBuilder))
            {
                StringBuilder experienceBuilder = new StringBuilder(@" ( SELECT DISTINCT [ME].[MemberId] FROM
[MemberExperience] [ME]");
                experienceBuilder.Append(" WHERE ");
                experienceBuilder.Append(experienceWhereBuilder);
                experienceBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [E].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(experienceBuilder.ToString());
            }

            if (workPermit > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[E].[WorkAuthorizationLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(workPermit.ToString());
            }

            if (gender > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[E].[GenderLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(gender.ToString());
            }

            if (maritalStatus > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[E].[MaritalStatusLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(maritalStatus.ToString());
            }

            if (educationId > 0)
            {
                StringBuilder educationBuilder = new StringBuilder(@" ( SELECT DISTINCT [ME].[MemberId] FROM
[MemberEducation] [ME]");
                educationBuilder.Append(" WHERE ");
                educationBuilder.Append(" [ME].[LevelOfEducationLookupId] ");
                educationBuilder.Append(" = ");
                educationBuilder.Append(educationId.ToString());
                educationBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [E].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(educationBuilder.ToString());
            }

            //Assigned Manager
            if (assignedManager > 0)
            {
                StringBuilder assignedManagerBuilder = new StringBuilder(@" ( SELECT DISTINCT [MemberId]
                                                FROM [MemberManager] [MM]");
                assignedManagerBuilder.Append(" WHERE ");
                assignedManagerBuilder.Append(" [MM].[ManagerId] ");
                assignedManagerBuilder.Append(" = ");
                assignedManagerBuilder.Append(assignedManager);
                assignedManagerBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [E].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(assignedManagerBuilder.ToString());
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[E].[FirstName]";
            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    String.Empty
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Employee> response = new PagedResponse<Employee>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<Employee>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        BusinessOverview IEmployeeDataAccess.GetBusinessOverviewByEmployeeId(int employeeId)
        {
            if (employeeId < 1)
            {
                throw new ArgumentNullException("employeeId");
            }

            const string SP = "dbo.Employee_GetBusinessOverviewByEmployeeId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@EmployeeId", DbType.Int32, employeeId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        BusinessOverview businessOverview = new BusinessOverview();

                        businessOverview.TodaysApplicantCount = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        businessOverview.LastSevenDaysApplicantCount = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                        businessOverview.LastThirtyDaysApplicantCount = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                        businessOverview.TotalApplicantCount = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);

                        businessOverview.TodaysCompanyCount = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                        businessOverview.LastSevenDaysCompanyCount = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                        businessOverview.LastThirtyDaysCompanyCount = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                        businessOverview.TotalCompanyCount = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);

                        businessOverview.TodaysRequisitionCount = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                        businessOverview.LastSevenDaysRequisitionCount = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);
                        businessOverview.LastThirtyDaysRequisitionCount = reader.IsDBNull(10) ? 0 : reader.GetInt32(10);
                        businessOverview.TotalRequisitionCount = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);

                        businessOverview.TodaysPlacementCount = reader.IsDBNull(12) ? 0 : reader.GetInt32(12);
                        businessOverview.LastSevenDaysPlacementCount = reader.IsDBNull(13) ? 0 : reader.GetInt32(13);
                        businessOverview.LastThirtyDaysPlacementCount = reader.IsDBNull(14) ? 0 : reader.GetInt32(14);
                        businessOverview.TotalPlacementCount = reader.IsDBNull(15) ? 0 : reader.GetInt32(15);

                        businessOverview.TodaysConsultantCount = reader.IsDBNull(16) ? 0 : reader.GetInt32(16);
                        businessOverview.LastSevenDaysConsultantCount = reader.IsDBNull(17) ? 0 : reader.GetInt32(17);
                        businessOverview.LastThirtyDaysConsultantCount = reader.IsDBNull(18) ? 0 : reader.GetInt32(18);
                        businessOverview.TotalConsultantCount = reader.IsDBNull(19) ? 0 : reader.GetInt32(19);

                        businessOverview.TotalTaskCount = reader.IsDBNull(20) ? 0 : reader.GetInt32(20);
                        businessOverview.TotalCompletedTaskCount = reader.IsDBNull(21) ? 0 : reader.GetInt32(21);
                        businessOverview.TotalManagerCount = reader.IsDBNull(22) ? 0 : reader.GetInt32(22);
                        businessOverview.TotalTrainingCount = reader.IsDBNull(23) ? 0 : reader.GetInt32(23);

                        businessOverview.TotalHoursWorked = reader.IsDBNull(24) ? 0 : reader.GetDecimal(24);
                        businessOverview.TotalOverTimeWorked = reader.IsDBNull(25) ? 0 : reader.GetDecimal(25);
                        businessOverview.TotalWorkDays = reader.IsDBNull(26) ? 0 : reader.GetInt32(26);
                        businessOverview.TotalLateDays = reader.IsDBNull(27) ? 0 : reader.GetInt32(27);
                        businessOverview.TotalAbsentdays = reader.IsDBNull(28) ? 0 : reader.GetInt32(28);

                        return businessOverview;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        HireDesk IEmployeeDataAccess.GetHireDeskByEmployeeId(int employeeId)
        {
            if (employeeId < 1)
            {
                throw new ArgumentNullException("employeeId");
            }

            const string SP = "dbo.Employee_GetHireDeskByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, employeeId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        HireDesk hireDesk = new HireDesk();

                        hireDesk.RequisitionCount2Days = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        hireDesk.MyRequisitionCount2Days = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                        hireDesk.RequisitionCount7Days = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                        hireDesk.MyRequisitionCount7Days = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                        hireDesk.RequisitionCount30Days = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                        hireDesk.MyRequisitionCount30Days = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                        hireDesk.RequisitionCount90Days = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                        hireDesk.MyRequisitionCount90Days = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);

                        hireDesk.ApplicantCount2Days = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                        hireDesk.MyApplicantCount2Days = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);
                        hireDesk.ApplicantCount7Days = reader.IsDBNull(10) ? 0 : reader.GetInt32(10);
                        hireDesk.MyApplicantCount7Days = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
                        hireDesk.ApplicantCount30Days = reader.IsDBNull(12) ? 0 : reader.GetInt32(12);
                        hireDesk.MyRequisitionCount30Days = reader.IsDBNull(13) ? 0 : reader.GetInt32(13);
                        hireDesk.ApplicantCount90Days = reader.IsDBNull(14) ? 0 : reader.GetInt32(14);
                        hireDesk.MyApplicantCount90Days = reader.IsDBNull(15) ? 0 : reader.GetInt32(15);

                        hireDesk.ReviewedApplicantCount2Days = reader.IsDBNull(16) ? 0 : reader.GetInt32(16);
                        hireDesk.ReviewedApplicantCount7Days = reader.IsDBNull(17) ? 0 : reader.GetInt32(17);
                        hireDesk.ReviewedApplicantCount30Days = reader.IsDBNull(18) ? 0 : reader.GetInt32(18);
                        hireDesk.ReviewedApplicantCount90Days = reader.IsDBNull(19) ? 0 : reader.GetInt32(19);

                        hireDesk.LevelIApplicantCount2Days = reader.IsDBNull(20) ? 0 : reader.GetInt32(20);
                        hireDesk.LevelIApplicantCount7Days = reader.IsDBNull(21) ? 0 : reader.GetInt32(21);
                        hireDesk.LevelIApplicantCount30Days = reader.IsDBNull(22) ? 0 : reader.GetInt32(22);
                        hireDesk.LevelIApplicantCount90Days = reader.IsDBNull(23) ? 0 : reader.GetInt32(23);

                        hireDesk.LevelIIApplicantCount2Days = reader.IsDBNull(24) ? 0 : reader.GetInt32(24);
                        hireDesk.LevelIIApplicantCount7Days = reader.IsDBNull(25) ? 0 : reader.GetInt32(25);
                        hireDesk.LevelIIApplicantCount30Days = reader.IsDBNull(26) ? 0 : reader.GetInt32(26);
                        hireDesk.LevelIIApplicantCount90Days = reader.IsDBNull(27) ? 0 : reader.GetInt32(27);

                        hireDesk.LevelIIIApplicantCount2Days = reader.IsDBNull(28) ? 0 : reader.GetInt32(28);
                        hireDesk.LevelIIIApplicantCount7Days = reader.IsDBNull(29) ? 0 : reader.GetInt32(29);
                        hireDesk.LevelIIIApplicantCount30Days = reader.IsDBNull(30) ? 0 : reader.GetInt32(30);
                        hireDesk.LevelIIIApplicantCount90Days = reader.IsDBNull(31) ? 0 : reader.GetInt32(31);

                        hireDesk.OfferLettersCount2Days = reader.IsDBNull(32) ? 0 : reader.GetInt32(32);
                        hireDesk.OfferLettersCount7Days = reader.IsDBNull(33) ? 0 : reader.GetInt32(33);
                        hireDesk.OfferLettersCount30Days = reader.IsDBNull(34) ? 0 : reader.GetInt32(34);
                        hireDesk.OfferLettersCount90Days = reader.IsDBNull(35) ? 0 : reader.GetInt32(35);

                        hireDesk.HiredCount2Days = reader.IsDBNull(36) ? 0 : reader.GetInt32(36);
                        hireDesk.HiredCount7Days = reader.IsDBNull(37) ? 0 : reader.GetInt32(37);
                        hireDesk.HiredCount30Days = reader.IsDBNull(38) ? 0 : reader.GetInt32(38);
                        hireDesk.HiredCount90Days = reader.IsDBNull(39) ? 0 : reader.GetInt32(39);

                        hireDesk.RejectedCount2Days = reader.IsDBNull(40) ? 0 : reader.GetInt32(40);
                        hireDesk.RejectedCount7Days = reader.IsDBNull(41) ? 0 : reader.GetInt32(41);
                        hireDesk.RejectedCount30Days = reader.IsDBNull(42) ? 0 : reader.GetInt32(42);
                        hireDesk.RejectedCount90Days = reader.IsDBNull(43) ? 0 : reader.GetInt32(43);

                        hireDesk.TotalSuccessfulHire = reader.IsDBNull(44) ? 0 : reader.GetInt32(44);
                        hireDesk.TotalRequisitionProcessed = reader.IsDBNull(45) ? 0 : reader.GetInt32(45);

                        return hireDesk;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        
        // 0.1 starts
        DataTable IEmployeeDataAccess.GetProductivityOverviewByDate(string RequireDate)
        {
            if (RequireDate.Equals(string.Empty))
            {
                throw new ArgumentException("RequireDate");
            }

            const string SP = "dbo.Employee_GetProductivityOverviewbyDate";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RequireDate", DbType.String, RequireDate);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                   DataTable ProductivityCount = new DataTable("dTab");
                   ProductivityCount.Columns.Add("Id");
                   ProductivityCount.Columns.Add("EId");
                   ProductivityCount.Columns.Add("EName");
                   ProductivityCount.Columns.Add("Requistion");
                   ProductivityCount.Columns.Add("Preselected");
                   ProductivityCount.Columns.Add("Level1");
                   ProductivityCount.Columns.Add("Level2");
                   ProductivityCount.Columns.Add("Level3");
                   ProductivityCount.Columns.Add("FinalHire");
                   ProductivityCount.Columns.Add("Submissions");
                   ProductivityCount.Columns.Add("Clients");

                   while (reader.Read())
                    {
                        DataRow dRow = ProductivityCount.NewRow();
                        dRow["Id"] = reader.GetInt32(0);
                        dRow["EId"] = reader.GetInt32(1);
                        dRow["EName"] = reader.GetString(2);
                        dRow["Requistion"] = reader.GetInt32(3);
                        dRow["Preselected"] = reader.GetInt32(4);
                        dRow["Level1"] = reader.GetInt32(5);
                        dRow["Level2"] = reader.GetInt32(6);
                        dRow["Level3"] = reader.GetInt32(7);
                        dRow["FinalHire"] = reader.GetInt32(8);
                        //dRow["Submissions"] = reader.GetInt32(9);//0.2
                        dRow["Submissions"] = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);//0.2
                        dRow["Clients"] = reader.GetInt32(10);
                       
                        ProductivityCount.Rows.Add(dRow);
                    }
                    return ProductivityCount;
                }
            }
        }
        //0.1 ends

       PagedResponse<EmployeeProductivity > IEmployeeDataAccess.GetPagedEmployeeProductivity(DateTime StartDate,DateTime EndDate, int MemberId,PagedRequest request)
        {

           
            const string SP = "dbo.Employee_GetProductivity";
            string whereClause = "";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                   NullConverter .Convert (StartDate ),
                                                    NullConverter .Convert (EndDate ),
                                                    MemberId 

												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<EmployeeProductivity > response = new PagedResponse<EmployeeProductivity >();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new EmployeeBuilder()).BuildEmployeeProductivity(reader); 

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }


       PagedResponse<EmployeeProductivity> IEmployeeDataAccess.GetPagedByProductityReport(PagedRequest request)
       {
           const string SP = "dbo.Employee_GetPagedByProductivity";
           string whereClause = "";
           StringBuilder sb = new StringBuilder();
           StringBuilder sbdate = new StringBuilder();
           string column = null;
           string value = null;

           string sdate = "";
           string edate = "";
           foreach (DictionaryEntry entry in request.Conditions)
           {
               column = StringHelper.Convert(entry.Key);
               value = StringHelper.Convert(entry.Value);

               if (!StringHelper.IsBlank(column))
               {
                   value = value.Replace("'", "''");

                   if (StringHelper.IsEqual(column, "MemberId"))
                   {
                       if (sb.ToString() != "") sb.Append(" And ");
                       sb.Append(" E.ID in (" + value + ")");
                   }
                   if (StringHelper.IsEqual(column, "TeamID"))
                   {
                       if (sb.ToString() != "") sb.Append(" And ");
                       sb.Append(" E.Id in (select EmployeeID from EmployeeTeamDetail where EmployeeTeamID in ("+value+"))");
                   }

                   if (StringHelper.IsEqual(column, "StartDate"))
                   {


                       sdate = "Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ";
                   }
                   if (StringHelper.IsEqual(column, "EndDate"))
                   {
                       edate = "Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ";
                   }
               }
           }
           whereClause = sb.ToString();
           string WhereDateClause = " between " + sdate + " And " + edate ;
           object[] paramValues = new object[] { 
                                                request.PageIndex,
												request.RowPerPage,
                                                 whereClause ,
                                                WhereDateClause ,
                                                StringHelper.Convert(request.SortColumn),
                                                StringHelper.Convert(request.SortOrder)
                                                };
           using (DbCommand cmd = Database.GetStoredProcCommand(SP,paramValues )) 
           {
               PagedResponse<EmployeeProductivity> response = new PagedResponse<EmployeeProductivity>();
               
               using (IDataReader reader = Database.ExecuteReader(cmd)) 
               {
                   response.Response = (new EmployeeBuilder()).BuildEmployeeProductivityReport(reader);
                   if ((reader.NextResult()) && (reader.Read())) 
                   {
                       response.TotalRow = reader.GetInt32(0);
                   
                   }
               
               }
               return response;
              
           }
         
       }



       PagedResponse<EmployeeProductivity> IEmployeeDataAccess.GetPagedByTeamProductityReport(PagedRequest request) 
       {
           const string SP = "dbo.Employee_GetPagedByProductivityByTeam";
           string whereClause = "";
           StringBuilder sb = new StringBuilder();
           StringBuilder sbdate = new StringBuilder();
           string column = null;
           string value = null;

           string sdate = "";
           string edate = "";
           string UserId="";
           foreach (DictionaryEntry entry in request.Conditions)
           {
               column = StringHelper.Convert(entry.Key);
               value = StringHelper.Convert(entry.Value);

               if (!StringHelper.IsBlank(column))
               {
                   value = value.Replace("'", "''");

                   if (StringHelper.IsEqual(column, "MemberId"))
                   {
                       UserId =value ;
                   }
                   if (StringHelper.IsEqual(column, "TeamID"))
                   {
                       if (sb.ToString() != "") sb.Append(" And ");
                       sb.Append(" ET.ID in (" + value + ")");

                   }

                   if (StringHelper.IsEqual(column, "StartDate"))
                   {


                       sdate = "Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ";
                   }
                   if (StringHelper.IsEqual(column, "EndDate"))
                   {
                       edate = "Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ";
                   }
               }
           }
           whereClause = sb.ToString();
           string WhereDateClause = " between " + sdate + " And " + edate;

           object[] paramValues = new object[] { 
                                                request.PageIndex,
												request.RowPerPage,
                                                WhereDateClause ,
                                                UserId  ,
                                                whereClause,
                                                StringHelper.Convert(request.SortColumn),
                                                StringHelper.Convert(request.SortOrder)
                                                };
           using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
           {

               PagedResponse<EmployeeProductivity> response = new PagedResponse<EmployeeProductivity>();
               using (IDataReader reader = Database.ExecuteReader(cmd)) 
               {
                   response.Response = (new EmployeeBuilder()).BuildEmployeeProductivityReport(reader);
                   if ((reader.NextResult()) && (reader.Read()))
                   {
                       response.TotalRow = reader.GetInt32(0);

                   }
               
               }
               return response;
           }
       
       }


        DataTable IEmployeeDataAccess.GetMyProductivityOverviewByDate(string RequireDate, int MemberId)
        {
            if (RequireDate.Equals(string.Empty))
            {
                throw new ArgumentException("RequireDate");
            }

            if (MemberId.Equals(null))
            {
                throw new ArgumentException("MemberId");
            }

            const string SP = "dbo.Employee_GetMyProductivityOverviewbyDate";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RequireDate", DbType.String, RequireDate);
                Database.AddInParameter(cmd, "@EmpId", DbType.Int32, MemberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    DataTable ProductivityCount = new DataTable("dTab");
                    ProductivityCount.Columns.Add("Id");
                    ProductivityCount.Columns.Add("EId");
                    ProductivityCount.Columns.Add("EName");
                    ProductivityCount.Columns.Add("Requistion");
                    ProductivityCount.Columns.Add("Preselected");
                    ProductivityCount.Columns.Add("Level1");
                    ProductivityCount.Columns.Add("Level2");
                    ProductivityCount.Columns.Add("Level3");
                    ProductivityCount.Columns.Add("FinalHire");
                    ProductivityCount.Columns.Add("Submissions");
                    ProductivityCount.Columns.Add("Clients");

                    while (reader.Read())
                    {
                        DataRow dRow = ProductivityCount.NewRow();
                        dRow["Id"] = reader.GetInt32(0);
                        dRow["EId"] = reader.GetInt32(1);
                        dRow["EName"] = reader.GetString(2);
                        dRow["Requistion"] = reader.GetInt32(3);
                        dRow["Preselected"] = reader.GetInt32(4);
                        dRow["Level1"] = reader.GetInt32(5);
                        dRow["Level2"] = reader.GetInt32(6);
                        dRow["Level3"] = reader.GetInt32(7);
                        dRow["FinalHire"] = reader.GetInt32(8);
                       // dRow["Submissions"] = reader.GetInt32(9);//0.2
                        dRow["Submissions"] = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);//0.2
                        dRow["Clients"] = reader.GetInt32(10);

                        ProductivityCount.Rows.Add(dRow);
                    }
                    return ProductivityCount;
                }
            }

                 
        }

        void IEmployeeDataAccess.EmployeeFirstLogin_Create(int MemberID)
        {
            const string SP = "dbo.Employee_EmployeeFirstLogin_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberID", DbType.Int32, MemberID);

                Database.ExecuteNonQuery(cmd);
            }
        }

        bool IEmployeeDataAccess.IsEmployeeFirstLogin(int MemberID)
        {
            const string SP = "dbo.Employee_IsEmployeeFirstLogin";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return reader.IsDBNull(0) ? false : reader.GetBoolean(0);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }



        
         System .Collections .Generic . IList <EmployeeProductivity> IEmployeeDataAccess.GetEmployeeProductivityReport(PagedRequest request)
        {


            const string SP = "dbo.Employee_GetProductivityReport";
            string whereClause = "";
            string sdate = "";
            string edate = "";
            StringBuilder sb = new StringBuilder();
            string column = null;
            string value = null;
            foreach (DictionaryEntry entry in request.Conditions)
            {
                column = StringHelper.Convert(entry.Key);
                value = StringHelper.Convert(entry.Value);

                if (!StringHelper.IsBlank(column))
                {
                    value = value.Replace("'", "''");

                    if (StringHelper.IsEqual(column, "MemberId"))
                    {
                        if (sb.ToString() != "") sb.Append(" And ");
                        sb.Append(" E.ID in (" + value + ")");
                    }
                    if (StringHelper.IsEqual(column, "TeamID"))
                    {
                        if (sb.ToString() != "") sb.Append(" And ");
                        sb.Append(" E. ID in (select EmployeeID from EmployeeTeamDetail where EmployeeTeamId in (" + value + "))");
                    }

                    if (StringHelper.IsEqual(column, "StartDate"))
                    {


                        sdate = "Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ";
                    }
                    if (StringHelper.IsEqual(column, "EndDate"))
                    {
                        edate = "Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ";
                    }
                }
            }
            whereClause = sb.ToString();
            string @WhereDateClause = " between " + sdate + " And " + edate ;
            object[] paramValues = new object[] { whereClause, @WhereDateClause };
            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                System .Collections .Generic .IList <EmployeeProductivity> response = new  System .Collections .Generic .List <EmployeeProductivity>();
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response = (new EmployeeBuilder()).BuildEmployeeProductivityReport (reader); ;
                                      
                }

                return response;
            }





            //using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            //{
            //    Database.AddInParameter(cmd, "@RequireDate", DbType.String, RequireDate);
            //    Database.AddInParameter(cmd, "@EmpId", DbType.Int32, MemberId);

            //    using (IDataReader reader = Database.ExecuteReader(cmd))
            //    {
            //        DataTable ProductivityCount = new DataTable("dTab");
            //        ProductivityCount.Columns.Add("Id");
            //        ProductivityCount.Columns.Add("EId");
            //        ProductivityCount.Columns.Add("EName");
            //        ProductivityCount.Columns.Add("Requistion");
            //        ProductivityCount.Columns.Add("Preselected");
            //        ProductivityCount.Columns.Add("Level1");
            //        ProductivityCount.Columns.Add("Level2");
            //        ProductivityCount.Columns.Add("Level3");
            //        ProductivityCount.Columns.Add("FinalHire");
            //        ProductivityCount.Columns.Add("Submissions");
            //        ProductivityCount.Columns.Add("Clients");

            //        while (reader.Read())
            //        {
            //            DataRow dRow = ProductivityCount.NewRow();
            //            dRow["Id"] = reader.GetInt32(0);
            //            dRow["EId"] = reader.GetInt32(1);
            //            dRow["EName"] = reader.GetString(2);
            //            dRow["Requistion"] = reader.GetInt32(3);
            //            dRow["Preselected"] = reader.GetInt32(4);
            //            dRow["Level1"] = reader.GetInt32(5);
            //            dRow["Level2"] = reader.GetInt32(6);
            //            dRow["Level3"] = reader.GetInt32(7);
            //            dRow["FinalHire"] = reader.GetInt32(8);
            //            // dRow["Submissions"] = reader.GetInt32(9);//0.2
            //            dRow["Submissions"] = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);//0.2
            //            dRow["Clients"] = reader.GetInt32(10);

            //            ProductivityCount.Rows.Add(dRow);
            //        }
            //        return ProductivityCount;
            //    }
            //}
        }
         

         ArrayList IEmployeeDataAccess.GetAllEmployeeByTeamId(string id) 
         {
             const string SP = "dbo.Employee_GetAllEmployeeByTeamId";
             using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
             {
                 Database.AddInParameter(cmd, "@Id", DbType.String, id);
             using(IDataReader reader=Database.ExecuteReader(cmd))
             {
                 ArrayList result = new ArrayList();
                 while (reader.Read())
                 {
                     result .Add 
                         (new Employee(){Id =reader.GetInt32(0),FirstName=reader.GetString(1)});
                 }
                 return result;
             }
             return null;
             }
         
         }

         System.Collections.Generic.IList<EmployeeProductivity > IEmployeeDataAccess.GetAllListGroupByDate(string UserIds, string TeamIds, int StartDate, int EndDate)
         {
             const string SP = "dbo.EmployeeProductivity_GetReportByDay";
             StringBuilder WhereClause = new StringBuilder();
             if (UserIds  !="")
             {
                 WhereClause.Append(" JobPostingId=" + UserIds );
             }
             if (TeamIds !="")
             {
                 if (WhereClause.ToString() != string.Empty) WhereClause.Append(" And ");
                 WhereClause.Append(" [EmployeeReferrerId] = " + TeamIds );
             }
             if (StartDate > 0)
             {
                 if (WhereClause.ToString() != string.Empty) WhereClause.Append(" And ");
                 WhereClause.Append(" [intCreateDate] >= " + StartDate);
             }
             if (EndDate > 0)
             {
                 if (WhereClause.ToString() != string.Empty) WhereClause.Append(" And ");
                 WhereClause.Append(" [intCreateDate] <= " + EndDate);
             }

             using (DbCommand cmd = Database.GetStoredProcCommand(SP))
             {
                 Database.AddInParameter(cmd, "@TeamId", DbType.AnsiString, StringHelper.Convert(TeamIds ));
                 Database.AddInParameter(cmd, "@UserID", DbType.AnsiString, StringHelper.Convert(UserIds ));
                 Database.AddInParameter(cmd, "@StartDate", DbType.AnsiString, StringHelper.Convert(StartDate ));
                 Database.AddInParameter(cmd, "@EndDate", DbType.AnsiString, StringHelper.Convert(EndDate ));
                 using (IDataReader reader = Database.ExecuteReader(cmd))
                 {
                         return  (new EmployeeBuilder()).BuildEmployeeProductivityReport(reader); ;
                 }
             }
         }

         ArrayList IEmployeeDataAccess.GetEmployeeNameandContactNumber(int EmployeeId) 
         {
             string[] arrEmp = null;
             const string SP = "dbo.Employee_GetEmployeeNameAndContactNumber";
             using (DbCommand cmd = Database.GetStoredProcCommand(SP))
             {
                 Database.AddInParameter(cmd, "@EmployeeId", DbType.String, EmployeeId);
                 try 
                 {
                     using (IDataReader reader = Database.ExecuteReader(cmd))
                     {
                         ArrayList result = new ArrayList();
                         while (reader.Read())
                         {
                             result.Add(new Employee() { FirstName = reader.IsDBNull(0) ? string.Empty : reader.GetString(0), PrimaryPhone = reader.IsDBNull(1) ? string.Empty : reader.GetString(1) });
                         }
                         return result;
                     }
                
                    
                 }
                 catch (Exception ex) { }
                 return null;
             }
         }

        //***********************ADDED BY PRAVIN KHOT ON 25/May/2016 TIMEZONE*******************

         ArrayList IEmployeeDataAccess.GetAllTimezone()
         {
             const string SP = "dbo.Employee_GetAllTimeZone";
             using (DbCommand cmd = Database.GetStoredProcCommand(SP))
             {
                 try
                 {
                     using (IDataReader reader = Database.ExecuteReader(cmd))
                     {
                         ArrayList result = new ArrayList();
                         while (reader.Read())
                         {
                             result.Add(new Employee() { Timezoneid = reader.IsDBNull(0) ? 0 : reader.GetInt32(0), TimeZone = reader.IsDBNull(1) ? string.Empty : reader.GetString(1) });
                         }
                         return result;
                     }


                 }
                 catch (Exception ex) { }
                 return null;
             }

         }
         void IEmployeeDataAccess.Employee_SaveTimezone(int MemberId, int TimezoneId)
         {
             const string SP = "dbo.Employee_SaveTimeZone";

             using (DbCommand cmd = Database.GetStoredProcCommand(SP))
             {
                 Database.AddInParameter(cmd, "@MemberID", DbType.Int32, MemberId);
                 Database.AddInParameter(cmd, "@TimeZone", DbType.Int32, TimezoneId);

                 Database.ExecuteNonQuery(cmd);

             }
         }

         Employee IEmployeeDataAccess.GetTimeZoneByMemberId(int MemberId)
         {
             const string SP = "dbo.Employee_GetTimeZoneByMemberId";
             using (DbCommand cmd = Database.GetStoredProcCommand(SP))
             {
                 Database.AddInParameter(cmd, "@MemberID", DbType.Int32, MemberId);
                 try
                 {
                     using (IDataReader reader = Database.ExecuteReader(cmd))
                     {
                         if (reader.Read())
                         {
                             return (new Employee() { Timezoneid = reader.IsDBNull(0) ? 0 : reader.GetInt32(0), TimeZone = reader.IsDBNull(1) ? string.Empty : reader.GetString(1) });
                         }
                         else
                         {
                             return null;
                         }
                     }
                 }
                 catch (Exception ex) { }

                 return null;
             }
         }
        //************************************END*********************************

        #endregion
    }
}