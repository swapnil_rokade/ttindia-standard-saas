﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:PasswordResetDataAccess.cs
    Description: This page is used for Countries  functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetPasswordResetIdByPasswordResetCode() to assist 
                                                                Resume parser team
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class PasswordResetDataAccess : BaseDataAccess, IPasswordResetDataAccess
    {
        #region Constructors

        public PasswordResetDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder< PasswordReset > CreateEntityBuilder<PasswordReset>()
        {
            return (new PasswordResetBuilder()) as IEntityBuilder<PasswordReset>;
        }

        #endregion

        #region  Methods

        PasswordReset IPasswordResetDataAccess.Add(PasswordReset PasswordReset)
        {
            const string SP = "dbo.Member_PasswordResetLink_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberID", DbType.Int32 , Convert .ToInt32 (PasswordReset .MemberID ));
                Database.AddInParameter(cmd, "@Link", DbType.AnsiString, StringHelper.Convert(PasswordReset.Link ));
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        PasswordReset = CreateEntityBuilder<PasswordReset>().BuildEntity(reader);
                    }
                    else
                    {
                        PasswordReset = null;
                    }
                }

                if (PasswordReset == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("PasswordReset already exists. Please specify another PasswordReset.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this PasswordReset.");
                            }
                    }
                }

                return PasswordReset;
            }
        }

  

        PasswordReset IPasswordResetDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Member_PasswordResetRequests_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<PasswordReset>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        PasswordReset IPasswordResetDataAccess.Update(PasswordReset PasswordReset)
        {
            const string SP = "dbo.Member_PasswordResetLink_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ID", DbType.Int32, Convert.ToInt32(PasswordReset.Id ));
                Database.AddInParameter(cmd, "@MemberID", DbType.Int32, Convert.ToInt32(PasswordReset.MemberID));
                Database.AddInParameter(cmd, "@Link", DbType.AnsiString, StringHelper.Convert(PasswordReset.Link));
                Database.AddInParameter(cmd, "@IsValid", DbType.Int32, Convert.ToInt32(PasswordReset.IsValid ));

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        PasswordReset = CreateEntityBuilder<PasswordReset>().BuildEntity(reader);
                    }
                    else
                    {
                        PasswordReset = null;
                    }
                }

                if (PasswordReset == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("PasswordReset already exists. Please specify another PasswordReset.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this PasswordReset.");
                            }
                    }
                }

                return PasswordReset;
            }
        }


  
        

    
        #endregion
    }
}