﻿using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class PasswordResetBuilder : IEntityBuilder<PasswordReset>
    {
        IList<PasswordReset> IEntityBuilder<PasswordReset>.BuildEntities(IDataReader reader)
        {
            List<PasswordReset> PasswordResets = new List<PasswordReset>();

            while (reader.Read())
            {
                PasswordResets.Add(((IEntityBuilder<PasswordReset>)this).BuildEntity(reader));
            }

            return (PasswordResets.Count > 0) ? PasswordResets : null;
        }

        PasswordReset IEntityBuilder<PasswordReset>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MemberID = 1;
            const int FLD_LNK = 2;
            const int FLD_REQUESTDATE = 3;
            const int FLD_EXPIREDDATE = 4;
            const int FLD_ISVALID = 5;

            PasswordReset PasswordReset = new PasswordReset();

            PasswordReset.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            PasswordReset.MemberID = reader.IsDBNull(FLD_MemberID ) ? 0 : reader.GetInt32(FLD_MemberID );
            PasswordReset.Link  = reader.IsDBNull(FLD_LNK ) ? string.Empty : reader.GetString(FLD_LNK );
            PasswordReset.RequestDate = reader.IsDBNull(FLD_REQUESTDATE) ? System.DateTime.MinValue : reader.GetDateTime(FLD_REQUESTDATE);
            PasswordReset.ExpiredDate  = reader.IsDBNull(FLD_EXPIREDDATE) ? System.DateTime.MinValue : reader.GetDateTime(FLD_EXPIREDDATE);
            PasswordReset.IsValid = reader.IsDBNull(FLD_ISVALID) ? 0 : reader.GetInt32(FLD_ISVALID);
           
            return PasswordReset;
        }
    }
}