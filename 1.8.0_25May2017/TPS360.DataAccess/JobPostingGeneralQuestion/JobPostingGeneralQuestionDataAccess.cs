﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingGeneralQuestionDataAccess : BaseDataAccess, IJobPostingGeneralQuestionDataAccess
    {
        #region Constructors

        public JobPostingGeneralQuestionDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<JobPostingGeneralQuestion> CreateEntityBuilder<JobPostingGeneralQuestion>()
        {
            return (new JobPostingGeneralQuestionBuilder()) as IEntityBuilder<JobPostingGeneralQuestion>;
        }

        #endregion

        #region  Methods

        JobPostingGeneralQuestion IJobPostingGeneralQuestionDataAccess.Add(JobPostingGeneralQuestion jobPostingGeneralQuestion)
        {
            const string SP = "dbo.JobPostingGeneralQuestion_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Question", DbType.AnsiString, StringHelper.Convert(jobPostingGeneralQuestion.Question));
                Database.AddInParameter(cmd, "@ScreeningScore", DbType.Int32, jobPostingGeneralQuestion.ScreeningScore);
                Database.AddInParameter(cmd, "@QuestionOrder", DbType.Int32, jobPostingGeneralQuestion.QuestionOrder);
                Database.AddInParameter(cmd, "@QuestionType", DbType.AnsiString, StringHelper.Convert(jobPostingGeneralQuestion.QuestionType));
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingGeneralQuestion.JobPostingId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, jobPostingGeneralQuestion.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPostingGeneralQuestion = CreateEntityBuilder<JobPostingGeneralQuestion>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPostingGeneralQuestion = null;
                    }
                }

                if (jobPostingGeneralQuestion == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting general question already exists. Please specify another job posting general question .");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this job posting general question .");
                            }
                    }
                }

                return jobPostingGeneralQuestion;
            }
        }

        JobPostingGeneralQuestion IJobPostingGeneralQuestionDataAccess.Update(JobPostingGeneralQuestion jobPostingGeneralQuestion)
        {
            const string SP = "dbo.JobPostingGeneralQuestion_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, jobPostingGeneralQuestion.Id);
                Database.AddInParameter(cmd, "@Question", DbType.AnsiString, StringHelper.Convert(jobPostingGeneralQuestion.Question));
                Database.AddInParameter(cmd, "@ScreeningScore", DbType.Int32, jobPostingGeneralQuestion.ScreeningScore);
                Database.AddInParameter(cmd, "@QuestionOrder", DbType.Int32, jobPostingGeneralQuestion.QuestionOrder);
                Database.AddInParameter(cmd, "@QuestionType", DbType.AnsiString, StringHelper.Convert(jobPostingGeneralQuestion.QuestionType));
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, jobPostingGeneralQuestion.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPostingGeneralQuestion = CreateEntityBuilder<JobPostingGeneralQuestion>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPostingGeneralQuestion = null;
                    }
                }

                if (jobPostingGeneralQuestion == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting general question  already exists. Please specify another job posting general question .");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this job posting general question .");
                            }
                    }
                }

                return jobPostingGeneralQuestion;
            }
        }

        JobPostingGeneralQuestion IJobPostingGeneralQuestionDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingGeneralQuestion_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingGeneralQuestion>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<JobPostingGeneralQuestion> IJobPostingGeneralQuestionDataAccess.GetAll()
        {
            const string SP = "dbo.JobPostingGeneralQuestion_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingGeneralQuestion>().BuildEntities(reader);
                }
            }
        }

        bool IJobPostingGeneralQuestionDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingGeneralQuestion_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting general question which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this job posting general question .");
                        }
                }
            }
        }

        IList<JobPostingGeneralQuestion> IJobPostingGeneralQuestionDataAccess.GetAllByJobPostingId(int jobPostingId, string questionType)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentException("jobPostingId");
            }

            const string SP = "dbo.JobPostingGeneralQuestion_GetJobPostingGeneralQuestionByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);
                Database.AddInParameter(cmd, "@QuestionType", DbType.AnsiString, questionType);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingGeneralQuestion>().BuildEntities(reader);
                }
            }
        }

        bool IJobPostingGeneralQuestionDataAccess.DeleteByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "dbo.JobPostingGeneralQuestion_DeleteJobPostingGeneralQuestionByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting general question  which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this job posting general question .");
                        }
                }
            }
        }

        #endregion
    }
}