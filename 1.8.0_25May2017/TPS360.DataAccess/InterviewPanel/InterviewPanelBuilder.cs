﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewPanelBuilder.cs
    Description         :   This page is used Call the DB Table Column for Interview Panel.
    Created By          :   Pravin khot
    Created On          :   27/Nov/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using System.Data;

namespace TPS360.DataAccess
{
    internal sealed class InterviewPanelBuilder : IEntityBuilder<InterviewPanel>
    {
        IList<InterviewPanel> IEntityBuilder<InterviewPanel>.BuildEntities(IDataReader reader)
        {
            List<InterviewPanel> interFeedback = new List<InterviewPanel>();

            while (reader.Read())
            {
                interFeedback.Add(((IEntityBuilder<InterviewPanel>)this).BuildEntity(reader));
            }

            return (interFeedback.Count > 0) ? interFeedback : null;
        }

        InterviewPanel IEntityBuilder<InterviewPanel>.BuildEntity(IDataReader reader) 
        {
                    
            const int FLD_INTERVIEWPANEL_ID = 0;
            const int FLD_INTERVIEWPANELTYPE_ID = 1;
           
            const int FLD_INTERVIEWNAME = 2;
            const int FLD_INTERVIEWMODE = 3;

            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_CREATEDATE = 4;
            const int FLD_UPDATEDATE = 5;
            const int FLD_SKILL = 8;
            const int FLD_PANELNAME = 9;


            InterviewPanel InterviewPanel = new InterviewPanel();

            InterviewPanel.InterviewPanel_Id = reader.IsDBNull(FLD_INTERVIEWPANEL_ID) ? 0 : reader.GetInt32(FLD_INTERVIEWPANEL_ID);
            InterviewPanel.InterviewPanelType_Id = reader.IsDBNull(FLD_INTERVIEWPANELTYPE_ID) ? 0 : reader.GetInt32(FLD_INTERVIEWPANELTYPE_ID);

            InterviewPanel.Interviewer_Name = reader.IsDBNull(FLD_INTERVIEWNAME) ? string.Empty : reader.GetString(FLD_INTERVIEWNAME);
         
            InterviewPanel.Interview_Mode = reader.IsDBNull(FLD_INTERVIEWMODE) ? string.Empty : reader.GetString(FLD_INTERVIEWMODE);

            InterviewPanel.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            InterviewPanel.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            InterviewPanel.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            InterviewPanel.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            InterviewPanel.InterviewSkill = reader.IsDBNull(FLD_SKILL) ? string.Empty : reader.GetString(FLD_SKILL);
            InterviewPanel.InterviewPanel_Name = reader.IsDBNull(FLD_PANELNAME) ? string.Empty : reader.GetString(FLD_PANELNAME);
                   
            return InterviewPanel;
        }








        #region IEntityBuilder<InterviewPanel> InterviewPanel


        public InterviewPanel BuildEntity(IDataReader dataReader)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
