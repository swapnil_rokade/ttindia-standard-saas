﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingAssessmentBuilder : IEntityBuilder<JobPostingAssessment>
    {
        IList<JobPostingAssessment> IEntityBuilder<JobPostingAssessment>.BuildEntities(IDataReader reader)
        {
            List<JobPostingAssessment> jobPostingAssessments = new List<JobPostingAssessment>();

            while (reader.Read())
            {
                jobPostingAssessments.Add(((IEntityBuilder<JobPostingAssessment>)this).BuildEntity(reader));
            }

            return (jobPostingAssessments.Count > 0) ? jobPostingAssessments : null;
        }

        JobPostingAssessment IEntityBuilder<JobPostingAssessment>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_TESTMASTERID = 1;
            const int FLD_JOBPOSTINGID = 2;
            const int FLD_CREATORID = 3;
            const int FLD_UPDATORID = 4;
            const int FLD_CREATEDATE = 5;
            const int FLD_UPDATEDATE = 6;

            JobPostingAssessment jobPostingAssessment = new JobPostingAssessment();

            jobPostingAssessment.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            jobPostingAssessment.TestMasterId = reader.IsDBNull(FLD_TESTMASTERID) ? 0 : reader.GetInt32(FLD_TESTMASTERID);
            jobPostingAssessment.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            jobPostingAssessment.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            jobPostingAssessment.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            jobPostingAssessment.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            jobPostingAssessment.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return jobPostingAssessment;
        }
    }
}