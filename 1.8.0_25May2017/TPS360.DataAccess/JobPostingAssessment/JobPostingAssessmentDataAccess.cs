﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingAssessmentDataAccess : BaseDataAccess, IJobPostingAssessmentDataAccess
    {
        #region Constructors

        public JobPostingAssessmentDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<JobPostingAssessment> CreateEntityBuilder<JobPostingAssessment>()
        {
            return (new JobPostingAssessmentBuilder()) as IEntityBuilder<JobPostingAssessment>;
        }

        #endregion

        #region  Methods

        JobPostingAssessment IJobPostingAssessmentDataAccess.Add(JobPostingAssessment jobPostingAssessment)
        {
            const string SP = "dbo.JobPostingAssessment_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@TestMasterId", DbType.Int32, jobPostingAssessment.TestMasterId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingAssessment.JobPostingId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, jobPostingAssessment.CreatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPostingAssessment = CreateEntityBuilder<JobPostingAssessment>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPostingAssessment = null;
                    }
                }

                if (jobPostingAssessment == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting assessment already exists. Please specify another job posting assessment.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this job posting assessment.");
                            }
                    }
                }

                return jobPostingAssessment;
            }
        }

        JobPostingAssessment IJobPostingAssessmentDataAccess.Update(JobPostingAssessment jobPostingAssessment)
        {
            const string SP = "dbo.JobPostingAssessment_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, jobPostingAssessment.Id);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, jobPostingAssessment.UpdatorId);
                Database.AddInParameter(cmd, "@UpdateDate", DbType.DateTime, jobPostingAssessment.UpdateDate);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        jobPostingAssessment = CreateEntityBuilder<JobPostingAssessment>().BuildEntity(reader);
                    }
                    else
                    {
                        jobPostingAssessment = null;
                    }
                }

                if (jobPostingAssessment == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Job posting assessment already exists. Please specify another job posting assessment.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this job posting assessment.");
                            }
                    }
                }

                return jobPostingAssessment;
            }
        }

        JobPostingAssessment IJobPostingAssessmentDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingAssessment_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingAssessment>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        JobPostingAssessment IJobPostingAssessmentDataAccess.GetByJobPostingIdAndTestMasterId(int jobPostingId, int testMasterId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            if (testMasterId < 1)
            {
                throw new ArgumentNullException("testMasterId");
            }

            const string SP = "dbo.JobPostingAssessment_GetByJobPostingIdAndTestMasterId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);
                Database.AddInParameter(cmd, "@TestMasterId", DbType.Int32, testMasterId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<JobPostingAssessment>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<JobPostingAssessment> IJobPostingAssessmentDataAccess.GetAll()
        {
            const string SP = "dbo.JobPostingAssessment_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingAssessment>().BuildEntities(reader);
                }
            }
        }

        IList<JobPostingAssessment> IJobPostingAssessmentDataAccess.GetAllByJobPostingId(int jobPostingId)
        {
            const string SP = "dbo.JobPostingAssessment_GetAllByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<JobPostingAssessment>().BuildEntities(reader);
                }
            }
        }

        bool IJobPostingAssessmentDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.JobPostingAssessment_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting assessment which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this job posting assessment.");
                        }
                }
            }
        }

        bool IJobPostingAssessmentDataAccess.DeleteByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "dbo.JobPostingAssessment_DeleteJobPostingAssessmentByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a job posting assessment which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this job posting assessment.");
                        }
                }
            }
        }

        #endregion
    }
}