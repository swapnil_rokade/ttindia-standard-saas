﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberSourceHistoryDataAccess : BaseDataAccess, IMemberSourceHistoryDataAccess
    {
        #region Constructors

        public MemberSourceHistoryDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberSourceHistory> CreateEntityBuilder<MemberSourceHistory>()
        {
            return (new MemberSourceHistoryBuilder()) as IEntityBuilder<MemberSourceHistory>;
        }

        #endregion

        #region  Methods

        MemberSourceHistory IMemberSourceHistoryDataAccess.Add(MemberSourceHistory membersourcehistory)
        {
            const string SP = "dbo.MemberSourceHistory_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, membersourcehistory.MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, membersourcehistory.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, membersourcehistory.UpdatorId);

                Database.AddInParameter(cmd, "@CreateDate", DbType.DateTime, NullConverter.Convert(membersourcehistory.CreateDate));
                Database.AddInParameter(cmd, "@UpdateDate", DbType.DateTime, NullConverter.Convert(membersourcehistory.UpdateDate));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, membersourcehistory.IsRemoved);
                Database.AddInParameter(cmd, "@Source", DbType.Int32, membersourcehistory.Source);
                Database.AddInParameter(cmd, "@ExpireDate", DbType.DateTime, NullConverter.Convert(membersourcehistory.ExpireDate));
                Database.AddInParameter(cmd, "@SourceDescription", DbType.AnsiString, membersourcehistory.SourceDescription);
                Database.AddInParameter(cmd, "@UserId", DbType.Int32, membersourcehistory.UserId);
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        membersourcehistory = CreateEntityBuilder<MemberSourceHistory>().BuildEntity(reader);
                    }
                    else
                    {
                        membersourcehistory = null;
                    }
                }

                if (membersourcehistory == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberSourceHistory already exists. Please specify another membersourcehistory.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this membersourcehistory.");
                            }
                    }
                }

                return membersourcehistory;
            }
        }

        MemberSourceHistory IMemberSourceHistoryDataAccess.Update(MemberSourceHistory membersourcehistory)
        {
            const string SP = "dbo.MemberSourceHistory_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, membersourcehistory.MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, membersourcehistory.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, membersourcehistory.UpdatorId);

                Database.AddInParameter(cmd, "@CreateDate", DbType.DateTime, NullConverter.Convert(membersourcehistory.CreateDate));
                Database.AddInParameter(cmd, "@UpdateDate", DbType.DateTime, NullConverter.Convert(membersourcehistory.UpdateDate));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, membersourcehistory.IsRemoved);
                Database.AddInParameter(cmd, "@Source", DbType.Int32, membersourcehistory.Source);
                Database.AddInParameter(cmd, "@ExpireDate", DbType.DateTime, NullConverter.Convert(membersourcehistory.ExpireDate));
                Database.AddInParameter(cmd, "@SourceDescription", DbType.AnsiString, membersourcehistory.SourceDescription);
                Database.AddInParameter(cmd, "@UserId", DbType.Int32, membersourcehistory.UserId);
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        membersourcehistory = CreateEntityBuilder<MemberSourceHistory>().BuildEntity(reader);
                    }
                    else
                    {
                        membersourcehistory = null;
                    }
                }

                if (membersourcehistory == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberSourceHistory already exists. Please specify another membersourcehistory.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this membersourcehistory.");
                            }
                    }
                }

                return membersourcehistory;
            }
        }

        MemberSourceHistory IMemberSourceHistoryDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberSourceHistory_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberSourceHistory>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberSourceHistory> IMemberSourceHistoryDataAccess.GetAll()
        {
            const string SP = "dbo.MemberSourceHistory_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberSourceHistory>().BuildEntities(reader);
                }
            }
        }


        bool  IMemberSourceHistoryDataAccess.IsCandidateSourceExpired(string primaryEmail)
        {

            const string SP = "dbo.Member_IsCandidateSourceExpired";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@PrimaryEmail", DbType.String, primaryEmail);
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? false : reader.GetBoolean(0);
                    }
                    return false;
                }
            }
        }

        bool IMemberSourceHistoryDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberSourceHistory_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a membersourcehistory which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this membersourcehistory.");
                        }
                }
            }
        }


        PagedResponse<MemberSourceHistory> IMemberSourceHistoryDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberSourceHistory_GetPaged";
            string whereClause = string.Empty;

            
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (System .Collections . DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "MemberId"))
                        {
                            if (value != "0")
                                whereClause = "[MSH].[MemberID]=" + value;
                        }
                    }
                }
            }

            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "asc";
            }

           // request.SortColumn = "[MS].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberSourceHistory> response = new PagedResponse<MemberSourceHistory>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberSourceHistory>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        #endregion
    }
}