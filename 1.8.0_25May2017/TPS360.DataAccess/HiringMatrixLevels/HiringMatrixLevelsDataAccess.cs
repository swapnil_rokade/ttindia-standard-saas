﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:HiringMatrixLevelsDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
       0.1             13/May/2016          pravin khot         added new - HiringMatrixLevel_Id(int LevelId)
 * -------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class HiringMatrixLevelsDataAccess : BaseDataAccess, IHiringMatrixLevelsDataAccess
    {
        #region Constructors

        public HiringMatrixLevelsDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<HiringMatrixLevels> CreateEntityBuilder<HiringMatrixLevels>()
        {
            return (new HiringMatrixLevelsBuilder()) as IEntityBuilder<HiringMatrixLevels>;
        }

        #endregion

        #region  Methods

        HiringMatrixLevels IHiringMatrixLevelsDataAccess.Add(HiringMatrixLevels hiringMatrixLevels)
        {
            const string SP = "dbo.HiringMatrixLevels_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, StringHelper.Convert(hiringMatrixLevels.Name));
                Database.AddInParameter(cmd, "@SortOrder", DbType.AnsiString, Convert .ToInt32 (hiringMatrixLevels.SortingOrder ));
                Database.AddInParameter(cmd, "@CreatorID", DbType.AnsiString, Convert.ToInt32(hiringMatrixLevels.CreatorId));
                Database.AddInParameter(cmd, "@UpdatorID", DbType.AnsiString, Convert.ToInt32(hiringMatrixLevels.UpdatorId));
             
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        hiringMatrixLevels = CreateEntityBuilder<HiringMatrixLevels>().BuildEntity(reader);
                    }
                    else
                    {
                        hiringMatrixLevels = null;
                    }
                }

                if (hiringMatrixLevels == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("HiringMatrixLevels already exists. Please specify another HiringMatrixLevels.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this HiringMatrixLevels.");
                            }
                    }
                }

                return hiringMatrixLevels;
            }
        }

        HiringMatrixLevels IHiringMatrixLevelsDataAccess.Update(HiringMatrixLevels hiringMatrixLevels)
        {
            const string SP = "dbo.HiringMatrixLevels_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, hiringMatrixLevels.Id);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, StringHelper.Convert(hiringMatrixLevels.Name));
                Database.AddInParameter(cmd, "@SortOrder", DbType.AnsiString, Convert.ToInt32(hiringMatrixLevels.SortingOrder));
                Database.AddInParameter(cmd, "@UpdatorID", DbType.AnsiString, Convert.ToInt32(hiringMatrixLevels.UpdatorId));
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        hiringMatrixLevels = CreateEntityBuilder<HiringMatrixLevels>().BuildEntity(reader);
                    }
                    else
                    {
                        hiringMatrixLevels = null;
                    }
                }

                if (hiringMatrixLevels == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("HiringMatrixLevels already exists. Please specify another HiringMatrixLevels.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this HiringMatrixLevels.");
                            }
                    }
                }

                return hiringMatrixLevels;
            }
        }

        HiringMatrixLevels IHiringMatrixLevelsDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.HiringMatrixLevels_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<HiringMatrixLevels>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<HiringMatrixLevels> IHiringMatrixLevelsDataAccess.GetAll()
        {
            const string SP = "dbo.HiringMatrixLevels_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<HiringMatrixLevels>().BuildEntities(reader);
                }
            }
        }

        bool IHiringMatrixLevelsDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.HiringMatrixLevels_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a HiringMatrixLevels which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this HiringMatrixLevels.");
                        }
                }
            }
        }

        void IHiringMatrixLevelsDataAccess.DeleteAllHiringMatrix()
        {
            using (DbCommand cmd = Database.GetStoredProcCommand("HiringMatrixLevels_Delete"))
            {
                Database.ExecuteNonQuery(cmd);
            }
        }

        HiringMatrixLevels IHiringMatrixLevelsDataAccess.GetPreviousLevelByID(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.HiringMatrixLevels_GetPreviousLevelById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<HiringMatrixLevels>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        HiringMatrixLevels IHiringMatrixLevelsDataAccess.GetNextLevelByID(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.HiringMatrixLevels_GetNextLevelById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<HiringMatrixLevels>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        HiringMatrixLevels IHiringMatrixLevelsDataAccess.GetInitialLevel()
        {
            const string SP = "dbo.HiringMatrixLevels_GetInitialLevel";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<HiringMatrixLevels>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        HiringMatrixLevels IHiringMatrixLevelsDataAccess.GetFinalLevel()
        {
            const string SP = "dbo.HiringMatrixLevels_GetFinalLevel";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<HiringMatrixLevels>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        int IHiringMatrixLevelsDataAccess.GetHiringMatrixLevelsCount()
        {
            const string SP = "HiringMatrixLevels_Count";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (int)reader.GetValue(0);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }
        IList<HiringMatrixLevels> IHiringMatrixLevelsDataAccess.GetAllWithCount(int JobPostingId)
        {
            if (JobPostingId < 1)
            {
                throw new ArgumentNullException("JobPostingId");
            }
            const string SP = "dbo.HiringMatrixLevels_GetAllWithCount";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<HiringMatrixLevels>().BuildEntities(reader);
                }
            }
        }

        IList<HiringMatrixLevels> IHiringMatrixLevelsDataAccess.GetAllWithCountByMemberId(int MemberId)
        {
            if (MemberId < 1)
            {
                throw new ArgumentNullException("MemberId");
            }
            const string SP = "dbo.HiringMatrixLevels_GetAllWithCountByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<HiringMatrixLevels>().BuildEntities(reader);
                }
            }
        }

        HiringMatrixLevels IHiringMatrixLevelsDataAccess.GetLevelByName(string  LevelName)
        {

            const string SP = "dbo.HiringMatrixLevels_GetLevelByName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString ,  LevelName );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<HiringMatrixLevels>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        //**************Added by pravin khot on 13/May/2016****************
        int IHiringMatrixLevelsDataAccess.HiringMatrixLevel_Id(int LevelId)
        {
            const string SP = "HiringMatrixLevel_Id";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@LevelId", DbType.Int32, LevelId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (int)reader.GetValue(0);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }
        //************************END**********************************
        #endregion
    }
}