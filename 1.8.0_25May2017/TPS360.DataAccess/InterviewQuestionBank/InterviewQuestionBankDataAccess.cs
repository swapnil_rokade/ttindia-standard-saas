﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewQuestionBankDataAccess.cs
    Description         :   This page is used Call the Stored Proceedures for Interviewer Feed back.
    Created By          :   Prasanth
    Created On          :   15/Oct/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;
using System.Text;
using TPS360.Common.BusinessEntities;
namespace TPS360.DataAccess
{
    internal sealed class InterviewQuestionBankDataAccess : BaseDataAccess,IInterviewQuestionBankDataAccess
    {
         #region Constructors

        public InterviewQuestionBankDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<InterviewQuestionBank> CreateEntityBuilder<InterviewQuestionBank>()
        {
            return (new InterviewQuestionBankBuilder()) as IEntityBuilder<InterviewQuestionBank>;
        }

        #endregion

        #region  Methods
        void IInterviewQuestionBankDataAccess.Add(InterviewQuestionBank InterviewQuestionBank) 
        {
            const string SP = "dbo.InterviewQuestionBank_Create";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewQuestionBank.InterviewId);
                Database.AddInParameter(cmd, "@QuestionBankType_Id", DbType.String, InterviewQuestionBank.QuestionBankType_Id);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, InterviewQuestionBank.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewQuestionBank.UpdatorId);
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewQuestionBank.InterviewId);
                Database.ExecuteReader(cmd);
             
            }
        
        }

        void IInterviewQuestionBankDataAccess.Update(InterviewQuestionBank InterviewQuestionBank)
        {
            const string SP = "dbo.InterviewQuestionBank_Update";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewQuestionBank_Id", DbType.Int32, InterviewQuestionBank.InterviewId);
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewQuestionBank.InterviewId);
                Database.AddInParameter(cmd, "@QuestionBankType_Id", DbType.String, InterviewQuestionBank.QuestionBankType_Id);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewQuestionBank.UpdatorId);
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewQuestionBank.InterviewId);
                Database.ExecuteReader(cmd);
             

                Database.ExecuteReader(cmd);

            }

        }
        
        //Check this functionality later 0123456789
        IList<InterviewQuestionBank> IInterviewQuestionBankDataAccess.GetAll()
        {
            const string SP = "dbo.InterviewerFeedback_GetAll";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<InterviewQuestionBank>().BuildEntities(reader);
                }
            }

        }


        //Check this functionality later 0123456789
        void IInterviewQuestionBankDataAccess.DeleteById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.InterviewerFeedback_DeleteById";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                Database.ExecuteNonQuery(cmd);  
            }

        }


        //Check this functionality later 0123456789
        InterviewQuestionBank IInterviewQuestionBankDataAccess.GetById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.InterviewerFeedback_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<InterviewQuestionBank>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

        }



        IList<InterviewQuestionBank> IInterviewQuestionBankDataAccess.GetByInterviewId(int InterviewId)
        {
            if (InterviewId < 0)
            {
                throw new ArgumentNullException("InterviewId");
            }
            const string SP = "dbo.InterviewerFeedback_GetByInterviewId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    return CreateEntityBuilder<InterviewQuestionBank>().BuildEntities(reader);

                }
            }
        }

        //Check this functionality later 0123456789
        PagedResponse<InterviewQuestionBank> IInterviewQuestionBankDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.InterviewerFeedback_GetPaged";
            
            string whereClause = string.Empty;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<InterviewQuestionBank> response = new PagedResponse<InterviewQuestionBank>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<InterviewQuestionBank>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        #endregion
    }
}
