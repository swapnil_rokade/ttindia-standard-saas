﻿using System;
using System.Data;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class JobPostingSearchAgentBuilder : IEntityBuilder<JobPostingSearchAgent>
    {
        IList<JobPostingSearchAgent> IEntityBuilder<JobPostingSearchAgent>.BuildEntities(IDataReader reader)
        {
            List<JobPostingSearchAgent> jobPostings = new List<JobPostingSearchAgent>();

            while (reader.Read())
            {
                jobPostings.Add(((IEntityBuilder<JobPostingSearchAgent>)this).BuildEntity(reader));
            }

            return (jobPostings.Count > 0) ? jobPostings : null;
        }
        JobPostingSearchAgent IEntityBuilder<JobPostingSearchAgent>.BuildEntity(IDataReader reader)
        {
            JobPostingSearchAgent jobPosting = new JobPostingSearchAgent();

            const int FLD_ID = 0;
            const int FLD_JOBPOSTINGID = 1;
            const int FLD_KEYWORDS = 2;
            const int FLD_JOBTITLE = 3;
            const int FLD_CITY = 4;
            const int FLD_STATEID = 5;
            const int FLD_COUNTRYID = 6;
            const int FLD_MINEXPERIENCE = 7;
            const int FLD_MAXEXPERIENCE = 8;
            const int FLD_SALARYFROM = 9;
            const int FLD_SALARYTO = 10;
            const int FLD_CURRENCYLOOKUPID = 11;
            const int FLD_SALARYTYPE = 12;
            const int FLD_HOTLISTID = 13;
            const int FLD_EDUCATIONLOOKUPID = 14;
            const int FLD_EMPLOYEMENTTYPELOOKUPID = 15;
            const int FLD_RESUMELASTUPDATED = 16;
            const int FLD_CANDIDATETYPELOOKUPID = 17;
            const int FLD_WORKSCHEDULELOOKUPID = 18;
            const int FLD_WORKSTATUSLOOKUPID = 19;
            const int FLD_HIRINGSTATUSLOOKUPID = 20;
            const int FLD_AVAILABLEAFTER = 21;
            const int FLD_SECURITUCLEARANCE = 22;
            const int FLD_CREATORID = 23;
            const int FLD_UPDATORID = 24;
            const int FLD_CREATEDATE = 25;
            const int FLD_UPDATEDATE = 26;
            const int FLD_ISREMOVED = 27;
            const int FLD_STATENAME = 28;
            const int FLD_COUNTRYNAME = 29;
            const int FLD_CURRENCYNAME = 30;
            const int FLD_HOTLISTNAME = 31;
            const int FLD_EDUCATIONTYPENAME = 32;
            const int FLD_EMPLOYEMENTTYPENAME = 33;
            const int FLD_CANDIDATETYPENAME = 34;
            const int FLD_WORKSCHEDULETYPENAME = 35;
            const int FLD_WORKSTATUSTYPENAME = 36;
            const int FLD_HIRINGSTATUSTYPENAME = 37;

            jobPosting.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            jobPosting.JobPostingId  = reader.IsDBNull(FLD_JOBPOSTINGID ) ? 0 : reader.GetInt32 (FLD_JOBPOSTINGID );
            jobPosting.KeyWord = reader.IsDBNull(FLD_KEYWORDS) ? string.Empty : reader.GetString(FLD_KEYWORDS);
            jobPosting.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            jobPosting.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
            jobPosting.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
            jobPosting.CountryId = reader.IsDBNull(FLD_COUNTRYID) ? 0 : reader.GetInt32(FLD_COUNTRYID);
            jobPosting.MinExperience  = reader.IsDBNull(FLD_MINEXPERIENCE ) ? string.Empty : reader.GetString(FLD_MINEXPERIENCE );
            jobPosting.MaxExperience = reader.IsDBNull(FLD_MAXEXPERIENCE) ? string.Empty : reader.GetString(FLD_MAXEXPERIENCE);
            jobPosting.SalaryFrom  = reader.IsDBNull(FLD_SALARYFROM ) ? string.Empty : reader.GetString(FLD_SALARYFROM );
            jobPosting.SalaryTo  = reader.IsDBNull(FLD_SALARYTO ) ? string.Empty : reader.GetString(FLD_SALARYTO );
            jobPosting.CurrencyLookUpId  = reader.IsDBNull(FLD_CURRENCYLOOKUPID ) ? 0 : reader.GetInt32(FLD_CURRENCYLOOKUPID );
            jobPosting.SalaryType  = reader.IsDBNull(FLD_SALARYTYPE ) ? string.Empty : reader.GetString(FLD_SALARYTYPE );
            jobPosting.HotListId  = reader.IsDBNull(FLD_HOTLISTID ) ? 0 : reader.GetInt32(FLD_HOTLISTID );
            jobPosting.EducationLookupId  = reader.IsDBNull(FLD_EDUCATIONLOOKUPID ) ? string.Empty : reader.GetString(FLD_EDUCATIONLOOKUPID );
            jobPosting.EmployementTypeLookUpId  = reader.IsDBNull(FLD_EMPLOYEMENTTYPELOOKUPID ) ? 0 : reader.GetInt32(FLD_EMPLOYEMENTTYPELOOKUPID );
            jobPosting.ResumeLastUpdated  = reader.IsDBNull(FLD_RESUMELASTUPDATED ) ? string.Empty : reader.GetString(FLD_RESUMELASTUPDATED );
            jobPosting.CandidateTypeLookUpId  = reader.IsDBNull(FLD_CANDIDATETYPELOOKUPID ) ? 0 : reader.GetInt32(FLD_CANDIDATETYPELOOKUPID );
            jobPosting.WorkScheduleLookupId  = reader.IsDBNull(FLD_WORKSCHEDULELOOKUPID ) ? 0 : reader.GetInt32 (FLD_WORKSCHEDULELOOKUPID );
            jobPosting.WorkStatusLookUpId  = reader.IsDBNull(FLD_WORKSTATUSLOOKUPID ) ? string.Empty : reader.GetString(FLD_WORKSTATUSLOOKUPID );
            jobPosting.HiringStatusLookUpId  = reader.IsDBNull(FLD_HIRINGSTATUSLOOKUPID ) ? string.Empty : reader.GetString(FLD_HIRINGSTATUSLOOKUPID );
            jobPosting.AvailableAfter  = reader.IsDBNull(FLD_AVAILABLEAFTER ) ? DateTime.MinValue : reader.GetDateTime(FLD_AVAILABLEAFTER );
            jobPosting.SecurityClearance = reader.IsDBNull(FLD_SECURITUCLEARANCE) ? false : reader.GetBoolean(FLD_SECURITUCLEARANCE);
            jobPosting.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            jobPosting.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            jobPosting.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            jobPosting.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            jobPosting.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            jobPosting.StateName = reader.IsDBNull(FLD_STATENAME) ? string.Empty : reader.GetString(FLD_STATENAME);
            jobPosting.CountryName  = reader.IsDBNull(FLD_COUNTRYNAME ) ? string.Empty : reader.GetString(FLD_COUNTRYNAME );
            jobPosting.CurrencyName  = reader.IsDBNull(FLD_CURRENCYNAME ) ? string.Empty : reader.GetString(FLD_CURRENCYNAME );
            jobPosting.HotlistName  = reader.IsDBNull(FLD_HOTLISTNAME ) ? string.Empty : reader.GetString(FLD_HOTLISTNAME );
            jobPosting.EducationTypeName  = reader.IsDBNull(FLD_EDUCATIONTYPENAME ) ? string.Empty : reader.GetString(FLD_EDUCATIONTYPENAME );
            jobPosting.EmployementTypeName  = reader.IsDBNull(FLD_EMPLOYEMENTTYPENAME ) ? string.Empty : reader.GetString(FLD_EMPLOYEMENTTYPENAME );
            jobPosting.CandidateTypeName  = reader.IsDBNull(FLD_CANDIDATETYPENAME ) ? string.Empty : reader.GetString(FLD_CANDIDATETYPENAME );
            jobPosting.WorkScheduleTypeName  = reader.IsDBNull(FLD_WORKSCHEDULETYPENAME ) ? string.Empty : reader.GetString(FLD_WORKSCHEDULETYPENAME );
            jobPosting.WorkStatusTypeName  = reader.IsDBNull(FLD_WORKSTATUSTYPENAME ) ? string.Empty : reader.GetString(FLD_WORKSTATUSTYPENAME );
            jobPosting.HiringStatusTypeName  = reader.IsDBNull(FLD_HIRINGSTATUSTYPENAME ) ? string.Empty : reader.GetString(FLD_HIRINGSTATUSTYPENAME );

            return jobPosting;
        }
    }
}
