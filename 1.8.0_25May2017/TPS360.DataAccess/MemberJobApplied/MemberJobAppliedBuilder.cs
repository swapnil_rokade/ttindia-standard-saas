﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberJobAppliedBuilder : IEntityBuilder<MemberJobApplied>
    {
        IList<MemberJobApplied> IEntityBuilder<MemberJobApplied>.BuildEntities(IDataReader reader)
        {
            List<MemberJobApplied> MemberJobApplieds = new List<MemberJobApplied>();

            while (reader.Read())
            {
                MemberJobApplieds.Add(((IEntityBuilder<MemberJobApplied>)this).BuildEntity(reader));
            }

            return (MemberJobApplieds.Count > 0) ? MemberJobApplieds : null;
        }

        MemberJobApplied IEntityBuilder<MemberJobApplied>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERID = 1;
            const int  FLD_JOBPOSTINGID= 2;
            const int FLD_JOBTITLE = 3;
            const int FLD_JOBPOSTEDDATE = 4;
            const int FLD_CITY = 5;
            const int FLD_STATE = 6;
            const int FLD_ISREMOVED = 7;
            const int FLD_CREATORID = 8;
            const int FLD_UPDATORID = 9;
            const int FLD_CREATEDATE = 10;
            const int FLD_UPDATEDATE = 11;

            MemberJobApplied MemberJobApplied = new MemberJobApplied();

            MemberJobApplied.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            MemberJobApplied.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            MemberJobApplied.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32 (FLD_JOBPOSTINGID);
            MemberJobApplied.JobTitle   = reader.IsDBNull(FLD_JOBTITLE ) ? string.Empty : reader.GetString(FLD_JOBTITLE );
            MemberJobApplied.JobPostedDate  = reader.IsDBNull(FLD_JOBPOSTEDDATE ) ? DateTime .MinValue : reader.GetDateTime (FLD_JOBPOSTEDDATE );
            MemberJobApplied.JobCity  = reader.IsDBNull(FLD_CITY ) ? string .Empty  : reader.GetString (FLD_CITY );
            MemberJobApplied.JobState = reader.IsDBNull(FLD_STATE) ? string.Empty : reader.GetString(FLD_STATE);
            MemberJobApplied.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            MemberJobApplied.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            MemberJobApplied.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            MemberJobApplied.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            MemberJobApplied.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return MemberJobApplied;
        }
    }
}