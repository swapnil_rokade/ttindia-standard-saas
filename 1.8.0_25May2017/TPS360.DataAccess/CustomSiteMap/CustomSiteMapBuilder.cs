﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class CustomSiteMapBuilder : BaseEntityBuilder, IEntityBuilder<CustomSiteMap>
    {
        private Context _context = new Context();
        private static Dictionary<int, CustomSiteMap> _customSiteMapCache = new Dictionary<int, CustomSiteMap>();

        IList<CustomSiteMap> IEntityBuilder<CustomSiteMap>.BuildEntities(IDataReader reader)
        {
            List<CustomSiteMap> customSiteMaps = new List<CustomSiteMap>();

            while (reader.Read())
            {
                customSiteMaps.Add(((IEntityBuilder<CustomSiteMap>)this).BuildEntity(reader));
            }

            return (customSiteMaps.Count > 0) ? customSiteMaps : null;
        }

        CustomSiteMap IEntityBuilder<CustomSiteMap>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_TITLE = 1;
            const int FLD_DESCRIPTION = 2;
            const int FLD_URL = 3;
            const int FLD_IMAGEURL = 4;
            const int FLD_SORTORDER = 5;
            const int FLD_ISCONTAINER = 6;
            const int FLD_ISREMOVED = 7;
            const int FLD_ROLES = 8;
            const int FLD_PARENTID = 9;
            const int FLD_SITEMAPTYPE = 10;
            const int FLD_CREATEDATE = 11;
            const int FLD_UPDATEDATE = 12;
            const int FLD_ISDEFAULT = 13;
            const int FLD_HASCOUNT = 14;

            CustomSiteMap customSiteMap = new CustomSiteMap();

            customSiteMap.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            customSiteMap.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString(FLD_TITLE);
            customSiteMap.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
            customSiteMap.Url = reader.IsDBNull(FLD_URL) ? string.Empty : reader.GetString(FLD_URL);
            customSiteMap.ImageUrl = reader.IsDBNull(FLD_IMAGEURL) ? string.Empty : reader.GetString(FLD_IMAGEURL);
            customSiteMap.SortOrder = reader.IsDBNull(FLD_SORTORDER) ? 0 : reader.GetInt32(FLD_SORTORDER);
            customSiteMap.IsVirtual = reader.IsDBNull(FLD_ISCONTAINER) ? false : reader.GetBoolean(FLD_ISCONTAINER);
            customSiteMap.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            customSiteMap.Roles = reader.IsDBNull(FLD_ROLES) ? string.Empty : reader.GetString(FLD_ROLES);
            customSiteMap.ParentId = reader.IsDBNull(FLD_PARENTID) ? 0 : reader.GetInt32(FLD_PARENTID);
            customSiteMap.SiteMapType = (SiteMapType)(reader.IsDBNull(FLD_SITEMAPTYPE) ? 0 : reader.GetInt32(FLD_SITEMAPTYPE));
            customSiteMap.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            customSiteMap.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            if (customSiteMap.ParentId > 0)
            {
                customSiteMap.ParentMenu = GetCustomSiteMap(customSiteMap.ParentId);
            }

            customSiteMap.IsDefault = reader.IsDBNull(FLD_ISDEFAULT) ? false : reader.GetBoolean(FLD_ISDEFAULT);
            customSiteMap.HasCount = reader.IsDBNull(FLD_HASCOUNT) ? false : reader.GetBoolean(FLD_HASCOUNT);
            return customSiteMap;
        }

        CustomSiteMap GetCustomSiteMap(int customSiteMapId)
        {
            if (!_customSiteMapCache.ContainsKey(customSiteMapId))
            {
                ICustomSiteMapDataAccess da = new CustomSiteMapDataAccess(_context);

                CustomSiteMap customSiteMap = da.GetById(customSiteMapId);

                if (customSiteMap == null)
                {
                    return null;
                }

                _customSiteMapCache.Add(customSiteMapId, customSiteMap);
            }

            return _customSiteMapCache[customSiteMapId];
        }
    }
}