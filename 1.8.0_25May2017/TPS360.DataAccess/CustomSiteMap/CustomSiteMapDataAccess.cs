﻿
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class CustomSiteMapDataAccess : BaseDataAccess, ICustomSiteMapDataAccess
    {
        #region Constructors

        public CustomSiteMapDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CustomSiteMap> CreateEntityBuilder<CustomSiteMap>()
        {
            return (new CustomSiteMapBuilder()) as IEntityBuilder<CustomSiteMap>;
        }

        #endregion

        #region  Methods

        CustomSiteMap ICustomSiteMapDataAccess.Add(CustomSiteMap customSiteMap)
        {
            const string SP = "dbo.CustomSiteMap_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, customSiteMap.Title);
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(customSiteMap.Description));
                Database.AddInParameter(cmd, "@Url", DbType.AnsiString, StringHelper.Convert(customSiteMap.Url));
                Database.AddInParameter(cmd, "@ImageUrl", DbType.AnsiString, StringHelper.Convert(customSiteMap.ImageUrl));
                Database.AddInParameter(cmd, "@SortOrder", DbType.Int32, NullConverter.Convert(customSiteMap.SortOrder));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, customSiteMap.IsRemoved);
                Database.AddInParameter(cmd, "@Roles", DbType.AnsiString, StringHelper.Convert(customSiteMap.Roles));
                Database.AddInParameter(cmd, "@ParentId", DbType.Int32, NullConverter.Convert(customSiteMap.ParentId));
                Database.AddInParameter(cmd, "@SiteMapType", DbType.Int32, (int) customSiteMap.SiteMapType);
                Database.AddInParameter(cmd, "@IsDefault", DbType.Boolean, customSiteMap.IsDefault);
                Database.AddInParameter(cmd, "@HasCount", DbType.Boolean, customSiteMap.HasCount );
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        customSiteMap = CreateEntityBuilder<CustomSiteMap>().BuildEntity(reader);
                    }
                    else
                    {
                        customSiteMap = null;
                    }
                }

                if (customSiteMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                        {
                            throw new ArgumentException("Custom site map already exists. Please specify another custom site map.");
                        }
                        default:
                        {
                            throw new SystemException("An unexpected error has occurred while creating this custom site map.");
                        }
                    }
                }

                return customSiteMap;
            }
        }

        CustomSiteMap ICustomSiteMapDataAccess.Update(CustomSiteMap customSiteMap)
        {
            const string SP = "dbo.CustomSiteMap_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, customSiteMap.Id);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, StringHelper.Convert(customSiteMap.Title));
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(customSiteMap.Description));
                Database.AddInParameter(cmd, "@Url", DbType.AnsiString, StringHelper.Convert(customSiteMap.Url));
                Database.AddInParameter(cmd, "@ImageUrl", DbType.AnsiString, StringHelper.Convert(customSiteMap.ImageUrl));
                Database.AddInParameter(cmd, "@SortOrder", DbType.Int32, NullConverter.Convert(customSiteMap.SortOrder));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, customSiteMap.IsRemoved);
                Database.AddInParameter(cmd, "@Roles", DbType.AnsiString, StringHelper.Convert(customSiteMap.Roles));
                Database.AddInParameter(cmd, "@ParentId", DbType.Int32, customSiteMap.ParentId);
                Database.AddInParameter(cmd, "@SiteMapType", DbType.Int32, customSiteMap.SiteMapType);
                Database.AddInParameter(cmd, "@IsDefault", DbType.Boolean, customSiteMap.IsDefault);
                Database.AddInParameter(cmd, "@HasCount", DbType.Boolean, customSiteMap.HasCount );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        customSiteMap = CreateEntityBuilder<CustomSiteMap>().BuildEntity(reader);
                    }
                    else
                    {
                        customSiteMap = null;
                    }
                }

                if (customSiteMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Custom site map already exists. Please specify another custom site map.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this custom site map.");
                            }
                    }
                }

                return customSiteMap;
            }
        }

        CustomSiteMap ICustomSiteMapDataAccess.GetAllByParentIDAndMemberPrivilege(int ParentId, int MemberID)
        {
            if (ParentId < 1)
            {
                throw new ArgumentNullException("ParentId");
            }
            if (MemberID < 1)
            {
                throw new ArgumentNullException("MemberID");
            }

            const string SP = "dbo.CustomSiteMap_GetAllByParentAndMemberPrivilege";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ParentId", DbType.Int32, ParentId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CustomSiteMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        CustomSiteMap ICustomSiteMapDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CustomSiteMap_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CustomSiteMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        CustomSiteMap ICustomSiteMapDataAccess.GetByName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            const string SP = "dbo.CustomSiteMap_GetByName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Name", DbType.String, name);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CustomSiteMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        CustomSiteMap ICustomSiteMapDataAccess.GetRoot()
        {
            const string SP = "dbo.CustomSiteMap_GetRoot";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CustomSiteMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<CustomSiteMap> ICustomSiteMapDataAccess.GetAll()
        {
            const string SP = "dbo.CustomSiteMap_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CustomSiteMap>().BuildEntities(reader);
                }
            }
        }

        public IList<CustomSiteMap> GetAllParent()
        {
            const string SP = "dbo.CustomSiteMap_GetAllParent";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CustomSiteMap>().BuildEntities(reader);
                }
            }
        }

        public IList<CustomSiteMap> GetAllByParent(int parentId)
        {
            const string SP = "dbo.CustomSiteMap_GetAllByParent";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ParentId", DbType.Int32, parentId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CustomSiteMap>().BuildEntities(reader);
                }
            }
        }

        PagedResponse<CustomSiteMap> ICustomSiteMapDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.CustomSiteMap_GetPaged";
            string whereClause = string.Empty;

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "Title";
            }

            request.SortColumn = "[CS].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<CustomSiteMap> response = new PagedResponse<CustomSiteMap>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<CustomSiteMap>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        bool ICustomSiteMapDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CustomSiteMap_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a custom site map which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this custom site map.");
                        }
                }
            }
        }


        IList<MenuListCount> ICustomSiteMapDataAccess.getMenuListCount(int ParentId, int Id)
        {
            const string SP = "dbo.MenuList_GetCountbyParentIdandID";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@parentid", DbType.Int32, ParentId );
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id );
                IList<MenuListCount> menuList = new List<MenuListCount>();
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while(reader.Read())
                    {
                        MenuListCount menu = new MenuListCount();
                        menu.Id = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        menu.MenuName = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
                        menu.Count = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                     
                            menuList.Add(menu);
                     
                    }
                    return menuList;
                }
            }
        }
        #endregion
    }
}