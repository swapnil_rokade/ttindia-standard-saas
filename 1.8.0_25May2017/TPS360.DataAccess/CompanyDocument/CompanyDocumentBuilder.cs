﻿using System.Data;
using System.Collections.Generic;
using System;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CompanyDocumentBuilder : IEntityBuilder<CompanyDocument>
    {
        IList<CompanyDocument> IEntityBuilder<CompanyDocument>.BuildEntities(IDataReader reader)
        {
            List<CompanyDocument> companyDocuments = new List<CompanyDocument>();

            while (reader.Read())
            {
                companyDocuments.Add(((IEntityBuilder<CompanyDocument>)this).BuildEntity(reader));
            }

            return (companyDocuments.Count > 0) ? companyDocuments : null;
        }

        CompanyDocument IEntityBuilder<CompanyDocument>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_TITLE = 1;
            const int FLD_FILENAME = 2;
            const int FLD_DESCRIPTION = 3;
            const int FLD_DOCUMENTTYPE = 4;
            const int FLD_ISREMOVED = 5;
            const int FLD_COMPANYID = 6;
            const int FLD_CREATORID = 7;
            const int FLD_UPDATORID = 8;
            const int FLD_CREATEDATE = 9;
            const int FLD_UPDATEDATE = 10;
            const int FLD_DOCUMENTTYPENAME = 11;
            CompanyDocument companyDocument = new CompanyDocument();

            companyDocument.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            companyDocument.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString(FLD_TITLE);
            companyDocument.FileName = reader.IsDBNull(FLD_FILENAME) ? string.Empty : reader.GetString(FLD_FILENAME);
            companyDocument.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
            companyDocument.DocumentType = reader.IsDBNull(FLD_DOCUMENTTYPE) ? 0 : reader.GetInt32(FLD_DOCUMENTTYPE);
            companyDocument.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            companyDocument.CompanyId = reader.IsDBNull(FLD_COMPANYID) ? 0 : reader.GetInt32(FLD_COMPANYID);
            companyDocument.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            companyDocument.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            companyDocument.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            companyDocument.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            if (reader.Depth > FLD_DOCUMENTTYPENAME)
            {
                companyDocument.DocumentTypeName = reader.IsDBNull(FLD_DOCUMENTTYPENAME) ? string.Empty : reader.GetString(FLD_DOCUMENTTYPENAME);
            }
            return companyDocument;
        }
    }
}