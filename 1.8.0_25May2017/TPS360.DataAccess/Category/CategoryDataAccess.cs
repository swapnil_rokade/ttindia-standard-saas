﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:CategoryDataAccess.cs
    Description: This page is used for Countries  functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetCategoryIdByCategoryCode() to assist 
                                                                Resume parser team
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class CategoryDataAccess : BaseDataAccess, ICategoryDataAccess
    {
        #region Constructors

        public CategoryDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<Category> CreateEntityBuilder< Category>()
        {
            return (new CategoryBuilder()) as IEntityBuilder<Category>;
        }

        #endregion

        #region  Methods

        Category  ICategoryDataAccess.Add(Category  category)
        {
            const string SP = "dbo.Category_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, StringHelper.Convert(category .Name ));
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        category  = CreateEntityBuilder<Category>().BuildEntity(reader);
                    }
                    else
                    {
                        category  = null;
                    }
                }

                if (category == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Category already exists. Please specify another category.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this category.");
                            }
                    }
                }

                return category;
            }
        }

        Category ICategoryDataAccess.Update(Category category)
        {
            const string SP = "dbo.Category_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, category.Id);
                Database.AddInParameter(cmd, "@Name", DbType.AnsiString, StringHelper.Convert(category.Name));
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        category = CreateEntityBuilder<Category>().BuildEntity(reader);
                    }
                    else
                    {
                        category = null;
                    }
                }

                if (category == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Category already exists. Please specify another category.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this category.");
                            }
                    }
                }

                return category;
            }
        }

        Category ICategoryDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Category_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Category >()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<Category > ICategoryDataAccess.GetAll()
        {
            const string SP = "dbo.Category_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Category >().BuildEntities(reader);
                }
            }
        }

        bool ICategoryDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Category_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a category which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this category.");
                        }
                }
            }
        }

        Int32 ICategoryDataAccess.GetCategoryIdByCategoryName(string categoryName)
        {
            if (categoryName ==null)
            {
                categoryName ="";
            }

            const string SP = "dbo.Category_GetCategoryIdByCategoryName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CategoryName", DbType.String, categoryName );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 categoryId = 0;

                    while (reader.Read())
                    {
                        categoryId = reader.GetInt32(0);
                    }

                    return categoryId;
                }
            }
        }

        #endregion
    }
}