﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberDocumentDataAccess : BaseDataAccess, IMemberDocumentDataAccess
    {
        #region Constructors

        public MemberDocumentDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberDocument> CreateEntityBuilder<MemberDocument>()
        {
            return (new MemberDocumentBuilder()) as IEntityBuilder<MemberDocument>;
        }

        #endregion

        #region  Methods

        MemberDocument IMemberDocumentDataAccess.Add(MemberDocument memberDocument)
        {
            const string SP = "dbo.MemberDocument_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, StringHelper.Convert(memberDocument.Title));
                Database.AddInParameter(cmd, "@FileName", DbType.AnsiString, StringHelper.Convert(memberDocument.FileName));
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(memberDocument.Description));
                Database.AddInParameter(cmd, "@FileTypeLookupId", DbType.Int32, memberDocument.FileTypeLookupId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberDocument.IsRemoved);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberDocument.MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberDocument.CreatorId);
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberDocument = CreateEntityBuilder<MemberDocument>().BuildEntity(reader);
                    }
                    else
                    {
                        memberDocument = null;
                    }
                }

                if (memberDocument == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member document already exists. Please specify another member document.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member document.");
                            }
                    }
                }

                return memberDocument;
            }
        }

        MemberDocument IMemberDocumentDataAccess.Update(MemberDocument memberDocument)
        {
            const string SP = "dbo.MemberDocument_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberDocument.Id);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, StringHelper.Convert(memberDocument.Title));
                Database.AddInParameter(cmd, "@FileName", DbType.AnsiString, StringHelper.Convert(memberDocument.FileName));
                Database.AddInParameter(cmd, "@Description", DbType.AnsiString, StringHelper.Convert(memberDocument.Description));
                Database.AddInParameter(cmd, "@FileTypeLookupId", DbType.Int32, memberDocument.FileTypeLookupId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberDocument.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberDocument.UpdatorId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberDocument = CreateEntityBuilder<MemberDocument>().BuildEntity(reader);
                    }
                    else
                    {
                        memberDocument = null;
                    }
                }

                if (memberDocument == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberDocument already exists. Please specify another member document.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member document.");
                            }
                    }
                }

                return memberDocument;
            }
        }

        MemberDocument IMemberDocumentDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberDocument_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberDocument>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberDocument> IMemberDocumentDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberDocument_GetMemberDocumentByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberDocument>().BuildEntities(reader);
                }
            }           
        }

        PagedResponse<MemberDocument> IMemberDocumentDataAccess.GetPagedByMemberId(int MemberID,PagedRequest request)
        {
            if (MemberID < 1)
            {
                throw new ArgumentNullException("MemberID");
            }

            string whereClause = string.Empty;
            whereClause = "MemberId = " + MemberID;
            if (request.SortColumn == null) request.SortColumn = "[MD].[Title]";
            else if (request.SortColumn == string.Empty) request.SortColumn = "[MD].[Title]";
            else if (request.SortColumn == "[G].[Name]")
                request.SortColumn = request.SortColumn;
            else request.SortColumn = "[MD].[" + request.SortColumn + "]";

            if (request.SortOrder == null) request.SortOrder = "ASC";
            else if (request.SortOrder == string.Empty) request.SortOrder = "ASC";



            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (value.Trim() == "Resume")
                        {
                            if (StringHelper.IsEqual(column, "type"))
                            {
                                sb.Append("AND [G].[Name]");
                                sb.Append(" = ");
                                sb.Append("'Word Resume'");
                            }
                        }
                        else
                            if (StringHelper.IsEqual(column, "type"))
                            {
                                sb.Append("AND [G].[Name]");
                                sb.Append(" <> ");
                                sb.Append("'Word Resume'");
                            }
                        {
                        }
                    }
                }

                whereClause += sb.ToString();
            }




            object[] paramValues = new object[] {request .PageIndex,
													request .RowPerPage   ,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request .SortColumn  ),
                                                    StringHelper.Convert(request.SortOrder),
												};
            const string SP = "dbo.MemberDocument_GetPagedMemberDocumentByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberDocument> response = new PagedResponse<MemberDocument>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberDocument>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        IList<MemberDocument> IMemberDocumentDataAccess.GetAllByTypeAndMemberId(string type,int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            if (type==string.Empty)
            {
                throw new ArgumentNullException("type");
            }

            const string SP = "dbo.MemberDocument_GetByTypeAndMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@Type", DbType.String, type);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberDocument>().BuildEntities(reader);
                }
            }
        }


       MemberDocument IMemberDocumentDataAccess.GetAllByMemberIDTypeAndFileName(int memberId, string type, string FileName)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            if (type == string.Empty)
            {
                throw new ArgumentNullException("type");
            }

            const string SP = "dbo.MemberDocument_GetMemberDocumentByMemberIdTypeAndFileName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@Type", DbType.String, type);
                Database.AddInParameter(cmd, "@FileName", DbType.String, FileName );
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberDocument>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberDocument> IMemberDocumentDataAccess.GetAll()
        {
            const string SP = "dbo.MemberDocument_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberDocument>().BuildEntities(reader);
                }
            }
        }

        bool IMemberDocumentDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberDocument_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member document which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member document.");
                        }
                }
            }
        }


        bool IMemberDocumentDataAccess.DeleteByMemberId(int MemberID)
        {
            if (MemberID < 1)
            {
                throw new ArgumentNullException("MemberID");
            }

            const string SP = "dbo.MemberDocument_DeleteByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberID", DbType.Int32, MemberID);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member document which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member document.");
                        }
                }
            }
        }



        IList<MemberDocument> IMemberDocumentDataAccess.GetAllByTypeAndMembersId(string membersId, string documentType)
        {
            if (string.IsNullOrEmpty(membersId))
            {
                throw new ArgumentNullException("MembersId");
            }
            if (string.IsNullOrEmpty(documentType))
            {
                throw new ArgumentNullException("DocumentType");
            }

            const string SP = "dbo.MemberDocument_GetAllByTypeAndMembersId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MembersId", DbType.String, membersId);
                Database.AddInParameter(cmd, "@Type", DbType.String, documentType);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberDocument>().BuildEntities(reader);
                }
            }
        }

        MemberDocument IMemberDocumentDataAccess.GetByMemberIdAndFileName(int memberId, string fileName)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName");
            }

            const string SP = "dbo.MemberDocument_GetMemberDocumentByMemberIdAndFileName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@FileName", DbType.String, fileName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberDocument>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberDocument> IMemberDocumentDataAccess.GetLatestResumeByMemberID(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberDocument_GetLatestResumeDocument";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                     return CreateEntityBuilder<MemberDocument>().BuildEntities(reader);
                }
            }
        }


        MemberDocument IMemberDocumentDataAccess.GetRecentResumeDocument(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberDocument_GetLatestResumeDocumentByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberDocument>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        #endregion
    }
}