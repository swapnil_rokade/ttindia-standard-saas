﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: InterviewFeedbackDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              24/Dec/2015            pravin khot          Introduced by function BuilInterviewFeedbackReportPrint,BuilInterviewFeedbackReportPrintAsses
    0.2              25/Jan/2016            pravin khot          Introduced by FLD_AnswerType added in function BuilGetInterviewFeedbackReportsPrintAsses
 ---------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using System.Data;

namespace TPS360.DataAccess
{
    internal sealed class InterviewFeedbackBuilder : IEntityBuilder<InterviewFeedback>
    {
        IList<InterviewFeedback> IEntityBuilder<InterviewFeedback>.BuildEntities(IDataReader reader)
        {
            List<InterviewFeedback> interFeedback = new List<InterviewFeedback>();

            while (reader.Read())
            {
                interFeedback.Add(((IEntityBuilder<InterviewFeedback>)this).BuildEntity(reader));
            }

            return (interFeedback.Count > 0) ? interFeedback : null;
        }

        InterviewFeedback IEntityBuilder<InterviewFeedback>.BuildEntity(IDataReader reader) 
        {
            const int FLD_ID = 0;
            const int FLD_TITLE = 1;
            const int FLD_INTERVIEWFEEDBACK = 2;
            const int FLD_SUBMITTEDBY = 3;
            const int FLD_INTERVIEWROUND = 4;
            const int FLD_CREATEDATE = 5;
            const int FLD_UPDATEDATE = 6;
            const int FLD_CREATORID = 7;
            const int FLD_UPDATORID = 8;
            const int FLD_INTERVIEWID = 9;


            InterviewFeedback interviewFeedback = new InterviewFeedback();

            interviewFeedback.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            interviewFeedback.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString(FLD_TITLE);
            interviewFeedback.Feedback = reader.IsDBNull(FLD_INTERVIEWFEEDBACK) ? string.Empty : reader.GetString(FLD_INTERVIEWFEEDBACK);
            interviewFeedback.SubmittedBy = reader.IsDBNull(FLD_SUBMITTEDBY) ? string.Empty : reader.GetString(FLD_SUBMITTEDBY);
            interviewFeedback.InterviewRound = reader.IsDBNull(FLD_INTERVIEWROUND) ? 0 : reader.GetInt32(FLD_INTERVIEWROUND);
            interviewFeedback.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            interviewFeedback.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            interviewFeedback.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            interviewFeedback.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            interviewFeedback.InterviewId = reader.IsDBNull(FLD_INTERVIEWID) ? 0 : reader.GetInt32(FLD_INTERVIEWID);


            return interviewFeedback;
        }


        //*******************************add by pravin khot *********************************
        public IList<InterviewFeedback> BuildInterviewFeedbackReport(IDataReader reader)
        //IList<InterviewFeedback> IEntityBuilder<InterviewFeedback>.BuilInterviewFeedbackReport(IDataReader reader)
        {
            List<InterviewFeedback> interFeedback = new List<InterviewFeedback>();
            while (reader.Read())
            {
                interFeedback.Add(this.BuildGetInterviewFeedbackReports(reader));
                //interFeedback.Add(((IEntityBuilder<InterviewFeedback>)this).BuilGetInterviewFeedbackReports(reader));
            }

            return (interFeedback.Count > 0) ? interFeedback : null;
        }
        public InterviewFeedback BuildGetInterviewFeedbackReports(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_CandidateName = 1;
            const int FLD_JobTitle = 2;
            const int FLD_ReqCode = 3;
            const int FLD_FIRSTNAME = 4;
            const int FLD_CREATEDATE = 5;
            const int FLD_INTERVIEWFEEDBACK = 6;
            const int FLD_INTERVIEWID = 7;



            InterviewFeedback interviewFeedback = new InterviewFeedback();

            interviewFeedback.CandidateID = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            interviewFeedback.CandidateName = reader.IsDBNull(FLD_CandidateName) ? string.Empty : reader.GetString(FLD_CandidateName);
            interviewFeedback.JobTitle = reader.IsDBNull(FLD_JobTitle) ? string.Empty : reader.GetString(FLD_JobTitle);
            interviewFeedback.JobPostingCode = reader.IsDBNull(FLD_ReqCode) ? string.Empty : reader.GetString(FLD_ReqCode);
            //interviewFeedback.ReqCode = reader.IsDBNull(FLD_ReqCode) ? 0 : reader.GetInt32(FLD_ReqCode);
            interviewFeedback.InterviewerName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            interviewFeedback.InterviewerDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            interviewFeedback.Feedback = reader.IsDBNull(FLD_INTERVIEWFEEDBACK) ? string.Empty : reader.GetString(FLD_INTERVIEWFEEDBACK);
            interviewFeedback.InterviewId = reader.IsDBNull(FLD_INTERVIEWID) ? 0 : reader.GetInt32(FLD_INTERVIEWID);

            return interviewFeedback;
        }


        //*******************************add by pravin khot *********************************
        public IList<InterviewFeedback> BuildInterviewFeedbackReportPrint(IDataReader reader)
        //IList<InterviewFeedback> IEntityBuilder<InterviewFeedback>.BuilInterviewFeedbackReport(IDataReader reader)
        {
            List<InterviewFeedback> interFeedback = new List<InterviewFeedback>();
            while (reader.Read())
            {
                interFeedback.Add(this.BuildGetInterviewFeedbackReportsPrint(reader));
                //interFeedback.Add(((IEntityBuilder<InterviewFeedback>)this).BuilGetInterviewFeedbackReports(reader));
            }

            return (interFeedback.Count > 0) ? interFeedback : null;
        }
        public InterviewFeedback BuildGetInterviewFeedbackReportsPrint(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_CandidateName = 1;
            const int FLD_Email = 2;
            const int FLD_EmployeeID = 3;
            const int FLD_InterviewRound = 4;
            const int FLD_DocumentName = 5;
            const int FLD_INTERVIEWFEEDBACK = 6;
            const int FLD_OverAllFeedback = 7;
            const int FLD_Title = 8;
          

            InterviewFeedback interviewFeedback = new InterviewFeedback();

            interviewFeedback.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            interviewFeedback.CandidateName = reader.IsDBNull(FLD_CandidateName) ? string.Empty : reader.GetString(FLD_CandidateName);

            interviewFeedback.Email = reader.IsDBNull(FLD_Email) ? string.Empty : reader.GetString(FLD_Email);
            interviewFeedback.EmployeeID = reader.IsDBNull(FLD_EmployeeID) ? string.Empty : reader.GetString(FLD_EmployeeID);
            interviewFeedback.InterviewRoundRpt = reader.IsDBNull(FLD_InterviewRound) ? string.Empty : reader.GetString(FLD_InterviewRound);
            interviewFeedback.DocumentName = reader.IsDBNull(FLD_DocumentName) ? string.Empty : reader.GetString(FLD_DocumentName);
            interviewFeedback.Feedback = reader.IsDBNull(FLD_INTERVIEWFEEDBACK) ? string.Empty : reader.GetString(FLD_INTERVIEWFEEDBACK);
            interviewFeedback.OverallFeedback = reader.IsDBNull(FLD_OverAllFeedback) ? string.Empty : reader.GetString(FLD_OverAllFeedback);
            interviewFeedback.Title = reader.IsDBNull(FLD_Title) ? string.Empty : reader.GetString(FLD_Title);
          

            return interviewFeedback;
        }

        //*******************************add by pravin khot *********************************
        public IList<InterviewFeedback> BuildInterviewFeedbackReportPrintAssessment(IDataReader reader)
        //IList<InterviewFeedback> IEntityBuilder<InterviewFeedback>.BuilInterviewFeedbackReport(IDataReader reader)
        {
            List<InterviewFeedback> interFeedback = new List<InterviewFeedback>();
            while (reader.Read())
            {
                interFeedback.Add(this.BuildGetInterviewFeedbackReportsPrintAssessment(reader));
                //interFeedback.Add(((IEntityBuilder<InterviewFeedback>)this).BuilGetInterviewFeedbackReports(reader));
            }

            return (interFeedback.Count > 0) ? interFeedback : null;
        }
        public InterviewFeedback BuildGetInterviewFeedbackReportsPrintAssessment(IDataReader reader)
        {

            const int FLD_Question = 0;
            const int FLD_Response = 1;
            const int FLD_rationale = 2;
            const int FLD_AnswerType = 3; //code added by pravin khot on 25/Jan/2016

            InterviewFeedback interviewFeedback = new InterviewFeedback();

            interviewFeedback.Question = reader.IsDBNull(FLD_Question) ? string.Empty : reader.GetString(FLD_Question);
            interviewFeedback.Response = reader.IsDBNull(FLD_Response) ? string.Empty : reader.GetString(FLD_Response);
            interviewFeedback.rationale = reader.IsDBNull(FLD_rationale) ? string.Empty : reader.GetString(FLD_rationale);
            interviewFeedback.AnswerType = reader.IsDBNull(FLD_AnswerType) ? string.Empty : reader.GetString(FLD_AnswerType);  //code added by pravin khot on 25/Jan/2016

            return interviewFeedback;
        }


        #region IEntityBuilder<InterviewFeedback> InterviewFeedback


        public InterviewFeedback BuildEntity(IDataReader dataReader)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
