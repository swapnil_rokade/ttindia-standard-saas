﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberSignatureBuilder : IEntityBuilder<MemberSignature>
    {
        IList<MemberSignature> IEntityBuilder<MemberSignature>.BuildEntities(IDataReader reader)
        {
            List<MemberSignature> memberSignatures = new List<MemberSignature>();

            while (reader.Read())
            {
                memberSignatures.Add(((IEntityBuilder<MemberSignature>)this).BuildEntity(reader));
            }

            return (memberSignatures.Count > 0) ? memberSignatures : null;
        }

        MemberSignature IEntityBuilder<MemberSignature>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_SIGNATURE = 1;
            const int FLD_LOGO = 2;
            const int FLD_ACTIVE = 3;
            const int FLD_ISREMOVED = 4;
            const int FLD_MEMBERID = 5;
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_CREATEDATE = 8;
            const int FLD_UPDATEDATE = 9;

            MemberSignature memberSignature = new MemberSignature();

            memberSignature.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberSignature.Signature = reader.IsDBNull(FLD_SIGNATURE) ? string.Empty : reader.GetString(FLD_SIGNATURE);
            memberSignature.Logo = reader.IsDBNull(FLD_LOGO) ? string.Empty : reader.GetString(FLD_LOGO);
            memberSignature.Active = reader.IsDBNull(FLD_ACTIVE) ? false : reader.GetBoolean(FLD_ACTIVE);
            memberSignature.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberSignature.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberSignature.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberSignature.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberSignature.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberSignature.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberSignature;
        }
    }
}