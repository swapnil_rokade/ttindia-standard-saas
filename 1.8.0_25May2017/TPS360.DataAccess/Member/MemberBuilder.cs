﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberBuilder.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              10/Jun/2016            Prasanth Kumar G    Introduced UserName , IsLDAP
 ---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberBuilder : IEntityBuilder<Member>
    {
        IList<Member> IEntityBuilder<Member>.BuildEntities(IDataReader reader)
        {
            List<Member> members = new List<Member>();

            while (reader.Read())
            {
                members.Add(((IEntityBuilder<Member>)this).BuildEntity(reader));
            }

            return (members.Count > 0) ? members : null;
        }
        public IList<Candidate> BuildCandidateEntities(IDataReader reader)
        {
            List<Candidate> members = new List<Candidate>();

            while (reader.Read())
            {
                members.Add(BuildCandidateEntity(reader));
            }

            return (members.Count > 0) ? members : null;
        }
        
        public IList<Member> BuildInterviewEntities(IDataReader reader)
        {
            List<Member> members = new List<Member>();

            while (reader.Read())
            {
                members.Add(BuildInterviewEntity(reader));
            }

            return (members.Count > 0) ? members : null;
        }
        public Member BuildInterviewEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_FIRSTNAME = 1;
            const int FLD_LASTNAME = 2;
            const int FLD_PRIMARYEMAIL = 3;

            Member  member = new Member ();

            member.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            member.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            member.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            member.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
            return member;
        }
        public Candidate BuildCandidateEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_FIRSTNAME = 1;
            const int FLD_LASTNAME = 2;
            const int FLD_PERMANENTCITY = 3;
            const int FLD_PRIMARYEMAIL = 4;
            const int FLD_CELLPHONE = 5;
            const int FLD_CURRENTPOSITION=6;
            const int FLD_YEARSOFEXPERIENCE=7;
            const int FLD_REMARKS=8;

            Candidate member = new Candidate();

            member.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            member.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            member.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            member.PermanentCity = reader.IsDBNull(FLD_PERMANENTCITY) ? string.Empty : reader.GetString(FLD_PERMANENTCITY);
            member.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
            member.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
            member.CurrentPosition = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
            member.TotalExperienceYears = reader.IsDBNull(FLD_YEARSOFEXPERIENCE) ? string.Empty : reader.GetString(FLD_YEARSOFEXPERIENCE);
            member.Remarks = reader.IsDBNull(FLD_REMARKS) ? string.Empty : reader.GetString(FLD_REMARKS);
            return member;
        }
        Member IEntityBuilder<Member>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERCODE = 1;
            const int FLD_FIRSTNAME = 2;
            const int FLD_MIDDLENAME = 3;
            const int FLD_LASTNAME = 4;
            const int FLD_NICKNAME = 5;
            const int FLD_DATEOFBIRTH = 6;
            const int FLD_PERMANENTADDRESSLINE1 = 7;
            const int FLD_PERMANENTADDRESSLINE2 = 8;
            const int FLD_PERMANENTCITY = 9;
            const int FLD_PERMANENTSTATEID = 10;
            const int FLD_PERMANENTZIP = 11;
            const int FLD_PERMANENTCOUNTRYID = 12;
            const int FLD_PERMANENTPHONE = 13;
            const int FLD_PERMANENTPHONEEXT = 14;
            const int FLD_PERMANENTMOBILE = 15;
            const int FLD_PRIMARYEMAIL = 16;
            const int FLD_ALTERNATEEMAIL = 17;
            const int FLD_PRIMARYPHONE = 18;
            const int FLD_PRIMARYPHONEEXTENSION = 19;
            const int FLD_CELLPHONE = 20;
            const int FLD_AUTOMATEDEMAILSTATUS = 21;
            const int FLD_RESUMESOURCE = 22;
            const int FLD_RESUMESHARING = 23;
            const int FLD_STATUS = 24;
            const int FLD_ISREMOVED = 25;
            const int FLD_USERID = 26;
            const int FLD_CREATORID = 27;
            const int FLD_UPDATORID = 28;
            const int FLD_CREATEDATE = 29;
            const int FLD_UPDATEDATE = 30;
            const int FLD_MEMBERTYPE = 31;
            const int FLD_EXPYEARS = 32;
            const int FLD_CURRENTCTC = 33;
            const int FLD_EXPECTEDCTC = 34;
            const int FLD_CURRENTEMPLOYER = 35;
            const int FLD_AVAILABILITY = 36;
            const int FLD_SKILL = 37;
            const int FLD_REMARKS = 38;//12373
            //Code introduced by Prasanth on 10/Jun/2016 Start
            const int FLD_USERNAME = 32;
            const int FLD_ISLDAP = 33;
            //*****************END*********************
            Member member = new Member();
            //Code commented by Prasanth on 10/Jun/2016 Start
            //if (reader.FieldCount > 32)
            //{
            //    member.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            //    member.MemberCode = reader.IsDBNull(FLD_MEMBERCODE) ? string.Empty : reader.GetString(FLD_MEMBERCODE);
            //    member.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            //    member.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
            //    member.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            //    member.NickName = reader.IsDBNull(FLD_NICKNAME) ? string.Empty : reader.GetString(FLD_NICKNAME);
            //    member.DateOfBirth = reader.IsDBNull(FLD_DATEOFBIRTH) ? DateTime.MinValue : reader.GetDateTime(FLD_DATEOFBIRTH);
            //    member.PermanentAddressLine1 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE1);
            //    member.PermanentAddressLine2 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE2);
            //    member.PermanentCity = reader.IsDBNull(FLD_PERMANENTCITY) ? string.Empty : reader.GetString(FLD_PERMANENTCITY);
            //    member.PermanentStateId = reader.IsDBNull(FLD_PERMANENTSTATEID) ? 0 : reader.GetInt32(FLD_PERMANENTSTATEID);
            //    member.PermanentZip = reader.IsDBNull(FLD_PERMANENTZIP) ? string.Empty : reader.GetString(FLD_PERMANENTZIP);
            //    member.PermanentCountryId = reader.IsDBNull(FLD_PERMANENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_PERMANENTCOUNTRYID);
            //    member.PermanentPhone = reader.IsDBNull(FLD_PERMANENTPHONE) ? string.Empty : reader.GetString(FLD_PERMANENTPHONE);
            //    member.PermanentPhoneExt = reader.IsDBNull(FLD_PERMANENTPHONEEXT) ? string.Empty : reader.GetString(FLD_PERMANENTPHONEEXT);
            //    member.PermanentMobile = reader.IsDBNull(FLD_PERMANENTMOBILE) ? string.Empty : reader.GetString(FLD_PERMANENTMOBILE);
            //    member.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
            //    member.AlternateEmail = reader.IsDBNull(FLD_ALTERNATEEMAIL) ? string.Empty : reader.GetString(FLD_ALTERNATEEMAIL);
            //    member.PrimaryPhone = reader.IsDBNull(FLD_PRIMARYPHONE) ? string.Empty : reader.GetString(FLD_PRIMARYPHONE);
            //    member.PrimaryPhoneExtension = reader.IsDBNull(FLD_PRIMARYPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_PRIMARYPHONEEXTENSION);
            //    member.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
            //    member.AutomatedEmailStatus = reader.IsDBNull(FLD_AUTOMATEDEMAILSTATUS) ? false : reader.GetBoolean(FLD_AUTOMATEDEMAILSTATUS);
            //    member.ResumeSource = reader.IsDBNull(FLD_RESUMESOURCE) ? 0 : reader.GetInt32(FLD_RESUMESOURCE);
            //    member.ResumeSharing = reader.IsDBNull(FLD_RESUMESHARING) ? 0 : reader.GetInt32(FLD_RESUMESHARING);
            //    member.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
            //    member.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            //    member.UserId = reader.IsDBNull(FLD_USERID) ? Guid.Empty : reader.GetGuid(FLD_USERID);
            //    member.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            //    member.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            //    member.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            //    member.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            //    member.MemberType = reader.IsDBNull(FLD_MEMBERTYPE) ? 0 : reader.GetInt32(FLD_MEMBERTYPE);//12373
            //    member.ExpYears = reader.IsDBNull(FLD_EXPYEARS) ? string.Empty : reader.GetString(FLD_EXPYEARS);
            //    member.CurrentCTC = reader.IsDBNull(FLD_CURRENTCTC) ? string.Empty : reader.GetString(FLD_CURRENTCTC);
            //    member.ExpectedCTC = reader.IsDBNull(FLD_EXPECTEDCTC) ? string.Empty : reader.GetString(FLD_EXPECTEDCTC);
            //    member.CurrentEmployer = reader.IsDBNull(FLD_CURRENTEMPLOYER) ? string.Empty : reader.GetString(FLD_CURRENTEMPLOYER);
            //    member.Avaliability = reader.IsDBNull(FLD_AVAILABILITY) ? string.Empty : reader.GetString(FLD_AVAILABILITY);
            //    member.Skills = reader.IsDBNull(FLD_SKILL) ? string.Empty : reader.GetString(FLD_SKILL);
            //    member.Remarks = reader.IsDBNull(FLD_REMARKS) ? string.Empty : reader.GetString(FLD_REMARKS);
            //}
            //else
            //{
            //    member.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            //    member.MemberCode = reader.IsDBNull(FLD_MEMBERCODE) ? string.Empty : reader.GetString(FLD_MEMBERCODE);
            //    member.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            //    member.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
            //    member.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            //    member.NickName = reader.IsDBNull(FLD_NICKNAME) ? string.Empty : reader.GetString(FLD_NICKNAME);
            //    member.DateOfBirth = reader.IsDBNull(FLD_DATEOFBIRTH) ? DateTime.MinValue : reader.GetDateTime(FLD_DATEOFBIRTH);
            //    member.PermanentAddressLine1 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE1);
            //    member.PermanentAddressLine2 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE2);
            //    member.PermanentCity = reader.IsDBNull(FLD_PERMANENTCITY) ? string.Empty : reader.GetString(FLD_PERMANENTCITY);
            //    member.PermanentStateId = reader.IsDBNull(FLD_PERMANENTSTATEID) ? 0 : reader.GetInt32(FLD_PERMANENTSTATEID);
            //    member.PermanentZip = reader.IsDBNull(FLD_PERMANENTZIP) ? string.Empty : reader.GetString(FLD_PERMANENTZIP);
            //    member.PermanentCountryId = reader.IsDBNull(FLD_PERMANENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_PERMANENTCOUNTRYID);
            //    member.PermanentPhone = reader.IsDBNull(FLD_PERMANENTPHONE) ? string.Empty : reader.GetString(FLD_PERMANENTPHONE);
            //    member.PermanentPhoneExt = reader.IsDBNull(FLD_PERMANENTPHONEEXT) ? string.Empty : reader.GetString(FLD_PERMANENTPHONEEXT);
            //    member.PermanentMobile = reader.IsDBNull(FLD_PERMANENTMOBILE) ? string.Empty : reader.GetString(FLD_PERMANENTMOBILE);
            //    member.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
            //    member.AlternateEmail = reader.IsDBNull(FLD_ALTERNATEEMAIL) ? string.Empty : reader.GetString(FLD_ALTERNATEEMAIL);
            //    member.PrimaryPhone = reader.IsDBNull(FLD_PRIMARYPHONE) ? string.Empty : reader.GetString(FLD_PRIMARYPHONE);
            //    member.PrimaryPhoneExtension = reader.IsDBNull(FLD_PRIMARYPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_PRIMARYPHONEEXTENSION);
            //    member.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
            //    member.AutomatedEmailStatus = reader.IsDBNull(FLD_AUTOMATEDEMAILSTATUS) ? false : reader.GetBoolean(FLD_AUTOMATEDEMAILSTATUS);
            //    member.ResumeSource = reader.IsDBNull(FLD_RESUMESOURCE) ? 0 : reader.GetInt32(FLD_RESUMESOURCE);
            //    member.ResumeSharing = reader.IsDBNull(FLD_RESUMESHARING) ? 0 : reader.GetInt32(FLD_RESUMESHARING);
            //    member.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
            //    member.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            //    member.UserId = reader.IsDBNull(FLD_USERID) ? Guid.Empty : reader.GetGuid(FLD_USERID);
            //    member.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            //    member.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            //    member.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            //    member.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            //    member.MemberType = reader.IsDBNull(FLD_MEMBERTYPE) ? 0 : reader.GetInt32(FLD_MEMBERTYPE);//12373
            //}
            //******************************END*********************************
            //Code introduced by Prasanth on 10/Jun/2016 Start

            member.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            member.MemberCode = reader.IsDBNull(FLD_MEMBERCODE) ? string.Empty : reader.GetString(FLD_MEMBERCODE);
            member.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            member.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
            member.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            member.NickName = reader.IsDBNull(FLD_NICKNAME) ? string.Empty : reader.GetString(FLD_NICKNAME);
            member.DateOfBirth = reader.IsDBNull(FLD_DATEOFBIRTH) ? DateTime.MinValue : reader.GetDateTime(FLD_DATEOFBIRTH);
            member.PermanentAddressLine1 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE1);
            member.PermanentAddressLine2 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE2);
            member.PermanentCity = reader.IsDBNull(FLD_PERMANENTCITY) ? string.Empty : reader.GetString(FLD_PERMANENTCITY);
            member.PermanentStateId = reader.IsDBNull(FLD_PERMANENTSTATEID) ? 0 : reader.GetInt32(FLD_PERMANENTSTATEID);
            member.PermanentZip = reader.IsDBNull(FLD_PERMANENTZIP) ? string.Empty : reader.GetString(FLD_PERMANENTZIP);
            member.PermanentCountryId = reader.IsDBNull(FLD_PERMANENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_PERMANENTCOUNTRYID);
            member.PermanentPhone = reader.IsDBNull(FLD_PERMANENTPHONE) ? string.Empty : reader.GetString(FLD_PERMANENTPHONE);
            member.PermanentPhoneExt = reader.IsDBNull(FLD_PERMANENTPHONEEXT) ? string.Empty : reader.GetString(FLD_PERMANENTPHONEEXT);
            member.PermanentMobile = reader.IsDBNull(FLD_PERMANENTMOBILE) ? string.Empty : reader.GetString(FLD_PERMANENTMOBILE);
            member.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
            member.AlternateEmail = reader.IsDBNull(FLD_ALTERNATEEMAIL) ? string.Empty : reader.GetString(FLD_ALTERNATEEMAIL);
            member.PrimaryPhone = reader.IsDBNull(FLD_PRIMARYPHONE) ? string.Empty : reader.GetString(FLD_PRIMARYPHONE);
            member.PrimaryPhoneExtension = reader.IsDBNull(FLD_PRIMARYPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_PRIMARYPHONEEXTENSION);
            member.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
            member.AutomatedEmailStatus = reader.IsDBNull(FLD_AUTOMATEDEMAILSTATUS) ? false : reader.GetBoolean(FLD_AUTOMATEDEMAILSTATUS);
            member.ResumeSource = reader.IsDBNull(FLD_RESUMESOURCE) ? 0 : reader.GetInt32(FLD_RESUMESOURCE);
            member.ResumeSharing = reader.IsDBNull(FLD_RESUMESHARING) ? 0 : reader.GetInt32(FLD_RESUMESHARING);
            member.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
            member.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            member.UserId = reader.IsDBNull(FLD_USERID) ? Guid.Empty : reader.GetGuid(FLD_USERID);
            member.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            member.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            member.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            member.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            member.MemberType = reader.IsDBNull(FLD_MEMBERTYPE) ? 0 : reader.GetInt32(FLD_MEMBERTYPE);


            if (reader.FieldCount == 34)
            {
                member.UserName = reader.IsDBNull(FLD_USERNAME) ? string.Empty : reader.GetString(FLD_USERNAME);
                member.IsLDAP = reader.IsDBNull(FLD_ISLDAP) ? false : reader.GetBoolean(FLD_ISLDAP);

            }
            else if (reader.FieldCount > 34)
            {
                member.ExpYears = reader.IsDBNull(FLD_EXPYEARS) ? string.Empty : reader.GetString(FLD_EXPYEARS);
                member.CurrentCTC = reader.IsDBNull(FLD_CURRENTCTC) ? string.Empty : reader.GetString(FLD_CURRENTCTC);
                member.ExpectedCTC = reader.IsDBNull(FLD_EXPECTEDCTC) ? string.Empty : reader.GetString(FLD_EXPECTEDCTC);
                member.CurrentEmployer = reader.IsDBNull(FLD_CURRENTEMPLOYER) ? string.Empty : reader.GetString(FLD_CURRENTEMPLOYER);
                member.Avaliability = reader.IsDBNull(FLD_AVAILABILITY) ? string.Empty : reader.GetString(FLD_AVAILABILITY);
                member.Skills = reader.IsDBNull(FLD_SKILL) ? string.Empty : reader.GetString(FLD_SKILL);
                member.Remarks = reader.IsDBNull(FLD_REMARKS) ? string.Empty : reader.GetString(FLD_REMARKS);
            }
            //*********************END***********************
            return member;
        }
    }
}