﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              May-28-2009           Veda             DefectId:10493; Added "GetLicenseKey"
 *  0.2              24-Jun-2009           Anand Dixit      Changes due to renaming of LicenseSectionHandler to TPS360Section handler are implemented
 *  0.3              Oct-07-2009           Veda             DefectId:11534 ; Added Licenseflag
 *  0.4              Oct-28-2009           Ranjit Kumar.I   DefectID #11769 added code in GetLicenseKey method
 *  0.5              Nov-19-1009           Sandeesh         Enhancement Id:10998 -Added a new method 'GetMemberUserNameById' for getting the username of the user
 *  0.6              Dec-23-2009           Anand Dixit      Modified GetLicenseKey method (implemented using(..) to dispose disposibles.)
*   0.7              02/Dec/2015           pravin khot      GetAllNameWithEmailByRoleName_IP added
*   0.8              09/Dec/2015           pravin khot      GetAllSuggestedInterviewer(string RequisitionName)
 *  0.9              23/Feb/2016           pravin khot      Introduced by DeleteRoleRequisition, UpdateRoleRequisition , GetCustomRoleRequisitiondetail
    1.0              7/March/2016          pravin khot       Introduced by GetAllNameWithEmailByRoleNameIfExisting
 *  1.1              27/April/2016         pravin khot      Introduced by UpdateMemberthroughvendor,CandidateAvailableInRequisition_Name
 *  1.2              16/May/2016           pravin khot       added new -GetAllNameByTeamMemberId
    1.2              10/Jun/2016           Prasanth Kumar G  Introduced LDAP,Function GetMemberByMemberUserName
    1.3              10/Jun/2016           Prasanth Kumar G  Introduced function GetMemberADUserNameById
 ---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Configuration;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Collections.Specialized;
using System.Web;
using System.Data.SqlTypes;
using TPS360.Common;

namespace TPS360.DataAccess
{
    internal sealed class MemberDataAccess : BaseDataAccess, IMemberDataAccess
    {
        #region Constructors

        public MemberDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<Member> CreateEntityBuilder<Member>()
        {
            return (new MemberBuilder()) as IEntityBuilder<Member>;
        }

        #endregion

        #region  Methods
        Member IMemberDataAccess.GetMemberByMemberEmail(string PrimaryEmail)
        {

            const string SP = "dbo.Member_GetByMemberEmail";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                Database.AddInParameter(cmd, "@PrimaryEmail", DbType.String, PrimaryEmail);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Member>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        //functon introduced by Prasanth on 10/Jun/2016 
        Member IMemberDataAccess.GetMemberByMemberUserName(string PrimaryEmail)
        {

            const string SP = "dbo.Member_GetByMemberUserName";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@UserName", DbType.String, PrimaryEmail);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Member>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        Member IMemberDataAccess.Add(Member member)
        {
            const string SP = "dbo.Member_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberCode", DbType.AnsiString, StringHelper.Convert(member.MemberCode));
                Database.AddInParameter(cmd, "@FirstName", DbType.AnsiString, StringHelper.Convert(member.FirstName));
                Database.AddInParameter(cmd, "@MiddleName", DbType.AnsiString, StringHelper.Convert(member.MiddleName));
                Database.AddInParameter(cmd, "@LastName", DbType.AnsiString, StringHelper.Convert(member.LastName));
                Database.AddInParameter(cmd, "@NickName", DbType.AnsiString, StringHelper.Convert(member.NickName));
                Database.AddInParameter(cmd, "@DateOfBirth", DbType.DateTime, NullConverter.Convert(member.DateOfBirth));
                Database.AddInParameter(cmd, "@PermanentAddressLine1", DbType.AnsiString, StringHelper.Convert(member.PermanentAddressLine1));
                Database.AddInParameter(cmd, "@PermanentAddressLine2", DbType.AnsiString, StringHelper.Convert(member.PermanentAddressLine2));
                Database.AddInParameter(cmd, "@PermanentCity", DbType.AnsiString, StringHelper.Convert(member.PermanentCity));
                Database.AddInParameter(cmd, "@PermanentStateId", DbType.Int32, member.PermanentStateId);
                Database.AddInParameter(cmd, "@PermanentZip", DbType.AnsiString, StringHelper.Convert(member.PermanentZip));
                Database.AddInParameter(cmd, "@PermanentCountryId", DbType.Int32, member.PermanentCountryId);
                Database.AddInParameter(cmd, "@PermanentPhone", DbType.AnsiString, member.PermanentPhone);
                Database.AddInParameter(cmd, "@PermanentPhoneExt", DbType.AnsiString, member.PermanentPhoneExt);
                Database.AddInParameter(cmd, "@PermanentMobile", DbType.AnsiString, member.PermanentMobile);
                Database.AddInParameter(cmd, "@PrimaryEmail", DbType.AnsiString, StringHelper.Convert(member.PrimaryEmail));
                Database.AddInParameter(cmd, "@AlternateEmail", DbType.AnsiString, StringHelper.Convert(member.AlternateEmail));
                Database.AddInParameter(cmd, "@PrimaryPhone", DbType.AnsiString, StringHelper.Convert(member.PrimaryPhone));
                Database.AddInParameter(cmd, "@PrimaryPhoneExtension", DbType.AnsiString, StringHelper.Convert(member.PrimaryPhoneExtension));
                Database.AddInParameter(cmd, "@CellPhone", DbType.AnsiString, StringHelper.Convert(member.CellPhone));
                Database.AddInParameter(cmd, "@AutomatedEmailStatus", DbType.Boolean, member.AutomatedEmailStatus);
                Database.AddInParameter(cmd, "@ResumeSource", DbType.Int32, member.ResumeSource);
                Database.AddInParameter(cmd, "@ResumeSharing", DbType.Int32, member.ResumeSharing);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, member.Status);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, member.IsRemoved);
                Database.AddInParameter(cmd, "@UserId", DbType.Guid, member.UserId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, member.CreatorId);
                Database.AddInParameter(cmd, "@MemberType", DbType.Int32, member.MemberType);//12373

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {



                        member = CreateEntityBuilder<Member>().BuildEntity(reader);
                    }
                    else
                    {
                        member = null;
                    }
                }

                if (member == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member already exists. Please specify another member.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member.");
                            }
                    }
                }

                return member;
            }
        }


        Member IMemberDataAccess.AddFullInfo(Member member, bool IsCreateMemberDetailandExtendedInfo, int Availability, bool IsCreateMemberManager)
        {
            const string SP = "AddFullMemberInfo";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                //Code introduced by Prasanth on 10/Jun/2016 Starty
                Database.AddInParameter(cmd, "@UserName", DbType.AnsiString, StringHelper.Convert(member.UserName));
                Database.AddInParameter(cmd, "@IsLDAP", DbType.Boolean, StringHelper.Convert(member.IsLDAP));
                //*****************END************************s
                Database.AddInParameter(cmd, "@FirstName", DbType.AnsiString, StringHelper.Convert(member.FirstName));
                Database.AddInParameter(cmd, "@LastName", DbType.AnsiString, StringHelper.Convert(member.LastName));
                Database.AddInParameter(cmd, "@PrimaryEmail", DbType.AnsiString, StringHelper.Convert(member.PrimaryEmail));
                Database.AddInParameter(cmd, "@ResumeSource", DbType.Int32, member.ResumeSource);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, member.Status);
                Database.AddInParameter(cmd, "@UserId", DbType.Guid, member.UserId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, member.CreatorId);
                Database.AddInParameter(cmd, "@CreateDate", DbType.DateTime, member.CreateDate);//12373
                Database.AddInParameter(cmd, "@IsCreateMemberDetailandExtendedInfo", DbType.Boolean, IsCreateMemberDetailandExtendedInfo);
                Database.AddInParameter(cmd, "@Availability", DbType.Int32, Availability);
                Database.AddInParameter(cmd, "@IsCreateMemberManager", DbType.Boolean, IsCreateMemberManager);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        member = CreateEntityBuilder<Member>().BuildEntity(reader);
                    }
                    else
                    {
                        member = null;
                    }
                }

                int returnCode = GetReturnCodeFromParameter(cmd);
                if (member == null)
                {

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member already exists. Please specify another member.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member.");
                            }
                    }
                }

                if (returnCode == 1010) //
                {
                    member = new Member();
                }

                return member;
            }
        }

        //-----------------CODE introduced by pravin khot 09/Dec/2015-------------------
        ArrayList IMemberDataAccess.GetAllSuggestedInterviewer(int RequisitionId)
        {
            ArrayList allMemberNameList = new ArrayList();
            if (RequisitionId < 1)
            {
                throw new ArgumentNullException("RequisitionId");
            }

            const string SP = "dbo.GetAllSuggestedInterviewerWithEmail";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RequisitionId", DbType.AnsiString, RequisitionId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        allMemberNameList.Add(new { Id = reader.GetInt32(0), Name = reader.GetString(1) });
                    }
                }
            }

            return allMemberNameList;
        }
        //-------------------------------------End-----------------------------------------

        //-----------------CODE introduced by pravin khot 02/dec/2015-------------------
        ArrayList IMemberDataAccess.GetAllNameWithEmailByRoleName_IP(string roleName, int paneltypeid)
        {
            ArrayList allMemberNameList = new ArrayList();

            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentException("RoleName");
            }

            const string SP = "dbo.Member_GetAllNameAndEmailByRoleName_IP";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RoleName", DbType.AnsiString, roleName);
                Database.AddInParameter(cmd, "@paneltypeid", DbType.Int32, paneltypeid);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        allMemberNameList.Add(new { Id = reader.GetInt32(0), Name = reader.GetString(1) });
                    }
                }
            }

            return allMemberNameList;
        }
        //--------------------end------------------------------------

        ArrayList IMemberDataAccess.GetLicenseKey(string intLicenseKey, string varchrDomainName)
        {
            const string SP = "LicenseKey_Validate";
        //    TPS360SectionHandler licenseSection = WebConfigurationManager.GetSection("tps360license") as TPS360SectionHandler; ;
            ArrayList newarry = new ArrayList();
            string TPS360LV = new TPS360ConfigElement().Value;
            using (SqlConnection l_connection = new SqlConnection())
            {
                l_connection.ConnectionString = TPS360LV;
                l_connection.Open();
                using (DbCommand cmd = l_connection.CreateCommand())
                {
                    cmd.CommandText = SP;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DbParameter param = cmd.CreateParameter();
                    DbParameter param1 = cmd.CreateParameter();

                    param.DbType = DbType.AnsiString;
                    param.ParameterName = "@intLicenseKey";
                    param.Value = intLicenseKey;
                    cmd.Parameters.Add(param);

                    param1.DbType = DbType.AnsiString;
                    param1.ParameterName = "@varchrDomainName";
                    param1.Value = varchrDomainName;
                    cmd.Parameters.Add(param1);
                    using (SqlDataReader reader = (SqlDataReader)cmd.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        if (reader.Read())
                        {
                            if (reader.GetInt32(2) != 0)// DefectId #11769
                            {
                                DateTime dtStart = reader.GetDateTime(0);
                                DateTime dtExpire = reader.GetDateTime(1);
                                int LicenseValidFlag = reader.GetInt32(2);  //0.3
                                int NoofUsersAllowed = reader.GetInt32(3);
                                newarry.Add(dtStart);
                                newarry.Add(dtExpire);
                                newarry.Add(LicenseValidFlag); //0.3
                                newarry.Add(NoofUsersAllowed);
                            }
                        }
                    }
                }
                l_connection.Close();
            }
            return newarry;

        } //0.1

        Member IMemberDataAccess.Update(Member member)
        {
            const string SP = "dbo.Member_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, member.Id);
                Database.AddInParameter(cmd, "@MemberCode", DbType.AnsiString, StringHelper.Convert(member.MemberCode));
                Database.AddInParameter(cmd, "@FirstName", DbType.AnsiString, StringHelper.Convert(member.FirstName));
                Database.AddInParameter(cmd, "@MiddleName", DbType.AnsiString, StringHelper.Convert(member.MiddleName));
                Database.AddInParameter(cmd, "@LastName", DbType.AnsiString, StringHelper.Convert(member.LastName));
                Database.AddInParameter(cmd, "@NickName", DbType.AnsiString, StringHelper.Convert(member.NickName));
                Database.AddInParameter(cmd, "@DateOfBirth", DbType.DateTime, NullConverter.Convert(member.DateOfBirth));
                Database.AddInParameter(cmd, "@PermanentAddressLine1", DbType.AnsiString, StringHelper.Convert(member.PermanentAddressLine1));
                Database.AddInParameter(cmd, "@PermanentAddressLine2", DbType.AnsiString, StringHelper.Convert(member.PermanentAddressLine2));
                Database.AddInParameter(cmd, "@PermanentCity", DbType.AnsiString, StringHelper.Convert(member.PermanentCity));
                Database.AddInParameter(cmd, "@PermanentStateId", DbType.Int32, member.PermanentStateId);
                Database.AddInParameter(cmd, "@PermanentZip", DbType.AnsiString, StringHelper.Convert(member.PermanentZip));
                Database.AddInParameter(cmd, "@PermanentCountryId", DbType.Int32, member.PermanentCountryId);
                Database.AddInParameter(cmd, "@PermanentPhone", DbType.AnsiString, member.PermanentPhone);
                Database.AddInParameter(cmd, "@PermanentPhoneExt", DbType.AnsiString, member.PermanentPhoneExt);
                Database.AddInParameter(cmd, "@PermanentMobile", DbType.AnsiString, member.PermanentMobile);
                Database.AddInParameter(cmd, "@PrimaryEmail", DbType.AnsiString, StringHelper.Convert(member.PrimaryEmail));
                Database.AddInParameter(cmd, "@AlternateEmail", DbType.AnsiString, StringHelper.Convert(member.AlternateEmail));
                Database.AddInParameter(cmd, "@PrimaryPhone", DbType.AnsiString, StringHelper.Convert(member.PrimaryPhone));
                Database.AddInParameter(cmd, "@PrimaryPhoneExtension", DbType.AnsiString, StringHelper.Convert(member.PrimaryPhoneExtension));
                Database.AddInParameter(cmd, "@CellPhone", DbType.AnsiString, StringHelper.Convert(member.CellPhone));
                Database.AddInParameter(cmd, "@AutomatedEmailStatus", DbType.Boolean, member.AutomatedEmailStatus);
                Database.AddInParameter(cmd, "@ResumeSource", DbType.Int32, member.ResumeSource);
                Database.AddInParameter(cmd, "@ResumeSharing", DbType.Int32, member.ResumeSharing);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, member.Status);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, member.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, member.UpdatorId);
                Database.AddInParameter(cmd, "@MemberType", DbType.Int32, member.MemberType);//12373
                Database.AddInParameter(cmd, "@UserId", DbType.Guid, member.UserId);
                //Code introduced by Prasanth on 10/Jun/2016 Starty
                Database.AddInParameter(cmd, "@UserName", DbType.AnsiString, StringHelper.Convert(member.UserName));
                Database.AddInParameter(cmd, "@IsLDAP", DbType.Boolean, StringHelper.Convert(member.IsLDAP));
                //*****************END************************s
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        member = CreateEntityBuilder<Member>().BuildEntity(reader);
                    }
                    else
                    {
                        member = null;
                    }
                }

                if (member == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member already exists. Please specify another member.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member.");
                            }
                    }
                }

                return member;
            }
        }

        Member IMemberDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Member_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Member>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        /* 0.5 End*/

        public string GetMemberUserNameById(int id)     
        {
            const string SP = "dbo.Member_GetUserNameById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                string strUserName = (Database.ExecuteScalar(cmd) == null || Database.ExecuteScalar(cmd).ToString() == "") ? "" : Convert.ToString(Database.ExecuteScalar(cmd)); //0.4 "put null" here 
                return strUserName;
            }
        }
        //function introduced by Prasanth on 10/Jun/2016
        public string GetMemberADUserNameById(int id)
        {
            const string SP = "dbo.Member_GetADUserNameById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                string strUserName = (Database.ExecuteScalar(cmd) == null || Database.ExecuteScalar(cmd).ToString() == "") ? "" : Convert.ToString(Database.ExecuteScalar(cmd)); //0.4 "put null" here 
                return strUserName;
            }
        }

        public string GetMemberNameById(int id)
        {
            const string SP = "dbo.Member_GetMemberNameById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                string strMemberName = (Database.ExecuteScalar(cmd) == null || Database.ExecuteScalar(cmd).ToString() == "") ? "" : Convert.ToString(Database.ExecuteScalar(cmd));
                return strMemberName;
            }
        }
        /* 0.5 End*/

        Member IMemberDataAccess.GetByUserId(Guid userId)
        {
            const string SP = "dbo.Member_GetByUserId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@UserId", DbType.Guid, userId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Member>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        Member IMemberDataAccess.GetByUserName(string userName)
        {
            const string SP = "dbo.Member_GetByUserName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@UserName", DbType.AnsiString, userName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Member>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<Member> IMemberDataAccess.GetAllByJobPostingId(int jobPostingId)
        {
            const string SP = "dbo.Member_GetAllMemberByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Member>().BuildEntities(reader);
                }
            }
        }

        IList<Member> IMemberDataAccess.GetAllByCustomRoleId(int customRoleId)
        {
            const string SP = "dbo.Member_GetAllByCustomRoleId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CustomRoleId", DbType.Int32, customRoleId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Member>().BuildEntities(reader);
                }
            }
        }

        IList<Member> IMemberDataAccess.GetAllByCreatorIdAndRoleName(int creatorId, string roleName)
        {
            const string SP = "dbo.Member_GetAllByCreatorIdAndRoleName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, creatorId);
                Database.AddInParameter(cmd, "@RoleName", DbType.String, roleName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Member>().BuildEntities(reader);
                }
            }
        }

        IList<Member> IMemberDataAccess.GetAllMemberByMemberGroupId(int memberGroupId)
        {
            if (memberGroupId < 1)
            {
                throw new ArgumentNullException("memberGroupId");
            }

            const string SP = "dbo.Member_GetAllMemberByMemberGroupId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberGroupId", DbType.Int32, memberGroupId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Member>().BuildEntities(reader);
                }
            }
        }

        IList<Member> IMemberDataAccess.GetAllMemberGroupManagerByMemberGroupId(int memberGroupId)
        {
            if (memberGroupId < 1)
            {
                throw new ArgumentNullException("memberGroupId");
            }

            const string SP = "dbo.Member_GetAllMemberGroupManagerByMemberGroupId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberGroupId", DbType.Int32, memberGroupId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Member>().BuildEntities(reader);
                }
            }
        }

        IList<Member> IMemberDataAccess.GetAll()
        {
            const string SP = "dbo.Member_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Member>().BuildEntities(reader);
                }
            }
        }

        PagedResponse<Member> IMemberDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.Member_GetPaged";
            string whereClause = string.Empty;

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "FirstName";
            }

            request.SortColumn = "[M].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Member> response = new PagedResponse<Member>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<Member>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<Candidate> IMemberDataAccess.GetPagedByMemberGroupId(PagedRequest request)
        {
            const string SP = "dbo.Member_GetPagedByMemberGroupId";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "MemberGroupId"))
                        {
                            sb.Append("[MGM].[MemberGroupId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "FirstName";
            }

            request.SortColumn = "[C].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Candidate> response = new PagedResponse<Candidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new MemberBuilder()).BuildCandidateEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<Member> IMemberDataAccess.GetPagedByRoleNameAndCreatorId(PagedRequest request)
        {
            const string SP = "dbo.Member_GetPagedByRoleNameAndCreatorId";
            string whereClause = string.Empty;
            //int intCreatorId = 0;
            string roleName = String.Empty;
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "CreatorId"))
                        {
                            sb.Append("[M].[JobTitle]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else if (StringHelper.IsEqual(column, "RoleName"))
                        {
                            sb.Append("[R].[RoleName]");
                            sb.Append(" LIKE '");
                            if (!value.StartsWith("%"))
                            {
                                sb.Append("%");
                            }
                            sb.Append(value);
                            if (!value.EndsWith("%"))
                            {
                                sb.Append("%");
                            }
                            sb.Append("'");
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "FirstName";
            }

            request.SortColumn = "[M].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Member> response = new PagedResponse<Member>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<Member>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        void IMemberDataAccess.UpdateAspNet_User(string UserName,Guid UserId)
        {
            const string SP = "dbo.aspnet_Users_UpdateUser";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@UserName", DbType.AnsiString, UserName);
                Database.AddInParameter(cmd, "@UserId", DbType.Guid , UserId);
                Database.ExecuteNonQuery(cmd);
            }
        }
        bool IMemberDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Member_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member.");
                        }
                }
            }
        }

        ArrayList IMemberDataAccess.GetAllNameByRoleName(string roleName)
        {
            ArrayList allMemberNameList = new ArrayList();

            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentException("RoleName");
            }

            const string SP = "dbo.Member_GetAllNameByRoleName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RoleName", DbType.AnsiString, roleName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        allMemberNameList.Add(new { Id = reader.GetInt32(0), Name = reader.GetString(1) });
                    }
                }
            }

            return allMemberNameList;
        }
         
        ArrayList IMemberDataAccess.GetAllNameWithEmailByRoleName(string roleName)
        {
            ArrayList allMemberNameList = new ArrayList();

            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentException("RoleName");
            }

            const string SP = "dbo.Member_GetAllNameAndEmailByRoleName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RoleName", DbType.AnsiString, roleName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        allMemberNameList.Add(new { Id = reader.GetInt32(0), Name = reader.GetString(1) });
                    }
                }
            }

            return allMemberNameList;
        }
        //**************Code added by pravin khot on 7/March/2016*******************
        ArrayList IMemberDataAccess.GetAllNameWithEmailByRoleNameIfExisting(string roleName)
        {
            ArrayList allMemberNameList = new ArrayList();

            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentException("RoleName");
            }

            const string SP = "dbo.Member_GetAllNameAndEmailByRoleNameIfExisting";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RoleName", DbType.AnsiString, roleName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        allMemberNameList.Add(new { Id = reader.GetInt32(0), Name = reader.GetString(1) });
                    }
                }
            }

            return allMemberNameList;
        }
        //**********************************END********************************************
        ArrayList IMemberDataAccess.GetAllEmployeeNameWithEmail(string roleName)
        {
            ArrayList allMemberNameList = new ArrayList();

            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentException("RoleName");
            }

            const string SP = "dbo.Member_GetAllNameAndEmailByRoleName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RoleName", DbType.AnsiString, roleName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        allMemberNameList.Add(new Employee  { Id = reader.GetInt32(0), FirstName  = reader.GetString(1) });
                    }
                }
            }

            return allMemberNameList;
        }

        IList<Member> IMemberDataAccess.GetAllByIds(string Ids)
        {
            if (string.IsNullOrEmpty(Ids))
            {
                throw new ArgumentException("Ids");
            }

            const string SP = "dbo.Member_GetAllByMultipleIds";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Ids", DbType.AnsiString, Ids);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Member>().BuildEntities(reader);
                }
            }
        }

        IList<Member> IMemberDataAccess.GetAllMemberDetailsByInterviewId (int InterviewId,int MemberId)
        {
            if (InterviewId < 0)
            {
                throw new ArgumentException("InterviewId");
            }

            if (MemberId < 0)
            {
                throw new ArgumentException("MemberId");
            }
            const string SP = "dbo.InterviewMailDetail_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewId", DbType.AnsiString, InterviewId );
                Database.AddInParameter(cmd, "@MemberId", DbType.AnsiString, MemberId );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return (new MemberBuilder()).BuildInterviewEntities(reader); 
                }
            }
        }

        string IMemberDataAccess.GetNewMermberIdentityId()
        {
            string newMemberId = string.Empty;
            const string SP = "dbo.Member_NewMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        newMemberId = reader.GetInt32(0).ToString();
                    }
                }
            }

            return newMemberId;
        }

        Int32 IMemberDataAccess.GetMemberIdByMemberEmail(string Email)
        {
            if (string.IsNullOrEmpty(Email))
            {
                throw new ArgumentNullException("Email");
            }

            const string SP = "dbo.Member_GetMemberIdByMemberEmail";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Email", DbType.String, Email );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 MemberId = 0;

                    while (reader.Read())
                    {
                        MemberId = reader.GetInt32(0);
                    }

                    return MemberId;
                }
            }
        }

        Int32 IMemberDataAccess.GetMemberIdByEmail(string Email)
        {
            if (string.IsNullOrEmpty(Email))
            {
                throw new ArgumentNullException("Email");
            }

            const string SP = "dbo.Member_GetMemberIdByEmail";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Email", DbType.String, Email);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 MemberId = 0;

                    while (reader.Read())
                    {
                        MemberId = reader.GetInt32(0);
                    }

                    return MemberId;
                }
            }
        }


        Int32 IMemberDataAccess.GetCreatorIdForMember(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Member_GetCreatorIdForMember";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);             

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 CreatorId = 0;

                    while (reader.Read())
                    {
                        CreatorId = reader.GetInt32(0);
                    }

                    return CreatorId;
                }
            }
        }
        void IMemberDataAccess.UnSubsciribeMemberFromBulkEmail(int MemberID)
        {
            if (MemberID < 0)
            {
                throw new ArgumentException("MemberId");
            }
                const string SP = "dbo.Member_UnSubscribeBulkemail";

                  using (DbCommand cmd = Database.GetStoredProcCommand(SP))
                  {
                      Database.AddInParameter(cmd, "@MemberID", DbType.Int32 , MemberID );
                      Database.ExecuteNonQuery(cmd);

                  }

           
        }
        int IMemberDataAccess.GetMemberStatusByMemberId(int memberId) 
        {
            if (memberId < 0) 
            {
                throw new ArgumentException("MemberId");
            }
            const string SP = "dbo.Member_GetMemberStatusByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberId);
                using (IDataReader reader = Database.ExecuteReader(cmd)) 
                {
                    if (reader.Read()) 
                    {
                        return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                
                }
            
            }
            return 0;
        }

        int IMemberDataAccess.GetMemberIdByCompanyContact(int CompanyContactId)
        {
            if (CompanyContactId < 0)
            {
                throw new ArgumentException("CompanyContactId");
            }
            const string SP = "dbo.Member_GetMemberIdByCompanyContact";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyContactId", DbType.Int32, CompanyContactId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }

                }

            }
            return 0;
        }

        //**********Code added by pravin khot on 23/Feb/2016*** START CODE ******
        void IMemberDataAccess.DeleteRoleRequisition()
        {
            const string SP = "dbo.DeleteRoleRequisitionNew";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.ExecuteNonQuery(cmd);
            }
        }

        void IMemberDataAccess.UpdateRoleRequisition(int RoleId, int MemberId, int sectionid)
        {
            const string SP = "dbo.UpdateRoleRequisitionByRoleIdNew";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RoleId", DbType.Int32, RoleId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                Database.AddInParameter(cmd, "@sectionid", DbType.Int32, sectionid);
                Database.ExecuteNonQuery(cmd);
            }
        }

        IList<CustomRole> IMemberDataAccess.GetCustomRoleRequisitiondetail(int sectionid)
        {

            const string SP = "dbo.GetRolesofRequisitionNew";
            IList<CustomRole> role = new List<CustomRole>();
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@sectionid", DbType.Int32, sectionid);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    //Member mem = new Member();
                    while (reader.Read())
                    {
                        role.Add(new CustomRole { Id = reader.GetInt32(0), Name = reader.GetString(1) });
                    }

                    return role;
                }

                return null;
            }
        }
        //************************END*********************************************

        //*******************code added by pravin khot on 27/April/2016**************
        Member IMemberDataAccess.UpdateMemberthroughvendor(Member member)
        {
            const string SP = "dbo.Member_UpdateMemberthroughvendor";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, member.Id);
                Database.AddInParameter(cmd, "@MemberCode", DbType.AnsiString, StringHelper.Convert(member.MemberCode));
                Database.AddInParameter(cmd, "@FirstName", DbType.AnsiString, StringHelper.Convert(member.FirstName));
                Database.AddInParameter(cmd, "@MiddleName", DbType.AnsiString, StringHelper.Convert(member.MiddleName));
                Database.AddInParameter(cmd, "@LastName", DbType.AnsiString, StringHelper.Convert(member.LastName));
                Database.AddInParameter(cmd, "@NickName", DbType.AnsiString, StringHelper.Convert(member.NickName));
                Database.AddInParameter(cmd, "@DateOfBirth", DbType.DateTime, NullConverter.Convert(member.DateOfBirth));
                Database.AddInParameter(cmd, "@PermanentAddressLine1", DbType.AnsiString, StringHelper.Convert(member.PermanentAddressLine1));
                Database.AddInParameter(cmd, "@PermanentAddressLine2", DbType.AnsiString, StringHelper.Convert(member.PermanentAddressLine2));
                Database.AddInParameter(cmd, "@PermanentCity", DbType.AnsiString, StringHelper.Convert(member.PermanentCity));
                Database.AddInParameter(cmd, "@PermanentStateId", DbType.Int32, member.PermanentStateId);
                Database.AddInParameter(cmd, "@PermanentZip", DbType.AnsiString, StringHelper.Convert(member.PermanentZip));
                Database.AddInParameter(cmd, "@PermanentCountryId", DbType.Int32, member.PermanentCountryId);
                Database.AddInParameter(cmd, "@PermanentPhone", DbType.AnsiString, member.PermanentPhone);
                Database.AddInParameter(cmd, "@PermanentPhoneExt", DbType.AnsiString, member.PermanentPhoneExt);
                Database.AddInParameter(cmd, "@PermanentMobile", DbType.AnsiString, member.PermanentMobile);
                Database.AddInParameter(cmd, "@PrimaryEmail", DbType.AnsiString, StringHelper.Convert(member.PrimaryEmail));
                Database.AddInParameter(cmd, "@AlternateEmail", DbType.AnsiString, StringHelper.Convert(member.AlternateEmail));
                Database.AddInParameter(cmd, "@PrimaryPhone", DbType.AnsiString, StringHelper.Convert(member.PrimaryPhone));
                Database.AddInParameter(cmd, "@PrimaryPhoneExtension", DbType.AnsiString, StringHelper.Convert(member.PrimaryPhoneExtension));
                Database.AddInParameter(cmd, "@CellPhone", DbType.AnsiString, StringHelper.Convert(member.CellPhone));
                Database.AddInParameter(cmd, "@AutomatedEmailStatus", DbType.Boolean, member.AutomatedEmailStatus);
                Database.AddInParameter(cmd, "@ResumeSource", DbType.Int32, member.ResumeSource);
                Database.AddInParameter(cmd, "@ResumeSharing", DbType.Int32, member.ResumeSharing);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, member.Status);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, member.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, member.CreatorId);
                Database.AddInParameter(cmd, "@MemberType", DbType.Int32, member.MemberType);//12373
                Database.AddInParameter(cmd, "@UserId", DbType.Guid, member.UserId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        member = CreateEntityBuilder<Member>().BuildEntity(reader);
                    }
                    else
                    {
                        member = null;
                    }
                }

                if (member == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member already exists. Please specify another member.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member.");
                            }
                    }
                }

                return member;
            }
        }

        string IMemberDataAccess.CandidateAvailableInRequisition_Name(int MemberId)
        {
            const string SP = "dbo.MemberAvailableInRequisition_Name";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.String, MemberId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                    if (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty  : reader.GetString(0);
                    }
                    else
                    {
                        return null ;
                    }
            }
        }

        string IMemberDataAccess.CandidateAvailableInRequisition_Name(int MemberId, int CurrentJobPostingId)
        {
            const string SP = "dbo.MemberAvailableInUndoRequisition_Name";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.String, MemberId);
                Database.AddInParameter(cmd, "@CurrentJobPostingId", DbType.String, CurrentJobPostingId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                    if (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                    else
                    {
                        return null;
                    }
            }
        }
        //*************************END******************************************

        //***********************Code added by pravin khot on 16/May/2016*************
        ArrayList IMemberDataAccess.GetAllNameByTeamMemberId(string roleName, int memberid)
        {
            ArrayList allMemberNameList = new ArrayList();
            if (memberid < 1)
            {
                throw new ArgumentNullException("memberid");
            }
            if (string.IsNullOrEmpty(roleName))
            {
                throw new ArgumentException("RoleName");
            }

            const string SP = "dbo.Member_GetAllNameByTeamMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RoleName", DbType.AnsiString, roleName);
                Database.AddInParameter(cmd, "@memberid", DbType.Int32, memberid);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        allMemberNameList.Add(new Employee() { Id = reader.GetInt32(0), FirstName = reader.GetString(1) });
                    }
                }
            }

            return allMemberNameList;
        }
        //**************************END*****************************
        #endregion
    }
}