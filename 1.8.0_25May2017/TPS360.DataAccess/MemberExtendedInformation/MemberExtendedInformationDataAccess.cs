﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberExtendedInformationDataAccess.cs
    Description: This page is used as data access layer for member extended information.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Apr-20-2009         Shivanand           Defect #10169; In methods Add() & Update(), 3 In parameters added viz. ExpectedYearlyMaxRate,
                                                                          ExpectedMonthlyMaxRate & ExpectedHourlyMaxRate.
   0.2             Sept-14-2009       Nagarathna V.B       Enhancement 11473;Mail set up for indiviual user. 
 * 0.3             24/May/2016        pravin khot          added- UpdateMemberManagerIsPrimary
    ------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Text.RegularExpressions;

namespace TPS360.DataAccess
{
    internal sealed class MemberExtendedInformationDataAccess : BaseDataAccess, IMemberExtendedInformationDataAccess
    {
        #region Constructors

        public MemberExtendedInformationDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberExtendedInformation> CreateEntityBuilder<MemberExtendedInformation>()
        {
            return (new MemberExtendedInformationBuilder()) as IEntityBuilder<MemberExtendedInformation>;
        }

        #endregion

        #region  Methods

        MemberExtendedInformation IMemberExtendedInformationDataAccess.Add(MemberExtendedInformation memberExtendedInformation)
        {
            const string SP = "dbo.MemberExtendedInformation_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Relocation", DbType.Boolean, memberExtendedInformation.Relocation);
                Database.AddInParameter(cmd, "@CurrentPosition", DbType.AnsiString, StringHelper.Convert(memberExtendedInformation.CurrentPosition));
                Database.AddInParameter(cmd, "@AdditionalJobTitle", DbType.AnsiString, StringHelper.Convert(memberExtendedInformation.AdditionalJobTitle));
                Database.AddInParameter(cmd, "@LastEmployer", DbType.AnsiString, StringHelper.Convert(memberExtendedInformation.LastEmployer));
                Database.AddInParameter(cmd, "@LastEmployerTypeLookupId", DbType.Int32, memberExtendedInformation.LastEmployerTypeLookupId);
                Database.AddInParameter(cmd, "@HighestEducationLookupId", DbType.Int32, memberExtendedInformation.HighestEducationLookupId);
                double TotalExp;
                if (double.TryParse(memberExtendedInformation.TotalExperienceYears, out TotalExp))
                {
                    //Database.AddInParameter(cmd, "@TotalExperienceYears", DbType.AnsiString, TotalExp.ToString());
                    Database.AddInParameter(cmd, "@TotalExperienceYears", DbType.AnsiString, memberExtendedInformation.TotalExperienceYears);
                }
                else 
                {

                    Database.AddInParameter(cmd, "@TotalExperienceYears", DbType.AnsiString, "");
                }
                Database.AddInParameter(cmd, "@Availability", DbType.Int32, memberExtendedInformation.Availability);
                
                if (memberExtendedInformation.AvailableDate > DateTime.MinValue)
                {
                    Database.AddInParameter(cmd, "@AvailableDate", DbType.DateTime, NullConverter.Convert(memberExtendedInformation.AvailableDate));
                } 
                Database.AddInParameter(cmd, "@CurrentYearlyRate", DbType.Decimal, memberExtendedInformation.CurrentYearlyRate);
                Database.AddInParameter(cmd, "@CurrentYearlyCurrencyLookupId", DbType.Int32, memberExtendedInformation.CurrentYearlyCurrencyLookupId);
                Database.AddInParameter(cmd, "@CurrentMonthlyRate", DbType.Decimal, memberExtendedInformation.CurrentMonthlyRate);
                Database.AddInParameter(cmd, "@CurrentMonthlyCurrencyLookupId", DbType.Int32, memberExtendedInformation.CurrentMonthlyCurrencyLookupId);
                Database.AddInParameter(cmd, "@CurrentHourlyRate", DbType.Decimal, memberExtendedInformation.CurrentHourlyRate);
                Database.AddInParameter(cmd, "@CurrentHourlyCurrencyLookupId", DbType.Int32, memberExtendedInformation.CurrentHourlyCurrencyLookupId);
                Database.AddInParameter(cmd, "@ExpectedYearlyRate", DbType.Decimal, memberExtendedInformation.ExpectedYearlyRate);
                Database.AddInParameter(cmd, "@ExpectedYearlyCurrencyLookupId", DbType.Int32, memberExtendedInformation.ExpectedYearlyCurrencyLookupId);
                Database.AddInParameter(cmd, "@ExpectedMonthlyRate", DbType.Decimal, memberExtendedInformation.ExpectedMonthlyRate);
                Database.AddInParameter(cmd, "@ExpectedMonthlyCurrencyLookupId", DbType.Int32, memberExtendedInformation.ExpectedMonthlyCurrencyLookupId);
                Database.AddInParameter(cmd, "@ExpectedHourlyRate", DbType.Decimal, memberExtendedInformation.ExpectedHourlyRate);
                Database.AddInParameter(cmd, "@ExpectedHourlyCurrencyLookupId", DbType.Int32, memberExtendedInformation.ExpectedHourlyCurrencyLookupId);
                Database.AddInParameter(cmd, "@WillingToTravel", DbType.Boolean, memberExtendedInformation.WillingToTravel);
                Database.AddInParameter(cmd, "@PassportStatus", DbType.Boolean, memberExtendedInformation.PassportStatus);
                Database.AddInParameter(cmd, "@SalaryTypeLookupId", DbType.Int32, memberExtendedInformation.SalaryTypeLookupId);
                Database.AddInParameter(cmd, "@JobTypeLookupId", DbType.Int32, memberExtendedInformation.JobTypeLookupId);
                Database.AddInParameter(cmd, "@SecurityClearance", DbType.Boolean, memberExtendedInformation.SecurityClearance);
                Database.AddInParameter(cmd, "@WorkAuthorizationLookupId", DbType.Int32, memberExtendedInformation.WorkAuthorizationLookupId);
                Database.AddInParameter(cmd, "@InternalRatingLookupId", DbType.Int32, memberExtendedInformation.InternalRatingLookupId);
                Database.AddInParameter(cmd, "@PreferredLocation", DbType.AnsiString, StringHelper.Convert(memberExtendedInformation.PreferredLocation));
                Database.AddInParameter(cmd, "@IndustryExperience", DbType.AnsiString, StringHelper.Convert(memberExtendedInformation.IndustryExperience));
                Database.AddInParameter(cmd, "@IndustryTypeLookupId", DbType.Int32, memberExtendedInformation.IndustryTypeLookupId);
                Database.AddInParameter(cmd, "@Remarks", DbType.AnsiString, StringHelper.Convert(memberExtendedInformation.Remarks));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberExtendedInformation.IsRemoved);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberExtendedInformation.MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberExtendedInformation.CreatorId);
                Database.AddInParameter(cmd, "@Category", DbType.AnsiString, memberExtendedInformation.Category);
              // 0.1 starts
                Database.AddInParameter(cmd, "@ExpectedYearlyMaxRate", DbType.Decimal, memberExtendedInformation.ExpectedYearlyMaxRate);
                Database.AddInParameter(cmd, "@ExpectedMonthlyMaxRate", DbType.Decimal, memberExtendedInformation.ExpectedMonthlyMaxRate);
                Database.AddInParameter(cmd, "@ExpectedHourlyMaxRate", DbType.Decimal, memberExtendedInformation.ExpectedHourlyMaxRate);
                Database.AddInParameter(cmd, "@WorkScheduleLookUpID", DbType.Int32, memberExtendedInformation.WorkScheduleLookupId);
                Database.AddInParameter(cmd, "@CurrentSalaryPayCycle", DbType.Int32, memberExtendedInformation.CurrentSalaryPayCycle );
                Database.AddInParameter(cmd, "@ExpectedSalaryPayCycle", DbType.Int32, memberExtendedInformation.ExpectedSalaryPayCycle );

                Database.AddInParameter(cmd, "@PassportNumber", DbType.AnsiString , memberExtendedInformation.PassportNumber );
                Database.AddInParameter(cmd, "@FunctionAreaLookupId", DbType.Int32, memberExtendedInformation.FunctionAreaLookupId );
                Database.AddInParameter(cmd, "@WillingToReLocate", DbType.Boolean , memberExtendedInformation.WillingToReLocate );
                Database.AddInParameter(cmd, "@OldDbRefId", DbType.AnsiString , memberExtendedInformation.OldDbRefId );
                Database.AddInParameter(cmd, "@PANNumber", DbType.AnsiString, memberExtendedInformation.PANNumber );
                Database.AddInParameter(cmd, "@Website", DbType.AnsiString, memberExtendedInformation.Website);
                Database.AddInParameter(cmd, "@LinkedinProfile", DbType.AnsiString, memberExtendedInformation.LinkedinProfile);
                Database.AddInParameter(cmd, "@SourceLookupId", DbType.Int32, memberExtendedInformation.SourceLookupId);
                Database.AddInParameter(cmd, "@SourceDescription", DbType.AnsiString, memberExtendedInformation.SourceDescription);
                Database.AddInParameter(cmd, "@IdCardLookUpId", DbType.Int32, memberExtendedInformation.IdCardLookUpId);
                Database.AddInParameter(cmd, "@IdCardDetail", DbType.String, memberExtendedInformation.IdCardDetail);
              // 0.1 ends

                //Database.AddInParameter(cmd, "@MailSetting", DbType.Binary, memberExtendedInformation.MailSetting);//0.2

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberExtendedInformation = CreateEntityBuilder<MemberExtendedInformation>().BuildEntity(reader);
                    }
                    else
                    {
                        memberExtendedInformation = null;
                    }
                }

                if (memberExtendedInformation == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member extended information already exists. Please specify another member extended information.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member extended information.");
                            }
                    }
                }

                return memberExtendedInformation;
            }
        }

        MemberExtendedInformation IMemberExtendedInformationDataAccess.Update(MemberExtendedInformation memberExtendedInformation)
        {
            const string SP = "dbo.MemberExtendedInformation_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberExtendedInformation.Id);
                Database.AddInParameter(cmd, "@Relocation", DbType.Boolean, memberExtendedInformation.Relocation);
                Database.AddInParameter(cmd, "@CurrentPosition", DbType.AnsiString, StringHelper.Convert(memberExtendedInformation.CurrentPosition));
                Database.AddInParameter(cmd, "@AdditionalJobTitle", DbType.AnsiString, StringHelper.Convert(memberExtendedInformation.AdditionalJobTitle));
                Database.AddInParameter(cmd, "@LastEmployer", DbType.AnsiString, StringHelper.Convert(memberExtendedInformation.LastEmployer));
                Database.AddInParameter(cmd, "@LastEmployerTypeLookupId", DbType.Int32, memberExtendedInformation.LastEmployerTypeLookupId);
                Database.AddInParameter(cmd, "@HighestEducationLookupId", DbType.Int32, memberExtendedInformation.HighestEducationLookupId);
                double TotalExp;
                if (double.TryParse(memberExtendedInformation.TotalExperienceYears, out TotalExp))
                {
                    //Database.AddInParameter(cmd, "@TotalExperienceYears", DbType.AnsiString, TotalExp.ToString());
                    Database.AddInParameter(cmd, "@TotalExperienceYears", DbType.AnsiString, memberExtendedInformation.TotalExperienceYears);
                }
                else
                {

                    Database.AddInParameter(cmd, "@TotalExperienceYears", DbType.AnsiString, "");
                }
                Database.AddInParameter(cmd, "@Availability", DbType.Int32, memberExtendedInformation.Availability);
                if (memberExtendedInformation.AvailableDate > DateTime.MinValue)
                {
                    Database.AddInParameter(cmd, "@AvailableDate", DbType.DateTime, NullConverter.Convert(memberExtendedInformation.AvailableDate));
                }
                Database.AddInParameter(cmd, "@CurrentYearlyRate", DbType.Decimal, memberExtendedInformation.CurrentYearlyRate);
                Database.AddInParameter(cmd, "@CurrentYearlyCurrencyLookupId", DbType.Int32, memberExtendedInformation.CurrentYearlyCurrencyLookupId);
                Database.AddInParameter(cmd, "@CurrentMonthlyRate", DbType.Decimal, memberExtendedInformation.CurrentMonthlyRate);
                Database.AddInParameter(cmd, "@CurrentMonthlyCurrencyLookupId", DbType.Int32, memberExtendedInformation.CurrentMonthlyCurrencyLookupId);
                Database.AddInParameter(cmd, "@CurrentHourlyRate", DbType.Decimal, memberExtendedInformation.CurrentHourlyRate);
                Database.AddInParameter(cmd, "@CurrentHourlyCurrencyLookupId", DbType.Int32, memberExtendedInformation.CurrentHourlyCurrencyLookupId);
                Database.AddInParameter(cmd, "@CurrentSalaryNote", DbType.String, memberExtendedInformation.CurrentSalaryNote);
                Database.AddInParameter(cmd, "@ExpectedYearlyRate", DbType.Decimal, memberExtendedInformation.ExpectedYearlyRate);
                Database.AddInParameter(cmd, "@ExpectedYearlyCurrencyLookupId", DbType.Int32, memberExtendedInformation.ExpectedYearlyCurrencyLookupId);
                Database.AddInParameter(cmd, "@ExpectedMonthlyRate", DbType.Decimal, memberExtendedInformation.ExpectedMonthlyRate);
                Database.AddInParameter(cmd, "@ExpectedMonthlyCurrencyLookupId", DbType.Int32, memberExtendedInformation.ExpectedMonthlyCurrencyLookupId);
                Database.AddInParameter(cmd, "@ExpectedHourlyRate", DbType.Decimal, memberExtendedInformation.ExpectedHourlyRate);
                Database.AddInParameter(cmd, "@ExpectedHourlyCurrencyLookupId", DbType.Int32, memberExtendedInformation.ExpectedHourlyCurrencyLookupId);
                Database.AddInParameter(cmd, "@ExpectedSalaryNote", DbType.String, memberExtendedInformation.ExpectedSalaryNote);
                Database.AddInParameter(cmd, "@WillingToTravel", DbType.Boolean, memberExtendedInformation.WillingToTravel);
                Database.AddInParameter(cmd, "@PassportStatus", DbType.Boolean, memberExtendedInformation.PassportStatus);
                Database.AddInParameter(cmd, "@SalaryTypeLookupId", DbType.Int32, memberExtendedInformation.SalaryTypeLookupId);
                Database.AddInParameter(cmd, "@JobTypeLookupId", DbType.Int32, memberExtendedInformation.JobTypeLookupId);
                Database.AddInParameter(cmd, "@SecurityClearance", DbType.Boolean, memberExtendedInformation.SecurityClearance);
                Database.AddInParameter(cmd, "@WorkAuthorizationLookupId", DbType.Int32, memberExtendedInformation.WorkAuthorizationLookupId);
                Database.AddInParameter(cmd, "@InternalRatingLookupId", DbType.Int32, memberExtendedInformation.InternalRatingLookupId);
                Database.AddInParameter(cmd, "@PreferredLocation", DbType.AnsiString, StringHelper.Convert(memberExtendedInformation.PreferredLocation));
                Database.AddInParameter(cmd, "@IndustryExperience", DbType.AnsiString, StringHelper.Convert(memberExtendedInformation.IndustryExperience));
                Database.AddInParameter(cmd, "@IndustryTypeLookupId", DbType.Int32, memberExtendedInformation.IndustryTypeLookupId);
                Database.AddInParameter(cmd, "@Remarks", DbType.AnsiString, StringHelper.Convert(memberExtendedInformation.Remarks));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberExtendedInformation.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberExtendedInformation.UpdatorId);
                Database.AddInParameter(cmd, "@Category", DbType.AnsiString, memberExtendedInformation.Category);
                // 0.1 starts
                Database.AddInParameter(cmd, "@ExpectedYearlyMaxRate", DbType.Decimal, memberExtendedInformation.ExpectedYearlyMaxRate);
                Database.AddInParameter(cmd, "@ExpectedMonthlyMaxRate", DbType.Decimal, memberExtendedInformation.ExpectedMonthlyMaxRate);
                Database.AddInParameter(cmd, "@ExpectedHourlyMaxRate", DbType.Decimal, memberExtendedInformation.ExpectedHourlyMaxRate);
                Database.AddInParameter(cmd, "@WorkScheduleLookUpID", DbType.Int32, memberExtendedInformation.WorkScheduleLookupId);
                // 0.1 ends
                Database.AddInParameter(cmd, "@MailSetting", DbType.Binary, memberExtendedInformation.MailSetting);//0.2
                Database.AddInParameter(cmd, "@CurrentSalaryPayCycle", DbType.Int32, memberExtendedInformation.CurrentSalaryPayCycle);
                Database.AddInParameter(cmd, "@ExpectedSalaryPayCycle", DbType.Int32, memberExtendedInformation.ExpectedSalaryPayCycle);
                Database.AddInParameter(cmd, "@PANNumber", DbType.AnsiString, memberExtendedInformation.PANNumber);
                Database.AddInParameter(cmd, "@Website", DbType.AnsiString, memberExtendedInformation.Website);
                Database.AddInParameter(cmd,"@LinkedinProfile",DbType.AnsiString,memberExtendedInformation.LinkedinProfile);
                Database.AddInParameter(cmd, "@SourceLookupId", DbType.Int32, memberExtendedInformation.SourceLookupId);
                Database.AddInParameter(cmd, "@sourceDescription", DbType.AnsiString, memberExtendedInformation.SourceDescription);
                Database.AddInParameter(cmd, "@IdCardLookUpId", DbType.Int32, memberExtendedInformation.IdCardLookUpId);
                Database.AddInParameter(cmd, "@IdCardDetail", DbType.String, memberExtendedInformation.IdCardDetail);
                Database.AddInParameter(cmd, "@NoticePeriod", DbType.String, memberExtendedInformation.NoticePeriod);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberExtendedInformation = CreateEntityBuilder<MemberExtendedInformation>().BuildEntity(reader);
                    }
                    else
                    {
                        memberExtendedInformation = null;
                    }
                }

                if (memberExtendedInformation == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member extended information already exists. Please specify another member extended information.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member extended information.");
                            }
                    }
                }

                return memberExtendedInformation;
            }
        }

        MemberExtendedInformation IMemberExtendedInformationDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberExtendedInformation_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberExtendedInformation>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberExtendedInformation IMemberExtendedInformationDataAccess.GetByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberExtendedInformation_GetByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberExtendedInformation>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberExtendedInformation> IMemberExtendedInformationDataAccess.GetAll()
        {
            const string SP = "dbo.MemberExtendedInformation_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberExtendedInformation>().BuildEntities(reader);
                }
            }
        }

        bool IMemberExtendedInformationDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberExtendedInformation_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member extended information which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member extended information.");
                        }
                }
            }
        }

        bool IMemberExtendedInformationDataAccess.DeleteByMemberId(int MemberId)
        {
            if (MemberId < 1)
            {
                throw new ArgumentNullException("MemberId");
            }

            const string SP = "dbo.MemberExtendedInformation_DeleteByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member extended information which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member extended information.");
                        }
                }
            }
        }

        void IMemberExtendedInformationDataAccess.UpdateUpdateDateByMemberId(int MemberId)
        {
            const string SP = "dbo.MemberExtendedInformation_UpdateUpdateDateByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId );
                Database.ExecuteNonQuery(cmd);
            }
        }
       
        //**************Added by pravin khot on 24/May/2016***********
        void IMemberExtendedInformationDataAccess.UpdateMemberManagerIsPrimaryTrue(int candidateid, int AssignManagerId)
        {
            const string SP = "dbo.UpdateMemberManagerIsPrimaryTrue";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@candidateid", DbType.Int32, candidateid);
                Database.AddInParameter(cmd, "@AssignManagerId", DbType.Int32, AssignManagerId);
                Database.ExecuteNonQuery(cmd);
            }
        }
        //**************************END***************************
       /* MemberExtendedInformation IMemberExtendedInformationDataAccess.Update(MemberExtendedInformation MailSetting)
        {
            const string SP = "dbo.MailSetting_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, MailSetting.MemberId);
                Database.AddInParameter(cmd, "@MailSetting", DbType.Int32, MailSetting.MailSetting);
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        siteSetting = CreateEntityBuilder<SiteSetting>().BuildEntity(reader);
                    }
                    else
                    {
                        siteSetting = null;
                    }
                }

                //if (siteSetting == null)
                //{
                //    int returnCode = GetReturnCodeFromParameter(cmd);

                //    switch (returnCode)
                //    {
                //        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                //            {
                //                throw new ArgumentException("SiteSetting already exists. Please specify another siteSetting.");
                //            }
                //        default:
                //            {
                //                throw new SystemException("An unexpected error has occurred while updating this siteSetting.");
                //            }
                //    }
                //}

                return siteSetting;
            }
        }*/

       /* MemberExtendedInformation IMemberExtendedInformationDataAccess.GetMailSettingByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MailSetting_GetByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberExtendedInformation>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }*/


  

        #endregion
    }
}