﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberExtendedInformationBuilder.cs
    Description: This page is used for building entities related to member extended information.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Apr-20-2009         Shivanand           Defect #10169; New constatnts declared viz. FLD_EXPECTEDYEARLYMAXRATE,FLD_EXPECTEDMONTHLYMAXRATE
                                                                          FLD_EXPECTEDHOURLYMAXRATE & used to assign values for entiteis in memberExtendedInformation object.
    0.2            Sept-14-2009        Nagarathna V.B       Enhanccment #11473; Indiviual user mail set up.
    ------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;
using System.Data;
using System.Collections.Generic;


using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberExtendedInformationBuilder : IEntityBuilder<MemberExtendedInformation>
    {
        IList<MemberExtendedInformation> IEntityBuilder<MemberExtendedInformation>.BuildEntities(IDataReader reader)
        {
            List<MemberExtendedInformation> memberExtendedInformations = new List<MemberExtendedInformation>();

            while (reader.Read())
            {
                memberExtendedInformations.Add(((IEntityBuilder<MemberExtendedInformation>)this).BuildEntity(reader));
            }

            return (memberExtendedInformations.Count > 0) ? memberExtendedInformations : null;
        }

        MemberExtendedInformation IEntityBuilder<MemberExtendedInformation>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_RELOCATION = 1;
            const int FLD_CURRENTPOSITION = 2;
            const int FLD_ADDITIONALJOBTITLE = 3;
            const int FLD_LASTEMPLOYER = 4;
            const int FLD_LASTEMPLOYERTYPELOOKUPID = 5;
            const int FLD_HIGHESTEDUCATIONLOOKUPID = 6;
            const int FLD_TOTALEXPERIENCEYEARS = 7;
            const int FLD_AVAILABILITY = 8;
            const int FLD_AVAILABLEDATE = 9;
            const int FLD_CURRENTYEARLYRATE = 10;
            const int FLD_CURRENTYEARLYCURRENCYLOOKUPID = 11;
            const int FLD_CURRENTMONTHLYRATE = 12;
            const int FLD_CURRENTMONTHLYCURRENCYLOOKUPID = 13;
            const int FLD_CURRENTHOURLYRATE = 14;
            const int FLD_CURRENTHOURLYCURRENCYLOOKUPID = 15;
            const int FLD_EXPECTEDYEARLYRATE = 16;
            const int FLD_EXPECTEDYEARLYCURRENCYLOOKUPID = 17;
            const int FLD_EXPECTEDMONTHLYRATE = 18;
            const int FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID = 19;
            const int FLD_EXPECTEDHOURLYRATE = 20;
            const int FLD_EXPECTEDHOURLYCURRENCYLOOKUPID = 21;
            const int FLD_WILLINGTOTRAVEL = 22;
            const int FLD_PASSPORTSTATUS = 23;
            const int FLD_SALARYTYPELOOKUPID = 24;
            const int FLD_JOBTYPELOOKUPID = 25;
            const int FLD_SECURITYCLEARANCE = 26;
            const int FLD_WORKAUTHORIZATIONLOOKUPID = 27;
            const int FLD_INTERNALRATINGLOOKUPID = 28;
            const int FLD_PREFERREDLOCATION = 29;
            const int FLD_INDUSTRYEXPERIENCE = 30;
            const int FLD_INDUSTRYTYPELOOKUPID = 31;
            const int FLD_REMARKS = 32;
            const int FLD_ISREMOVED = 33;
            const int FLD_MEMBERID = 34;
            const int FLD_CREATORID = 35;
            const int FLD_UPDATORID = 36;
            const int FLD_CREATEDATE = 37;
            const int FLD_UPDATEDATE = 38;
            const int FLD_CATEGORY = 39;
            // 0.1 starts
            const int FLD_EXPECTEDYEARLYMAXRATE = 40; 
            const int FLD_EXPECTEDMONTHLYMAXRATE = 41;  
            const int FLD_EXPECTEDHOURLYMAXRATE = 42;  
            // 0.1 ends
            const int FLD_WORKSCHEDULELOOKUPID = 43;
            const int FLD_CURRENTSALARYPAYCYCLE = 44;
            const int FLD_EXPECTEDSALARYPAYCYCLE = 45;

            const int FLD_MAILSETTING = 46;  //0.2
            const int FLD_PANNUMBER=47;
            const int FLD_WEBSITE = 48;
            const int FLD_LINKEDINPROFILE = 49;
            const int FLD_SOURCELOOKUPID = 50;
            const int FLD_SOURCEDESCRIPTION = 51;
            const int FLD_IDCARDLOOKUPID = 52;
            const int FLD_IDCARDDETAIL = 53;
            const int FLD_CURRENTSALARYNOTE = 54;
            const int FLD_EXPECTEDSALARYNOTE = 55;
            const int FLD_NOTICEPERIOD = 56;


            MemberExtendedInformation memberExtendedInformation = new MemberExtendedInformation();

            memberExtendedInformation.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberExtendedInformation.Relocation = reader.IsDBNull(FLD_RELOCATION) ? false : reader.GetBoolean(FLD_RELOCATION);
            memberExtendedInformation.CurrentPosition = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
            memberExtendedInformation.AdditionalJobTitle = reader.IsDBNull(FLD_ADDITIONALJOBTITLE) ? string.Empty : reader.GetString(FLD_ADDITIONALJOBTITLE);
            memberExtendedInformation.LastEmployer = reader.IsDBNull(FLD_LASTEMPLOYER) ? string.Empty : reader.GetString(FLD_LASTEMPLOYER);
            memberExtendedInformation.LastEmployerTypeLookupId = reader.IsDBNull(FLD_LASTEMPLOYERTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_LASTEMPLOYERTYPELOOKUPID);
            memberExtendedInformation.HighestEducationLookupId = reader.IsDBNull(FLD_HIGHESTEDUCATIONLOOKUPID) ? 0 : reader.GetInt32(FLD_HIGHESTEDUCATIONLOOKUPID);
            memberExtendedInformation.TotalExperienceYears = reader.IsDBNull(FLD_TOTALEXPERIENCEYEARS) ? string.Empty : reader.GetString(FLD_TOTALEXPERIENCEYEARS);
            memberExtendedInformation.Availability = reader.IsDBNull(FLD_AVAILABILITY) ? 0 : reader.GetInt32(FLD_AVAILABILITY);
            memberExtendedInformation.AvailableDate = reader.IsDBNull(FLD_AVAILABLEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_AVAILABLEDATE);
            memberExtendedInformation.CurrentYearlyRate = reader.IsDBNull(FLD_CURRENTYEARLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTYEARLYRATE);
            memberExtendedInformation.CurrentYearlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTYEARLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTYEARLYCURRENCYLOOKUPID);
            memberExtendedInformation.CurrentMonthlyRate = reader.IsDBNull(FLD_CURRENTMONTHLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTMONTHLYRATE);
            memberExtendedInformation.CurrentMonthlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTMONTHLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTMONTHLYCURRENCYLOOKUPID);
            memberExtendedInformation.CurrentHourlyRate = reader.IsDBNull(FLD_CURRENTHOURLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTHOURLYRATE);
            memberExtendedInformation.CurrentHourlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTHOURLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTHOURLYCURRENCYLOOKUPID);
            memberExtendedInformation.CurrentSalaryNote = reader.IsDBNull(FLD_CURRENTSALARYNOTE) ? string.Empty : reader.GetString(FLD_CURRENTSALARYNOTE);
            memberExtendedInformation.ExpectedYearlyRate = reader.IsDBNull(FLD_EXPECTEDYEARLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDYEARLYRATE);
            memberExtendedInformation.ExpectedYearlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDYEARLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDYEARLYCURRENCYLOOKUPID);
            memberExtendedInformation.ExpectedMonthlyRate = reader.IsDBNull(FLD_EXPECTEDMONTHLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDMONTHLYRATE);
            memberExtendedInformation.ExpectedMonthlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID);
            memberExtendedInformation.ExpectedHourlyRate = reader.IsDBNull(FLD_EXPECTEDHOURLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDHOURLYRATE);
            memberExtendedInformation.ExpectedHourlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDHOURLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDHOURLYCURRENCYLOOKUPID);
            memberExtendedInformation.ExpectedSalaryNote = reader.IsDBNull(FLD_EXPECTEDSALARYNOTE) ? string.Empty : reader.GetString(FLD_EXPECTEDSALARYNOTE);
            memberExtendedInformation.WillingToTravel = reader.IsDBNull(FLD_WILLINGTOTRAVEL) ? false : reader.GetBoolean(FLD_WILLINGTOTRAVEL);
            memberExtendedInformation.PassportStatus = reader.IsDBNull(FLD_PASSPORTSTATUS) ? false : reader.GetBoolean(FLD_PASSPORTSTATUS);
            memberExtendedInformation.SalaryTypeLookupId = reader.IsDBNull(FLD_SALARYTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_SALARYTYPELOOKUPID);
            memberExtendedInformation.JobTypeLookupId = reader.IsDBNull(FLD_JOBTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_JOBTYPELOOKUPID);
            memberExtendedInformation.SecurityClearance = reader.IsDBNull(FLD_SECURITYCLEARANCE) ? false : reader.GetBoolean(FLD_SECURITYCLEARANCE);
            memberExtendedInformation.WorkAuthorizationLookupId = reader.IsDBNull(FLD_WORKAUTHORIZATIONLOOKUPID) ? 0 : reader.GetInt32(FLD_WORKAUTHORIZATIONLOOKUPID);
            memberExtendedInformation.InternalRatingLookupId = reader.IsDBNull(FLD_INTERNALRATINGLOOKUPID) ? 0 : reader.GetInt32(FLD_INTERNALRATINGLOOKUPID);
            memberExtendedInformation.PreferredLocation = reader.IsDBNull(FLD_PREFERREDLOCATION) ? string.Empty : reader.GetString(FLD_PREFERREDLOCATION);
            memberExtendedInformation.IndustryExperience = reader.IsDBNull(FLD_INDUSTRYEXPERIENCE) ? string.Empty : reader.GetString(FLD_INDUSTRYEXPERIENCE);
            memberExtendedInformation.IndustryTypeLookupId = reader.IsDBNull(FLD_INDUSTRYTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_INDUSTRYTYPELOOKUPID);
            memberExtendedInformation.Remarks = reader.IsDBNull(FLD_REMARKS) ? string.Empty : reader.GetString(FLD_REMARKS);
            memberExtendedInformation.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberExtendedInformation.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberExtendedInformation.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberExtendedInformation.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberExtendedInformation.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberExtendedInformation.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            memberExtendedInformation.Category = reader.IsDBNull(FLD_CATEGORY) ? string.Empty : reader.GetString(FLD_CATEGORY);
        // 0.1 starts

            //if (reader.FieldCount > FLD_EXPECTEDYEARLYMAXRATE && reader.FieldCount <= FLD_EXPECTEDHOURLYMAXRATE)
            //{
                memberExtendedInformation.ExpectedYearlyMaxRate = reader.IsDBNull(FLD_EXPECTEDYEARLYMAXRATE) ? Convert.ToDecimal(0.00) : reader.GetDecimal(FLD_EXPECTEDYEARLYMAXRATE);
                memberExtendedInformation.ExpectedMonthlyMaxRate = reader.IsDBNull(FLD_EXPECTEDMONTHLYMAXRATE) ? Convert.ToDecimal(0.00) : reader.GetDecimal(FLD_EXPECTEDMONTHLYMAXRATE);
                memberExtendedInformation.ExpectedHourlyMaxRate = reader.IsDBNull(FLD_EXPECTEDHOURLYMAXRATE) ? Convert.ToDecimal(0.00) : reader.GetDecimal(FLD_EXPECTEDHOURLYMAXRATE);
            //}
         //  0.1 ends
            //0.2 starts
            //memberExtendedInformation.MailSetting =  (byte[])(reader.GetValue(FLD_MAILSETTING));//0.2
                memberExtendedInformation.WorkScheduleLookupId = reader.IsDBNull(FLD_WORKSCHEDULELOOKUPID) ? 0 : reader.GetInt32(FLD_WORKSCHEDULELOOKUPID);
                memberExtendedInformation.CurrentSalaryPayCycle = reader.IsDBNull(FLD_CURRENTSALARYPAYCYCLE) ? 0 : reader.GetInt32(FLD_CURRENTSALARYPAYCYCLE);
                memberExtendedInformation.ExpectedSalaryPayCycle = reader.IsDBNull(FLD_EXPECTEDSALARYPAYCYCLE) ? 0 : reader.GetInt32(FLD_EXPECTEDSALARYPAYCYCLE);
                if (reader.FieldCount > FLD_MAILSETTING)
                {
                    if (!reader.IsDBNull(FLD_MAILSETTING))
                    {
                        memberExtendedInformation.MailSetting = (byte[])(reader.GetValue(FLD_MAILSETTING));
                    }
                }
                memberExtendedInformation.PANNumber = reader.IsDBNull(FLD_PANNUMBER) ? string.Empty : reader.GetString(FLD_PANNUMBER);
            //0.2 ends
            memberExtendedInformation.PANNumber = reader.IsDBNull(FLD_PANNUMBER) ? string.Empty : reader.GetValue(FLD_PANNUMBER).ToString();
            memberExtendedInformation.Website = reader.IsDBNull(FLD_WEBSITE) ? string.Empty : reader.GetString (FLD_WEBSITE);
            memberExtendedInformation.LinkedinProfile = reader.IsDBNull(FLD_LINKEDINPROFILE) ? string.Empty : reader.GetString(FLD_LINKEDINPROFILE);
            memberExtendedInformation.SourceLookupId=reader.IsDBNull(FLD_SOURCELOOKUPID)?0:reader.GetInt32(FLD_SOURCELOOKUPID);
            memberExtendedInformation.SourceDescription = reader.IsDBNull(FLD_SOURCEDESCRIPTION) ? string.Empty : reader.GetString(FLD_SOURCEDESCRIPTION);
            memberExtendedInformation.IdCardLookUpId = reader.IsDBNull(FLD_IDCARDLOOKUPID) ? 0 : reader.GetInt32(FLD_IDCARDLOOKUPID);
            memberExtendedInformation.IdCardDetail = reader.IsDBNull(FLD_IDCARDDETAIL) ? string.Empty : reader.GetString(FLD_IDCARDDETAIL);
            memberExtendedInformation.NoticePeriod = reader.IsDBNull(FLD_NOTICEPERIOD) ? string.Empty : reader.GetString(FLD_NOTICEPERIOD);
            return memberExtendedInformation;
        }
    }
}