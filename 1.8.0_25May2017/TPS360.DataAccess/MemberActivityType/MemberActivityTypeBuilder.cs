﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberActivityTypeBuilder : IEntityBuilder<MemberActivityType>
    {
        IList<MemberActivityType> IEntityBuilder<MemberActivityType>.BuildEntities(IDataReader reader)
        {
            List<MemberActivityType> memberActivityTypes = new List<MemberActivityType>();

            while (reader.Read())
            {
                memberActivityTypes.Add(((IEntityBuilder<MemberActivityType>)this).BuildEntity(reader));
            }

            return (memberActivityTypes.Count > 0) ? memberActivityTypes : null;
        }

        MemberActivityType IEntityBuilder<MemberActivityType>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_NAME = 1;
            const int FLD_DESCRIPTION = 2;
            const int FLD_ACTIVITYTYPE = 3;
            const int FLD_ISREMOVED = 4;
            const int FLD_CREATORID = 5;
            const int FLD_UPDATORID = 6;
            const int FLD_CREATEDATE = 7;
            const int FLD_UPDATEDATE = 8;

            MemberActivityType memberActivityType = new MemberActivityType();

            memberActivityType.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberActivityType.Name = reader.IsDBNull(FLD_NAME) ? string.Empty : reader.GetString(FLD_NAME);
            memberActivityType.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
            memberActivityType.ActivityType = reader.IsDBNull(FLD_ACTIVITYTYPE) ? 0 : reader.GetInt32(FLD_ACTIVITYTYPE);
            memberActivityType.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberActivityType.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberActivityType.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberActivityType.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberActivityType.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberActivityType;
        }
    }
}