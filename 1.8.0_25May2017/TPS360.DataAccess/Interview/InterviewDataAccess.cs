﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberInterviewSchedule.ascx.cs
    Description: This is the user control page used for member interview functionalities
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Jan-20-2009           Yogeesh Bhat        Defect ID: 9754; Added new parameters "MemberJobCartId" and "TypeLookUpId" in IInterviewDataAccess.Update()
    0.2            20/Oct/2015           Prasanth Kumar G    Introduced QustionBankTypeLookup
 *  0.3             10/Dec/2015           Pravin khot        Introduced by SuggestedInterviewer_Id(string InterviewrEmail)
 *  0.4              4/May/2016           pravin khot        Introduced by CancelById,UpdateIcsCodeById
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;

namespace TPS360.DataAccess
{
    internal sealed class InterviewDataAccess : BaseDataAccess, IInterviewDataAccess
    {
        #region Constructors

        public InterviewDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<Interview> CreateEntityBuilder<Interview>()
        {
            return (new InterviewBuilder()) as IEntityBuilder<Interview>;
        }

        #endregion

        #region  Methods

        Interview IInterviewDataAccess.Add(Interview interview)
        {
            const string SP = "dbo.Interview_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Location", DbType.AnsiString, interview.Location);
                Database.AddInParameter(cmd, "@Remark", DbType.AnsiString, interview.Remark);
                Database.AddInParameter(cmd, "@Feedback", DbType.AnsiString, interview.Feedback);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, interview.Status);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, interview.MemberId);
                Database.AddInParameter(cmd, "@TypeLookupId", DbType.Int32, interview.TypeLookupId);
                Database.AddInParameter(cmd, "@InterviewLevel", DbType.Int32, interview.InterviewLevel);
                Database.AddInParameter(cmd, "@ActivityId", DbType.Int32, interview.ActivityId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, interview.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, interview.CreatorId);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, interview.Title);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, interview.JobPostingId);
                Database.AddInParameter(cmd, "@ClientId", DbType.Int32, interview.ClientId);
                Database.AddInParameter(cmd ,"@StartDateTime", DbType .DateTime ,interview .StartDateTime );
                Database.AddInParameter(cmd ,"@Duration" ,DbType .Int32 ,interview .Duration );
                Database.AddInParameter(cmd, "@AllDayEvent", DbType.Boolean, interview.AllDayEvent);
                Database.AddInParameter(cmd, "@EnableReminder", DbType.Boolean, interview.EnableReminder);
                Database.AddInParameter(cmd, "@ReminderInterval", DbType.Int32, interview.ReminderInterval);
                Database.AddInParameter(cmd, "@OtherInterviewers", DbType.AnsiString, interview.OtherInterviewers);
                //Database.AddInParameter(cmd, "@InterviewDocumentId", DbType.Int32, interview.InterviewDocumentId);
                //Line introduced by Prasanth on 20/Oct/2015
                Database.AddInParameter(cmd, "@QuestionBankTypeLookupId", DbType.Int32, interview.QuestionBankTypeLookupId);

                //**************Code added by pravin khot on 4/May/2016**************************
                Database.AddInParameter(cmd, "@IcsFileUIDCode", DbType.AnsiString, interview.IcsFileUIDCode);
                Database.AddInParameter(cmd, "@IsCancel", DbType.Int32, interview.IsCancel);
                Database.AddInParameter(cmd, "@TimezoneId", DbType.Int32, interview.TimezoneId);
                //********************************END*****************************************
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        interview = CreateEntityBuilder<Interview>().BuildEntity(reader);
                    }
                    else
                    {
                        interview = null;
                    }
                }

                if (interview == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Interview already exists. Please specify another interview.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this interview.");
                            }
                    }
                }

                return interview;
            }
        }

        Interview IInterviewDataAccess.Update(Interview interview)
        {
            const string SP = "dbo.Interview_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, interview.Id);
                Database.AddInParameter(cmd, "@Location", DbType.AnsiString, interview.Location);
                Database.AddInParameter(cmd, "@Remark", DbType.AnsiString, interview.Remark);
                Database.AddInParameter(cmd, "@Feedback", DbType.AnsiString, interview.Feedback);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, interview.Status);
                Database.AddInParameter(cmd, "@InterviewLevel", DbType.Int32, interview.InterviewLevel);
                Database.AddInParameter(cmd, "@ActivityId", DbType.Int32, interview.ActivityId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, interview.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, interview.UpdatorId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, interview.MemberId);  //0.1
                Database.AddInParameter(cmd, "@TypeLookUpId", DbType.Int32, interview.TypeLookupId);        //0.1
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, interview.Title);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, interview.JobPostingId);
                Database.AddInParameter(cmd, "@ClientId", DbType.Int32, interview.ClientId);
                Database.AddInParameter(cmd, "@StartDateTime", DbType.DateTime, interview.StartDateTime);
                Database.AddInParameter(cmd, "@Duration", DbType.Int32, interview.Duration);
                Database.AddInParameter(cmd, "@AllDayEvent", DbType.Boolean, interview.AllDayEvent);
                Database.AddInParameter(cmd, "@EnableReminder", DbType.Boolean, interview.EnableReminder);
                Database.AddInParameter(cmd, "@ReminderInterval", DbType.Int32, interview.ReminderInterval);
                Database.AddInParameter(cmd, "@OtherInterviewers", DbType.AnsiString, interview.OtherInterviewers);
                //Database.AddInParameter(cmd, "@InterviewDocumentId", DbType.Int32, interview.InterviewDocumentId);
                //Code introduced by Prasanth on 20/Oct/2015
                Database.AddInParameter(cmd, "@QuestionBankTypeLookupId", DbType.Int32, interview.QuestionBankTypeLookupId);

                //**************Code added by pravin khot on 4/May/2016**************************
                Database.AddInParameter(cmd, "@IcsFileUIDCode", DbType.AnsiString, interview.IcsFileUIDCode);
                Database.AddInParameter(cmd, "@IsCancel", DbType.Int32, interview.IsCancel);
                Database.AddInParameter(cmd, "@TimezoneId", DbType.Int32, interview.TimezoneId);
                //********************************END*****************************************
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        interview = CreateEntityBuilder<Interview>().BuildEntity(reader);
                    }
                    else
                    {
                        interview = null;
                    }
                }

                if (interview == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Interview already exists. Please specify another interview.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this interview.");
                            }
                    }
                }

                return interview;
            }
        }

        Interview IInterviewDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Interview_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Interview>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        //****************************code added by pravin khot 10/Dec/2015*********************************
        string IInterviewDataAccess.SuggestedInterviewer_Id(int InterviewerId)
        {
            const string SP = "dbo.SuggestedInterviewer_Id";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@InterviewerId", DbType.Int32, InterviewerId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                    if (reader.Read())
                    {
                        return reader.GetString(0).ToString();
                    }
                    else
                    {
                        return string.Empty;
                    }
            }
        }
        //*************************************End***********************************************************
        IList<Interview> IInterviewDataAccess.GetAll()
        {
            const string SP = "dbo.Interview_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Interview>().BuildEntities(reader);
                }
            }
        }

        bool IInterviewDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Interview_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a interview which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this interview.");
                        }
                }
            }
        }

        //******************Code added by pravin khot on 4/May/2016************************
        bool IInterviewDataAccess.CancelById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Interview_CancelById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot Cancel a interview which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while Cancelling this interview.");
                        }
                }
            }
        }

        bool IInterviewDataAccess.UpdateIcsCodeById(int id,string ics)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Interview_UpdateIcsCodeById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);
                Database.AddInParameter(cmd, "@IcsFileUIDCode", DbType.AnsiString, ics);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot Cancel a interview which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while Cancelling this interview.");
                        }
                }
            }
        }
        //*******************************END*****************************************
        IList<Interview> IInterviewDataAccess.GetAllByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 0)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "dbo.Interview_GetAllByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Interview>().BuildEntities(reader);
                }
            }
        }

        IList<Interview> IInterviewDataAccess.GetInterviewDateByMemberId(int MemberId)
        {
            if (MemberId < 0)
            {
                throw new ArgumentException("MemberId");
            }
            const string SP = "dbo.Interview_GetStartDateByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return (new InterviewBuilder()).BuildInterviewEntities(reader);
                }
            }
        }

        IList<Interview> IInterviewDataAccess.GetInterviewByMemberIdAndJobPostingID(int MemberId,int JobPostingID)
        {
            if (MemberId < 0)
            {
                throw new ArgumentException("MemberId");
            }
            if (JobPostingID < 0)
            {
                throw new ArgumentException("JobPostingID");
            }

            const string SP = "dbo.Interview_GetByMemberIdAndJobPostingID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                Database.AddInParameter(cmd, "@JobPostingID", DbType.Int32, JobPostingID );

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return (new InterviewBuilder()).BuildInterviewEntities(reader);
                }
            }
        }

        bool IInterviewDataAccess.UpdateIntNoShow_ById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.UpdateInterview_NoShowById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot No-Show a interview which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while No-Show this interview.");
                        }
                }
            }
        }

        bool IInterviewDataAccess.UpdateIntCompleted_ById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.UpdateInterview_CompletedById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot Completed a interview which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while Moving to Completed this interview.");
                        }
                }
            }
        }

        Interview IInterviewDataAccess.AddInterviewStatusDetails(Interview InterviewStatusDetails)
        {
            const string SP = "dbo.Interview_Create_InterviewStatusDetails";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);

                //************** Code added by Sumit Sonawane on 1/Mar/2017 **************************
                Database.AddInParameter(cmd, "@InterviewId", DbType.Int32, InterviewStatusDetails.Id);
                Database.AddInParameter(cmd, "@IsCancel", DbType.Int32, InterviewStatusDetails.IsCancel);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, InterviewStatusDetails.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, InterviewStatusDetails.UpdatorId);
                Database.AddInParameter(cmd, "@CreateDate", DbType.DateTime, InterviewStatusDetails.CreateDate);
                Database.AddInParameter(cmd, "@UpdateDate", DbType.DateTime, InterviewStatusDetails.UpdateDate);
             
                //********************************END*****************************************

                Database.ExecuteNonQuery(cmd);
                return InterviewStatusDetails;
            }
        }

        Int32 IInterviewDataAccess.GetContactIdByJobPostingId (int JobpostingId)
        {

            if (JobpostingId < 1)
            {
                throw new ArgumentNullException("JobpostingId");
            }

            const string SP = "dbo.Interview_GetContactIdByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobpostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 ContactId = 0;

                    while (reader.Read())
                    {
                        ContactId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }

                    return ContactId;
                }
            }
        }

        string IInterviewDataAccess.GetInterviewTemplate(int MemberID, int InterviewID,string Template_Type,int CreatorId)
        {

            string SP = "dbo.Interview_GetPersonalInterviewTemplate";
            switch (Template_Type)
            {
                case "Face to Face":
                    SP = "dbo.Interview_GetPersonalInterviewTemplate";
                    break;
                case "Phone":
                    SP = "Interview_GetTeleconInterviewTemplate";
                    break;
                case "ProfileReject":
                    SP = "Interview_GetProfileRejectInterviewTemplate";
                    break;
                case "Reject":
                    SP = "Interview_GetRejectInterviewTemplate";
                    break;
                case "Initial Screening":
                    SP = "Interview_GetShortlistInterviewTemplate";
                    break;
            }
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberID", DbType.Int32, MemberID );

                Database.AddInParameter(cmd, "@InterviewID", DbType.Int32, InterviewID );
                
                if (SP == "Interview_GetRejectInterviewTemplate" || SP == "Interview_GetProfileRejectInterviewTemplate" || SP == "Interview_GetShortlistInterviewTemplate") 
                {
                    Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, CreatorId);
                }

                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.GetString (0).ToString();
                    }
                }
            }

            return string.Empty; ;
        }
        #endregion
    }
}