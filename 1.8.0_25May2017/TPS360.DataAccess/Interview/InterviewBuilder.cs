﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewBuilder.cs
    Description         :   This page is used to fill up Interview
    Created By          :    
    Created On          :    
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 20/Oct/2015         Prasanth Kumar G    Introduced Assessment Form (QuestionBank Type)
 *  0.2                 4/May/2016          pravin khot         New fields added-IcsFileUIDCode,IsCancel
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class InterviewBuilder : IEntityBuilder<Interview>
    {
        IList<Interview> IEntityBuilder<Interview>.BuildEntities(IDataReader reader)
        {
            List<Interview> interviews = new List<Interview>();

            while (reader.Read())
            {
                interviews.Add(((IEntityBuilder<Interview>)this).BuildEntity(reader));
            }

            return (interviews.Count > 0) ? interviews : null;
        }
        public IList<Interview> BuildInterviewEntities(IDataReader reader)
        {
            List<Interview> interview = new List<Interview>();

            while (reader.Read())
            {
                interview.Add(BuildInterviewEntity(reader));
            }

            return (interview.Count > 0) ? interview : null;
        }
        public Interview BuildInterviewEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_STARTDATETIME = 1;
            const int FLD_DURATION = 2;
            const int FLD_ALLDAYEVENT = 3;

            Interview interview = new Interview();

            interview.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            interview.StartDateTime = reader.IsDBNull(FLD_STARTDATETIME) ? DateTime.MinValue : reader.GetDateTime(FLD_STARTDATETIME);
            interview.Duration = reader.IsDBNull(FLD_DURATION) ? 0 : reader.GetInt32(FLD_DURATION);
            interview.AllDayEvent = reader.IsDBNull(FLD_ALLDAYEVENT) ? false : reader.GetBoolean(FLD_ALLDAYEVENT);
            return interview;
        }
        Interview IEntityBuilder<Interview>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_LOCATION = 1;
            const int FLD_REMARK = 2;
            const int FLD_FEEDBACK = 3;
            const int FLD_STATUS = 4;
            const int FLD_MEMBERID = 5;
            const int FLD_TYPELOOKUPID = 6;
            const int FLD_INTERVIEWLEVEL = 7;
            const int FLD_ACTIVITYID = 8;
            const int FLD_ISREMOVED = 9;
            const int FLD_CREATORID = 10;
            const int FLD_UPDATORID = 11;
            const int FLD_CREATEDATE = 12;
            const int FLD_UPDATEDATE = 13;
            const int FLD_TITLE = 14;
            const int FLD_JOBPOSTINGID = 15;
            const int FLD_CLIENTID = 16;
            const int FLD_STARTDATETIME = 17;
            const int FLD_DURATION = 18;
            const int FLD_ALLDAYEVENT = 19;
            const int FLD_ENABLEREMINDER = 20;
            const int FLD_REMINDERINTERVAL = 21;
            const int FLD_OTHERINTERVIEWERS = 22;
            //const int FLD_INDERVIEWDOCUMENTID = 23;
            const int FLD_QUESTIONBANKTYPELOOKUPID = 23; //Line introduced by Prasanth on 20/Oct/2015
            const int IcsFileUIDCode = 24;//ADDED BY PRAVIN KHOT ON 5/May/2016
            const int IsCancel = 25;//ADDED BY PRAVIN KHOT ON 5/May/2016
            const int FLD_TIMEZONE = 26;

            Interview interview = new Interview();

            interview.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            interview.Location = reader.IsDBNull(FLD_LOCATION) ? string.Empty : reader.GetString(FLD_LOCATION);
            interview.Remark = reader.IsDBNull(FLD_REMARK) ? string.Empty : reader.GetString(FLD_REMARK);
            interview.Feedback = reader.IsDBNull(FLD_FEEDBACK) ? string.Empty : reader.GetString(FLD_FEEDBACK);
            interview.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
            interview.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            interview.TypeLookupId = reader.IsDBNull(FLD_TYPELOOKUPID) ? 0 : reader.GetInt32(FLD_TYPELOOKUPID);
            interview.InterviewLevel = reader.IsDBNull(FLD_INTERVIEWLEVEL) ? 0 : reader.GetInt32(FLD_INTERVIEWLEVEL);
            interview.ActivityId = reader.IsDBNull(FLD_ACTIVITYID) ? 0 : reader.GetInt32(FLD_ACTIVITYID);
            interview.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            interview.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            interview.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            interview.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            interview.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            interview.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString(FLD_TITLE);
            interview.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            interview.ClientId = reader.IsDBNull(FLD_CLIENTID) ? 0 : reader.GetInt32(FLD_CLIENTID);
            interview.StartDateTime = reader.IsDBNull(FLD_STARTDATETIME) ? DateTime.MinValue : reader.GetDateTime(FLD_STARTDATETIME);
            interview.Duration = reader.IsDBNull(FLD_DURATION) ? 0 : reader.GetInt32(FLD_DURATION);
            interview.AllDayEvent = reader.IsDBNull(FLD_ALLDAYEVENT) ? false : reader.GetBoolean(FLD_ALLDAYEVENT);
            interview.EnableReminder = reader.IsDBNull(FLD_ENABLEREMINDER) ? false : reader.GetBoolean(FLD_ENABLEREMINDER);
            interview.ReminderInterval = reader.IsDBNull(FLD_REMINDERINTERVAL) ? 0 : reader.GetInt32(FLD_REMINDERINTERVAL);
            interview.OtherInterviewers = reader.IsDBNull(FLD_OTHERINTERVIEWERS) ? string.Empty : reader.GetString(FLD_OTHERINTERVIEWERS);
            //interview.InterviewDocumentId = reader.IsDBNull(FLD_INDERVIEWDOCUMENTID) ? 0 : reader.GetInt32(FLD_INDERVIEWDOCUMENTID);
            //Line introduced by Prasanth on 20/Oct/2015
            interview.QuestionBankTypeLookupId = reader.IsDBNull(FLD_QUESTIONBANKTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_QUESTIONBANKTYPELOOKUPID);

            if (reader.FieldCount > IcsFileUIDCode)
            {
                interview.IcsFileUIDCode = reader.IsDBNull(IcsFileUIDCode) ? string.Empty : reader.GetString(IcsFileUIDCode);
            }
            if (reader.FieldCount > IsCancel)
            {
                interview.IsCancel = reader.IsDBNull(IsCancel) ? 0 : reader.GetInt32(IsCancel);
            }
            if (reader.FieldCount > FLD_TIMEZONE)
            {
                interview.TimezoneId = reader.IsDBNull(FLD_TIMEZONE) ? 0 : reader.GetInt32(FLD_TIMEZONE);
            }
            return interview;

        }
    }
}