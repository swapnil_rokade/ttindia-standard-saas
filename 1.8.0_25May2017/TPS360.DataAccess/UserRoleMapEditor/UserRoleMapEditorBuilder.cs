﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: UserRoleMapEditorBuilder.cs
    Description:  UserRoleMapEditorBuilder
    Created By: PRAVIN KHOT
    Created On: 26/Feb/2016
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class UserRoleMapEditorBuilder : IEntityBuilder<UserRoleMapEditor>
    {

        IList<UserRoleMapEditor> IEntityBuilder<UserRoleMapEditor>.BuildEntities(IDataReader reader)
        {
            List<UserRoleMapEditor> UserRoleMapping = new List<UserRoleMapEditor>();

            while (reader.Read())
            {
                UserRoleMapping.Add(((IEntityBuilder<UserRoleMapEditor>)this).BuildEntity(reader));
            }

            return (UserRoleMapping.Count > 0) ? UserRoleMapping : null;
        }

        UserRoleMapEditor IEntityBuilder<UserRoleMapEditor>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_ROLE = 1;
            const int FLD_SYSTEMROLE = 2;
            const int FLD_CREATORID = 3;
            const int FLD_UPDATORID = 4;
            const int FLD_CREATEDATE = 5;
            const int FLD_UPDATEDATE = 6;

            UserRoleMapEditor UserRoleMapping = new UserRoleMapEditor();

            UserRoleMapping.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            UserRoleMapping.Role = reader.IsDBNull(FLD_ROLE) ? string.Empty : reader.GetString(FLD_ROLE);
            UserRoleMapping.SystemRole = reader.IsDBNull(FLD_SYSTEMROLE) ? string.Empty : reader.GetString(FLD_SYSTEMROLE);
            UserRoleMapping.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            UserRoleMapping.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            UserRoleMapping.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            UserRoleMapping.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            return UserRoleMapping;
        }
    }
}
