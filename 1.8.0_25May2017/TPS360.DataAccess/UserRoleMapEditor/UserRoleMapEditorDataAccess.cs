﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: UserRoleMapEditorDataAccess.cs
    Description:  UserRoleMapEditorDataAccess
    Created By: PRAVIN KHOT
    Created On: 26/Feb/2016
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------------------------------------------------------------
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class UserRoleMapEditorDataAccess : BaseDataAccess,IUserRoleMapEditorDataAccess
    {
          #region Constructors

        public UserRoleMapEditorDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<UserRoleMapEditor> CreateEntityBuilder<UserRoleMapEditor>()
        {
            return (new UserRoleMapEditorBuilder()) as IEntityBuilder<UserRoleMapEditor>;
        }

        #endregion
        #region  Methods
        UserRoleMapEditor IUserRoleMapEditorDataAccess.Add(UserRoleMapEditor UserRoleMapping)
        {
            const string SP = "dbo.UserRoleMapping_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, StringHelper.Convert(UserRoleMapping.CreatorId));
                Database.AddInParameter(cmd, "@RoleId", DbType.Int32, StringHelper.Convert(UserRoleMapping.RoleId));
                Database.AddInParameter(cmd, "@SystemRole", DbType.AnsiString, StringHelper.Convert(UserRoleMapping.SystemRole));
                Database.ExecuteNonQuery(cmd);
                return null;
            }
        }

        UserRoleMapEditor IUserRoleMapEditorDataAccess.Update(UserRoleMapEditor UserRoleMapping)
        {
            const string SP = "dbo.UserRoleMapping_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, UserRoleMapping.Id);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, UserRoleMapping.UpdatorId);
                Database.AddInParameter(cmd, "@RoleId", DbType.Int32, StringHelper.Convert(UserRoleMapping.RoleId));
                Database.AddInParameter(cmd, "@SystemRole", DbType.AnsiString, StringHelper.Convert(UserRoleMapping.SystemRole));
                Database.ExecuteNonQuery(cmd);
                return null;
            }

        }
        bool IUserRoleMapEditorDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.UserRoleMapping_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a industryTeamBuilder which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this industryTeamBuilder.");
                        }
                }
            }
        }


        PagedResponse<UserRoleMapEditor> IUserRoleMapEditorDataAccess.GetPaged(PagedRequest request) 
        {
            const string SP = "dbo.UserRoleMapping_GetPaged";
            string whereClause = string.Empty;
            if (request.SortColumn == null)
            {
                request.SortColumn = "";
            }
            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<UserRoleMapEditor> response = new PagedResponse<UserRoleMapEditor>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<UserRoleMapEditor>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        
        }
        UserRoleMapEditor IUserRoleMapEditorDataAccess.GetAllByUserRoleMappingId(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.UserRoleMapping_GetByUserRoleMappingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<UserRoleMapEditor>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        ArrayList IUserRoleMapEditorDataAccess.GetUserByUserRoleid(int buid, int jobpostingId)
        {
            if (buid < 1)
            {
                throw new ArgumentNullException("buid");
            }

            const string SP = "dbo.UserBuMapping_GetUserByBUId";
            
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@BUid", DbType.Int32, buid);
                Database.AddInParameter(cmd, "@jobpostingId", DbType.Int32, jobpostingId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList recList = new ArrayList();
                    while (reader.Read()) 
                    {
                        recList.Add(new Employee { Id = reader.GetInt32(0), FirstName = reader.GetString(1) });
                    }
                    return recList;
                }
            }
            
        }
        ArrayList IUserRoleMapEditorDataAccess.GetUserNameByUserRoleid(int buid, int jobpostingId)
        {
            if (buid < 1)
            {
                throw new ArgumentNullException("buid");
            }

            const string SP = "dbo.UserBuMapping_GetUserNameByBUId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@BUid", DbType.Int32, buid);
                Database.AddInParameter(cmd, "@jobpostingId", DbType.Int32, jobpostingId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList recList = new ArrayList();
                    while (reader.Read())
                    {
                        recList.Add(new Employee { Id = reader.GetInt32(0), FirstName = reader.GetString(1) });
                    }
                    return recList;
                }
            }

        }

        Int32 IUserRoleMapEditorDataAccess.UserRoleMapping_Id(int RoleId)
        {
            const string SP = "dbo.UserRoleMapping_EditId";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RoleId", DbType.Int32, RoleId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                    if (reader.Read())
                    {
                        return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                    else
                    {
                        return 0;
                    }
            }
        }
        #endregion
    }
}
