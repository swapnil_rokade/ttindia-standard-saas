﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberGroupManagerBuilder : IEntityBuilder<MemberGroupManager>
    {
        IList<MemberGroupManager> IEntityBuilder<MemberGroupManager>.BuildEntities(IDataReader reader)
        {
            List<MemberGroupManager> memberGroupManagers = new List<MemberGroupManager>();

            while (reader.Read())
            {
                memberGroupManagers.Add(((IEntityBuilder<MemberGroupManager>)this).BuildEntity(reader));
            }

            return (memberGroupManagers.Count > 0) ? memberGroupManagers : null;
        }

        MemberGroupManager IEntityBuilder<MemberGroupManager>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERGROUPID = 1;
            const int FLD_MEMBERID = 2;
            const int FLD_ISREMOVED = 3;
            const int FLD_CREATORID = 4;
            const int FLD_UPDATORID = 5;
            const int FLD_CREATEDATE = 6;
            const int FLD_UPDATEDATE = 7;

            MemberGroupManager memberGroupManager = new MemberGroupManager();

            memberGroupManager.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberGroupManager.MemberGroupId = reader.IsDBNull(FLD_MEMBERGROUPID) ? 0 : reader.GetInt32(FLD_MEMBERGROUPID);
            memberGroupManager.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberGroupManager.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberGroupManager.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberGroupManager.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberGroupManager.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberGroupManager.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberGroupManager;
        }
    }
}