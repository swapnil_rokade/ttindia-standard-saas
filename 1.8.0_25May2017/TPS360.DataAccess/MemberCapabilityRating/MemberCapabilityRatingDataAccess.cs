﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberCapabilityRatingDataAccess : BaseDataAccess, IMemberCapabilityRatingDataAccess
    {
        #region Constructors

        public MemberCapabilityRatingDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberCapabilityRating> CreateEntityBuilder<MemberCapabilityRating>()
        {
            return (new MemberCapabilityRatingBuilder()) as IEntityBuilder<MemberCapabilityRating>;
        }

        #endregion

        #region  Methods

        MemberCapabilityRating IMemberCapabilityRatingDataAccess.Add(MemberCapabilityRating memberCapabilityRating)
        {
            const string SP = "dbo.MemberCapabilityRating_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberCapabilityRating.MemberId);
                Database.AddInParameter(cmd, "@PositionId", DbType.Int32, memberCapabilityRating.PositionId);
                Database.AddInParameter(cmd, "@CapabilityType", DbType.Int32, memberCapabilityRating.CapabilityType);
                Database.AddInParameter(cmd, "@PositionCapabilityMapId", DbType.Int32, memberCapabilityRating.PositionCapabilityMapId);
                Database.AddInParameter(cmd, "@Rating", DbType.Int32, memberCapabilityRating.Rating);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberCapabilityRating.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberCapabilityRating.CreatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberCapabilityRating = CreateEntityBuilder<MemberCapabilityRating>().BuildEntity(reader);
                    }
                    else
                    {
                        memberCapabilityRating = null;
                    }
                }

                if (memberCapabilityRating == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberCapabilityRating already exists. Please specify another memberCapabilityRating.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberCapabilityRating.");
                            }
                    }
                }

                return memberCapabilityRating;
            }
        }

        MemberCapabilityRating IMemberCapabilityRatingDataAccess.Update(MemberCapabilityRating memberCapabilityRating)
        {
            const string SP = "dbo.MemberCapabilityRating_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberCapabilityRating.Id);
                Database.AddInParameter(cmd, "@CapabilityType", DbType.Int32, memberCapabilityRating.CapabilityType);
                Database.AddInParameter(cmd, "@PositionCapabilityMapId", DbType.Int32, memberCapabilityRating.PositionCapabilityMapId);
                Database.AddInParameter(cmd, "@Rating", DbType.Int32, memberCapabilityRating.Rating);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberCapabilityRating.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberCapabilityRating.UpdatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberCapabilityRating = CreateEntityBuilder<MemberCapabilityRating>().BuildEntity(reader);
                    }
                    else
                    {
                        memberCapabilityRating = null;
                    }
                }

                if (memberCapabilityRating == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberCapabilityRating already exists. Please specify another memberCapabilityRating.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberCapabilityRating.");
                            }
                    }
                }

                return memberCapabilityRating;
            }
        }

        MemberCapabilityRating IMemberCapabilityRatingDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberCapabilityRating_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberCapabilityRating>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberCapabilityRating> IMemberCapabilityRatingDataAccess.GetAll()
        {
            const string SP = "dbo.MemberCapabilityRating_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberCapabilityRating>().BuildEntities(reader);
                }
            }
        }

        bool IMemberCapabilityRatingDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberCapabilityRating_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberCapabilityRating which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberCapabilityRating.");
                        }
                }
            }
        }

        IList<MemberCapabilityRating> IMemberCapabilityRatingDataAccess.GetAllByMemberIdAndPositionId(int memberId, int positionId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            if (positionId < 1)
            {
                throw new ArgumentNullException("positionId");
            }

            const string SP = "dbo.MemberCapabilityRating_GetAllMemberCapabilityRatingByMemberIdAndPositionId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {                
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@PositionId", DbType.Int32, positionId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberCapabilityRating>().BuildEntities(reader);
                }
            }
        }

        bool IMemberCapabilityRatingDataAccess.DeleteByMemberIdAndPositionId(int memberId, int positionId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            if (positionId < 1)
            {
                throw new ArgumentNullException("positionId");
            }

            const string SP = "dbo.MemberCapabilityRating_DeleteMemberCapabilityRatingByMemberIdAndPositionId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@PositionId", DbType.Int32, positionId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberCapabilityRating which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberCapabilityRating.");
                        }
                }
            }
        }

        MemberCapabilityRating IMemberCapabilityRatingDataAccess.GetByMemberIdAndPositionIdAndCapabilityTypeAndPositionCapabilityMapId(int memberId, int positionId, int capabilityType, int positionCapabilityMapId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            if (positionId < 1)
            {
                throw new ArgumentNullException("positionId");
            }
            if (capabilityType < 1)
            {
                throw new ArgumentNullException("capabilityType");
            }


            const string SP = "dbo.MemberCapabilityRating_GetMemberCapabilityRatingByMemberIdAndPositionIdAndCapabilityTypeAndPositionCapabilityMapId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@PositionId", DbType.Int32, positionId);
                Database.AddInParameter(cmd, "@CapabilityType", DbType.Int32, capabilityType);
                Database.AddInParameter(cmd, "@PositionCapabilityMapId", DbType.Int32, positionCapabilityMapId);


                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberCapabilityRating>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        #endregion
    }
}