﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberExperienceDetailDataAccess : BaseDataAccess, IMemberExperienceDetailDataAccess
    {
        #region Constructors

        public MemberExperienceDetailDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberExperienceDetail> CreateEntityBuilder<MemberExperienceDetail>()
        {
            return (new MemberExperienceDetailBuilder()) as IEntityBuilder<MemberExperienceDetail>;
        }

        #endregion

        #region  Methods

        MemberExperienceDetail IMemberExperienceDetailDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberExperienceDetail_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberExperienceDetail>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberExperienceDetail> IMemberExperienceDetailDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberExperienceDetail_GetAllByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberExperienceDetail>().BuildEntities(reader);
                }
            }
        }

        PagedResponse<MemberExperienceDetail> IMemberExperienceDetailDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberExperienceDetail_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "MemberId"))
                        {
                            sb.Append("[ME].[MemberId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "DateFrom";
            }

            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "DESC";
            }

            request.SortColumn = "[ME].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberExperienceDetail> response = new PagedResponse<MemberExperienceDetail>();
                cmd.CommandTimeout = 0;
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response =CreateEntityBuilder<MemberExperienceDetail>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        #endregion
    }
}