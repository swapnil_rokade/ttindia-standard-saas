﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: RejectCandidateDataAccess.cs
    Description: This page is used for reject candidate data access.
    Created By: 
    Created On:
    Modification Log:
    ----------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ----------------------------------------------------------------------------------------------------------------------------------------    
 * 0.1         14/March/2016        pravin khot          Introduced by getWhereClauseForReportReject,GetPagedForReportReject,GetPagedForReportRejectBySelectedColumn

 * ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 */
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using TPS360.Common.BusinessEntity;

namespace TPS360.DataAccess
{
    internal sealed class RejectCandidateDataAccess : BaseDataAccess, IRejectCandidateDataAccess
    {
        #region Constructors

        public RejectCandidateDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<RejectCandidate> CreateEntityBuilder<RejectCandidate>()
        {
            return (new RejectCandidateBuilder()) as IEntityBuilder<RejectCandidate>;
        }

        #endregion

        #region  Methods

        void IRejectCandidateDataAccess.Add(RejectCandidate RejectCandidate, string MemberId)
        {
            const string SP = "dbo.RejectCandidate_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.AnsiString ,MemberId );
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, RejectCandidate.JobPostingID );
                Database.AddInParameter(cmd, "@RejectDetails", DbType.AnsiString  , RejectCandidate.RejectDetails );
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, RejectCandidate.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, RejectCandidate.UpdatorId );
                Database.AddInParameter(cmd, "@HiringMatrixLevel", DbType.Int32, RejectCandidate.HiringMatrixLevel );
                Database.ExecuteNonQuery(cmd);
            }
        }

        void IRejectCandidateDataAccess.Update(RejectCandidate RejectCandidate)
        {
            const string SP = "dbo.RejectCandidate_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, RejectCandidate.Id);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32 , RejectCandidate .MemberId );
                Database.AddInParameter(cmd, "@JobPostingID", DbType.Int32, RejectCandidate.JobPostingID );
                Database.AddInParameter(cmd, "@RejectDetails", DbType.AnsiString  , RejectCandidate.RejectDetails);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, RejectCandidate.UpdatorId);
                Database.AddInParameter(cmd, "@HiringMatrixLevel", DbType.Int32, RejectCandidate.HiringMatrixLevel);
                Database.ExecuteNonQuery(cmd);
            }
        }

        RejectCandidate IRejectCandidateDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.RejectCandidate_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<RejectCandidate>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<RejectCandidate> IRejectCandidateDataAccess.GetByMemberId(int Memberid)
        {
            if (Memberid < 1)
            {
                throw new ArgumentNullException("Memberid");
            }

            const string SP = "dbo.RejectCandidate_GetByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, Memberid);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    //IList<RejectCandidate> Candidate =new IList<RejectCandidate>();
                    //while (reader.Read())
                    //{
                    return CreateEntityBuilder<RejectCandidate>().BuildEntities(reader);
                    //}
                    //if (Candidate.Count != 0)
                    //    return Candidate;
                    //else
                    //    return null;
                }
            }
        }

        bool IRejectCandidateDataAccess.DeleteByMemberId(int Memberid)
        {
            if (Memberid < 1)
            {
                throw new ArgumentNullException("Memberid");
            }

            const string SP = "dbo.RejectCandidate_DeleteByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, Memberid);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a Reject Details which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this Reject Details.");
                        }
                }
            }
        }

        RejectCandidate IRejectCandidateDataAccess.GetByMemberIdAndJobPostingId(int Memberid,int JobPostingID)
        {
            if (Memberid < 1)
            {
                throw new ArgumentNullException("Memberid");
            }
            if (JobPostingID < 1)
            {
                throw new ArgumentNullException("JobPostingID");
            }
            const string SP = "dbo.RejectCandidate_GetByMemberIdAndJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, Memberid);
                Database.AddInParameter(cmd, "@JobPostingID", DbType.Int32, JobPostingID );
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<RejectCandidate>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        bool IRejectCandidateDataAccess.DeleteByMemberIdAndJobPostingID(int Memberid,int  JobPostingID)
        {
            if (Memberid < 1)
            {
                throw new ArgumentNullException("Memberid");
            }
            if (JobPostingID  < 1)
            {
                throw new ArgumentNullException("JobPostingId");
            }
            const string SP = "dbo.RejectCandidate_DeleteByMemberIdAndJobPostingID";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, Memberid);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingID );

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a Reject Details which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this Reject Details.");
                        }
                }
            }
        }

        PagedResponse<RejectCandidate> IRejectCandidateDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.RejectCandidate_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobPostingId"))
                        {
                            sb.Append("[RC].[JobPostingId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[M].[FirstName]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "asc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<RejectCandidate> response = new PagedResponse<RejectCandidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new RejectCandidateBuilder()).BuildEntitiesForRejectCandidateList(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        int IRejectCandidateDataAccess.GetRejectCandidateCount(int JobPostingId)
        {
            if (JobPostingId < 1)
            {
                throw new ArgumentNullException("JobPostingId");
            }

            const string SP = "RejectCandidate_Count";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (int)reader.GetValue(0);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        //*********************Code added by pravin khot on 14/March/2016****************START CODE***************


        private StringBuilder getWhereClauseForReportReject(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource,
                  int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
                  int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string JobPostingId)
        {


            StringBuilder whereClause = new StringBuilder();
            if (addedFrom != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                //  whereClause.Append("[C].[CreateDate]");
                whereClause.Append("[R].[CreateDate]");
                whereClause.Append(" >= ");
                // whereClause.Append("'" + addedFrom.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(addedFrom).ToString("dd/MM/yyyy") + "',103) ");
            }
            if (addedTo != null && addedTo != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                // whereClause.Append("[C].[CreateDate]");
                whereClause.Append("[R].[CreateDate]");
                whereClause.Append(" < ");
                //whereClause.Append("'" + addedTo.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(addedTo).AddDays(1).ToString("dd/MM/yyyy") + "',103) ");
            }
            if (addedBy > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[R].[UpdatorId]");
                whereClause.Append(" = ");
                whereClause.Append(addedBy.ToString());
            }
            if (addedBySource > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[ResumeSource]");
                whereClause.Append(" = ");
                whereClause.Append(addedBySource.ToString());
            }          
           
            if (country > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[PermanentCountryId]");//0.8
                whereClause.Append(" = ");
                whereClause.Append(country);
            }
            if (state > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[PermanentStateId]");//0.8
                whereClause.Append(" = ");
                whereClause.Append(state);
            }
            if (!string.IsNullOrEmpty(city))
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[PermanentCity]");//0.8
                whereClause.Append(" LIKE ");
                whereClause.Append("'%" + city + "%'");
            }

            StringBuilder interviewWhereBuilder = new StringBuilder();
            if (interviewFrom != null && interviewFrom != DateTime.MinValue)
            {
                if (interviewLevel > 1 && interviewLevel < 5)
                {
                    if (!StringHelper.IsBlank(interviewWhereBuilder))
                    {
                        interviewWhereBuilder.Append(" AND ");
                    }
                    // interviewWhereBuilder.Append("[A].[StartDateTimeUtc]");
                    interviewWhereBuilder.Append("[dbo].[GetUTCDateOnly]([A].[StartDateTimeUtc])");
                    interviewWhereBuilder.Append(" >= ");
                    // interviewWhereBuilder.Append("'" + interviewFrom.AddHours(-6).ToShortDateString() + "'");

                    interviewWhereBuilder.Append("[dbo].[GetDateOnly]('" + Convert.ToDateTime(interviewFrom) + "')");
                }
            }
            if (interviewTo != null && interviewTo != DateTime.MinValue)
            {
                if (interviewLevel > 1 && interviewLevel < 5)
                {
                    if (!StringHelper.IsBlank(interviewWhereBuilder))
                    {
                        interviewWhereBuilder.Append(" AND ");
                    }
                    // interviewWhereBuilder.Append("[A].[StartDateTimeUtc]");
                    interviewWhereBuilder.Append("[dbo].[GetUTCDateOnly]([A].[StartDateTimeUtc])");
                    interviewWhereBuilder.Append(" <= ");
                    //  interviewWhereBuilder.Append("'" + interviewTo.AddHours(-6).ToShortDateString() + "'");
                    interviewWhereBuilder.Append("[dbo].[GetDateOnly]('" + Convert.ToDateTime(interviewTo) + "')");
                }
            }
            if (interviewLevel > 1 && interviewLevel < 5)
            {
                if (!StringHelper.IsBlank(interviewWhereBuilder))
                {
                    interviewWhereBuilder.Append(" AND ");
                }
                interviewWhereBuilder.Append("[I].[InterviewLevel]");
                interviewWhereBuilder.Append(" = ");
                interviewWhereBuilder.Append(interviewLevel.ToString());
            }



            if (interviewStatus > 0)
            {
                if (!StringHelper.IsBlank(interviewWhereBuilder))
                {
                    interviewWhereBuilder.Append(" AND ");
                }
                interviewWhereBuilder.Append("[I].[Status]");
                interviewWhereBuilder.Append(" = ");
                interviewWhereBuilder.Append(interviewStatus.ToString());
            }

            if (!StringHelper.IsBlank(interviewWhereBuilder))
            {
                if (interviewLevel > 1 && interviewLevel < 5)
                {
                    StringBuilder interviewBuilder = new StringBuilder(@" ( SELECT DISTINCT [M].[Id] FROM
                    [Interview] [I] LEFT JOIN [MemberJobCart] [MJC] ON [I].[MemberJobCartId]=[MJC].[Id]
                    LEFT JOIN [Member] [M] ON [MJC].[MemberId]=[M].[Id]
                    LEFT JOIN [Activity] [A] ON [A].[ActivityId]=[I].[ActivityID]");

                    interviewBuilder.Append(" WHERE ");
                    interviewBuilder.Append(interviewWhereBuilder);

                    interviewBuilder.Append(" ) ");


                    if (!StringHelper.IsBlank(whereClause))
                    {
                        whereClause.Append(" AND ");
                    }
                    whereClause.Append(" [C].[Id] ");
                    whereClause.Append(" IN ");
                    whereClause.Append(interviewBuilder.ToString());
                }
            }

            StringBuilder experienceWhereBuilder = new StringBuilder();
            if (industry > 0)
            {
                if (!StringHelper.IsBlank(experienceWhereBuilder))
                {
                    experienceWhereBuilder.Append(" AND ");
                }
                experienceWhereBuilder.Append("[ME].[IndustryCategoryLookupId]");
                experienceWhereBuilder.Append(" = ");
                experienceWhereBuilder.Append(industry.ToString());
            }
            if (functionalCategory > 0)
            {
                if (!StringHelper.IsBlank(experienceWhereBuilder))
                {
                    experienceWhereBuilder.Append(" AND ");
                }
                experienceWhereBuilder.Append("[ME].[FunctionalCategoryLookupId]");
                experienceWhereBuilder.Append(" = ");
                experienceWhereBuilder.Append(functionalCategory.ToString());
            }

            if (!StringHelper.IsBlank(experienceWhereBuilder))
            {
                StringBuilder experienceBuilder = new StringBuilder(@" ( SELECT DISTINCT [ME].[MemberId] FROM
                [MemberExperience] [ME]");
                experienceBuilder.Append(" WHERE ");
                experienceBuilder.Append(experienceWhereBuilder);
                experienceBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(experienceBuilder.ToString());
            }

            if (workPermit > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[ME].[WorkAuthorizationLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(workPermit.ToString());
            }
            if (WorkSchedule > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[ME].[WorkScheduleLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(WorkSchedule.ToString());
            }

            if (gender > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[GenderLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(gender.ToString());
            }

            if (maritalStatus > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[MaritalStatusLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(maritalStatus.ToString());
            }

            if (educationId > 0)
            {
                StringBuilder educationBuilder = new StringBuilder(@" ( SELECT DISTINCT [ME].[MemberId] FROM
                [MemberEducation] [ME]");
                educationBuilder.Append(" WHERE ");
                educationBuilder.Append(" [ME].[LevelOfEducationLookupId] ");
                educationBuilder.Append(" = ");
                educationBuilder.Append(educationId.ToString());
                educationBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(educationBuilder.ToString());
            }

            if (assessmentId > 0)
            {
                StringBuilder assessmentBuilder = new StringBuilder(@" ( SELECT DISTINCT [MT].[MemberId] FROM
                [MemberTestScore] [MT]");
                assessmentBuilder.Append(" WHERE ");
                assessmentBuilder.Append(" [MT].[TestMasterId] ");
                assessmentBuilder.Append(" = ");
                assessmentBuilder.Append(assessmentId.ToString());
                assessmentBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(assessmentBuilder.ToString());
            }

            if (isBroadcastedResume)
            {
                StringBuilder broadcastResumeBuilder = new StringBuilder(@" ( SELECT DISTINCT [ActivityOnObjectId]
                FROM [MemberActivity] [MA] LEFT JOIN
                [MemberActivityType] [MAT] ON [MA].[MemberActivityTypeId]=[MAT].[Id] ");
                broadcastResumeBuilder.Append(" WHERE ");
                broadcastResumeBuilder.Append(" [MA].[ActivityOn] ");
                broadcastResumeBuilder.Append(" = ");
                broadcastResumeBuilder.Append((int)MemberType.Candidate);
                broadcastResumeBuilder.Append(" AND ");
                broadcastResumeBuilder.Append(" [MAT].[ActivityType] ");
                broadcastResumeBuilder.Append(" = ");
                broadcastResumeBuilder.Append((int)ActivityType.ResumeBroadCast);
                broadcastResumeBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(broadcastResumeBuilder.ToString());
            }

            if (isReferredApplicants)
            {
                StringBuilder referredApplicantsBuilder = new StringBuilder(@" ( SELECT [Id] FROM [Candidate] ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[CreatorId] ");
                whereClause.Append(" IN ");
                whereClause.Append(referredApplicantsBuilder.ToString());
            }

            if (hasJobAgent)
            {

            }

            //Assigned Manager
            if (assignedManager > 0)
            {
                StringBuilder assignedManagerBuilder = new StringBuilder(@" ( SELECT DISTINCT [MemberId]
                                                FROM [MemberManager] [MM]");
                assignedManagerBuilder.Append(" WHERE ");
                assignedManagerBuilder.Append(" [MM].[ManagerId] ");
                assignedManagerBuilder.Append(" = ");
                assignedManagerBuilder.Append(assignedManager);
                assignedManagerBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(assignedManagerBuilder.ToString());
            }
            if (interviewLevel == 1 || interviewLevel == 5)
            {
                StringBuilder tmpinterviewWhereBuilder = new StringBuilder();
                tmpinterviewWhereBuilder.Append("[MJCD].[SelectionStepLookupId]");
                tmpinterviewWhereBuilder.Append(" = ");
                tmpinterviewWhereBuilder.Append(interviewLevel.ToString());


                StringBuilder interviewBuilder = new StringBuilder(@" ( SELECT DISTINCT [M].[Id] FROM
                        [MemberJobCartDetail] [MJCD] LEFT JOIN [MemberJobCart] [MJC] ON [MJCD].[MemberJobCartId]=[MJC].[Id]
                        LEFT JOIN [Member] [M] ON [MJC].[MemberId]=[M].[Id]");

                interviewBuilder.Append(" WHERE ");
                interviewBuilder.Append(tmpinterviewWhereBuilder);

                interviewBuilder.Append(" ) ");


                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" And  ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(interviewBuilder.ToString());
            }
            //JobPostingId
            if (Convert.ToInt32(JobPostingId) > 0)
            {

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[R].[JobPostingId] ");
                whereClause.Append(" = ");
                whereClause.Append(JobPostingId.ToString());
            }
            return whereClause;
        }
        PagedResponse<RejectCandidate> IRejectCandidateDataAccess.GetPagedForReportReject(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, 
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string JobPostingId)
        {
            const string SP = "dbo.Rejectedcandidate_GetPagedReport";
            StringBuilder whereClause = new StringBuilder();
            whereClause = getWhereClauseForReportReject(request, addedFrom, addedTo, addedBy, addedBySource, 
             country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
             gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, JobPostingId);

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    String.Empty
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<RejectCandidate> response = new PagedResponse<RejectCandidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<RejectCandidate>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        PagedResponse<DynamicDictionary> IRejectCandidateDataAccess.GetPagedForReportRejectBySelectedColumn(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, 
      int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
      int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string JobPostingId, IList<string> CheckedList)
        {
            string Column = "";
            foreach (string s in CheckedList)
            {
                if (Column != "") Column += ",";
                Column += s;
            }
            const string SP = "dbo.Rejectedcandidate_GetPagedReportForSelectedColumns";
            StringBuilder whereClause = new StringBuilder();
            whereClause = getWhereClauseForReportReject(request, addedFrom, addedTo, addedBy, addedBySource, 
             country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
             gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, JobPostingId);
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                   Column 
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<DynamicDictionary> response = new PagedResponse<DynamicDictionary>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                   
                    response.Response = (new RejectCandidateBuilder()).BuildEntities(reader, CheckedList);// CreateEntityBuilder<Candidate>().BuildEntities(reader, CheckedList);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        //***********************************END*****************************************************

        #endregion
    }
}