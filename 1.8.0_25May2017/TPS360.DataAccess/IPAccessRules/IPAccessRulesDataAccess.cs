﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:CountryDataAccess.cs
    Description: This page is used for Countries  functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetCountryIdByCountryCode() to assist 
                                                                Resume parser team
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class IPAccessRulesDataAccess : BaseDataAccess, IIPAccessRulesDataAccess
    {
        #region Constructors

        public IPAccessRulesDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<IPAccessRules > CreateEntityBuilder<IPAccessRules>()
        {
            return (new IPAccessRulesBuilder ()) as IEntityBuilder<IPAccessRules >;
        }

        #endregion

        #region  Methods

        IPAccessRules IIPAccessRulesDataAccess.Add(IPAccessRules IPAccessRules)
        {
            const string SP = "dbo.IPAccessRules_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.AnsiString  , StringHelper .Convert ( IPAccessRules .MemberId)  );
                Database.AddInParameter(cmd, "@UserIp", DbType.AnsiString, StringHelper.Convert(IPAccessRules.AllowedIP ));
                Database.AddInParameter(cmd, "@UserIpTo", DbType.AnsiString, StringHelper.Convert(IPAccessRules.AllowedIPTo));
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        IPAccessRules = CreateEntityBuilder<IPAccessRules>().BuildEntity(reader);
                    }
                    else
                    {
                        IPAccessRules = null;
                    }
                }

                if (IPAccessRules == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("IPAccessRules already exists. Please specify another IPAccessRules.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this IPAccessRules.");
                            }
                    }
                }

                return IPAccessRules;
            }
        }

        IPAccessRules IIPAccessRulesDataAccess.Update(IPAccessRules IPAccessRules)
        {
            const string SP = "dbo.IPAccessRules_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, IPAccessRules.Id);
                Database.AddInParameter(cmd, "@MemberId", DbType.AnsiString, StringHelper.Convert(IPAccessRules.MemberId));
                Database.AddInParameter(cmd, "@UserIp", DbType.AnsiString, StringHelper.Convert(IPAccessRules.AllowedIP));
                Database.AddInParameter(cmd, "@UserIpTo", DbType.AnsiString, StringHelper.Convert(IPAccessRules.AllowedIPTo));
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        IPAccessRules = CreateEntityBuilder<IPAccessRules>().BuildEntity(reader);
                    }
                    else
                    {
                        IPAccessRules = null;
                    }
                }

                if (IPAccessRules == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("IPAccessRules already exists. Please specify another IPAccessRules.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this IPAccessRules.");
                            }
                    }
                }

                return IPAccessRules;
            }
        }

        IPAccessRules IIPAccessRulesDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.IPAccessRules_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<IPAccessRules>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<IPAccessRules> IIPAccessRulesDataAccess.GetAll()
        {
            const string SP = "dbo.IPAccessRules_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<IPAccessRules>().BuildEntities(reader);
                }
            }
        }

        bool IIPAccessRulesDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.IPAccessRules_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a IPAccessRules which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this IPAccessRules.");
                        }
                }
            }
        }
        PagedResponse<IPAccessRules> IIPAccessRulesDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.IPAccessRules_GetPaged";
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[M].[FirstName]";
            }

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(""),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<IPAccessRules> response = new PagedResponse<IPAccessRules>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<IPAccessRules>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;

            }
        }
        #endregion
    }
}