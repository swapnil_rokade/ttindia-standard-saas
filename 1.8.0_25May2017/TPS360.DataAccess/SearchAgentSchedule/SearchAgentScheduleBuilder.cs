﻿using System;
using System.Data;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class SearchAgentScheduleBuilder : IEntityBuilder<SearchAgentSchedule>
    {
        IList<SearchAgentSchedule> IEntityBuilder<SearchAgentSchedule>.BuildEntities(IDataReader reader)
        {
            List<SearchAgentSchedule> SearchAgent = new List<SearchAgentSchedule>();

            while (reader.Read())
            {
                SearchAgent.Add(((IEntityBuilder<SearchAgentSchedule>)this).BuildEntity(reader));
            }

            return (SearchAgent.Count > 0) ? SearchAgent : null;
        }
        SearchAgentSchedule IEntityBuilder<SearchAgentSchedule>.BuildEntity(IDataReader reader)
        {
            SearchAgentSchedule SearchAgent = new SearchAgentSchedule();

            const int FLD_ID = 0;
            const int FLD_JOBPOSTINGID = 1;
            const int FLD_STARTDATE = 2;
            const int FLD_REPEAT = 3;
            const int FLD_ENDDATE = 4;
            const int FLD_LASTSENTDATE = 5;
            const int FLD_NEXTSENDDATE = 6;
            const int FLD_CREATORID = 7;
            const int FLD_UPDATORID = 8;
            const int FLD_CREATEDATE = 9;
            const int FLD_UPDATEDATE =10;
            const int FLD_ISREMOVED = 11;

            SearchAgent.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            SearchAgent.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            SearchAgent.StartDate = reader.IsDBNull(FLD_STARTDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_STARTDATE);
            SearchAgent.Repeat = reader.IsDBNull(FLD_REPEAT) ? string.Empty : reader.GetString(FLD_REPEAT);
            SearchAgent.EndDate = reader.IsDBNull(FLD_ENDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ENDDATE);
            SearchAgent.LastSentDate = reader.IsDBNull(FLD_LASTSENTDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_LASTSENTDATE);
            SearchAgent.NextSendDate = reader.IsDBNull(FLD_NEXTSENDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_NEXTSENDDATE);
            SearchAgent.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            SearchAgent.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            SearchAgent.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            SearchAgent.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            SearchAgent.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);

            return SearchAgent;
        }
    }
}
