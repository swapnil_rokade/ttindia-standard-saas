﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
namespace TPS360.DataAccess
{
    internal sealed class SearchAgentScheduleDataAccess : BaseDataAccess,ISearchAgentScheduleDataAccess  
    {
          #region Constructors

        public SearchAgentScheduleDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<SearchAgentSchedule> CreateEntityBuilder<SearchAgentSchedule>()
        {
            return (new SearchAgentScheduleBuilder()) as IEntityBuilder<SearchAgentSchedule>;
        }

        #endregion

        #region Methods
        void ISearchAgentScheduleDataAccess.AddCandidateForMail (SearchAgentMailToCandidate  Candidate)
        {
            const string SP = "dbo.SearchAgentMailToCandidate_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, Candidate.JobPostingId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, Candidate.MemberId );
                Database.AddInParameter(cmd, "@MemberEmailId", DbType.Int32 , Candidate.MemberEmailId );
                Database.ExecuteNonQuery(cmd);
            }
        }

        SearchAgentSchedule ISearchAgentScheduleDataAccess.Add(SearchAgentSchedule SearchAgent)
        {
            const string SP = "dbo.SearchAgentSchedule_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, SearchAgent.JobPostingId);
                Database.AddInParameter(cmd, "@StartDate", DbType.DateTime , SearchAgent.StartDate );
                Database.AddInParameter(cmd, "@Repeat", DbType.AnsiString, StringHelper.Convert(SearchAgent.Repeat ));
                Database.AddInParameter(cmd, "@EndDate", DbType.DateTime ,NullConverter .Convert (SearchAgent.EndDate) );
                Database.AddInParameter(cmd, "@LastSentDate", DbType.DateTime ,NullConverter .Convert ( SearchAgent.LastSentDate) );
                Database.AddInParameter(cmd, "@NextSendDate", DbType.DateTime , SearchAgent.NextSendDate );
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, SearchAgent.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, SearchAgent.UpdatorId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, SearchAgent.IsRemoved);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        SearchAgent = CreateEntityBuilder<SearchAgentSchedule>().BuildEntity(reader);
                    }
                    else
                    {
                        SearchAgent = null;
                    }
                }

                if (SearchAgent == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Search Agent Schedule already exists. Please specify another Search Agent Schedule.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this Search Agent Schedule.");
                            }
                    }
                }

                return SearchAgent;
            }
        }

        SearchAgentSchedule ISearchAgentScheduleDataAccess.Update(SearchAgentSchedule SearchAgent)
        {
            const string SP = "dbo.SearchAgentSchedule_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, SearchAgent.Id);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, SearchAgent.JobPostingId);
                Database.AddInParameter(cmd, "@StartDate", DbType.DateTime, SearchAgent.StartDate );
                Database.AddInParameter(cmd, "@Repeat", DbType.AnsiString, StringHelper.Convert(SearchAgent.Repeat));
                Database.AddInParameter(cmd, "@EndDate", DbType.DateTime,NullConverter .Convert ( SearchAgent.EndDate));
                Database.AddInParameter(cmd, "@LastSentDate", DbType.DateTime, NullConverter .Convert (SearchAgent.LastSentDate));
                Database.AddInParameter(cmd, "@NextSendDate", DbType.DateTime, SearchAgent.NextSendDate);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, SearchAgent.UpdatorId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, SearchAgent.IsRemoved);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        SearchAgent = CreateEntityBuilder<SearchAgentSchedule>().BuildEntity(reader);
                    }
                    else
                    {
                        SearchAgent = null;
                    }
                }

                if (SearchAgent == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Search Agent Schedule already exists. Please specify another Search Agent Schedule.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this Search Agent Schedule.");
                            }
                    }
                }

                return SearchAgent;
            }
        }

        SearchAgentSchedule ISearchAgentScheduleDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.SearchAgentSchedule_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<SearchAgentSchedule>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        SearchAgentSchedule ISearchAgentScheduleDataAccess.GetByJobPostingId(int JobPostingId)
        {
            if (JobPostingId < 1)
            {
                throw new ArgumentNullException("JobPostingId");
            }

            const string SP = "dbo.SearchAgentSchedule_GetByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<SearchAgentSchedule>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        bool ISearchAgentScheduleDataAccess.DeleteByJobPostingId(int JobPostingId)
        {
            if (JobPostingId < 1)
            {
                throw new ArgumentNullException("JobPostingId");
            }

            const string SP = "dbo.SearchAgentSchedule_DeleteByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a search agent schedule which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this search agent schedule.");
                        }
                }
            }
        }

        IList<SearchAgentSchedule> ISearchAgentScheduleDataAccess.GetAll()
        {
            const string SP = "dbo.SearchAgentSchedule_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<SearchAgentSchedule>().BuildEntities(reader);
                }
            }
        }
        #endregion
    }
}
