﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberObjectiveAndSummaryBuilder : IEntityBuilder<MemberObjectiveAndSummary>
    {
        IList<MemberObjectiveAndSummary> IEntityBuilder<MemberObjectiveAndSummary>.BuildEntities(IDataReader reader)
        {
            List<MemberObjectiveAndSummary> memberObjectiveAndSummarys = new List<MemberObjectiveAndSummary>();

            while (reader.Read())
            {
                memberObjectiveAndSummarys.Add(((IEntityBuilder<MemberObjectiveAndSummary>)this).BuildEntity(reader));
            }

            return (memberObjectiveAndSummarys.Count > 0) ? memberObjectiveAndSummarys : null;
        }

        MemberObjectiveAndSummary IEntityBuilder<MemberObjectiveAndSummary>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_OBJECTIVE = 1;
            const int FLD_SUMMARY = 2;
            const int FLD_COPYPASTERESUME = 3;
            const int FLD_RAWCOPYPASTERESUME = 4;
            const int FLD_MEMBERID = 5;
            const int FLD_ISREMOVED = 6;
            const int FLD_CREATORID = 7;
            const int FLD_UPDATORID = 8;
            const int FLD_CREATEDATE = 9;
            const int FLD_UPDATEDATE = 10;

            MemberObjectiveAndSummary memberObjectiveAndSummary = new MemberObjectiveAndSummary();

            memberObjectiveAndSummary.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberObjectiveAndSummary.Objective = reader.IsDBNull(FLD_OBJECTIVE) ? string.Empty : reader.GetString(FLD_OBJECTIVE);
            memberObjectiveAndSummary.Summary = reader.IsDBNull(FLD_SUMMARY) ? string.Empty : reader.GetString(FLD_SUMMARY);
            memberObjectiveAndSummary.CopyPasteResume = reader.IsDBNull(FLD_COPYPASTERESUME) ? string.Empty : reader.GetString(FLD_COPYPASTERESUME);
            memberObjectiveAndSummary.RawCopyPasteResume = reader.IsDBNull(FLD_RAWCOPYPASTERESUME) ? string.Empty : reader.GetString(FLD_RAWCOPYPASTERESUME);
            memberObjectiveAndSummary.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberObjectiveAndSummary.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberObjectiveAndSummary.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberObjectiveAndSummary.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberObjectiveAndSummary.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberObjectiveAndSummary.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberObjectiveAndSummary;
        }
    }
}