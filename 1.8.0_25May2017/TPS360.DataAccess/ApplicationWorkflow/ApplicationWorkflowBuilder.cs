﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class ApplicationWorkflowBuilder : IEntityBuilder<ApplicationWorkflow>
    {
        IList<ApplicationWorkflow> IEntityBuilder<ApplicationWorkflow>.BuildEntities(IDataReader reader)
        {
            List<ApplicationWorkflow> applicationWorkflows = new List<ApplicationWorkflow>();

            while (reader.Read())
            {
                applicationWorkflows.Add(((IEntityBuilder<ApplicationWorkflow>)this).BuildEntity(reader));
            }

            return (applicationWorkflows.Count > 0) ? applicationWorkflows : null;
        }

        ApplicationWorkflow IEntityBuilder<ApplicationWorkflow>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_TITLE = 1;
            const int FLD_DESCRIPTION = 2;
            const int FLD_REQUIREAPPROVAL = 3;
            const int FLD_TIMEFRAME = 4;
            const int FLD_ISEMAILALERT = 5;
            const int FLD_ISSMSALERT = 6;
            const int FLD_ISDASHBOARDALERT = 7;

            ApplicationWorkflow applicationWorkflow = new ApplicationWorkflow();

            applicationWorkflow.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            applicationWorkflow.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString(FLD_TITLE);
            applicationWorkflow.Description = reader.IsDBNull(FLD_DESCRIPTION) ? string.Empty : reader.GetString(FLD_DESCRIPTION);
            applicationWorkflow.RequireApproval = reader.IsDBNull(FLD_REQUIREAPPROVAL) ? false : reader.GetBoolean(FLD_REQUIREAPPROVAL);
            applicationWorkflow.TimeFrame = reader.IsDBNull(FLD_TIMEFRAME) ? string.Empty : reader.GetString(FLD_TIMEFRAME);
            applicationWorkflow.IsEmailAlert = reader.IsDBNull(FLD_ISEMAILALERT) ? false : reader.GetBoolean(FLD_ISEMAILALERT);
            applicationWorkflow.IsSmsAlert = reader.IsDBNull(FLD_ISSMSALERT) ? false : reader.GetBoolean(FLD_ISSMSALERT);
            applicationWorkflow.IsDashboardAlert = reader.IsDBNull(FLD_ISDASHBOARDALERT) ? false : reader.GetBoolean(FLD_ISDASHBOARDALERT);

            return applicationWorkflow;
        }
    }
}