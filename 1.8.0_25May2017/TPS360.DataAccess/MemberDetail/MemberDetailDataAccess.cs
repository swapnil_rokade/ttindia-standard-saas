﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberDetailDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author                  MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              April-4-2009           Nagarathna V.B          Defect ID: 9545; created new method "GetSSNCount" 
   ---------------------------------------------------------------------------------------------------------------------------------------------
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberDetailDataAccess : BaseDataAccess, IMemberDetailDataAccess
    {
        #region Constructors

        public MemberDetailDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberDetail> CreateEntityBuilder<MemberDetail>()
        {
            return (new MemberDetailBuilder()) as IEntityBuilder<MemberDetail>;
        }

        #endregion

        #region  Methods

        MemberDetail IMemberDetailDataAccess.Add(MemberDetail memberDetail)
        {
            const string SP = "dbo.MemberDetail_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);

                Database.AddInParameter(cmd, "@IsCurrentSameAsPrimaryAddress", DbType.Boolean, memberDetail.IsCurrentSameAsPrimaryAddress);
                Database.AddInParameter(cmd, "@CurrentAddressLine1", DbType.AnsiString, StringHelper.Convert(memberDetail.CurrentAddressLine1));
                Database.AddInParameter(cmd, "@CurrentAddressLine2", DbType.AnsiString, StringHelper.Convert(memberDetail.CurrentAddressLine2));
                Database.AddInParameter(cmd, "@CurrentCity", DbType.AnsiString, StringHelper.Convert(memberDetail.CurrentCity));
                Database.AddInParameter(cmd, "@CurrentStateId", DbType.Int32, memberDetail.CurrentStateId);
                Database.AddInParameter(cmd, "@CurrentZip", DbType.AnsiString, StringHelper.Convert(memberDetail.CurrentZip));
                Database.AddInParameter(cmd, "@CurrentCountryId", DbType.Int32, memberDetail.CurrentCountryId);
                Database.AddInParameter(cmd, "@HomePhone", DbType.AnsiString, StringHelper.Convert(memberDetail.HomePhone));
                Database.AddInParameter(cmd, "@OfficePhone", DbType.AnsiString, StringHelper.Convert(memberDetail.OfficePhone));
                Database.AddInParameter(cmd, "@OfficePhoneExtension", DbType.AnsiString, StringHelper.Convert(memberDetail.OfficePhoneExtension));
                Database.AddInParameter(cmd, "@Fax", DbType.AnsiString, StringHelper.Convert(memberDetail.Fax));
                Database.AddInParameter(cmd, "@CityOfBirth", DbType.AnsiString, StringHelper.Convert(memberDetail.CityOfBirth));
                Database.AddInParameter(cmd, "@ProvinceOfBirth", DbType.AnsiString, StringHelper.Convert(memberDetail.ProvinceOfBirth));
                Database.AddInParameter(cmd, "@StateIdOfBirth", DbType.AnsiString, StringHelper.Convert(memberDetail.StateIdOfBirth));
                Database.AddInParameter(cmd, "@CountryIdOfBirth", DbType.AnsiString, StringHelper.Convert(memberDetail.CountryIdOfBirth));
                Database.AddInParameter(cmd, "@CountryIdOfCitizenship", DbType.AnsiString, StringHelper.Convert(memberDetail.CountryIdOfCitizenship));
                Database.AddInParameter(cmd, "@SSNPAN", DbType.AnsiString, StringHelper.Convert(memberDetail.SSNPAN));
                Database.AddInParameter(cmd, "@BirthMark", DbType.AnsiString, StringHelper.Convert(memberDetail.BirthMark));
                Database.AddInParameter(cmd, "@GenderLookupId", DbType.Int32, StringHelper.Convert(memberDetail.GenderLookupId));
                Database.AddInParameter(cmd, "@EthnicGroupLookupId", DbType.Int32, memberDetail.EthnicGroupLookupId);
                Database.AddInParameter(cmd, "@Photo", DbType.AnsiString, StringHelper.Convert(memberDetail.Photo));
                Database.AddInParameter(cmd, "@BloodGroupLookupId", DbType.Int32, memberDetail.BloodGroupLookupId);
                Database.AddInParameter(cmd, "@Height", DbType.AnsiString, StringHelper.Convert(memberDetail.Height));
                Database.AddInParameter(cmd, "@Weight", DbType.AnsiString, StringHelper.Convert(memberDetail.Weight));
                Database.AddInParameter(cmd, "@EmergencyContactPerson", DbType.AnsiString, StringHelper.Convert(memberDetail.EmergencyContactPerson));
                Database.AddInParameter(cmd, "@EmergencyContactNumber", DbType.AnsiString, StringHelper.Convert(memberDetail.EmergencyContactNumber));
                Database.AddInParameter(cmd, "@EmergencyContactRelation", DbType.AnsiString, StringHelper.Convert(memberDetail.EmergencyContactRelation));
                Database.AddInParameter(cmd, "@FatherName", DbType.AnsiString, StringHelper.Convert(memberDetail.FatherName));
                Database.AddInParameter(cmd, "@MotherName", DbType.AnsiString, StringHelper.Convert(memberDetail.MotherName));
                Database.AddInParameter(cmd, "@MaritalStatusLookupId", DbType.Int32, memberDetail.MaritalStatusLookupId);
                Database.AddInParameter(cmd, "@SpouseName", DbType.AnsiString, StringHelper.Convert(memberDetail.SpouseName));
                //Database.AddInParameter(cmd, "@AnniversaryDate", DbType.DateTime, memberDetail.AnniversaryDate);
                if (memberDetail.AnniversaryDate > DateTime.MinValue)
                {
                    Database.AddInParameter(cmd, "@AnniversaryDate", DbType.DateTime, NullConverter.Convert(memberDetail.AnniversaryDate));
                }
                Database.AddInParameter(cmd, "@NumberOfChildren", DbType.Int32, memberDetail.NumberOfChildren);
                Database.AddInParameter(cmd, "@DisabilityInformation", DbType.AnsiString, StringHelper.Convert(memberDetail.DisabilityInformation));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberDetail.IsRemoved);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberDetail.MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberDetail.CreatorId);

                Database.AddInParameter(cmd, "@CandidateIndustryLookupID", DbType.Int32, StringHelper.Convert(memberDetail.CandidateIndustryLookupID));
                Database.AddInParameter(cmd, "@JobFunctionLookupID", DbType.Int32, StringHelper.Convert(memberDetail.JobFunctionLookupID));
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberDetail = CreateEntityBuilder<MemberDetail>().BuildEntity(reader);
                    }
                    else
                    {
                        memberDetail = null;
                    }
                }

                if (memberDetail == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member detail already exists. Please specify another member detail.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member detail.");
                            }
                    }
                }

                return memberDetail;
            }
        }

        MemberDetail IMemberDetailDataAccess.Update(MemberDetail memberDetail)
        {
            const string SP = "dbo.MemberDetail_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberDetail.Id);
                Database.AddInParameter(cmd, "@IsCurrentSameAsPrimaryAddress", DbType.Boolean, memberDetail.IsCurrentSameAsPrimaryAddress);
                Database.AddInParameter(cmd, "@CurrentAddressLine1", DbType.AnsiString, StringHelper.Convert(memberDetail.CurrentAddressLine1));
                Database.AddInParameter(cmd, "@CurrentAddressLine2", DbType.AnsiString, StringHelper.Convert(memberDetail.CurrentAddressLine2));
                Database.AddInParameter(cmd, "@CurrentCity", DbType.AnsiString, StringHelper.Convert(memberDetail.CurrentCity));
                Database.AddInParameter(cmd, "@CurrentStateId", DbType.Int32, memberDetail.CurrentStateId);
                Database.AddInParameter(cmd, "@CurrentZip", DbType.AnsiString, StringHelper.Convert(memberDetail.CurrentZip));
                Database.AddInParameter(cmd, "@CurrentCountryId", DbType.Int32, memberDetail.CurrentCountryId);
                Database.AddInParameter(cmd, "@HomePhone", DbType.AnsiString, StringHelper.Convert(memberDetail.HomePhone));
                Database.AddInParameter(cmd, "@OfficePhone", DbType.AnsiString, StringHelper.Convert(memberDetail.OfficePhone));
                Database.AddInParameter(cmd, "@OfficePhoneExtension", DbType.AnsiString, StringHelper.Convert(memberDetail.OfficePhoneExtension));
                Database.AddInParameter(cmd, "@Fax", DbType.AnsiString, StringHelper.Convert(memberDetail.Fax));
                Database.AddInParameter(cmd, "@CityOfBirth", DbType.AnsiString, StringHelper.Convert(memberDetail.CityOfBirth));
                Database.AddInParameter(cmd, "@ProvinceOfBirth", DbType.AnsiString, StringHelper.Convert(memberDetail.ProvinceOfBirth));
                Database.AddInParameter(cmd, "@StateIdOfBirth", DbType.AnsiString, StringHelper.Convert(memberDetail.StateIdOfBirth));
                Database.AddInParameter(cmd, "@CountryIdOfBirth", DbType.AnsiString, StringHelper.Convert(memberDetail.CountryIdOfBirth));
                Database.AddInParameter(cmd, "@CountryIdOfCitizenship", DbType.AnsiString, StringHelper.Convert(memberDetail.CountryIdOfCitizenship));
                Database.AddInParameter(cmd, "@SSNPAN", DbType.AnsiString, StringHelper.Convert(memberDetail.SSNPAN));
                Database.AddInParameter(cmd, "@BirthMark", DbType.AnsiString, StringHelper.Convert(memberDetail.BirthMark));
                Database.AddInParameter(cmd, "@GenderLookupId", DbType.Int32, memberDetail.GenderLookupId);
                Database.AddInParameter(cmd, "@EthnicGroupLookupId", DbType.Int32, memberDetail.EthnicGroupLookupId);
                Database.AddInParameter(cmd, "@Photo", DbType.AnsiString, StringHelper.Convert(memberDetail.Photo));
                Database.AddInParameter(cmd, "@BloodGroupLookupId", DbType.Int32, memberDetail.BloodGroupLookupId);
                Database.AddInParameter(cmd, "@Height", DbType.AnsiString, StringHelper.Convert(memberDetail.Height));
                Database.AddInParameter(cmd, "@Weight", DbType.AnsiString, StringHelper.Convert(memberDetail.Weight));
                Database.AddInParameter(cmd, "@EmergencyContactPerson", DbType.AnsiString, StringHelper.Convert(memberDetail.EmergencyContactPerson));
                Database.AddInParameter(cmd, "@EmergencyContactNumber", DbType.AnsiString, StringHelper.Convert(memberDetail.EmergencyContactNumber));
                Database.AddInParameter(cmd, "@EmergencyContactRelation", DbType.AnsiString, StringHelper.Convert(memberDetail.EmergencyContactRelation));
                Database.AddInParameter(cmd, "@FatherName", DbType.AnsiString, StringHelper.Convert(memberDetail.FatherName));
                Database.AddInParameter(cmd, "@MotherName", DbType.AnsiString, StringHelper.Convert(memberDetail.MotherName));
                Database.AddInParameter(cmd, "@MaritalStatusLookupId", DbType.Int32, memberDetail.MaritalStatusLookupId);
                Database.AddInParameter(cmd, "@SpouseName", DbType.AnsiString, StringHelper.Convert(memberDetail.SpouseName));
                Database.AddInParameter(cmd, "@AnniversaryDate", DbType.DateTime, NullConverter.Convert(memberDetail.AnniversaryDate));
                Database.AddInParameter(cmd, "@NumberOfChildren", DbType.Int32, memberDetail.NumberOfChildren);
                Database.AddInParameter(cmd, "@DisabilityInformation", DbType.AnsiString, StringHelper.Convert(memberDetail.DisabilityInformation));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberDetail.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberDetail.UpdatorId);

                
                Database.AddInParameter(cmd, "@CandidateIndustryLookupID", DbType.Int32, memberDetail.CandidateIndustryLookupID);
                Database.AddInParameter(cmd, "@JobFunctionLookupID", DbType.Int32, memberDetail.JobFunctionLookupID);
               
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberDetail = CreateEntityBuilder<MemberDetail>().BuildEntity(reader);
                    }
                    else
                    {
                        memberDetail = null;
                    }
                }

                if (memberDetail == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member detail already exists. Please specify another member detail.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member detail.");
                            }
                    }
                }

                return memberDetail;
            }
        }

        MemberDetail IMemberDetailDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberDetail_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberDetail>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        //0.1 starts
        int IMemberDetailDataAccess.GetSSNCount(string strSSN,int MemberId)
        {
            if (strSSN =="")
            {
                throw new ArgumentNullException("strSSN");
            }

            const string SP = "dbo.MemberSSN_GetCount";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                
                Database.AddInParameter(cmd, "@SSNPAN", DbType.Int32, strSSN);
                Database.AddInParameter(cmd, "@MId", DbType.Int32, MemberId);
                Database.AddOutParameter(cmd, "@Count", DbType.Int32, 4);
                Database.ExecuteScalar(cmd);
                int isExist = GetCountFromParameter(cmd);
                 switch (isExist)
                 {
                     case SqlConstants.DB_STATUS_CODE_NO_DUPLICATE_DATA:
                         {
                             return isExist;
                         }
                     case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                         {
                             throw new ArgumentException("SSN is already exist.Please specify another SSN");
                         }
                     default:
                         {
                             throw new SystemException("An unexpected error has occurred while updating this member detail.");
                         }
                 }
            }
        }
        //0.1 ends

        MemberDetail IMemberDetailDataAccess.GetByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberDetail_GetByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberDetail>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberDetail> IMemberDetailDataAccess.GetAll()
        {
            const string SP = "dbo.MemberDetail_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberDetail>().BuildEntities(reader);
                }
            }
        }

        bool IMemberDetailDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberDetail_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member detail which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member detail.");
                        }
                }
            }
        }
        bool IMemberDetailDataAccess.DeleteByMembertId(int MemberId)
        {
            if (MemberId < 1)
            {
                throw new ArgumentNullException("MemberId");
            }

            const string SP = "dbo.MemberDetail_DeleteMemberDetailByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member detail which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member detail.");
                        }
                }
            }
        }

        bool IMemberDetailDataAccess.BuildMemberManager_ByMemberId(int MemberId, int CreatorId)
        {
            if (MemberId < 1)
            {
                throw new ArgumentNullException("MemberId");
            }

            const string SP = "dbo.BuildMemberManager_ByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, CreatorId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member detail which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member detail.");
                        }
                }
            }
        }


        #endregion
    }
}