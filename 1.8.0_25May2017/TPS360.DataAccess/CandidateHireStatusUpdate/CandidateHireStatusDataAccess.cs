﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;


namespace TPS360.DataAccess
{
    internal sealed class CandidateHireStatusDataAccess : BaseDataAccess, ICandidateHireStatusDataAccess
    {
        #region Constructors

        public CandidateHireStatusDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CandidatesHireStatus> CreateEntityBuilder<CandidatesHireStatus>()
        {
            return (new CandidatesHireStautsBuilder()) as IEntityBuilder<CandidatesHireStatus>;
        }

        #endregion

        IList<CandidatesHireStatus> ICandidateHireStatusDataAccess.GetCandidateListbyManagerId(int managerId)
        {
            if (managerId < 1)
            {
                throw new ArgumentNullException("managerId");
            }

            const string SP = "dbo.CandidateHiringStatusUpdate_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, managerId))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CandidatesHireStatus>().BuildEntities(reader);
                }
            }
        }

        IList<HiringMatrixLevels> ICandidateHireStatusDataAccess.GetAllHiringManager(int MemberID)
        {
            const string SP = "dbo.GetHireMatrixLevel_HM";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP,MemberID))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<HiringMatrixLevels>().BuildEntities(reader);
                }
            }
        }

        IList<CandidatesHireStatus> ICandidateHireStatusDataAccess.GetAllHiringStatusRoleId(int RoleId)
        {
            const string SP = "dbo.GetAllHiringStatusRole";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, RoleId))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CandidatesHireStatus>().BuildEntities(reader);
                }
            }
        }

        void ICandidateHireStatusDataAccess.GetUpdateCandidateStatus(int ID, int jobstatus)
        {
            if (ID < 1 && jobstatus < 1)
            {
                throw new ArgumentNullException("ID");
            }

            const string SP = "dbo.CandidateHiringStatusbyManager_Update";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, ID);
                Database.AddInParameter(cmd, "@jobstatus", DbType.Int32, jobstatus);
                Database.ExecuteNonQuery(cmd);
            }
        }

        void ICandidateHireStatusDataAccess.GetCandidHiringStatusRoleSave(int Roleid, int StatusID, int creatorId, bool IsRemovedFlag)
        {
            const string SP = "dbo.CandideHiringStatusRole_Save";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RoleId", DbType.Int32, Roleid);
                Database.AddInParameter(cmd, "@StatusID", DbType.Int32, StatusID);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, creatorId);
                Database.AddInParameter(cmd, "@IsRemovedFlag", DbType.Boolean, IsRemovedFlag);
                Database.ExecuteNonQuery(cmd);
            }
        }

        void ICandidateHireStatusDataAccess.GetSendCandidtaeStatusEmail(int ID, int CandidateId, int RequsitionStatus, int ManagerID, int JobPostingId, int AdminMemberid)
        {
            if (ID < 1 && CandidateId < 1 && RequsitionStatus < 1 && ManagerID > 1)
            {
                throw new ArgumentNullException("ID");
            }

            const string SP = "dbo.SendCadidateHireStatusEmail";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, ID);
                Database.AddInParameter(cmd, "@CandidateId", DbType.Int32, CandidateId);
                Database.AddInParameter(cmd, "@RequsitionStatus", DbType.Int32, RequsitionStatus);
                Database.AddInParameter(cmd, "@ManagerID", DbType.Int32, ManagerID);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);
                Database.AddInParameter(cmd, "@AdminMemberid", DbType.Int32, AdminMemberid);
                Database.ExecuteNonQuery(cmd);
            }
        }

        IList<CustomRole> ICandidateHireStatusDataAccess.GetCustomUserRoleAll()
        {
            const string SP = "dbo.CandidateStatusHiringManagerRole_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CustomRole>().BuildEntities(reader);
                }
            }
        }

    }
}
