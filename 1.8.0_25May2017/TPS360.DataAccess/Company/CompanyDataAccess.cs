﻿/*-------------------------------------------------------------------------------------------------------------   
  FileName: CompanyDataAccess.cs
   Description: 
   Created By: 
   Created On:
   Modification Log:
   ------------------------------------------------------------------------------------------------------------  
 * Ver.No.             Date                  Author              Modification
   ------------------------------------------------------------------------------------------------------------  
 * 0.1             17-Feb-2009             Sandeesh            Defect Fix - ID: 9964 ; Implemented sorting.
 * 0.2              24-Feb-2009            Sandeesh            Defect Fix - ID: 9964 ; Implemented sorting.
 * 0.3              8-Mar-2010             Nagarathna V.B        Enhancement Id:12129:submission report.GetPrimaryContact();
 * 0.4              8/June/2016            pravin khot         added GetAllClientsByStatusByCandidateId
 * ----------------------------------------------------------------------------------------------------------------
*/

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class CompanyDataAccess : BaseDataAccess, ICompanyDataAccess
    {
        #region Constructors

        public CompanyDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<Company> CreateEntityBuilder<Company>()
        {
            return (new CompanyBuilder()) as IEntityBuilder<Company>;
        }

        #endregion

        #region  Methods

        Company ICompanyDataAccess.Add(Company company)
        {
            const string SP = "dbo.Company_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyName", DbType.AnsiString, StringHelper.Convert(company.CompanyName));
                Database.AddInParameter(cmd, "@Address1", DbType.AnsiString, StringHelper.Convert(company.Address1));
                Database.AddInParameter(cmd, "@Address2", DbType.AnsiString, StringHelper.Convert(company.Address2));
                Database.AddInParameter(cmd, "@City", DbType.AnsiString, StringHelper.Convert(company.City));
                Database.AddInParameter(cmd, "@StateId", DbType.Int32, company.StateId);
                Database.AddInParameter(cmd, "@Zip", DbType.AnsiString, StringHelper.Convert(company.Zip));
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, company.CountryId);
                Database.AddInParameter(cmd, "@WebAddress", DbType.AnsiString, StringHelper.Convert(company.WebAddress));
                Database.AddInParameter(cmd, "@CompanyLogo", DbType.AnsiString, StringHelper.Convert(company.CompanyLogo));
                Database.AddInParameter(cmd, "@CompanyInformation", DbType.AnsiString, StringHelper.Convert(company.CompanyInformation));
                Database.AddInParameter(cmd, "@IndustryType", DbType.Int32, company.IndustryType);
                Database.AddInParameter(cmd, "@CompanySize", DbType.Int32, company.CompanySize);
                Database.AddInParameter(cmd, "@OwnerType", DbType.Int32, company.OwnerType);
                Database.AddInParameter(cmd, "@TickerSymbol", DbType.AnsiString, StringHelper.Convert(company.TickerSymbol));
                Database.AddInParameter(cmd, "@OfficePhone", DbType.AnsiString, StringHelper.Convert(company.OfficePhone));
                Database.AddInParameter(cmd, "@OfficePhoneExtension", DbType.AnsiString, StringHelper.Convert(company.OfficePhoneExtension));
                Database.AddInParameter(cmd, "@TollFreePhone", DbType.AnsiString, StringHelper.Convert(company.TollFreePhone));
                Database.AddInParameter(cmd, "@TollFreePhoneExtension", DbType.AnsiString, StringHelper.Convert(company.TollFreePhoneExtension));
                Database.AddInParameter(cmd, "@FaxNumber", DbType.AnsiString, StringHelper.Convert(company.FaxNumber));
                Database.AddInParameter(cmd, "@PrimaryEmail", DbType.AnsiString, StringHelper.Convert(company.PrimaryEmail));
                Database.AddInParameter(cmd, "@SecondaryEmail", DbType.AnsiString, StringHelper.Convert(company.SecondaryEmail));
                Database.AddInParameter(cmd, "@EINNumber", DbType.AnsiString, StringHelper.Convert(company.EINNumber));
                Database.AddInParameter(cmd, "@AnnualRevenue", DbType.AnsiString, StringHelper.Convert(company.AnnualRevenue));
                Database.AddInParameter(cmd, "@NumberOfEmployee", DbType.Int32, company.NumberOfEmployee);
                Database.AddInParameter(cmd, "@CompanyType", DbType.AnsiString, StringHelper.Convert(company.CompanyType));
                Database.AddInParameter(cmd, "@CompanyStatus", DbType.Int32 , (int)company .CompanyStatus );
                Database.AddInParameter(cmd, "@IsEndClient", DbType.Boolean, company.IsEndClient);
                Database.AddInParameter(cmd, "@IsTier1Client", DbType.Boolean, company.IsTier1Client);
                Database.AddInParameter(cmd, "@IsTier2Client", DbType.Boolean, company.IsTier2Client);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, company.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, company.CreatorId);
                Database.AddInParameter(cmd, "@IsVolumeHireClient", DbType.Boolean, company.IsVolumeHireClient);
                Database.AddInParameter(cmd, "@ContractStartDate", DbType.Date, NullConverter.Convert(company.ContractStartDate));
                Database.AddInParameter(cmd, "@ContractEndDate", DbType.Date, NullConverter.Convert(company.ContractEndDate));
                Database.AddInParameter(cmd, "@ServiceFee", DbType.Decimal, company.ServiceFee);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        company = CreateEntityBuilder<Company>().BuildEntity(reader);
                    }
                    else
                    {
                        company = null;
                    }
                }

                if (company == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Company already exists. Please specify another company.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this company.");
                            }
                    }
                }

                return company;
            }
        }

        Company ICompanyDataAccess.Update(Company company)
        {
            const string SP = "dbo.Company_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, company.Id);
                Database.AddInParameter(cmd, "@CompanyName", DbType.AnsiString, company.CompanyName);
                Database.AddInParameter(cmd, "@Address1", DbType.AnsiString, StringHelper.Convert(company.Address1));
                Database.AddInParameter(cmd, "@Address2", DbType.AnsiString, StringHelper.Convert(company.Address2));
                Database.AddInParameter(cmd, "@City", DbType.AnsiString, company.City);
                Database.AddInParameter(cmd, "@StateId", DbType.Int32, company.StateId);
                Database.AddInParameter(cmd, "@Zip", DbType.AnsiString, company.Zip);
                Database.AddInParameter(cmd, "@CountryId", DbType.Int32, company.CountryId);
                Database.AddInParameter(cmd, "@WebAddress", DbType.AnsiString, company.WebAddress);
                Database.AddInParameter(cmd, "@CompanyLogo", DbType.AnsiString, company.CompanyLogo);
                Database.AddInParameter(cmd, "@CompanyInformation", DbType.AnsiString, company.CompanyInformation);
                Database.AddInParameter(cmd, "@IndustryType", DbType.Int32, company.IndustryType);
                Database.AddInParameter(cmd, "@CompanySize", DbType.Int32, company.CompanySize);
                Database.AddInParameter(cmd, "@OwnerType", DbType.Int32, company.OwnerType);
                Database.AddInParameter(cmd, "@TickerSymbol", DbType.AnsiString, company.TickerSymbol);
                Database.AddInParameter(cmd, "@OfficePhone", DbType.AnsiString, company.OfficePhone);
                Database.AddInParameter(cmd, "@OfficePhoneExtension", DbType.AnsiString, company.OfficePhoneExtension);
                Database.AddInParameter(cmd, "@TollFreePhone", DbType.AnsiString, company.TollFreePhone);
                Database.AddInParameter(cmd, "@TollFreePhoneExtension", DbType.AnsiString, company.TollFreePhoneExtension);
                Database.AddInParameter(cmd, "@FaxNumber", DbType.AnsiString, company.FaxNumber);
                Database.AddInParameter(cmd, "@PrimaryEmail", DbType.AnsiString, company.PrimaryEmail);
                Database.AddInParameter(cmd, "@SecondaryEmail", DbType.AnsiString, company.SecondaryEmail);
                Database.AddInParameter(cmd, "@EINNumber", DbType.AnsiString, company.EINNumber);
                Database.AddInParameter(cmd, "@AnnualRevenue", DbType.AnsiString, company.AnnualRevenue);
                Database.AddInParameter(cmd, "@NumberOfEmployee", DbType.Int32, company.NumberOfEmployee);
                Database.AddInParameter(cmd, "@CompanyType", DbType.AnsiString, company.CompanyType);
                Database.AddInParameter(cmd, "@CompanyStatus", DbType.Int32, company.CompanyStatus);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, company.IsRemoved);
                Database.AddInParameter(cmd, "@IsEndClient", DbType.Boolean, company.IsEndClient);
                Database.AddInParameter(cmd, "@IsTier1Client", DbType.Boolean, company.IsTier1Client);
                Database.AddInParameter(cmd, "@IsTier2Client", DbType.Boolean, company.IsTier2Client);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, company.UpdatorId);
                Database.AddInParameter(cmd, "@IsVolumeHireClient", DbType.Boolean, company.IsVolumeHireClient);
                Database.AddInParameter(cmd, "@ContractStartDate", DbType.Date, NullConverter.Convert(company.ContractStartDate));
                Database.AddInParameter(cmd, "@ContractEndDate", DbType.Date, NullConverter.Convert(company.ContractEndDate));
                Database.AddInParameter(cmd, "@ServiceFee", DbType.Decimal, company.ServiceFee);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        company = CreateEntityBuilder<Company>().BuildEntity(reader);
                    }
                    else
                    {
                        company = null;
                    }
                }

                if (company == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Company already exists. Please specify another company.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this company.");
                            }
                    }
                }

                return company;
            }
        }

        bool ICompanyDataAccess.UpdateStatus(int companyId, CompanyStatus status)
        {
            const string SP = "dbo.Company_UpdateStatus";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);
                Database.AddInParameter(cmd, "@CompanyStatus", DbType.Int32, status);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_SUCCESS_UPDATE:
                        {
                            return true;

                        }
                        default:
                        {
                            throw new SystemException("An unexpected error has occurred while updating this company status.");
                        }
                    }
                }
            }
        }

        bool ICompanyDataAccess.ChangePendingStatus(int companyId, bool IsPending)
        {
            const string SP = "dbo.Company_ChangePendingStatus";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);
                Database.AddInParameter(cmd, "@IsPending", DbType.Int32, IsPending);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_SUCCESS_UPDATE:
                            {
                                return true;

                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this company.");
                            }
                    }
                }
            }
        }

        Company ICompanyDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Company_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Company>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<Company> ICompanyDataAccess.GetAll()
        {
            const string SP = "dbo.Company_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Company>().BuildEntities(reader);
                }
            }
        }
         
        CompanyOverviewDetails ICompanyDataAccess.GetCompanyOverviewDetails(int CompanyId)
        {
            const string SP = "dbo.Company_GetCompanyDetails";
            CompanyOverviewDetails overviewdetails = new CompanyOverviewDetails(); ;
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, CompanyId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {

                        overviewdetails.CompanyId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        overviewdetails.PrimaryManagerId = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                        overviewdetails.PrimaryManager = reader.IsDBNull(2) ? string.Empty : reader.GetString(2);
                        overviewdetails.RequisitionsCount = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                        overviewdetails.ContactsCount = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                        overviewdetails.DocumentsCount = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                        return overviewdetails;
                    }
                    else
                        return null;
                }
            }
        }
       

        IList<Company> ICompanyDataAccess.GetAllPendingClient()
        {
            const string SP = "dbo.Company_GetAllPendingClient";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<Company>().BuildEntities(reader);
                }
            }
        }
        int ICompanyDataAccess.GetCompanyCount()
        {
            const string SP = "dbo.Company_GetCompanyCount";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
               

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                }

                return 0;
            }
        }
        IList<Company> ICompanyDataAccess.GetPaged(string preText, int Count)
        {
            const string SP = "dbo.Company_GetPaged";
            string whereClause = string.Empty;
            whereClause = "[C].[CompanyName] like '" + preText + "%'";
            object[] paramValues = new object[] {	0,
													8,
													StringHelper.Convert(whereClause),
													"[C].[CompanyName]",
                                                    "ASC"
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                IList<Company> response = new List<Company>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response = CreateEntityBuilder<Company>().BuildEntities(reader);

                   
                }

                return response;
            }


        }
        PagedResponse<Company> ICompanyDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.Company_GetPaged";
            string whereClause = string.Empty;
           
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                string fromCreateDate = null;
                string toCreateDate = null;

                string fromUpdateDate = null;
                string toUpdateDate = null;
                
                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "fromcreatedate"))
                        {
                            fromCreateDate = value;
                            continue;
                        }
                        else if (StringHelper.IsEqual(column, "tocreatedate"))
                        {
                            toCreateDate = value;
                            continue;
                        }
                        else if (StringHelper.IsEqual(column, "fromupdatedate"))
                        {
                            fromUpdateDate = value;
                            continue;
                        }
                        else if (StringHelper.IsEqual(column, "toupdatedate"))
                        {
                            toUpdateDate = value;
                            continue;
                        }
                        else if (StringHelper.IsEqual(column, "endclient"))
                        {
                            sb.Append("[C].[IsEndClient]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else if (StringHelper.IsEqual(column, "tier1client"))
                        {
                            sb.Append("[C].[IsTier1Client]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else if (StringHelper.IsEqual(column, "tier2client"))
                        {
                            sb.Append("[C].[IsTier2Client]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else if (StringHelper.IsEqual(column, "industrytype"))
                        {
                            sb.Append("[C].[IndustryType]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else if (StringHelper.IsEqual(column, "country"))
                        {
                            if (value != "0")
                            {
                                sb.Append("([C].[CountryId]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                            else
                            {
                                sb.Append(" Or ");
                                sb.Append("[C].[CountryId]");
                                sb.Append(" = ");
                                sb.Append("0");
                            }
                            sb.Append(")");
                        }
                        else if (StringHelper.IsEqual(column, "companystatus"))
                        {
                            sb.Append("[C].[CompanyStatus]");
                            sb.Append(" IN ");
                            sb.Append("(");
                            sb.Append(value);
                            sb.Append(")");
                        }
                        else if (StringHelper.IsEqual(column, "state"))
                        {
                            sb.Append("[C].[StateId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else if (StringHelper.IsEqual(column, "employeeid"))
                        {
                            string filter = " ( [C].[CreatorId] = {0} OR [C].[Id] in (select CompanyId from CompanyAssignedManager where MemberId={0}))";
                            sb.Append(string.Format(filter, value));
                        }
                        else if (StringHelper.IsEqual(column, "campaignid"))
                        {
                            string filter = "[C].[Id] IN (SELECT [CC].[CompanyId] FROM [CampaignCompany] AS [CC] WHERE [CC].[CampaignId] = {0})";
                            sb.Append(string.Format(filter, value));
                        }
                        else if (StringHelper.IsEqual(column, "ispending"))
                        {
                            sb.Append("[C].[IsPending]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else if (StringHelper.IsEqual(column, "pendingstatus"))
                        {
                            string filter = "[C].[Id] IN (SELECT [CSCR].[CompanyId] FROM [CompanyStatusChangeRequest] AS [CSCR] WHERE [CSCR].[RequestedStatus] = {0})";
                            sb.Append(string.Format(filter, value));
                        }
                        else if (StringHelper.IsEqual(column, "companycontact"))
                        {
                            //string filter = "[C].[Id] IN (SELECT [CPC].[CompanyId] FROM [CompanyContact] AS [CPC] WHERE [CPC].[FirstName] LIKE '%{0}%' OR [CPC].[FirstName] LIKE '%{1}%' OR [CPC].[LastName] LIKE '%{0}%' OR [CPC].[LastName] LIKE '%{1}%')";
                            //sb.Append(string.Format(filter, value));
                            sb.Append("[C].[Id] IN (SELECT [CPC].[CompanyId] FROM [CompanyContact] AS [CPC] WHERE ");
                            char[] delim = { ' ' };
                            string[] valueList = value.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                            if (valueList.Length == 2)
                            {
                                sb.Append(" [CPC].[FirstName]");
                                sb.Append(" like '%");
                                sb.Append(valueList[0]);
                                sb.Append("%' ");
                                sb.Append(" OR [CPC].[LastName]");
                                sb.Append(" like '%");
                                sb.Append(valueList[1]);
                                sb.Append("%') ");
                            }
                            
                            else
                            {
                                sb.Append(" [CPC].[FirstName]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                                sb.Append(" OR [CPC].[LastName]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%') ");
                            }

                        }
                        else if (StringHelper.IsEqual(column, "state"))
                        {
                            sb.Append("[S].[Name]");
                            sb.Append(" LIKE '");
                            if (!value.StartsWith("%"))
                            {
                                sb.Append("%");
                            }
                            sb.Append(value);
                            if (!value.EndsWith("%"))
                            {
                                sb.Append("%");
                            }
                            sb.Append("'");
                        }
                        else if (StringHelper.IsEqual(column, "CreatorId"))
                        {
                            if (sb.ToString() != string.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[CreatorId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else if (StringHelper.IsEqual(column, "quickSearchKeyWord"))
                        {
                            sb.Append("([C].[CompanyName] LIKE '%" + value + "%'");
                            sb.Append(" OR ");
                            sb.Append("[C].[PrimaryEmail] LIKE '%" + value + "%'");
                            sb.Append(" OR ");
                            sb.Append("[C].[SecondaryEmail] LIKE '%" + value + "%')");
                        }
                        else
                        {
                            sb.Append("[C].[");
                            sb.Append(column);
                            sb.Append("]");
                            sb.Append(" LIKE '");
                            if (!value.StartsWith("%"))
                            {
                                sb.Append("%");
                            }
                            sb.Append(value);
                            if (!value.EndsWith("%"))
                            {
                                sb.Append("%");
                            }
                            sb.Append("'");
                        }
                    }

                    sb.Append(" AND ");
                }

                if (!StringHelper.IsBlank(fromCreateDate) && !StringHelper.IsBlank(toCreateDate))
                {
                    sb.Append("[C].[CreateDate] >= ");
                    sb.Append("'");
                    sb.Append(fromCreateDate);
                    sb.Append("'");
                    sb.Append(" AND ");
                    sb.Append("[C].[CreateDate] < dateadd(dd,1,");
                    sb.Append("'");
                    sb.Append(toCreateDate);
                    sb.Append("')");
                    sb.Append(" AND ");
                }

                if (!StringHelper.IsBlank(fromUpdateDate) && !StringHelper.IsBlank(toUpdateDate))
                {
                    sb.Append("[C].[UpdateDate] >= ");
                    sb.Append("'");
                    sb.Append(fromUpdateDate);
                    sb.Append("'");
                    sb.Append(" AND ");
                    sb.Append("[C].[UpdateDate] < dateadd(dd,1,");
                    sb.Append("'");
                    sb.Append(toUpdateDate);
                    sb.Append("')");
                    sb.Append(" AND ");
                }

                //Remove the Last AND Clasue
                sb.Remove(sb.Length - 5, 5);
                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "CompanyName";
            }

            //0.2 Start - reverted the changes
            //0.1 Start

             //request.SortColumn = "[C].[" + request.SortColumn + "]"; 
            // request.SortColumn = "[" + request.SortColumn + "]";

            //0.1 End
            //0.2 End 

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Company> response = new PagedResponse<Company>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<Company>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        bool ICompanyDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Company_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a company which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company.");
                        }
                }
            }
        }

        bool ICompanyDataAccess.DeleteFromMemberById(int id, int memberId)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Company_DeleteFromMemberById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a company which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company.");
                        }
                }
            }
        }

        Hashtable ICompanyDataAccess.GetAllByEndClientType(int endClientType)
        {
            if (endClientType < 0)
            {
                throw new ArgumentException("EndClientType");
            }

            const string SP = "dbo.Company_GetAllByEndClientType";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@EndClientType", DbType.Int32, endClientType);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Hashtable allCompanyList = new Hashtable();

                    while (reader.Read())
                    {
                        allCompanyList.Add(reader.GetInt32(0), reader.GetString(1));
                    }

                    return allCompanyList;
                }
            }
        }

        ArrayList ICompanyDataAccess.GetAllClients()
        {
            const string SP = "dbo.Company_GetAllClients";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList clientList = new ArrayList();

                    while (reader.Read())
                    {
                        clientList.Add(new { Id = reader.GetInt32(0), Name = reader.GetString(1) });
                    }

                    return clientList;
                }
            }
        }

        ArrayList ICompanyDataAccess.GetAllClientsByStatus(int status)
        {
            const string SP = "Company_GetAllClientsByStatus";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Status", DbType.Int32, status);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList clientList = new ArrayList();

                    while (reader.Read())
                    {
                        clientList.Add(new Company () { Id = reader.GetInt32(0), CompanyName = reader.GetString(1) });
                    }

                    return clientList;
                }
            }
        }
        //************Added by pravin khot on 8/June/2016************
        ArrayList ICompanyDataAccess.GetAllClientsByStatusByCandidateId(int status, int candidateid)
        {
            const string SP = "Company_GetAllClientsByStatusByCandidateId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Status", DbType.Int32, status);
                Database.AddInParameter(cmd, "@candidateid", DbType.Int32, candidateid);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList clientList = new ArrayList();

                    while (reader.Read())
                    {
                        clientList.Add(new Company() { Id = reader.GetInt32(0), CompanyName = reader.GetString(1) });
                    }

                    return clientList;
                }
            }
        }
        //***********************END***************************

        ArrayList ICompanyDataAccess.GetAllCompanyList()
        {
            const string SP = "dbo.Company_GetAllClients";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList clientList = new ArrayList();

                    while (reader.Read())
                    {
                        clientList.Add(new Company () { Id = reader.GetInt32(0), CompanyName = reader.GetString(1) });
                    }

                    return clientList;
                }
            }
        }
        //12129
        ArrayList ICompanyDataAccess.GetPrimaryContact()
        {
            const string SP = "dbo.Company_GetPrimaryContact";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList clientList = new ArrayList();

                    while (reader.Read())
                    {
                        //clientList.Add(new { Id = reader.GetInt32(0), Email = reader.GetString(1) });
                        clientList.Add(new { ReceiverEmail = reader.GetString(0) });
                    }

                    return clientList;
                }
            }
        }

        ArrayList ICompanyDataAccess.GetAllNameByCompanyStatus(CompanyStatus companyStatus)
        {
            const string SP = "dbo.Company_GetAllNameByCompanyStatus";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyStatus", DbType.Int32, (int)companyStatus);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList clientList = new ArrayList();

                    while (reader.Read())
                    {
                        clientList.Add(new { Id = reader.GetInt32(0), Name = reader.GetString(1) });
                    }

                    return clientList;
                }
            }
        }

        ArrayList ICompanyDataAccess.GetAllVendorPartnerNames()
        {
            const string SP = "dbo.Company_GetAllVendorPartnerNames";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList clientList = new ArrayList();

                    while (reader.Read())
                    {
                        clientList.Add(new { Id = reader.GetInt32(0), Name = reader.GetString(1) });
                    }

                    return clientList;
                }
            }
        }

        Int32 ICompanyDataAccess.GetCompanyIdByName(string companyName)
        {
            if (string.IsNullOrEmpty(companyName))
            {
                throw new ArgumentNullException("companyName");
            }

            const string SP = "dbo.Company_GetCompanyIdByCompanyName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@companyName", DbType.String, companyName);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 companyId = 0;

                    while (reader.Read())
                    {
                        companyId = reader.GetInt32(0);
                    }

                    return companyId;
                }
            }
        }

        public string GetCompanyNameById(int CompanyId)
        {
            if (CompanyId < 1)
            {
                throw new ArgumentNullException("CompanyId");
            }
            const string SP = "dbo.Company_GetCompanyNameById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, CompanyId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                    return string.Empty;
                }
            }
        }

        string ICompanyDataAccess.GetCompanyNameByVendorMemberID(int MemberId)
        {
            if (MemberId < 1)
            {
                throw new ArgumentNullException("MemberId");
            }

            const string SP = "dbo.Company_GetCompanyNameByVendorMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                    return string.Empty;
                }
            }
        }

        Int32 ICompanyDataAccess.GetCompayByJobPostingId (int JobpostingId)
        {

            if (JobpostingId < 1)
            {
                throw new ArgumentNullException("JobpostingId");
            }

            const string SP = "dbo.Company_GetByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobpostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    Int32 companyId = 0;

                    while (reader.Read())
                    {
                        companyId = reader.GetInt32(0);
                    }

                    return companyId;
                }
            }
        }
        int ICompanyDataAccess.GetCompanyIdByNameForDataImport(string CompanyName) 
        {
            const string SP = "dbo.Company_GetCompanyIdByName";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                Database.AddInParameter(cmd, "@BUName", DbType.String, CompanyName);
                using (IDataReader reader = Database.ExecuteReader(cmd)) 
                {
                    while (reader.Read()) 
                    {
                        return reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    }
                    return 0;
                }
            }
        
        }
        #endregion
    }
}