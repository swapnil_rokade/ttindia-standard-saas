﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class CompanyBuilder : IEntityBuilder<Company>
    {
        IList<Company> IEntityBuilder<Company>.BuildEntities(IDataReader reader)
        {
            List<Company> companys = new List<Company>();

            while (reader.Read())
            {
                companys.Add(((IEntityBuilder<Company>)this).BuildEntity(reader));
            }

            return (companys.Count > 0) ? companys : null;
        }

        Company IEntityBuilder<Company>.BuildEntity(IDataReader reader)
        {

            if (reader.FieldCount == 8)
            {
                Company company = new Company();
                const int FLD_ID = 0;
                const int FLD_COMPANYNAME = 1;
                const int FLD_CITY = 2;
                const int FLD_STATEID = 3;
                const int FLD_OFFICEPHONE = 4;
                const int FLD_OFFICEPHONEEXTENSION = 5;
                const int FLD_PRIMARYEMAIL = 6;
                const int FLD_COMPANY_STATUS = 7;
                company.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                company.CompanyName = reader.IsDBNull(FLD_COMPANYNAME) ? string.Empty : reader.GetString(FLD_COMPANYNAME);
                company.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
                company.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
                company.OfficePhone = reader.IsDBNull(FLD_OFFICEPHONE) ? string.Empty : reader.GetString(FLD_OFFICEPHONE);
                company.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
                company.OfficePhoneExtension = reader.IsDBNull(FLD_OFFICEPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_OFFICEPHONEEXTENSION);
                company.CompanyStatus = (CompanyStatus)(reader.IsDBNull(FLD_COMPANY_STATUS) ? 0 : reader.GetInt32(FLD_COMPANY_STATUS));
                using (Context ctx = new Context())
                {
                    ICompanyContactDataAccess da = new CompanyContactDataAccess(ctx);
                    company.PrimaryContact = da.GetPrimary(company.Id);
                }
                return company;
            }
            else
            {
                const int FLD_ID = 0;
                const int FLD_COMPANYNAME = 1;
                const int FLD_ADDRESS1 = 2;
                const int FLD_ADDRESS2 = 3;
                const int FLD_CITY = 4;
                const int FLD_STATEID = 5;
                const int FLD_ZIP = 6;
                const int FLD_COUNTRYID = 7;
                const int FLD_WEBADDRESS = 8;
                const int FLD_COMPANYLOGO = 9;
                const int FLD_COMPANYINFORMATION = 10;
                const int FLD_INDUSTRYTYPE = 11;
                const int FLD_COMPANYSIZE = 12;
                const int FLD_OWNERTYPE = 13;
                const int FLD_TICKERSYMBOL = 14;
                const int FLD_OFFICEPHONE = 15;
                const int FLD_OFFICEPHONEEXTENSION = 16;
                const int FLD_TOLLFREEPHONE = 17;
                const int FLD_TOLLFREEPHONEEXTENSION = 18;
                const int FLD_FAXNUMBER = 19;
                const int FLD_PRIMARYEMAIL = 20;
                const int FLD_SECONDARYEMAIL = 21;
                const int FLD_EINNUMBER = 22;
                const int FLD_ANNUALREVENUE = 23;
                const int FLD_NUMBEROFEMPLOYEE = 24;
                const int FLD_COMPANYTYPE = 25;
                const int FLD_COMPANY_STATUS = 26;
                const int FLD_IS_PENDING = 27;
                const int FLD_ISREMOVED = 28;
                const int FLD_ISENDCLIENT = 29;
                const int FLD_ISTIER1CLIENT = 30;
                const int FLD_ISTIER2CLIENT = 31;
                const int FLD_CREATORID = 32;
                const int FLD_UPDATORID = 33;
                const int FLD_CREATEDATE = 34;
                const int FLD_UPDATEDATE = 35;
                const int FLD_ISVOLUMEHIRECLIENT = 36;
                const int FLD_CONTRACTSTARTDATE = 37;
                const int FLD_CONTRACTENDDATE= 38;
                const int FLD_SERVICEFEE = 39;


                Company company = new Company();

                company.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                company.CompanyName = reader.IsDBNull(FLD_COMPANYNAME) ? string.Empty : reader.GetString(FLD_COMPANYNAME);
                company.Address1 = reader.IsDBNull(FLD_ADDRESS1) ? string.Empty : reader.GetString(FLD_ADDRESS1);
                company.Address2 = reader.IsDBNull(FLD_ADDRESS2) ? string.Empty : reader.GetString(FLD_ADDRESS2);
                company.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
                company.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);
                company.Zip = reader.IsDBNull(FLD_ZIP) ? string.Empty : reader.GetString(FLD_ZIP);
                company.CountryId = reader.IsDBNull(FLD_COUNTRYID) ? 0 : reader.GetInt32(FLD_COUNTRYID);
                company.WebAddress = reader.IsDBNull(FLD_WEBADDRESS) ? string.Empty : reader.GetString(FLD_WEBADDRESS);
                company.CompanyLogo = reader.IsDBNull(FLD_COMPANYLOGO) ? string.Empty : reader.GetString(FLD_COMPANYLOGO);
                company.CompanyInformation = reader.IsDBNull(FLD_COMPANYINFORMATION) ? string.Empty : reader.GetString(FLD_COMPANYINFORMATION);
                company.IndustryType = reader.IsDBNull(FLD_INDUSTRYTYPE) ? 0 : reader.GetInt32(FLD_INDUSTRYTYPE);
                company.CompanySize = reader.IsDBNull(FLD_COMPANYSIZE) ? 0 : reader.GetInt32(FLD_COMPANYSIZE);
                company.OwnerType = reader.IsDBNull(FLD_OWNERTYPE) ? 0 : reader.GetInt32(FLD_OWNERTYPE);
                company.TickerSymbol = reader.IsDBNull(FLD_TICKERSYMBOL) ? string.Empty : reader.GetString(FLD_TICKERSYMBOL);
                company.OfficePhone = reader.IsDBNull(FLD_OFFICEPHONE) ? string.Empty : reader.GetString(FLD_OFFICEPHONE);
                company.OfficePhoneExtension = reader.IsDBNull(FLD_OFFICEPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_OFFICEPHONEEXTENSION);
                company.TollFreePhone = reader.IsDBNull(FLD_TOLLFREEPHONE) ? string.Empty : reader.GetString(FLD_TOLLFREEPHONE);
                company.TollFreePhoneExtension = reader.IsDBNull(FLD_TOLLFREEPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_TOLLFREEPHONEEXTENSION);
                company.FaxNumber = reader.IsDBNull(FLD_FAXNUMBER) ? string.Empty : reader.GetString(FLD_FAXNUMBER);
                company.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
                company.SecondaryEmail = reader.IsDBNull(FLD_SECONDARYEMAIL) ? string.Empty : reader.GetString(FLD_SECONDARYEMAIL);
                company.EINNumber = reader.IsDBNull(FLD_EINNUMBER) ? string.Empty : reader.GetString(FLD_EINNUMBER);
                company.AnnualRevenue = reader.IsDBNull(FLD_ANNUALREVENUE) ? string.Empty : reader.GetString(FLD_ANNUALREVENUE);
                company.NumberOfEmployee = reader.IsDBNull(FLD_NUMBEROFEMPLOYEE) ? 0 : reader.GetInt32(FLD_NUMBEROFEMPLOYEE);
                company.CompanyType = reader.IsDBNull(FLD_COMPANYTYPE) ? string.Empty : reader.GetString(FLD_COMPANYTYPE);
                company.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
                company.IsEndClient = reader.IsDBNull(FLD_ISENDCLIENT) ? false : reader.GetBoolean(FLD_ISENDCLIENT);
                company.IsTier1Client = reader.IsDBNull(FLD_ISTIER1CLIENT) ? false : reader.GetBoolean(FLD_ISTIER1CLIENT);
                company.IsTier2Client = reader.IsDBNull(FLD_ISTIER2CLIENT) ? false : reader.GetBoolean(FLD_ISTIER2CLIENT);
                company.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                company.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                company.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                company.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

                company.ContractStartDate = reader.IsDBNull(FLD_CONTRACTSTARTDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CONTRACTSTARTDATE);
                company.ContractEndDate = reader.IsDBNull(FLD_CONTRACTENDDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CONTRACTENDDATE);
                company.ServiceFee = reader.IsDBNull(FLD_SERVICEFEE) ? 0 : reader.GetDecimal(FLD_SERVICEFEE);
                using (Context ctx = new Context())
                {
                    ICompanyContactDataAccess da = new CompanyContactDataAccess(ctx);
                    company.PrimaryContact = da.GetPrimary(company.Id);
                }

                company.CompanyStatus = (CompanyStatus)(reader.IsDBNull(FLD_COMPANY_STATUS) ? 0 : reader.GetInt32(FLD_COMPANY_STATUS));

                company.IsPending = reader.IsDBNull(FLD_IS_PENDING) ? false : reader.GetBoolean(FLD_IS_PENDING);

                company.IsVolumeHireClient = reader.IsDBNull(FLD_ISVOLUMEHIRECLIENT) ? false : reader.GetBoolean(FLD_ISVOLUMEHIRECLIENT);

                return company;
            }
        }
    }
}