﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberAttributeBuilder : IEntityBuilder<MemberAttribute>
    {
        IList<MemberAttribute> IEntityBuilder<MemberAttribute>.BuildEntities(IDataReader reader)
        {
            List<MemberAttribute> memberAttributes = new List<MemberAttribute>();

            while (reader.Read())
            {
                memberAttributes.Add(((IEntityBuilder<MemberAttribute>)this).BuildEntity(reader));
            }

            return (memberAttributes.Count > 0) ? memberAttributes : null;
        }

        MemberAttribute IEntityBuilder<MemberAttribute>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_COMMENT = 1;
            const int FLD_SCORE = 2;
            const int FLD_APPRAISALATTRIBUTEID = 3;
            const int FLD_APPRAISALRATINGID = 4;
            const int FLD_MEMBERID = 5;
            const int FLD_ISREMOVED = 6;
            const int FLD_CREATORID = 7;
            const int FLD_UPDATORID = 8;
            const int FLD_CREATEDATE = 9;
            const int FLD_UPDATEDATE = 10;

            MemberAttribute memberAttribute = new MemberAttribute();

            memberAttribute.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberAttribute.Comment = reader.IsDBNull(FLD_COMMENT) ? string.Empty : reader.GetString(FLD_COMMENT);
            memberAttribute.Score = reader.IsDBNull(FLD_SCORE) ? 0 : reader.GetDecimal(FLD_SCORE);
            memberAttribute.AppraisalAttributeId = reader.IsDBNull(FLD_APPRAISALATTRIBUTEID) ? 0 : reader.GetInt32(FLD_APPRAISALATTRIBUTEID);
            memberAttribute.AppraisalRatingId = reader.IsDBNull(FLD_APPRAISALRATINGID) ? 0 : reader.GetInt32(FLD_APPRAISALRATINGID);
            memberAttribute.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberAttribute.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberAttribute.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberAttribute.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberAttribute.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberAttribute.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberAttribute;
        }
    }
}