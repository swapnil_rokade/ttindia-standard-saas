﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberExperienceBuilder.cs
    Description: This page is used to build entity for MemberExperience.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Dec-22-2008         Shivanand           Defect #9511; In the method BuildEntity(),new constants added [FLD_CONTACTNAME,FLD_COMPANYEMAIL,
                                                                FLD_COMPANYPHONE,FLD_COMPANYWEBSITE] and used to build memberExperience object. 
    0.2            July-7-2009         Gopala Swamy J       Defect #10847; Added new constants FLD_STATEID
    ------------------------------------------------------------------------------------------------------------------------------
*/
using System;
using System.Data;
using System.Collections.Generic;


using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberExperienceBuilder : IEntityBuilder<MemberExperience>
    {
        IList<MemberExperience> IEntityBuilder<MemberExperience>.BuildEntities(IDataReader reader)
        {
            List<MemberExperience> memberExperiences = new List<MemberExperience>();

            while (reader.Read())
            {
                memberExperiences.Add(((IEntityBuilder<MemberExperience>)this).BuildEntity(reader));
            }

            return (memberExperiences.Count > 0) ? memberExperiences : null;
        }

        MemberExperience IEntityBuilder<MemberExperience>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_COMPANYNAME = 1;
            const int FLD_POSITIONNAME = 2;
            const int FLD_CITY = 3;
            const int FLD_DATEFROM = 4;
            const int FLD_DATETO = 5;
            const int FLD_ISTILLDATE = 6;
            const int FLD_RESPONSIBILITIES = 7;
            const int FLD_ISREMOVED = 8;
            const int FLD_COUNTRYLOOKUPID = 9;
            const int FLD_INDUSTRYCATEGORYLOOKUPID = 10;
            const int FLD_FUNCTIONALCATEGORYLOOKUPID = 11;
            const int FLD_MEMBERID = 12;
            const int FLD_CREATORID = 13;
            const int FLD_UPDATORID = 14;
            const int FLD_CREATEDATE = 15;
            const int FLD_UPDATEDATE = 16;

    // 0.1 starts
            const int FLD_CONTACTNAME = 17;
            const int FLD_COMPANYEMAIL = 18;
            const int FLD_COMPANYPHONE = 19;
            const int FLD_COMPANYWEBSITE = 20;
    // 0.1 ends
            const int FLD_STATEID = 21; //0.2

            MemberExperience memberExperience = new MemberExperience();

            memberExperience.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberExperience.CompanyName = reader.IsDBNull(FLD_COMPANYNAME) ? string.Empty : reader.GetString(FLD_COMPANYNAME);
            memberExperience.PositionName = reader.IsDBNull(FLD_POSITIONNAME) ? string.Empty : reader.GetString(FLD_POSITIONNAME);
            memberExperience.City = reader.IsDBNull(FLD_CITY) ? string.Empty : reader.GetString(FLD_CITY);
            memberExperience.DateFrom = reader.IsDBNull(FLD_DATEFROM) ? DateTime.MinValue : reader.GetDateTime(FLD_DATEFROM);
            memberExperience.DateTo = reader.IsDBNull(FLD_DATETO) ? DateTime.MinValue : reader.GetDateTime(FLD_DATETO);
            memberExperience.IsTillDate = reader.IsDBNull(FLD_ISTILLDATE) ? false : reader.GetBoolean(FLD_ISTILLDATE);
            memberExperience.Responsibilities = reader.IsDBNull(FLD_RESPONSIBILITIES) ? string.Empty : reader.GetString(FLD_RESPONSIBILITIES);
            memberExperience.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberExperience.CountryLookupId = reader.IsDBNull(FLD_COUNTRYLOOKUPID) ? 0 : reader.GetInt32(FLD_COUNTRYLOOKUPID);
            memberExperience.IndustryCategoryLookupId = reader.IsDBNull(FLD_INDUSTRYCATEGORYLOOKUPID) ? 0 : reader.GetInt32(FLD_INDUSTRYCATEGORYLOOKUPID);
            memberExperience.FunctionalCategoryLookupId = reader.IsDBNull(FLD_FUNCTIONALCATEGORYLOOKUPID) ? 0 : reader.GetInt32(FLD_FUNCTIONALCATEGORYLOOKUPID);
            memberExperience.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberExperience.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberExperience.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberExperience.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberExperience.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

    // 0.1 starts
            memberExperience.ContactName = reader.IsDBNull(FLD_CONTACTNAME) ? string.Empty : reader.GetString(FLD_CONTACTNAME);
            memberExperience.CompanyEmail = reader.IsDBNull(FLD_COMPANYEMAIL) ? string.Empty : reader.GetString(FLD_COMPANYEMAIL);
            memberExperience.CompanyPhone = reader.IsDBNull(FLD_COMPANYPHONE) ? string.Empty : reader.GetString(FLD_COMPANYPHONE);
            memberExperience.CompanyWebsite = reader.IsDBNull(FLD_COMPANYWEBSITE) ? string.Empty : reader.GetString(FLD_COMPANYWEBSITE);
    // 0.1 ends
            //0.2 starts here 
            if (reader.FieldCount > FLD_STATEID)
            {
                memberExperience.StateId = reader.IsDBNull(FLD_STATEID) ? 0 : reader.GetInt32(FLD_STATEID);

            }
            //0.2 end here

            return memberExperience;
        }
    }
}