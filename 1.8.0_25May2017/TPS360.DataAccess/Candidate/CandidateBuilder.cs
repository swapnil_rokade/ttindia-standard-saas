﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateBuilder.cs
    Description: This page is used to build candidate entities.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ------------------------------------------------------------------------------------------------------------------------------    
    0.1            Dec-02-2008        Shivanand        Defect #8745; In the method BuildSearchEntity(),new constant FLD_PERMANENTSTATEID is added &
                                                           used to assign "PermanentStateId" in candidate entity.
                                                                     New constants added viz. FLD_PermanentAddressLine1,FLD_PermanentAddressLine2,FLD_PermanentCity,
                                                                        FLD_PermanentCountryId,FLD_PermanentPhone,FLD_PermanentMobile,FLD_AlternateEmail,FLD_CurrentAddressLine1,
                                                                        FLD_CurrentAddressLine2,FLD_CurrentStateId,FLD_OfficePhone
 *   0.2            3/Feb/2016       Pravin khot      Code change by IEntityBuilder<Candidate>.BuildEntity  
 *   0.3           4/March/2016      pravin khot      added new method BuildCDMEntities
 *   0.4           22/June/2016      pravin khot      added - else if (reader.FieldCount == 16)
                                                                       
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CandidateBuilder : IEntityBuilder<Candidate>
    {
        IList<Candidate> IEntityBuilder<Candidate>.BuildEntities(IDataReader reader)
        {
            List<Candidate> Candidates = new List<Candidate>();

            while (reader.Read())
            {
               
                Candidates.Add(((IEntityBuilder<Candidate>)this).BuildEntity(reader));
            }

            return (Candidates.Count > 0) ? Candidates : null;
        }
        public IList<DynamicDictionary> BuildEntities(IDataReader reader, IList<string> CheckedList)
        {
            List<DynamicDictionary> re = new List<DynamicDictionary>();
            while (reader.Read())
            {

                re.Add(BuildEntityForReport(reader, CheckedList));

            }

            return (re.Count > 0) ? re : null;
        }
        public IList<Candidate> BuildEntitiesForVendorCandidatePerformance(IDataReader reader)
        {
            List<Candidate> Candidates = new List<Candidate>();

            while (reader.Read())
            {
                Candidates.Add(BuildEntityForVendorCandidatePerformance(reader));
            }

            return (Candidates.Count > 0) ? Candidates : null;
        }
        DynamicDictionary BuildEntityForReport(IDataReader reader, IList<string> CheckedList)
        {
            DynamicDictionary newDyn = new DynamicDictionary();
            foreach (string s in CheckedList)
            {
                try
                {
                    string m = "";
                    //if (s == "Name")
                    //    m = reader["FirstName"].ToString () + " " + reader["LastName"].ToString ();
                    //else 
                    m = reader[s].ToString();
                    if (s == "ResumeSource")
                    {
                        m = TPS360.Common.EnumHelper.GetDescription((TPS360.Common.Shared.ResumeSource)Convert.ToInt32(m));
                    }

                    newDyn.properties.Add(s, m);
                }
                catch
                {
                }
            }
            return newDyn;
        }

        Candidate BuildEntityForVendorCandidatePerformance(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_FIRSTNAME = 1;
            const int FLD_MIDDLENAME = 2;
            const int FLD_LASTNAME = 3;
            const int FLD_PRIMARYEMAIL = 4;
            const int FLD_HiringStatus = 5;
            const int FLD_JOBPOSTINGID = 6;
            const int FLD_JobTitle = 7;
            const int FLD_PROCESSDAYS = 8;

            Candidate Candidate = new Candidate();
            Candidate.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            Candidate.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            Candidate.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
            Candidate.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            Candidate.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
            Candidate.HiringStatus = reader.IsDBNull(FLD_HiringStatus) ? string.Empty : reader.GetString(FLD_HiringStatus);
            Candidate.JobId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            Candidate.JOBTITLE = reader.IsDBNull(FLD_JobTitle) ? string.Empty : reader.GetString(FLD_JobTitle);
            Candidate.PROCEDDDAYS = reader.IsDBNull(FLD_PROCESSDAYS) ? 0 : reader.GetInt32(FLD_PROCESSDAYS);
            return Candidate;
        }
       
        Candidate IEntityBuilder<Candidate>.BuildEntity(IDataReader reader)
        {           
           if (reader.FieldCount == 12 || reader.FieldCount == 13 || reader.FieldCount == 15) //CODE CHANGE TO PRAVIN KHOT OLD CODE-(reader.FieldCount == 12 || reader.FieldCount == 13) on 3/Feb/2016
            {
                const int FLD_ID = 0;
                const int FLD_FIRSTNAME = 1;
                const int FLD_MIDDLENAME = 2;
                const int FLD_LASTNAME = 3;
                const int FLD_PRIMARYEMAIL = 4;
                const int FLD_CELLPHONE = 5;
                const int FLD_HOMEPHONE = 6;
                const int FLD_CURRENTCITY = 7;
                const int FLD_STATENAME = 8;
                const int FLD_CURRENTPOSITION = 9;
                const int FLD_REMARKS = 10;
                const int FLD_CREATEDATE = 11;
                const int FLD_CREATORNAME = 12;
                const int FLD_RequisitionNAME = 13;// add by pravin khot 
                const int FLD_JobPostingCode = 14;// add by pravin khot 
               // const int FLD_SOURCEDESCRIPTION = 15;
                //candidate.SourceDescription = reader.IsDBNull(FLD_SOURCEDESCRIPTION) ? string.Empty : reader.GetString(FLD_SOURCEDESCRIPTION);

                //if (reader.FieldCount == 12)
                //{
                //    FLD_JobPostingCode = 13;// add by pravin khot 
                //}
                // 0.1

                Candidate Candidate = new Candidate();
                Candidate.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                Candidate.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
                Candidate.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
                Candidate.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
                Candidate.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
                Candidate.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
                Candidate.HomePhone = reader.IsDBNull(FLD_HOMEPHONE) ? string.Empty : reader.GetString(FLD_HOMEPHONE);
                Candidate.CurrentCity = reader.IsDBNull(FLD_CURRENTCITY) ? string.Empty : reader.GetString(FLD_CURRENTCITY);

                Candidate.StateName = reader.IsDBNull(FLD_STATENAME) ? string.Empty : reader.GetString(FLD_STATENAME);
                Candidate.CurrentPosition = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
                Candidate.Remarks = reader.IsDBNull(FLD_REMARKS) ? string.Empty : reader.GetString(FLD_REMARKS);
                Candidate.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                if (reader.FieldCount > FLD_CREATORNAME)
                    Candidate.CreatorName = reader.IsDBNull(FLD_CREATORNAME) ? string.Empty : reader.GetString(FLD_CREATORNAME);
                //**************Code added by pravin khot on 28/Jan/2016******************
                if (reader.FieldCount > FLD_RequisitionNAME)
                {
                    Candidate.RequisitionName = reader.IsDBNull(FLD_RequisitionNAME) ? string.Empty : reader.GetString(FLD_RequisitionNAME);
                    Candidate.JobId = reader.IsDBNull(FLD_JobPostingCode) ? 0 : reader.GetInt32(FLD_JobPostingCode);
                }
                //*****************************End*****************************************
                return Candidate;
            }
                //***********Code added by pravin khot on 22/June/2016*****
            else if (reader.FieldCount == 16) 
            {
                const int FLD_ID = 0;
                const int FLD_FIRSTNAME = 1;
                const int FLD_MIDDLENAME = 2;
                const int FLD_LASTNAME = 3;
                const int FLD_PRIMARYEMAIL = 4;
                const int FLD_CELLPHONE = 5;
                const int FLD_HOMEPHONE = 6;
                const int FLD_CURRENTCITY = 7;
                const int FLD_STATENAME = 8;
                const int FLD_CURRENTPOSITION = 9;
                const int FLD_REMARKS = 10;
                const int FLD_CREATEDATE = 11;
                const int FLD_CREATORNAME = 12;
                const int FLD_SourceId = 13;// add by pravin khot 
                const int FLD_SourceName = 14;// add by pravin khot 
                const int FLD_SourceDescription = 15;// add by pravin khot 
              
                Candidate Candidate = new Candidate();
                Candidate.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                Candidate.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
                Candidate.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
                Candidate.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
                Candidate.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
                Candidate.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
                Candidate.HomePhone = reader.IsDBNull(FLD_HOMEPHONE) ? string.Empty : reader.GetString(FLD_HOMEPHONE);
                Candidate.CurrentCity = reader.IsDBNull(FLD_CURRENTCITY) ? string.Empty : reader.GetString(FLD_CURRENTCITY);

                Candidate.StateName = reader.IsDBNull(FLD_STATENAME) ? string.Empty : reader.GetString(FLD_STATENAME);
                Candidate.CurrentPosition = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
                Candidate.Remarks = reader.IsDBNull(FLD_REMARKS) ? string.Empty : reader.GetString(FLD_REMARKS);
                Candidate.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                if (reader.FieldCount > FLD_CREATORNAME)
                    Candidate.CreatorName = reader.IsDBNull(FLD_CREATORNAME) ? string.Empty : reader.GetString(FLD_CREATORNAME);
                //**************Code added by pravin khot on 28/Jan/2016******************
                if (reader.FieldCount > FLD_SourceId)
                {
                    //Candidate. = reader.IsDBNull(FLD_SourceId) ? string.Empty : reader.GetString(FLD_SourceId);
                    Candidate.Source = reader.IsDBNull(FLD_SourceName) ? string.Empty : reader.GetString(FLD_SourceName);
                    Candidate.SourceDescription = reader.IsDBNull(FLD_SourceDescription) ? string.Empty : reader.GetString(FLD_SourceDescription);
                }
                //*****************************End*****************************************
                return Candidate;
            }
                //***********************END***********************************
            else if (reader.FieldCount == 112)
            {
                const int FLD_ID = 0;
                const int FLD_FIRSTNAME = 1;
                const int FLD_MIDDLENAME = 2;
                const int FLD_LASTNAME = 3;
                const int FLD_PRIMARYEMAIL = 4;
                const int FLD_PRIMARYPHONE = 5;
                const int FLD_PRIMARYPHONEEXTENSION = 6;
                const int FLD_CELLPHONE = 7;
                const int FLD_STATUS = 8;
                const int FLD_HOMEPHONE = 9;
                const int FLD_CURRENTCITY = 10;
                const int FLD_STATENAME = 11;
                const int FLD_STATECODE = 12;
                const int FLD_OFFICECITY = 13;
                const int FLD_CURRENTPOSITION = 14;
                const int FLD_REMARKS = 15;
                const int FLD_OBJECTIVE = 16;
                const int FLD_SUMMARY = 17;
                const int FLD_SKILLS = 18;
                const int FLD_USERID = 19;
                const int FLD_CREATORID = 20;
                const int FLD_UPDATORID = 21;
                const int FLD_CREATEDATE = 22;
                const int FLD_UPDATEDATE = 23;

                const int FLD_RECORDTYPE = 24;
                const int FLD_NICKNAME = 25;
                const int FLD_DATEOFBIRTH = 26;
                const int FLD_PERMANENTADDRESSLINE1 = 27;
                const int FLD_PERMANENTADDRESSLINE2 = 28;
                const int FLD_PERMANENTCITY = 29;
                const int FLD_PERMANENTSTATEID = 30;
                const int FLD_PERMANENTSTATENAME = 31;
                const int FLD_PERMANENTSTATECODE = 32;
                const int FLD_PERMANENTZIP = 33;
                const int FLD_PERMANENTCOUNTRYID = 34;
                const int FLD_PERMANENTCOUNTRYNAME = 35;
                const int FLD_PERMANENTCOUNTRYCODE = 36;
                const int FLD_PERMANENTPHONE = 37;
                const int FLD_PERMANENTPHONEEXT = 38;
                const int FLD_PERMANENTMOBILE = 39;
                const int FLD_ALTERNATEEMAIL = 40;
                const int FLD_RESUMESOURCE = 41;
                const int FLD_CURRENTADDRESSLINE1 = 42;
                const int FLD_CURRENTADDRESSLINE2 = 43;
                const int FLD_CURRENTSTATEID = 44;
                const int FLD_CURRENTSTATENAME = 45;
                const int FLD_CURRENTSTATECODE = 46;
                const int FLD_CURRENTZIP = 47;
                const int FLD_CURRENTCOUNTRYID = 48;
                const int FLD_CURRENTCOUNTRYNAME = 49;
                const int FLD_CURRENTCOUNTRYCODE = 50;
                const int FLD_OFFICEPHONE = 51;
                const int FLD_OFFICEPHONEEXTENSION = 52;
                const int FLD_CITYOFBIRTH = 53;
                const int FLD_COUNTRYIDOFBIRTH = 54;
                const int FLD_BIRTHCOUNTRYNAME = 55;
                const int FLD_BIRTHCOUNTRYCODE = 56;
                const int FLD_COUNTRYIDOFCITIZENSHIP = 57;
                const int FLD_CITIZENSHIPCOUNTRYNAME = 58;
                const int FLD_CITIZENSHIPCOUNTRYCODE = 59;
                const int FLD_GENDERLOOKUPID = 60;
                const int FLD_GENDER = 61;
                const int FLD_ETHNICGROUPLOOKUPID = 62;
                const int FLD_ETHNICGROUP = 63;
                const int FLD_BLOODGROUPLOOKUPID = 64;
                const int FLD_BLOODGROUP = 65;
                const int FLD_MARITALSTATUSLOOKUPID = 66;
                const int FLD_MARITALSTATUS = 67;
                const int FLD_RELOCATION = 68;
                const int FLD_LASTEMPLOYER = 69;
                const int FLD_TOTALEXPERIENCEYEARS = 70;
                const int FLD_AVAILABILITY = 71;
                const int FLD_AVAILABILITYTEXT = 72;
                const int FLD_AVAILABLEDATE = 73;
                const int FLD_CURRENTYEARLYRATE = 74;
                const int FLD_CURRENTYEARLYCURRENCYLOOKUPID = 75;
                const int FLD_CURRENTYEARLYCURRENCY = 76;
                const int FLD_CURRENTMONTHLYRATE = 77;
                const int FLD_CURRENTMONTHLYCURRENCYLOOKUPID = 78;
                const int FLD_CURRENTMONTHLYCURRENCY = 79;
                const int FLD_CURRENTHOURLYRATE = 80;
                const int FLD_CURRENTHOURLYCURRENCYLOOKUPID = 81;
                const int FLD_CURRENTHOURLYCURRENCY = 82;
                const int FLD_EXPECTEDYEARLYRATE = 83;
                const int FLD_EXPECTEDYEARLYCURRENCYLOOKUPID = 84;
                const int FLD_EXPECTEDYEARLYCURRENCY = 85;
                const int FLD_EXPECTEDMONTHLYRATE = 86;
                const int FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID = 87;
                const int FLD_EXPECTEDMONTHLYCURRENCY = 88;
                const int FLD_EXPECTEDHOURLYRATE = 89;
                const int FLD_EXPECTEDHOURLYCURRENCYLOOKUPID = 90;
                const int FLD_EXPECTEDHOURLYCURRENCY = 91;
                const int FLD_WILLINGTOTRAVEL = 92;
                const int FLD_SALARYTYPELOOKUPID = 93;
                const int FLD_SALARYTYPE = 94;
                const int FLD_JOBTYPELOOKUPID = 95;
                const int FLD_JOBTYPE = 96;
                const int FLD_SECURITYCLEARANCE = 97;
                const int FLD_WORKAUTHORIZATIONLOOKUPID = 98;
                const int FLD_WORKAUTHORIZATION = 99;
                const int FLD_CREATOR = 100;
                const int FLD_UPDATOR = 101;
                const int FLD_WORKSCEDULELOOKIPID = 102;
                const int FLD_WORKSCEDULE = 103;
                const int FLD_WESITE = 104;
                const int FLD_LINKEDINPROFILE = 105;
                const int FLD_PASSPORTSTATUS = 106;
                const int FLD_IDCARD = 107;
                const int FLD_IDCARDDETAIL = 108;
                const int FLD_HIGHEREDUCATION = 109;
                const int FLD_SOURCE = 110;
                const int FLD_SOURCEDESCRIPTION = 111;

                Candidate candidate = new Candidate();

                candidate.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                candidate.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
                candidate.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
                candidate.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
                candidate.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
                candidate.PrimaryPhone = reader.IsDBNull(FLD_PRIMARYPHONE) ? string.Empty : reader.GetString(FLD_PRIMARYPHONE);
                candidate.PrimaryPhoneExtension = reader.IsDBNull(FLD_PRIMARYPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_PRIMARYPHONEEXTENSION);
                candidate.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
                candidate.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
                candidate.HomePhone = reader.IsDBNull(FLD_HOMEPHONE) ? string.Empty : reader.GetString(FLD_HOMEPHONE);
                candidate.CurrentCity = reader.IsDBNull(FLD_CURRENTCITY) ? string.Empty : reader.GetString(FLD_CURRENTCITY);
                candidate.StateName = reader.IsDBNull(FLD_STATENAME) ? string.Empty : reader.GetString(FLD_STATENAME);
                candidate.StateCode = reader.IsDBNull(FLD_STATECODE) ? string.Empty : reader.GetString(FLD_STATECODE);
                candidate.OfficeCity = reader.IsDBNull(FLD_OFFICECITY) ? string.Empty : reader.GetString(FLD_OFFICECITY);
                candidate.CurrentPosition = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
                candidate.Remarks = reader.IsDBNull(FLD_REMARKS) ? string.Empty : reader.GetString(FLD_REMARKS);
                candidate.Objective = reader.IsDBNull(FLD_OBJECTIVE) ? string.Empty : reader.GetString(FLD_OBJECTIVE);
                candidate.Summary = reader.IsDBNull(FLD_SUMMARY) ? string.Empty : reader.GetString(FLD_SUMMARY);
                candidate.Skills = reader.IsDBNull(FLD_SKILLS) ? string.Empty : reader.GetString(FLD_SKILLS);
                candidate.UserId = reader.IsDBNull(FLD_USERID) ? Guid.Empty : reader.GetGuid(FLD_USERID);
                candidate.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                candidate.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                candidate.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                //candidate.PermanentAddressLine1 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE1);
                //candidate.PermanentAddressLine2 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE2);
                candidate.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
                string strRecordType = reader.IsDBNull(FLD_RECORDTYPE) ? string.Empty : reader.GetString(FLD_RECORDTYPE);
                string IDCard = "";
                string IDCardDetail = "";
                if (strRecordType.ToUpper() == "LIST")
                {
                    candidate.LastEmployer = reader.IsDBNull(25) ? string.Empty : reader.GetString(25);
                }
                else if (strRecordType.ToUpper() == "REPORT")
                {
                    candidate.NickName = reader.IsDBNull(FLD_NICKNAME) ? string.Empty : reader.GetString(FLD_NICKNAME);
                    candidate.DateOfBirth = reader.IsDBNull(FLD_DATEOFBIRTH) ? DateTime.MinValue : reader.GetDateTime(FLD_DATEOFBIRTH);
                    candidate.PermanentAddressLine1 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE1);
                    candidate.PermanentAddressLine2 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE2);
                    candidate.PermanentCity = reader.IsDBNull(FLD_PERMANENTCITY) ? string.Empty : reader.GetString(FLD_PERMANENTCITY);
                    candidate.PermanentStateId = reader.IsDBNull(FLD_PERMANENTSTATEID) ? 0 : reader.GetInt32(FLD_PERMANENTSTATEID);
                    candidate.PermanentStateName = reader.IsDBNull(FLD_PERMANENTSTATENAME) ? string.Empty : reader.GetString(FLD_PERMANENTSTATENAME);
                    candidate.PermanentStateCode = reader.IsDBNull(FLD_PERMANENTSTATECODE) ? string.Empty : reader.GetString(FLD_PERMANENTSTATECODE);
                    candidate.PermanentZip = reader.IsDBNull(FLD_PERMANENTZIP) ? string.Empty : reader.GetString(FLD_PERMANENTZIP);
                    candidate.PermanentCountryId = reader.IsDBNull(FLD_PERMANENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_PERMANENTCOUNTRYID);
                    candidate.PermanentCountryName = reader.IsDBNull(FLD_PERMANENTCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_PERMANENTCOUNTRYNAME);
                    candidate.PermanentCountryCode = reader.IsDBNull(FLD_PERMANENTCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_PERMANENTCOUNTRYCODE);
                    candidate.PermanentPhone = reader.IsDBNull(FLD_PERMANENTPHONE) ? string.Empty : reader.GetString(FLD_PERMANENTPHONE);
                    candidate.PermanentPhoneExt = reader.IsDBNull(FLD_PERMANENTPHONEEXT) ? string.Empty : reader.GetString(FLD_PERMANENTPHONEEXT);
                    candidate.PermanentMobile = reader.IsDBNull(FLD_PERMANENTMOBILE) ? string.Empty : reader.GetString(FLD_PERMANENTMOBILE);
                    candidate.AlternateEmail = reader.IsDBNull(FLD_ALTERNATEEMAIL) ? string.Empty : reader.GetString(FLD_ALTERNATEEMAIL);
                    candidate.ResumeSource = reader.IsDBNull(FLD_RESUMESOURCE) ? 0 : reader.GetInt32(FLD_RESUMESOURCE);
                    candidate.CurrentAddressLine1 = reader.IsDBNull(FLD_CURRENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE1);
                    candidate.CurrentAddressLine2 = reader.IsDBNull(FLD_CURRENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE2);
                    candidate.CurrentStateId = reader.IsDBNull(FLD_CURRENTSTATEID) ? 0 : reader.GetInt32(FLD_CURRENTSTATEID);
                    candidate.CurrentStateName = reader.IsDBNull(FLD_CURRENTSTATENAME) ? string.Empty : reader.GetString(FLD_CURRENTSTATENAME);
                    candidate.CurrentStateCode = reader.IsDBNull(FLD_CURRENTSTATECODE) ? string.Empty : reader.GetString(FLD_CURRENTSTATECODE);
                    candidate.CurrentZip = reader.IsDBNull(FLD_CURRENTZIP) ? string.Empty : reader.GetString(FLD_CURRENTZIP);
                    candidate.CurrentCountryId = reader.IsDBNull(FLD_CURRENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_CURRENTCOUNTRYID);
                    candidate.CurrentCountryName = reader.IsDBNull(FLD_CURRENTCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_CURRENTCOUNTRYNAME);
                    candidate.CurrentCountryCode = reader.IsDBNull(FLD_CURRENTCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_CURRENTCOUNTRYCODE);
                    candidate.OfficePhone = reader.IsDBNull(FLD_OFFICEPHONE) ? string.Empty : reader.GetString(FLD_OFFICEPHONE);
                    candidate.OfficePhoneExtension = reader.IsDBNull(FLD_OFFICEPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_OFFICEPHONEEXTENSION);
                    candidate.CityOfBirth = reader.IsDBNull(FLD_CITYOFBIRTH) ? string.Empty : reader.GetString(FLD_CITYOFBIRTH);
                    candidate.CountryIdOfBirth = reader.IsDBNull(FLD_COUNTRYIDOFBIRTH) ? string.Empty : reader.GetString(FLD_COUNTRYIDOFBIRTH);
                    candidate.BirthCountryName = reader.IsDBNull(FLD_BIRTHCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_BIRTHCOUNTRYNAME);
                    candidate.BirthCountryCode = reader.IsDBNull(FLD_BIRTHCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_BIRTHCOUNTRYCODE);
                    candidate.CountryIdOfCitizenship = reader.IsDBNull(FLD_COUNTRYIDOFCITIZENSHIP) ? string.Empty : reader.GetString(FLD_COUNTRYIDOFCITIZENSHIP);
                    candidate.CitizenshipCountryName = reader.IsDBNull(FLD_CITIZENSHIPCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_CITIZENSHIPCOUNTRYNAME);
                    candidate.CitizenshipCountryCode = reader.IsDBNull(FLD_CITIZENSHIPCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_CITIZENSHIPCOUNTRYCODE);
                    candidate.GenderLookupId = reader.IsDBNull(FLD_GENDERLOOKUPID) ? 0 : reader.GetInt32(FLD_GENDERLOOKUPID);
                    candidate.Gender = reader.IsDBNull(FLD_GENDER) ? string.Empty : reader.GetString(FLD_GENDER);
                    candidate.EthnicGroupLookupId = reader.IsDBNull(FLD_ETHNICGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_ETHNICGROUPLOOKUPID);
                    candidate.EthnicGroup = reader.IsDBNull(FLD_ETHNICGROUP) ? string.Empty : reader.GetString(FLD_ETHNICGROUP);
                    candidate.BloodGroupLookupId = reader.IsDBNull(FLD_BLOODGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_BLOODGROUPLOOKUPID);
                    candidate.BloodGroup = reader.IsDBNull(FLD_BLOODGROUP) ? string.Empty : reader.GetString(FLD_BLOODGROUP);
                    candidate.MaritalStatusLookupId = reader.IsDBNull(FLD_MARITALSTATUSLOOKUPID) ? 0 : reader.GetInt32(FLD_MARITALSTATUSLOOKUPID);
                    candidate.MaritalStatus = reader.IsDBNull(FLD_MARITALSTATUS) ? string.Empty : reader.GetString(FLD_MARITALSTATUS);
                    candidate.Relocation = reader.IsDBNull(FLD_RELOCATION) ? false : reader.GetBoolean(FLD_RELOCATION);
                    candidate.LastEmployer = reader.IsDBNull(FLD_LASTEMPLOYER) ? string.Empty : reader.GetString(FLD_LASTEMPLOYER);
                    candidate.TotalExperienceYears = reader.IsDBNull(FLD_TOTALEXPERIENCEYEARS) ? string.Empty : reader.GetString(FLD_TOTALEXPERIENCEYEARS);
                    candidate.Availability = reader.IsDBNull(FLD_AVAILABILITY) ? 0 : reader.GetInt32(FLD_AVAILABILITY);
                    candidate.AvailabilityText = reader.IsDBNull(FLD_AVAILABILITYTEXT) ? string.Empty : reader.GetString(FLD_AVAILABILITYTEXT);
                    candidate.AvailableDate = reader.IsDBNull(FLD_AVAILABLEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_AVAILABLEDATE);
                    candidate.CurrentYearlyRate = reader.IsDBNull(FLD_CURRENTYEARLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTYEARLYRATE);
                    candidate.CurrentYearlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTYEARLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTYEARLYCURRENCYLOOKUPID);
                    candidate.CurrentYearlyCurrency = reader.IsDBNull(FLD_CURRENTYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTYEARLYCURRENCY);
                    candidate.CurrentMonthlyRate = reader.IsDBNull(FLD_CURRENTMONTHLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTMONTHLYRATE);
                    candidate.CurrentMonthlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTMONTHLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTMONTHLYCURRENCYLOOKUPID);
                    candidate.CurrentMonthlyCurrency = reader.IsDBNull(FLD_CURRENTMONTHLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTMONTHLYCURRENCY);
                    candidate.CurrentHourlyRate = reader.IsDBNull(FLD_CURRENTHOURLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTHOURLYRATE);
                    candidate.CurrentHourlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTHOURLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTHOURLYCURRENCYLOOKUPID);
                    candidate.CurrentHourlyCurrency = reader.IsDBNull(FLD_CURRENTHOURLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTHOURLYCURRENCY);
                    candidate.ExpectedYearlyRate = reader.IsDBNull(FLD_EXPECTEDYEARLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDYEARLYRATE);
                    candidate.ExpectedYearlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDYEARLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDYEARLYCURRENCYLOOKUPID);
                    candidate.ExpectedYearlyCurrency = reader.IsDBNull(FLD_EXPECTEDYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDYEARLYCURRENCY);
                    candidate.ExpectedMonthlyRate = reader.IsDBNull(FLD_EXPECTEDMONTHLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDMONTHLYRATE);
                    candidate.ExpectedMonthlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID);
                    candidate.ExpectedMonthlyCurrency = reader.IsDBNull(FLD_EXPECTEDMONTHLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDMONTHLYCURRENCY);
                    candidate.ExpectedHourlyRate = reader.IsDBNull(FLD_EXPECTEDHOURLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDHOURLYRATE);
                    candidate.ExpectedHourlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDHOURLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDHOURLYCURRENCYLOOKUPID);
                    candidate.ExpectedHourlyCurrency = reader.IsDBNull(FLD_EXPECTEDHOURLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDHOURLYCURRENCY);
                    candidate.WillingToTravel = reader.IsDBNull(FLD_WILLINGTOTRAVEL) ? false : reader.GetBoolean(FLD_WILLINGTOTRAVEL);
                    candidate.SalaryTypeLookupId = reader.IsDBNull(FLD_SALARYTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_SALARYTYPELOOKUPID);
                    candidate.SalaryType = reader.IsDBNull(FLD_SALARYTYPE) ? string.Empty : reader.GetString(FLD_SALARYTYPE);
                    candidate.JobTypeLookupId = reader.IsDBNull(FLD_JOBTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_JOBTYPELOOKUPID);
                    candidate.JobType = reader.IsDBNull(FLD_JOBTYPE) ? string.Empty : reader.GetString(FLD_JOBTYPE);
                    candidate.SecurityClearance = reader.IsDBNull(FLD_SECURITYCLEARANCE) ? false : reader.GetBoolean(FLD_SECURITYCLEARANCE);
                    candidate.WorkAuthorizationLookupId = reader.IsDBNull(FLD_WORKAUTHORIZATIONLOOKUPID) ? 0 : reader.GetInt32(FLD_WORKAUTHORIZATIONLOOKUPID);
                    candidate.WorkAuthorization = reader.IsDBNull(FLD_WORKAUTHORIZATION) ? string.Empty : reader.GetString(FLD_WORKAUTHORIZATION);
                    candidate.CreatorName = reader.IsDBNull(FLD_CREATOR) ? string.Empty : reader.GetString(FLD_CREATOR);
                    candidate.LastUpdatorName = reader.IsDBNull(FLD_UPDATOR) ? string.Empty : reader.GetString(FLD_UPDATOR);
                    candidate.WorkScheduleLookupId = reader.IsDBNull(FLD_WORKSCEDULELOOKIPID) ? 0 : reader.GetInt32(FLD_WORKSCEDULELOOKIPID);
                    candidate.WorkSchedule = reader.IsDBNull(FLD_WORKSCEDULE) ? string.Empty : reader.GetString(FLD_WORKSCEDULE);
                    candidate.Website = reader.IsDBNull(FLD_WESITE) ? string.Empty : reader.GetString(FLD_WESITE);
                    candidate.LinkedInProfile = reader.IsDBNull(FLD_LINKEDINPROFILE) ? string.Empty : reader.GetString(FLD_LINKEDINPROFILE);
                    candidate.PassportStatus = reader.IsDBNull(FLD_PASSPORTSTATUS) ? false : reader.GetBoolean(FLD_PASSPORTSTATUS);
                    IDCard = reader.IsDBNull(FLD_IDCARD) ? string.Empty : reader.GetString(FLD_IDCARD);
                    IDCardDetail = reader.IsDBNull(FLD_IDCARDDETAIL) ? string.Empty : reader.GetString(FLD_IDCARDDETAIL);

                    if (IDCard != "")
                    {
                        candidate.IDCard = IDCard;
                        if (IDCardDetail != "")
                            candidate.IDCard += "-" + IDCardDetail;
                    }
                    else
                    {
                        candidate.IDCard = "";
                    }
                    candidate.HighestDegree = reader.IsDBNull(FLD_HIGHEREDUCATION) ? string.Empty : reader.GetString(FLD_HIGHEREDUCATION);
                    candidate.Source = reader.IsDBNull(FLD_SOURCE) ? string.Empty : reader.GetString(FLD_SOURCE);
                    candidate.SourceDescription = reader.IsDBNull(FLD_SOURCEDESCRIPTION) ? string.Empty : reader.GetString(FLD_SOURCEDESCRIPTION);
                }
                return candidate;
            }
            //change for 5 columns added
            else if (reader.FieldCount == 115)
            {
                const int FLD_ID = 0;
                const int FLD_FIRSTNAME = 1;
                const int FLD_MIDDLENAME = 2;
                const int FLD_LASTNAME = 3;
                const int FLD_PRIMARYEMAIL = 4;
                const int FLD_PRIMARYPHONE = 5;
                const int FLD_PRIMARYPHONEEXTENSION = 6;
                const int FLD_CELLPHONE = 7;
                const int FLD_STATUS = 8;
                const int FLD_HOMEPHONE = 9;
                const int FLD_CURRENTCITY = 10;
                const int FLD_STATENAME = 11;
                const int FLD_STATECODE = 12;
                const int FLD_OFFICECITY = 13;
                const int FLD_CURRENTPOSITION = 14;
                const int FLD_REMARKS = 15;
                const int FLD_OBJECTIVE = 16;
                const int FLD_SUMMARY = 17;
                const int FLD_SKILLS = 18;
                const int FLD_USERID = 19;
                const int FLD_CREATORID = 20;
                const int FLD_UPDATORID = 21;
                const int FLD_CREATEDATE = 22;
                const int FLD_UPDATEDATE = 23;

                const int FLD_RECORDTYPE = 24;
                const int FLD_NICKNAME = 25;
                const int FLD_DATEOFBIRTH = 26;
                const int FLD_PERMANENTADDRESSLINE1 = 27;
                const int FLD_PERMANENTADDRESSLINE2 = 28;
                const int FLD_PERMANENTCITY = 29;
                const int FLD_PERMANENTSTATEID = 30;
                const int FLD_PERMANENTSTATENAME = 31;
                const int FLD_PERMANENTSTATECODE = 32;
                const int FLD_PERMANENTZIP = 33;
                const int FLD_PERMANENTCOUNTRYID = 34;
                const int FLD_PERMANENTCOUNTRYNAME = 35;
                const int FLD_PERMANENTCOUNTRYCODE = 36;
                const int FLD_PERMANENTPHONE = 37;
                const int FLD_PERMANENTPHONEEXT = 38;
                const int FLD_PERMANENTMOBILE = 39;
                const int FLD_ALTERNATEEMAIL = 40;
                const int FLD_RESUMESOURCE = 41;
                const int FLD_CURRENTADDRESSLINE1 = 42;
                const int FLD_CURRENTADDRESSLINE2 = 43;
                const int FLD_CURRENTSTATEID = 44;
                const int FLD_CURRENTSTATENAME = 45;
                const int FLD_CURRENTSTATECODE = 46;
                const int FLD_CURRENTZIP = 47;
                const int FLD_CURRENTCOUNTRYID = 48;
                const int FLD_CURRENTCOUNTRYNAME = 49;
                const int FLD_CURRENTCOUNTRYCODE = 50;
                const int FLD_OFFICEPHONE = 51;
                const int FLD_OFFICEPHONEEXTENSION = 52;
                const int FLD_CITYOFBIRTH = 53;
                const int FLD_COUNTRYIDOFBIRTH = 54;
                const int FLD_BIRTHCOUNTRYNAME = 55;
                const int FLD_BIRTHCOUNTRYCODE = 56;
                const int FLD_COUNTRYIDOFCITIZENSHIP = 57;
                const int FLD_CITIZENSHIPCOUNTRYNAME = 58;
                const int FLD_CITIZENSHIPCOUNTRYCODE = 59;
                const int FLD_GENDERLOOKUPID = 60;
                const int FLD_GENDER = 61;
                const int FLD_ETHNICGROUPLOOKUPID = 62;
                const int FLD_ETHNICGROUP = 63;
                const int FLD_BLOODGROUPLOOKUPID = 64;
                const int FLD_BLOODGROUP = 65;
                const int FLD_MARITALSTATUSLOOKUPID = 66;
                const int FLD_MARITALSTATUS = 67;
                const int FLD_RELOCATION = 68;
                const int FLD_LASTEMPLOYER = 69;
                const int FLD_TOTALEXPERIENCEYEARS = 70;
                const int FLD_AVAILABILITY = 71;
                const int FLD_AVAILABILITYTEXT = 72;
                const int FLD_AVAILABLEDATE = 73;
                const int FLD_CURRENTYEARLYRATE = 74;
                const int FLD_CURRENTYEARLYCURRENCYLOOKUPID = 75;
                const int FLD_CURRENTYEARLYCURRENCY = 76;
                const int FLD_CURRENTMONTHLYRATE = 77;
                const int FLD_CURRENTMONTHLYCURRENCYLOOKUPID = 78;
                const int FLD_CURRENTMONTHLYCURRENCY = 79;
                const int FLD_CURRENTHOURLYRATE = 80;
                const int FLD_CURRENTHOURLYCURRENCYLOOKUPID = 81;
                const int FLD_CURRENTHOURLYCURRENCY = 82;
                const int FLD_EXPECTEDYEARLYRATE = 83;
                const int FLD_EXPECTEDYEARLYCURRENCYLOOKUPID = 84;
                const int FLD_EXPECTEDYEARLYCURRENCY = 85;
                const int FLD_EXPECTEDMONTHLYRATE = 86;
                const int FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID = 87;
                const int FLD_EXPECTEDMONTHLYCURRENCY = 88;
                const int FLD_EXPECTEDHOURLYRATE = 89;
                const int FLD_EXPECTEDHOURLYCURRENCYLOOKUPID = 90;
                const int FLD_EXPECTEDHOURLYCURRENCY = 91;
                const int FLD_WILLINGTOTRAVEL = 92;
                const int FLD_SALARYTYPELOOKUPID = 93;
                const int FLD_SALARYTYPE = 94;
                const int FLD_JOBTYPELOOKUPID = 95;
                const int FLD_JOBTYPE = 96;
                const int FLD_SECURITYCLEARANCE = 97;
                const int FLD_WORKAUTHORIZATIONLOOKUPID = 98;
                const int FLD_WORKAUTHORIZATION = 99;
                const int FLD_CREATOR = 100;
                const int FLD_UPDATOR = 101;
                const int FLD_WORKSCEDULELOOKIPID = 102;
                const int FLD_WORKSCEDULE = 103;
                const int FLD_WESITE = 104;
                const int FLD_LINKEDINPROFILE = 105;
                const int FLD_PASSPORTSTATUS = 106;
                const int FLD_IDCARD = 107;
                const int FLD_IDCARDDETAIL = 108;
                const int FLD_HIGHEREDUCATION = 109;
                const int FLD_SOURCE = 110;
                const int FLD_SOURCEDESCRIPTION = 111;

                const int FLD_CANDIDATEINDUSTRYLOOKUPID = 112;
                const int FLD_CANDIDATEINDUSTRY = 113;
                const int FLD_JOBFUNCTIONLOOKUPID = 114;
                const int FLD_JOBFUNCTION = 115;

                Candidate candidate = new Candidate();

                candidate.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                candidate.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
                candidate.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
                candidate.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
                candidate.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
                candidate.PrimaryPhone = reader.IsDBNull(FLD_PRIMARYPHONE) ? string.Empty : reader.GetString(FLD_PRIMARYPHONE);
                candidate.PrimaryPhoneExtension = reader.IsDBNull(FLD_PRIMARYPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_PRIMARYPHONEEXTENSION);
                candidate.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
                candidate.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
                candidate.HomePhone = reader.IsDBNull(FLD_HOMEPHONE) ? string.Empty : reader.GetString(FLD_HOMEPHONE);
                candidate.CurrentCity = reader.IsDBNull(FLD_CURRENTCITY) ? string.Empty : reader.GetString(FLD_CURRENTCITY);
                candidate.StateName = reader.IsDBNull(FLD_STATENAME) ? string.Empty : reader.GetString(FLD_STATENAME);
                candidate.StateCode = reader.IsDBNull(FLD_STATECODE) ? string.Empty : reader.GetString(FLD_STATECODE);
                candidate.OfficeCity = reader.IsDBNull(FLD_OFFICECITY) ? string.Empty : reader.GetString(FLD_OFFICECITY);
                candidate.CurrentPosition = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
                candidate.Remarks = reader.IsDBNull(FLD_REMARKS) ? string.Empty : reader.GetString(FLD_REMARKS);
                candidate.Objective = reader.IsDBNull(FLD_OBJECTIVE) ? string.Empty : reader.GetString(FLD_OBJECTIVE);
                candidate.Summary = reader.IsDBNull(FLD_SUMMARY) ? string.Empty : reader.GetString(FLD_SUMMARY);
                candidate.Skills = reader.IsDBNull(FLD_SKILLS) ? string.Empty : reader.GetString(FLD_SKILLS);
                candidate.UserId = reader.IsDBNull(FLD_USERID) ? Guid.Empty : reader.GetGuid(FLD_USERID);
                candidate.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                candidate.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                candidate.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                //candidate.PermanentAddressLine1 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE1);
                //candidate.PermanentAddressLine2 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE2);
                candidate.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
                string strRecordType = reader.IsDBNull(FLD_RECORDTYPE) ? string.Empty : reader.GetString(FLD_RECORDTYPE);
                string IDCard = "";
                string IDCardDetail = "";
                if (strRecordType.ToUpper() == "LIST")
                {
                    candidate.LastEmployer = reader.IsDBNull(25) ? string.Empty : reader.GetString(25);
                }
                else if (strRecordType.ToUpper() == "REPORT")
                {
                    candidate.NickName = reader.IsDBNull(FLD_NICKNAME) ? string.Empty : reader.GetString(FLD_NICKNAME);
                    candidate.DateOfBirth = reader.IsDBNull(FLD_DATEOFBIRTH) ? DateTime.MinValue : reader.GetDateTime(FLD_DATEOFBIRTH);
                    candidate.PermanentAddressLine1 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE1);
                    candidate.PermanentAddressLine2 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE2);
                    candidate.PermanentCity = reader.IsDBNull(FLD_PERMANENTCITY) ? string.Empty : reader.GetString(FLD_PERMANENTCITY);
                    candidate.PermanentStateId = reader.IsDBNull(FLD_PERMANENTSTATEID) ? 0 : reader.GetInt32(FLD_PERMANENTSTATEID);
                    candidate.PermanentStateName = reader.IsDBNull(FLD_PERMANENTSTATENAME) ? string.Empty : reader.GetString(FLD_PERMANENTSTATENAME);
                    candidate.PermanentStateCode = reader.IsDBNull(FLD_PERMANENTSTATECODE) ? string.Empty : reader.GetString(FLD_PERMANENTSTATECODE);
                    candidate.PermanentZip = reader.IsDBNull(FLD_PERMANENTZIP) ? string.Empty : reader.GetString(FLD_PERMANENTZIP);
                    candidate.PermanentCountryId = reader.IsDBNull(FLD_PERMANENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_PERMANENTCOUNTRYID);
                    candidate.PermanentCountryName = reader.IsDBNull(FLD_PERMANENTCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_PERMANENTCOUNTRYNAME);
                    candidate.PermanentCountryCode = reader.IsDBNull(FLD_PERMANENTCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_PERMANENTCOUNTRYCODE);
                    candidate.PermanentPhone = reader.IsDBNull(FLD_PERMANENTPHONE) ? string.Empty : reader.GetString(FLD_PERMANENTPHONE);
                    candidate.PermanentPhoneExt = reader.IsDBNull(FLD_PERMANENTPHONEEXT) ? string.Empty : reader.GetString(FLD_PERMANENTPHONEEXT);
                    candidate.PermanentMobile = reader.IsDBNull(FLD_PERMANENTMOBILE) ? string.Empty : reader.GetString(FLD_PERMANENTMOBILE);
                    candidate.AlternateEmail = reader.IsDBNull(FLD_ALTERNATEEMAIL) ? string.Empty : reader.GetString(FLD_ALTERNATEEMAIL);
                    candidate.ResumeSource = reader.IsDBNull(FLD_RESUMESOURCE) ? 0 : reader.GetInt32(FLD_RESUMESOURCE);
                    candidate.CurrentAddressLine1 = reader.IsDBNull(FLD_CURRENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE1);
                    candidate.CurrentAddressLine2 = reader.IsDBNull(FLD_CURRENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE2);
                    candidate.CurrentStateId = reader.IsDBNull(FLD_CURRENTSTATEID) ? 0 : reader.GetInt32(FLD_CURRENTSTATEID);
                    candidate.CurrentStateName = reader.IsDBNull(FLD_CURRENTSTATENAME) ? string.Empty : reader.GetString(FLD_CURRENTSTATENAME);
                    candidate.CurrentStateCode = reader.IsDBNull(FLD_CURRENTSTATECODE) ? string.Empty : reader.GetString(FLD_CURRENTSTATECODE);
                    candidate.CurrentZip = reader.IsDBNull(FLD_CURRENTZIP) ? string.Empty : reader.GetString(FLD_CURRENTZIP);
                    candidate.CurrentCountryId = reader.IsDBNull(FLD_CURRENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_CURRENTCOUNTRYID);
                    candidate.CurrentCountryName = reader.IsDBNull(FLD_CURRENTCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_CURRENTCOUNTRYNAME);
                    candidate.CurrentCountryCode = reader.IsDBNull(FLD_CURRENTCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_CURRENTCOUNTRYCODE);
                    candidate.OfficePhone = reader.IsDBNull(FLD_OFFICEPHONE) ? string.Empty : reader.GetString(FLD_OFFICEPHONE);
                    candidate.OfficePhoneExtension = reader.IsDBNull(FLD_OFFICEPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_OFFICEPHONEEXTENSION);
                    candidate.CityOfBirth = reader.IsDBNull(FLD_CITYOFBIRTH) ? string.Empty : reader.GetString(FLD_CITYOFBIRTH);
                    candidate.CountryIdOfBirth = reader.IsDBNull(FLD_COUNTRYIDOFBIRTH) ? string.Empty : reader.GetString(FLD_COUNTRYIDOFBIRTH);
                    candidate.BirthCountryName = reader.IsDBNull(FLD_BIRTHCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_BIRTHCOUNTRYNAME);
                    candidate.BirthCountryCode = reader.IsDBNull(FLD_BIRTHCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_BIRTHCOUNTRYCODE);
                    candidate.CountryIdOfCitizenship = reader.IsDBNull(FLD_COUNTRYIDOFCITIZENSHIP) ? string.Empty : reader.GetString(FLD_COUNTRYIDOFCITIZENSHIP);
                    candidate.CitizenshipCountryName = reader.IsDBNull(FLD_CITIZENSHIPCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_CITIZENSHIPCOUNTRYNAME);
                    candidate.CitizenshipCountryCode = reader.IsDBNull(FLD_CITIZENSHIPCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_CITIZENSHIPCOUNTRYCODE);
                    candidate.GenderLookupId = reader.IsDBNull(FLD_GENDERLOOKUPID) ? 0 : reader.GetInt32(FLD_GENDERLOOKUPID);
                    candidate.Gender = reader.IsDBNull(FLD_GENDER) ? string.Empty : reader.GetString(FLD_GENDER);
                    candidate.EthnicGroupLookupId = reader.IsDBNull(FLD_ETHNICGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_ETHNICGROUPLOOKUPID);
                    candidate.EthnicGroup = reader.IsDBNull(FLD_ETHNICGROUP) ? string.Empty : reader.GetString(FLD_ETHNICGROUP);
                    candidate.BloodGroupLookupId = reader.IsDBNull(FLD_BLOODGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_BLOODGROUPLOOKUPID);
                    candidate.BloodGroup = reader.IsDBNull(FLD_BLOODGROUP) ? string.Empty : reader.GetString(FLD_BLOODGROUP);
                    candidate.MaritalStatusLookupId = reader.IsDBNull(FLD_MARITALSTATUSLOOKUPID) ? 0 : reader.GetInt32(FLD_MARITALSTATUSLOOKUPID);
                    candidate.MaritalStatus = reader.IsDBNull(FLD_MARITALSTATUS) ? string.Empty : reader.GetString(FLD_MARITALSTATUS);
                    candidate.Relocation = reader.IsDBNull(FLD_RELOCATION) ? false : reader.GetBoolean(FLD_RELOCATION);
                    candidate.LastEmployer = reader.IsDBNull(FLD_LASTEMPLOYER) ? string.Empty : reader.GetString(FLD_LASTEMPLOYER);
                    candidate.TotalExperienceYears = reader.IsDBNull(FLD_TOTALEXPERIENCEYEARS) ? string.Empty : reader.GetString(FLD_TOTALEXPERIENCEYEARS);
                    candidate.Availability = reader.IsDBNull(FLD_AVAILABILITY) ? 0 : reader.GetInt32(FLD_AVAILABILITY);
                    candidate.AvailabilityText = reader.IsDBNull(FLD_AVAILABILITYTEXT) ? string.Empty : reader.GetString(FLD_AVAILABILITYTEXT);
                    candidate.AvailableDate = reader.IsDBNull(FLD_AVAILABLEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_AVAILABLEDATE);
                    candidate.CurrentYearlyRate = reader.IsDBNull(FLD_CURRENTYEARLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTYEARLYRATE);
                    candidate.CurrentYearlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTYEARLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTYEARLYCURRENCYLOOKUPID);
                    candidate.CurrentYearlyCurrency = reader.IsDBNull(FLD_CURRENTYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTYEARLYCURRENCY);
                    candidate.CurrentMonthlyRate = reader.IsDBNull(FLD_CURRENTMONTHLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTMONTHLYRATE);
                    candidate.CurrentMonthlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTMONTHLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTMONTHLYCURRENCYLOOKUPID);
                    candidate.CurrentMonthlyCurrency = reader.IsDBNull(FLD_CURRENTMONTHLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTMONTHLYCURRENCY);
                    candidate.CurrentHourlyRate = reader.IsDBNull(FLD_CURRENTHOURLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTHOURLYRATE);
                    candidate.CurrentHourlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTHOURLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTHOURLYCURRENCYLOOKUPID);
                    candidate.CurrentHourlyCurrency = reader.IsDBNull(FLD_CURRENTHOURLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTHOURLYCURRENCY);
                    candidate.ExpectedYearlyRate = reader.IsDBNull(FLD_EXPECTEDYEARLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDYEARLYRATE);
                    candidate.ExpectedYearlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDYEARLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDYEARLYCURRENCYLOOKUPID);
                    candidate.ExpectedYearlyCurrency = reader.IsDBNull(FLD_EXPECTEDYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDYEARLYCURRENCY);
                    candidate.ExpectedMonthlyRate = reader.IsDBNull(FLD_EXPECTEDMONTHLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDMONTHLYRATE);
                    candidate.ExpectedMonthlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID);
                    candidate.ExpectedMonthlyCurrency = reader.IsDBNull(FLD_EXPECTEDMONTHLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDMONTHLYCURRENCY);
                    candidate.ExpectedHourlyRate = reader.IsDBNull(FLD_EXPECTEDHOURLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDHOURLYRATE);
                    candidate.ExpectedHourlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDHOURLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDHOURLYCURRENCYLOOKUPID);
                    candidate.ExpectedHourlyCurrency = reader.IsDBNull(FLD_EXPECTEDHOURLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDHOURLYCURRENCY);
                    candidate.WillingToTravel = reader.IsDBNull(FLD_WILLINGTOTRAVEL) ? false : reader.GetBoolean(FLD_WILLINGTOTRAVEL);
                    candidate.SalaryTypeLookupId = reader.IsDBNull(FLD_SALARYTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_SALARYTYPELOOKUPID);
                    candidate.SalaryType = reader.IsDBNull(FLD_SALARYTYPE) ? string.Empty : reader.GetString(FLD_SALARYTYPE);
                    candidate.JobTypeLookupId = reader.IsDBNull(FLD_JOBTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_JOBTYPELOOKUPID);
                    candidate.JobType = reader.IsDBNull(FLD_JOBTYPE) ? string.Empty : reader.GetString(FLD_JOBTYPE);
                    candidate.SecurityClearance = reader.IsDBNull(FLD_SECURITYCLEARANCE) ? false : reader.GetBoolean(FLD_SECURITYCLEARANCE);
                    candidate.WorkAuthorizationLookupId = reader.IsDBNull(FLD_WORKAUTHORIZATIONLOOKUPID) ? 0 : reader.GetInt32(FLD_WORKAUTHORIZATIONLOOKUPID);
                    candidate.WorkAuthorization = reader.IsDBNull(FLD_WORKAUTHORIZATION) ? string.Empty : reader.GetString(FLD_WORKAUTHORIZATION);
                    candidate.CreatorName = reader.IsDBNull(FLD_CREATOR) ? string.Empty : reader.GetString(FLD_CREATOR);
                    candidate.LastUpdatorName = reader.IsDBNull(FLD_UPDATOR) ? string.Empty : reader.GetString(FLD_UPDATOR);
                    candidate.WorkScheduleLookupId = reader.IsDBNull(FLD_WORKSCEDULELOOKIPID) ? 0 : reader.GetInt32(FLD_WORKSCEDULELOOKIPID);
                    candidate.WorkSchedule = reader.IsDBNull(FLD_WORKSCEDULE) ? string.Empty : reader.GetString(FLD_WORKSCEDULE);
                    candidate.Website = reader.IsDBNull(FLD_WESITE) ? string.Empty : reader.GetString(FLD_WESITE);
                    candidate.LinkedInProfile = reader.IsDBNull(FLD_LINKEDINPROFILE) ? string.Empty : reader.GetString(FLD_LINKEDINPROFILE);
                    candidate.PassportStatus = reader.IsDBNull(FLD_PASSPORTSTATUS) ? false : reader.GetBoolean(FLD_PASSPORTSTATUS);
                    IDCard = reader.IsDBNull(FLD_IDCARD) ? string.Empty : reader.GetString(FLD_IDCARD);
                    IDCardDetail = reader.IsDBNull(FLD_IDCARDDETAIL) ? string.Empty : reader.GetString(FLD_IDCARDDETAIL);

                    if (IDCard != "")
                    {
                        candidate.IDCard = IDCard;
                        if (IDCardDetail != "")
                            candidate.IDCard += "-" + IDCardDetail;
                    }
                    else
                    {
                        candidate.IDCard = "";
                    }
                    candidate.HighestDegree = reader.IsDBNull(FLD_HIGHEREDUCATION) ? string.Empty : reader.GetString(FLD_HIGHEREDUCATION);
                    candidate.Source = reader.IsDBNull(FLD_SOURCE) ? string.Empty : reader.GetString(FLD_SOURCE);
                    candidate.SourceDescription = reader.IsDBNull(FLD_SOURCEDESCRIPTION) ? string.Empty : reader.GetString(FLD_SOURCEDESCRIPTION);

                    candidate.CandidateIndustryLookupID = reader.IsDBNull(FLD_CANDIDATEINDUSTRYLOOKUPID) ? 0 : reader.GetInt32(FLD_CANDIDATEINDUSTRYLOOKUPID);
                    candidate.CandidateIndustry = reader.IsDBNull(FLD_CANDIDATEINDUSTRY) ? string.Empty : reader.GetString(FLD_CANDIDATEINDUSTRY);
                    candidate.JobFunctionLookupID = reader.IsDBNull(FLD_JOBFUNCTIONLOOKUPID) ? 0 : reader.GetInt32(FLD_JOBFUNCTIONLOOKUPID);
                    candidate.JobFunction = reader.IsDBNull(FLD_JOBFUNCTION) ? string.Empty : reader.GetString(FLD_JOBFUNCTION);
                }
                return candidate;
            }
            //change for 5 columns added
            else
            {
                const int FLD_ID = 0;
                const int FLD_FIRSTNAME = 1;
                const int FLD_MIDDLENAME = 2;
                const int FLD_LASTNAME = 3;
                const int FLD_PRIMARYEMAIL = 4;
                const int FLD_PRIMARYPHONE = 5;
                const int FLD_PRIMARYPHONEEXTENSION = 6;
                const int FLD_CELLPHONE = 7;
                const int FLD_STATUS = 8;
                const int FLD_HOMEPHONE = 9;
                const int FLD_CURRENTCITY = 10;
                const int FLD_STATENAME = 11;
                const int FLD_STATECODE = 12;
                const int FLD_OFFICECITY = 13;
                const int FLD_CURRENTPOSITION = 14;
                const int FLD_REMARKS = 15;
                const int FLD_OBJECTIVE = 16;
                const int FLD_SUMMARY = 17;
                const int FLD_SKILLS = 18;
                const int FLD_USERID = 19;
                const int FLD_CREATORID = 20;
                const int FLD_UPDATORID = 21;
                const int FLD_CREATEDATE = 22;
                const int FLD_UPDATEDATE = 23;

                const int FLD_RECORDTYPE = 24;
                const int FLD_NICKNAME = 25;
                const int FLD_DATEOFBIRTH = 26;
                const int FLD_PERMANENTADDRESSLINE1 = 27;
                const int FLD_PERMANENTADDRESSLINE2 = 28;
                const int FLD_PERMANENTCITY = 29;
                const int FLD_PERMANENTSTATEID = 30;
                const int FLD_PERMANENTSTATENAME = 31;
                const int FLD_PERMANENTSTATECODE = 32;
                const int FLD_PERMANENTZIP = 33;
                const int FLD_PERMANENTCOUNTRYID = 34;
                const int FLD_PERMANENTCOUNTRYNAME = 35;
                const int FLD_PERMANENTCOUNTRYCODE = 36;
                const int FLD_PERMANENTPHONE = 37;
                const int FLD_PERMANENTPHONEEXT = 38;
                const int FLD_PERMANENTMOBILE = 39;
                const int FLD_ALTERNATEEMAIL = 40;
                const int FLD_RESUMESOURCE = 41;
                const int FLD_CURRENTADDRESSLINE1 = 42;
                const int FLD_CURRENTADDRESSLINE2 = 43;
                const int FLD_CURRENTSTATEID = 44;
                const int FLD_CURRENTSTATENAME = 45;
                const int FLD_CURRENTSTATECODE = 46;
                const int FLD_CURRENTZIP = 47;
                const int FLD_CURRENTCOUNTRYID = 48;
                const int FLD_CURRENTCOUNTRYNAME = 49;
                const int FLD_CURRENTCOUNTRYCODE = 50;
                const int FLD_OFFICEPHONE = 51;
                const int FLD_OFFICEPHONEEXTENSION = 52;
                const int FLD_CITYOFBIRTH = 53;
                const int FLD_COUNTRYIDOFBIRTH = 54;
                const int FLD_BIRTHCOUNTRYNAME = 55;
                const int FLD_BIRTHCOUNTRYCODE = 56;
                const int FLD_COUNTRYIDOFCITIZENSHIP = 57;
                const int FLD_CITIZENSHIPCOUNTRYNAME = 58;
                const int FLD_CITIZENSHIPCOUNTRYCODE = 59;
                const int FLD_GENDERLOOKUPID = 60;
                const int FLD_GENDER = 61;
                const int FLD_ETHNICGROUPLOOKUPID = 62;
                const int FLD_ETHNICGROUP = 63;
                const int FLD_BLOODGROUPLOOKUPID = 64;
                const int FLD_BLOODGROUP = 65;
                const int FLD_MARITALSTATUSLOOKUPID = 66;
                const int FLD_MARITALSTATUS = 67;
                const int FLD_RELOCATION = 68;
                const int FLD_LASTEMPLOYER = 69;
                const int FLD_TOTALEXPERIENCEYEARS = 70;
                const int FLD_AVAILABILITY = 71;
                const int FLD_AVAILABILITYTEXT = 72;
                const int FLD_AVAILABLEDATE = 73;
                const int FLD_CURRENTYEARLYRATE = 74;
                const int FLD_CURRENTYEARLYCURRENCYLOOKUPID = 75;
                const int FLD_CURRENTYEARLYCURRENCY = 76;
                const int FLD_CURRENTMONTHLYRATE = 77;
                const int FLD_CURRENTMONTHLYCURRENCYLOOKUPID = 78;
                const int FLD_CURRENTMONTHLYCURRENCY = 79;
                const int FLD_CURRENTHOURLYRATE = 80;
                const int FLD_CURRENTHOURLYCURRENCYLOOKUPID = 81;
                const int FLD_CURRENTHOURLYCURRENCY = 82;
                const int FLD_EXPECTEDYEARLYRATE = 83;
                const int FLD_EXPECTEDYEARLYCURRENCYLOOKUPID = 84;
                const int FLD_EXPECTEDYEARLYCURRENCY = 85;
                const int FLD_EXPECTEDMONTHLYRATE = 86;
                const int FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID = 87;
                const int FLD_EXPECTEDMONTHLYCURRENCY = 88;
                const int FLD_EXPECTEDHOURLYRATE = 89;
                const int FLD_EXPECTEDHOURLYCURRENCYLOOKUPID = 90;
                const int FLD_EXPECTEDHOURLYCURRENCY = 91;
                const int FLD_WILLINGTOTRAVEL = 92;
                const int FLD_SALARYTYPELOOKUPID = 93;
                const int FLD_SALARYTYPE = 94;
                const int FLD_JOBTYPELOOKUPID = 95;
                const int FLD_JOBTYPE = 96;
                const int FLD_SECURITYCLEARANCE = 97;
                const int FLD_WORKAUTHORIZATIONLOOKUPID = 98;
                const int FLD_WORKAUTHORIZATION = 99;
                const int FLD_CREATOR = 100;
                const int FLD_UPDATOR = 101;
                const int FLD_WORKSCEDULELOOKIPID = 102;
                const int FLD_WORKSCEDULE = 103;
                const int FLD_WESITE = 104;
                const int FLD_LINKEDINPROFILE = 105;
                const int FLD_PASSPORTSTATUS = 106;
                const int FLD_IDCARD = 107;
                const int FLD_IDCARDDETAIL = 108;
                const int FLD_HIGHEREDUCATION = 109;
                const int FLD_SOURCE = 110;
                const int FLD_SOURCEDESCRIPTION = 111;

                //5 Columns added 
                const int FLD_NOTICEPERIOD = 112;
                const int FLD_DEPARTMENT = 113;
                const int FLD_JOBCODE = 114;
                const int FLD_FEEDBACKSTATUS = 115;
                const int FLD_HIRINGMANAGER = 116;
                //5 Columns added            
                const int FLD_ReqCode = 117;
                const int FLD_HiringStatus = 118;

                Candidate candidate = new Candidate();

                candidate.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                candidate.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
                candidate.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
                candidate.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
                candidate.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
                candidate.PrimaryPhone = reader.IsDBNull(FLD_PRIMARYPHONE) ? string.Empty : reader.GetString(FLD_PRIMARYPHONE);
                candidate.PrimaryPhoneExtension = reader.IsDBNull(FLD_PRIMARYPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_PRIMARYPHONEEXTENSION);
                candidate.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
                candidate.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
                candidate.HomePhone = reader.IsDBNull(FLD_HOMEPHONE) ? string.Empty : reader.GetString(FLD_HOMEPHONE);
                candidate.CurrentCity = reader.IsDBNull(FLD_CURRENTCITY) ? string.Empty : reader.GetString(FLD_CURRENTCITY);
                candidate.StateName = reader.IsDBNull(FLD_STATENAME) ? string.Empty : reader.GetString(FLD_STATENAME);
                candidate.StateCode = reader.IsDBNull(FLD_STATECODE) ? string.Empty : reader.GetString(FLD_STATECODE);
                candidate.OfficeCity = reader.IsDBNull(FLD_OFFICECITY) ? string.Empty : reader.GetString(FLD_OFFICECITY);
                candidate.CurrentPosition = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
                candidate.Remarks = reader.IsDBNull(FLD_REMARKS) ? string.Empty : reader.GetString(FLD_REMARKS);
                candidate.Objective = reader.IsDBNull(FLD_OBJECTIVE) ? string.Empty : reader.GetString(FLD_OBJECTIVE);
                candidate.Summary = reader.IsDBNull(FLD_SUMMARY) ? string.Empty : reader.GetString(FLD_SUMMARY);
                candidate.Skills = reader.IsDBNull(FLD_SKILLS) ? string.Empty : reader.GetString(FLD_SKILLS);
                candidate.UserId = reader.IsDBNull(FLD_USERID) ? Guid.Empty : reader.GetGuid(FLD_USERID);
                candidate.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                candidate.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
                candidate.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
                //candidate.PermanentAddressLine1 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE1);
                //candidate.PermanentAddressLine2 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE2);
                candidate.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
                string strRecordType = reader.IsDBNull(FLD_RECORDTYPE) ? string.Empty : reader.GetString(FLD_RECORDTYPE);
                string IDCard = "";
                string IDCardDetail = "";
                if (strRecordType.ToUpper() == "LIST")
                {
                    candidate.LastEmployer = reader.IsDBNull(25) ? string.Empty : reader.GetString(25);
                }
                else if (strRecordType.ToUpper() == "REPORT")
                {
                    candidate.NickName = reader.IsDBNull(FLD_NICKNAME) ? string.Empty : reader.GetString(FLD_NICKNAME);
                    candidate.DateOfBirth = reader.IsDBNull(FLD_DATEOFBIRTH) ? DateTime.MinValue : reader.GetDateTime(FLD_DATEOFBIRTH);
                    candidate.PermanentAddressLine1 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE1);
                    candidate.PermanentAddressLine2 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE2);
                    candidate.PermanentCity = reader.IsDBNull(FLD_PERMANENTCITY) ? string.Empty : reader.GetString(FLD_PERMANENTCITY);
                    candidate.PermanentStateId = reader.IsDBNull(FLD_PERMANENTSTATEID) ? 0 : reader.GetInt32(FLD_PERMANENTSTATEID);
                    candidate.PermanentStateName = reader.IsDBNull(FLD_PERMANENTSTATENAME) ? string.Empty : reader.GetString(FLD_PERMANENTSTATENAME);
                    candidate.PermanentStateCode = reader.IsDBNull(FLD_PERMANENTSTATECODE) ? string.Empty : reader.GetString(FLD_PERMANENTSTATECODE);
                    candidate.PermanentZip = reader.IsDBNull(FLD_PERMANENTZIP) ? string.Empty : reader.GetString(FLD_PERMANENTZIP);
                    candidate.PermanentCountryId = reader.IsDBNull(FLD_PERMANENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_PERMANENTCOUNTRYID);
                    candidate.PermanentCountryName = reader.IsDBNull(FLD_PERMANENTCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_PERMANENTCOUNTRYNAME);
                    candidate.PermanentCountryCode = reader.IsDBNull(FLD_PERMANENTCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_PERMANENTCOUNTRYCODE);
                    candidate.PermanentPhone = reader.IsDBNull(FLD_PERMANENTPHONE) ? string.Empty : reader.GetString(FLD_PERMANENTPHONE);
                    candidate.PermanentPhoneExt = reader.IsDBNull(FLD_PERMANENTPHONEEXT) ? string.Empty : reader.GetString(FLD_PERMANENTPHONEEXT);
                    candidate.PermanentMobile = reader.IsDBNull(FLD_PERMANENTMOBILE) ? string.Empty : reader.GetString(FLD_PERMANENTMOBILE);
                    candidate.AlternateEmail = reader.IsDBNull(FLD_ALTERNATEEMAIL) ? string.Empty : reader.GetString(FLD_ALTERNATEEMAIL);
                    candidate.ResumeSource = reader.IsDBNull(FLD_RESUMESOURCE) ? 0 : reader.GetInt32(FLD_RESUMESOURCE);
                    candidate.CurrentAddressLine1 = reader.IsDBNull(FLD_CURRENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE1);
                    candidate.CurrentAddressLine2 = reader.IsDBNull(FLD_CURRENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE2);
                    candidate.CurrentStateId = reader.IsDBNull(FLD_CURRENTSTATEID) ? 0 : reader.GetInt32(FLD_CURRENTSTATEID);
                    candidate.CurrentStateName = reader.IsDBNull(FLD_CURRENTSTATENAME) ? string.Empty : reader.GetString(FLD_CURRENTSTATENAME);
                    candidate.CurrentStateCode = reader.IsDBNull(FLD_CURRENTSTATECODE) ? string.Empty : reader.GetString(FLD_CURRENTSTATECODE);
                    candidate.CurrentZip = reader.IsDBNull(FLD_CURRENTZIP) ? string.Empty : reader.GetString(FLD_CURRENTZIP);
                    candidate.CurrentCountryId = reader.IsDBNull(FLD_CURRENTCOUNTRYID) ? 0 : reader.GetInt32(FLD_CURRENTCOUNTRYID);
                    candidate.CurrentCountryName = reader.IsDBNull(FLD_CURRENTCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_CURRENTCOUNTRYNAME);
                    candidate.CurrentCountryCode = reader.IsDBNull(FLD_CURRENTCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_CURRENTCOUNTRYCODE);
                    candidate.OfficePhone = reader.IsDBNull(FLD_OFFICEPHONE) ? string.Empty : reader.GetString(FLD_OFFICEPHONE);
                    candidate.OfficePhoneExtension = reader.IsDBNull(FLD_OFFICEPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_OFFICEPHONEEXTENSION);
                    candidate.CityOfBirth = reader.IsDBNull(FLD_CITYOFBIRTH) ? string.Empty : reader.GetString(FLD_CITYOFBIRTH);
                    candidate.CountryIdOfBirth = reader.IsDBNull(FLD_COUNTRYIDOFBIRTH) ? string.Empty : reader.GetString(FLD_COUNTRYIDOFBIRTH);
                    candidate.BirthCountryName = reader.IsDBNull(FLD_BIRTHCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_BIRTHCOUNTRYNAME);
                    candidate.BirthCountryCode = reader.IsDBNull(FLD_BIRTHCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_BIRTHCOUNTRYCODE);
                    candidate.CountryIdOfCitizenship = reader.IsDBNull(FLD_COUNTRYIDOFCITIZENSHIP) ? string.Empty : reader.GetString(FLD_COUNTRYIDOFCITIZENSHIP);
                    candidate.CitizenshipCountryName = reader.IsDBNull(FLD_CITIZENSHIPCOUNTRYNAME) ? string.Empty : reader.GetString(FLD_CITIZENSHIPCOUNTRYNAME);
                    candidate.CitizenshipCountryCode = reader.IsDBNull(FLD_CITIZENSHIPCOUNTRYCODE) ? string.Empty : reader.GetString(FLD_CITIZENSHIPCOUNTRYCODE);
                    candidate.GenderLookupId = reader.IsDBNull(FLD_GENDERLOOKUPID) ? 0 : reader.GetInt32(FLD_GENDERLOOKUPID);
                    candidate.Gender = reader.IsDBNull(FLD_GENDER) ? string.Empty : reader.GetString(FLD_GENDER);
                    candidate.EthnicGroupLookupId = reader.IsDBNull(FLD_ETHNICGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_ETHNICGROUPLOOKUPID);
                    candidate.EthnicGroup = reader.IsDBNull(FLD_ETHNICGROUP) ? string.Empty : reader.GetString(FLD_ETHNICGROUP);
                    candidate.BloodGroupLookupId = reader.IsDBNull(FLD_BLOODGROUPLOOKUPID) ? 0 : reader.GetInt32(FLD_BLOODGROUPLOOKUPID);
                    candidate.BloodGroup = reader.IsDBNull(FLD_BLOODGROUP) ? string.Empty : reader.GetString(FLD_BLOODGROUP);
                    candidate.MaritalStatusLookupId = reader.IsDBNull(FLD_MARITALSTATUSLOOKUPID) ? 0 : reader.GetInt32(FLD_MARITALSTATUSLOOKUPID);
                    candidate.MaritalStatus = reader.IsDBNull(FLD_MARITALSTATUS) ? string.Empty : reader.GetString(FLD_MARITALSTATUS);
                    candidate.Relocation = reader.IsDBNull(FLD_RELOCATION) ? false : reader.GetBoolean(FLD_RELOCATION);
                    candidate.LastEmployer = reader.IsDBNull(FLD_LASTEMPLOYER) ? string.Empty : reader.GetString(FLD_LASTEMPLOYER);
                    candidate.TotalExperienceYears = reader.IsDBNull(FLD_TOTALEXPERIENCEYEARS) ? string.Empty : reader.GetString(FLD_TOTALEXPERIENCEYEARS);
                    candidate.Availability = reader.IsDBNull(FLD_AVAILABILITY) ? 0 : reader.GetInt32(FLD_AVAILABILITY);
                    candidate.AvailabilityText = reader.IsDBNull(FLD_AVAILABILITYTEXT) ? string.Empty : reader.GetString(FLD_AVAILABILITYTEXT);
                    candidate.AvailableDate = reader.IsDBNull(FLD_AVAILABLEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_AVAILABLEDATE);
                    candidate.CurrentYearlyRate = reader.IsDBNull(FLD_CURRENTYEARLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTYEARLYRATE);
                    candidate.CurrentYearlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTYEARLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTYEARLYCURRENCYLOOKUPID);
                    candidate.CurrentYearlyCurrency = reader.IsDBNull(FLD_CURRENTYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTYEARLYCURRENCY);
                    candidate.CurrentMonthlyRate = reader.IsDBNull(FLD_CURRENTMONTHLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTMONTHLYRATE);
                    candidate.CurrentMonthlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTMONTHLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTMONTHLYCURRENCYLOOKUPID);
                    candidate.CurrentMonthlyCurrency = reader.IsDBNull(FLD_CURRENTMONTHLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTMONTHLYCURRENCY);
                    candidate.CurrentHourlyRate = reader.IsDBNull(FLD_CURRENTHOURLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTHOURLYRATE);
                    candidate.CurrentHourlyCurrencyLookupId = reader.IsDBNull(FLD_CURRENTHOURLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_CURRENTHOURLYCURRENCYLOOKUPID);
                    candidate.CurrentHourlyCurrency = reader.IsDBNull(FLD_CURRENTHOURLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTHOURLYCURRENCY);
                    candidate.ExpectedYearlyRate = reader.IsDBNull(FLD_EXPECTEDYEARLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDYEARLYRATE);
                    candidate.ExpectedYearlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDYEARLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDYEARLYCURRENCYLOOKUPID);
                    candidate.ExpectedYearlyCurrency = reader.IsDBNull(FLD_EXPECTEDYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDYEARLYCURRENCY);
                    candidate.ExpectedMonthlyRate = reader.IsDBNull(FLD_EXPECTEDMONTHLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDMONTHLYRATE);
                    candidate.ExpectedMonthlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDMONTHLYCURRENCYLOOKUPID);
                    candidate.ExpectedMonthlyCurrency = reader.IsDBNull(FLD_EXPECTEDMONTHLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDMONTHLYCURRENCY);
                    candidate.ExpectedHourlyRate = reader.IsDBNull(FLD_EXPECTEDHOURLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDHOURLYRATE);
                    candidate.ExpectedHourlyCurrencyLookupId = reader.IsDBNull(FLD_EXPECTEDHOURLYCURRENCYLOOKUPID) ? 0 : reader.GetInt32(FLD_EXPECTEDHOURLYCURRENCYLOOKUPID);
                    candidate.ExpectedHourlyCurrency = reader.IsDBNull(FLD_EXPECTEDHOURLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDHOURLYCURRENCY);
                    candidate.WillingToTravel = reader.IsDBNull(FLD_WILLINGTOTRAVEL) ? false : reader.GetBoolean(FLD_WILLINGTOTRAVEL);
                    candidate.SalaryTypeLookupId = reader.IsDBNull(FLD_SALARYTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_SALARYTYPELOOKUPID);
                    candidate.SalaryType = reader.IsDBNull(FLD_SALARYTYPE) ? string.Empty : reader.GetString(FLD_SALARYTYPE);
                    candidate.JobTypeLookupId = reader.IsDBNull(FLD_JOBTYPELOOKUPID) ? 0 : reader.GetInt32(FLD_JOBTYPELOOKUPID);
                    candidate.JobType = reader.IsDBNull(FLD_JOBTYPE) ? string.Empty : reader.GetString(FLD_JOBTYPE);
                    candidate.SecurityClearance = reader.IsDBNull(FLD_SECURITYCLEARANCE) ? false : reader.GetBoolean(FLD_SECURITYCLEARANCE);
                    candidate.WorkAuthorizationLookupId = reader.IsDBNull(FLD_WORKAUTHORIZATIONLOOKUPID) ? 0 : reader.GetInt32(FLD_WORKAUTHORIZATIONLOOKUPID);
                    candidate.WorkAuthorization = reader.IsDBNull(FLD_WORKAUTHORIZATION) ? string.Empty : reader.GetString(FLD_WORKAUTHORIZATION);
                    candidate.CreatorName = reader.IsDBNull(FLD_CREATOR) ? string.Empty : reader.GetString(FLD_CREATOR);
                    candidate.LastUpdatorName = reader.IsDBNull(FLD_UPDATOR) ? string.Empty : reader.GetString(FLD_UPDATOR);
                    candidate.WorkScheduleLookupId = reader.IsDBNull(FLD_WORKSCEDULELOOKIPID) ? 0 : reader.GetInt32(FLD_WORKSCEDULELOOKIPID);
                    candidate.WorkSchedule = reader.IsDBNull(FLD_WORKSCEDULE) ? string.Empty : reader.GetString(FLD_WORKSCEDULE);
                    candidate.Website = reader.IsDBNull(FLD_WESITE) ? string.Empty : reader.GetString(FLD_WESITE);
                    candidate.LinkedInProfile = reader.IsDBNull(FLD_LINKEDINPROFILE) ? string.Empty : reader.GetString(FLD_LINKEDINPROFILE);
                    candidate.PassportStatus = reader.IsDBNull(FLD_PASSPORTSTATUS) ? false : reader.GetBoolean(FLD_PASSPORTSTATUS);
                    IDCard = reader.IsDBNull(FLD_IDCARD) ? string.Empty : reader.GetString(FLD_IDCARD);
                    IDCardDetail = reader.IsDBNull(FLD_IDCARDDETAIL) ? string.Empty : reader.GetString(FLD_IDCARDDETAIL);

                    if (IDCard != "")
                    {
                        candidate.IDCard = IDCard;
                        if (IDCardDetail != "")
                            candidate.IDCard += "-" + IDCardDetail;
                    }
                    else
                    {
                        candidate.IDCard = "";
                    }
                    candidate.HighestDegree = reader.IsDBNull(FLD_HIGHEREDUCATION) ? string.Empty : reader.GetString(FLD_HIGHEREDUCATION);
                    candidate.Source = reader.IsDBNull(FLD_SOURCE) ? string.Empty : reader.GetString(FLD_SOURCE);
                    candidate.SourceDescription = reader.IsDBNull(FLD_SOURCEDESCRIPTION) ? string.Empty : reader.GetString(FLD_SOURCEDESCRIPTION);

                    //5 Columns added 
                    candidate.NoticePeriod = reader.IsDBNull(FLD_NOTICEPERIOD) ? string.Empty : reader.GetString(FLD_NOTICEPERIOD);
                    candidate.Department = reader.IsDBNull(FLD_DEPARTMENT) ? string.Empty : reader.GetString(FLD_DEPARTMENT);
                    candidate.JobCode = reader.IsDBNull(FLD_JOBCODE) ? string.Empty : reader.GetString(FLD_JOBCODE);
                    candidate.FeedbackStatus = reader.IsDBNull(FLD_FEEDBACKSTATUS) ? string.Empty : reader.GetString(FLD_FEEDBACKSTATUS);
                    candidate.HiringManager = reader.IsDBNull(FLD_HIRINGMANAGER) ? string.Empty : reader.GetString(FLD_HIRINGMANAGER);
                   //5 Columns added 
                    if (reader.FieldCount > FLD_ReqCode)
                    {
                        candidate.ReqCode = reader.IsDBNull(FLD_ReqCode) ? string.Empty : reader.GetString(FLD_ReqCode); //added by pravin khot on 28/July/2016

                    }
                    if (reader.FieldCount > FLD_HiringStatus)
                    {
                        candidate.HiringStatus = reader.IsDBNull(FLD_HiringStatus) ? string.Empty : reader.GetString(FLD_HiringStatus); //added by pravin khot on 28/July/2016

                    }      
                }
                return candidate;
            }
        //change for 5 columns added
        }

        public IList<Candidate> BuildSearchEntities(IDataReader reader)
        {
            List<Candidate> Candidates = new List<Candidate>();

            while (reader.Read())
            {
                Candidates.Add(this.BuildSearchEntity(reader));
            }

            return (Candidates.Count > 0) ? Candidates : null;
        }

        public IList<Candidate> BuildSearchEntities(IDataReader reader,string Dispopt)
        {
            List<Candidate> Candidates = new List<Candidate>();

            while (reader.Read())
            {
                Candidates.Add(this.BuildSearchEntity_Expand(reader));
            }

            return (Candidates.Count > 0) ? Candidates : null;
        }
      

        #region Candidatesearch for presise
        public Candidate BuildSearchEntity_Expand(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_FIRSTNAME = 1;
            const int FLD_MIDDLENAME = 2;
            const int FLD_LASTNAME = 3;
            const int FLD_PRIMARYEMAIL = 4;
            const int FLD_PRIMARYPHONE = 5;
            const int FLD_PRIMARYPHONEEXTENSION = 6;
            const int FLD_CELLPHONE = 7;
            const int FLD_STATUS = 8;
            const int FLD_HOMEPHONE = 9;
            const int FLD_CURRENTCITY = 10;
            const int FLD_USERID = 11;
            const int FLD_CREATORID = 12;
            const int FLD_UPDATORID = 13;
            const int FLD_CREATEDATE = 14;
            const int FLD_UPDATEDATE = 15;

            const int FLD_PERMANENTSTATEID = 16;  //0.1
            const int FLD_AVAILABLEDATE = 17; //8669

            // 0.1       
            const int FLD_PermanentAddressLine1 = 18;
            const int FLD_PermanentAddressLine2 = 19;
            const int FLD_PermanentCity = 20;
            const int FLD_PermanentCountryId = 21;
            const int FLD_PermanentPhone = 22;
            const int FLD_PermanentMobile = 23;
            const int FLD_AlternateEmail = 24;
            const int FLD_CurrentAddressLine1 = 25;
            const int FLD_CurrentAddressLine2 = 26;
            const int FLD_CurrentStateId = 27;
            const int FLD_OfficePhone = 28;
            const int FLD_RANK = 29;
            const int FLD_CURRENTPOSITION = 30;
            const int FLD_LASTEMPLOYER = 31;
            const int FLD_WORKAUTHORIZATIONLOOKUPID = 32;
            const int FLD_CURRENTYEARLYRATE = 33;
            const int FLD_CURRENTMONTHLYRATE = 34;
            const int FLD_CURRENTHOURLYRATE = 35;
            const int FLD_AVAILABILITY = 36;
            const int FLD_EXPECTEDYEARLYRATE = 37;
            const int FLD_EXPECTEDYEARLYMAXRATE = 38;
            const int FLD_EXPECTEDMONTHLYRATE = 39;
            const int FLD_EXPECTEDMONTHLYMAXRATE = 40;
            const int FLD_EXPECTEDHOURLYRATE = 41;
            const int FLD_EXPECTEDHOURLYMAXRATE = 42;
            const int FLD_SECURITYCLEARANCE = 43;
            const int FLD_OBJECTIVE = 44;
            const int FLD_SUMMARY = 45;
            const int FLD_WORKSCHEDULELOOKUPID = 46;
            const int FLD_TOTALEXPERIENCEYEARS = 47;
            const int FLD_CURRENTSALARYPAYCYCLE = 48;
            const int FLD_EXPECTEDSALARYPAYCYCLE = 49;
            const int FLD_HIGHESTHIRINGSTATUS = 50;
            const int FLD_REMARKS = 51;
            const int FLD_MEMBERTYPE = 52;
            // 0.1
            const int FLD_WORKAUTHERIZATION = 53;
            const int FLD_AVAILABILITYTEXT = 54;
            const int FLD_CURRENTYEARLYCURRENCY = 55;
            const int FLD_EXPECTEDYEARLYCURRENCY = 56;
            const int FLD_MEMBERTYPENAME = 57;
            const int FLD_WORKSCHEDULE = 58;
            const int FLD_PERMANENTSTATENAME = 59;
            const int FLD_PRIMARYMANAGERNAME = 60;
            const int FLD_SKILL = 61;
            const int FLD_HIGHESTDEGREE = 62;


            Candidate Candidate = new Candidate();
            Candidate.DisplayOption = "Expand";
            Candidate.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            Candidate.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            Candidate.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
            Candidate.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            Candidate.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
            Candidate.PrimaryPhone = reader.IsDBNull(FLD_PRIMARYPHONE) ? string.Empty : reader.GetString(FLD_PRIMARYPHONE);
            Candidate.PrimaryPhoneExtension = reader.IsDBNull(FLD_PRIMARYPHONEEXTENSION) ? string.Empty : reader.GetString(FLD_PRIMARYPHONEEXTENSION);
            Candidate.CellPhone = reader.IsDBNull(FLD_CELLPHONE) ? string.Empty : reader.GetString(FLD_CELLPHONE);
            Candidate.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
            Candidate.HomePhone = reader.IsDBNull(FLD_HOMEPHONE) ? string.Empty : reader.GetString(FLD_HOMEPHONE);
            Candidate.CurrentCity = reader.IsDBNull(FLD_CURRENTCITY) ? string.Empty : reader.GetString(FLD_CURRENTCITY);
            Candidate.UserId = reader.IsDBNull(FLD_USERID) ? Guid.Empty : reader.GetGuid(FLD_USERID);
            Candidate.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            Candidate.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            Candidate.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            Candidate.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            Candidate.PermanentStateId = reader.IsDBNull(FLD_PERMANENTSTATEID) ? 0 : reader.GetInt32(FLD_PERMANENTSTATEID); //0.1    
            Candidate.AvailableDate = reader.IsDBNull(FLD_AVAILABLEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_AVAILABLEDATE); //8669

            // 0.1
            Candidate.PermanentAddressLine1 = reader.IsDBNull(FLD_PermanentAddressLine1) ? string.Empty : reader.GetString(FLD_PermanentAddressLine1);
            Candidate.PermanentAddressLine1 = reader.IsDBNull(FLD_PermanentAddressLine2) ? string.Empty : reader.GetString(FLD_PermanentAddressLine2);
            Candidate.PermanentCity = reader.IsDBNull(FLD_PermanentCity) ? string.Empty : reader.GetString(FLD_PermanentCity);
            Candidate.PermanentCountryId = reader.IsDBNull(FLD_PermanentCountryId) ? 0 : reader.GetInt32(FLD_PermanentCountryId);
            Candidate.PermanentPhone = reader.IsDBNull(FLD_PermanentPhone) ? string.Empty : reader.GetString(FLD_PermanentPhone);
            Candidate.PermanentMobile = reader.IsDBNull(FLD_PermanentMobile) ? string.Empty : reader.GetString(FLD_PermanentMobile);
            Candidate.AlternateEmail = reader.IsDBNull(FLD_AlternateEmail) ? string.Empty : reader.GetString(FLD_AlternateEmail);
            Candidate.CurrentAddressLine1 = reader.IsDBNull(FLD_CurrentAddressLine1) ? string.Empty : reader.GetString(FLD_CurrentAddressLine1);
            Candidate.CurrentAddressLine2 = reader.IsDBNull(FLD_CurrentAddressLine2) ? string.Empty : reader.GetString(FLD_CurrentAddressLine2);
            Candidate.CurrentStateId = reader.IsDBNull(FLD_CurrentStateId) ? 0 : reader.GetInt32(FLD_CurrentStateId);
            Candidate.OfficePhone = reader.IsDBNull(FLD_OfficePhone) ? string.Empty : reader.GetString(FLD_OfficePhone);

            if (reader.FieldCount > 29)
            {
                Candidate.Rank = reader.IsDBNull(FLD_RANK) ? string.Empty : reader.GetString(FLD_RANK);
                //
                Candidate.CurrentPosition = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
                Candidate.LastEmployer = reader.IsDBNull(FLD_LASTEMPLOYER) ? string.Empty : reader.GetString(FLD_LASTEMPLOYER);
                Candidate.WorkAuthorizationLookupId = reader.IsDBNull(FLD_WORKAUTHORIZATIONLOOKUPID) ? 0 : reader.GetInt32(FLD_WORKAUTHORIZATIONLOOKUPID);
                Candidate.CurrentYearlyRate = reader.IsDBNull(FLD_CURRENTYEARLYRATE) ? 0 : reader.GetDecimal(FLD_CURRENTYEARLYRATE);
                Candidate.Availability = reader.IsDBNull(FLD_AVAILABILITY) ? 0 : reader.GetInt32(FLD_AVAILABILITY);
                Candidate.ExpectedYearlyRate = reader.IsDBNull(FLD_EXPECTEDYEARLYRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDYEARLYRATE);
                Candidate.ExpectedYearlyMaxRate = reader.IsDBNull(FLD_EXPECTEDYEARLYMAXRATE) ? 0 : reader.GetDecimal(FLD_EXPECTEDYEARLYMAXRATE);

                Candidate.SecurityClearance = reader.IsDBNull(FLD_SECURITYCLEARANCE) ? false : reader.GetBoolean(FLD_SECURITYCLEARANCE);
                Candidate.Objective = reader.IsDBNull(FLD_OBJECTIVE) ? string.Empty : reader.GetString(FLD_OBJECTIVE);
                Candidate.Summary = reader.IsDBNull(FLD_SUMMARY) ? string.Empty : reader.GetString(FLD_SUMMARY);
                Candidate.WorkScheduleLookupId = reader.IsDBNull(FLD_WORKSCHEDULELOOKUPID) ? 0 : reader.GetInt32(FLD_WORKSCHEDULELOOKUPID);
                Candidate.TotalExperienceYears = reader.IsDBNull(FLD_TOTALEXPERIENCEYEARS) ? "" : reader.GetString(FLD_TOTALEXPERIENCEYEARS);
                Candidate.CurrentSalaryPayCycle = reader.IsDBNull(FLD_CURRENTSALARYPAYCYCLE) ? 3 : reader.GetInt32(FLD_CURRENTSALARYPAYCYCLE);
                Candidate.ExpectedSalaryPayCycle = reader.IsDBNull(FLD_EXPECTEDSALARYPAYCYCLE) ? 3 : reader.GetInt32(FLD_EXPECTEDSALARYPAYCYCLE);
                Candidate.HighestHiringStatus = reader.IsDBNull(FLD_HIGHESTHIRINGSTATUS) ? "" : reader.GetString(FLD_HIGHESTHIRINGSTATUS);
                Candidate.Remarks = reader.IsDBNull(FLD_REMARKS) ? "" : reader.GetString(FLD_REMARKS);
                Candidate.MemberType = reader.IsDBNull(FLD_MEMBERTYPE) ? 0 : reader.GetInt32(FLD_MEMBERTYPE);
            }
            if (reader.FieldCount > 53)
            {
                Candidate.WorkAuthorization = reader.IsDBNull(FLD_WORKAUTHERIZATION) ? string.Empty : reader.GetString(FLD_WORKAUTHERIZATION);
                Candidate.AvailabilityText = reader.IsDBNull(FLD_AVAILABILITYTEXT) ? string.Empty : reader.GetString(FLD_AVAILABILITYTEXT);
                Candidate.CurrentYearlyCurrency = reader.IsDBNull(FLD_CURRENTYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_CURRENTYEARLYCURRENCY);
                Candidate.ExpectedYearlyCurrency = reader.IsDBNull(FLD_EXPECTEDYEARLYCURRENCY) ? string.Empty : reader.GetString(FLD_EXPECTEDYEARLYCURRENCY);
                Candidate.MemberTypeName = reader.IsDBNull(FLD_MEMBERTYPENAME) ? string.Empty : reader.GetString(FLD_MEMBERTYPENAME); ;
                Candidate.WorkSchedule = reader.IsDBNull(FLD_WORKSCHEDULE) ? string.Empty : reader.GetString(FLD_WORKSCHEDULE); ;
                Candidate.PermanentStateName = reader.IsDBNull(FLD_PERMANENTSTATENAME) ? string.Empty : reader.GetString(FLD_PERMANENTSTATENAME); ;
                Candidate.PrimaryManagerName = reader.IsDBNull(FLD_PRIMARYMANAGERNAME) ? string.Empty : reader.GetString(FLD_PRIMARYMANAGERNAME);
                Candidate.Skills = reader.IsDBNull(FLD_SKILL) ? string.Empty : reader.GetString(FLD_SKILL);
                Candidate.HighestDegree = reader.IsDBNull(FLD_HIGHESTDEGREE) ? string.Empty : reader.GetString(FLD_HIGHESTDEGREE);
            }

            // 0.1
            return Candidate;
        }
        #endregion

        public Candidate BuildSearchEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_CandidateName = 1;
            const int FLD_CurrentPosition = 2;
            const int FLD_PrimaryEmail = 3;
            const int FLD_CellPhone = 4;
            const int FLD_CurrentCity = 5;
            const int FLD_CurrentStateId = 6;
            const int FLD_Rank = 7;
            const int FLD_TOTALEXPERIENCEYEARS = 8;
            const int FLD_HiringStatus = 9;
            const int FLD_SourceName = 10;
            const int FLD_SourceDescription = 11;

            Candidate Candidate = new Candidate();
            Candidate.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            Candidate.FirstName = reader.IsDBNull(FLD_CandidateName) ? string.Empty : reader.GetString(FLD_CandidateName);
            Candidate.CurrentPosition = reader.IsDBNull(FLD_CurrentPosition) ? string.Empty : reader.GetString(FLD_CurrentPosition);
            Candidate.PrimaryEmail = reader.IsDBNull(FLD_PrimaryEmail) ? string.Empty : reader.GetString(FLD_PrimaryEmail);
            Candidate.CellPhone = reader.IsDBNull(FLD_CellPhone) ? string.Empty : reader.GetString(FLD_CellPhone);
            Candidate.CurrentCity = reader.IsDBNull(FLD_CurrentCity) ? string.Empty : reader.GetString(FLD_CurrentCity);
            Candidate.CurrentStateId = reader.IsDBNull(FLD_CurrentStateId) ? 0 : reader.GetInt32(FLD_CurrentStateId);
            Candidate.TotalExperienceYears = reader.IsDBNull(FLD_TOTALEXPERIENCEYEARS) ? "" : reader.GetString(FLD_TOTALEXPERIENCEYEARS);
            Candidate.Rank = reader.IsDBNull(FLD_Rank) ? string.Empty : reader.GetString(FLD_Rank);

            Candidate.HighestHiringStatus = reader.IsDBNull(FLD_HiringStatus) ? string.Empty : reader.GetString(FLD_HiringStatus);

            if (reader.FieldCount > FLD_SourceName)
            {
                Candidate.Source = reader.IsDBNull(FLD_SourceName) ? string.Empty : reader.GetString(FLD_SourceName);
                Candidate.SourceDescription = reader.IsDBNull(FLD_SourceDescription) ? string.Empty : reader.GetString(FLD_SourceDescription);
            }
            return Candidate;
        }
        public Candidate BuildSearchEntity_Sub(IDataReader reader)
        {
            const int FLD_PMgr = 0;
            const int FLD_Skills = 1;
            const int FLD_Availability = 2;
            const int FLD_WorkSchedule = 3;
            const int FLD_WorkStatus = 4;
            const int FLD_Objective = 5;
            const int FLD_Remarks = 6;

            const int FLD_CurrentYearlyRate = 7;
            const int FLD_CurrentMonthlyRate = 8;
            const int FLD_CurrentHourlyRate = 9;
            const int FLD_ExpectedYearlyRate = 10;
            const int FLD_ExpectedYearlyMaxRate = 11;
            const int FLD_ExpectedMonthlyRate = 12;
            const int FLD_ExpectedMonthlyMaxRate = 13;
            const int FLD_ExpectedHourlyRate = 14;
            const int FLD_ExpectedHourlyMaxRate = 15;


            const int FLD_CurrentSalaryCurrency = 16;
            const int FLD_ExpectedSalaryCurrency = 17;
            const int FLD_CurrentCompany = 18;
            const int FLD_HighestDegree = 19;
            const int FLD_CandidateType = 20;
            const int FLD_LastUpdated = 21;
            const int FLD_CurrentSalaryPayCycle = 22;
            const int FLD_ExpectedSalaryPayCycle = 23;
            const int FLD_AvailableDate = 24;

            Candidate Candidate = new Candidate();

            Candidate.PrimaryManagerName = reader.IsDBNull(FLD_PMgr) ? string.Empty : reader.GetString(FLD_PMgr);
            Candidate.Skills = reader.IsDBNull(FLD_Skills) ? string.Empty : reader.GetString(FLD_Skills);
            Candidate.AvailabilityText = reader.IsDBNull(FLD_Availability) ? string.Empty : reader.GetString(FLD_Availability);
            Candidate.WorkSchedule = reader.IsDBNull(FLD_WorkSchedule) ? string.Empty : reader.GetString(FLD_WorkSchedule);
            Candidate.WorkAuthorization = reader.IsDBNull(FLD_WorkStatus) ? string.Empty : reader.GetString(FLD_WorkStatus);
            Candidate.Objective = reader.IsDBNull(FLD_Objective) ? string.Empty : reader.GetString(FLD_Objective);
            Candidate.Remarks = reader.IsDBNull(FLD_Remarks) ? string.Empty : reader.GetString(FLD_Remarks);

            Candidate.CurrentYearlyRate = reader.IsDBNull(FLD_CurrentYearlyRate) ? 0 : reader.GetDecimal(FLD_CurrentYearlyRate);
            Candidate.CurrentMonthlyRate = reader.IsDBNull(FLD_CurrentMonthlyRate) ? 0 : reader.GetDecimal(FLD_CurrentMonthlyRate);
            Candidate.CurrentHourlyRate = reader.IsDBNull(FLD_CurrentHourlyRate) ? 0 : reader.GetDecimal(FLD_CurrentHourlyRate);
            Candidate.ExpectedYearlyRate = reader.IsDBNull(FLD_ExpectedYearlyRate) ? 0 : reader.GetDecimal(FLD_ExpectedYearlyRate);
            Candidate.ExpectedYearlyMaxRate = reader.IsDBNull(FLD_ExpectedYearlyMaxRate) ? 0 : reader.GetDecimal(FLD_ExpectedYearlyMaxRate);
            Candidate.ExpectedMonthlyRate = reader.IsDBNull(FLD_ExpectedMonthlyRate) ? 0 : reader.GetDecimal(FLD_ExpectedMonthlyRate);
            Candidate.ExpectedMonthlyMaxRate = reader.IsDBNull(FLD_ExpectedMonthlyMaxRate) ? 0 : reader.GetDecimal(FLD_ExpectedMonthlyMaxRate);
            Candidate.ExpectedHourlyRate = reader.IsDBNull(FLD_ExpectedHourlyRate) ? 0 : reader.GetDecimal(FLD_ExpectedHourlyRate);
            Candidate.ExpectedHourlyMaxRate = reader.IsDBNull(FLD_ExpectedHourlyMaxRate) ? 0 : reader.GetDecimal(FLD_ExpectedHourlyMaxRate);

            Candidate.CurrentYearlyCurrency = reader.IsDBNull(FLD_CurrentSalaryCurrency) ? string.Empty : reader.GetString(FLD_CurrentSalaryCurrency);
            Candidate.ExpectedYearlyCurrency = reader.IsDBNull(FLD_ExpectedSalaryCurrency) ? string.Empty : reader.GetString(FLD_ExpectedSalaryCurrency);
            Candidate.CurrentPosition = reader.IsDBNull(FLD_CurrentCompany) ? string.Empty : reader.GetString(FLD_CurrentCompany);
            Candidate.HighestDegree = reader.IsDBNull(FLD_HighestDegree) ? string.Empty : reader.GetString(FLD_HighestDegree);
            Candidate.MemberTypeName = reader.IsDBNull(FLD_CandidateType) ? string.Empty : reader.GetString(FLD_CandidateType);
            Candidate.UpdateDate = reader.IsDBNull(FLD_LastUpdated) ? new DateTime() : reader.GetDateTime(FLD_LastUpdated);
            Candidate.ExpectedSalaryPayCycle = reader.IsDBNull(FLD_ExpectedSalaryPayCycle) ? 0 : reader.GetInt32(FLD_ExpectedSalaryPayCycle);
            Candidate.CurrentSalaryPayCycle = reader.IsDBNull(FLD_CurrentSalaryPayCycle) ? 0 : reader.GetInt32(FLD_CurrentSalaryPayCycle);
            Candidate.AvailableDate = reader.IsDBNull(FLD_AvailableDate) ? new DateTime() : reader.GetDateTime(FLD_AvailableDate);
            return Candidate;
        }

        //*************Code added by pravin khot on 4/March/2016*******START***********
        public IList<Candidate> BuildCDMEntities(IDataReader reader)
        {
            List<Candidate> Candidates = new List<Candidate>();

            while (reader.Read())
            {
                Candidates.Add(this.BuildCDMSearch(reader));
            }

            return (Candidates.Count > 0) ? Candidates : null;
        }
        public Candidate BuildCDMSearch(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_TITLE = 1;
            const int FLD_FIRSTNAME = 2;
            const int FLD_MIDDLENAME = 3;
            const int FLD_LASTNAME = 4;
            const int FLD_PRIMARYEMAIL = 5;
            const int FLD_FATHERNAME = 6;
            const int FLD_DATEOFBIRTH = 7;
            const int FLD_GENDER = 8;
            const int FLD_DEPARTMENT = 9;
            const int FLD_CURRENTPOSITION = 10;
            const int FLD_PERMANENTADDRESSLINE1 = 11;
            const int FLD_PERMANENTADDRESSLINE2 = 12;
            const int FLD_PERMANENTCITY = 13;
            const int FLD_PERMANENTSTATENAME = 14;
            const int FLD_PERMANENTZIP = 15;
            const int FLD_PERMANENTMOBILE = 16;
            const int FLD_CURRENTADDRESSLINE1 = 17;
            const int FLD_CURRENTADDRESSLINE2 = 18;
            const int FLD_CURRENTCITY = 19;
            const int FLD_CURRENTSTATENAME = 20;
             const int FLD_CURRENTZIP = 21;

             const int FLD_Band = 22;
                   const int FLD_structure = 23;
                        const int FLD_InputComponent = 24;
                       const int FLD_ExpectedDOJ = 25;
                        const int FLD_BondName = 26;
                        const int FLD_Bondamount = 27;
						const int FLD_Bondfromdate = 28;
						const int FLD_Bondenddate = 29;
                        const int FLD_MarkLetterTo = 30;
                        const int FLD_HiringSourceName = 31;
                        const int FLD_HiringCost = 32;
                        const int FLD_Hiringcomment = 33;
                      

                        const int FLD_OFFICECITY = 34;
                        const int FLD_BRANCH = 35;
                        const int FLD_MARITALSTATUS = 36;
                        const int FLD_RequisitionDate = 37;
                        const int FLD_WeddingAnniversary = 38;
                        const int FLD_project = 39;
                        const int FLD_CareerLevel = 40;
                        const int FLD_BenefitBand = 41;
                        const int FLD_SALARYTYPE = 42;
                        const int FLD_JobRole = 43;
                        const int FLD_JOBCODE = 44;
                        const int FLD_JoiningDate = 45;      
         
            Candidate candidate = new Candidate();

            candidate.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            candidate.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString(FLD_TITLE);
            candidate.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            candidate.MiddleName = reader.IsDBNull(FLD_MIDDLENAME) ? string.Empty : reader.GetString(FLD_MIDDLENAME);
            candidate.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            candidate.PrimaryEmail = reader.IsDBNull(FLD_PRIMARYEMAIL) ? string.Empty : reader.GetString(FLD_PRIMARYEMAIL);
            candidate.FatherName = reader.IsDBNull(FLD_FATHERNAME) ? string.Empty : reader.GetString(FLD_FATHERNAME);
            candidate.DateOfBirth = reader.IsDBNull(FLD_DATEOFBIRTH) ? DateTime.MinValue : reader.GetDateTime(FLD_DATEOFBIRTH);
            candidate.Gender = reader.IsDBNull(FLD_GENDER) ? string.Empty : reader.GetString(FLD_GENDER);
            candidate.Department = reader.IsDBNull(FLD_DEPARTMENT) ? string.Empty : reader.GetString(FLD_DEPARTMENT);
            candidate.CurrentPosition = reader.IsDBNull(FLD_CURRENTPOSITION) ? string.Empty : reader.GetString(FLD_CURRENTPOSITION);
            candidate.PermanentAddressLine1 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE1);
            candidate.PermanentAddressLine2 = reader.IsDBNull(FLD_PERMANENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_PERMANENTADDRESSLINE2);
            candidate.PermanentCity = reader.IsDBNull(FLD_PERMANENTCITY) ? string.Empty : reader.GetString(FLD_PERMANENTCITY);
            candidate.PermanentStateName = reader.IsDBNull(FLD_PERMANENTSTATENAME) ? string.Empty : reader.GetString(FLD_PERMANENTSTATENAME);
            candidate.PermanentZip = reader.IsDBNull(FLD_PERMANENTZIP) ? string.Empty : reader.GetString(FLD_PERMANENTZIP);
            candidate.PermanentMobile = reader.IsDBNull(FLD_PERMANENTMOBILE) ? string.Empty : reader.GetString(FLD_PERMANENTMOBILE);
            candidate.CurrentAddressLine1 = reader.IsDBNull(FLD_CURRENTADDRESSLINE1) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE1);
            candidate.CurrentAddressLine2 = reader.IsDBNull(FLD_CURRENTADDRESSLINE2) ? string.Empty : reader.GetString(FLD_CURRENTADDRESSLINE2);
            candidate.CurrentCity = reader.IsDBNull(FLD_CURRENTCITY) ? string.Empty : reader.GetString(FLD_CURRENTCITY);
            candidate.CurrentStateName = reader.IsDBNull(FLD_CURRENTSTATENAME) ? string.Empty : reader.GetString(FLD_CURRENTSTATENAME);
            candidate.CurrentZip = reader.IsDBNull(FLD_CURRENTZIP) ? string.Empty : reader.GetString(FLD_CURRENTZIP);
            candidate.Band = reader.IsDBNull(FLD_Band) ? string.Empty : reader.GetString(FLD_Band);
            candidate.structure = reader.IsDBNull(FLD_structure) ? string.Empty : reader.GetString(FLD_structure);
            candidate.InputComponent = reader.IsDBNull(FLD_InputComponent) ? string.Empty : reader.GetString(FLD_InputComponent);
            candidate.ExpectedDOJ = reader.IsDBNull(FLD_ExpectedDOJ) ? DateTime.MinValue : reader.GetDateTime(FLD_ExpectedDOJ);
            candidate.BondName = reader.IsDBNull(FLD_BondName) ? string.Empty : reader.GetString(FLD_BondName);
            candidate.Bondamount = reader.IsDBNull(FLD_Bondamount) ? string.Empty : reader.GetString(FLD_Bondamount);
            candidate.Bondfromdate = reader.IsDBNull(FLD_Bondfromdate) ? DateTime.MinValue : reader.GetDateTime(FLD_Bondfromdate);
            candidate.Bondenddate = reader.IsDBNull(FLD_Bondenddate) ? DateTime.MinValue : reader.GetDateTime(FLD_Bondenddate);
            candidate.MarkLetterTo = reader.IsDBNull(FLD_MarkLetterTo) ? string.Empty : reader.GetString(FLD_MarkLetterTo);
            candidate.HiringSourceName = reader.IsDBNull(FLD_HiringSourceName) ? string.Empty : reader.GetString(FLD_HiringSourceName);
            candidate.HiringCost = reader.IsDBNull(FLD_HiringCost) ? string.Empty : reader.GetString(FLD_HiringCost);
            candidate.Hiringcomment = reader.IsDBNull(FLD_Hiringcomment) ? string.Empty : reader.GetString(FLD_Hiringcomment);
            candidate.OfficeCity = reader.IsDBNull(FLD_OFFICECITY) ? string.Empty : reader.GetString(FLD_OFFICECITY);
            candidate.Branch = reader.IsDBNull(FLD_BRANCH) ? string.Empty : reader.GetString(FLD_BRANCH);
            candidate.MaritalStatus  = reader.IsDBNull(FLD_MARITALSTATUS) ? string.Empty : reader.GetString(FLD_MARITALSTATUS);
            candidate.ReqDate = reader.IsDBNull(FLD_RequisitionDate) ? DateTime.MinValue : reader.GetDateTime(FLD_RequisitionDate);
            candidate.WeddingDate = reader.IsDBNull(FLD_WeddingAnniversary) ? DateTime.MinValue : reader.GetDateTime(FLD_WeddingAnniversary);
            candidate.project  = reader.IsDBNull(FLD_project) ? string.Empty : reader.GetString(FLD_project);
            candidate.CareerLevel = reader.IsDBNull(FLD_CareerLevel) ? string.Empty : reader.GetString(FLD_CareerLevel);
            candidate.BenefitBand = reader.IsDBNull(FLD_BenefitBand) ? string.Empty : reader.GetString(FLD_BenefitBand);
            candidate.SalaryType = reader.IsDBNull(FLD_SALARYTYPE) ? string.Empty : reader.GetString(FLD_SALARYTYPE);
            candidate.JobRole = reader.IsDBNull(FLD_JobRole) ? string.Empty : reader.GetString(FLD_JobRole);
            candidate.JobCode = reader.IsDBNull(FLD_JOBCODE) ? string.Empty : reader.GetString(FLD_JOBCODE);
           // candidate.JoiningDate = reader.IsDBNull(FLD_JoiningDate) ? string.Empty : reader.GetString(FLD_JoiningDate);
            candidate.JoiningDate = reader.IsDBNull(FLD_JoiningDate) ? DateTime.MinValue : reader.GetDateTime(FLD_JoiningDate);

            return candidate;
        }

        //*********************END*************************************
   
    }
}
