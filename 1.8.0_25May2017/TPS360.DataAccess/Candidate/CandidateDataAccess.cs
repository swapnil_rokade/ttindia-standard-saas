﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CandidateDataAccess.cs
    Description: This page is used for candidate data access.
    Created By: 
    Created On:
    Modification Log:
    ----------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ----------------------------------------------------------------------------------------------------------------------------------------    
    0.1            Nov-18-2008        Shivanand        Defect #9313; In the method GetSearchQuery(),whereClause is constructed with IN clause 
                                                            using "workStatus" parameter.
    0.2            Dec-02-2008        Shivanand        Defect #8745; 
                                                            In the method BuildAllKeyWordsQuery(),new parameters are added ie [CurrentCity,PrimaryPhone,CellPhone,Homephone]
                                                            and are used to build the query.
                                                            In the method BuildAnyKeyWordQuery(),new parameters are added ie [CurrentCity,PrimaryPhone,CellPhone,Homephone]
                                                            and are used to build the query.
                                                            In the method GetSearchQuery(),new parameter "state" is added & used to build whereclause.
                                                            In the method GetAllOnSearch(),new parameter "strState" is added & used to call method GetSearchQuery().
                                                            In the method BuildAllKeyWordsQuery() & BuildAnyKeyWordQuery(), new parameters are added.
                                                            In the method GetSearchQuery(), where clause construction logic is changed.
 * 0.3         Apr-2-2009           Sandeesh             Defect Id: #10170 : Query optimation for precise Search  
 * 0.4         Apr-7-2009           Sandeesh             Defect Id: #10170 : Query optimation for precise Search with mulitiple keywords 
*  0.5         Apr-29-2009          Sandeesh            Defect Id : #10378  Combined the "All Keywords" and "Any Keyword" searching boxes into one "Keyword Search" box that supports boolean syntax searching.
   0.6         May-20-2009          Shivanand           Defect #10452; Changes made in method GetSearchQuery().
   0.7         Aug-20-2009          Shivanand           Defect #10714; Changes made in method GetSearchQuery().
 * 0.8         Mar-15-2010          Sudarshan R         Defect ID:12297; Changed made in the method GetPagedForReport(). Changed Current country,state,city to Permanent country,state,city
   0.9         12/Jan/2016          Pravin khot          Introduced by GetPagedCarrerPortalList
 * 1.0         4/March/2016         pravin khot          Introduced by GetPagedCandidateCDMDetailsInfoReport 
 * 1.1         16/March/2016        pravin khot          In function GetPagedCandidateCDMDetailsInfoReport change convert date format of  [G8].[JoiningDate]
 * 1.2         23/May/2016          Sumit Sonawane
   1.3         8/Aug/2016           pravin khot         modify - GetAllOnSearchForPrecises
 * 1.4         20/Jan/2017          Sumit Sonawane      added GetCandidateAgeByMemberID
 * ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 */

using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using TPS360.Common.BusinessEntity;
// using System.Web.Configuration;

namespace TPS360.DataAccess
{
    internal sealed class CandidateDataAccess : BaseDataAccess, ICandidateDataAccess
    {
        #region Constructors

        public CandidateDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<Candidate> CreateEntityBuilder<Candidate>()
        {
            return (new CandidateBuilder()) as IEntityBuilder<Candidate>;
        }

        #endregion

        #region  Methods

        Candidate ICandidateDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Candidate_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Candidate>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        //
        CandidateOverviewDetails ICandidateDataAccess.GetCandidateOverviewDetails(int CandidateId)
        {
            const string SP = "dbo.Candidate_GetCandidateOverviewDetails";
            CandidateOverviewDetails overviewdetails = new CandidateOverviewDetails(); ;
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CandidateId", DbType.Int32, CandidateId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {

                        overviewdetails.Id = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        overviewdetails.CandidateName = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
                        overviewdetails.JobTitle = reader.IsDBNull(2) ? string.Empty : reader.GetString(2);
                        overviewdetails.PrimaryManager = reader.IsDBNull(3) ? string.Empty : reader.GetString(3);
                        overviewdetails.CandidateEmail = reader.IsDBNull(4) ? string.Empty : reader.GetString(4);
                        overviewdetails.CurrentCompany = reader.IsDBNull(5) ? string.Empty : reader.GetString(5);
                        overviewdetails.InternalRating = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                        overviewdetails.Mobile = reader.IsDBNull(7) ? string.Empty : reader.GetString(7);
                        overviewdetails.YearsOfExperience = reader.IsDBNull(8) ? string.Empty : reader.GetString(8);
                        overviewdetails.ResumeSourceId = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);
                        overviewdetails.ResumeSourceName = reader.IsDBNull(10) ? string.Empty : reader.GetString(10);
                        overviewdetails.Status = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
                        overviewdetails.Location = reader.IsDBNull(12) ? string.Empty : reader.GetString(12);
                        overviewdetails.HighestEducation = reader.IsDBNull(13) ? string.Empty : reader.GetString(13);
                        overviewdetails.CandidateType = reader.IsDBNull(14) ? string.Empty : reader.GetString(14);
                        overviewdetails.Availability = reader.IsDBNull(15) ? string.Empty : reader.GetString(15);
                        overviewdetails.LastUpdated = reader.IsDBNull(16) ? DateTime.MinValue : reader.GetDateTime(16);
                        overviewdetails.Remarks = reader.IsDBNull(17) ? string.Empty : reader.GetString(17);
                        overviewdetails.CurrentCTC = reader.IsDBNull(18) ? string.Empty : reader.GetString(18);
                        overviewdetails.ExpectedCTC = reader.IsDBNull(19) ? string.Empty : reader.GetString(19);
                        overviewdetails.NoticePeriod = reader.IsDBNull(20) ? string.Empty : reader.GetString(20);

                        overviewdetails.Notes = reader.IsDBNull(21) ? string.Empty : reader.GetString(21);
                        overviewdetails.Categary = reader.IsDBNull(22) ? string.Empty : reader.GetString(22);
                        overviewdetails.CreateDate = reader.IsDBNull(23) ? DateTime.MinValue : reader.GetDateTime(23);
                        overviewdetails.MemberName = reader.IsDBNull(24) ? string.Empty : reader.GetString(24);
                        return overviewdetails;
                    }
                    else
                        return null;
                }
            }
        }
        //

        IList<Candidate> ICandidateDataAccess.GetAllCandidateByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.Candidate_GetByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return (CreateEntityBuilder<Candidate>()).BuildEntities(reader);
                }
            }
        }

        Candidate ICandidateDataAccess.GetByIdForHR(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.Candidate_GetByIdForHR";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<Candidate>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        //*****************************Code added by pravin khot on 12/Jan/2016*******************

        PagedResponse<Candidate> ICandidateDataAccess.GetPagedCarrerPortalList(PagedRequest request)
        {
            const string SP = "dbo.MemberCareerPortalList_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");
                        if (StringHelper.IsEqual(column, "CandidateID"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[Id] =");
                            sb.Append(value);
                        }
                        if (StringHelper.IsEqual(column, "memberId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[Id]");
                            sb.Append(" IN ");
                            sb.Append(" ( SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId= ");
                            sb.Append(value);
                            sb.Append(" ) ");
                        }
                        if (StringHelper.IsEqual(column, "CandidateName"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                char[] delim = { ' ' };
                                string[] valueList = value.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                                if (valueList.Length == 2)
                                {
                                    sb.Append(" ([C].[FirstName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[0]);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[LastName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[1]);
                                    sb.Append("%') ");
                                }
                                else if (valueList.Length == 3)
                                {
                                    sb.Append(" ([C].[FirstName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[0]);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[LastName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[1]);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[MiddleName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[2]);
                                    sb.Append("%') ");
                                }
                                else
                                {
                                    sb.Append(" ([C].[FirstName]");
                                    sb.Append(" like '%");
                                    sb.Append(value);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[LastName]");
                                    sb.Append(" like '%");
                                    sb.Append(value);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[MiddleName]");
                                    sb.Append(" like '%");
                                    sb.Append(value);
                                    sb.Append("%') ");
                                }

                            }
                        }
                        if (StringHelper.IsEqual(column, "CandidateEmail"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append("( [C].[PrimaryEmail]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                                sb.Append(" or ");
                                sb.Append("[C].[AlternateEmail]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                                sb.Append(") ");
                            }
                        }
                        if (StringHelper.IsEqual(column, "Position"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[CurrentPosition]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                            }
                        }

                        if (StringHelper.IsEqual(column, "AssignedManager"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[ID]");
                                sb.Append(" IN (");
                                sb.Append("SELECT DISTINCT [MemberId] FROM [MemberManager] [MM] WHERE  [MM].[ManagerId] =");
                                sb.Append(value);
                                sb.Append(")");
                            }
                        }
                        if (StringHelper.IsEqual(column, "HiringLevel"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [MJCD].[SelectionStepLookupId]");
                                sb.Append(" =");
                                sb.Append(value);

                            }
                        }
                        //if (StringHelper.IsEqual(column, "MobilePhone"))
                        //{
                        //    if (value.Trim() != string.Empty)
                        //    {
                        //        if (sb.ToString() != String.Empty)
                        //        {
                        //            sb.Append(" AND ");
                        //        }
                        //        sb.Append("( [C].[PermanentMobile]");
                        //        sb.Append(" like '%");
                        //        sb.Append(value);
                        //        sb.Append("%' ");
                        //        sb.Append(" or ");
                        //        sb.Append(" [C].[CellPhone]");
                        //        sb.Append(" like '%");
                        //        sb.Append(value);
                        //        sb.Append("%' ");
                        //        sb.Append(" ) ");
                        //    }
                        //}
                        if (StringHelper.IsEqual(column, "City"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[PermanentCity]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                            }
                        }
                        if (StringHelper.IsEqual(column, "StateId"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[PermanentStateid]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }

                        if (StringHelper.IsEqual(column, "CountryId"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[PermanentCountryID]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }
                        if (StringHelper.IsEqual(column, "CandidateType"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[MemberType]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }
                        if (StringHelper.IsEqual(column, "JobTitle"))
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[JobTitle]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");
                            }
                        }
                        if (StringHelper.IsEqual(column, "ReqCode"))
                        {
                            if (value.Trim() != "")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[JobPostingCode]");
                                sb.Append(" LIKE '");
                                if (!value.StartsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append(value);
                                if (!value.EndsWith("%"))
                                {
                                    sb.Append("%");
                                }
                                sb.Append("'");
                            }
                        }
                        if (StringHelper.IsEqual(column, "JobPostingFromDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[CP].[CreateDate]");
                                sb.Append(" >= ");
                                sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ");

                            }
                        }
                        if (StringHelper.IsEqual(column, "JobPostingToDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[CP].[CreateDate]");
                                sb.Append(" < ");
                                sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ");
                            }
                        }
                        if (StringHelper.IsEqual(column, "Client"))
                        {
                            if (!string.Equals(value, ""))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append("[J].[ClientId]");
                                sb.Append(" in (");

                                sb.Append(value);
                                sb.Append(")");
                            }
                        }


                        //if (StringHelper.IsEqual(column, "BUId"))
                        //{
                        //    if (value.Trim() != "0")
                        //    {
                        //        if (sb.ToString() != String.Empty)
                        //        {
                        //            sb.Append(" AND ");
                        //        }
                        //        sb.Append(" [JP].[JOBSTATUS]");
                        //        sb.Append(" = ");
                        //        sb.Append(value);
                        //    }
                        //}

                        //if (StringHelper.IsEqual(column, "RequiId"))
                        //{
                        //    if (value.Trim() != "0")
                        //    {
                        //        if (sb.ToString() != String.Empty)
                        //        {
                        //            sb.Append(" AND ");
                        //        }
                        //        sb.Append(" [JP].[ID]");
                        //        sb.Append(" = ");
                        //        sb.Append(value);
                        //    }
                        //}
                        else if (StringHelper.IsEqual(column, "CreatorId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[CreatorId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else if (StringHelper.IsEqual(column, "hotListManagerId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" OR ");
                            }
                            sb.Append("[C].[Id]");
                            sb.Append(" IN ");
                            //                            sb.Append(@"( SELECT [MG].[MemberId] FROM [MemberGroupMap] [MG] LEFT JOIN [MemberGroupManager] [MGM]
                            //                                    ON [MG].[MemberGroupId]=[MGM].[MemberGroupId]
                            //                                    WHERE [MGM].[MemberId]=" + value + @"
                            //                                    AND [MG].[MemberId] NOT IN (SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId=" + value + ") ");
                            sb.Append(@"( SELECT [MG].[MemberId] FROM [MemberGroupMap] [MG] LEFT JOIN [MemberGroupManager] [MGM]
                                    ON [MG].[MemberGroupId]=[MGM].[MemberGroupId]
                                    WHERE [MGM].[MemberId]=" + value + @"
                                    AND [MG].[MemberId] NOT IN (SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId=" + value + ") ");
                            sb.Append(" ) ");
                        }
                        else if (StringHelper.IsEqual(column, "CompanyId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }

                            //sb.Append("[C].[Id]");
                            //sb.Append(" IN ");
                            //sb.Append(" ( SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId ");
                            //sb.Append(" IN ");
                            //sb.Append("( SELECT DISTINCT MemberId FROM CompanyContact WHERE CompanyId= ");
                            //sb.Append(value);
                            //sb.Append(" ) ");
                            //sb.Append(" ) ");
                            sb.Append("[C].[CreatorId]");
                            sb.Append(" IN ");
                            sb.Append("( SELECT DISTINCT MemberId FROM CompanyContact WHERE MemberId!=0 AND CompanyId= ");
                            sb.Append(value);
                            sb.Append(" ) ");
                        }

                        else if (StringHelper.IsEqual(column, "DateFrom"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[UpdateDate]");
                            sb.Append(" >=convert(datetime,'");
                            sb.Append(value);
                            sb.Append("',103)");
                        }
                        else if (StringHelper.IsEqual(column, "DateTo"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[UpdateDate]");
                            sb.Append(" <=convert(datetime,'");
                            sb.Append(value);
                            sb.Append("',103)");
                        }

                        else if (StringHelper.IsEqual(column, "recentApplicant"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            if (value == "Today")
                                sb.Append(" CONVERT(VARCHAR,[C].[UpdateDate],101) = CONVERT(VARCHAR,GETDATE(),101) ");
                            else if (value == "Yesterday")
                                sb.Append(" DATEDIFF(dd,[C].[UpdateDate],GETDATE()) = 1 ");
                            else if (value == "PastWeek")
                                sb.Append(" DATEDIFF(week,[C].[UpdateDate],GETDATE()) = 1 ");
                            else if (value == "PastMonth")
                                sb.Append(" DATEDIFF(Month,[C].[UpdateDate],GETDATE()) = 1 ");
                            else if (value == "PastQuarter")
                                sb.Append(" DATEDIFF(Quarter,[C].[UpdateDate],GETDATE()) =1  ");
                            else if (value == "PastYear")
                                sb.Append(" DATEDIFF(YEAR,[C].[UpdateDate],GETDATE())=1 ");
                            else if (value == "Monthtilldate")
                                sb.Append(" [C].[UpdateDate] between (SELECT CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(getdate())-1),getdate()),101) AS Date_Value) and  getdate() ");

                        }

                        else if (StringHelper.IsEqual(column, "quickSearchKeyWord"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            //split the search string by space char. if more than one word, use first word as first name and second word as last name
                            string[] valueTokens = value.Split(' ');
                            if (valueTokens.Length > 1)
                            {
                                sb.Append("([C].[FirstName] LIKE '%" + valueTokens[0] + "%'");
                                sb.Append(" OR ");
                                sb.Append("[C].[LastName] LIKE '%" + valueTokens[1] + "%'");
                                sb.Append(" OR ");
                            }
                            else
                            {
                                sb.Append("([C].[FirstName] LIKE '%" + value + "%'");
                                sb.Append(" OR ");
                                sb.Append("[C].[LastName] LIKE '%" + value + "%'");
                                sb.Append(" OR ");
                            }

                            sb.Append("[C].[MiddleName] LIKE '%" + value + "%'");
                            sb.Append(" OR ");
                            sb.Append("[C].[PrimaryEmail] LIKE '%" + value + "%'");
                            sb.Append(" OR ");
                            sb.Append("[C].[AlternateEmail] LIKE '%" + value + "%')");
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "ASC";

            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Candidate> response = new PagedResponse<Candidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<Candidate>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        //*********************************************End******************************************

        private string BuildAllKeyWordsQuery(string allKeyWords)
        {
            string strQuery = string.Empty;
            string basicQuery = string.Empty;
            string resumeQuery = string.Empty;

            StringBuilder sbName = new StringBuilder();
            StringBuilder sbCurrentPositon = new StringBuilder();
            StringBuilder sbPrimaryEmail = new StringBuilder();
            StringBuilder sbSkillSet = new StringBuilder();
            StringBuilder sbBasic = new StringBuilder();
            StringBuilder sbResume = new StringBuilder();
            // 0.2 starts
            StringBuilder sbCurrentCity = new StringBuilder();
            StringBuilder sbPrimaryPhone = new StringBuilder();
            StringBuilder sbCellPhone = new StringBuilder();
            StringBuilder sbHomephone = new StringBuilder();
            //custom
            //StringBuilder sbApplicantType = new StringBuilder();
            // 0.2 ends

            // 0.2 starts
            StringBuilder sbPermanentAddressLine1 = new StringBuilder();
            StringBuilder sbPermanentAddressLine2 = new StringBuilder();
            StringBuilder sbPermanentCity = new StringBuilder();
            StringBuilder sbPermanentPhone = new StringBuilder();
            StringBuilder sbAlternateEmail = new StringBuilder();
            StringBuilder sbCurrentAddressLine1 = new StringBuilder();
            StringBuilder sbCurrentAddressLine2 = new StringBuilder();
            StringBuilder sbOfficePhone = new StringBuilder();
            StringBuilder sbPermanentMobile = new StringBuilder();
            // 0.2 ends      

            //0.5 Start
            /*   string[] keyWords = StringHelper.BuildKeyWordArray(allKeyWords);

              for (int i = 0; i < keyWords.Length; i++)
              {
                
             // 0.3  Start 

                  if (!string.IsNullOrEmpty(keyWords[i].ToString().Trim()))
                  {

                      keyWords[i] = keyWords[i].ToString().Replace("'", "' + char(39)+ '");  //10288 
                    
                      //sbName.Append("[CPS].[FullName] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbPrimaryEmail.Append("[CPS].[PrimaryEmail] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbSkillSet.Append("[CPS].[SkillSet] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbCurrentPositon.Append("[CPS].[CurrentPosition] LIKE '%" + keyWords[i] + "%' AND ");

                      //// 0.2 starts
                      //sbCurrentCity.Append("[CPS].[CurrentCity] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbPrimaryPhone.Append("[CPS].[PrimaryPhone] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbCellPhone.Append("[CPS].[CellPhone] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbHomephone.Append("[CPS].[Homephone] LIKE '%" + keyWords[i] + "%' AND ");



                      //sbPermanentAddressLine1.Append("[CPS].[PermanentAddressLine1] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbPermanentAddressLine2.Append("[CPS].[PermanentAddressLine2] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbPermanentCity.Append("[CPS].[PermanentCity] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbPermanentPhone.Append("[CPS].[PermanentPhone] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbAlternateEmail.Append("[CPS].[AlternateEmail] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbCurrentAddressLine1.Append("[CPS].[CurrentAddressLine1] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbCurrentAddressLine2.Append("[CPS].[CurrentAddressLine2] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbOfficePhone.Append("[CPS].[OfficePhone] LIKE '%" + keyWords[i] + "%' AND ");
                      //sbPermanentMobile.Append("[CPS].[PermanentMobile] LIKE '%" + keyWords[i] + "%' AND ");
                      //// 0.2 ends

                      //sbResume.Append("([MOS].[RawCopyPasteResume] LIKE '%" + keyWords[i] + "%') AND ");

                      //0.3 End

                      // sbName.Append("REPLACE(C.FirstName + ' ' + C.MiddleName + ' ' + C.LastName, '  ', ' ') LIKE '%" + keyWords[i] + "%' AND ");
                      sbName.Append("C.FirstName  LIKE '%" + keyWords[i] + "%' AND ");
                      sbName.Append("C.MiddleName  LIKE '%" + keyWords[i] + "%' AND ");
                      sbName.Append("C.LastName  LIKE '%" + keyWords[i] + "%' AND ");     // 0.3
                      sbPrimaryEmail.Append("[C].[PrimaryEmail] LIKE '%" + keyWords[i] + "%' AND ");
                     // sbSkillSet.Append("dbo.GetMemberSkill(C.Id) LIKE '%" + keyWords[i] + "%' AND "); // 0.4
                      sbSkillSet.Append("dbo.#tbMemberSkills.SkillName LIKE '%" + keyWords[i] + "%' AND ");// 0.4
                      sbCurrentPositon.Append("[MEI].[CurrentPosition] LIKE '%" + keyWords[i] + "%' AND ");

                      // 0.2 starts
                      sbCurrentCity.Append("[MD].[CurrentCity] LIKE '%" + keyWords[i] + "%' AND ");
                      sbPrimaryPhone.Append("[C].[PrimaryPhone] LIKE '%" + keyWords[i] + "%' AND ");
                      sbCellPhone.Append("[C].[CellPhone] LIKE '%" + keyWords[i] + "%' AND ");
                      sbHomephone.Append("[C].[Homephone] LIKE '%" + keyWords[i] + "%' AND ");



                      sbPermanentAddressLine1.Append("[C].[PermanentAddressLine1] LIKE '%" + keyWords[i] + "%' AND ");
                      sbPermanentAddressLine2.Append("[C].[PermanentAddressLine2] LIKE '%" + keyWords[i] + "%' AND ");
                      sbPermanentCity.Append("[C].[PermanentCity] LIKE '%" + keyWords[i] + "%' AND ");
                      sbPermanentPhone.Append("[C].[PermanentPhone] LIKE '%" + keyWords[i] + "%' AND ");
                      sbAlternateEmail.Append("[C].[AlternateEmail] LIKE '%" + keyWords[i] + "%' AND ");
                      sbCurrentAddressLine1.Append("[C].[CurrentAddressLine1] LIKE '%" + keyWords[i] + "%' AND ");
                      sbCurrentAddressLine2.Append("[C].[CurrentAddressLine2] LIKE '%" + keyWords[i] + "%' AND ");
                      sbOfficePhone.Append("[C].[OfficePhone] LIKE '%" + keyWords[i] + "%' AND ");
                      sbPermanentMobile.Append("[C].[PermanentMobile] LIKE '%" + keyWords[i] + "%' AND ");
                      // 0.2 ends

                      sbResume.Append("([MOS].[RawCopyPasteResume] LIKE '%" + keyWords[i] + "%') AND "); */

            if (!string.IsNullOrEmpty(allKeyWords.ToString().Trim()))
            {

                allKeyWords = allKeyWords.ToString().Replace("'", "' + char(39)+ '");  //10288 
                sbName.Append("C.FirstName  LIKE '%" + allKeyWords + "%' AND ");
                sbName.Append("C.MiddleName  LIKE '%" + allKeyWords + "%' AND ");
                sbName.Append("C.LastName  LIKE '%" + allKeyWords + "%' AND ");     // 0.3
                sbPrimaryEmail.Append("[C].[PrimaryEmail] LIKE '%" + allKeyWords + "%' AND ");
                sbSkillSet.Append("dbo.#tbMemberSkills.SkillName LIKE '%" + allKeyWords + "%' AND ");// 0.4
                sbCurrentPositon.Append("[MEI].[CurrentPosition] LIKE '%" + allKeyWords + "%' AND ");
                sbCurrentCity.Append("[MD].[CurrentCity] LIKE '%" + allKeyWords + "%' AND ");
                sbPrimaryPhone.Append("[C].[PrimaryPhone] LIKE '%" + allKeyWords + "%' AND ");
                //custom   
                //sbApplicantType.Append("[C].[ApplicantType] LIKE '%" + allKeyWords + "%' AND ");
                sbCellPhone.Append("[C].[CellPhone] LIKE '%" + allKeyWords + "%' AND ");
                sbHomephone.Append("[C].[Homephone] LIKE '%" + allKeyWords + "%' AND ");
                sbPermanentAddressLine1.Append("[C].[PermanentAddressLine1] LIKE '%" + allKeyWords + "%' AND ");
                sbPermanentAddressLine2.Append("[C].[PermanentAddressLine2] LIKE '%" + allKeyWords + "%' AND ");
                sbPermanentCity.Append("[C].[PermanentCity] LIKE '%" + allKeyWords + "%' AND ");
                sbPermanentPhone.Append("[C].[PermanentPhone] LIKE '%" + allKeyWords + "%' AND ");
                sbAlternateEmail.Append("[C].[AlternateEmail] LIKE '%" + allKeyWords + "%' AND ");
                sbCurrentAddressLine1.Append("[C].[CurrentAddressLine1] LIKE '%" + allKeyWords + "%' AND ");
                sbCurrentAddressLine2.Append("[C].[CurrentAddressLine2] LIKE '%" + allKeyWords + "%' AND ");
                sbOfficePhone.Append("[C].[OfficePhone] LIKE '%" + allKeyWords + "%' AND ");
                sbPermanentMobile.Append("[C].[PermanentMobile] LIKE '%" + allKeyWords + "%' AND ");
                sbResume.Append("([MOS].[RawCopyPasteResume] LIKE '%" + allKeyWords + "%') AND ");

                // 0.5 Ends
            }


            // 0.2 starts
            basicQuery = RemoveLastAND(sbName) + " OR " +
                RemoveLastAND(sbPrimaryEmail) + " OR " +
                RemoveLastAND(sbCurrentPositon) + " OR " +

                RemoveLastAND(sbCurrentCity) + " OR " +
                RemoveLastAND(sbPrimaryPhone) + " OR " +
                RemoveLastAND(sbCellPhone) + " OR " +
                RemoveLastAND(sbHomephone) + " OR " +


                RemoveLastAND(sbPermanentAddressLine1) + " OR " +
                RemoveLastAND(sbPermanentAddressLine2) + " OR " +
                RemoveLastAND(sbPermanentCity) + " OR " +
                RemoveLastAND(sbPermanentPhone) + " OR " +
                RemoveLastAND(sbAlternateEmail) + " OR " +
                RemoveLastAND(sbCurrentAddressLine1) + " OR " +
                RemoveLastAND(sbCurrentAddressLine2) + " OR " +
                RemoveLastAND(sbOfficePhone) + " OR " +
                RemoveLastAND(sbPermanentMobile) + " OR " +


                RemoveLastAND(sbSkillSet);
            // 0.2 ends

            resumeQuery = @"SELECT [MOS].[MemberId] FROM [MemberObjectiveAndSummary] AS [MOS] WHERE " + sbResume.ToString().Substring(0, sbResume.Length - 5);

            // strQuery = "( " + basicQuery + " OR [CPS].[Id] IN (" + resumeQuery + ") )"; // 0.3

            strQuery = "( " + basicQuery + " OR [C].[Id] IN (" + resumeQuery + ") )"; // 0.3

            return strQuery;
        }

        private string BuildAnyKeyWordQuery(string anyKeyWord)
        {
            string strQuery = string.Empty;
            string basicQuery = string.Empty;
            string resumeQuery = string.Empty;

            StringBuilder sbName = new StringBuilder();
            StringBuilder sbCurrentPositon = new StringBuilder();
            StringBuilder sbPrimaryEmail = new StringBuilder();
            StringBuilder sbSkillSet = new StringBuilder();
            StringBuilder sbBasic = new StringBuilder();
            StringBuilder sbResume = new StringBuilder();

            // 0.2 starts
            StringBuilder sbCurrentCity = new StringBuilder();
            StringBuilder sbPrimaryPhone = new StringBuilder();
            StringBuilder sbCellPhone = new StringBuilder();
            StringBuilder sbHomephone = new StringBuilder();



            StringBuilder sbPermanentAddressLine1 = new StringBuilder();
            StringBuilder sbPermanentAddressLine2 = new StringBuilder();
            StringBuilder sbPermanentCity = new StringBuilder();
            StringBuilder sbPermanentPhone = new StringBuilder();
            StringBuilder sbAlternateEmail = new StringBuilder();
            StringBuilder sbCurrentAddressLine1 = new StringBuilder();
            StringBuilder sbCurrentAddressLine2 = new StringBuilder();
            StringBuilder sbOfficePhone = new StringBuilder();
            StringBuilder sbPermanentMobile = new StringBuilder();
            // 0.2 ends


            //   string[] keyWords = StringHelper.BuildKeyWordArray(anyKeyWord); //0.5
            string[] keyWords = StringHelper.BuildSearchKeyWordArray(anyKeyWord); //0.5

            for (int i = 0; i < keyWords.Length; i++)
            {

                // 0.3     
                if (!string.IsNullOrEmpty(keyWords[i].ToString().Trim()))     // 10249 
                {

                    keyWords[i] = keyWords[i].ToString().Replace("'", "' + char(39)+ '");  //10288 

                    //sbName.Append("[CPS].[FullName] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbPrimaryEmail.Append("[CPS].[PrimaryEmail] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbSkillSet.Append("[CPS].[SkillSet] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbCurrentPositon.Append("[CPS].[CurrentPosition] LIKE '%" + keyWords[i] + "%' OR ");

                    //// 0.2 starts
                    //sbCurrentCity.Append("[CPS].[CurrentCity] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbPrimaryPhone.Append("[CPS].[PrimaryPhone] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbCellPhone.Append("[CPS].[CellPhone] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbHomephone.Append("[CPS].[Homephone] LIKE '%" + keyWords[i] + "%' OR ");

                    //sbPermanentAddressLine1.Append("[CPS].[PermanentAddressLine1] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbPermanentAddressLine2.Append("[CPS].[PermanentAddressLine2] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbPermanentCity.Append("[CPS].[PermanentCity] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbPermanentPhone.Append("[CPS].[PermanentPhone] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbAlternateEmail.Append("[CPS].[AlternateEmail] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbCurrentAddressLine1.Append("[CPS].[CurrentAddressLine1] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbCurrentAddressLine2.Append("[CPS].[CurrentAddressLine2] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbOfficePhone.Append("[CPS].[OfficePhone] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbPermanentMobile.Append("[CPS].[PermanentMobile] LIKE '%" + keyWords[i] + "%' OR ");
                    // 0.2 ends

                    // 0.3

                    // sbName.Append("REPLACE(C.FirstName + ' ' + C.MiddleName + ' ' + C.LastName, '  ', ' ') LIKE '%" + keyWords[i] + "%' OR ");
                    sbName.Append("C.FirstName  LIKE '%" + keyWords[i] + "%' OR ");
                    sbName.Append("C.MiddleName  LIKE '%" + keyWords[i] + "%' OR ");
                    sbName.Append("C.LastName  LIKE '%" + keyWords[i] + "%' OR ");     // 0.3

                    sbPrimaryEmail.Append("[C].[PrimaryEmail] LIKE '%" + keyWords[i] + "%' OR ");
                    //sbSkillSet.Append("dbo.GetMemberSkill(C.Id) LIKE '%" + keyWords[i] + "%' OR "); // 0.4
                    sbSkillSet.Append("dbo.#tbMemberSkills.SkillName LIKE '%" + keyWords[i] + "%' OR ");// 0.4
                    sbCurrentPositon.Append("[MEI].[CurrentPosition] LIKE '%" + keyWords[i] + "%' OR ");

                    // 0.2 starts
                    sbCurrentCity.Append("[MD].[CurrentCity] LIKE '%" + keyWords[i] + "%' OR ");
                    sbPrimaryPhone.Append("[C].[PrimaryPhone] LIKE '%" + keyWords[i] + "%' OR ");
                    sbCellPhone.Append("[C].[CellPhone] LIKE '%" + keyWords[i] + "%' OR ");
                    sbHomephone.Append("[C].[Homephone] LIKE '%" + keyWords[i] + "%' OR ");



                    sbPermanentAddressLine1.Append("[C].[PermanentAddressLine1] LIKE '%" + keyWords[i] + "%' OR ");
                    sbPermanentAddressLine2.Append("[C].[PermanentAddressLine2] LIKE '%" + keyWords[i] + "%' OR ");
                    sbPermanentCity.Append("[C].[PermanentCity] LIKE '%" + keyWords[i] + "%' OR ");
                    sbPermanentPhone.Append("[C].[PermanentPhone] LIKE '%" + keyWords[i] + "%' OR ");
                    sbAlternateEmail.Append("[C].[AlternateEmail] LIKE '%" + keyWords[i] + "%' OR ");
                    sbCurrentAddressLine1.Append("[C].[CurrentAddressLine1] LIKE '%" + keyWords[i] + "%' OR ");
                    sbCurrentAddressLine2.Append("[C].[CurrentAddressLine2] LIKE '%" + keyWords[i] + "%' OR ");
                    sbOfficePhone.Append("[C].[OfficePhone] LIKE '%" + keyWords[i] + "%' OR ");
                    sbPermanentMobile.Append("[C].[PermanentMobile] LIKE '%" + keyWords[i] + "%' OR ");
                    // 0.2 ends

                    sbResume.Append("([MOS].[RawCopyPasteResume] LIKE '%" + keyWords[i] + "%') OR ");
                }
            }

            // 0.2 starts
            basicQuery = RemoveLastOR(sbName) + " OR " +
                RemoveLastOR(sbPrimaryEmail) + " OR " +
                RemoveLastOR(sbCurrentPositon) + " OR " +

                RemoveLastOR(sbCurrentCity) + " OR " +
                RemoveLastOR(sbPrimaryPhone) + " OR " +
                RemoveLastOR(sbCellPhone) + " OR " +
                RemoveLastOR(sbHomephone) + " OR " +

                RemoveLastOR(sbPermanentAddressLine1) + " OR " +
                RemoveLastOR(sbPermanentAddressLine2) + " OR " +
                RemoveLastOR(sbPermanentCity) + " OR " +
                RemoveLastOR(sbPermanentPhone) + " OR " +
                RemoveLastOR(sbAlternateEmail) + " OR " +
                RemoveLastOR(sbCurrentAddressLine1) + " OR " +
                RemoveLastOR(sbCurrentAddressLine2) + " OR " +
                RemoveLastOR(sbOfficePhone) + " OR " +
                RemoveLastOR(sbPermanentMobile) + " OR " +


                RemoveLastOR(sbSkillSet);
            // 0.2 ends

            resumeQuery = @"SELECT [MOS].[MemberId] FROM [MemberObjectiveAndSummary] AS [MOS] WHERE " + sbResume.ToString().Substring(0, sbResume.Length - 4);

            //  strQuery = "( " + basicQuery + " OR [CPS].[Id] IN (" + resumeQuery + ") )"; //0.3

            strQuery = "( " + basicQuery + " OR [C].[Id] IN (" + resumeQuery + ") )"; //0.3

            return strQuery;
        }


        private string BuildAllAndAnyKeyWordQuery(string allKeyWords, string anyKeyWord)
        {
            string strQuery = string.Empty;
            strQuery = "( (" + BuildAllKeyWordsQuery(allKeyWords) + ") AND (" + BuildAnyKeyWordQuery(anyKeyWord) + ") )";
            return strQuery;
        }

        private string RemoveLastAND(StringBuilder sb)
        {
            return "(" + sb.ToString().Substring(0, sb.Length - 5) + ")";
        }

        private string RemoveLastOR(StringBuilder sb)
        {
            return "(" + sb.ToString().Substring(0, sb.Length - 4) + ")";
        }

        private string GetSearchKey(string allKeyWords, string anyKeyWord)
        {
            string returnVal = string.Empty;
            if (allKeyWords == null && anyKeyWord == null)
            {
                return string.Empty;
            }
            else if (!string.IsNullOrEmpty(allKeyWords) && !string.IsNullOrEmpty(anyKeyWord))
            {
                return allKeyWords + ", " + anyKeyWord;
            }
            else if (string.IsNullOrEmpty(allKeyWords) && !string.IsNullOrEmpty(anyKeyWord))
            {
                return anyKeyWord;
            }
            else if (!string.IsNullOrEmpty(allKeyWords) && string.IsNullOrEmpty(anyKeyWord))
            {
                return allKeyWords;
            }
            else
            {
                return string.Empty;
            }
            //else if (!string.IsNullOrEmpty(allKeyWords))
            //{
            //    //string[] strKeys = allKeyWords.Split(',');
            //    //if (strKeys.Length > 0)
            //    //    return strKeys[0];
            //    //else
            //    //    return string.Empty;
            //}
            //else if (!string.IsNullOrEmpty(anyKeyWord))
            //{
            //    //string[] strKeys = anyKeyWord.Split(',');
            //    //if (strKeys.Length > 0)
            //    //    return strKeys[0];
            //    //else
            //    //    return string.Empty;
            //}            
        }
        //custom
        public string GetSearchQueryForPrecise(string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string memberPositionId, string isMyList, string minExpYr, string maxExpYr, string state, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddWhereClause, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string SourceId, string CandidateID, string NoticePeriod) //0.2
        {
            string whereClause = string.Empty;

            StringBuilder whereClauseSb = new StringBuilder();

            if (CandidateID != null && CandidateID != "" && CandidateID != "0")
            {
                if (whereClauseSb.ToString() != string.Empty) whereClauseSb.Append (" And");
                whereClauseSb.Append (" [C].[Id]=" + CandidateID);
            }

            if (!string.IsNullOrEmpty(currentPosition))
            {
                whereClauseSb.Append(" AND [MEI].[CurrentPosition] LIKE '%" + currentPosition + "%'");
            }
            if (!string.IsNullOrEmpty(CurrentCompany) && !string.IsNullOrEmpty(CompanyStatus))
            {
                if (CompanyStatus == "Current")
                {
                    whereClauseSb.Append(" AND [C].[LastEmployer] like '%" + CurrentCompany + "%'");
                }
                else
                {
                    whereClauseSb.Append(" AND [C].[Id] in (select MemberId from MemberExperience where companyname like '%" + CurrentCompany + "%')");
                }
            }
            if (!string.IsNullOrEmpty(CandidateEmail))
            {
                whereClauseSb.Append(" AND ([C].[PrimaryEmail] like '%" + CandidateEmail + "%' or [C].[AlternateEmail] like '%" + CandidateEmail + "%')");
            }
            if (!string.IsNullOrEmpty(MobilePhone))
            {
                whereClauseSb.Append(" AND ([C].[PermanentMobile] like '%" + MobilePhone + "%' or [C].[CellPhone] like '%" + MobilePhone + "%')");
            }
            if (AddWhereClause)
            {
                if (!string.IsNullOrEmpty(currentCity))
                {
                    whereClauseSb.Append(" AND [C].[PermanentCity] LIKE '%" + currentCity + "%'");   // 10255               
                }
                if (!string.IsNullOrEmpty(state))
                {
                    whereClauseSb.Append(" AND ([C].[CurrentStateId] = " + state + " OR [C].[PermanentStateId]=" + state + ")"); //0.2  // 0.3
                }
                if (!string.IsNullOrEmpty(ZipCode))
                {
                    whereClauseSb.Append(" AND [C].[PermanentZip] = '" + ZipCode + "'");
                }
            }

            if (!string.IsNullOrEmpty(currentCountryId))
            {
                whereClauseSb.Append(" AND [C].[PermanentCountryId]=" + currentCountryId);  // 10714
            }

            if (!string.IsNullOrEmpty(jobType))
            {
                whereClauseSb.Append(" AND [MEI].[JobTypeLookupId] = " + jobType); // 0.3
            }
            if (!string.IsNullOrEmpty(Education))
            {
                whereClauseSb.Append(" AND [C].[Id] in (select Distinct MemberId from MemberEducation where LevelofEducationLookupId in(" + Education + "))");
            }
            if (!string.IsNullOrEmpty(HiringStatus))
            {
                whereClauseSb.Append(" AND [HS].[HiringStatusId] in(" + HiringStatus + ")");
            }
            if (!string.IsNullOrEmpty(WorkSchedule))
            {
                whereClauseSb.Append(" AND [MEI].[WorkScheduleLookUpId] = " + WorkSchedule);
            }


            if (!string.IsNullOrEmpty(Gender))
            {
                whereClauseSb.Append(" AND [C].[GenderLookupId] = " + Gender);
            }

            if (!string.IsNullOrEmpty(Nationality))
            {
                whereClauseSb.Append(" AND [MDS].[CountryIdOfBirth] = " + Nationality);
            }

            if (!string.IsNullOrEmpty(Industry))
            {
                whereClauseSb.Append(" AND [MDS].[CandidateIndustryLookupID] = " + Industry);
            }

            if (!string.IsNullOrEmpty(JobFunction))
            {
                whereClauseSb.Append(" AND [MDS].[JobFunctionLookupID] = " + JobFunction);
            }



            if (!string.IsNullOrEmpty(workStatus))
            {
                whereClauseSb.Append(" AND [MEI].[WorkAuthorizationLookupId] IN (" + workStatus + ")");  // 0.3
            }

            if (!string.IsNullOrEmpty(securityClearence))
            {
                whereClauseSb.Append(" AND [MEI].[SecurityClearance] = " + securityClearence); //0.3
            }
            //custom
            if (!string.IsNullOrEmpty(applicantType))
            {
                whereClauseSb.Append(" AND [M].[MemberType] = " + applicantType);
            }



            // 0.2 ends

            if (!string.IsNullOrEmpty(availability))
            {
                whereClauseSb.Append("AND CONVERT (varchar(50),[MEI].MEIAvailableDate, 111) > CONVERT(varchar(50), CAST( '" + availability + "' AS DATETIME),111)");  // 0.6
            }

            if (!string.IsNullOrEmpty(industryType))
            {
                whereClauseSb.Append(" AND [MEI].[IndustryTypeLookupId] = " + industryType); // 0.3
            }

            if (!string.IsNullOrEmpty(lastUpdateDate))
            {
                DateTime dt = DateTime.Now;
                try
                {
                    dt = dt.AddDays(-int.Parse(lastUpdateDate));
                    whereClauseSb.Append(" AND [MEI].[UpdateDate] >= '" + dt.ToString("MM/dd/yyyy") + "'"); // 0.3
                }
                catch
                {

                }
            }


            if (!string.IsNullOrEmpty(salaryType))
            {
                double minYearly = 0;
                double maxYearly = 0;
                if ((!string.IsNullOrEmpty(salaryFrom)) && (!string.IsNullOrEmpty(salaryTo)))
                {
                    minYearly = Convert.ToDouble(salaryFrom);
                    maxYearly = Convert.ToDouble(salaryTo);
                }
                string s = "";
                if (SalarySelector == "1")
                {
                    s = "( cast([C].[CurrentYearlyRate] as Decimal(18,3))  between " + (minYearly) + " and  " + (maxYearly);
                    s += " AND [C].[CurrentSalaryPayCycle]=" + salaryType + (currencyType != null ? (" AND  [C].[CurrentYearlyCurrencyLookupId]=" + currencyType) : "") + ")";
                }
                else
                {

                    s += " (( cast( [C].[ExpectedYearlyRate] as Decimal(18,3)) between " + minYearly + " and  " + maxYearly;
                    s += " OR cast( [C].[ExpectedYearlyMaxRate] as Decimal(18,3))   between  " + minYearly + " and " + maxYearly + "))";
                    s += " AND [C].[ExpectedSalaryPayCycle]=" + salaryType + (currencyType != null ? ("  AND  [C].[CurrentYearlyCurrencyLookupId]=" + currencyType) : "");
                }
                whereClauseSb.Append(" AND  ");
                whereClauseSb.Append(s);
            }

            if ((!string.IsNullOrEmpty(minExpYr)) && (!string.IsNullOrEmpty(maxExpYr)))
            {
                try
                {
                    decimal.Parse(minExpYr);
                    decimal.Parse(maxExpYr);
                    // whereClauseSb.Append(" AND [CPS].[Experience] >= " + minExpYr + " AND Experience <=" + maxExpYr); // 0.3
                    whereClauseSb.Append(" AND CAST(CASE ISNUMERIC(MEI.TotalExperienceYears) WHEN 1 THEN CAST(MEI.TotalExperienceYears AS DECIMAL(10, 2)) ELSE 0 END AS DECIMAL(10,2)) >= " + minExpYr + " AND CAST(CASE ISNUMERIC(MEI.TotalExperienceYears) WHEN 1 THEN CAST(MEI.TotalExperienceYears AS DECIMAL(10, 2)) ELSE 0 END AS DECIMAL(10,2)) <=" + maxExpYr); // 0.3

                }
                catch { }
            }
            else if (!string.IsNullOrEmpty(minExpYr))
            {
                try
                {
                    decimal.Parse(minExpYr);
                    //whereClauseSb.Append(" AND [CPS].[Experience] >= " + minExpYr); // 0.3
                    whereClauseSb.Append(" AND CAST(CASE ISNUMERIC(MEI.TotalExperienceYears) WHEN 1 THEN CAST(MEI.TotalExperienceYears AS DECIMAL(10, 2)) ELSE 0 END AS DECIMAL(10,2))  >= " + minExpYr); // 0.3

                }
                catch { }
            }
            else if (!string.IsNullOrEmpty(maxExpYr))
            {
                try
                {
                    decimal.Parse(maxExpYr);
                    whereClauseSb.Append(" AND CAST(CASE ISNUMERIC(MEI.TotalExperienceYears) WHEN 1 THEN CAST(MEI.TotalExperienceYears AS DECIMAL(10, 2)) ELSE 0 END AS DECIMAL(10,2))  <= " + maxExpYr);  // 0.3
                }
                catch { }
            }
            //For Assigned Managers
            if (!string.IsNullOrEmpty(AssignedManagerId))
            {
                if (Convert.ToInt32(AssignedManagerId) > 0)
                {
                    whereClauseSb.Append("AND [C].[Id] in (select MemberID from MemberManager where ManagerID=" + AssignedManagerId + ")");
                }
            }
            if (!string.IsNullOrEmpty(functionalCategory))
            {
                //whereClauseSb.Append(" AND [CPS].[FunctionalCategoryLookupId] = " + functionalCategory);
            }

            if ((!string.IsNullOrEmpty(hotListId)))
            {
                try
                {
                    //  whereClauseSb.Append(" AND [CPS].[Id] IN (SELECT [MGM].[MemberId] FROM [MemberGroupMap] AS [MGM], [MemberGroup] AS [MG] WHERE [MGM].[MemberGroupId] = [MG].[Id] AND [MGM].[CreatorId] = " + int.Parse(memberId) + " AND [MG].[Id] = " + int.Parse(hotListId) + ")");  // 0.3
                    //whereClauseSb.Append(" AND [C].[Id] IN (SELECT [MGM].[MemberId] FROM [MemberGroupMap] AS [MGM], [MemberGroup] AS [MG] WHERE [MGM].[MemberGroupId] = [MG].[Id] AND [MGM].[CreatorId] = " + int.Parse(memberId) + " AND [MG].[Id] = " + int.Parse(hotListId) + ")");  // 0.3
                    whereClauseSb.Append(" AND [C].[Id] IN (SELECT [MGM].[MemberId] FROM [MemberGroupMap] AS [MGM], [MemberGroup] AS [MG] WHERE [MGM].[MemberGroupId] = [MG].[Id] AND [MG].[Id] = " + int.Parse(hotListId) + ")");  // 0.3
                }
                catch
                {

                }
            }

            if (!string.IsNullOrEmpty(hiringMatrixId))
            {
                try
                {
                    // whereClauseSb.Append(" AND [CPS].[Id] IN (SELECT [MJC].[MemberId] FROM [MemberJobCart] AS [MJC], [MemberJobCartDetail] AS [MJCD] WHERE [MJC].[Id] = [MJCD].[MemberJobCartId] AND [MJCD].[SelectionStepLookupId] = " + int.Parse(hiringMatrixId) + ")");  // 0.3
                    whereClauseSb.Append(" AND [C].[Id] IN (SELECT [MJC].[MemberId] FROM [MemberJobCart] AS [MJC], [MemberJobCartDetail] AS [MJCD] WHERE [MJC].[Id] = [MJCD].[MemberJobCartId] AND [MJCD].[SelectionStepLookupId] = " + int.Parse(hiringMatrixId) + ")");  // 0.3
                }
                catch
                {

                }
            }
            if (!string.IsNullOrEmpty(internalRating) && ((internalRating.Trim()) != "0"))
            {
                try
                {
                    // whereClauseSb.Append(" AND [CPS].[Id] IN (SELECT [MJC].[MemberId] FROM [MemberJobCart] AS [MJC], [MemberJobCartDetail] AS [MJCD] WHERE [MJC].[Id] = [MJCD].[MemberJobCartId] AND [MJCD].[SelectionStepLookupId] = " + int.Parse(hiringMatrixId) + ")");  // 0.3
                    whereClauseSb.Append(" AND [C].internalRatingLookupId=" + internalRating);  // 0.3
                }
                catch
                {

                }
            }
            if (!string.IsNullOrEmpty(NoticePeriod) && ((NoticePeriod.Trim()) != ""))
            {
                try
                {
                    // whereClauseSb.Append(" AND [CPS].[Id] IN (SELECT [MJC].[MemberId] FROM [MemberJobCart] AS [MJC], [MemberJobCartDetail] AS [MJCD] WHERE [MJC].[Id] = [MJCD].[MemberJobCartId] AND [MJCD].[SelectionStepLookupId] = " + int.Parse(hiringMatrixId) + ")");  // 0.3
                    //whereClauseSb.Append(" AND [MEI].NoticePeriod=''" + NoticePeriod + "''");  // 0.3
                    whereClauseSb.Append(" AND [MEI].NoticePeriod LIKE '%" + NoticePeriod + "%'");
                }
                catch
                {


                }
            }

            if (!string.IsNullOrEmpty(memberPositionId))
            {
                try
                {
                    // whereClauseSb.Append(" AND [CPS].[Id] IN (SELECT [MPM].[MemberId] FROM [MemberPositionMap] AS [MPM] WHERE [MPM].[PositionId]=" + int.Parse(memberPositionId) + ")"); 0.3
                    whereClauseSb.Append(" AND [C].[Id] IN (SELECT [MPM].[MemberId] FROM [MemberPositionMap] AS [MPM] WHERE [MPM].[PositionId]=" + int.Parse(memberPositionId) + ")"); //0.3
                }
                catch
                {

                }
            }

            if ((!string.IsNullOrEmpty(isMyList)) && (isMyList == "1") && (!string.IsNullOrEmpty(memberId)))
            {
                // whereClauseSb.Append(" AND [CPS].[Id] IN  ( SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId= " + memberId + ")");  // 0.3
                whereClauseSb.Append(" AND [C].[Id] IN  ( SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId= " + memberId + ")");  // 0.3
            }


            if (SourceId != null && SourceId != "" && SourceId != "0")
            {
                whereClauseSb.Append("AND [C].[SourceLookupId]=" + SourceId);
            }
            whereClause = whereClauseSb.ToString();

            if (whereClause.Trim().StartsWith("AND"))  // 8669
            {
                whereClause = whereClause.Substring(4);
            }

            return whereClause;
        }

        public string GetSearchQuery(string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string memberPositionId, string isMyList, string minExpYr, string maxExpYr, string state, string applicantType, string internalRating) //0.2
        {
            string whereClause = string.Empty;

            StringBuilder whereClauseSb = new StringBuilder();



            if (!string.IsNullOrEmpty(currentPosition))
            {
                //whereClauseSb.Append(" AND [CPS].[CurrentPosition] LIKE '%" + currentPosition + "%'");  // 0.3
                whereClauseSb.Append(" AND [MEI].[CurrentPosition] LIKE '%" + currentPosition + "%'");


            }

            if (!string.IsNullOrEmpty(currentCity))
            {
                // whereClauseSb.Append(" AND [CPS].[CurrentCity] LIKE '%" + currentCity + "%'");
                //whereClauseSb.Append(" AND [MD].[CurrentCity] LIKE '%" + currentCity + "%'");  // 0.3  // 10255
                whereClauseSb.Append(" AND [C].[PermanentCity] LIKE '%" + currentCity + "%'");   // 10255               
            }

            if (!string.IsNullOrEmpty(currentCountryId))
            {
                // whereClauseSb.Append(" AND [CPS].[CurrentCountryId] = " + currentCountryId); //0.2
                // whereClauseSb.Append(" AND ([CPS].[CurrentCountryId] = " + currentCountryId + "OR [CPS].[PermanentCountryId]=" + currentCountryId + ")"); //0.2

                // whereClauseSb.Append(" AND ([MD].[CurrentCountryId] = " + currentCountryId + " OR [C].[PermanentCountryId]=" + currentCountryId + ")"); //0.3  // 10714
                whereClauseSb.Append(" AND [C].[PermanentCountryId]=" + currentCountryId);  // 10714
            }

            if (!string.IsNullOrEmpty(jobType))
            {
                //  whereClauseSb.Append(" AND [CPS].[JobTypeLookupId] = " + jobType); // 0.3
                whereClauseSb.Append(" AND [MEI].[JobTypeLookupId] = " + jobType); // 0.3

            }

            if (!string.IsNullOrEmpty(workStatus))
            {
                // 0.1 starts

                // whereClauseSb.Append(" AND [CPS].[WorkAuthorizationLookupId] = " + workStatus);

                // whereClauseSb.Append(" AND [CPS].[WorkAuthorizationLookupId] IN(" + workStatus + ")");  // 0.3
                // 0.1 ends

                whereClauseSb.Append(" AND [MEI].[WorkAuthorizationLookupId] IN (" + workStatus + ")");  // 0.3

            }

            if (!string.IsNullOrEmpty(securityClearence))
            {
                // whereClauseSb.Append(" AND [CPS].[SecurityClearance] = " + securityClearence);  //0.3
                whereClauseSb.Append(" AND [MEI].[SecurityClearance] = " + securityClearence); //0.3

            }
            //custom
            if (!string.IsNullOrEmpty(applicantType))
            {
                whereClauseSb.Append(" AND [M].[MemberType] = " + applicantType);
            }


            // 0.2 starts 

            if (!string.IsNullOrEmpty(state))
            {
                whereClauseSb.Append(" AND ([C].[CurrentStateId] = " + state + " OR [C].[PermanentStateId]=" + state + ")"); //0.2  // 0.3
            }

            // 0.2 ends

            if (!string.IsNullOrEmpty(availability))
            {
                whereClauseSb.Append("AND CONVERT (varchar(50),[MEI].AvailableDate, 111) > CONVERT(varchar(50), CAST( '" + availability + "' AS DATETIME),111)");  // 0.6
            }

            if (!string.IsNullOrEmpty(industryType))
            {
                whereClauseSb.Append(" AND [MEI].[IndustryTypeLookupId] = " + industryType); // 0.3
            }

            if (!string.IsNullOrEmpty(lastUpdateDate))
            {
                DateTime dt = DateTime.Now;
                try
                {
                    dt = dt.AddDays(-int.Parse(lastUpdateDate));
                    // whereClauseSb.Append(" AND [CPS].[UpdateDate] >= '" + dt.ToShortDateString() + "'"); // 0.3
                    whereClauseSb.Append(" AND [MEI].[UpdateDate] >= '" + dt.ToString("MM/dd/yyyy") + "'"); // 0.3
                }
                catch
                {

                }
            }


            if (!string.IsNullOrEmpty(salaryType))
            {
                double minYearly = 0;
                double maxYearly = 0;
                if ((!string.IsNullOrEmpty(salaryFrom)) && (!string.IsNullOrEmpty(salaryTo)))
                {
                    minYearly = Convert.ToDouble(salaryFrom);
                    maxYearly = Convert.ToDouble(salaryTo);
                }
                string s = "( ( cast([C].[CurrentYearlyRate] as Decimal(18,3))  between " + (minYearly - 1) + " and  " + (maxYearly + 1);
                s += " AND [C].[CurrentSalaryPayCycle]=" + salaryType + ")";
                s += " OR (( cast( [C].[ExpectedYearlyRate] as Decimal(18,3)) between  " + minYearly + " and  " + maxYearly;
                s += "  OR cast( [C].[ExpectedYearlyMaxRate] as Decimal(18,3))   between  " + minYearly + " and " + maxYearly + "))";
                s += " AND [C].[ExpectedSalaryPayCycle]=" + salaryType + ")";
                whereClauseSb.Append(" AND  ");
                whereClauseSb.Append(s);

            }

            if ((!string.IsNullOrEmpty(minExpYr)) && (!string.IsNullOrEmpty(maxExpYr)))
            {
                try
                {
                    decimal.Parse(minExpYr);
                    decimal.Parse(maxExpYr);
                    // whereClauseSb.Append(" AND [CPS].[Experience] >= " + minExpYr + " AND Experience <=" + maxExpYr); // 0.3
                    whereClauseSb.Append(" AND CAST(CASE ISNUMERIC(MEI.TotalExperienceYears) WHEN 1 THEN CAST(MEI.TotalExperienceYears AS DECIMAL(10, 2)) ELSE 0 END AS DECIMAL(10,2)) >= " + minExpYr + " AND CAST(CASE ISNUMERIC(MEI.TotalExperienceYears) WHEN 1 THEN CAST(MEI.TotalExperienceYears AS DECIMAL(10, 2)) ELSE 0 END AS DECIMAL(10,2)) <=" + maxExpYr); // 0.3

                }
                catch { }
            }
            else if (!string.IsNullOrEmpty(minExpYr))
            {
                try
                {
                    decimal.Parse(minExpYr);
                    //whereClauseSb.Append(" AND [CPS].[Experience] >= " + minExpYr); // 0.3
                    whereClauseSb.Append(" AND CAST(CASE ISNUMERIC(MEI.TotalExperienceYears) WHEN 1 THEN CAST(MEI.TotalExperienceYears AS DECIMAL(10, 2)) ELSE 0 END AS DECIMAL(10,2))  >= " + minExpYr); // 0.3

                }
                catch { }
            }
            else if (!string.IsNullOrEmpty(maxExpYr))
            {
                try
                {
                    decimal.Parse(maxExpYr);
                    whereClauseSb.Append(" AND CAST(CASE ISNUMERIC(MEI.TotalExperienceYears) WHEN 1 THEN CAST(MEI.TotalExperienceYears AS DECIMAL(10, 2)) ELSE 0 END AS DECIMAL(10,2))  <= " + maxExpYr);  // 0.3
                }
                catch { }
            }

            if (!string.IsNullOrEmpty(functionalCategory))
            {
                //whereClauseSb.Append(" AND [CPS].[FunctionalCategoryLookupId] = " + functionalCategory);
            }

            if ((!string.IsNullOrEmpty(hotListId)) && (!string.IsNullOrEmpty(memberId)))
            {
                try
                {
                    //  whereClauseSb.Append(" AND [CPS].[Id] IN (SELECT [MGM].[MemberId] FROM [MemberGroupMap] AS [MGM], [MemberGroup] AS [MG] WHERE [MGM].[MemberGroupId] = [MG].[Id] AND [MGM].[CreatorId] = " + int.Parse(memberId) + " AND [MG].[Id] = " + int.Parse(hotListId) + ")");  // 0.3
                    //whereClauseSb.Append(" AND [C].[Id] IN (SELECT [MGM].[MemberId] FROM [MemberGroupMap] AS [MGM], [MemberGroup] AS [MG] WHERE [MGM].[MemberGroupId] = [MG].[Id] AND [MGM].[CreatorId] = " + int.Parse(memberId) + " AND [MG].[Id] = " + int.Parse(hotListId) + ")");  // 0.3
                    whereClauseSb.Append(" AND [C].[Id] IN (SELECT [MGM].[MemberId] FROM [MemberGroupMap] AS [MGM], [MemberGroup] AS [MG] WHERE [MGM].[MemberGroupId] = [MG].[Id] AND [MG].[Id] = " + int.Parse(hotListId) + ")");  // 0.3
                }
                catch
                {

                }
            }

            if (!string.IsNullOrEmpty(hiringMatrixId))
            {
                try
                {
                    // whereClauseSb.Append(" AND [CPS].[Id] IN (SELECT [MJC].[MemberId] FROM [MemberJobCart] AS [MJC], [MemberJobCartDetail] AS [MJCD] WHERE [MJC].[Id] = [MJCD].[MemberJobCartId] AND [MJCD].[SelectionStepLookupId] = " + int.Parse(hiringMatrixId) + ")");  // 0.3
                    whereClauseSb.Append(" AND [C].[Id] IN (SELECT [MJC].[MemberId] FROM [MemberJobCart] AS [MJC], [MemberJobCartDetail] AS [MJCD] WHERE [MJC].[Id] = [MJCD].[MemberJobCartId] AND [MJCD].[SelectionStepLookupId] = " + int.Parse(hiringMatrixId) + ")");  // 0.3
                }
                catch
                {

                }
            }
            if (!string.IsNullOrEmpty(internalRating) && ((internalRating.Trim()) != "0"))
            {
                try
                {
                    // whereClauseSb.Append(" AND [CPS].[Id] IN (SELECT [MJC].[MemberId] FROM [MemberJobCart] AS [MJC], [MemberJobCartDetail] AS [MJCD] WHERE [MJC].[Id] = [MJCD].[MemberJobCartId] AND [MJCD].[SelectionStepLookupId] = " + int.Parse(hiringMatrixId) + ")");  // 0.3
                    whereClauseSb.Append(" AND [C].internalRatingLookupId=" + internalRating);  // 0.3
                }
                catch
                {

                }
            }


            if (!string.IsNullOrEmpty(memberPositionId))
            {
                try
                {
                    // whereClauseSb.Append(" AND [CPS].[Id] IN (SELECT [MPM].[MemberId] FROM [MemberPositionMap] AS [MPM] WHERE [MPM].[PositionId]=" + int.Parse(memberPositionId) + ")"); 0.3
                    whereClauseSb.Append(" AND [C].[Id] IN (SELECT [MPM].[MemberId] FROM [MemberPositionMap] AS [MPM] WHERE [MPM].[PositionId]=" + int.Parse(memberPositionId) + ")"); //0.3
                }
                catch
                {

                }
            }

            if ((!string.IsNullOrEmpty(isMyList)) && (isMyList == "1") && (!string.IsNullOrEmpty(memberId)))
            {
                // whereClauseSb.Append(" AND [CPS].[Id] IN  ( SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId= " + memberId + ")");  // 0.3
                whereClauseSb.Append(" AND [C].[Id] IN  ( SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId= " + memberId + ")");  // 0.3
            }

            whereClause = whereClauseSb.ToString();

            if (whereClause.Trim().StartsWith("AND"))  // 8669
            {
                whereClause = whereClause.Substring(4);
            }

            return whereClause;
        }
        PagedResponse<Candidate> ICandidateDataAccess.GetPagedForSavedQuery(PagedRequest request)
        {
            const string SP = "dbo.Candidate_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "memberId"))
                        {
                            sb.Append("[C].[Id]");
                            sb.Append(" IN ");
                            sb.Append(" ( SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId= ");
                            sb.Append(value);
                            sb.Append(" ) ");
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Candidate> response = new PagedResponse<Candidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<Candidate>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<Candidate> ICandidateDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.Candidate_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");
                        if (StringHelper.IsEqual(column, "CandidateID"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[Id] =");
                            sb.Append(value);
                        }
                        if (StringHelper.IsEqual(column, "memberId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[Id]");
                            sb.Append(" IN ");
                            sb.Append(" ( SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId= ");
                            sb.Append(value);
                            sb.Append(" ) ");
                        }
                        if (StringHelper.IsEqual(column, "CandidateName"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                char[] delim = { ' ' };
                                string[] valueList = value.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                                if (valueList.Length == 2)
                                {
                                    sb.Append(" ([C].[FirstName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[0]);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[LastName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[1]);
                                    sb.Append("%') ");
                                }
                                else if (valueList.Length == 3)
                                {
                                    sb.Append(" ([C].[FirstName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[0]);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[LastName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[1]);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[MiddleName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[2]);
                                    sb.Append("%') ");
                                }
                                else
                                {
                                    sb.Append(" ([C].[FirstName]");
                                    sb.Append(" like '%");
                                    sb.Append(value);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[LastName]");
                                    sb.Append(" like '%");
                                    sb.Append(value);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[MiddleName]");
                                    sb.Append(" like '%");
                                    sb.Append(value);
                                    sb.Append("%') ");
                                }

                            }
                        }
                        if (StringHelper.IsEqual(column, "CandidateEmail"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append("( [C].[PrimaryEmail]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                                sb.Append(" or ");
                                sb.Append("[C].[AlternateEmail]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                                sb.Append(") ");
                            }
                        }
                        if (StringHelper.IsEqual(column, "Position"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[CurrentPosition]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                            }
                        }

                        if (StringHelper.IsEqual(column, "AssignedManager"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[ID]");
                                sb.Append(" IN (");
                                sb.Append("SELECT DISTINCT [MemberId] FROM [MemberManager] [MM] WHERE  [MM].[ManagerId] =");
                                sb.Append(value);
                                sb.Append(")");
                            }
                        }
                        if (StringHelper.IsEqual(column, "HiringLevel"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [MJCD].[SelectionStepLookupId]");
                                sb.Append(" =");
                                sb.Append(value);

                            }
                        }
                        if (StringHelper.IsEqual(column, "MobilePhone"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append("( [C].[PermanentMobile]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                                sb.Append(" or ");
                                sb.Append(" [C].[CellPhone]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                                sb.Append(" ) ");
                            }
                        }
                        if (StringHelper.IsEqual(column, "City"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[PermanentCity]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                            }
                        }
                        if (StringHelper.IsEqual(column, "StateId"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[PermanentStateid]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }

                        if (StringHelper.IsEqual(column, "CountryId"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[PermanentCountryID]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }
                        if (StringHelper.IsEqual(column, "CandidateType"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[MemberType]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }


                        if (StringHelper.IsEqual(column, "SourceId"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[SourceLookupId]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }










                        else if (StringHelper.IsEqual(column, "CreatorId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[CreatorId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else if (StringHelper.IsEqual(column, "hotListManagerId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" OR ");
                            }
                            sb.Append("[C].[Id]");
                            sb.Append(" IN ");
                            //                            sb.Append(@"( SELECT [MG].[MemberId] FROM [MemberGroupMap] [MG] LEFT JOIN [MemberGroupManager] [MGM]
                            //                                    ON [MG].[MemberGroupId]=[MGM].[MemberGroupId]
                            //                                    WHERE [MGM].[MemberId]=" + value + @"
                            //                                    AND [MG].[MemberId] NOT IN (SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId=" + value + ") ");
                            sb.Append(@"( SELECT [MG].[MemberId] FROM [MemberGroupMap] [MG] LEFT JOIN [MemberGroupManager] [MGM]
                                    ON [MG].[MemberGroupId]=[MGM].[MemberGroupId]
                                    WHERE [MGM].[MemberId]=" + value + @"
                                    AND [MG].[MemberId] NOT IN (SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId=" + value + ") ");
                            sb.Append(" ) ");
                        }
                        else if (StringHelper.IsEqual(column, "CompanyId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }

                            //sb.Append("[C].[Id]");
                            //sb.Append(" IN ");
                            //sb.Append(" ( SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId ");
                            //sb.Append(" IN ");
                            //sb.Append("( SELECT DISTINCT MemberId FROM CompanyContact WHERE CompanyId= ");
                            //sb.Append(value);
                            //sb.Append(" ) ");
                            //sb.Append(" ) ");
                            sb.Append("[C].[CreatorId]");
                            sb.Append(" IN ");
                            sb.Append("( SELECT DISTINCT MemberId FROM CompanyContact WHERE MemberId!=0 AND CompanyId= ");
                            sb.Append(value);
                            sb.Append(" ) ");
                        }

                        else if (StringHelper.IsEqual(column, "DateFrom"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[UpdateDate]");
                            sb.Append(" >=convert(datetime,'");
                            sb.Append(value);
                            sb.Append("',103)");
                        }
                        else if (StringHelper.IsEqual(column, "DateTo"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[UpdateDate]");
                            sb.Append(" <=convert(datetime,'");
                            sb.Append(value);
                            sb.Append("',103)");
                        }

                        else if (StringHelper.IsEqual(column, "recentApplicant"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            if (value == "Today")
                                sb.Append(" CONVERT(VARCHAR,[C].[UpdateDate],101) = CONVERT(VARCHAR,GETDATE(),101) ");
                            else if (value == "Yesterday")
                                sb.Append(" DATEDIFF(dd,[C].[UpdateDate],GETDATE()) = 1 ");
                            else if (value == "PastWeek")
                                sb.Append(" DATEDIFF(week,[C].[UpdateDate],GETDATE()) = 1 ");
                            else if (value == "PastMonth")
                                sb.Append(" DATEDIFF(Month,[C].[UpdateDate],GETDATE()) = 1 ");
                            else if (value == "PastQuarter")
                                sb.Append(" DATEDIFF(Quarter,[C].[UpdateDate],GETDATE()) =1  ");
                            else if (value == "PastYear")
                                sb.Append(" DATEDIFF(YEAR,[C].[UpdateDate],GETDATE())=1 ");
                            else if (value == "Monthtilldate")
                                sb.Append(" [C].[UpdateDate] between (SELECT CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(getdate())-1),getdate()),101) AS Date_Value) and  getdate() ");

                        }

                        else if (StringHelper.IsEqual(column, "quickSearchKeyWord"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            //split the search string by space char. if more than one word, use first word as first name and second word as last name
                            string[] valueTokens = value.Split(' ');
                            if (valueTokens.Length > 1)
                            {
                                sb.Append("([C].[FirstName] LIKE '%" + valueTokens[0] + "%'");
                                sb.Append(" OR ");
                                sb.Append("[C].[LastName] LIKE '%" + valueTokens[1] + "%'");
                                sb.Append(" OR ");
                            }
                            else
                            {
                                sb.Append("([C].[FirstName] LIKE '%" + value + "%'");
                                sb.Append(" OR ");
                                sb.Append("[C].[LastName] LIKE '%" + value + "%'");
                                sb.Append(" OR ");
                            }

                            sb.Append("[C].[MiddleName] LIKE '%" + value + "%'");
                            sb.Append(" OR ");
                            sb.Append("[C].[PrimaryEmail] LIKE '%" + value + "%'");
                            sb.Append(" OR ");
                            sb.Append("[C].[AlternateEmail] LIKE '%" + value + "%')");
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "ASC";

            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Candidate> response = new PagedResponse<Candidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<Candidate>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<Candidate> ICandidateDataAccess.GetPagedVendorCandidatePerformance(PagedRequest request)
        {
            const string SP = "dbo.Candidate_GetPagedVendorCandidatePerformance";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");
                        if (StringHelper.IsEqual(column, "CandidateID"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[Id] =");
                            sb.Append(value);
                        }
                        if (StringHelper.IsEqual(column, "memberId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[Id]");
                            sb.Append(" IN ");
                            sb.Append(" ( SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId= ");
                            sb.Append(value);
                            sb.Append(" ) ");
                        }
                        if (StringHelper.IsEqual(column, "CandidateName"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                char[] delim = { ' ' };
                                string[] valueList = value.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                                if (valueList.Length == 2)
                                {
                                    sb.Append(" ([C].[FirstName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[0]);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[LastName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[1]);
                                    sb.Append("%') ");
                                }
                                else if (valueList.Length == 3)
                                {
                                    sb.Append(" ([C].[FirstName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[0]);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[LastName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[1]);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[MiddleName]");
                                    sb.Append(" like '%");
                                    sb.Append(valueList[2]);
                                    sb.Append("%') ");
                                }
                                else
                                {
                                    sb.Append(" ([C].[FirstName]");
                                    sb.Append(" like '%");
                                    sb.Append(value);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[LastName]");
                                    sb.Append(" like '%");
                                    sb.Append(value);
                                    sb.Append("%' ");
                                    sb.Append(" OR [C].[MiddleName]");
                                    sb.Append(" like '%");
                                    sb.Append(value);
                                    sb.Append("%') ");
                                }

                            }
                        }
                        if (StringHelper.IsEqual(column, "CandidateEmail"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append("( [C].[PrimaryEmail]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                                sb.Append(" or ");
                                sb.Append("[C].[AlternateEmail]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                                sb.Append(") ");
                            }
                        }
                    
                        if (StringHelper.IsEqual(column, "HiringLevel"))
                        {
                            if (value.Trim() != "0")
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [MJCD].[SelectionStepLookupId]");
                                sb.Append(" =");
                                sb.Append(value);

                            }
                        }
                        if (StringHelper.IsEqual(column, "MobilePhone"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append("( [C].[PermanentMobile]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                                sb.Append(" or ");
                                sb.Append(" [C].[CellPhone]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                                sb.Append(" ) ");
                            }
                        }
                        if (StringHelper.IsEqual(column, "City"))
                        {
                            if (value.Trim() != string.Empty)
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append(" [C].[PermanentCity]");
                                sb.Append(" like '%");
                                sb.Append(value);
                                sb.Append("%' ");
                            }
                        }
                    

                        if (StringHelper.IsEqual(column, "CreatorId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[CreatorId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                   
                        else if (StringHelper.IsEqual(column, "CompanyId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                        
                            sb.Append("[C].[CreatorId]");
                            sb.Append(" IN ");
                            sb.Append("( SELECT DISTINCT MemberId FROM CompanyContact WHERE MemberId!=0 AND CompanyId= ");
                            sb.Append(value);
                            sb.Append(" ) ");
                        }

                        else if (StringHelper.IsEqual(column, "DateFrom"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[UpdateDate]");
                            sb.Append(" >=convert(datetime,'");
                            sb.Append(value);
                            sb.Append("',103)");
                        }
                        else if (StringHelper.IsEqual(column, "DateTo"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[C].[UpdateDate]");
                            sb.Append(" <=convert(datetime,'");
                            sb.Append(value);
                            sb.Append("',103)");
                        }

                        else if (StringHelper.IsEqual(column, "recentApplicant"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            if (value == "Today")
                                sb.Append(" CONVERT(VARCHAR,[C].[UpdateDate],101) = CONVERT(VARCHAR,GETDATE(),101) ");
                            else if (value == "Yesterday")
                                sb.Append(" DATEDIFF(dd,[C].[UpdateDate],GETDATE()) = 1 ");
                            else if (value == "PastWeek")
                                sb.Append(" DATEDIFF(week,[C].[UpdateDate],GETDATE()) = 1 ");
                            else if (value == "PastMonth")
                                sb.Append(" DATEDIFF(Month,[C].[UpdateDate],GETDATE()) = 1 ");
                            else if (value == "PastQuarter")
                                sb.Append(" DATEDIFF(Quarter,[C].[UpdateDate],GETDATE()) =1  ");
                            else if (value == "PastYear")
                                sb.Append(" DATEDIFF(YEAR,[C].[UpdateDate],GETDATE())=1 ");
                            else if (value == "Monthtilldate")
                                sb.Append(" [C].[UpdateDate] between (SELECT CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(getdate())-1),getdate()),101) AS Date_Value) and  getdate() ");

                        }

                        else if (StringHelper.IsEqual(column, "quickSearchKeyWord"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            //split the search string by space char. if more than one word, use first word as first name and second word as last name
                            string[] valueTokens = value.Split(' ');
                            if (valueTokens.Length > 1)
                            {
                                sb.Append("([C].[FirstName] LIKE '%" + valueTokens[0] + "%'");
                                sb.Append(" OR ");
                                sb.Append("[C].[LastName] LIKE '%" + valueTokens[1] + "%'");
                                sb.Append(" OR ");
                            }
                            else
                            {
                                sb.Append("([C].[FirstName] LIKE '%" + value + "%'");
                                sb.Append(" OR ");
                                sb.Append("[C].[LastName] LIKE '%" + value + "%'");
                                sb.Append(" OR ");
                            }

                            sb.Append("[C].[MiddleName] LIKE '%" + value + "%'");
                            sb.Append(" OR ");
                            sb.Append("[C].[PrimaryEmail] LIKE '%" + value + "%'");
                            sb.Append(" OR ");
                            sb.Append("[C].[AlternateEmail] LIKE '%" + value + "%')");
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "ASC";

            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Candidate> response = new PagedResponse<Candidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new CandidateBuilder()).BuildEntitiesForVendorCandidatePerformance(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }
                return response;             
            }
        }

        private StringBuilder getWhereClauseForReport(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
                   int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
                   int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager)
        {


            StringBuilder whereClause = new StringBuilder();
            if (addedFrom != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                //  whereClause.Append("[C].[CreateDate]");
                whereClause.Append("[C].[CreateDate]");
                whereClause.Append(" >= ");
                // whereClause.Append("'" + addedFrom.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(addedFrom).ToString("dd/MM/yyyy") + "',103) ");
            }
            if (addedTo != null && addedTo != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                // whereClause.Append("[C].[CreateDate]");
                whereClause.Append("[C].[CreateDate]");
                whereClause.Append(" < ");
                //whereClause.Append("'" + addedTo.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(addedTo).AddDays(1).ToString("dd/MM/yyyy") + "',103) ");
            }
            if (addedBy > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[CreatorId]");
                whereClause.Append(" = ");
                whereClause.Append(addedBy.ToString());
            }
            if (addedBySource > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[ResumeSource]");
                whereClause.Append(" = ");
                whereClause.Append(addedBySource.ToString());
            }
            if (updatedFrom != null && updatedFrom != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                //  whereClause.Append("[C].[UpdateDate]");
                whereClause.Append("[C].[UpdateDate]");
                whereClause.Append(" >= ");
                //whereClause.Append("'" + updatedFrom.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(updatedFrom).ToString("dd/MM/yyyy") + "',103) ");
            }
            if (updatedTo != null && updatedTo != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                //  whereClause.Append("[C].[UpdateDate]");
                whereClause.Append("[C].[UpdateDate]");
                whereClause.Append(" < ");
                //whereClause.Append("'" + updatedTo.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(updatedTo).AddDays(1).ToString("dd/MM/yyyy") + "',103) ");

            }
            if (updatedBy > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[UpdatorId]");
                whereClause.Append(" = ");
                whereClause.Append(updatedBy.ToString());
            }

            if (country > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[PermanentCountryId]");//0.8
                whereClause.Append(" = ");
                whereClause.Append(country);
            }
            if (state > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[PermanentStateId]");//0.8
                whereClause.Append(" = ");
                whereClause.Append(state);
            }
            if (!string.IsNullOrEmpty(city))
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[PermanentCity]");//0.8
                whereClause.Append(" LIKE ");
                whereClause.Append("'%" + city + "%'");
            }

            StringBuilder interviewWhereBuilder = new StringBuilder();
            if (interviewFrom != null && interviewFrom != DateTime.MinValue)
            {
                if (interviewLevel > 1 && interviewLevel < 5)
                {
                    if (!StringHelper.IsBlank(interviewWhereBuilder))
                    {
                        interviewWhereBuilder.Append(" AND ");
                    }
                    // interviewWhereBuilder.Append("[A].[StartDateTimeUtc]");
                    interviewWhereBuilder.Append("[dbo].[GetUTCDateOnly]([A].[StartDateTimeUtc])");
                    interviewWhereBuilder.Append(" >= ");
                    // interviewWhereBuilder.Append("'" + interviewFrom.AddHours(-6).ToShortDateString() + "'");

                    interviewWhereBuilder.Append("[dbo].[GetDateOnly]('" + Convert.ToDateTime(interviewFrom) + "')");
                }
            }
            if (interviewTo != null && interviewTo != DateTime.MinValue)
            {
                if (interviewLevel > 1 && interviewLevel < 5)
                {
                    if (!StringHelper.IsBlank(interviewWhereBuilder))
                    {
                        interviewWhereBuilder.Append(" AND ");
                    }
                    // interviewWhereBuilder.Append("[A].[StartDateTimeUtc]");
                    interviewWhereBuilder.Append("[dbo].[GetUTCDateOnly]([A].[StartDateTimeUtc])");
                    interviewWhereBuilder.Append(" <= ");
                    //  interviewWhereBuilder.Append("'" + interviewTo.AddHours(-6).ToShortDateString() + "'");
                    interviewWhereBuilder.Append("[dbo].[GetDateOnly]('" + Convert.ToDateTime(interviewTo) + "')");
                }
            }
            if (interviewLevel > 1 && interviewLevel < 5)
            {
                if (!StringHelper.IsBlank(interviewWhereBuilder))
                {
                    interviewWhereBuilder.Append(" AND ");
                }
                interviewWhereBuilder.Append("[I].[InterviewLevel]");
                interviewWhereBuilder.Append(" = ");
                interviewWhereBuilder.Append(interviewLevel.ToString());
            }



            if (interviewStatus > 0)
            {
                if (!StringHelper.IsBlank(interviewWhereBuilder))
                {
                    interviewWhereBuilder.Append(" AND ");
                }
                interviewWhereBuilder.Append("[I].[Status]");
                interviewWhereBuilder.Append(" = ");
                interviewWhereBuilder.Append(interviewStatus.ToString());
            }

            if (!StringHelper.IsBlank(interviewWhereBuilder))
            {
                if (interviewLevel > 1 && interviewLevel < 5)
                {
                    StringBuilder interviewBuilder = new StringBuilder(@" ( SELECT DISTINCT [M].[Id] FROM
                    [Interview] [I] LEFT JOIN [MemberJobCart] [MJC] ON [I].[MemberJobCartId]=[MJC].[Id]
                    LEFT JOIN [Member] [M] ON [MJC].[MemberId]=[M].[Id]
                    LEFT JOIN [Activity] [A] ON [A].[ActivityId]=[I].[ActivityID]");

                    interviewBuilder.Append(" WHERE ");
                    interviewBuilder.Append(interviewWhereBuilder);

                    interviewBuilder.Append(" ) ");


                    if (!StringHelper.IsBlank(whereClause))
                    {
                        whereClause.Append(" AND ");
                    }
                    whereClause.Append(" [C].[Id] ");
                    whereClause.Append(" IN ");
                    whereClause.Append(interviewBuilder.ToString());
                }
            }

            StringBuilder experienceWhereBuilder = new StringBuilder();
            if (industry > 0)
            {
                if (!StringHelper.IsBlank(experienceWhereBuilder))
                {
                    experienceWhereBuilder.Append(" AND ");
                }
                experienceWhereBuilder.Append("[ME].[IndustryCategoryLookupId]");
                experienceWhereBuilder.Append(" = ");
                experienceWhereBuilder.Append(industry.ToString());
            }
            if (functionalCategory > 0)
            {
                if (!StringHelper.IsBlank(experienceWhereBuilder))
                {
                    experienceWhereBuilder.Append(" AND ");
                }
                experienceWhereBuilder.Append("[ME].[FunctionalCategoryLookupId]");
                experienceWhereBuilder.Append(" = ");
                experienceWhereBuilder.Append(functionalCategory.ToString());
            }

            if (!StringHelper.IsBlank(experienceWhereBuilder))
            {
                StringBuilder experienceBuilder = new StringBuilder(@" ( SELECT DISTINCT [ME].[MemberId] FROM
                [MemberExperience] [ME]");
                experienceBuilder.Append(" WHERE ");
                experienceBuilder.Append(experienceWhereBuilder);
                experienceBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(experienceBuilder.ToString());
            }

            if (workPermit > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[ME].[WorkAuthorizationLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(workPermit.ToString());
            }
            if (WorkSchedule > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[ME].[WorkScheduleLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(WorkSchedule.ToString());
            }

            if (gender > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[GenderLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(gender.ToString());
            }

            if (maritalStatus > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[MaritalStatusLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(maritalStatus.ToString());
            }

            if (educationId > 0)
            {
                StringBuilder educationBuilder = new StringBuilder(@" ( SELECT DISTINCT [ME].[MemberId] FROM
                [MemberEducation] [ME]");
                educationBuilder.Append(" WHERE ");
                educationBuilder.Append(" [ME].[LevelOfEducationLookupId] ");
                educationBuilder.Append(" = ");
                educationBuilder.Append(educationId.ToString());
                educationBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(educationBuilder.ToString());
            }

            if (assessmentId > 0)
            {
                StringBuilder assessmentBuilder = new StringBuilder(@" ( SELECT DISTINCT [MT].[MemberId] FROM
                [MemberTestScore] [MT]");
                assessmentBuilder.Append(" WHERE ");
                assessmentBuilder.Append(" [MT].[TestMasterId] ");
                assessmentBuilder.Append(" = ");
                assessmentBuilder.Append(assessmentId.ToString());
                assessmentBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(assessmentBuilder.ToString());
            }

            if (isBroadcastedResume)
            {
                StringBuilder broadcastResumeBuilder = new StringBuilder(@" ( SELECT DISTINCT [ActivityOnObjectId]
                FROM [MemberActivity] [MA] LEFT JOIN
                [MemberActivityType] [MAT] ON [MA].[MemberActivityTypeId]=[MAT].[Id] ");
                broadcastResumeBuilder.Append(" WHERE ");
                broadcastResumeBuilder.Append(" [MA].[ActivityOn] ");
                broadcastResumeBuilder.Append(" = ");
                broadcastResumeBuilder.Append((int)MemberType.Candidate);
                broadcastResumeBuilder.Append(" AND ");
                broadcastResumeBuilder.Append(" [MAT].[ActivityType] ");
                broadcastResumeBuilder.Append(" = ");
                broadcastResumeBuilder.Append((int)ActivityType.ResumeBroadCast);
                broadcastResumeBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(broadcastResumeBuilder.ToString());
            }

            if (isReferredApplicants)
            {
                StringBuilder referredApplicantsBuilder = new StringBuilder(@" ( SELECT [Id] FROM [Candidate] ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[CreatorId] ");
                whereClause.Append(" IN ");
                whereClause.Append(referredApplicantsBuilder.ToString());
            }

            if (hasJobAgent)
            {

            }

            //Assigned Manager
            if (assignedManager > 0)
            {
                StringBuilder assignedManagerBuilder = new StringBuilder(@" ( SELECT DISTINCT [MemberId]
                                                FROM [MemberManager] [MM]");
                assignedManagerBuilder.Append(" WHERE ");
                assignedManagerBuilder.Append(" [MM].[ManagerId] ");
                assignedManagerBuilder.Append(" = ");
                assignedManagerBuilder.Append(assignedManager);
                assignedManagerBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(assignedManagerBuilder.ToString());
            }
            if (interviewLevel == 1 || interviewLevel == 5)
            {
                StringBuilder tmpinterviewWhereBuilder = new StringBuilder();
                tmpinterviewWhereBuilder.Append("[MJCD].[SelectionStepLookupId]");
                tmpinterviewWhereBuilder.Append(" = ");
                tmpinterviewWhereBuilder.Append(interviewLevel.ToString());


                StringBuilder interviewBuilder = new StringBuilder(@" ( SELECT DISTINCT [M].[Id] FROM
                        [MemberJobCartDetail] [MJCD] LEFT JOIN [MemberJobCart] [MJC] ON [MJCD].[MemberJobCartId]=[MJC].[Id]
                        LEFT JOIN [Member] [M] ON [MJC].[MemberId]=[M].[Id]");

                interviewBuilder.Append(" WHERE ");
                interviewBuilder.Append(tmpinterviewWhereBuilder);

                interviewBuilder.Append(" ) ");


                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" And  ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(interviewBuilder.ToString());


            }
            return whereClause;
        }
        PagedResponse<Candidate> ICandidateDataAccess.GetPagedForReport(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager)
        {
            const string SP = "dbo.Candidate_GetPagedReport";
            StringBuilder whereClause = new StringBuilder();
            whereClause = getWhereClauseForReport(request, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
             country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
             gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager);

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    String.Empty
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Candidate> response = new PagedResponse<Candidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<Candidate>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }


        PagedResponse<DynamicDictionary> ICandidateDataAccess.GetPagedForReportBySelectedColumn(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
        int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
        int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, IList<string> CheckedList)
        {
            string Column = "";
            foreach (string s in CheckedList)
            {
                if (Column != "") Column += ",";
                Column += s;
            }
            const string SP = "dbo.Candidate_GetPagedReportForSelectedColumns";
            StringBuilder whereClause = new StringBuilder();
            whereClause = getWhereClauseForReport(request, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
             country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
             gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager);
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                   Column 
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<DynamicDictionary> response = new PagedResponse<DynamicDictionary>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    response.Response = (new CandidateBuilder()).BuildEntities(reader, CheckedList);// CreateEntityBuilder<Candidate>().BuildEntities(reader, CheckedList);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        //custom - added applicant type
        PagedResponse<Candidate> ICandidateDataAccess.GetAllOnSearch(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType) // 0.2  // 9372
        {

            string SP = string.Empty;
            SP = "dbo.Candidate_Search_GetPaged";

            string whereClause = string.Empty;

            whereClause = GetSearchQuery(allKeyWords, anyKeyWord, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, functionalCategory, hiringMatrixId, hotListId, memberId, memberPositionId, isMyList, minExpYr, maxExpYr, strState, applicantType, ""); //0.2

            string searchKey = GetSearchKey(allKeyWords, anyKeyWord);

            searchKey = searchKey.Replace("(", string.Empty);   // 10289
            searchKey = searchKey.Replace(")", string.Empty);  // 10289
            searchKey = searchKey.Replace("[", string.Empty);  // 10290
            searchKey = searchKey.Replace("]", string.Empty);  // 10290                        

            searchKey = searchKey.Replace("'", "char(39)"); //  10288 

            //evan - load containsPhrase var

            string containsPhrase = string.Empty;
            //if (!StringHelper.IsBlank(allKeyWords))
            //{
            //    string[] keyWords = StringHelper.BuildSearchKeyWordArray(allKeyWords);

            //    foreach (string str in keyWords)
            //    {
            //        switch (str.ToLower().Trim())
            //        {
            //            case "and":
            //                {
            //                    if ((!containsPhrase.Trim().EndsWith(" and")) && (!containsPhrase.Trim().EndsWith(" or")))
            //                    {
            //                        containsPhrase = containsPhrase + " AND ";
            //                    }
            //                    break;

            //                }
            //            case "or":
            //                {
            //                    if ((!containsPhrase.Trim().EndsWith(" and")) && (!containsPhrase.Trim().EndsWith(" or")))
            //                    {
            //                        containsPhrase = containsPhrase + " OR ";
            //                    }

            //                    break;
            //                }

            //            case "":
            //                {
            //                    break;
            //                }

            //            default:
            //                {
            //                    containsPhrase = containsPhrase + "\"" + str + "\"";
            //                    break;
            //                }
            //        }
            //    }
            //}
            containsPhrase = allKeyWords;

            object[] paramValues = new object[] {	request.PageIndex,
													int.Parse(rowPerPage),
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    searchKey,
                                                    maxResults,
                                                    containsPhrase
												};
            //if (isVolumeHire == "1")
            //{
            //    paramValues = new object[] {	request.PageIndex,
            //                                        int.Parse(rowPerPage),
            //                                        StringHelper.Convert(whereClause),
            //                                        StringHelper.Convert(request.SortColumn),
            //                                        StringHelper.Convert(request.SortOrder)
            //                                };
            //}

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Candidate> response = new PagedResponse<Candidate>();

                cmd.CommandTimeout = 600; // Time out to be fetched from web.config               

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new CandidateBuilder()).BuildSearchEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }

                    // 9372
                    if (maxResults > 0)
                    {
                        if ((reader.NextResult()) && (reader.Read()))
                        {
                            response.TotalRow = reader.GetInt32(0);
                        }
                    }
                    // 9372

                }

                return response;
            }
        }


        PagedResponse<Candidate> ICandidateDataAccess.GetAllOnSearchForPrecise(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddWhereClause, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string CandidateID) // 0.2  // 9372

        {

            string SP = string.Empty;
            SP = "dbo.Candidate_Search_GetPaged";

            string whereClause = string.Empty;


            whereClause = GetSearchQueryForPrecise(allKeyWords, anyKeyWord, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, memberPositionId, isMyList, minExpYr, maxExpYr, strState, applicantType, Education, WorkSchedule, Gender, Nationality, Industry, JobFunction, HiringStatus, CurrentCompany, CompanyStatus, CandidateEmail, MobilePhone, ZipCode, Radius, AddWhereClause, IsMile, AssignedManagerId, SalarySelector, internalRating,"0",CandidateID,string.Empty ); //0.2


            


            string searchKey = GetSearchKey(allKeyWords, anyKeyWord);

            searchKey = searchKey.Replace("(", string.Empty);   // 10289
            searchKey = searchKey.Replace(")", string.Empty);  // 10289
            searchKey = searchKey.Replace("[", string.Empty);  // 10290
            searchKey = searchKey.Replace("]", string.Empty);  // 10290                        

            searchKey = searchKey.Replace("'", "char(39)"); //  10288 

            //evan - load containsPhrase var

            string containsPhrase = string.Empty;
            containsPhrase = allKeyWords;

            object[] paramValues = new object[] {	request.PageIndex,
													int.Parse(rowPerPage),
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    searchKey,
                                                    maxResults,
                                                    containsPhrase,
                                                    "("+HiringStatus +")",
                                                    AddWhereClause ? string .Empty : ZipCode ,
                                                    AddWhereClause ? string .Empty :Radius ,
                                                    IsMile ,
                                                    AddWhereClause ? string .Empty :currentCity ,
                                                    AddWhereClause ? string .Empty :strState 
												};


            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Candidate> response = new PagedResponse<Candidate>();

                cmd.CommandTimeout = 600; // Time out to be fetched from web.config               

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new CandidateBuilder()).BuildSearchEntities(reader, "");

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }

                    // 9372
                    if (maxResults > 0 && maxResults < response.TotalRow)
                    {
                        if ((reader.NextResult()) && (reader.Read()))
                        {
                            response.TotalRow = reader.GetInt32(0);
                        }
                    }
                    // 9372

                }

                return response;
            }
        }
        //*************Added parameter bool ShowPreciseSearch by pravin khot on 8/Aug/2016
        PagedResponse<Candidate> ICandidateDataAccess.GetAllOnSearchForPrecises(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddWhereClause, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string SourceID, string CandidateID, string NoticePeriod,bool ShowPreciseSearch) // 0.2  // 9372
        {

            string SP = string.Empty;
            SP = "dbo.candidate_search_getpaged-07052012";

            string whereClause = string.Empty;

            whereClause = GetSearchQueryForPrecise(allKeyWords, anyKeyWord, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, industryType, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, functionalCategory, hiringMatrixId, hotListId, memberId, memberPositionId, isMyList, minExpYr, maxExpYr, strState, applicantType, Education, WorkSchedule, Gender, Nationality, Industry, JobFunction, HiringStatus, CurrentCompany, CompanyStatus, CandidateEmail, MobilePhone, ZipCode, Radius, AddWhereClause, IsMile, AssignedManagerId, SalarySelector, internalRating, SourceID, CandidateID, NoticePeriod); //0.2

            string searchKey = GetSearchKey(allKeyWords, anyKeyWord);

            searchKey = searchKey.Replace("(", string.Empty);   // 10289
            searchKey = searchKey.Replace(")", string.Empty);  // 10289
            searchKey = searchKey.Replace("[", string.Empty);  // 10290
            searchKey = searchKey.Replace("]", string.Empty);  // 10290                        

            searchKey = searchKey.Replace("'", "char(39)"); //  10288 

            //evan - load containsPhrase var

            string containsPhrase = string.Empty;
            containsPhrase = allKeyWords;

            object[] paramValues = new object[] {	request.PageIndex,
													int.Parse(rowPerPage),
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    searchKey,
                                                    maxResults,
                                                    containsPhrase,
                                                    "("+HiringStatus +")",
                                                    AddWhereClause ? string .Empty : ZipCode ,
                                                    AddWhereClause ? string .Empty :Radius ,
                                                    IsMile ,
                                                    AddWhereClause ? string .Empty :currentCity ,
                                                    AddWhereClause ? string .Empty :strState ,
                                                    ShowPreciseSearch //parameter added by pravin khot on 8/Aug/2016 
												};


            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Candidate> response = new PagedResponse<Candidate>();

                cmd.CommandTimeout = 600; // Time out to be fetched from web.config               

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new CandidateBuilder()).BuildSearchEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }

                    // 9372
                    if (maxResults > 0 && maxResults < response.TotalRow)
                    {
                        if ((reader.NextResult()) && (reader.Read()))
                        {
                            response.TotalRow = reader.GetInt32(0);
                        }
                    }
                    // 9372

                }

                return response;
            }
        }

        public Candidate GetOnSearchForPrecise_Sub(int CandidateId)
        {
            string sp_name = "Candidate_Search_GetPaged_Sub";
            using (DbCommand cmd = Database.GetStoredProcCommand(sp_name))
            {
                Database.AddInParameter(cmd, "@CandidateID", DbType.Int32, CandidateId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        Candidate c = (new CandidateBuilder()).BuildSearchEntity_Sub(reader);
                        return c;
                    }
                    return new Candidate();
                }
            }
        }
        IList<Candidate> ICandidateDataAccess.GetAllOnSearchForSearchAgent(string allKeyWords, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string hotListId, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType, string Education, string WorkSchedule,string HiringStatus, int JobPostingId)
        {

            string SP = string.Empty;
            SP = "dbo.Candidate_Search_GetPaged";

            string whereClause = string.Empty;


            whereClause = GetSearchQueryForPrecise(allKeyWords, string.Empty, currentPosition, currentCity, currentCountryId, jobType, workStatus, securityClearence, availability, string.Empty, lastUpdateDate, salaryType, salaryFrom, salaryTo, currencyType, string.Empty, string.Empty, hotListId, string.Empty, string.Empty, string.Empty, minExpYr, maxExpYr, strState, applicantType, Education, WorkSchedule, string.Empty, string.Empty, string.Empty, string.Empty, HiringStatus, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, true, false, "0", "0", "0", "0", "0", string.Empty);

            if (whereClause != string.Empty)
                whereClause += " AND ";
            whereClause += "[C].[Id] not in (select MemberId from SearchAgentMailToCandidate where JobPostingId=" + JobPostingId + ") ";
            whereClause += " And [C].[AutomatedEmailStatus]=1";
            string searchKey = GetSearchKey(allKeyWords, string.Empty);

            searchKey = searchKey.Replace("(", string.Empty);   // 10289
            searchKey = searchKey.Replace(")", string.Empty);  // 10289
            searchKey = searchKey.Replace("[", string.Empty);  // 10290
            searchKey = searchKey.Replace("]", string.Empty);  // 10290                        

            searchKey = searchKey.Replace("'", "char(39)"); //  10288 

            //evan - load containsPhrase var

            string containsPhrase = string.Empty;
            containsPhrase = allKeyWords;

            object[] paramValues = new object[] {	0,
													0,
													StringHelper.Convert(whereClause),
													string .Empty ,
                                                   string .Empty ,
                                                    searchKey,
                                                    maxResults,
                                                    containsPhrase,
                                                    "("+HiringStatus +")",
                                                    "","10",false ,"",""

												};


            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                IList<Candidate> response = new List<Candidate>();

                cmd.CommandTimeout = 600; // Time out to be fetched from web.config               

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response = (new CandidateBuilder()).BuildSearchEntities(reader, "");
                }

                return response;
            }
        }

        PagedResponse<Candidate> ICandidateDataAccess.GetAllOnSearchForSavedQuery(PagedRequest request, string WhereClause, string memberId, string rowPerPage)
        {
            const string SP = "dbo.Candidate_Search_GetPaged";

            string whereClause = string.Empty;

            whereClause = WhereClause;

            object[] paramValues = new object[] {	request.PageIndex,
                                                    int.Parse(rowPerPage),
                                                    StringHelper.Convert(whereClause),
                                                    StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    string.Empty
                                                };

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Candidate> response = new PagedResponse<Candidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new CandidateBuilder()).BuildSearchEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);

                    }
                }

                return response;
            }
        }



        PagedResponse<Candidate> ICandidateDataAccess.GetPagedForSubmittedCandidate(PagedRequest request)
        {
            const string SP = "dbo.SubmittedCandidate_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "memberId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[E].[Id]");
                            sb.Append(" IN ");
                            sb.Append(" ( SELECT DISTINCT MemberId FROM MemberManager WHERE ManagerId= ");
                            sb.Append(value);
                            sb.Append(" ) ");
                        }
                        else if (StringHelper.IsEqual(column, "selectionStep"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[E].[Id]");
                            sb.Append(" IN ");
                            sb.Append(@" ( SELECT DISTINCT [MJC].[MemberId] FROM [MemberJobCart] [MJC] LEFT JOIN
[MemberJobCartDetail] [MJCD] ON [MJC].[Id]=[MJCD].[MemberJobCartId]
WHERE [MJCD].[SelectionStepLookupId]=");
                            sb.Append(value);
                            sb.Append(" ) ");
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[E].[FirstName]";
            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Candidate> response = new PagedResponse<Candidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new CandidateBuilder()).BuildSearchEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        int ICandidateDataAccess.GetCandidateCountByMemberID(int MemberID)
        {
            if (MemberID < 1)
            {
                throw new ArgumentNullException("MemberID");
            }
            const string SP = "dbo.Candidate_GetCandidateCountByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader != null ? Convert.ToInt32(reader.GetValue(0).ToString()) : 0;
                    }
                    return 0;
                }
            }
        }


        // ********Code added by Sumit Sonawane on 20/Jan/2017********
        string ICandidateDataAccess.GetCandidateAgeByMemberID(int MemberID)
        {
            if (MemberID < 1)
            {
                throw new ArgumentNullException("MemberID");
            }
            const string SP = "dbo.Candidate_GetAgeDateOfBirthById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberID);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        //return reader != null ? Convert.ToInt32(reader.GetValue(0).ToString()) : 0;
                        return reader.IsDBNull(0) ? "" : reader.GetString(0);
                    }
                    return "";
                }
            }
        }
        // *************************************************End*****************************************

        ///////////////////////////////////////////Sumit 24/01/2017//////////////////////////////////////////////////////
        public int GetAllCandidateCountFromView(string jobCrtor, int jobid, int Step)
        {
            const string SP = "dbo.GetAllCandidateCountFromview";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@jobCrtor", DbType.String, jobCrtor);
                Database.AddInParameter(cmd, "@jobid", DbType.Int32, jobid);
                Database.AddInParameter(cmd, "@Step", DbType.Int32, Step);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader != null ? Convert.ToInt32(reader.GetValue(0).ToString()) : 0;
                    }
                    return 0;
                }
            }
        }
        // *************************************************End*****************************************

        int ICandidateDataAccess.GetCandidateIdbyPrimaryEmail(string PrimaryEmail)
        {
            const string SP = "dbo.Candidate_GetIdByPrimaryEmail";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@PrimaryEmail", DbType.String, PrimaryEmail);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader != null ? Convert.ToInt32(reader.GetValue(0).ToString()) : 0;
                    }
                    return 0;
                }
            }
        }

        void  ICandidateDataAccess.GetMemberUpdateCreatorId(int id, int creatorid)
        {
            const string SP = "dbo.Member_UpdateCreatorId";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, creatorid);
                Database.ExecuteNonQuery(cmd);
            }

        }

        bool ICandidateDataAccess.CheckCandidateDuplication(string firstName, string lastName, string mobileNo, DateTime dob, string email, int IdCardLookUpId, string IdCardDetail) 
        {
            const string SP = "dbo.CheckCandidateDuplication";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                Database.AddInParameter(cmd, "@FirstName", DbType.String, firstName);
                Database.AddInParameter(cmd, "@LastName", DbType.String, lastName);
                Database.AddInParameter(cmd, "@mobileno", DbType.String, mobileNo);
                Database.AddInParameter(cmd, "@Dob", DbType.DateTime, NullConverter.Convert(dob));
                Database.AddInParameter(cmd, "@email", DbType.String, email);
                Database.AddInParameter(cmd, "@IdCardLookUpId", DbType.Int32, IdCardLookUpId);
                Database.AddInParameter(cmd, "@IdCardDetail", DbType.String, IdCardDetail);
                using (IDataReader reader = Database.ExecuteReader(cmd)) 
                {
                    while (reader.Read()) 
                    {
                        return reader .IsDBNull (0)? false : reader.GetBoolean(0) ;
                    }
                   return false;
                }
            }
        
        }

        string ICandidateDataAccess.GetCandidateEmailById(int Id) 
        {
            const string SP = "dbo.Candidate_GetCandidaeEmailIdById";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                Database.AddInParameter(cmd, "@ID", DbType.Int32, Id);

                using (IDataReader reader = Database.ExecuteReader(cmd)) 
                {
                    while (reader.Read()) 
                    {
                        return reader.IsDBNull(0) ? "" : reader.GetString(0);
                    }
                    return "";
                }
            }
        }

        string ICandidateDataAccess.GetCandidateNameByEmailId(string EmailId)
        {
            const string SP = "dbo.Candidate_GetCandidateNameBYEmailId";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CandidateEmail", DbType.String, EmailId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.IsDBNull(0) ? "" : reader.GetString(0);
                    }
                    return "";
                }
            }
        }


        //*********************Code added by pravin khot on 4/March/2016*******************************
        PagedResponse<Candidate> ICandidateDataAccess.GetPagedCandidateCDMDetailsInfoReport(PagedRequest request)
        {

            const string SP = "dbo.Candidate_CDM_Details";
            //StringBuilder whereClause = new StringBuilder();
            string whereClause = string.Empty;
            string emptystr = string.Empty;


            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "SourcedDateFrom"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                              
                                sb.Append("[G8].[JoiningDate]");
                               
                                //**********Code commented by pravin khot on 16/March/2016*********
                                //sb.Append(" >=convert(datetime,'");
                                //sb.Append(value);
                                //sb.Append("',103)");
                                //**************END****************************

                                //**********Code line added by pravin khot on 16/March/2016*********
                                sb.Append(" >= ");
                                sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(0).ToString("dd/MM/yyyy") + "',103) ");
                                //**************END*************************
                            }
                        }
                        else if (StringHelper.IsEqual(column, "SourcedDateTo"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.ToString() != String.Empty)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append("[G8].[JoiningDate]");
                               
                                //**********Code commented by pravin khot on 16/March/2016*********
                                //sb.Append(" <=convert(datetime,'");
                                //sb.Append(value);
                                //sb.Append("',103)");
                                //**************END****************************

                                //**********Code line added by pravin khot on 16/March/2016*********
                                sb.Append(" <= ");
                                sb.Append("Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(0).ToString("dd/MM/yyyy") + "',103) ");
                                //**************END*************************

                            }
                        }

                    }
                }
                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "ASC";

            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder), 
                                                    emptystr
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Candidate> response = new PagedResponse<Candidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    //response.Response = CreateEntityBuilder<Candidate>().BuildEntities(reader);
                    response.Response =(new CandidateBuilder()).BuildCDMEntities(reader);
                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }                
                }
                return response;
            }
        }
        //****************************END**********************************************

        ///////////////////////Code Added by Sumit Sonawane on 23/May/2016////////////////////////////////////////////////

        private StringBuilder getWhereClauseForReport(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
                 int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
                 int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, bool IsTeamReport, string TeamMemberIds, int TeamId)
        {


            StringBuilder whereClause = new StringBuilder();
            if (addedFrom != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                //  whereClause.Append("[C].[CreateDate]");
                whereClause.Append("[C].[CreateDate]");
                whereClause.Append(" >= ");
                // whereClause.Append("'" + addedFrom.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(addedFrom).ToString("dd/MM/yyyy") + "',103) ");
            }
            if (addedTo != null && addedTo != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                // whereClause.Append("[C].[CreateDate]");
                whereClause.Append("[C].[CreateDate]");
                whereClause.Append(" < ");
                //whereClause.Append("'" + addedTo.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(addedTo).AddDays(1).ToString("dd/MM/yyyy") + "',103) ");
            }
            if (addedBy > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[CreatorId]");
                whereClause.Append(" = ");
                whereClause.Append(addedBy.ToString());
            }
            if (IsTeamReport && addedBy == 0 && TeamId == 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }

                whereClause.Append("[C].[CreatorId]");
                whereClause.Append(" ");
                whereClause.Append("IN");
                whereClause.Append(" ");
                whereClause.Append("(");
                whereClause.Append("SELECT DISTINCT Employeeid FROM EmployeeTeamDetail [ETD] Join EmployeeTeam [ET] ON [ET].id=[ETD].EmployeeTeamid Where [ET].TeamLeader=");
                whereClause.Append(TeamMemberIds);
                whereClause.Append(")");
            }
            if (IsTeamReport && TeamId > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }

                whereClause.Append("[C].[CreatorId]");
                whereClause.Append(" ");
                whereClause.Append("IN");
                whereClause.Append(" ");
                whereClause.Append("(");
                whereClause.Append("SELECT DISTINCT Employeeid FROM EmployeeTeamDetail [ETD] Join EmployeeTeam [ET] ON [ET].id=[ETD].EmployeeTeamid Where [ET].TeamLeader=");
                whereClause.Append(TeamMemberIds);
                whereClause.Append(" ");
                whereClause.Append("AND");
                whereClause.Append(" ");
                whereClause.Append("[ET].Id=");
                whereClause.Append(TeamId);
                whereClause.Append(")");
            }

            if (addedBySource > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[ResumeSource]");
                whereClause.Append(" = ");
                whereClause.Append(addedBySource.ToString());
            }
            if (updatedFrom != null && updatedFrom != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                //  whereClause.Append("[C].[UpdateDate]");
                whereClause.Append("[C].[UpdateDate]");
                whereClause.Append(" >= ");
                //whereClause.Append("'" + updatedFrom.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(updatedFrom).ToString("dd/MM/yyyy") + "',103) ");
            }
            if (updatedTo != null && updatedTo != DateTime.MinValue)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                //  whereClause.Append("[C].[UpdateDate]");
                whereClause.Append("[C].[UpdateDate]");
                whereClause.Append(" < ");
                //whereClause.Append("'" + updatedTo.ToShortDateString() + "'");
                whereClause.Append("Convert(DateTime,'" + Convert.ToDateTime(updatedTo).AddDays(1).ToString("dd/MM/yyyy") + "',103) ");

            }
            if (updatedBy > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[UpdatorId]");
                whereClause.Append(" = ");
                whereClause.Append(updatedBy.ToString());
            }

            if (country > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[PermanentCountryId]");//0.8
                whereClause.Append(" = ");
                whereClause.Append(country);
            }
            if (state > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[PermanentStateId]");//0.8
                whereClause.Append(" = ");
                whereClause.Append(state);
            }
            if (!string.IsNullOrEmpty(city))
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[PermanentCity]");//0.8
                whereClause.Append(" LIKE ");
                whereClause.Append("'%" + city + "%'");
            }

            StringBuilder interviewWhereBuilder = new StringBuilder();
            if (interviewFrom != null && interviewFrom != DateTime.MinValue)
            {
                if (interviewLevel > 1 && interviewLevel < 5)
                {
                    if (!StringHelper.IsBlank(interviewWhereBuilder))
                    {
                        interviewWhereBuilder.Append(" AND ");
                    }
                    // interviewWhereBuilder.Append("[A].[StartDateTimeUtc]");
                    interviewWhereBuilder.Append("[dbo].[GetUTCDateOnly]([A].[StartDateTimeUtc])");
                    interviewWhereBuilder.Append(" >= ");
                    // interviewWhereBuilder.Append("'" + interviewFrom.AddHours(-6).ToShortDateString() + "'");

                    interviewWhereBuilder.Append("[dbo].[GetDateOnly]('" + Convert.ToDateTime(interviewFrom) + "')");
                }
            }
            if (interviewTo != null && interviewTo != DateTime.MinValue)
            {
                if (interviewLevel > 1 && interviewLevel < 5)
                {
                    if (!StringHelper.IsBlank(interviewWhereBuilder))
                    {
                        interviewWhereBuilder.Append(" AND ");
                    }
                    // interviewWhereBuilder.Append("[A].[StartDateTimeUtc]");
                    interviewWhereBuilder.Append("[dbo].[GetUTCDateOnly]([A].[StartDateTimeUtc])");
                    interviewWhereBuilder.Append(" <= ");
                    //  interviewWhereBuilder.Append("'" + interviewTo.AddHours(-6).ToShortDateString() + "'");
                    interviewWhereBuilder.Append("[dbo].[GetDateOnly]('" + Convert.ToDateTime(interviewTo) + "')");
                }
            }
            if (interviewLevel > 1 && interviewLevel < 5)
            {
                if (!StringHelper.IsBlank(interviewWhereBuilder))
                {
                    interviewWhereBuilder.Append(" AND ");
                }
                interviewWhereBuilder.Append("[I].[InterviewLevel]");
                interviewWhereBuilder.Append(" = ");
                interviewWhereBuilder.Append(interviewLevel.ToString());
            }



            if (interviewStatus > 0)
            {
                if (!StringHelper.IsBlank(interviewWhereBuilder))
                {
                    interviewWhereBuilder.Append(" AND ");
                }
                interviewWhereBuilder.Append("[I].[Status]");
                interviewWhereBuilder.Append(" = ");
                interviewWhereBuilder.Append(interviewStatus.ToString());
            }

            if (!StringHelper.IsBlank(interviewWhereBuilder))
            {
                if (interviewLevel > 1 && interviewLevel < 5)
                {
                    StringBuilder interviewBuilder = new StringBuilder(@" ( SELECT DISTINCT [M].[Id] FROM
                    [Interview] [I] LEFT JOIN [MemberJobCart] [MJC] ON [I].[MemberJobCartId]=[MJC].[Id]
                    LEFT JOIN [Member] [M] ON [MJC].[MemberId]=[M].[Id]
                    LEFT JOIN [Activity] [A] ON [A].[ActivityId]=[I].[ActivityID]");

                    interviewBuilder.Append(" WHERE ");
                    interviewBuilder.Append(interviewWhereBuilder);

                    interviewBuilder.Append(" ) ");


                    if (!StringHelper.IsBlank(whereClause))
                    {
                        whereClause.Append(" AND ");
                    }
                    whereClause.Append(" [C].[Id] ");
                    whereClause.Append(" IN ");
                    whereClause.Append(interviewBuilder.ToString());
                }
            }

            StringBuilder experienceWhereBuilder = new StringBuilder();
            if (industry > 0)
            {
                if (!StringHelper.IsBlank(experienceWhereBuilder))
                {
                    experienceWhereBuilder.Append(" AND ");
                }
                experienceWhereBuilder.Append("[ME].[IndustryCategoryLookupId]");
                experienceWhereBuilder.Append(" = ");
                experienceWhereBuilder.Append(industry.ToString());
            }
            if (functionalCategory > 0)
            {
                if (!StringHelper.IsBlank(experienceWhereBuilder))
                {
                    experienceWhereBuilder.Append(" AND ");
                }
                experienceWhereBuilder.Append("[ME].[FunctionalCategoryLookupId]");
                experienceWhereBuilder.Append(" = ");
                experienceWhereBuilder.Append(functionalCategory.ToString());
            }

            if (!StringHelper.IsBlank(experienceWhereBuilder))
            {
                StringBuilder experienceBuilder = new StringBuilder(@" ( SELECT DISTINCT [ME].[MemberId] FROM
                [MemberExperience] [ME]");
                experienceBuilder.Append(" WHERE ");
                experienceBuilder.Append(experienceWhereBuilder);
                experienceBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(experienceBuilder.ToString());
            }

            if (workPermit > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[ME].[WorkAuthorizationLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(workPermit.ToString());
            }
            if (WorkSchedule > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[ME].[WorkScheduleLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(WorkSchedule.ToString());
            }

            if (gender > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[GenderLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(gender.ToString());
            }

            if (maritalStatus > 0)
            {
                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append("[C].[MaritalStatusLookupId]");
                whereClause.Append(" = ");
                whereClause.Append(maritalStatus.ToString());
            }

            if (educationId > 0)
            {
                StringBuilder educationBuilder = new StringBuilder(@" ( SELECT DISTINCT [ME].[MemberId] FROM
                [MemberEducation] [ME]");
                educationBuilder.Append(" WHERE ");
                educationBuilder.Append(" [ME].[LevelOfEducationLookupId] ");
                educationBuilder.Append(" = ");
                educationBuilder.Append(educationId.ToString());
                educationBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(educationBuilder.ToString());
            }

            if (assessmentId > 0)
            {
                StringBuilder assessmentBuilder = new StringBuilder(@" ( SELECT DISTINCT [MT].[MemberId] FROM
                [MemberTestScore] [MT]");
                assessmentBuilder.Append(" WHERE ");
                assessmentBuilder.Append(" [MT].[TestMasterId] ");
                assessmentBuilder.Append(" = ");
                assessmentBuilder.Append(assessmentId.ToString());
                assessmentBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(assessmentBuilder.ToString());
            }

            if (isBroadcastedResume)
            {
                StringBuilder broadcastResumeBuilder = new StringBuilder(@" ( SELECT DISTINCT [ActivityOnObjectId]
                FROM [MemberActivity] [MA] LEFT JOIN
                [MemberActivityType] [MAT] ON [MA].[MemberActivityTypeId]=[MAT].[Id] ");
                broadcastResumeBuilder.Append(" WHERE ");
                broadcastResumeBuilder.Append(" [MA].[ActivityOn] ");
                broadcastResumeBuilder.Append(" = ");
                broadcastResumeBuilder.Append((int)MemberType.Candidate);
                broadcastResumeBuilder.Append(" AND ");
                broadcastResumeBuilder.Append(" [MAT].[ActivityType] ");
                broadcastResumeBuilder.Append(" = ");
                broadcastResumeBuilder.Append((int)ActivityType.ResumeBroadCast);
                broadcastResumeBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(broadcastResumeBuilder.ToString());
            }

            if (isReferredApplicants)
            {
                StringBuilder referredApplicantsBuilder = new StringBuilder(@" ( SELECT [Id] FROM [Candidate] ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[CreatorId] ");
                whereClause.Append(" IN ");
                whereClause.Append(referredApplicantsBuilder.ToString());
            }

            if (hasJobAgent)
            {

            }

            //Assigned Manager
            if (assignedManager > 0)
            {
                StringBuilder assignedManagerBuilder = new StringBuilder(@" ( SELECT DISTINCT [MemberId]
                                                FROM [MemberManager] [MM]");
                assignedManagerBuilder.Append(" WHERE ");
                assignedManagerBuilder.Append(" [MM].[ManagerId] ");
                assignedManagerBuilder.Append(" = ");
                assignedManagerBuilder.Append(assignedManager);
                assignedManagerBuilder.Append(" ) ");

                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" AND ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(assignedManagerBuilder.ToString());
            }
            if (interviewLevel == 1 || interviewLevel == 5)
            {
                StringBuilder tmpinterviewWhereBuilder = new StringBuilder();
                tmpinterviewWhereBuilder.Append("[MJCD].[SelectionStepLookupId]");
                tmpinterviewWhereBuilder.Append(" = ");
                tmpinterviewWhereBuilder.Append(interviewLevel.ToString());


                StringBuilder interviewBuilder = new StringBuilder(@" ( SELECT DISTINCT [M].[Id] FROM
                        [MemberJobCartDetail] [MJCD] LEFT JOIN [MemberJobCart] [MJC] ON [MJCD].[MemberJobCartId]=[MJC].[Id]
                        LEFT JOIN [Member] [M] ON [MJC].[MemberId]=[M].[Id]");

                interviewBuilder.Append(" WHERE ");
                interviewBuilder.Append(tmpinterviewWhereBuilder);

                interviewBuilder.Append(" ) ");


                if (!StringHelper.IsBlank(whereClause))
                {
                    whereClause.Append(" And  ");
                }
                whereClause.Append(" [C].[Id] ");
                whereClause.Append(" IN ");
                whereClause.Append(interviewBuilder.ToString());


            }
            return whereClause;
        }


        PagedResponse<Candidate> ICandidateDataAccess.GetPagedForReport(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
          int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
          int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, bool IsTeamReport, string TeamMemberids, int TeamId)
        {
            const string SP = "dbo.Candidate_GetPagedReport";
            StringBuilder whereClause = new StringBuilder();
            whereClause = getWhereClauseForReport(request, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
             country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
             gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, IsTeamReport, TeamMemberids, TeamId);

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
                                                    request.RowPerPage,
                                                    StringHelper.Convert(whereClause),
                                                    StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                    String.Empty
                                                };

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<Candidate> response = new PagedResponse<Candidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<Candidate>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }



        PagedResponse<DynamicDictionary> ICandidateDataAccess.GetPagedForReportBySelectedColumn(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
        int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
        int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, IList<string> CheckedList, bool IsTeamReport, string TeamMembersId, int TeamId)
        {
            string Column = "";
            foreach (string s in CheckedList)
            {
                if (Column != "") Column += ",";
                Column += s;
            }
            const string SP = "dbo.Candidate_GetPagedReportForSelectedColumns";
            StringBuilder whereClause = new StringBuilder();
            whereClause = getWhereClauseForReport(request, addedFrom, addedTo, addedBy, addedBySource, updatedFrom, updatedTo, updatedBy,
             country, state, city, interviewFrom, interviewTo, interviewLevel, interviewStatus, industry, functionalCategory, workPermit, WorkSchedule,
             gender, maritalStatus, educationId, assessmentId, isBroadcastedResume, isReferredApplicants, hasJobAgent, assignedManager, IsTeamReport, TeamMembersId, TeamId);
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[C].[FirstName]";
            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
                                                    request.RowPerPage,
                                                    StringHelper.Convert(whereClause),
                                                    StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder),
                                                   Column 
                                                };

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<DynamicDictionary> response = new PagedResponse<DynamicDictionary>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    response.Response = (new CandidateBuilder()).BuildEntities(reader, CheckedList);// CreateEntityBuilder<Candidate>().BuildEntities(reader, CheckedList);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


      
        #endregion
    }
}