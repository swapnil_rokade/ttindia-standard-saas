﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberJobCartAlertBuilder : IEntityBuilder<MemberJobCartAlert>
    {
        IList<MemberJobCartAlert> IEntityBuilder<MemberJobCartAlert>.BuildEntities(IDataReader reader)
        {
            List<MemberJobCartAlert> memberJobCartAlerts = new List<MemberJobCartAlert>();

            while (reader.Read())
            {
                memberJobCartAlerts.Add(((IEntityBuilder<MemberJobCartAlert>)this).BuildEntity(reader));
            }

            return (memberJobCartAlerts.Count > 0) ? memberJobCartAlerts : null;
        }

        MemberJobCartAlert IEntityBuilder<MemberJobCartAlert>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_MEMBERJOBCARTALERTSETUPID = 1;
            const int FLD_JOBPOSTINGID = 2;
            const int FLD_ISREMOVED = 3;
            const int FLD_CREATORID = 4;
            const int FLD_CREATEDATE = 5;
            const int FLD_UPDATORID = 6;
            const int FLD_UPDATEDATE = 7;

            MemberJobCartAlert memberJobCartAlert = new MemberJobCartAlert();

            memberJobCartAlert.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberJobCartAlert.MemberJobCartAlertSetupId = reader.IsDBNull(FLD_MEMBERJOBCARTALERTSETUPID) ? 0 : reader.GetInt32(FLD_MEMBERJOBCARTALERTSETUPID);
            memberJobCartAlert.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            memberJobCartAlert.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberJobCartAlert.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberJobCartAlert.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberJobCartAlert.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberJobCartAlert.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return memberJobCartAlert;
        }
    }
}