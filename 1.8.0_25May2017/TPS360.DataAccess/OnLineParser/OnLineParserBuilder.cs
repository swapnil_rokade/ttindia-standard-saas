﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewPanelBuilder.cs
    Description         :   This page is used Call the DB Table Column for Interview Panel.
    Created By          :   Pravin khot
    Created On          :   27/Nov/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using System.Data;

namespace TPS360.DataAccess
{
    internal sealed class OnLineParserBuilder : IEntityBuilder<OnLineParser>
    {
        IList<OnLineParser> IEntityBuilder<OnLineParser>.BuildEntities(IDataReader reader)
        {
            List<OnLineParser> OnLineParser = new List<OnLineParser>();

            while (reader.Read())
            {
                OnLineParser.Add(((IEntityBuilder<OnLineParser>)this).BuildEntity(reader));
            }

            return (OnLineParser.Count > 0) ? OnLineParser : null;
        }

        OnLineParser IEntityBuilder<OnLineParser>.BuildEntity(IDataReader reader) 
        {

            const int FLD_OnLineParser_ID = 0;        
            const int FLD_TITLE = 1;
            const int FLD_PATH = 2; 
            const int FLD_STATUS = 3;                 
            const int FLD_CREATEDATE = 4;
            const int FLD_UPDATEDATE = 5;
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_XMLFilePath = 8;
            const int FLD_ParseStatus = 9;
            const int FLD_DatabaseStatus = 10;
            const int FLD_MemebrId = 11;  

            OnLineParser OnLineParser = new OnLineParser();

            OnLineParser.OnLineParser_Id = reader.IsDBNull(FLD_OnLineParser_ID) ? 0 : reader.GetInt32(FLD_OnLineParser_ID);
            OnLineParser.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString (FLD_TITLE);
            OnLineParser.Path = reader.IsDBNull(FLD_PATH) ? string.Empty : reader.GetString(FLD_PATH);
            OnLineParser.Status = reader.IsDBNull(FLD_STATUS) ? 0 : reader.GetInt32(FLD_STATUS);
            OnLineParser.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            OnLineParser.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            OnLineParser.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            OnLineParser.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            OnLineParser.XMLFilePath = reader.IsDBNull(FLD_XMLFilePath) ? string.Empty : reader.GetString(FLD_XMLFilePath);
            OnLineParser.ParseStatus = reader.IsDBNull(FLD_ParseStatus) ? 0 : reader.GetInt32(FLD_ParseStatus);
            OnLineParser.DatabaseStatus = reader.IsDBNull(FLD_DatabaseStatus) ? 0 : reader.GetInt32(FLD_DatabaseStatus);
            OnLineParser.MemebrId = reader.IsDBNull(FLD_MemebrId) ? 0 : reader.GetInt32(FLD_MemebrId); 
            return OnLineParser;
        }

        #region IEntityBuilder<OnLineParser> OnLineParser


        public OnLineParser BuildEntity(IDataReader dataReader)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
