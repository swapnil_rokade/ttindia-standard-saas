﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   OnLineParserDataAccess.cs
    Description         :   This page is used Call the Stored Proceedures for Interview Panel.
    Created By          :   Pravin
    Created On          :   20/Nov/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;
using System.Text;
using TPS360.Common.BusinessEntities;
namespace TPS360.DataAccess
{
    internal sealed class OnLineParserDataAccess : BaseDataAccess, IOnLineParserDataAccess
    {
         #region Constructors

        public OnLineParserDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<OnLineParser> CreateEntityBuilder<OnLineParser>()
        {
            return (new OnLineParserBuilder()) as IEntityBuilder<OnLineParser>;
        }
      
        #endregion

        #region  Methods

        OnLineParser IOnLineParserDataAccess.Add(OnLineParser OnLineParser) 
        {
            const string SP = "dbo.OnLineParser_Create";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Title", DbType.String, OnLineParser.Title);
                Database.AddInParameter(cmd, "@Path", DbType.String, OnLineParser.Path);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, OnLineParser.Status);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, OnLineParser.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, OnLineParser.UpdatorId);
                Database.ExecuteNonQuery(cmd);
                return OnLineParser;         
            }
        
        }
       
        OnLineParser IOnLineParserDataAccess.Update(OnLineParser OnLineParser)
        {
            const string SP = "dbo.OnLineParser_Update";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {

                Database.AddInParameter(cmd, "@OnLineParser_Id", DbType.Int32, OnLineParser.OnLineParser_Id);
                Database.AddInParameter(cmd, "@Title", DbType.String, OnLineParser.Title);
                Database.AddInParameter(cmd, "@Path", DbType.String, OnLineParser.Path);
                Database.AddInParameter(cmd, "@Status", DbType.Int32, OnLineParser.Status);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, OnLineParser.CreatorId);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, OnLineParser.UpdatorId);
                Database.AddInParameter(cmd, "@XMLFilePath", DbType.String, OnLineParser.XMLFilePath);
                Database.AddInParameter(cmd, "@ParseStatus", DbType.String, OnLineParser.ParseStatus);
                Database.AddInParameter(cmd, "@DatabaseStatus", DbType.String, OnLineParser.DatabaseStatus);
                Database.AddInParameter(cmd, "@MemebrId", DbType.String, OnLineParser.MemebrId);
                Database.ExecuteNonQuery(cmd);
                return OnLineParser;
                         
            }
        }
      
      
        IList<OnLineParser> IOnLineParserDataAccess.GetAll()
        {
            const string SP = "dbo.OnLineParser_GetAll";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<OnLineParser>().BuildEntities(reader);
                }
            }

        }

        IList<OnLineParser> IOnLineParserDataAccess.GetAll(string SortExpression)
        {
            const string SP = "dbo.OnLineParser_All";
                     
            string SortColumn = "[CreateDate]";
            string SortOrder = "desc";
            string[] part = (string.IsNullOrEmpty(SortExpression)) ? null : SortExpression.Split(' ');

            if (part != null && part.Length > 0)
            {
                SortColumn = part[0];

                if (part.Length > 1 && part[1] != null)
                {
                    SortOrder = part[1];
                }
                else
                {
                    SortOrder = "asc";
                }
            }

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                
                Database.AddInParameter(cmd, "@SortColumn", DbType.String, SortColumn);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<OnLineParser>().BuildEntities(reader);
                }
            }
        }
      
        bool IOnLineParserDataAccess.DeleteById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.OnLineParser_DeleteById";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                Database.ExecuteNonQuery(cmd);
                return true;
            }

        }     
        
        OnLineParser IOnLineParserDataAccess.GetById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.OnLineParser_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<OnLineParser>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

        }

        Int32 IOnLineParserDataAccess.ParserCount_GetByUserId(int UserId)
        {
            //if (UserId < 1)
            //{
            //    throw new ArgumentNullException("UserId");
            //}

            const string SP = "dbo.OnLineParserCount_GetByUserId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@UserId", DbType.Int32, UserId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        return reader.GetValue(0) == null ? 0 : Convert.ToInt32(reader.GetValue(0));
                    }
                    return 0;
                }
            }

        }

        OnLineParser IOnLineParserDataAccess.GetByPath(string path, int CurrentMemberId)
        {
          
            const string SP = "dbo.OnLineParser_GetByPath";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Path", DbType.String, path);
                Database.AddInParameter(cmd, "@CurrentMemberId", DbType.Int32, CurrentMemberId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<OnLineParser>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

        }      
       
        //Check this Functionality Later 0123456789
        PagedResponse<OnLineParser> IOnLineParserDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.OnLineParser_GetPaged";
            
            string whereClause = string.Empty;
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {                       
                        if (StringHelper.IsEqual(column, "CreatorId"))
                        {
                            //if (sb.ToString() != String.Empty)
                            //{
                            //    sb.Append(" AND ");
                            //}
                            sb.Append("[CreatorId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }                       
                                
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[Title]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "ASC";

            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<OnLineParser> response = new PagedResponse<OnLineParser>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<OnLineParser>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        #endregion
    }
}
