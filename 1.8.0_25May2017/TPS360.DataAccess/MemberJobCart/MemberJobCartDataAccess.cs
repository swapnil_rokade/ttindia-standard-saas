﻿/* 
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberJobCartDataAccess.cs
    Description: This is the class file used for job cart data access.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Feb-25-2009          Jagadish N            Defect id: 8841; Implemented sorting.
 *  0.2              26-Feb-2009          Sandeesh              Defect Id : 9671 - Added  new method to                                                                          implement Sorting
    0.3              25/Jan/2016          pravin khot           Introduced by CareerPortalToRequisitionAdd
 *  0.4              27/April/2016        pravin khot           Introduced by AddCandidateToMultipleRequisition
 *  0.5              24/June/2016         pravin khot           added-RejectById(int id)

 * -------------------------------------------------------------------------------------------------------------------------------------------       
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberJobCartDataAccess : BaseDataAccess, IMemberJobCartDataAccess
    {
        #region Constructors

        public MemberJobCartDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberJobCart> CreateEntityBuilder<MemberJobCart>()
        {
            return (new MemberJobCartBuilder()) as IEntityBuilder<MemberJobCart>;
        }

        #endregion

        #region  Methods

        MemberJobCart IMemberJobCartDataAccess.Add(MemberJobCart memberJobCart)
        {
            const string SP = "dbo.MemberJobCart_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ApplyDate", DbType.DateTime, NullConverter.Convert(memberJobCart.ApplyDate));
                Database.AddInParameter(cmd, "@IsPrivate", DbType.Boolean, memberJobCart.IsPrivate);
                Database.AddInParameter(cmd, "@IsInternal", DbType.Boolean, memberJobCart.IsInternal);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberJobCart.MemberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, memberJobCart.JobPostingId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberJobCart.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberJobCart.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberJobCart = CreateEntityBuilder<MemberJobCart>().BuildEntity(reader);
                    }
                    else
                    {
                        memberJobCart = null;
                    }
                }

                if (memberJobCart == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member job cart already exists. Please specify another member job cart.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member job cart.");
                            }
                    }
                }

                return memberJobCart;
            }
        }
        //**************************Code added by pravin khot on 25/Jan/2016********************************
        int IMemberJobCartDataAccess.CareerPortalToRequisitionAdd(string CanIds)
        {

            const string SP = "dbo.MemberCareerPortalToRequisition_Add";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                // AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CanIds", DbType.String, CanIds);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {

                    if (reader.Read())
                    {
                        return reader[0] == null ? 0 : Convert.ToInt32(reader[0]);
                    }
                    return 0;
                }
            }

        }
        //***********************************End***********************************************************
        MemberJobCart IMemberJobCartDataAccess.Update(MemberJobCart memberJobCart)
        {
            const string SP = "dbo.MemberJobCart_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberJobCart.Id);
                Database.AddInParameter(cmd, "@ApplyDate", DbType.DateTime, NullConverter.Convert(memberJobCart.ApplyDate));
                Database.AddInParameter(cmd, "@IsPrivate", DbType.Boolean, memberJobCart.IsPrivate);
                Database.AddInParameter(cmd, "@IsInternal", DbType.Boolean, memberJobCart.IsInternal);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberJobCart.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberJobCart.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberJobCart = CreateEntityBuilder<MemberJobCart>().BuildEntity(reader);
                    }
                    else
                    {
                        memberJobCart = null;
                    }
                }

                if (memberJobCart == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member job cart already exists. Please specify another member job cart.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member job cart.");
                            }
                    }
                }

                return memberJobCart;
            }
        }

        MemberJobCart IMemberJobCartDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberJobCart_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberJobCart>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberJobCart> IMemberJobCartDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberJobCart_GetAllMemberJobCartByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberJobCart>().BuildEntities(reader);
                }
            }
        }

        //0.1 starts here
        IList<MemberJobCart> IMemberJobCartDataAccess.GetAllByMemberId(int memberId, string sortExpression)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            string SortColumn = "[JP].[JobTitle]";
            string SortOrder = "ASC";
            string[] part = (string.IsNullOrEmpty(sortExpression)) ? null : sortExpression.Split(' ');

            if (part != null && part.Length > 0)
            {
                SortColumn = part[0];

                if (part.Length > 1 && part[1] != null)
                {
                    SortOrder = part[1];
                }
                else
                {
                    SortOrder = "ASC";
                }
            }


            const string SP = "dbo.MemberJobCart_GetAllMemberJobCartByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@SortColumn", DbType.String, SortColumn);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberJobCart>().BuildEntities(reader);
                }
            }
        }
        //0.1 ends here

        IList<MemberJobCart> IMemberJobCartDataAccess.GetAll()
        {
            const string SP = "dbo.MemberJobCart_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberJobCart>().BuildEntities(reader);
                }
            }
        }

        MemberJobCart IMemberJobCartDataAccess.GetByMemberIdAndJobPostingId(int memberId, int jobpostingId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            if (jobpostingId < 1)
            {
                throw new ArgumentNullException("jobpostingId");
            }

            const string SP = "dbo.MemberJobCart_GetByMemberIdAndJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobpostingId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberJobCart>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        PagedResponse<MemberJobCart> IMemberJobCartDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberJobCart_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "MemberId"))
                        {
                            sb.Append("[M].[MemberId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[M].[ApplyDate]";  // 8971
            }

          //  request.SortColumn = "[M].[" + request.SortColumn + "]";  //8971

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberJobCart> response = new PagedResponse<MemberJobCart>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberJobCart>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
         //Candidate Status in Employee Referral Portal.     
		PagedResponse<MemberJobCart> IMemberJobCartDataAccess.GetPagedForEmployeeReferal(PagedRequest request)
        {
            const string SP = "dbo.MemberJobCart_GetPagedForEmployeeReferal";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "MemberId"))
                        {
                            sb.Append("[MJC].[MemberId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[MJC].[ApplyDate]";  // 8971
            }

            //  request.SortColumn = "[M].[" + request.SortColumn + "]";  //8971

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberJobCart> response = new PagedResponse<MemberJobCart>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberJobCart>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
		//Candidate Status in Employee Referral Portal.     
        bool IMemberJobCartDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberJobCart_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member job cart which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member job cart.");
                        }
                }
            }
        }

        //********************Coed added by pravin khot on 24/June/2016*****************
        bool IMemberJobCartDataAccess.RejectById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberJobCart_RejectById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot reject a member job cart which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while rejecting this member job cart.");
                        }
                }
            }
        }
        //**********************************END***************************************
        IList<MemberJobCart> IMemberJobCartDataAccess.GetAllByJobPostingIdAndSelectionStep(int jobPostingId, int selectionStep)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("JobPostingId");
            }
            if (selectionStep < 1)
            {
                throw new ArgumentNullException("SelectionStep");
            }

            const string SP = "dbo.MemberJobCart_GetALLMemberJobCartByJobPostingIdAndSelectionStep";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);
                Database.AddInParameter(cmd, "@SelectionStep", DbType.Int32, selectionStep);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberJobCart>().BuildEntities(reader);
                }
            }
        }

        IList<MemberJobCart> IMemberJobCartDataAccess.GetAllByMemberIdAndSelectionStep(int memberId, int selectionStep)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            if (selectionStep < 1)
            {
                throw new ArgumentNullException("SelectionStep");
            }

            const string SP = "dbo.MemberJobCart_GetAllByMemberIdAndSelectionStep";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@SelectionStep", DbType.Int32, selectionStep);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberJobCart>().BuildEntities(reader);
                }
            }
        }

// 0.2 start

        IList<MemberJobCart> IMemberJobCartDataAccess.GetAllSubmittedCandidateInterviewScore(int memberId, int selectionStep, string sortExpression)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }
            if (selectionStep < 1)
            {
                throw new ArgumentNullException("SelectionStep");
            }

            const string SP = "dbo.MemberJobCart_GetAllSubmittedCandidateInterviewScore";
           
            string SortColumn = "JobTitle";
            string SortOrder = "ASC";
            string[] part = (string.IsNullOrEmpty(sortExpression)) ? null : sortExpression.Split(' ');

            if (part != null && part.Length > 0)
            {
                SortColumn = part[0];

                if (part.Length > 1 && part[1] != null)
                {
                    SortOrder = part[1];
                }
                else
                {
                    SortOrder = "ASC";
                }
            }
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@SelectionStep", DbType.Int32, selectionStep);
                Database.AddInParameter(cmd, "@SortColumn", DbType.String, SortColumn);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                   return new MemberJobCartBuilder().BuildJobcartEntities(reader);
                }
            }
        }

//0.2 End


        int[] IMemberJobCartDataAccess.GetHiringMatrixMemberCount(int jobPostingId)
        {
            IList <int> hiringMatrixMemberCount=new List <int>();
            //int[] hiringMatrixMemberCount ;//= new int[6];

            if (jobPostingId < 0)
            {
                throw new ArgumentException("JobPostingId");
            }

            const string SP = "dbo.MemberJobCart_GetHiringMatrixMemberCountByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {                
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);
                Database.AddInParameter(cmd, "@DateWhenCountTobeDone", DbType.AnsiString, "NULL");
                int count = 0;
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        hiringMatrixMemberCount.Add (reader.GetInt32(0));
                        //hiringMatrixMemberCount[count] = (reader.GetInt32(0));
                        count++;
                    }
                }
            }

            return  hiringMatrixMemberCount .ToArray ();
        }

        int[] IMemberJobCartDataAccess.GetHiringMatrixMemberCount(int jobPostingId, string dateWhenCountTobeDone)
        {
            int[] hiringMatrixMemberCount = new int[6];

            if (jobPostingId < 0)
            {
                throw new ArgumentException("JobPostingId");
            }

            if (string.IsNullOrEmpty(dateWhenCountTobeDone))
            {
                throw new ArgumentException("DateWhenCountTobeDone");
            }

            const string SP = "dbo.MemberJobCart_GetHiringMatrixMemberCountByJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, jobPostingId);
                Database.AddInParameter(cmd, "@DateWhenCountTobeDone", DbType.AnsiString, dateWhenCountTobeDone);
                int count = 0;
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        hiringMatrixMemberCount[count] = (reader.GetInt32(0));
                        count++;
                    }
                }
            }

            return hiringMatrixMemberCount;
        }
        void IMemberJobCartDataAccess.MoveToNextLevel(int CurrentLevel, int UpdatorId, string MemberIds, int JobPostingID, string MovingDirection)
        {
            if (JobPostingID < 0)
            {
                throw new ArgumentException("JobPostingID");
            }
            //if (CurrentLevel < 0)
            //{
            //    throw new ArgumentException("CurrentLevel");
            //}
            if (UpdatorId < 0)
            {
                throw new ArgumentException("UpdatorId");
            }
            if (string.IsNullOrEmpty(MemberIds))
            {
                throw new ArgumentException("MemberIds");
            }

            const string SP = "dbo.MemberJobCart_MoveToNextLevel";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                bool isLandTApp = false;
                if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"] != null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower() == "landt")
                    {
                        isLandTApp = true;
                    }
                }

                Database.AddInParameter(cmd, "@CurrentSelectionStepLookupId", DbType.Int32, CurrentLevel );
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, UpdatorId );
                Database.AddInParameter(cmd, "@MemberIds", DbType.AnsiString , MemberIds );
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32 , JobPostingID );
                Database.AddInParameter(cmd, "@MovingDirection", DbType.AnsiString , MovingDirection );
                Database.AddInParameter(cmd, "@isLandT", DbType.Boolean, isLandTApp);
                Database.ExecuteNonQuery(cmd);
            }
        }


        int IMemberJobCartDataAccess.AddCandidateToRequisition(int CreatorId, string MemberIds, int JobPostingId)
        {

            if (JobPostingId < 0)
            {
                throw new ArgumentException("JobPostingID");
            }
          
            if (CreatorId < 0)
            {
                throw new ArgumentException("CreatorId");
            }
            if (string.IsNullOrEmpty(MemberIds))
            {
                throw new ArgumentException("MemberIds");
            }

            const string SP = "dbo.MemberJobCart_AddCandidateToRequisition";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {

                bool isLandTApp = false ;
                if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"] != null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower() == "landt")
                    {
                        isLandTApp= true ;
                    }
                }
               
                Database.AddInParameter(cmd, "@CreatorID", DbType.Int32, CreatorId );
                Database.AddInParameter(cmd, "@MemberIds", DbType.AnsiString, MemberIds);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId  );
                Database.AddInParameter(cmd, "@isLandT", DbType.Boolean ,isLandTApp );
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return reader[0] == null ? 0 : Convert.ToInt32(reader[0]);
                    }
                    return 0;
                }
            }
        }

        //*******************Code added by pravin khot on 27/April/2016*********************
        int IMemberJobCartDataAccess.AddCandidateToMultipleRequisition(int CreatorId, string MemberIds, int JobPostingId)
        {

            if (JobPostingId < 0)
            {
                throw new ArgumentException("JobPostingID");
            }

            if (CreatorId < 0)
            {
                throw new ArgumentException("CreatorId");
            }
            if (string.IsNullOrEmpty(MemberIds))
            {
                throw new ArgumentException("MemberIds");
            }

            const string SP = "dbo.MemberJobCart_AddCandidateToMultipleRequisition";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {

                bool isLandTApp = false;
                if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"] != null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["ApplicationSource"].ToString().ToLower() == "landt")
                    {
                        isLandTApp = true;
                    }
                }

                Database.AddInParameter(cmd, "@CreatorID", DbType.Int32, CreatorId);
                Database.AddInParameter(cmd, "@MemberIds", DbType.AnsiString, MemberIds);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);
                Database.AddInParameter(cmd, "@isLandT", DbType.Boolean, isLandTApp);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return reader[0] == null ? 0 : Convert.ToInt32(reader[0]);
                    }
                    return 0;
                }
            }
        }
        //*******************************END***********************************************
        void IMemberJobCartDataAccess.UpdateByStatus(int RequisitionId, string  MemberId, int StatusId, int UpdatorId)
        {
            if (RequisitionId < 0)
            {
                throw new ArgumentException("RequisitionId");
            }
            if (MemberId =="")
            {
                throw new ArgumentException("MemberId");
            }
            if (StatusId < 0)
            {
                throw new ArgumentException("StatusId");
            }
            if (UpdatorId < 0)
            {
                throw new ArgumentException("UpdatorId");
            }
            const string SP = "dbo.MemberJobCart_UpdateByStatus";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@RequisitionId", DbType.Int32, RequisitionId );
                Database.AddInParameter(cmd, "@MemberId", DbType.AnsiString , MemberId );
                Database.AddInParameter(cmd, "@StatusId", DbType.Int32 , StatusId );
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, UpdatorId );
                Database.ExecuteNonQuery(cmd);
            }
        }

        void IMemberJobCartDataAccess.UpdateSourceByMemberIDandJobPostingID(int MemberId, int JobPostingId, int SourceId)
        {
            if (MemberId <0)
            {
                throw new ArgumentException("MemberId");
            }
            if (JobPostingId  < 0)
            {
                throw new ArgumentException("StatusId");
            }
            if (SourceId  < 0)
            {
                throw new ArgumentException("UpdatorId");
            }
            const string SP = "dbo.MemberJobCart_UpdateSourceByMemberIdAndJobPostingId";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32 , MemberId);
                Database.AddInParameter(cmd, "@JobPostingID", DbType.Int32, JobPostingId );
                Database.AddInParameter(cmd, "@Source", DbType.Int32, SourceId );
                Database.ExecuteNonQuery(cmd);
            }

        }
        //Candidate Status RejectToUnRejectCandidate    
        bool IMemberJobCartDataAccess.RejectToUnRejectCandidate(string MemberIds, int JobPostingId)
        {
            if (string.IsNullOrEmpty(MemberIds))
            {
                throw new ArgumentException("MemberIds");
            }
            if (JobPostingId < 1)
            {
                throw new ArgumentNullException("JobPostingId");
            }

            const string SP = "dbo.RejectToUnRejectCandidate";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberIds", DbType.AnsiString, MemberIds);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member job cart which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member job cart.");
                        }
                }
            }
        }
        IList<int> IMemberJobCartDataAccess.CheckCandidatesInHiringMatrix(string CandidateIds,int JobpostingId) 
        {
            IList<int> CandidateIdList = new List<int>();
            if (JobpostingId < 0) 
            {
                throw new ArgumentException("JobPostingId");
            }
            const string SP = "MemberJobCart_CheckCandidateInHiringMatrix";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                Database.AddInParameter(cmd, "@CandidateId", DbType.String,CandidateIds);
                Database.AddInParameter(cmd, "@JobpostingId", DbType.String, JobpostingId);
                using (IDataReader reader = Database.ExecuteReader(cmd)) 
                {
                    while (reader.Read()) 
                    {
                        CandidateIdList.Add(reader.GetInt32(0));
                    
                    }
                
                }
            
            }
            return CandidateIdList;
        
        }

        string IMemberJobCartDataAccess.GetRecruiternameByMemberId(int memberId) 
        {
            string recruiterName = "";
            const string SP = "dbo.MemberJobCart_GetRecruiternameByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                       recruiterName = reader.GetString(0);
                        
                    }
                }
            }
            return recruiterName;
        }

        string IMemberJobCartDataAccess.GetCandidateStatusByMemberIdAndJobPostingId(int MemberId, int JobPostingId) 
        {
            const string SP = "dbo.MemberJobCart_GetCandidateStatusByMemberIdAndJobPostingId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP)) 
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, JobPostingId);
                using (IDataReader reader = Database.ExecuteReader(cmd)) 
                {
                    while (reader.Read()) 
                    {
                        return reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    }
                
                }
            }
            return "";
        }

        int IMemberJobCartDataAccess.GetActiveRequisitionCountByMemberId(int memberId)
        {
            const string SP = "dbo.MemberJobCart_GetActiveRequisitionCountByMemberId";
            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {

                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return reader[0] == null ? 0 : Convert.ToInt32(reader[0]);
                    }
                    return 0;
                }
            }
        }
        #endregion
    }
}