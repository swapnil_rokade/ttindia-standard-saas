﻿/*Modification Log:
   ------------------------------------------------------------------------------------------------------------   Ver.No.             Date                  Author              Modification
   ------------------------------------------------------------------------------------------------------------    0.1             26-Feb-2009             Sandeesh            Defect Fix - ID: 9671; Added new methods to implement sorting.
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class MemberJobCartBuilder : IEntityBuilder<MemberJobCart>
    {
        IList<MemberJobCart> IEntityBuilder<MemberJobCart>.BuildEntities(IDataReader reader)
        {
            List<MemberJobCart> memberJobCarts = new List<MemberJobCart>();

            while (reader.Read())
            {
                memberJobCarts.Add(((IEntityBuilder<MemberJobCart>)this).BuildEntity(reader));
            }

            return (memberJobCarts.Count > 0) ? memberJobCarts : null;
        }

        MemberJobCart IEntityBuilder<MemberJobCart>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_APPLYDATE = 1;
            const int FLD_ISPRIVATE = 2;
            const int FLD_ISINTERNAL = 3;
            const int FLD_MEMBERID = 4;
            const int FLD_JOBPOSTINGID = 5;
            const int FLD_ISREMOVED = 6;
            const int FLD_CREATORID = 7;
            const int FLD_UPDATORID = 8;
            const int FLD_CREATEDATE = 9;
            const int FLD_UPDATEDATE = 10;
            const int FLD_SourceID = 11;
            const int FLD_STATUSID = 12;
            const int FLD_JOBTITLE = 13;
            const int FLD_JOBPOSTINGCODE = 14;
            const int FLD_PUBLISHERNAME = 15;
            const int FLD_JOBPOSTINGUPDATEDATE = 16;
            const int FLD_CURRENTLEVEL = 17;
            const int FLD_CURRENTLEVERNAME = 18;
            const int FLD_CLIENTJOBID = 19;
            const int FLD_COMPANYNAME = 20;
			//Candidate Status in Employee Referral Portal.
            const int FLD_MEMBERNAME = 21;

            MemberJobCart memberJobCart = new MemberJobCart();

            memberJobCart.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberJobCart.ApplyDate = reader.IsDBNull(FLD_APPLYDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_APPLYDATE);
            memberJobCart.IsPrivate = reader.IsDBNull(FLD_ISPRIVATE) ? false : reader.GetBoolean(FLD_ISPRIVATE);
            memberJobCart.IsInternal = reader.IsDBNull(FLD_ISINTERNAL) ? false : reader.GetBoolean(FLD_ISINTERNAL);
            memberJobCart.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberJobCart.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            memberJobCart.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberJobCart.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberJobCart.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberJobCart.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberJobCart.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            memberJobCart.SourceId = reader.IsDBNull(FLD_SourceID) ? 0 : reader.GetInt32(FLD_SourceID);
            if (reader.FieldCount > FLD_STATUSID)
            {
                memberJobCart.Status = reader.IsDBNull(FLD_STATUSID) ? 0 : reader.GetInt32(FLD_STATUSID);
            }
            if (reader.FieldCount > FLD_JOBTITLE)
            {
                memberJobCart.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
                memberJobCart.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
                memberJobCart.PublisherName = reader.IsDBNull(FLD_PUBLISHERNAME) ? string.Empty : reader.GetString(FLD_PUBLISHERNAME);
                memberJobCart.JobPostingUpdateDate = reader.IsDBNull(FLD_JOBPOSTINGUPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_JOBPOSTINGUPDATEDATE);
                memberJobCart.CurrentLevel = reader.IsDBNull(FLD_CURRENTLEVEL) ? 0 : reader.GetInt32(FLD_CURRENTLEVEL);
                memberJobCart.CurrentLevelName = reader.IsDBNull(FLD_CURRENTLEVERNAME) ? string.Empty : reader.GetString(FLD_CURRENTLEVERNAME);
                memberJobCart.ClientJobId = reader.IsDBNull(FLD_CLIENTJOBID) ? string.Empty : reader.GetString(FLD_CLIENTJOBID);
                memberJobCart.CompanyName = reader.IsDBNull(FLD_COMPANYNAME) ? string.Empty : reader.GetString(FLD_COMPANYNAME);
            }
			//Candidate Status in Employee Referral Portal.
            if (reader.FieldCount > FLD_MEMBERNAME)
                memberJobCart.MemberName = reader.IsDBNull(FLD_MEMBERNAME) ? string.Empty : reader.GetString(FLD_MEMBERNAME);

            return memberJobCart;
        }

        //0.1 start
        public IList<MemberJobCart> BuildJobcartEntities(IDataReader reader)

        {
            List<MemberJobCart> memberJobCarts = new List<MemberJobCart>();

            while (reader.Read())
            {
                memberJobCarts.Add(this.BuildJobcartEntity(reader));
            }

            return (memberJobCarts.Count > 0) ? memberJobCarts : null;
        }

        public MemberJobCart BuildJobcartEntity(IDataReader reader)
        {
            const int FLD_JOBPOSTINGID = 0;
            const int FLD_JOBTITLE = 1;
            const int FLD_COMPANYNAME = 2;
            const int FLD_FIRSTINTERVIEWTOTAL = 3;
            const int FLD_SECONDINTERVIEWTOTAL = 4;
            const int FLD_THIRDINTERVIEWTOTAL = 5;
            MemberJobCart memberJobCart = new MemberJobCart();
            memberJobCart.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);

            memberJobCart.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            memberJobCart.CompanyName = reader.IsDBNull(FLD_COMPANYNAME) ? string.Empty : reader.GetString(FLD_COMPANYNAME);
            memberJobCart.FirstInterviewTotal = reader.IsDBNull(FLD_FIRSTINTERVIEWTOTAL) ? string.Empty : reader.GetString(FLD_FIRSTINTERVIEWTOTAL);
            memberJobCart.SecondInterviewTotal = reader.IsDBNull(FLD_SECONDINTERVIEWTOTAL) ? string.Empty : reader.GetString(FLD_SECONDINTERVIEWTOTAL);
            memberJobCart.ThirdInterviewTotal = reader.IsDBNull(FLD_THIRDINTERVIEWTOTAL) ? string.Empty : reader.GetString(FLD_THIRDINTERVIEWTOTAL);
            return memberJobCart;
        }
        //0.1 End
    }
}