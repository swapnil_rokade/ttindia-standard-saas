﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberAlertDataAccess : BaseDataAccess, IMemberAlertDataAccess
    {
        #region Constructors

        public MemberAlertDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberAlert> CreateEntityBuilder<MemberAlert>()
        {
            return (new MemberAlertBuilder()) as IEntityBuilder<MemberAlert>;
        }

        #endregion

        #region  Methods

        MemberAlert IMemberAlertDataAccess.Add(MemberAlert memberAlert)
        {
            const string SP = "dbo.MemberAlert_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@AlertId", DbType.Int32, memberAlert.AlertId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberAlert.MemberId);               
                Database.AddInParameter(cmd, "@AlertStatus", DbType.Int32, memberAlert.AlertStatus);
                Database.AddInParameter(cmd, "@AlertViewDate", DbType.DateTime, NullConverter.Convert(memberAlert.AlertViewDate));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberAlert.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberAlert.CreatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberAlert = CreateEntityBuilder<MemberAlert>().BuildEntity(reader);
                    }
                    else
                    {
                        memberAlert = null;
                    }
                }

                if (memberAlert == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberAlert already exists. Please specify another memberAlert.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this memberAlert.");
                            }
                    }
                }

                return memberAlert;
            }
        }

        MemberAlert IMemberAlertDataAccess.Update(MemberAlert memberAlert)
        {
            const string SP = "dbo.MemberAlert_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberAlert.Id);               
                Database.AddInParameter(cmd, "@AlertStatus", DbType.Int32, memberAlert.AlertStatus);
                Database.AddInParameter(cmd, "@AlertViewDate", DbType.DateTime, NullConverter.Convert(memberAlert.AlertViewDate));
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberAlert.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberAlert.UpdatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberAlert = CreateEntityBuilder<MemberAlert>().BuildEntity(reader);
                    }
                    else
                    {
                        memberAlert = null;
                    }
                }

                if (memberAlert == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("MemberAlert already exists. Please specify another memberAlert.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this memberAlert.");
                            }
                    }
                }

                return memberAlert;
            }
        }

        MemberAlert IMemberAlertDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberAlert_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberAlert>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberAlert> IMemberAlertDataAccess.GetAll()
        {
            const string SP = "dbo.MemberAlert_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberAlert>().BuildEntities(reader);
                }
            }
        }

        IList<MemberAlert> IMemberAlertDataAccess.GetAllByMemberId(int memberId)
        {
            const string SP = "dbo.MemberAlert_GetAllByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberAlert>().BuildEntities(reader);
                }
            }
        }

        PagedResponse<MemberAlert> IMemberAlertDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberAlert_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "employeeid"))
                        {
                            sb.Append("[MA].[MemberId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }

                    sb.Append(" AND ");
                }

                //Remove the Last AND Clasue
                sb.Remove(sb.Length - 5, 5);
                whereClause = sb.ToString();
            }

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberAlert> response = new PagedResponse<MemberAlert>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberAlert>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        bool IMemberAlertDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberAlert_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a memberAlert which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this memberAlert.");
                        }
                }
            }
        }

        #endregion
    }
}