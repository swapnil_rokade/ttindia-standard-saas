﻿using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class CompanyAssignedManagerBuilder : IEntityBuilder<CompanyAssignedManager>
    {
        IList<CompanyAssignedManager> IEntityBuilder<CompanyAssignedManager>.BuildEntities(IDataReader reader)
        {
            List<CompanyAssignedManager> companyAssignedManagers = new List<CompanyAssignedManager>();

            while (reader.Read())
            {
                companyAssignedManagers.Add(((IEntityBuilder<CompanyAssignedManager>)this).BuildEntity(reader));
            }

            return (companyAssignedManagers.Count > 0) ? companyAssignedManagers : null;
        }

        CompanyAssignedManager IEntityBuilder<CompanyAssignedManager>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_COMPANYID = 1;
            const int FLD_MEMBERID = 2;
            const int FLD_ASSIGNDATE = 3;
            const int FLD_ISPRIMARYMANAGER = 4;
            const int FLD_ISREMOVED = 5;
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_CREATEDATE = 8;
            const int FLD_UPDATEDATE = 9;

            CompanyAssignedManager companyAssignedManager = new CompanyAssignedManager();

            companyAssignedManager.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            companyAssignedManager.CompanyId = reader.IsDBNull(FLD_COMPANYID) ? 0 : reader.GetInt32(FLD_COMPANYID);
            companyAssignedManager.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            companyAssignedManager.AssignDate = reader.IsDBNull(FLD_ASSIGNDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ASSIGNDATE);
            companyAssignedManager.IsPrimaryManager = reader.IsDBNull(FLD_ISPRIMARYMANAGER) ? false : reader.GetBoolean(FLD_ISPRIMARYMANAGER);
            companyAssignedManager.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            companyAssignedManager.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            companyAssignedManager.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            companyAssignedManager.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            companyAssignedManager.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            return companyAssignedManager;
        }
    }
}