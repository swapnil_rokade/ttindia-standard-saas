﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: CompanyAssignedManagerDataAccess.cs
    Description: This is .cs page used to assign managers to company.
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              Jan-22-2008           Jagadish            Defect ID: 8822; Implemented sorting.
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class CompanyAssignedManagerDataAccess : BaseDataAccess, ICompanyAssignedManagerDataAccess
    {
        #region Constructors

        public CompanyAssignedManagerDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CompanyAssignedManager> CreateEntityBuilder<CompanyAssignedManager>()
        {
            return (new CompanyAssignedManagerBuilder()) as IEntityBuilder<CompanyAssignedManager>;
        }

        #endregion

        #region  Methods

        CompanyAssignedManager ICompanyAssignedManagerDataAccess.Add(CompanyAssignedManager companyAssignedManager)
        {
            const string SP = "dbo.CompanyAssignedManager_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyAssignedManager.CompanyId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, companyAssignedManager.MemberId);
                Database.AddInParameter(cmd, "@IsPrimaryManager", DbType.Boolean, companyAssignedManager.IsPrimaryManager);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, companyAssignedManager.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, companyAssignedManager.CreatorId);                

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        companyAssignedManager = CreateEntityBuilder<CompanyAssignedManager>().BuildEntity(reader);
                    }
                    else
                    {
                        companyAssignedManager = null;
                    }
                }

                if (companyAssignedManager == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Company assigned manager already exists. Please specify another company assigned manager.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this company assigned manager.");
                            }
                    }
                }

                return companyAssignedManager;
            }
        }

        CompanyAssignedManager ICompanyAssignedManagerDataAccess.Update(CompanyAssignedManager companyAssignedManager)
        {
            const string SP = "dbo.CompanyAssignedManager_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, companyAssignedManager.Id);
                Database.AddInParameter(cmd, "@IsPrimaryManager", DbType.Boolean, companyAssignedManager.IsPrimaryManager);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, companyAssignedManager.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        companyAssignedManager = CreateEntityBuilder<CompanyAssignedManager>().BuildEntity(reader);
                    }
                    else
                    {
                        companyAssignedManager = null;
                    }
                }

                if (companyAssignedManager == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("CompanyAssignedManager already exists. Please specify another company assigned manager.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this company assigned manager.");
                            }
                    }
                }

                return companyAssignedManager;
            }
        }

        CompanyAssignedManager ICompanyAssignedManagerDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CompanyAssignedManager_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CompanyAssignedManager>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<CompanyAssignedManager> ICompanyAssignedManagerDataAccess.GetAllByCompanyId(int companyId)
        {
            const string SP = "dbo.CompanyAssignedManager_GetAllCompanyAssignedManagerByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyAssignedManager>().BuildEntities(reader);
                }
            }
        }

        //0.1
        IList<CompanyAssignedManager> ICompanyAssignedManagerDataAccess.GetAllByCompanyId(int companyId, string sortExpression)
        {
            const string SP = "dbo.CompanyAssignedManager_GetAllCompanyAssignedManagerByCompanyId";

            string SortColumn = "[CAM].[Id]";
            string SortOrder = "ASC";
            string[] part = (string.IsNullOrEmpty(sortExpression)) ? null : sortExpression.Split(' ');

            if (part != null && part.Length > 0)
            {
                SortColumn = part[0];

                if (part.Length > 1 && part[1] != null)
                {
                    SortOrder = part[1];
                }
                else
                {
                    SortOrder = "ASC";
                }
            }

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);
                Database.AddInParameter(cmd, "@SortColumn", DbType.String, SortColumn);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyAssignedManager>().BuildEntities(reader);
                }
            }
        }

        IList<CompanyAssignedManager> ICompanyAssignedManagerDataAccess.GetAll()
        {
            const string SP = "dbo.CompanyAssignedManager_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyAssignedManager>().BuildEntities(reader);
                }
            }
        }

        IList<CompanyAssignedManager> ICompanyAssignedManagerDataAccess.GetAllByMemberId(int memberId)
        {
            const string SP = "dbo.CompanyAssignedManager_GetAllByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<CompanyAssignedManager>().BuildEntities(reader);
                }
            }
        }

        bool ICompanyAssignedManagerDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.CompanyAssignedManager_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a company assigned manager which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company assigned manager.");
                        }
                }
            }
        }

        bool ICompanyAssignedManagerDataAccess.DeleteByCompanyIdAndMemberId(int companyId, int memberId)
        {
            if (companyId < 1)
            {
                throw new ArgumentNullException("companyId");
            }

            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.CompanyAssignedManager_DeleteByCompanyIdAndMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a company assigned manager which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this company assigned manager.");
                        }
                }
            }
        }
        CompanyAssignedManager ICompanyAssignedManagerDataAccess.GetCompanyAssignedManagerByCompanyIdAndMemberId(int companyId, int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            if (companyId < 1)
            {
                throw new ArgumentNullException("companyId");
            }

            const string SP = "dbo.CompanyAssignedManager_GetAssignedManagerByCompanyIdAndMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CompanyAssignedManager>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        string ICompanyAssignedManagerDataAccess.GetPrimaryManagerNameByCompanyId(int companyId)
        {
            if (companyId < 1)
            {
                throw new ArgumentNullException("companyId");
            }

            const string SP = "dbo.Member_GetPrimaryManagerNameByCompanyId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyId);

                string strPrimaryManagerName = (Database.ExecuteScalar(cmd) == null || Database.ExecuteScalar(cmd).ToString() == "") ? "" : Convert.ToString(Database.ExecuteScalar(cmd));
                return strPrimaryManagerName;
            }

        }
        #endregion
    }
}