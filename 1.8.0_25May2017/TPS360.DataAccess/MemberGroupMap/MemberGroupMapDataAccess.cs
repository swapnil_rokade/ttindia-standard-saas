﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
using System.Collections;
namespace TPS360.DataAccess
{
    internal sealed class MemberGroupMapDataAccess : BaseDataAccess, IMemberGroupMapDataAccess
    {
        #region Constructors

        public MemberGroupMapDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberGroupMap> CreateEntityBuilder<MemberGroupMap>()
        {
            return (new MemberGroupMapBuilder()) as IEntityBuilder<MemberGroupMap>;
        }

        #endregion

        #region  Methods

        MemberGroupMap IMemberGroupMapDataAccess.Add(MemberGroupMap memberGroupMap)
        {
            const string SP = "dbo.MemberGroupMap_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberGroupMap.MemberId);
                Database.AddInParameter(cmd, "@MemberGroupId", DbType.Int32, memberGroupMap.MemberGroupId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberGroupMap.IsRemoved);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberGroupMap.CreatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberGroupMap = CreateEntityBuilder<MemberGroupMap>().BuildEntity(reader);
                    }
                    else
                    {
                        memberGroupMap = null;
                    }
                }

                if (memberGroupMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member group map already exists. Please specify another member group map.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member group map.");
                            }
                    }
                }

                return memberGroupMap;
            }
        }

        MemberGroupMap IMemberGroupMapDataAccess.Update(MemberGroupMap memberGroupMap)
        {
            const string SP = "dbo.MemberGroupMap_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberGroupMap.Id);
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberGroupMap.MemberId);
                Database.AddInParameter(cmd, "@MemberGroupId", DbType.Int32, memberGroupMap.MemberGroupId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberGroupMap.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberGroupMap.UpdatorId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberGroupMap = CreateEntityBuilder<MemberGroupMap>().BuildEntity(reader);
                    }
                    else
                    {
                        memberGroupMap = null;
                    }
                }

                if (memberGroupMap == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member group map already exists. Please specify another member group map.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member group map.");
                            }
                    }
                }

                return memberGroupMap;
            }
        }

        MemberGroupMap IMemberGroupMapDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberGroupMap_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberGroupMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        MemberGroupMap IMemberGroupMapDataAccess.GetByMemberIdandMemberGroupId(int memberId, int memberGroupId)
        {
            if ((memberId < 1) && (memberGroupId < 1))
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberGroupMap_GetByMemberIdandMemberGroupId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@MemberGroupId", DbType.Int32, memberGroupId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberGroupMap>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<MemberGroupMap> IMemberGroupMapDataAccess.GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.MemberGroupMap_GetAllMemberGroupMapByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberGroupMap>().BuildEntities(reader);
                }
            }
        }

        IList<MemberGroupMap> IMemberGroupMapDataAccess.GetAll()
        {
            const string SP = "dbo.MemberGroupMap_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberGroupMap>().BuildEntities(reader);
                }
            }
        }

        bool IMemberGroupMapDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberGroupMap_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member group map which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member group map.");
                        }
                }
            }
        }

        bool IMemberGroupMapDataAccess.DeleteByIdandMemberGroupId(int id, int memberGroupId)
        {
            if ((id < 1) || (memberGroupId < 1))
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberGroupMap_DeleteByIdandMemberGroupId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);
                Database.AddInParameter(cmd, "@MemberGroupId", DbType.Int32, memberGroupId);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member group map which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member group map.");
                        }
                }
            }
        }


        PagedResponse<MemberGroupMap> IMemberGroupMapDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberGroupMap_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "MemberId"))
                        {
                            sb.Append("[MP].[MemberId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "Name";
            }

            request.SortColumn = "[MG].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberGroupMap> response = new PagedResponse<MemberGroupMap>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberGroupMap>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        #endregion
    }
}