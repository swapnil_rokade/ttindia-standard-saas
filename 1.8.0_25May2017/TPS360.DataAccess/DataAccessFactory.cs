/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: DataAccessFacory.cs
    Description: This is the class library page.
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              20/Jul/2015            Prasanth Kumar G    Introduced InterviewerFeed
    0.2              16/Oct/2015            Prasanth Kumar G    Introduced InterviewQuestionBank, QuestionBank, InterviewResponse
 *  0.3              20/Nov/2015            Pravin khot         Introduced by IInterviewPanelDataAccess  
 *  0.4              27/Nov/2015            Pravin khot         Introduced by IInterviewPanelMasterDataAccess
 *  0.5              23/Feb/2016            Pravin khot         Introduced by #region UserRoleMapEditor
    0.6              24Apr2017              Prasanth Kumar G    Introduced Errorlogdb
    ---------------------------------------------------------------------------------------------------------------------------------------------
*/
using System.Diagnostics;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    public sealed class DataAccessFactory : BaseDataAccessFactory
	{
        #region Constructer

        public DataAccessFactory(Context context):base(context)
		{
		}

		#endregion

		#region Factory Methods

        #region ActionLog

        [DebuggerStepThrough()]
        public override IActionLogDataAccess CreateActionLogDataAccess()
        {
            string type = typeof(ActionLogDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new ActionLogDataAccess(CurrentContext);
            }

            return (IActionLogDataAccess)CurrentContext[type];
        }

        #endregion

        #region Activity

        [DebuggerStepThrough()]
        public override IActivityDataAccess CreateActivityDataAccess()
        {
            string type = typeof(ActivityDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new ActivityDataAccess(CurrentContext);
            }

            return (IActivityDataAccess)CurrentContext[type];
        }

        #endregion

        #region ActivityResource

        [DebuggerStepThrough()]
        public override IActivityResourceDataAccess CreateActivityResourceDataAccess()
        {
            string type = typeof(ActivityResourceDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new ActivityResourceDataAccess(CurrentContext);
            }

            return (IActivityResourceDataAccess)CurrentContext[type];
        }

        #endregion

        #region Alert

        [DebuggerStepThrough()]
        public override IAlertDataAccess CreateAlertDataAccess()
        {
            string type = typeof(AlertDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new AlertDataAccess(CurrentContext);
            }

            return (IAlertDataAccess)CurrentContext[type];
        }

        #endregion

        #region ApplicationWorkflow

        [DebuggerStepThrough()]
        public override IApplicationWorkflowDataAccess CreateApplicationWorkflowDataAccess()
        {
            string type = typeof(ApplicationWorkflowDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new ApplicationWorkflowDataAccess(CurrentContext);
            }

            return (IApplicationWorkflowDataAccess)CurrentContext[type];
        }

        #endregion

        #region ApplicationWorkflowMap

        [DebuggerStepThrough()]
        public override IApplicationWorkflowMapDataAccess CreateApplicationWorkflowMapDataAccess()
        {
            string type = typeof(ApplicationWorkflowMapDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new ApplicationWorkflowMapDataAccess(CurrentContext);
            }

            return (IApplicationWorkflowMapDataAccess)CurrentContext[type];
        }

        #endregion

        #region ASPNetMembership

        [DebuggerStepThrough()]
        public override IASPNetMembershipDataAccess CreateASPNetMembershipDataAccess()
        {
            string type = typeof(ASPNetMembershipDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new ASPNetMembershipDataAccess(CurrentContext);
            }

            return (IASPNetMembershipDataAccess)CurrentContext[type];
        }

        #endregion

        #region AutoMailSending
        [DebuggerStepThrough()]
        public override IAutoMailSendingDataAccess  CreateAutoMailSendingDataAccess()
        {
            string type = typeof(AutoMailSendingDataAccess ).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new AutoMailSendingDataAccess (CurrentContext);
            }

            return (IAutoMailSendingDataAccess )CurrentContext[type];
        }
        #endregion

        #region CandidateDashboard

        [DebuggerStepThrough()]
        public override ICandidateDashboardDataAccess CreateCandidateDashboardDataAccess()
        {
            string type = typeof(CandidateDashboardDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CandidateDashboardDataAccess(CurrentContext);
            }

            return (ICandidateDashboardDataAccess)CurrentContext[type];
        }

        #endregion

        #region CandidateRequisitionStatus

        [DebuggerStepThrough()]
        public override ICandidateRequisitionStatusDataAccess CreateCandidateRequisitionStatusDataAccess()
        {
            string type = typeof(CandidateRequisitionStatusDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CandidateRequisitionStatusDataAccess(CurrentContext);
            }

            return (ICandidateRequisitionStatusDataAccess)CurrentContext[type];
        }

        #endregion

        #region Category

        [DebuggerStepThrough()]
        public override ICategoryDataAccess CreateCategoryDataAccess()
        {
            string type = typeof(CategoryDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CategoryDataAccess(CurrentContext);
            }

            return (ICategoryDataAccess)CurrentContext[type];
        }

        #endregion

        #region CandidateReportDashboard

        [DebuggerStepThrough()]
        public override ICandidateReportDashboardDataAccess CreateCandidateReportDashboardDataAccess()
        {
            string type = typeof(CandidateReportDashboardDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CandidateReportDashboardDataAccess(CurrentContext);
            }

            return (ICandidateReportDashboardDataAccess)CurrentContext[type];
        }

        #endregion

        #region Candidate

        [DebuggerStepThrough()]
        public override ICandidateDataAccess CreateCandidateDataAccess()
        {
            string type = typeof(ICandidateDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CandidateDataAccess(CurrentContext);
            }

            return (ICandidateDataAccess)CurrentContext[type];
        }

        #endregion

        #region CandidateSourcingInfo

        [DebuggerStepThrough()]
        public override ICandidateSourcingInfoDataAccess CreateCandidateSourcingInfoDataAccess()
        {
            string type = typeof(ICandidateSourcingInfoDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CandidateSourcingInfoDataAccess(CurrentContext);
            }

            return (ICandidateSourcingInfoDataAccess)CurrentContext[type];
        }

        #endregion

        #region CommonNote

        [DebuggerStepThrough()]
        public override ICommonNoteDataAccess CreateCommonNoteDataAccess()
        {
            string type = typeof(CommonNoteDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CommonNoteDataAccess(CurrentContext);
            }

            return (ICommonNoteDataAccess)CurrentContext[type];
        }

        #endregion

        #region Company

        [DebuggerStepThrough()]
        public override ICompanyDataAccess CreateCompanyDataAccess()
        {
            string type = typeof(CompanyDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CompanyDataAccess(CurrentContext);
            }

            return (ICompanyDataAccess)CurrentContext[type];
        }

        #endregion

        #region CompanyAssignedManager

        [DebuggerStepThrough()]
        public override ICompanyAssignedManagerDataAccess CreateCompanyAssignedManagerDataAccess()
        {
            string type = typeof(CompanyAssignedManagerDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CompanyAssignedManagerDataAccess(CurrentContext);
            }

            return (ICompanyAssignedManagerDataAccess)CurrentContext[type];
        }

        #endregion

        #region CompanyContact

        [DebuggerStepThrough()]
        public override ICompanyContactDataAccess CreateCompanyContactDataAccess()
        {
            string type = typeof(CompanyContactDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CompanyContactDataAccess(CurrentContext);
            }

            return (ICompanyContactDataAccess)CurrentContext[type];
        }

        #endregion

        #region CompanyDocument

        [DebuggerStepThrough()]
        public override ICompanyDocumentDataAccess CreateCompanyDocumentDataAccess()
        {
            string type = typeof(CompanyDocumentDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CompanyDocumentDataAccess(CurrentContext);
            }

            return (ICompanyDocumentDataAccess)CurrentContext[type];
        }

        #endregion

        #region CompanyNote

        [DebuggerStepThrough()]
        public override ICompanyNoteDataAccess CreateCompanyNoteDataAccess()
        {
            string type = typeof(CompanyNoteDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CompanyNoteDataAccess(CurrentContext);
            }

            return (ICompanyNoteDataAccess)CurrentContext[type];
        }

        #endregion

        #region CompanySkill

        [DebuggerStepThrough()]
        public override ICompanySkillDataAccess CreateCompanySkillDataAccess()
        {
            string type = typeof(CompanySkillDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CompanySkillDataAccess(CurrentContext);
            }

            return (ICompanySkillDataAccess)CurrentContext[type];
        }

        #endregion

        #region CompanyStatusChangeRequest

        [DebuggerStepThrough()]
        public override ICompanyStatusChangeRequestDataAccess CreateCompanyStatusChangeRequestDataAccess()
        {
            string type = typeof(CompanyStatusChangeRequestDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CompanyStatusChangeRequestDataAccess(CurrentContext);
            }

            return (ICompanyStatusChangeRequestDataAccess)CurrentContext[type];
        }

        #endregion

        #region Country

        [DebuggerStepThrough()]
        public override ICountryDataAccess CreateCountryDataAccess()
        {
            string type = typeof(CountryDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CountryDataAccess(CurrentContext);
            }

            return (ICountryDataAccess)CurrentContext[type];
        }

        #endregion

        #region CustomRole

        [DebuggerStepThrough()]
        public override ICustomRoleDataAccess CreateCustomRoleDataAccess()
        {
            string type = typeof(CustomRoleDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CustomRoleDataAccess(CurrentContext);
            }

            return (ICustomRoleDataAccess)CurrentContext[type];
        }

        #endregion
        
        #region CustomRolePrivilege

        [DebuggerStepThrough()]
        public override ICustomRolePrivilegeDataAccess CreateCustomRolePrivilegeDataAccess()
        {
            string type = typeof(CustomRolePrivilegeDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CustomRolePrivilegeDataAccess(CurrentContext);
            }

            return (ICustomRolePrivilegeDataAccess)CurrentContext[type];
        }

        #endregion

        #region CustomSiteMap

        [DebuggerStepThrough()]
        public override ICustomSiteMapDataAccess CreateCustomSiteMapDataAccess()
        {
            string type = typeof(CustomSiteMapDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CustomSiteMapDataAccess(CurrentContext);
            }

            return (ICustomSiteMapDataAccess)CurrentContext[type];
        }

        #endregion

        #region Department

        [DebuggerStepThrough()]
        public override IDepartmentDataAccess CreateDepartmentDataAccess()
        {
            string type = typeof(DepartmentDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new DepartmentDataAccess(CurrentContext);
            }

            return (IDepartmentDataAccess)CurrentContext[type];
        }

        #endregion

        #region DuplicateMember

        [DebuggerStepThrough()]
        public override IDuplicateMemberDataAccess CreateDuplicateMemberDataAccess()
        {
            string type = typeof(IDuplicateMemberDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new DuplicateMemberDataAccess (CurrentContext);
            }

            return (IDuplicateMemberDataAccess)CurrentContext[type];
        }

        #endregion

        #region EmployeeReferral

        [DebuggerStepThrough()]
        public override IEmployeeReferralDataAccess CreateEmployeeReferralDataAccess()
        {
            string type = typeof(EmployeeReferralDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new EmployeeReferralDataAccess(CurrentContext);
            }

            return (IEmployeeReferralDataAccess)CurrentContext[type];
        }

        #endregion

        #region EmployeeTeamBuilder

        [DebuggerStepThrough()]
        public override IEmployeeTeamBuilderDataAccess CreateEmployeeTeamBuilderDataAccess()
        {
            string type = typeof(EmployeeTeamBuilderDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new EmployeeTeamBuilderDataAccess(CurrentContext);
            }

            return (IEmployeeTeamBuilderDataAccess)CurrentContext[type];
        }

        #endregion


        #region EventLog

        [DebuggerStepThrough()]
        public override IEventLogDataAccess CreateEventLogDataAccess()
        {
            string type = typeof(EventLogDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new EventLogDataAccess(CurrentContext);
            }

            return (IEventLogDataAccess)CurrentContext[type];
        }

        #endregion

        #region GenericLookup

        [DebuggerStepThrough()]
        public override IGenericLookupDataAccess CreateGenericLookupDataAccess()
        {
            string type = typeof(GenericLookupDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new GenericLookupDataAccess(CurrentContext);
            }

            return (IGenericLookupDataAccess)CurrentContext[type];
        }

        #endregion

        #region HiringMatrix

        [DebuggerStepThrough()]
        public override IHiringMatrixDataAccess CreateHiringMatrixDataAccess()
        {
            string type = typeof(IHiringMatrixDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new HiringMatrixDataAccess(CurrentContext);
            }

            return (IHiringMatrixDataAccess)CurrentContext[type];
        }

        #endregion

        #region HiringMatrixLevels

        [DebuggerStepThrough()]
        public override IHiringMatrixLevelsDataAccess CreateHiringMatrixLevelsDataAccess()
        {
            string type = typeof(IHiringMatrixLevelsDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new HiringMatrixLevelsDataAccess(CurrentContext);
            }

            return (IHiringMatrixLevelsDataAccess)CurrentContext[type];
        }

        #endregion

        #region Interview

        [DebuggerStepThrough()]
        public override IInterviewDataAccess CreateInterviewDataAccess()
        {
            string type = typeof(InterviewDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new InterviewDataAccess(CurrentContext);
            }

            return (IInterviewDataAccess)CurrentContext[type];
        }

        #endregion

        #region InterviewInterviewerMap

        [DebuggerStepThrough()]
        public override IInterviewInterviewerMapDataAccess CreateInterviewInterviewerMapDataAccess()
        {
            string type = typeof(InterviewInterviewerMapDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new InterviewInterviewerMapDataAccess(CurrentContext);
            }

            return (IInterviewInterviewerMapDataAccess)CurrentContext[type];
        }

        #endregion


        #region IPAccessRules

        [DebuggerStepThrough()]
        public override IIPAccessRulesDataAccess CreateIPAccessRulesDataAccess()
        {
            string type = typeof(IPAccessRulesDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new IPAccessRulesDataAccess(CurrentContext);
            }

            return (IIPAccessRulesDataAccess)CurrentContext[type];
        }

        #endregion

        #region InterviewFeed

        [DebuggerStepThrough()]
        public override IInterviewFeedbackDataAccess CreateInterviewFeedbackDataAccess()
        {
            string type = typeof(InterviewFeedbackDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new InterviewFeedbackDataAccess(CurrentContext);
            }

            return (InterviewFeedbackDataAccess)CurrentContext[type];
        }

        #endregion

        //Code introduced by Prasanth on 20/Jul/2015 Start
        #region InterviewerFeedback

        [DebuggerStepThrough()]
        public override IInterviewerFeedbackDataAccess CreateInterviewerFeedbackDataAccess()
        {
            string type = typeof(InterviewerFeedbackDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new InterviewerFeedbackDataAccess(CurrentContext);
            }

            return (InterviewerFeedbackDataAccess)CurrentContext[type];
        }

        #endregion
        //******************END**************************


        //Code introduced by Prasanth on 16/Oct/2015 Start
        #region InterviewQuestionBank

        [DebuggerStepThrough()]
        public override IInterviewQuestionBankDataAccess CreateInterviewQuestionBankDataAccess()
        {
            string type = typeof(InterviewerFeedbackDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new InterviewQuestionBankDataAccess(CurrentContext);
            }

            return (InterviewQuestionBankDataAccess)CurrentContext[type];
        }

        #endregion

        #region QuestionBank

        [DebuggerStepThrough()]
        public override IQuestionBankDataAccess CreateQuestionBankDataAccess()
        {
            string type = typeof(QuestionBankDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new QuestionBankDataAccess(CurrentContext);
            }

            return (IQuestionBankDataAccess)CurrentContext[type];
        }

        #endregion
        #region InterviewResponse

        [DebuggerStepThrough()]
        public override IInterviewResponseDataAccess CreateInterviewResponseDataAccess()
        {
            string type = typeof(InterviewResponseDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new InterviewResponseDataAccess(CurrentContext);
            }

            return (IInterviewResponseDataAccess)CurrentContext[type];
        }

        #endregion
        //********************END*************************

        //code introduced by PRAVIN KHOT 20/NOV/2015 *******************
        #region InterviewPanel

        [DebuggerStepThrough()]
        public override IInterviewPanelDataAccess CreateInterviewPanelDataAccess()
        {
            string type = typeof(InterviewPanelDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new InterviewPanelDataAccess(CurrentContext);
            }

            return (InterviewPanelDataAccess)CurrentContext[type];
        }

        #endregion
        //******************END******************************

        //code introduced by PRAVIN KHOT 27/NOV/2015 *******************
        #region InterviewPanelMaster

        [DebuggerStepThrough()]
        public override IInterviewPanelMasterDataAccess CreateInterviewPanelMasterDataAccess()
        {
            string type = typeof(InterviewPanelMasterDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new InterviewPanelMasterDataAccess(CurrentContext);
            }

            return (InterviewPanelMasterDataAccess)CurrentContext[type];
        }

        #endregion
        //******************END******************************

        #region JobPosting

        [DebuggerStepThrough()]
        public override IJobPostingDataAccess CreateJobPostingDataAccess()
        {
            string type = typeof(JobPostingDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new JobPostingDataAccess(CurrentContext);
            }

            return (IJobPostingDataAccess)CurrentContext[type];
        }

        #endregion

        #region JobPostingSearchAgent

        [DebuggerStepThrough()]
        public override IJobPostingSearchAgentDataAccess CreateJobPostingSearchAgentDataAccess()
        {
            string type = typeof(JobPostingSearchAgentDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new JobPostingSearchAgentDataAccess(CurrentContext);
            }

            return (IJobPostingSearchAgentDataAccess)CurrentContext[type];
        }
        #endregion

        #region JobPostingAssessment

        [DebuggerStepThrough()]
        public override IJobPostingAssessmentDataAccess CreateJobPostingAssessmentDataAccess()
        {
            string type = typeof(JobPostingAssessmentDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new JobPostingAssessmentDataAccess(CurrentContext);
            }

            return (IJobPostingAssessmentDataAccess)CurrentContext[type];
        }

        #endregion

        #region JobPostingDocument

        [DebuggerStepThrough()]
        public override IJobPostingDocumentDataAccess CreateJobPostingDocumentDataAccess()
        {
            string type = typeof(JobPostingDocumentDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new JobPostingDocumentDataAccess(CurrentContext);
            }

            return (IJobPostingDocumentDataAccess)CurrentContext[type];
        }

        #endregion

        #region JobPostingGeneralQuestion

        [DebuggerStepThrough()]
        public override IJobPostingGeneralQuestionDataAccess CreateJobPostingGeneralQuestionDataAccess()
        {
            string type = typeof(JobPostingGeneralQuestionDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new JobPostingGeneralQuestionDataAccess(CurrentContext);
            }

            return (IJobPostingGeneralQuestionDataAccess)CurrentContext[type];
        }

        #endregion

        #region JobPostingHiringMatrix

        [DebuggerStepThrough()]
        public override IJobPostingHiringMatrixDataAccess CreateJobPostingHiringMatrixDataAccess()
        {
            string type = typeof(JobPostingHiringMatrixDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new JobPostingHiringMatrixDataAccess(CurrentContext);
            }

            return (IJobPostingHiringMatrixDataAccess)CurrentContext[type];
        }

        #endregion

        #region JobPostingHiringTeam

        [DebuggerStepThrough()]
        public override IJobPostingHiringTeamDataAccess CreateJobPostingHiringTeamDataAccess()
        {
            string type = typeof(JobPostingHiringTeamDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new JobPostingHiringTeamDataAccess(CurrentContext);
            }

            return (IJobPostingHiringTeamDataAccess)CurrentContext[type];
        }

        #endregion

        #region JobPostingNote

        [DebuggerStepThrough()]
        public override IJobPostingNoteDataAccess CreateJobPostingNoteDataAccess()
        {
            string type = typeof(JobPostingNoteDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new JobPostingNoteDataAccess(CurrentContext);
            }

            return (IJobPostingNoteDataAccess)CurrentContext[type];
        }

        #endregion

        #region JobPostingSkillSet

        [DebuggerStepThrough()]
        public override IJobPostingSkillSetDataAccess CreateJobPostingSkillSetDataAccess()
        {
            string type = typeof(JobPostingSkillSetDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new JobPostingSkillSetDataAccess(CurrentContext);
            }

            return (IJobPostingSkillSetDataAccess)CurrentContext[type];
        }

        #endregion

        #region MailQueue
        [DebuggerStepThrough()]
        public override IMailQueueDataAccess CreateMailQueueDataAccess()
        {
            string type = typeof(IMailQueueDataAccess).ToString();
            if (!CurrentContext.Contains(type)) 
            {
                CurrentContext[type] = new MailQueueDataAccess(CurrentContext);
            }
            return (IMailQueueDataAccess)CurrentContext[type];
        }
        #endregion
        #region Member

        [DebuggerStepThrough()]
        public override IMemberDataAccess CreateMemberDataAccess()
        {
            string type = typeof(IMemberDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberDataAccess(CurrentContext);
            }

            return (IMemberDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberActivity

        [DebuggerStepThrough()]
        public override IMemberActivityDataAccess CreateMemberActivityDataAccess()
        {
            string type = typeof(MemberActivityDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberActivityDataAccess(CurrentContext);
            }

            return (IMemberActivityDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberActivityType

        [DebuggerStepThrough()]
        public override IMemberActivityTypeDataAccess CreateMemberActivityTypeDataAccess()
        {
            string type = typeof(MemberActivityTypeDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberActivityTypeDataAccess(CurrentContext);
            }

            return (IMemberActivityTypeDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberAlert

        [DebuggerStepThrough()]
        public override IMemberAlertDataAccess CreateMemberAlertDataAccess()
        {
            string type = typeof(MemberAlertDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberAlertDataAccess(CurrentContext);
            }

            return (IMemberAlertDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberAttendence

        [DebuggerStepThrough()]
        public override IMemberAttendenceDataAccess CreateMemberAttendenceDataAccess()
        {
            string type = typeof(MemberAttendenceDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberAttendenceDataAccess(CurrentContext);
            }

            return (IMemberAttendenceDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberAttribute

        [DebuggerStepThrough()]
        public override IMemberAttributeDataAccess CreateMemberAttributeDataAccess()
        {
            string type = typeof(MemberAttributeDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberAttributeDataAccess(CurrentContext);
            }

            return (IMemberAttributeDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberAttendenceYearlyReport

        [DebuggerStepThrough()]
        public override IMemberAttendenceYearlyReport CreateMemberAttendenceYearlyReport()
        {
            string type = typeof(MemberAttendenceReportDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberAttendenceReportDataAccess(CurrentContext);
            }

            return (IMemberAttendenceYearlyReport)CurrentContext[type];
        }

        #endregion

        #region MemberCertificationMap

        [DebuggerStepThrough()]
        public override IMemberCertificationMapDataAccess CreateMemberCertificationMapDataAccess()
        {
            string type = typeof(MemberCertificationMapDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberCertificationMapDataAccess(CurrentContext);
            }

            return (IMemberCertificationMapDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberCapabilityRating

        [DebuggerStepThrough()]
        public override IMemberCapabilityRatingDataAccess CreateMemberCapabilityRatingDataAccess()
        {
            string type = typeof(MemberCapabilityRatingDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberCapabilityRatingDataAccess(CurrentContext);
            }

            return (IMemberCapabilityRatingDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberChangeArchive

        [DebuggerStepThrough()]
        public override IMemberChangeArchiveDataAccess CreateMemberChangeArchiveDataAccess()
        {
            string type = typeof(MemberChangeArchiveDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberChangeArchiveDataAccess(CurrentContext);
            }

            return (IMemberChangeArchiveDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberCustomRoleMap

        [DebuggerStepThrough()]
        public override IMemberCustomRoleMapDataAccess CreateMemberCustomRoleMapDataAccess()
        {
            string type = typeof(MemberCustomRoleMapDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberCustomRoleMapDataAccess(CurrentContext);
            }

            return (IMemberCustomRoleMapDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberDailyReport

        [DebuggerStepThrough()]
        public override IMemberDailyReportDataAccess CreateMemberDailyReportDataAccess()
        {
            string type = typeof(MemberDailyReportDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberDailyReportDataAccess(CurrentContext);
            }

            return (IMemberDailyReportDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberDetail

        [DebuggerStepThrough()]
        public override IMemberDetailDataAccess CreateMemberDetailDataAccess()
        {
            string type = typeof(MemberDetailDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberDetailDataAccess(CurrentContext);
            }

            return (IMemberDetailDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberDocument

        [DebuggerStepThrough()]
        public override IMemberDocumentDataAccess CreateMemberDocumentDataAccess()
        {
            string type = typeof(MemberDocumentDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberDocumentDataAccess(CurrentContext);
            }

            return (IMemberDocumentDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberEducation

        [DebuggerStepThrough()]
        public override IMemberEducationDataAccess CreateMemberEducationDataAccess()
        {
            string type = typeof(MemberEducationDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberEducationDataAccess(CurrentContext);
            }

            return (IMemberEducationDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberEmail

        [DebuggerStepThrough()]
        public override IMemberEmailDataAccess CreateMemberEmailDataAccess()
        {
            string type = typeof(MemberEmailDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberEmailDataAccess(CurrentContext);
            }

            return (IMemberEmailDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberEmailAttachment

        [DebuggerStepThrough()]
        public override IMemberEmailAttachmentDataAccess CreateMemberEmailAttachmentDataAccess()
        {
            string type = typeof(MemberEmailAttachmentDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberEmailAttachmentDataAccess(CurrentContext);
            }

            return (IMemberEmailAttachmentDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberEmailDetail

        [DebuggerStepThrough()]
        public override IMemberEmailDetailDataAccess CreateMemberEmailDetailDataAccess()
        {
            string type = typeof(MemberEmailDetailDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberEmailDetailDataAccess(CurrentContext);
            }

            return (IMemberEmailDetailDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberExperience

        [DebuggerStepThrough()]
        public override IMemberExperienceDataAccess CreateMemberExperienceDataAccess()
        {
            string type = typeof(MemberExperienceDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberExperienceDataAccess(CurrentContext);
            }

            return (IMemberExperienceDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberExperienceDetail

        [DebuggerStepThrough()]
        public override IMemberExperienceDetailDataAccess CreateMemberExperienceDetailDataAccess()
        {
            string type = typeof(MemberExperienceDetailDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberExperienceDetailDataAccess(CurrentContext);
            }

            return (IMemberExperienceDetailDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberExtendedInformation

        [DebuggerStepThrough()]
        public override IMemberExtendedInformationDataAccess CreateMemberExtendedInformationDataAccess()
        {
            string type = typeof(MemberExtendedInformationDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberExtendedInformationDataAccess(CurrentContext);
            }

            return (IMemberExtendedInformationDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberGroup

        [DebuggerStepThrough()]
        public override IMemberGroupDataAccess CreateMemberGroupDataAccess()
        {
            string type = typeof(MemberGroupDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberGroupDataAccess(CurrentContext);
            }

            return (IMemberGroupDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberGroupManager

        [DebuggerStepThrough()]
        public override IMemberGroupManagerDataAccess CreateMemberGroupManagerDataAccess()
        {
            string type = typeof(MemberGroupManagerDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberGroupManagerDataAccess(CurrentContext);
            }

            return (IMemberGroupManagerDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberGroupMap

        [DebuggerStepThrough()]
        public override IMemberGroupMapDataAccess CreateMemberGroupMapDataAccess()
        {
            string type = typeof(MemberGroupMapDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberGroupMapDataAccess(CurrentContext);
            }

            return (IMemberGroupMapDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberHiringDetails

        [DebuggerStepThrough()]
        public override IMemberHiringDetailsDataAccess CreateMemberHiringDetailsDataAccess()
        {
            string type = typeof(MemberHiringDetailsDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberHiringDetailsDataAccess(CurrentContext);
            }

            return (IMemberHiringDetailsDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberHiringProcess

        [DebuggerStepThrough()]
        public override IMemberHiringProcessDataAccess CreateMemberHiringProcessDataAccess()
        {
            string type = typeof(MemberHiringProcessDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberHiringProcessDataAccess(CurrentContext);
            }

            return (IMemberHiringProcessDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberJobApplied

        [DebuggerStepThrough()]
        public override IMemberJobAppliedDataAccess CreateMemberJobAppliedDataAccess()
        {
            string type = typeof(MemberJobAppliedDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberJobAppliedDataAccess(CurrentContext);
            }

            return (IMemberJobAppliedDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberJobCart

        [DebuggerStepThrough()]
        public override IMemberJobCartDataAccess CreateMemberJobCartDataAccess()
        {
            string type = typeof(MemberJobCartDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberJobCartDataAccess(CurrentContext);
            }

            return (IMemberJobCartDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberJobCartAlert

        [DebuggerStepThrough()]
        public override IMemberJobCartAlertDataAccess CreateMemberJobCartAlertDataAccess()
        {
            string type = typeof(MemberJobCartAlertDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberJobCartAlertDataAccess(CurrentContext);
            }

            return (IMemberJobCartAlertDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberJobCartDetail

        [DebuggerStepThrough()]
        public override IMemberJobCartDetailDataAccess CreateMemberJobCartDetailDataAccess()
        {
            string type = typeof(MemberJobCartDetailDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberJobCartDetailDataAccess(CurrentContext);
            }

            return (IMemberJobCartDetailDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberJobCartAlertSetup

        [DebuggerStepThrough()]
        public override IMemberJobCartAlertSetupDataAccess CreateMemberJobCartAlertSetupDataAccess()
        {
            string type = typeof(MemberJobCartAlertSetupDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberJobCartAlertSetupDataAccess(CurrentContext);
            }

            return (IMemberJobCartAlertSetupDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberJoiningDetails

        [DebuggerStepThrough()]
        public override IMemberJoiningDetailsDataAccess CreateMemberJoiningDetailsDataAccess()
        {
            string type = typeof(MemberJoiningDetailsDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberJoiningDetailsDataAccess(CurrentContext);
            }

            return (IMemberJoiningDetailsDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberManager

        [DebuggerStepThrough()]
        public override IMemberManagerDataAccess CreateMemberManagerDataAccess()
        {
            string type = typeof(MemberManagerDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberManagerDataAccess(CurrentContext);
            }

            return (IMemberManagerDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberNote

        [DebuggerStepThrough()]
        public override IMemberNoteDataAccess CreateMemberNoteDataAccess()
        {
            string type = typeof(MemberNoteDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberNoteDataAccess(CurrentContext);
            }

            return (IMemberNoteDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberObjectiveAndSummary

        [DebuggerStepThrough()]
        public override IMemberObjectiveAndSummaryDataAccess CreateMemberObjectiveAndSummaryDataAccess()
        {
            string type = typeof(MemberObjectiveAndSummaryDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberObjectiveAndSummaryDataAccess(CurrentContext);
            }

            return (IMemberObjectiveAndSummaryDataAccess)CurrentContext[type];
        }

        #endregion


        #region MemberOfferRejection

        [DebuggerStepThrough()]
        public override IMemberOfferRejectionDataAccess CreateMemberOfferRejectionDataAccess()
        {
            string type = typeof(MemberOfferRejectionDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberOfferRejectionDataAccess(CurrentContext);
            }

            return (IMemberOfferRejectionDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberPendingJoners

        [DebuggerStepThrough()]
        public override IMemberPendingJoinersDataAccess  CreateMemberPendingJoinersDataAccess()
        {
            string type = typeof(MemberPendingJoinersDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberPendingJoinersDataAccess(CurrentContext);
            }

            return (IMemberPendingJoinersDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberPrivilege

        [DebuggerStepThrough()]
        public override IMemberPrivilegeDataAccess CreateMemberPrivilegeDataAccess()
        {
            string type = typeof(MemberPrivilegeDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberPrivilegeDataAccess(CurrentContext);
            }

            return (IMemberPrivilegeDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberReference

        [DebuggerStepThrough()]
        public override IMemberReferenceDataAccess CreateMemberReferenceDataAccess()
        {
            string type = typeof(MemberReferenceDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberReferenceDataAccess(CurrentContext);
            }

            return (IMemberReferenceDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberSignature

        [DebuggerStepThrough()]
        public override IMemberSignatureDataAccess CreateMemberSignatureDataAccess()
        {
            string type = typeof(MemberSignatureDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberSignatureDataAccess(CurrentContext);
            }

            return (IMemberSignatureDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberSkillMap

        [DebuggerStepThrough()]
        public override IMemberSkillMapDataAccess CreateMemberSkillMapDataAccess()
        {
            string type = typeof(MemberSkillMapDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberSkillMapDataAccess(CurrentContext);
            }

            return (IMemberSkillMapDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberSubmission

        [DebuggerStepThrough()]
        public override IMemberSubmissionDataAccess CreateMemberSubmissionDataAccess()
        {
            string type = typeof(MemberSubmissionDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberSubmissionDataAccess(CurrentContext);
            }

            return (IMemberSubmissionDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberSkillSet

        [DebuggerStepThrough()]
        public override IMemberSkillSetDataAccess CreateMemberSkillSetDataAccess()
        {
            string type = typeof(MemberSkillSetDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberSkillSetDataAccess(CurrentContext);
            }

            return (IMemberSkillSetDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberSourceHistory

        [DebuggerStepThrough()]
        public override IMemberSourceHistoryDataAccess CreateMemberSourceHistoryDataAccess()
        {
            string type = typeof(MemberSourceHistoryDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberSourceHistoryDataAccess(CurrentContext);
            }

            return (IMemberSourceHistoryDataAccess)CurrentContext[type];
        }

        #endregion

        #region OccupationalGroup

        [DebuggerStepThrough()]
        public override IOccupationalGroupDataAccess CreateOccupationalGroupDataAccess()
        {
            string type = typeof(OccupationalGroupDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new OccupationalGroupDataAccess(CurrentContext);
            }

            return (IOccupationalGroupDataAccess)CurrentContext[type];
        }

        #endregion

        #region OccupationalSeries

        [DebuggerStepThrough()]
        public override IOccupationalSeriesDataAccess CreateOccupationalSeriesDataAccess()
        {
            string type = typeof(OccupationalSeriesDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new OccupationalSeriesDataAccess(CurrentContext);
            }

            return (IOccupationalSeriesDataAccess)CurrentContext[type];
        }

        #endregion

        #region PasswordReset

        [DebuggerStepThrough()]
        public override IPasswordResetDataAccess CreatePasswordResetDataAccess()
        {
            string type = typeof(PasswordResetDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new PasswordResetDataAccess(CurrentContext);
            }

            return (IPasswordResetDataAccess)CurrentContext[type];
        }

        #endregion

        #region Recurrence

        [DebuggerStepThrough()]
        public override IRecurrenceDataAccess CreateRecurrenceDataAccess()
        {
            string type = typeof(RecurrenceDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new RecurrenceDataAccess(CurrentContext);
            }

            return (IRecurrenceDataAccess)CurrentContext[type];
        }

        #endregion

        #region RejectCandidate

        [DebuggerStepThrough()]
        public override IRejectCandidateDataAccess CreateRejectCandidateDataAccess()
        {
            string type = typeof(RejectCandidateDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new RejectCandidateDataAccess(CurrentContext);
            }

            return (IRejectCandidateDataAccess)CurrentContext[type];
        }

        #endregion

        #region Resource

        [DebuggerStepThrough()]
        public override IResourceDataAccess CreateResourceDataAccess()
        {
            string type = typeof(ResourceDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new ResourceDataAccess(CurrentContext);
            }

            return (IResourceDataAccess)CurrentContext[type];
        }

        #endregion

        #region SavedQuery

        [DebuggerStepThrough()]
        public override ISavedQueryDataAccess CreateSavedQueryDataAccess()
        {
            string type = typeof(SavedQueryDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new SavedQueryDataAccess(CurrentContext);
            }

            return (ISavedQueryDataAccess)CurrentContext[type];
        }

        #endregion

        #region SearchAgentEmailTemplate

        [DebuggerStepThrough()]
        public override ISearchAgentEmailTemplateDataAccess CreateSearchAgentEmailTemplateDataAccess()
        {
            string type = typeof(SearchAgentEmailTemplateDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new SearchAgentEmailTemplateDataAccess(CurrentContext);
            }

            return (ISearchAgentEmailTemplateDataAccess)CurrentContext[type];
        }

        #endregion

        #region SearchAgentSchedule

        [DebuggerStepThrough()]
        public override ISearchAgentScheduleDataAccess CreateSearchAgentScheduleDataAccess()
        {
            string type = typeof(SearchAgentScheduleDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new SearchAgentScheduleDataAccess(CurrentContext);
            }

            return (ISearchAgentScheduleDataAccess)CurrentContext[type];
        }

        #endregion

        #region SiteSetting

        [DebuggerStepThrough()]
        public override ISiteSettingDataAccess CreateSiteSettingDataAccess()
        {
            string type = typeof(SiteSettingDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new SiteSettingDataAccess(CurrentContext);
            }

            return (ISiteSettingDataAccess)CurrentContext[type];
        }

        #endregion

        #region Skill

        [DebuggerStepThrough()]
        public override ISkillDataAccess CreateSkillDataAccess()
        {
            string type = typeof(SkillDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new SkillDataAccess(CurrentContext);
            }

            return (ISkillDataAccess)CurrentContext[type];
        }

        #endregion

        #region State

        [DebuggerStepThrough()]
        public override IStateDataAccess CreateStateDataAccess()
        {
            string type = typeof(StateDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new StateDataAccess(CurrentContext);
            }

            return (IStateDataAccess)CurrentContext[type];
        }

        #endregion

        #region ZipCode

        [DebuggerStepThrough()]
        public override IZipCodeCityDataAccess CreateZipCodeCityDataAccess()
        {
            string type = typeof(ZipcodeDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new ZipcodeDataAccess(CurrentContext);
            }

            return (IZipCodeCityDataAccess)CurrentContext[type];
        }

        #endregion

        #region Employee

        [DebuggerStepThrough()]
        public override IEmployeeDataAccess CreateEmployeeDataAccess()
        {
            string type = typeof(IEmployeeDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new EmployeeDataAccess(CurrentContext);
            }

            return (IEmployeeDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberInterview

        [DebuggerStepThrough()]
        public override IMemberInterviewDataAccess CreateMemberInterviewDataAccess()
        {
            string type = typeof(IMemberInterviewDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberInterviewDataAccess(CurrentContext);
            }

            return (IMemberInterviewDataAccess)CurrentContext[type];
        }

        #endregion

        #region MemberSkill

        [DebuggerStepThrough()]
        public override IMemberSkillDataAccess CreateMemberSkillDataAccess()
        {
            string type = typeof(MemberSkillDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberSkillDataAccess(CurrentContext);
            }

            return (IMemberSkillDataAccess)CurrentContext[type];
        }

        #endregion

        #region WebParserDomain

        [DebuggerStepThrough()]
        public override IWebParserDomainDataAccess CreateWebParserDomainDataAccess()
        {
            string type = typeof(WebParserDomainDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new WebParserDomainDataAccess(CurrentContext);
            }

            return (IWebParserDomainDataAccess)CurrentContext[type];
        }

        #endregion

        [DebuggerStepThrough()]
        public override IUserAccessApp CreateUserAccessApp()
        {
            string type = typeof(UserAccessApp).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new UserAccessApp(CurrentContext);
            }

            return (IUserAccessApp)CurrentContext[type];
        }


        [DebuggerStepThrough()]
        public override IReqNotesDataAccess CreateReqNotesDataAccess()
        {
            string type = typeof(ReqNotesDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new ReqNotesDataAccess(CurrentContext);
            }

            return (IReqNotesDataAccess)CurrentContext[type];
        }


        //Code introduced by Prasanth on 24/Dec/2015 Start
        #region MemberInterviewFeedbackDocument

        [DebuggerStepThrough()]
        public override IMemberInterviewFeedbackDataAccess CreateMemberInterviewFeedbackDocumentDataAccess()
        {
            string type = typeof(MemberInterviewFeedbackDocumentDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new MemberInterviewFeedbackDocumentDataAccess(CurrentContext);
            }

            return (IMemberInterviewFeedbackDataAccess)CurrentContext[type];
        }

        #endregion

        //*******Code added by pravin khot on 23/Feb/2016*****************
        #region UserRoleMapEditor

        [DebuggerStepThrough()]
        public override IUserRoleMapEditorDataAccess CreateUserRoleMapEditorDataAccess()
        {
            string type = typeof(UserRoleMapEditorDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new UserRoleMapEditorDataAccess(CurrentContext);
            }

            return (UserRoleMapEditorDataAccess)CurrentContext[type];
        }

        #endregion
        //****************************END***********************************************
        //*******Code added by pravin khot on 30/Aug/2016*****************
        #region OnLineParser

        [DebuggerStepThrough()]
        public override IOnLineParserDataAccess CreateOnLineParserDataAccess()
        {
            string type = typeof(OnLineParserDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new OnLineParserDataAccess(CurrentContext);
            }

            return (OnLineParserDataAccess)CurrentContext[type];
        }

        #endregion
        //****************************END***********************************************

        //*********Pravin khot � Candidate Hiring Status Update � 3/March/2017 � Start *****************
        [DebuggerStepThrough()]
        public override ICandidateHireStatusDataAccess CreateGetCandidateHireStatusDataAccess()
        {
            string type = typeof(CandidateHireStatusDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CandidateHireStatusDataAccess(CurrentContext);
            }

            return (ICandidateHireStatusDataAccess)CurrentContext[type];
        }

        [DebuggerStepThrough()]
        public override ICandidateHireStatusDataAccess CreateHiringMatrixLevelForHiringManagersDataAccess()
        {
            string type = typeof(CandidateHireStatusDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CandidateHireStatusDataAccess(CurrentContext);
            }

            return (ICandidateHireStatusDataAccess)CurrentContext[type];
        }

        [DebuggerStepThrough()]
        public override ICandidateHireStatusDataAccess CreateHireStatusUpdateDataAccess()
        {
            string type = typeof(CandidateHireStatusDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CandidateHireStatusDataAccess(CurrentContext);
            }

            return (ICandidateHireStatusDataAccess)CurrentContext[type];
        }

        [DebuggerStepThrough()]
        public override ICandidateHireStatusDataAccess CreateSendCandidateStatusEmailDataAccess()
        {
            string type = typeof(CandidateHireStatusDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CandidateHireStatusDataAccess(CurrentContext);
            }

            return (ICandidateHireStatusDataAccess)CurrentContext[type];
        }

        [DebuggerStepThrough()]
        public override ICandidateHireStatusDataAccess CreateAllHiringStatusasperRoleId()
        {
            string type = typeof(CandidateHireStatusDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CandidateHireStatusDataAccess(CurrentContext);
            }

            return (ICandidateHireStatusDataAccess)CurrentContext[type];
        }

        [DebuggerStepThrough()]
        public override ICandidateHireStatusDataAccess CreateCustomUserRoleDataAccess()
        {
            string type = typeof(CandidateHireStatusDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CandidateHireStatusDataAccess(CurrentContext);
            }

            return (ICandidateHireStatusDataAccess)CurrentContext[type];
        }

        [DebuggerStepThrough()]
        public override ICandidateHireStatusDataAccess CreateHiringStatusRoleSaveDataAccess()
        {
            string type = typeof(CandidateHireStatusDataAccess).ToString();

            if (!CurrentContext.Contains(type))
            {
                CurrentContext[type] = new CandidateHireStatusDataAccess(CurrentContext);
            }

            return (ICandidateHireStatusDataAccess)CurrentContext[type];
        }
        //*********Pravin khot � Candidate Hiring Status Update � 3/March/2017 � End *****************

      
    }
        #endregion



    
}