using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal abstract class BaseEntityBuilder
    {
        #region Instance Variables

        private Context _context = new Context();
        private static Dictionary<int, Member> _memberCache = new Dictionary<int, Member>();

        #endregion

        #region Properties

        #endregion

        #region Constructer & Destructer

        protected BaseEntityBuilder()
        {
           
        }        

        #endregion

        #region Protected Methods

        protected Member GetMember(int memberId)
        {
            if (!_memberCache.ContainsKey(memberId))
            {
                IMemberDataAccess da = new MemberDataAccess(_context);

                Member member = da.GetById(memberId);

                if (member == null)
                {
                    return null;
                }

                _memberCache.Add(memberId, member);
            }

            return _memberCache[memberId];
        }
        
        #endregion
    }
}