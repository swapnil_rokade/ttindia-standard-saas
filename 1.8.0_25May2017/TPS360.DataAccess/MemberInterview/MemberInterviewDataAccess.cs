﻿
/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberInterviewDataAccess.cs
    Description: This is the class library page.
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              15/Dec/2015           Pravin khot         IMemberInterviewDataAccess.SuggestedInterviewerId(int interviewId, int InterviewerId)
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/


using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberInterviewDataAccess : BaseDataAccess, IMemberInterviewDataAccess
    {
        #region Constructors

        public MemberInterviewDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberInterview> CreateEntityBuilder<MemberInterview>()
        {
            return (new MemberInterviewBuilder()) as IEntityBuilder<MemberInterview>;
        }

        #endregion

        #region  Methods

        MemberInterview IMemberInterviewDataAccess.GetById(int Id)
        {
            if (Id < 1)
            {
                throw new ArgumentNullException("Id");
            }

            const string SP = "dbo.MemberInterview_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, Id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberInterview>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        //******************************code added by pravin khot on 15/Dec/2015**********
        bool IMemberInterviewDataAccess.SuggestedInterviewerId(int interviewId, int InterviewerId)
        {
            if (interviewId < 1)
            {
                throw new ArgumentNullException("Id");
            }
            else
            {
                try
                {
                    const string SP = "dbo.MemberInterview_SuggestedInterviewerId";
                    using (DbCommand cmd = Database.GetStoredProcCommand(SP))
                    {
                        Database.AddInParameter(cmd, "@interviewId", DbType.Int32, interviewId);
                        Database.AddInParameter(cmd, "@InterviewerId", DbType.Int32, InterviewerId);
                        Database.ExecuteNonQuery(cmd);
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }

        }
        //**************************End******************************************
        IList<MemberInterview> IMemberInterviewDataAccess.GetAllByMemberId(int memberId)
        {
            const string SP = "dbo.MemberInterview_GetAllByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberInterview>().BuildEntities(reader);
                }
            }
        }

        IList<MemberInterview> IMemberInterviewDataAccess.GetAllByMemberId(int memberId, string sortExpression)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            string SortColumn = "[JP].[JobTitle]";
            string SortOrder = "ASC";
            string[] part = (string.IsNullOrEmpty(sortExpression)) ? null : sortExpression.Split(' ');

            if (part != null && part.Length > 0)
            {
                SortColumn = part[0];

                if (part.Length > 1 && part[1] != null)
                {
                    SortOrder = part[1];
                }
                else
                {
                    SortOrder = "ASC";
                }
            }

            const string SP = "dbo.MemberInterview_GetAllByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);
                Database.AddInParameter(cmd, "@SortColumn", DbType.String, SortColumn);
                Database.AddInParameter(cmd, "@SortOrder", DbType.String, SortOrder);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberInterview>().BuildEntities(reader);
                }
            }
        }

        PagedResponse<MemberInterview> IMemberInterviewDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberInterview_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "CandidateId"))
                        {
                            sb.Append("[I].[MemberId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[I].[StartDateTime]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberInterview> response = new PagedResponse<MemberInterview>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberInterview>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        PagedResponse<MemberInterview> IMemberInterviewDataAccess.GetPagedForDashboard(PagedRequest request)
        {
            const string SP = "dbo.MemberInterview_GetReport";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");
                        
                        if (StringHelper.IsEqual(column, "MemberId"))
                        {
                            if (value != string.Empty)
                            {
                                sb.Append("([IM].[InterviewerId]");
                                sb.Append(" = ");
                                sb.Append(value);
                                sb.Append(" or [MJC].[CreatorId]=");
                                sb.Append(value);
                                sb.Append(" ) ");
                            }
                            else
                            {
                                sb.Append(" [I].[StartDateTime]>=getdate() ");
                            }
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[I].[StartDateTime]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberInterview> response = new PagedResponse<MemberInterview>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new MemberInterviewBuilder()).BuildInterviewReports(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }


        public int getTime(string Time, out string Min)
        {
            Min = string.Empty;
            if (Time != string.Empty)
            {
                string[] TimeSplit = Time.Split(' ');
                string[] Minit = TimeSplit[0].Split(':');
                if (TimeSplit[1].ToUpper() == "PM")
                {
                    Min = Minit[1];
                    if (Minit[0] == "12")
                        return Int32.Parse(Minit[0]);
                    else
                        return Int32.Parse(Minit[0]) + 12;
                }
                else
                {
                    Min = Minit[1];
                    if (Minit[0] == "12")
                        return 0;
                    else
                        return Int32.Parse(Minit[0]);
                }
            }
            return 0;
        }
        PagedResponse<MemberInterview> IMemberInterviewDataAccess.GetInterviewReport(PagedRequest request)
        {
            const string SP = "dbo.MemberInterview_GetReport";
            string whereClause = string.Empty;
            string Min = string.Empty;
            int Hour = 0;
            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "InterviewType"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append(" [I].[TypeLookUpId]");
                                sb.Append(" = ");
                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                            }

                        }
                        if (StringHelper.IsEqual(column, "StartDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append(" [I].[StartDateTime]");
                                sb.Append(" >= ");
                                value = "Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ";
                                sb.Append(value);
                            }
                        }
                        if (StringHelper.IsEqual(column, "EndDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append(" [I].[StartDateTime]");
                                sb.Append(" < ");
                                value = "Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ";
                                sb.Append(value);

                            }
                        }
                        if (StringHelper.IsEqual(column, "StartStartTime"))
                        {
                            if (value != string.Empty)
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                // Hour = getTime(value, out Min);
                                sb.Append(" ((datepart(hh,[I].[StartDateTime]) * 60) + datepart(mi,[I].[StartDateTime]) >=");
                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                                sb.Append(" and [I].[AllDayEvent]=0) ");
                            }

                            //if (value != string.Empty && !string.Equals(value, "0"))
                            //{
                            //    if (sb.Length > 0)
                            //    {
                            //        sb.Append(" and ");
                            //    }
                            //    Hour = getTime(value,out Min );
                            //    sb.Append(" (datepart(hh,[I].[StartDateTime]) >=");
                            //    sb.Append("'");
                            //    sb.Append(Hour);
                            //    sb.Append("'");
                            //    if (Min !=string .Empty && Min !="00")
                            //    {
                            //        sb.Append(" and datepart(mi,[I].[StartDateTime]) >=");
                            //        sb.Append("'");
                            //        sb.Append(Min);
                            //        sb.Append("'");
                            //    }
                            //    sb.Append(" and [I].[AllDayEvent]=0) ");
                            //}
                        }
                        if (StringHelper.IsEqual(column, "StartEndTime"))
                        {
                            if (value != string.Empty)
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                // Hour = getTime(value, out Min);
                                string SET = request.Conditions["StartStartTime"].ToString();
                                if (SET.Trim() == string.Empty)
                                    SET = ">=";
                                else
                                    SET = "<=";

                                sb.Append(" ((datepart(hh,[I].[StartDateTime])*60) + datepart(mi,[I].[StartDateTime]) " + SET);
                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                                //if (Min != string.Empty && Min != "00")
                                //{
                                //    sb.Append(" and datepart(mi,[I].[StartDateTime]) <=");
                                //    sb.Append("'");
                                //    sb.Append(Min);
                                //    sb.Append("'");
                                //}
                                sb.Append(" and [I].[AllDayEvent]=0) ");


                                //if (sb.Length > 0)
                                //{
                                //    sb.Append(" and ");
                                //}
                                //Hour = getTime(value, out Min);
                                //sb.Append(" (datepart(hh,[I].[StartDateTime]) <=");
                                //sb.Append("'");
                                //sb.Append(Hour);
                                //sb.Append("'");
                                //if (Min != string.Empty && Min != "00")
                                //{
                                //    sb.Append(" and datepart(mi,[I].[StartDateTime]) <=");
                                //    sb.Append("'");
                                //    sb.Append(Min);
                                //    sb.Append("'");
                                //}
                                //sb.Append(" and [I].[AllDayEvent]=0) ");
                            }
                        }
                        if (StringHelper.IsEqual(column, "EndStartTime"))
                        {
                            if (value != string.Empty)
                            {

                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                string EST = request.Conditions["EndEndTime"].ToString();
                                if (EST.Trim() == string.Empty)
                                    EST = "<=";
                                else
                                    EST = ">=";
                                // Hour = getTime(value, out Min);
                                sb.Append(" ((datepart(hh,dateadd(ss,[I].[Duration],[I].[StartDateTime])) *60) + datepart(mi,dateadd(ss,[I].[Duration],[I].[StartDateTime])) " + EST);
                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                                //if (Min != string.Empty && Min != "00")
                                //{
                                //    sb.Append(" and datepart(mi,dateadd(ss,[I].[Duration],[I].[StartDateTime]))>=");
                                //    sb.Append("'");
                                //    sb.Append(Min);
                                //    sb.Append("'");
                                //}
                                sb.Append(" and [I].[AllDayEvent]=0) ");


                                //if (sb.Length > 0)
                                //{
                                //    sb.Append(" and ");
                                //}
                                //Hour = getTime(value, out Min);
                                //sb.Append(" (datepart(hh,dateadd(ss,[I].[Duration],[I].[StartDateTime])) >=");
                                //sb.Append("'");
                                //sb.Append(Hour);
                                //sb.Append("'");
                                //if (Min != string.Empty && Min != "00")
                                //{
                                //    sb.Append(" and datepart(mi,dateadd(ss,[I].[Duration],[I].[StartDateTime]))>=");
                                //    sb.Append("'");
                                //    sb.Append(Min);
                                //    sb.Append("'");
                                //}
                                //sb.Append(" and [I].[AllDayEvent]=0) ");
                            }
                        }
                        if (StringHelper.IsEqual(column, "EndEndTime"))
                        {
                            if (value != string.Empty)
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                //Hour = getTime(value, out Min);
                                sb.Append(" ((datepart(hh,dateadd(ss,[I].[Duration],[I].[StartDateTime])) * 60) + datepart(mi,dateadd(ss,[I].[Duration],[I].[StartDateTime])) <=");
                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                                //if (Min != string.Empty && Min != "00")
                                //{
                                //    sb.Append(" and datepart(mi,dateadd(ss,[I].[Duration],[I].[StartDateTime]))<=");
                                //    sb.Append("'");
                                //    sb.Append(Min);
                                //    sb.Append("'");
                                //}
                                sb.Append(" and [I].[AllDayEvent]=0) ");


                                //if (sb.Length > 0)
                                //{
                                //    sb.Append(" and ");
                                //}
                                //Hour = getTime(value, out Min);
                                //sb.Append(" (datepart(hh,dateadd(ss,[I].[Duration],[I].[StartDateTime])) <=");
                                //sb.Append("'");
                                //sb.Append(Hour);
                                //sb.Append("'");
                                //if (Min != string.Empty && Min != "00")
                                //{
                                //    sb.Append(" and datepart(mi,dateadd(ss,[I].[Duration],[I].[StartDateTime]))<=");
                                //    sb.Append("'");
                                //    sb.Append(Min);
                                //    sb.Append("'");
                                //}
                                //sb.Append(" and [I].[AllDayEvent]=0) ");
                            }
                        }



                        if (StringHelper.IsEqual(column, "Requisition"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append(" [I].[JobPostingId]");

                                sb.Append(" = ");

                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                            }
                        }
                        if (StringHelper.IsEqual(column, "Client"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append(" [I].[ClientId]");
                                sb.Append(" = ");
                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                            }

                        }

                        if (StringHelper.IsEqual(column, "ClientInterviewers"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append(" [I].[Id] in (select distinct Id from InterviewReportView  where InterviewerId in (select MemberId from CompanyContact where ID=");
                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                                sb.Append(")");
                                sb.Append(")");
                            }
                        }

                        if (StringHelper.IsEqual(column, "InternalInterviewers"))
                        {
                            if (!string.Equals(value, "0"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append(" [I].[Id] in (select distinct Id from InterviewReportView where InterviewerId=");
                                sb.Append("'");
                                sb.Append(value);
                                sb.Append("'");
                                sb.Append(")");
                            }
                        }
                        if (StringHelper.IsEqual(column, "Location"))
                        {
                            if (value != string.Empty)
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append(" [I].[Location] like '%");
                                sb.Append(value);
                                sb.Append("%'");
                            }
                        }
                    }
                }
                whereClause = sb.ToString();
            }
            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = " [I].[StartDateTime]";
                request.SortOrder = "DESC";
            }

            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberInterview> response = new PagedResponse<MemberInterview>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = (new MemberInterviewBuilder()).BuildInterviewReports(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        #endregion
    }
}