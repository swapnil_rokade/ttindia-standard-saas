﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberInterviewSchedule.ascx.cs
    Description: This is the user control page used for member interview functionalities
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Dec-02-2008          Anand Dixit        Defect ID: 8955; commented some lines of code (112-118)
 *  0.2               4/May/2016          pravin khot        new field added- memberInterview.QuestionBankTypeLookupId,IcsFileUIDCode,IsCancel
 *  0.3              12/May/2016          pravin khot        Added new field-FLD_INTERVIEWFEEDBACK
    ------------------------------------------------------------------------------------------------------------------------------
*/
using System;
using System.Data;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class MemberInterviewBuilder : IEntityBuilder<MemberInterview>
    {
        IList<MemberInterview> IEntityBuilder<MemberInterview>.BuildEntities(IDataReader reader)
        {
            List<MemberInterview> memberInterview = new List<MemberInterview>();

            while (reader.Read())
            {
                memberInterview.Add(((IEntityBuilder<MemberInterview>)this).BuildEntity(reader));
            }

            return (memberInterview.Count > 0) ? memberInterview : null;
        }

        public IList<MemberInterview> BuildEntitiesForDashboard(IDataReader reader)
        {
            List<MemberInterview> memberInterview = new List<MemberInterview>();

            while (reader.Read())
            {
                memberInterview.Add(BuildEntityForDashboard(reader));
            }

            return (memberInterview.Count > 0) ? memberInterview : null;
        }
        MemberInterview IEntityBuilder<MemberInterview>.BuildEntity(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_LOCATION = 1;
            const int FLD_REMARK = 2;
            const int FLD_MEMBERID = 3;
            const int FLD_TYPELOOKUPID = 4;
            const int FLD_ISREMOVED = 5;
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_CREATEDATE = 8;
            const int FLD_UPDATEDATE = 9;
            const int FLD_TITLE = 10;
            const int FLD_JOBPOSTINGID = 11;
            const int FLD_JOBTITLE = 12;
            const int FLD_JOBPOSTINGCODE = 13;
            const int FLD_COMPANYID = 14;
            const int FLD_COMPANYNAME = 15;
            const int FLD_TYPENAME = 16;
            const int FLD_INTERVIEWERNAME = 17;
            const int FLD_STARTDATETIME = 18;
            const int FLD_DURATION = 19;
            const int FLD_ACTIVITYID = 20;
            const int FLD_ALLDAYEVENT = 21;
            const int FLD_ENABLEREMINDER = 22;
            const int FLD_REMINDERINTERVAL = 23;
          //  const int FLD_INDERVIEWDOCUMENTID = 24;
            const int FLD_OTHERINTERVIEWERS = 24;
            const int QuestionBankTypeLookupId = 25;
            const int IcsFileUIDCode = 26;//ADDED BY PRAVIN KHOT ON 5/May/2016
            const int IsCancel = 27;//ADDED BY PRAVIN KHOT ON 5/May/2016
             const int FlD_TIMEZONEID=28;//ADDED BY PRAVIN KHOT ON 5/May/2016
       
            
            MemberInterview memberInterview = new MemberInterview();

            memberInterview.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberInterview.Location = reader.IsDBNull(FLD_LOCATION) ? string.Empty : reader.GetString(FLD_LOCATION);
            memberInterview.Remark = reader.IsDBNull(FLD_REMARK) ? string.Empty : reader.GetString(FLD_REMARK);
            memberInterview.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberInterview.TypeLookupId = reader.IsDBNull(FLD_TYPELOOKUPID) ? 0 : reader.GetInt32(FLD_TYPELOOKUPID);
            memberInterview.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberInterview.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberInterview.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberInterview.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberInterview.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            memberInterview.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString(FLD_TITLE);
            memberInterview.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            memberInterview.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            memberInterview.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
            memberInterview.CompanyId = reader.IsDBNull(FLD_COMPANYID) ? 0 : reader.GetInt32(FLD_COMPANYID);
            memberInterview.CompanyName = reader.IsDBNull(FLD_COMPANYNAME) ? string.Empty : reader.GetString(FLD_COMPANYNAME);
            memberInterview.InterviewTypeName = reader.IsDBNull(FLD_TYPENAME) ? string.Empty : reader.GetString(FLD_TYPENAME);
            memberInterview.InterviewerName = reader.IsDBNull(FLD_INTERVIEWERNAME) ? string.Empty : reader.GetString(FLD_INTERVIEWERNAME);
            memberInterview.StartDateTime  = reader.IsDBNull(FLD_STARTDATETIME) ? DateTime.MinValue : reader.GetDateTime(FLD_STARTDATETIME);
            memberInterview.Duration = reader.IsDBNull(FLD_DURATION) ? 0 : reader.GetInt32(FLD_DURATION);
            memberInterview.ActivityId = reader.IsDBNull(FLD_ACTIVITYID) ? 0 : reader.GetInt32(FLD_ACTIVITYID);
            memberInterview.AllDayEvent = reader.IsDBNull(FLD_ALLDAYEVENT) ? false : reader.GetBoolean(FLD_ALLDAYEVENT);
            memberInterview.EnableReminder = reader.IsDBNull(FLD_ENABLEREMINDER) ? false : reader.GetBoolean(FLD_ENABLEREMINDER);
            memberInterview.ReminderInterval = reader.IsDBNull(FLD_REMINDERINTERVAL) ? 0 : reader.GetInt32(FLD_REMINDERINTERVAL);
            //memberInterview.InterviewDocumentId = reader.IsDBNull(FLD_INDERVIEWDOCUMENTID) ? 0 : reader.GetInt32(FLD_INDERVIEWDOCUMENTID);
           // memberInterview.QuestionBankTypeLookupId = reader.IsDBNull(QuestionBankTypeLookupId) ? 0 : reader.GetInt32(QuestionBankTypeLookupId);
            if (reader.FieldCount > FLD_OTHERINTERVIEWERS)
            {
                memberInterview.OtherInterviewers = reader.IsDBNull(FLD_OTHERINTERVIEWERS) ? string.Empty : reader.GetString(FLD_OTHERINTERVIEWERS);
            }
            //**************Code added by pravin khot on 4/May/2016*********************
            if (reader.FieldCount > QuestionBankTypeLookupId)
            {
                memberInterview.QuestionBankTypeLookupId = reader.IsDBNull(QuestionBankTypeLookupId) ? 0 : reader.GetInt32(QuestionBankTypeLookupId);
            }
            if (reader.FieldCount > IcsFileUIDCode)
            {
                memberInterview.IcsFileUIDCode = reader.IsDBNull(IcsFileUIDCode) ? string.Empty : reader.GetString(IcsFileUIDCode);
            }
            if (reader.FieldCount > IsCancel)
            {
                memberInterview.IsCancel = reader.IsDBNull(IsCancel) ? 0 : reader.GetInt32(IsCancel);
            }
            if (reader.FieldCount > FlD_TIMEZONEID)
            {
                memberInterview.TimezoneId = reader.IsDBNull(FlD_TIMEZONEID) ? 0 : reader.GetInt32(FlD_TIMEZONEID);
            }
            //****************************************END****************************


            return memberInterview;
        }

        MemberInterview BuildEntityForDashboard(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_LOCATION = 1;
            const int FLD_MEMBERID = 2;
            const int FLD_STARTDATETIME = 3;
            const int FLD_DURATION = 4;
            const int FLD_ISREMOVED = 5;
            const int FLD_JOBPOSTINGID = 6;
            const int FLD_JOBTITLE = 7;
            const int FLD_JOBPOSTINGCODE = 8;
            const int FLD_FIRSTNAME = 9;
            const int FLD_LASTNAME = 10;
            const int FLD_ALLDAYEVENT = 11;
            MemberInterview memberInterview = new MemberInterview();

            memberInterview.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberInterview.Location = reader.IsDBNull(FLD_LOCATION) ? string.Empty : reader.GetString(FLD_LOCATION);
            memberInterview.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberInterview.StartDateTime = reader.IsDBNull(FLD_STARTDATETIME) ? DateTime.MinValue : reader.GetDateTime(FLD_STARTDATETIME);
            memberInterview.Duration = reader.IsDBNull(FLD_DURATION) ? 0 : reader.GetInt32(FLD_DURATION);
            memberInterview.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberInterview.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            memberInterview.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            memberInterview.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
            memberInterview.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            memberInterview.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            memberInterview.AllDayEvent = reader.IsDBNull(FLD_ALLDAYEVENT) ? false : reader.GetBoolean(FLD_ALLDAYEVENT);
            return memberInterview;
        }

        public IList<MemberInterview> BuildInterviewReports(IDataReader reader)
        {
            List<MemberInterview > interview = new List<MemberInterview >();

            while (reader.Read())
            {
                interview.Add(this.BuildInterviewReport (reader ));
            }

            return (interview.Count > 0) ? interview : null;
        }
        public MemberInterview  BuildInterviewReport(IDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_LOCATION = 1;
            const int FLD_REMARK = 2;
            const int FLD_MEMBERID = 3;
            const int FLD_TYPELOOKUPID = 4;
            const int FLD_ISREMOVED = 5;
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_CREATEDATE = 8;
            const int FLD_UPDATEDATE = 9;
            const int FLD_TITLE = 10;
            const int FLD_JOBPOSTINGID = 11;
            const int FLD_JOBTITLE = 12;
            const int FLD_JOBPOSTINGCODE = 13;
            const int FLD_COMPANYID = 14;
            const int FLD_COMPANYNAME = 15;
            const int FLD_TYPENAME = 16;
            const int FLD_STARTDATETIME = 17;
            const int FLD_DURATION = 18;
            const int FLD_FIRSTNAME = 19;
            const int FLD_LASTNAME = 20;
            const int FLD_INTERVIEWERNAME = 21;
            const int FLD_CLIENTINTERVIEWERNAME = 22;
            const int FLD_ALLDAYEVENT = 23;
            const int FLD_OTHERINTERVIEWERS = 24;
            const int FLD_INTERVIEWFEEDBACK = 25;//ADDED BY PRAVIN KHOT ON 12/May/2016


            MemberInterview memberInterview = new MemberInterview();

            memberInterview.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            memberInterview.Location = reader.IsDBNull(FLD_LOCATION) ? string.Empty : reader.GetString(FLD_LOCATION);
            memberInterview.Remark = reader.IsDBNull(FLD_REMARK) ? string.Empty : reader.GetString(FLD_REMARK);
            memberInterview.MemberId = reader.IsDBNull(FLD_MEMBERID) ? 0 : reader.GetInt32(FLD_MEMBERID);
            memberInterview.TypeLookupId = reader.IsDBNull(FLD_TYPELOOKUPID) ? 0 : reader.GetInt32(FLD_TYPELOOKUPID);
            memberInterview.IsRemoved = reader.IsDBNull(FLD_ISREMOVED) ? false : reader.GetBoolean(FLD_ISREMOVED);
            memberInterview.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            memberInterview.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            memberInterview.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            memberInterview.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);
            memberInterview.Title = reader.IsDBNull(FLD_TITLE) ? string.Empty : reader.GetString(FLD_TITLE);
            memberInterview.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            memberInterview.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
            memberInterview.JobPostingCode = reader.IsDBNull(FLD_JOBPOSTINGCODE) ? string.Empty : reader.GetString(FLD_JOBPOSTINGCODE);
            memberInterview.CompanyId = reader.IsDBNull(FLD_COMPANYID) ? 0 : reader.GetInt32(FLD_COMPANYID);
            memberInterview.CompanyName = reader.IsDBNull(FLD_COMPANYNAME) ? string.Empty : reader.GetString(FLD_COMPANYNAME);
            memberInterview.InterviewTypeName = reader.IsDBNull(FLD_TYPENAME) ? string.Empty : reader.GetString(FLD_TYPENAME);
            memberInterview.StartDateTime = reader.IsDBNull(FLD_STARTDATETIME) ? DateTime.MinValue : reader.GetDateTime(FLD_STARTDATETIME);
            memberInterview.Duration = reader.IsDBNull(FLD_DURATION) ? 0 : reader.GetInt32(FLD_DURATION);
            memberInterview.FirstName = reader.IsDBNull(FLD_FIRSTNAME) ? string.Empty : reader.GetString(FLD_FIRSTNAME);
            memberInterview.LastName = reader.IsDBNull(FLD_LASTNAME) ? string.Empty : reader.GetString(FLD_LASTNAME);
            memberInterview.InterviewerName = reader.IsDBNull(FLD_INTERVIEWERNAME) ? string.Empty : reader.GetString(FLD_INTERVIEWERNAME);
            memberInterview.ClientInterviewerName = reader.IsDBNull(FLD_CLIENTINTERVIEWERNAME) ? string.Empty : reader.GetString(FLD_CLIENTINTERVIEWERNAME);
            memberInterview.AllDayEvent = reader.IsDBNull(FLD_ALLDAYEVENT) ? false : reader.GetBoolean(FLD_ALLDAYEVENT);
            memberInterview.OtherInterviewers = reader.IsDBNull(FLD_OTHERINTERVIEWERS) ? string.Empty : reader.GetString(FLD_OTHERINTERVIEWERS);
            //*****************Added by pravin khot on 12/May/2016***********
            if (reader.FieldCount > FLD_INTERVIEWFEEDBACK)
                memberInterview.Feedback = reader.IsDBNull(FLD_INTERVIEWFEEDBACK) ? string.Empty : reader.GetString(FLD_INTERVIEWFEEDBACK);
            //********************************END******************************
            return memberInterview;
        }
    }
}