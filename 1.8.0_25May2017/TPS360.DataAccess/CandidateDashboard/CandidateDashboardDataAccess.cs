﻿using System;
using System.Data;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    internal sealed class CandidateDashboardDataAccess : BaseDataAccess, ICandidateDashboardDataAccess
    {
        #region Constructors

        public CandidateDashboardDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<CandidateDashboard> CreateEntityBuilder<CandidateDashboard>()
        {
            return (new CandidateDashboardBuilder()) as IEntityBuilder<CandidateDashboard>;
        }

        #endregion

        #region  Methods

        CandidateDashboard ICandidateDashboardDataAccess.GetByMemberId(int memberId)
        {
            const int FLD_REGISTRATIONDATE = 0;
            const int FLD_UPDATEDATE = 1;
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "dbo.CandidateDashboard_GetByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, memberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<CandidateDashboard>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        #endregion
    }
}