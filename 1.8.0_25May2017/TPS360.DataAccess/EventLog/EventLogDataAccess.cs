﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class EventLogDataAccess : BaseDataAccess ,IEventLogDataAccess 
    {
        #region Constructors

        public EventLogDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<EventLogForRequisitionAndCandidate> CreateEntityBuilder<EventLogForRequisitionAndCandidate>()
        {
            return (new EventLogBuilder ()) as IEntityBuilder<EventLogForRequisitionAndCandidate>;
        }

        #endregion

        #region  Methods

        void IEventLogDataAccess.Add(EventLogForRequisitionAndCandidate Log, string CandidateId)
        {
            const string SP = "dbo.EventLogForRequisitionAndCandidate_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@ActionType", DbType.AnsiString, Log .ActionType );
                Database.AddInParameter(cmd, "@JobPostingId", DbType.Int32, Log.JobPostingID);
                Database.AddInParameter(cmd, "@CandidateId", DbType.AnsiString, CandidateId );
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, Log.CreatorId);
                Database.ExecuteNonQuery(cmd);
            }
        }
        IList<EventLogForRequisitionAndCandidate> IEventLogDataAccess.GetAll(int JobPostingId, int CandidateId, int CreatorId)
        {

            const string SP = "dbo.EventLogForRequisitionAndCandidate_GetPaged";
            string whereClause = string.Empty;

                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;
                if (JobPostingId >0)
                {
                    if (sb.ToString() != String.Empty)
                    {
                        sb.Append(" AND ");
                    }
                    sb.Append("[EL].[JobPostingId]");
                    sb.Append(" = ");
                    sb.Append(JobPostingId );
                }
                if (CandidateId > 0)
                {
                    if (sb.ToString() != String.Empty)
                    {
                        sb.Append(" AND ");
                    }
                    sb.Append("[EL].[CandidateId]");
                    sb.Append(" = ");
                    sb.Append(CandidateId );
                }

                if (CreatorId > 0)
                {
                    if (sb.ToString() != String.Empty)
                    {
                        sb.Append(" AND ");
                    }
                    sb.Append("[EL].[CreatorId]");
                    sb.Append(" = ");
                    sb.Append(CreatorId );
                }
                whereClause = sb.ToString();


            object[] paramValues = new object[] {	0,
													0,
													StringHelper.Convert(whereClause),
													"[EL].[ActionDate]",
                                                    "desc"
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                IList <EventLogForRequisitionAndCandidate> response = new List <EventLogForRequisitionAndCandidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response = CreateEntityBuilder<EventLogForRequisitionAndCandidate>().BuildEntities(reader);
                }

                return response;
            }
        }
        PagedResponse<EventLogForRequisitionAndCandidate> IEventLogDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.EventLogForRequisitionAndCandidate_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobPostingId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[EL].[JobPostingId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        if (StringHelper.IsEqual(column, "CandidateId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[EL].[CandidateId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }

                        if (StringHelper.IsEqual(column, "CreatorId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[EL].[CreatorId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        if (StringHelper.IsEqual(column, "ClientId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[J].[ClientId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[EL].[ActionDate]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<EventLogForRequisitionAndCandidate> response = new PagedResponse<EventLogForRequisitionAndCandidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<EventLogForRequisitionAndCandidate>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        PagedResponse<EventLogForRequisitionAndCandidate> IEventLogDataAccess.GetPagedForReport(PagedRequest request)
        {
            const string SP = "dbo.EventLogForRequisitionAndCandidate_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "JobPostingId"))
                        {
                            if (value != "0")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append("[EL].[JobPostingId]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }
                        if (StringHelper.IsEqual(column, "StartDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append(" [EL].[ActionDate]");
                                sb.Append(" >= ");
                                value = "Convert(DateTime,'" + Convert.ToDateTime(value).ToString("dd/MM/yyyy") + "',103) ";
                                sb.Append(value);
                            }
                        }
                        if (StringHelper.IsEqual(column, "EndDate"))
                        {
                            if (value != DateTime.MinValue.ToString())
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" and ");
                                }
                                sb.Append(" [EL].[ActionDate]");
                                sb.Append(" < ");
                                value = "Convert(DateTime,'" + Convert.ToDateTime(value).AddDays(1).ToString("dd/MM/yyyy") + "',103) ";
                                sb.Append(value);

                            }
                        }
                        if (StringHelper.IsEqual(column, "ClientId"))
                        {
                            if (value != "0")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append("[J].[ClientId]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }

                        if (StringHelper.IsEqual(column, "CreatorId"))
                        {
                            if (value != "0")
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append("[EL].[CreatorId]");
                                sb.Append(" = ");
                                sb.Append(value);
                            }
                        }
                        if (StringHelper.IsEqual(column, "ActionType"))
                        {
                            if (value != string.Empty)
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(" AND ");
                                }
                                sb.Append("[EL].[ActionType] like '");
                                sb.Append(value);
                                sb.Append("%'");
                            }
                        }
                    }
                }

                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "[EL].[ActionDate]";
            }
            if (StringHelper.IsBlank(request.SortOrder))
            {
                request.SortOrder = "desc";
            }
            request.SortColumn = request.SortColumn;

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<EventLogForRequisitionAndCandidate> response = new PagedResponse<EventLogForRequisitionAndCandidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<EventLogForRequisitionAndCandidate>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        ////////////////////////////Sumit Sonawane 1/Mar/2017///////////////////////////////////////////////////////////////////
        PagedResponse<EventLogForRequisitionAndCandidate> IEventLogDataAccess.getPagedHiringMatrixEventLogDetails(int MemberID, PagedRequest request)
        {
            if (MemberID < 1)
            {
                throw new ArgumentNullException("MemberID");
            }

            string whereClause = string.Empty;
            whereClause = "CandidateId = " + MemberID;
            if (request.SortColumn == null) request.SortColumn = "[CHMLD].[Comment]";
            else if (request.SortColumn == string.Empty) request.SortColumn = "[CHMLD].[Comment]";
            else if (request.SortColumn == "[CHMLD].[JobPostingId]")
                request.SortColumn = request.SortColumn;
            else request.SortColumn = "[CHMLD].[" + request.SortColumn + "]";

            if (request.SortOrder == null) request.SortOrder = "ASC";
            else if (request.SortOrder == string.Empty) request.SortOrder = "ASC";



            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                whereClause += sb.ToString();
            }




            object[] paramValues = new object[] {request .PageIndex,
													request .RowPerPage   ,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request .SortColumn  ),
                                                    StringHelper.Convert(request.SortOrder),
												};
            const string SP = "dbo.MemberHiringLog_GetPagedHiringLogByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<EventLogForRequisitionAndCandidate> response = new PagedResponse<EventLogForRequisitionAndCandidate>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<EventLogForRequisitionAndCandidate>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }
        #endregion
    }
}
