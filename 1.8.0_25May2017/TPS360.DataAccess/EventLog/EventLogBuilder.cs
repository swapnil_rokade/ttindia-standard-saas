﻿using System;
using System.Data;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    internal sealed class EventLogBuilder:IEntityBuilder <EventLogForRequisitionAndCandidate>
    {
        IList<EventLogForRequisitionAndCandidate> IEntityBuilder<EventLogForRequisitionAndCandidate>.BuildEntities(IDataReader reader)
        {
            List<EventLogForRequisitionAndCandidate> Log = new List<EventLogForRequisitionAndCandidate>();

            while (reader.Read())
            {
                Log.Add(((IEntityBuilder<EventLogForRequisitionAndCandidate>)this).BuildEntity(reader));
            }

            return (Log.Count > 0) ? Log : null;
        }

        EventLogForRequisitionAndCandidate IEntityBuilder<EventLogForRequisitionAndCandidate>.BuildEntity(IDataReader reader)
        {
            if (reader.FieldCount == 4)
            {
                const int FLD_HiringLevelLogId = 0;
                const int FLD_UpdatorId = 1;
                const int FLD_UpdateDate = 2;
                const int FLD_HiringLevelComment = 3;

                EventLogForRequisitionAndCandidate Log = new EventLogForRequisitionAndCandidate();


                Log.HiringLevelLogId = reader.IsDBNull(FLD_HiringLevelLogId) ? 0 : reader.GetInt32(FLD_HiringLevelLogId);
                Log.UpdatorId = reader.IsDBNull(FLD_UpdatorId) ? 0 : reader.GetInt32(FLD_UpdatorId);
                Log.UpdateDate = reader.IsDBNull(FLD_UpdateDate) ? DateTime.MinValue : reader.GetDateTime(FLD_UpdateDate);
                Log.HiringLevelComment = reader.IsDBNull(FLD_HiringLevelComment) ? string.Empty : reader.GetString(FLD_HiringLevelComment);

                return Log;
            }
            else
            {

                const int FLD_ID = 0;
                const int FLD_ACTIONTYPE = 1;
                const int FLD_ACTIONDATE = 2;
                const int FLD_JOBPOSTINGID = 3;
                const int FLD_CANDIDATEID = 4;
                const int FLD_CREATORID = 5;
                const int FLD_USERNAME = 6;
                const int FLD_JOBTITLE = 7;
                EventLogForRequisitionAndCandidate Log = new EventLogForRequisitionAndCandidate();

                Log.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
                Log.ActionType = reader.IsDBNull(FLD_ACTIONTYPE) ? string.Empty : reader.GetString(FLD_ACTIONTYPE);
                Log.ActionDate = reader.IsDBNull(FLD_ACTIONDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_ACTIONDATE);
                Log.JobPostingID = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
                Log.CandidateId = reader.IsDBNull(FLD_CANDIDATEID) ? 0 : reader.GetInt32(FLD_CANDIDATEID);
                Log.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
                Log.UserName = reader.IsDBNull(FLD_USERNAME) ? string.Empty : reader.GetString(FLD_USERNAME);
                Log.JobTitle = reader.IsDBNull(FLD_JOBTITLE) ? string.Empty : reader.GetString(FLD_JOBTITLE);
                return Log;
            }
        }
    }
}
