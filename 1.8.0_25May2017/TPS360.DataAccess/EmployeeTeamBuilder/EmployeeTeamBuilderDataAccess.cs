﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Helper;
using TPS360.Common.Shared;


namespace TPS360.DataAccess
{
    internal sealed class EmployeeTeamBuilderDataAccess : BaseDataAccess, IEmployeeTeamBuilderDataAccess
    {
        #region Constructors

        public EmployeeTeamBuilderDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<EmployeeTeamBuilder> CreateEntityBuilder<EmployeeTeamBuilder>()
        {
            return (new EmployeeTeamBuilderBuilder()) as IEntityBuilder<EmployeeTeamBuilder>;
        }

        #endregion

        #region  Methods

        EmployeeTeamBuilder IEmployeeTeamBuilderDataAccess.Add(EmployeeTeamBuilder employeeTeamBuilders)
        {
            const string SP = "dbo.EmployeeTeamBuilder_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, StringHelper.Convert(employeeTeamBuilders.Title));
                Database.AddInParameter(cmd, "@TeamLeader", DbType.Int32,employeeTeamBuilders.TeamLeader);
                Database.AddInParameter(cmd, "@EmployeeId", DbType.AnsiString, StringHelper.Convert(employeeTeamBuilders.EmployeeId) );
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, employeeTeamBuilders.CreatorId);
                Database.ExecuteNonQuery(cmd);
                return null;
            }
        }

        EmployeeTeamBuilder IEmployeeTeamBuilderDataAccess.Update(EmployeeTeamBuilder employeeTeamBuilders) 
        {
            const string SP = "dbo.EmployeeTeamBuilder_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32 , employeeTeamBuilders.Id );
                Database.AddInParameter(cmd, "@Title", DbType.AnsiString, StringHelper.Convert(employeeTeamBuilders.Title));
                Database.AddInParameter(cmd, "@TeamLeader", DbType.Int32, employeeTeamBuilders.TeamLeader);
                Database.AddInParameter(cmd, "@EmployeeId", DbType.AnsiString, StringHelper.Convert(employeeTeamBuilders.EmployeeId));
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, employeeTeamBuilders.CreatorId);
                Database.ExecuteNonQuery(cmd);
                return null;
            }
        
        }
       

        EmployeeTeamBuilder IEmployeeTeamBuilderDataAccess.GetByTeamId(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.EmployeeTeam_GetByTeamId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<EmployeeTeamBuilder>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        IList<EmployeeTeamBuilder> IEmployeeTeamBuilderDataAccess.GetAllTeam()
        {
            const string SP = "dbo.EmployeeTeam_GetAllTeam";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<EmployeeTeamBuilder>().BuildEntities(reader);
                }
            }
        }

        bool IEmployeeTeamBuilderDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.EmployeeTeamBuilder_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a employeeTeamBuilders which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this employeeTeamBuilders.");
                        }
                }
            }
        }

        PagedResponse<EmployeeTeamBuilder> IEmployeeTeamBuilderDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.EmployeeTeamBuilder_GetPaged";
            string whereClause = string.Empty;
            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<EmployeeTeamBuilder> response = new PagedResponse<EmployeeTeamBuilder>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<EmployeeTeamBuilder>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        ArrayList IEmployeeTeamBuilderDataAccess.GetAllTeamByTeamLeaderId(int TeamLeaderId)
        {

            if (TeamLeaderId < 0)
            {
                throw new ArgumentException("TeamLeaderId");
            }
            const string SP = "dbo.EmployeeTeam_GetByTeamLeaderId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@TeamLeader", DbType.Int32, TeamLeaderId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    ArrayList teamList = new ArrayList();

                    while (reader.Read())
                    {
                        teamList.Add(new EmployeeTeamBuilder { Id = reader.GetInt32(0), Title = reader.GetString(1) });//1.3
                    }

                    return teamList;
                }
            }
        }

        /////////////Code Added by Sumit Sonawane on 20/May/2016/////////////////////////

        IList<EmployeeTeamBuilder> IEmployeeTeamBuilderDataAccess.GetTeamsByMemberId(int MemberId)
        {
            if (MemberId < 1)
            {
                throw new ArgumentNullException("MemberId");
            }

            const string SP = "dbo.EmployeeTeam_GetByTeamByMemberId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@MemberId", DbType.Int32, MemberId);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<EmployeeTeamBuilder>().BuildEntities(reader);
                }
            }
        }
        /////////////////////////////////end///////////////////////////////////////////


        #endregion
    }
}