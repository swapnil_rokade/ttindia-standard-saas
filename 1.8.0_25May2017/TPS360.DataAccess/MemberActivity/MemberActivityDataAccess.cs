﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using Microsoft.Practices.EnterpriseLibrary.Data;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;

namespace TPS360.DataAccess
{
    internal sealed class MemberActivityDataAccess : BaseDataAccess, IMemberActivityDataAccess
    {
        #region Constructors

        public MemberActivityDataAccess(Context context)
            : base(context)
        {
        }

        protected override IEntityBuilder<MemberActivity> CreateEntityBuilder<MemberActivity>()
        {
            return (new MemberActivityBuilder()) as IEntityBuilder<MemberActivity>;
        }

        #endregion

        #region  Methods

        MemberActivity IMemberActivityDataAccess.Add(MemberActivity memberActivity)
        {
            const string SP = "dbo.MemberActivity_Create";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@ActivityOn", DbType.Int32, memberActivity.ActivityOn);
                Database.AddInParameter(cmd, "@ActivityOnObjectId", DbType.Int32, memberActivity.ActivityOnObjectId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberActivity.IsRemoved);
                Database.AddInParameter(cmd, "@MemberActivityTypeId", DbType.Int32, memberActivity.MemberActivityTypeId);
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, memberActivity.CreatorId);
               
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberActivity = CreateEntityBuilder<MemberActivity>().BuildEntity(reader);
                    }
                    else
                    {
                        memberActivity = null;
                    }
                }

                if (memberActivity == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member activity already exists. Please specify another member activity.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while creating this member activity.");
                            }
                    }
                }

                return memberActivity;
            }
        }

        MemberActivity IMemberActivityDataAccess.Update(MemberActivity memberActivity)
        {
            const string SP = "dbo.MemberActivity_Update";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, memberActivity.Id);
                Database.AddInParameter(cmd, "@ActivityOn", DbType.Int32, memberActivity.ActivityOn);
                Database.AddInParameter(cmd, "@ActivityOnObjectId", DbType.Int32, memberActivity.ActivityOnObjectId);
                Database.AddInParameter(cmd, "@IsRemoved", DbType.Boolean, memberActivity.IsRemoved);
                Database.AddInParameter(cmd, "@UpdatorId", DbType.Int32, memberActivity.UpdatorId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        memberActivity = CreateEntityBuilder<MemberActivity>().BuildEntity(reader);
                    }
                    else
                    {
                        memberActivity = null;
                    }
                }

                if (memberActivity == null)
                {
                    int returnCode = GetReturnCodeFromParameter(cmd);

                    switch (returnCode)
                    {
                        case SqlConstants.DB_STATUS_CODE_ERROR_DUPLICATE_DATA:
                            {
                                throw new ArgumentException("Member activity already exists. Please specify another member activity.");
                            }
                        default:
                            {
                                throw new SystemException("An unexpected error has occurred while updating this member activity.");
                            }
                    }
                }

                return memberActivity;
            }
        }

        MemberActivity IMemberActivityDataAccess.GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberActivity_GetById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    if (reader.Read())
                    {
                        return (CreateEntityBuilder<MemberActivity>()).BuildEntity(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

       

        IList<MemberActivity> IMemberActivityDataAccess.GetAll()
        {
            const string SP = "dbo.MemberActivity_GetAll";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberActivity>().BuildEntities(reader);
                }
            }
        }

        IList<MemberActivity> IMemberActivityDataAccess.GetAllByCreatorId(int creatorId)
        {
            if (creatorId < 1)
            {
                throw new ArgumentNullException("creatorId");
            }

            const string SP = "dbo.MemberActivity_GetAllMemberActivityByCreatorId";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                Database.AddInParameter(cmd, "@CreatorId", DbType.Int32, creatorId);
                
                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    return CreateEntityBuilder<MemberActivity>().BuildEntities(reader);
                }
            }
        }

        PagedResponse<MemberActivity> IMemberActivityDataAccess.GetPaged(PagedRequest request)
        {
            const string SP = "dbo.MemberActivity_GetPaged";
            string whereClause = string.Empty;

            if (request.Conditions.Count > 0)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                string column = null;
                string value = null;               

                foreach (DictionaryEntry entry in request.Conditions)
                {
                    column = StringHelper.Convert(entry.Key);
                    value = StringHelper.Convert(entry.Value);

                    if (!StringHelper.IsBlank(column))
                    {
                        value = value.Replace("'", "''");

                        if (StringHelper.IsEqual(column, "creatorId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[M].[CreatorId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        if (StringHelper.IsEqual(column, "createDate"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append(" convert(varchar, [M].[CreateDate], 101) ");
                            sb.Append(" = ");
                            sb.Append("'"+value+"'");
                        }
                        else if (StringHelper.IsEqual(column, "activityOn"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[M].[ActivityOn]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                        else if (StringHelper.IsEqual(column, "activityOnObjectId"))
                        {
                            if (sb.ToString() != String.Empty)
                            {
                                sb.Append(" AND ");
                            }
                            sb.Append("[M].[ActivityOnObjectId]");
                            sb.Append(" = ");
                            sb.Append(value);
                        }
                    }
                }
                whereClause = sb.ToString();
            }

            if (StringHelper.IsBlank(request.SortColumn))
            {
                request.SortColumn = "CreateDate";
                request.SortOrder = "DESC";
            }

            request.SortColumn = "[M].[" + request.SortColumn + "]";

            object[] paramValues = new object[] {	request.PageIndex,
													request.RowPerPage,
													StringHelper.Convert(whereClause),
													StringHelper.Convert(request.SortColumn),
                                                    StringHelper.Convert(request.SortOrder)
												};

            using (DbCommand cmd = Database.GetStoredProcCommand(SP, paramValues))
            {
                PagedResponse<MemberActivity> response = new PagedResponse<MemberActivity>();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    response.Response = CreateEntityBuilder<MemberActivity>().BuildEntities(reader);

                    if ((reader.NextResult()) && (reader.Read()))
                    {
                        response.TotalRow = reader.GetInt32(0);
                    }
                }

                return response;
            }
        }

        bool IMemberActivityDataAccess.DeleteById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "dbo.MemberActivity_DeleteById";

            using (DbCommand cmd = Database.GetStoredProcCommand(SP))
            {
                AddOutputParameter(cmd);
                Database.AddInParameter(cmd, "@Id", DbType.Int32, id);

                Database.ExecuteNonQuery(cmd);

                int returnCode = GetReturnCodeFromParameter(cmd);

                switch (returnCode)
                {
                    case SqlConstants.DB_STATUS_CODE_SUCCESS_DELETE:
                        {
                            return true;
                        }
                    case SqlConstants.DB_STATUS_CODE_ERROR_CHILD_EXISTS:
                        {
                            throw new ArgumentException("Cannot delete a member activity which has association.");
                        }
                    default:
                        {
                            throw new SystemException("An unexpected error has occurred while deleting this member activity.");
                        }
                }
            }
        }

        #endregion
    }
}