﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

using System.Data.SqlClient;

namespace TPS360.Automation
{
    internal sealed class JobPostingDataAccess:BaseDataAccess
    {
        #region Constructors

        public JobPostingDataAccess()
        {

        }

        protected override IEntityBuilder<JobPosting> CreateEntityBuilder<JobPosting>()
        {
            return (new JobPostingBuilder()) as IEntityBuilder<JobPosting>;
        }

        #endregion

        #region  Methods

        public JobPosting GetById(int id)
        {
            if (id < 1)
            {
                throw new ArgumentNullException("id");
            }

            const string SP = "JobPosting_GetById";

            using (SqlConnection objSqlCon = new SqlConnection("context connection=true;"))
            {
                objSqlCon.Open();
                using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Id", SqlDbType.Int);
                    cmd.Parameters["@Id"].Value = id;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return CreateEntityBuilder<JobPosting>().BuildEntity(reader);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }

        public IList<JobPosting> GetAllByMemberId(int memberId)
        {
            if (memberId < 1)
            {
                throw new ArgumentNullException("memberId");
            }

            const string SP = "JobPosting_GetAllJobPostingByMemberId";

            using (SqlConnection objSqlCon = new SqlConnection("context connection=true;"))
            {
                objSqlCon.Open();
                using (SqlCommand cmd = new SqlCommand(SP,objSqlCon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@MemberId", SqlDbType.Int);
                    cmd.Parameters["@MemberId"].Value = memberId;
                    
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        return CreateEntityBuilder<JobPosting>().BuildEntities(reader);
                    }
                }
            }
        }

        #endregion
    }
}