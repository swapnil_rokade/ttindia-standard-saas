﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

using System.Data.SqlClient;

namespace TPS360.Automation
{
    internal sealed class JobPostingAssessmentDataAccess:BaseDataAccess
    {
        #region Constructors

        public JobPostingAssessmentDataAccess()
        {
        }

        protected override IEntityBuilder<JobPostingAssessment> CreateEntityBuilder<JobPostingAssessment>()
        {
            return (new JobPostingAssessmentBuilder()) as IEntityBuilder<JobPostingAssessment>;
        }

        #endregion

        #region Method
        
        public IList<JobPostingAssessment> GetAllByJobPostingId(int jobPostingId)
        {
            if (jobPostingId < 1)
            {
                throw new ArgumentNullException("jobPostingId");
            }

            const string SP = "JobPostingAssessment_GetAllByJobPostingId";

            using (SqlConnection objSqlCon = new SqlConnection("context connection=true;"))
            {
                objSqlCon.Open();
                using (SqlCommand cmd = new SqlCommand(SP, objSqlCon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@jobPostingId", SqlDbType.Int);
                    cmd.Parameters["@jobPostingId"].Value = jobPostingId;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        return CreateEntityBuilder<JobPostingAssessment>().BuildEntities(reader);
                    }
                }
            }
        }

        #endregion
    }
}
