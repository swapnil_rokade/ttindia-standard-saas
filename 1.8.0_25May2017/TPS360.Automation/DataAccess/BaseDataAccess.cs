using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace TPS360.Automation
{
    internal interface IEntityBuilder<T> where T : BaseEntity
    {
        IList<T> BuildEntities(SqlDataReader dataReader);

        T BuildEntity(SqlDataReader dataReader);
    }

    internal abstract class BaseDataAccess
    {
        #region Constructer & Destructer

        protected BaseDataAccess()
        {
        }

        #endregion

        #region Protected Methods

        [DebuggerStepThrough()]
        protected abstract IEntityBuilder<T> CreateEntityBuilder<T>() where T : BaseEntity;

        [DebuggerStepThrough()]
        protected virtual int GetReturnCodeFromParameter(SqlCommand command)
        {
            return (int)command.Parameters[SqlConstants.PRM_RETURN_CODE].Value;
        }

        [DebuggerStepThrough()]
        protected virtual int GetNewIdFromParameter(SqlCommand command, string paramName)
        {
            return (int)command.Parameters[paramName].Value;
        }

        [DebuggerStepThrough()]
        protected virtual int GetTotalRowFromParameter(SqlCommand command)
        {
            return (int)command.Parameters[SqlConstants.PRM_TOTAL_ROW].Value;
        }

        [DebuggerStepThrough()]
        protected virtual int GetTotalPageFromParameter(SqlCommand command)
        {
            return (int)command.Parameters[SqlConstants.PRM_TOTAL_PAGE].Value;
        }

        [DebuggerStepThrough()]
        protected virtual int GetCountFromParameter(SqlCommand command)
        {
            return (int)command.Parameters[SqlConstants.PRM_COUNT].Value;
        }

        [DebuggerStepThrough()]
        protected void AddOutputParameter(SqlCommand command)
        {
            SqlParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@ReturnCode";
            parameter.SqlDbType = SqlDbType.Int;
            parameter.Direction = ParameterDirection.Output;
            parameter.Size = 4;
        }

        #endregion
    }
}