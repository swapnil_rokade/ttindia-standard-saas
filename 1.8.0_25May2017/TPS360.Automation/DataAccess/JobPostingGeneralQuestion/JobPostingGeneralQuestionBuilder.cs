﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace TPS360.Automation
{
    internal sealed class JobPostingGeneralQuestionBuilder : IEntityBuilder<JobPostingGeneralQuestion>
    {
        IList<JobPostingGeneralQuestion> IEntityBuilder<JobPostingGeneralQuestion>.BuildEntities(SqlDataReader reader)
        {
            List<JobPostingGeneralQuestion> jobPostingGeneralQuestions = new List<JobPostingGeneralQuestion>();

            while (reader.Read())
            {
                jobPostingGeneralQuestions.Add(((IEntityBuilder<JobPostingGeneralQuestion>)this).BuildEntity(reader));
            }

            return (jobPostingGeneralQuestions.Count > 0) ? jobPostingGeneralQuestions : null;
        }

        JobPostingGeneralQuestion IEntityBuilder<JobPostingGeneralQuestion>.BuildEntity(SqlDataReader reader)
        {
            const int FLD_ID = 0;
            const int FLD_QUESTION = 1;
            const int FLD_SCREENINGSCORE = 2;
            const int FLD_QUESTIONORDER = 3;
            const int FLD_QUESTIONTYPE = 4;
            const int FLD_JOBPOSTINGID = 5;
            const int FLD_CREATORID = 6;
            const int FLD_UPDATORID = 7;
            const int FLD_CREATEDATE = 8;
            const int FLD_UPDATEDATE = 9;

            JobPostingGeneralQuestion jobPostingGeneralQuestion = new JobPostingGeneralQuestion();

            jobPostingGeneralQuestion.Id = reader.IsDBNull(FLD_ID) ? 0 : reader.GetInt32(FLD_ID);
            jobPostingGeneralQuestion.Question = reader.IsDBNull(FLD_QUESTION) ? string.Empty : reader.GetString(FLD_QUESTION);
            jobPostingGeneralQuestion.ScreeningScore = reader.IsDBNull(FLD_SCREENINGSCORE) ? 0 : reader.GetInt32(FLD_SCREENINGSCORE);
            jobPostingGeneralQuestion.QuestionOrder = reader.IsDBNull(FLD_QUESTIONORDER) ? 0 : reader.GetInt32(FLD_QUESTIONORDER);
            jobPostingGeneralQuestion.QuestionType = reader.IsDBNull(FLD_QUESTIONTYPE) ? string.Empty : reader.GetString(FLD_QUESTIONTYPE);
            jobPostingGeneralQuestion.JobPostingId = reader.IsDBNull(FLD_JOBPOSTINGID) ? 0 : reader.GetInt32(FLD_JOBPOSTINGID);
            jobPostingGeneralQuestion.CreatorId = reader.IsDBNull(FLD_CREATORID) ? 0 : reader.GetInt32(FLD_CREATORID);
            jobPostingGeneralQuestion.UpdatorId = reader.IsDBNull(FLD_UPDATORID) ? 0 : reader.GetInt32(FLD_UPDATORID);
            jobPostingGeneralQuestion.CreateDate = reader.IsDBNull(FLD_CREATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_CREATEDATE);
            jobPostingGeneralQuestion.UpdateDate = reader.IsDBNull(FLD_UPDATEDATE) ? DateTime.MinValue : reader.GetDateTime(FLD_UPDATEDATE);

            return jobPostingGeneralQuestion;
        }
    }
}
