﻿using System;

namespace TPS360.Automation
{
    internal sealed class JobPosting : BaseEntity
    {
        #region Properties

        
        public string JobTitle
        {
            get;
            set;
        }

        
        public string JobPostingCode
        {
            get;
            set;
        }

        
        public string ClientJobId
        {
            get;
            set;
        }

        
        public int NoOfOpenings
        {
            get;
            set;
        }

        
        public string PayRate
        {
            get;
            set;
        }

        
        public int PayRateCurrencyLookupId
        {
            get;
            set;
        }

        
        public string PayCycle
        {
            get;
            set;
        }

        
        public bool TravelRequired
        {
            get;
            set;
        }

        
        public string TravelRequiredPercent
        {
            get;
            set;
        }

        
        public string OtherBenefits
        {
            get;
            set;
        }

        
        public int JobStatus
        {
            get;
            set;
        }

        
        public int JobDurationLookupId
        {
            get;
            set;
        }

        
        public string JobDurationMonth
        {
            get;
            set;
        }

        
        public string JobAddress1
        {
            get;
            set;
        }

        
        public string JobAddress2
        {
            get;
            set;
        }

        
        public string City
        {
            get;
            set;
        }

        
        public string ZipCode
        {
            get;
            set;
        }

        
        public int CountryId
        {
            get;
            set;
        }

        
        public int StateId
        {
            get;
            set;
        }

        
        public string StartDate
        {
            get;
            set;
        }

        
        public DateTime FinalHiredDate
        {
            get;
            set;
        }

        
        public string JobDescription
        {
            get;
            set;
        }

        
        public string AuthorizationTypeLookupId
        {
            get;
            set;
        }

        
        public DateTime PostedDate
        {
            get;
            set;
        }

        
        public DateTime ActivationDate
        {
            get;
            set;
        }

        
        public int RequiredDegreeLookupId
        {
            get;
            set;
        }

        
        public string MinExpRequired
        {
            get;
            set;
        }

        
        public string MaxExpRequired
        {
            get;
            set;
        }

        
        public int VendorGroupId
        {
            get;
            set;
        }

        
        public bool IsJobActive
        {
            get;
            set;
        }

        
        public bool PublishedForInternal
        {
            get;
            set;
        }

        
        public bool PublishedForPublic
        {
            get;
            set;
        }

        
        public bool PublishedForPartner
        {
            get;
            set;
        }

        
        public int MinAgeRequired
        {
            get;
            set;
        }

        
        public int MaxAgeRequired
        {
            get;
            set;
        }

        
        public int JobType
        {
            get;
            set;
        }

        
        public bool IsApprovalRequired
        {
            get;
            set;
        }

        
        public string InternalNote
        {
            get;
            set;
        }

        
        public int ClientId
        {
            get;
            set;
        }

        
        public int ClientProjectId
        {
            get;
            set;
        }

        
        public string ClientProject
        {
            get;
            set;
        }

        
        public int ClientEndClientId
        {
            get;
            set;
        }

        
        public string ClientHourlyRate
        {
            get;
            set;
        }

        
        public int ClientHourlyRateCurrencyLookupId
        {
            get;
            set;
        }

        
        public string ClientRatePayCycle
        {
            get;
            set;
        }

        
        public string ClientDisplayName
        {
            get;
            set;
        }

        
        public int ClientId2
        {
            get;
            set;
        }

        
        public int ClientId3
        {
            get;
            set;
        }

        
        public string ClientJobDescription
        {
            get;
            set;
        }

        
        public string TaxTermLookupIds
        {
            get;
            set;
        }

        
        public int JobCategoryLookupId
        {
            get;
            set;
        }

        
        public int JobCategorySubId
        {
            get;
            set;
        }

        
        public int JobIndustryLookupId
        {
            get;
            set;
        }

        
        public decimal ExpectedRevenue
        {
            get;
            set;
        }

        
        public int ExpectedRevenueCurrencyLookupId
        {
            get;
            set;
        }

        
        public string SourcingChannel
        {
            get;
            set;
        }

        
        public decimal SourcingExpenses
        {
            get;
            set;
        }

        
        public int SourcingExpensesCurrencyLookupId
        {
            get;
            set;
        }

        
        public bool WorkflowApproved
        {
            get;
            set;
        }

        
        public bool PublishedForVendor
        {
            get;
            set;
        }

        
        public bool TeleCommunication
        {
            get;
            set;
        }

        
        public bool IsTemplate
        {
            get;
            set;
        }

        
        public bool IsRemoved
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public JobPosting()
            : base()
        {
        }

        #endregion
    }
}