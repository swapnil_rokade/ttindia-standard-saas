﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Automation
{
    internal sealed class TestMaster : BaseEntity
    {
        #region Properties

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }

        public int NumberOfDisplayQuestions
        {
            get;
            set;
        }

        public string Duration
        {
            get;
            set;
        }

        public int TotalScore
        {
            get;
            set;
        }

        public int PassingScore
        {
            get;
            set;
        }

        public bool NegativeMarking
        {
            get;
            set;
        }

        public int NegativeMarks
        {
            get;
            set;
        }

        public int PublishStatus
        {
            get;
            set;
        }

        public bool IsFromExistingTest
        {
            get;
            set;
        }

        public bool IsRemoved
        {
            get;
            set;
        }

        public int PublisherId
        {
            get;
            set;
        }

        public DateTime PublishDate
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public TestMaster()
            : base()
        {
        }

        #endregion
    }
}
