﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberExtendedInformation.cs
    Description: This page is used to hold properties related to additional information about member.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.         Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             Apr-20-2009         Shivanand           Defect# 10169; New prperties added viz. ExpectedYearlyMaxRate,ExpectedMonthlyMaxRate,
                                                                           ExpectedHourlyMaxRate.
    0.2             Sept-14-2009        Nagarathna          Enhancement #11473; Mail set up for individual user.
    -------------------------------------------------------------------------------------------------------------------------------------- 
*/


using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberExtendedInformation", Namespace = "http://www.tps360.com/types")]
    public class MemberExtendedInformation : BaseEntity
    {
        #region Properties

        [DataMember]
        public bool Relocation
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentPosition
        {
            get;
            set;
        }

        [DataMember]
        public string AdditionalJobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string LastEmployer
        {
            get;
            set;
        }

        [DataMember]
        public int LastEmployerTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int HighestEducationLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string TotalExperienceYears
        {
            get;
            set;
        }

        [DataMember]
        public int Availability
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AvailableDate
        {
            get;
            set;
        }

        [DataMember]
        public decimal CurrentYearlyRate
        {
            get;
            set;
        }


        [DataMember]
        public int CurrentYearlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal CurrentMonthlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentMonthlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal CurrentHourlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentHourlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentSalaryNote
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedYearlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedYearlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedMonthlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedMonthlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedHourlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedHourlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string ExpectedSalaryNote
        {
            get;
            set;
        }

        [DataMember]
        public bool WillingToTravel
        {
            get;
            set;
        }

        [DataMember]
        public bool PassportStatus
        {
            get;
            set;
        }

        [DataMember]
        public int SalaryTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int JobTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public bool SecurityClearance
        {
            get;
            set;
        }

        [DataMember]
        public int WorkAuthorizationLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int WorkScheduleLookupId
        {
            get;
            set;
        } 

        [DataMember]
        public int InternalRatingLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string PreferredLocation
        {
            get;
            set;
        }

        [DataMember]
        public string IndustryExperience
        {
            get;
            set;
        }

        [DataMember]
        public int IndustryTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Remarks
        {
            get;
            set;
        }

        [DataMember]
        public string Note
        {
            get;
            set;
        }

        [DataMember]
        public string Category
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

    // 0.1 starts

        [DataMember]
        public decimal ExpectedYearlyMaxRate
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedMonthlyMaxRate
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedHourlyMaxRate
        {
            get;
            set;
        }

    // 0.1 ends
        [DataMember]
        public byte[] MailSetting//0.2  
        {
            get;
            set;
        }
        [DataMember]
        public int CurrentSalaryPayCycle
        {
            get;
            set;
        }
        [DataMember]
        public int ExpectedSalaryPayCycle
        {
            get;
            set;
        }

        [DataMember]
        public string PassportNumber
        {
            get;
            set;
        }
        [DataMember]
        public int FunctionAreaLookupId
        {
            get;
            set;
        }
        [DataMember]
        public bool WillingToReLocate
        {
            get;
            set;
        }
        [DataMember]
        public string OldDbRefId
        {
            get;
            set;
        }

        [DataMember]
        public string PANNumber
        {
            get;
            set;
        }
        [DataMember]
        public string Website
        {
            get;
            set;
        }
        [DataMember]
        public string LinkedinProfile
        {
            get;
            set;
        }
        [DataMember]
        public int SourceLookupId
        {
            get;
            set;
        }
        [DataMember]
        public string SourceDescription
        {
            get;
            set;
        }
        [DataMember]
        public int IdCardLookUpId
        {
            get;
            set;
        }
        public string IdCardDetail
        {
            get;
            set;
        }

        [DataMember]
        public string NoticePeriod
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public MemberExtendedInformation()
            : base()
        {
        }

        #endregion
    }
}