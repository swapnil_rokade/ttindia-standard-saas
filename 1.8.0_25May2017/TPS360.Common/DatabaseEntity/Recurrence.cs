﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Recurrence", Namespace = "http://www.tps360.com/types")]
    public class Recurrence : BaseEntity
    {
        #region Properties

        [DataMember]
        public int RecurrenceID
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndDateUtc
        {
            get;
            set;
        }

        [DataMember]
        public int DayOfWeekMaskUtc
        {
            get;
            set;
        }

        [DataMember]
        public int UtcOffset
        {
            get;
            set;
        }

        [DataMember]
        public int DayOfMonth
        {
            get;
            set;
        }

        [DataMember]
        public int MonthOfYear
        {
            get;
            set;
        }

        [DataMember]
        public int PeriodMultiple
        {
            get;
            set;
        }

        [DataMember]
        public string Period
        {
            get;
            set;
        }

        [DataMember]
        public int EditType
        {
            get;
            set;
        }

        [DataMember]
        public DateTime LastReminderDateTimeUtc
        {
            get;
            set;
        }

        [DataMember]
        public byte[] _ts
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public Recurrence()
            : base()
        {
        }

        #endregion
    }
}