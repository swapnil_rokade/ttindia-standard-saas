﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "OccupationalGroup", Namespace = "http://www.tps360.com/types")]
    public class OccupationalGroup : BaseEntity
    {
        #region Properties

        [DataMember]
        public string GroupTitle
        {
            get;
            set;
        }

       

        #endregion

        #region Constructor

        public OccupationalGroup()
            : base()
        {
        }

        #endregion
    }
}