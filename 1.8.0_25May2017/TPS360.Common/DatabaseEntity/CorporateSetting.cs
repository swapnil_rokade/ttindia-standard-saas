﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CorporateSetting", Namespace = "http://www.tps360.com/types")]
    public class CorporateSetting : BaseEntity
    {
        #region Properties

        [DataMember]
        public byte[] CurrentSetting
        {
            get;
            set;
        }

        [DataMember]
        public byte[] PreviousSetting
        {
            get;
            set;
        }

        [DataMember]
        public int SettingType
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CorporateSetting()
            : base()
        {
        }

        #endregion
    }
}