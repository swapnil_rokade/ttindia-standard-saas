﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Lead", Namespace = "http://www.tps360.com/types")]
    public class Lead : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int LeadPriority
        {
            get;
            set;
        }

        [DataMember]
        public int SalesProbability
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedRevenue
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedRevenueCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int SalesLifeTime
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }

        [DataMember]
        public int Source
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public Lead()
            : base()
        {
        }

        #endregion
    }
}