﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "OnBoardingOffBoardingChecklistManager", Namespace = "http://www.tps360.com/types")]
    public class OnBoardingOffBoardingChecklistManager : BaseEntity
    {
        #region Properties

        [DataMember]
        public int OnBoardingOffBoardingChecklistId
        {
            get;
            set;
        }

        [DataMember]
        public int AssignedManagerId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public OnBoardingOffBoardingChecklistManager()
            : base()
        {
        }

        #endregion
    }
}