﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "WorkOrderExpense", Namespace = "http://www.tps360.com/types")]
    public class WorkOrderExpense : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ExpenseType
        {
            get;
            set;
        }

        [DataMember]
        public decimal Cost
        {
            get;
            set;
        }

        [DataMember]
        public int WorkOrderProfitAndLossDetailId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public WorkOrderExpense()
            : base()
        {
        }

        #endregion
    }
}