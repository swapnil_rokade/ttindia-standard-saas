﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Organization", Namespace = "http://www.tps360.com/types")]
    public class Organization : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Address1
        {
            get;
            set;
        }

        [DataMember]
        public string Address2
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public int StateId
        {
            get;
            set;
        }

        [DataMember]
        public string Zip
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        [DataMember]
        public string WebAddress
        {
            get;
            set;
        }

        [DataMember]
        public string Logo
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyInformation
        {
            get;
            set;
        }

        [DataMember]
        public int IndustryType
        {
            get;
            set;
        }

        [DataMember]
        public int OrganizationSize
        {
            get;
            set;
        }

        [DataMember]
        public int OwnerType
        {
            get;
            set;
        }

        [DataMember]
        public string TickerSymbol
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhone
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public string TollFreePhone
        {
            get;
            set;
        }

        [DataMember]
        public string TollFreePhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public string FaxNumber
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryEmail
        {
            get;
            set;
        }

        [DataMember]
        public string SecondaryEmail
        {
            get;
            set;
        }

        [DataMember]
        public string EINNumber
        {
            get;
            set;
        }

        [DataMember]
        public string AnnualRevenue
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfEmployee
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public Organization()
            : base()
        {
        }

        #endregion
    }
}