﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ActivityReport.cs
    Description: This page is used for Audit Trail Activity reports functionality 
    Created By: Prashant
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
   -------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Runtime.Serialization;
namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ActivityReport", Namespace = "http://www.tps360.com/types")]
    public class ActivityReport : BaseEntity
    {

       
        #region Properties

        [DataMember]
        public long  ID
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DateAndTime
        {
            get;
            set;
        }
        [DataMember]
        public string Employee
        {
            get;
            set;
        }

        [DataMember]
        public string Role
        {
            get;
            set;
        }
        [DataMember]
        public string ActivityType
        {
            get;
            set;
        }

        [DataMember]
        public string Requisition
        {
            get;
            set;
        }
        public string Account
        {
            get;
            set;
        }

        [DataMember]
        public string AccountContact
        {
            get;
            set;
        }
        public string Applicant
        {
            get;
            set;
        }

        [DataMember]
        public string ApplicantType
        {
            get;
            set;
        }
        [DataMember]
        public int MemberID
        {
            get;
            set;
        }
        [DataMember]
        public long EntityID
        {
            get;
            set;
        }
        [DataMember]
        public int AccountID
        {
            get;
            set;
        }
        #endregion
         #region Constructor

        public ActivityReport()
            : base()
        {
        }
         #endregion

    }
}
