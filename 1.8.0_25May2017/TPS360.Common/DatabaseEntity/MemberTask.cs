﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberTask", Namespace = "http://www.tps360.com/types")]
    public class MemberTask : BaseEntity
    {
        #region Properties

        [DataMember]
        public int AssignedBy
        {
            get;
            set;
        }

        [DataMember]
        public string CompletionNote
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AssignedDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime CompletionDate
        {
            get;
            set;
        }

        [DataMember]
        public int PercentageComplete
        {
            get;
            set;
        }

        [DataMember]
        public int CompletionStatus
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int CommonTaskId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberTask()
            : base()
        {
        }

        #endregion
    }
}