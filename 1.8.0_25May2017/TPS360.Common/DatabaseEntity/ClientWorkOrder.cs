﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ClientWorkOrder", Namespace = "http://www.tps360.com/types")]
    public class ClientWorkOrder : BaseEntity
    {
        #region Properties

        [DataMember]
        public string WorkOrderCode
        {
            get;
            set;
        }

        [DataMember]
        public int ClientId
        {
            get;
            set;
        }

        [DataMember]
        public int ContactId
        {
            get;
            set;
        }

        [DataMember]
        public int OrganizationId
        {
            get;
            set;
        }

        [DataMember]
        public int OrganizationContactId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime WorkOrderDate
        {
            get;
            set;
        }

        [DataMember]
        public string MasterAgreementNumber
        {
            get;
            set;
        }

        [DataMember]
        public string ClientReferenceNumber
        {
            get;
            set;
        }

        [DataMember]
        public DateTime StartDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndDate
        {
            get;
            set;
        }

        [DataMember]
        public int ApproximateDurationLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal DailyWorkHours
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentTermsLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int RequisitionId
        {
            get;
            set;
        }

        [DataMember]
        public int ProjectId
        {
            get;
            set;
        }

        [DataMember]
        public string EndClientName
        {
            get;
            set;
        }

        [DataMember]
        public bool IsTravelExpenseAvailable
        {
            get;
            set;
        }

        [DataMember]
        public bool IsHotelAccommodationAvailable
        {
            get;
            set;
        }

        [DataMember]
        public bool IsOnsiteInterviewExpenseAvailable
        {
            get;
            set;
        }

        [DataMember]
        public string ProjectManager
        {
            get;
            set;
        }

        [DataMember]
        public string InvoiceAndPaymentTerms
        {
            get;
            set;
        }

        [DataMember]
        public string Note
        {
            get;
            set;
        }

        [DataMember]
        public string WorkOrderFootNote
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public ClientWorkOrder()
            : base()
        {
        }

        #endregion
    }
}