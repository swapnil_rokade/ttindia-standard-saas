﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberHiringDetails", Namespace = "http://www.tps360.com/types")]
    public class MemberHiringDetails : BaseEntity
    {
        #region Properties

        [DataMember]
        public string JobPostingCode
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string ApplicantName
        {
            get;
            set;
        }
        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public string OfferedPosition
        {
            get;
            set;
        }

        [DataMember]
        public string OfferedSalary
        {
            get;
            set;
        }
        [DataMember]
        public int OfferedSalaryPayCycle
        {
            get;
            set;
        }

        [DataMember]
        public int OfferedSalaryCurrency
        {
            get;
            set;
        }

        [DataMember]
        public bool OfferAccepted
        {
            get;
            set;
        }

        [DataMember]
        public DateTime JoiningDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime OfferedDate
        {
            get;
            set;
        }

        [DataMember]
        public string CommissionPayRate
        {
            get;
            set;
        }

        [DataMember]
        public int CommissionCurrency
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCTC 
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentCTCLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string MinExpectedCTC
        {
            get;
            set;
        }

        [DataMember]
        public string MaxExpectedCTC
        {
            get;
            set;
        }
        
        [DataMember]
        public int ExpectedCTCLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Grade
        {
            get;
            set;
        }

        [DataMember]
        public string Experience
        {
            get;
            set;
        }

        [DataMember]
        public int Source
        {
            get;
            set;
        }

        [DataMember]
        public string SourceDescription
        {
            get;
            set;
        }

        [DataMember]
        public int FitmentPercentile
        {
            get;
            set;
        }

        [DataMember]
        public string JoiningBonus
        {
            get;
            set;
        }

        [DataMember]
        public string RelocationAllowance
        {
            get;
            set;
        }

        [DataMember]
        public string SpecialLumpSum
        {
            get;
            set;
        }

        [DataMember]
        public string NoticePeriodPayout
        {
            get;
            set;
        }

        [DataMember]
        public string PlacementFees
        {
            get;
            set;
        }

        [DataMember]
        public int BVStatus
        {
            get;
            set;
        }

        [DataMember]
        public int OfferCategory
        {
            get;
            set;
        }
        
        [DataMember]
        public decimal BasicEmbedded
        {
            get;
            set;
        }

        [DataMember]
        public decimal HRAEmbedded
        {
            get;
            set;
        }

        [DataMember]
        public decimal Conveyance
        {
            get;
            set;
        }

        [DataMember]
        public decimal Medical
        {
            get;
            set;
        }

        [DataMember]
        public decimal EducationalAllowanceEmbedded
        {
            get;
            set;
        }

        [DataMember]
        public decimal LUFS
        {
            get;
            set;
        }

        [DataMember]
        public decimal Adhoc
        {
            get;
            set;
        }

        [DataMember]
        public decimal MPP
        {
            get;
            set;
        }

        [DataMember]
        public decimal AGVI
        {
            get;
            set;
        }

        [DataMember]
        public decimal LTAEmbedded
        {
            get;
            set;
        }

        [DataMember]
        public decimal PFEmbedded
        {
            get;
            set;
        }

        [DataMember]
        public decimal GratuityEmbedded
        {
            get;
            set;
        }

        [DataMember]
        public decimal MaximumAnnualIncentive
        {
            get;
            set;
        }

        [DataMember]
        public decimal EmbeddedAllowance
        {
            get;
            set;
        }

        [DataMember]
        public decimal SalesIncentive
        {
            get;
            set;
        }

        [DataMember]
        public decimal PLMAllowance
        {
            get;
            set;
        }

        [DataMember]
        public decimal CarAllowance
        {
            get;
            set;
        }

        [DataMember]
        public decimal GrandTotal
        {
            get;
            set;
        }
       
        
        [DataMember]
        public decimal BasicMech 
        {
            get;
            set;
        }

        [DataMember]
        public decimal FlexiPay1
        {
            get;
            set;
        }

        [DataMember]
        public decimal FlexiPay2
        {
            get;
            set;
        }

        [DataMember]
        public decimal AdditionalAllowance
        {
            get;
            set;
        }

        [DataMember]
        public decimal AdHocAllowance
        {
            get;
            set;
        }

        [DataMember]
        public decimal LocationAllowance
        {
            get;
            set;
        }

        [DataMember]
        public decimal SAFAllowance
        {
            get;
            set;
        }

        [DataMember]
        public decimal HRAMech 
        {
            get;
            set;
        }

        [DataMember]
        public decimal HLISA 
        {
            get;
            set;
        }

        [DataMember]
        public decimal EducationAllowanceMech
        {
            get;
            set;
        }
        [DataMember]
        public decimal ACLRA 
        {
            get;
            set;
        }

        [DataMember]
        public decimal CLRA
        {
            get;
            set;
        }

        [DataMember]
        public decimal SpecialAllowance
        {
            get;
            set;
        }

        [DataMember]
        public decimal SpecialPerformancePay
        {
            get;
            set;
        }

        [DataMember]
        public decimal ECAL
        {
            get;
            set;
        }

        [DataMember]
        public decimal CarMileageReimbursement 
        {
            get;
            set;
        }

        [DataMember]
        public decimal TelephoneReimbursement
        {
            get;
            set;
        }

        [DataMember]
        public decimal LTAMech  
        {
            get;
            set;
        }

        [DataMember]
        public decimal PFMech  
        {
            get;
            set;
        }

        [DataMember]
        public decimal GratuityMech  
        {
            get;
            set;
        }

        [DataMember]
        public decimal MedicalreimbursementsDomiciliary
        {
            get;
            set;
        }

        [DataMember]
        public decimal PCScheme
        {
            get;
            set;
        }

        [DataMember]
        public decimal RetentionPay
        {
            get;
            set;
        }

        [DataMember]
        public decimal PLRMech
        {
            get;
            set;
        }

        [DataMember]
        public decimal SalesIncentiveMech
        {
            get;
            set;
        }
       

        [DataMember]
        public decimal TotalCTC
        {
            get;
            set;
        }


        [DataMember]
        public int ActiveRecruiterID
        {
            get;
            set;
        }
        [DataMember]
        public string ActiveRecruiterName
        {
            get;
            set;
        }

        [DataMember]
        public string OfferStatus
        {
            get;
            set;
        }
        [DataMember]
        public string WorkLocation
        {
            get;
            set;
        }

        [DataMember]
        public decimal RoleAllowance
        {
            get;
            set;
        }
        [DataMember]
        public decimal SiteAllowance
        {
            get;
            set;
        }
        [DataMember]
        public decimal RetentionBonus
        {
            get;
            set;
        }
        [DataMember]
        public decimal Reimbursement
        {
            get;
            set;
        }
        [DataMember]
        public decimal ESIC
        {
            get;
            set;
        }
        [DataMember]
        public decimal RelocationAllowanceForCom
        {
            get;
            set;
        }
        [DataMember]
        public decimal Bonus
        {
            get;
            set;
        }

        [DataMember]
        public string CandidateName
        {
            get;
            set;
        }
        [DataMember]
        public string Designation
        {
            get;
            set;
        }
        [DataMember]
        public string Location
        {
            get;
            set;
        }
        [DataMember]
        public decimal GrossSalary
        {
            get;
            set;
        }
        [DataMember]
        public decimal PerformanceLinkedIncentive
        {
            get;
            set;
        }
        [DataMember]
        public int BandId
        {
            get;
            set;
        }
        [DataMember]
        public int ManagementBandId
        {
            get;
            set;
        }
        [DataMember]
        public string Band
        {
            get;
            set;
        }
        [DataMember]
        public string ManagementBand
        {
            get;
            set;
        }
        [DataMember]
        public string BU
        {
            get;
            set;
        }

        [DataMember]
        public int CityId
        {
            get;
            set;
        }
        [DataMember]
        public string City
        {
            get;
            set;
        }
        [DataMember]
        public string Name
        {
            get;
            set;
        }
        [DataMember]
        public string Address
        {
            get;
            set;
        }
        [DataMember]
        public string Address2
        {
            get;
            set;
        }
        [DataMember]
        public int PostCode
        {
            get;
            set;
        }
        [DataMember]
        public string JobTitle1
        {
            get;
            set;
        }
        [DataMember]
        public string Supervisor
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public MemberHiringDetails()
            : base()
        {
        }

        #endregion
    }
}