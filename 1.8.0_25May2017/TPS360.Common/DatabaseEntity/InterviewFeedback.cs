﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewFeedback.cs
    Description         :   This page is used to Create Properties for Interview Feed back.
    Created By          :   
    Created On          :   
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1                 20/JUL/2015         Prasanth            Introduced FirstName,LastName,Email,EmployeeId,DocumentName
 *  0.2                 28/Dec/2015         Prasanth            Introduced OverallFeedback
 *  0.3                 25/Jan/2016         pravin              Introduced by AnswerType
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "InterviewFeedback", Namespace = "http://www.tps360.com/types")]
    public class InterviewFeedback : BaseEntity
    {
        #region Properties

        public string Title
        {
            get;
            set;
        }

        public string Feedback
        {
            get;
            set;
        }

        public string SubmittedBy
        {
            get;
            set;
        }
        public DateTime SubmittedDate
        {
            get;
            set;
        }

        public int CreatorId
        {
            get;
            set;
        }
        public int UpdatorId
        {
            get;
            set;
        }

        public int InterviewId
        {
            get;
            set;
        }

        public int InterviewRound
        {
            get;
            set;
        }

        //Code introduced by Prasanth on 20/Jul/2015 Start
        public string FirstName
        {
            get;
            set;
        }
        public string LastName
        {
            get;
            set;
        }
        public string EmployeeID
        {
            get;
            set;
        }
        public string Email
        {
            get;
            set;
        }
        public string DocumentName
        {
            get;
            set;
        }

        //*******************END**************************

        public string OverallFeedback  //Introduced by Prasanth on 28/Dec/2015
        {
            get;
            set;
        }

        //Code introduced by Pravin  on 24/Dec/2015 Start
        public int CandidateID
        {
            get;
            set;
        }
        public string CandidateName
        {
            get;
            set;
        }
        public int ReqCode
        {
            get;
            set;
        }

        public string JobTitle
        {
            get;
            set;
        }
        public string InterviewerName
        {
            get;
            set;
        }
        public DateTime InterviewerDate
        {
            get;
            set;
        }
        //public string OverAllFeedback  OverAllFeedback
        //{
        //    get;
        //    set;
        //}
        public string InterviewRoundRpt
        {
            get;
            set;
        }

        public string Question
        {
            get;
            set;
        }
        public string Response
        {
            get;
            set;
        }
        public string rationale
        {
            get;
            set;
        }
        public string AnswerType //added by pravin khot on 25/Jan/2016
        {
            get;
            set;
        }
        public string JobPostingCode //added by pravin khot on 25/Jan/2016
        {
            get;
            set;
        }

        //*******************END**************************


        #endregion

        #region Constructor

        public InterviewFeedback()
            : base()
        {
        }

        #endregion
    }
}