﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Country", Namespace = "http://www.tps360.com/types")]
    public class Country : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string CountryCode
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public Country()
            : base()
        {
        }

        #endregion
    }
}