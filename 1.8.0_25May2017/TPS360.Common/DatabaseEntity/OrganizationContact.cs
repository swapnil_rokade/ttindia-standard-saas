﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "OrganizationContact", Namespace = "http://www.tps360.com/types")]
    public class OrganizationContact : BaseEntity
    {
        #region Properties

        [DataMember]
        public int OrganizationBranchOfficeId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsPrimaryContact
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public OrganizationContact()
            : base()
        {
        }

        #endregion
    }
}