﻿using System;
using System.Runtime.Serialization;
using TPS360.Common.Shared;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CustomSiteMap", Namespace = "http://www.tps360.com/types")]
    public class CustomSiteMap : BaseEntity
    {
        #region Properties

        private CustomSiteMap _parentMenu;

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public string Url
        {
            get;
            set;
        }

        [DataMember]
        public string ImageUrl
        {
            get;
            set;
        }

        [DataMember]
        public int SortOrder
        {
            get;
            set;
        }

        [DataMember]
        public bool IsVirtual
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public string Roles
        {
            get;
            set;
        }

        [DataMember]
        public CustomSiteMap ParentMenu
        {
            get
            {
                if (_parentMenu == null)
                {
                    _parentMenu = new CustomSiteMap();
                }

                return _parentMenu;
            }
            set
            {
                _parentMenu = value;
            }
        }

        [DataMember]
        public int ParentId
        {
            get;
            set;
        }
        
        [DataMember]
        public SiteMapType SiteMapType
        {
            get;
            set;
        }

        [DataMember]
        public string[] RolesArray
        {
            get
            {
                if (!string.IsNullOrEmpty(Roles))
                {
                    return this.Roles.Split(new char[] { ',', ';', '|' }).Clone() as string[];
                }

                return null;
            }
        }

        [DataMember]
        public bool IsDefault
        {
            get;
            set;
        }

        [DataMember]
        public bool HasCount
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public CustomSiteMap()
            : base()
        {
        }

        #endregion
    }
}