﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CampaignNote", Namespace = "http://www.tps360.com/types")]
    public class CampaignNote : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CampaignId
        {
            get;
            set;
        }

        [DataMember]
        public int CommonNoteId
        {
            get;
            set;
        }

        private CommonNote _commonNote;

        [DataMember]
        public CommonNote CommonNote
        {
            get
            {
                if (_commonNote == null)
                {
                    _commonNote = new CommonNote();
                }

                return _commonNote;
            }
            set
            {
                _commonNote = value;
            }
        }

        #endregion

        #region Constructor

        public CampaignNote()
            : base()
        {
        }

        #endregion
    }
}