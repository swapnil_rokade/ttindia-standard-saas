﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberJobCartAlert", Namespace = "http://www.tps360.com/types")]
    public class MemberJobCartAlert : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberJobCartAlertSetupId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberJobCartAlert()
            : base()
        {
        }

        #endregion
    }
}