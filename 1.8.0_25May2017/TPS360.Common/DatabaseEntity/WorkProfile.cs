﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "WorkProfile", Namespace = "http://www.tps360.com/types")]
    public class WorkProfile : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Address1
        {
            get;
            set;
        }

        [DataMember]
        public string Address2
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public string ZipCode
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        [DataMember]
        public int StateId
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhone
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhoneExt
        {
            get;
            set;
        }

        [DataMember]
        public string OfficeMobile
        {
            get;
            set;
        }

        [DataMember]
        public string OfficeFax
        {
            get;
            set;
        }

        [DataMember]
        public string Email
        {
            get;
            set;
        }

        [DataMember]
        public int FunctionalJobCategoryId
        {
            get;
            set;
        }

        [DataMember]
        public int FunctionalJobTitleId
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string DailyWorkHours
        {
            get;
            set;
        }

        [DataMember]
        public string WeeklyWorkHours
        {
            get;
            set;
        }

        [DataMember]
        public string MonthlyWorkHours
        {
            get;
            set;
        }

        [DataMember]
        public int DailyWorkScheduleLookUpId
        {
            get;
            set;
        }

        [DataMember]
        public int WeekLyWorkSchedulelookupId
        {
            get;
            set;
        }

        [DataMember]
        public string SupervVisorName
        {
            get;
            set;
        }

        [DataMember]
        public string SuperVisorDesignation
        {
            get;
            set;
        }

        [DataMember]
        public string JobDescription
        {
            get;
            set;
        }

        [DataMember]
        public int OfficeId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public WorkProfile()
            : base()
        {
        }

        #endregion
    }
}
