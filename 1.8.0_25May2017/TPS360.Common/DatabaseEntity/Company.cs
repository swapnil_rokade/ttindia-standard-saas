﻿using System;
using System.Runtime.Serialization;
using TPS360.Common.Shared;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Company", Namespace = "http://www.tps360.com/types")]
    public class Company : BaseEntity
    {
        #region Properties

        private CompanyContact _primaryContact;

        [DataMember]
        public string CompanyName
        {
            get;
            set;
        }

        [DataMember]
        public string Address1
        {
            get;
            set;
        }

        [DataMember]
        public string Address2
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public int StateId
        {
            get;
            set;
        }

        [DataMember]
        public string Zip
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        [DataMember]
        public string WebAddress
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyLogo
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyInformation
        {
            get;
            set;
        }

        [DataMember]
        public int IndustryType
        {
            get;
            set;
        }

        [DataMember]
        public int CompanySize
        {
            get;
            set;
        }

        [DataMember]
        public int OwnerType
        {
            get;
            set;
        }

        [DataMember]
        public string TickerSymbol
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhone
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public string TollFreePhone
        {
            get;
            set;
        }

        [DataMember]
        public string TollFreePhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public string FaxNumber
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryEmail
        {
            get;
            set;
        }

        [DataMember]
        public string SecondaryEmail
        {
            get;
            set;
        }

        [DataMember]
        public string EINNumber
        {
            get;
            set;
        }

        [DataMember]
        public string AnnualRevenue
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfEmployee
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyType
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public bool IsEndClient
        {
            get;
            set;
        }

        [DataMember]
        public bool IsTier1Client
        {
            get;
            set;
        }

        [DataMember]
        public bool IsTier2Client
        {
            get;
            set;
        }

        [DataMember]
        public CompanyStatus CompanyStatus
        {
            get;
            set;
        }

        [DataMember]
        public CompanyContact PrimaryContact
        {
            get
            {
                if (_primaryContact == null)
                {
                    _primaryContact = new CompanyContact();
                }

                return _primaryContact;
            }
            set
            {
                _primaryContact = value;
            }
        }

        [DataMember]
        public bool IsPending
        {
            get;
            set;
        }

        public bool IsExternalRegistration
        {
            get;
            set;
        }

        public bool IsVolumeHireClient
        {
            get;
            set;
        }


        [DataMember]
        public DateTime ContractStartDate
        {
            get;
            set;
        }


        [DataMember]
        public DateTime ContractEndDate
        {
            get;
            set;
        }

        [DataMember]
        public decimal ServiceFee
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public Company()
            : base()
        {
        }

        #endregion
    }
}