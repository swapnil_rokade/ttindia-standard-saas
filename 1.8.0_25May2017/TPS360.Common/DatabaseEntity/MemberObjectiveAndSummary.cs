﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberObjectiveAndSummary", Namespace = "http://www.tps360.com/types")]
    public class MemberObjectiveAndSummary : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Objective
        {
            get;
            set;
        }

        [DataMember]
        public string Summary
        {
            get;
            set;
        }

        [DataMember]
        public string CopyPasteResume
        {
            get;
            set;
        }
        
        [DataMember]
        public string RawCopyPasteResume
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public string ResumeHighlight
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public MemberObjectiveAndSummary()
            : base()
        {
        }

        #endregion
    }
}