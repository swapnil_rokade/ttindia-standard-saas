﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "SearchAgentEmailTemplate", Namespace = "http://www.tps360.com/types")]
    public class SearchAgentEmailTemplate : BaseEntity
    {
        #region Properties

       [DataMember]
        public int SenderId
        {
            get;
            set;
        }

    
        [DataMember]
        public int JobPostingID
        {
            get;
            set;
        }

        [DataMember]
        public string Subject
        {
            get;
            set;
        }

        [DataMember]
        public string EmailBody
        {
            get;
            set;
        }


        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }
      
       
      
        #endregion

        #region Constructor

        public SearchAgentEmailTemplate()
            : base()
        {
        }

        #endregion
    }
}