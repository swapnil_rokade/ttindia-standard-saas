﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: JobPosting.cs
    Description: This page is used for Job posting functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Nov-13-2008           Yogeesh Bhat           Defect ID:9180; Added new data member IsExpensesPaid
    0.2            Mar-06-2009           Yogeesh Bhat           Defect Id: 10049; Added new data member RequisitionSource and EmailSubject
    0.3            22/May/2015           Prasanth Kumar G       Introduced TotalTimeTaken For Requisition Aging Report
 *  0.4            2/Feb/2015            Pravin khot            Introduced by IList<GenericLookup_Levels>GenericLookupLevels,GenericLookup_Levels
    0.5            20/May/2016           Sumit Sonawane         Introduced some new data members
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "JobPosting", Namespace = "http://www.tps360.com/types")]
    public class JobPosting : BaseEntity
    {
        #region Properties

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string JobPostingCode
        {
            get;
            set;
        }

        [DataMember]
        public string ClientJobId
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfOpenings
        {
            get;
            set;
        }

        [DataMember]
        public string PayRate
        {
            get;
            set;
        }

        [DataMember]
        public int PayRateCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string PayCycle
        {
            get;
            set;
        }

        [DataMember]
        public bool TravelRequired
        {
            get;
            set;
        }

        [DataMember]
        public string TravelRequiredPercent
        {
            get;
            set;
        }

        [DataMember]
        public string OtherBenefits
        {
            get;
            set;
        }

        [DataMember]
        public int JobStatus
        {
            get;
            set;
        }

        [DataMember]
        public string  JobDurationLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string JobDurationMonth
        {
            get;
            set;
        }

        [DataMember]
        public string JobAddress1
        {
            get;
            set;
        }

        [DataMember]
        public string JobAddress2
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public string ZipCode
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        [DataMember]
        public int StateId
        {
            get;
            set;
        }

        [DataMember]
        public string StartDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime FinalHiredDate
        {
            get;
            set;
        }

        [DataMember]
        public string JobDescription
        {
            get;
            set;
        }

        [DataMember]
        public string AuthorizationTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime PostedDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ActivationDate
        {
            get;
            set;
        }

        [DataMember]
        public string  RequiredDegreeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string MinExpRequired
        {
            get;
            set;
        }

        [DataMember]
        public string MaxExpRequired
        {
            get;
            set;
        }

       
        [DataMember]
        public bool IsJobActive
        {
            get;
            set;
        }
     
        [DataMember]
        public int JobType
        {
            get;
            set;
        }
        [DataMember]
        public int NoofSubmissions
        {
            get;
            set;
        }
        [DataMember]
        public int WIP
        {
            get;
            set;
        }
        [DataMember]
        public int Rejected
        {
            get;
            set;
        }
        [DataMember]
        public int Joined
        {
            get;
            set;
        }
        [DataMember]
        public bool IsApprovalRequired
        {
            get;
            set;
        }

        [DataMember]
        public string InternalNote
        {
            get;
            set;
        }

        [DataMember]
        public int ClientId
        {
            get;
            set;
        }

      

      

        [DataMember]
        public string ClientHourlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ClientHourlyRateCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string ClientRatePayCycle
        {
            get;
            set;
        }


      

        [DataMember]
        public string TaxTermLookupIds
        {
            get;
            set;
        }

     

   

        [DataMember]
        public decimal ExpectedRevenue
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedRevenueCurrencyLookupId
        {
            get;
            set;
        }

    

        [DataMember]
        public bool WorkflowApproved
        {
            get;
            set;
        }

      

        [DataMember]
        public bool TeleCommunication
        {
            get;
            set;
        }

        [DataMember]
        public bool IsTemplate
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }
        [DataMember]
        public string RawDescription
        {
            get;
            set;
        }
        [DataMember]
        public int JobDepartmentLookUpId
        {
            get;
            set;
        }
        [DataMember]
        public string JobSkillLookUpId
        {
            get;
            set;
        }
        //0.1 starts here
        [DataMember]
        public bool IsExpensesPaid
        {
            get;
            set;
        }

        [DataMember]
        public int OccuptionalSeriesLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int PayGradeLookupId
        {
            get;
            set;
        }
      
        [DataMember]
        public int WorkScheduleLookupId
        {
            get;
            set;
        }


        [DataMember]
        public bool SecurityClearance
        {
            get;
            set;
        }

        [DataMember]
        public int  ClientContactId
        {
            get;
            set;
        }

        [DataMember]
        public int JobCategoryLookupId
        {
            get;
            set;
        }





        [DataMember]
        public System .Collections .Generic .IList <Hiring_Levels> HiringMatrixLevels
        {
            get;
            set;
        }

        //*******Code introduced by Pravin khot on 2/Feb/2015 Start*************
        [DataMember]
        public System.Collections.Generic.IList<GenericLookup_Levels> GenericLookupLevels
        {
            get;
            set;
        }
        //*************************END********************************************


        [DataMember]
        public string ReportingTo
        {
            get;
            set;
        }
        [DataMember]
        public int NoOfReportees
        {
            get;
            set;
        }
        [DataMember]
        public string  MinimumQualifyingParameters
        {
            get;
            set;
        }

        [DataMember]
        public decimal  MaxPayRate
        {
            get;
            set;
        }

        [DataMember]
        public bool AllowRecruitersToChangeStatus
        {
            get;
            set;
        }
        //0.1 ends here

        [DataMember]
        public string ClientBrief
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfAssignedManagers
        {
            get;
            set;
        }

        [DataMember]
        public int TurnArroundTime
        {
            get;
            set;
        }

        [DataMember]
        public int InterViewCount
        {
            get;
            set;
        }
        [DataMember]
        public int JoiningDetailsCount
        {
            get;
            set;
        }
        [DataMember]
        public int OfferDetailsCount
        {
            get;
            set;
        }
        [DataMember]
        public int SubmissionCount
        {
            get;
            set;
        }
        [DataMember]
        public string InterviewCandidateName
        {
            get;
            set;
        }
        [DataMember]
        public string SubmittedCandidateName
        {
            get;
            set;
        }
        [DataMember]
        public string OfferedCandidateName
        {
            get;
            set;
        }
        [DataMember]
        public string JoinedCandidateName
        {
            get;
            set;
        }
        
        [DataMember]
        public string JobPostNotes
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfUnfilledOpenings
        {
            get;
            set;
        }


        [DataMember]
        public string RequisitionSource
        {
            get;
            set;
        }

        [DataMember]
        public string EmailSubject
        {
            get;
            set;
        }
        [DataMember]
        public bool ShowInCandidatePortal
        {
            get;
            set;

        }

        [DataMember]
        public bool ShowInEmployeeReferralPortal
        {
            get;
            set;

        }


        public bool  IsApplied
        {
            get;
            set;
        }
        public bool DisplayRequisitionInVendorPortal
        {
            get;
            set;
        }

        public string CreatorName
        {
            get;
            set;
        }
        [DataMember]
        public DateTime OpenDate
        {
            get;
            set;
        }
        [DataMember]
        public int SalesRegionLookUpId
        {
            get;
            set;
        }
        [DataMember]
        public int SalesGroupLookUpId
        {
            get;
            set;
        }
        [DataMember]
        public string CustomerName
        {
            get;
            set;
        }
        [DataMember]
        public int POAvailability
        {
            get;
            set;
        }
        public int JobLocationLookUpID
        {
            get;
            set;
        }
        public int EmployementTypeLookUpID
        {
            get;
            set;

        }

        public string JobLocationText
        {
            get;
            set;
        }

        public string SalesRegionText
        {
            get;
            set;
        }

        public string SalesGroupText
        {
            get;
            set;
        }

        public string BUContactText
        {
            get;
            set;
        }

        public string JobCategoryText
        {
            get;
            set;
        }
        public string VendorList
        {
            get;
            set;
        }
        public string RequisitionType
        {
            get;
            set;
        }
        //Code introduced by Prasanth on 22/May/2015 Start
        public int TotalTimeTaken
        {
            get;
            set;
        }
        //****************END*********************


        public string RequestionBranch
        {
            get;
            set;
        }
        public string RequestionGrade
        {
            get;
            set;
        }

        public string onnoticeperiod
        {
            get;
            set;
        }

        public string Filled
        {
            get;
            set;
        }

        public int Available
        {
            get;
            set;
        }
        //Code introduced by Pravin khot on 18/Jan/2016 Start
        public int MemberId
        {
            get;
            set;
        }
        [DataMember]
        public int CareerJobId
        {
            get;
            set;
        }
        //****************END*********************
        /////////////Code Added by Sumit Sonawane on 20/May/2016//////////0.5///////////////
        public int newEmploymentlookUpId
        {
            get;
            set;
        }
        public int RequestTypeLookupId
        {
            get;
            set;
        }
        public string CurrentStage
        {
            get;
            set;
        }
        public DateTime RevisedFulfillmentDate
        {
            get;
            set;
        }
        [DataMember]
        public DateTime TAAssinedDate
        {
            get;
            set;
        }
        [DataMember]
        public string NewPayRate
        {
            get;
            set;
        }
        [DataMember]
        public DateTime RMGAssinedDate
        {
            get;
            set;
        }
        [DataMember]
        public string PendingDate
        {
            get;
            set;
        }
        [DataMember]
        public string WorkTime
        {
            get;
            set;
        }

        [DataMember]
        public bool SendNotificationMail
        {
            get;
            set;
        }
        [DataMember]
        public string JobSecSkillLookUpId
        {
            get;
            set;
        }
        public int ToolLookupId
        {
            get;
            set;
        }
        public int FunctionalLookupId
        {
            get;
            set;
        }
        public int DomainLookupId
        {
            get;
            set;
        }
        public int EngineeringGroupLookupId
        {
            get;
            set;
        }
        [DataMember]
        public int ToolSkillId
        {
            get;
            set;
        }
        [DataMember]
        public int FunctionSkillId
        {
            get;
            set;
        }
        public string Remarks
        {
            get;
            set;
        }
        public string OpportunityID
        {
            get;
            set;
        }
        public string OpportunityName
        {
            get;
            set;
        }
        public string SalesContact
        {
            get;
            set;
        }
        public int CloneCount
        {
            get;
            set;
        }
        public string VendorContactList
        {
            get;
            set;
        }

        [DataMember]
        public int CustomerNameLookUpId
        {
            get;
            set;
        }
        [DataMember]
        public int ProjectDurationLookUpId
        {
            get;
            set;
        }
        [DataMember]
        public int DomainSkillId
        {
            get;
            set;
        }
        public DateTime PETAssinedDate
        {
            get;
            set;
        }
        public int IndustryGroupId
        {
            get;
            set;
        }

        //****************END**SUMIT*******************

        #endregion

        #region Constructor

        public JobPosting()
            : base()
        {
        }

     

        #endregion
    }
    public struct Hiring_Levels
    {
        public int HiringMatrixLevelID;
        public int CandidateCount;

    }
    //*******Code introduced by Pravin khot on 2/Feb/2015 Start*************
    public struct GenericLookup_Levels
    {
        public int GenericLookupID;
        public int GenericLookupCount;

    }
    //*************END***************************
}