﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyAssignedManager", Namespace = "http://www.tps360.com/types")]
    public class CompanyAssignedManager : BaseEntity
    {
        #region Properties        

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AssignDate
        {
            get;
            set;
        }

        [DataMember]
        public bool IsPrimaryManager
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CompanyAssignedManager()
            : base()
        {
        }

        #endregion
    }
}