using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "JobPostingAssessment", Namespace = "http://www.tps360.com/types")]
    public class JobPostingAssessment : BaseEntity
    {
        #region Properties

        [DataMember]
        public int TestMasterId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public JobPostingAssessment()
            : base()
        {
        }

        #endregion
    }
}