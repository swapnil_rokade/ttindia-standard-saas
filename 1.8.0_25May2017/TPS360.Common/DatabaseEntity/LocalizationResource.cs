﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "LocalizationResource", Namespace = "http://www.delphi360.com/types")]
    public class LocalizationResource : BaseEntity
    {
        #region Properties

        [DataMember]
        public string ResourceGroup
        {
            get;
            set;
        }

        [DataMember]
        public string CultureCode
        {
            get;
            set;
        }

        [DataMember]
        public string ResourceName
        {
            get;
            set;
        }

        [DataMember]
        public string ResourceValue
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public LocalizationResource()
            : base()
        {
        }

        #endregion
    }
}