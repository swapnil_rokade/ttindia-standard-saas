﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ProjectTeam", Namespace = "http://www.tps360.com/types")]
    public class ProjectTeam : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ProjectId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public string MemberType
        {
            get;
            set;
        }        

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public ProjectTeam()
            : base()
        {
        }

        #endregion
    }
}