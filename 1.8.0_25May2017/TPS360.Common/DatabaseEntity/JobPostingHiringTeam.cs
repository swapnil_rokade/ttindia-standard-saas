﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "JobPostingHiringTeam", Namespace = "http://www.tps360.com/types")]
    public class JobPostingHiringTeam : BaseEntity
    {
        #region Properties

        [DataMember]
        public string EmployeeType
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryEmail
        {
            get;
            set;
        }

        [DataMember]
        public string NamePrimaryEmail
        {
            get;
            set;
        }
        //**************Added by pravin khot on 2/March/2017
        [DataMember]
        public int RecruiterGroupId
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfOenings
        {
            get;
            set;
        }
        [DataMember]
        public string RecruiterGroupName
        {
            get;
            set;
        }
        [DataMember]
        public bool IsPRIMARY
        {
            get;
            set;
        }
        [DataMember]
        public string NoOfOening
        {
            get;
            set;
        }
        //**********END************************
        #endregion

        #region Constructor

        public JobPostingHiringTeam()
            : base()
        {
        }

        #endregion
    }
}