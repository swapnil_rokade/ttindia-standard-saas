﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberJbApplied", Namespace = "http://www.tps360.com/types")]
    public class MemberJobApplied : BaseEntity
    {
        #region Properties

    

        [DataMember]
        public int MemberId 
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public DateTime   JobPostedDate
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string JobCity
        {
            get;
            set;
        }

        [DataMember]
        public string JobState
        {
            get;
            set;
        }



        #endregion

        #region Constructor

        public MemberJobApplied()
            : base()
        {
        }

        #endregion
    }
}