﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberH1B1", Namespace = "http://www.tps360.com/types")]
    public class MemberH1B1 : BaseEntity
    {
        #region Properties

        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        public string MiddleName
        {
            get;
            set;
        }

        [DataMember]
        public string AttentionTo
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentAddress1
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentAddress2
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public int StateId
        {
            get;
            set;
        }

        [DataMember]
        public int CountryId
        {
            get;
            set;
        }

        [DataMember]
        public string ZipCode
        {
            get;
            set;
        }

        [DataMember]
        public string ProvinceOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public int BirthCountryId
        {
            get;
            set;
        }

        [DataMember]
        public int CitizenshipCountryId
        {
            get;
            set;
        }

        [DataMember]
        public string OtherName1
        {
            get;
            set;
        }

        [DataMember]
        public string OtherName2
        {
            get;
            set;
        }

        [DataMember]
        public string OtherName3
        {
            get;
            set;
        }

        [DataMember]
        public int HighestLevelOfEducation
        {
            get;
            set;
        }

        [DataMember]
        public string MajorFieldOfStudy
        {
            get;
            set;
        }

        [DataMember]
        public bool HasEarnedMastersOrHigherDegree
        {
            get;
            set;
        }

        [DataMember]
        public string HigherEducationInstituteName
        {
            get;
            set;
        }

        [DataMember]
        public string HigherEducationInstituteAddress
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DegreeAwardDate
        {
            get;
            set;
        }

        [DataMember]
        public string TypeOfUSDegree
        {
            get;
            set;
        }

        [DataMember]
        public string USSocialSecurityNumber
        {
            get;
            set;
        }

        [DataMember]
        public string USANumber
        {
            get;
            set;
        }

        [DataMember]
        public string I94NumberArrivalDocument
        {
            get;
            set;
        }

        [DataMember]
        public string I94ArrivalDocument
        {
            get;
            set;
        }

        [DataMember]
        public string PreviousReceiptNumber
        {
            get;
            set;
        }

        [DataMember]
        public DateTime LastArrivalDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime StatusExpirationDate
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentNonimmigrantStatus
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentUSAddress
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string WorkAreaAddress
        {
            get;
            set;
        }

        [DataMember]
        public string NonTechnicalJobDescription
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EmploymentFrom
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EmploymentTo
        {
            get;
            set;
        }

        [DataMember]
        public bool IsFulltimePosition
        {
            get;
            set;
        }

        [DataMember]
        public decimal WagesPerWeek
        {
            get;
            set;
        }

        [DataMember]
        public decimal WatesPerYear
        {
            get;
            set;
        }

        [DataMember]
        public decimal WagesPerHour
        {
            get;
            set;
        }

        [DataMember]
        public string OtherCompensation
        {
            get;
            set;
        }

        [DataMember]
        public decimal LCAOfferedPerYearRate
        {
            get;
            set;
        }

        [DataMember]
        public string LCACode
        {
            get;
            set;
        }

        [DataMember]
        public string NAICSCode
        {
            get;
            set;
        }

        [DataMember]
        public string RequestedNonimmigrantClass
        {
            get;
            set;
        }

        [DataMember]
        public string PetitionReceiptNumber
        {
            get;
            set;
        }

        [DataMember]
        public int BasisForClassification
        {
            get;
            set;
        }

        [DataMember]
        public string PriorPetitionReceiptNumber
        {
            get;
            set;
        }

        [DataMember]
        public int TotalWorkersInPetition
        {
            get;
            set;
        }

        [DataMember]
        public int RequestedAction
        {
            get;
            set;
        }

        [DataMember]
        public int ConsulateOfficeType
        {
            get;
            set;
        }

        [DataMember]
        public string ConsulateOfficeAddress
        {
            get;
            set;
        }

        [DataMember]
        public string ConsulateOfficeCity
        {
            get;
            set;
        }

        [DataMember]
        public string USStateForeignCountry
        {
            get;
            set;
        }

        [DataMember]
        public string ConsulatePersonForeignAddress
        {
            get;
            set;
        }

        [DataMember]
        public bool EveryOneHaveValidPassport
        {
            get;
            set;
        }

        [DataMember]
        public bool FilingAnyOtherPetitionWithThis
        {
            get;
            set;
        }

        [DataMember]
        public bool AreApplicationsForReplacementFiled
        {
            get;
            set;
        }

        [DataMember]
        public bool AreApplicationByDependentsFiled
        {
            get;
            set;
        }

        [DataMember]
        public bool AnyPersonInRemovalProceedings
        {
            get;
            set;
        }

        [DataMember]
        public bool HaveEverFieldAnImmigrationPetitionForAnyPerson
        {
            get;
            set;
        }

        [DataMember]
        public bool EverBeenGivenRequestingClassification
        {
            get;
            set;
        }

        [DataMember]
        public bool EverBeenDeniedRequestingClassification
        {
            get;
            set;
        }

        [DataMember]
        public bool EverPreviouslyFiledPetitionForThisPerson
        {
            get;
            set;
        }

        [DataMember]
        public bool AnyPersonNotBeenWithTheGroupForAtleastOneYear
        {
            get;
            set;
        }

        [DataMember]
        public int ClassificationSought
        {
            get;
            set;
        }

        [DataMember]
        public string ProposedDutyDescription
        {
            get;
            set;
        }

        [DataMember]
        public string PresentOccupationAndPriorExperienceSummary
        {
            get;
            set;
        }

        [DataMember]
        public bool IsPetitionerDependentEmployer
        {
            get;
            set;
        }

        [DataMember]
        public bool HasPetitionerEverBeenFoundToBeAWillfulViolator
        {
            get;
            set;
        }

        [DataMember]
        public bool IsBeneficiaryAnExemptH1BNonimmigrant
        {
            get;
            set;
        }

        [DataMember]
        public bool IsBeneficiarysAnnualRateAtleast60K
        {
            get;
            set;
        }

        [DataMember]
        public bool IsBeneficiaryHasMastersOrHigherDegree
        {
            get;
            set;
        }

        [DataMember]
        public bool IsHigherEducationInstitutionPartB
        {
            get;
            set;
        }

        [DataMember]
        public bool IsNonprofitOrganizationPartB
        {
            get;
            set;
        }

        [DataMember]
        public bool IsNonProfitResearchOrganizationPartB
        {
            get;
            set;
        }

        [DataMember]
        public bool IsSecondOrSubsequentRequestPartB
        {
            get;
            set;
        }

        [DataMember]
        public bool IsNotForAnAmendmentForExtensionPetitionPartB
        {
            get;
            set;
        }

        [DataMember]
        public bool IsForCorrectionOfAUSCISErrorPartB
        {
            get;
            set;
        }

        [DataMember]
        public bool IsPrimaryOrSecondaryInstitutionPartB
        {
            get;
            set;
        }

        [DataMember]
        public bool IsNonProfitClinicalTrainingOrganizationPartB
        {
            get;
            set;
        }

        [DataMember]
        public bool HasCurrentlyEmployedNoMoreThan25FullTimeInUSPartB
        {
            get;
            set;
        }

        [DataMember]
        public bool IsHigherEducationInstitutionPartC
        {
            get;
            set;
        }

        [DataMember]
        public bool IsNonprofitOrganizationPartC
        {
            get;
            set;
        }

        [DataMember]
        public bool IsNonProfitResearchOrganizationPartC
        {
            get;
            set;
        }

        [DataMember]
        public bool IsBeneficiaryAJ1NonimmigrantPartC
        {
            get;
            set;
        }

        [DataMember]
        public bool HasGrantedH1BInPast6YearsPartC
        {
            get;
            set;
        }

        [DataMember]
        public bool IsPetitionForRequestToChangeEmployeerPartC
        {
            get;
            set;
        }

        [DataMember]
        public bool HasBeneficiaryEarnedMastersOrHigherFromUSPartC
        {
            get;
            set;
        }

        [DataMember]
        public bool HasfiledPetitionEligibleForPremiumProcessing
        {
            get;
            set;
        }

        [DataMember]
        public bool AreAttorneyEligibleForPremiumProcessing
        {
            get;
            set;
        }

        [DataMember]
        public bool AreApplicantEligibleForPremiumProcessing
        {
            get;
            set;
        }

        [DataMember]
        public bool AreAttorneyOfApplicationEligibleForPremiumProcessing
        {
            get;
            set;
        }

        [DataMember]
        public string FormNumberOfRelatedPetition
        {
            get;
            set;
        }

        [DataMember]
        public string ReceiptNumberOfRelatedPetition
        {
            get;
            set;
        }

        [DataMember]
        public string ClassificationTypeBeingRequested
        {
            get;
            set;
        }

        [DataMember]
        public string PetitionerInTheRelatingCase
        {
            get;
            set;
        }

        [DataMember]
        public string BeneficiaryInTheRelatingCase
        {
            get;
            set;
        }

        [DataMember]
        public int H1BEmployerId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberH1B1()
            : base()
        {
        }

        #endregion
    }
}