﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   OnLineParser.cs
    Description         :   This page is used to Create Properties for InterviewPanel.
    Created By          :   Pravin
    Created On          :   30/Aug/2016
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "OnLineParser", Namespace = "http://www.tps360.com/types")]
    public class OnLineParser : BaseEntity
    {
        #region Properties

        public int OnLineParser_Id
        {
            get;
            set;
        }

        public int InterviewPanelType_Id
        {
            get;
            set;
        }
        public int Interviewer_Id
        {
            get;
            set;
        }

        public string Interviewer_Name    
        {
            get;
            set;
        }       
        public int Status    
        {
            get;
            set;
        }
        public string Title  
        {
            get;
            set;
        }
        public string Path  
        {
            get;
            set;
        }
        public string XMLFilePath
        {
            get;
            set;
        }
        public int ParseStatus
        {
            get;
            set;
        }
        public int DatabaseStatus
        {
            get;
            set;
        }
        public int MemebrId
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public OnLineParser()
            : base()
        {
        }

        #endregion
    }
}