﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "OrganizationFunctionalityCategoryMap", Namespace = "http://www.tps360.com/types")]
    public class OrganizationFunctionalityCategoryMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int OrganizationBranchOfficeId
        {
            get;
            set;
        }

        [DataMember]
        public int FunctionalityCategoryLookupId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public OrganizationFunctionalityCategoryMap()
            : base()
        {
        }

        #endregion
    }
}