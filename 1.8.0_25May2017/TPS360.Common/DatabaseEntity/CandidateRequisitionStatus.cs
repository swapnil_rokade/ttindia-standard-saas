﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CandidateRequisitionStatus", Namespace = "http://www.tps360.com/types")]
    public  class CandidateRequisitionStatus: BaseEntity
    {
        #region Properties
        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }
        [DataMember]
        public int MemberId
        {
            get;
            set;
        }
        [DataMember]
        public int StatusId
        {
            get;
            set;
        }
        [DataMember]
        public bool  IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public string CandidateName
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }
        [DataMember]
        public string LevelName
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AddedToRequisitionDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime RejectedDate
        {
            get;
            set;
        }
        [DataMember]
        public string  UsersName
        {
            get;
            set;
        }

        
        #endregion

        #region Constructor

        public CandidateRequisitionStatus()
            : base()
        {
        }

        #endregion
    }
}
