﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "OrganizationFunctionalityCategoryPositionMap", Namespace = "http://www.tps360.com/types")]
    public class OrganizationFunctionalityCategoryPositionMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberOrganizationFunctionalityCategoryMapId
        {
            get;
            set;
        }

        [DataMember]
        public int PositionId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public OrganizationFunctionalityCategoryPositionMap()
            : base()
        {
        }

        #endregion
    }
}