﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ReferenceQueryReferenceCheckMap", Namespace = "http://www.tps360.com/types")]
    public class ReferenceQueryReferenceCheckMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Response
        {
            get;
            set;
        }

        [DataMember]
        public int ReferenceQueryId
        {
            get;
            set;
        }

        [DataMember]
        public int ReferenceCheckId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public ReferenceQueryReferenceCheckMap()
            : base()
        {
        }

        #endregion
    }
}