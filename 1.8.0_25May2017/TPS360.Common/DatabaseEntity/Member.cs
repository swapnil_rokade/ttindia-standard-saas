﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Member.cs
    Description: This page is used for member
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Dec-02-2008        Yogeesh Bhat        Defect ID:8836; added new parameter ConsultantSource and Vendor
    0.2            10/Jun/2016        Prasanth Kumar G    Introduced Username,IsLDAP Property.
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Member", Namespace = "http://www.tps360.com/types")]
    public class Member : BaseEntity
    {
        #region Properties

        [DataMember]
        public string MemberCode
        {
            get;
            set;
        }
        //Code introduced by Prasanth on 10/Jun/2016 Start
        [DataMember]
        public string UserName
        {
            get;
            set;
        }

        //*******************END************************
        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string MiddleName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        public string NickName
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DateOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentAddressLine1
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentAddressLine2
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentCity
        {
            get;
            set;
        }

        [DataMember]
        public int PermanentStateId
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentZip
        {
            get;
            set;
        }

        [DataMember]
        public int PermanentCountryId
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentPhone
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentPhoneExt
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentMobile
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryEmail
        {
            get;
            set;
        }

        [DataMember]
        public string AlternateEmail
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryPhone
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryPhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public string CellPhone
        {
            get;
            set;
        }

        [DataMember]
        public bool AutomatedEmailStatus
        {
            get;
            set;
        }

        [DataMember]
        public int ResumeSource
        {
            get;
            set;
        }

        [DataMember]
        public int ResumeSharing
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public Guid UserId
        {
            get;
            set;
        }

        public int Vendor
        {
            get;
            set;
        }

        public int ConsultantSource
        {
            get;
            set;
        }
        //12373
        public int MemberType
        {
            get;
            set;
        }
        public string CurrentCTC
        {
            get;
            set;
        }

        public string ExpectedCTC
        {
            get;
            set;
        }

        public string CurrentEmployer
        {
            get;
            set;
        }

        public string Avaliability
        {
            get;
            set;
        }

        public string Skills
        {
            get;
            set;
        }

        public string ExpYears
        {
            get;
            set;
        }

        public string Remarks
        {
            get;
            set;
        }
        //Code introduced by Prasanth on 10/Jun/2016 Start
        [DataMember]
        public bool IsLDAP
        {
            get;
            set;
        }
        //*******************End**************************
        #endregion

        #region Constructor

        public Member()
            : base()
        {
        }

        #endregion
    }
}