﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberGeneralQuestion", Namespace = "http://www.tps360.com/types")]
    public class MemberGeneralQuestion : BaseEntity
    {
        #region Properties

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingGeneralQuestionId
        {
            get;
            set;
        }

        [DataMember]
        public string Answer
        {
            get;
            set;
        }

        [DataMember]
        public decimal FirstInterviewScore
        {
            get;
            set;
        }

        [DataMember]
        public decimal SecondInterviewScore
        {
            get;
            set;
        }

        [DataMember]
        public decimal ThirdInterviewScore
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberGeneralQuestion()
            : base()
        {
        }

        #endregion
    }
}