﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberEmailAttachment", Namespace = "http://www.tps360.com/types")]
    public class MemberEmailAttachment : BaseEntity
    {
        #region Properties

        [DataMember]
        public string AttachmentFile
        {
            get;
            set;
        }

        [DataMember]
        public int AttachmentSize
        {
            get;
            set;
        }

        [DataMember]
        public int MemberEmailId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberEmailAttachment()
            : base()
        {
        }

        #endregion
    }
}