﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Position", Namespace = "http://www.tps360.com/types")]
    public class Position : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int ParentId
        {
            get;
            set;
        }
        
        [DataMember]
        public string SalaryRange
        {
            get;
            set;
        }

        [DataMember]
        public int SalaryRating
        {
            get;
            set;
        }

        [DataMember]
        public int Education
        {
            get;
            set;
        }

        [DataMember]
        public int EducationRating
        {
            get;
            set;
        }

        [DataMember]
        public int PositionTierLookUpId
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public Position()
            : base()
        {
        }

        #endregion
    }
}