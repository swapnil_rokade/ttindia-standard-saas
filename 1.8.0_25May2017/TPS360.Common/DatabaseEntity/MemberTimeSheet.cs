﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberTimeSheet", Namespace = "http://www.tps360.com/types")]
    public class MemberTimeSheet : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int ProjectId
        {
            get;
            set;
        }

        [DataMember]
        public int MemberManagerId
        {
            get;
            set;
        }

        [DataMember]
        public int ManagerId
        {
            get;
            set;
        }

        [DataMember]
        public int ClientId
        {
            get;
            set;
        }

        [DataMember]
        public int Duration
        {
            get;
            set;
        }

        [DataMember]
        public DateTime StartDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndDate
        {
            get;
            set;
        }

        [DataMember]
        public decimal TotalHoursWorked
        {
            get;
            set;
        }

        [DataMember]
        public decimal OverTimeWorked
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public int SalaryStatus
        {
            get;
            set;
        }

        [DataMember]
        public bool IsApproved
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }       


        #endregion

        #region Constructor

        public MemberTimeSheet()
            : base()
        {
        }

        #endregion
    }
}