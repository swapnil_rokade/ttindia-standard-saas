﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ProjectDocument", Namespace = "http://www.tps360.com/types")]
    public class ProjectDocument : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public string FileName
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public int FileTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int ProjectId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public ProjectDocument()
            : base()
        {
        }

        #endregion
    }
}