﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanySkill", Namespace = "http://www.tps360.com/types")]
    public class CompanySkill : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }

        [DataMember]
        public int SkillId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CompanySkill()
            : base()
        {
        }

        #endregion
    }
}