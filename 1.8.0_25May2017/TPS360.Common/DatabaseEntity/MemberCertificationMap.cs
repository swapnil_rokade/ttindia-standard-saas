﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberCertificationMap", Namespace = "http://www.tps360.com/types")]
    public class MemberCertificationMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public string CerttificationName
        {
            get;
            set;
        }

        [DataMember]
        public DateTime    ValidFrom
        {
            get;
            set;
        }

        [DataMember]
        public  DateTime  ValidTo
        {
            get;
            set;
        }

        [DataMember]
        public string IssuingAthority
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

      
        #endregion

        #region Constructor

        public MemberCertificationMap()
            : base()
        {
        }

        #endregion
    }
}