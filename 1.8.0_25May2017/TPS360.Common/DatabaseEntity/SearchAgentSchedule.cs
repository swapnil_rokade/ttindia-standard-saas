﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "SearchAgentSchedule", Namespace = "http://www.tps360.com/types")]
    public class SearchAgentSchedule:BaseEntity 
    {
        #region Properties
        [DataMember]
        public DateTime StartDate
        {
            get;
            set;
        }

        [DataMember]
        public string Repeat
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime NextSendDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime LastSentDate
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }
        
        [DataMember]
        public bool  IsRemoved
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public SearchAgentSchedule()
            : base()
        {
        }

        #endregion
    }
}
