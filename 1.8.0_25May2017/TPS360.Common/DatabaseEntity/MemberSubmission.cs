﻿/*-------------------------------------------------------------------------------------------------------------  
 * FileName: MemberSubmission.cs
   Description: 
   Created By: 
   Created On:
   Modification Log:
   ------------------------------------------------------------------------------------------------------------   
 * Ver.No.             Date                  Author              Modification
   ------------------------------------------------------------------------------------------------------------    
 * 0.1             26-Feb-2009             Sandeesh            Defect Fix - ID: 9671; Added new properties to this class.
 * 0.2             11-Mar-2010             Nagarathna V.B       Enhacement Id:12129;added recevieremail.
 * ---------------------------------------------------------------------------------------------------------------
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberSubmission", Namespace = "http://www.tps360.com/types")]
    public class MemberSubmission : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }
        //12129
        [DataMember]
        public string ReceiverEmail
        {
            get;
            set;
        }
        [DataMember]
        public int MemberEmailDetailId
        {
            get;
            set;
        }

        [DataMember]
        public  DateTime  SubmittedDate
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public MemberSubmission() : base()
        {

        }

        #endregion
    }
}
