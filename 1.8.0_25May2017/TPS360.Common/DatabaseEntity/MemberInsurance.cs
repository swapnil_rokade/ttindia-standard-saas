﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberInsurance", Namespace = "http://www.tps360.com/types")]
    public class MemberInsurance : BaseEntity
    {
        #region Properties

        [DataMember]
        public string InsuranceNo
        {
            get;
            set;
        }

        [DataMember]
        public int InsuranceStatus
        {
            get;
            set;
        }

        [DataMember]
        public decimal Amount
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DateOfPurchase
        {
            get;
            set;
        }

        [DataMember]
        public string PaidBy
        {
            get;
            set;
        }

        [DataMember]
        public string InsuranceDetail
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberInsurance()
            : base()
        {
        }

        #endregion
    }
}