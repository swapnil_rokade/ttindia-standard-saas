﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TrainingCourse", Namespace = "http://www.tps360.com/types")]
    public class TrainingCourse : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Subject
        {
            get;
            set;
        }

        [DataMember]
        public string ShortDescription
        {
            get;
            set;
        }

        [DataMember]
        public string OverallDiscussion
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TrainingCourse()
            : base()
        {
        }

        #endregion
    }
}