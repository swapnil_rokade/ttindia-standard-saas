﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TaskDocument", Namespace = "http://www.tps360.com/types")]
    public class TaskDocument : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Title
        {
            get;
            set;
        }

        [DataMember]
        public string DocumentPath
        {
            get;
            set;
        }

        [DataMember]
        public int CommonTaskId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TaskDocument()
            : base()
        {
        }

        #endregion
    }
}