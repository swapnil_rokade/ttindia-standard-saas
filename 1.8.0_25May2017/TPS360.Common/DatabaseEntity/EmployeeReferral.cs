﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "EmployeeReferral", Namespace = "http://www.tps360.com/types")]
    public class EmployeeReferral : BaseEntity
    {
        #region Properties

        [DataMember]
        public string RefererEmail
        {
            get;
            set;
        }
        [DataMember]
        public string RefererFirstName
        {
            get;
            set;
        }
        [DataMember]
        public string RefererLastName
        {
            get;
            set;
        }
        [DataMember]
        public string EmployeeId
        {
            get;
            set;
        }
        [DataMember]
        public int MemberId
        {
            get;
            set;
        }
        [DataMember]
        public string JobpostingId
        {
            get;
            set;
        }

        [DataMember]
        public string CandidateName
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }
         

       

        #endregion

        #region Constructor

        public EmployeeReferral()
            : base()
        {
        }

        #endregion
    }
}