﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "PositionFunctionalCapabilityMap", Namespace = "http://www.tps360.com/types")]
    public class PositionFunctionalCapabilityMap : BaseEntity
    {
        #region Properties

        [DataMember]
        public int PositionId
        {
            get;
            set;
        }

        [DataMember]
        public int FunctionalCapabilityId
        {
            get;
            set;
        }

        [DataMember]
        public int Rating
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public PositionFunctionalCapabilityMap()
            : base()
        {
        }

        #endregion
    }
}