﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TestQuestion", Namespace = "http://www.tps360.com/types")]
    public class TestQuestion : BaseEntity
    {
        #region Properties

        [DataMember]
        public int QuestionType
        {
            get;
            set;
        }

        [DataMember]
        public string Question
        {
            get;
            set;
        }

        [DataMember]
        public int Score
        {
            get;
            set;
        }

        [DataMember]
        public int DifficultyLevel
        {
            get;
            set;
        }

        [DataMember]
        public bool Active
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int TestMasterId
        {
            get;
            set;
        }       

        [DataMember]
        public int ModuleId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TestQuestion()
            : base()
        {
        }

        #endregion
    }
}