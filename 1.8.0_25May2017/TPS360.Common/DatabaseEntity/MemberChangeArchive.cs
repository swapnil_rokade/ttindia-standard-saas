﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberChangeArchive", Namespace = "http://www.tps360.com/types")]
    public class MemberChangeArchive : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public byte[] ChangedObject
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemove
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberChangeArchive()
            : base()
        {
        }

        #endregion
    }
}