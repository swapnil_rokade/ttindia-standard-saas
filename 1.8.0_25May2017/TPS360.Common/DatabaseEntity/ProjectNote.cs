﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ProjectNote", Namespace = "http://www.tps360.com/types")]
    public class ProjectNote : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ProjectId
        {
            get;
            set;
        }

        [DataMember]
        public int CommonNoteId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public ProjectNote()
            : base()
        {
        }

        #endregion
    }
}