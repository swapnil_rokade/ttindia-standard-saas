﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "AuditTrailActivity", Namespace = "http://www.tps360.com/types")]

    public class AuditTrailActivity : BaseEntity
    {
        #region Properties

        [DataMember]
        public Int64 ID
        {
            get;
            set;
        }

        [DataMember]
        public string Description
        {
            get;
            set;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public Int64 ParentID
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public AuditTrailActivity(): base()
        {

        }
        #endregion
    }
}
