﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberDailyReport", Namespace = "http://www.tps360.com/types")]
    public class MemberDailyReport : BaseEntity
    {
        #region Properties

        [DataMember]
        public DateTime LoginTime
        {
            get;
            set;
        }

        [DataMember]
        public DateTime LogoutTime
        {
            get;
            set;
        }

        [DataMember]
        public string WorkDescription
        {
            get;
            set;
        }

        [DataMember]
        public string ManagementNote
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberDailyReport()
            : base()
        {
        }

        #endregion
    }
}