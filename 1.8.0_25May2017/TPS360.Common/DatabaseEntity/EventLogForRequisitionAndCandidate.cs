﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "EventLogForRequisitionAndCandidate", Namespace = "http://www.tps360.com/types")]
    public class EventLogForRequisitionAndCandidate:BaseEntity 
    {
          #region Properties

        [DataMember]
        public int CandidateId
        {
            get;
            set;
        }

        public int JobPostingID
        {
            get;
            set;
        }

        [DataMember]
        public string  ActionType
        {
            get;
            set;
        }

        [DataMember]
        public string UserName
        {
            get;
            set;
        }

        [DataMember]
        public DateTime  ActionDate
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }
        ////////////////////////////Sumit Sonawane 1/Mar/2017///////////////////////////////////////////////////////////////////
        public int HiringLevelLogId
        {
            get;
            set;
        }

        public string HiringLevelComment
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public EventLogForRequisitionAndCandidate()
            : base()
        {
        }

        #endregion
    }
}
