﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName            :   InterviewQuestionBank.cs
    Description         :   This page is used to Create Properties for InterviewQuestionBank.
    Created By          :   Prasanth
    Created On          :   15/Oct/2015
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "InterviewQuestionBank", Namespace = "http://www.tps360.com/types")]
    public class InterviewQuestionBank : BaseEntity
    {
        #region Properties

        public int InterviewQuestionBank_Id
        {
            get;
            set;
        }

        public int InterviewId
        {
            get;
            set;
        }

        public int QuestionBankType_Id
        {
            get;
            set;
        }

    
        public string QuestionBankType
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public InterviewQuestionBank()
            : base()
        {
        }

        #endregion
    }
}