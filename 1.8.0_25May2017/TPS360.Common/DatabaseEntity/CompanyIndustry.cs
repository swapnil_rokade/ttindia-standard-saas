﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyIndustry", Namespace = "http://www.tps360.com/types")]
    public class CompanyIndustry : BaseEntity
    {
        #region Properties

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }

        [DataMember]
        public int IndustryLookupId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CompanyIndustry()
            : base()
        {
        }

        #endregion
    }
}