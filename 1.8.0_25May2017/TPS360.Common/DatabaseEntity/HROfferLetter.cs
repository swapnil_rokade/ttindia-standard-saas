﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "HROfferLetter", Namespace = "http://www.tps360.com/types")]
    public class HROfferLetter : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Position
        {
            get;
            set;
        }

        [DataMember]
        public string Department
        {
            get;
            set;
        }

        [DataMember]
        public string WorkDescription
        {
            get;
            set;
        }

        [DataMember]
        public int ContractType
        {
            get;
            set;
        }

        [DataMember]
        public string SupervisorName
        {
            get;
            set;
        }

        [DataMember]
        public string SuperVisorDesignation
        {
            get;
            set;
        }

        [DataMember]
        public string WorkLocation
        {
            get;
            set;
        }

        [DataMember]
        public string WorkPresentAddress
        {
            get;
            set;
        }

        [DataMember]
        public string WorkPermanentAddress
        {
            get;
            set;
        }

        [DataMember]
        public DateTime IssueDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime JoiningDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndingDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AppraisalDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AppraisalReminderDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime LastAcceptanceDate
        {
            get;
            set;
        }

        [DataMember]
        public int OfferValidityPeriod
        {
            get;
            set;
        }

        [DataMember]
        public int DailyWorkingHours
        {
            get;
            set;
        }

        [DataMember]
        public int WeeklyWorkingHours
        {
            get;
            set;
        }

        [DataMember]
        public int MonthlyWorkingHours
        {
            get;
            set;
        }

        [DataMember]
        public int WorkDayType
        {
            get;
            set;
        }

        [DataMember]
        public int WorkRotationPeriod
        {
            get;
            set;
        }

        [DataMember]
        public int ProvisionPeriod
        {
            get;
            set;
        }

        [DataMember]
        public int NoticePeriod
        {
            get;
            set;
        }

        [DataMember]
        public int SalaryType
        {
            get;
            set;
        }

        [DataMember]
        public decimal BaseSalary
        {
            get;
            set;
        }

        [DataMember]
        public string BaseSalaryInWords
        {
            get;
            set;
        }

        [DataMember]
        public int BaseSalaryCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal GrossSalary
        {
            get;
            set;
        }

        [DataMember]
        public string GrossSalaryInWords
        {
            get;
            set;
        }

        [DataMember]
        public int GrossSalaryCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal RegularOvertimeRate
        {
            get;
            set;
        }

        [DataMember]
        public string RegularOvertimeRateInWords
        {
            get;
            set;
        }

        [DataMember]
        public int RegularOvertimeCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public decimal HolidayOvertimeRate
        {
            get;
            set;
        }

        [DataMember]
        public string HolidayOvertimeRateInWords
        {
            get;
            set;
        }

        [DataMember]
        public int HolidayOvertimeCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string PublishedOfferLetter
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int PayCycle
        {
            get;
            set;
        }

        [DataMember]
        public string PayDay
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentType
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentBy
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentNotification
        {
            get;
            set;
        }

        [DataMember]
        public int DailyWorkSchedule
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public HROfferLetter()
            : base()
        {
        }

        #endregion
    }
}