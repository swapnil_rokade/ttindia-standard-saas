﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "TestAnswer", Namespace = "http://www.tps360.com/types")]
    public class TestAnswer : BaseEntity
    {
        #region Properties

        [DataMember]
        public string Answer
        {
            get;
            set;
        }

        [DataMember]
        public bool IsCorrect
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public int QuestionId
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public TestAnswer()
            : base()
        {
        }

        #endregion
    }
}