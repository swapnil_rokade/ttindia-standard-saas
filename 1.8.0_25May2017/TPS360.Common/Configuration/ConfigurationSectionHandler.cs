using System;
using System.IO;
using System.Xml;
using System.Configuration;
using System.Globalization;
using TPS360.Common.Helper;


namespace TPS360.Common
{
    /*
		<configSections>
			<section name="commonSetting" type="TPS360.Common.ConfigurationSectionHandler, TPS360.Common" />
		</configSections>
		
		<commonSetting>
            <mailMap location="MailTemplate">
              <map key="leadCreated" from="admin@tps360.com" subject="A new lead created." file="MailTemplate\LeadCreated.html" />
            </mailMap>
          </commonSetting>
    */

    internal sealed class ConfigurationSectionHandler:IConfigurationSectionHandler
	{
		public ConfigurationSectionHandler()
		{
		}
		
		
		object IConfigurationSectionHandler.Create(object parent,
			object configContext,
			XmlNode section)
		{
			if (section.ChildNodes.Count == 0)
			{
                throw new ConfigurationErrorsException("Please specify the common setting.", section);
			}

			Settings appSetting = new Settings();

			foreach(XmlNode xndSection in section.ChildNodes)
			{
				if(StringHelper.IsEqual(xndSection.LocalName, "mailMap"))
				{
					if (xndSection.ChildNodes.Count == 0)
					{
                        throw new ConfigurationErrorsException("Please specify the mail map setting.", section);
					}

					string currentPath = AppDomain.CurrentDomain.BaseDirectory;
					string templateDirectory = GetAttributeValue(xndSection.Attributes["location"]);

					if (templateDirectory == null)
					{
						templateDirectory = "MailTemplate";
					}

					if (!Directory.Exists(templateDirectory))
					{
						if (Directory.Exists(Path.Combine(currentPath, templateDirectory)))
						{
							templateDirectory = Path.Combine(currentPath, templateDirectory);
						}
					}

					if (StringHelper.IsBlank(templateDirectory))
					{
                        throw new ConfigurationErrorsException("Template directory does not exists.", xndSection);
					}

					foreach(XmlNode xndMap in xndSection.ChildNodes)
					{
						if (StringHelper.IsEqual(xndMap.LocalName, "map"))
						{
							string key = GetAttributeValue(xndMap.Attributes["key"]);
							string from = GetAttributeValue(xndMap.Attributes["from"]);
							string to = GetAttributeValue(xndMap.Attributes["to"]);
							string cc = GetAttributeValue(xndMap.Attributes["cc"]);
							string bcc = GetAttributeValue(xndMap.Attributes["bcc"]);
							string subject = GetAttributeValue(xndMap.Attributes["subject"]);
							string file = GetAttributeValue(xndMap.Attributes["file"]);
							string body;
					
							if (key == null)
							{
                                throw new ConfigurationErrorsException("Key must be specified for mail map.", xndMap);
							}

							if ((from == null) && (to == null))
							{
                                throw new ConfigurationErrorsException("From/To must be specified for mail map.", xndMap);
							}

							if (file == null)
							{
                                throw new ConfigurationErrorsException("Template file must be specified for mail map.", xndMap);
							}

							file = Path.Combine(templateDirectory, file);

							if (!File.Exists(file))
							{
                                throw new ConfigurationErrorsException("Template file does not exists.", xndMap);
							}

							using(StreamReader sr = new StreamReader(file))
							{
								body =  sr.ReadToEnd();
							}

							appSetting.SetMailMap(key, new MailMap(from, to, cc, bcc, subject, body));
						}
					}
				}
			}

			return appSetting;
		}


		private string GetAttributeValue(XmlNode attribute)
		{
			try
			{
				return attribute.Value.Trim();
			}
			catch
			{
			}

			return string.Empty;
		}
	}
}