using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable()]
    public abstract class BaseEntity : ICloneable
    {
        [DataMember]
        public virtual int Id
        {
            get;
            set;
        }

        [DataMember]
        public int CreatorId
        {
            get;
            set;
        }

        [DataMember]
        public int UpdatorId
        {
            get;
            set;
        }

        [XmlIgnore()]
        public Member Creator
        {
            get;
            set;
        }

        [XmlIgnore()]
        public Member Updator
        {
            get;
            set;
        }

        [XmlIgnore()]
        public virtual bool IsNew
        {
            [DebuggerStepThrough()]
            get
            {
                return (Id <= 0);
            }
        }

        [DataMember]
        public DateTime CreateDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime UpdateDate
        {
            get;
            set;
        }
        [DataMember]
        public string Name
        {
            get;
            set;
        }

        protected BaseEntity()
        {
            CreateDate = UpdateDate = DateTime.Now;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public virtual BaseEntity Clone()
        {
            using (MemoryStream ms = new MemoryStream())
            {
                (new BinaryFormatter()).Serialize(ms, this);

                ms.Seek(0, SeekOrigin.Begin);

                return (new BinaryFormatter()).Deserialize(ms) as BaseEntity;
            }
        }
    }
}