﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: EmployeeOverviewDetails.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author                  MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
 *  0.1              28/May/2016           Prasanth Kumar G         Introduced LDAP,Username
 ---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using TPS360.Common.BusinessEntities;

namespace TPS360.Common.BusinessEntity
{

    [Serializable]
    [DataContract(Name = "EmployeeOverview", Namespace="http://www.tps360.com/types")]
    public class EmployeeOverviewDetails : BaseEntity
    {
        #region Properties
        [DataMember]
        public string Name
        {
            get;
            set;
        }
        [DataMember]
        public string EmailId
        {
            get;
            set;
        }
        [DataMember]
        public string Mobile
        {
            get;
            set;
        }
        [DataMember]
        public string Location
        {
            get;
            set;
        }
        [DataMember]
        public string AccessRole
        {
            get;
            set;
        }
        [DataMember]
        public int RequisitionCount
        {
            get;
            set;
        }
        [DataMember]
        public int AssignedCandidateCount
        {
            get;
            set;
        }
        [DataMember]
        public int InterviewCount
        {
            get;
            set;
        }
        [DataMember]
        public int DocumentsCount
        {
            get;
            set;
        }
        //Code introduced by Prasanth on 28/May/2016 Start
        [DataMember]
        public string UserName
        {
            get;
            set;
        }
        public Boolean IsLDAP
        {
            get;
            set;
        }
        //******************END***************************
        #endregion

        #region Constructor

        public EmployeeOverviewDetails()
            : base()
        {
        }

        #endregion
    }
}
