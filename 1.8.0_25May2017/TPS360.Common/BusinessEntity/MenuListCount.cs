﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MenuListCount", Namespace = "http://www.tps360.com/types")]
    public class MenuListCount : BaseEntity
    {
        #region Properties

        [DataMember]
        public int   CustonSiteMapId
        {
            get;
            set;
        }

        [DataMember]
        public string MenuName
        {
            get;
            set;
        }

        [DataMember]
        public int Count
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MenuListCount()
            : base()
        {
        }

        #endregion
    }
}