﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: HiringMatrix.cs
    Description: This page is used to hold properties related to hiring matrix.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Apr-20-2009         Shivanand           Defect # 10361; New property "RoleName" is added.
    ------------------------------------------------------------------------------------------------------------------------------------- 
*/


using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "HiringMatrix", Namespace = "http://www.tps360.com/types")]
    public class HiringMatrix : BaseEntity
    {
        #region Properties
        [DataMember]
        public int StatusId
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentLevel
        {
            get;
            set;
        }

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string MiddleName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        public string CellPhone
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryEmail
        {
            get;
            set;
        }

        [DataMember]
        public DateTime UpdateDate
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentPosition
        {
            get;
            set;
        }

        [DataMember]
        public string TotalExperienceYears
        {
            get;
            set;
        }

        [DataMember]
        public string LastEmployer
        {
            get;
            set;
        }

        [DataMember]
        public decimal CurrentYearlyRate
        {
            get;
            set;
        }

        [DataMember]
        public string  CurrentYearlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedYearlyRate
        {
            get;
            set;
        }
        [DataMember]
        public decimal ExpectedYearlyMaxRate
        {
            get;
            set;
        }
        [DataMember]
        public string  ExpectedYearlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public string WorkAuthorization
        {
            get;
            set;
        }

        [DataMember]
        public int Availability
        {
            get;
            set;
        }
        [DataMember]
        public DateTime  AvailableFrom
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCity
        {
            get;
            set;
        }

        [DataMember]
        public string HiringNote
        {
            get;
            set;
        }

        [DataMember]
        public DateTime  NoteUpdatedDate
        {
            get;
            set;
        }

        [DataMember]
        public string NoteUpdatorName
        {
            get;
            set;
        }
        [DataMember]
        public string CurrentState
        {
            get;
            set;
        }

        [DataMember]
        public string Objective
        {
            get;
            set;
        }
        [DataMember]
        public string Remarks
        {
            get;
            set;
        }

        [DataMember]
        public string  WorkSchedule
        {
            get;
            set;
        }

        [DataMember]
        public string MemberType
        {
            get;
            set;
        }
        [DataMember]
        public string PrimaryManager
        {
            get;
            set;
        }
        [DataMember]
        public string Skills
        {
            get;
            set;
        }
        [DataMember]
        public string HighestDegree
        {
            get;
            set;
        }
        [DataMember]
        public decimal MatchingPercentage
        {
            get;
            set;
        }

        [DataMember]
        public string AvailabilityText
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentSalaryPayCycle
        {
            get;
            set;
        }
              [DataMember]
        public int ExpectedSalaryPayCycle
        {
            get;
            set;
        }
        [DataMember]
        public int WorkScheduleLookupId
        {
            get;
            set;
        }
        [DataMember]
        public int WorkStatusID
        {
            get;
            set;
        }
        [DataMember]
        public string EducationLevelIDs
        {
            get;
            set;
        }
        [DataMember]
        public int CurrentStateID
        {
            get;
            set;
        }

        [DataMember]
        public string AddedBy
        {
            get;
            set;
        }
        [DataMember]
        public DateTime AddedOn
        {
            get;
            set;
        }


        [DataMember]
        public int SourceID
        {
            get;
            set;
        }

        [DataMember]
        public string SourceName
        {
            get;
            set;
        }

        [DataMember]
        public bool HasUpcommingInterview
        {
            get;
            set;
        }

        [DataMember]
        public bool HasSubmissionDetails
        {
            get;
            set;
        }
        [DataMember]
        public bool HasOfferDetails
        {
            get;
            set;
        }
        [DataMember]
        public bool HasJoiningDetails
        {
            get;
            set;
        }
        [DataMember]
        public bool HasOfferDeclineDetails
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingID
        {
            get;
            set;
        }

        [DataMember]
        public string CandidateName
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentCountry
        {
            get;
            set;
        }
        [DataMember]
        public int JobPostingCreatorID
        {
            get;
            set;
        }

        public int RequisitionStatusID
        {
            get;
            set;
        }
        public string RequisitionStatus
        {
            get;
            set;
        }
        public string Requisition
        {
            get;
            set;
        }
        public int SelectionStepLookupId
        {
            get;
            set;
        }
        public string HiringLevelStatus
        {
            get;
            set;
        }
        ////////////////////////////Sumit Sonawane 1/Mar/2017///////////////////////////////////////////////////////////////////
        public int HiringLevelLogId
        {
            get;
            set;
        }

        public string HiringLevelComment
        {
            get;
            set;
        }

        public int TeamMemberId
        {
            get;
            set;
        }
        public int Teamrn
        {
            get;
            set;
        }
        public string NoticePeriod
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public HiringMatrix()
            : base()
        {
        }

        #endregion
    }
}
