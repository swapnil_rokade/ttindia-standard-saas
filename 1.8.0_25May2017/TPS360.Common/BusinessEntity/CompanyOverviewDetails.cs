﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CompanyOverviewDetails", Namespace = "http://www.tps360.com/types")]
    public class CompanyOverviewDetails : BaseEntity 
    {
        #region Properties
        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }
        [DataMember]
        public int PrimaryManagerId
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryManager
        {
            get;
            set;
        }

        [DataMember]
        public int RequisitionsCount
        {
            get;
            set;
        }

        [DataMember]
        public int ContactsCount
        {
            get;
            set;
        }

        [DataMember]
        public int DocumentsCount
        {
            get;
            set;
        }


        [DataMember]
        public DateTime ContractStartDate
        {
            get;
            set;
        }


        [DataMember]
        public DateTime ContractEndDate
        {
            get;
            set;
        }

        [DataMember]
        public decimal ServiceFee
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public CompanyOverviewDetails()
            : base()
        {
        }

        #endregion
    }
}
