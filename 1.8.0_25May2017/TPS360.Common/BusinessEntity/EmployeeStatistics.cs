﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Employee", Namespace = "http://www.tps360.com/types")]
    public class EmployeeStatistics
    {
        #region Properties

        [DataMember]
        public int EmployeeId
        {
            get;
            set;
        }

        [DataMember]
        public int TotalLead
        {
            get
            {
                return NumberOfCreatedLead + NumberOfAssociatedLead;
            }
        }

        [DataMember]
        public int NumberOfCreatedLead
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfAssociatedLead
        {
            get;
            set;
        }

        [DataMember]
        public int TotalCampaign
        {
            get
            {
                return NumberOfCreatedCampaign + NumberOfAssociatedCampaign;
            }
        }

        [DataMember]
        public int NumberOfCreatedCampaign
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfAssociatedCampaign
        {
            get;
            set;
        }

        [DataMember]
        public int TotalClient
        {
            get
            {
                return NumberOfCreatedClient + NumberOfAssociatedClient;
            }
        }

        [DataMember]
        public int NumberOfCreatedClient
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfAssociatedClient
        {
            get;
            set;
        }

        [DataMember]
        public int NumberOfCreatedInvoice
        {
            get;
            set;
        }

        [DataMember]
        public int EmployeeOpenRequisition
        {
            get;
            set;
        }

        [DataMember]
        public int EmployeeSubmittedRequisition
        {
            get;
            set;
        }

        [DataMember]
        public int EmployeeSuccessfullPlacement
        {
            get;
            set;
        }

        [DataMember]
        public int OpenRequisition
        {
            get;
            set;
        }

        [DataMember]
        public int EmployeeClosedUnsuccessfulRequisition
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public EmployeeStatistics()
            : base()
        {
        }

        #endregion
    }
}
