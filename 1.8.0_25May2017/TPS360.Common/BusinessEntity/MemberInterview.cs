﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: MemberInterview.cs
    Description:
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
 *  0.1                 20/May/2016        Sumit Sonawane       Introduced new data member BUinterviewerNames
----------------------------------------------------------------------------------------------------------------------------------------------
*/

using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberInterview", Namespace = "http://www.tps360.com/types")]
    public class MemberInterview : Interview
    {
        #region Properties

        [DataMember]
        public DateTime StartDateTimeUtc
        {
            get;
            set;
        }

        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string JobPostingCode
        {
            get;
            set;
        }

        [DataMember]
        public string InterviewTypeName
        {
            get;
            set;
        }

        [DataMember]
        public string InterviewerName
        {
            get;
            set;
        }

        [DataMember]
        public string ClientInterviewerName
        {
            get;
            set;
        }
       
        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        public string CompanyName
        {
            get;
            set;
        }

        [DataMember]
        public int CompanyId
        {
            get;
            set;
        }

        /////////////Code Added by Sumit Sonawane on 20/May/2016/////////////////////////
        [DataMember]
        public string BUinterviewerNames
        {
            get;
            set;
        }

        ////////////////////////////////////////////////////////////////////////////
        #endregion

        #region Constructor

        public MemberInterview()
            : base()
        {
        }

        #endregion
    }
}
