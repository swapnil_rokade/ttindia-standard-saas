﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberInterview", Namespace = "http://www.tps360.com/types")]
    public class MemberPendingJoiners : BaseEntity 
    {
        #region Properties

        [DataMember]
        public DateTime ExpectedDateOfJoining
        {
            get;
            set;
        }
        [DataMember]
        public int JobPostingID
        {
            get;
            set;
        }
        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string JobPostingCode
        {
            get;
            set;
        }


        [DataMember]
        public int MemberID
        {
            get;
            set;
        }
        [DataMember]
        public string CandidateName
        {
            get;
            set;
        }

        [DataMember]
        public int ActiveRecruiterID
        {
            get;
            set;
        }
        [DataMember]
        public string ActiveRecruiterName
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public MemberPendingJoiners()
            : base()
        {
        }

        #endregion
    }
}
