﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Candidate.cs
    Description: 
    Created By:
    Created On:
    Modification Log: 
    --------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    --------------------------------------------------------------------------------------------------------------------------------------
 *   0.1               12/Jan/2016         pravin khot         added fields-RequisitionName, JobId
 *   0.2               11/March/2016       pravin khot         added fields using for candidate CDM reports.
 * ------------------------------------------------------------------------------------------------------------------------------------------- 
*/

using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Employee", Namespace = "http://www.tps360.com/types")]
    public class Candidate : Member
    {
        #region Properties

        [DataMember]
        public string HomePhone
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCity
        {
            get;
            set;
        }

        [DataMember]
        public string StateName
        {
            get;
            set;
        }

        [DataMember]
        public string StateCode
        {
            get;
            set;
        }

        [DataMember]
        public string OfficeCity
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentPosition
        {
            get;
            set;
        }

        [DataMember]
        public string Remarks
        {
            get;
            set;
        }

        [DataMember]
        public string Objective
        {
            get;
            set;
        }

        [DataMember]
        public string Summary
        {
            get;
            set;
        }

        [DataMember]
        public string Skills
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentStateName
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentStateCode
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentCountryName
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentCountryCode
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentAddressLine1
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentAddressLine2
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentStateId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentStateName
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentStateCode
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentZip
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentCountryId
        {
            get;
            set;
        }
        [DataMember]
        public int PROCEDDDAYS
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCountryName
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCountryCode
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhone
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public string CityOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string CountryIdOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string BirthCountryName
        {
            get;
            set;
        }

        [DataMember]
        public string BirthCountryCode
        {
            get;
            set;
        }

        [DataMember]
        public string CountryIdOfCitizenship
        {
            get;
            set;
        }

        [DataMember]
        public string CitizenshipCountryName
        {
            get;
            set;
        }

        [DataMember]
        public string CitizenshipCountryCode
        {
            get;
            set;
        }

        [DataMember]
        public int GenderLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Gender
        {
            get;
            set;
        }

        [DataMember]
        public int EthnicGroupLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string EthnicGroup
        {
            get;
            set;
        }

        [DataMember]
        public int BloodGroupLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string BloodGroup
        {
            get;
            set;
        }

        [DataMember]
        public int MaritalStatusLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string MaritalStatus
        {
            get;
            set;
        }

        [DataMember]
        public bool Relocation
        {
            get;
            set;
        }

        [DataMember]
        public string LastEmployer
        {
            get;
            set;
        }

        [DataMember]
        public string TotalExperienceYears
        {
            get;
            set;
        }

        [DataMember]
        public int Availability
        {
            get;
            set;
        }

        [DataMember]
        public string AvailabilityText
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AvailableDate
        {
            get;
            set;
        }

        [DataMember]
        public decimal CurrentYearlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentYearlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentYearlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal CurrentMonthlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentMonthlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentMonthlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal CurrentHourlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentHourlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentHourlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedYearlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedYearlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string ExpectedYearlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedMonthlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedMonthlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string ExpectedMonthlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedHourlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedHourlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string ExpectedHourlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public bool WillingToTravel
        {
            get;
            set;
        }

        [DataMember]
        public int SalaryTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string SalaryType
        {
            get;
            set;
        }

        [DataMember]
        public int JobTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string JobType
        {
            get;
            set;
        }

        [DataMember]
        public bool SecurityClearance
        {
            get;
            set;
        }

        [DataMember]
        public int WorkAuthorizationLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int WorkScheduleLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string  WorkSchedule
        {
            get;
            set;
        }

        [DataMember]
        public string WorkAuthorization
        {
            get;
            set;
        }

        [DataMember]
        public string CreatorName
        {
            get;
            set;
        }

        [DataMember]
        public string LastUpdatorName
        {
            get;
            set;
        }
        public string Rank
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedYearlyMaxRate
        {
            get;
            set;
        } 

        [DataMember]
        public decimal ExpectedMonthlyMaxRate
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedHourlyMaxRate
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentSalaryPayCycle
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedSalaryPayCycle
        {
            get;
            set;
        }

        [DataMember]
        public string HighestHiringStatus
        {
            get;
            set;
        }

        [DataMember]
        public string MemberTypeName
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryManagerName
        {
            get;
            set;
        }

        [DataMember]
        public string HighestDegree
        {
            get;
            set;
        }

        [DataMember]
        public string DisplayOption
        {
            get;
            set;
        }
        [DataMember]
        public string Website
        {
            get;
            set;
        }
        [DataMember]
        public string LinkedInProfile
        {
            get;
            set;
        }
        [DataMember]
        public bool PassportStatus
        {
            get;
            set;
        }
        [DataMember]
        public string IDCard
        {
            get;
            set;
        }
        [DataMember]
        public string Source
        {
            get;
            set;
        }
        [DataMember]
        public string ReqCode
        {
            get;
            set;
        }
        [DataMember]
        public string HiringStatus
        {
            get;
            set;
        }
        [DataMember]
        public string SourceDescription
        {
            get;
            set;
        }


        [DataMember]
        public int CandidateIndustryLookupID
        {
            get;
            set;
        }

        [DataMember]
        public string CandidateIndustry
        {
            get;
            set;
        }

        [DataMember]
        public int JobFunctionLookupID
        {
            get;
            set;
        }

        [DataMember]
        public string JobFunction
        {
            get;
            set;
        }

        //5 Columns added
        [DataMember]
        public string NoticePeriod
        {
            get;
            set;
        }

        
        [DataMember]
        public string Department
        {
            get;
            set;
        }

        [DataMember]
        public string JobCode
        {
            get;
            set;
        }

        [DataMember]
        public string JOBTITLE
        {
            get;
            set;
        }
        [DataMember]
        public string FeedbackStatus
        {
            get;
            set;
        }

        [DataMember]
        public string HiringManager
        {
            get;
            set;
        }
        //***************code added by pravin khot on 12/Jan/2016
        [DataMember]
        public string RequisitionName
        {
            get;
            set;
        }
        [DataMember]
        public int JobId
        {
            get;
            set;
        }

        //**************************End****************************************


        //***************code added by pravin khot on 11/March/2016 - added fields using for candidate CDM reports*******
        [DataMember]
        public string FatherName
        {
            get;
            set;
        }
        [DataMember]
        public string Title
        {
            get;
            set;
        }


         [DataMember]
        public string structure
        {
            get;
            set;
        }
        [DataMember]
        public string InputComponent
        {
            get;
            set;
        }


        [DataMember]
        public string Bondamount
        {
            get;
            set;
        }
        [DataMember]
        public DateTime Bondfromdate
        {
            get;
            set;
        }
           [DataMember]
        public DateTime Bondenddate
        {
            get;
            set;
        }
       
        public string MarkLetterTo
        {
            get;
            set;
        }
        [DataMember]
        public string HiringSourceName
        {
            get;
            set;
        }
        [DataMember]
        public string HiringCost
        {
            get;
            set;
        }
        [DataMember]
        public string Hiringcomment
        {
            get;
            set;
        }
        [DataMember]
        public DateTime ExpectedDOJ
        {
            get;
            set;
        }
        [DataMember]
        public string BondName
        {
            get;
            set;
        }
        [DataMember]
        public DateTime ReqDate
        {
            get;
            set;
        }
        [DataMember]
        public DateTime WeddingDate
        {
            get;
            set;
        }
        [DataMember]
        public string project
        {
            get;
            set;
        }

       

        [DataMember]
        public string CareerLevel
        {
            get;
            set;
        }
        [DataMember]
        public string BenefitBand
        {
            get;
            set;
        }
        [DataMember]
        public string SalaryGrade
        {
            get;
            set;
        }
        [DataMember]
        public string JobRole
        {
            get;
            set;
        }
        [DataMember]
        public DateTime JoiningDate
        {
            get;
            set;
        }
       
    
        [DataMember]
        public string Branch
        {
            get;
            set;
        }
        [DataMember]
        public string Band
        {
            get;
            set;
        }
        //**************************End****************************************
     
        #endregion

        #region Constructor

        public Candidate()
            : base()
        {
        }

        #endregion
    }
}
