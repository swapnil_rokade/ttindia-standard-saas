﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "VHCandidate", Namespace = "http://www.tps360.com/types")]
    public class VHCandidate : BaseEntity
    {
        #region Properties

        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string MiddleName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        public string NickName
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DateOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentAddressLine1
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentAddressLine2
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentCity
        {
            get;
            set;
        }

        [DataMember]
        public int PermanentStateId
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentZip
        {
            get;
            set;
        }

        [DataMember]
        public int PermanentCountryId
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentPhone
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentPhoneExt
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentMobile
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryEmail
        {
            get;
            set;
        }

        [DataMember]
        public string AlternateEmail
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryPhone
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryPhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public string CellPhone
        {
            get;
            set;
        }        

        [DataMember]
        public int ResumeSource
        {
            get;
            set;
        }

        [DataMember]
        public int Status
        {
            get;
            set;
        }


        [DataMember]
        public string CurrentAddressLine1
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentAddressLine2
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCity
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentStateId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentZip
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentCountryId
        {
            get;
            set;
        }

        [DataMember]
        public string HomePhone
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhone
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhoneExtension
        {
            get;
            set;
        }


        [DataMember]
        public string CityOfBirth
        {
            get;
            set;
        }


        [DataMember]
        public string CountryIdOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string CountryIdOfCitizenship
        {
            get;
            set;
        }
        
        [DataMember]
        public int GenderLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int EthnicGroupLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int BloodGroupLookupId
        {
            get;
            set;
        }

        [DataMember]
        public int MaritalStatusLookupId
        {
            get;
            set;
        }        
        
        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }

        [DataMember]
        public Guid UserId
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public VHCandidate()
            : base()
        {
        }

        #endregion
    }
}