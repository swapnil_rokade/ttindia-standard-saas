﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Employee.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
 *     0.1              25/May/2016        pravin khot         added- TimeZone
    -------------------------------------------------------------------------------------------------------------------------------------------       
*/
using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Employee", Namespace = "http://www.tps360.com/types")]
    public class Employee : Member
    {
        #region Properties

        [DataMember]
        public string HomePhone
        {
            get;
            set;
        }

        [DataMember]
        public string SystemAccess
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCity
        {
            get;
            set;
        }

        [DataMember]
        public string StateName
        {
            get;
            set;
        }

        [DataMember]
        public string StateCode
        {
            get;
            set;
        }

        [DataMember]
        public string OfficeCity
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentPosition
        {
            get;
            set;
        }

        [DataMember]
        public string Remarks
        {
            get;
            set;
        }

        [DataMember]
        public string Objective
        {
            get;
            set;
        }

        [DataMember]
        public string Summary
        {
            get;
            set;
        }

        [DataMember]
        public string Skills
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentStateName
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentStateCode
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentCountryName
        {
            get;
            set;
        }

        [DataMember]
        public string PermanentCountryCode
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentAddressLine1
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentAddressLine2
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentStateId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentStateName
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentStateCode
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentZip
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentCountryId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCountryName
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCountryCode
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhone
        {
            get;
            set;
        }

        [DataMember]
        public string OfficePhoneExtension
        {
            get;
            set;
        }

        [DataMember]
        public string CityOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string CountryIdOfBirth
        {
            get;
            set;
        }

        [DataMember]
        public string BirthCountryName
        {
            get;
            set;
        }

        [DataMember]
        public string BirthCountryCode
        {
            get;
            set;
        }

        [DataMember]
        public string CountryIdOfCitizenship
        {
            get;
            set;
        }

        [DataMember]
        public string CitizenshipCountryName
        {
            get;
            set;
        }

        [DataMember]
        public string CitizenshipCountryCode
        {
            get;
            set;
        }

        [DataMember]
        public int GenderLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string Gender
        {
            get;
            set;
        }

        [DataMember]
        public int EthnicGroupLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string EthnicGroup
        {
            get;
            set;
        }

        [DataMember]
        public int BloodGroupLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string BloodGroup
        {
            get;
            set;
        }

        [DataMember]
        public int MaritalStatusLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string MaritalStatus
        {
            get;
            set;
        }

        [DataMember]
        public bool Relocation
        {
            get;
            set;
        }

        [DataMember]
        public string LastEmployer
        {
            get;
            set;
        }

        [DataMember]
        public string TotalExperienceYears
        {
            get;
            set;
        }

        [DataMember]
        public int Availability
        {
            get;
            set;
        }

        [DataMember]
        public string AvailabilityText
        {
            get;
            set;
        }

        [DataMember]
        public DateTime AvailableDate
        {
            get;
            set;
        }

        [DataMember]
        public decimal CurrentYearlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentYearlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentYearlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal CurrentMonthlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentMonthlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentMonthlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal CurrentHourlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int CurrentHourlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentHourlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedYearlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedYearlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string ExpectedYearlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedMonthlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedMonthlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string ExpectedMonthlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpectedHourlyRate
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedHourlyCurrencyLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string ExpectedHourlyCurrency
        {
            get;
            set;
        }

        [DataMember]
        public bool WillingToTravel
        {
            get;
            set;
        }

        [DataMember]
        public int SalaryTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string SalaryType
        {
            get;
            set;
        }

        [DataMember]
        public int JobTypeLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string JobType
        {
            get;
            set;
        }

        [DataMember]
        public bool SecurityClearance
        {
            get;
            set;
        }

        [DataMember]
        public int WorkAuthorizationLookupId
        {
            get;
            set;
        }

        [DataMember]
        public string WorkAuthorization
        {
            get;
            set;
        }

        [DataMember]
        public string CreatorName
        {
            get;
            set;
        }

        [DataMember]
        public string LastUpdatorName
        {
            get;
            set;
        }



        [DataMember]
        public int Leads
        {
            get;
            set;
        }

        [DataMember]
        public Decimal LeadsAmount
        {
            get;
            set;
        }

        [DataMember]
        public int Opportunity
        {
            get;
            set;
        }

        [DataMember]
        public Decimal OpportunityAmount
        {
            get;
            set;
        }

        [DataMember]
        public int LostOpportunity
        {
            get;
            set;
        }

        [DataMember]
        public Decimal LostOpportunityAmount
        {
            get;
            set;
        }

        [DataMember]
        public int SuccessfulSales
        {
            get;
            set;
        }

        [DataMember]
        public Decimal SuccessfulSalesAmount
        {
            get;
            set;
        }

        [DataMember]
        public int Invoice
        {
            get;
            set;
        }

        [DataMember]
        public Decimal InvoiceAmount
        {
            get;
            set;
        }

        [DataMember]
        public int InvoicePaid
        {
            get;
            set;
        }

        [DataMember]
        public Decimal InvoicePaidAmount
        {
            get;
            set;
        }

        [DataMember]
        public int Expense
        {
            get;
            set;
        }

        [DataMember]
        public Decimal ExpenseAmount
        {
            get;
            set;
        }

        [DataMember]
        public int ExpensePaid
        {
            get;
            set;
        }

        [DataMember]
        public Decimal ExpensePaidAmount
        {
            get;
            set;
        }

        [DataMember]
        public int Company
        {
            get;
            set;
        }

        [DataMember]
        public int ProspectiveClient
        {
            get;
            set;
        }

        [DataMember]
        public int Client
        {
            get;
            set;
        }

        [DataMember]
        public int Vendor
        {
            get;
            set;
        }

        [DataMember]
        public int Partner
        {
            get;
            set;
        }

        [DataMember]
        public int Applicant
        {
            get;
            set;
        }

        [DataMember]
        public int Consultant
        {
            get;
            set;
        }

        [DataMember]
        public int ConsultantEngaged
        {
            get;
            set;
        }

        [DataMember]
        public int ConsultantBench
        {
            get;
            set;
        }

        [DataMember]
        public int HotList
        {
            get;
            set;
        }

        [DataMember]
        public int Requisition
        {
            get;
            set;
        }

        [DataMember]
        public int RequisitionOpen
        {
            get;
            set;
        }

        [DataMember]
        public int RequisitionClosed
        {
            get;
            set;
        }

        [DataMember]
        public int RequisitionSubmitted
        {
            get;
            set;
        }

        [DataMember]
        public int SuccessfulHire
        {
            get;
            set;
        }

        [DataMember]
        public int UnsuccessfulHire
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryManager
        {
            get;
            set;
        }
        //added by pravinkhot on 25/May/2016
        [DataMember]
        public string TimeZone
        {
            get;
            set;
        }
        [DataMember]
        public int Timezoneid
        {
            get;
            set;
        }
        //*******END********************
        #endregion

        #region Constructor

        public Employee()
            : base()
        {
        }

        #endregion
    }
}
