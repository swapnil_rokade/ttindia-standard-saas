﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CandidateOverviewDetails", Namespace = "http://www.tps360.com/types")]
    public class CandidateOverviewDetails : BaseEntity 
    {
        #region Properties
        [DataMember]
        public int  Id
        {
            get;
            set;
        }
        [DataMember]
        public string CandidateName
        {
            get;
            set;
        }
        [DataMember]
        public string JobTitle
        {
            get;
            set;
        }

        [DataMember]
        public string PrimaryManager
        {
            get;
            set;
        }

        [DataMember]
        public string CandidateEmail
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCompany
        {
            get;
            set;
        }
        [DataMember]
        public int InternalRating
        {
            get;
            set;
        }
        [DataMember]
        public string Mobile
        {
            get;
            set;
        }
        [DataMember]
        public string YearsOfExperience
        {
            get;
            set;
        }

        [DataMember]
        public int ResumeSourceId
        {
            get;
            set;
        }

        [DataMember]
        public string ResumeSourceName 
        {
            get;
            set;
        }

        [DataMember]
        public string Location
        {
            get;
            set;
        }

        
       
        [DataMember]
        public string HighestEducation
        {
            get;
            set;
        }

        [DataMember]
        public string CandidateType
        {
            get;
            set;
        }
        [DataMember]
        public string Availability
        {
            get;
            set;
        }

        [DataMember]
        public DateTime LastUpdated 
        { 
            get;
            set;
        }

        [DataMember]
        public string Notes //added by pravin khot on 12/July/2016
        {
            get;
            set;
        }
        [DataMember]
        public string Categary //added by pravin khot on 12/July/2016
        {
            get;
            set;
        }
        [DataMember]
        public string MemberName //added by pravin khot on 12/July/2016
        {
            get;
            set;
        }

        [DataMember]
        public string Remarks
        {
            get;
            set;
        }
        public int Status
        {
            get;
            set;
        }

        [DataMember]
        public string CurrentCTC
        {
            get;
            set;
        }
        [DataMember]
        public string ExpectedCTC
        {
            get;
            set;
        }
        [DataMember]
        public string NoticePeriod
        {
            get;
            set;
        }
        #endregion

        #region Constructor

        public CandidateOverviewDetails()
            : base()
        {
        }

        #endregion
    }
}
