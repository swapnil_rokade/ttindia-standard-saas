﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "HireDesk", Namespace = "http://www.tps360.com/types")]
    public class HireDesk
    {
        #region Properties

        [DataMember]
        public int ApplicantCount2Days
        {
            get;
            set;
        }
        [DataMember]
        public int ApplicantCount7Days
        {
            get;
            set;
        }
        [DataMember]
        public int ApplicantCount30Days
        {
            get;
            set;
        }
        [DataMember]
        public int ApplicantCount90Days
        {
            get;
            set;
        }

        [DataMember]
        public int MyApplicantCount2Days
        {
            get;
            set;
        }
        [DataMember]
        public int MyApplicantCount7Days
        {
            get;
            set;
        }
        [DataMember]
        public int MyApplicantCount30Days
        {
            get;
            set;
        }
        [DataMember]
        public int MyApplicantCount90Days
        {
            get;
            set;
        }

        [DataMember]
        public int RequisitionCount2Days
        {
            get;
            set;
        }
        [DataMember]
        public int RequisitionCount7Days
        {
            get;
            set;
        }
        [DataMember]
        public int RequisitionCount30Days
        {
            get;
            set;
        }
        [DataMember]
        public int RequisitionCount90Days
        {
            get;
            set;
        }
        [DataMember]
        public int MyRequisitionCount2Days
        {
            get;
            set;
        }
        [DataMember]
        public int MyRequisitionCount7Days
        {
            get;
            set;
        }
        [DataMember]
        public int MyRequisitionCount30Days
        {
            get;
            set;
        }
        [DataMember]
        public int MyRequisitionCount90Days
        {
            get;
            set;
        }
        [DataMember]
        public int ReviewedApplicantCount2Days
        {
            get;
            set;
        }
        [DataMember]
        public int ReviewedApplicantCount7Days
        {
            get;
            set;
        }
        [DataMember]
        public int ReviewedApplicantCount30Days
        {
            get;
            set;
        }
        [DataMember]
        public int ReviewedApplicantCount90Days
        {
            get;
            set;
        }
        [DataMember]
        public int LevelIApplicantCount2Days
        {
            get;
            set;
        }
        [DataMember]
        public int LevelIApplicantCount7Days
        {
            get;
            set;
        }
        [DataMember]
        public int LevelIApplicantCount30Days
        {
            get;
            set;
        }
        [DataMember]
        public int LevelIApplicantCount90Days
        {
            get;
            set;
        }
        [DataMember]
        public int LevelIIApplicantCount2Days
        {
            get;
            set;
        }
        [DataMember]
        public int LevelIIApplicantCount7Days
        {
            get;
            set;
        }
        [DataMember]
        public int LevelIIApplicantCount30Days
        {
            get;
            set;
        }
        [DataMember]
        public int LevelIIApplicantCount90Days
        {
            get;
            set;
        }
        [DataMember]
        public int LevelIIIApplicantCount2Days
        {
            get;
            set;
        }
        [DataMember]
        public int LevelIIIApplicantCount7Days
        {
            get;
            set;
        }
        [DataMember]
        public int LevelIIIApplicantCount30Days
        {
            get;
            set;
        }
        [DataMember]
        public int LevelIIIApplicantCount90Days
        {
            get;
            set;
        }
        [DataMember]
        public int OfferLettersCount2Days
        {
            get;
            set;
        }
        [DataMember]
        public int OfferLettersCount7Days
        {
            get;
            set;
        }
        [DataMember]
        public int OfferLettersCount30Days
        {
            get;
            set;
        }
        [DataMember]
        public int OfferLettersCount90Days
        {
            get;
            set;
        }
        [DataMember]
        public int HiredCount2Days
        {
            get;
            set;
        }
        [DataMember]
        public int HiredCount7Days
        {
            get;
            set;
        }
        [DataMember]
        public int HiredCount30Days
        {
            get;
            set;
        }
        [DataMember]
        public int HiredCount90Days
        {
            get;
            set;
        }
        [DataMember]
        public int RejectedCount2Days
        {
            get;
            set;
        }
        [DataMember]
        public int RejectedCount7Days
        {
            get;
            set;
        }
        [DataMember]
        public int RejectedCount30Days
        {
            get;
            set;
        }
        [DataMember]
        public int RejectedCount90Days
        {
            get;
            set;
        }
        [DataMember]
        public int TotalSuccessfulHire
        {
            get;
            set;
        }
        [DataMember]
        public int TotalRequisitionProcessed
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public HireDesk()
            : base()
        {
        }

        #endregion
    }
}
