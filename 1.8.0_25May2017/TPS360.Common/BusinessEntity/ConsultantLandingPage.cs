﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "ConsultantLandingPage", Namespace = "http://www.tps360.com/types")]
    public class ConsultantLandingPage : BaseEntity
    {
        #region Properties

        [DataMember]
        public int ConsultantCount
        {
            get;
            set;
        }

        [DataMember]
        public int MyConsultantCount
        {
            get;
            set;
        }

        [DataMember]
        public int MyBenchConsultantCount
        {
            get;
            set;
        }

        [DataMember]
        public int MyEngagedConsultantCount
        {
            get;
            set;
        }

        [DataMember]
        public int PreselectedConsultantCount
        {
            get;
            set;
        }

        [DataMember]
        public int LevelIConsultantCount
        {
            get;
            set;
        }

        [DataMember]
        public int LevelIIConsultantCount
        {
            get;
            set;
        }

        [DataMember]
        public int LevelIIIConsultantCount
        {
            get;
            set;
        }

        [DataMember]
        public int FinalHiredConsultantCount
        {
            get;
            set;
        }

        [DataMember]
        public int ScheduledInterviewCount
        {
            get;
            set;
        }

        [DataMember]
        public int CompletedInterviewCount
        {
            get;
            set;
        }

        [DataMember]
        public int FeedbackedInterviewCount
        {
            get;
            set;
        }

        [DataMember]
        public int LevelIInterviewCount
        {
            get;
            set;
        }

        [DataMember]
        public int LevelIIInterviewCount
        {
            get;
            set;
        }

        [DataMember]
        public int LevelIIIInterviewCount
        {
            get;
            set;
        }

        [DataMember]
        public decimal ReceivedAmount
        {
            get;
            set;
        }

        [DataMember]
        public decimal PaymentAmount
        {
            get;
            set;
        }

        [DataMember]
        public decimal ExpenseAmount
        {
            get;
            set;
        }

        [DataMember]
        public decimal CommissionAmount
        {
            get;
            set;
        }

        [DataMember]
        public decimal TaxAmount
        {
            get;
            set;
        }

        [DataMember]
        public decimal NetProfitAmount
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public ConsultantLandingPage()
            : base()
        {
        }

        #endregion
    }
}