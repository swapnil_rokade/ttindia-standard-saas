﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ErrorLogDB.cs
    Description: issue id 1044
    Created By: Prasanth Kumar 
    Created On: 24/Apr/2017
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
  
    -------------------------------------------------------------------------------------------------------------------------------------------       
*/

using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MenuListCount", Namespace = "http://www.tps360.com/types")]
    public class ErrorLogDB: BaseEntity
    {
        #region Properties

     

        [DataMember]
        public string ExceptionData
        {
            get;
            set;
        }

        [DataMember]
        public int ExceptionID
        {
            get;
            set;
        }


        [DataMember]
        public string ExceptionMessage
        {
            get;
            set;
        }
 
        [DataMember]
        public string ExceptionStackTrace
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public ErrorLogDB()
            : base()
        {
        }

        
        #endregion
    }
}