﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "MemberPosition", Namespace = "http://www.tps360.com/types")]
    public class MemberPosition : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public int PositionId
        {
            get;
            set;
        }

        [DataMember]
        public string PositionName
        {
            get;
            set;
        }

        [DataMember]
        public string PositionDescription
        {
            get;
            set;
        }

        [DataMember]
        public int CategoryId
        {
            get;
            set;
        }

        [DataMember]
        public bool IsRemoved
        {
            get;
            set;
        }


        #endregion

        #region Constructor

        public MemberPosition()
            : base()
        {
        }

        #endregion
    }
}