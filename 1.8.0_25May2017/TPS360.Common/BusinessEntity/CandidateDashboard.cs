﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "CandidateDashboard", Namespace = "http://www.tps360.com/types")]
    public class CandidateDashboard : BaseEntity
    {
        #region Properties

        [DataMember]
        public int MemberId
        {
            get;
            set;
        }

        [DataMember]
        public DateTime RegistrationDate
        {
            get;
            set;
        }

        [DataMember]
        public int JobsAddedSinceLastLogin
        {
            get;
            set;
        }

        [DataMember]
        public int JobsInJobCart
        {
            get;
            set;
        }

        [DataMember]
        public int JobsApplied
        {
            get;
            set;
        }

        [DataMember]
        public int JobsSubmitted
        {
            get;
            set;
        }

        [DataMember]
        public int HotList
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewsPending
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewsCompleted
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewsFeedbacked
        {
            get;
            set;
        }

        [DataMember]
        public int TestsPending
        {
            get;
            set;
        }

        [DataMember]
        public int TestsCompleted
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public CandidateDashboard()
            : base()
        {
        }

        #endregion
    }
}