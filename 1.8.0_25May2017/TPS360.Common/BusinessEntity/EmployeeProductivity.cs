﻿using System;
using System.Runtime.Serialization;

namespace TPS360.Common.BusinessEntities
{
    [Serializable]
    [DataContract(Name = "Employee", Namespace = "http://www.tps360.com/types")]
    public class EmployeeProductivity :BaseEntity 
    {
        #region Properties

        [DataMember]
        public int EmployeeId 
        {
            get;
            set;
        }

        [DataMember]
        public string EmployeeName
        {
            get;
            set;
        }

        [DataMember]
        public int  JobCount
        {
            get;
            set;
        }

        [DataMember]
        public System.Collections.Generic.IList<Hiring_Levels> HiringMatrixLevels
        {
            get;
            set;
        }



        [DataMember]
        public int SubmissionCount
        {
            get;
            set;
        }

        [DataMember]
        public int  NewClientCount
        {
            get;
            set;
        }


        [DataMember]
        public int InterviewsCount
        {
            get;
            set;
        }

        [DataMember]
        public int OfferCount
        {
            get;
            set;
        }

        [DataMember]
        public int OfferRejectedCount
        {
            get;
            set;
        }


        [DataMember]
        public int JoinedCount
        {
            get;
            set;
        }




        //////////////////////code added by Sumit Sonawane on 21/Mar/2017////////////////////////////////////////////////////////////////////////////
        [DataMember]
        public int CandidateSourcedCount
        {
            get;
            set;
        }

        [DataMember]
        public int NewCandidateCount
        {
            get;
            set;
        }
        [DataMember]
        public int PendingJoiners
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewCompleted
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewNoShow
        {
            get;
            set;
        }

        [DataMember]
        public int InterviewReschedule
        {
            get;
            set;
        }

        [DataMember]
        public int ExpectedTimeToFill
        {
            get;
            set;
        }

        [DataMember]
        public int ActualTimeToFill
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfCandidateJoinedGraph
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfCandidateRejectedGraph
        {
            get;
            set;
        }

        [DataMember]
        public int ClientIdGraph
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfCandidateForSkillGraph
        {
            get;
            set;
        }

        [DataMember]
        public int SkillIdGraph
        {
            get;
            set;
        }

        [DataMember]
        public int YearGraph
        {
            get;
            set;
        }

        [DataMember]
        public int MonthGraph
        {
            get;
            set;
        }

        [DataMember]
        public string MonthNameGraph
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfOpeningsGraph
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfOpenings1Graph
        {
            get;
            set;
        }

        [DataMember]
        public int RequisitionsCountGraph
        {
            get;
            set;
        }

        [DataMember]
        public int NoOfOpeningsJoinedGraph
        {
            get;
            set;
        }

        [DataMember]
        public int JobPostingIdGraph
        {
            get;
            set;
        }
        [DataMember]
        public int JoinedCandidatesGraph
        {
            get;
            set;
        }
        [DataMember]

        public DateTime UpdateDateGraph
        {
            get;
            set;
        }

        [DataMember]

        public DateTime OpenDateGraph
        {
            get;
            set;
        }

        [DataMember]
        public int JobStatusGraph
        {
            get;
            set;
        }

        [DataMember]
        public int CountByJobStatusGraph
        {
            get;
            set;
        }

        [DataMember]
        public int CountNoOfOpeningsGraph
        {
            get;
            set;
        }

        [DataMember]
        public int CountNoOfOpeningsTempGraph
        {
            get;
            set;
        }
        ////////////////////// End ////////////////////////////////////////////////////////////////////////////

        #endregion

        #region Constructor

        public EmployeeProductivity()
            : base()
        {
        }

        #endregion
    }
}
