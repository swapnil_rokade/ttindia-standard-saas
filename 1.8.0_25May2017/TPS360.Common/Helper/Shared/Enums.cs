/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: Enums.cs
    Description: This page is used for Enums
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Nov-11-2008        Yogeesh Bhat        Defect ID:9180; added new Enum for Job Duration Type.
    0.2            Nov-18-2008        Shivanand           Defect #8670; New field "ConsultantPreciseSearch" is added in enum Search query.
    0.3            Dec-02-2008        Yogeesh Bhat        Defect # 8836: Added new enum ConsultantSource
    0.4            Feb-04-2009        Yogeesh Bhat        Defect #9804: Added new enum for LookUpType
    0.5            Feb-05-2009        N.Srilakshmi        Defect #9838: Added new enum in DefaultSiteSettings   
 *  0.6            Mar-09-2009        N.Srilakshmi        Defect #10067:Added new enum in DefaultSiteSetting   
    0.7            Jun-24-2009        Yogeesh Bhat        Defect Id: 10758: Added new value for DefaultSiteSetting enum
    0.8            Jun-29-2009        Veda                Defect Id: 10511: Modify Default Sitemap 
    0.9            Jun-29-2009        Nagarathna          Enhancement Id:10510 changeing the look up data label.
 *  1.0            Feb-25-2010        Nagarathna V.B      Enhancement Id:12140 added candidatekey.
 *  1.1            March-17-2010      Basavaraj A         Defect id:12322 ; Corrected the Spelling Mistakes
 *  1.2            20/Aug/2015        Prasanth Kumar G    Introduced Interviewer FeedBack
 *  1.3            13/Oct/2015        Prasanth Kumar G    Introduced AnswerType, QuestionBank Type
 *  1.4            23/Feb/2016        pravin khot         Introduced by  [EnumDescription("In-Active")]
 *  1.5            27/April/2016      pravin khot         Introduced by  AllowCandidatetobeaddedonMultipleRequisition,AllowVendorstoUpdateCandidateProfiles
 *  1.6            24/May/2016        pravin khot         Introduced by VenueMaster
 *  1.7            15/July/2016       pravin khot         added-TimeZone
 *  1.8             8/Aug/2016        pravin khot         added-AllowShowPreciseSearchAllCandidates = 26  
--------------- ----------------------------------------------------------------------------------------------------------------------------        
*/

using System;

namespace TPS360.Common.Shared
{
    [Serializable()]
    public enum LookupType : int
    {
        [EnumDescription("None")]
        None = 0,
        //[EnumDescription("Resource Group")]
        //ResourceGroup = 1,
        //[EnumDescription("Culture Code")]
        //CultureCode = 2,
        //[EnumDescription("Issue Type")]
        //IssueType = 3,
        [EnumDescription("Work Authorization Type")]
        WorkAuthorizationType = 4,
        [EnumDescription("Educational Qualification Type")]
        EducationalQualificationType = 5,
        [EnumDescription("Employment Type")] //0.9
        EmploymentDurationType = 6,
        [EnumDescription("Gender")]
        Gender = 7,
        //[EnumDescription("Ethnic Group")]
        //EthnicGroup = 8,
        [EnumDescription("Marital Status")]
        MaritalStatus = 9,
        //[EnumDescription("Blood Group")]
        //BloodGroup = 10,
        //[EnumDescription("JobCategory Type")]
        //JobCategoryType = 11,
        [EnumDescription("Industry Type")]
        IndustryType = 12,
        //[EnumDescription("Salary Payment Type")]//0.9
        //TaxTermType = 13,
        [EnumDescription("Visa Type")]
        VisaType = 14,
        [EnumDescription("Company Size")]
        CompanySize = 15,
        [EnumDescription("Owner Type")]
        OwnerType = 16,
        [EnumDescription("Benefits")]
        Benifits = 17,
        [EnumDescription("Currency")]
        Currency = 18,
        //[EnumDescription("Campaign Status")]
        //CampaignStatus = 19,
        [EnumDescription("Document Type")]
        DocumentType = 20,
        //[EnumDescription("Sales Life Cycle")]
        //SalesLifeCycle = 21,
        //[EnumDescription("Sales Probability")]
        //SalesProbability = 22,
        //[EnumDescription("Field Of Study")]
        //FieldOfStudy = 23,
        //[EnumDescription("Candidate Selection Step")]
        //CandidateSelectionStep = 24,
        [EnumDescription("Interview Type")]
        InterviewType = 25,
        //[EnumDescription("Template Category")]
        //TemplateCategory = 26,
        [EnumDescription("Notes and Activities Type")]
        NotesType = 27,
        //[EnumDescription("Data Checklist")]
        //DataCheckList = 28,
        [EnumDescription("Internal Rating Type")]
        InternalRating = 29,
        [EnumDescription("Privacy & Availability")]
        ResumeAvailability = 30,
        //[EnumDescription("Project Status Category")]
        //ProjectStatusCategory = 31,
        //[EnumDescription("Setup Search Result")]
        //SetupSearchResult = 32,
        //[EnumDescription("Query Check Category")]
        //QueryCheckCategory = 33,
        [EnumDescription("Proficiency")]
        Proficiency = 34,
        //[EnumDescription("Insurance Type")]
        //Insurance = 35,
        //[EnumDescription("Guest House Type")]
        //GuestHouse = 36,
        [EnumDescription("Email Type")]
        EmailType = 37,
        //[EnumDescription("Functional Category")]
        //FunctionalCategory = 38,
        //[EnumDescription("Followup Action")]
        //FollowupAction = 39,
        [EnumDescription("Company Notes and Activities Type")]
        CompanyNotesType = 40,
        //[EnumDescription("Campaign Notes and Activities Type")]
        //CampaignNotesType = 41,
        //[EnumDescription("Candidate and Consultant Notes and Activities Type")]
        //CandidateConsultantNotesType = 42,
        //[EnumDescription("Employee Notes and Activities Type")]
        //EmployeeNotesType = 43,
        //[EnumDescription("Employee HR Notes and Activities Type")]
        //EmployeeHRNotesType = 44,
        //[EnumDescription("Expense Category")]
        //ExpenseCategory = 45,
        //[EnumDescription("Call Back Status")]
        //CallBackStatus = 46,
        //[EnumDescription("Member Submission Stage")]
        //MemberSubmissionStage = 47,
        //[EnumDescription("Organization Functional Category")]
        //OrganizationFunctionalCategory = 48,
        //[EnumDescription("Organization Tier")]
        //OrganizationTier = 49,
        //[EnumDescription("Office Type")]
        //OfficeType = 50,
        //[EnumDescription("Position Tier")]
        //PositionTier = 51,
        //[EnumDescription("HR Offer Letter Allowance")]
        //HROfferLetterAllounce = 52,
        //[EnumDescription("HR Offer Letter Benefit")]
        //HROfferLetterBenefit = 53,
        //[EnumDescription("HR Offer Letter Bouns")]
        //HROfferLetterBonus = 54,
        //[EnumDescription("HR Offer Letter Leave")]
        //HROfferLetterLeave = 55,
        [EnumDescription("Pay Cycle")]
        PayCycle = 56,
        //[EnumDescription("Holiday Category")]
        //HolidayCategory = 57,
        //[EnumDescription("Volume Hire Internal Rating Type")]
        //VHInternalRating = 58,
        //[EnumDescription("Daily Work Schedule")]
        //DailyWorkSchedule = 59,
        //[EnumDescription("Salary Payment By")]
        //SalaryPaymentBy = 60,
        //[EnumDescription("Salary Payment Notification")]
        //SalaryPaymentNotification = 61,
        //[EnumDescription("Branch Office Type")]
        //BranchOfficeType = 62,
        [EnumDescription("Job Duration Type")]      //0.1
        JobDurationType = 63,
        [EnumDescription("Requisition Status")]      //0.4
        RequisitionStatus = 64,                 //1.0
        [EnumDescription("Member Type")]
        MemberType = 65,
        [EnumDescription("Payment Type")]
        PaymentType = 66,
        [EnumDescription("Work Schedule")]
        WorkSchedule = 67,
        //[EnumDescription("Candidate Requisition Status")]
        //CandidateRequisitionStatus = 68,
        [EnumDescription("Funtional Type")]
        FuntionalType = 69,
        [EnumDescription("Industry Type")]
        IndustryTypes = 70,
        [EnumDescription("Sales Region")]
        SalesRegion = 71,
        [EnumDescription("Sales Group")]
        SalesGroup = 72,
        [EnumDescription("Job Category")]
        JobCategory = 73,
        [EnumDescription("Source Type")]
        SourceType = 74,
        [EnumDescription("Source Desciption")]
        SourceDesciption = 75,
        [EnumDescription("Employment Type")]
        EmploymentType = 76,
        [EnumDescription("BV Status")]
        BVStatus = 77,
        [EnumDescription("Fitment Percentile")]
        FitmentPercentile = 78,
        [EnumDescription("Job Location")]
        JobLocation = 79,
        [EnumDescription("Reason For Rejection")]
        ReasonForRejection = 80,
        [EnumDescription("Offered Grade Embedded")]
        OfferedGradeEmbedded = 81,
        [EnumDescription("Offered Grade Mech")]
        OfferedGradeMech = 82,
        [EnumDescription("Salary Components Embedded")]
        SalaryComponentsEmbedded = 83,
        [EnumDescription("Offer Category")]
        OfferCategory = 84,
        [EnumDescription("Salary Components Mech")]
        SalaryComponentsMech = 85,
        [EnumDescription("Requisition Status Values")]
        RequisitionStatusValues = 86,
        [EnumDescription("ID Proof")]
        IDProof = 87,
        [EnumDescription("Priority")]
        Priority = 94,
        [EnumDescription("Policy & Strategy/GM")]
        PolicyandStrategyGM = 95,
        [EnumDescription("Implementation/ Management")]
        ImplementationOrManagement = 96,
        [EnumDescription("Contribution/ Execution")]
        ContributionOrExecution = 97,
        [EnumDescription("Trainee")]
        Trainee = 98,
        [EnumDescription("Interview Rounds")]
        InterviewRounds = 99,
        [EnumDescription("Requisition Type")]
        RequisitionType = 100,
        [EnumDescription("Industry")]
        Industry = 101,
        [EnumDescription("Job Function")]
        JobFunction = 102,
        //Code introduced by Prasanth on 20/Aug/2015 Start
        [EnumDescription("Interviewer Document Type")]
        InterviewerDocumentType = 103,
        //******************END****************

        //Code introduced by Prasanth on 13/Oct/2015 Start
        [EnumDescription("Answer Type")]
        AnswerType = 104,
        [EnumDescription("Question Bank Type")]
        QuestionBankType = 105,
        //******************END*****************
        //Code introduced by pravin khot on 24/May/2016
        [EnumDescription("Venue Master")]
        VenueMaster = 106,
        //*********************END****************

        /*[EnumDescription("RequestionBranch")]
        RequestionBranch = 106,
        [EnumDescription("RequestionGrade")]
        RequestionGrade = 107*/

    }

    //0.3 starts here
    [Serializable()]
    public enum ConsultantSource : int
    {
        [EnumDescription("Internal")]
        Internal = 0,
        [EnumDescription("Independent")]
        Independent = 1,
        [EnumDescription("Vendor")]
        Vendor = 2
    }
    //0.3 ends here

    [Serializable()]
    public enum YesNo : int
    {
        [EnumDescription("Yes")]
        Yes = 1,
        [EnumDescription("No")]
        No = 2
    }

    [Serializable()]
    public enum EventLogForRequisition
    {
        [EnumDescription("Created Requisition Draft")]
        RequisitionCreated = 1,
        [EnumDescription("Edited Requisition")]
        RequisitionEdited = 2,
        [EnumDescription("Published Requisition")]
        RequisitionPublished = 3,
        [EnumDescription("Assigned Recruiter to Requisition")]
        RequisitionAssigned = 4,
        [EnumDescription("Changed Requisition Status")]
        RequisitionStatusChange = 5,
        [EnumDescription("Opened Hiring Matrix")]
        HiringMatrixOpened = 6

    }
    [Serializable()]
    public enum EventLogForCandidate
    {
        [EnumDescription("Added <candidate name> to Requisition")]
        CandidateAddedToRequisition = 1,
        [EnumDescription("Submitted <candidate name> to Client/Vendor")]
        CandidateSubmittedToClient = 2,
        [EnumDescription("Scheduled Interview for <candidate name>")]
        CandidateInterviewScheduled = 3,
        [EnumDescription("Entered Submission Details for <candidate name>")]
        CandidateSubmissionDetailsEntered = 4,
        [EnumDescription("Entered Offer Details for <candidate name>")]
        CandidateOfferDetailsEntered = 5,
        [EnumDescription("Entered Joining Details for <candidate name>")]
        CandidateJoiningDetailsEntered = 6,
        [EnumDescription("Emailed Requisition Details to <candidate name> ")]
        RequisitionDetailsEmailedToCandidate = 7,
        [EnumDescription("Changed Candidate Status for <candidate name> to <new status level>")]
        CandidateStatusChange = 8,
        [EnumDescription("Rejected <candidate name>")]
        CandidateRejected = 9,
        [EnumDescription("Edited Scheduled Interview for <candidate name>")]
        CandidateInterviewScheduledEdited = 10,
        [EnumDescription("Edited Submission Details for <candidate name>")]
        CandidateSubmissionDetailsEdited = 11,
        [EnumDescription("Edited Offer Details for <candidate name>")]
        CandidateOfferDetailsEdited = 12,
        [EnumDescription("Edited Joining Details for <candidate name>")]
        CandidateJoiningDetailsEdited = 13,
        [EnumDescription("Edited Recruiter Source for <candidate name>")]
        CandidateRecruterSourceEdited = 14,
        [EnumDescription("Added Hiring Note for <candidate name>")]
        CandidateHiringNoteAdded = 15,
        [EnumDescription("Edited Hiring Note for <candidate name>")]
        CandidateHiringNoteEdited = 16,
        [EnumDescription("Submitted <candidate name> to BU")]
        CandidateSubmittedToBU = 17,
        [EnumDescription("<candidate name> Remove From Hiring Matrix")]
        RemoveCandidateFromHiringMatrix = 18


    }
    #region Assessment Module

    [Serializable()]
    public enum AssessmentType : int
    {
        [EnumDescription("Online")]
        Online = 1,
        [EnumDescription("Phone")]
        Phone = 2,
        [EnumDescription("Short Question")]
        ShortQuestion = 3
    }

    [Serializable()]
    public enum AssessmentQuestionType : int
    {
        [EnumDescription("Multiple Answer")]
        MultipleAnswer = 1,
        [EnumDescription("Single Answer")]
        SingleAnswer = 2,
        [EnumDescription("True False")]
        TrueFalse = 3
    }

    [Serializable()]
    public enum AssessmentQuestionDifficultyLevel : int
    {
        [EnumDescription("Easy")]
        Easy = 1,
        [EnumDescription("Medium")]
        Medium = 2,
        [EnumDescription("Hard")]
        Hard = 3
    }

    [Serializable()]
    public enum AssessmentPusblishStatus : int
    {
        [EnumDescription("Draft")]
        Draft = 1,
        [EnumDescription("Pending")]
        Pending = 2,
        [EnumDescription("Published")]
        Published = 3
    }

    #endregion

    [Serializable()]
    public enum SystemDefaultRole : int
    {
        [EnumDescription("System Administrator")]
        SystemAdministrator = 1,
        [EnumDescription("Employee")]
        Employee = 2,
        [EnumDescription("Candidate")]
        Candidate = 3,
        [EnumDescription("Consultant")]
        Consultant = 4,
        [EnumDescription("Vendor")]
        Vendor = 5,
        [EnumDescription("Client")]
        Client = 6,
        [EnumDescription("Partner")]
        Partner = 7
    }

    [Serializable()]
    public enum SiteMapType : int
    {
        [EnumDescription("TPS360")]
        TPS360 = 1,
        [EnumDescription("Application Top Menu")]
        ApplicationTopMenu = 2,
        [EnumDescription("Admin Portal Menu")]
        AdminPortalMenu = 3,
        [EnumDescription("Employee Portal Menu")]
        EmployeePortalMenu = 4,
        //[EnumDescription("Consultant Career Portal Menu")]//12140
        //ConsultantCareerPortalMenu = 5,
        [EnumDescription("Candidate Career Portal Menu")]
        CandidateCareerPortalMenu = 6,
        [EnumDescription("Company Portal Menu")]   //0.8
        ClientPortalMenu = 7,
        [EnumDescription("Partner Portal Menu")]
        PartnerPortalMenu = 8,
        [EnumDescription("Vendor Portal Menu")]
        VendorPortalMenu = 9,
        [EnumDescription("Company Overview Menu")]
        CompanyOverviewMenu = 235,
        [EnumDescription("Campaign Overview Menu")]
        CampaignOverviewMenu = 236,
        [EnumDescription("Candidate Overview Menu")]
        CandidateOverviewMenu = 358,
        [EnumDescription("Employee")]
        Employee = 15,
        [EnumDescription("ATS")]
        ATS = 12,
        [EnumDescription("SFA")]
        SFA = 11,
        [EnumDescription("Requisition")]
        Requisition = 13,
        //[EnumDescription("Consultant Overview Menu")]//12140
        //ConsultantOverviewMenu = 359,
        [EnumDescription("Employee Overview Menu")]
        EmployeeOverviewMenu = 360,

        [EnumDescription("Candidate Portal Menu")]
        CandidatePortalMenu = 610,

        [EnumDescription("Candidate Profile")]
        CandidateProfileMenu = 611,

        [EnumDescription("Candidate Search Job")]
        CandidateSearchJob = 612,

        [EnumDescription("Department Overview Menu")]
        DepartmentOverviewMenu = 626,


        [EnumDescription("Vendor Profile Menu")]
        VendorProfileMenu = 642,

        [EnumDescription("Vendor Portal Top Menu")]
        VendorPortalTopMenu = 651,


        [EnumDescription("Candidate Profile Menu For Vendor")]
        CandidateProfileMenuForVendor = 658

    }

    [Serializable()]
    public enum ModuleType : int
    {
        [EnumDescription("Requisition")]
        Requisition = 1,
        [EnumDescription("Employee")]
        Employee = 2,
        [EnumDescription("Candidate")]
        Candidate = 3,
        [EnumDescription("Consultant")]
        Consultant = 4,
        [EnumDescription("Project Management")]
        ProjectManagement = 5,
        [EnumDescription("Member Time Sheet")]
        TimeSheet = 6,
        [EnumDescription("Precise Search Query")]
        SearchQuery = 7,
        [EnumDescription("Invoice")]
        Invoice = 8
    }

    [Serializable()]
    public enum ClientType : int
    {
        [EnumDescription("End Client")]
        EndClient = 1,
        [EnumDescription("Tier1 Client")]
        Tier1Client = 2,
        [EnumDescription("Tier2 Client")]
        Tier2Client = 3,
        [EnumDescription("Volume Hire Client")]
        VHClient = 4
    }

    [Serializable()]
    public enum PassportDrivingLicenseStatus : int
    {
        [EnumDescription("Ready")]
        Ready = 1,
        [EnumDescription("Not Ready")]
        NotReady = 2
    }

    [Serializable()]
    public enum SalesStatus : int
    {
        [EnumDescription("Unknown")]
        Unknown = 0,
        [EnumDescription("Lead")]
        Lead = 1,
        [EnumDescription("Bad Lead")]
        BadLead = 2,
        [EnumDescription("Opportunity")]
        Opportunity = 3,
        [EnumDescription("Successful Sale")]
        SuccessfullSales = 4,
        [EnumDescription("Lost Opportunity")]
        LostOpportunity = 5
    }

    [Serializable()]
    public enum LeadPriority : int
    {
        [EnumDescription("Unkonwn")]
        None = 0,
        [EnumDescription("High")]
        High = 1,
        [EnumDescription("Low")]
        Low = 2
    }

    [Serializable()]
    public enum LeadSource : int
    {
        [EnumDescription("Unknown")]
        Unknown = 0,
        [EnumDescription("Internal")]
        Internal = 1,
        [EnumDescription("External")]
        External = 2,
        [EnumDescription("Other")]
        Other = 3
    }

    [Serializable()]
    public enum ResumeSource : int
    {
        [EnumDescription("Self Registration")]
        SelfRegistration = 1,
        [EnumDescription("Admin")]
        Admin = 2,
        [EnumDescription("Employee")]
        Employee = 3,
        [EnumDescription("Client")]
        Client = 4,
        [EnumDescription("Supplier")]
        Supplier = 5,
        [EnumDescription("Partner")]
        Partner = 6,
        [EnumDescription("Resume Parser")]
        ResumeParser = 7,
        [EnumDescription("Outlook Mail")]
        OutlookMail = 8,
        [EnumDescription("Monster")]
        Monster = 9,
        [EnumDescription("Monster India")]
        MonsterIndia = 10,
        [EnumDescription("Career Builder")]
        CareerBuilder = 11,
        [EnumDescription("Dice")]
        Dice = 12,
        [EnumDescription("Times Jobs")]
        TimesJobs = 13,
        [EnumDescription("Hot Jobs")]
        HotJobs = 14,
        [EnumDescription("Naukri")]
        Naukri = 15,
        [EnumDescription("Outlook Contacts")]
        OutlookContacts = 16,
        [EnumDescription("Referral Program")]
        ReferralProgram = 17,
        [EnumDescription("Vendor")]
        Vendor = 18

    }

    [Serializable()]
    public enum MemberStatus : int
    {
        [EnumDescription("Active")]
        Active = 1,
        [EnumDescription("Suspend")]
        Suspend = 2
    }

    [Serializable()]
    public enum AccessStatus : int
    {
        [EnumDescription("Enabled")]
        Enabled = 1,
        [EnumDescription("Blocked")]
        Blocked = 2,
        //added by pravin khot
        [EnumDescription("In-Active")]
        InActive = 3
        //**********END***************
    }

    [Serializable()]
    public enum CompanyStatus : int
    {
        [EnumDescription("Unknown")]
        Unknown = 0,
        [EnumDescription("Company")]
        Company = 1,
        [EnumDescription("Prospective Client")]
        ProspectiveClient = 2,
        [EnumDescription("Client")]
        Client = 3,
        [EnumDescription("Vendor")]
        Vendor = 4,
        [EnumDescription("Partner")]
        Partner = 5,
        [EnumDescription("Department")]
        Department = 6
    }

    [Serializable()]
    public enum CompanyGroupType : int
    {
        [EnumDescription("Unknown Group")]
        Unknown = 0,
        [EnumDescription("Client Group")]
        ClientGroup = 1,
        [EnumDescription("Vendor Group")]
        VendorGroup = 2,
        [EnumDescription("Partner Group")]
        PartnerGroup = 3,
        [EnumDescription("Broadcast Group")]
        BroadcastGroup = 4
    }

    [Serializable()]
    public enum ApproveStatus : int
    {
        [EnumDescription("Pending")]
        Pending = 1,
        [EnumDescription("Declined")]
        Declined = 2,
        [EnumDescription("Approved")]
        Approved = 3
    }

    [Serializable()]
    public enum TimeSheetDuration : int
    {
        [EnumDescription("Weekly")]
        Weekly = 1,
        [EnumDescription("Biweekly")]
        Biweekly = 2,
        [EnumDescription("Monthly")]
        Monthly = 3
    }

    [Serializable()]
    public enum TimeSheetStatus : int
    {
        [EnumDescription("Draft")]
        Draft = 1,
        [EnumDescription("Pending")]
        Pending = 2,
        [EnumDescription("Approved")]
        Approved = 3,
        [EnumDescription("Rejected")]
        Rejected = 4
    }

    [Serializable()]
    public enum SalaryStatus : int
    {
        [EnumDescription("Paid")]
        Paid = 1,
        [EnumDescription("Due")]
        Due = 2
    }

    [Serializable()]
    public enum EmailAddressType : int
    {
        [EnumDescription("CC")]
        CC = 1,
        [EnumDescription("BCC")]
        BCC = 2,
        [EnumDescription("To")]
        To = 3
    }

    [Serializable()]
    public enum EmailSendingType : int
    {
        [EnumDescription("Broadcast")]
        Broadcast = 1,
        [EnumDescription("Single")]
        Single = 2
    }

    [Serializable()]
    public enum ResumeSharing : int
    {
        [EnumDescription("Public - Available in All Recruiters Search")]
        Public = 1,
        [EnumDescription("Private - Available Only in My Search")]
        Private = 2
    }

    public enum RegistrationOption
    {
        None = 0,
        NotRequired = 1,
        Optional = 2,
        Required = 3
    }

    public enum JobStatus : int
    {
        [EnumDescription("Open")]
        Open = 1,
        [EnumDescription("Open - Difficult skillset")]
        OpenDifficultskillset = 11,
        [EnumDescription("Open - F2F Not Feasible")]
        OpenF2FNotFeasible = 12,
        [EnumDescription("Open - Low Rate")]
        OpenLowRate = 13,
        [EnumDescription("Open - Resume Searching")]
        OpenResumeSearching = 14,
        [EnumDescription("Submitted")]
        Submitted = 2,
        [EnumDescription("Submitted -  Waiting for feedback")]
        SubmittedWaitingforfeedback = 21,
        [EnumDescription("Submitted - Interview in progress")]
        SubmittedInterviewinprogress = 22,
        [EnumDescription("Closed")]
        Closed = 3,
        [EnumDescription("Closed - Without submission")]
        ClosedWithoutsubmission = 31,
        [EnumDescription("Closed - Successful")]
        ClosedSuccessful = 32,
        [EnumDescription("Closed - Interviewed not successful")]
        ClosedInterviewednotsuccessful = 33,
        [EnumDescription("Closed - Submitted not successful")]
        ClosedSubmittednotSuccessful = 34,
        [EnumDescription("Draft")]
        Draft = 4,
    }

    public enum EmployeeType : int
    {
        Internal = 1,
        External = 2,
    }

    #region Task Management

    [Serializable()]
    public enum TaskStatus : int
    {
        [EnumDescription("Not Started")] //1.1
        NotStartted = 1,
        [EnumDescription("In Progress")]
        InProgress = 2,
        [EnumDescription("Waiting on someone else")]
        WaitingOnSomeoneElse = 3,
        [EnumDescription("Deferred")]   //1.1
        Defferred = 4,
        [EnumDescription("Completed")]
        Completed = 5
    }

    [Serializable()]
    public enum TaskPriority : int
    {
        [EnumDescription("Low")]
        Low = 1,
        [EnumDescription("Normal")]
        Normal = 2,
        [EnumDescription("High")]
        High = 3
    }

    [Serializable()]
    public enum TaskCompletePercentage : int
    {
        [EnumDescription("0%")]
        Zero = 1,
        [EnumDescription("25%")]
        TwentyFive = 2,
        [EnumDescription("50%")]
        Fifty = 3,
        [EnumDescription("75%")]
        SeventyFive = 4,
        [EnumDescription("100%")]
        Hundred = 5
    }

    [Serializable()]
    public enum MasterCommonTask : int
    {
        [EnumDescription("UnAssigned")]
        UnAssigned = 1,
        [EnumDescription("Open")]
        Open = 2,
        [EnumDescription("Completed")]
        Completed = 3
    }

    #endregion

    [Serializable()]
    public enum WorkFlowTitle : int
    {
        //Accounts
        AcctsIsManagerApprovalRequiredToGenerateWorkOrderPurchaseOrder = 1001,
        AcctsIsManagerApprovalRequiredtoGenerateInvoice = 1002,
        AcctsShouldEmployeeGeneratesPOAlertedAfterWeekEndClientHasNotEntered = 1003,
        //ATS
        ATSCandidatesAbleToViewAssignedManager = 2001,
        ATSCandidatesAppearHotListToMyCandidateList = 2002,
        ATSCandidatesPrivateOrPublic = 2003,
        ATSCandidateInformationArchived = 2004,
        ATSCandidateBeSentEmail = 2005,
        ATSShouldAssignedManagersForCandidateAlertedWeekBeforeCandidateBirthday = 2006,
        ATSShouldAssignedManagersForCandidateAlertedWhenCandidateUpdatesResume = 2007,
        ATSShouldAssignedManagersForCandidateAlertedWhenCandidateAppliesForJobFromCareerPortal = 2008,
        ATSShouldAssignedManagersForCandidateAlertedWhenCandidateEntersFeedbackForInterviewInCareerPortal = 2009,
        ATSShouldAssignedManagersForCandidateAlertedWhenCandidateCompletesOnlineTest = 2010,
        ATSShouldAssignedManagersForCandidateAlertedWhenCandidateRefersFriend = 2011,
        ATSShouldAssignedManagersForCandidateAlertedWhenCandidateChangesTheirAvailability = 2012,
        ATSShouldAssignedManagersForCandidateAlertedWhenCandidateSubmitsReference = 2013,
        ATSShouldAssignedManagersForCandidateBCCedAllEmailSentToCandidate = 2014,
        ATSShouldAssignedManagersForCandidateAlertedWhenCandidateAddedHotList = 2015,
        ATSShouldAssignedManagersForCandidateAlertedWhenCandidateConvertedToConsultant = 2016,
        //Assessment
        AssessmentSystemTestAdministratorApproval = 3001,
        //SFA
        SFAManagerAlertRequiredNewLead = 4001,
        SFAManagerAlertRequiredLeadToOpportunity = 4002,
        SFAManagerAlertRequiredSaleCompleted = 4003,
        SFAManagerAlertRequiredConvertCompanyToClient = 4004,
        SFAManagerApprovalRequiredRemovalClient = 4005,
        SFAManagerApprovalRequiredInvoicesChecklist = 4006,
        SFAClientProfileInformationReadonly = 4007,
        SFAIsManagerApprovalRequiredForSubmittedTimeSheets = 4008,
        SFAIsManagerApprovalRequiredToGenerateIncentiveCalculationForEachSuccessfulSale = 4009,
        SFAWhenPOCreatedShouldLocationForPOComparedToLCALocationForConsultantOnPOSendAtler = 4010,
        SFAShouldAssignedManagersForClientAlertedCientInsuranceExpirationBeforeSelectedTimeframe = 4011,
        SFAShouldSelectedemployeesalertedwhenLeadGeneratedFromInternalExternalSource = 4012,
        SFAShouldAssignedManagersAlertedWhenOpportunityConvertedToLostOpportunity = 4013,
        SFAShouldAssignedManagersForCampaignBeAlertedWhenCompaniesAddedCampaign = 4014,
        SFAShouldAssignedManagersForCampaignAlertedWhenLeadCreatedForCompanyCampaign = 4015,
        SFAShouldAssignedManagersForClientBCCedAllEmailSentToClient = 4016,
        //Consultant
        ConWillSelectedConsultantPortalPagesReadOnlyForConsultants = 5001,
        ConShouldConsultantAlertedIfResumeNotUpdatedInLastTimeframe = 5002,
        ConDuringAddingConsultantSystemBeAutomaticallyEmailedTheirUserID = 5003,
        ConShpuldActiveConsultantSentTimesheetReminderAlert = 5004,
        ConShouldConsultantInformationAutomaticallyArchived = 5005,
        ConsultantManagementConsultantsSalaryHiddenInConsultantPortal = 5006,
        ConsultantManagementTimeSheetModificationRestricted = 5007,
        ConsultantManagementShouldExpenseReimbursementModificationRestricted = 5008,
        ConsultantManagementManagerApprovalRequiredConsultantMovedBenchlist = 5009,
        ConIsManagerApprovalRequiredChecklistBeforeConsultantMovedToEngagedList = 5010,
        ConsultantManagementConsultantsSubmitTimeSheetsClientApp = 5011,
        ConsultantManagementEstimatedPayrollRequestsSelectedUsersApproval = 5012,
        ConsultantManagementSendAvailabilityAlert = 5013,
        ConShouldUserAddsConsultantBeAlertedThatLCAinfoEntered = 5014,
        ConsultantManagementsentAlertSelectedTimeframeBeforePassportLicenseExpire = 5015,
        ConShouldAssignedManagersForConsultantAlertedWeekBeforeConsultantBbirthday = 5016,
        ConShouldConsultantAlertedWhenPaymentMadeForThem = 5017,
        ConShouldAssignedManagersForConsultantAlertedWhenConsultantEntersFeedbackForInterviewInConsultantPortal = 5018,
        ConShouldAssignedManagersForConsultantAlertedWhenConsultantEntersFeedbackSubmissionConsultantPortal = 5019,
        ConShouldAssignedManagersConsultantAlertedWhenConsultantSelfRatingSubmissionConsultantPortal = 5020,
        ConShouldAssignedManagersConsultantAlertedWhenConsultantCompletesOnlineTest = 5021,
        ConShouldAssignedManagersConsultantAlertedWhenConsultantRefersFriend = 5022,
        ConShouldAssignedManagersConsultantAlertedWhenConsultantChangesAvailability = 5023,
        ConShouldAssignedManagersConsultantAlertedWhenConsultantSubmitsReference = 5024,
        ConShouldApplicantCheckProjectLocationMatchLCALocation = 5025,
        ConShouldAssignedManagersBeAlertedSelectednumberDaysBeforeProjectEnds = 5026,
        ConShouldExpenseReimbursementRequestsBeApprovedByOnlySelectedEmployees = 5027,
        ConShouldFinalPayrollRequestsGoToSelectedEmployeesForApprovalBeforeGenerating = 5028,
        ConShouldManagersForConsultantAlertedWeekBeforeConsultantAnniversary = 5029,
        ConShouldAssignedManagersForConsultantBCCedAllEmailSentToTheConsultant = 5030,
        ConShouldAssignedManagersForConsultantAlertedWhenConsultantAddedToHotList = 5031,
        //Employee
        EmpDuringAddingEmployeeToSystemShouldEmployeeAutomaticallyEmailedTheirUserIDPassword = 6001,
        EmpShouldManagersForEmployeeAlertedWhenEmployeeFillsOutProfile = 6002,
        EmpShouldManagersForEmployeeAlertedWeekBeforeEmployeeBirthday = 6003,
        EmpShouldManagersForAnEmployeeAlertedWeekBeforeEmployeeAnniversary = 6004,
        EmpShouldManagersForEmployeeAlertedWhenEmployeeSubmitsDailyReport = 6005,
        //Requisition
        RequisitionManagementMatchingConsultantsForRequisitionAutomaticallyShown = 7001,
        RequisitionManagementApprovalToPublishRequisitionInternally = 7002,
        RequisitionManagementApprovalToPublishRequisitionCompanyCareerPortal = 7003,
        RequisitionManagementApprovalRequiredBroadcastRequisitionVendors = 7004,
        RequisitionManagementApprovalRequiredToPublishRequisitionToExternalJobPortals = 7005,
        RequisitionManagementDeletionRequisitionsBePreventedExceptForApprovedUesrs = 7006,
        RequisitionManagementWhenRequisitionPublishedShouldCreatorAutomaticallyAddedAssignedManager = 7007,
        RequisitionManagementShouldSelectedApprovedUsersAbilityToChangeStats = 7008,
        RequisitionManagementWhenSelectingClientForRequisitionClientAtomaticallyAddedToRequisition = 7009,
        RequisitionManagementSelectingProjectForRequisitionProjectHiringManagersAutomaticallyAdded = 7010,
        RequisitionManagementShouldSubmissionCandidatesCconsultantsClientForRequisitionRestrictedOnlyClientAccount = 7011,
        RequisitionManagementWhenConsultantRemovedFromRequisitionUserEnterRreasonForRejection = 7012,
        RequisitionManagementWhenRequisitionDescriptionEmailSentToCandidateConsultantAutomaticallyCCed = 7013,
        RequisitionManagementShouldAssignedManagersRequisitionAlertedWhenRequisitionPublished = 7014,
        RequisitionManagementShouldApplicationAutomaticallyCheckDisplayDuplicateRequisitionsJobTitleLocation = 7015,
        RequisitionManagementWhenRecruiterSubmitsConsultantCandidateClientForRequisitionAlerted = 7016,
        RequisitionManagementWhenSubmittingCorpToCorpConsultantsAlert = 7017,
        RequisitionManagementShouldAssignedRecruitersRequisitionAlertedIfFinalHireCompletedByDate = 7018,
        RequisitionManagementShouldAssignedRecruitersForRequisitionAlertedIfFinalHiretoBeCompletedByDatePassed = 7019,
        RequisitionManagementShouldAssignedRecruitersForRequisitionAlertedWhenRecruiterAddsCandidatesHiringMatrix = 7020,
        RequisitionManagementShouldAssignedManagersForCandidateAlertedWhenStatusForSubmissionChanged = 7021,
        RequisitionManagementShouldAssignedManagersForCandidateAlertedWhenCandidateCompletesInitialInterviewForRequisition = 7022,
        RequisitionManagementShouldEmployeesAlertedIfUnAssignedFromRequisition = 7023,
        //VMS
        VMSIsManagerApprovalRequiredForNewVendorsWithCheckList = 8001,
        VMSIsManagerApprovalRequiredForRemovalVendors = 8002,
        VMSShouldVendorProfileInformationBeReadOnlyClientPortal = 8003,
        VMSShouldAssignedManagersForVendorAlertedAboutVendorInsuranceExpirationBeforeSelectedTimeframe = 8004,
        VMSShouldSelectedEmployeesAlertedWhenVendorSelfRegistersExternalWebSite = 8005,
        VMSShouldAssignedManagersVendorBCCedAllEmailSentToVendor = 8006,
        VMSShouldAssignedManagersForVendorAlertedWhenVendorSubmitsReferencesInVendorPortal = 8007,
        VMSShouldAssignedManagersForVendorAlertedWhenVendorSubmitsConsultantVendorPortal = 8008,
        //PRM
        PRMIsManagerApprovalRequiredToAddPartnerOrConvertCompanyToPartnerWithCheckList = 9001,
        PRMIsManagerApprovalRequiredForRemovalPartners = 9002,
        PRMShouldPartnerProfileInformationReadOnlyInPartnerPortal = 9003,
        PRMShouldAssignedManagersForPartnerBeAlertedAboutPartnerInsuranceExpirationBeforeTimeframe = 9004,
        PRMShouldSelectedEmployeesAlertedWhenPartnerSelfRegistersExternalWebsite = 9005,
        PRMShouldAssignedManagersPartnerBCCedOnAllEmailSentToPartner = 9006,
        PRMShouldAssignedManagersForPartnerAlertedWhenPartnerSubmitsReferencesInThePartnerPortal = 9007,
        PRMShouldAssignedManagersPartnerAlertedWhenPartnerSubmitsConsultantThePartnerPortal = 9008,
        //Projects
        ProjectShouldEmployeeAlertedWhenTheyAssignedTask = 10001,
        ProjectShouldCreatorTaskAlertedWhenAssigneeCompletesTaskSubmitsReport = 10002,
        ProjectShouldTeamMembersForProjectAlertedWhenProjectCreated = 10003,
        ProjectShouldTeamMembersForProjectAlertedWhenProjectStatusChanged = 10004,
        //Immigration
        ImmigrationShouldAssignedManagersConsultantAlertedMonthBeforeConsultantVisaExpirationDateGoingToExpire = 11001,
        ImmigrationShouldAssignedManagersForConsultantAlertedMonthBeforeConsultantH1B1ExpirationDateGoingToExpire = 11002,
        ImmigrationShouldAssignedManagersForConsultantAlertedMonthBeforeConsultantI94ExpirationDateGoingExpire = 11003,
        ImmigrationShouldTheAssignedManagersForConsultantAlertedMonthBeforeH1ApprovalDate = 11004,
        ImmigrationoShouldAssignedManagersForConsultantAlertedMonthBeforeH1QueryRespondDate = 11005
    }

    #region Member Activity

    [Serializable()]
    public enum MemberType : int
    {
        [EnumDescription("Candidate")]
        Candidate = 1,
        [EnumDescription("Consultant")]
        Consultant = 2,
        [EnumDescription("Employee")]
        Employee = 3,
        [EnumDescription("Company")]
        Company = 4
    }

    [Serializable()]
    public enum ActivityType : int
    {
        [EnumDescription("Unknown")]
        Unknown = 0,
        [EnumDescription("Add Member")]
        AddMember = 1,
        [EnumDescription("Add Basic Info")]
        AddBasicInfo = 2,
        [EnumDescription("Update Basic Info")]
        UpdateBasicInfo = 3,
        [EnumDescription("Add Additional Info")]
        AddAdditionalInfo = 4,
        [EnumDescription("Update Additional Info")]
        UpdateAdditionalInfo = 5,
        [EnumDescription("Add Experience")]
        AddExperience = 6,
        [EnumDescription("Update Experience")]
        UpdateExperience = 7,
        [EnumDescription("Delete Experience")]
        DeleteExperience = 8,
        [EnumDescription("Add Education")]
        AddEducation = 9,
        [EnumDescription("Update Education")]
        UpdateEducation = 10,
        [EnumDescription("Delete Education")]
        DeleteEducation = 11,
        [EnumDescription("Add Skill")]
        AddSkill = 12,
        [EnumDescription("Update Skill Set")]
        UpdateSkill = 13,
        [EnumDescription("Delete Skill")]
        DeleteSkill = 14,
        [EnumDescription("Add Objective")]
        AddObjective = 15,
        [EnumDescription("Update Objective")]
        UpdateObjective = 16,
        [EnumDescription("Add Summery")]
        AddSummery = 17,
        [EnumDescription("Update Summery")]
        UpdateSummery = 18,
        [EnumDescription("Add Copy Paste")]
        AddCopyPaste = 19,
        [EnumDescription("Update Copy Paste")]
        UpdateCopyPaste = 20,
        [EnumDescription("Upload Document")]
        UploadDocument = 21,
        [EnumDescription("Update Document")]
        UpdateDocument = 22,
        [EnumDescription("Delete Document")]
        DeleteDocument = 23,
        [EnumDescription("Add JobCart")]
        AddJobCart = 24,
        [EnumDescription("Delete JobCart")]
        DeleteJobCart = 25,
        [EnumDescription("Add Refference")]
        AddRefference = 26,
        [EnumDescription("Delete Refference")]
        DeleteRefference = 27,
        [EnumDescription("Add Referral")]
        AddReferral = 28,
        [EnumDescription("Change Password")]
        ChangePassword = 29,
        [EnumDescription("Change Resume Privacy")]
        ChangeResumePrivacy = 30,
        [EnumDescription("Add Company")]
        AddCompany = 31,
        [EnumDescription("Update Company")]
        UpdateCompany = 32,
        [EnumDescription("Delete Company")]
        DeleteCompany = 33,
        [EnumDescription("Company Converted To Client")]
        CompanyConvertedToClient = 34,
        [EnumDescription("Company Converted To Partner")]
        CompanyConvertedToPartner = 35,
        [EnumDescription("Company Converted To Vendor")]
        CompanyConvertedToVendor = 36,
        [EnumDescription("Add Company Contact")]
        AddCompanyContact = 37,
        [EnumDescription("Update Company Contact")]
        UpdateCompanyContact = 38,
        [EnumDescription("Delete Company Contact")]
        DeleteCompanyContact = 39,
        [EnumDescription("Add Campaign")]
        AddCampaign = 40,
        [EnumDescription("Update Campaign")]
        UpdateCampaign = 41,
        [EnumDescription("Delete Campaign")]
        DeleteCampaign = 42,
        [EnumDescription("Add Lead")]
        AddLead = 43,
        [EnumDescription("Update Lead")]
        UpdateLead = 44,
        [EnumDescription("Delete Lead")]
        DeleteLead = 45,
        [EnumDescription("Request For Convert Company To Client")]
        RequestForConvertCompanyToClient = 46,
        [EnumDescription("Request For Convert Company To Partner")]
        RequestForConvertCompanyToPartner = 47,
        [EnumDescription("Request For Convert Company To Vendor")]
        RequestForConvertCompanyToVendor = 48,
        [EnumDescription("Add Company Industry Type")]
        AddCompanyIndustryType = 49,
        [EnumDescription("Update Company Industry Type")]
        UpdateCompanyIndustryType = 50,
        [EnumDescription("Delete Company Industry Type")]
        DeleteCompanyIndustryType = 51,
        [EnumDescription("Add Company Financial Data")]
        AddCompanyFinancialData = 52,
        [EnumDescription("Update Company Financial Data")]
        UpdateCompanyFinancialData = 53,
        [EnumDescription("Delete Company Financial Data")]
        DeleteCompanyFinancialData = 54,
        [EnumDescription("Add Company Reference")]
        AddCompanyReference = 55,
        [EnumDescription("Update Company Reference")]
        UpdateCompanyReference = 56,
        [EnumDescription("Delete Company Reference")]
        DeleteCompanyReference = 57,
        [EnumDescription("Add Company Insurance")]
        AddCompanyInsurance = 58,
        [EnumDescription("Update Company Insurance")]
        UpdateCompanyInsurance = 59,
        [EnumDescription("Delete Company Insurance")]
        DeleteCompanyInsurance = 60,
        [EnumDescription("Resume BroadCast")]
        ResumeBroadCast = 61,
        [EnumDescription("Resume Submission")]
        ResumeSubmission = 62,
        [EnumDescription("Add Job Title")]
        AddJobTitle = 63,
        [EnumDescription("Delete Job Title")]
        DeleteJobTitle = 64,
        [EnumDescription("Login")]
        Login = 65,
        [EnumDescription("Logout")]
        Logout = 66,
        [EnumDescription("Submitted Daily Report")]
        SubmitMemberDailyReport = 67,
        [EnumDescription("Reviewed Candidate")]
        CandidateReviewed = 68,
        [EnumDescription("Reviewed Consultant")]
        ConsultantReviewed = 69
    }

    #endregion

    [Serializable()]
    public enum JobPostingQuestionType : int
    {
        [EnumDescription("General Question")]
        GeneralQuestion = 1,
        [EnumDescription("Additional Questions")]
        AdditionalQuestions = 2
    }

    [Serializable()]
    public enum DataCheckList : int
    {
        [EnumDescription("Company Data")]
        CompanyData = 1,
        [EnumDescription("Key Executives/Owners/Contacts Data")]
        ManagementData = 2,
        [EnumDescription("Financial Data")]
        FinancialData = 3,
        [EnumDescription("References")]
        References = 4,
        [EnumDescription("Insurance Data")]
        InsuranceData = 5,
        [EnumDescription("Company Documents/Brochures")]
        CompanyDocuments = 6
    }

    [Serializable()]
    public enum MemberGroupType : int
    {
        [EnumDescription("Candidate")]
        Candidate = 1,
        [EnumDescription("Consultant")]
        Consultant = 2,
        [EnumDescription("Employee")]
        Employee = 3,
        [EnumDescription("Client")]
        Client = 4,
        [EnumDescription("Vendor")]
        Vendor = 5,
        [EnumDescription("Partner")]
        Partner = 6
    }

    [Serializable()]
    public enum EmailType : int
    {
        [EnumDescription("Sent")]
        Sent = 390,
        [EnumDescription("Received")]
        Received = 391,
        [EnumDescription("Forwarded")]
        Forwarded = 392,
        [EnumDescription("Replied")]
        Replied = 393
    }

    [Serializable()]
    public enum InterviewStatus : int
    {
        [EnumDescription("Pending")]
        Pending = 1,
        [EnumDescription("Completed")]
        Completed = 2,
        [EnumDescription("Feadbacked")]
        Feadbacked = 3,
    }

    [Serializable()]
    public enum PaymentType : int
    {
        [EnumDescription("Hourly")]
        Hourly = 1,
        [EnumDescription("Daily")]
        Daily = 2,
        [EnumDescription("Monthly")]
        Monthly = 3,
        [EnumDescription("Yearly")]
        Yearly = 4,
    }

    [Serializable()]
    public enum MemberSelectionStep : int
    {
        [EnumDescription("Pre-Selected List")]
        PreSelectd = 1,
        [EnumDescription("Level I Interview")]
        LevelIInterview = 2,
        [EnumDescription("Level II Interview")]
        LevelIIInterview = 3,
        [EnumDescription("Level III Interview")]
        LevelIIIInterview = 4,
        [EnumDescription("Final Hired")]
        FinalHired = 5
    }

    [Serializable()]
    public enum VHMemberSelectionStep : int
    {
        [EnumDescription("Applicant List")]
        Applicants = 1,
        [EnumDescription("Selected List")]
        Selected = 2,
        [EnumDescription("Mobilization")]
        Mobilization = 3,
        [EnumDescription("Final Hired")]
        FinalHired = 4,
        [EnumDescription("Dropout List")]
        DropoutList = 5,
        [EnumDescription("Pending List")]
        PendingList = 6
    }

    [Serializable()]
    public enum ContentCategory : int
    {
        [EnumDescription("Sales/Marketing Templates")]
        SalesMarketingTemplates = 1,
        [EnumDescription("Candidates/Consultant Templates")]
        CandidatesConsultantTemplates = 2,
        [EnumDescription("News Editor")]
        NewsEditor = 3,
        [EnumDescription("Web Content")]
        WebContent = 4
    }

    [Serializable()]
    public enum PublishFolder : int
    {
        [EnumDescription("../News/")]
        News = 1,
        [EnumDescription("../Web/")]
        Web = 2
    }

    [Serializable()]
    public enum CampaignCompanyStatus : int
    {
        [EnumDescription("Unknown")]
        Unknown = 0,
        [EnumDescription("Not Called Yet")]
        NotCalledYet = 1,
        [EnumDescription("In Callback List")]
        InCallbackList = 2,
        [EnumDescription("Removed From Campaign")]
        RemovedFromCampaign = 3
    }


    [Serializable()]
    public enum Months : int
    {
        [EnumDescription("January")]
        January = 1,
        [EnumDescription("February")]
        February = 2,
        [EnumDescription("March")]
        March = 3,
        [EnumDescription("April")]
        April = 4,
        [EnumDescription("May")]
        May = 5,
        [EnumDescription("June")]
        June = 6,
        [EnumDescription("July")]
        July = 7,
        [EnumDescription("August")]
        August = 8,
        [EnumDescription("September")]
        September = 9,
        [EnumDescription("October")]
        October = 10,
        [EnumDescription("November")]
        November = 11,
        [EnumDescription("December")]
        December = 12
    }

    [Serializable()]
    public enum SearchQuery : int
    {
        [EnumDescription("ATS Precise Search")]
        ATSPreciseSearch = 1,
        // 0.2 starts
        [EnumDescription("Consultant Precise Search")]
        ConsultantPreciseSearch = 2
        //  0.2 ends

    }

    [Serializable()]
    public enum PODuration : int
    {
        [EnumDescription("1-10/Month")]
        OneToTen = 1,
        [EnumDescription("11-30/Month")]
        ElevenToThirty = 2,
        [EnumDescription("31-60/Month")]
        ThirtyOneToSixty = 3
    }

    [Serializable()]
    public enum POPaymentTerms : int
    {
        [EnumDescription("15 Days")]
        Fifteen = 1,
        [EnumDescription("30 Days")]
        Thirty = 2,
        [EnumDescription("45 Days")]
        FourtyFive = 3,
        [EnumDescription("60 Days")]
        Sixty = 4,
        [EnumDescription("90 Days")]
        Ninety = 5
    }

    [Serializable()]
    public enum TrainingCourseMaterial : int
    {
        [EnumDescription("Power Point Document")]
        PowerPoint = 1,
        [EnumDescription("Word Document")]
        Word = 2,
        [EnumDescription("Excel Document")]
        Excel = 3,
        [EnumDescription("Text File")]
        Text = 4,
        [EnumDescription("Video")]
        Video = 5,
        [EnumDescription("Zip")]
        Zip = 6
    }

    [Serializable()]
    public enum ExpenseStatus : int
    {
        [EnumDescription("Submitted")]
        Submitted = 1,
        [EnumDescription("Approved")]
        Approved = 2,
        [EnumDescription("Declined")]
        Declined = 3
    }

    [Serializable()]
    public enum TrainingFeedback : int
    {
        [EnumDescription("Not Started")]
        NotStarted = 1,
        [EnumDescription("In Progress")]
        InProgress = 2,
        [EnumDescription("Not Satisfactory")]
        NotSatisfactory = 3,
        [EnumDescription("Successfully Completed")]
        SuccessfullyCompleted = 4
    }

    [Serializable()]
    public enum TrainingProgramStatus : int
    {
        [EnumDescription("Not Started")]
        NotStarted = 1,
        [EnumDescription("In Progress")]
        InProgress = 2,
        [EnumDescription("Completed")]
        Completed = 3
    }

    [Serializable()]
    public enum TrainingProgramScheduleType : int
    {
        [EnumDescription("Hands On")]
        HandsOn = 1,
        [EnumDescription("Lecture")]
        Lecture = 2
    }

    public enum WorkDayType : int
    {
        [EnumDescription("Monday to Friday")]
        MondayToFriday = 1,
        [EnumDescription("Saturday to Thursday")]
        SaturdayToThursday = 2
    }

    public enum ContractType : int
    {
        [EnumDescription("Permanent")]
        Permanent = 1,
        [EnumDescription("Contractual Position")]
        ContractualPosition = 2
    }

    [Serializable()]
    public enum Allowance : int
    {
        [EnumDescription("Free")]
        Free = 1,
        [EnumDescription("Allowance")]
        Allowance = 2,
        [EnumDescription("Not Provided")]
        NotProvided = 3
    }

    [Serializable()]
    public enum VolumeHirePayment : int
    {
        [EnumDescription("Candidate")]
        Candidate = 1,
        [EnumDescription("Client")]
        Client = 2,
        [EnumDescription("Co-Payment")]
        CoPayment = 3
    }

    [Serializable()]
    public enum TraineeFeedback : int
    {
        [EnumDescription("Below Average")]
        BelowAverage = 1,
        [EnumDescription("Average")]
        Average = 2,
        [EnumDescription("Good")]
        Good = 3,
        [EnumDescription("Very Good")]
        VeryGood = 4
    }

    [Serializable()]
    public enum VHWorkDuration : int
    {
        [EnumDescription("Week")]
        Week = 1,
        [EnumDescription("Month")]
        Month = 2,
        [EnumDescription("Year")]
        Year = 3
    }

    [Serializable()]
    public enum MemberDocumentType : int
    {
        [EnumDescription("Word Resume")]
        WordResume = 55,
        [EnumDescription("Video Resume")]
        VideoResume = 58,
        [EnumDescription("Photo")]
        Photo = 57
    }

    [Serializable()]
    public enum VolumeHireRequisitionDocumentType : int
    {
        [EnumDescription("Demand Letter")]
        DemandLetter = 1,
        [EnumDescription("Power of Attorney")]
        PowerOfAttorney = 2,
        [EnumDescription("Specimen Agreement")]
        SpecimenAgreement = 3,
        [EnumDescription("NDA")]
        NDA = 4,
        [EnumDescription("Contract")]
        Contract = 5
    }

    [Serializable()]
    public enum TrainingProgramCompletionStatus : int
    {
        [EnumDescription("Not Started")]
        NotStarted = 1,
        [EnumDescription("In Progress")]
        InProgress = 2,
        [EnumDescription("Completed")]
        Completed = 3
    }

    [Serializable()]
    public enum PassportStatus : int
    {
        [EnumDescription("Ready")]
        Ready = 1,
        [EnumDescription("Not Ready")]
        NotReady = 2
    }

    [Serializable()]
    public enum VolumeHirePassportStatus : int
    {
        [EnumDescription("With Ambe")]
        WithAmbe = 1,
        [EnumDescription("With SubAgent")]
        WithSubAgent = 2,
        [EnumDescription("With Candidate")]
        WithCandidate = 3
    }

    [Serializable()]
    public enum VolumeHireEmigration : int
    {
        [EnumDescription("ECR")]
        ECR = 1,
        [EnumDescription("ECNR")]
        ECNR = 2

    }

    [Serializable()]
    public enum VolumeHireVisaStatus : int
    {
        [EnumDescription("In Process")]
        InProcess = 1,
        [EnumDescription("Successful")]
        Successful = 2,
        [EnumDescription("Failed")]
        Failed = 3
    }

    [Serializable()]
    public enum VolumeHireAirTicketStatus : int
    {
        [EnumDescription("PTA Pending")]
        PTAPending = 1,
        [EnumDescription("PTA Received")]
        PTAReceived = 2,
        [EnumDescription("Self Purchase")]
        SelfPurchase = 3
    }

    [Serializable()]
    public enum VolumeHireSource : int
    {
        [EnumDescription("PTACandidate")]
        PTAPending = 1
    }

    [Serializable()]
    public enum VolumeHireInsuranceStatus : int
    {
        [EnumDescription("In Process")]
        InProcess = 1,
        [EnumDescription("Successful")]
        Successful = 2,
        [EnumDescription("Failed")]
        Failed = 3
    }

    [Serializable()]
    public enum VolumeHireTradeTestStatus : int
    {
        [EnumDescription("In Process")]
        InProcess = 1,
        [EnumDescription("Successful")]
        Successful = 2,
        [EnumDescription("Failed")]
        Failed = 3
    }

    [Serializable()]
    public enum VolumeHireAssessmentStatus : int
    {
        [EnumDescription("In Process")]
        InProcess = 1,
        [EnumDescription("Successful")]
        Successful = 2,
        [EnumDescription("Failed")]
        Failed = 3
    }

    [Serializable()]
    public enum VolumeHireClientContact : int
    {
        [EnumDescription("In Process")]
        InProcess = 1,
        [EnumDescription("Signed")]
        Signed = 2,
        [EnumDescription("Sent To Client")]
        SentToClient = 3
    }

    [Serializable()]
    public enum VolumeHireMedicalStatus : int
    {
        [EnumDescription("In Process")]
        InProcess = 1,
        [EnumDescription("Fit")]
        Fit = 2,
        [EnumDescription("UnFit")]
        UnFit = 3
    }

    [Serializable()]
    public enum VolumeHireMedicalTestType : int
    {
        [EnumDescription("Medical Test 1")]
        Type1 = 1,
        [EnumDescription("Medical Test 2")]
        Type2 = 2
    }

    [Serializable()]
    public enum VolumeHirePCCStatus : int
    {
        [EnumDescription("In Process")]
        InProcess = 1,
        [EnumDescription("Successful")]
        Successful = 2,
        [EnumDescription("Failed")]
        Failed = 3
    }

    [Serializable()]
    public enum VolumeHireAttestationStatus : int
    {
        [EnumDescription("In Process")]
        InProcess = 1,
        [EnumDescription("Successful")]
        Successful = 2,
        [EnumDescription("Failed")]
        Failed = 3
    }

    [Serializable()]
    public enum PaasportEmigrationClearance : int
    {
        [EnumDescription("ECR")]
        ECR = 1,
        [EnumDescription("ECNR")]
        ECNR = 2
    }

    [Serializable()]
    public enum VolumeHireProviders : int
    {
        [EnumDescription("Provider 1")]
        Provider1 = 1,
        [EnumDescription("Provider 2")]
        Provider2 = 2
    }

    [Serializable()]
    public enum HRHolidayType : int
    {
        [EnumDescription("Type 1")]
        Type1 = 1,
        [EnumDescription("Type 2")]
        Type2 = 2,
        [EnumDescription("Type 3")]
        Type3 = 3
    }

    [Serializable()]
    public enum HolidayCalendarStatus : int
    {
        [EnumDescription("Status 1")]
        Status1 = 1,
        [EnumDescription("Status 2")]
        Status2 = 2,
        [EnumDescription("Status 3")]
        Status3 = 3
    }

    [Serializable()]
    public enum ClientWorkOrderStatus : int
    {
        [EnumDescription("Approved")]
        Approved = 1,
        [EnumDescription("Pending")]
        Pending = 2,
        [EnumDescription("Declined")]
        Declined = 3
    }

    [Serializable()]
    public enum InvoiceStatus : int
    {
        [EnumDescription("Approved")]
        Approved = 1,
        [EnumDescription("Pending")]
        Pending = 2,
        [EnumDescription("Declined")]
        Declined = 3
    }

    [Serializable()]
    public enum AlertType : int
    {
        [EnumDescription("SFA")]
        SFA = 1,
        [EnumDescription("VMS")]
        VMS = 2,
        [EnumDescription("PMS")]
        PMS = 3,
        [EnumDescription("Consultant")]
        Consultant = 4,
        [EnumDescription("Employee")]
        Employee = 5,
        [EnumDescription("Requisition")]
        Requisition = 6,
        [EnumDescription("Project")]
        Project = 7,
        [EnumDescription("Immigration")]
        Immigration = 8,
    }

    [Serializable()]
    public enum AlertStatus : int
    {
        [EnumDescription("Not Viewed")]
        NotViewed = 1,
        [EnumDescription("Viewed")]
        Viewed = 2
    }

    [Serializable()]
    public enum AttendanceType : int
    {
        [EnumDescription("Present")]
        Present = 0,
        [EnumDescription("Absent Without Reason")]
        AbsentWithoutReason = 1,
        [EnumDescription("Holiday-Religious")]
        HolidayReligious = 2,
        [EnumDescription("Holiday- Government")]
        HolidayGovernment = 3,
        [EnumDescription("Holiday-Company")]
        HolidayCompany = 4,
        [EnumDescription("Sick Leave")]
        SickLeave = 5,
        [EnumDescription("Late Start")]
        LateStart = 6,
        [EnumDescription("Leave Early")]
        LeaveEarly = 7,
        [EnumDescription("Unpaid Leave")]
        UnpaidLeave = 8,
        [EnumDescription("No work")]
        NoWork = 9,
        [EnumDescription("Suspended")]
        Suspended = 10,
        [EnumDescription("Tardy")]
        Tardy = 11,
        [EnumDescription("Annual Vacation")]
        AnnualVacation = 12,
        [EnumDescription("Workers Compensation")]
        WorkersCompensation = 13,
        [EnumDescription("Jury Duty")]
        JuryDuty = 14,
        [EnumDescription("Parental Leave")]
        ParentalLeave = 15,
        [EnumDescription("Education/Training")]
        EducationTraining = 16
    }

    [Serializable()]
    public enum DefaultSiteSetting : int
    {
        [EnumDescription("Country")]
        Country = 1,
        [EnumDescription("Currency")]
        Currency = 2,
        [EnumDescription("Language")]
        Language = 3,
        [EnumDescription("SMTP")]
        SMTP = 4,
        [EnumDescription("Admin Emial")]
        AdminEmail = 5,
        [EnumDescription("Payment Type")]//0.5
        PaymentType = 6,
        [EnumDescription("SMTP User")]
        SMTPUser = 7,
        [EnumDescription("SMTP Password")]
        SMTPassword = 8,
        [EnumDescription("SMTP Port")]
        SMTPort = 9,
        [EnumDescription("SMTP EnableSSL")]
        SMTPEnableSSL = 10,
        [EnumDescription("Tweeter UserName")]
        TweeterUserName = 11,
        [EnumDescription("Tweeter Password")]
        TweeterPassword = 12,
        [EnumDescription("Final Hire Completion Timeframe")]
        FinalHireCompletionTimeframe = 13,
        [EnumDescription("Application Edition")]
        ApplicationEdition = 14,
        [EnumDescription("Company Name")]
        CompanyName = 15,
        [EnumDescription("Req Status Change Permissions")]
        ReqStatusChangePermissions = 16,
        [EnumDescription("CandidatePortal Job Publishing")]
        CandidatePortalJobPublishing = 17,
        [EnumDescription("Resume Parser Download Link")]
        ResumeParserDownloadLink = 18,
        [EnumDescription("Automatic Email Match in Req Editor")]
        AutomaticEmailMatchinReqEditor = 19,
        [EnumDescription("EnableReq Publishing to Employee Referral Portal")]
        EmployeeReferralPortaPublishing = 20,
        [EnumDescription("EnableReq Publishing to Vendor Portal")]
        VendorPortaPublishing = 21,
        [EnumDescription("Associate requisitions with entities from module")]
        AssociateRequisitionsWithEntitiesFromModule = 22,

        //**********Code added by pravin khot on 27/April/2016*************
        [EnumDescription("Allow Vendors to Update Candidate Profiles")]
        AllowVendorstoUpdateCandidateProfiles = 23,
        [EnumDescription("Allow Candidate to be added on Multiple Requisition")]
        AllowCandidatetobeaddedonMultipleRequisition = 24,
        //*******************************END***************************************
        //**********Code added by pravin khot on 27/April/2016*************
       [EnumDescription("TimeZone")]
        TimeZone = 25,
        //*******************************END***************************************
         //**********Code added by pravin khot on 8/Aug/2016*************
       [EnumDescription("Allow Show Precise Search All Candidates")]
       AllowShowPreciseSearchAllCandidates = 26      
        //*******************************END***************************************
    }

    public enum DefaultAuditTrailSetting : int
    {
        [EnumDescription("Admin")]
        Admin = 1,
        [EnumDescription("SFA")]
        SFA = 2,
        [EnumDescription("ATS")]
        ATS = 3,
        [EnumDescription("Requisition")]
        Requisition = 4,
        [EnumDescription("Employee")]
        Employee = 5,
        [EnumDescription("Consultant")]
        Consultant = 6,
        [EnumDescription("TaskManagement")]
        TaskManagement = 7,
        [EnumDescription("Reports")]
        Reports = 8,
        [EnumDescription("My Profile")]
        MyProfile = 9,
        [EnumDescription("Emp Camp")]
        EmpCamp = 10,
        [EnumDescription("Enable Audit Trail")]
        EnableAuditTrail = 11
    }
    [Serializable()]
    public enum SettingType : int
    {
        [EnumDescription("Application Site Setting")]
        SiteSetting = 1,
        [EnumDescription("Audit Trail Setting")]
        AuditTrailSetting = 2
    }
    [Serializable]
    public enum ExpenseType : int
    {
        [EnumDescription("Guest House")]
        GuestHouse = 1,
        [EnumDescription("Travel")]
        Travel = 2,
        [EnumDescription("Tax Rate")]
        Tax = 3
    }

    public enum BasisForClassification : int
    {
        [EnumDescription("New employment")]
        NewEmployment = 1,
        [EnumDescription("New concurrent employment")]
        NewConcurrentEmployment = 2,
        [EnumDescription("Continuation")]
        Continuation = 3,
        [EnumDescription("Change of employer")]
        ChangeOfEmployer = 4,
        [EnumDescription("Change employement")]
        ChangeEmployement = 5,
        [EnumDescription("Amended petition")]
        AmendedPetition = 6
    }

    public enum RequestedAction : int
    {
        [EnumDescription("Notify the office in part 4")]
        Notifytheofficeinpart4 = 1,
        [EnumDescription("Amend the stay of the person(s)")]
        Amendthestayoftheperson = 2,
        [EnumDescription("Change the person(s)' status")]
        Changethepersonstatus = 3,
        [EnumDescription("Extend the status of a nonimmigrant")]
        Extendthestatusofanonimmigrant = 4,
        [EnumDescription("Extend the stay of the person(s)")]
        Extendthestayoftheperson = 5,
        [EnumDescription("Change status to a nonimmigrant classification")]
        Changestatustoanonimmigrantclassification = 6
    }

    public enum GoalStatus : int
    {
        [EnumDescription("New")]
        New = 1,
        [EnumDescription("Rated")]
        Rated = 2,
        [EnumDescription("Not Rated")]
        NotRated = 3
    }

    public enum JobType : int
    {
        [EnumDescription("Candidate Job Alert")]
        CandidateJobAlert = 1,
        [EnumDescription("Candidate Resume Update")]
        CandidateResumeUpdate = 2,
        [EnumDescription("Candidate Birthday")]
        CandidateBirthday = 3
    }

    public enum OfferLetterStatus : int
    {
        [EnumDescription("Sent")]
        Sent = 1,
        [EnumDescription("Draft")]
        Draft = 2
    }

    public enum MemberCapability : int
    {
        [EnumDescription("Member Functional Capability")]
        Functional = 1,
        [EnumDescription("Member Industry Capability")]
        Industry = 2,
        [EnumDescription("Member Technical Capability")]
        Technical = 3,
        [EnumDescription("Member Educational Capability")]
        Educational = 4,
        [EnumDescription("Member Salary Capability")]
        Salary = 5
    }

    public enum ResumeBuilderOption : int
    {
        [EnumDescription("None")]
        None = 0,
        [EnumDescription("Step By Step Resume Builder")]
        StepByStep = 1,
        [EnumDescription("Copy/Paste Resume")]
        CopyPastResume = 2,
        [EnumDescription("Upload Resume")]
        UploadResume = 3
    }



}