﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

using TPS360.Common.Shared;


namespace TPS360.Common.Helper
{
    public class WorkFlowMailHelper
    {
        #region Member Variables
        
        WorkFlowTitle _currentWorkFlowTitle;
        //int _memberId = 0;
        MemberType _memberType=MemberType.Candidate;

        #endregion Member Variables

        #region Properties

        public MemberType CurrentMemberType
        {
            get { return _memberType; }
            set { _memberType = value; }
        }

        public string ApplicantName
        {
            get;
            set;
        }

        public string ApplicantEmail
        {
            get;
            set;
        }

        public string ApplicantPassword
        {
            get;
            set;
        }

        public string HotListName
        {
            get;
            set;
        }

        public string TestName
        {
            get;
            set;
        }

        public string TestScore
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        public WorkFlowMailHelper(WorkFlowTitle workFlowTitle)
        {
            _currentWorkFlowTitle = workFlowTitle;
        }

        public void SendAlertByEmail(List<string> emailIds)
        {
            try
            {
                string strMemberIds = String.Empty;
                EmailManager newMailManager = new EmailManager();
                newMailManager.From = "faisal@tps360.com";
                foreach (string strEmail in emailIds)
                {
                    if (!string.IsNullOrEmpty(strEmail))
                    {
                        newMailManager.To.Add(strEmail);
                    }
                }
                newMailManager.Subject = GetAlertSubject();
                newMailManager.Body = GetAlertBody();
                newMailManager.Send();

            }
            catch { }
        }

        public void SendAlertBySMS()
        {
        }

        private string GetAlertBody()
        {
            switch (_currentWorkFlowTitle)
            {
                case WorkFlowTitle.ATSShouldAssignedManagersForCandidateAlertedWhenCandidateAddedHotList:
                    return GetAlertAddToHotList();
                case WorkFlowTitle.ATSShouldAssignedManagersForCandidateAlertedWhenCandidateConvertedToConsultant:
                    return GetAlertConvertedToConsultant();
                case WorkFlowTitle.ATSShouldAssignedManagersForCandidateAlertedWhenCandidateUpdatesResume:
                    return GetAlertResumeUpdate();
                case WorkFlowTitle.ConShouldAssignedManagersForConsultantAlertedWhenConsultantAddedToHotList:
                    return GetAlertAddToHotList();
                case WorkFlowTitle.RequisitionManagementShouldEmployeesAlertedIfUnAssignedFromRequisition:
                    return GetAlertForUnAssignEmployeeFromRequisition();
                case WorkFlowTitle.ConDuringAddingConsultantSystemBeAutomaticallyEmailedTheirUserID:
                    return GetAlertConsultantCreated();
                case WorkFlowTitle.RequisitionManagementWhenRecruiterSubmitsConsultantCandidateClientForRequisitionAlerted:
                    return GetAlertApplicantSubmitted();
                case WorkFlowTitle.RequisitionManagementShouldAssignedManagersForCandidateAlertedWhenCandidateCompletesInitialInterviewForRequisition:
                    return GetAlertApplicantCompletesInitialInterview();
                case WorkFlowTitle.RequisitionManagementShouldAssignedRecruitersRequisitionAlertedIfFinalHireCompletedByDate:
                    return GetAlertRequisitionFinalHireDateWithinNextDay();
                case WorkFlowTitle.RequisitionManagementShouldAssignedRecruitersForRequisitionAlertedIfFinalHiretoBeCompletedByDatePassed:
                    return GetAlertRequisitionFinalHireDateHasPassed();
                case WorkFlowTitle.RequisitionManagementShouldAssignedRecruitersForRequisitionAlertedWhenRecruiterAddsCandidatesHiringMatrix:
                    return GetAlertRequisitionRecruiterAddApplicantToHiringMatrix();
                default:
                    return string.Empty;
            }
        }

        private string GetAlertSubject()
        {     
            switch (_currentWorkFlowTitle)
            {
                case WorkFlowTitle.ATSShouldAssignedManagersForCandidateAlertedWhenCandidateAddedHotList:
                    return "Alert : Candidate Added To a Hot List";
                case WorkFlowTitle.ATSShouldAssignedManagersForCandidateAlertedWhenCandidateConvertedToConsultant:
                    return "Alert : Candidate Converted To Consultant";
                case WorkFlowTitle.ATSShouldAssignedManagersForCandidateAlertedWhenCandidateUpdatesResume:
                    return "Alert : Candidate Resume Updated";
                case WorkFlowTitle.ConShouldAssignedManagersForConsultantAlertedWhenConsultantAddedToHotList:
                    return "Alert : Consultant Added To a Hot List";
                case WorkFlowTitle.RequisitionManagementShouldEmployeesAlertedIfUnAssignedFromRequisition:
                    return "Alert : Un-assigned from requisition";
                case WorkFlowTitle.ConDuringAddingConsultantSystemBeAutomaticallyEmailedTheirUserID:
                    return "Alert : Consultant Created";
                case WorkFlowTitle.RequisitionManagementWhenRecruiterSubmitsConsultantCandidateClientForRequisitionAlerted:
                    return "Alert : Applicant submitted to the client";
                case WorkFlowTitle.RequisitionManagementShouldAssignedManagersForCandidateAlertedWhenCandidateCompletesInitialInterviewForRequisition:
                    return "Alert : Applicant completes the Initial Interview for a requisition";
                case WorkFlowTitle.RequisitionManagementShouldAssignedRecruitersRequisitionAlertedIfFinalHireCompletedByDate:
                    return "Alert : Final Hire to be Completed by Date is within the next day";
                case WorkFlowTitle.RequisitionManagementShouldAssignedRecruitersForRequisitionAlertedIfFinalHiretoBeCompletedByDatePassed:
                    return "Alert : Final Hire to be Completed by Date has passed";
                case WorkFlowTitle.RequisitionManagementShouldAssignedRecruitersForRequisitionAlertedWhenRecruiterAddsCandidatesHiringMatrix:
                    return "Alert : Recruiter added applicant to the Hiring Matrix";
                default:
                    return string.Empty;
            }
        }
       
        #region Common

        private string GetStyle()
        {
            StringBuilder strStyle = new StringBuilder();
            strStyle.Append(@"  <link href=http://www.delphistaffing.net/StyleSheets/Client/styles.css rel=stylesheet type=text/css />
                                <style type=text/css>
                                TD.JSBorderLine
                                {
                                    background-color:#D3D3D3;
                                }
                                TD.JSFormHeading
                                {
                                    BACKGROUND-COLOR: #006699; font-weight:bold; Color:white;font-family:Verdana; 
                                    text-align:left;
                                    font-size:12px;
                                }
                                .JSFormHeading
                                {
                                    BACKGROUND-COLOR: #006699; font-weight:bold; Color:white;font-family:Verdana; 
                                    text-align:center;
                                    font-size:12px;
                                }
                                TD.NewFormValue
                                {
                                    BACKGROUND-COLOR: white;
                                    font-weight:normal;
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                .NewFormValue
                                {
                                    BACKGROUND-COLOR: white;
                                    font-weight:normal;
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                .NewFormValue1
                                {
                                    BACKGROUND-COLOR: #aa97f0;
                                    font-weight:normal;
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                .NewFormValue2
                                {
                                    BACKGROUND-COLOR: #cce599
                                    font-weight:normal;
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                .AlternateItem
                                {
                                    BACKGROUND-COLOR: #e1f2fb;
                                    font-weight:normal; 
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                TD.JSFormField
                                {
                                    BACKGROUND-COLOR: #e1f2fb;
                                    font-weight:bold; 
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                .JSFormField
                                {
                                    BACKGROUND-COLOR: #e1f2fb;
                                    font-weight:normal; 
                                    color:Black;
                                    font-family:Verdana; 
                                    font-size:11px;
                                }
                                </style>");
            return strStyle.ToString();
        }

        #endregion

        #region Cosultant Alert

        public String GetAlertConsultantCreated()
        {
            StringBuilder strHTMLText = new StringBuilder();
            //strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Consultant Created</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Name</td>");
            strHTMLText.Append("                                <td>" + ApplicantName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>User Id</td>");
            strHTMLText.Append("                                <td>" + ApplicantEmail + "</td>");
            strHTMLText.Append("                            </tr>");


            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Password</td>");
            strHTMLText.Append("                                <td>" + ApplicantPassword + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        #endregion Cosultant Alert

        #region ATS Alerts

        public String GetAlertResumeUpdate()
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");
            
            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Resume Updated</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>"+CurrentMemberType.ToString()+" Name</td>");
            strHTMLText.Append("                                <td>" + ApplicantName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Email</td>");
            strHTMLText.Append("                                <td>" + ApplicantEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertAppliedForJob(string strApplicantName, string strApplicantEmail, string strRequisitionTitle)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Applied For a Job</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Name</td>");
            strHTMLText.Append("                                <td>" + strApplicantName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Email</td>");
            strHTMLText.Append("                                <td>" + strApplicantEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Requisition</td>");
            strHTMLText.Append("                                <td>" + strRequisitionTitle + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertInterviewFeedback(string strApplicantName, string strApplicantEmail, DateTime dtInterviewTime, string strLocation, string strJob, string strFeedback)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Feedback For Interview</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Name</td>");
            strHTMLText.Append("                                <td>" + strApplicantName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Email</td>");
            strHTMLText.Append("                                <td>" + strApplicantEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td nowrap>Interview Date & Time:</td>");
            strHTMLText.Append("                                <td>" + dtInterviewTime.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=21% nowrap>Location:</td>");
            strHTMLText.Append("                                <td>" + strLocation + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=21% nowrap>For Job:</td>");
            strHTMLText.Append("                                <td>" +strJob + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Feedback</td>");
            strHTMLText.Append("                                <td>" + strFeedback + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertCompleteTest()
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Completed a Test</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Name</td>");
            strHTMLText.Append("                                <td>" + ApplicantName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Email</td>");
            strHTMLText.Append("                                <td>" + ApplicantEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Test Name</td>");
            strHTMLText.Append("                                <td>" + TestName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Score</td>");
            strHTMLText.Append("                                <td>" + TestScore + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");


            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertReferFriend(string strApplicantName, string strApplicantEmail, string strFriendName, string strFriendEmail)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Refere a Friend</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Name</td>");
            strHTMLText.Append("                                <td>" + strApplicantName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Email</td>");
            strHTMLText.Append("                                <td>" + strApplicantEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Friend Name</td>");
            strHTMLText.Append("                                <td>" + strFriendName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Friend Email</td>");
            strHTMLText.Append("                                <td>" + strFriendEmail + "</td>");
            strHTMLText.Append("                            </tr>");


            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertChangeAvailability(string strApplicantName,string strApplicantEmail, string strCurrentAvailability)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Change Availability Status</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Name</td>");
            strHTMLText.Append("                                <td>" + strApplicantName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Email</td>");
            strHTMLText.Append("                                <td>" + strApplicantEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            
            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Current Availability Status</td>");
            strHTMLText.Append("                                <td>" + strCurrentAvailability + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertSubmitReference(string strApplicantName, string strApplicantEmail, string strReferenceType, string strReferenceName, string strReferenceEmail)
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Add New Reference</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Name</td>");
            strHTMLText.Append("                                <td>" + strApplicantName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Email</td>");
            strHTMLText.Append("                                <td>" + strApplicantEmail + "</td>");
            strHTMLText.Append("                            </tr>");


            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Reference Type</td>");
            strHTMLText.Append("                                <td>" + strReferenceType + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Reference Name</td>");
            strHTMLText.Append("                                <td>" + strReferenceName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Reference Email</td>");
            strHTMLText.Append("                                <td>" + strReferenceEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertAddToHotList()
        {
            StringBuilder strHTMLText = new StringBuilder();
            //strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Added To a HotList</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Name</td>");
            strHTMLText.Append("                                <td>" + ApplicantName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>" + CurrentMemberType.ToString() + " Email</td>");
            strHTMLText.Append("                                <td>" + ApplicantEmail + "</td>");
            strHTMLText.Append("                            </tr>");


            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Hot List Title</td>");
            strHTMLText.Append("                                <td>" + HotListName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertConvertedToConsultant()
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append(GetStyle());

            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            //Add header image
            //strHTMLText.Append("<tr>");
            //if (isPreview == false)
            //{
            //    strHTMLText.Append("<td align=left><img src=\"cid:Email_Banner\" style='height: 83px;width:100%;'/></td>");
            //}
            //else
            //{
            //    strHTMLText.Append("<td align=left><img src='" + strLogoFileTop + "' style='height: 83px;width:100%;'/></td>");
            //}
            //strHTMLText.Append("<tr>");
            //strHTMLText.Append("    <td></br></td>");
            //strHTMLText.Append("</tr>");
            //strHTMLText.Append("</tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Converted To Consultant</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Name</td>");
            strHTMLText.Append("                                <td>" + ApplicantName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Candidate Email</td>");
            strHTMLText.Append("                                <td>" + ApplicantEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        #endregion ATS Alerts

        #region Requisition Email Alerts

        public string GetAlertForUnAssignEmployeeFromRequisition()
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Unassigned employee from requisition</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Name</td>");
            strHTMLText.Append("                                <td>" + ApplicantName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>User Id</td>");
            strHTMLText.Append("                                <td>" + ApplicantEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public String GetAlertApplicantSubmitted()
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Applicant Submitted</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Name</td>");
            strHTMLText.Append("                                <td>" + ApplicantName + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>User Id</td>");
            strHTMLText.Append("                                <td>" + ApplicantEmail + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public string GetAlertApplicantCompletesInitialInterview()
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Applicant completes the Initial Interview for a requisition</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public string GetAlertRequisitionFinalHireDateWithinNextDay()
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Requisition Final Hire to be Completed by Date is within the next day</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public string GetAlertRequisitionFinalHireDateHasPassed()
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Requisition Final Hire to be Completed by Date has passed</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        public string GetAlertRequisitionRecruiterAddApplicantToHiringMatrix()
        {
            StringBuilder strHTMLText = new StringBuilder();
            strHTMLText.Append("<table width=780px border=0 align=center cellpadding=0 cellspacing=0>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td colspan=4>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=0 cellpadding=5>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td class=JSFormHeading>Application Work Flow Alert</td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td height=5 bgcolor=#FFCC00><SPACER height=1 width=1 type=block></td>");
            strHTMLText.Append("		        </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");
            strHTMLText.Append("    <tr>");
            strHTMLText.Append("        <td>");
            strHTMLText.Append("            <table width=100% border=0 cellspacing=1 cellpadding=0>");
            strHTMLText.Append("                <tr>");
            strHTMLText.Append("                    <td valign=top align=left width=100%>");
            strHTMLText.Append("                        <table width=100% border=0 cellspacing=1 cellpadding=5>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Type</td>");
            strHTMLText.Append("                                <td>Recruiter added applicant to the Hiring Matrix</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                            <tr>");
            strHTMLText.Append("                                <td width=100% nowrap>Alert Time</td>");
            strHTMLText.Append("                                <td>" + DateTime.Now.ToLongDateString() + "</td>");
            strHTMLText.Append("                            </tr>");

            strHTMLText.Append("                        </table>");
            strHTMLText.Append("                    </td>");
            strHTMLText.Append("                </tr>");
            strHTMLText.Append("            </table>");
            strHTMLText.Append("        </td>");
            strHTMLText.Append("    </tr>");

            strHTMLText.Append("</table>");

            return strHTMLText.ToString();
        }

        #endregion

        #endregion Methods
    }
}