﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IOnBoardingOffBoardingChecklistDataAccess

    public interface IOnBoardingOffBoardingChecklistDataAccess
    {
        OnBoardingOffBoardingChecklist Add(OnBoardingOffBoardingChecklist onBoardingOffBoardingChecklist);

        OnBoardingOffBoardingChecklist Update(OnBoardingOffBoardingChecklist onBoardingOffBoardingChecklist);

        OnBoardingOffBoardingChecklist GetById(int id);

        IList<OnBoardingOffBoardingChecklist> GetAll();

        IList<OnBoardingOffBoardingChecklist> GetAllParent();

        IList<OnBoardingOffBoardingChecklist> GetAllByParentId(int parentId);

        PagedResponse<OnBoardingOffBoardingChecklist> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}