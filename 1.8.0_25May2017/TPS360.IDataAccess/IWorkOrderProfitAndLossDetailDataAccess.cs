﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IWorkOrderProfitAndLossDetailDataAccess

    public interface IWorkOrderProfitAndLossDetailDataAccess
    {
        WorkOrderProfitAndLossDetail Add(WorkOrderProfitAndLossDetail workOrderProfitAndLossDetail);

        WorkOrderProfitAndLossDetail Update(WorkOrderProfitAndLossDetail workOrderProfitAndLossDetail);

        WorkOrderProfitAndLossDetail GetById(int id);

        IList<WorkOrderProfitAndLossDetail> GetAll();

        bool DeleteById(int id);

        PagedResponse<WorkOrderProfitAndLossDetail> GetPaged(PagedRequest request);
    }

    #endregion
}