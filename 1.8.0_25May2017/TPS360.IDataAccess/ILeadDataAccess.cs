﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ILeadDataAccess

    public interface ILeadDataAccess
    {
        Lead Add(Lead lead);

        Lead Update(Lead lead);

        bool UpdateStatus(int leadId, SalesStatus status);

        Lead GetById(int id);

        PagedResponse<Lead> GetPaged(PagedRequest request);

        IList<Lead> GetAll();

        bool DeleteById(int id);

        bool DeleteFromMemberById(int id, int memberId);
    }

    #endregion
}

