﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberImmigrationDataAccess

    public interface IMemberImmigrationDataAccess
    {
        MemberImmigration Add(MemberImmigration memberImmigration);

        MemberImmigration Update(MemberImmigration memberImmigration);

        MemberImmigration GetById(int id);

        MemberImmigration GetByMemberId(int memberId);

        IList<MemberImmigration> GetAll();

        PagedResponse<MemberImmigration> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}