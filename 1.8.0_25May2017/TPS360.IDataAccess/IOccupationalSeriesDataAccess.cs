﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:ICountryDataAccess.cs
    Description: This page is used for Countries  functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetCountryIdByCountryCode() to assist 
                                                                Resume parser team
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using System;

namespace TPS360.DataAccess
{
    #region IOccupationalSeriesDataAccess

    public interface IOccupationalSeriesDataAccess
    {


        IList<OccupationalSeries> GetAll();

        IList<OccupationalSeries> GetAllBySearch(string keyword, int count);
        OccupationalSeries GetbyId(int id);

            }

    #endregion
}