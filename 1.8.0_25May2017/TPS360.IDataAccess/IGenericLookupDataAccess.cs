﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IGenericLookupDataAccess.cs
    Description:  
    Created By:  
    Created On:  
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
 *  1.0                 5/Dec/2015          Prasanth Kumar G    Introduced Function for Get interviewe Response
 *  2.0                 25/May/2016         pravin khot          GetByDescription
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IGenericLookupDataAccess

    public interface IGenericLookupDataAccess
    {
        GenericLookup Add(GenericLookup genericLookup);

        GenericLookup Update(GenericLookup genericLookup);

        GenericLookup GetById(int id);

        GenericLookup GetByDescription(string location);//added by pravin khot on 25/May/2016

        IList<GenericLookup> GetAll();

        PagedResponse<GenericLookup> GetPaged(PagedRequest request);

        IList<GenericLookup> GetAllByLookupType(LookupType lookupType);

        IList<GenericLookup> GetAllByLookupType(string lookupTypes);// For Optimization by Vignesh

        IList<GenericLookup> GetAllByLookupTypeAndName(LookupType lookupType,string lookupName);

        bool DeleteById(int id);

        string GetLookUPNamesByIds(string Ids);

        IList<GenericLookup> GetByIds(string IDs);
        //Line introduced by Prasanth on 5/Dec/2015 
        IList<GenericLookup> InterviewAssessment_GetBy_InterviewId_InterviewerEmail(int InterviewId, string Email);
    }

    #endregion
}