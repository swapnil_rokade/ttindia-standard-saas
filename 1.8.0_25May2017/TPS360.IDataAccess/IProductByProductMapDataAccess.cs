﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IProductByProductMapDataAccess

    public interface IProductByProductMapDataAccess
    {
        ProductByProductMap Add(ProductByProductMap productByProductMap);

        ProductByProductMap Update(ProductByProductMap productByProductMap);

        ProductByProductMap GetById(int id);

        IList<ProductByProductMap> GetAll();

        IList<ProductByProductMap> GetAllByProductId(int productId);

        bool DeleteById(int id);

        bool DeleteByProductId(int productId);
    }

    #endregion
}