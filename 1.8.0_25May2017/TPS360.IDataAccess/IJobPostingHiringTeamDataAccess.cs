﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IJobPostingHiringTeamDataAccess

    public interface IJobPostingHiringTeamDataAccess
    {


        void AddMultipleRecruiters(string MemberIds, int JobpostingId, int CreatorId, string employeeType);

        void AddMultipleRecruitersWithOpening(int MemberId, int AssignOpenings, int JobpostingId, int CreatorId, string employeeType, bool IsPrimary); //added by pravin khot on 2/March/2017

        IList<JobPostingHiringTeam> GetAllRecruiterGroupByJobPostingId(int jobPostingId); //added by pravin khot on 2/March/2017

        JobPostingHiringTeam Add(JobPostingHiringTeam jobPostingHiringTeam);

        JobPostingHiringTeam Update(JobPostingHiringTeam jobPostingHiringTeam);

        JobPostingHiringTeam GetById(int id);

        JobPostingHiringTeam GetByMemberId(int memberId, int jobPostingId);

        IList<JobPostingHiringTeam> GetAll();
        
        IList<JobPostingHiringTeam> GetAllByJobPostingId(int jobPostingId);

        bool DeleteById(int id);

        bool DeleteByJobPostingId(int jobPostingId);

        IList<JobPostingHiringTeam> GetAllByMemberId(int memberId);
    }

    #endregion
}