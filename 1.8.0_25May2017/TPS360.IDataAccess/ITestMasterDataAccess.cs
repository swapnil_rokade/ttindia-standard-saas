﻿using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ITestMasterDataAccess

    public interface ITestMasterDataAccess
    {
        TestMaster Add(TestMaster testMaster);

        TestMaster Update(TestMaster testMaster);

        TestMaster GetById(int id);

        TestMaster GetByName(string name);

        IList<TestMaster> GetAll();

        bool DeleteById(int id);

        PagedResponse<TestMaster> GetPaged(PagedRequest pageRequest);

        IList<TestMaster> GetAllTestMasterByTypeAndPublishStatus(int typeId, int publishStatusId);

        IList<TestMaster> GetAllTestMasterByName(string testName);

    }

    #endregion
}