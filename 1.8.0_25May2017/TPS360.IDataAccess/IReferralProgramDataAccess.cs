﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IReferralProgramDataAccess

    public interface IReferralProgramDataAccess
    {
        ReferralProgram Add(ReferralProgram referralProgram);

        ReferralProgram Update(ReferralProgram referralProgram);

        ReferralProgram GetById(int id);

        ReferralProgram GetByTitle(string title);

        ReferralProgram GetCurrentDescription();

        IList<ReferralProgram> GetAll();

        PagedResponse<ReferralProgram> GetPaged(PagedRequest request);

        bool DeleteById(int id);

       
    }

    #endregion
}