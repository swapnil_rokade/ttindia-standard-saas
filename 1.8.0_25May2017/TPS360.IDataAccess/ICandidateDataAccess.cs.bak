﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ICandidateDataAccess.cs
    Description: This page defenes interface for Candidate Data Access.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ------------------------------------------------------------------------------------------------------------------------------    
    0.1            Dec-02-2008        Shivanand        Defect #8745; New parameter "strState" is added in the method signature of "GetAllOnSearch()".
                                                                     New parameter "strState" is added in the method signature of "GetSearchQuery()".   
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;
using System.Collections.Generic;

namespace TPS360.DataAccess
{
    #region ICandidateDataAccess

    public interface ICandidateDataAccess
    {
        Candidate GetById(int id);

        IList<Candidate> GetAllCandidateByMemberId(int memberId);

        Candidate GetByIdForHR(int id);

        CandidateOverviewDetails GetCandidateOverviewDetails(int CandidateId);
        PagedResponse<Candidate> GetPaged(PagedRequest request);

        PagedResponse<Candidate> GetPagedForSubmittedCandidate(PagedRequest request);

        PagedResponse<Candidate> GetPagedForReport(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
        int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
        int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager);


        PagedResponse<DynamicDictionary> GetPagedForReportBySelectedColumn(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
        int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
        int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, IList<string> CheckedList);


        PagedResponse<Candidate> GetPagedForSavedQuery(PagedRequest request);
        //custom
        PagedResponse<Candidate> GetAllOnSearch(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType); //0.1  // 9372

        PagedResponse<Candidate> GetAllOnSearchForPrecise(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType, string Education, string WorkSchedule, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddWhereClause, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating);

        PagedResponse<Candidate> GetAllOnSearchForPrecises(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType, string Education, string WorkSchedule, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddWhereClause, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string SourceID);

        Candidate GetOnSearchForPrecise_Sub(int CandidateId);

        IList<Candidate> GetAllOnSearchForSearchAgent(string allKeyWords, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string hotListId, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType, string Education, string WorkSchedule, string HiringStatus, int JobPostingId);

        PagedResponse<Candidate> GetAllOnSearchForSavedQuery(PagedRequest request, string whereClause, string memberId, string rowPerPage);
        //custom
        string GetSearchQuery(string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string memberPositionId, string isMyList, string minExpYr, string maxExpYr, string strState, string applicantType, string internalRating);  //0.1
        string GetSearchQueryForPrecise(string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string memberPositionId, string isMyList, string minExpYr, string maxExpYr, string strState, string applicantType, string Education, string WorkSchedule, string HiringStatus, string CurrentCompany, string CompanyStaus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddWhereClause, bool isMile, string AssignedManagerId, string Salarytype, string internalRating, string SourceID);
        int GetCandidateCountByMemberID(int MemberId);

        int GetCandidateIdbyPrimaryEmail(string primaryEmail);

        void GetMemberUpdateCreatorId(int memberid,int creatorid);

        bool CheckCandidateDuplication(string firstName, string lastName, string mobileNo, DateTime dob, string email, int IdCardLookUpId, string IdCardDetail);

    }

    #endregion
}