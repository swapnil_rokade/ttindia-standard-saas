﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IHolidayCalendarDetailDataAccess

    public interface IHolidayCalendarDetailDataAccess
    {
        HolidayCalendarDetail Add(HolidayCalendarDetail holidayCalendarDetail);

        HolidayCalendarDetail Update(HolidayCalendarDetail holidayCalendarDetail);

        HolidayCalendarDetail GetById(int id);

        IList<HolidayCalendarDetail> GetAll();

        IList<HolidayCalendarDetail> GetAllByHolidayCalendarId(int holidayCalendarId);

        bool DeleteById(int id);

        bool DeleteByHolidayCalendarId(int holidayCalendarId);
    }

    #endregion
}