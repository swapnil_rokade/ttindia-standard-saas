﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IInterviewDataAccess.cs
    Description:  
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
   
 *  0.1              10/Dec/2015            pravin khot         Introduced by SuggestedInterviewer_Id
 *  0.2               4/May/2016            pravin khot         Introduced by CancelById,UpdateIcsCodeById
 *  0.3               1/Mar/2017            Sumit Sonawane      Introduced by UpdateIntNoShow_ById
 * ----------------------------------------------------------------------------------------------------------------------------------------------
*/




using System.Collections.Generic;
using System.Collections;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IInterviewDataAccess

    public interface IInterviewDataAccess
    {
        Interview Add(Interview interview);

        Interview Update(Interview interview);

        Interview GetById(int id);

        IList<Interview> GetAll();

        bool DeleteById(int id);
        bool CancelById(int id); //added by pravin khot on 4/May/2016
        bool UpdateIcsCodeById(int id, string ics);//added by pravin khot on 4/May/2016

        IList<Interview> GetAllByJobPostingId(int jobPostingId);

        IList<Interview> GetInterviewDateByMemberId(int MemberId);
        
        IList<Interview> GetInterviewByMemberIdAndJobPostingID(int MemberId, int JobPostingID);

        int GetContactIdByJobPostingId(int JobPostingId);

        string GetInterviewTemplate(int MemberID, int InterviewID, string Template_Type, int CreatorId);
        //******************************code introduced by Pravin khot 10/Dec/2015**************************

        string SuggestedInterviewer_Id(int InterviewerId);

        //*************************************************************************
        //Code added by Sumit Sonawane on 1/Mar/2017 ***********
        bool UpdateIntNoShow_ById(int id);
        bool UpdateIntCompleted_ById(int id);
        Interview AddInterviewStatusDetails(Interview InterviewStatusDetails);
        //***************************END********************
    }

    #endregion
}