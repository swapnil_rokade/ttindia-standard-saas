﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ILeadTeamDataAccess

    public interface ILeadTeamDataAccess
    {
        LeadTeam Add(LeadTeam leadTeam);

        LeadTeam Update(LeadTeam leadTeam);

        LeadTeam GetById(int id);

        IList<LeadTeam> GetAllByLeadId(int leadId);

        IList<LeadTeam> GetAll();

        IList<LeadTeam> GetAllByMemberId(int memberId);

        bool DeleteById(int id);

        bool DeleteByLeadId(int leadId);

        bool DeleteByLeadIdAndMemberId(int leadId, int memberId);
    }

    #endregion
}

