﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberInterviewAndSelectionDataAccess

    public interface IMemberInterviewAndSelectionDataAccess
    {
        MemberInterviewAndSelection Add(MemberInterviewAndSelection memberInterviewAndSelection);

        MemberInterviewAndSelection Update(MemberInterviewAndSelection memberInterviewAndSelection);

        MemberInterviewAndSelection GetById(int id);

        MemberInterviewAndSelection GetByMemberId(int memberId);

        IList<MemberInterviewAndSelection> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}