﻿
/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IMemberSourceHistoryDataAccess.cs
    Description: his is used for Member Source History data access
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
   
 --------------------------------------------------------------------------------------------------------------------------------------------        
*/



using System.Collections.Generic;

 
using System.Collections;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using TPS360.Common.Helper;
namespace TPS360.DataAccess
{
    #region IMemberSourceHistoryDataAccess

    public interface IMemberSourceHistoryDataAccess
    {
        MemberSourceHistory Add(MemberSourceHistory membersourcehistory);

        MemberSourceHistory Update(MemberSourceHistory membersourcehistory);

        MemberSourceHistory GetById(int id);

        IList<MemberSourceHistory> GetAll();

        bool DeleteById(int id);

        bool IsCandidateSourceExpired(string primaryEmail);

        PagedResponse<MemberSourceHistory> GetPaged(PagedRequest request);

    }

    #endregion
}