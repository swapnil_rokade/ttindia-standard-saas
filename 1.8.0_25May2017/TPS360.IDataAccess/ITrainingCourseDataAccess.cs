﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ITrainingCourseDataAccess

    public interface ITrainingCourseDataAccess
    {
        TrainingCourse Add(TrainingCourse trainingCourse);

        TrainingCourse Update(TrainingCourse trainingCourse);

        TrainingCourse GetById(int id);

        IList<TrainingCourse> GetAll();

        PagedResponse<TrainingCourse> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}