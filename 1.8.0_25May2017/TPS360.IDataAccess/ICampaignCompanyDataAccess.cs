﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ICampaignCompanyDataAccess

    public interface ICampaignCompanyDataAccess
    {
        CampaignCompany Add(CampaignCompany campaignCompany);

        CampaignCompany Update(CampaignCompany campaignCompany);

        bool UpdateStatus(int campaignCompanyId, CampaignCompanyStatus status);

        bool UpdatePostMailLabelStatus(int campaignCompanyId, bool enable);

        bool RemoveFromCampaign(int campaignCompanyId, string comments);

        CampaignCompany GetById(int id);

        IList<CampaignCompany> GetAll();

        IList<CampaignCompany> GetAllByCampaignId(int campaignId);

        PagedResponse<CampaignCompany> GetPaged(PagedRequest request);

        bool DeleteById(int id);

        bool DeleteByCampaignId(int campaignId);

        bool DeleteByCampaignIdAndCompanyId(int campaignId, int companyId);
    }

    #endregion
}

