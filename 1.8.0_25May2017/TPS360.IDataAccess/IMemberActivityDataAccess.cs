﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberActivityDataAccess

    public interface IMemberActivityDataAccess
    {
        MemberActivity Add(MemberActivity memberActivity);

        MemberActivity Update(MemberActivity memberActivity);

        MemberActivity GetById(int id);
        
        IList<MemberActivity> GetAll();

        PagedResponse<MemberActivity> GetPaged(PagedRequest request);

        IList<MemberActivity> GetAllByCreatorId(int creatorId);

        bool DeleteById(int id);
    }

    #endregion
}