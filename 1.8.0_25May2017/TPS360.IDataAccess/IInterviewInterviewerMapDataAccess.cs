﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IInterviewInterviewerMapDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            5/May/2016          pravin khot               New added-GetAllsuggestedInterviewId
  * * -------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IInterviewInterviewerMapDataAccess

    public interface IInterviewInterviewerMapDataAccess
    {
        InterviewInterviewerMap Add(InterviewInterviewerMap InterviewerMap);

        InterviewInterviewerMap AddSuggestedInterviewer(InterviewInterviewerMap InterviewerMap); //Code added by pravin khot on 11/Dec/2015

        InterviewInterviewerMap Update(InterviewInterviewerMap InterviewerMap);

        InterviewInterviewerMap GetById(int id);

        IList<InterviewInterviewerMap> GetAll();

        IList<InterviewInterviewerMap> GetAllInterviewersByInterviewId(int InterviewId);

        IList<InterviewInterviewerMap> GetAllsuggestedInterviewId(int InterviewId); //Code added by pravin khot on 5/May/2015

        bool DeleteById(int id);

        bool DeleteByInterviewerId(int interviewerId);

        bool DeleteByInterviewId(int interviewId);

        string GetInterviersNameByInterviewId(int InterviewId);
    }

    #endregion
}