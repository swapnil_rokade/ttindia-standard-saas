﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberTrainingDataAccess

    public interface IMemberTrainingDataAccess
    {
        MemberTraining Add(MemberTraining memberTraining);

        MemberTraining Update(MemberTraining memberTraining);

        MemberTraining GetById(int id);

        IList<MemberTraining> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}