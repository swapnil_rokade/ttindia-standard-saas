﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ITrainingProgramTraineeMapDataAccess

    public interface ITrainingProgramTraineeMapDataAccess
    {
        TrainingProgramTraineeMap Add(TrainingProgramTraineeMap trainingProgramTraineeMap);

        TrainingProgramTraineeMap Update(TrainingProgramTraineeMap trainingProgramTraineeMap);

        TrainingProgramTraineeMap GetById(int id);

        TrainingProgramTraineeMap GetByMemberIdAndTrainingProgramId(int memberId, int trainingProgramId);

        IList<TrainingProgramTraineeMap> GetAll();

        IList<TrainingProgramTraineeMap> GetAllTrainingProgramTraineeMapByMemberId(int memberId);

        PagedResponse<TrainingProgramTraineeMap> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}