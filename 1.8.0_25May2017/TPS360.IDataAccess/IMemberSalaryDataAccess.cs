﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberSalaryDataAccess

    public interface IMemberSalaryDataAccess
    {
        MemberSalary Add(MemberSalary memberSalary);

        MemberSalary Update(MemberSalary memberSalary);

        MemberSalary GetById(int id);

        MemberSalary GetByMemberId(int memberId);

        IList<MemberSalary> GetAll();

        PagedResponse<MemberSalary> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}