﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IWorkProfileDataAccess

    public interface IWorkProfileDataAccess
    {
        WorkProfile Add(WorkProfile workProfile);

        WorkProfile Update(WorkProfile workProfile);

        WorkProfile GetById(int id);

        WorkProfile GetByMemberId(int id);

        IList<WorkProfile> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}