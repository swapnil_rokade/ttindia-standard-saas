﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ITrainingCourseModuleDataAccess

    public interface ITrainingCourseModuleDataAccess
    {
        TrainingCourseModule Add(TrainingCourseModule trainingCourseModule);

        TrainingCourseModule Update(TrainingCourseModule trainingCourseModule);

        TrainingCourseModule GetById(int id);

        IList<TrainingCourseModule> GetAllParent();

        IList<TrainingCourseModule> GetAll();

        bool DeleteById(int id);

        PagedResponse<TrainingCourseModule> GetPaged(PagedRequest request);
    }

    #endregion
}