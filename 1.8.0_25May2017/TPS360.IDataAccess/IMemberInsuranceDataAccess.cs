﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberInsuranceDataAccess

    public interface IMemberInsuranceDataAccess
    {
        MemberInsurance Add(MemberInsurance memberInsurance);

        MemberInsurance Update(MemberInsurance memberInsurance);

        MemberInsurance GetById(int id);

        MemberInsurance GetByMemberId(int memberId);

        IList<MemberInsurance> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}