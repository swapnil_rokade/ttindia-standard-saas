﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberHiringProcessDataAccess

    public interface IMemberHiringProcessDataAccess
    {
        MemberHiringProcess Add(MemberHiringProcess memberHiringProcess);

        MemberHiringProcess Update(MemberHiringProcess memberHiringProcess);

        MemberHiringProcess GetById(int id);

        IList<MemberHiringProcess> GetAll();

        bool DeleteById(int id);

        IList<MemberHiringProcess> GetByMemberIdJobPostingId(int memberId, int jobPostingId);

        MemberHiringProcess GetByMemberIdJobPostingIdAndInterviewLevelId(int memberid, int jobpostingid, int interviewlevelid);

    }

    #endregion
}