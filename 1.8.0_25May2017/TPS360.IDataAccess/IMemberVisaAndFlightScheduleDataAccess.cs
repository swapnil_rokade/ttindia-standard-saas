﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberVisaAndFlightScheduleDataAccess

    public interface IMemberVisaAndFlightScheduleDataAccess
    {
        MemberVisaAndFlightSchedule Add(MemberVisaAndFlightSchedule memberVisaAndFlightSchedule);

        MemberVisaAndFlightSchedule Update(MemberVisaAndFlightSchedule memberVisaAndFlightSchedule);

        MemberVisaAndFlightSchedule GetById(int id);

        MemberVisaAndFlightSchedule GetByMemberId(int memberId);

        IList<MemberVisaAndFlightSchedule> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}