﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ICampaignDataAccess

    public interface ICampaignDataAccess
    {
        Campaign Add(Campaign campaign);

        Campaign Update(Campaign campaign);

        Campaign GetById(int id);

        IList<Campaign> GetAll();

        PagedResponse<Campaign> GetPaged(PagedRequest request);

        bool DeleteById(int id);

        bool DeleteFromMemberById(int id, int memberId);
    }

    #endregion
}

