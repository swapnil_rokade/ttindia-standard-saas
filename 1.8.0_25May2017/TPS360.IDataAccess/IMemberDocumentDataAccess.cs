﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
namespace TPS360.DataAccess
{
    #region IMemberDocumentDataAccess

    public interface IMemberDocumentDataAccess
    {
        MemberDocument Add(MemberDocument memberDocument);

        MemberDocument Update(MemberDocument memberDocument);

        MemberDocument GetById(int id);

        IList<MemberDocument> GetAllByMemberId(int memberId);

        IList<MemberDocument> GetAllByTypeAndMemberId(string type, int memberId);

        MemberDocument GetAllByMemberIDTypeAndFileName(int memberId,string type, string FileName );

        IList<MemberDocument> GetAll();

        bool DeleteById(int id);
        
        bool DeleteByMemberId(int MemberId);
        
        IList<MemberDocument> GetAllByTypeAndMembersId(string membersId, string documentType);

        MemberDocument GetByMemberIdAndFileName(int memberId, string fileName);

        PagedResponse<MemberDocument> GetPagedByMemberId(int MemberID, PagedRequest request);

        IList<MemberDocument> GetLatestResumeByMemberID(int memberId);

        MemberDocument GetRecentResumeDocument(int memberId);
    }

    #endregion
}