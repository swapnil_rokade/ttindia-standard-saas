﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IOrganizationDataAccess

    public interface IOrganizationDataAccess
    {
        Organization Add(Organization organization);

        Organization Update(Organization organization);

        Organization GetById(int id);

        Organization GetOrganization();

        IList<Organization> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}