﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ITrainingCourseModuleTrainerMapDataAccess

    public interface ITrainingCourseModuleTrainerMapDataAccess
    {
        TrainingCourseModuleTrainerMap Add(TrainingCourseModuleTrainerMap trainingCourseModuleTrainerMap);

        TrainingCourseModuleTrainerMap Update(TrainingCourseModuleTrainerMap trainingCourseModuleTrainerMap);

        TrainingCourseModuleTrainerMap GetById(int id);

        TrainingCourseModuleTrainerMap GetByMemberIdAndTrainingCourseModuleId(int memberId, int trainingCourseModuleId);

        IList<TrainingCourseModuleTrainerMap> GetAll();

        bool DeleteById(int id);

        IList<TrainingCourseModuleTrainerMap> GetAllByModuleId(int id);
    }

    #endregion
}