﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:ICountryDataAccess.cs
    Description: This page is used for Countries  functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetCountryIdByCountryCode() to assist 
                                                                Resume parser team
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using System;
using TPS360.Common.Shared;
namespace TPS360.DataAccess
{
    #region IIPAccessRulesDataAccess

    public interface IIPAccessRulesDataAccess
    {
        IPAccessRules Add(IPAccessRules accessrules);

        IPAccessRules Update(IPAccessRules accessrules);

        IPAccessRules GetById(int id);

        IList<IPAccessRules> GetAll();

        PagedResponse<IPAccessRules> GetPaged(PagedRequest  request);

        bool DeleteById(int id);

    }

    #endregion
}