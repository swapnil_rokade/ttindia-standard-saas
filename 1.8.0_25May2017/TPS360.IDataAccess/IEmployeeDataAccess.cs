﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IEmployeeDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             8-June-2009            Nagarathna V.B       enhancement:10525;   add "GetProductivityOverviewByDate"
 *  0.1             25/May/2016            pravin khot          added- TIMEZONE
 *  0.3             7/June/2016            pravin khot          added - GetByEmailId
 *  0.4             21/Mar/2017            Sumit Sonawane       modify - Graphs
    -------------------------------------------------------------------------------------------------------------------------------------------       
*/


using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;
using System.Data;
using TPS360.Common.BusinessEntity;
using System.Collections.Generic;
using System.Collections;

namespace TPS360.DataAccess
{
    #region IEmployeeDataAccess

    public interface IEmployeeDataAccess
    {
        Employee GetById(int id);
        //////////////////////code added by Sumit Sonawane on 21/Mar/2017////////////////////////////////////////////////////////////////////////////
        System.Collections.Generic.IList<EmployeeProductivity> GetEmployeeProductivityDetails();
        System.Collections.Generic.IList<EmployeeProductivity> GetEmployeePresenttoInterviewRatio();
        System.Collections.Generic.IList<EmployeeProductivity> GetEmployeeTimetoHire();
        System.Collections.Generic.IList<EmployeeProductivity> GetJoinedToRejectedByBusiness();
        System.Collections.Generic.IList<EmployeeProductivity> GetCandidateBySkillId();
        System.Collections.Generic.IList<EmployeeProductivity> GetJobPostingByMonthYear();
        ////////////////////// End ////////////////////////////////////////////////////////////////////////////

        Employee GetByEmailId(string EmailId);//added by pravin khot on 7/June/2016

        Employee GetByIdForHR(int id);

        EmployeeOverviewDetails GetEmployeeOverviewDetails(int memberid);

        Employee GetActivitiesReportByIdAndCreateDate(int id,DateTime createDate);

        PagedResponse<Employee> GetPaged(PagedRequest request);

        PagedResponse<Employee> GetPaged(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, int industry, int functionalCategory, int workPermit,
            int gender, int maritalStatus, int educationId, int assignedManager);

        PagedResponse<Employee> GetPagedWorkReport(PagedRequest request);

        EmployeeStatistics GetStatisticsByEmployeeId(int employeeId);

        BusinessOverview GetBusinessOverviewByEmployeeId(int employeeId);

        HireDesk GetHireDeskByEmployeeId(int employeeId);

        DataTable GetProductivityOverviewByDate(string RequireDate);//0.1

        DataTable GetMyProductivityOverviewByDate(string RequireDate, int MemberId);

        PagedResponse<EmployeeProductivity> GetPagedEmployeeProductivity(DateTime StartDate,DateTime EndDate, int MemberId, PagedRequest request);


        void EmployeeFirstLogin_Create(int MemberID);

        bool IsEmployeeFirstLogin(int MemberID);

        ArrayList GetAllEmployeeByTeamId(string id);

        PagedResponse<EmployeeProductivity> GetPagedByProductityReport(PagedRequest request);
        
        PagedResponse<EmployeeProductivity> GetPagedByTeamProductityReport(PagedRequest request);

        System .Collections .Generic .IList<EmployeeProductivity> GetEmployeeProductivityReport(PagedRequest request);

        System.Collections.Generic.IList<EmployeeProductivity > GetAllListGroupByDate(string UserIds, string TeamIds, int StartDate, int EndDate);

        ArrayList GetEmployeeNameandContactNumber(int EmployeeId);

        //**************ADDED BY PRAVIN KHOT ON 25/May/2016**************TIMEZONE
        ArrayList GetAllTimezone();

        void Employee_SaveTimezone(int MemberId, int TimezoneId);

        Employee GetTimeZoneByMemberId(int MemberId);
        //******************END*****************
    }

    #endregion
}