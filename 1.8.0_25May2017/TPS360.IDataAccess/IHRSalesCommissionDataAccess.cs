﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IHRSalesCommissionDataAccess

    public interface IHRSalesCommissionDataAccess
    {
        HRSalesCommission Add(HRSalesCommission hRSalesCommission);

        HRSalesCommission Update(HRSalesCommission hRSalesCommission);

        HRSalesCommission GetById(int id);

        HRSalesCommission GetByHROfferLetterId(int hROfferLetterId);

        IList<HRSalesCommission> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}