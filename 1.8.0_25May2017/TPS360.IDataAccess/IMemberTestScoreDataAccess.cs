﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberTestScoreDataAccess

    public interface IMemberTestScoreDataAccess
    {
        MemberTestScore Add(MemberTestScore memberTestScore);

        MemberTestScore Update(MemberTestScore memberTestScore);

        MemberTestScore GetById(int id);

        IList<MemberTestScore> GetAllByMemberId(int memberId);

        IList<MemberTestScore> GetAllByTestMasterId(int testMasterId);

        IList<MemberTestScore> GetAll();

        bool DeleteById(int id);

        PagedResponse<MemberTestScore> GetPaged(PagedRequest pageRequest);
    }

    #endregion
}