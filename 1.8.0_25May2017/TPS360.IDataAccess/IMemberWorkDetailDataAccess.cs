﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberWorkDetailDataAccess

    public interface IMemberWorkDetailDataAccess
    {
        MemberWorkDetail Add(MemberWorkDetail memberWorkDetail);

        MemberWorkDetail Update(MemberWorkDetail memberWorkDetail);

        MemberWorkDetail GetById(int id);

        IList<MemberWorkDetail> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}