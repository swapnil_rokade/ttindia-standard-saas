﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberExperienceDetailDataAccess

    public interface IMemberExperienceDetailDataAccess
    {
        MemberExperienceDetail GetById(int id);

        IList<MemberExperienceDetail> GetAllByMemberId(int memberId);

        PagedResponse<MemberExperienceDetail> GetPaged(PagedRequest request);
    }

    #endregion
}