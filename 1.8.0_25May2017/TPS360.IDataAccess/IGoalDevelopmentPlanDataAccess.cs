﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IGoalDevelopmentPlanDataAccess

    public interface IGoalDevelopmentPlanDataAccess
    {
        GoalDevelopmentPlan Add(GoalDevelopmentPlan goalDevelopmentPlan);

        GoalDevelopmentPlan Update(GoalDevelopmentPlan goalDevelopmentPlan);

        GoalDevelopmentPlan GetById(int id);

        IList<GoalDevelopmentPlan> GetAll();

        bool DeleteById(int id);

        GoalDevelopmentPlan GetByMemberGoalId(int id);
    }

    #endregion
}