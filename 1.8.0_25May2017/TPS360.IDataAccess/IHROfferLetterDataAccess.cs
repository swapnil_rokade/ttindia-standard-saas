﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;

namespace TPS360.DataAccess
{
    #region IHROfferLetterDataAccess

    public interface IHROfferLetterDataAccess
    {
        HROfferLetter Add(HROfferLetter hROfferLetter);

        HROfferLetter Update(HROfferLetter hROfferLetter);

        HROfferLetter GetById(int id);

        HROfferLetter GetByMemberId(int memberId);

        IList<HROfferLetter> GetAll();

        IList<HROfferLetter> GetAllByMemberIdAndJobPostingId(int memberId, int jobPostingId);

        PagedResponse<HROfferLetter> GetPaged(PagedRequest request);

        bool DeleteById(int id);

        ArrayList GetHROfferLetterList();
    }

    #endregion
}