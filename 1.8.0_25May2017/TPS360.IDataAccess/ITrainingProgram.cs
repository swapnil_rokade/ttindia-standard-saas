﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ITrainingProgramDataAccess

    public interface ITrainingProgramDataAccess
    {
        TrainingProgram Add(TrainingProgram trainingProgram);

        TrainingProgram Update(TrainingProgram trainingProgram);

        TrainingProgram GetById(int id);

        IList<TrainingProgram> GetAll();

        PagedResponse<TrainingProgram> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}