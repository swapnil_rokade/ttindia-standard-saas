﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ITaskDocumentDataAccess

    public interface ITaskDocumentDataAccess
    {
        TaskDocument Add(TaskDocument taskDocument);

        TaskDocument Update(TaskDocument taskDocument);

        TaskDocument GetById(int id);

        IList<TaskDocument> GetAllByCommonTaskId(int commonTaskId);

        IList<TaskDocument> GetAll();

        bool DeleteById(int id);

        bool DeleteByCommonTaskId(int id);
    }

    #endregion
}