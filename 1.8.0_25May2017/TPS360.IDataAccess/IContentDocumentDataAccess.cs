﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IContentDocumentDataAccess

    public interface IContentDocumentDataAccess
    {
        ContentDocument Add(ContentDocument contentDocument);

        ContentDocument Update(ContentDocument contentDocument);

        ContentDocument GetById(int id);

        IList<ContentDocument> GetAllByContentId(int contentId);

        IList<ContentDocument> GetAll();        

        bool DeleteByContentId(int contentId);

        bool DeleteById(int id);
    }

    #endregion
}