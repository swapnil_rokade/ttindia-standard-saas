﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IProjectJobPostingMapDataAccess

    public interface IProjectJobPostingMapDataAccess
    {
        ProjectJobPostingMap Add(ProjectJobPostingMap projectJobPostingMap);

        ProjectJobPostingMap Update(ProjectJobPostingMap projectJobPostingMap);

        ProjectJobPostingMap GetById(int id);

        ProjectJobPostingMap GetByProjectIdAndJobPostingId(int projectId, int jobPostingId);
        
        IList<ProjectJobPostingMap> GetAll();

        bool DeleteById(int id);

        bool DeleteByProjectIdAndJobPostingId(int projectId, int jobPostingId);
    }

    #endregion
}