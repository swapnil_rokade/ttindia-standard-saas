﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;

namespace TPS360.DataAccess
{
    #region IMemberTimeSheetDataAccess

    public interface IMemberTimeSheetDataAccess
    {
        MemberTimeSheet Add(MemberTimeSheet memberTimeSheet);

        MemberTimeSheet Update(MemberTimeSheet memberTimeSheet);

        MemberTimeSheet GetById(int id);

        IList<MemberTimeSheet> GetAll();

        ArrayList GetNameByMemberId(int MemberId);

        bool DeleteById(int id);

        PagedResponse<MemberTimeSheet> GetPaged(PagedRequest request);
    }

    #endregion
}