﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberPositionMapDataAccess

    public interface IMemberPositionMapDataAccess
    {
        MemberPositionMap Add(MemberPositionMap memberPositionMap);

        MemberPositionMap Update(MemberPositionMap memberPositionMap);

        MemberPositionMap GetById(int id);

        IList<MemberPositionMap> GetAll();

        IList<MemberPositionMap> GetAllByMemberId(int memberId);

        MemberPositionMap GetByMemberIdAndPositionId(int memberId, int positionId);        

        bool DeleteById(int id);
    }

    #endregion
}