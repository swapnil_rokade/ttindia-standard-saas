﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IOrganizationFunctionalityCategoryPositionMapDataAccess

    public interface IOrganizationFunctionalityCategoryPositionMapDataAccess
    {
        OrganizationFunctionalityCategoryPositionMap Add(OrganizationFunctionalityCategoryPositionMap organizationFunctionalityCategoryPositionMap);

        OrganizationFunctionalityCategoryPositionMap Update(OrganizationFunctionalityCategoryPositionMap organizationFunctionalityCategoryPositionMap);

        OrganizationFunctionalityCategoryPositionMap GetById(int id);

        IList<OrganizationFunctionalityCategoryPositionMap> GetAll();

        IList<OrganizationFunctionalityCategoryPositionMap> GetAllByOrganizationFunctionalityCategoryMapId(int organizationFunctionalityCategoryMapId);

        bool DeleteById(int id);
    }

    #endregion
}