﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberGeneralQuestionDataAccess

    public interface IMemberGeneralQuestionDataAccess
    {
        MemberGeneralQuestion Add(MemberGeneralQuestion memberGeneralQuestion);

        MemberGeneralQuestion Update(MemberGeneralQuestion memberGeneralQuestion);

        MemberGeneralQuestion GetById(int id);

        IList<MemberGeneralQuestion> GetAll();

        bool DeleteById(int id);

        MemberGeneralQuestion GetByJobPostingIdAndMemberIdAndJobPostingGeneralQuestionId(int jobPostingId, int memberId, int jobPostingGeneralQuestionId);
    }

    #endregion
}