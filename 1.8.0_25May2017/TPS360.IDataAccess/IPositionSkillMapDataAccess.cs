﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IPositionSkillMapDataAccess

    public interface IPositionSkillMapDataAccess
    {
        PositionSkillMap Add(PositionSkillMap positionSkillMap);

        PositionSkillMap Update(PositionSkillMap positionSkillMap);

        PositionSkillMap GetById(int id);

        IList<PositionSkillMap> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}