﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IMemberExtendedInformationDataAccess.cs
    Description: This is the class library page.
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
      0.1             24/May/2016         pravin khot           added - UpdateMemberManagerIsPrimaryTrue
---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberExtendedInformationDataAccess

    public interface IMemberExtendedInformationDataAccess
    {
        MemberExtendedInformation Add(MemberExtendedInformation memberExtendedInformation);

        MemberExtendedInformation Update(MemberExtendedInformation memberExtendedInformation);

        MemberExtendedInformation GetById(int id);

        MemberExtendedInformation GetByMemberId(int id);

        IList<MemberExtendedInformation> GetAll();

        bool DeleteById(int id);
        
        bool DeleteByMemberId(int MemberId);

        void UpdateUpdateDateByMemberId(int MemberId);

        void UpdateMemberManagerIsPrimaryTrue(int candidateid,int AssignManagerId);//added by pravin khot on 24/May/2016
    }

    #endregion
}