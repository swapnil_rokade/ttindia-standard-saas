﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICompetencyDataAccess
	
	public interface ICompetencyDataAccess
    {
        Competency Add(Competency competency);
		
		Competency Update(Competency competency);
		
		Competency GetById(int id);
		
		IList<Competency> GetAll();

		bool DeleteById(int id);
    }
	
	#endregion
}