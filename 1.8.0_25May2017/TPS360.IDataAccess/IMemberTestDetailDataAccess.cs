﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberTestDetailDataAccess

    public interface IMemberTestDetailDataAccess
    {
        MemberTestDetail Add(MemberTestDetail memberTestDetail);

        MemberTestDetail Update(MemberTestDetail memberTestDetail);

        MemberTestDetail GetById(int id);

        IList<MemberTestDetail> GetAllByMemberId(int memberId);

        IList<MemberTestDetail> GetAllByTestMasterId(int testMasterId);

        IList<MemberTestDetail> GetAll();

        bool DeleteById(int id);

        IList<MemberTestDetail> GetAllByMemberIdAndTestMasterId(int memberId, int testMasterId);

        IList<MemberTestDetail> GetAllByCurrentAppearedExam(int currentTestId, int memberIdForAssessment, int examTakeNumber);

        IList<MemberTestDetail> GetAllByCurrentAppearedExam(int currentTestId, int memberIdForAssessment);
    }

    #endregion
}