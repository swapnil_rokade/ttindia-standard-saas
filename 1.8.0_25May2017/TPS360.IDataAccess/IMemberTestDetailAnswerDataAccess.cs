﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberTestDetailAnswerDataAccess

    public interface IMemberTestDetailAnswerDataAccess
    {
        MemberTestDetailAnswer Add(MemberTestDetailAnswer memberTestDetailAnswer);

        MemberTestDetailAnswer Update(MemberTestDetailAnswer memberTestDetailAnswer);

        MemberTestDetailAnswer GetById(int id);

        IList<MemberTestDetailAnswer> GetAll();

        bool DeleteById(int id);

        MemberTestDetailAnswer GetByTestDetailIdAndTestAnswerId(int memberTestDetailId, int testAnswerId);

        IList<MemberTestDetailAnswer> GetAllByMemberTestDetailId(int memberTestDetailId);
    }

    #endregion
}