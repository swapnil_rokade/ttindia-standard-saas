﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ICompanyDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            Mar-8-2010           Nagarathna V.B        Enhancement Id: 12129; GetPrimaryContact() created for submission REport.
 *  0.2            8/June/2016          pravin khot           added - GetAllClientsByStatusByCandidateId
  
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System.Collections;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;

namespace TPS360.DataAccess
{
    #region ICompanyDataAccess

    public interface ICompanyDataAccess
    {
        Company Add(Company company);

        Company Update(Company company);

        bool UpdateStatus(int companyId, CompanyStatus status);

        bool ChangePendingStatus(int companyId, bool IsPending);

        Company GetById(int id);

        IList<Company> GetAll();

        CompanyOverviewDetails GetCompanyOverviewDetails( int CompanyId);

        IList<Company> GetAllPendingClient();

        PagedResponse<Company> GetPaged(PagedRequest request);

        bool DeleteById(int id);

        bool DeleteFromMemberById(int id, int memberId);

        Hashtable GetAllByEndClientType(int endClientType);

        ArrayList GetAllClients();
        //12129
        ArrayList GetPrimaryContact();

        ArrayList GetAllNameByCompanyStatus(CompanyStatus companyStatus);

        ArrayList GetAllVendorPartnerNames();

        Int32 GetCompanyIdByName(string companyName);

        IList<Company> GetPaged(string preText, int count);

        int GetCompanyCount();

        string GetCompanyNameById(int CompanyId);

        string GetCompanyNameByVendorMemberID(int MemberId);

        Int32  GetCompayByJobPostingId(int JobpostingId);

        ArrayList GetAllCompanyList();

        ArrayList GetAllClientsByStatus(int status);

        int GetCompanyIdByNameForDataImport(string CompanyName);

        ArrayList GetAllClientsByStatusByCandidateId(int status,int candidateid);//added by pravin khot on 8/June/2016
    }

    #endregion
}