﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IMemberDetailDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author                  MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              April-4-2009           Nagarathna V.B          Defect ID: 9545; created new method "GetSSNCount" 
   ---------------------------------------------------------------------------------------------------------------------------------------------
*/
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberDetailDataAccess

    public interface IMemberDetailDataAccess
    {
        MemberDetail Add(MemberDetail memberDetail);

        MemberDetail Update(MemberDetail memberDetail);

        MemberDetail GetById(int id);

        MemberDetail GetByMemberId(int id);

        int GetSSNCount(string strSSN, int MemberId);//0.1

        IList<MemberDetail> GetAll();

        bool DeleteById(int id);
       
        bool DeleteByMembertId(int MemberId);

        bool BuildMemberManager_ByMemberId(int MemberId, int CreatorId);
        
    }

    #endregion
}