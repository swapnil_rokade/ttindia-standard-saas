﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberExpenseDataAccess

    public interface IMemberExpenseDataAccess
    {
        MemberExpense Add(MemberExpense memberExpense);

        MemberExpense Update(MemberExpense memberExpense);

        MemberExpense GetById(int id);

        IList<MemberExpense> GetAllByMemberId(int memberId);

        IList<MemberExpense> GetAllByMemberIdAndExpenseCategoryLookupId(int memberId, int expenseCategoryLookupId);

        IList<MemberExpense> GetAllByMemberIdAndStatus(int memberId, int status);

        IList<MemberExpense> GetAll();

        bool DeleteById(int id);

        PagedResponse<MemberExpense> GetPaged(PagedRequest request);
    }

    #endregion
}