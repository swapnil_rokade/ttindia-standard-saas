﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IHROfferLetterBenefitDataAccess

    public interface IHROfferLetterBenefitDataAccess
    {
        HROfferLetterBenefit Add(HROfferLetterBenefit hROfferLetterBenefit);

        HROfferLetterBenefit Update(HROfferLetterBenefit hROfferLetterBenefit);

        HROfferLetterBenefit GetById(int id);

        IList<HROfferLetterBenefit> GetAll();

        IList<HROfferLetterBenefit> GetAllByHROfferLetterId(int hROfferLetterId);

        bool DeleteByHROfferLetterId(int hROfferLetterId);

        bool DeleteById(int id);
    }

    #endregion
}