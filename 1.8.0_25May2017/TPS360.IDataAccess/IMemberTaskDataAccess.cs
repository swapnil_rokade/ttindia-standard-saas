﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberTaskDataAccess

    public interface IMemberTaskDataAccess
    {
        MemberTask Add(MemberTask memberTask);

        MemberTask Update(MemberTask memberTask);

        MemberTask GetById(int id);

        IList<MemberTask> GetAllByMemberId(int memberId);

        IList<MemberTask> GetAllByCommonTaskId(int commonTaskId);

        IList<MemberTask> GetAllByMemberIdAndCommonTaskId(int memberId, int commonTaskId);

        IList<MemberTask> GetAll();

        bool DeleteById(int id);

        PagedResponse<MemberTask> GetPaged(PagedRequest pageRequest);

        MemberTask GetByCommonTaskId(int id);

        MemberTask GetByMemberIdAndCommonTaskId(int memberId,int commonTaskId);
    }

    #endregion
}