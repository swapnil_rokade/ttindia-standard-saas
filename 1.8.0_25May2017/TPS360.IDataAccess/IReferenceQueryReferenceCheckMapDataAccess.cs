using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IReferenceQueryReferenceCheckMapDataAccess
	
	public interface IReferenceQueryReferenceCheckMapDataAccess
    {
        ReferenceQueryReferenceCheckMap Add(ReferenceQueryReferenceCheckMap referenceQueryReferenceCheckMap);
		
		ReferenceQueryReferenceCheckMap Update(ReferenceQueryReferenceCheckMap referenceQueryReferenceCheckMap);
		
		ReferenceQueryReferenceCheckMap GetById(int id);
		
		IList<ReferenceQueryReferenceCheckMap> GetAll();

		bool DeleteById(int id);

        bool DeleteByReferenceCheckId(int referenceCheckId);
    }
	
	#endregion
}