﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IMemberDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              May-28-2009           Veda             DefectId:10493; Added "GetLicenseKey"
 *  0.2              Nov-19-1009           Sandeesh         Enhancement Id:10998 -Users in the Admin role should have all of the access/deletion rights of the cst2 superuser
    0.3              02/dec/2015           pravin khot      GetAllNameWithEmailByRoleName_IP added 
 *  0.4              09/Dec/2015           pravin khot      GetAllSuggestedInterviewer
 *  0.5              23/Feb/2016           pravin khot      Introduced by DeleteRoleRequisition, UpdateRoleRequisition,GetCustomRoleRequisitiondetail
    0.6              7/March/2016          pravin khot      Introduced by GetAllNameWithEmailByRoleNameIfExisting
 *  0.7              27/April/2016         pravin khot      Introduced by UpdateMemberthroughvendor,CandidateAvailableInRequisition_Name
 *  0.8              16/May/2016           pravin khot      added- GetAllNameByTeamMemberId
    0.8              10/Jun/2016           prasanth Kumar g  Introduced function GetMemberByMemberUserName
 *  0.9              10/Jun/2016           prasanth Kumar g  Introduced function GetMemberADUserNameById
 ---------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;

namespace TPS360.DataAccess
{
    #region IMemberDataAccess

    public interface IMemberDataAccess
    {
        Member Add(Member member);

        Member AddFullInfo(Member member, bool IsCreateMemberDetailandExtendedInfo, int Availability, bool IsCreateMemberManager);

        ArrayList GetLicenseKey(string intLicenseKey, string varchrDomainName); //0.1

        Member Update(Member member);

        Member UpdateMemberthroughvendor(Member member); //code added by pravin khot on 27/April/2016

        string CandidateAvailableInRequisition_Name(int MemberId, int CurrentJobPostingId); //code added by pravin khot on 27/April/2016
        string CandidateAvailableInRequisition_Name(int MemberId); //code added by pravin khot on 27/April/2016

        Member GetById(int id);

        Member GetByUserId(Guid userId);

        Member GetByUserName(string userName);

        IList<Member> GetAll();

        IList<Member> GetAllByJobPostingId(int jobPostingId);

        IList<Member> GetAllByCustomRoleId(int customRoleId);

        IList<Member> GetAllByCreatorIdAndRoleName(int creatorId,string roleName);

        IList<Member> GetAllMemberByMemberGroupId(int memberGroupId);

        IList<Member> GetAllMemberGroupManagerByMemberGroupId(int memberGroupId);

        IList<Member> GetAllMemberDetailsByInterviewId(int InterviewId,int MemberId);

        PagedResponse<Member> GetPaged(PagedRequest request);

        PagedResponse<Candidate> GetPagedByMemberGroupId(PagedRequest request);

        PagedResponse<Member> GetPagedByRoleNameAndCreatorId(PagedRequest request);
        Member GetMemberByMemberEmail(string PrimaryEmail);
        Member GetMemberByMemberUserName(string UserName); //Line introduced by Prasanth on 10/Jun/2016
        bool DeleteById(int id);
        void UpdateAspNet_User(string UserName, Guid UserId);
        ArrayList GetAllNameByRoleName(string roleName);
        ArrayList GetAllNameWithEmailByRoleName(string roleName);

        ArrayList GetAllNameWithEmailByRoleNameIfExisting(string roleName);//code introduced by pravin khot using existing user in company contact 7/March/2016 

        ArrayList GetAllSuggestedInterviewer(int RequisitionId); //code introduced by pravin khot using suggested name in interview schedule 09/Dec/2015 

        IList<Member> GetAllByIds(string Ids);

        string GetNewMermberIdentityId();
        string GetMemberUserNameById(int id);//0.2
        string GetMemberADUserNameById(int id);//Line introduced by Prasanth on 10/Jun/2016
        string GetMemberNameById(int id);
        Int32 GetMemberIdByMemberEmail(string Email);
        Int32 GetMemberIdByEmail(string Email);
        Int32 GetCreatorIdForMember(int id);

        void UnSubsciribeMemberFromBulkEmail(int MemberID);

        ArrayList GetAllEmployeeNameWithEmail(string roleName);

        int GetMemberStatusByMemberId(int memberId);

        int GetMemberIdByCompanyContact(int CompanyContactId);

        ArrayList GetAllNameWithEmailByRoleName_IP(string roleName, int paneltypeid);  //code introduced by pravin khot 02/dec/2015
        //********Code added by pravin khot on 23/Feb/2016*********
        void DeleteRoleRequisition();

        void UpdateRoleRequisition(int RoleId, int MemberId, int sectionid);

        IList<CustomRole> GetCustomRoleRequisitiondetail(int sectionid);
        //******************END*********************************

        ArrayList GetAllNameByTeamMemberId(string roleName, int memberid); //code added by pravin khot on 16/May/2016
    }

    #endregion
}