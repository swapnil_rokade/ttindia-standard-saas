﻿using System.Collections.Generic;
using System;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberPrivilegeDataAccess

    public interface IMemberPrivilegeDataAccess
    {
        MemberPrivilege Add(MemberPrivilege memberPrivilege);

        MemberPrivilege Update(MemberPrivilege memberPrivilege);

        MemberPrivilege GetById(int id);

        void DeleteAll_MemberPrivilege();

        IList<MemberPrivilege> GetAllByMemberId(int memberId);

        IList<MemberPrivilege> GetAll();

        bool DeleteById(int id);
        bool DeleteByMemberIdWithOutAdminAccess(int memberId);
        bool DeleteByMemberId(int memberId);
        bool GetMemberPrivilegeForMemberIdAndRequiredURL(int memberId, string Url);
    }

    #endregion
}