﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberTierDataAccess

    public interface IMemberTierDataAccess
    {
        MemberTier Add(MemberTier memberTier);

        MemberTier Update(MemberTier memberTier);

        MemberTier GetById(int id);

        MemberTier GetByMemberId(int MemberId);

        MemberTier GetByMemberIdAndTierId(int MemberId,int TierId);

        IList<MemberTier> GetAll();

        IList<MemberTier> GetAllByTierLookUpId(int tierLookUpId);

        bool DeleteById(int id);
    }

    #endregion
}