﻿/*Modification Log:
   ------------------------------------------------------------------------------------------------------------   Ver.No.             Date                  Author              Modification
   ------------------------------------------------------------------------------------------------------------    0.1             04-Feb-2009             Sandeesh            Defect Fix - ID: 10027; Added new method to get the H1B1 details of the employee.*/
using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberH1B1DataAccess

    public interface IMemberH1B1DataAccess
    {
        MemberH1B1 Add(MemberH1B1 memberH1B1);

        MemberH1B1 Update(MemberH1B1 memberH1B1);

        MemberH1B1 GetById(int id);       

        IList<MemberH1B1> GetAll();

        IList<MemberH1B1> GetAllMemberH1B1ByMemberId(int memberH1B1); 
        //0.1 Start
        MemberH1B1 GetMemberH1B1ByMemberId(int memberId);
        //0.1 End
        bool DeleteById(int id);
    }

    #endregion
}