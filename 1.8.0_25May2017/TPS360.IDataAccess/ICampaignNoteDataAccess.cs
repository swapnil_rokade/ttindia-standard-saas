﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICampaignNoteDataAccess

    public interface ICampaignNoteDataAccess
    {
        CampaignNote Add(CampaignNote campaignNote);

        CampaignNote GetById(int id);

        IList<CampaignNote> GetAll();

        IList<CampaignNote> GetAllByCampaignId(int campaignId);

        bool DeleteById(int id);
    }

    #endregion
}