﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IProductDataAccess

    public interface IProductDataAccess
    {
        Product Add(Product product);

        Product Update(Product product);

        Product GetById(int id);

        IList<Product> GetAll();

        PagedResponse<Product> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}