﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ITrainingProgramScheduleDataAccess

    public interface ITrainingProgramScheduleDataAccess
    {
        TrainingProgramSchedule Add(TrainingProgramSchedule trainingProgramSchedule);

        TrainingProgramSchedule Update(TrainingProgramSchedule trainingProgramSchedule);

        TrainingProgramSchedule GetById(int id);

        IList<TrainingProgramSchedule> GetAll();

        bool DeleteById(int id);

        IList<TrainingProgramSchedule> GetAllByTrainingProgramId(int id);
    }

    #endregion
}