﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IOnBoardingOffBoardingChecklistManagerDataAccess

    public interface IOnBoardingOffBoardingChecklistManagerDataAccess
    {
        OnBoardingOffBoardingChecklistManager Add(OnBoardingOffBoardingChecklistManager onBoardingOffBoardingChecklistManager);

        OnBoardingOffBoardingChecklistManager Update(OnBoardingOffBoardingChecklistManager onBoardingOffBoardingChecklistManager);

        OnBoardingOffBoardingChecklistManager GetById(int id);

        IList<OnBoardingOffBoardingChecklistManager> GetAllByOnBoardingOffBoardingChecklistId(int onBoardingOffBoardingChecklistId);

        IList<OnBoardingOffBoardingChecklistManager> GetAllByAssignedManagerId(int assignedManagerId);

        IList<OnBoardingOffBoardingChecklistManager> GetAll();

        bool DeleteById(int id);

        bool DeleteByOnBoardingOffBoardingChecklistId(int onBoardingOffBoardingChecklistId);

        bool DeleteByAssignedManagerId(int assignedManagerId);

        bool DeleteByOnBoardingOffBoardingChecklistIdAndAssignedManagerId(int onBoardingOffBoardingChecklistId, int assignedManagerId);
    }

    #endregion
}