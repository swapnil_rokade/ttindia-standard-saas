﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IInterviewAlertDataAccess

    public interface IInterviewAlertDataAccess
    {
        InterviewAlert Add(InterviewAlert interviewAlert);

        InterviewAlert Update(InterviewAlert interviewAlert);

        InterviewAlert GetById(int id);

        IList<InterviewAlert> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}