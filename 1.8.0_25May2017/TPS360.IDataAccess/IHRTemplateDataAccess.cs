﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IHRTemplateDataAccess

    public interface IHRTemplateDataAccess
    {
        HRTemplate Add(HRTemplate hRTemplate);

        HRTemplate Update(HRTemplate hRTemplate);

        HRTemplate GetById(int id);

        IList<HRTemplate> GetAll();

        PagedResponse<HRTemplate> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}