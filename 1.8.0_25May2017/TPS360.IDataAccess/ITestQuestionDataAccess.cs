﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ITestQuestionDataAccess

    public interface ITestQuestionDataAccess
    {
        TestQuestion Add(TestQuestion testQuestion);

        TestQuestion Update(TestQuestion testQuestion);

        TestQuestion GetById(int id);

        IList<TestQuestion> GetAll();

        bool DeleteById(int id);

        IList<TestQuestion> GetAllByTestMasterId(int id);

        PagedResponse<TestQuestion> GetPaged(PagedRequest pageRequest);
    }

    #endregion
}