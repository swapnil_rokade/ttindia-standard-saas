﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IClientWorkOrderDetailDataAccess

    public interface IClientWorkOrderDetailDataAccess
    {
        ClientWorkOrderDetail Add(ClientWorkOrderDetail clientWorkOrderDetail);

        ClientWorkOrderDetail Update(ClientWorkOrderDetail clientWorkOrderDetail);

        ClientWorkOrderDetail GetById(int id);

        IList<ClientWorkOrderDetail> GetAllByClientWorkOrderId(int clientWorkOrderId);

        IList<ClientWorkOrderDetail> GetAll();

        bool DeleteById(int id);

        PagedResponse<ClientWorkOrderDetail> GetPaged(PagedRequest request);

        ClientWorkOrderDetail GetByConsultantId(int consultantId);

        bool DeleteByClientWorkOrderId(int clientWorkOrderId);
    }

    #endregion
}