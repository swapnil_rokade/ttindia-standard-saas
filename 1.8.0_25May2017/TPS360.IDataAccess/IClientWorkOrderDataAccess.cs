﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using System.Collections;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IClientWorkOrderDataAccess

    public interface IClientWorkOrderDataAccess
    {
        ClientWorkOrder Add(ClientWorkOrder clientWorkOrder);

        ClientWorkOrder Update(ClientWorkOrder clientWorkOrder);

        ClientWorkOrder GetById(int id);

        IList<ClientWorkOrder> GetAll();

        IList<ClientWorkOrder> GetAllByClientId(int clientId);
                
        PagedResponse<ClientWorkOrder> GetAllOnSearch(PagedRequest request, string clientId);

        string GetSearchQuery(string clientId);

        ArrayList GetAllClientWorkOrderReferenceNumberList();

        bool DeleteById(int id);
    }

    #endregion
}