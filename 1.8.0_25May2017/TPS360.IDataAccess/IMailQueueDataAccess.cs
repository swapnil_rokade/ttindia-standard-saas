﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMailQueueDataAccess
    public interface IMailQueueDataAccess
    {
        int Add(MailQueue mailqueue);


        bool DeleteMailQueueById(int id, out int MemberEmailId);

        IList<MailQueue> GetAllMailInQueue();

        void UpdateFileNamesInMailQueue(string fileNames, int mailQueueId, int NoOfAttachments);

        void UpdateMailbodyForInterviewTemplate(string EmailBody, int MemberEmailId);
    }
    #endregion
}
