﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IGuestHouseDetailDataAccess

    public interface IGuestHouseDetailDataAccess
    {
        GuestHouseDetail Add(GuestHouseDetail guestHouseDetail);

        GuestHouseDetail Update(GuestHouseDetail guestHouseDetail);

        GuestHouseDetail GetById(int id);

        IList<GuestHouseDetail> GetAll();

        IList<GuestHouseDetail> GetAllByGuestHouseId(int guestHouseId);

        bool DeleteById(int id);

        bool DeleteByGuestHouseId(int guestHouseId);
    }

    #endregion
}