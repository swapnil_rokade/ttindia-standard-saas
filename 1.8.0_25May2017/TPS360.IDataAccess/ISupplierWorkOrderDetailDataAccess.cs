﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ISupplierWorkOrderDetailDataAccess

    public interface ISupplierWorkOrderDetailDataAccess
    {
        SupplierWorkOrderDetail Add(SupplierWorkOrderDetail supplierWorkOrderDetail);

        SupplierWorkOrderDetail Update(SupplierWorkOrderDetail supplierWorkOrderDetail);

        SupplierWorkOrderDetail GetById(int id);

        IList<SupplierWorkOrderDetail> GetAll();

        bool DeleteById(int id);

        SupplierWorkOrderDetail GetByConsultantId(int consultantId);

        bool DeleteBySupplierWorkOrderId(int supplierWorkOrderId);

        IList<SupplierWorkOrderDetail> GetAllBySupplierWorkOrderId(int supplierWorkOrderId);
    }

    #endregion
}