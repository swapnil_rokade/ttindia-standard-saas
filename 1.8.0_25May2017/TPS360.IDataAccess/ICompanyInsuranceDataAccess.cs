﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICompanyInsuranceDataAccess

    public interface ICompanyInsuranceDataAccess
    {
        CompanyInsurance Add(CompanyInsurance companyInsurance);

        CompanyInsurance Update(CompanyInsurance companyInsurance);

        CompanyInsurance GetById(int id);

        IList<CompanyInsurance> GetAll();

        IList<CompanyInsurance> GetAllByCompanyId(int companyId);

        bool DeleteById(int id);

        bool DeleteByCompanyId(int companyId);
    }

    #endregion
}