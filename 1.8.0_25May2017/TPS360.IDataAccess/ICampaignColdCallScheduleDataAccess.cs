﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICampaignColdCallScheduleDataAccess

    public interface ICampaignColdCallScheduleDataAccess
    {
        CampaignColdCallSchedule Add(CampaignColdCallSchedule campaignColdCallSchedule);

        CampaignColdCallSchedule Update(CampaignColdCallSchedule campaignColdCallSchedule);

        CampaignColdCallSchedule GetById(int id);

        CampaignColdCallSchedule GetTopBySortOrder(int campaignCompanyId, string SortOrder);

        IList<CampaignColdCallSchedule> GetAllByCampaignCompanyId(int campaignCompanyId);

        IList<CampaignColdCallSchedule> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}