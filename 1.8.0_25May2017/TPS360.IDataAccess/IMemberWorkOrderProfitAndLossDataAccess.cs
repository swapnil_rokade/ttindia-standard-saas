﻿using System;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;


namespace TPS360.DataAccess
{
    #region MemberWorkOrderProfitAndLossDataAccess

    public interface IMemberWorkOrderProfitAndLossDataAccess
    { 
        PagedResponse<MemberWorkOrderProfitAndLoss> GetPaged(PagedRequest request);
    }

    #endregion
}