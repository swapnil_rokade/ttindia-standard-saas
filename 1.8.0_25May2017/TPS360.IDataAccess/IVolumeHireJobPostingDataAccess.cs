﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IVolumeHireJobPostingDataAccess

    public interface IVolumeHireJobPostingDataAccess
    {
        VolumeHireJobPosting Add(VolumeHireJobPosting volumeHireJobPosting);

        VolumeHireJobPosting Update(VolumeHireJobPosting volumeHireJobPosting);

        VolumeHireJobPosting GetById(int id);

        VolumeHireJobPosting GetByJobPostingId(int jobPostingId);

        IList<VolumeHireJobPosting> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}