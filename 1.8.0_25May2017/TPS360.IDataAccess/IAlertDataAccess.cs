﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IAlertDataAccess

    public interface IAlertDataAccess
    {
        Alert Add(Alert alert);

        Alert Update(Alert alert);

        Alert GetById(int id);

        Alert GetByWorkflowTitle(WorkFlowTitle workFlowTitle);

        IList<Alert> GetAll();

        PagedResponse<Alert> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}