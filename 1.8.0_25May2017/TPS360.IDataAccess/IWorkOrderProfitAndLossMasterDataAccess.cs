﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IWorkOrderProfitAndLossMasterDataAccess

    public interface IWorkOrderProfitAndLossMasterDataAccess
    {
        WorkOrderProfitAndLossMaster Add(WorkOrderProfitAndLossMaster workOrderProfitAndLossMaster);

        WorkOrderProfitAndLossMaster Update(WorkOrderProfitAndLossMaster workOrderProfitAndLossMaster);

        WorkOrderProfitAndLossMaster GetById(int id);

        WorkOrderProfitAndLossMaster GetBySupplierWorkOrderId(int SupplierWorkOrderId);

        IList<WorkOrderProfitAndLossMaster> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}