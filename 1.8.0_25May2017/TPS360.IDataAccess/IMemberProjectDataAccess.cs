﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberProjectDataAccess

    public interface IMemberProjectDataAccess
    {
        MemberProject Add(MemberProject memberProject);

        MemberProject Update(MemberProject memberProject);

        MemberProject GetById(int id);

        MemberProject GetByMemberId(int id);

        IList<MemberProject> GetAll();

        PagedResponse<MemberProject> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}