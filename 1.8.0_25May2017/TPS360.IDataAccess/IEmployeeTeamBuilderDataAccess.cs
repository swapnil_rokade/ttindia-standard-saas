﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IEmployeeTeamBuilderDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              16/May/2016            pravin khot         GetAllTeamByTeamLeaderId
 *  0.2              20/May/2016            SUMIT SONAWANE      GetTeamsByMemberId
--------------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System.Collections.Generic;
using System.Collections;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IEmployeeTeamBuilderDataAccess

    public interface IEmployeeTeamBuilderDataAccess
    {
        EmployeeTeamBuilder Add(EmployeeTeamBuilder employeeTeamBuilders);

        EmployeeTeamBuilder Update(EmployeeTeamBuilder employeeTeamBuilders);

        EmployeeTeamBuilder GetByTeamId(int id);

        IList<EmployeeTeamBuilder> GetAllTeam();
        PagedResponse<EmployeeTeamBuilder> GetPaged(PagedRequest request);

        bool DeleteById(int id);

        ArrayList GetAllTeamByTeamLeaderId(int TeamleaderId);//ADDED BY PRAVIN KHOT 0N 16/May/2016
        IList<EmployeeTeamBuilder> GetTeamsByMemberId(int MemberId);//ADDED BY SUMIT SONAWANE 0N 20/May/2016
    }

    #endregion
}