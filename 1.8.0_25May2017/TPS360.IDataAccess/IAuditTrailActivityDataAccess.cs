﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IAuditTrailActivityDataAccess

    public interface IAuditTrailActivityDataAccess
    {
        IList<AuditTrailActivity> GetAllActivityType();

        AuditTrailActivity GetActivityTypeById(int id);
    }

    #endregion
}
