/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: BaseDataAccessFactory.cs
    Description:  
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              20/Jul/2015            Prasanth Kumar G    Introduced IInterviewerFeedbackDataAccess
    0.2              16/Oct/2015            Prasanth Kumar G    Introduced IInterviewQuestionBankDataAccess, IInterviewResponseDataAccess, QuestionBank
 *  0.3              20/Nov/2015            pravin khot         Introduced by IInterviewPanelDataAccess,CreateInterviewPanelMasterDataAccess
    0.4              24/Dec/2015            Prasanth Kumar G    Introduced CreateMemberInterviewFeedbackDocumentDataAccess
 *  0.5               23/Feb/2016            pravin khot         Introduced by CreateUserRoleMapEditorDataAccess

 * ----------------------------------------------------------------------------------------------------------------------------------------------
*/
using System;
using System.Diagnostics;

using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    public abstract class BaseDataAccessFactory
    {
        #region Static Variables

        private static readonly Configuration.TypeInfo _typeInfo = Configuration.Settings.TypeInfo;

        #endregion

        #region Instance Variables

        private Context _context;

        #endregion

        #region Property

        protected virtual Context CurrentContext
        {
            [DebuggerStepThrough()]
            get
            {
                if (_context == null)
                {
                    _context = new Context();
                }

                return _context;
            }
        }

        #endregion

        #region Constructer

        [DebuggerStepThrough()]
        public BaseDataAccessFactory(Context context)
        {
            _context = context;
        }

        #endregion

        #region Static Methods

        [DebuggerStepThrough()]
        public static BaseDataAccessFactory Create(Context context)
        {
            return (BaseDataAccessFactory)AppDomain.CurrentDomain.CreateInstanceAndUnwrap(
                                _typeInfo.AssemblyName,
                                _typeInfo.TypeName, false,  System.Reflection.BindingFlags.Default,
                                null,
                                new object[] { context },
                                null,
                                null,
                                null
                                );
        }

        #endregion

        #region Factory Methods

        public abstract IActionLogDataAccess CreateActionLogDataAccess();

        public abstract IActivityDataAccess CreateActivityDataAccess();

        public abstract IActivityResourceDataAccess CreateActivityResourceDataAccess();

        public abstract IAlertDataAccess CreateAlertDataAccess();

        public abstract IApplicationWorkflowDataAccess CreateApplicationWorkflowDataAccess();

        public abstract IApplicationWorkflowMapDataAccess CreateApplicationWorkflowMapDataAccess();

        public abstract IASPNetMembershipDataAccess CreateASPNetMembershipDataAccess();

        public abstract IAutoMailSendingDataAccess CreateAutoMailSendingDataAccess();

        public abstract ICandidateRequisitionStatusDataAccess  CreateCandidateRequisitionStatusDataAccess();

        public abstract ICategoryDataAccess CreateCategoryDataAccess();

        public abstract ICommonNoteDataAccess CreateCommonNoteDataAccess();

        public abstract ICompanyDataAccess CreateCompanyDataAccess();

        public abstract ICompanyAssignedManagerDataAccess CreateCompanyAssignedManagerDataAccess();

        public abstract ICompanyContactDataAccess CreateCompanyContactDataAccess();

        public abstract ICompanyDocumentDataAccess CreateCompanyDocumentDataAccess();

        public abstract ICompanyNoteDataAccess CreateCompanyNoteDataAccess();

        public abstract ICompanySkillDataAccess CreateCompanySkillDataAccess();

        public abstract ICompanyStatusChangeRequestDataAccess CreateCompanyStatusChangeRequestDataAccess();

        public abstract ICountryDataAccess CreateCountryDataAccess();

        public abstract ICustomRoleDataAccess CreateCustomRoleDataAccess();

        public abstract ICustomRolePrivilegeDataAccess CreateCustomRolePrivilegeDataAccess();

        public abstract ICustomSiteMapDataAccess CreateCustomSiteMapDataAccess();

        public abstract IDepartmentDataAccess CreateDepartmentDataAccess();

        public abstract IDuplicateMemberDataAccess CreateDuplicateMemberDataAccess(); //Code added by sukanta on 10th Sept'09

        public abstract IEventLogDataAccess CreateEventLogDataAccess();

        public abstract IEmployeeReferralDataAccess CreateEmployeeReferralDataAccess();

        public abstract IEmployeeTeamBuilderDataAccess CreateEmployeeTeamBuilderDataAccess(); 

        public abstract IGenericLookupDataAccess CreateGenericLookupDataAccess();

        public abstract IHiringMatrixDataAccess CreateHiringMatrixDataAccess();

        public abstract IHiringMatrixLevelsDataAccess CreateHiringMatrixLevelsDataAccess();

        public abstract IInterviewDataAccess CreateInterviewDataAccess();

        public abstract IInterviewInterviewerMapDataAccess CreateInterviewInterviewerMapDataAccess();

        public abstract IIPAccessRulesDataAccess CreateIPAccessRulesDataAccess();

        public abstract IMemberDataAccess CreateMemberDataAccess();

        public abstract IMemberActivityDataAccess CreateMemberActivityDataAccess();

        public abstract IMemberActivityTypeDataAccess CreateMemberActivityTypeDataAccess();

        public abstract IMemberAlertDataAccess CreateMemberAlertDataAccess();

        public abstract IMemberAttendenceDataAccess CreateMemberAttendenceDataAccess();

        public abstract IMemberAttributeDataAccess CreateMemberAttributeDataAccess();
        
        public abstract IMemberAttendenceYearlyReport CreateMemberAttendenceYearlyReport();

        public abstract IMemberCapabilityRatingDataAccess CreateMemberCapabilityRatingDataAccess();

        public abstract IMemberCertificationMapDataAccess CreateMemberCertificationMapDataAccess();

        public abstract IMemberChangeArchiveDataAccess CreateMemberChangeArchiveDataAccess();

        public abstract IMemberCustomRoleMapDataAccess CreateMemberCustomRoleMapDataAccess();

        public abstract IMemberDailyReportDataAccess CreateMemberDailyReportDataAccess();
        
        public abstract IMemberDetailDataAccess CreateMemberDetailDataAccess();

        public abstract IMemberDocumentDataAccess CreateMemberDocumentDataAccess();

        public abstract IMemberEducationDataAccess CreateMemberEducationDataAccess();

        public abstract IMemberEmailDataAccess CreateMemberEmailDataAccess();

        public abstract IMemberEmailAttachmentDataAccess CreateMemberEmailAttachmentDataAccess();

        public abstract IMemberEmailDetailDataAccess CreateMemberEmailDetailDataAccess();

        public abstract IMemberExperienceDataAccess CreateMemberExperienceDataAccess();

        public abstract IMemberExperienceDetailDataAccess CreateMemberExperienceDetailDataAccess();

        public abstract IMemberExtendedInformationDataAccess CreateMemberExtendedInformationDataAccess();

        public abstract IMemberGroupDataAccess CreateMemberGroupDataAccess();

        public abstract IMemberGroupManagerDataAccess CreateMemberGroupManagerDataAccess();

        public abstract IMemberGroupMapDataAccess CreateMemberGroupMapDataAccess();

        public abstract IMemberHiringDetailsDataAccess CreateMemberHiringDetailsDataAccess();

        public abstract IMemberHiringProcessDataAccess CreateMemberHiringProcessDataAccess();

        public abstract IMemberJobAppliedDataAccess CreateMemberJobAppliedDataAccess();

        public abstract IMemberJoiningDetailsDataAccess CreateMemberJoiningDetailsDataAccess();

        public abstract IMemberOfferRejectionDataAccess CreateMemberOfferRejectionDataAccess();

        public abstract IJobPostingDataAccess CreateJobPostingDataAccess();

        public abstract IJobPostingSearchAgentDataAccess CreateJobPostingSearchAgentDataAccess();

        public abstract IJobPostingAssessmentDataAccess CreateJobPostingAssessmentDataAccess();

        public abstract IJobPostingDocumentDataAccess CreateJobPostingDocumentDataAccess();

        public abstract IJobPostingGeneralQuestionDataAccess CreateJobPostingGeneralQuestionDataAccess();

        public abstract IJobPostingHiringMatrixDataAccess CreateJobPostingHiringMatrixDataAccess();

        public abstract IJobPostingHiringTeamDataAccess CreateJobPostingHiringTeamDataAccess();

        public abstract IJobPostingNoteDataAccess CreateJobPostingNoteDataAccess();

        public abstract IReqNotesDataAccess CreateReqNotesDataAccess();

        public abstract IJobPostingSkillSetDataAccess CreateJobPostingSkillSetDataAccess();

        public abstract IMemberJobCartDataAccess CreateMemberJobCartDataAccess();

        public abstract IMemberJobCartAlertDataAccess CreateMemberJobCartAlertDataAccess();

        public abstract IMemberJobCartDetailDataAccess CreateMemberJobCartDetailDataAccess();

        public abstract IMemberJobCartAlertSetupDataAccess CreateMemberJobCartAlertSetupDataAccess();       

        public abstract IMemberManagerDataAccess CreateMemberManagerDataAccess();

        public abstract IMemberNoteDataAccess CreateMemberNoteDataAccess();

        public abstract IMemberObjectiveAndSummaryDataAccess CreateMemberObjectiveAndSummaryDataAccess();

        public abstract IMemberPendingJoinersDataAccess CreateMemberPendingJoinersDataAccess();

        public abstract IMemberPrivilegeDataAccess CreateMemberPrivilegeDataAccess();

        public abstract IMemberReferenceDataAccess CreateMemberReferenceDataAccess();

        public abstract IMemberSignatureDataAccess CreateMemberSignatureDataAccess();

        public abstract IMemberSkillMapDataAccess CreateMemberSkillMapDataAccess();

        public abstract IMemberSubmissionDataAccess CreateMemberSubmissionDataAccess();

        public abstract IMemberSkillSetDataAccess CreateMemberSkillSetDataAccess();

        public abstract IMemberSourceHistoryDataAccess CreateMemberSourceHistoryDataAccess();

        public abstract IOccupationalGroupDataAccess CreateOccupationalGroupDataAccess();
        
        public abstract IOccupationalSeriesDataAccess CreateOccupationalSeriesDataAccess();

        public abstract IPasswordResetDataAccess CreatePasswordResetDataAccess();

        public abstract IRecurrenceDataAccess CreateRecurrenceDataAccess();

        public abstract IRejectCandidateDataAccess CreateRejectCandidateDataAccess();

        public abstract IResourceDataAccess CreateResourceDataAccess();

        public abstract ISavedQueryDataAccess CreateSavedQueryDataAccess();

        public abstract ISearchAgentEmailTemplateDataAccess CreateSearchAgentEmailTemplateDataAccess();

        public abstract ISearchAgentScheduleDataAccess CreateSearchAgentScheduleDataAccess();

        public abstract ISiteSettingDataAccess CreateSiteSettingDataAccess();

        public abstract ISkillDataAccess CreateSkillDataAccess();

        public abstract IStateDataAccess CreateStateDataAccess();

        public abstract IZipCodeCityDataAccess CreateZipCodeCityDataAccess();

        public abstract IEmployeeDataAccess CreateEmployeeDataAccess();

        public abstract ICandidateDashboardDataAccess CreateCandidateDashboardDataAccess();

        public abstract ICandidateReportDashboardDataAccess CreateCandidateReportDashboardDataAccess();

        public abstract ICandidateDataAccess CreateCandidateDataAccess();

        public abstract ICandidateSourcingInfoDataAccess CreateCandidateSourcingInfoDataAccess();

        public abstract IMemberInterviewDataAccess CreateMemberInterviewDataAccess();

        public abstract IMemberSkillDataAccess CreateMemberSkillDataAccess();

        public abstract IWebParserDomainDataAccess CreateWebParserDomainDataAccess();

        public abstract IUserAccessApp CreateUserAccessApp();

        public abstract IMailQueueDataAccess CreateMailQueueDataAccess();

        public abstract IInterviewFeedbackDataAccess CreateInterviewFeedbackDataAccess();
        public abstract IInterviewerFeedbackDataAccess CreateInterviewerFeedbackDataAccess(); //Code introduced by Prasanth on 20/Jul/2015
        //Code introduced by Prasanth on 16/Oct/2015 Start
        public abstract IInterviewQuestionBankDataAccess CreateInterviewQuestionBankDataAccess();
        public abstract IInterviewResponseDataAccess CreateInterviewResponseDataAccess();
        public abstract IQuestionBankDataAccess CreateQuestionBankDataAccess();

        //**********************END****************************

        //Code introduced by Pravin on 20/Nov/2015 Start

        public abstract IInterviewPanelDataAccess CreateInterviewPanelDataAccess();

        //**********************END****************************
        //Code introduced by Pravin on 20/Nov/2015 Start

        public abstract IInterviewPanelMasterDataAccess CreateInterviewPanelMasterDataAccess();

        //**********************END****************************


        public abstract IMemberInterviewFeedbackDataAccess CreateMemberInterviewFeedbackDocumentDataAccess(); //Line introduced by Prasanth on 24/Dec/2015 

        public abstract IUserRoleMapEditorDataAccess CreateUserRoleMapEditorDataAccess(); //Line introduced by Pravin khot on 23/Feb/2016 

        //Code introduced by Pravin on 30/Aug/2016 Start

        public abstract IOnLineParserDataAccess CreateOnLineParserDataAccess();

        //**********************END****************************

        //*********Pravin khot � Candidate Hiring Status Update � 3/March/2017 � Start *****************

        public abstract ICandidateHireStatusDataAccess CreateGetCandidateHireStatusDataAccess();

        public abstract ICandidateHireStatusDataAccess CreateHiringMatrixLevelForHiringManagersDataAccess();

        public abstract ICandidateHireStatusDataAccess CreateHireStatusUpdateDataAccess();

        public abstract ICandidateHireStatusDataAccess CreateHiringStatusRoleSaveDataAccess();

        public abstract ICandidateHireStatusDataAccess CreateSendCandidateStatusEmailDataAccess();

        public abstract ICandidateHireStatusDataAccess CreateAllHiringStatusasperRoleId();

        public abstract ICandidateHireStatusDataAccess CreateCustomUserRoleDataAccess();

        //*********Pravin khot � Candidate Hiring Status Update � 3/March/2017 � End ***************** 
        
        #endregion
    }
}