﻿using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IVHCandidateDataAccess

    public interface IVHCandidateDataAccess
    {
        VHCandidate GetById(int Id);

        PagedResponse<VHCandidate> GetPaged(PagedRequest request);

        PagedResponse<VHCandidate> GetPagedOnSearch(PagedRequest request, string allKeys, string anyKey);

        PagedResponse<VHCandidate> GetAllOnSearch(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string lastUpdateDate, string memberId, string rowPerPage, string functionalCategory, string memberPositionId, string minExpYr, string maxExpYr);

        string GetSearchQuery(string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string lastUpdateDate, string memberId, string functionalCategory, string memberPositionId, string minExpYr, string maxExpYr);
    }

    #endregion
}