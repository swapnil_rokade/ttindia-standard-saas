﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IWorkOrderCommissionDataAccess

    public interface IWorkOrderCommissionDataAccess
    {
        WorkOrderCommission Add(WorkOrderCommission workOrderCommission);

        WorkOrderCommission Update(WorkOrderCommission workOrderCommission);

        WorkOrderCommission GetById(int id);

        IList<WorkOrderCommission> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}