﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IInvoiceDetailDataAccess

    public interface IInvoiceDetailDataAccess
    {
        InvoiceDetail Add(InvoiceDetail invoiceDetail);

        InvoiceDetail Update(InvoiceDetail invoiceDetail);

        InvoiceDetail GetById(int id);

        IList<InvoiceDetail> GetAll();

        IList<InvoiceDetail> GetAllByInvoiceId(int invoiceId);

        bool DeleteById(int id);

        bool DeleteByInvoiceId(int invoiceId);
    }

    #endregion
}