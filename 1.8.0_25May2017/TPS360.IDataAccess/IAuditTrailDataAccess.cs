﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IAuditTrailDataAccess

    public interface IAuditTrailDataAccess
    {
        bool AddAutitTrail(AuditTrail auditTrail);
    }

    #endregion 
}
