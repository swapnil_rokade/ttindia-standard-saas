﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IByProductDataAccess

    public interface IByProductDataAccess
    {
        ByProduct Add(ByProduct byProduct);

        ByProduct Update(ByProduct byProduct);

        ByProduct GetById(int id);

        IList<ByProduct> GetAll();

        PagedResponse<ByProduct> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}