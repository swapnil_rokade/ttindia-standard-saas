﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberH1B1DocumentDataAccess

    public interface IMemberH1B1DocumentDataAccess
    {
        MemberH1B1Document Add(MemberH1B1Document memberH1B1Document);

        MemberH1B1Document Update(MemberH1B1Document memberH1B1Document);

        MemberH1B1Document Update(MemberH1B1Document memberH1B1Document, int id);

        MemberH1B1Document GetById(int id);

        IList<MemberH1B1Document> GetAll();

        IList<MemberH1B1Document> GetAllByMemberH1B1Id(int memberH1B1Id);

        bool DeleteById(int id);
    }

    #endregion
}