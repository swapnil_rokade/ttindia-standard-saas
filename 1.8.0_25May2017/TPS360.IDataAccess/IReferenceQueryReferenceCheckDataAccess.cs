using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IReferenceQueryReferenceCheckDataAccess

    public interface IReferenceQueryReferenceCheckDataAccess
    {

        IList<ReferenceQueryReferenceCheck> GetAllByMemberReferenceId(int memberReferenceId);

    }
	
	#endregion
}
