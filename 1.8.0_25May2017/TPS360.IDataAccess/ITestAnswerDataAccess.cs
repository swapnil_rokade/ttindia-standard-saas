﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ITestAnswerDataAccess

    public interface ITestAnswerDataAccess
    {
        TestAnswer Add(TestAnswer testAnswer);

        TestAnswer Update(TestAnswer testAnswer);

        TestAnswer GetById(int id);

        IList<TestAnswer> GetAll();

        bool DeleteById(int id);

        IList<TestAnswer> GetAllByTestQuestionId(int id);

        IList<TestAnswer> GetAllCorrectAnswerByTestQuestionId(int id);
    }

    #endregion
}