﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IProductDocumentDataAccess

    public interface IProductDocumentDataAccess
    {
        ProductDocument Add(ProductDocument productDocument);

        ProductDocument Update(ProductDocument productDocument);

        ProductDocument GetById(int id);

        IList<ProductDocument> GetAll();

        IList<ProductDocument> GetAllByProductId(int productId);

        bool DeleteById(int id);

        bool DeleteByProductId(int productId);
    }

    #endregion
}

