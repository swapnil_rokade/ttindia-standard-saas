﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
namespace TPS360.DataAccess
{
    #region IUserAccessApp

    public interface IUserAccessApp
    {
        int EntryofUserAccessApp(string uname, string sessionId, string AppName, string DomainName, string MacId,int allowedusers, string RequestedIp);
        int DeleteUserAccessApp(string uname, string AppName, string DomainName);
        void UpdateUserAccessApp(string uname, string AppName, string DomainName);
        void EntryUserAccessAppReset(string uname, string AppName, string DomainName);
        int IsAvailableSessionId(string SessionID);
        int InsertSessionOutTime(string uname, int time, string SessionID, string AppName, string DomainName);
        int GetSessionTimeOutValue(string uname, string AppName, string DomainName);
        void UpdateAspStateTempSessions(string uname, string sessionId, string AppName, string DomainName);

        PagedResponse < CurrentSessions> CurrentSessions_GetPaged( PagedRequest request);
       
    }

    #endregion
}
