﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IIndustryCapabilityDataAccess

    public interface IIndustryCapabilityDataAccess
    {
        IndustryCapability Add(IndustryCapability industryCapability);

        IndustryCapability Update(IndustryCapability industryCapability);

        IndustryCapability GetById(int id);

        IList<IndustryCapability> GetAll();

        IList<IndustryCapability> GetAllByIndustryCategoryId(int industryCategoryId);

        PagedResponse<IndustryCapability> GetPaged(PagedRequest request);

        bool DeleteById(int id);
        
    }

    #endregion
}