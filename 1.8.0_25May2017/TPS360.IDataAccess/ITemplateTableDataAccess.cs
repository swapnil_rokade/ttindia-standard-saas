﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ITemplateTableDataAccess

    public interface ITemplateTableDataAccess
    {
        TemplateTable Add(TemplateTable templateTable);

        TemplateTable Update(TemplateTable templateTable);

        TemplateTable GetById(int id);

        IList<TemplateTable> GetAll();

        IList<TemplateTable> GetAllAvailableTable();

        bool DeleteById(int id);
    }

    #endregion
}