﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberSalaryIncrementHistoryDataAccess

    public interface IMemberSalaryIncrementHistoryDataAccess
    {
        MemberSalaryIncrementHistory Add(MemberSalaryIncrementHistory memberSalaryIncrementHistory);

        MemberSalaryIncrementHistory Update(MemberSalaryIncrementHistory memberSalaryIncrementHistory);

        MemberSalaryIncrementHistory GetById(int id);

        IList<MemberSalaryIncrementHistory> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}