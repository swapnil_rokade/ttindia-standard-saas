﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberOnBoardingOffBoardingChecklistDetailDataAccess

    public interface IMemberOnBoardingOffBoardingChecklistDetailDataAccess
    {
        MemberOnBoardingOffBoardingChecklistDetail Add(MemberOnBoardingOffBoardingChecklistDetail memberOnBoardingOffBoardingChecklistDetail);

        MemberOnBoardingOffBoardingChecklistDetail Update(MemberOnBoardingOffBoardingChecklistDetail memberOnBoardingOffBoardingChecklistDetail);

        MemberOnBoardingOffBoardingChecklistDetail GetById(int id);

        IList<MemberOnBoardingOffBoardingChecklistDetail> GetAllByChecklistId(int id);

        IList<MemberOnBoardingOffBoardingChecklistDetail> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}