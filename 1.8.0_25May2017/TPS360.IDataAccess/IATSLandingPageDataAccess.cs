﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{

    #region IAtsLandingPageDataAccess

    public interface IATSLandingPageDataAccess
    {
        ATSLandingPage GetByMemberId(int memberId);
    }

    #endregion
}
