﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IJobPostingDocumentDataAccess

    public interface IJobPostingDocumentDataAccess
    {
        JobPostingDocument Add(JobPostingDocument jobPostingDocument);

        JobPostingDocument Update(JobPostingDocument jobPostingDocument);

        JobPostingDocument GetById(int id);

        IList<JobPostingDocument> GetAllByJobPostingId(int jobPostingId);

        IList<JobPostingDocument> GetAll();        

        bool DeleteById(int id);

        bool DeleteByJobPostingId(int jobPostingId);
    }

    #endregion
}