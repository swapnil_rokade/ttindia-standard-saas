﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IVolumeHireMemberWorkProfileDataAccess

    public interface IVolumeHireMemberWorkProfileDataAccess
    {
        VolumeHireMemberWorkProfile Add(VolumeHireMemberWorkProfile volumeHireMemberWorkProfile);

        VolumeHireMemberWorkProfile Update(VolumeHireMemberWorkProfile volumeHireMemberWorkProfile);

        VolumeHireMemberWorkProfile GetById(int id);

        VolumeHireMemberWorkProfile GetByMemberId(int memberId);

        IList<VolumeHireMemberWorkProfile> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}