﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IPerformanceStandardDataAccess

    public interface IPerformanceStandardDataAccess
    {
        PerformanceStandard Add(PerformanceStandard performanceStandard);

        PerformanceStandard Update(PerformanceStandard performanceStandard);

        PerformanceStandard GetById(int id);

        IList<PerformanceStandard> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}