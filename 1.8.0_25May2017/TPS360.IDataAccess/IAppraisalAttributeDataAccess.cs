﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IAppraisalAttributeDataAccess

    public interface IAppraisalAttributeDataAccess
    {
        AppraisalAttribute Add(AppraisalAttribute appraisalAttribute);

        AppraisalAttribute Update(AppraisalAttribute appraisalAttribute);

        AppraisalAttribute GetById(int id);

        IList<AppraisalAttribute> GetAll();

        PagedResponse<AppraisalAttribute> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}