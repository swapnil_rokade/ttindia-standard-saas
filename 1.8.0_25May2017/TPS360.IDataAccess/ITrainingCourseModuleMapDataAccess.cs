﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ITrainingCourseModuleMapDataAccess

    public interface ITrainingCourseModuleMapDataAccess
    {
        TrainingCourseModuleMap Add(TrainingCourseModuleMap trainingCourseModuleMap);

        TrainingCourseModuleMap Update(TrainingCourseModuleMap trainingCourseModuleMap);

        TrainingCourseModuleMap GetById(int id);

        IList<TrainingCourseModuleMap> GetAll();

        bool DeleteById(int id);

        TrainingCourseModuleMap GetByCourseIdAndModuleId(int courseId, int moduleId);
    }

    #endregion
}