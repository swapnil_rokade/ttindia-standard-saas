﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ILeadCompanyContactDataAccess

    public interface ILeadCompanyContactDataAccess
    {
        LeadCompanyContact Add(LeadCompanyContact leadCompanyContact);       

        LeadCompanyContact GetById(int id);

        IList<LeadCompanyContact> GetAll();

        IList<LeadCompanyContact> GetAllByCompanyId(int leadId, int companyId);

        bool DeleteById(int id);
    }

    #endregion
}