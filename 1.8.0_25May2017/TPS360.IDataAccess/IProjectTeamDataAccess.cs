﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IProjectTeamDataAccess

    public interface IProjectTeamDataAccess
    {
        ProjectTeam Add(ProjectTeam projectTeam);

        ProjectTeam Update(ProjectTeam projectTeam);

        ProjectTeam GetById(int id);

        ProjectTeam GetByProjectIdAndMemberId(int projectId, int memberId);

        IList<ProjectTeam> GetAllByProjectId(int projectId);

        IList<ProjectTeam> GetAll();

        bool DeleteById(int id);

        bool DeleteByProjectId(int projectId);
    }

    #endregion
}