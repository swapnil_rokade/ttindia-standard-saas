﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using System;

namespace TPS360.DataAccess
{
    #region IZipCodeCityDataAccess

    public interface IZipCodeCityDataAccess
    {

        IList<string> GetAllCity(string keyword);

        IList<string> GetAllByCountryId(int countryId);

         
    }

    #endregion
}