﻿using TPS360.Common.BusinessEntities;
using System;

namespace TPS360.DataAccess
{
    #region IConsultantLandingPageDataAccess

    public interface IConsultantLandingPageDataAccess
    {
        ConsultantLandingPageOverAllRecord GetByMemberId(int memberId);
    }

    #endregion
}