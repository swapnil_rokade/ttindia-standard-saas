﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IProjectDocumentDataAccess

    public interface IProjectDocumentDataAccess
    {
        ProjectDocument Add(ProjectDocument projectDocument);

        ProjectDocument Update(ProjectDocument projectDocument);

        ProjectDocument GetById(int id);

        IList<ProjectDocument> GetAllByProjectId(int projectId);

        IList<ProjectDocument> GetAll();

        bool DeleteById(int id);

        bool DeleteByProjectId(int projectId);

        ProjectDocument GetByFileName(string fileName, int projectId);
    }

    #endregion
}