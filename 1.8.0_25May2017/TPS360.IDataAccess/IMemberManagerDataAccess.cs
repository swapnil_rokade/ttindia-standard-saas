﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using System.Collections;
using TPS360.Common .Shared ;
namespace TPS360.DataAccess
{
    #region IMemberManagerDataAccess

    public interface IMemberManagerDataAccess
    {
        MemberManager Add(MemberManager memberManager);

        MemberManager Update(MemberManager memberManager);

        MemberManager GetById(int id);

        MemberManager GetByMemberIdAndManagerId(int memberId, int managerId);

        MemberManager GetPrimaryByMemberId(int memberId);

        IList<MemberManager> GetAllByMemberId(int memberId);

        IList<MemberManager> GetAll();

        bool DeleteById(int id);

        bool DeleteByMemberIdAndManagerId(int memberId, int managerId);

        bool DeleteAllMemberManagerByMemberId(int memberId );

        ArrayList GetListByMemberId(int memberId);

        PagedResponse<Member> GetPagedByMemberId(int MemberID, PagedRequest request);

        string GetPrimaryManagerNameByMemberId(int menerId);
    }

    #endregion
}