﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ITrainingCourseStudyMaterialDataAccess

    public interface ITrainingCourseStudyMaterialDataAccess
    {
        TrainingCourseStudyMaterial Add(TrainingCourseStudyMaterial trainingCourseStudyMaterial);

        TrainingCourseStudyMaterial Update(TrainingCourseStudyMaterial trainingCourseStudyMaterial);

        TrainingCourseStudyMaterial GetById(int id);

        IList<TrainingCourseStudyMaterial> GetAll();

        PagedResponse<TrainingCourseStudyMaterial> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}