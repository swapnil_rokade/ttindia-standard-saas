﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IPositionCompetencyMapDataAccess

    public interface IPositionCompetencyMapDataAccess
    {
        PositionCompetencyMap Add(PositionCompetencyMap positionCompetencyMap);

        PositionCompetencyMap Update(PositionCompetencyMap positionCompetencyMap);

        PositionCompetencyMap GetById(int id);

        IList<PositionCompetencyMap> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}