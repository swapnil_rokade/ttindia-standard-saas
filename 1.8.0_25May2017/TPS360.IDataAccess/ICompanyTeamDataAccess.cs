﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICompanyTeamDataAccess

    public interface ICompanyTeamDataAccess
    {
        CompanyTeam Add(CompanyTeam companyTeam);

        CompanyTeam Update(CompanyTeam companyTeam);

        CompanyTeam GetById(int id);

        IList<CompanyTeam> GetAll();

        IList<CompanyTeam> GetAllByCompanyId(int companyId);

        bool DeleteById(int id);

        bool DeleteByCompanyIdAndMemberId(int companyId, int memberId);
    }

    #endregion
}