﻿using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberGroupMapDataAccess

    public interface IMemberGroupMapDataAccess
    {
        MemberGroupMap Add(MemberGroupMap memberGroupMap);

        MemberGroupMap Update(MemberGroupMap memberGroupMap);

        MemberGroupMap GetById(int id);

        MemberGroupMap GetByMemberIdandMemberGroupId(int memberId, int memberGroupId);

        IList<MemberGroupMap> GetAllByMemberId(int memberId);

        IList<MemberGroupMap> GetAll();

        bool DeleteById(int id);

        bool DeleteByIdandMemberGroupId(int id, int memberGroupId);

        PagedResponse<MemberGroupMap> GetPaged(PagedRequest request);
    }

    #endregion
}