﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ITestModuleDataAccess

    public interface ITestModuleDataAccess
    {
        TestModule Add(TestModule testModule);

        TestModule Update(TestModule testModule);

        TestModule GetById(int id);

        IList<TestModule> GetAll();

        IList<TestModule> GetAllByTestMasterId(int id);

        bool DeleteById(int id);

        TestModule GetTestModuleByNameAndTestMasterId(string name, int id);
    }

    #endregion
}