﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberOrganizationFunctionalityCategoryMapDataAccess

    public interface IMemberOrganizationFunctionalityCategoryMapDataAccess
    {
        MemberOrganizationFunctionalityCategoryMap Add(MemberOrganizationFunctionalityCategoryMap memberOrganizationFunctionalityCategoryMap);

        MemberOrganizationFunctionalityCategoryMap Update(MemberOrganizationFunctionalityCategoryMap memberOrganizationFunctionalityCategoryMap);

        MemberOrganizationFunctionalityCategoryMap GetById(int id);

        IList<MemberOrganizationFunctionalityCategoryMap> GetAll();

        IList<MemberOrganizationFunctionalityCategoryMap> GetAllByOrganizationFunctionalityCategoryMapId(int OrganizationFunctionalityCategoryMapId);

       // IList<MemberOrganizationFunctionalityCategoryMap> GetAllByMemberIdAndorgFncCategoryId(int MemberId, int orgFncCategoryId);

        bool DeleteById(int id);

        bool DeleteByMemberIdAndorgFncCategoryId(int MemberId, int orgFncCategoryId);
    }

    #endregion
}