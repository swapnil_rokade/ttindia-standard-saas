﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICompanyIndustryDataAccess

    public interface ICompanyIndustryDataAccess
    {
        CompanyIndustry Add(CompanyIndustry companyIndustry);

        CompanyIndustry Update(CompanyIndustry companyIndustry);

        CompanyIndustry GetById(int id);

        IList<CompanyIndustry> GetAll();

        IList<CompanyIndustry> GetAllByCompanyId(int companyId);

        bool DeleteById(int id);

        bool DeleteByCompanyId(int companyId);
    }

    #endregion
}