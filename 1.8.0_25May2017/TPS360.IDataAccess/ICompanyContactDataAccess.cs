﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ICompanyContactDataAccess.cs
    Description:  
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              02/dec/2015            Pravin khot   Introduced GetAllByCompanyId_IP
    0.2              26/Feb/2016            pravin khot   Introduced by GetCompanyContactByEmailAndCompanyId
    0.3               23/March/2017         pravin khot   Introduced by GetCompanyContactByJobPostingHiringTeam
 * ----------------------------------------------------------------------------------------------------------------------------------------------
*/
using System.Collections;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;

namespace TPS360.DataAccess
{
    #region ICompanyContactDataAccess

    public interface ICompanyContactDataAccess
    {
        CompanyContact Add(CompanyContact companyContact);

        CompanyContact Update(CompanyContact companyContact);

        bool UpdateMemberId(int contactId, int memberId);

        bool UpdateResumeFile(int contactId, string fileName);

        CompanyContact GetById(int id);

        CompanyContact GetByEmail(string email);

        CompanyContact GetByMemberId(int memberId);

        CompanyContact GetPrimary(int companyId);

        IList<CompanyContact> GetAll();

        PagedResponse<CompanyContact> GetPaged(PagedRequest request);

        IList<CompanyContact> GetAllByCompanyId(int companyId);

        IList<CompanyContact> GetAllByCompanyGroupId(int companyGroupId);

        IList<CompanyContact> GetAllByComapnyIdAndLogin(int companyId, bool IsLoginCreated);

        bool DeleteById(int id);

        bool DeleteByCompanyId(int companyId);

        IList<CompanyContact> GetAllByVolumeHireGroup();

        IList<CompanyContact> GetAllContactByIdAndDate(int ID, string Date);

        ArrayList GetAllCompanyContactsByCompanyId(int CompanyId);

        PagedResponse<ListWithCount> GetPagedVendorSubmissionSummary(PagedRequest request, string type);

        IList<ListWithCount> GetAllVendorSubmissionsGroupByDate(int JobPostingId, int VendorId,int ContactID, int StartDate, int EndDate);

        PagedResponse<VendorSubmissions> GetPagedVendorSubmissions(PagedRequest request);

        //-------------code added by pravin khot 02/dec/2015 ---------------
        IList<CompanyContact> GetAllByCompanyId_IP(int companyId);
        //---------------------end--------------------------------
        CompanyContact GetCompanyContactByEmailAndCompanyId(string email, int CompanyId);//code added by pravin khot on 26/Feb/2016
        string GetCompanyContactByJobPostingHiringTeam(int CompanyContactId); //code added by pravin khot on 23/March/2017

    }

    #endregion
}