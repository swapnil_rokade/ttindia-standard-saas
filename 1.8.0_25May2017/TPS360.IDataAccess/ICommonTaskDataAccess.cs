﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ICommonTaskDataAccess

    public interface ICommonTaskDataAccess
    {
        CommonTask Add(CommonTask commonTask);

        CommonTask Update(CommonTask commonTask);

        CommonTask GetById(int id);

        IList<CommonTask> GetAllByTaskCategoryId(int taskCategoryId);

        IList<CommonTask> GetAll();

        bool DeleteById(int id);

        PagedResponse<CommonTask> GetPagedByUnAssignedList(PagedRequest pageRequest);

        PagedResponse<CommonTask> GetPagedByOpenList(PagedRequest pageRequest);

        PagedResponse<CommonTask> GetPagedByCompletedList(PagedRequest pageRequest);
    }

    #endregion
}