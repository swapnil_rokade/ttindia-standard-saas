﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ITrainingProgramDetailDataAccess

    public interface ITrainingProgramDetailDataAccess
    {
        TrainingProgramDetail Add(TrainingProgramDetail trainingProgramDetail);

        TrainingProgramDetail Update(TrainingProgramDetail trainingProgramDetail);

        TrainingProgramDetail GetById(int id);

        IList<TrainingProgramDetail> GetAll();

        PagedResponse<TrainingProgramDetail> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}