﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IGoalCommentDataAccess

    public interface IGoalCommentDataAccess
    {
        GoalComment Add(GoalComment goalComment);

        GoalComment Update(GoalComment goalComment);

        GoalComment GetById(int id);

        IList<GoalComment> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}