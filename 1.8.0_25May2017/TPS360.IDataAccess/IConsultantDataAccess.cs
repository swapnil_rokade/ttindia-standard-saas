﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IConsultantDataAccess.cs
    Description: This page defenes interface for Candidate Data Access.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.        Date               Author           Modification
    ------------------------------------------------------------------------------------------------------------------------------    
    0.1            Dec-02-2008        Shivanand        Defect #8745; New parameter "state" is added in the method signature of "GetAllOnSearch()".
                                                                     New parameter "state" is added in the method signature of "GetSearchQuery()".   
-------------------------------------------------------------------------------------------------------------------------------------------        
*/

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;
using System.Collections;
using System.Collections.Generic;

namespace TPS360.DataAccess
{
    #region IConsultantDataAccess

    public interface IConsultantDataAccess
    {
        Consultant GetById(int id);

        IList<Consultant> GetAllConsultantByMemberId(int memberId);

        Consultant GetByIdForHR(int id);
        
        PagedResponse<Consultant> GetPaged(PagedRequest request);

        PagedResponse<Consultant> GetPagedForDashboard(PagedRequest request);

        PagedResponse<Consultant> GetPagedForReport(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent,bool isEngaged,bool isBench,DateTime payementFrom,
            DateTime payementTo,int payementType,int assignedManager);

        PagedResponse<Consultant> GetAllOnSearch(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string minExpYr, string maxExpYr, string state, int maxResults); //0.1  // 9372

        PagedResponse<Consultant> GetAllOnSearchForSavedQuery(PagedRequest request, string whereClause, string memberId, string rowPerPage);

        string GetSearchQuery(string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string memberPositionId, string minExpYr, string maxExpYr, string state); // 0.1

        ArrayList GetAllName();

        ArrayList GetAllNameByWorkOrderId(int workorderId);
    }

    #endregion
}