﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IOnLineParserDataAccess.cs
    Description: This is the DataAccess  library page.
    Created By: Pravin khot
    Created On: 20/nov/2015
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    
*/
using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;



namespace TPS360.DataAccess
{
    #region IOnLineParserDataAccess

    public interface IOnLineParserDataAccess
    {
        OnLineParser Add(OnLineParser OnLineParser);       

        OnLineParser Update(OnLineParser OnLineParser);       

        OnLineParser GetById(int id);

        IList<OnLineParser> GetAll();
        IList<OnLineParser> GetAll(string SortExpression);
        PagedResponse<OnLineParser> GetPaged(PagedRequest request);

        bool DeleteById(int InterviewPanelMaster_ById);
        OnLineParser GetByPath(string path, int CurrentMemberId);
        int ParserCount_GetByUserId(int Userid);
    }

    #endregion
}