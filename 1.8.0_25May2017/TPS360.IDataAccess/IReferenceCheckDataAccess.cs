using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IReferenceCheckDataAccess
	
	public interface IReferenceCheckDataAccess
    {
        ReferenceCheck Add(ReferenceCheck referenceCheck);
		
		ReferenceCheck Update(ReferenceCheck referenceCheck);
		
		ReferenceCheck GetById(int id);

        ReferenceCheck GetByMemberReferenceId(int membermeferenceId);
		
		IList<ReferenceCheck> GetAll();

		bool DeleteById(int id);
    }
	
	#endregion
}
