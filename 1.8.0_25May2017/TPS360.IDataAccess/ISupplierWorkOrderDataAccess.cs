﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ISupplierWorkOrderDataAccess

    public interface ISupplierWorkOrderDataAccess
    {
        SupplierWorkOrder Add(SupplierWorkOrder supplierWorkOrder);

        SupplierWorkOrder Update(SupplierWorkOrder supplierWorkOrder);

        SupplierWorkOrder GetById(int id);

        IList<SupplierWorkOrder> GetAll();

        PagedResponse<SupplierWorkOrder> GetAllOnSearch(PagedRequest request, string vendorId);

        string GetSearchQuery(string vendorId);

        bool DeleteById(int id);
    }

    #endregion
}