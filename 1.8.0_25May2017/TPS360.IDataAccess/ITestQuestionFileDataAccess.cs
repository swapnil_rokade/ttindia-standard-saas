﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ITestQuestionFileDataAccess

    public interface ITestQuestionFileDataAccess
    {
        TestQuestionFile Add(TestQuestionFile testQuestionFile);

        TestQuestionFile Update(TestQuestionFile testQuestionFile);

        TestQuestionFile GetById(int id);

        TestQuestionFile GetByTestQuestionId(int testQuestionId);

        IList<TestQuestionFile> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}