﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IWorkOrderManagerDataAccess

    public interface IWorkOrderManagerDataAccess
    {
        WorkOrderManager Add(WorkOrderManager workOrderManager);

        WorkOrderManager Update(WorkOrderManager workOrderManager);

        WorkOrderManager GetById(int id);

        IList<WorkOrderManager> GetAll();

        bool DeleteById(int id);

        bool DeleteBySupplierWorkOrderId(int supplierWorkOrderId);

        IList<WorkOrderManager> GetAllBySupplierWorkOrderId(int supplierWorkOrderId);
    }

    #endregion
}