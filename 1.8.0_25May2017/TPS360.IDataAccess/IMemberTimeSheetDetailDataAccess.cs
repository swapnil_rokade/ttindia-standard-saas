﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IMemberTimeSheetDetailDataAccess.cs
    Description: This is the class library page.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Feb-11-2009           Jagadish            Defect ID: 8871; Added an overload for method 'GetAllByMemberTimeSheetId'.
-------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberTimeSheetDetailDataAccess

    public interface IMemberTimeSheetDetailDataAccess
    {
        MemberTimeSheetDetail Add(MemberTimeSheetDetail memberTimeSheetDetail);

        MemberTimeSheetDetail Update(MemberTimeSheetDetail memberTimeSheetDetail);

        MemberTimeSheetDetail GetById(int id);

        IList<MemberTimeSheetDetail> GetAllByMemberTimeSheetId(int memberTimeSheetId);

        //0.1
        IList<MemberTimeSheetDetail> GetAllByMemberTimeSheetId(int memberTimeSheetId, string sortExpression);

        IList<MemberTimeSheetDetail> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}