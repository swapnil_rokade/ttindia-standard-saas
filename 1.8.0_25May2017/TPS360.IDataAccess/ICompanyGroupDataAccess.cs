﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ICompanyGroupDataAccess

    public interface ICompanyGroupDataAccess
    {
        CompanyGroup Add(CompanyGroup companyGroup);

        CompanyGroup Update(CompanyGroup companyGroup);

        CompanyGroup GetById(int id);

        IList<CompanyGroup> GetAll();

        IList<CompanyGroup> GetAllByGroupType(int groupType);

        PagedResponse<CompanyGroup> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}