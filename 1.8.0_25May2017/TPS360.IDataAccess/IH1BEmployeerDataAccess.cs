﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IH1BEmployeerDataAccess

    public interface IH1BEmployeerDataAccess
    {
        H1BEmployeer Add(H1BEmployeer h1BEmployeer);

        H1BEmployeer Update(H1BEmployeer h1BEmployeer);

        H1BEmployeer GetById(int id);

        IList<H1BEmployeer> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}