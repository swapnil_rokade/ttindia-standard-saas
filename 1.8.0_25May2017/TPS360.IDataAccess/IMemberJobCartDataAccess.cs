﻿/*Modification Log:
   Ver.No.             Date                  Author              Modification
   
 * 0.1             26-Feb-2009             Sandeesh            Defect Fix - ID: 9671; Added new method to implement sorting.
 * 0.2             25/Jan/2016             pravin khot         Introduced by CareerPortalToRequisitionAdd.
 * 0.3             27/April/2016           pravin khot         Introduced by AddCandidateToMultipleRequisition
 * 0.4             24/June/2016            pravin khot         added-RejectById
 * ------------------------------------------------------------------------------------------------------------    
*/
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberJobCartDataAccess

    public interface IMemberJobCartDataAccess
    {
        MemberJobCart Add(MemberJobCart memberJobCart);

        MemberJobCart Update(MemberJobCart memberJobCart);

        MemberJobCart GetById(int id);

        IList<MemberJobCart> GetAll();

        IList<MemberJobCart> GetAllByMemberId(int memberId);
        int CareerPortalToRequisitionAdd(string CanIds); //code added by pravin khot on 25/Jan/2016


        IList<MemberJobCart> GetAllByMemberId(int memberId, string sortExpression); //Defect # 8841

        IList<MemberJobCart> GetAllByMemberIdAndSelectionStep(int memberId, int selectionStep);

        MemberJobCart GetByMemberIdAndJobPostingId(int memberId, int jobpostingId);  

        PagedResponse<MemberJobCart> GetPaged(PagedRequest request);

        //Candidate Status in Employee Referral Portal.
        PagedResponse<MemberJobCart> GetPagedForEmployeeReferal(PagedRequest request);
        bool DeleteById(int id);
        bool RejectToUnRejectCandidate(string MemberIds, int JobPostingId); //added by pravin khot on 24/June/2016

        bool RejectById(int id);//added by pravin khot on 24/June/2016

        IList<MemberJobCart> GetAllByJobPostingIdAndSelectionStep(int jobPostingId, int selectionStep);

        int[] GetHiringMatrixMemberCount(int jobPostingId);
        int[] GetHiringMatrixMemberCount(int jobPostingId, string dateWhenCountTobeDone);
        //0.1 Start
        IList<MemberJobCart> GetAllSubmittedCandidateInterviewScore(int memberId, int selectionStep, string sortExpression);
        //0.1 End

        void MoveToNextLevel(int CurrentLevel, int UpdatorId, string MemberIds, int JobPostingID,string MovingDirection);


        int AddCandidateToRequisition(int CreatorId, string MemberIds, int JobPostingId);
        int AddCandidateToMultipleRequisition(int CreatorId, string MemberIds, int JobPostingId);//code added by pravin khot on 27/April/2016


        void  UpdateByStatus(int RequisitionId,string MemberId,int StatusId,int UpdatorId);

        void UpdateSourceByMemberIDandJobPostingID(int MemberId, int JobPostingId, int SourceId);

        IList<int> CheckCandidatesInHiringMatrix(string CandidateIds,int JobpostingId);

        string GetRecruiternameByMemberId(int memberId);

        string GetCandidateStatusByMemberIdAndJobPostingId(int Memberid, int JobPostingId);

        int GetActiveRequisitionCountByMemberId(int memberId);
    }

    #endregion
}