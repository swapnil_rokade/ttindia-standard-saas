﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICampaignColdCallScriptDataAccess

    public interface ICampaignColdCallScriptDataAccess
    {
        CampaignColdCallScript Add(CampaignColdCallScript campaignColdCallScript);

        CampaignColdCallScript Update(CampaignColdCallScript campaignColdCallScript);

        CampaignColdCallScript GetById(int id);

        IList<CampaignColdCallScript> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}