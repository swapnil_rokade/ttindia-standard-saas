﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IPositionDataAccess

    public interface IPositionDataAccess
    {
        Position Add(Position position);

        Position Update(Position position);

        Position GetById(int id);

        IList<Position> GetAll();

        IList<Position> GetAllParent();

        IList<Position> GetAllPositionByParentId(int parentId, string search);

        PagedResponse<Position> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}