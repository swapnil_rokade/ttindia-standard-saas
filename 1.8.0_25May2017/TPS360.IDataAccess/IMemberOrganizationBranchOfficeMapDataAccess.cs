﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberOrganizationBranchOfficeMapDataAccess

    public interface IMemberOrganizationBranchOfficeMapDataAccess
    {
        MemberOrganizationBranchOfficeMap Add(MemberOrganizationBranchOfficeMap memberOrganizationBranchOfficeMap);

        MemberOrganizationBranchOfficeMap Update(MemberOrganizationBranchOfficeMap memberOrganizationBranchOfficeMap);

        MemberOrganizationBranchOfficeMap GetById(int id);

        MemberOrganizationBranchOfficeMap GetByMemberIdAndBranchOfficeId(int memberId,int branchOfficeId);

        IList<MemberOrganizationBranchOfficeMap> GetAll();

        IList<MemberOrganizationBranchOfficeMap> GetAllByMemberId(int MemberId);

        bool DeleteById(int id);

        bool DeleteByMemberId(int MemberId);
    }

    #endregion
}