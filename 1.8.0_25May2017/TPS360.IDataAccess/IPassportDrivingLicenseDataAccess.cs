﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IPassportDrivingLicenseDataAccess

    public interface IPassportDrivingLicenseDataAccess
    {
        PassportDrivingLicense Add(PassportDrivingLicense passprotDrivingLicense);

        PassportDrivingLicense Update(PassportDrivingLicense passprotDrivingLicense);

        PassportDrivingLicense GetById(int id);

        PassportDrivingLicense GetByMemberId(int id);

        IList<PassportDrivingLicense> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}