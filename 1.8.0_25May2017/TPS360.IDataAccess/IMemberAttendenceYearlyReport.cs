﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberAttendenceDataAccess

    public interface IMemberAttendenceYearlyReport
    {
        IList<MemberAttendenceYearlyReport> GetMemberAttendeceYearlyReport(Int32 year, Int32 memberId);
    }

    #endregion
}
