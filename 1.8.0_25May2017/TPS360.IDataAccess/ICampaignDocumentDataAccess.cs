﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICampaignDocumentDataAccess

    public interface ICampaignDocumentDataAccess
    {
        CampaignDocument Add(CampaignDocument campaignDocument);

        CampaignDocument Update(CampaignDocument campaignDocument);

        CampaignDocument GetById(int id);

        IList<CampaignDocument> GetAll();

        IList<CampaignDocument> GetAllByCampaignId(int campaignId);

        bool DeleteById(int id);

        bool DeleteByCampaignId(int campaignId);
    }

    #endregion
}

