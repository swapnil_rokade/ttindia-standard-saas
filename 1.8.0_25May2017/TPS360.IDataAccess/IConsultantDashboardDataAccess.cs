﻿using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IConsultantDashboardDataAccess

    public interface IConsultantDashboardDataAccess
    {
        ConsultantDashboard GetByMemberId(int memberId);
    }

    #endregion
}
