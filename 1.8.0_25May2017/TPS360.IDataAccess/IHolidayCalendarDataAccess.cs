﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IHolidayCalendarDataAccess

    public interface IHolidayCalendarDataAccess
    {
        HolidayCalendar Add(HolidayCalendar holidayCalendar);

        HolidayCalendar Update(HolidayCalendar holidayCalendar);

        HolidayCalendar GetById(int id);

        IList<HolidayCalendar> GetAll();

        PagedResponse<HolidayCalendar> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}