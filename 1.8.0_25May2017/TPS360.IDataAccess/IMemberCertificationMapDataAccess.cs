﻿using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberCertificationMapDataAccess

    public interface IMemberCertificationMapDataAccess
    {
        MemberCertificationMap Add(MemberCertificationMap memberCertificationMap);

        MemberCertificationMap Update(MemberCertificationMap memberCertificationMap);

        MemberCertificationMap GetById(int id);

        IList<MemberCertificationMap> GetAll();

        IList<MemberCertificationMap> GetAllByMemberId(int memberId);

        MemberCertificationMap GetByCertificationName(string CertificationName);


        PagedResponse<MemberCertificationMap> GetPaged(PagedRequest request);

        PagedResponse<MemberCertificationMap> GetPagedByMemberId(PagedRequest request);
       
        bool DeleteById(int id);
        
        bool DeleteByMemberId(int MemberId);

    }

    #endregion
}