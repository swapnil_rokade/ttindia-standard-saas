﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IHiringMatrixDataAccess

    public interface IHiringMatrixDataAccess
    {
        PagedResponse<HiringMatrix> GetPaged(PagedRequest request);

        PagedResponse<HiringMatrix> MinGetPaged(PagedRequest request);

        PagedResponse<HiringMatrix> MinGetPagedAll(PagedRequest request);

        HiringMatrix GetByJobPostingIDandCandidateId(int JobPostingId, int CandidateId);
        ////////////////////////////Sumit Sonawane 1/Mar/2017///////////////////////////////////////////////////////////////////
        HiringMatrix getMatrixLevelByJobPostingIDAndCandidateId(int JobPostingId, int CandidateId);
        HiringMatrix getHiringMatrixLogDetails(HiringMatrix AddHiringMatrixLogDetails);
        ////////////////////////////Sumit Sonawane 22/07/2016///////////////////////////////////////////////////////////////////
        PagedResponse<HiringMatrix> GetPagedBucketView(PagedRequest request);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////Sumit Sonawane 27/07/2016///////////////////////////////////////////////////////////////////
        PagedResponse<HiringMatrix> BucketView(PagedRequest request);
        PagedResponse<HiringMatrix> BucketViewLoadAll(PagedRequest request);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    #endregion
}