﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IGuestInterviewerDataAccess

    public interface IGuestInterviewerDataAccess
    {
        GuestInterviewer Add(GuestInterviewer guestInterviewer);

        GuestInterviewer Update(GuestInterviewer guestInterviewer);

        GuestInterviewer GetById(int id);

        IList<GuestInterviewer> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}