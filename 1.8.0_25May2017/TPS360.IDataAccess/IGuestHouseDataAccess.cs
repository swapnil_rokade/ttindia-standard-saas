﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IGuestHouseDataAccess

    public interface IGuestHouseDataAccess
    {
        GuestHouse Add(GuestHouse guestHouse);

        GuestHouse Update(GuestHouse guestHouse);

        GuestHouse GetById(int id);

        IList<GuestHouse> GetAll();

        PagedResponse<GuestHouse> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}