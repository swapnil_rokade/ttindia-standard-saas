﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICompanyReferenceDataAccess

    public interface ICompanyReferenceDataAccess
    {
        CompanyReference Add(CompanyReference companyReference);

        CompanyReference Update(CompanyReference companyReference);

        CompanyReference GetById(int id);

        IList<CompanyReference> GetAll();

        IList<CompanyReference> GetAllByCompanyId(int companyId);

        bool DeleteById(int id);

        bool DeleteByCompanyId(int companyId);
    }

    #endregion
}