﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberAlertDataAccess

    public interface IMemberAlertDataAccess
    {
        MemberAlert Add(MemberAlert memberAlert);

        MemberAlert Update(MemberAlert memberAlert);

        MemberAlert GetById(int id);

        IList<MemberAlert> GetAll();

        IList<MemberAlert> GetAllByMemberId(int memberId);

        PagedResponse<MemberAlert> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}