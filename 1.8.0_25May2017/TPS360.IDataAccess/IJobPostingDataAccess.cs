﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IJobPostingDataAccess.cs
    Description: This is used for job posting data access
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Dec-10-2008          Yogeesh Bhat         Defect id: 9398; Added new methods GetRequisitionCountByMemberId, UpdateMemberRequisitionCount
 *  0.2              Mar-10-2009            Nagarathna          Defect Id:10068; Added new method.
 *  0.3              Nov-25-2009           Gopala Swamy J        Defect Id:11588 :Added one parameter
 *  0.4             Feb-22-2010           Nagarathna V.B        Enhancement Id:12129; Submission Report
 *  0.5             22/May/2015            Prasanth Kumar G      Introduced GetRequisitionAgingReport
 *  0.6             8/Jan/2016             Pravin khot           Introduced by MemberCareerOpportunities
 *  0.7             18/Jan/2016            pravin khot            Introduced by AddCareerJob
 *  0.8             29/Jan/2016             pravin khot           Introduced by GetAllByBUID
 *  0.9             2/Feb/2016              Pravin khot           Introduced by GetRequisitionSourceBreakupReport
 *  1.0             25/May/2016             pravin khot           Introduced by GetRequisitionStatusBy_Id,GetRequisitionStatus_Id,GetTimeZoneId_MemberId
     1.1            8/June/2016             pravin khot            addded GetAllByCandidateId
-------------------------------------------------------------------------------------------------------------------------------------------       
*/

using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;
using System.Data;
using System;
namespace TPS360.DataAccess
{
    #region IJobPostingDataAccess

    public interface IJobPostingDataAccess
    {
        JobPosting Add(JobPosting jobPosting);

        JobPosting Update(JobPosting jobPosting);

        JobPosting GetById(int id);

        IList<JobPosting> GetAll();

        IList<JobPosting> GetAllRequisitionByEmployeeId(int memberId);

        IList<JobPosting> GetAllByMemberId(int memberId);

        ArrayList GetAllByCandidatesId(int ClientId, int CandidateId);//added by pravin khot on 8/June/2016

        IList<JobPosting> GetAllByJobCartAlertId(int jobCartAlertId);

        IList<JobPosting> GetAllByProjectId(int projectId);

        IList<JobPosting> GetAllByProjectId(int projectId, string sortExpression);//0.2
        int GetCountOfJobPostingByStatusAndManagerId(int ManagerId);

        int GetRequisitionStatusBy_Id(int JobPostingId); //added by pravin khot on 25/May/2016
        string  GetRequisitionStatus_Id(int JobPostingId); //added by pravin khot on 25/May/2016
        int GetTimeZoneId_MemberId(int MemberId); //added by pravin khot on 25/May/2016

        ArrayList  GetPagedJobPostingByStatusAndManagerId(int status, int ManagerId, int count, string SearchKey);

        PagedResponse<JobPosting> GetPaged(PagedRequest request);

        PagedResponse<JobPosting> GetPagedWithCandidateCount(PagedRequest request);

        PagedResponse<JobPosting> GetPaged(string allKeys, string anyKey, string jobTitle, string jobType, string city, string lastUpdateDate, int jobStatus, string jobIndustry, PagedRequest request); // 8971

        bool DeleteById(int id);

        JobPosting CreateFromExistingJob(int id, string jobPostingCode,int creatorId); //0.3

        JobPosting CreateProjectJobFromExistingTemplateJob(int projectId, int jobId);

        ArrayList GetAllByStatus(int status);

        ArrayList GetAllByStatusId(int statusId);


        ArrayList GetAllByCleintId(int ClientId);

        ArrayList GetAllByCleintIdAndManagerId(int ClientId, int ManagerId);

        ArrayList GetByCleintIdAndManagerId(int ClientId, int ManagerId, int count, string SearchKey);


        ArrayList GetAllByInterview();

        DataTable GetAllJobPostingByStatus(int status);

        ArrayList GetAllByStatusAndManagerId(int status, int managerId);

        ArrayList GetAllJobPostingByCandidateId(int candidateId);
        PagedResponse<JobPosting> GetReport(PagedRequest request);

        // Code introduced by Prasanth on 22/May/2015 Start
        //PagedResponse<JobPosting> GetRequisitionAgingReport(string IsTemplate, string Account, string ReqStartDateFrom, string ReqEndDateTo, string Recruiter);
        PagedResponse<JobPosting> GetRequisitionAgingReport(PagedRequest request);
        //***********************END**************************

        //****************code added by pravin khot on 15/Jan/2016************************
        IList<JobPosting> MemberCareerOpportunities();
        JobPosting AddCareerJob(JobPosting jobPosting); //Code added by pravin khot on 18/Jan/2016
        ArrayList GetAllByBUID(int BUId);    //Code added by pravin khot on 29/Jan/2016  
        //***********************END**************************

        // Code introduced by Pravin khot on 2/Feb/2016 Start
        PagedResponse<JobPosting> GetRequisitionSourceBreakupReport(PagedRequest request);
        //***********************END**************************

        //0.4  12129
        PagedResponse<Submission> GetSubmissionReport(PagedRequest request);
    
        PagedResponse<Submission> GetDashBoardSubmission(PagedRequest request);

        PagedResponse<JobPosting> GetPagedVolumeHire(string allKeys, string anyKey, string jobTitle, string jobType, string city, string lastUpdateDate, string volumeHire, int jobStatus, string jobIndustry, PagedRequest request);//9811

        IList<JobPosting> GetAllVolumeHireByStatus(int jobStatus);

        int GetRequisitionCountByMemberId(int MemberId);

        int GetLastRequisitionCodeByMemberId(int MemberId);
        //0.1
        int UpdateMemberRequisitionCount(int MemberId, int RequisitionCount);   //0.1

        int GetCountOfJobPostingByStatus();
        JobPosting GetByMemberIdAndJobPostingCode(int MemberId, string JobPostingCode);

        string GetJobPosting_RequiredSkillNamesByJobPostingId(int JobPostingId);

        string  GetJobPostingIdByJobPostingCode(string JobPostingCode);

        void UpdateStatusById(int statusid, int jobpostingid,int updatorid);
        PagedResponse<JobPosting> GetPagedForCandidatePortal(PagedRequest request, int MemberId);


        PagedResponse<JobPosting> GetPagedForVendorPortal(PagedRequest request,int VendorId);

        PagedResponse<JobPosting> GetPagedForVendorPortalForMyPerformance(PagedRequest request, int VendorId, int MemberId);

        PagedResponse<JobPosting> GetPagedEmployeeReferal(PagedRequest request);

        ArrayList GetAllEmployeeReferralEnabled();

        ArrayList GetAllVendorPortalEnabled();

        ArrayList GetJobPostingIdByReferrerIdForERPortal(int ReferrerId);

        ArrayList GetAllBUId(int BUId);  //added by pravin khot on 9/Aug/2016
        ArrayList GetAllBymemberId(int memberId);
    }

    #endregion
}