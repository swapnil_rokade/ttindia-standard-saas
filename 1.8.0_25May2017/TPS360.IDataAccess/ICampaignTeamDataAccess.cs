﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICampaignTeamDataAccess

    public interface ICampaignTeamDataAccess
    {
        CampaignTeam Add(CampaignTeam campaignTeam);

        CampaignTeam Update(CampaignTeam campaignTeam);

        CampaignTeam GetById(int id);

        IList<CampaignTeam> GetAll();

        IList<CampaignTeam> GetAllByMemberId(int memberId);

        IList<CampaignTeam> GetAllByCampaignId(int campaignId);

        bool DeleteById(int id);

        bool DeleteByCampaignId(int campaignId);
    }

    #endregion
}

