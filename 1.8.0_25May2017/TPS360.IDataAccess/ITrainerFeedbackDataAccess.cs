﻿using System;
using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ITrainerFeedbackDataAccess

    public interface ITrainerFeedbackDataAccess
    {
        TrainerFeedback Add(TrainerFeedback trainerFeedback);

        TrainerFeedback Update(TrainerFeedback trainerFeedback);

        TrainerFeedback GetById(int id);

        IList<TrainerFeedback> GetAll();

        PagedResponse<TrainerFeedback> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}