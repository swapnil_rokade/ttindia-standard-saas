﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IPositionIndustryCapabilityMapDataAccess

    public interface IPositionIndustryCapabilityMapDataAccess
    {
        PositionIndustryCapabilityMap Add(PositionIndustryCapabilityMap positionIndustryCapabilityMap);

        PositionIndustryCapabilityMap Update(PositionIndustryCapabilityMap positionIndustryCapabilityMap);

        PositionIndustryCapabilityMap GetById(int id);

        IList<PositionIndustryCapabilityMap> GetAll();

        IList<PositionIndustryCapabilityMap> GetAllByPositionId(int positionId);

        bool DeleteById(int id);

        bool DeleteByPositionId(int positionId);

        PositionIndustryCapabilityMap GetByPositionIdAndIndustryCapabilityId(int positionId, int industryCapabilityId);
    }

    #endregion
}