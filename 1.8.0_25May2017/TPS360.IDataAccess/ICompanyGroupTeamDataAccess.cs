﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICompanyGroupTeamDataAccess

    public interface ICompanyGroupTeamDataAccess
    {
        CompanyGroupTeam Add(CompanyGroupTeam companyGroupTeam);

        CompanyGroupTeam Update(CompanyGroupTeam companyGroupTeam);

        CompanyGroupTeam GetById(int id);

        CompanyGroupTeam GetManager(int companyGroupId);

        IList<CompanyGroupTeam> GetAll();

        IList<CompanyGroupTeam> GetAllByCompanyGroupId(int companyGroupId);

        bool DeleteById(int id);

        bool DeleteByCompanyGroupIdAndMemberId(int companyGroupId, int memberId);
    }

    #endregion
}