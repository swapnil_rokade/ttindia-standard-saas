﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IOrganizationContactDataAccess

    public interface IOrganizationContactDataAccess
    {
        OrganizationContact Add(OrganizationContact organizationContact);

        OrganizationContact Update(OrganizationContact organizationContact);

        OrganizationContact GetById(int id);

        OrganizationContact GetByOrgIdAndMemberId(int orgId, int memberId);

        OrganizationContact GetPrimaryContactByOrganizationBranchOfficeId(int OrganizationBranchOfficeId);

        IList<OrganizationContact> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}