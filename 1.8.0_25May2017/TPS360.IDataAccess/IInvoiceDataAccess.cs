﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IInvoiceDataAccess

    public interface IInvoiceDataAccess
    {
        Invoice Add(Invoice invoice);

        Invoice Update(Invoice invoice);

        Invoice GetById(int id);

        IList<Invoice> GetAll();

        PagedResponse<Invoice> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}