﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName:ICountryDataAccess.cs
    Description: This page is used for Countries  functionality
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              MOdification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetCountryIdByCountryCode() to assist 
                                                                Resume parser team
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
*/

using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using System;

namespace TPS360.DataAccess
{
    #region ICountryDataAccess

    public interface ICountryDataAccess
    {
        Country Add(Country country);

        Country Update(Country country);

        Country GetById(int id);

        IList<Country> GetAll();

        bool DeleteById(int id);

        Int32 GetCountryIdByCountryName(string countryName);

        //0.1 starts here

        Int32 GetStateIdByStateCode(string StateCode);

        Int32 GetCountryIdByCountryCode(string CountryCode);

        Int32 GetStateIdByStateCodeAndCountryId(string StateCode,int CountryId);

        Int32 GetStateIdByStateNameAndCountryId(string Name, int CountryId);

        string GetCountryNameById(int CountryId);
        //0.1 end here

    }

    #endregion
}