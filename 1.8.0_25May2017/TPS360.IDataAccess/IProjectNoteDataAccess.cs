﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IProjectNoteDataAccess

    public interface IProjectNoteDataAccess
    {
        ProjectNote Add(ProjectNote projectNote);        

        ProjectNote GetById(int id);

        IList<ProjectNote> GetAllByProjectId(int projectId);

        IList<ProjectNote> GetAll();

        bool DeleteById(int id);

        bool DeleteByProjectId(int projectId);
    }

    #endregion
}