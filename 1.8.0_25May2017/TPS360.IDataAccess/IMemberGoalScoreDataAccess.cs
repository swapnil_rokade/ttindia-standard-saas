﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberGoalScoreDataAccess

    public interface IMemberGoalScoreDataAccess
    {
        MemberGoalScore Add(MemberGoalScore memberGoalScore);

        MemberGoalScore Update(MemberGoalScore memberGoalScore);

        MemberGoalScore GetById(int id);

        IList<MemberGoalScore> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}