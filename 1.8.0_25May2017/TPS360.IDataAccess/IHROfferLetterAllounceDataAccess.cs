﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IHROfferLetterAllounceDataAccess

    public interface IHROfferLetterAllounceDataAccess
    {
        HROfferLetterAllounce Add(HROfferLetterAllounce hROfferLetterAllounce);

        HROfferLetterAllounce Update(HROfferLetterAllounce hROfferLetterAllounce);

        HROfferLetterAllounce GetById(int id);

        IList<HROfferLetterAllounce> GetAll();

        IList<HROfferLetterAllounce> GetAllByHROfferLetterId(int hROfferLetterId);

        bool DeleteByHROfferLetterId(int hROfferLetterId);

        bool DeleteById(int id);
    }

    #endregion
}