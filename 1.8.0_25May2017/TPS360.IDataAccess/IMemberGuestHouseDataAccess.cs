﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberGuestHouseDataAccess

    public interface IMemberGuestHouseDataAccess
    {
        MemberGuestHouse Add(MemberGuestHouse memberGuestHouse);

        MemberGuestHouse Update(MemberGuestHouse memberGuestHouse);

        MemberGuestHouse GetById(int id);

        IList<MemberGuestHouse> GetAllByMemberId(int id);

        IList<MemberGuestHouse> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}