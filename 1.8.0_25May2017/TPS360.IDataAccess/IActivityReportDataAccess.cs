﻿using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;
using System.Data;

namespace TPS360.DataAccess
{
    #region IActivityReportDataAccess

    public interface IActivityReportDataAccess
    {
        PagedResponse<ActivityReport> GetPaged(PagedRequest request);
       
    }

    #endregion
}
