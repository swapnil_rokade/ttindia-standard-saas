﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IPositionFunctionalCapabilityMapDataAccess

    public interface IPositionFunctionalCapabilityMapDataAccess
    {
        PositionFunctionalCapabilityMap Add(PositionFunctionalCapabilityMap positionFunctionalCapabilityMap);

        PositionFunctionalCapabilityMap Update(PositionFunctionalCapabilityMap positionFunctionalCapabilityMap);

        PositionFunctionalCapabilityMap GetById(int id);

        IList<PositionFunctionalCapabilityMap> GetAll();

        IList<PositionFunctionalCapabilityMap> GetAllByPositionId(int positionId);

        bool DeleteById(int id);

        bool DeleteByPositionId(int positionId);

        PositionFunctionalCapabilityMap GetByPositionIdAndFunctionalCapabilityId(int positionId, int functionalCapabilityId);
    }

    #endregion
}