﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    public interface ILocalizationResourceDataAccess
    {
        LocalizationResource Add(LocalizationResource localizationResource);

        LocalizationResource Update(LocalizationResource localizationResource);

        LocalizationResource GetById(int id);

        IList<LocalizationResource> GetAll();

        PagedResponse<LocalizationResource> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }
}