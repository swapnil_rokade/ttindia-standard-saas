﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: ICompanyGroupContactDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    --------------------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author                  MOdification
    ---------------------------------------------------------------------------------------------------------------------------------------------
    0.1              April-28-2009           Nagarathna V.B          Defect ID: 10258; created new method "GetCompanyGroupContactCount" 
   ---------------------------------------------------------------------------------------------------------------------------------------------
*/
using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ICompanyGroupContactDataAccess

    public interface ICompanyGroupContactDataAccess
    {
        CompanyGroupContact Add(CompanyGroupContact companyGroupContact);

        int GetCompanyGroupContactCount(int CompanyGroupId, int CompanyContactId);//0.1

        CompanyGroupContact GetById(int id);

        IList<CompanyGroupContact> GetAll();

        IList<CompanyGroupContact> GetAllByCompanyGroupId(int companyGroupId);

        PagedResponse<CompanyGroupContact> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}