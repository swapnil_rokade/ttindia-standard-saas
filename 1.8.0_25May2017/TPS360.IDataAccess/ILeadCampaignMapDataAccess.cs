﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ILeadCampaignMapDataAccess

    public interface ILeadCampaignMapDataAccess
    {
        LeadCampaignMap Add(LeadCampaignMap leadCampaignMap);

        LeadCampaignMap Update(LeadCampaignMap leadCampaignMap);

        LeadCampaignMap GetById(int id);

        IList<LeadCampaignMap> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}

