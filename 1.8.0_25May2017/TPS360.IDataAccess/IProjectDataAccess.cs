﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;

namespace TPS360.DataAccess
{
    #region IProjectDataAccess

    public interface IProjectDataAccess
    {
        Project Add(Project project);

        Project Update(Project project);

        Project GetById(int id);

        IList<Project> GetAll();

        PagedResponse<Project> GetPaged(PagedRequest request);

        bool DeleteById(int id);

        ArrayList GetAllProjects();
    }

    #endregion
}