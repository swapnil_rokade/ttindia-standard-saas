﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IHROfferLetterBonusDataAccess

    public interface IHROfferLetterBonusDataAccess
    {
        HROfferLetterBonus Add(HROfferLetterBonus hROfferLetterBonus);

        HROfferLetterBonus Update(HROfferLetterBonus hROfferLetterBonus);

        HROfferLetterBonus GetById(int id);

        IList<HROfferLetterBonus> GetAll();

        IList<HROfferLetterBonus> GetAllByHROfferLetterId(int hROfferLetterId);

        bool DeleteByHROfferLetterId(int hROfferLetterId);

        bool DeleteById(int id);
    }

    #endregion
}