﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IWorkOrderExpenseDataAccess

    public interface IWorkOrderExpenseDataAccess
    {
        WorkOrderExpense Add(WorkOrderExpense workOrderExpense);

        WorkOrderExpense Update(WorkOrderExpense workOrderExpense);

        WorkOrderExpense GetById(int id);

        IList<WorkOrderExpense> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}