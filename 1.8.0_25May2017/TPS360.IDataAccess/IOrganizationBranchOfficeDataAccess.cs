﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IOrganizationBranchOfficeDataAccess

    public interface IOrganizationBranchOfficeDataAccess
    {
        OrganizationBranchOffice Add(OrganizationBranchOffice organizationBranchOffice);

        OrganizationBranchOffice Update(OrganizationBranchOffice organizationBranchOffice);

        OrganizationBranchOffice GetById(int id);

        IList<OrganizationBranchOffice> GetAllByOrganizationId(int organizationId);

        IList<OrganizationBranchOffice> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}