using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IAutomatedEmailRemovalDataAccess

    public interface IAutomatedEmailRemovalDataAccess
    {
        AutomatedEmailRemoval Add(AutomatedEmailRemoval automatedEmailRemoval);

        AutomatedEmailRemoval Update(AutomatedEmailRemoval automatedEmailRemoval);

        AutomatedEmailRemoval GetById(int id);

        IList<AutomatedEmailRemoval> GetAll();

        PagedResponse<AutomatedEmailRemoval> GetPaged(PagedRequest request);
        
        bool DeleteById(int id);
    }

    #endregion
}
