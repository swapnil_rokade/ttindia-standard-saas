﻿
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberPositionDataAccess

    public interface IMemberPositionDataAccess
    {
        IList<MemberPosition> GetAllByMemberId(int memberId);
    }

    #endregion
}