﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IReferenceCheckQueryDataAccess

    public interface IReferenceCheckQueryDataAccess
    {
        ReferenceCheckQuery Add(ReferenceCheckQuery referenceCheckQuery);

        ReferenceCheckQuery Update(ReferenceCheckQuery referenceCheckQuery);

        ReferenceCheckQuery GetById(int id);

        IList<ReferenceCheckQuery> GetAllByQueryCategoryLookupId(int queryCategoryLookupId);

        IList<ReferenceCheckQuery> GetAll();

        PagedResponse<ReferenceCheckQuery> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}