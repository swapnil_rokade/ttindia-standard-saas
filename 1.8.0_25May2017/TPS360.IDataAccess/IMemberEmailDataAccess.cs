﻿using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;

namespace TPS360.DataAccess
{
    #region IMemberEmailDataAccess

    public interface IMemberEmailDataAccess
    {
        bool  ValidateSenderEmailID(string PrimaryEmail);
        
        IList<MemberEmail> GetByEmailAddressAndCreatedDate(string emailAddress, string createdDate);

        MemberEmail Add(MemberEmail memberEmail);

        MemberEmail Update(MemberEmail memberEmail);

        MemberEmail GetById(int id);

        MemberEmail GetMemberEmailSenderByMemberIdSubjectDate(Int32 memberId, string subject, DateTime date);

        MemberEmail GetMemberEmailReceiverByMemberIdSubjectDate(Int32 memberId, string subject, DateTime date);

        IList<MemberEmail> Synchronise(string fromdate, string todate, string subject, string toemail, string uid);

        IList<MemberEmail> GetAll();

        IList<MemberEmail> GetByEmailAddress(string emailAddress);

        PagedResponse<MemberEmail> GetPaged(PagedRequest request);

        bool DeleteById(int id);

        string GetMemberEmailTypeById(int Id);

    }

    #endregion
}