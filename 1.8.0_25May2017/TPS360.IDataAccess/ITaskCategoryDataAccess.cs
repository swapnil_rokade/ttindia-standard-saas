﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ITaskCategoryDataAccess

    public interface ITaskCategoryDataAccess
    {
        TaskCategory Add(TaskCategory taskCategory);

        TaskCategory Update(TaskCategory taskCategory);

        TaskCategory GetById(int id);

        IList<TaskCategory> GetAll();

        PagedResponse<TaskCategory> GetPaged(PagedRequest request);

        bool DeleteById(int id);

        TaskCategory GetByName(string name);
    }

    #endregion
}