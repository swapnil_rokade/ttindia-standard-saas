﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IOrganizationFunctionalityCategoryMapDataAccess

    public interface IOrganizationFunctionalityCategoryMapDataAccess
    {
        OrganizationFunctionalityCategoryMap Add(OrganizationFunctionalityCategoryMap organizationFunctionalityCategoryMap);

        OrganizationFunctionalityCategoryMap Update(OrganizationFunctionalityCategoryMap organizationFunctionalityCategoryMap);

        OrganizationFunctionalityCategoryMap GetById(int id);

        OrganizationFunctionalityCategoryMap GetByOrganizationBranchOfficeIdAndFunctionalityCategoryLookupId(int organizationBranchOfficeId, int functionalityCategoryLookupId);

        IList<OrganizationFunctionalityCategoryMap> GetAllByOrganizationBranchOfficeId(int organizationBranchOfficeId);
        
        IList<OrganizationFunctionalityCategoryMap> GetAll();

        bool DeleteByOrganizationBranchOfficeId(int organizationBranchOfficeId);

        bool DeleteById(int id);
    }

    #endregion
}