﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberGoalDataAccess

    public interface IMemberGoalDataAccess
    {
        MemberGoal Add(MemberGoal memberGoal);

        MemberGoal Update(MemberGoal memberGoal);

        MemberGoal GetById(int id);

        IList<MemberGoal> GetAll();

        IList<MemberGoal> GetAllByMemberId(int memberId);

        bool DeleteById(int id);
    }

    #endregion
}