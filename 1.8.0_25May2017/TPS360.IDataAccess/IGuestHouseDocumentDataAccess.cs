﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IGuestHouseDocumentDataAccess

    public interface IGuestHouseDocumentDataAccess
    {
        GuestHouseDocument Add(GuestHouseDocument guestHouseDocument);

        GuestHouseDocument Update(GuestHouseDocument guestHouseDocument);

        GuestHouseDocument GetById(int id);

        IList<GuestHouseDocument> GetAll();

        IList<GuestHouseDocument> GetAllByGuestHouseId(int guestHouseId);

        bool DeleteById(int id);

        bool DeleteByGuestHouseId(int guestHouseId);
    }

    #endregion
}