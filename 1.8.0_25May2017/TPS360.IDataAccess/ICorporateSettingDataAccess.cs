﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICorporateSettingDataAccess

    public interface ICorporateSettingDataAccess
    {
        CorporateSetting Add(CorporateSetting corporateSetting);

        CorporateSetting Update(CorporateSetting corporateSetting);

        CorporateSetting GetById(int id);

        IList<CorporateSetting> GetAll();

        bool DeleteById(int id);
    }

    #endregion
}