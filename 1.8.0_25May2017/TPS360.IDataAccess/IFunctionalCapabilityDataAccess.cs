﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IFunctionalCapabilityDataAccess

    public interface IFunctionalCapabilityDataAccess
    {
        FunctionalCapability Add(FunctionalCapability functionalCapability);

        FunctionalCapability Update(FunctionalCapability functionalCapability);

        FunctionalCapability GetById(int id);

        IList<FunctionalCapability> GetAll();

        IList<FunctionalCapability> GetAllByFunctionalCategoryId(int functionalCategoryId);

        PagedResponse<FunctionalCapability> GetPaged(PagedRequest request);

        bool DeleteById(int id);
        
    }

    #endregion
}