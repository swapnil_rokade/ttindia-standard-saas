﻿using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IRejectCandidateDataAccess
    public interface IEventLogDataAccess
    {
        void Add(EventLogForRequisitionAndCandidate Log,string CandidateId);

        PagedResponse<EventLogForRequisitionAndCandidate> GetPaged(PagedRequest request);

        IList<EventLogForRequisitionAndCandidate> GetAll(int JobPostingId, int CandidateId, int CreatorId);

        PagedResponse<EventLogForRequisitionAndCandidate> GetPagedForReport(PagedRequest request);
        ////////////////////////////Sumit Sonawane 1/Mar/2017///////////////////////////////////////////////////////////////////
        PagedResponse<EventLogForRequisitionAndCandidate> getPagedHiringMatrixEventLogDetails(int MemberID, PagedRequest request);
        
    }
    #endregion
}
