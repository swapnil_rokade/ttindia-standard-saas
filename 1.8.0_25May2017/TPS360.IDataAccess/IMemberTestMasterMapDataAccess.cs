﻿using System.Collections.Generic;
using TPS360.Common.Shared;
using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberTestMasterMapDataAccess

    public interface IMemberTestMasterMapDataAccess
    {
        MemberTestMasterMap Add(MemberTestMasterMap memberTestMasterMap);

        MemberTestMasterMap Update(MemberTestMasterMap memberTestMasterMap);

        MemberTestMasterMap GetById(int id);

        IList<MemberTestMasterMap> GetAllByMemberId(int memberId);

        IList<MemberTestMasterMap> GetAllByTestMasterId(int testMasterId);

        IList<MemberTestMasterMap> GetAll();

        bool DeleteById(int id);

        MemberTestMasterMap GetByMemberIdAndTestmasterId(int memberId, int testMasterId);

        PagedResponse<MemberTestMasterMap> GetPaged(PagedRequest pageRequest);
    }

    #endregion
}