﻿using System;
using System.Collections.Generic;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;


namespace TPS360.DataAccess
{
    #region IAttorneyDataAccess

    public interface IAttorneyDataAccess
    {
        Attorney Add(Attorney attorney);

        Attorney Update(Attorney attorney);

        Attorney GetById(int id);

        IList<Attorney> GetAll();

        PagedResponse<Attorney> GetPaged(PagedRequest request);

        ArrayList GetAttorneyList();


        bool DeleteById(int id);
    }

    #endregion
}