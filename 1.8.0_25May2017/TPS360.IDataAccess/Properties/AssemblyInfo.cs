using System;
using System.Reflection;

[assembly: AssemblyTitle("TPS360.IDataAccess")]
[assembly: AssemblyCompany("Talent Plus Software")]
[assembly: AssemblyProduct("TPS360.IDataAccess")]
[assembly: AssemblyCopyright("Copyright � 2008 Talent Plus Software, LLC")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: CLSCompliant(false)]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
