﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IMemberOnBoardingOffBoardingChecklistDataAccess

    public interface IMemberOnBoardingOffBoardingChecklistDataAccess
    {
        MemberOnBoardingOffBoardingChecklist Add(MemberOnBoardingOffBoardingChecklist memberOnBoardingOffBoardingChecklist);

        MemberOnBoardingOffBoardingChecklist Update(MemberOnBoardingOffBoardingChecklist memberOnBoardingOffBoardingChecklist);

        MemberOnBoardingOffBoardingChecklist GetById(int id);

        IList<MemberOnBoardingOffBoardingChecklist> GetAll();

        IList<MemberOnBoardingOffBoardingChecklist> GetAllByMemberId(int memberId);

        PagedResponse<MemberOnBoardingOffBoardingChecklist> GetPaged(PagedRequest request);
        
        bool DeleteById(int id);
    }

    #endregion
}