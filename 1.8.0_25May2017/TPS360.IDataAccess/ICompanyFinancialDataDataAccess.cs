﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ICompanyFinancialDataDataAccess

    public interface ICompanyFinancialDataDataAccess
    {
        CompanyFinancialData Add(CompanyFinancialData companyFinancialData);

        CompanyFinancialData Update(CompanyFinancialData companyFinancialData);

        CompanyFinancialData GetById(int id);

        IList<CompanyFinancialData> GetAll();

        IList<CompanyFinancialData> GetAllByComapnyId(int companyId);

        bool DeleteById(int id);

        bool DeleteByCompanyId(int companyId);
    }

    #endregion
}