﻿/*
-------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IEmployeeReferralDataAccess.cs
    Description: 
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author              Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1             14-November-2012        Charles    
 *  0.2             26/May/2016             Sumit Soanwane
    -------------------------------------------------------------------------------------------------------------------------------------------       
*/


using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System;
using System.Data;
using TPS360.Common.BusinessEntity;
using System.Collections.Generic;
using System.Collections;
namespace TPS360.DataAccess
{
    #region IEmployeeReferralDataAccess

    public interface IEmployeeReferralDataAccess
    {
        int Add(EmployeeReferral obj);     

        PagedResponse<EmployeeReferral> GetPaged(PagedRequest request);

        PagedResponse<ListWithCount> GetPagedReferralSummary(PagedRequest request, string type);

        ArrayList GetAllReferrerList();

        IList<ListWithCount> GetAllListGroupByDate(int JobPostingId, int ReferrerID, int StartDate, int EndDate);
        //////////////////////code added by Sumit on 26/May/2016////////////////////////////////////////////////////////////////////////////
        IList<ListWithCount> GetAllListGroupByDate(string JobPostingId, int ReferrerID, int StartDate, int EndDate);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       
        EmployeeReferral GetByMemberId(int MemberID);
    }

    #endregion
}