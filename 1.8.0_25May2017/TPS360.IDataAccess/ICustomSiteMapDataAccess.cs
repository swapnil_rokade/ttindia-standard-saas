﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region ICustomSiteMapDataAccess

    public interface ICustomSiteMapDataAccess
    {
        CustomSiteMap Add(CustomSiteMap customSiteMap);

        CustomSiteMap Update(CustomSiteMap customSiteMap);

        CustomSiteMap GetById(int id);

        CustomSiteMap GetByName(string name);

        CustomSiteMap GetRoot();

        IList<CustomSiteMap> GetAll();

        IList<CustomSiteMap> GetAllParent();

        IList<CustomSiteMap> GetAllByParent(int parentId);

        PagedResponse<CustomSiteMap> GetPaged(PagedRequest request);


        CustomSiteMap GetAllByParentIDAndMemberPrivilege(int ParentId, int MemberID);

       

        bool DeleteById(int id);

        IList<MenuListCount> getMenuListCount(int ParentId, int Id);
    }

    #endregion
}