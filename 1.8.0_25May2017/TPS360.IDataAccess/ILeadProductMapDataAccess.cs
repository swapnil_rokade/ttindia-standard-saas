﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region ILeadProductMapDataAccess

    public interface ILeadProductMapDataAccess
    {
        LeadProductMap Add(LeadProductMap leadProductMap);

        LeadProductMap Update(LeadProductMap leadProductMap);

        LeadProductMap GetById(int id);

        IList<LeadProductMap> GetAllByProductId(int productId);

        IList<LeadProductMap> GetAllByLeadId(int leadId);

        IList<LeadProductMap> GetAll();

        bool DeleteById(int id);

        bool DeleteByLeadId(int leadId);
    }

    #endregion
}

