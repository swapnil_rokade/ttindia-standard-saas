﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IHROfferLetterLeaveDataAccess

    public interface IHROfferLetterLeaveDataAccess
    {
        HROfferLetterLeave Add(HROfferLetterLeave hROfferLetterLeave);

        HROfferLetterLeave Update(HROfferLetterLeave hROfferLetterLeave);

        HROfferLetterLeave GetById(int id);

        IList<HROfferLetterLeave> GetAll();

        IList<HROfferLetterLeave> GetAllByHROfferLetterId(int hROfferLetterId);

        bool DeleteByHROfferLetterId(int hROfferLetterId);

        bool DeleteById(int id);
    }

    #endregion
}