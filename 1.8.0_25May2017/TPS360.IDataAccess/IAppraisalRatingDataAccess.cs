﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;

namespace TPS360.DataAccess
{
    #region IAppraisalRatingDataAccess

    public interface IAppraisalRatingDataAccess
    {
        AppraisalRating Add(AppraisalRating appraisalRating);

        AppraisalRating Update(AppraisalRating appraisalRating);

        AppraisalRating GetById(int id);

        IList<AppraisalRating> GetAll();

        PagedResponse<AppraisalRating> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}