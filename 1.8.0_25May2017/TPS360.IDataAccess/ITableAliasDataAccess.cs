﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;

namespace TPS360.DataAccess
{
    #region ITableAliasDataAccess

    public interface ITableAliasDataAccess
    {
        TableAlias Add(TableAlias tableAlias);

        TableAlias Update(TableAlias tableAlias);

        TableAlias GetById(int id);

        TableAlias GetByName(string name);

        TableAlias GetByParentIdAndName(int parentId, string name);

        IList<TableAlias> GetAllParent();

        IList<TableAlias> GetAllByParentId(int parentId);

        IList<TableAlias> GetAll();

        PagedResponse<TableAlias> GetPaged(PagedRequest request);

        bool DeleteById(int id);

        bool DeleteByNames(string names);

        bool DeleteByParentIdAndNames(int parentId, string names);

        ArrayList GetAllTable();

        ArrayList GetAllColumnByTableName(string tableName);
    }

    #endregion
}