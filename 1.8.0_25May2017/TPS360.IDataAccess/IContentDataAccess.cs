﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;

namespace TPS360.DataAccess
{
    #region IContentDataAccess

    public interface IContentDataAccess
    {
        Content Add(Content content);

        Content Update(Content content);

        Content GetById(int id);

        IList<Content> GetAll();

        IList<Content> GetAllByCategory(ContentCategory category);

        ArrayList GetAllByCreatorId(int creatorId);

        ArrayList GetAllMaster(int creatorId);

        PagedResponse<Content> GetPaged(PagedRequest request);

        bool DeleteById(int id);
    }

    #endregion
}