﻿using System;
using System.Collections.Generic;

using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;
namespace TPS360.DataAccess
{
    #region IMemberHiringDetailsDataAccess

    public interface IMemberHiringDetailsDataAccess
    {
        void  Add(MemberHiringDetails memberHiringDetails,string MemberId);

        MemberHiringDetails Update(MemberHiringDetails memberHiringDetails);

        MemberHiringDetails GetById(int id);

        MemberHiringDetails GetByMemberIdAndJobPosstingID(int memberId,int JobPostingId);

        bool DeleteHiringDetialsById(int HiringDetailsID);

        PagedResponse<MemberHiringDetails> GetPaged(PagedRequest PageRequest);

        
        
        PagedResponse<MemberOfferJoinDetails > GetPagedOfferJoined(PagedRequest request);

        PagedResponse<MemberHiringDetails> GetPagedEmbeddedSalarydetails(PagedRequest request);

        PagedResponse<MemberHiringDetails>  GetPagedMechSalarydetails(PagedRequest request);

        PagedResponse<MemberOfferJoinDetails> GetPagedOfferJoinedCommon(PagedRequest request);

        MemberHiringDetails GetSalaryComponentsByJobpostingIdAndMemberId(int jobpostingId, int MemberId);

        MemberHiringDetails GetOfferDetailsByJobpostingIdAndMemberId(int jobpostingId, int MemberId);

        IList<MemberHiringDetails> GetAllManagementBand();

        IList<MemberHiringDetails> GetAllLocation();

        IList<MemberHiringDetails> GetAllManagementBandByManagementBandId(int ManagementBandId);

        IList<MemberHiringDetails> GetAllBand();
    }
    



    #endregion
}