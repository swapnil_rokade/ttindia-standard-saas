﻿using System.Collections.Generic;

using TPS360.Common.BusinessEntities;

namespace TPS360.DataAccess
{
    #region IMemberGroupManagerDataAccess

    public interface IMemberGroupManagerDataAccess
    {
        MemberGroupManager Add(MemberGroupManager memberGroupManager);

        MemberGroupManager GetById(int id);

        IList<MemberGroupManager> GetAllByMemberId(int memberId);

        IList<MemberGroupManager> GetAll();
        
        int GetTotalMemberByMemberGroupId(int memberGroupId);

        bool DeleteById(int id);

        bool DeleteByMemberGroupIdAndMemberId(int memberGroupId, int memberId);
    }

    #endregion
}