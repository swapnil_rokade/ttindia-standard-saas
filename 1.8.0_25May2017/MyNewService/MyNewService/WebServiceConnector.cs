﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services;
using System.Collections;
using System.Configuration;
using System.Windows.Forms;


namespace MyNewService
{
    class WebServiceConnector
    {
        public bool CallWebService()
        {
            try
            {
                using (MailSendingWebService datainsert = new MailSendingWebService())
                {
                    datainsert.LinkUrl = GetWebserviceUrl("ServiceSoapMailSending");
                    datainsert.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    return datainsert.MailToSearchAgent();
                }
            }
            catch (Exception ex)
            {
               // ErrorLogger.Log(ex);
                return false ;
            }
        }
        public string GetWebserviceUrl(string key)
        {
            string WebService = "";
            try
            {
                WebService = System.Configuration.ConfigurationSettings.AppSettings["Service"].ToString();
            }
            catch
            {
            }
            if (WebService != "")
            {
                WebService += System.Configuration.ConfigurationSettings.AppSettings[key].ToString();
                return WebService;
            }
            else
            {
                string strAddress = "";
                Configuration c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                ConfigurationSectionGroup csg = c.GetSectionGroup("system.serviceModel");
                if (csg != null)
                {
                    ConfigurationSection css = csg.Sections["client"];
                    if (css != null)
                    {
                        System.ServiceModel.Configuration.ClientSection cs = (System.ServiceModel.Configuration.ClientSection)csg.Sections["client"];

                        System.ServiceModel.Configuration.ChannelEndpointElementCollection epCol = cs.Endpoints;

                        foreach (System.ServiceModel.Configuration.ChannelEndpointElement endpt in epCol)
                        {

                            if (endpt.Name == key)
                            {
                                strAddress = endpt.Address.ToString();
                                break;
                            }
                        }
                    }
                }
                return strAddress;
            }

        }
    }
}
