using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Net.Mail;
using System.Timers;
using System.Collections.Generic ;
using System.Text;

namespace MyNewService
{
	public class Service1 : System.ServiceProcess.ServiceBase
	{
		private System.Diagnostics.EventLog eventLog1;
        System.Timers.Timer time = new System.Timers.Timer();
        private IContainer components;
        
		public Service1()
		{
			InitializeComponent();
			if(!System.Diagnostics.EventLog.SourceExists("SearchAgents"))
			{
                System.Diagnostics.EventLog.CreateEventSource("SearchAgents", "EmailSent");
			}

            eventLog1.Source = "SearchAgents";
            eventLog1.Log = "EmailSent";
			
		}

		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new MyNewService.Service1()};

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);	
		}

		private void InitializeComponent()
		{
            this.eventLog1 = new System.Diagnostics.EventLog();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).BeginInit();
            // 
            // eventLog1
            // 
            this.eventLog1.Log = "EmailSent";
            this.eventLog1.Source = "SearchAgents";
            // 
            // Service1
            // 
            this.ServiceName = "TalentrackrEmailAgent";
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).EndInit();

		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnStart(string[] args)
		{
            time.Interval = (60000*5);
            time.Elapsed += new ElapsedEventHandler(this.time_elapsed);
            time.Enabled = true;
            time.Start();
			eventLog1.WriteEntry("Service Started");
		}
		
		protected override void OnStop()
		{
            time.Stop();
            time.Enabled = false;
		}

		protected override void OnContinue()
		{
            time.Start();
			//eventLog1.WriteEntry("my service is continuing in working");
		}

        public void time_elapsed(object sender, ElapsedEventArgs e)
        {
            //eventLog1.WriteEntry("started"); 
            time.Stop();
            WebServiceConnector connect = new WebServiceConnector();
            bool IsSent = connect.CallWebService();
            if (IsSent == true)
            {
                eventLog1.WriteEntry("Mail Send Successfully");
            }
            time.Start();
        }
           
        
	}
}
