﻿using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;

namespace MyNewService
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "ServiceSoap", Namespace = "http://tempuri.org/")]
    public partial class MailSendingWebService:System .Web .Services .Protocols .SoapHttpClientProtocol 
    {

        private System.Threading.SendOrPostCallback GetSearchAgentOperationCompleted;

        public MailSendingWebService()
        {
        }
        public string LinkUrl
        {
            set { this.Url = value; }
        }
        public event GetSearchAgentCompletedEventHandler GetSearchAgentCompleted;

        #region MailToSearchAgent
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/MailToSearchAgent", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool MailToSearchAgent()
        {
            object[] results = this.Invoke("MailToSearchAgent",new object[]{} );
            return ((bool)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginMailToSearchAgent(System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("MailToSearchAgent",new object []{}, callback, asyncState);
        }

        /// <remarks/>
        public bool EndMailToSearchAgent(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((bool)(results[0]));
        }

        /// <remarks/>
        public void MailToSearchAgentAsync()
        {
            this.MailToSearchAgentAsync(null);
        }

        /// <remarks/>
        public void MailToSearchAgentAsync(object userState)
        {
            if ((this.GetSearchAgentOperationCompleted == null))
            {
                this.GetSearchAgentOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSearchAgentOperationCompleted);
            }
            this.InvokeAsync("MailToSearchAgent",new object []{}, this.GetSearchAgentOperationCompleted, userState);
        }

        private void OnSearchAgentOperationCompleted(object arg)
        {
            if ((this.GetSearchAgentCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetSearchAgentCompleted(this, new GetSearchAgentCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        #endregion

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    public partial class SearchAsAgent
    {
        
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    public delegate void GetSearchAgentCompletedEventHandler(object sender, GetSearchAgentCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetSearchAgentCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal GetSearchAgentCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public string Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }

}
