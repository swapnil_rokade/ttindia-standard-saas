﻿/*
 -------------------------------------------------------------------------------------------------------------------------------------------
    FileName: IFacade.cs
    Description: This is the class library page.
    Created By: 
    Created On:
    Modification Log:
    ------------------------------------------------------------------------------------------------------------------------------
    Ver.No.             Date                Author             Modification
    ------------------------------------------------------------------------------------------------------------------------------
    0.1              Nov-10-2008           Jagadish            Defect ID: 9184; (Kaizen) Added a method declaration called 'DeleteJobPostingSkillSetByJobPostingId'
                                                               to delete jobposting skills having same JobPostingId.
    0.2              Dec-02-2008           Shivanand           Defect #8745; New parameter "strState" is added in the method signature of "GetAllCandidateOnSearch()".
                                                                             New parameter "strState" is added in the method signature of "GetCandidateSearchQuery()".
                                                                             New parameter "strState" is added in the method signature of "GetAllConsultantOnSearch()".
    0.3              Dec-10-2008           Yogeesh Bhat        Defect# 9398: Added new method signature GetRequisitionCountByMemberId, UpdateMemberRequisitionCount
    0.4              Jan-09-2008           Jagadish            Defect ID: 9073; Added an overload for method 'GetAllCommonNoteByMemberId'.
    0.5              Jan-22-2008           Jagadish            Defect ID: 8822; Implemented sorting.
    0.6              Feb-2-2009            Gopala Swamy        Defect ID: 9061 Changed Prototypes to function called "GetAllCommonNoteByProjectId()"  
    0.7              Feb-02-2008           Sandeesh            Defect ID: 9376; Implemented sorting.
    0.8              Feb-04-2008           Gopala Swamy        Defect Id: 9090;Added  overloaded function "GetAllMemberSkillByMemberId(int,string)";
    0.9              Feb-11-2009           Jagadish            Defect ID: 8871; Added an overload for method 'GetAllMemberTimeSheetDetailByMemberTimeSheetId'.
 *  0.10             Feb-20-2009           Rajendra            Defect ID: 9811;Added an parameter "string jobIndustry" for overload method 'GetPagedVolumeHireJobPosting'.
 *  0.11             Feb-26-2009           Sandeesh            Defect id :9671 Added new method to implement                                                                   sorting
 *  0.12             04-Feb-2009           Sandeesh            Defect Fix - ID: 10027; Added new method to get the H1B1 details of the employee.
 *  0.13             10-Mar-2009           Nagarathna          Defect Id: 10068; added an overload for method "GetAllJobPostingByProjectId".
 *  0.14             18-Mar-2009           Rajendra            Defect ID: 10165; Added an overload update method "UpdateMemberH1B1Document";
 *  0.15             4-April-2009          Nagarathna V.B      Defect Id:9545  Added new method to get count of SSN "GetMemberSSNCount()"
    0.16             28-April-2009         Nagarathna V.B      Defect Id:10258  Added new menthod to get count of COmpanyGroupcontact "GetCompanyGroupContact"
 *  0.17            28-May-2009            Veda                 DefectId:10493 Added new method "GetLicenseKeybyId"
 *  0.18            June-8-2009            Nagarathna V.B       Enhancement:10525  adding "GetProductivityOverviewByDate";
    0.19            July-7-2009           Gopala Swamy J         Misc:Added 1) GetStateIdByStateCode() 2) GetCountryIdByCountryCode() to assist 
                                                                Resume parser team
 * 0.20             Sept-10-2009          Sukanta Ghorui        Added new method GetDuplicateRecords for searching possible Duplicate records
 * 0.21             Sept-18-2009          Gopala Swamy J        Added new method;
 * 0.22              Nov-19-1009           Sandeesh             Enhancement Id:10998 -Added a new method 'GetMemberUserNameById' for getting the username of the user
   0.23              Nov-25-2009           Gopala Swamy J       Defect Id:11588 :Added one parameter
   0.24              Dec-17-2009           Rajendra A.S         Defect Id:11983 :Added new Method MemberSubmission();
 * 0.25              Feb-22-2010           Nagarathna V.B       Enhancement :12129;Submission Report,GetPrimaryContact()
 * 0.26              22/May/2015           Prasanth Kumar G     Introduced GetPagedRequisitionAgingReport
 * 0.27              20/Jul/2015           Prasanth Kumar G     Introduced InterviewerFeedback
 * 0.28              16/Oct/2015           Prasanth Kumar G     Introduced IntervieweQuestionBank,QuestionBank
 * 0.29              05/Dec/2015           Prasanth Kumar G     Introduced Interview Assessment
 * 0.29              20/nov/2015           pravin khot          Inroduced by interview panel.
 * 0.30              27/nov/2015           pravin khot          Introduced by InterviewPanelMaster.
 * 0.31              02/Dec/2015           pravin khot          Introduced by GetAllMemberNameWithEmailByRoleName_IP,GetAllCompanyContactByComapnyId_IP
 * 0.32              09/Dec/2015           pravin khot          Introduced by GetAllSuggestedInterviewerWithEmail.  
 * 0.33              10/Dec/2015           pravin khot          Introduced by SuggestedInterviewer_Id.
 * 0.34              11/Dec/2015          pravin khot          Introduced by AddSuggestedInterviewInterviewerMap.
 * 0.35              15/Dec/2015          pravin khot          Introduced by MemberInterview_SuggestedInterviewerId.
*  0.36              24/Dec/2015           Prasanth Kumar G     Introduced Member Interview feedback document
 * 0.37              27/Dec/2015           Prasanth Kumar G     Introduced function InterviewFeedback_GetByInterviewId_InterviewerEmail
 * 0.38              27/01/2016            suresh.s             Introduced by GetPagedMemberJobCartForEmployeeReferal method
 * 0.39               8/Jan/2016            Pravin khot          Introduced by MemberCareerAllOpportunities.
 *0.40               12/Jan/2016           Pravin khot          Introduced by GetPagedCareerPortalSubmissionsList.
 *0.41               18/Jan/2016           pravin khot          Introduced by CareerJobId_Add.
 *0.42                25/Jan/2016          pravin khot          Introduced by MemberCareerPortalToRequisition_Add.
 *0.43                29/Jan/2016          pravin khot          Introduced by GetAllJobPostingByBUId.
 *0.44                2/Feb/2016           Pravin khot          Introduced by GetPagedRequisitionSourceBreakupReport.
 *0.45                23/Feb/2016          pravin khot          Introduced by DeleteRoleRequisitionDenial ,UpdateRoleRequisitionDenial,GetCustomRoleRequisition
 *0.46                23/Feb/2016          pravin khot          Introduced by  #region UserRoleMapEditor
 *0.47                26/Feb/2016          pravin khot          Introduced by GetCompanyContactByEmailAndCompanyId
 *0.48                3/March/2016         pravin khot          Introduced by GetAllCustomUser
 *0.49                4/March/2016         pravin khot          Introduced by GetPagedCandidateCDMDetailsReport
 *0.50                7/March/2016         pravin khot          Introduced by GetAllMemberNameWithEmailByRoleNameIfExisting
 *0.51                14/March/2016        pravin khot          Introduced by GetPagedCandidateReject,GetPagedRejectCandidateForSelectedColumn
 *0.52                27/April/2016        pravin khot          Introduced by UpdateMemberthroughvendor,CandidateAvailableInRequisition_Name ,MemberJobCart_AddCandidateToMultipleRequisition
 *0.53                4/May/2016           pravin khot          Introduced by CancelInterviewById,UpdateInterviewIcsCodeById,GetAllsuggestedInterviewersByInterviewId
 *0.54                13/May/2016          pravin khot          Introduced by HiringMatrixLevel_Id
 *0.55                16/May/2016          pravin khot          added- GetAllMemberNameByTeamMemberId , GetAllTeamByTeamLeaderId , 
 *0.56                24/May/2016          pravin khot          added-UpdateMemberManagerIsPrimary , GetRequisitionStatusBy_Id
 *0.57                25/May/2016          pravin khot          added-GetGenericLookupByDescription,GetRequisitionStatusBy_Id , GetRequisitionStatus_Id ,time zone,GetTimeZoneIdBy_MemberId
 *0.58                20/May/2016          Sumit Sonawane       EmployeeTeamBuilder_GetTeamsByMemberId
 *0.59                26/May/2016          Sumit Sonawane       EmployeeReferral_GetCountGroupbyDate
 *0.60                20/May/2016          Sumit Sonawane       GetPagedCandidate , GetPagedCandidateForSelectedColumn
 *0.61                7/June/2016          pravin khot          added new - GetEmployeeByEmailId 
 *0.62                8/June/2016          pravin khot          added new- GetAllClientsByStatusByCandidateId,GetAllJobPostingByCandidateId
* 0.63                10/Jun/2016          Prasanth Kumar G     Introduced Function GetMemberByMemberUserName
  0.64                10/Jun/2016          Prasanth Kumar G     Introduced Function GetMemberADUserNameById
 *0.65                24/June/2016         pravin khot          added -RejectMemberJobCartById 
 *0.66                8/Aug/2016           pravin khot          modify-GetAllCandidateOnSearchForPrecises
 *0.67                1/Mar/2017           Sumit Sonawane       modify - restrict to rescheduled/cancel Interview feedback.
 *0.68                21/Mar/2017          Sumit Sonawane       modify - Graphs
  0.69                23/March/2017        pravin khot          added- GetCompanyContactByJobPostingHiringTeam
  0.70                24Apr2017           Prasanth Kumar G        Introduced Errorlogdb issue id 1044
 * * * -------------------------------------------------------------------------------------------------------------------------------------------        
*/
using System;
using System.Collections.Generic;
using System.ServiceModel;
using TPS360.Common.BusinessEntities;
using TPS360.Common.Shared;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using TPS360.Common.BusinessEntity;
namespace TPS360.BusinessFacade
{
    [ServiceContract(Name = "IFacade")]
    public interface IFacade : IDisposable
    {
        #region Automation


        [OperationContract]
        bool CreateEmailAccount(string emailId, string smtp, string connectionString);

        [OperationContract]
        bool DeleteEmailAccount(int Id, string connectionString);

        [OperationContract]
        DataTable GetAllEmailAccount(string connectionString);

        [OperationContract]
        bool SetDefaultEmailAccount(int Id, string connectionString);

        [OperationContract]
        int GetDefaultEmailAccountId(string connectionString);

        #endregion

        #region ActionLog

        [OperationContract]
        ActionLog AddActionLog(ActionLog actionLog);

        [OperationContract]
        ActionLog UpdateActionLog(ActionLog actionLog);

        [OperationContract]
        ActionLog GetActionLogById(int id);

        [OperationContract]
        IList<ActionLog> GetAllActionLog();

        [OperationContract]
        bool DeleteActionLogById(int id);

        #endregion

        #region Activity

        [OperationContract]
        Activity AddActivity(Activity activity);

        [OperationContract]
        Activity UpdateActivity(Activity activity);

        [OperationContract]
        Activity AddActivityForInterview(Activity activity);

        [OperationContract]
        Activity UpdateActivityForInterview(Activity activity);

        [OperationContract]
        Activity GetActivityById(int id);

        //[OperationContract]
        //IList<Activity> GetAllActivity();

        [OperationContract]
        bool DeleteActivityById(int id);      

        [OperationContract]
        IList<Activity> GetAllActivityByResourceId(int resourceID);

        [OperationContract]
        IList<Activity> GetAllActivityByResourceIdAndDate(int resourceID, string Date);


        #endregion

        #region ActivityResource

        [OperationContract]
        ActivityResource AddActivityResource(ActivityResource activityResource);

        //[OperationContract]
        //ActivityResource UpdateActivityResource(ActivityResource activityResource);

        [OperationContract]
        ActivityResource GetActivityResourceByActivityID(int activityID);

        [OperationContract]
        IList<ActivityResource> GetAllActivityResource();

        [OperationContract]
        bool DeleteActivityResourceByActivityID(int activityID);

        #endregion

        #region Alert

        [OperationContract]
        Alert AddAlert(Alert alert);

        [OperationContract]
        Alert UpdateAlert(Alert alert);

        [OperationContract]
        Alert GetAlertById(int id);

        [OperationContract]
        IList<Alert> GetAllAlert();

        [OperationContract]
        PagedResponse<Alert> GetPagedAlert(PagedRequest request);

        [OperationContract]
        bool DeleteAlertById(int id);

        [OperationContract]
        Alert GetAlertByWorkflowTitle(WorkFlowTitle workFlowTitle);

        #endregion

        #region ApplicationWorkflow

        [OperationContract]
        ApplicationWorkflow AddApplicationWorkflow(ApplicationWorkflow applicationWorkflow);

        [OperationContract]
        ApplicationWorkflow UpdateApplicationWorkflow(ApplicationWorkflow applicationWorkflow);

        [OperationContract]
        ApplicationWorkflow GetApplicationWorkflowById(int id);

        [OperationContract]
        ApplicationWorkflow GetApplicationWorkflowByTitle(string title);

        [OperationContract]
        ApplicationWorkflow GetApplicationWorkflowByMemberId(int memberId);

        [OperationContract]
        IList<ApplicationWorkflow> GetAllApplicationWorkflow();

        [OperationContract]
        bool DeleteApplicationWorkflowById(int id);

        #endregion

        #region ApplicationWorkflowMap

        [OperationContract]
        ApplicationWorkflowMap AddApplicationWorkflowMap(ApplicationWorkflowMap applicationWorkflowMap);

        [OperationContract]
        ApplicationWorkflowMap UpdateApplicationWorkflowMap(ApplicationWorkflowMap applicationWorkflowMap);

        [OperationContract]
        ApplicationWorkflowMap GetApplicationWorkflowMapById(int id);

        [OperationContract]
        ApplicationWorkflowMap GetApplicationWorkflowMapByApplicationWorkflowIdAndMemberId(int applicationWorkflowId, int memberId);

        [OperationContract]
        IList<ApplicationWorkflowMap> GetAllApplicationWorkflowMapByApplicationWorkflowId(int applicationWorkflowId);

        [OperationContract]
        IList<ApplicationWorkflowMap> GetAllApplicationWorkflowMap();

        [OperationContract]
        bool DeleteApplicationWorkflowMapById(int id);

        [OperationContract]
        bool DeleteApplicationWorkflowMapByApplicationWorkflowId(int applicationWorkFlowId);

        #endregion

        #region ASPNetMembership

        [OperationContract]
        bool LockUser(Guid userId);

        #endregion

        #region AutoMailSending
        [OperationContract]
        IList<AutoMailSending> GetSearchDetails();
        #endregion

        #region Candidate

        [OperationContract]
        Candidate GetCandidateById(int id);

        [OperationContract]
        IList<Candidate> GetAllCandidateByMemberId(int memberId);

        [OperationContract]
        Candidate GetCandidateByIdForHR(int id);

        [OperationContract]
        PagedResponse<Candidate> GetPagedCandidate(PagedRequest request);

        [OperationContract]
        PagedResponse<Candidate> GetPagedVendorCandidatePerformance(PagedRequest request);

        [OperationContract]
        PagedResponse<Candidate> GetPagedCandidateForSubmittedCandidate(PagedRequest request);

        //************************Code added by pravin khot on 12/Jan/2016**********************************
        [OperationContract]
        PagedResponse<Candidate> GetPagedCareerPortalSubmissionsList(PagedRequest request);
        //*********************************End*****************************************

        [OperationContract]
        PagedResponse<Candidate> GetPagedCandidate(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager);


        [OperationContract]
        PagedResponse<DynamicDictionary> GetPagedCandidateForSelectedColumn(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManagerm, IList<string> CheckedList);


       

        [OperationContract]
        int GetCandidateCountByMemberId(int MemberID);

        [OperationContract]
        int getCandidateIdbyPrimaryEmail(string PrimaryEmail);

        [OperationContract]
        void getMemberUpdateCreatorId(int Id, int creatorId);
        [OperationContract]
        bool CheckCandidateDuplication(string firstName, string lastName, string mobileNo, DateTime dob, string email, int IdCardLookUpId, string IdCardDetail);
        string GetCandidateEmailById(int Id);

        string GetCandidateNameByEmailId(string EmailId);

       // ********Code added by pravin khot on 4/March/2016********
        [OperationContract]
        PagedResponse<Candidate> GetPagedCandidateCDMDetailsReport(PagedRequest request);
        //***********************END********************************

        // ********Code added by Sumit Sonawane on 20/May/2016********

        [OperationContract]
        PagedResponse<Candidate> GetPagedCandidate(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, bool IsTeamReport, string TeamMemberIds, int TeamID);

        [OperationContract]
        PagedResponse<DynamicDictionary> GetPagedCandidateForSelectedColumn(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManagerm, IList<string> CheckedList, bool IsTeamReport, string TeamMembersId, int TeamId);



        //***********************END********************************
        // ********Code added by Sumit Sonawane on 20/Jan/2017********
        [OperationContract]
        //int GetCandidateAgebodByMemberId(int MemberID);
        string GetCandidateAgebodByMemberId(int MemberID);
        int getAllCandiCountFronView(string jobCrtor, int jobid, int Step);
        #endregion

        #region CandidateSourcingReport

        [OperationContract]
        PagedResponse<CandidateSourcingInfo> GetPagedForCandidateSourcingReport(PagedRequest request);
        

        #endregion

        #region CandidateDashboard

        [OperationContract]
        CandidateDashboard GetCandidateDashboardByMemberId(int memberId);

        #endregion

        #region CandidateReportDashboard

        [OperationContract]
        CandidateReportDashboard GetCandidateReportDashboardByParameter(DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager);

        #endregion

        #region Candidate Search

         [OperationContract]
        PagedResponse<Candidate> GetAllCandidateOnSearch(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType); //0.2  // 9372

        [OperationContract]
         PagedResponse<Candidate> GetAllCandidateOnSearchForPrecise(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddWhereClause, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string CandidateID);

        //*************Added parameter bool ShowPreciseSearch by pravin khot on 8/Aug/2016
        [OperationContract]
        PagedResponse<Candidate> GetAllCandidateOnSearchForPrecises(PagedRequest request, string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string rowPerPage, string memberPositionId, string isVolumeHire, string isMyList, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddWhereClause, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string SourceId, string CandidateID, string NoticePeriod, bool ShowPreciseSearch);
        //******************************END************************************

        [OperationContract]
        Candidate GetOnSearchForPrecise_Sub(int CandidateId);

        [OperationContract]
        IList<Candidate> GetAllCandidateOnSearchForSearchAgent(string allKeyWords, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string hotListId, string minExpYr, string maxExpYr, string strState, int maxResults, string applicantType, string Education, string WorkSchedule,string HiringStatus, int JobPostingId);

        [OperationContract]
        PagedResponse<Candidate> GetAllCandidateOnSearchForSavedQuery(PagedRequest request, string WhereClause, string memberId, string rowPerPage);

        [OperationContract]
        string GetCandidateSearchQuery(string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string memberPositionId, string isMyList, string minExpYr, string maxExpYr, string strState, string applicantType, string internalRating); //0.2

        [OperationContract]
        string GetCandidateSearchQueryForPreciseSearch(string allKeyWords, string anyKeyWord, string currentPosition, string currentCity, string currentCountryId, string jobType, string workStatus, string securityClearence, string availability, string industryType, string lastUpdateDate, string salaryType, string salaryFrom, string salaryTo, string currencyType, string functionalCategory, string hiringMatrixId, string hotListId, string memberId, string memberPositionId, string isMyList, string minExpYr, string maxExpYr, string strState, string applicantType, string Education, string WorkSchedule, string Gender, string Nationality, string Industry, string JobFunction, string HiringStatus, string CurrentCompany, string CompanyStatus, string CandidateEmail, string MobilePhone, string ZipCode, string Radius, bool AddWhereClause, bool IsMile, string AssignedManagerId, string SalarySelector, string internalRating, string CandidateID);

        #endregion

        #region CandidateRequisitionStatus

        [OperationContract]
        void AddCandidateRequisitionStatus(int JobPostingId, string MemberId, int CreatorId);

        [OperationContract]
        void UpdateCandidateRequisitionStatus(int JobPostingId, string MemberId, int CreatorId, int StatusId);

        [OperationContract]
        CandidateRequisitionStatus GetByRequisitionIdandMemberId(int RequisitionId, int MemberId);

        [OperationContract]
        PagedResponse<CandidateRequisitionStatus> GetPagedCandidateREquisitionStatus(PagedRequest request, int ManagerId);
        #endregion

        #region Category

        [OperationContract]
        Category AddCategory(Category category);

        [OperationContract]
        Category UpdateCategory(Category category);

        [OperationContract]
        Category GetCategoryById(int id);

        [OperationContract]
        IList<Category> GetAllCategory();

        [OperationContract]
        bool DeleteCategoryById(int id);

        [OperationContract]
        Int32 GetCategoryIdByCategoryName(string category);

        #endregion

        #region CommonNote

        [OperationContract]
        CommonNote AddCommonNote(CommonNote commonNote);

        [OperationContract]
        CommonNote UpdateCommonNote(CommonNote commonNote);

        [OperationContract]
        CommonNote GetCommonNoteById(int id);

        [OperationContract]
        IList<CommonNote> GetAllCommonNoteByMemberId(int memberId);

        [OperationContract]
        IList<CommonNote> GetAllCommonNoteByMemberId(int memberId, string SortExpression); // 0.4

        [OperationContract]
        IList<CommonNote> GetAllCommonNoteByCompanyId(int companyId, string SortExpression); // 0.4

        [OperationContract]
        IList<CommonNote> GetAllCommonNoteByCampaignId(int campaignId);

        [OperationContract]
        IList<CommonNote> GetAllCommonNoteByProjectId(int projectId, string sortExpression);//0.6

        [OperationContract]
        IList<CommonNote> GetAllCommonNote();

        [OperationContract]
        bool DeleteCommonNoteById(int id);

        [OperationContract]
        CommonNote GetCommonNoteByCompanyIdAndNoteCategoryLookUpId(int companyId, int noteCategoryLookUpId);

        #endregion

        #region Company

        [OperationContract]
        Company AddCompany(Company company, CompanyStatus requestedStatus, bool isSelfRegistration);

        [OperationContract]
        Company UpdateCompany(Company company);

        [OperationContract]
        bool UpdateCompanyStatus(int companyId, CompanyStatus status);

        [OperationContract]
        bool ChangePendingStatus(int companyId, bool IsPending);

        [OperationContract]
        Company GetCompanyById(int id); 

        [OperationContract]
        CompanyOverviewDetails GetCompanyOverviewDetails(int CompanyId);

        [OperationContract]
        CandidateOverviewDetails GetCandidateOverviewDetails(int CandidateId);

        [OperationContract]
        IList<Company> GetAllCompany();

        [OperationContract]
        IList<Company> GetAllPendingClient();

        [OperationContract]
        PagedResponse<Company> GetPagedCompany(PagedRequest request);

        [OperationContract]
        bool DeleteCompanyById(int id);

        [OperationContract]
        bool DeleteCompanyFromMemberById(int id, int memberId);

        [OperationContract]
        Hashtable GetAllCompanyByEndClientType(int endClientType);

        [OperationContract]
        ArrayList GetAllClients();

        [OperationContract]
        ArrayList GetAllClientsByStatus(int status);

        [OperationContract]
        ArrayList GetAllClientsByStatusByCandidateId(int status, int CandidateId);//added by pravin khot on 8/June/2016*******
        //12129
        [OperationContract]
        ArrayList GetPrimaryContact();

        [OperationContract]
        ArrayList GetAllCompanyNameByCompanyStatus(CompanyStatus companyStatus);

        [OperationContract]
        Int32 GetCompanyByJobPostingId(int JobpostingId);

        [OperationContract]
        ArrayList GetAllVendorPartnerNames();

        [OperationContract]
        Int32 GetCompanyIdByName(string companyName);
        [OperationContract]
        IList<Company> Company_GetPaged(string preText, int Count);
        [OperationContract]
        int Company_GetCompanyCount();
        string GetCompanyNameById(int CompanyId);
        [OperationContract]
        string GetCompanyNameByVendorMemberId(int MemberId);

        [OperationContract]
        ArrayList GetAllCompanyList();

        int GetCompanyIdByNameForDataImport(string CompanyName);

        #endregion

        #region CompanyAssignedManager

        [OperationContract]
        CompanyAssignedManager AddCompanyAssignedManager(CompanyAssignedManager companyAssignedManager);

        [OperationContract]
        CompanyAssignedManager UpdateCompanyAssignedManager(CompanyAssignedManager companyAssignedManager);

        [OperationContract]
        CompanyAssignedManager GetCompanyAssignedManagerById(int id);

        [OperationContract]
        IList<CompanyAssignedManager> GetAllCompanyAssignedManagerByCompanyId(int companyId);

        [OperationContract]
        IList<CompanyAssignedManager> GetAllCompanyAssignedManagerByCompanyId(int companyId, string sortExpression); // 0.5

        [OperationContract]
        IList<CompanyAssignedManager> GetAllCompanyAssignedManager();

        [OperationContract]
        IList<CompanyAssignedManager> GetAllCompanyAssignedManagerByMemberId(int memberId);

        [OperationContract]
        bool DeleteCompanyAssignedManagerById(int id);

        [OperationContract]
        bool DeleteCompanyAssignedManagerByCompanyIdAndMemberId(int companyId, int memberId);

        [OperationContract]
        CompanyAssignedManager GetCompanyAssignedManagerByCompanyIdAndMemberId(int companyId, int memberId);

        [OperationContract]
        string GetPrimaryManagerNameByCompanyId(int companyId);

        //-------code introduced by pravin khot 02/Dec/2015--------------
        [OperationContract]
        IList<CompanyContact> GetAllCompanyContactByComapnyId_IP(int companyId);
        //-------------------end-------------------------

        #endregion

        #region CompanyContact

        [OperationContract]
        CompanyContact AddCompanyContact(CompanyContact companyContact);

        [OperationContract]
        CompanyContact UpdateCompanyContact(CompanyContact companyContact);

        [OperationContract]
        bool UpdateCompanyContactMemberId(int contactId, int memberId);

        [OperationContract]
        bool UpdateCompanyContactResumeFile(int contactId, string fileName);

        [OperationContract]
        CompanyContact GetCompanyContactByEmail(string email);

        [OperationContract]
        CompanyContact GetCompanyContactByMemberId(int memberId);

        [OperationContract]
        CompanyContact GetCompanyContactById(int id);

        [OperationContract]
        PagedResponse<CompanyContact> GetPagedCompanyContact(PagedRequest request);

        [OperationContract]
        CompanyContact GetCompanyPrimaryContact(int companyId);

        [OperationContract]
        IList<CompanyContact> GetAllCompanyContact();

        [OperationContract]
        IList<CompanyContact> GetAllCompanyContactByComapnyId(int companyId);

        [OperationContract]
        IList<CompanyContact> GetAllCompanyContactByCompanyGroupId(int companyGroupId);

        [OperationContract]
        IList<CompanyContact> GetAllCompanyContactByComapnyIdAndLogin(int companyId, bool IsLoginCreated);

        [OperationContract]
        bool DeleteCompanyContactById(int id);

        [OperationContract]
        bool DeleteCompanyContactByCompanyId(int companyId);

        [OperationContract]
        IList<CompanyContact> GetAllCompanyContactByVolumeHireGroup();

        [OperationContract]
        IList<CompanyContact> GetAllContactByIdAndDate(int ID, string Date);

        [OperationContract]
        ArrayList GetAllCompanyContactsByCompanyId(int companyId);

        [OperationContract]
        PagedResponse<ListWithCount> GetPagedVendorSubmissionsSummary(PagedRequest request, string type);

        [OperationContract]
        IList<ListWithCount> GetAllVendorSubmissionsGroupByDate(int JobPostingId, int VendorId, int ContactID, int StartDate, int EndDate);

        [OperationContract]
        PagedResponse<VendorSubmissions> GetPagedVendorSubmissions(PagedRequest request);

        //*************Code added by pravin khot on 26/Feb/2016***************
        [OperationContract]
        CompanyContact GetCompanyContactByEmailAndCompanyId(string email, int CompanyId);
        //******************************END************************************

        //*************Code added by pravin khot on 23/March/2017***************
        [OperationContract]
        string GetCompanyContactByJobPostingHiringTeam(int CompanyContactId);
        //******************************END************************************
        #endregion

        #region CompanyDocument

        [OperationContract]
        CompanyDocument AddCompanyDocument(CompanyDocument companyDocument);

        [OperationContract]
        CompanyDocument UpdateCompanyDocument(CompanyDocument companyDocument);

        [OperationContract]
        CompanyDocument GetCompanyDocumentById(int id);

        [OperationContract]
        IList<CompanyDocument> GetAllCompanyDocumentByCompanyId(int companyId);

        [OperationContract]
        IList<CompanyDocument> GetAllCompanyDocument();

        [OperationContract]
        bool DeleteCompanyDocumentById(int id);

        [OperationContract]
        bool DeleteCompanyDocumentByCompanyId(int companyId);

        [OperationContract]
        CompanyDocument GetCompanyDocumentByCompanyIdTypeAndFileName(int companyId, string Type, string fileName);

        [OperationContract]
        PagedResponse<CompanyDocument> CompanyDocumentGetPaged(int companyid, PagedRequest request);
        #endregion

        #region CompanyNote

        [OperationContract]
        CompanyNote AddCompanyNote(CompanyNote companyNote);

        [OperationContract]
        CompanyNote GetCompanyNoteById(int id);

        [OperationContract]
        IList<CompanyNote> GetAllCompanyNote();

        [OperationContract]
        IList<CompanyNote> GetAllCompanyNoteByCompanyId(int companyId);

        [OperationContract]
        bool DeleteCompanyNoteById(int id);

        [OperationContract]
        bool DeleteCompanyNoteByCompanyId(int companyId);

        #endregion

        #region CompanySkill

        [OperationContract]
        CompanySkill AddCompanySkill(CompanySkill companySkill);

        [OperationContract]
        CompanySkill GetCompanySkillById(int id);

        [OperationContract]
        IList<CompanySkill> GetAllCompanySkill();

        [OperationContract]
        IList<CompanySkill> GetAllCompanySkillByCompanyId(int companyId);

        [OperationContract]
        bool DeleteCompanySkillById(int id);

        [OperationContract]
        bool DeleteCompanySkillByCompanyId(int companyId);

        #endregion

        #region CompanyStatusChangeRequest

        [OperationContract]
        CompanyStatusChangeRequest AddCompanyStatusChangeRequest(CompanyStatusChangeRequest companyStatusChangeRequest);

        [OperationContract]
        CompanyStatusChangeRequest UpdateCompanyStatusChangeRequest(CompanyStatusChangeRequest companyStatusChangeRequest);

        [OperationContract]
        bool CompanyStatusChangeRequestApprove(int companyId, string comments, int changerId);

        [OperationContract]
        bool CompanyStatusChangeRequestDecline(int companyId, string comments, int changerId);

        [OperationContract]
        CompanyStatusChangeRequest GetCompanyStatusChangeRequestById(int id);

        [OperationContract]
        CompanyStatusChangeRequest GetCompanyStatusChangeRequestByCompanyId(int companyId);

        [OperationContract]
        IList<CompanyStatusChangeRequest> GetAllCompanyStatusChangeRequestByCompanyId(int companyId);

        [OperationContract]
        IList<CompanyStatusChangeRequest> GetAllCompanyStatusChangeRequest();

        [OperationContract]
        bool DeleteCompanyStatusChangeRequestById(int id);

        [OperationContract]
        bool DeleteCompanyStatusChangeRequestByCompanyId(int companyId);

        #endregion

        #region Country

        [OperationContract]
        Country AddCountry(Country country);

        [OperationContract]
        Country UpdateCountry(Country country);

        [OperationContract]
        Country GetCountryById(int id);

        [OperationContract]
        IList<Country> GetAllCountry();

        [OperationContract]
        bool DeleteCountryById(int id);

        [OperationContract]
        Int32 GetCountryIdByCountryName(string countryName);

        //0.19 starts here
        [OperationContract]
        Int32 GetStateIdByStateCode(string stateCode);

        [OperationContract]
        Int32 GetStateIdByStateCodeAndCountryId(string stateCode, int CountryId);

        [OperationContract]
        Int32 GetStateIdByStateNameAndCountryId(string Name, int CountryId);

        [OperationContract]
        Int32 GetCountryIdByCountryCode(string countryCode);

        [OperationContract]
        string GetCountryNameById(int CountryId);
        //0.19 end here


        #endregion

        #region CustomRole

        [OperationContract]
        CustomRole AddCustomRole(CustomRole customRole);

        [OperationContract]
        CustomRole UpdateCustomRole(CustomRole customRole);

        [OperationContract]
        CustomRole GetCustomRoleById(int id);

        [OperationContract]
        CustomRole GetCustomRoleByName(string Name);

        [OperationContract]
        IList<CustomRole> GetAllCustomRole();

        //***********Added by pravin khot on 16/Feb/2016*******
        [OperationContract]
        IList<CustomRole> GetAllCustomUser();
        //********************END**************************

        [OperationContract]
        string[] GetAllCustomRoleNames();

        [OperationContract]
        PagedResponse<CustomRole> GetPagedCustomRole(PagedRequest request);

        [OperationContract]
        bool DeleteCustomRoleById(int id);

        [OperationContract]
        string GetCustomRoleNameByMemberId(int MemberId);


        #endregion

        #region CustomRolePrivilege

        [OperationContract]
        CustomRolePrivilege AddCustomRolePrivilege(CustomRolePrivilege customRole);

        [OperationContract]
        CustomRolePrivilege UpdateCustomRolePrivilege(CustomRolePrivilege customRole);

        [OperationContract]
        CustomRolePrivilege GetCustomRolePrivilegeById(int id);

        [OperationContract]
        ArrayList GetAllCustomRolePrivilegeIdsByRoleId(int roleId);

        [OperationContract]
        IList<CustomRolePrivilege> GetAllCustomRolePrivilegeByRoleId(int roleId);

        [OperationContract]
        IList<CustomRolePrivilege> GetAllCustomRolePrivilege();

        [OperationContract]
        bool DeleteCustomRolePrivilegeById(int id);

        [OperationContract]
        bool DeleteCustomRolePrivilegeByRoleId(int roleId);

        [OperationContract]
        void DeleteAllCustomRolePrivilege();

        #endregion

        #region CustomSiteMap

        [OperationContract]
        CustomSiteMap AddCustomSiteMap(CustomSiteMap customSiteMap);

        [OperationContract]
        CustomSiteMap UpdateCustomSiteMap(CustomSiteMap customSiteMap);

        [OperationContract]
        CustomSiteMap GetCustomSiteMapById(int id);

        [OperationContract]
        CustomSiteMap GetCustomSiteMapByName(string name);

        [OperationContract]
        CustomSiteMap GetCustomSiteMapRoot();

        [OperationContract]
        IList<CustomSiteMap> GetAllCustomSiteMap();

        [OperationContract]
        IList<CustomSiteMap> GetAllParentCustomSiteMap();

        [OperationContract]
        IList<CustomSiteMap> GetAllCustomSiteMapByParent(int parentId);

        [OperationContract]
        PagedResponse<CustomSiteMap> GetPagedCustomSiteMap(PagedRequest request);

        [OperationContract]
        bool DeleteCustomSiteMapById(int id);

        [OperationContract]
        CustomSiteMap GetAllCustomSiteMapByParentIdAndMemberPrivilege(int ParentId, int MemberId);

        [OperationContract]
        IList<MenuListCount> CustomSiteMap_GetMenuListCount(int ParentId, int Id);
        #endregion

        #region Department

        [OperationContract]
        Department AddDepartment(Department department);

        [OperationContract]
        Department UpdateDepartment(Department department);

        [OperationContract]
        Department GetDepartmentById(int id);

        [OperationContract]
        IList<Department> GetAllDepartment();

        [OperationContract]
        bool DeleteDepartmentById(int id);

        [OperationContract]
        Int32 GetDepartmentIdByDepartmentName(string departmentName);


        [OperationContract]
        IList<Department> GetAllDepartmentBySearch(string keyword, int count);

        #endregion

        #region Duplicate Member

        [OperationContract]
        IList<DuplicateMember> GetDuplicateRecords(bool checkFirstName, string strFirstName,
            bool checkMiddleName, string strMiddleName,
            bool checkLastName, string strLastName,
            bool checkEmailId, string strEmailId,
            bool checkDOB, DateTime? DOB,
            bool checkSSN, string strSSN);

        #endregion

        #region Employee

        [OperationContract]
        Employee GetEmployeeById(int id);

        [OperationContract]
        Employee GetEmployeeByEmailId(string  email); //added by pravin khot on 7/June/2016

        [OperationContract]
        Employee GetEmployeeByIdForHR(int id);

        [OperationContract]
        Employee GetEmployeeActivitiesReportByIdAndCreateDate(int id, DateTime createDate);

        [OperationContract]
        PagedResponse<Employee> GetPagedEmployee(PagedRequest request);

        [OperationContract]
        PagedResponse<Employee> GetPagedEmployeeReport(PagedRequest request);

        [OperationContract]
        PagedResponse<Employee> GetPagedEmployee(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, DateTime updatedFrom, DateTime updatedTo, int updatedBy,
            int country, int state, string city, int industry, int functionalCategory, int workPermit,
            int gender, int maritalStatus, int educationId, int assignedManager);



        [OperationContract]
        EmployeeStatistics GetEmployeeStatisticsByEmployeeId(int employeeId);

        [OperationContract]
        BusinessOverview GetBusinessOverviewByEmployeeId(int employeeId);

        [OperationContract]
        HireDesk GetEmployeeHireDeskByEmployeeId(int employeeId);

        // starts 0.18
        [OperationContract]
        DataTable GetProductivityOverviewByDate(string RequieDate);
        // ends 0.18

        DataTable GetMyProductivityOverviewByDate(string RequireDate, int MemberId);

        [OperationContract]
        PagedResponse<EmployeeProductivity> GetPagedEmployeeProductivity(DateTime StartDate, DateTime EndDate, int MemberId, PagedRequest request);

        [OperationContract]
        EmployeeOverviewDetails GetEmployeeOverviewDetails(int memberid);


        [OperationContract]
        void Employee_EmployeeFirstLoginCreate(int MemberId);
        [OperationContract]
        bool Employee_IsEmployeeFirstLogin(int MemberId);


        [OperationContract]
        IList<EmployeeProductivity> GetEmployeeProductivityReport(PagedRequest request);

        [OperationContract]
        ArrayList GetAllEmployeeByTeamId(string id);

        [OperationContract]
        PagedResponse<EmployeeProductivity> GetPagedByProductityReport(PagedRequest request);

        [OperationContract]
        PagedResponse<EmployeeProductivity> GetPagedByTeamProductityReport(PagedRequest request);

        [OperationContract]
        IList<EmployeeProductivity> GetEmployeeProductivityReportByDate(string UserIds, string TeamIds, int StartDate, int EndDate);

        [OperationContract]
        ArrayList GetEmployeeNameAndContactNumber(int EmployeeId);

        //*****************added by pravin khot on 16/May/2016************* time zone
        [OperationContract]
        ArrayList GetAllTeamByTeamLeaderId(int TeamleaderId);

        [OperationContract]
        ArrayList GetAllTimeZone();

        [OperationContract]
        void Employee_SaveTimezone(int MemberId, int TimezoneId);

        [OperationContract]
        Employee GetTimeZoneByMemberId(int MemberId);
        //*****************************END********************************

        //////////////////////code added by Sumit Sonawane on 21/Mar/2017////////////////////////////////////////////////////////////////////////////
        [OperationContract]
        IList<EmployeeProductivity> GetEmployeeProductivityDetails();

        [OperationContract]
        IList<EmployeeProductivity> GetEmployeePresenttoInterviewRatio();

        [OperationContract]
        IList<EmployeeProductivity> GetEmployeeTimetoHire();

        [OperationContract]
        IList<EmployeeProductivity> GetJoinedToRejectedByBusiness();

        [OperationContract]
        IList<EmployeeProductivity> GetCandidateBySkillId();

        [OperationContract]
        IList<EmployeeProductivity> GetJobPostingByMonthYear();
        ////////////////////// End ////////////////////////////////////////////////////////////////////////////
        
        #endregion

        #region EmployeeTeamBuilder

        [OperationContract]
        EmployeeTeamBuilder AddEmployeeTeamBuilder(EmployeeTeamBuilder employeeTeamBuilders);

        [OperationContract]
        EmployeeTeamBuilder UpdateEmployeeTeamBuilder(EmployeeTeamBuilder employeeTeamBuilder);
        [OperationContract]
        EmployeeTeamBuilder EmployeeTeamBuilder_GetByTeamId(int id);

        [OperationContract]
        IList<EmployeeTeamBuilder> EmployeeTeamBuilder_GetAllTeam();

        [OperationContract]
        bool DeleteEmployeeTeamBuilderById(int id);

        [OperationContract]
        PagedResponse<EmployeeTeamBuilder> GetPagedEmployeeTeamBuilder(PagedRequest request);

        //*****************added by Sumit Sonawane on 20/May/2016*************
        [OperationContract]
        IList<EmployeeTeamBuilder> EmployeeTeamBuilder_GetTeamsByMemberId(int MemberId);

        //*****************************END********************************

        #endregion

        #region EmployeeReferral

        [OperationContract]
        int AddReferral(EmployeeReferral employeeref);

        [OperationContract]
        PagedResponse<EmployeeReferral> GetPagedEmployeeReferral(PagedRequest request);

        [OperationContract]
        PagedResponse<ListWithCount> EmployeeReferral_GetPagedSummary(PagedRequest request, string type);


        [OperationContract]
        ArrayList GetAllEmployeeReferrerList();

        [OperationContract]
        IList<ListWithCount> EmployeeReferral_GetCountGroupbyDate(int JobPostingId, int ReferrerID, int StartDate, int EndDate);
        //////////////////////code added by Sumit on 26/May/2016//////////////////////
        [OperationContract]
        IList<ListWithCount> EmployeeReferral_GetCountGroupbyDate(string JobPostingId, int ReferrerID, int StartDate, int EndDate);
        //////////////////////////////////////////////////////////////////////////////
        [OperationContract]
        EmployeeReferral EmployeeReferral_GetByMemberId(int MemberID);

        #endregion

        #region EventLog
        [OperationContract]
        void AddEventLog(EventLogForRequisitionAndCandidate Log, string CandidateId);

        [OperationContract]
        PagedResponse<EventLogForRequisitionAndCandidate> GetPagedForEventLog(PagedRequest request);

        [OperationContract]
        IList<EventLogForRequisitionAndCandidate> GetAllEventLog(int JobPostingId, int CandidateId, int CreatorId);

        [OperationContract]
        PagedResponse<EventLogForRequisitionAndCandidate> GetPagedEventLogReport(PagedRequest request);

        ////////////////////////////Sumit Sonawane 1/Mar/2017///////////////////////////////////////////////////////////////////
        [OperationContract]
        PagedResponse<EventLogForRequisitionAndCandidate> GetPagedHiringMatrixEventLog(int MemberID, PagedRequest request);
        #endregion

        #region GenericLookup

        [OperationContract]
        GenericLookup AddGenericLookup(GenericLookup genericLookup);

        [OperationContract]
        GenericLookup UpdateGenericLookup(GenericLookup genericLookup);

        [OperationContract]
        GenericLookup GetGenericLookupById(int id);

        [OperationContract]
        GenericLookup GetGenericLookupByDescription(string location);//added by pravin khot on 25/May/2016

        [OperationContract]
        IList<GenericLookup> GetAllGenericLookup();

        [OperationContract]
        IList<GenericLookup> GetAllGenericLookupByLookupType(LookupType lookupType);

        [OperationContract]
        IList<GenericLookup> GetAllGenericLookupByLookupType(string lookuptypes); // For Optimization by Vignesh

        [OperationContract]
        IList<GenericLookup> GetAllGenericLookupByLookupTypeAndName(LookupType lookupType, string lookupName);

        [OperationContract]
        PagedResponse<GenericLookup> GetPagedGenericLookup(PagedRequest request);

        [OperationContract]
        bool DeleteGenericLookupById(int id);

        [OperationContract]
        string GetLookUPNamesByIds(string ids);

        [OperationContract]
        IList<GenericLookup> GetGenericLookupbyIDs(string IDs);

        //Line introduced by Prasanth on 05/Dec/2015
        [OperationContract]
        IList<GenericLookup> InterviewAssessment_GetBy_InterviewId_InterviewerEmail(int InterviewId, string Email);
        #endregion

        #region HiringMatrix

        [OperationContract]
        PagedResponse<HiringMatrix> GetPagedHiringMatrix(PagedRequest request);

        [OperationContract]
        PagedResponse<HiringMatrix> GetMinPagedHiringMatrix(PagedRequest request);

        [OperationContract]
        PagedResponse<HiringMatrix> GetMinPagedHiringMatrixAll(PagedRequest request);

        HiringMatrix getHiringMatrixByJobPostingIDAndCandidateId(int JobPostingID, int CandidateId);

        ////////////////////////////Sumit Sonawane 22/07/2016///////////////////////////////////////////////////////////////////
        [OperationContract]
        PagedResponse<HiringMatrix> GetPagedHiringMatrixBucketView(PagedRequest request);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////Sumit Sonawane 1/Mar/2017///////////////////////////////////////////////////////////////////
        [OperationContract]
        HiringMatrix getHiringMatrixLevelByJobPostingIDAndCandidateId(int JobPostingID, int CandidateId);

        [OperationContract]
        HiringMatrix AddHiringMatrixLogDetails(HiringMatrix AddHiringMatrixLogDetails);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////Sumit Sonawane 27/07/2016///////////////////////////////////////////////////////////////////
        [OperationContract]
        PagedResponse<HiringMatrix> HiringMatrixBucket(PagedRequest request);

        [OperationContract]
        PagedResponse<HiringMatrix> HiringMatrixBucketLoad(PagedRequest request);

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #endregion

        #region HiringMatrixLevels
        [OperationContract]
        HiringMatrixLevels AddHiringMatrixLevels(HiringMatrixLevels hiringMatrixLevels);

        [OperationContract]
        HiringMatrixLevels UpdateHiringMatrixLevels(HiringMatrixLevels hiringMatrixLevels);

        [OperationContract]
        HiringMatrixLevels GetHiringMatrixLevelsById(int id);

        [OperationContract]
        IList<HiringMatrixLevels> GetAllHiringMatrixLevels();

        //*********Pravin khot – Candidate Hiring Status Update – 3/March/2017 – Start *****************
        [OperationContract]
        IList<HiringMatrixLevels> GetAllHiringMatrixLevelsForHiringManager(int MemberId);
        //*********Pravin khot – Candidate Hiring Status Update – 3/March/2017 – End *****************

        [OperationContract]
        bool DeleteHiringMatrixLevelsById(int id);

        [OperationContract]
        void DeleteHiringMatrixLevels();

        [OperationContract]
        HiringMatrixLevels HiringMatrixLevel_GetPreviousLevelById(int id);

        [OperationContract]
        HiringMatrixLevels HiringMatrixLevel_GetNextLevelById(int id);

        [OperationContract]
        HiringMatrixLevels HiringMatrixLevel_GetInitialLevel();

        [OperationContract]
        HiringMatrixLevels HiringMatrixLevel_GetLastLevel();

        [OperationContract]
        int GetHiringMatrixLevelsCount();

        [OperationContract]
        IList<HiringMatrixLevels> GetAllHiringMatrixLevelsWithCount(int JobPostingId);

        [OperationContract]
        HiringMatrixLevels HiringMatrixLevel_GetLevelByName(string LevelName);

        Int32 HiringMatrixLevel_Id(int LevelId);//added by pravin khot on 13/May/2016

        [OperationContract]
        IList<HiringMatrixLevels> GetAllHiringMatrixLevelsWithCountByMemberId(int MemberId);

        #endregion

        #region Interview

        [OperationContract]
        Interview AddInterview(Interview interview);

        [OperationContract]
        Interview UpdateInterview(Interview interview);

        [OperationContract]
        Interview GetInterviewById(int id);

        [OperationContract]
        IList<Interview> GetAllInterview();

        [OperationContract]
        bool DeleteInterviewById(int id);

        //added by pravin khot on 4/May/2016***********
        [OperationContract]
        bool CancelInterviewById(int id);
            
        [OperationContract]
        bool UpdateInterviewIcsCodeById(int id,string ics);
        //**************END*****************

        //Code added by Sumit Sonawane on 1/Mar/2017 ***********
        [OperationContract]
        bool UpdateInterviewNoShow_ById(int id);

        [OperationContract]
        bool UpdateInterviewCompleted_ById(int id);

        [OperationContract]
        Interview AddInterviewStatusDetails(Interview InterviewStatusDetails);
        //**************END*****************

        [OperationContract]
        IList<Interview> GetAllInterviewByJobPostingId(int jobPostingId);

        [OperationContract]
        IList<Interview> GetInterviewDateByMemberId(int MemberId);

        [OperationContract]
        int GetContactIdByJobPostingId(int JobPostingId);

        [OperationContract]
        IList<Interview> GetInterviewByMemberIdAndJobPostingID(int MemberId, int JobPostingId);


        [OperationContract]
        string Interview_GetInterviewTemplate(int memberId, int InterviewID, string Template_Type, int CreatorId);

        //***************Code introduced by Pravin on 10/Dec/2015 Start****************************************

        string SuggestedInterviewer_Id(int InterviewerId);

        //********************************************End********************************************************
        #endregion

        #region InterviewInterviewerMap

        [OperationContract]
        InterviewInterviewerMap AddInterviewInterviewerMap(InterviewInterviewerMap interviewInterviewerMap);

        //***************Code introduced by Pravin on 11/Dec/2015 Start****************************************

        [OperationContract]
        InterviewInterviewerMap AddSuggestedInterviewInterviewerMap(InterviewInterviewerMap interviewInterviewerMap);

        //********************************************End********************************************************

        [OperationContract]
        InterviewInterviewerMap UpdateInterviewInterviewerMap(InterviewInterviewerMap interviewInterviewerMap);

        [OperationContract]
        InterviewInterviewerMap GetInterviewInterviewerMapById(int id);

        [OperationContract]
        IList<InterviewInterviewerMap> GetAllInterviewInterviewerMap();

        [OperationContract]
        IList<InterviewInterviewerMap> GetAllInterviewersByInterviewId(int InterviewId);

        //added by pravin khot on 4/May/2016******** 
        [OperationContract]
        IList<InterviewInterviewerMap> GetAllsuggestedInterviewersByInterviewId(int InterviewId);
        //*****************END************************

        [OperationContract]
        string GetInterviersNameByInterviewId(int Interviewid);

        [OperationContract]
        bool DeleteInterviewInterviewerMapById(int id);

        [OperationContract]
        bool DeleteInterviewInterviewerMapByInterviewerId(int interviewerId);

        [OperationContract]
        bool DeleteInterviewInterviewerMapByInterviewId(int interviewId);

        #endregion

        #region IPAccessRules
        [OperationContract]
        IPAccessRules AddIPAccessRules(IPAccessRules rules);

        [OperationContract]
        IPAccessRules UpdateIPAccessRules(IPAccessRules rules);

        [OperationContract]
        IPAccessRules IPAccessRules_GetById(int Id);

        [OperationContract]
        IList<IPAccessRules> GetAllIPAccessRules();

        [OperationContract]
        bool IPAccessRules_DeleteById(int Id);

        [OperationContract]
        PagedResponse<IPAccessRules> IPAccessRules_GetPaged(PagedRequest request);


        #endregion

		#region InterviewFeedback

        void InterviewFeedback_Add(InterviewFeedback interviewFeedback);

        void InterviewFeedback_Update(InterviewFeedback interviewFeedback);

        InterviewFeedback InterviewFeedback_GetById(int id);

        IList<InterviewFeedback> InterviewFeedback_GetAll();

        PagedResponse<InterviewFeedback> InterviewFeedback_GetPaged(PagedRequest request);

        void InterviewFeedback_DeleteById(int id);

        IList<InterviewFeedback> InterviewFeedback_GetByInterviewId(int InterviewId);

        //Line introduced by Prasanth on 27/Dec/2015
        InterviewFeedback InterviewFeedback_GetByInterviewId_InterviewerEmail(int InterviewId, string InterviewerEmail);
        #endregion


        //***************Code introduced by Prasanth on 20/Jul/2015 Start****************************************
        #region InterviewerFeedback

        [OperationContract]
        void InterviewerFeedback_Add(InterviewFeedback interviewFeedback);

        [OperationContract]
        void InterviewerFeedback_Update(InterviewFeedback interviewFeedback);

        [OperationContract]
        InterviewFeedback InterviewerFeedback_GetById(int id);

        [OperationContract]
        IList<InterviewFeedback> InterviewerFeedback_GetAll();

        [OperationContract]
        PagedResponse<InterviewFeedback> InterviewerFeedback_GetPaged(PagedRequest request);

        [OperationContract]
        void InterviewerFeedback_DeleteById(int id);

        [OperationContract]
        IList<InterviewFeedback> InterviewerFeedback_GetByInterviewId(int InterviewId);

        //***************Code introduced by Pravin on 22/Dec/2015 Start****************************************
        IList<InterviewFeedback> InterviewFeedback_GetByInterviewIdDetail(int InterviewId);
        IList<InterviewFeedback> InterviewFeedback_GetByInterviewIdAllEmail(int InterviewId);
        //************************************END****************************************************

        //******************************code added by pravin khot on 24/Dec/2015********************
        [OperationContract]
        PagedResponse<InterviewFeedback> GetPagedInterviewFeedbackReport(PagedRequest request);

        [OperationContract]
        PagedResponse<InterviewFeedback> GetPagedInterviewFeedbackReportPrint(PagedRequest request);

        [OperationContract]
        PagedResponse<InterviewFeedback> GetPagedInterviewAssesReportPrint(PagedRequest request);
        //********************************************End*****************************************




        #endregion

        //************************************END****************************************************


        //***************Code introduced by Prasanth on 16/Oct/2015 Start****************************************
        #region InterviewQuestionBank

        [OperationContract]
        void InterviewQuestionBank_Add(InterviewQuestionBank IntervieweQuestionBank);

        [OperationContract]
        void InterviewQuestionBank_Update(InterviewQuestionBank InterviewQuestionBank);

        [OperationContract]
        InterviewQuestionBank InterviewQuestionBank_GetById(int id);

        [OperationContract]
        IList<InterviewQuestionBank> InterviewQuestionBank_GetAll();

        [OperationContract]
        PagedResponse<InterviewQuestionBank> InterviewQuestionBank_GetPaged(PagedRequest request);

        [OperationContract]
        void InterviewQuestionBank_DeleteById(int id);

        [OperationContract]
        IList<InterviewQuestionBank> InterviewQuestionBank_GetByInterviewId(int InterviewId);
        #endregion



        #region QuestionBank

        //void QuestionBank_Add(QuestionBank QuestionBank);

        [OperationContract]
        QuestionBank QuestionBank_Add(QuestionBank QuestionBank);

        [OperationContract]
        QuestionBank QuestionBank_Update(QuestionBank QuestionBank);

        [OperationContract]
        QuestionBank QuestionBank_GetById(int id);

        [OperationContract]
        IList<QuestionBank> QuestionBank_GetAll();

        [OperationContract]
        IList<QuestionBank> QuestionBank_GetAll(string SortExpression);

        [OperationContract]
        PagedResponse<QuestionBank> QuestionBank_GetPaged(PagedRequest request);

        bool QuestionBank_DeleteById(int id);


        #endregion



        #region InterviewResponse


        [OperationContract]
        void InterviewResponse_Add(InterviewResponse InterviewResponse);

        [OperationContract]
        void InterviewResponse_Update(InterviewResponse InterviewResponse);

        [OperationContract]
        InterviewResponse InterviewResponse_GetById(int id);

        [OperationContract]
        IList<InterviewResponse> InterviewResponse_GetAll();

        [OperationContract]
        PagedResponse<InterviewResponse> InterviewResponse_GetPaged(PagedRequest request);

        [OperationContract]
        void InterviewResponse_DeleteById(int id);


        [OperationContract]
        IList<InterviewResponse> InterviewResponse_GetByInterviewId(int InterviewId);


        [OperationContract]
        IList<InterviewResponse> InterviewResponse_GetByInterviewId_Email(int InterviewId, string Email);

        //IList<GenericLookup> InterviewAssessment_GetBy_InterviewId_InterviewerEmail(int InterviewId, string Email);

        #endregion
        //************************************END****************************************************

        //code introduced by pravin khot add new interview panel 23/Nov/2015
        //*********start******************
        #region InterviewPanel


        [OperationContract]
        InterviewPanel InterviewPanel_Add(InterviewPanel InterviewPanel);

        [OperationContract]
        InterviewPanel InterviewPanel_AddOtherInterviewer(InterviewPanel InterviewPanel);

        [OperationContract]
        InterviewPanel InterviewPanel_Update(InterviewPanel InterviewPanel);

        [OperationContract]
        InterviewPanel InterviewPanel_UpdateOtherInterviewer(InterviewPanel InterviewPanel);

        InterviewPanel InterviewPanel_GetById(int id);

        IList<InterviewPanel> InterviewPanel_GetAll();
        IList<InterviewPanel> InterviewPanel_GetAll(string SortExpression);
        PagedResponse<InterviewPanel> InterviewPanel_GetPaged(PagedRequest request);

        bool InterviewPanel_DeleteById(int id);
        bool InterviewPanel_UpdateDeleteById(int InterviewPanelids);
        bool Interview_ChkEmailId(int InterviewerId, int panelid);
        bool Interview_ClientChkEmailId(int InterviewerId, int panelid);
        IList<InterviewPanel> InterviewPanel_OtherChkEmailId(int panelid);
        IList<InterviewPanel> InterviewSchedule_OtherChkEmailId(int RequisitionId);



        #endregion

        //*********end*******************

        //code introduced by pravin khot add new InterviewPanelMaster 27/nov/2015
        //*********start******************
        #region InterviewPanelMaster


        [OperationContract]
        InterviewPanelMaster InterviewPanelMaster_Add(InterviewPanelMaster InterviewPanelMaster);

        [OperationContract]
        InterviewPanelMaster InterviewPanelMaster_Update(InterviewPanelMaster InterviewPanelMaster);

        InterviewPanelMaster InterviewPanelMaster_GetById(int id);

        IList<InterviewPanelMaster> InterviewPanelMaster_GetAll();
        IList<InterviewPanelMaster> InterviewPanelMaster_GetAll(string SortExpression);
        PagedResponse<InterviewPanelMaster> InterviewPanelMaster_GetPaged(PagedRequest request);

        bool InterviewPanelMaster_DeleteById(int id);

        [OperationContract]
        string GetSkills(int InterviewPanelMaster_ById);

        Int32 InterviewPanelMaster_Id(string InterviewPanel_Name);
        Int32 InterviewPanelMaster_Id(string InterviewPanel_Name, int InterviewPanel_EditId);
        //InterviewPanelMaster InterviewPanelMaster_Id(string name);


        #endregion

        //*********end*******************

        #region JobPosting

        [OperationContract]
        ArrayList JobPosting_GetAllEmployeeReferralEnabledByReferrerId(int ReferrerId);

        [OperationContract]
        JobPosting AddJobPosting(JobPosting jobPosting);

        [OperationContract]
        JobPosting UpdateJobPosting(JobPosting jobPosting);

        //*************************Code added by pravin khot on 18/Jan/2016***********************
        [OperationContract]
        JobPosting CareerJobId_Add(JobPosting jobPosting);
        IList<JobPosting> MemberCareerAllOpportunities();
        //*********************************End*********************************************

        //**********Code added by pravin khot on 29/Jan/2016*****************
        [OperationContract]
        ArrayList GetAllJobPostingByBUId(int BUId);
        //******************End******************************

        //Code introduced by Pravin khot on 2/Feb/2016
        [OperationContract]
        PagedResponse<JobPosting> GetPagedRequisitionSourceBreakupReport(PagedRequest request);
        //********************END*******************
        [OperationContract]
        void UpdateJobPostingStatusById(int statusid, int jobpostingid, int updatorid);

        [OperationContract]
        JobPosting GetJobPostingById(int id);

        [OperationContract]
        IList<JobPosting> GetAllJobPosting();

        IList<JobPosting> GetAllRequisitionByEmployeeId(int memberId);

        [OperationContract]
        IList<JobPosting> GetAllJobPostingByMemberId(int memberId);

         [OperationContract]
        ArrayList GetAllJobPostingByCandidatesId(int ClientId, int CandidateId);  //added by pravin khot on 8/June/2016      

        [OperationContract]
        IList<JobPosting> GetAllJobPostingByJobCartAlertId(int jobCartAlertId);

        [OperationContract]
        IList<JobPosting> GetAllJobPostingByProjectId(int projectId);
        [OperationContract]
        int GetCountOfJobPostingByStatusAndManagerId(int ManagerId);
        [OperationContract]
        ArrayList GetPagedJobPostingByStatusAndManagerId(int status, int ManagerId, int count, string SearchKey);
        //starts here 0.13
        [OperationContract]
        IList<JobPosting> GetAllJobPostingByProjectId(int projectId, string sortExpression);
        //ends here 0.13

        [OperationContract]
        PagedResponse<JobPosting> GetPagedJobPosting(PagedRequest request);

        [OperationContract]
        PagedResponse<JobPosting> GetPagedJobPostingWithCandidateCount(PagedRequest request);

        [OperationContract]
        PagedResponse<JobPosting> GetPagedJobPosting(string allKeys, string anyKey, string jobTitle, string jobType, string city, string lastUpdateDate, int jobStatus, string jobIndustry, PagedRequest request); // 8971

        [OperationContract]
        bool DeleteJobPostingById(int id, int currentMemberId);

        [OperationContract]
        JobPosting CreateJobPostingFromExistingJob(int id, string jobPostingCode, int creatorId);

        [OperationContract]
        JobPosting CreateProjectJobFromExistingTemplateJob(int projectId, int jobId);

        [OperationContract]
        ArrayList GetAllJobPostingByStatus(int status);

        [OperationContract]
        ArrayList GetAllJobPostingByClientId(int ClientId);

        [OperationContract]
        ArrayList GetJobPostingByClientIdAndManagerId(int ClientId, int ManagerId, int count, string SearchKey);

        [OperationContract]
        ArrayList GetAllJobPostingByClientIdAndManagerId(int ClientId, int ManagerId);


        [OperationContract]
        ArrayList GetAllJobPostingByInterview();

        [OperationContract]
        DataTable GetAllJobPostingListByStatus(int status);

        [OperationContract]
        ArrayList GetAllJobPostingListByStatusId(int statusId);

        [OperationContract]
        ArrayList GetAllJobPostingByStatusAndManagerId(int status, int managerId);

        [OperationContract]
        ArrayList GetAllJobPostingByCandidateId(int candidateId);

        [OperationContract]
        PagedResponse<JobPosting> GetPagedJobPostingReport(PagedRequest request);

        //Code introduced by Prasanth on 22/May/2015
        [OperationContract]
        //PagedResponse<JobPosting> GetPagedRequisitionAgingReport(string IsTemplate, string Account, string ReqStartDateFrom, string ReqEndDateTo, string Recruiter);
        PagedResponse<JobPosting> GetPagedRequisitionAgingReport(PagedRequest request);
        //********************END*******************

        //0.25    12129
        [OperationContract]
        PagedResponse<Submission> GetPagedSubmissionReport(PagedRequest request);

        [OperationContract]
        PagedResponse<Submission> GetPagedDashBoardSubmission(PagedRequest request);

        [OperationContract]
        PagedResponse<JobPosting> GetPagedVolumeHireJobPosting(string allKeys, string anyKey, string jobTitle, string jobType, string city, string lastUpdateDate, string volumeHire, int jobStatus, string jobIndustry, PagedRequest pageRequest);//9811

        [OperationContract]
        IList<JobPosting> GetAllJobPostingVolumeHireByStatus(int jobStatus);

        [OperationContract]
        int GetRequisitionCountByMemberId(int MemberId);        //0.3


        [OperationContract]
        int GetLastRequisitionCodeByMemberId(int MemberId);        //0.3

        [OperationContract]
        int UpdateMemberRequisitionCount(int MemberId, int RequisitionCount);       //0.3

        [OperationContract]
        int GetCountOfJobPostingByStatus();


        [OperationContract]
        JobPosting GetJobPostingByMemberIdAndJobPostingCode(int MemberId, string JobPostingCode);


        [OperationContract]
        string JobPosting_GetRequiredSkillNamesByJobPostingID(int JobPostingID);

        [OperationContract]
        string GetJobPostingIdByJobPostingCode(string JobPostingCode);

        [OperationContract]
        int GetRequisitionStatusBy_Id(int JobPostingId);//added by pravin khot on 25/May/2016

        [OperationContract]
        int GetTimeZoneIdBy_MemberId(int MemberId);//added by pravin khot on 25/May/2016

        [OperationContract]
        string GetRequisitionStatus_Id(int JobPostingId);//added by pravin khot on 25/May/2016

        [OperationContract]
        PagedResponse<JobPosting> GetPagedForCandidatePortal(PagedRequest request, int MemberId);  

        [OperationContract]
        PagedResponse<JobPosting> GetPagedForVendorPortal(PagedRequest request,int VendorId);

         [OperationContract]
        PagedResponse<JobPosting> GetPagedForVendorPortalForMyPerformance(PagedRequest request, int VendorId,int MemberId);

        [OperationContract]
        PagedResponse<JobPosting> JobPosting_GetPagedEmployeeReferal(PagedRequest request);

        [OperationContract]
        ArrayList JobPosting_GetAllEmployeeReferralEnabled();
        [OperationContract]
        ArrayList JobPosting_GetAllVendorPortalEnabled();

        [OperationContract]
        ArrayList GetAllBUContactIdByBUId(int BUId);   //added by pravin khot on 8/Aug/2016

        [OperationContract]
        ArrayList GetAllJobPostingListBymemberId(int memberId);

        #endregion

        #region JobPostingSearchAgent

        [OperationContract]
        JobPostingSearchAgent AddJobPostingSearchAgent(JobPostingSearchAgent jobPosting);

        [OperationContract]
        JobPostingSearchAgent UpdateJobPostingSearchAgent(JobPostingSearchAgent jobPosting);

        [OperationContract]
        JobPostingSearchAgent GetJobPostingSearchAgentById(int id);

        [OperationContract]
        bool DeleteJobPostingSearchAgentByJobPostingId(int JobPostingId);

        [OperationContract]
        JobPostingSearchAgent GetJobPostingSearchAgentByJobPostingId(int JobPostingId);

        [OperationContract]
        void UpdateJobPostingSearchAgentByJobPostingIdAndIsRemoved(int JobPostingId, bool IsRemoved);

        #endregion

        #region JobPostingDocument

        [OperationContract]
        JobPostingDocument AddJobPostingDocument(JobPostingDocument jobPostingDocument);

        [OperationContract]
        JobPostingDocument UpdateJobPostingDocument(JobPostingDocument jobPostingDocument);

        [OperationContract]
        JobPostingDocument GetJobPostingDocumentById(int id);

        [OperationContract]
        IList<JobPostingDocument> GetAllJobPostingDocumentByJobPostingId(int jobPostingId);

        [OperationContract]
        IList<JobPostingDocument> GetAllJobPostingDocument();

        [OperationContract]
        bool DeleteJobPostingDocumentById(int id);

        [OperationContract]
        bool DeleteJobPostingDocumentByJobPostingId(int jobPostingId);

        #endregion

        #region JobPostingHiringTeam

        [OperationContract]
        void AddMultipleJobPostingHiringTeam(string MemberIds, int JobpostingId, int CreatorId, string employeeType);

        //**********Added by pravin khot on 2/March/2017
        [OperationContract]
        void AddMultipleJobPostingHiringTeamWithOpening(int MemberId,int AssignOpenings, int JobpostingId, int CreatorId, string employeeType,bool IsPrimary);

        [OperationContract]
        IList<JobPostingHiringTeam> GetAllJobPostingRecruiterGroupByJobPostingId(int jobPostingId); 
        //******************END*************************

        [OperationContract]
        JobPostingHiringTeam AddJobPostingHiringTeam(JobPostingHiringTeam jobPostingHiringTeam);

        [OperationContract]
        JobPostingHiringTeam UpdateJobPostingHiringTeam(JobPostingHiringTeam jobPostingHiringTeam);

        [OperationContract]
        JobPostingHiringTeam GetJobPostingHiringTeamById(int id);

        [OperationContract]
        JobPostingHiringTeam GetJobPostingHiringTeamByMemberId(int memberId, int jobPostingId);

        [OperationContract]
        IList<JobPostingHiringTeam> GetAllJobPostingHiringTeam();

        [OperationContract]
        IList<JobPostingHiringTeam> GetAllJobPostingHiringTeamByJobPostingId(int jobPostingId);

        [OperationContract]
        bool DeleteJobPostingHiringTeamById(int id);

        [OperationContract]
        bool DeleteJobPostingHiringTeamByJobPostingId(int jobPostingId);

        [OperationContract]
        IList<JobPostingHiringTeam> GetAllJobPostingHiringTeamByMemberId(int memberId);

      


        #endregion

        #region JobPostingNote

        [OperationContract]
        JobPostingNote AddJobPostingNote(JobPostingNote jobPostingNote);

        [OperationContract]
        JobPostingNote GetJobPostingNoteById(int id);

        [OperationContract]
        IList<JobPostingNote> GetAllJobPostingNote();

        [OperationContract]
        IList<JobPostingNote> GetAllJobPostingNoteByJobPostingId(int jobPostingId);

        [OperationContract]
        bool DeleteJobPostingNoteById(int id);

        [OperationContract]
        bool DeleteJobPostingNoteByJobPostingId(int jobPostingId);

        [OperationContract]
        //IList<RequisitionNotesEntry> GetAllReqNotesByJobPostingId(int jobPostingId);
        PagedResponse<RequisitionNotesEntry> GetAllReqNotesByJobPostingId(PagedRequest page);

        #endregion

        #region Job & Schedule


        [OperationContract]
        bool CreateJobSchedule(string JobType, string SqlCommandText, DateTime StartDateTime, DateTime EndDateTime, bool Active, string DbConnectionString);

        [OperationContract]
        DataTable GetAllJobSchedule(string DbConnectionString);

        [OperationContract]
        bool DeleteJobSchedule(string JobId, string DbConnectionString);

        [OperationContract]
        bool UpdateJobSchedule(string JobId, string JobType, string SqlCommandText, DateTime StartDateTime, DateTime EndDateTime, bool Active, string DbConnectionString);

        [OperationContract]
        bool ChangeConfiguration(bool enable, string DbConnectionString);

        #endregion

        #region JobPostingSkillSet

        [OperationContract]
        JobPostingSkillSet AddJobPostingSkillSet(JobPostingSkillSet jobPostingSkillSet);

        [OperationContract]
        JobPostingSkillSet UpdateJobPostingSkillSet(JobPostingSkillSet jobPostingSkillSet);

        [OperationContract]
        JobPostingSkillSet GetJobPostingSkillSetById(int id);

        [OperationContract]
        JobPostingSkillSet GetJobPostingSkillSetBySkillId(int skillId);

        [OperationContract]
        JobPostingSkillSet GetJobPostingSkillSetByJobPostingIdAndSkillId(int jobPostingId, int skillId);

        [OperationContract]
        IList<JobPostingSkillSet> GetAllJobPostingSkillSet();

        [OperationContract]
        bool DeleteJobPostingSkillSetById(int id);

        // 0.1 starts here
        [OperationContract]
        bool DeleteJobPostingSkillSetByJobPostingId(int id);
        // 0.1 ends here

        [OperationContract]
        IList<JobPostingSkillSet> GetAllJobPostingSkillSetByJobPostingId(int jobPostingId);

        #endregion

        #region MailQueue

        [OperationContract]
        int AddMailInMailQueue(MailQueue mailqueue);


        [OperationContract]
        IList<MailQueue> GetAllMailInQueue();

        [OperationContract]
        bool DeleteMailQueueById(int id, out int MemberEmailId);

        [OperationContract]
        void UpdateFileNamesInMailQueue(string fileNames, int mailQueueId, int NoOfAttachments);

        [OperationContract]
        void UpdateMailbodyForInterviewTemplate(string EmailBody, int MemberEmailId);

        #endregion
      
        #region Member

        [OperationContract]
        Member AddMember(Member member);

        [OperationContract]
        Member AddFullMemberInfo(Member member, bool IsCreateMemberDetailandExtendedInfo, int Availability, bool IsCreateMemberManager);

        [OperationContract]
        ArrayList GetLicenseKeybyId(string intLicenseKey, string varchrDomainName); //0.17

        [OperationContract]
        Member UpdateMember(Member member);

        [OperationContract]
        Member UpdateMemberStatus(Member member);

        [OperationContract]
        Member UpdateMemberResumeSharing(Member member);

        [OperationContract]
        void UnSubscribeMe(int memberId);

        //*******code added by pravin khot on 27/April/2016***********
        string CandidateAvailableInRequisition_Name(int MemberId, int CurrentJobPostingId);
        string CandidateAvailableInRequisition_Name(int MemberId);
        [OperationContract]
        Member UpdateMemberthroughvendor(Member member);
        //**********************END******************************

        [OperationContract]
        Member GetMemberById(int id);

        [OperationContract]
        string GetMemberUserNameById(int id);//0.22
        //Code introduced by Prasanth on 10/Jun/2016 Start
        [OperationContract]
        string GetMemberADUserNameById(int id);
        //*****************END***********************
        [OperationContract]
        string GetMemberNameById(int id);//0.22

        [OperationContract]
        Member GetMemberByUserId(Guid userId);

        [OperationContract]
        Member GetMemberByUserName(string userId);

        [OperationContract]
        IList<Member> GetAllMember();

        [OperationContract]
        IList<Member> GetAllMemberByJobPostingId(int jobPostingId);

        [OperationContract]
        IList<Member> GetAllMemberDetailsByInterviewId(int InterviewId, int MemberId);

        [OperationContract]
        IList<Member> GetAllMemberByCustomrRoleId(int customRoleId);

        [OperationContract]
        IList<Member> GetAllMemberByCreatorIdAndRoleName(int creatorId, string roleName);

        [OperationContract]
        IList<Member> GetAllMemberByMemberGroupId(int memberGroupId);

        [OperationContract]
        IList<Member> GetAllMemberGroupManagerByMemberGroupId(int memberGroupId);

        [OperationContract]
        PagedResponse<Member> GetPagedMember(PagedRequest request);

        [OperationContract]
        PagedResponse<Candidate> GetPagedMemberByMemberGroupId(PagedRequest request);

        [OperationContract]
        PagedResponse<Member> GetPagedMemberByCreatorIdAndRoleName(PagedRequest request);

        [OperationContract]
        bool DeleteMemberById(int id);

        [OperationContract]
        void UpdateAspNet_User(string UserName, Guid UserId);

        [OperationContract]
        ArrayList GetAllMemberNameByRoleName(string roleName);

        [OperationContract]
        ArrayList GetAllMemberNameWithEmailByRoleName(string roleName);
        //*****************Code added by pravin khot on 7/March/2016**************
        [OperationContract]
        ArrayList GetAllMemberNameWithEmailByRoleNameIfExisting(string roleName);
        //****************************END****************************************
        [OperationContract]
        IList<Member> GetAllMemberByIds(string Ids);

        [OperationContract]
        string GetNewMermberIdentityId();

        [OperationContract]
        Int32 GetMemberIdByMemberEmail(string Email);
        //Code introduced by Prasanth on 10/Jun/2016 Start
        [OperationContract]
        Member GetMemberByMemberUserName(string UserName);

        //*****************END*************************
        [OperationContract]
        Member GetMemberByMemberEmail(string PrimaryEmail);

        [OperationContract]
        Int32 GetMemberIdByEmail(string Email);

        [OperationContract]
        Int32 GetCreatorIdForMember(int id);

        [OperationContract]
        ArrayList GetAllEmployeeNameWithEmail(string role);
        [OperationContract]
        int GetMemberStatusByMemberId(int memberId);
        [OperationContract]
        int GetMemberIdByCompanyContact(int CompanyContactId);

        //----------------------code introduced by pravin khot 09/Dec/2015 Using Suggested Interviewer With Email-------------
        [OperationContract]
        ArrayList GetAllSuggestedInterviewerWithEmail(int RequisitionId);
        //----------------------end---------------------

        //----------------------code introduced by pravin khot 02/Dec/2015-------------
        [OperationContract]
        ArrayList GetAllMemberNameWithEmailByRoleName_IP(string roleName, int paneltypeid);
        //----------------------end---------------------

        //************Code added by pravin khot on 23/Feb/2016************
        [OperationContract]
        void DeleteRoleRequisitionDenial();

        [OperationContract]
        void UpdateRoleRequisitionDenial(int RoleId, int MemberId, int sectionid);

        [OperationContract]
        IList<CustomRole> GetCustomRoleRequisition(int sectionid);
        //**************************END***********************************

        //******************Code added by pravin khot on 16/May/2016***********
        [OperationContract]
        ArrayList GetAllMemberNameByTeamMemberId(string roleName, int memberid);
        //****************************END******************************

        #endregion

        #region MemberActivity

        [OperationContract]
        MemberActivity AddMemberActivity(MemberActivity memberActivity);

        [OperationContract]
        MemberActivity UpdateMemberActivity(MemberActivity memberActivity);

        [OperationContract]
        MemberActivity GetMemberActivityById(int id);


        [OperationContract]
        IList<MemberActivity> GetAllMemberActivity();

        [OperationContract]
        IList<MemberActivity> GetAllMemberActivityByCreatorId(int creatorId);

        //[OperationContract]
        //PagedResponse<MemberActivity> GetPagedMemberActivity(PagedRequest request);

        [OperationContract]
        PagedResponse<MemberActivity> GetPagedMemberActivity(PagedRequest pageRequest);

        [OperationContract]
        bool DeleteMemberActivityById(int id);

        #endregion

        #region MemberActivityType

        [OperationContract]
        MemberActivityType AddMemberActivityType(MemberActivityType memberActivityType);

        [OperationContract]
        MemberActivityType UpdateMemberActivityType(MemberActivityType memberActivityType);

        [OperationContract]
        MemberActivityType GetMemberActivityTypeById(int id);

        [OperationContract]
        MemberActivityType GetMemberActivityType(int ActivityType);


        [OperationContract]
        IList<MemberActivityType> GetAllMemberActivityType();

        [OperationContract]
        PagedResponse<MemberActivityType> GetPagedMemberActivityType(PagedRequest request);

        [OperationContract]
        bool DeleteMemberActivityTypeById(int id);

        #endregion

        #region MemberAlert

        [OperationContract]
        MemberAlert AddMemberAlert(MemberAlert memberAlert);

        [OperationContract]
        MemberAlert UpdateMemberAlert(MemberAlert memberAlert);

        [OperationContract]
        MemberAlert GetMemberAlertById(int id);

        [OperationContract]
        IList<MemberAlert> GetAllMemberAlert();

        [OperationContract]
        IList<MemberAlert> GetAllMemberAlertByMemberId(int memberId);

        [OperationContract]
        PagedResponse<MemberAlert> GetPagedMemberAlert(PagedRequest request);

        [OperationContract]
        bool DeleteMemberAlertById(int id);

        #endregion

        #region MemberAttendence

        [OperationContract]
        MemberAttendence AddMemberAttendence(MemberAttendence memberAttendence);

        [OperationContract]
        MemberAttendence UpdateMemberAttendence(MemberAttendence memberAttendence);

        [OperationContract]
        MemberAttendence GetMemberAttendenceById(int id);

        [OperationContract]
        MemberAttendence GetMemberAttendenceByAttendanceDateAndMemberId(DateTime attendanceDate, int memberId);

        [OperationContract]
        IList<MemberAttendence> GetAllMemberAttendence();

        [OperationContract]
        IList<MemberAttendence> GetAllMemberAttendenceByMemberId(int memberId);

        [OperationContract]
        bool DeleteMemberAttendenceById(int id);

        [OperationContract]
        IList<MemberAttendence> GetMemberAttendenceByYearAndMemberId(Int32 year, Int32 memberId);

        #endregion

        #region MemberAttribute

        [OperationContract]
        MemberAttribute AddMemberAttribute(MemberAttribute memberAttribute);

        [OperationContract]
        MemberAttribute UpdateMemberAttribute(MemberAttribute memberAttribute);

        [OperationContract]
        MemberAttribute GetMemberAttributeById(int id);

        [OperationContract]
        IList<MemberAttribute> GetAllMemberAttribute();

        [OperationContract]
        bool DeleteMemberAttributeById(int id);

        #endregion

        #region MemberAttendenceYearlyReport

        [OperationContract]
        IList<MemberAttendenceYearlyReport> GetMemberAttendenceByYearlyReport(Int32 year, Int32 memberId);

        #endregion

        #region MemberCapabilityRating

        [OperationContract]
        MemberCapabilityRating AddMemberCapabilityRating(MemberCapabilityRating memberCapabilityRating);

        [OperationContract]
        MemberCapabilityRating UpdateMemberCapabilityRating(MemberCapabilityRating memberCapabilityRating);

        [OperationContract]
        MemberCapabilityRating GetMemberCapabilityRatingById(int id);

        [OperationContract]
        IList<MemberCapabilityRating> GetAllMemberCapabilityRating();

        [OperationContract]
        bool DeleteMemberCapabilityRatingById(int id);

        [OperationContract]
        IList<MemberCapabilityRating> GetAllMemberCapabilityRatingByMemberIdAndPositionId(int memberId, int positionId);

        [OperationContract]
        bool DeleteMemberCapabilityRatingByMemberIdAndPositionId(int memberId, int positionId);

        [OperationContract]
        MemberCapabilityRating GetMemberCapabilityRatingByMemberIdAndPositionIdAndCapabilityTypeAndPositionCapabilityMapId(int memberId, int positionId, int capabilityType, int positionCapabilityMapId);

        #endregion

        #region MemberCertificationMap

        [OperationContract]
        MemberCertificationMap AddMemberCertificationMap(MemberCertificationMap memberCertificationMap);

        [OperationContract]
        MemberCertificationMap UpdateMemberCertificationMap(MemberCertificationMap memberCertificationMap);

        [OperationContract]
        MemberCertificationMap GetMemberCertificationMapById(int id);

        [OperationContract]
        IList<MemberCertificationMap> GetAllMemberCertificationMap();

        [OperationContract]
        IList<MemberCertificationMap> GetAllMemberCertificationMapByMemberId(int memberId);


        [OperationContract]
        bool DeleteMemberCertificationMapById(int id);


        [OperationContract]
        PagedResponse<MemberCertificationMap> GetPagedMemberCertificationMap(PagedRequest request);



        [OperationContract]
        PagedResponse<MemberCertificationMap> GetPagedMemberCertificationMapByMemberId(PagedRequest request);

        [OperationContract]
        MemberCertificationMap GetCertificationMapByCertificationName(string CertificationName);

        [OperationContract]
        bool DeleteMemberCertificationMapByMemberId(int MemberId);
        #endregion

        #region MemberChangeArchive

        [OperationContract]
        MemberChangeArchive AddMemberChangeArchive(MemberChangeArchive memberChangeArchive);

        [OperationContract]
        MemberChangeArchive UpdateMemberChangeArchive(MemberChangeArchive memberChangeArchive);

        [OperationContract]
        MemberChangeArchive GetMemberChangeArchiveById(int id);

        [OperationContract]
        IList<MemberChangeArchive> GetAllMemberChangeArchiveByMemberId(int memberId);

        [OperationContract]
        IList<MemberChangeArchive> GetAllMemberChangeArchive();

        [OperationContract]
        bool DeleteMemberChangeArchiveById(int id);

        #endregion

        #region MemberCustomRoleMap

        [OperationContract]
        MemberCustomRoleMap AddMemberCustomRoleMap(MemberCustomRoleMap memberCustomRoleMap);

        [OperationContract]
        MemberCustomRoleMap UpdateMemberCustomRoleMap(MemberCustomRoleMap memberCustomRoleMap);

        [OperationContract]
        MemberCustomRoleMap GetMemberCustomRoleMapById(int id);

        [OperationContract]
        MemberCustomRoleMap GetMemberCustomRoleMapByMemberId(int memberId);

        [OperationContract]
        IList<MemberCustomRoleMap> GetAllMemberCustomRoleMap();

        [OperationContract]
        bool DeleteMemberCustomRoleMapById(int id);

        #endregion

        #region MemberPrivilege

        [OperationContract]
        MemberPrivilege AddMemberPrivilege(MemberPrivilege memberPrivilege);

        [OperationContract]
        MemberPrivilege UpdateMemberPrivilege(MemberPrivilege memberPrivilege);

        [OperationContract]
        MemberPrivilege GetMemberPrivilegeById(int id);

        [OperationContract]
        ArrayList GetAllMemberPrivilegeIdsByMemberId(int memberId);

        [OperationContract]
        IList<MemberPrivilege> GetAllMemberPrivilegeByMemberId(int memberId);

        [OperationContract]
        IList<MemberPrivilege> GetAllMemberPrivilege();

        [OperationContract]
        bool DeleteMemberPrivilegeById(int id);

        [OperationContract]
        bool DeleteMemberPrivilegeByMemberId(int memberId);

        [OperationContract]
        bool GetMemberPrivilegeForMemberIdAndRequiredURL(int memberId, string Url);

        [OperationContract]
        bool DeleteByMemberIdWithOutAdminAccess(int memberId);

        [OperationContract]
        void DeleteAllMemberPrivilige();
        #endregion

        #region MemberDailyReport

        [OperationContract]
        MemberDailyReport AddMemberDailyReport(MemberDailyReport memberDailyReport);

        [OperationContract]
        MemberDailyReport UpdateMemberDailyReport(MemberDailyReport memberDailyReport);

        [OperationContract]
        MemberDailyReport GetMemberDailyReportById(int id);

        [OperationContract]
        MemberDailyReport GetMemberDailyReportByLoginTimeAndMemberId(DateTime loginTime, int memberId);

        [OperationContract]
        IList<MemberDailyReport> GetAllMemberDailyReport();

        [OperationContract]
        PagedResponse<MemberDailyReport> GetPagedMemberDailyReport(PagedRequest request);


        [OperationContract]
        bool DeleteMemberDailyReportById(int id);

        [OperationContract]
        bool UpdateAllMemberDailyReportByLoginTimeAndMemberId(DateTime loginTime, int memberId);

        #endregion

        #region MemberDetail

        [OperationContract]
        MemberDetail AddMemberDetail(MemberDetail memberDetail);

        [OperationContract]
        MemberDetail UpdateMemberDetail(MemberDetail memberDetail);

        [OperationContract]
        MemberDetail GetMemberDetailById(int id);

        //starts 0.15
        //[OperationContract]
        //MemberDetail GetMemberSSNCount(string strSSN);
        [OperationContract]
        int GetMemberSSNCount(string strSSN, int MemberId);
        //ends 0.15

        [OperationContract]
        MemberDetail GetMemberDetailByMemberId(int memberId);

        [OperationContract]
        IList<MemberDetail> GetAllMemberDetail();

        [OperationContract]
        bool DeleteMemberDetailById(int id);


        [OperationContract]
        bool DeleteMemberDetailByMemberId(int MemberId);


        [OperationContract]
        bool BuildMemberManager_ByMemberId(int MemberId, int CreatorId);
        #endregion

        #region MemberDocument

        [OperationContract]
        MemberDocument AddMemberDocument(MemberDocument memberDocument);

        [OperationContract]
        MemberDocument UpdateMemberDocument(MemberDocument memberDocument);

        [OperationContract]
        MemberDocument GetMemberDocumentById(int id);

        [OperationContract]
        IList<MemberDocument> GetAllMemberDocumentByMemberId(int memberId);

        [OperationContract]
        IList<MemberDocument> GetAllMemberDocumentByTypeAndMemberId(string type, int memberId);

        [OperationContract]
        IList<MemberDocument> GetAllMemberDocument();

        [OperationContract]
        bool DeleteMemberDocumentById(int id);

        [OperationContract]
        IList<MemberDocument> GetAllMemberDocumentByTypeAndMembersId(string membersId, string documentType);


        [OperationContract]
        MemberDocument GetMemberDocumentByMemberIdTypeAndFileName(int memberId, string Type, string fileName);

        [OperationContract]
        MemberDocument GetMemberDocumentByMemberIdAndFileName(int memberId, string fileName);

        [OperationContract]
        bool DeleteMemberDocumentByMemberId(int MemberId);

        [OperationContract]
        PagedResponse<MemberDocument> GetPagedMemberDocumentByMemberID(int MemberId, PagedRequest request);

        [OperationContract]
        IList<MemberDocument> GetLatestResumeByMemberID(int memberId);


        [OperationContract]
        MemberDocument GetRecentResumeByMemberID(int memberId);

        #endregion

        #region Member Education

        [OperationContract]
        MemberEducation AddMemberEducation(MemberEducation memberEducation);

        [OperationContract]
        MemberEducation UpdateMemberEducation(MemberEducation memberEducation);

        [OperationContract]
        MemberEducation GetMemberEducationById(int id);

        [OperationContract]
        MemberEducation GetMemberEducationByMemberId(int memberId);

        [OperationContract]
        MemberEducation GetHighestEducationByMemberId(int memberId);

        //evan
        [OperationContract]
        string GetLevelofEducationByLookupId(int lookupId);

        [OperationContract]
        IList<MemberEducation> GetAllMemberEducation();

        [OperationContract]
        IList<MemberEducation> GetAllMemberEducationByMemberId(int memberId);

        [OperationContract]
        PagedResponse<MemberEducation> GetPagedMemberEducation(PagedRequest request);

        [OperationContract]
        PagedResponse<MemberEducation> GetPagedMemberEducationByMemberId(PagedRequest request);

        [OperationContract]
        bool DeleteMemberEducationById(int id);

        bool DeleteMemberEducationByMemberId(int id);

        #endregion

        #region MemberEmail

        [OperationContract]
        MemberEmail AddMemberEmail(MemberEmail memberEmail);

        [OperationContract]
        MemberEmail UpdateMemberEmail(MemberEmail memberEmail);

        [OperationContract]
        MemberEmail GetMemberEmailById(int id);

        [OperationContract]
        MemberEmail GetMemberEmailSenderByMemberIdSubjectDate(Int32 memberId, string subject, DateTime date);

        [OperationContract]
        MemberEmail GetMemberEmailReceiverByMemberIdSubjectDate(Int32 memberId, string subject, DateTime date);

        [OperationContract]
        IList<MemberEmail> GetAllMemberEmail();

        [OperationContract]
        IList<MemberEmail> Synchronise(string fromdate, string todate, string subject, string toemail, string uid);

        [OperationContract]
        IList<MemberEmail> GetByEmailAddress(string emailAddress);

        [OperationContract]
        bool ValidateSenderEmailID(string PrimaryEmail);

        [OperationContract]
        IList<MemberEmail> GetByEmailAddressAndCreatedDate(string emailAddress, string createdDate);

        [OperationContract]
        PagedResponse<MemberEmail> GetPagedMemberEmail(PagedRequest request);

        [OperationContract]
        bool DeleteMemberEmailById(int id);

        [OperationContract]
        string GetMemberEmailTypeById(int id);

        #endregion

        #region MemberEmailAttachment

        [OperationContract]
        MemberEmailAttachment AddMemberEmailAttachment(MemberEmailAttachment memberEmailAttachment);

        [OperationContract]
        MemberEmailAttachment UpdateMemberEmailAttachment(MemberEmailAttachment memberEmailAttachment);

        [OperationContract]
        MemberEmailAttachment GetMemberEmailAttachmentById(int id);

        [OperationContract]
        IList<MemberEmailAttachment> GetAllMemberEmailAttachmentByMemberEmailId(int memberEmailId);

        [OperationContract]
        IList<MemberEmailAttachment> GetAllMemberEmailAttachment();

        [OperationContract]
        bool DeleteMemberEmailAttachmentById(int id);

        [OperationContract]
        bool DeleteMemberEmailAttachmentByMemberEmailId(int memberEmailId);

        #endregion

        #region MemberEmailDetail

        [OperationContract]
        MemberEmailDetail AddMemberEmailDetail(MemberEmailDetail memberEmailDetail);

        [OperationContract]
        MemberEmailDetail UpdateMemberEmailDetail(MemberEmailDetail memberEmailDetail);

        [OperationContract]
        MemberEmailDetail GetMemberEmailDetailById(int id);

        [OperationContract]
        IList<MemberEmailDetail> GetAllMemberEmailDetailByMemberEmailId(int memberEmailId);

        [OperationContract]
        IList<MemberEmailDetail> GetAllMemberEmailDetail();

        [OperationContract]
        bool DeleteMemberEmailDetailById(int id);

        [OperationContract]
        bool DeleteMemberEmailDetailByMemberEmailId(int memberEmailId);

        #endregion

        #region MemberExperience

        [OperationContract]
        MemberExperience AddMemberExperience(MemberExperience memberExperience);

        [OperationContract]
        MemberExperience UpdateMemberExperience(MemberExperience memberExperience);

        [OperationContract]
        MemberExperience GetMemberExperienceById(int id);

        [OperationContract]
        IList<MemberExperience> GetAllMemberExperience();

        [OperationContract]
        IList<MemberExperience> GetAllMemberExperienceByMemberId(int memberId);

        [OperationContract]
        PagedResponse<MemberExperience> GetPagedMemberExperience(PagedRequest request);

        [OperationContract]
        PagedResponse<MemberExperience> GetPagedMemberExperienceByMemberId(PagedRequest request);

        [OperationContract]
        bool DeleteMemberExperienceById(int id);


        [OperationContract]
        bool DeleteMemberExperienceByMemberId(int id);//0.21 

        #endregion

        #region MemberExperienceDetail

        [OperationContract]
        MemberExperienceDetail GetMemberExperienceDetailById(int id);

        [OperationContract]
        IList<MemberExperienceDetail> GetAllMemberExperienceDetailByMemberId(int memberId);

        [OperationContract]
        PagedResponse<MemberExperienceDetail> GetPagedMemberExperienceDetail(PagedRequest request);

        #endregion

        #region MemberExtendedInformation

        [OperationContract]
        MemberExtendedInformation AddMemberExtendedInformation(MemberExtendedInformation memberExtendedInformation);

        [OperationContract]
        MemberExtendedInformation UpdateMemberExtendedInformation(MemberExtendedInformation memberExtendedInformation);

        [OperationContract]
        MemberExtendedInformation GetMemberExtendedInformationById(int id);

        [OperationContract]
        MemberExtendedInformation GetMemberExtendedInformationByMemberId(int id);

        [OperationContract]
        IList<MemberExtendedInformation> GetAllMemberExtendedInformation();

        [OperationContract]
        bool DeleteMemberExtendedInformationById(int id);

        [OperationContract]
        bool DeleteMemberExtendedInformationByMemberId(int MemberId);

        [OperationContract]
        void UpdateUpdateDateByMemberId(int MemberId);

        [OperationContract]
        void UpdateMemberManagerIsPrimary(int candidateid, int AssignManagerId); //added by pravin khot on 24/May/2016

        #endregion

        #region MemberGroup

        [OperationContract]
        MemberGroup AddMemberGroup(MemberGroup memberGroup);

        [OperationContract]
        MemberGroup UpdateMemberGroup(MemberGroup memberGroup);

        [OperationContract]
        MemberGroup GetMemberGroupById(int id);

        [OperationContract]
        IList<MemberGroup> GetAllMemberGroup();

        [OperationContract]
        IList<MemberGroup> GetAllMemberGroup(int groupType);

        [OperationContract]
        IList<MemberGroup> GetAllMemberGroupByMemberId(int memberId);

        [OperationContract]
        PagedResponse<MemberGroup> GetPagedMemberGroup(PagedRequest request);

        [OperationContract]
        PagedResponse<MemberGroup> GetPagedMemberGroupByMemberId(PagedRequest request);

        [OperationContract]
        PagedResponse<MemberGroup> GetPagedMemberGroupByMemberIdandGroupType(PagedRequest request);

        [OperationContract]
        bool DeleteMemberGroupById(int id);

        [OperationContract]
        ArrayList GetAllMemberGroupNameByManagerId(int managerId);

        [OperationContract]
        ArrayList GetAllMemberGroupNameByManagerId(int managerId, int groupType);
        [OperationContract]
        int GetMemberGroupCountByManagerIDAndGroupType(int ManagerID, int groupType);

        [OperationContract]
        int GetMemberGroupCountByGroupType(int groupType);

        [OperationContract]
        MemberGroup GetMemberGroupByName(string name);

        [OperationContract]
        bool GetMemberGroupAccessByCreatorIdAndGroupId(int CreatorId, int GroupId);

        #endregion

        #region MemberGroupManager

        [OperationContract]
        MemberGroupManager AddMemberGroupManager(MemberGroupManager memberGroupManager);

        [OperationContract]
        MemberGroupManager GetMemberGroupManagerById(int id);

        [OperationContract]
        IList<MemberGroupManager> GetAllMemberGroupManagerByMemberId(int memberId);

        [OperationContract]
        IList<MemberGroupManager> GetAllMemberGroupManager();

        [OperationContract]
        int GetTotalMemberByMemberGroupId(int memberGroupId);

        [OperationContract]
        bool DeleteMemberGroupManagerById(int id);

        [OperationContract]
        bool DeleteMemberGroupManagerByMemberGroupIdAndMemberId(int memberGroupId, int memberId);

        #endregion

        #region MemberGroupMap

        [OperationContract]
        MemberGroupMap AddMemberGroupMap(MemberGroupMap memberGroupMap);

        [OperationContract]
        MemberGroupMap UpdateMemberGroupMap(MemberGroupMap memberGroupMap);

        [OperationContract]
        MemberGroupMap GetMemberGroupMapById(int id);

        [OperationContract]
        MemberGroupMap GetMemberGroupMapByMemberIdandMemberGroupId(int memberId, int memberGroupId);

        [OperationContract]
        IList<MemberGroupMap> GetAllMemberGroupMapByMemberId(int memberId);

        [OperationContract]
        IList<MemberGroupMap> GetAllMemberGroupMap();

        [OperationContract]
        bool DeleteMemberGroupMapById(int id);

        [OperationContract]
        bool DeleteMemberGroupMapByIdandMemberGroupId(int id, int memberGroupId);


        [OperationContract]
        PagedResponse<MemberGroupMap> MembetGroupMapGetPaged(PagedRequest request);
        #endregion

        #region MemberHiringDetails

        [OperationContract]
        void AddMemberHiringDetails(MemberHiringDetails memberHiringDetails, string MemberId);

        [OperationContract]
        MemberHiringDetails UpdateMemberHiringDetails(MemberHiringDetails memberHiringDetails);

        [OperationContract]
        MemberHiringDetails GetMemberHiringDetailsById(int id);

        [OperationContract]
        MemberHiringDetails GetMemberHiringDetailsByMemberIdAndJobPostingID(int memberID, int jobposintID);

        [OperationContract]
        bool DeleteMemberHiringDetailsByID(int Id);

        [OperationContract]
        PagedResponse<MemberHiringDetails> GetPaged(PagedRequest request);

        [OperationContract]
        PagedResponse<MemberHiringDetails> MemberHiringDetails_GetPagedEmbeddedDetails(PagedRequest reqest);

        [OperationContract]
        PagedResponse<MemberHiringDetails> MemberHiringDetails_GetPagedMechDetails(PagedRequest request);

        [OperationContract]
        PagedResponse<MemberOfferJoinDetails> MemberHiringDetails_GetPagedOfferJoinDetails(PagedRequest request);
        [OperationContract]
        PagedResponse<MemberOfferJoinDetails> MemberHiringDetails_GetPagedOfferJoinDetailsForCommon(PagedRequest request);

        [OperationContract]
        MemberHiringDetails GetSalaryComponentsByJobpostingIdAndMemberId(int jobpostingId, int MemberId);

        [OperationContract]
        MemberHiringDetails GetOfferDetailsByJobpostingIdAndMemberId(int jobpostingId, int MemberId);

        [OperationContract]
        IList<MemberHiringDetails> GetAllManagementBand();

        [OperationContract]
        IList<MemberHiringDetails> GetAllLocation();

        [OperationContract]
        IList<MemberHiringDetails> GetAllManagementBandByManagementBandId(int ManagementBandId);

        [OperationContract]
        IList<MemberHiringDetails> GetAllBand();

        #endregion

        #region MemberHiringProcess

        [OperationContract]
        MemberHiringProcess AddMemberHiringProcess(MemberHiringProcess memberHiringProcess);

        [OperationContract]
        MemberHiringProcess UpdateMemberHiringProcess(MemberHiringProcess memberHiringProcess);

        [OperationContract]
        MemberHiringProcess GetMemberHiringProcessById(int id);

        [OperationContract]
        IList<MemberHiringProcess> GetAllMemberHiringProcess();

        [OperationContract]
        bool DeleteMemberHiringProcessById(int id);

        [OperationContract]
        IList<MemberHiringProcess> GetMemberHiringProcessByMemberIdJobPostingId(int memberId, int jobPostingId);

        [OperationContract]
        MemberHiringProcess GetMemberHiringProcessByMemberIdJobPostingIdAndInterviewLevelId(int memberId, int jobPostingId, int interviewlevelid);

        #endregion

        #region MemberJobApplied
        [OperationContract]
        PagedResponse<MemberJobApplied> MemberJobapplied_GetPaged(PagedRequest request);
        [OperationContract]
        MemberJobApplied AddMemberJobApplied(MemberJobApplied jobApplied);
        [OperationContract]
        MemberJobApplied MemberJobApplied_GetByMemberIDANDJobPostingID(int memberid, int jobpostingid);
        #endregion

        #region MemberJobCart

        [OperationContract]
        MemberJobCart AddMemberJobCart(MemberJobCart memberJobCart);

        [OperationContract]
        MemberJobCart UpdateMemberJobCart(MemberJobCart memberJobCart);

        [OperationContract]
        MemberJobCart GetMemberJobCartById(int id);

        [OperationContract]
        IList<MemberJobCart> GetAllMemberJobCartByMemberId(int memberId);

        [OperationContract]
        IList<MemberJobCart> GetAllMemberJobCartByMemberId(int memberId, string SortExpression); // Defect id 8841

        [OperationContract]
        IList<MemberJobCart> GetAllMemberJobCart();

        [OperationContract]
        bool DeleteMemberJobCartById(int id);

        [OperationContract]
        bool RejectToUnRejectCandidateStatusChange(string MemberIds, int JobPostingId); //added by pravin khot on 24/June/2016

        [OperationContract]
        bool RejectMemberJobCartById(int id); //added by pravin khot on 24/June/2016
        // ************code added by pravin khot on 25/Jan/2016*****************
        [OperationContract]
        int MemberCareerPortalToRequisition_Add(string CanIds);
        //*************************End************************************************
        [OperationContract]
        PagedResponse<MemberJobCart> GetPagedMemberJobCart(PagedRequest request);

        [OperationContract]
        MemberJobCart GetMemberJobCartByMemberIdAndJobPostringId(int memberId, int jobpostingId);
		
		//Candidate Status in Employee Referral Portal.
		[OperationContract]
		PagedResponse<MemberJobCart> GetPagedMemberJobCartForEmployeeReferal(PagedRequest request);
        //Candidate Status in Employee Referral Portal.
		
        [OperationContract]
        IList<MemberJobCart> GetAllMemberJobCartByByJobPostingIdAndSelectionStep(int jobPostingId, int selectionStep);

        [OperationContract]
        int[] GetHiringMatrixMemberCount(int jobPostingId);

        [OperationContract]
        int[] GetHiringMatrixMemberCount(int jobPostingId, string dateWhenCountTobeDone);

        IList<MemberJobCart> GetAllMemberJobCartByMemberIdAndSelectionStep(int memberId, int selectionStep);

        //0.11 start
        IList<MemberJobCart> GetAllSubmittedCandidateInterviewScore(int memberId, int selectionStep, string sortExpression);
        //0.11 End
        [OperationContract]
        void MemberJobCart_MoveToNextLevel(int CurrentLevel, int UpdatorId, string MemberIds, int JobPostingID, string MovingDirection);

        int MemberJobCart_AddCandidateToRequisition(int CreatorId, string MemberIds, int JobPostingId);

        int MemberJobCart_AddCandidateToMultipleRequisition(int CreatorId, string MemberIds, int JobPostingId);//line added by pravin khot on 27/April/2016

        void UpdateByStatus(int RequisitionId, string MemberId, int StatusId, int UpdatorId);
        [OperationContract]
        void UpdateMemberJobCartByMemberIdAndJobPostingID(int MemberId, int JobPostingId, int sourceID);

        [OperationContract]
        IList<int> CheckCandidatesInHiringMatrix(string CandidateIds, int JobpostingId);

        string GetRecruiternameByMemberId(int memberId);

        string GetCandidateStatusByMemberIdAndJobPostingId(int Memberid, int JobPostingId);

        int GetActiveRequsiutionCountByMemberId(int memberId);
        #endregion

        #region MemberJobCartAlert

        [OperationContract]
        MemberJobCartAlert AddMemberJobCartAlert(MemberJobCartAlert memberJobCartAlert);

        [OperationContract]
        MemberJobCartAlert UpdateMemberJobCartAlert(MemberJobCartAlert memberJobCartAlert);

        [OperationContract]
        MemberJobCartAlert GetMemberJobCartAlertById(int id);

        [OperationContract]
        IList<MemberJobCartAlert> GetAllMemberJobCartAlert();

        [OperationContract]
        IList<MemberJobCartAlert> GetAllByMemberJobCartAlertSetupId(int memberJobCartAlertSetupId);

        [OperationContract]
        IList<MemberJobCartAlert> GetAllByJobPostingId(int jobPostingId);

        [OperationContract]
        bool DeleteMemberJobCartAlertById(int id);

        [OperationContract]
        bool RemoveAllByMemberJobCartAlertSetupId(int memberJobCartAlertSetupId);

        [OperationContract]
        bool DeleteAllByMemberJobCartAlertSetupId(int memberJobCartAlertSetupId);

        #endregion

        #region MemberJobCartDetail

        [OperationContract]
        MemberJobCartDetail AddMemberJobCartDetail(MemberJobCartDetail memberJobCartDetail);

        [OperationContract]
        MemberJobCartDetail UpdateMemberJobCartDetail(MemberJobCartDetail memberJobCartDetail);

        [OperationContract]
        MemberJobCartDetail GetMemberJobCartDetailById(int id);

        [OperationContract]
        IList<MemberJobCartDetail> GetAllMemberJobCartDetail();

        [OperationContract]
        bool DeleteMemberJobCartDetailById(int id);

        [OperationContract]
        bool DeleteAllMemberJobCartDetailByJobCartId(int memberJobCartId);

        [OperationContract]
        bool RemoveAllMemberJobCartDetailByJobCartId(int memberJobCartId);

        [OperationContract]
        IList<MemberJobCartDetail> GetAllMemberJobCartDetailByMemberJobCartId(int memberJobCartId);

        [OperationContract]
        MemberJobCartDetail GetMemberJobCartDetailByMemberJobCartIdAndSelectionStepId(int memberJobCartId, int selectionStepId);

        [OperationContract]
        MemberJobCartDetail GetMemberJobCartDetailCurrentByMemberJobCartId(int memberJobCartId);

        #endregion

        #region MemberJobCartAlertSetup

        [OperationContract]
        MemberJobCartAlertSetup AddMemberJobCartAlertSetup(MemberJobCartAlertSetup memberJobCartAlertSetup);

        [OperationContract]
        MemberJobCartAlertSetup UpdateMemberJobCartAlertSetup(MemberJobCartAlertSetup memberJobCartAlertSetup);

        [OperationContract]
        MemberJobCartAlertSetup GetMemberJobCartAlertSetupById(int id);

        [OperationContract]
        MemberJobCartAlertSetup GetMemberJobCartAlertSetupByMemberId(int memberId);

        [OperationContract]
        IList<MemberJobCartAlertSetup> GetAllMemberJobCartAlertSetupByMemberId(int memberId);

        [OperationContract]
        IList<MemberJobCartAlertSetup> GetAllMemberJobCartAlertSetup();

        [OperationContract]
        bool DeleteMemberJobCartAlertSetupById(int id);

        [OperationContract]
        bool DeleteMemberJobCartAlertSetupByMemberId(int memberId);

        #endregion

        #region MemberJoiningDetails

        [OperationContract]
        void AddMemberJoiningDetails(MemberJoiningDetail memberJoiningDetails, string MemberId);

        [OperationContract]
        MemberJoiningDetail GetMemberJoiningDetailsById(int id);

        [OperationContract]
        MemberJoiningDetail GetMemberJoiningDetailsByMemberIdAndJobPostingID(int memberID, int jobposintID);

        [OperationContract]
        bool DeleteMemberJoiningDetailsByID(int Id);

        #endregion

        #region MemberManager

        [OperationContract]
        MemberManager AddMemberManager(MemberManager memberManager);

        [OperationContract]
        MemberManager UpdateMemberManager(MemberManager memberManager);

        [OperationContract]
        MemberManager GetMemberManagerById(int id);

        [OperationContract]
        MemberManager GetMemberManagerByMemberIdAndManagerId(int memberId, int managerId);

        [OperationContract]
        MemberManager GetMemberManagerPrimaryByMemberId(int memberId);

        [OperationContract]
        IList<MemberManager> GetAllMemberManagerByMemberId(int memberId);

        [OperationContract]
        IList<MemberManager> GetAllMemberManager();

        [OperationContract]
        bool DeleteMemberManagerById(int id);

        [OperationContract]
        bool DeleteMemberManagerByMemberIdAndManagerId(int memberId, int managerId);

        [OperationContract]
        bool DeleteAllMemberManagerByMemberId(int memberId);


        [OperationContract]
        ArrayList GetMemberManagerListByMemberId(int memberId);

        [OperationContract]
        PagedResponse<Member> GetPagedMemberManagerByMemberID(int MemberId, PagedRequest request);

        [OperationContract]
        string getMemberManager_PrimaryManagerName(int MemberId);



        #endregion

        #region MemberInterview

        [OperationContract]
        MemberInterview GetMemberInterviewById(int id);

        [OperationContract]
        IList<MemberInterview> GetAllMemberInterviewByMemberId(int memberId);

        // Defect id : 10254
        [OperationContract]
        IList<MemberInterview> GetAllMemberInterviewByMemberId(int memberId, string sortExpression);

        [OperationContract]
        PagedResponse<MemberInterview> GetPagedMemberInterview(PagedRequest request);

        [OperationContract]
        PagedResponse<MemberInterview> GetPagedMemberInterviewForDashboard(PagedRequest request);

        [OperationContract]
        PagedResponse<MemberInterview> GetPagedInterviewReport(PagedRequest request);

        //*************************Code added by pravin khot on 15/Dec/2015**************

        bool MemberInterview_SuggestedInterviewerId(int interviewId, int InterviewerId);
        //*******************************end**********************************************
        #endregion

        #region MemberNote

        [OperationContract]
        MemberNote AddMemberNote(MemberNote memberNote);

        [OperationContract]
        MemberNote GetMemberNoteById(int id);

        [OperationContract]
        IList<MemberNote> GetAllMemberNote();

        [OperationContract]
        bool DeleteMemberNoteById(int id);

        [OperationContract]
        IList<MemberNote> GetAllMemberNoteByMemberId(int id);

        #endregion

        #region MemberObjectiveAndSummary

        [OperationContract]
        MemberObjectiveAndSummary AddMemberObjectiveAndSummary(MemberObjectiveAndSummary memberObjectiveAndSummary);

        [OperationContract]
        MemberObjectiveAndSummary UpdateMemberObjectiveAndSummary(MemberObjectiveAndSummary memberObjectiveAndSummary);

        [OperationContract]
        MemberObjectiveAndSummary GetMemberObjectiveAndSummaryById(int id);

        [OperationContract]
        MemberObjectiveAndSummary GetMemberObjectiveAndSummaryByMemberId(int memberId);

        [OperationContract]
        IList<MemberObjectiveAndSummary> GetAllMemberObjectiveAndSummary();

        [OperationContract]
        bool DeleteMemberObjectiveAndSummaryById(int id);

        #endregion

        #region MemberPedingJoiners

        [OperationContract]
        PagedResponse<MemberPendingJoiners> MemberPendingJoiners_GetPaged(PagedRequest request);
        #endregion

        #region MemberOfferRejection

        [OperationContract]
        void AddMemberOfferRejection(MemberOfferRejection offerRejection, string memberid);
        [OperationContract]
        MemberOfferRejection MemberOfferRejection_GetbyId(int id);
        [OperationContract]
        MemberOfferRejection MemberOfferRejection_GetbyMemberIdAndJobPostingID(int memberid, int JobPostingId);
        #endregion

        #region MemberReference

        [OperationContract]
        MemberReference AddMemberReference(MemberReference memberReference);

        [OperationContract]
        MemberReference UpdateMemberReference(MemberReference memberReference);

        [OperationContract]
        MemberReference GetMemberReferenceById(int id);

        [OperationContract]
        IList<MemberReference> GetAllMemberReferenceByMemberId(int memberId);

        //0.7 start
        [OperationContract]
        IList<MemberReference> GetAllMemberReferenceByMemberId(int memberId, string sortExpression);
        //0.7 End

        [OperationContract]
        IList<MemberReference> GetAllMemberReference();

        [OperationContract]
        PagedResponse<MemberReference> GetPagedMemberReference(PagedRequest request);

        [OperationContract]
        bool DeleteMemberReferenceById(int id);

        [OperationContract]
        string GetMemberReferenceEmailById(int id);

        #endregion

        #region MemberSignature

        [OperationContract]
        MemberSignature AddMemberSignature(MemberSignature memberSignature);

        [OperationContract]
        MemberSignature UpdateMemberSignature(MemberSignature memberSignature);

        [OperationContract]
        IList<MemberSignature> GetAllMemberSignatureByMemberId(int memberId);

        [OperationContract]
        MemberSignature GetMemberSignatureById(int id);

        [OperationContract]
        MemberSignature GetActiveMemberSignatureByMemberId(int memberId);

        [OperationContract]
        IList<MemberSignature> GetAllMemberSignature();

        [OperationContract]
        bool DeleteMemberSignatureById(int id);

        [OperationContract]
        bool DeleteMemberSignatureByMemberId(int memberId);

        #endregion

        #region MemberSkillMap

        [OperationContract]
        MemberSkillMap AddMemberSkillMap(MemberSkillMap memberSkillMap);

        [OperationContract]
        MemberSkillMap UpdateMemberSkillMap(MemberSkillMap memberSkillMap);

        [OperationContract]
        MemberSkillMap GetMemberSkillMapById(int id);

        [OperationContract]
        IList<MemberSkillMap> GetAllMemberSkillMap();

        [OperationContract]
        IList<MemberSkillMap> GetAllMemberSkillMapByMemberId(int memberId);

        [OperationContract]
        MemberSkillMap GetMemberSkillMapByMemberIdAndSkillId(int memberId, int skillId);

        [OperationContract]
        bool DeleteMemberSkillMapById(int id);

        [OperationContract]
        bool DeleteMemberSkillMapByIds(string id);

        [OperationContract]
        PagedResponse<MemberSkillMap> GetPagedMemberSkillMap(PagedRequest request);
        [OperationContract]
        PagedResponse<MemberSkillMap> GetPagedMemberSkillMapByMemberId(PagedRequest request);

        [OperationContract]
        bool DeleteMemberSkillMapByMemberId(int MemberId);


        [OperationContract]
        string GetMemberSkillNamesByMemberId(int Memberid);
        #endregion

        #region MemberSkill

        [OperationContract]
        IList<MemberSkill> GetAllMemberSkillByMemberId(int memberId);

        [OperationContract]
        IList<MemberSkill> GetAllMemberSkillByMemberId(int memberId, string sortExpression);//0.8
        #endregion

        #region MemberSubmission

        [OperationContract]
        void AddMemberSubmission(MemberSubmission memberSubmission, string MemberId);

        [OperationContract]
        MemberSubmission UpdateMemberSubmission(MemberSubmission memberSubmission);

        [OperationContract]
        MemberSubmission GetMemberSubmissionsByMemberIDAndJobPostingId(int memberid, int jobpostingid);
        #endregion

        #region MemberSkillSet

        [OperationContract]
        MemberSkillSet AddMemberSkillSet(MemberSkillSet memberSkillSet);

        [OperationContract]
        MemberSkillSet UpdateMemberSkillSet(MemberSkillSet memberSkillSet);

        [OperationContract]
        MemberSkillSet GetMemberSkillSetById(int id);

        [OperationContract]
        IList<MemberSkillSet> GetAllMemberSkillSet();

        [OperationContract]
        bool DeleteMemberSkillSetById(int id);

        #endregion

        #region MemberSourceHistory

        [OperationContract]
        MemberSourceHistory AddMemberSourceHistory(MemberSourceHistory memberSkillSet);

        [OperationContract]
        MemberSourceHistory UpdateMemberSourceHistory(MemberSourceHistory memberSkillSet);

        [OperationContract]
        MemberSourceHistory GetMemberSourceHistoryById(int id);

        [OperationContract]
        IList<MemberSourceHistory> GetAllMemberSourceHistory();

        [OperationContract]
        bool IsMemberCandidateSourceExpired(string primaryEmail);

        [OperationContract]
        bool DeleteMemberSourceHistoryById(int id);
        [OperationContract]
        PagedResponse<MemberSourceHistory> GetPagedMemberSourceHistory(PagedRequest request);

        #endregion

        #region OccupationalSeries

        [OperationContract]
        IList<OccupationalSeries> GetAllOccupationalSeries();


        [OperationContract]
        IList<OccupationalSeries> GetAllOccupationalSeriesBySearch(string keyword, int count);
        [OperationContract]
        OccupationalSeries GetOccupationalSeriesById(int id);


        #endregion

        #region OccupationalGroup

        [OperationContract]
        IList<OccupationalGroup> GetAllOccupationalGroup();

        [OperationContract]
        IList<OccupationalGroup> GetAllOccupationalGroupBySearch(string keyword, int count);


        #endregion

        #region PasswordReset

        [OperationContract]
        PasswordReset AddPasswordReset(PasswordReset passwordreset);

        [OperationContract]
        PasswordReset GetPasswordResetById(int id);

        [OperationContract]
        PasswordReset UpdatePasswordReset(PasswordReset passwordreset);

        #endregion

        #region Recurrence

        [OperationContract]
        Recurrence AddRecurrence(Recurrence recurrence);

        [OperationContract]
        Recurrence UpdateRecurrence(Recurrence recurrence);

        [OperationContract]
        Recurrence GetRecurrenceByRecurrenceID(int recurrenceID);

        [OperationContract]
        IList<Recurrence> GetAllRecurrence();

        [OperationContract]
        bool DeleteRecurrenceByRecurrenceID(int recurrenceID);

        #endregion

        #region RejectCandidate

        [OperationContract]
        void AddRejectCandidate(RejectCandidate RejectCandidate, string MemberId);

        [OperationContract]
        void UpdateRejectCandidate(RejectCandidate RejectCandidate);

        [OperationContract]
        RejectCandidate GetRejectCandidateById(int id);

        [OperationContract]
        IList<RejectCandidate> GetRejectCandidateByMemberId(int memberId);

        [OperationContract]
        bool DeleteRejectCandidateByMemberId(int id);


        [OperationContract]
        RejectCandidate GetRejectCandidateByMemberIdAndJobPostingID(int memberId, int JobPostingID);

        [OperationContract]
        bool DeleteRejectCandidateByMemberIdAndJobPostingID(int MemberId, int JobPostingID);

        [OperationContract]
        PagedResponse<RejectCandidate> GetPagedForRejectCandidate(PagedRequest request);

        [OperationContract]
        int GetRejectCandidateCount(int JobPostingId);

        //************************Code added by pravin khot on 14/March/2016**********************************
        [OperationContract]
        PagedResponse<RejectCandidate> GetPagedCandidateReject(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, 
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManager, string JobPostingId);

        [OperationContract]
        PagedResponse<DynamicDictionary> GetPagedRejectCandidateForSelectedColumn(PagedRequest request, DateTime addedFrom, DateTime addedTo, int addedBy, int addedBySource, 
            int country, int state, string city, DateTime interviewFrom, DateTime interviewTo, int interviewLevel, int interviewStatus, int industry, int functionalCategory, int workPermit, int WorkSchedule,
            int gender, int maritalStatus, int educationId, int assessmentId, bool isBroadcastedResume, bool isReferredApplicants, bool hasJobAgent, int assignedManagerm, string JobPostingId, IList<string> CheckedList);

        //***********************************END********************************************
        #endregion

        #region Resource

        [OperationContract]
        Resource AddResource(Resource resource);

        [OperationContract]
        Resource UpdateResource(Resource resource);

        [OperationContract]
        Resource GetResourceByResourceID(int resourceID);

        [OperationContract]
        IList<Resource> GetAllResource();

        [OperationContract]
        bool DeleteResourceByResourceID(int resourceID);

        [OperationContract]
        Resource GetResourceByResourceName(string resourceName);

        #endregion

        #region SavedQuery

        [OperationContract]
        SavedQuery AddSavedQuery(SavedQuery savedQuery);

        [OperationContract]
        SavedQuery UpdateSavedQuery(SavedQuery savedQuery);

        [OperationContract]
        SavedQuery GetSavedQueryById(int id);

        [OperationContract]
        IList<SavedQuery> GetAllSavedQuery();

        [OperationContract]
        bool DeleteSavedQueryById(int id);

        [OperationContract]
        PagedResponse<SavedQuery> GetPagedSavedQuery(PagedRequest request);

        #endregion

        #region SearchAgentEmailTemplate

        [OperationContract]
        SearchAgentEmailTemplate AddSearchAgentEmailTemplate(SearchAgentEmailTemplate template);

        [OperationContract]
        SearchAgentEmailTemplate UpdateSearchAgentEmailTemplate(SearchAgentEmailTemplate template);

        [OperationContract]
        SearchAgentEmailTemplate GetSearchAgentEmailTemplateById(int id);

        [OperationContract]
        SearchAgentEmailTemplate GetSearchAgentEmailTemplateByJobPostingId(int JobPostingId);

        [OperationContract]
        bool DeleteSearchAgentEmailTemplateByJobPostingId(int JobPostingId);
        #endregion

        #region SearchAgentSchedule

        [OperationContract]
        SearchAgentSchedule AddSearchAgentSchedule(SearchAgentSchedule SearchAgent);

        [OperationContract]
        SearchAgentSchedule UpdateSearchAgentSchedule(SearchAgentSchedule SearchAgent);

        [OperationContract]
        SearchAgentSchedule GetSearchAgentScheduleById(int id);

        [OperationContract]
        bool DeleteSearchAgentScheduleByJobPostingId(int JobPostingId);

        [OperationContract]
        SearchAgentSchedule GetSearchAgentScheduleByJobPostingId(int JobPostingId);

        [OperationContract]
        IList<SearchAgentSchedule> GetAllSearchAgentSchedule();

        [OperationContract]
        void AddCandidateForMail(SearchAgentMailToCandidate Candidate);

        #endregion

        #region SiteSetting

        [OperationContract]
        SiteSetting AddSiteSetting(SiteSetting siteSetting);

        [OperationContract]
        SiteSetting UpdateSiteSetting(SiteSetting siteSetting);

        [OperationContract]
        SiteSetting GetSiteSettingById(int id);

        [OperationContract]
        IList<SiteSetting> GetAllSiteSetting();

        [OperationContract]
        bool DeleteSiteSettingById(int id);

        [OperationContract]
        SiteSetting GetSiteSettingBySettingType(int settingType);

        #endregion

        #region Skill

        [OperationContract]
        Skill AddSkill(Skill skill);

        [OperationContract]
        Skill UpdateSkill(Skill skill);

        [OperationContract]
        Skill GetSkillById(int id);

        [OperationContract]
        IList<Skill> GetAllSkill();

        [OperationContract]
        IList<Skill> GetAllParentSkill();

        [OperationContract]
        PagedResponse<Skill> GetPagedSkill(PagedRequest request);

        [OperationContract]
        bool DeleteSkillById(int id);

        [OperationContract]
        IList<Skill> GetAllSkillByParentId(int parentId, string searchSkill);

        [OperationContract]
        Int32 GetSkillIdBySkillName(string skill);

        [OperationContract]
        IList<Skill> GetAllSkillsBySearch(string keyword, int count);

        [OperationContract]
        string Skill_GetandAddSkillIds(string skillxml, int CreatorId);
        #endregion

        #region State

        [OperationContract]
        State AddState(State state);

        [OperationContract]
        State UpdateState(State state);

        [OperationContract]
        State GetStateById(int id);

        [OperationContract]
        IList<State> GetAllState();

        [OperationContract]
        IList<State> GetAllStateByCountryId(int countryId);

        [OperationContract]
        bool DeleteStateById(int id);

        [OperationContract]
        Int32 GetStateIdByStateName(string stateName);

        [OperationContract]
        string GetStateNameById(int id);
        #endregion

        #region Zipcode
        [OperationContract]
        IList<String> GetAllCity(string prefixText);
        #endregion

        #region WebParserDomain

        [OperationContract]
        WebParserDomain AddWebParserDomain(WebParserDomain memberManager);

        [OperationContract]
        WebParserDomain UpdateWebParserDomain(WebParserDomain memberManager);

        [OperationContract]
        WebParserDomain GetWebParserDomainById(int id);

        [OperationContract]
        IList<WebParserDomain> GetAllWebParserDomain();

        [OperationContract]
        bool DeleteWebParserDomainById(int id);

        [OperationContract]
        DataTable GetAllWebParserDomainName();

        #endregion

        #region UserAccessApp
        [OperationContract]
        int EntryofUserAccessApp(string uname, string sessionId, string AppName, string DomainName, string MacId, int allowedusers, string RequestedIp);

        [OperationContract]
        int DeleteUserAccessApp(string uname, string AppName, string DomainName);

        [OperationContract]
        void UpdateUserAccessApp(string uname, string AppName, string DomainName);

        [OperationContract]
        void EntryUserAccessAppReset(string uname, string AppName, string DomainName);

        [OperationContract]
        int IsAvailableSessionId(string SessionId);

        [OperationContract]
        int InsertSessionTimeout(string unmae, int time, string SessionId, string AppName, string DomainName);

        [OperationContract]
        int GetSessionTimeOutValue(string unmae, string AppName, string DomainName);

        [OperationContract]
        void UpdateAspStateTempSessions(string uname, string sessionId, string AppName, string DomainName);

        [OperationContract]
        PagedResponse<CurrentSessions> CurrentSessions_GetPaged(PagedRequest request);
        #endregion

        //Code introduced by Prasanth on 24/Dec/2015 Start
        #region MemberInterviewFeedbackDocument

        [OperationContract]
        MemberDocument AddMemberInterviewFeedbackDocument(MemberDocument memberDocument);

        [OperationContract]
        MemberDocument UpdateMemberInterviewFeedbackDocument(MemberDocument memberDocument);

        //[OperationContract]
        //MemberDocument GetMemberInterviewFeedbackDocumentById(int id);

        //[OperationContract]
        //IList<MemberDocument> GetAllMemberInterviewFeedbackDocumentByMemberId(int memberId);

        //[OperationContract]
        //IList<MemberDocument> GetAllMemberInterviewFeedbackDocumentByTypeAndMemberId(string type, int memberId);

        //[OperationContract]
        //IList<MemberDocument> GetAllMemberInterviewFeedbackDocument();

        //[OperationContract]
        //bool DeleteMemberInterviewFeedbackDocumentById(int id);

        //[OperationContract]
        //IList<MemberDocument> GetAllMemberInterviewFeedbackDocumentByTypeAndMembersId(string membersId, string documentType);


        //[OperationContract]
        //MemberDocument GetMemberInterviewFeedbackDocumentByMemberIdTypeAndFileName(int memberId, string Type, string fileName);

        [OperationContract]
        MemberDocument GetAllByMemberIDInterviewIDInterviewerEmailDocumentTypeAndFileName(int memberId, int InterviewID, string InterviewerEmail, string type, string FileName);


        //[OperationContract]
        //MemberDocument GetMemberInterviewFeedbackDocumentByMemberIdAndFileName(int memberId, string fileName);

        //[OperationContract]
        //bool DeleteMemberInterviewFeedbackDocumentByMemberId(int MemberId);

        //[OperationContract]
        //PagedResponse<MemberDocument> GetPagedMemberInterviewFeedbackDocumentByMemberID(int MemberId, PagedRequest request);

        //[OperationContract]
        //IList<MemberDocument> GetLatestResumeByMemberID(int memberId);


        //[OperationContract]
        //MemberDocument GetRecentResumeByMemberID(int memberId);

        #endregion
        //****************END*****************************

        //********New region added by pravin khot on 23/Feb/2016******Start
        #region UserRoleMapEditor
        [OperationContract]
        UserRoleMapEditor AddUserRoleMapping(UserRoleMapEditor UserRoleMapping);

        [OperationContract]
        PagedResponse<UserRoleMapEditor> UserRoleMapEditor_GetPaged(PagedRequest request);

        [OperationContract]
        bool DeleteUserRoleMappingByid(int id);

        [OperationContract]
        UserRoleMapEditor UpdateUserRoleMapping(UserRoleMapEditor UserRoleMapping);

        [OperationContract]
        UserRoleMapEditor UserRoleMapEditor_GetAllByBUMappingID(int UserRoleMappingId);

        [OperationContract]
        ArrayList UserRoleMapEditor_GetUserListByBuid(int BUId, int jobpostingId);

        [OperationContract]
        ArrayList UserRoleMapEditor_GetUserNameListByBuid(int BUId, int jobpostingId);

        Int32 UserRoleMap_Id(int RoleId);

        #endregion
        //*********************************END****************************************

        //*********start******************
        #region OnLineParser
        [OperationContract]
        OnLineParser OnLineParser_Add(OnLineParser OnLineParser);
        [OperationContract]
        OnLineParser OnLineParser_Update(OnLineParser OnLineParser);
        OnLineParser OnLineParser_GetById(int id);
        IList<OnLineParser> OnLineParser_GetAll();
        IList<OnLineParser> OnLineParser_GetAll(string SortExpression);
        PagedResponse<OnLineParser> OnLineParser_GetPaged(PagedRequest request);
        bool OnLineParser_DeleteById(int id);

        OnLineParser OnLineParser_GetByPath(string path, int CurrentMemberId);
        Int32 OnLineParserCount_GetByUserId(int id);
        #endregion
        //*********end*******************

        //*********Pravin khot – Candidate Hiring Status Update – 3/March/2017 – Start *****************

        [OperationContract]
        IList<CandidatesHireStatus> CandidateHiringStatusUpdate_GetAll(int ManagerId);

        [OperationContract]
        IList<CandidatesHireStatus> GetAllHiringStatusasperRoleId(int RoleId);

        [OperationContract]
        void CandidateHiringStatus_Update(int ID, int JobStatus);

        [OperationContract]
        void CandidHiringStatusRole_Save(int Roleid, int StatusID, int creatorId, bool IsRemovedFlag);

        [OperationContract]
        void SendCandidateStatusEmail(int ID, int CandidateId, int RequsitionStatus, int ManagerID, int JobPostingId, int AdminMemberid);

        [OperationContract]
        IList<CustomRole> GetAllCustomUserRole();

        //*********Pravin khot – Candidate Hiring Status Update – 3/March/2017 – End *****************

    }
}